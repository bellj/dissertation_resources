package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3DZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DZ {
    public Map A00 = C12970iu.A11();

    public final Map A00(String str) {
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(this.A00);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            C12990iw.A1Q(C12960it.A0c(String.valueOf(A15.getKey()), String.valueOf(str)), A11, A15);
        }
        return A11;
    }

    public String toString() {
        return AbstractC64703Go.A00(this.A00, 1);
    }
}
