package X;

/* renamed from: X.1gD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34481gD extends AnonymousClass1JQ {
    public final boolean A00;

    public C34481gD(AnonymousClass1JR r10, String str, long j, boolean z) {
        super(C27791Jf.A03, r10, str, "regular_low", 4, j, false);
        this.A00 = z;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34471gC.A02.A0T();
        boolean z = this.A00;
        A0T.A03();
        C34471gC r1 = (C34471gC) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = z;
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r2 = (C27831Jk) A01.A00;
        r2.A0P = (C34471gC) A0T.A02();
        r2.A00 |= 262144;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("UnarchiveChatsSettingMutation{rowId=");
        sb.append(this.A07);
        sb.append(", isUnarchiveChatsSettingEnabled=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
