package X;

import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.preference.WaFontListPreference;

/* renamed from: X.35S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35S extends AbstractC33641ei {
    public final FrameLayout A00;
    public final TextEmojiLabel A01;
    public final C21740xu A02;
    public final C14850m9 A03;
    public final AbstractC15340mz A04;
    public final C92784Xk A05 = new C92784Xk(4500);

    public AnonymousClass35S(C14900mE r13, AnonymousClass18U r14, C21740xu r15, AnonymousClass01d r16, AnonymousClass018 r17, C14850m9 r18, AnonymousClass1CH r19, AbstractC15340mz r20, C48242Fd r21) {
        super(r14, r13, r16, r17, r19, r21);
        this.A03 = r18;
        this.A02 = r15;
        AnonymousClass009.A05(r20);
        this.A04 = r20;
        TextEmojiLabel textEmojiLabel = new TextEmojiLabel(A02());
        this.A01 = textEmojiLabel;
        C12960it.A0s(A02(), textEmojiLabel, R.color.white);
        textEmojiLabel.setGravity(17);
        textEmojiLabel.setTextSize(AnonymousClass1OY.A02(A02().getResources(), r17, WaFontListPreference.A00));
        int A02 = (int) AnonymousClass1OY.A02(A02().getResources(), r17, WaFontListPreference.A00);
        textEmojiLabel.setPadding(A02, A02, A02, A02);
        FrameLayout frameLayout = new FrameLayout(A02());
        this.A00 = frameLayout;
        frameLayout.addView(textEmojiLabel, new FrameLayout.LayoutParams(-2, -2, 17));
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        return this.A05.A03;
    }

    @Override // X.AbstractC33641ei
    public void A07() {
        AbstractC33641ei.A00(this.A05, this);
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        this.A05.A02();
    }
}
