package X;

/* renamed from: X.1z9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44551z9 extends AbstractC16110oT {
    public Double A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public String A06;
    public String A07;

    public C44551z9() {
        super(976, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(10, this.A06);
        r3.Abe(3, this.A04);
        r3.Abe(9, this.A07);
        r3.Abe(5, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamChatDatabaseBackupEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "compressionRatio", this.A00);
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseBackupOverallResult", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseBackupVersion", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "freeDiskSpace", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "genericBackupFailureReason", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "msgstoreBackupSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sqliteVersion", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalBackupT", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
