package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import com.whatsapp.util.Log;

/* renamed from: X.3M3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M3 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(67);
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M3(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A04 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readString();
    }

    public AnonymousClass3M3(String str, String str2, String str3, String str4) {
        this.A04 = str;
        this.A02 = str2;
        this.A01 = str3;
        this.A03 = str4;
    }

    public byte[] A00() {
        String str = this.A03;
        byte[] bArr = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                bArr = Base64.decode(str, 0);
                return bArr;
            } catch (IllegalArgumentException e) {
                Log.e(e);
            }
        }
        return bArr;
    }

    @Override // java.lang.Object
    public String toString() {
        Object valueOf;
        StringBuilder A0k = C12960it.A0k("ShopEntityMetaData{id='");
        char A00 = C12990iw.A00(this.A00, A0k);
        A0k.append(", title='");
        A0k.append(this.A04);
        A0k.append(A00);
        A0k.append(", subTitle='");
        A0k.append(this.A02);
        A0k.append(A00);
        A0k.append(", imageUrl='");
        A0k.append(this.A01);
        A0k.append(A00);
        A0k.append(", thumbBase64Encoded='");
        String str = this.A03;
        if (str == null) {
            valueOf = "null";
        } else {
            valueOf = Integer.valueOf(str.length());
        }
        A0k.append(valueOf);
        A0k.append(A00);
        return C12970iu.A0v(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A04);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeString(this.A03);
    }
}
