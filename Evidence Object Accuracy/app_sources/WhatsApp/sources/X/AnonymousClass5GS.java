package X;

import javax.crypto.BadPaddingException;

/* renamed from: X.5GS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GS implements AnonymousClass5XV {
    public C94474bs A00;

    public AnonymousClass5GS(C94474bs r1) {
        this.A00 = r1;
    }

    public AnonymousClass5GS(AnonymousClass5XE r2, AnonymousClass5Wv r3) {
        this.A00 = new C114945Nr(r2, r3);
    }

    @Override // X.AnonymousClass5XV
    public int AHQ(int i) {
        return this.A00.A00(i);
    }

    @Override // X.AnonymousClass5XV
    public boolean AgA() {
        return !(this.A00 instanceof C114935Nq);
    }

    public AnonymousClass5GS(AnonymousClass5XE r3) {
        this.A00 = new C114945Nr(r3, new AnonymousClass5GD());
    }

    @Override // X.AnonymousClass5XV
    public int A97(byte[] bArr, int i) {
        int i2;
        C114945Nr r2;
        int i3;
        byte[] bArr2;
        try {
            C94474bs r22 = this.A00;
            if (r22 instanceof C114945Nr) {
                C114945Nr r23 = (C114945Nr) r22;
                AnonymousClass5XE r3 = r23.A01;
                int AAt = r3.AAt();
                boolean z = r23.A02;
                int i4 = ((C94474bs) r23).A00;
                if (z) {
                    if (i4 != AAt) {
                        i3 = 0;
                    } else if ((AAt << 1) + i <= bArr.length) {
                        i3 = r3.AZY(r23.A05, bArr, 0, i);
                        ((C94474bs) r23).A00 = 0;
                        i4 = 0;
                    } else {
                        r23.A01();
                        throw new C114975Nu("output buffer too short");
                    }
                    r23.A00.A5m(r23.A05, i4);
                    i2 = i3 + r23.A01.AZY(r23.A05, bArr, 0, i + i3);
                    r2 = r23;
                } else if (i4 == AAt) {
                    byte[] bArr3 = r23.A05;
                    int AZY = r3.AZY(bArr3, bArr3, 0, 0);
                    ((C94474bs) r23).A00 = 0;
                    i2 = AZY - r23.A00.AYq(r23.A05);
                    System.arraycopy(r23.A05, 0, bArr, i, i2);
                    r2 = r23;
                } else {
                    r23.A01();
                    throw new AnonymousClass5O2("last block incomplete in decryption");
                }
            } else if (!(r22 instanceof C114935Nq)) {
                int i5 = r22.A00;
                if (i + i5 <= bArr.length) {
                    i2 = 0;
                    r2 = r22;
                    if (i5 != 0) {
                        if (r22.A03) {
                            AnonymousClass5XE r1 = r22.A01;
                            byte[] bArr4 = r22.A05;
                            r1.AZY(bArr4, bArr4, 0, 0);
                            int i6 = r22.A00;
                            r22.A00 = 0;
                            System.arraycopy(r22.A05, 0, bArr, i, i6);
                            i2 = i6;
                            r2 = r22;
                        } else {
                            throw new AnonymousClass5O2("data not block size aligned");
                        }
                    }
                } else {
                    throw new C114975Nu("output buffer too short for doFinal()");
                }
            } else {
                int i7 = r22.A00;
                if (i7 + i <= bArr.length) {
                    AnonymousClass5XE r32 = r22.A01;
                    int AAt2 = r32.AAt();
                    int i8 = i7 - AAt2;
                    byte[] bArr5 = new byte[AAt2];
                    if (r22.A02) {
                        if (i7 >= AAt2) {
                            r32.AZY(r22.A05, bArr5, 0, 0);
                            int i9 = r22.A00;
                            if (i9 > AAt2) {
                                while (true) {
                                    bArr2 = r22.A05;
                                    if (i9 == bArr2.length) {
                                        break;
                                    }
                                    C72463ee.A0X(bArr5, bArr2, i9 - AAt2, i9);
                                    i9++;
                                }
                                for (int i10 = AAt2; i10 != i9; i10++) {
                                    C72453ed.A1S(bArr5, bArr2, i10 - AAt2, bArr2[i10], i10);
                                }
                                AnonymousClass5XE r12 = r22.A01;
                                if (r12 instanceof C112955Fl) {
                                    r12 = ((C112955Fl) r12).A01;
                                }
                                r12.AZY(bArr2, bArr, AAt2, i);
                                System.arraycopy(bArr5, 0, bArr, i + AAt2, i8);
                                i2 = r22.A00;
                                r2 = r22;
                            }
                            System.arraycopy(bArr5, 0, bArr, i, AAt2);
                            i2 = r22.A00;
                            r2 = r22;
                        } else {
                            throw new AnonymousClass5O2("need at least one block of input for CTS");
                        }
                    } else if (i7 >= AAt2) {
                        byte[] bArr6 = new byte[AAt2];
                        if (i7 > AAt2) {
                            if (r32 instanceof C112955Fl) {
                                ((C112955Fl) r32).A01.AZY(r22.A05, bArr5, 0, 0);
                            } else {
                                r32.AZY(r22.A05, bArr5, 0, 0);
                            }
                            for (int i11 = AAt2; i11 != r22.A00; i11++) {
                                int i12 = i11 - AAt2;
                                C72453ed.A1S(r22.A05, bArr6, i11, bArr5[i12], i12);
                            }
                            System.arraycopy(r22.A05, AAt2, bArr5, 0, i8);
                            r22.A01.AZY(bArr5, bArr, 0, i);
                            System.arraycopy(bArr6, 0, bArr, i + AAt2, i8);
                            i2 = r22.A00;
                            r2 = r22;
                        } else {
                            r32.AZY(r22.A05, bArr5, 0, 0);
                            System.arraycopy(bArr5, 0, bArr, i, AAt2);
                            i2 = r22.A00;
                            r2 = r22;
                        }
                    } else {
                        throw new AnonymousClass5O2("need at least one block of input for CTS");
                    }
                } else {
                    throw new C114975Nu("output buffer to small in doFinal");
                }
            }
            r2.A01();
            return i2;
        } catch (C114965Nt e) {
            throw new BadPaddingException(e.getMessage());
        }
    }

    @Override // X.AnonymousClass5XV
    public String AAf() {
        return this.A00.A01.AAf();
    }

    @Override // X.AnonymousClass5XV
    public int AEp(int i) {
        C94474bs r2 = this.A00;
        boolean z = r2 instanceof C114945Nr;
        int i2 = i + r2.A00;
        if (!z) {
            return i2;
        }
        int length = r2.A05.length;
        int i3 = i2 % length;
        if (i3 != 0) {
            i2 -= i3;
        } else if (!r2.A02) {
            return i2;
        }
        return i2 + length;
    }

    @Override // X.AnonymousClass5XV
    public AnonymousClass5XE AHO() {
        return this.A00.A01;
    }

    @Override // X.AnonymousClass5XV
    public void AIf(AnonymousClass20L r4, boolean z) {
        AnonymousClass5XE r0;
        C114945Nr r2;
        C94474bs r22 = this.A00;
        if (!(r22 instanceof C114945Nr)) {
            r22.A02 = z;
            r22.A01();
            r2 = r22;
        } else {
            C114945Nr r23 = (C114945Nr) r22;
            r23.A02 = z;
            r23.A01();
            if (r4 instanceof C113025Fs) {
                C113025Fs r42 = (C113025Fs) r4;
                r23.A00.AIb(r42.A00);
                r0 = r23.A01;
                r4 = r42.A01;
                r0.AIf(r4, z);
            }
            r23.A00.AIb(null);
            r2 = r23;
        }
        r0 = r2.A01;
        r0.AIf(r4, z);
    }

    @Override // X.AnonymousClass5XV
    public int AZZ(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int i4;
        byte[] bArr3;
        C94474bs r4 = this.A00;
        if (!(r4 instanceof C114945Nr)) {
            if (!(r4 instanceof C114935Nq)) {
                if (i2 >= 0) {
                    int AAt = r4.A01.AAt();
                    int A00 = r4.A00(i2);
                    if (A00 <= 0 || A00 + i3 <= bArr2.length) {
                        byte[] bArr4 = r4.A05;
                        int length = bArr4.length;
                        int i5 = r4.A00;
                        int i6 = length - i5;
                        if (i2 > i6) {
                            System.arraycopy(bArr, i, bArr4, i5, i6);
                            i4 = r4.A01.AZY(r4.A05, bArr2, 0, i3) + 0;
                            r4.A00 = 0;
                            i2 -= i6;
                            i += i6;
                            while (true) {
                                bArr4 = r4.A05;
                                if (i2 <= bArr4.length) {
                                    break;
                                }
                                i4 += r4.A01.AZY(bArr, bArr2, i, i3 + i4);
                                i2 -= AAt;
                                i += AAt;
                            }
                        } else {
                            i4 = 0;
                        }
                        System.arraycopy(bArr, i, bArr4, r4.A00, i2);
                        int i7 = r4.A00 + i2;
                        r4.A00 = i7;
                        byte[] bArr5 = r4.A05;
                        if (i7 == bArr5.length) {
                            int AZY = i4 + r4.A01.AZY(bArr5, bArr2, 0, i3 + i4);
                            r4.A00 = 0;
                            return AZY;
                        }
                        return i4;
                    }
                    throw new C114975Nu("output buffer too short");
                }
                throw C12970iu.A0f("Can't have a negative input length!");
            } else if (i2 >= 0) {
                int AAt2 = r4.A01.AAt();
                int A002 = r4.A00(i2);
                if (A002 <= 0 || A002 + i3 <= bArr2.length) {
                    byte[] bArr6 = r4.A05;
                    int length2 = bArr6.length;
                    int i8 = r4.A00;
                    int i9 = length2 - i8;
                    i4 = 0;
                    if (i2 > i9) {
                        System.arraycopy(bArr, i, bArr6, i8, i9);
                        int AZY2 = r4.A01.AZY(r4.A05, bArr2, 0, i3) + 0;
                        byte[] bArr7 = r4.A05;
                        System.arraycopy(bArr7, AAt2, bArr7, 0, AAt2);
                        r4.A00 = AAt2;
                        i2 -= i9;
                        i += i9;
                        while (i2 > AAt2) {
                            System.arraycopy(bArr, i, r4.A05, r4.A00, AAt2);
                            AZY2 += r4.A01.AZY(r4.A05, bArr2, 0, i3 + AZY2);
                            byte[] bArr8 = r4.A05;
                            System.arraycopy(bArr8, AAt2, bArr8, 0, AAt2);
                            i2 -= AAt2;
                            i += AAt2;
                        }
                        i4 = AZY2;
                    }
                    bArr3 = r4.A05;
                } else {
                    throw new C114975Nu("output buffer too short");
                }
            } else {
                throw C12970iu.A0f("Can't have a negative input length!");
            }
        } else if (i2 >= 0) {
            int AAt3 = r4.A01.AAt();
            int A003 = r4.A00(i2);
            if (A003 <= 0 || A003 + i3 <= bArr2.length) {
                bArr3 = r4.A05;
                int length3 = bArr3.length;
                int i10 = r4.A00;
                int i11 = length3 - i10;
                i4 = 0;
                if (i2 > i11) {
                    System.arraycopy(bArr, i, bArr3, i10, i11);
                    i4 = r4.A01.AZY(r4.A05, bArr2, 0, i3) + 0;
                    r4.A00 = 0;
                    i2 -= i11;
                    i += i11;
                    while (true) {
                        bArr3 = r4.A05;
                        if (i2 <= bArr3.length) {
                            break;
                        }
                        i4 += r4.A01.AZY(bArr, bArr2, i, i3 + i4);
                        i2 -= AAt3;
                        i += AAt3;
                    }
                }
            } else {
                throw new C114975Nu("output buffer too short");
            }
        } else {
            throw C12970iu.A0f("Can't have a negative input length!");
        }
        System.arraycopy(bArr, i, bArr3, r4.A00, i2);
        r4.A00 += i2;
        return i4;
    }

    @Override // X.AnonymousClass5XV
    public void AfL(byte[] bArr, int i, int i2) {
        throw C12980iv.A0u("AAD is not supported in the current mode.");
    }
}
