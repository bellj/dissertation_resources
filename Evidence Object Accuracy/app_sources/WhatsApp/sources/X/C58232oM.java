package X;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.whatsapp.registration.ChangeNumberNotifyContacts;
import com.whatsapp.registration.NotifyContactsSelector;

/* renamed from: X.2oM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58232oM extends AbstractC52172aN {
    public final /* synthetic */ ChangeNumberNotifyContacts A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58232oM(Context context, ChangeNumberNotifyContacts changeNumberNotifyContacts) {
        super(context);
        this.A00 = changeNumberNotifyContacts;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ChangeNumberNotifyContacts changeNumberNotifyContacts = this.A00;
        Intent A0D = C12990iw.A0D(changeNumberNotifyContacts, NotifyContactsSelector.class);
        A0D.putStringArrayListExtra("selected", C15380n4.A06(changeNumberNotifyContacts.A0G));
        changeNumberNotifyContacts.startActivityForResult(A0D, 1);
    }
}
