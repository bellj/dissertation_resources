package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0UE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UE {
    public static int A00 = Integer.MAX_VALUE;
    public static final int A01 = Math.max((int) (((float) Runtime.getRuntime().availableProcessors()) * 1.5f), 3);
    public static final Handler A02 = new Handler(Looper.getMainLooper());
    public static volatile AbstractC12510i2 A03;

    public static AbstractC12510i2 A00() {
        if (A03 == null) {
            synchronized (AnonymousClass0UE.class) {
                if (A03 == null) {
                    A03 = new C08550bO(A01);
                }
            }
        }
        return A03;
    }

    public static void A01(AnonymousClass0eL r3) {
        int i = A00;
        A00 = i - 1;
        r3.A00 = ((long) i) << 32;
        r3.A02 = null;
        r3.A01 = 0;
        ((C08550bO) A00()).A00.add(r3);
    }

    public static void A02(AnonymousClass0eL r3, String str) {
        int i = A00;
        A00 = i - 1;
        r3.A00 = ((long) i) << 32;
        r3.A02 = str;
        r3.A01 = 0;
        ((C08550bO) A00()).A00.add(r3);
    }
}
