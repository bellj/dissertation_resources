package X;

import android.content.ClipData;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContentInfo;

/* renamed from: X.0Xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07440Xz implements AbstractC12620iD {
    public final ContentInfo.Builder A00;

    public C07440Xz(ClipData clipData, int i) {
        this.A00 = new ContentInfo.Builder(clipData, i);
    }

    @Override // X.AbstractC12620iD
    public AnonymousClass0SR A6j() {
        return new AnonymousClass0SR(new AnonymousClass0Y1(this.A00.build()));
    }

    @Override // X.AbstractC12620iD
    public void Ac8(int i) {
        this.A00.setFlags(i);
    }

    @Override // X.AbstractC12620iD
    public void AcH(Uri uri) {
        this.A00.setLinkUri(uri);
    }

    @Override // X.AbstractC12620iD
    public void setExtras(Bundle bundle) {
        this.A00.setExtras(bundle);
    }
}
