package X;

import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1AE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AE implements AbstractC17450qp {
    public final AbstractC17450qp A00;
    public final Set A01;

    public AnonymousClass1AE(AbstractC17450qp r9, Set set) {
        HashMap hashMap = new HashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC17440qo r4 = (AbstractC17440qo) it.next();
            for (String str : r4.A00) {
                if (hashMap.containsKey(str)) {
                    String simpleName = getClass().getSimpleName();
                    StringBuilder sb = new StringBuilder("Duplicate support for action=");
                    sb.append(str);
                    sb.append(" by ");
                    sb.append(r4);
                    sb.append(" and ");
                    sb.append(hashMap.get(str));
                    Log.e(simpleName, sb.toString());
                }
                hashMap.put(str, r4);
            }
        }
        this.A01 = set;
        this.A00 = r9;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r5, C1093651k r6, C14240l5 r7) {
        for (AbstractC17440qo r2 : this.A01) {
            if (r2.A00.contains(r6.A00)) {
                return r2.A9j(r5, r6, r7);
            }
        }
        return this.A00.A9j(r5, r6, r7);
    }
}
