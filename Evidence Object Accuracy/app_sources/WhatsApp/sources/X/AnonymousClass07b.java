package X;

import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;

/* renamed from: X.07b  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07b implements Runnable {
    public final /* synthetic */ Toolbar A00;

    public AnonymousClass07b(Toolbar toolbar) {
        this.A00 = toolbar;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0XQ r0;
        ActionMenuView actionMenuView = this.A00.A0O;
        if (actionMenuView != null && (r0 = actionMenuView.A08) != null) {
            r0.A03();
        }
    }
}
