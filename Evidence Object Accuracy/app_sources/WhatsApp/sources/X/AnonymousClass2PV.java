package X;

/* renamed from: X.2PV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PV extends AnonymousClass1JW {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final String A02;
    public final boolean A03;

    public AnonymousClass2PV(AbstractC15710nm r2, C15450nH r3, AbstractC14640lm r4, AbstractC14640lm r5, String str, String str2, String str3, int i, boolean z, boolean z2) {
        super(r2, r3);
        int i2;
        this.A01 = r4;
        this.A00 = r5;
        this.A0G = str2;
        this.A0O = z;
        this.A03 = z2;
        super.A01 = i;
        this.A02 = str3;
        switch (str.hashCode()) {
            case -440536404:
                if (str.equals("media_message")) {
                    i2 = 23;
                    break;
                } else {
                    return;
                }
            case 3540562:
                if (str.equals("star")) {
                    i2 = 20;
                    break;
                } else {
                    return;
                }
            case 954925063:
                if (str.equals("message")) {
                    i2 = 4;
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        this.A05 = i2;
    }
}
