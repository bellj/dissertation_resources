package X;

import androidx.work.impl.WorkDatabase;
import java.util.LinkedList;

/* renamed from: X.0e7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractRunnableC10150e7 implements Runnable {
    public final C07580Zi A00 = new C07580Zi();

    public abstract void A00();

    public void A01(AnonymousClass022 r9, String str) {
        WorkDatabase workDatabase = r9.A04;
        AbstractC12700iM A0B = workDatabase.A0B();
        AbstractC11980hB A06 = workDatabase.A06();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            EnumC03840Ji AGv = A0B.AGv(str2);
            if (!(AGv == EnumC03840Ji.SUCCEEDED || AGv == EnumC03840Ji.FAILED)) {
                A0B.Act(EnumC03840Ji.CANCELLED, str2);
            }
            linkedList.addAll(A06.ACW(str2));
        }
        C07650Zp r7 = r9.A03;
        synchronized (r7.A09) {
            boolean z = true;
            C06390Tk.A00().A02(C07650Zp.A0B, String.format("Processor cancelling %s", str), new Throwable[0]);
            r7.A08.add(str);
            RunnableC10250eH r0 = (RunnableC10250eH) r7.A07.remove(str);
            if (r0 == null) {
                z = false;
                r0 = (RunnableC10250eH) r7.A06.remove(str);
            }
            C07650Zp.A00(r0, str);
            if (z) {
                r7.A01();
            }
        }
        for (AbstractC12570i8 r02 : r9.A07) {
            r02.A73(str);
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            A00();
            this.A00.A00(AbstractC12800iW.A01);
        } catch (Throwable th) {
            this.A00.A00(new AnonymousClass0GP(th));
        }
    }
}
