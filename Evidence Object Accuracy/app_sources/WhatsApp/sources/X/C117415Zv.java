package X;

import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;

/* renamed from: X.5Zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117415Zv extends CameraCaptureSession.CaptureCallback {
    public final C129415xg A00 = new C129415xg();
    public final C129425xh A01 = new C129425xh();
    public final AbstractC136416Ml A02;
    public final /* synthetic */ C1310360y A03;

    public C117415Zv(C1310360y r2, AbstractC136416Ml r3) {
        this.A03 = r2;
        this.A02 = r3;
    }

    @Override // android.hardware.camera2.CameraCaptureSession.CaptureCallback
    public void onCaptureCompleted(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, TotalCaptureResult totalCaptureResult) {
        super.onCaptureCompleted(cameraCaptureSession, captureRequest, totalCaptureResult);
        C129425xh r2 = this.A01;
        r2.A01(totalCaptureResult);
        this.A02.ANi(this.A03, r2);
    }

    @Override // android.hardware.camera2.CameraCaptureSession.CaptureCallback
    public void onCaptureFailed(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, CaptureFailure captureFailure) {
        super.onCaptureFailed(cameraCaptureSession, captureRequest, captureFailure);
        C129415xg r2 = this.A00;
        r2.A01(captureFailure);
        this.A02.ANj(r2, this.A03);
    }

    @Override // android.hardware.camera2.CameraCaptureSession.CaptureCallback
    public void onCaptureStarted(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, long j, long j2) {
        super.onCaptureStarted(cameraCaptureSession, captureRequest, j, j2);
        this.A02.ANk(captureRequest, this.A03, j, j2);
    }
}
