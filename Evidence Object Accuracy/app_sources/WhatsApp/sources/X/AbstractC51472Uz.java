package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;

/* renamed from: X.2Uz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51472Uz extends AnonymousClass2V0 {
    public AbstractC51472Uz(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
    }

    @Override // X.AnonymousClass2V0
    public CharSequence A02(C15370n3 r10, AbstractC15340mz r11) {
        Drawable A00;
        int i;
        String A04;
        Context context;
        int i2;
        Jid jid;
        if ((r10 == null || (jid = r10.A0D) == null || this.A08.A0F(jid)) && !r11.A0z.A02) {
            return "";
        }
        Context context2 = getContext();
        boolean z = r11.A0z.A02;
        C15570nT r4 = this.A08;
        C15610nY r5 = this.A0A;
        AnonymousClass018 r6 = this.A0F;
        CharSequence A01 = AnonymousClass3J0.A01(context2, r4, r5, r6, r10, z);
        boolean z2 = this instanceof AnonymousClass34d;
        if (z2) {
            A00 = C65023Hv.A00(getContext(), (C30421Xi) r11);
        } else if (!(this instanceof AnonymousClass34e)) {
            if (!(this instanceof AnonymousClass2Uy)) {
                boolean z3 = this instanceof AnonymousClass34c;
                context = getContext();
                if (!z3) {
                    i2 = R.drawable.msg_status_audio;
                } else {
                    i2 = R.drawable.msg_status_contact;
                }
            } else {
                context = getContext();
                i2 = R.drawable.msg_status_doc;
            }
            A00 = AnonymousClass2GE.A01(context, i2, R.color.msgStatusTint);
        } else {
            int i3 = R.drawable.ic_inline_live_location;
            if (r11 instanceof AnonymousClass1XO) {
                i3 = R.drawable.msg_status_location;
            }
            A00 = AnonymousClass2GE.A01(getContext(), i3, R.color.msgStatusTint);
        }
        if (!z2) {
            if (this instanceof AnonymousClass34e) {
                C63543Bz r1 = this.A0C;
                A04 = r1.A08;
                if (r11 instanceof AnonymousClass1XO) {
                    A04 = r1.A09;
                }
            } else if (this instanceof AnonymousClass2Uy) {
                C16440p1 r112 = (C16440p1) r11;
                if (TextUtils.isEmpty(r112.A01)) {
                    A04 = this.A0C.A02;
                } else {
                    A04 = r112.A01;
                }
            } else if (this instanceof AnonymousClass34c) {
                A04 = this.A0C.A01;
            }
            return AnonymousClass3J0.A02(A01, C52252aV.A01(((AnonymousClass2V0) this).A01.getPaint(), A00, A04));
        }
        AbstractC16130oV r113 = (AbstractC16130oV) r11;
        if (C30041Vv.A12(r113) || C30041Vv.A13(r113)) {
            i = r113.A00;
        } else {
            i = r113.A00;
            if (i == 0) {
                A04 = C44891zj.A03(r6, r113.A01);
                return AnonymousClass3J0.A02(A01, C52252aV.A01(((AnonymousClass2V0) this).A01.getPaint(), A00, A04));
            }
        }
        A04 = C38131nZ.A04(r6, (long) i);
        return AnonymousClass3J0.A02(A01, C52252aV.A01(((AnonymousClass2V0) this).A01.getPaint(), A00, A04));
    }
}
