package X;

import X.AbstractC11240fy;
import X.AnonymousClass03l;
import X.C07310Xm;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.1hP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35191hP implements AbstractC35201hQ, AnonymousClass11Q {
    public static int A0x;
    public static C246516i A0y = new C246516i(250);
    public static byte[] A0z;
    public static String[] A10 = {"GT-I9505", "GT-I9506", "GT-I9505G", "SGH-I337", "SGH-M919", "SCH-I545", "SPH-L720", "SCH-R970", "GT-I9508", "SGH-N045", "SC-04E"};
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A = 0;
    public long A0B;
    public Activity A0C;
    public Sensor A0D;
    public SensorEventListener A0E;
    public SensorManager A0F;
    public AudioManager.OnAudioFocusChangeListener A0G;
    public MediaPlayer A0H;
    public Visualizer A0I;
    public AbstractC116055Ty A0J;
    public AnonymousClass2MF A0K;
    public AbstractC116065Tz A0L;
    public C14820m6 A0M;
    public AnonymousClass4QW A0N;
    public C30421Xi A0O;
    public AbstractC28651Ol A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public final Handler A0a = new AnonymousClass2a5(Looper.getMainLooper(), this);
    public final PowerManager.WakeLock A0b;
    public final C236912q A0c;
    public final AbstractC15710nm A0d;
    public final C14900mE A0e;
    public final AnonymousClass11R A0f;
    public final AnonymousClass19B A0g;
    public final AnonymousClass11P A0h;
    public final AnonymousClass01d A0i;
    public final C16590pI A0j;
    public final C15890o4 A0k;
    public final AnonymousClass12H A0l;
    public final C14850m9 A0m;
    public final C22050yP A0n;
    public final AnonymousClass19C A0o;
    public final C21300xC A0p;
    public final AnonymousClass199 A0q;
    public final AnonymousClass2SY A0r = new C865547w(this);
    public final AnonymousClass19A A0s;
    public final AnonymousClass01H A0t;
    public final boolean A0u;
    public final boolean A0v;
    public volatile boolean A0w;

    public C35191hP(Activity activity, C236912q r6, AbstractC15710nm r7, C14900mE r8, AnonymousClass11R r9, AnonymousClass19B r10, AnonymousClass11P r11, AnonymousClass01d r12, C16590pI r13, C15890o4 r14, C14820m6 r15, AnonymousClass12H r16, C14850m9 r17, C22050yP r18, AnonymousClass19C r19, C21300xC r20, AnonymousClass199 r21, AnonymousClass19A r22, AnonymousClass01H r23, boolean z, boolean z2) {
        this.A0C = activity;
        this.A0j = r13;
        this.A0m = r17;
        this.A0e = r8;
        this.A0d = r7;
        this.A0q = r21;
        this.A0i = r12;
        this.A0p = r20;
        this.A0l = r16;
        this.A0n = r18;
        this.A0s = r22;
        this.A0k = r14;
        this.A0M = r15;
        this.A0g = r10;
        this.A0c = r6;
        this.A0o = r19;
        this.A0f = r9;
        this.A0h = r11;
        this.A0t = r23;
        this.A0Y = z;
        this.A0u = z2;
        if (A00() != -1) {
            PowerManager A0I = r12.A0I();
            if (A0I == null) {
                Log.w("messageaudioplayer pm=null");
            } else {
                this.A0b = C39151pN.A00(A0I, "WhatsApp MessageAudioPlayer ProximityWakeLock", A00());
            }
        }
        this.A0v = AbstractC28651Ol.A01(r17);
    }

    public static final int A00() {
        if (Build.VERSION.SDK_INT >= 21) {
            return 32;
        }
        try {
            return PowerManager.class.getDeclaredField("PROXIMITY_SCREEN_OFF_WAKE_LOCK").getInt(null);
        } catch (IllegalAccessException e) {
            Log.w("unable to access PROXIMITY_SCREEN_OFF_WAKE_LOCK field in PowerManager", e);
            return -1;
        } catch (NoSuchFieldException e2) {
            Log.w("no PROXIMITY_SCREEN_OFF_WAKE_LOCK field in PowerManager", e2);
            return -1;
        }
    }

    public static void A01(C30421Xi r3, int i) {
        A0y.put(r3.A0z, Integer.valueOf(i));
    }

    public int A02() {
        AbstractC28651Ol r0 = this.A0P;
        if (r0 == null) {
            return 0;
        }
        return Math.max(this.A04, r0.A02());
    }

    public final void A03() {
        AudioManager A0G = this.A0i.A0G();
        if (A0G != null) {
            AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = this.A0G;
            if (onAudioFocusChangeListener == null) {
                onAudioFocusChangeListener = new AnonymousClass3LV(this.A0h);
                this.A0G = onAudioFocusChangeListener;
            }
            A0G.abandonAudioFocus(onAudioFocusChangeListener);
        }
    }

    public final void A04() {
        C35191hP A00 = this.A0h.A00();
        if (A00 != this && A00 != null) {
            if (this.A0m.A07(952)) {
                A00.A0H(false, false);
            } else {
                A00.A0H(true, false);
            }
        }
    }

    public final void A05() {
        int i;
        if (this.A0I == null && !this.A0u && this.A0L != null && (i = Build.VERSION.SDK_INT) != 26) {
            if ((i != 28 || !Build.MANUFACTURER.equals("Xiaomi") || !Build.MODEL.equals("Mi 9 Lite")) && this.A0k.A00.A00.checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") == 0) {
                try {
                    Visualizer visualizer = new Visualizer(0);
                    this.A0I = visualizer;
                    visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
                    this.A0I.setDataCaptureListener(new C98184iH(this), Visualizer.getMaxCaptureRate() / 2, true, true);
                    this.A0I.setEnabled(true);
                } catch (RuntimeException e) {
                    Log.e("visualmediaplayer/start ", e);
                }
            }
        }
    }

    public final void A06() {
        if (this.A0Y) {
            boolean z = this.A0Q;
            AnonymousClass19C r1 = this.A0o;
            if (z) {
                r1.A01(this);
            } else {
                r1.A06 = true;
                r1.A0F.A04(14, null);
            }
        }
        AnonymousClass2MF r0 = this.A0K;
        if (r0 != null && r0.ACr().A0z.equals(this.A0O.A0z)) {
            this.A0K.ATS(A02());
        }
        A01(this.A0O, A02());
        this.A0w = false;
        this.A0c.A07(this);
        this.A0f.A01.remove(this);
    }

    public final void A07() {
        PowerManager.WakeLock wakeLock = this.A0b;
        if (wakeLock != null && !wakeLock.isHeld() && !this.A0f.A00 && !this.A0c.A08() && this.A0P.A0D() && !this.A0Q) {
            wakeLock.acquire();
            Log.i("messageaudioplayer/startProximityListener acquired proximityWakeLock");
        }
    }

    public final void A08() {
        PowerManager.WakeLock wakeLock = this.A0b;
        if (wakeLock != null && wakeLock.isHeld()) {
            if (Build.VERSION.SDK_INT >= 21) {
                wakeLock.release(1);
            } else {
                wakeLock.release();
            }
            Log.i("messageaudioplayer/stopproximitylistener released proximityWakeLock");
        }
    }

    public void A09(int i) {
        try {
            AbstractC28651Ol r0 = this.A0P;
            if (r0 != null) {
                r0.A0A(i);
            }
            this.A04 = i;
        } catch (IOException e) {
            Log.e(e);
        }
    }

    public void A0A(int i, boolean z, boolean z2) {
        String string;
        int AFz;
        int i2;
        A04();
        this.A0p.A00();
        if (this.A0C != null && !this.A0q.A02()) {
            this.A0C.setVolumeControlStream(3);
        }
        AudioManager A0G = this.A0i.A0G();
        if (A0G != null) {
            AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = this.A0G;
            if (onAudioFocusChangeListener == null) {
                onAudioFocusChangeListener = new AnonymousClass3LV(this.A0h);
                this.A0G = onAudioFocusChangeListener;
            }
            int i3 = 1;
            if (((AbstractC15340mz) this.A0O).A08 == 1) {
                i3 = 3;
            }
            A0G.requestAudioFocus(onAudioFocusChangeListener, 3, i3);
        }
        this.A0V = false;
        this.A0B = System.currentTimeMillis();
        if (this.A0P == null) {
            Log.i("messageaudioplayer/start");
            if (this.A0L != null && (i2 = Build.VERSION.SDK_INT) >= 17 && i2 <= 18 && "samsung".equals(Build.MANUFACTURER) && C37871n9.A01(Build.MODEL, A10)) {
                try {
                    C16150oX r2 = ((AbstractC16130oV) this.A0O).A02;
                    AnonymousClass009.A05(r2);
                    if (r2.A0F != null) {
                        MediaPlayer mediaPlayer = new MediaPlayer();
                        this.A0H = mediaPlayer;
                        mediaPlayer.setDataSource(r2.A0F.getAbsolutePath());
                        this.A0H.setAudioStreamType(3);
                        this.A0H.prepare();
                    }
                } catch (IOException e) {
                    Log.e(e);
                    this.A0d.AaV("messageaudioplayer/failed to prepare silentPlayer ", e.toString(), true);
                }
            }
            try {
                C16150oX r0 = ((AbstractC16130oV) this.A0O).A02;
                AnonymousClass009.A05(r0);
                File file = r0.A0F;
                if (file != null) {
                    this.A02 = 3;
                    AbstractC28651Ol A00 = AbstractC28651Ol.A00(this.A0j, this.A0m, file, 3);
                    this.A0P = A00;
                    try {
                        A00.A05();
                        if (!A0J(i)) {
                            A0x = 0;
                        }
                        if (z && (AFz = ((AnonymousClass19F) this.A0t.get()).AFz(this.A0O.A11)) >= 0) {
                            this.A04 = AFz;
                        }
                        this.A0P.A0A(this.A04);
                        this.A0P.A08();
                        AbstractC28651Ol r1 = this.A0P;
                        if (r1 instanceof AnonymousClass36Q) {
                            this.A03 = ((AbstractC16130oV) this.A0O).A00 * 1000;
                            r1.A0C(new AnonymousClass4KR(this));
                        } else {
                            this.A03 = r1.A03();
                        }
                        this.A0a.sendEmptyMessage(0);
                        C30421Xi r22 = this.A0O;
                        if (C246316g.A02(r22)) {
                            this.A0l.A08(r22, 25);
                        }
                        A05();
                        if (this.A0Y) {
                            AnonymousClass19C r11 = this.A0o;
                            C30421Xi r10 = this.A0O;
                            if (r10 != null && r10.A0y == 2) {
                                boolean z3 = true;
                                if (r11.A02 != r10) {
                                    r11.A02 = r10;
                                    Bitmap bitmap = null;
                                    r11.A03 = null;
                                    Context context = r11.A0E.A00;
                                    C005602s A002 = C22630zO.A00(context);
                                    A002.A0J = "media_playback@1";
                                    A002.A08(new AbstractC006703e() { // from class: androidx.core.app.NotificationCompat$DecoratedCustomViewStyle
                                        @Override // X.AbstractC006703e
                                        public String A05() {
                                            return "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle";
                                        }

                                        @Override // X.AbstractC006703e
                                        public RemoteViews A03(AbstractC11240fy r4) {
                                            RemoteViews remoteViews;
                                            if (Build.VERSION.SDK_INT >= 24 || (remoteViews = this.A00.A0E) == null) {
                                                return null;
                                            }
                                            return A09(remoteViews, true);
                                        }

                                        @Override // X.AbstractC006703e
                                        public RemoteViews A04(AbstractC11240fy r4) {
                                            RemoteViews remoteViews;
                                            if (Build.VERSION.SDK_INT >= 24 || (remoteViews = this.A00.A0E) == null) {
                                                return null;
                                            }
                                            return A09(remoteViews, false);
                                        }

                                        @Override // X.AbstractC006703e
                                        public void A08(AbstractC11240fy r3) {
                                            if (Build.VERSION.SDK_INT >= 24) {
                                                ((C07310Xm) r3).A02.setStyle(new Notification.DecoratedCustomViewStyle());
                                            }
                                        }

                                        public final RemoteViews A09(RemoteViews remoteViews, boolean z4) {
                                            ArrayList arrayList;
                                            int min;
                                            int i4 = 0;
                                            RemoteViews A02 = A02();
                                            A02.removeAllViews(R.id.actions);
                                            ArrayList arrayList2 = this.A00.A0N;
                                            if (arrayList2 == null) {
                                                arrayList = null;
                                            } else {
                                                arrayList = new ArrayList();
                                                Iterator it = arrayList2.iterator();
                                                while (it.hasNext()) {
                                                    arrayList.add(it.next());
                                                }
                                            }
                                            if (!z4 || arrayList == null || (min = Math.min(arrayList.size(), 3)) <= 0) {
                                                i4 = 8;
                                            } else {
                                                int i5 = 0;
                                                do {
                                                    AnonymousClass03l r102 = (AnonymousClass03l) arrayList.get(i5);
                                                    PendingIntent pendingIntent = r102.A01;
                                                    boolean z5 = false;
                                                    if (pendingIntent == null) {
                                                        z5 = true;
                                                    }
                                                    String packageName = this.A00.A0B.getPackageName();
                                                    int i6 = R.layout.notification_action;
                                                    if (z5) {
                                                        i6 = R.layout.notification_action_tombstone;
                                                    }
                                                    RemoteViews remoteViews2 = new RemoteViews(packageName, i6);
                                                    IconCompat A003 = r102.A00();
                                                    if (A003 != null) {
                                                        remoteViews2.setImageViewBitmap(R.id.action_image, A01(A003, this.A00.A0B.getResources().getColor(R.color.notification_action_color_filter), 0));
                                                    }
                                                    CharSequence charSequence = r102.A03;
                                                    remoteViews2.setTextViewText(R.id.action_text, charSequence);
                                                    if (!z5) {
                                                        remoteViews2.setOnClickPendingIntent(R.id.action_container, pendingIntent);
                                                    }
                                                    remoteViews2.setContentDescription(R.id.action_container, charSequence);
                                                    A02.addView(R.id.actions, remoteViews2);
                                                    i5++;
                                                } while (i5 < min);
                                            }
                                            A02.setViewVisibility(R.id.actions, i4);
                                            A02.setViewVisibility(R.id.action_divider, i4);
                                            A07(A02, remoteViews);
                                            return A02;
                                        }
                                    });
                                    A002.A0V = false;
                                    r11.A01 = A002;
                                    C18360sK.A01(A002, R.drawable.notifybar);
                                    int dimensionPixelSize = context.getResources().getDimensionPixelSize(17104901);
                                    int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(17104902);
                                    if (r10.A0z.A02) {
                                        C15570nT r02 = r11.A07;
                                        r02.A08();
                                        C27621Ig r12 = r02.A01;
                                        if (r12 != null) {
                                            bitmap = r11.A0B.A02(context, r12, dimensionPixelSize, dimensionPixelSize2);
                                        }
                                        int i4 = ((AbstractC15340mz) r10).A08;
                                        int i5 = R.string.conversations_most_recent_audio;
                                        if (i4 == 1) {
                                            i5 = R.string.conversations_most_recent_voice;
                                        }
                                        string = context.getString(i5);
                                    } else {
                                        C15550nR r13 = r11.A09;
                                        UserJid A0C = r10.A0C();
                                        AnonymousClass009.A05(A0C);
                                        C15370n3 A0B = r13.A0B(A0C);
                                        bitmap = r11.A0B.A02(context, A0B, dimensionPixelSize, dimensionPixelSize2);
                                        String A0A = r11.A0A.A0A(A0B, -1);
                                        int i6 = ((AbstractC15340mz) r10).A08;
                                        int i7 = R.string.notification_audio_message_from;
                                        if (i6 == 1) {
                                            i7 = R.string.notification_voice_message_from;
                                        }
                                        string = context.getString(i7, A0A);
                                    }
                                    r11.A03 = string;
                                    if (bitmap == null) {
                                        bitmap = AnonymousClass130.A00(r11.A08.A01.A00, (float) dimensionPixelSize2, R.drawable.avatar_contact, dimensionPixelSize);
                                    }
                                    r11.A01.A06(bitmap);
                                }
                                r11.A02 = r10;
                                AccessibilityManager A0P = r11.A0D.A0P();
                                if (A0P == null || !A0P.isTouchExplorationEnabled()) {
                                    z3 = false;
                                }
                                r11.A04 = z3;
                                r11.A06 = false;
                                r11.A05 = false;
                            }
                            r11.A01(this);
                        }
                        AnonymousClass2MF r03 = this.A0K;
                        if (r03 != null && r03.ACr().A0z.equals(this.A0O.A0z)) {
                            this.A0K.AWG(this.A03);
                        }
                        A0y.remove(this.A0O.A0z);
                        this.A0s.A03(this.A0r);
                        this.A0c.A06(this);
                        this.A0f.A01.add(this);
                        this.A0g.A05();
                        this.A0w = true;
                        A0B(this);
                        this.A0h.A09(this, z2);
                        return;
                    } catch (IOException | IllegalStateException e2) {
                        this.A0d.AaV("messageaudioplayer/failed to prepare mediaplayer", e2.toString(), true);
                        throw e2;
                    }
                }
            } catch (IOException | IllegalStateException e3) {
                Log.e(e3);
            }
            this.A0e.A07(R.string.gallery_audio_cannot_load, 0);
            this.A0V = true;
            A0H(true, false);
            return;
        }
        Log.i("messageaudioplayer/resume");
        try {
            this.A0P.A0A(this.A04);
            if (!A0J(i)) {
                A0x = 0;
            }
            this.A0P.A08();
            this.A0U = false;
            this.A0a.sendEmptyMessage(0);
            A0B(this);
            A05();
            if (this.A0Y) {
                this.A0o.A01(this);
            }
            AnonymousClass2MF r04 = this.A0K;
            if (r04 != null && r04.ACr().A0z.equals(this.A0O.A0z)) {
                this.A0K.AVR();
            }
            A0y.remove(this.A0O.A0z);
            this.A0g.A05();
            this.A0w = true;
            this.A0c.A06(this);
            this.A0f.A01.add(this);
        } catch (IOException | IllegalStateException unused) {
            this.A0e.A07(R.string.gallery_audio_cannot_load, 0);
            this.A0V = true;
            A0H(true, false);
        }
    }

    public final void A0B(C35191hP r5) {
        A07();
        if (this.A0F == null) {
            SensorManager A0D = this.A0i.A0D();
            this.A0F = A0D;
            if (A0D != null) {
                Sensor defaultSensor = A0D.getDefaultSensor(8);
                this.A0D = defaultSensor;
                if (defaultSensor != null) {
                    C97934hs r2 = new C97934hs(this, r5);
                    this.A0E = r2;
                    this.A0F.registerListener(r2, defaultSensor, 2);
                }
            }
        }
    }

    public void A0C(C30421Xi r3) {
        Number number = (Number) A0y.get(r3.A0z);
        if (number != null) {
            A09(number.intValue());
        }
    }

    public void A0D(boolean z) {
        Activity activity;
        float f;
        AbstractC28651Ol r0;
        if (this.A0f.A00 || this.A0c.A08()) {
            z = false;
        }
        if (this.A0R == z) {
            return;
        }
        if (!z || ((r0 = this.A0P) != null && r0.A0D())) {
            StringBuilder sb = new StringBuilder("messageaudioplayer/onearproximity ");
            sb.append(z);
            Log.i(sb.toString());
            this.A0R = z;
            if (this.A0b == null && (activity = this.A0C) != null) {
                Window window = activity.getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                if (z) {
                    this.A01 = attributes.screenBrightness;
                    f = 0.1f;
                } else {
                    f = this.A01;
                }
                attributes.screenBrightness = f;
                window.setAttributes(attributes);
            }
            AnonymousClass2MF r02 = this.A0K;
            if (r02 != null) {
                r02.APa(z);
            }
            AbstractC28651Ol r03 = this.A0P;
            if (r03 != null && r03.A0D()) {
                int A02 = this.A0P.A02();
                this.A0P.A06();
                this.A0P = null;
                C16150oX r04 = ((AbstractC16130oV) this.A0O).A02;
                AnonymousClass009.A05(r04);
                int i = 3;
                if (z) {
                    i = 0;
                }
                try {
                    this.A02 = i;
                    this.A0P = AbstractC28651Ol.A00(this.A0j, this.A0m, r04.A0F, i);
                    Activity activity2 = this.A0C;
                    if (activity2 != null) {
                        activity2.setVolumeControlStream(this.A02);
                    }
                    AudioManager A0G = this.A0i.A0G();
                    if (z && A0G != null) {
                        A0G.setSpeakerphoneOn(false);
                    }
                    this.A0P.A05();
                    int max = Math.max(0, A02 - 1000);
                    this.A04 = max;
                    this.A0P.A0A(max);
                    if (z || System.currentTimeMillis() - this.A0B < 1500) {
                        if (!A0J(A0x)) {
                            A0x = 0;
                        }
                        this.A0P.A08();
                        return;
                    }
                    this.A0U = true;
                    A06();
                    A08();
                    A03();
                } catch (IOException | IllegalStateException unused) {
                    this.A0e.A07(R.string.gallery_audio_cannot_load, 0);
                }
            }
        }
    }

    public void A0E(boolean z) {
        AbstractC28651Ol r0 = this.A0P;
        if (r0 != null && r0.A0D()) {
            this.A0P.A04();
            ((AnonymousClass19F) this.A0t.get()).Ac1();
            this.A0U = true;
            Visualizer visualizer = this.A0I;
            if (visualizer != null) {
                visualizer.setEnabled(false);
                this.A0I.release();
                this.A0I = null;
            }
            A06();
            A08();
            if (!z) {
                A0G(false);
            } else {
                float min = Math.min(((float) A02()) / ((float) this.A03), 1.0f);
                if (min > this.A00) {
                    this.A00 = min;
                }
            }
        }
        A03();
    }

    public void A0F(boolean z) {
        A04();
        AbstractC28651Ol r0 = this.A0P;
        if (r0 == null || !r0.A0D()) {
            int i = 0;
            if (z) {
                i = A0x;
            }
            A0A(i, true, false);
            return;
        }
        A0E(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002e, code lost:
        if ((r0 instanceof X.AnonymousClass47U) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0G(boolean r23) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35191hP.A0G(boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0012, code lost:
        if ((A02() + 50) > r9.A03) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H(boolean r10, boolean r11) {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35191hP.A0H(boolean, boolean):void");
    }

    public boolean A0I() {
        AbstractC28651Ol r0 = this.A0P;
        return r0 != null && r0.A0D();
    }

    public boolean A0J(int i) {
        float f;
        if (i == 0) {
            f = 1.0f;
        } else if (i == 1) {
            f = 1.5f;
        } else if (i == 2) {
            f = 2.0f;
        } else {
            StringBuilder sb = new StringBuilder("setFastPlaybackPlayerState: Did not handle FastPlaybackPlayerState: ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("messageaudioplayer/setFastPlaybackPlayerState fastPlaybackPlayerState: ");
        sb2.append(i);
        Log.i(sb2.toString());
        AbstractC28651Ol r1 = this.A0P;
        if (r1 == null || !(r1 instanceof AnonymousClass36Q) || !this.A0v) {
            return true;
        }
        return r1.A0E(this.A0d, f);
    }

    @Override // X.AbstractC35201hQ
    public void ANJ(int i) {
        if (i == 0) {
            A07();
        } else if (i == 2) {
            A08();
        }
    }

    @Override // X.AnonymousClass11Q
    public void ARE(boolean z) {
        if (z) {
            A08();
        } else {
            A07();
        }
    }
}
