package X;

import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;

/* renamed from: X.5bQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118195bQ extends AnonymousClass0Yo {
    public final /* synthetic */ BrazilPaymentCardDetailsActivity A00;

    public C118195bQ(BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity) {
        this.A00 = brazilPaymentCardDetailsActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123585nO.class)) {
            BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity = this.A00;
            C14830m7 r1 = ((ActivityC13790kL) brazilPaymentCardDetailsActivity).A05;
            AbstractC14440lR r10 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A0H;
            AbstractC28901Pl r2 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A09;
            C17070qD r5 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A0D;
            AnonymousClass605 r8 = brazilPaymentCardDetailsActivity.A0A;
            C21860y6 r3 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A0A;
            C130015yf r9 = brazilPaymentCardDetailsActivity.A0B;
            return new C123585nO(r1, r2, r3, ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A06, r5, brazilPaymentCardDetailsActivity.A08, brazilPaymentCardDetailsActivity.A09, r8, r9, r10);
        }
        throw C12970iu.A0f("View Model type mismatch. Expected a BrazilPaymentCardDetailsViewModel");
    }
}
