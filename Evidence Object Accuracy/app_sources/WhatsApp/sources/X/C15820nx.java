package X;

import android.text.TextUtils;
import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15820nx {
    public static final byte[] A0A = "backup encryption".getBytes();
    public static final byte[] A0B = "metadata authentication".getBytes();
    public static final byte[] A0C = "metadata encryption".getBytes();
    public final C20670w8 A00;
    public final C15900o5 A01;
    public final C17050qB A02;
    public final C14820m6 A03;
    public final C14850m9 A04;
    public final C244315m A05;
    public final C32881ct A06 = new C32881ct(new Random(), 3, 200);
    public final AbstractC14440lR A07;
    public final JniBridge A08;
    public final C21710xr A09;

    public C15820nx(C20670w8 r6, C15900o5 r7, C17050qB r8, C14820m6 r9, C14850m9 r10, C244315m r11, AbstractC14440lR r12, JniBridge jniBridge, C21710xr r14) {
        this.A04 = r10;
        this.A07 = r12;
        this.A08 = jniBridge;
        this.A00 = r6;
        this.A09 = r14;
        this.A02 = r8;
        this.A01 = r7;
        this.A03 = r9;
        this.A05 = r11;
    }

    public String A00(String str) {
        if (!TextUtils.isEmpty(str) && A04()) {
            try {
                C15900o5 r4 = this.A01;
                byte[] A03 = r4.A03();
                boolean z = false;
                if (A03 != null) {
                    z = true;
                }
                AnonymousClass009.A0C("root key is not defined", z);
                byte[] A00 = C32891cu.A00(A03, A0C, 32);
                byte[] A032 = r4.A03();
                boolean z2 = false;
                if (A032 != null) {
                    z2 = true;
                }
                AnonymousClass009.A0C("root key is not defined", z2);
                byte[] A002 = C32891cu.A00(A032, A0B, 32);
                boolean z3 = true;
                boolean z4 = false;
                if (A00.length == 32) {
                    z4 = true;
                }
                AnonymousClass009.A0B("wrong length of enc key", z4);
                boolean z5 = false;
                if (A002.length == 32) {
                    z5 = true;
                }
                AnonymousClass009.A0B("wrong length of auth key", z5);
                ByteBuffer wrap = ByteBuffer.wrap(Base64.decode(str, 2));
                int i = wrap.get();
                boolean z6 = false;
                if (i == 16) {
                    z6 = true;
                }
                StringBuilder sb = new StringBuilder("unexpected size of iv (");
                sb.append(i);
                sb.append(")");
                AnonymousClass009.A0B(sb.toString(), z6);
                byte[] bArr = new byte[i];
                wrap.get(bArr);
                int i2 = wrap.get();
                if (i2 != 32) {
                    z3 = false;
                }
                StringBuilder sb2 = new StringBuilder("unexpected size of mac (");
                sb2.append(i2);
                sb2.append(")");
                AnonymousClass009.A0B(sb2.toString(), z3);
                byte[] bArr2 = new byte[i2];
                wrap.get(bArr2);
                byte[] bArr3 = new byte[wrap.remaining()];
                wrap.get(bArr3);
                Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                instance.init(new SecretKeySpec(A002, DefaultCrypto.HMAC_SHA256));
                instance.update(bArr);
                instance.update(bArr3);
                if (MessageDigest.isEqual(instance.doFinal(), bArr2)) {
                    Cipher instance2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
                    instance2.init(2, new SecretKeySpec(A00, "AES"), new IvParameterSpec(bArr));
                    return new String(instance2.doFinal(bArr3));
                }
                throw new SecurityException("cannot authenticate");
            } catch (SecurityException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
                Log.e("EncBackupManager/failed to decrypt backup metadata", e);
            }
        }
        return null;
    }

    public String A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!A04()) {
            return str;
        }
        try {
            C15900o5 r5 = this.A01;
            byte[] A03 = r5.A03();
            boolean z = false;
            if (A03 != null) {
                z = true;
            }
            AnonymousClass009.A0C("root key is not defined", z);
            byte[] A00 = C32891cu.A00(A03, A0C, 32);
            byte[] A032 = r5.A03();
            boolean z2 = false;
            if (A032 != null) {
                z2 = true;
            }
            AnonymousClass009.A0C("root key is not defined", z2);
            byte[] A002 = C32891cu.A00(A032, A0B, 32);
            byte[] A0D = C003501n.A0D(16);
            boolean z3 = false;
            boolean z4 = false;
            if (A00.length == 32) {
                z4 = true;
            }
            AnonymousClass009.A0B("wrong length of enc key", z4);
            boolean z5 = false;
            if (A002.length == 32) {
                z5 = true;
            }
            AnonymousClass009.A0B("wrong length of auth key", z5);
            int length = A0D.length;
            if (length == 16) {
                z3 = true;
            }
            AnonymousClass009.A0B("wrong length of iv", z3);
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, new SecretKeySpec(A00, "AES"), new IvParameterSpec(A0D));
            byte[] doFinal = instance.doFinal(str.getBytes());
            Mac instance2 = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance2.init(new SecretKeySpec(A002, DefaultCrypto.HMAC_SHA256));
            instance2.update(A0D);
            instance2.update(doFinal);
            byte[] doFinal2 = instance2.doFinal();
            int length2 = doFinal2.length;
            ByteBuffer allocate = ByteBuffer.allocate(length + 1 + 1 + length2 + doFinal.length);
            allocate.put((byte) length);
            allocate.put(A0D);
            allocate.put((byte) length2);
            allocate.put(doFinal2);
            allocate.put(doFinal);
            return Base64.encodeToString(allocate.array(), 2);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            Log.e("EncBackupManager/failed to encrypt backup metadata", e);
            return null;
        }
    }

    public void A02() {
        C14820m6 r1 = this.A03;
        r1.A14(false);
        r1.A15(false);
        r1.A0X(0);
        r1.A16(false);
        C14350lI.A0M(new File(this.A01.A00.A00.getFilesDir(), "encrypted_backup.key"));
        Log.i("EncBackupManager/encrypted backup has been disabled");
    }

    public void A03(String str) {
        byte[] bytes = str.getBytes();
        byte[] A0D = C003501n.A0D(64);
        try {
            this.A01.A01(new C16560pF(C003501n.A05(bytes, A0D, 100000).getEncoded(), A0D, 100000));
            this.A03.A00.edit().putInt("encrypted_backup_num_attempts_remaining", 5).apply();
        } catch (IOException e) {
            Log.e("EncBackupManager/storePasswordHash failed", e);
        }
    }

    public boolean A04() {
        return this.A03.A00.getBoolean("encrypted_backup_enabled", false);
    }

    public boolean A05() {
        return this.A04.A07(576) || A04();
    }
}
