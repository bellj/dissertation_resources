package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.WorkDatabase_Impl;
import java.util.List;

/* renamed from: X.09q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020309q extends SQLiteOpenHelper {
    public boolean A00;
    public final AnonymousClass0SX A01;
    public final AnonymousClass0ZE[] A02;

    public C020309q(Context context, AnonymousClass0SX r8, String str, AnonymousClass0ZE[] r10) {
        super(context, str, null, r8.A01, new AnonymousClass0VA(r8, r10));
        this.A01 = r8;
        this.A02 = r10;
    }

    public synchronized AbstractC12920im A00() {
        AbstractC12920im r0;
        this.A00 = false;
        SQLiteDatabase writableDatabase = super.getWritableDatabase();
        if (this.A00) {
            close();
            r0 = A00();
        } else {
            r0 = A01(writableDatabase);
        }
        return r0;
    }

    public AnonymousClass0ZE A01(SQLiteDatabase sQLiteDatabase) {
        AnonymousClass0ZE[] r2 = this.A02;
        AnonymousClass0ZE r0 = r2[0];
        if (r0 == null || r0.A00 != sQLiteDatabase) {
            r2[0] = new AnonymousClass0ZE(sQLiteDatabase);
        }
        return r2[0];
    }

    @Override // android.database.sqlite.SQLiteOpenHelper, java.lang.AutoCloseable
    public synchronized void close() {
        super.close();
        this.A02[0] = null;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onConfigure(SQLiteDatabase sQLiteDatabase) {
        A01(sQLiteDatabase);
    }

    /* JADX INFO: finally extract failed */
    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        AnonymousClass0SX r4 = this.A01;
        AnonymousClass0ZE A01 = A01(sQLiteDatabase);
        Cursor AZi = A01.AZi(new AnonymousClass0ZK("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'"));
        try {
            boolean z = false;
            if (AZi.moveToFirst()) {
                if (AZi.getInt(0) == 0) {
                    z = true;
                }
            }
            AZi.close();
            AnonymousClass0PA r1 = r4.A02;
            r1.A01(A01);
            if (!z) {
                AnonymousClass0N3 A00 = r1.A00(A01);
                if (!A00.A01) {
                    StringBuilder sb = new StringBuilder("Pre-packaged database has an invalid schema: ");
                    sb.append(A00.A00);
                    throw new IllegalStateException(sb.toString());
                }
            }
            r4.A01(A01);
            WorkDatabase_Impl workDatabase_Impl = r1.A01;
            List list = ((AnonymousClass0QN) workDatabase_Impl).A01;
            if (list != null) {
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    ((AnonymousClass0QN) workDatabase_Impl).A01.get(i);
                }
            }
        } catch (Throwable th) {
            AZi.close();
            throw th;
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.A00 = true;
        this.A01.A02(A01(sQLiteDatabase), i, i2);
    }

    /* JADX INFO: finally extract failed */
    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        if (!this.A00) {
            AnonymousClass0SX r7 = this.A01;
            AnonymousClass0ZE A01 = A01(sQLiteDatabase);
            Cursor AZi = A01.AZi(new AnonymousClass0ZK("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'"));
            try {
                boolean z = false;
                if (AZi.moveToFirst()) {
                    if (AZi.getInt(0) != 0) {
                        z = true;
                    }
                }
                AZi.close();
                if (z) {
                    String str = null;
                    Cursor AZi2 = A01.AZi(new AnonymousClass0ZK("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
                    try {
                        if (AZi2.moveToFirst()) {
                            str = AZi2.getString(0);
                        }
                        AZi2.close();
                        if (!r7.A03.equals(str) && !r7.A04.equals(str)) {
                            throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                        }
                    } catch (Throwable th) {
                        AZi2.close();
                        throw th;
                    }
                } else {
                    AnonymousClass0N3 A00 = r7.A02.A00(A01);
                    if (A00.A01) {
                        r7.A01(A01);
                    } else {
                        StringBuilder sb = new StringBuilder("Pre-packaged database has an invalid schema: ");
                        sb.append(A00.A00);
                        throw new IllegalStateException(sb.toString());
                    }
                }
                WorkDatabase_Impl workDatabase_Impl = r7.A02.A01;
                workDatabase_Impl.A0A = A01;
                SQLiteDatabase sQLiteDatabase2 = A01.A00;
                sQLiteDatabase2.execSQL("PRAGMA foreign_keys = ON");
                C05230Ot r2 = ((AnonymousClass0QN) workDatabase_Impl).A06;
                synchronized (r2) {
                    if (r2.A0A) {
                        Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                    } else {
                        sQLiteDatabase2.execSQL("PRAGMA temp_store = MEMORY;");
                        sQLiteDatabase2.execSQL("PRAGMA recursive_triggers='ON';");
                        sQLiteDatabase2.execSQL("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
                        r2.A00(A01);
                        r2.A09 = new C02980Fp(sQLiteDatabase2.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 "));
                        r2.A0A = true;
                    }
                }
                List list = ((AnonymousClass0QN) workDatabase_Impl).A01;
                if (list != null) {
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        ((AnonymousClass0QN) workDatabase_Impl).A01.get(i);
                        sQLiteDatabase2.beginTransaction();
                        try {
                            StringBuilder sb2 = new StringBuilder("DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < ");
                            sb2.append(System.currentTimeMillis() - WorkDatabase.A00);
                            sb2.append(" AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))");
                            sQLiteDatabase2.execSQL(sb2.toString());
                            sQLiteDatabase2.setTransactionSuccessful();
                            sQLiteDatabase2.endTransaction();
                        } catch (Throwable th2) {
                            sQLiteDatabase2.endTransaction();
                            throw th2;
                        }
                    }
                }
                r7.A00 = null;
            } catch (Throwable th3) {
                AZi.close();
                throw th3;
            }
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.A00 = true;
        this.A01.A02(A01(sQLiteDatabase), i, i2);
    }
}
