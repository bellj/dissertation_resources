package X;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.io.File;

/* renamed from: X.1ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38721ob {
    public Drawable A00;
    public final int A01;
    public final AbstractC38761of A02;
    public final boolean A03;

    public /* synthetic */ C38721ob(C14900mE r10, C18810t5 r11, C38771og r12, int i) {
        AbstractC38761of r0 = r12.A04;
        if (r0 == null) {
            C18790t3 r2 = r12.A07;
            Drawable drawable = r12.A03;
            Drawable drawable2 = this.A00;
            File file = r12.A09;
            long j = r12.A01;
            r0 = new C58952ti(r10, r2, new C38731oc(drawable, drawable2), r11, file, r12.A0A, j);
        }
        this.A02 = r0;
        this.A03 = r12.A05;
        this.A01 = i;
        this.A00 = r12.A02;
    }

    public void A00(Drawable drawable, Drawable drawable2, ImageView imageView, AnonymousClass5WK r15, String str) {
        int i = this.A01;
        this.A02.A01(new AnonymousClass53K(drawable, drawable2, imageView, r15, str, str, i, i), this.A03);
    }

    public void A01(ImageView imageView, String str) {
        A00(null, null, imageView, null, str);
    }
}
