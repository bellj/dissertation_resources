package X;

/* renamed from: X.5Mi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114595Mi extends AnonymousClass1TM {
    public static final AnonymousClass1TK A02 = C72453ed.A12("1.3.6.1.5.5.7.48.2");
    public static final AnonymousClass1TK A03 = C72453ed.A12("1.3.6.1.5.5.7.48.1");
    public AnonymousClass1TK A00;
    public AnonymousClass5N1 A01;

    public C114595Mi(AbstractC114775Na r3) {
        if (r3.A0B() == 2) {
            this.A00 = AnonymousClass1TK.A00(AbstractC114775Na.A00(r3));
            this.A01 = AnonymousClass5N1.A01(AbstractC114775Na.A01(r3));
            return;
        }
        throw C12970iu.A0f("wrong number of elements in sequence");
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A00);
        return C94954co.A01(this.A01, A00);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AccessDescription: Oid(");
        A0k.append(this.A00.A01);
        return C12960it.A0d(")", A0k);
    }
}
