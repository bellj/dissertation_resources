package X;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C117785ad extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final AbstractC16710pd A02;
    public final AbstractC16710pd A03;
    public final AbstractC16710pd A04;
    public final AbstractC16710pd A05;
    public final AbstractC16710pd A06;
    public final AbstractC16710pd A07;
    public final AbstractC16710pd A08;
    public final AbstractC16710pd A09;

    public C117785ad(Context context) {
        super(context, null, 0);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A04 = AnonymousClass4Yq.A00(new AnonymousClass6LD(this));
        this.A08 = AnonymousClass4Yq.A00(new AnonymousClass6LH(this));
        this.A09 = AnonymousClass4Yq.A00(new AnonymousClass6LI(this));
        this.A02 = AnonymousClass4Yq.A00(new AnonymousClass6LB(this));
        this.A07 = AnonymousClass4Yq.A00(new AnonymousClass6LG(this));
        this.A05 = AnonymousClass4Yq.A00(new AnonymousClass6LE(this));
        this.A03 = AnonymousClass4Yq.A00(new AnonymousClass6LC(this));
        this.A06 = AnonymousClass4Yq.A00(new AnonymousClass6LF(this));
        LinearLayout.inflate(getContext(), R.layout.alerts_entry_banner, this);
        setOrientation(1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.C129855yP r7) {
        /*
            r6 = this;
            r5 = 0
            android.widget.TextView r1 = r6.getAlertTitle()
            X.24c r4 = r7.A02
            java.lang.String r0 = r4.A08
            r1.setText(r0)
            android.widget.TextView r1 = r6.getAlertBody()
            java.lang.String r0 = r4.A05
            r1.setText(r0)
            android.widget.TextView r1 = r6.getAlertActionText()
            java.lang.String r0 = r4.A04
            r1.setText(r0)
            int r1 = r4.A01
            r3 = 1
            if (r1 == r3) goto L_0x00d3
            r0 = 2
            if (r1 == r0) goto L_0x00a3
            r0 = 3
            if (r1 != r0) goto L_0x005f
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131231797(0x7f080435, float:1.8079685E38)
            X.C12990iw.A0x(r1, r2, r0)
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099701(0x7f060035, float:1.7811763E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            X.C016307r.A00(r0, r2)
            android.widget.LinearLayout r2 = r6.getAlertBannerComponent()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099694(0x7f06002e, float:1.7811748E38)
        L_0x0058:
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r2.setBackgroundColor(r0)
        L_0x005f:
            android.widget.ImageView r0 = r6.getAlertCloseIcon()
            r1 = 8
            r0.setVisibility(r1)
            int r2 = r7.A00
            android.widget.LinearLayout r0 = r6.getAlertCountLayout()
            if (r2 <= r3) goto L_0x0087
            r0.setVisibility(r5)
            android.widget.TextView r1 = r6.getAlertsCount()
            java.lang.String r0 = java.lang.String.valueOf(r2)
            r1.setText(r0)
            android.view.View r1 = r6.getRootView()
            r0 = 2
        L_0x0083:
            X.C117295Zj.A0n(r1, r7, r0)
            return
        L_0x0087:
            r0.setVisibility(r1)
            boolean r0 = r4.A09
            if (r0 == 0) goto L_0x009d
            android.widget.ImageView r0 = r6.getAlertCloseIcon()
            r0.setVisibility(r5)
            android.widget.ImageView r1 = r6.getAlertCloseIcon()
            r0 = 4
            X.C117295Zj.A0n(r1, r6, r0)
        L_0x009d:
            android.view.View r1 = r6.getRootView()
            r0 = 3
            goto L_0x0083
        L_0x00a3:
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131231672(0x7f0803b8, float:1.8079432E38)
            X.C12990iw.A0x(r1, r2, r0)
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099700(0x7f060034, float:1.781176E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            X.C016307r.A00(r0, r2)
            android.widget.LinearLayout r2 = r6.getAlertBannerComponent()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099696(0x7f060030, float:1.7811752E38)
            goto L_0x0058
        L_0x00d3:
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131231673(0x7f0803b9, float:1.8079434E38)
            X.C12990iw.A0x(r1, r2, r0)
            android.widget.ImageView r2 = r6.getAlertIcon()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099699(0x7f060033, float:1.7811759E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            X.C016307r.A00(r0, r2)
            android.widget.LinearLayout r2 = r6.getAlertBannerComponent()
            android.content.Context r1 = r6.getContext()
            r0 = 2131099695(0x7f06002f, float:1.781175E38)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C117785ad.A00(X.5yP):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    private final TextView getAlertActionText() {
        return (TextView) C16700pc.A05(this.A02);
    }

    private final LinearLayout getAlertBannerComponent() {
        return (LinearLayout) C16700pc.A05(this.A03);
    }

    private final TextView getAlertBody() {
        return (TextView) C16700pc.A05(this.A04);
    }

    private final ImageView getAlertCloseIcon() {
        return (ImageView) C16700pc.A05(this.A05);
    }

    private final LinearLayout getAlertCountLayout() {
        return (LinearLayout) C16700pc.A05(this.A06);
    }

    private final ImageView getAlertIcon() {
        return (ImageView) C16700pc.A05(this.A07);
    }

    private final TextView getAlertTitle() {
        return (TextView) C16700pc.A05(this.A08);
    }

    private final TextView getAlertsCount() {
        return (TextView) C16700pc.A05(this.A09);
    }
}
