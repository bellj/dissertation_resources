package X;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.OrientationEventListener;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import java.io.File;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* renamed from: X.643  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass643 implements TextureView.SurfaceTextureListener {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public AnonymousClass61N A07;
    public C127255uC A08;
    public C89444Jz A09;
    public AnonymousClass4K2 A0A;
    public AnonymousClass4K4 A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public final Context A0F;
    public final Handler A0G;
    public final HandlerThread A0H;
    public final OrientationEventListener A0I;
    public final TextureView A0J;
    public final C125385rA A0K;
    public final C128425w5 A0L;
    public final AnonymousClass66P A0M;
    public final AbstractC1311761o A0N;
    public final AbstractC136156Lf A0O;
    public final AbstractC136166Lg A0P;
    public final AbstractC129405xf A0Q;
    public final AbstractC129405xf A0R;
    public final EnumC124505pe A0S;
    public final C129775yH A0T;
    public final Object A0U;
    public final String A0V;
    public volatile AnonymousClass4K3 A0W;
    public volatile boolean A0X;

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v6, resolved type: X.661 */
    /* JADX WARN: Multi-variable type inference failed */
    public AnonymousClass643(Context context, TextureView textureView, AnonymousClass66P r10, boolean z) {
        EnumC124505pe r3;
        AnonymousClass662 r1;
        Context applicationContext = context.getApplicationContext();
        if (z) {
            r3 = EnumC124505pe.CAMERA2;
        } else {
            r3 = EnumC124505pe.CAMERA1;
        }
        if (C126605t9.A01 == null) {
            synchronized (C126605t9.class) {
                if (C126605t9.A01 == null) {
                    C126605t9.A01 = new C126605t9(r3);
                }
            }
        }
        EnumC124505pe r12 = C126605t9.A01.A00;
        EnumC124505pe r5 = EnumC124505pe.CAMERA1;
        if (r12 == r5) {
            if (AnonymousClass661.A0h == null) {
                synchronized (AnonymousClass661.class) {
                    if (AnonymousClass661.A0h == null) {
                        AnonymousClass661.A0h = new AnonymousClass661(context);
                    }
                }
            }
            AnonymousClass661 r13 = AnonymousClass661.A0h;
            r13.A0C = true;
            r1 = r13;
        } else if (r12 == EnumC124505pe.CAMERA2) {
            AnonymousClass662 A03 = AnonymousClass662.A03(context);
            A03.A0B();
            r1 = A03;
        } else {
            throw C12990iw.A0m(C12960it.A0b("Invalid Camera API: ", r12));
        }
        AnonymousClass63V r4 = new AnonymousClass63V();
        this.A0T = new C129775yH();
        this.A0U = C12970iu.A0l();
        this.A04 = -1;
        this.A03 = -1;
        this.A0C = true;
        this.A0Q = new C118945cd(this);
        this.A0R = new C118955ce(this);
        this.A0O = new AnonymousClass663(this);
        this.A0K = new C125385rA(this);
        this.A0L = new C128425w5(this);
        this.A0P = new AnonymousClass665(this);
        this.A0F = applicationContext;
        this.A0V = "WhatsAppCamera";
        this.A0S = z ? EnumC124505pe.CAMERA2 : r5;
        this.A02 = 1920;
        this.A01 = 1080;
        this.A0N = r1;
        this.A0M = r10;
        this.A0G = new Handler(Looper.getMainLooper(), r4);
        HandlerThread handlerThread = new HandlerThread("Simple-Lite-Camera-Callback-Thread");
        this.A0H = handlerThread;
        handlerThread.start();
        this.A00 = !this.A0N.AI9(0);
        this.A0E = true;
        this.A0J = textureView;
        textureView.setSurfaceTextureListener(this);
        this.A0I = new C117555a9(applicationContext, this);
    }

    public static /* synthetic */ void A00(AnonymousClass643 r1, Object obj, int i) {
        Handler handler = r1.A0G;
        handler.sendMessage(handler.obtainMessage(i, obj));
    }

    public int A01() {
        return this.A00;
    }

    public int A02() {
        AbstractC130695zp A06;
        AbstractC130695zp A062 = A06();
        if (A062 == null || (A06 = A06()) == null || !C117295Zj.A1S(AbstractC130695zp.A0W, A06)) {
            return 0;
        }
        return C12960it.A05(A062.A01(AbstractC130695zp.A0a));
    }

    public int A03() {
        AbstractC130695zp A06;
        int i;
        AbstractC130695zp A062 = A06();
        if (A062 == null || (A06 = A06()) == null) {
            return 100;
        }
        C125465rI r2 = AbstractC130695zp.A0W;
        if (!C117295Zj.A1S(r2, A06)) {
            return 100;
        }
        List A0Y = C117295Zj.A0Y(AbstractC130695zp.A0y, A062);
        AbstractC130695zp A063 = A06();
        if (A063 == null || !C117295Zj.A1S(r2, A063)) {
            i = 0;
        } else {
            i = this.A0N.AHu();
        }
        return C12960it.A05(A0Y.get(i));
    }

    public final int A04() {
        WindowManager windowManager = (WindowManager) this.A0F.getSystemService("window");
        if (windowManager != null) {
            return windowManager.getDefaultDisplay().getRotation();
        }
        return 0;
    }

    public View A05() {
        return this.A0J;
    }

    public final AbstractC130695zp A06() {
        AbstractC1311761o r1 = this.A0N;
        if (r1 == null || !r1.isConnected()) {
            return null;
        }
        try {
            return r1.ABG();
        } catch (C136076Kx unused) {
            return null;
        }
    }

    public void A07() {
        if (!this.A0E) {
            OrientationEventListener orientationEventListener = this.A0I;
            if (orientationEventListener.canDetectOrientation()) {
                orientationEventListener.disable();
            }
            this.A0E = true;
            AbstractC1311761o r1 = this.A0N;
            r1.AaM(this.A0L);
            r1.Ac9(null);
            r1.A8z(new C118925cb(this));
        }
    }

    public void A08() {
        EnumC124575pl r11;
        if (this.A0E) {
            this.A0E = false;
            OrientationEventListener orientationEventListener = this.A0I;
            if (orientationEventListener.canDetectOrientation()) {
                orientationEventListener.enable();
            }
            HandlerThread handlerThread = this.A0H;
            Looper looper = handlerThread.getLooper();
            if (looper != null) {
                AbstractC1311761o r4 = this.A0N;
                r4.Abs(new Handler(looper));
                AnonymousClass61N r8 = this.A07;
                if (r8 == null) {
                    r8 = new AnonymousClass61N(0, 0, 0);
                }
                int i = Build.VERSION.SDK_INT;
                if (i >= 26) {
                    r11 = EnumC124575pl.A02;
                } else if (i >= 19) {
                    r11 = EnumC124575pl.A04;
                } else {
                    r11 = EnumC124575pl.A03;
                }
                AnonymousClass66J r7 = new AnonymousClass66J(r8, new C128395w2(), EnumC124575pl.A02, r11, this.A0D);
                this.A04 = A04();
                r4.A5l(this.A0L);
                r4.Ac9(this.A0O);
                String str = this.A0V;
                int i2 = this.A00;
                int i3 = 0;
                if (i2 != 0) {
                    i3 = 1;
                    if (i2 != 1) {
                        throw C12990iw.A0m(C12960it.A0W(i2, "Could not convert camera facing to optic: "));
                    }
                }
                r4.A7Y(this.A0Q, new C129535xs(new C127225u9(this.A0M, this.A02, this.A01)), r7, null, null, str, i3, this.A04);
                return;
            }
            StringBuilder A0k = C12960it.A0k("Callback handler looper is null. CallbackHandlerThread is alive: ");
            A0k.append(handlerThread.isAlive());
            throw C12990iw.A0m(A0k.toString());
        }
    }

    public void A09() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        synchronized (this.A0U) {
            if (this.A0X) {
                this.A0N.AeV(new C119015ck(this, countDownLatch), false);
                try {
                    countDownLatch.await(5, TimeUnit.SECONDS);
                } catch (InterruptedException unused) {
                    throw C12990iw.A0m("Timeout stopping video recording.");
                }
            }
        }
    }

    public void A0A() {
        if (!this.A0E) {
            AbstractC1311761o r1 = this.A0N;
            if (r1.AK6()) {
                r1.Aef(this.A0R);
            }
        }
    }

    public void A0B(int i) {
        if (this.A00 != 1) {
            C129765yG r2 = new C129765yG();
            C125475rJ r1 = AbstractC130685zo.A0A;
            int i2 = 0;
            if (i != 0) {
                i2 = 1;
                if (i != 1) {
                    i2 = 2;
                    if (i != 2) {
                        i2 = 3;
                        if (i != 3) {
                            i2 = 4;
                            if (i != 4) {
                                throw C12990iw.A0m(C12960it.A0W(i, "Could not convert flash mode to optic: "));
                            }
                        }
                    }
                }
            }
            r2.A01(r1, Integer.valueOf(i2));
            this.A0N.ALV(new C118905cZ(), r2.A00());
        }
    }

    public void A0C(int i) {
        if (this.A0E) {
            AbstractC1311761o r1 = this.A0N;
            int i2 = 0;
            if (i != 0) {
                i2 = 1;
                if (i != 1) {
                    throw C12990iw.A0m(C12960it.A0W(i, "Could not convert camera facing to optic: "));
                }
            }
            if (r1.AI9(i2)) {
                this.A00 = i;
                return;
            }
            return;
        }
        throw C12960it.A0U("Initial camera facing must be set before initializing the camera.");
    }

    public void A0D(int i) {
        AbstractC130695zp A06 = A06();
        if (A06 != null && C117295Zj.A1S(AbstractC130695zp.A0W, A06)) {
            this.A0N.AdE(null, i);
        }
    }

    public void A0E(int i, int i2) {
        AbstractC130695zp A06 = A06();
        if (A06 != null) {
            float[] fArr = {(float) i, (float) i2};
            AbstractC1311761o r2 = this.A0N;
            r2.AKp(fArr);
            if (C117295Zj.A1S(AbstractC130695zp.A0O, A06)) {
                r2.AA3((int) fArr[0], (int) fArr[1]);
            }
        }
    }

    public void A0F(C130565zc r6, AnonymousClass4NI r7) {
        C129455xk r4 = new C129455xk(this, r7);
        AbstractC1311761o r3 = this.A0N;
        AnonymousClass60B r2 = new AnonymousClass60B();
        r2.A01(AnonymousClass60B.A06, Boolean.valueOf(!r6.A00));
        r2.A01(AnonymousClass60B.A08, Boolean.valueOf(r6.A01));
        r3.Aeh(r4, r2);
    }

    public void A0G(AnonymousClass61N r1) {
        this.A07 = r1;
    }

    public final void A0H(C127255uC r4) {
        AbstractC1311761o r2 = this.A0N;
        if (r2.isConnected() && r4 != null) {
            int A04 = A04();
            if (this.A04 == A04) {
                Object[] objArr = new Object[4];
                objArr[0] = this;
                objArr[1] = this.A08;
                C12960it.A1P(objArr, this.A06, 2);
                C12960it.A1P(objArr, this.A05, 3);
                Handler handler = this.A0G;
                handler.sendMessage(handler.obtainMessage(15, objArr));
                return;
            }
            this.A04 = A04;
            r2.Acd(new C118935cc(this), A04);
        }
    }

    public void A0I(C89444Jz r3) {
        if (!this.A0E) {
            AbstractC1311761o r1 = this.A0N;
            if (r1.isConnected()) {
                if (r3 != null) {
                    r1.A5k(this.A0P);
                } else if (this.A09 != null) {
                    r1.AaL(this.A0P);
                }
            }
        }
        this.A09 = r3;
    }

    public void A0J(AnonymousClass4K1 r2) {
        this.A0T.A01(r2);
    }

    public void A0K(AnonymousClass4K1 r2) {
        this.A0T.A02(r2);
    }

    public void A0L(AnonymousClass4K2 r1) {
        this.A0A = r1;
    }

    public void A0M(AnonymousClass4K3 r6, File file) {
        if (this.A0E) {
            Handler handler = this.A0G;
            handler.sendMessage(handler.obtainMessage(10, new Object[]{r6, C12960it.A0U("Cannot start video recording while camera is paused.")}));
            return;
        }
        synchronized (this.A0U) {
            if (this.A0X) {
                Handler handler2 = this.A0G;
                handler2.sendMessage(handler2.obtainMessage(10, new Object[]{r6, C12960it.A0U("Cannot start video recording. Another recording already in progress")}));
            } else {
                this.A0X = true;
                this.A0W = r6;
                this.A0N.AeN(new C118915ca(this), file);
            }
        }
    }

    public void A0N(AnonymousClass4K4 r1) {
        this.A0B = r1;
    }

    public void A0O(boolean z) {
        this.A0D = z;
    }

    public boolean A0P() {
        return this.A0N.AJz();
    }

    public boolean A0Q() {
        return this.A0N.AK6();
    }

    public boolean A0R() {
        return C12970iu.A1Z(this.A0S, EnumC124505pe.CAMERA2);
    }

    public boolean A0S(int i) {
        List A0Y;
        AbstractC130695zp A06 = A06();
        if (A06 == null || (A0Y = C117295Zj.A0Y(AbstractC130695zp.A0j, A06)) == null) {
            return false;
        }
        int i2 = 0;
        if (i != 0) {
            i2 = 1;
            if (i != 1) {
                i2 = 2;
                if (i != 2) {
                    i2 = 3;
                    if (i != 3) {
                        i2 = 4;
                        if (i != 4) {
                            throw C12990iw.A0m(C12960it.A0W(i, "Could not convert flash mode to optic: "));
                        }
                    }
                }
            }
        }
        return C117305Zk.A1Z(A0Y, i2);
    }

    @Override // java.lang.Object
    public void finalize() {
        int i = Build.VERSION.SDK_INT;
        HandlerThread handlerThread = this.A0H;
        if (i >= 18) {
            handlerThread.quitSafely();
        } else {
            handlerThread.quit();
        }
        super.finalize();
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        AnonymousClass66P r0 = this.A0M;
        synchronized (r0.A08) {
            r0.A0A = surfaceTexture;
            r0.A07.countDown();
        }
        onSurfaceTextureSizeChanged(surfaceTexture, i, i2);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        AnonymousClass637 r1;
        AnonymousClass66P r4 = this.A0M;
        synchronized (r4.A08) {
            if (r4.A0A != null) {
                r4.A09 = null;
                r4.A0A = null;
                r4.A07 = new CountDownLatch(1);
            }
            if (AnonymousClass66P.A0C && (r1 = r4.A0B) != null) {
                r1.A05(null, 0);
            }
        }
        return false;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        this.A06 = i;
        this.A05 = i2;
        A0H(this.A08);
    }
}
