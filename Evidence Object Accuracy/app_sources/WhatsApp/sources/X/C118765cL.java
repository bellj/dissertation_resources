package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;

/* renamed from: X.5cL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118765cL extends AnonymousClass03U {
    public final View A00;
    public final ImageView A01;
    public final TextEmojiLabel A02;
    public final /* synthetic */ IndiaUpiBankPickerActivity A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C118765cL(View view, IndiaUpiBankPickerActivity indiaUpiBankPickerActivity) {
        super(view);
        this.A03 = indiaUpiBankPickerActivity;
        this.A01 = C12970iu.A0L(view, R.id.provider_icon);
        this.A02 = (TextEmojiLabel) view.findViewById(R.id.bank_name);
        this.A00 = view.findViewById(R.id.divider);
    }
}
