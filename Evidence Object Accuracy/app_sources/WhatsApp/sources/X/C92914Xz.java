package X;

import android.media.AudioTrack;

/* renamed from: X.4Xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92914Xz {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public final AnonymousClass4XM A05;

    public C92914Xz(AudioTrack audioTrack) {
        if (AnonymousClass3JZ.A01 >= 19) {
            this.A05 = new AnonymousClass4XM(audioTrack);
            A02();
            return;
        }
        this.A05 = null;
        this.A00 = 3;
        this.A04 = 10000000;
    }

    public long A00() {
        AnonymousClass4XM r0 = this.A05;
        if (r0 != null) {
            return r0.A00;
        }
        return -1;
    }

    public long A01() {
        AnonymousClass4XM r0 = this.A05;
        if (r0 != null) {
            return r0.A00();
        }
        return -9223372036854775807L;
    }

    public void A02() {
        if (this.A05 != null) {
            this.A00 = 0;
            this.A03 = 0;
            this.A01 = -1;
            this.A02 = C12980iv.A0D(System.nanoTime());
            this.A04 = 10000;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        if (r7 != false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0061, code lost:
        if (r7 == false) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(long r10) {
        /*
            r9 = this;
            X.4XM r8 = r9.A05
            r5 = 0
            if (r8 == 0) goto L_0x0067
            long r0 = r9.A03
            long r3 = r10 - r0
            long r1 = r9.A04
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0067
            r9.A03 = r10
            boolean r7 = r8.A01()
            int r1 = r9.A00
            r3 = 3
            r6 = 1
            if (r1 == 0) goto L_0x003a
            r5 = 2
            if (r1 == r6) goto L_0x002d
            if (r1 == r5) goto L_0x0061
            if (r1 == r3) goto L_0x002a
            r0 = 4
            if (r1 == r0) goto L_0x0066
            java.lang.IllegalStateException r0 = X.C72463ee.A0D()
            throw r0
        L_0x002a:
            if (r7 == 0) goto L_0x0066
            goto L_0x0063
        L_0x002d:
            if (r7 == 0) goto L_0x0063
            long r3 = r8.A00
            long r1 = r9.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0066
            r9.A00 = r5
            goto L_0x005d
        L_0x003a:
            if (r7 == 0) goto L_0x0051
            long r3 = r8.A00()
            long r1 = r9.A02
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0067
            long r0 = r8.A00
            r9.A01 = r0
            r9.A00 = r6
            r0 = 10000(0x2710, double:4.9407E-320)
        L_0x004e:
            r9.A04 = r0
            return r7
        L_0x0051:
            long r0 = r9.A02
            long r10 = r10 - r0
            r1 = 500000(0x7a120, double:2.47033E-318)
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0066
            r9.A00 = r3
        L_0x005d:
            r0 = 10000000(0x989680, double:4.9406565E-317)
            goto L_0x004e
        L_0x0061:
            if (r7 != 0) goto L_0x0066
        L_0x0063:
            r9.A02()
        L_0x0066:
            return r7
        L_0x0067:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C92914Xz.A03(long):boolean");
    }
}
