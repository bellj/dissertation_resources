package X;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.qrcode.DevicePairQrScannerActivity;
import com.whatsapp.qrcode.WaQrScannerView;
import com.whatsapp.util.Log;

/* renamed from: X.1su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC41101su extends AbstractActivityC41111sv {
    public View A00;
    public View A01;
    public C15890o4 A02;
    public WaQrScannerView A03;
    public C21280xA A04;
    public String A05;
    public boolean A06 = true;
    public boolean A07;

    public void A2h(C35751ig r1) {
    }

    public void A2e() {
        int A02 = this.A02.A02("android.permission.CAMERA");
        WaQrScannerView waQrScannerView = this.A03;
        if (A02 != 0) {
            waQrScannerView.setVisibility(8);
            this.A00.setVisibility(8);
            this.A01.setVisibility(0);
            C35751ig r5 = new C35751ig(this);
            r5.A01 = R.drawable.permission_cam;
            int[] iArr = {R.string.localized_app_name};
            r5.A02 = R.string.permission_cam_access_on_wa_web_connect_request;
            r5.A0A = iArr;
            int[] iArr2 = {R.string.localized_app_name};
            r5.A03 = R.string.permission_cam_access_on_wa_web_connect;
            r5.A08 = iArr2;
            r5.A0C = new String[]{"android.permission.CAMERA"};
            r5.A06 = true;
            A2h(r5);
            startActivityForResult(r5.A00(), 1);
            return;
        }
        waQrScannerView.setVisibility(0);
        this.A00.setVisibility(0);
        this.A01.setVisibility(8);
    }

    public void A2f() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = (DevicePairQrScannerActivity) this;
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A0G(devicePairQrScannerActivity.A0Q);
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A0I(new RunnableBRunnable0Shape10S0100000_I0_10(devicePairQrScannerActivity, 3));
    }

    public void A2g(C49262Kb r4) {
        String str = r4.A02;
        Log.i("QrScannerActivity/result");
        if (str == null || str.equals(this.A05)) {
            this.A03.Aab();
        } else {
            this.A05 = str;
            A2f();
        }
        ((ActivityC13810kN) this).A09.A00.edit().putBoolean("qr_education", false).apply();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == 0) {
            finish();
        } else {
            this.A03.setVisibility(0);
            this.A00.setVisibility(0);
            this.A01.setVisibility(8);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        A1b(5);
        super.onCreate(bundle);
        setTitle(R.string.scan_qr_code);
        getWindow().addFlags(128);
        setContentView(getLayoutInflater().inflate(R.layout.qr_code_scanner, (ViewGroup) null, false));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1g(false);
        this.A06 = ((ActivityC13810kN) this).A09.A00.getBoolean("qr_education", true);
        this.A00 = findViewById(R.id.overlay);
        this.A03 = (WaQrScannerView) findViewById(R.id.qr_scanner_view);
        this.A01 = findViewById(R.id.shade);
        this.A03.setQrScannerCallback(new C69463Zi(this));
        View findViewById = findViewById(R.id.ok);
        View findViewById2 = findViewById(R.id.education);
        findViewById.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 41, findViewById2));
        if (this.A06) {
            findViewById2.setVisibility(0);
            this.A03.setVisibility(8);
            this.A00.setVisibility(8);
            this.A01.setVisibility(0);
            return;
        }
        findViewById2.setVisibility(8);
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (this.A03.getVisibility() == 0) {
            this.A03.setVisibility(4);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A03.getVisibility() == 4) {
            this.A03.setVisibility(0);
        }
    }
}
