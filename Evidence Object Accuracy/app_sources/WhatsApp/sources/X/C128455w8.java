package X;

/* renamed from: X.5w8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128455w8 {
    public final /* synthetic */ AnonymousClass61B A00;

    public C128455w8(AnonymousClass61B r1) {
        this.A00 = r1;
    }

    public void A00(AnonymousClass66I r4) {
        AnonymousClass61B r1 = this.A00;
        r1.A02 = r4;
        C1308560f r0 = r1.A04;
        if (r0 == null) {
            return;
        }
        if (r0.A09()) {
            AnonymousClass61B.A02(r1);
        } else if (r1.A00 != null) {
            try {
                r1.A04.A07("onFrameCaptured", r1.A0A);
            } catch (Exception unused) {
            }
        }
    }
}
