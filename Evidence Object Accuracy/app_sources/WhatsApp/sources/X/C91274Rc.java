package X;

/* renamed from: X.4Rc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91274Rc {
    public final int A00 = -1;
    public final AbstractC116045Tx A01;
    public final String A02;
    public final boolean A03;

    public C91274Rc(AbstractC116045Tx r2, String str, boolean z) {
        this.A02 = str;
        this.A03 = z;
        this.A01 = r2;
    }
}
