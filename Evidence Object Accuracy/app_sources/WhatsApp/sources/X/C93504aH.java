package X;

import java.security.cert.PKIXBuilderParameters;
import java.util.Set;

/* renamed from: X.4aH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93504aH {
    public int A00 = 5;
    public Set A01 = C12970iu.A12();
    public final C112085Bz A02;

    public C93504aH(C112085Bz r2) {
        this.A02 = r2;
    }

    public C93504aH(PKIXBuilderParameters pKIXBuilderParameters) {
        this.A02 = new C112085Bz(new C93814am(pKIXBuilderParameters));
        this.A00 = pKIXBuilderParameters.getMaxPathLength();
    }
}
