package X;

import android.graphics.Color;
import java.util.Arrays;

/* renamed from: X.0QA  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0QA {
    public int A00;
    public int A01;
    public boolean A02;
    public float[] A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;

    public AnonymousClass0QA(int i, int i2) {
        this.A07 = Color.red(i);
        this.A05 = Color.green(i);
        this.A04 = Color.blue(i);
        this.A08 = i;
        this.A06 = i2;
    }

    public final void A00() {
        int A06;
        int A062;
        if (!this.A02) {
            int i = this.A08;
            int A04 = C016907y.A04(4.5f, -1, i);
            int A042 = C016907y.A04(3.0f, -1, i);
            if (A04 == -1 || A042 == -1) {
                int A043 = C016907y.A04(4.5f, -16777216, i);
                int A044 = C016907y.A04(3.0f, -16777216, i);
                if (A043 == -1 || A044 == -1) {
                    if (A04 != -1) {
                        A06 = C016907y.A06(-1, A04);
                    } else {
                        A06 = C016907y.A06(-16777216, A043);
                    }
                    this.A00 = A06;
                    if (A042 != -1) {
                        A062 = C016907y.A06(-1, A042);
                    } else {
                        A062 = C016907y.A06(-16777216, A044);
                    }
                } else {
                    this.A00 = C016907y.A06(-16777216, A043);
                    A062 = C016907y.A06(-16777216, A044);
                }
            } else {
                this.A00 = C016907y.A06(-1, A04);
                A062 = C016907y.A06(-1, A042);
            }
            this.A01 = A062;
            this.A02 = true;
        }
    }

    public float[] A01() {
        float[] fArr = this.A03;
        if (fArr == null) {
            fArr = new float[3];
            this.A03 = fArr;
        }
        C016907y.A07(this.A07, this.A05, fArr, this.A04);
        return fArr;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass0QA.class != obj.getClass()) {
                return false;
            }
            AnonymousClass0QA r5 = (AnonymousClass0QA) obj;
            if (!(this.A06 == r5.A06 && this.A08 == r5.A08)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A08 * 31) + this.A06;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Swatch");
        sb.append(" [RGB: #");
        sb.append(Integer.toHexString(this.A08));
        sb.append(']');
        sb.append(" [HSL: ");
        sb.append(Arrays.toString(A01()));
        sb.append(']');
        sb.append(" [Population: ");
        sb.append(this.A06);
        sb.append(']');
        sb.append(" [Title Text: #");
        A00();
        sb.append(Integer.toHexString(this.A01));
        sb.append(']');
        sb.append(" [Body Text: #");
        A00();
        sb.append(Integer.toHexString(this.A00));
        sb.append(']');
        return sb.toString();
    }
}
