package X;

/* renamed from: X.51N  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass51N implements AnonymousClass28S {
    public boolean A00(char c) {
        if (!(this instanceof C80763sv)) {
            return C12960it.A1V(c, ((C80753su) this).A00);
        }
        return false;
    }

    @Override // X.AnonymousClass28S
    @Deprecated
    public /* bridge */ /* synthetic */ boolean A66(Object obj) {
        return A00(((Character) obj).charValue());
    }
}
