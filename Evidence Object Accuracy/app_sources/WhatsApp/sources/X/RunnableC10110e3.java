package X;

import android.view.View;
import java.util.ArrayList;

/* renamed from: X.0e3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10110e3 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractC06480Tu A01;
    public final /* synthetic */ ArrayList A02;
    public final /* synthetic */ ArrayList A03;
    public final /* synthetic */ ArrayList A04;
    public final /* synthetic */ ArrayList A05;

    public RunnableC10110e3(AbstractC06480Tu r1, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4, int i) {
        this.A01 = r1;
        this.A00 = i;
        this.A04 = arrayList;
        this.A02 = arrayList2;
        this.A05 = arrayList3;
        this.A03 = arrayList4;
    }

    @Override // java.lang.Runnable
    public void run() {
        for (int i = 0; i < this.A00; i++) {
            AnonymousClass028.A0k((View) this.A04.get(i), (String) this.A02.get(i));
            AnonymousClass028.A0k((View) this.A05.get(i), (String) this.A03.get(i));
        }
    }
}
