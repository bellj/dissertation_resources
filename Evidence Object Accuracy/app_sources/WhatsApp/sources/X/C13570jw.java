package X;

import android.content.Context;
import android.util.Log;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0jw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13570jw {
    public static C13570jw A04;
    public int A00 = 1;
    public ServiceConnectionC15180mh A01 = new ServiceConnectionC15180mh(this);
    public final Context A02;
    public final ScheduledExecutorService A03;

    public C13570jw(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.A03 = scheduledExecutorService;
        this.A02 = context.getApplicationContext();
    }

    public static synchronized C13570jw A00(Context context) {
        C13570jw r1;
        synchronized (C13570jw.class) {
            r1 = A04;
            if (r1 == null) {
                r1 = new C13570jw(context, Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1, new ThreadFactoryC16540pC("MessengerIpcClient"))));
                A04 = r1;
            }
        }
        return r1;
    }

    public final synchronized C13600jz A01(AbstractC13590jy r5) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(r5);
            StringBuilder sb = new StringBuilder(valueOf.length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.A01.A03(r5)) {
            ServiceConnectionC15180mh r0 = new ServiceConnectionC15180mh(this);
            this.A01 = r0;
            r0.A03(r5);
        }
        return r5.A03.A00;
    }
}
