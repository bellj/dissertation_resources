package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;

/* renamed from: X.1wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43421wz implements AbstractC43401wx {
    public BroadcastReceiver A00;
    public Handler A01;
    public final C18640sm A02;
    public final C247716u A03;
    public final C15410nB A04;
    public final C16590pI A05;
    public final C19890uq A06;

    public C43421wz(C18640sm r1, C247716u r2, C15410nB r3, C16590pI r4, C19890uq r5) {
        this.A05 = r4;
        this.A06 = r5;
        this.A04 = r3;
        this.A03 = r2;
        this.A02 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0038, code lost:
        if (X.C18640sm.A02() == false) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(boolean r7) {
        /*
            r6 = this;
            X.16u r0 = r6.A03
            android.net.NetworkInfo r2 = r0.A01()
            java.lang.String r0 = "xmpp/handler/network/active "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r2)
            java.lang.String r0 = " isRetry="
            r1.append(r0)
            r1.append(r7)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            r5 = 1
            if (r2 == 0) goto L_0x0050
            boolean r4 = r2.isConnected()
            int r1 = r2.getType()
            r0 = 0
            if (r1 != r5) goto L_0x002f
            r0 = 1
        L_0x002f:
            if (r4 == 0) goto L_0x003a
            if (r0 == 0) goto L_0x003a
            boolean r0 = X.C18640sm.A02()
            r3 = 1
            if (r0 != 0) goto L_0x003b
        L_0x003a:
            r3 = 0
        L_0x003b:
            X.0nB r0 = r6.A04
            r0.A00()
            X.0uq r2 = r6.A06
            if (r4 == 0) goto L_0x004e
            if (r3 != 0) goto L_0x004e
        L_0x0046:
            long r0 = (long) r1
            r2.A0D(r0, r5)
            r2.A0I(r3, r7)
            return
        L_0x004e:
            r5 = 0
            goto L_0x0046
        L_0x0050:
            r1 = -1
            r4 = 0
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C43421wz.A00(boolean):void");
    }

    @Override // X.AbstractC43401wx
    public long AC7() {
        NetworkInfo A01 = this.A03.A01();
        if (A01 != null) {
            return (long) A01.getType();
        }
        return -1;
    }

    @Override // X.AbstractC43401wx
    public void AaZ() {
        Handler handler = this.A01;
        AnonymousClass009.A05(handler);
        handler.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 37));
    }

    @Override // X.AbstractC43401wx
    public void AeC(Handler handler) {
        this.A01 = handler;
        Context context = this.A05.A00;
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        C43641xM r1 = new C43641xM(this);
        this.A00 = r1;
        if (context.registerReceiver(r1, intentFilter, null, handler) == null) {
            AnonymousClass009.A05(handler);
            if (!handler.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 36))) {
                Log.w("failed to post checkNetworkState isRetry: false");
            }
        }
    }

    @Override // X.AbstractC43401wx
    public void AeS() {
        this.A05.A00.unregisterReceiver(this.A00);
        this.A00 = null;
        this.A01 = null;
    }

    @Override // X.AbstractC43401wx
    public boolean isConnected() {
        NetworkInfo A01 = this.A03.A01();
        return A01 != null && A01.isConnected();
    }
}
