package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.util.Log;

/* renamed from: X.2cC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53002cC extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass12P A00;
    public AnonymousClass018 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public final View A04;
    public final View A05;
    public final WaImageView A06;
    public final WaImageView A07;
    public final WaTextView A08;
    public final WaTextView A09;

    public C53002cC(Context context, int i) {
        super(context, null, 0);
        ColorStateList A03;
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A00 = C12980iv.A0W(A00);
            this.A01 = C12960it.A0R(A00);
        }
        View A0F = C12960it.A0F(LayoutInflater.from(context), this, R.layout.conversations_archive_row_view);
        this.A04 = A0F;
        WaImageView A0X = C12980iv.A0X(A0F, R.id.archived_row_image);
        this.A06 = A0X;
        WaTextView A0N = C12960it.A0N(A0F, R.id.archived_row);
        this.A09 = A0N;
        WaTextView A0N2 = C12960it.A0N(A0F, R.id.archive_row_counter);
        this.A08 = A0N2;
        AnonymousClass009.A05(context);
        A0N2.setBackgroundDrawable(new C49742Mi(AnonymousClass00T.A00(context, R.color.transparent)));
        this.A05 = AnonymousClass028.A0D(A0F, R.id.content_indicator_container);
        WaImageView A0X2 = C12980iv.A0X(A0F, R.id.archive_row_important_chat_indicator);
        this.A07 = A0X2;
        C27531Hw.A06(A0N);
        C42631vX.A00(A0F);
        AnonymousClass23N.A01(A0F);
        C016307r.A00(AnonymousClass00T.A03(context, R.color.selector_archive_icon), A0X);
        C016307r.A00(AnonymousClass00T.A03(context, R.color.selector_unread_indicator), A0X2);
        A0N.setTextColor(AnonymousClass00T.A03(context, R.color.selector_list_item));
        int i2 = R.color.selector_archived_count_indicator;
        if (i != 1) {
            if (i != 2) {
                Log.e(C12960it.A0W(i, "archive/Unspoorted mode in ArchivePreviewView: "));
                A03 = null;
                this.A08.setTextColor(A03);
                addView(A0F);
            }
            i2 = R.color.selector_unread_indicator;
        }
        A03 = AnonymousClass00T.A03(context, i2);
        this.A08.setTextColor(A03);
        addView(A0F);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public void setContentIndicatorText(String str) {
        View view;
        if (TextUtils.isEmpty(str)) {
            Log.i("archive/set-content-indicator-to-empty");
            view = this.A05;
        } else {
            this.A05.setVisibility(0);
            boolean equals = "@".equals(str);
            WaImageView waImageView = this.A07;
            if (equals) {
                waImageView.setVisibility(0);
                view = this.A08;
            } else {
                waImageView.setVisibility(8);
                WaTextView waTextView = this.A08;
                waTextView.setText(str);
                waTextView.setVisibility(0);
                return;
            }
        }
        view.setVisibility(8);
    }

    public void setEnableState(boolean z) {
        this.A04.setClickable(z);
        this.A09.setEnabled(z);
        this.A06.setEnabled(z);
        this.A08.setEnabled(z);
        this.A07.setEnabled(z);
    }

    public void setImportantMessageTag(int i) {
        this.A07.setTag(Integer.valueOf(i));
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.A04.setOnClickListener(onClickListener);
    }

    public void setVisibility(boolean z) {
        this.A04.setVisibility(C12960it.A02(z ? 1 : 0));
    }
}
