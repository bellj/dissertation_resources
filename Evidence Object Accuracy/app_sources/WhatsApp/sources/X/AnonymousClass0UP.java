package X;

import android.graphics.Rect;
import androidx.window.sidecar.SidecarDeviceState;
import androidx.window.sidecar.SidecarDisplayFeature;
import androidx.window.sidecar.SidecarWindowLayoutInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0UP  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0UP {
    public static final AnonymousClass0Tb A01 = new AnonymousClass0Tb();
    public final AnonymousClass0JH A00;

    public AnonymousClass0UP() {
        this(AnonymousClass0JH.QUIET);
    }

    public AnonymousClass0UP(AnonymousClass0JH r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public static final AnonymousClass0SP A00(AbstractC11930h6 r1, AnonymousClass0JH r2, Object obj) {
        C16700pc.A0E(r2, 2);
        return new AnonymousClass0GG(r1, r2, obj);
    }

    public static final boolean A01(SidecarDeviceState sidecarDeviceState, SidecarDeviceState sidecarDeviceState2) {
        if (C16700pc.A0O(sidecarDeviceState, sidecarDeviceState2)) {
            return true;
        }
        if (sidecarDeviceState == null) {
            return false;
        }
        AnonymousClass0Tb r0 = A01;
        if (r0.A02(sidecarDeviceState) != r0.A02(sidecarDeviceState2)) {
            return false;
        }
        return true;
    }

    public static final boolean A02(SidecarDisplayFeature sidecarDisplayFeature, SidecarDisplayFeature sidecarDisplayFeature2) {
        if (C16700pc.A0O(sidecarDisplayFeature, sidecarDisplayFeature2)) {
            return true;
        }
        if (sidecarDisplayFeature == null || sidecarDisplayFeature2 == null || sidecarDisplayFeature.getType() != sidecarDisplayFeature2.getType()) {
            return false;
        }
        return C16700pc.A0O(sidecarDisplayFeature.getRect(), sidecarDisplayFeature2.getRect());
    }

    public final AbstractC11390gD A03(SidecarDeviceState sidecarDeviceState, SidecarDisplayFeature sidecarDisplayFeature) {
        AnonymousClass0SA r3;
        AnonymousClass0S9 r2;
        C16700pc.A0E(sidecarDisplayFeature, 0);
        SidecarDisplayFeature sidecarDisplayFeature2 = (SidecarDisplayFeature) A00(new AnonymousClass0ZV(), this.A00, sidecarDisplayFeature).A01("Type must be either TYPE_FOLD or TYPE_HINGE", new C11060fg()).A01("Feature bounds must not be 0", new C11070fh()).A01("TYPE_FOLD must have 0 area", new C11080fi()).A01("Feature be pinned to either left or top", new C11090fj()).A02();
        if (sidecarDisplayFeature2 != null) {
            int type = sidecarDisplayFeature2.getType();
            if (type == 1) {
                r3 = AnonymousClass0SA.A01;
            } else if (type == 2) {
                r3 = AnonymousClass0SA.A02;
            }
            int A02 = A01.A02(sidecarDeviceState);
            if (A02 == 0 || A02 == 1) {
                return null;
            }
            if (A02 == 2) {
                r2 = AnonymousClass0S9.A02;
            } else if (A02 != 3 && A02 == 4) {
                return null;
            } else {
                r2 = AnonymousClass0S9.A01;
            }
            Rect rect = sidecarDisplayFeature.getRect();
            C16700pc.A0B(rect);
            return new AnonymousClass0ZZ(new C05400Pk(rect), r2, r3);
        }
        return null;
    }

    public final AnonymousClass0PZ A04(SidecarDeviceState sidecarDeviceState, SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
        List A05;
        if (sidecarWindowLayoutInfo == null) {
            A05 = C16770pj.A0G();
        } else {
            SidecarDeviceState sidecarDeviceState2 = new SidecarDeviceState();
            AnonymousClass0Tb.A01(sidecarDeviceState2, A01.A02(sidecarDeviceState));
            A05 = A05(sidecarDeviceState2, AnonymousClass0Tb.A00(sidecarWindowLayoutInfo));
        }
        return new AnonymousClass0PZ(A05);
    }

    public final List A05(SidecarDeviceState sidecarDeviceState, List list) {
        C16700pc.A0E(list, 0);
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC11390gD A03 = A03(sidecarDeviceState, (SidecarDisplayFeature) it.next());
            if (A03 != null) {
                arrayList.add(A03);
            }
        }
        return arrayList;
    }

    public final boolean A06(SidecarWindowLayoutInfo sidecarWindowLayoutInfo, SidecarWindowLayoutInfo sidecarWindowLayoutInfo2) {
        if (C16700pc.A0O(sidecarWindowLayoutInfo, sidecarWindowLayoutInfo2)) {
            return true;
        }
        if (sidecarWindowLayoutInfo == null) {
            return false;
        }
        List A00 = AnonymousClass0Tb.A00(sidecarWindowLayoutInfo);
        List A002 = AnonymousClass0Tb.A00(sidecarWindowLayoutInfo2);
        if (A00 == A002) {
            return true;
        }
        if (A00 == null || A002 == null || A00.size() != A002.size()) {
            return false;
        }
        int size = A00.size();
        int i = 0;
        while (i < size) {
            boolean A02 = A02((SidecarDisplayFeature) A00.get(i), (SidecarDisplayFeature) A002.get(i));
            i++;
            if (!A02) {
                return false;
            }
        }
        return true;
    }
}
