package X;

/* renamed from: X.4xG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC107394xG implements AnonymousClass5YX {
    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public String toString() {
        return C12960it.A0d(C12980iv.A0r(this), C12960it.A0k("SCTE-35 splice command: type="));
    }
}
