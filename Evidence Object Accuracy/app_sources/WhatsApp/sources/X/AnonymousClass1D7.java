package X;

/* renamed from: X.1D7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D7 implements AbstractC16990q5 {
    public final C15820nx A00;
    public final AnonymousClass1D6 A01;
    public final C14820m6 A02;
    public final C21710xr A03;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOo() {
    }

    public AnonymousClass1D7(C15820nx r1, AnonymousClass1D6 r2, C14820m6 r3, C21710xr r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
        if (r6.A08(r4) == false) goto L_0x0050;
     */
    @Override // X.AbstractC16990q5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOp() {
        /*
            r8 = this;
            X.1D6 r6 = r8.A01
            X.0m9 r3 = r6.A08
            r0 = 932(0x3a4, float:1.306E-42)
            boolean r0 = r3.A07(r0)
            if (r0 != 0) goto L_0x001c
            X.0m6 r4 = r6.A05
            android.content.SharedPreferences r2 = r4.A00
            java.lang.String r1 = "gdrive_backup_quota_warning_visibility"
            r0 = 0
            int r0 = r2.getInt(r1, r0)
            if (r0 == 0) goto L_0x001c
            r4.A0L()
        L_0x001c:
            int r1 = r6.A00()
            r0 = 3
            if (r1 != r0) goto L_0x0068
            X.0m6 r7 = r6.A05
            android.content.SharedPreferences r2 = r7.A00
            java.lang.String r1 = "gdrive_backup_quota_warning_visibility"
            r0 = 0
            int r1 = r2.getInt(r1, r0)
            r0 = 2
            if (r1 != r0) goto L_0x0068
            long r4 = java.lang.System.currentTimeMillis()
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.DAYS
            r0 = 1242(0x4da, float:1.74E-42)
            int r0 = r3.A02(r0)
            long r0 = (long) r0
            long r2 = r2.toMillis(r0)
            long r2 = r2 + r4
            boolean r0 = X.C44771zW.A0K(r7, r4)
            if (r0 == 0) goto L_0x0050
            boolean r0 = r6.A08(r4)
            r1 = 1
            if (r0 != 0) goto L_0x0051
        L_0x0050:
            r1 = 0
        L_0x0051:
            boolean r0 = X.C44771zW.A0K(r7, r2)
            if (r0 == 0) goto L_0x0068
            boolean r0 = r6.A08(r2)
            if (r0 == 0) goto L_0x0068
            if (r1 != 0) goto L_0x0068
            X.0m6 r1 = r8.A02
            r0 = 1
            r1.A0R(r0)
            r6.A05()
        L_0x0068:
            X.0nx r4 = r8.A00
            boolean r0 = r4.A04()
            r3 = 1
            if (r0 == 0) goto L_0x00a4
            X.0m6 r2 = r4.A03
            int r0 = r2.A01()
            if (r0 == 0) goto L_0x00a4
            int r0 = r2.A01()
            if (r0 == r3) goto L_0x00a4
            java.lang.String r1 = r2.A09()
            boolean r0 = r4.A04()
            if (r0 == 0) goto L_0x00a4
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x00a4
            int r0 = r2.A06(r1)
            if (r0 != r3) goto L_0x00a4
            X.0xr r4 = r8.A03
            java.util.Random r3 = new java.util.Random
            r3.<init>()
            X.0m6 r2 = r8.A02
            r1 = 0
            X.023 r0 = X.AnonymousClass023.KEEP
            com.whatsapp.backup.google.workers.GoogleEncryptedReUploadWorker.A01(r0, r2, r4, r3, r1)
        L_0x00a4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1D7.AOp():void");
    }
}
