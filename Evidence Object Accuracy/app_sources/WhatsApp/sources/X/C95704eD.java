package X;

import android.animation.Animator;
import android.view.View;

/* renamed from: X.4eD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95704eD implements Animator.AnimatorListener {
    public final /* synthetic */ AbstractActivityC36611kC A00;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    public /* synthetic */ C95704eD(AbstractActivityC36611kC r1) {
        this.A00 = r1;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view;
        int i;
        AbstractActivityC36611kC r1 = this.A00;
        if (!r1.A0g.isEmpty()) {
            view = r1.A06;
            i = 0;
        } else {
            view = r1.A05;
            i = 4;
        }
        view.setVisibility(i);
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        View view;
        int i;
        AbstractActivityC36611kC r1 = this.A00;
        if (!r1.A0g.isEmpty()) {
            view = r1.A05;
            i = 0;
        } else {
            view = r1.A06;
            i = 8;
        }
        view.setVisibility(i);
    }
}
