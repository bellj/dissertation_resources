package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.2DP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DP implements AnonymousClass2DH {
    public final /* synthetic */ AnonymousClass22W A00;
    public final /* synthetic */ C22460z7 A01;

    public AnonymousClass2DP(AnonymousClass22W r1, C22460z7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        this.A00.APk();
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        C22460z7 r5 = this.A01;
        C14820m6 r4 = r5.A0B.A01;
        SharedPreferences sharedPreferences = r4.A00;
        sharedPreferences.edit().putInt("payment_background_backoff_attempt", 0).apply();
        sharedPreferences.edit().remove("payment_backgrounds_backoff_timestamp").apply();
        r4.A0k("payment_backgrounds_last_fetch_timestamp");
        r5.A0D.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 44, this.A00));
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        this.A00.AXX();
    }
}
