package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1h2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34991h2 extends C33711ex {
    public C34991h2(C16470p4 r1) {
        super(r1);
    }

    public static final JSONObject A01(AnonymousClass1ZD r1, C39971qq r2) {
        try {
            return AnonymousClass1QC.A08(r1, r2.A07);
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("CheckoutMessageCustomizer/getJsonParameter/invalid parameter json: ");
            sb.append(e.getMessage());
            Log.e(sb.toString());
            return null;
        }
    }

    @Override // X.C33711ex
    public CharSequence A07(Context context, Paint paint, AnonymousClass018 r6) {
        String str;
        AnonymousClass1ZD r0;
        C16470p4 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null || (str = r0.A08) == null) {
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            return super.A07(context, paint, r6);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        Drawable A01 = A01(context);
        return A01 != null ? C52252aV.A01(paint, A01, sb) : sb;
    }

    @Override // X.C33711ex
    public String A08(Context context) {
        String str;
        AnonymousClass1ZD r0;
        C16470p4 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null || (str = r0.A08) == null) {
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            return super.A08(context);
        }
        StringBuilder sb = new StringBuilder("*");
        sb.append(str);
        sb.append("*");
        return sb.toString();
    }

    @Override // X.C33711ex
    public String A09(AnonymousClass018 r6) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        C16470p4 r3 = this.A00;
        if (r3 != null) {
            AnonymousClass1ZD r1 = r3.A01;
            if (r1 != null) {
                C33711ex.A00(r1.A04.A00(), "\n", sb);
                C33711ex.A00(r1.A02(r6), "\n", sb);
            }
            str = r3.A07;
        } else {
            str = null;
        }
        C33711ex.A00(str, "\n", sb);
        if (r3 != null) {
            str2 = r3.A08;
        } else {
            str2 = null;
        }
        C33711ex.A00(str2, "\n", sb);
        C33711ex.A00(r6.A09(R.string.checkout_native_flow_message_button_text), "\n", sb);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01f9, code lost:
        if (r4 != null) goto L_0x00ea;
     */
    @Override // X.C33711ex
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(X.AbstractC15340mz r13, X.C39971qq r14) {
        /*
        // Method dump skipped, instructions count: 509
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34991h2.A0A(X.0mz, X.1qq):void");
    }
}
