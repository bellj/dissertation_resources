package X;

import android.content.Context;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120275fu extends AbstractC451020e {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractC136196Lo A01;
    public final /* synthetic */ C130155yt A02;
    public final /* synthetic */ Integer A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120275fu(Context context, C14900mE r2, C18650sn r3, AbstractC136196Lo r4, C130155yt r5, Integer num, int i) {
        super(context, r2, r3);
        this.A02 = r5;
        this.A01 = r4;
        this.A00 = i;
        this.A03 = num;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r3) {
        C130785zy.A04(r3, this.A01, null);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r6) {
        int i = r6.A00;
        if (i == 542720003) {
            this.A02.A0C.A06();
        } else if (i == 542720109) {
            AnonymousClass61F r4 = this.A02.A0C;
            C12970iu.A1C(C130105yo.A01(r4.A0F), "trusted_device_expiry_timestamp_sec", 0);
            r4.A0D.A06("alias-signing-key.data-fetch");
        }
        this.A02.A01 = C12960it.A1V(r6.A00, 443);
        C130785zy.A04(r6, this.A01, null);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r16) {
        AbstractC136196Lo r2;
        C130785zy r1;
        AnonymousClass1V8 r22;
        AbstractC136196Lo r12;
        C130785zy r0;
        try {
            try {
                C130155yt r9 = this.A02;
                r9.A01 = false;
                int i = this.A00;
                if (i != 7) {
                    r22 = C1308960j.A00(r9.A0B, r16.A0F("account"));
                    if (i == 5) {
                        r9.A0C.A09();
                    }
                } else {
                    r22 = r16.A0F("account");
                }
                C452120p A00 = C452120p.A00(r22);
                if (A00 != null) {
                    if (A00.A00 == 542720003) {
                        r9.A0C.A06();
                    }
                    r12 = this.A01;
                    r0 = new C130785zy(A00, null);
                } else {
                    AnonymousClass1V8 A0E = r22.A0E("step_up");
                    if (A0E != null) {
                        try {
                            C1316663q A002 = C125045qa.A00(A0E);
                            Integer num = this.A03;
                            if (num == null) {
                                Log.e("PAY: NoviActionManager/onResponseSuccess step up origin action is null");
                                C130785zy.A04(C117305Zk.A0L(), this.A01, null);
                                return;
                            } else if (A002.A02()) {
                                AbstractC136196Lo r4 = this.A01;
                                if (num.intValue() == 2) {
                                    C130785zy r02 = new C130785zy(null, r22);
                                    r02.A01 = A002;
                                    r4.AV8(r02);
                                    return;
                                }
                                C1310460z A0B = C117315Zl.A0B("account", AnonymousClass61S.A02("action", "novi-start-step-up-challenge"));
                                AnonymousClass61S[] r5 = new AnonymousClass61S[3];
                                AnonymousClass61S.A05("type", "MANUAL_REVIEW__AUTO_TRIGGERED", r5, 0);
                                AnonymousClass61S.A05("entry_flow", A002.A03, r5, 1);
                                C117305Zk.A1K(A0B, "step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", A002.A04), r5, 2));
                                r9.A09(new IDxAListenerShape19S0100000_3_I1(r4, 0), A0B, num, "set", 4);
                                return;
                            } else {
                                r9.A03.A0I(new Runnable(A002, num) { // from class: X.6J8
                                    public final /* synthetic */ C1316663q A01;
                                    public final /* synthetic */ Integer A02;

                                    {
                                        this.A01 = r2;
                                        this.A02 = r3;
                                    }

                                    @Override // java.lang.Runnable
                                    public final void run() {
                                        C120275fu r03 = C120275fu.this;
                                        r03.A02.A0E.A01(this.A01, this.A02.intValue());
                                    }
                                });
                                AbstractC136196Lo r13 = this.A01;
                                C130785zy r03 = new C130785zy(null, r22);
                                r03.A01 = A002;
                                r13.AV8(r03);
                                return;
                            }
                        } catch (AnonymousClass1V9 unused) {
                            Log.e("PAY: NoviActionManager/onResponseSuccess can't parse step up");
                            r2 = this.A01;
                            r1 = new C130785zy(C117305Zk.A0L(), null);
                            r2.AV8(r1);
                            return;
                        }
                    } else {
                        r12 = this.A01;
                        r0 = new C130785zy(null, r22);
                    }
                }
                r12.AV8(r0);
            } catch (C124735q1 unused2) {
                Log.e("PAY: NoviActionManager/onResponseSuccess - EncryptionException");
                r2 = this.A01;
                r1 = new C130785zy(C117305Zk.A0L(), null);
            }
        } catch (AnonymousClass1V9 unused3) {
            Log.e("PAY: NoviActionManager/onResponseSuccess can't parse account node");
            C130785zy.A04(C117305Zk.A0L(), this.A01, null);
        }
    }

    @Override // X.AbstractC451020e, X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        try {
            List A0J = r6.A0J("error");
            ArrayList A0l = C12960it.A0l();
            Iterator it = A0J.iterator();
            while (it.hasNext()) {
                A0l.add(C1308960j.A00(this.A02.A0B, C117305Zk.A0d(it)));
            }
            super.APv(new AnonymousClass1V8(r6.A00, r6.A0L(), (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0])), str);
        } catch (C124735q1 unused) {
            Log.e("PAY: NoviActionManager/sendIqInternal can't decrypt error node");
        }
    }
}
