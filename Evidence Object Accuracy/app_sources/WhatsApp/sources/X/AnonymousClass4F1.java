package X;

/* renamed from: X.4F1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4F1 {
    public static final void A00(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            StringBuilder A0k = C12960it.A0k("size=");
            A0k.append(j);
            A0k.append(" offset=");
            A0k.append(j2);
            A0k.append(" byteCount=");
            throw new ArrayIndexOutOfBoundsException(C12970iu.A0w(A0k, j3));
        }
    }
}
