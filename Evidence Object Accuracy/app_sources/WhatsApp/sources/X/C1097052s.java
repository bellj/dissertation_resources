package X;

import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;

/* renamed from: X.52s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1097052s implements AbstractC116455Vm {
    public final /* synthetic */ TextStatusComposerActivity A00;

    public C1097052s(TextStatusComposerActivity textStatusComposerActivity) {
        this.A00 = textStatusComposerActivity;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0T);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A0T, iArr, 0);
    }
}
