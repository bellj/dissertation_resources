package X;

/* renamed from: X.41K  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass41K extends AnonymousClass4RX {
    public final AnonymousClass4NJ A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass41K) && C16700pc.A0O(this.A00, ((AnonymousClass41K) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public AnonymousClass41K(AnonymousClass4NJ r7) {
        super(r7, 11, false, false, false);
        this.A00 = r7;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Error(errorBehaviour=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
