package X;

import java.util.Arrays;

/* renamed from: X.4X8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4X8 {
    public final int A00;
    public final int A01;
    public final int A02;

    public AnonymousClass4X8(int i, int i2, int i3) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass4X8 r5 = (AnonymousClass4X8) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01 && this.A02 == r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, this.A00);
        C12980iv.A1T(objArr, this.A01);
        C12990iw.A1V(objArr, this.A02);
        return Arrays.hashCode(objArr);
    }
}
