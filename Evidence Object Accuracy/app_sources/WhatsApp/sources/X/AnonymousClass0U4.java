package X;

import android.security.identity.IdentityCredential;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* renamed from: X.0U4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U4 {
    public final IdentityCredential A00;
    public final Signature A01;
    public final Cipher A02;
    public final Mac A03;

    public AnonymousClass0U4(IdentityCredential identityCredential) {
        this.A01 = null;
        this.A02 = null;
        this.A03 = null;
        this.A00 = identityCredential;
    }

    public AnonymousClass0U4(Signature signature) {
        this.A01 = signature;
        this.A02 = null;
        this.A03 = null;
        this.A00 = null;
    }

    public AnonymousClass0U4(Cipher cipher) {
        this.A01 = null;
        this.A02 = cipher;
        this.A03 = null;
        this.A00 = null;
    }

    public AnonymousClass0U4(Mac mac) {
        this.A01 = null;
        this.A02 = null;
        this.A03 = mac;
        this.A00 = null;
    }

    public IdentityCredential A00() {
        return this.A00;
    }
}
