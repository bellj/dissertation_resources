package X;

import android.graphics.Rect;

/* renamed from: X.4PZ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PZ {
    public final int A00;
    public final int A01;
    public final Rect A02;

    public AnonymousClass4PZ(Rect rect, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A02 = rect;
    }
}
