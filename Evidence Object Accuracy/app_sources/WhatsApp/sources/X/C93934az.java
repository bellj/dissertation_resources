package X;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.4az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93934az {
    public final C92364Vp A00;
    public final Object A01;

    public C93934az(C92364Vp r2, Object obj) {
        C95094d8.A03(obj, "json can not be null");
        C95094d8.A03(r2, "configuration can not be null");
        this.A00 = r2;
        this.A01 = obj;
    }

    public static final C94734cS A00(String str, AnonymousClass5T6[] r6) {
        ReentrantLock reentrantLock;
        AnonymousClass52L r4 = (AnonymousClass52L) AnonymousClass4G2.A00;
        Map map = r4.A02;
        C94734cS r2 = (C94734cS) map.get(str);
        if (r2 != null) {
            r4.A00(str);
        } else if (str.length() != 0) {
            r2 = new C94734cS(str, r6);
            if (map.put(str, r2) != null) {
                r4.A00(str);
            } else {
                reentrantLock = r4.A03;
                reentrantLock.lock();
                try {
                    r4.A01.addFirst(str);
                } finally {
                }
            }
            if (map.size() > r4.A00) {
                reentrantLock = r4.A03;
                reentrantLock.lock();
                try {
                    String str2 = (String) r4.A01.removeLast();
                    reentrantLock.unlock();
                    map.remove(str2);
                    return r2;
                } finally {
                }
            }
        } else {
            throw C12970iu.A0f("json can not be null or empty");
        }
        return r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004e, code lost:
        if (r6.A05() != false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        if (r7 != false) goto L_0x0082;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A01(java.lang.String r12, X.AnonymousClass5T6... r13) {
        /*
            r11 = this;
            java.lang.String r1 = "path can not be null or empty"
            if (r12 == 0) goto L_0x00de
            int r0 = r12.length()
            if (r0 == 0) goto L_0x00de
            X.4cS r4 = A00(r12, r13)
            java.lang.String r0 = "path can not be null"
            X.C95094d8.A03(r4, r0)
            java.lang.Object r10 = r11.A01
            X.4Vp r5 = r11.A00
            X.4Ad r9 = X.EnumC87084Ad.AS_PATH_LIST
            java.util.Set r1 = r5.A03
            boolean r8 = r1.contains(r9)
            X.4Ad r3 = X.EnumC87084Ad.ALWAYS_RETURN_LIST
            boolean r7 = r1.contains(r3)
            X.4Ad r0 = X.EnumC87084Ad.SUPPRESS_EXCEPTIONS
            boolean r2 = r1.contains(r0)
            X.4Wl r1 = r4.A00
            X.3wl r6 = r1.A00
            X.4YR r0 = r6.A01
            boolean r0 = r0 instanceof X.C83133wk
            r4 = 0
            if (r0 == 0) goto L_0x0070
            if (r8 != 0) goto L_0x0051
            if (r7 != 0) goto L_0x0051
            X.4bk r1 = r1.A00(r5, r10, r10)
            if (r2 == 0) goto L_0x00d6
            java.util.List r0 = r1.A01()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x00d6
        L_0x004a:
            boolean r0 = r6.A05()
            if (r0 == 0) goto L_0x0082
        L_0x0050:
            return r4
        L_0x0051:
            if (r2 != 0) goto L_0x004a
            java.lang.String r0 = "Options "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r9)
            java.lang.String r0 = " and "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " are not allowed when using path functions!"
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            X.5H8 r0 = new X.5H8
            r0.<init>(r1)
            throw r0
        L_0x0070:
            X.4bk r3 = r1.A00(r5, r10, r10)
            if (r8 == 0) goto L_0x008d
            if (r2 == 0) goto L_0x009c
            java.util.List r0 = r3.A01()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x009c
        L_0x0082:
            X.5Xw r0 = r5.A00
            X.52M r0 = (X.AnonymousClass52M) r0
            X.4YL r0 = r0.A01
            java.lang.Object r4 = r0.A01()
            return r4
        L_0x008d:
            if (r2 == 0) goto L_0x00ba
            java.util.List r0 = r3.A01()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x00ba
            if (r7 == 0) goto L_0x004a
            goto L_0x0082
        L_0x009c:
            int r0 = r3.A00
            if (r0 != 0) goto L_0x00db
            boolean r0 = r3.A09
            if (r0 != 0) goto L_0x0050
            java.lang.String r0 = "No results for path: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            X.4Wl r0 = r3.A02
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            X.3wD r0 = new X.3wD
            r0.<init>(r1)
            throw r0
        L_0x00ba:
            r2 = 0
            java.lang.Object r4 = r3.A00()
            if (r7 == 0) goto L_0x0050
            boolean r0 = r6.A05()
            if (r0 == 0) goto L_0x0050
            X.5Xw r1 = r5.A00
            r0 = r1
            X.52M r0 = (X.AnonymousClass52M) r0
            X.4YL r0 = r0.A01
            java.lang.Object r0 = r0.A01()
            r1.Abk(r0, r2, r4)
            return r0
        L_0x00d6:
            java.lang.Object r4 = r1.A00()
            return r4
        L_0x00db:
            java.lang.Object r4 = r3.A03
            return r4
        L_0x00de:
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C93934az.A01(java.lang.String, X.5T6[]):java.lang.Object");
    }
}
