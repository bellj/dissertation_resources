package X;

/* renamed from: X.1mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37661mn extends C37651mm {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C37661mn(java.lang.String r3, java.lang.Exception r4) {
        /*
            r2 = this;
            boolean r0 = r4 instanceof java.net.UnknownHostException
            if (r0 == 0) goto L_0x0009
            r1 = 2
        L_0x0005:
            r2.<init>(r4, r3, r1)
            return
        L_0x0009:
            boolean r0 = r4 instanceof java.net.SocketTimeoutException
            r1 = 19
            if (r0 == 0) goto L_0x0005
            r1 = 3
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37661mn.<init>(java.lang.String, java.lang.Exception):void");
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("ConnectionFailureException: ");
        sb.append(getMessage());
        return sb.toString();
    }
}
