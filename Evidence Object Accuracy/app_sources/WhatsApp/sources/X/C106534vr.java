package X;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.ConditionVariable;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.4vr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106534vr implements AnonymousClass5XY {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public AudioTrack A0D;
    public C94344be A0E;
    public AnonymousClass3HR A0F;
    public AnonymousClass5XL A0G;
    public AnonymousClass4WG A0H;
    public AnonymousClass4XS A0I;
    public AnonymousClass4XS A0J;
    public C91214Qw A0K;
    public C91214Qw A0L;
    public AnonymousClass4X4 A0M;
    public ByteBuffer A0N;
    public ByteBuffer A0O;
    public ByteBuffer A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public byte[] A0Y;
    public AnonymousClass5Xx[] A0Z;
    public ByteBuffer[] A0a;
    public final ConditionVariable A0b = new ConditionVariable(true);
    public final AnonymousClass3IX A0c;
    public final AnonymousClass4XV A0d = new AnonymousClass4XV(new C106544vs(this));
    public final C76653lz A0e;
    public final AnonymousClass5Q1 A0f;
    public final C90724Oz A0g;
    public final C90724Oz A0h;
    public final C76663m0 A0i;
    public final ArrayDeque A0j;
    public final AnonymousClass5Xx[] A0k;
    public final AnonymousClass5Xx[] A0l;

    public C106534vr(AnonymousClass3IX r8, AnonymousClass5Q1 r9) {
        this.A0c = r8;
        this.A0f = r9;
        C76653lz r4 = new C76653lz();
        this.A0e = r4;
        C76663m0 r3 = new C76663m0();
        this.A0i = r3;
        ArrayList A0l = C12960it.A0l();
        Collections.addAll(A0l, new C76643ly(), r4, r3);
        Collections.addAll(A0l, ((C106554vt) r9).A02);
        this.A0l = (AnonymousClass5Xx[]) A0l.toArray(new AnonymousClass5Xx[0]);
        this.A0k = new AnonymousClass5Xx[]{new C76683m2()};
        this.A00 = 1.0f;
        this.A0F = AnonymousClass3HR.A03;
        this.A01 = 0;
        this.A0H = new AnonymousClass4WG();
        C94344be r1 = C94344be.A03;
        this.A0L = new C91214Qw(r1, 0, 0, false);
        this.A0E = r1;
        this.A03 = -1;
        this.A0Z = new AnonymousClass5Xx[0];
        this.A0a = new ByteBuffer[0];
        this.A0j = new ArrayDeque();
        this.A0g = new C90724Oz();
        this.A0h = new C90724Oz();
    }

    public static int A00(int i) {
        AudioAttributes build = new AudioAttributes.Builder().setUsage(1).setContentType(3).build();
        int i2 = 8;
        while (!AudioTrack.isDirectPlaybackSupported(new AudioFormat.Builder().setEncoding(18).setSampleRate(i).setChannelMask(AnonymousClass3JZ.A00(i2)).build(), build)) {
            i2--;
            if (i2 <= 0) {
                return 0;
            }
        }
        return i2;
    }

    public static int A01(AudioTrack audioTrack, ByteBuffer byteBuffer, int i) {
        return audioTrack.write(byteBuffer, i, 1);
    }

    public static long A02(AnonymousClass4XV r2, long j) {
        return (j * SearchActionVerificationClientService.MS_TO_NS) / ((long) r2.A04);
    }

    public static AudioFormat A03(int i, int i2, int i3) {
        return new AudioFormat.Builder().setSampleRate(i).setChannelMask(i2).setEncoding(i3).build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0086, code lost:
        if (r2 == 14) goto L_0x0088;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0078 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.Pair A04(X.C100614mC r7, X.AnonymousClass3IX r8) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.A04(X.4mC, X.3IX):android.util.Pair");
    }

    public static void A05(AudioTrack audioTrack, float f) {
        audioTrack.setVolume(f);
    }

    public final int A06(AudioTrack audioTrack, ByteBuffer byteBuffer, int i, long j) {
        if (AnonymousClass3JZ.A01 >= 26) {
            return audioTrack.write(byteBuffer, i, 1, j * 1000);
        }
        if (this.A0N == null) {
            ByteBuffer allocate = ByteBuffer.allocate(16);
            this.A0N = allocate;
            allocate.order(ByteOrder.BIG_ENDIAN);
            this.A0N.putInt(1431633921);
        }
        if (this.A02 == 0) {
            this.A0N.putInt(4, i);
            this.A0N.putLong(8, j * 1000);
            this.A0N.position(0);
            this.A02 = i;
        }
        int remaining = this.A0N.remaining();
        if (remaining > 0) {
            int write = audioTrack.write(this.A0N, remaining, 1);
            if (write < 0) {
                this.A02 = 0;
                return write;
            } else if (write < remaining) {
                return 0;
            }
        }
        int write2 = audioTrack.write(byteBuffer, i, 1);
        if (write2 < 0) {
            this.A02 = 0;
            return write2;
        }
        this.A02 -= write2;
        return write2;
    }

    public final long A07() {
        AnonymousClass4XS r1 = this.A0I;
        if (r1.A04 == 0) {
            return this.A0C / ((long) r1.A05);
        }
        return this.A0B;
    }

    public final C91214Qw A08() {
        C91214Qw r0 = this.A0K;
        if (r0 != null) {
            return r0;
        }
        ArrayDeque arrayDeque = this.A0j;
        if (!arrayDeque.isEmpty()) {
            return (C91214Qw) arrayDeque.getLast();
        }
        return this.A0L;
    }

    public final void A09() {
        if (!this.A0W) {
            this.A0W = true;
            AnonymousClass4XV r6 = this.A0d;
            long A07 = A07();
            r6.A0K = r6.A00();
            r6.A0L = SystemClock.elapsedRealtime() * 1000;
            r6.A07 = A07;
            this.A0D.stop();
            this.A02 = 0;
        }
    }

    public final void A0A(long j) {
        C94344be r4;
        boolean z;
        if (this.A0X || !"audio/raw".equals(this.A0I.A07.A0T)) {
            r4 = C94344be.A03;
        } else {
            AnonymousClass5Q1 r1 = this.A0f;
            r4 = A08().A02;
            C106494vn r2 = ((C106554vt) r1).A01;
            float f = r4.A01;
            if (r2.A01 != f) {
                r2.A01 = f;
                r2.A0E = true;
            }
            float f2 = r4.A00;
            if (r2.A00 != f2) {
                r2.A00 = f2;
                r2.A0E = true;
            }
        }
        if (this.A0X || !"audio/raw".equals(this.A0I.A07.A0T)) {
            z = false;
        } else {
            AnonymousClass5Q1 r12 = this.A0f;
            z = A08().A03;
            ((C106554vt) r12).A00.A05 = z;
        }
        this.A0j.add(new C91214Qw(r4, Math.max(0L, j), (A07() * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A0I.A06), z));
        AnonymousClass5Xx[] r5 = this.A0I.A08;
        ArrayList A0l = C12960it.A0l();
        for (AnonymousClass5Xx r13 : r5) {
            if (r13.AJD()) {
                A0l.add(r13);
            } else {
                r13.flush();
            }
        }
        int size = A0l.size();
        this.A0Z = (AnonymousClass5Xx[]) A0l.toArray(new AnonymousClass5Xx[size]);
        this.A0a = new ByteBuffer[size];
        int i = 0;
        while (true) {
            AnonymousClass5Xx[] r14 = this.A0Z;
            if (i >= r14.length) {
                break;
            }
            AnonymousClass5Xx r0 = r14[i];
            r0.flush();
            this.A0a[i] = r0.AEn();
            i++;
        }
        AnonymousClass5XL r02 = this.A0G;
        if (r02 != null) {
            r02.AW9(z);
        }
    }

    public final void A0B(long j) {
        ByteBuffer byteBuffer;
        int length = this.A0Z.length;
        int i = length;
        do {
            if (i <= 0) {
                byteBuffer = this.A0O;
                if (byteBuffer == null) {
                    byteBuffer = AnonymousClass5Xx.A00;
                }
            } else {
                byteBuffer = this.A0a[i - 1];
            }
            if (i == length) {
                A0E(byteBuffer, j);
            } else {
                AnonymousClass5Xx r1 = this.A0Z[i];
                if (i > this.A03) {
                    r1.AZk(byteBuffer);
                }
                ByteBuffer AEn = r1.AEn();
                this.A0a[i] = AEn;
                if (AEn.hasRemaining()) {
                    i++;
                    continue;
                }
            }
            if (!byteBuffer.hasRemaining()) {
                i--;
                continue;
            } else {
                return;
            }
        } while (i >= 0);
    }

    public final void A0C(AudioTrack audioTrack) {
        AnonymousClass4X4 r0 = this.A0M;
        if (r0 == null) {
            r0 = new AnonymousClass4X4(this);
            this.A0M = r0;
        }
        r0.A00(audioTrack);
    }

    public final void A0D(C94344be r9, boolean z) {
        C91214Qw A08 = A08();
        if (!r9.equals(A08.A02) || z != A08.A03) {
            C91214Qw r1 = new C91214Qw(r9, -9223372036854775807L, -9223372036854775807L, z);
            if (this.A0D != null) {
                this.A0K = r1;
            } else {
                this.A0L = r1;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0063, code lost:
        if (r5 == -32) goto L_0x0065;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0E(java.nio.ByteBuffer r17, long r18) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.A0E(java.nio.ByteBuffer, long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r9.A0P != null) goto L_0x0038;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0F() {
        /*
            r9 = this;
            int r0 = r9.A03
            r8 = -1
            r7 = 1
            r6 = 0
            if (r0 != r8) goto L_0x002b
            r9.A03 = r6
            r0 = 0
        L_0x000a:
            r5 = 1
        L_0x000b:
            X.5Xx[] r4 = r9.A0Z
            int r3 = r4.length
            r1 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r0 >= r3) goto L_0x002d
            r0 = r4[r0]
            if (r5 == 0) goto L_0x001c
            r0.AZj()
        L_0x001c:
            r9.A0B(r1)
            boolean r0 = r0.AJN()
            if (r0 == 0) goto L_0x0038
            int r0 = r9.A03
            int r0 = r0 + r7
            r9.A03 = r0
            goto L_0x000a
        L_0x002b:
            r5 = 0
            goto L_0x000b
        L_0x002d:
            java.nio.ByteBuffer r0 = r9.A0P
            if (r0 == 0) goto L_0x0039
            r9.A0E(r0, r1)
            java.nio.ByteBuffer r0 = r9.A0P
            if (r0 == 0) goto L_0x0039
        L_0x0038:
            return r6
        L_0x0039:
            r9.A03 = r8
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.A0F():boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    @Override // X.AnonymousClass5XY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7W(X.C100614mC r14, int[] r15, int r16) {
        /*
            r13 = this;
            r2 = 0
            r5 = r14
            java.lang.String r1 = r14.A0T
            java.lang.String r0 = "audio/raw"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0064
            int r3 = r14.A0B
            boolean r0 = X.AnonymousClass3JZ.A0G(r3)
            X.C95314dV.A03(r0)
            int r2 = r14.A06
            int r7 = X.AnonymousClass3JZ.A03(r3, r2)
            X.5Xx[] r6 = r13.A0l
            X.3m0 r4 = r13.A0i
            int r1 = r14.A07
            int r0 = r14.A08
            r4.A03 = r1
            r4.A02 = r0
            int r1 = X.AnonymousClass3JZ.A01
            r0 = 21
            if (r1 >= r0) goto L_0x003e
            r0 = 8
            if (r2 != r0) goto L_0x003e
            if (r15 != 0) goto L_0x003e
            r1 = 6
            int[] r15 = new int[r1]
            r0 = 0
        L_0x0037:
            r15[r0] = r0
            int r0 = r0 + 1
            if (r0 >= r1) goto L_0x003e
            goto L_0x0037
        L_0x003e:
            X.3lz r0 = r13.A0e
            r0.A01 = r15
            int r0 = r14.A0F
            X.4bE r4 = new X.4bE
            r4.<init>(r0, r2, r3)
            int r3 = r6.length
            r2 = 0
        L_0x004b:
            if (r2 >= r3) goto L_0x0080
            r0 = r6[r2]
            X.4bE r1 = r0.A7V(r4)     // Catch: 4Bk -> 0x005d
            boolean r0 = r0.AJD()     // Catch: 4Bk -> 0x005d
            if (r0 == 0) goto L_0x005a
            r4 = r1
        L_0x005a:
            int r2 = r2 + 1
            goto L_0x004b
        L_0x005d:
            r1 = move-exception
            X.4CG r0 = new X.4CG
            r0.<init>(r5, r1)
            throw r0
        L_0x0064:
            X.5Xx[] r6 = new X.AnonymousClass5Xx[r2]
            int r10 = r14.A0F
            X.3IX r0 = r13.A0c
            android.util.Pair r1 = A04(r14, r0)
            if (r1 == 0) goto L_0x00ca
            java.lang.Object r0 = r1.first
            int r12 = X.C12960it.A05(r0)
            java.lang.Object r0 = r1.second
            int r11 = X.C12960it.A05(r0)
            r7 = -1
            r9 = -1
            r8 = 2
            goto L_0x008f
        L_0x0080:
            int r12 = r4.A02
            int r10 = r4.A03
            int r0 = r4.A01
            int r11 = X.AnonymousClass3JZ.A00(r0)
            int r9 = X.AnonymousClass3JZ.A03(r12, r0)
            r8 = 0
        L_0x008f:
            java.lang.String r1 = ") for: "
            if (r12 == 0) goto L_0x00b7
            if (r11 == 0) goto L_0x00a4
            X.4XS r4 = new X.4XS
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12)
            android.media.AudioTrack r0 = r13.A0D
            if (r0 == 0) goto L_0x00a1
            r13.A0J = r4
            return
        L_0x00a1:
            r13.A0I = r4
            return
        L_0x00a4:
            java.lang.String r0 = "Invalid output channel config (mode="
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            r0.append(r8)
            java.lang.String r1 = X.C12960it.A0Z(r14, r1, r0)
            X.4CG r0 = new X.4CG
            r0.<init>(r14, r1)
            throw r0
        L_0x00b7:
            java.lang.String r0 = "Invalid output encoding (mode="
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            r0.append(r8)
            java.lang.String r1 = X.C12960it.A0Z(r14, r1, r0)
            X.4CG r0 = new X.4CG
            r0.<init>(r14, r1)
            throw r0
        L_0x00ca:
            java.lang.String r0 = "Unable to configure passthrough for: "
            java.lang.String r1 = X.C12960it.A0b(r0, r14)
            X.4CG r0 = new X.4CG
            r0.<init>(r14, r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.A7W(X.4mC, int[], int):void");
    }

    @Override // X.AnonymousClass5XY
    public long ACA(boolean z) {
        boolean z2;
        long j;
        ArrayDeque arrayDeque;
        long j2;
        long j3;
        Method method;
        C106544vs r13;
        String str;
        long j4;
        if (this.A0D == null || this.A0U) {
            return Long.MIN_VALUE;
        }
        AnonymousClass4XV r9 = this.A0d;
        if (r9.A0M.getPlayState() == 3) {
            long A02 = A02(r9, r9.A00());
            long j5 = 0;
            if (A02 != 0) {
                long A0D = C12980iv.A0D(System.nanoTime());
                if (A0D - r9.A0A >= C26061Bw.A0L) {
                    long[] jArr = r9.A0V;
                    int i = r9.A02;
                    jArr[i] = A02 - A0D;
                    r9.A02 = (i + 1) % 10;
                    int i2 = r9.A05;
                    if (i2 < 10) {
                        i2++;
                        r9.A05 = i2;
                    }
                    r9.A0A = A0D;
                    r9.A0J = 0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        j5 += jArr[i3] / ((long) i2);
                        r9.A0J = j5;
                    }
                }
                if (!r9.A0S) {
                    C92914Xz r10 = r9.A0N;
                    if (r10.A03(A0D)) {
                        long A01 = r10.A01();
                        long A00 = r10.A00();
                        if (Math.abs(A01 - A0D) > 5000000) {
                            r13 = (C106544vs) r9.A0U;
                            str = "Spurious audio timestamp (system clock mismatch): ";
                        } else if (Math.abs(A02(r9, A00) - A02) > 5000000) {
                            r13 = (C106544vs) r9.A0U;
                            str = "Spurious audio timestamp (frame position mismatch): ";
                        } else if (r10.A00 == 4) {
                            r10.A02();
                        }
                        StringBuilder A0k = C12960it.A0k(str);
                        A0k.append(A00);
                        A0k.append(", ");
                        A0k.append(A01);
                        A0k.append(", ");
                        A0k.append(A0D);
                        A0k.append(", ");
                        A0k.append(A02);
                        A0k.append(", ");
                        C106534vr r6 = r13.A00;
                        AnonymousClass4XS r4 = r6.A0I;
                        if (r4.A04 == 0) {
                            j4 = r6.A0A / ((long) r4.A01);
                        } else {
                            j4 = r6.A09;
                        }
                        A0k.append(j4);
                        A0k.append(", ");
                        Log.w("DefaultAudioSink", C12970iu.A0w(A0k, r6.A07()));
                        r10.A00 = 4;
                        r10.A04 = 500000;
                    }
                    if (r9.A0Q && (method = r9.A0O) != null && A0D - r9.A09 >= 500000) {
                        try {
                            long intValue = (((long) ((Integer) method.invoke(r9.A0M, new Object[0])).intValue()) * 1000) - r9.A06;
                            r9.A0E = intValue;
                            long max = Math.max(intValue, 0L);
                            r9.A0E = max;
                            if (max > 5000000) {
                                Log.w("DefaultAudioSink", C12970iu.A0w(C12960it.A0k("Ignoring impossibly large audio latency: "), max));
                                r9.A0E = 0;
                            }
                        } catch (Exception unused) {
                            r9.A0O = null;
                        }
                        r9.A09 = A0D;
                    }
                }
            }
        }
        long nanoTime = System.nanoTime() / 1000;
        C92914Xz r2 = r9.A0N;
        if (r2.A00 == 2) {
            z2 = true;
            long A022 = A02(r9, r2.A00());
            long A012 = nanoTime - r2.A01();
            float f = r9.A00;
            if (f != 1.0f) {
                A012 = Math.round(((double) A012) * ((double) f));
            }
            j = A022 + A012;
        } else {
            z2 = false;
            if (r9.A05 == 0) {
                j = A02(r9, r9.A00());
            } else {
                j = r9.A0J + nanoTime;
            }
            if (!z) {
                j = Math.max(0L, j - r9.A0E);
            }
        }
        if (r9.A0R != z2) {
            r9.A0H = r9.A0D;
            r9.A0G = r9.A0B;
        }
        long j6 = nanoTime - r9.A0H;
        if (j6 < SearchActionVerificationClientService.MS_TO_NS) {
            long j7 = r9.A0G;
            float f2 = r9.A00;
            long j8 = j6;
            if (f2 != 1.0f) {
                j8 = Math.round(((double) j6) * ((double) f2));
            }
            long j9 = (j6 * 1000) / SearchActionVerificationClientService.MS_TO_NS;
            j = ((j * j9) + ((1000 - j9) * (j7 + j8))) / 1000;
        }
        if (!r9.A0T) {
            long j10 = r9.A0B;
            if (j > j10) {
                r9.A0T = true;
                long A023 = C95214dK.A02(j - j10);
                float f3 = r9.A00;
                if (f3 != 1.0f) {
                    A023 = Math.round(((double) A023) / ((double) f3));
                }
                long currentTimeMillis = System.currentTimeMillis() - C95214dK.A02(A023);
                AnonymousClass5XL r22 = ((C106544vs) r9.A0U).A00.A0G;
                if (r22 != null) {
                    r22.ATv(currentTimeMillis);
                }
            }
        }
        r9.A0D = nanoTime;
        r9.A0B = j;
        r9.A0R = z2;
        long min = Math.min(j, (A07() * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A0I.A06));
        while (true) {
            arrayDeque = this.A0j;
            if (arrayDeque.isEmpty() || min < ((C91214Qw) arrayDeque.getFirst()).A00) {
                break;
            }
            this.A0L = (C91214Qw) arrayDeque.remove();
        }
        C91214Qw r23 = this.A0L;
        long j11 = min - r23.A00;
        if (r23.A02.equals(C94344be.A03)) {
            j3 = r23.A01;
        } else if (arrayDeque.isEmpty()) {
            C106494vn r24 = ((C106554vt) this.A0f).A01;
            long j12 = r24.A04;
            if (j12 >= 1024) {
                long j13 = r24.A03;
                AnonymousClass4YE r0 = r24.A09;
                long j14 = j13 - ((long) ((r0.A00 * r0.A0G) << 1));
                int i4 = r24.A06.A03;
                int i5 = r24.A05.A03;
                if (i4 != i5) {
                    j14 *= (long) i4;
                    j12 *= (long) i5;
                }
                j11 = AnonymousClass3JZ.A07(j11, j14, j12);
            } else {
                j11 = (long) (((double) r24.A01) * ((double) j11));
            }
            j3 = this.A0L.A01;
        } else {
            C91214Qw r62 = (C91214Qw) arrayDeque.getFirst();
            long j15 = r62.A00 - min;
            float f4 = this.A0L.A02.A01;
            if (f4 != 1.0f) {
                j15 = Math.round(((double) j15) * ((double) f4));
            }
            j2 = r62.A01 - j15;
            return j2 + ((((C106554vt) this.A0f).A00.A04 * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A0I.A06));
        }
        j2 = j3 + j11;
        return j2 + ((((C106554vt) this.A0f).A00.A04 * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A0I.A06));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c A[RETURN] */
    @Override // X.AnonymousClass5XY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AD7(X.C100614mC r5) {
        /*
            r4 = this;
            java.lang.String r1 = r5.A0T
            java.lang.String r0 = "audio/raw"
            boolean r0 = r0.equals(r1)
            r3 = 0
            r2 = 2
            if (r0 == 0) goto L_0x0024
            int r1 = r5.A0B
            boolean r0 = X.AnonymousClass3JZ.A0G(r1)
            if (r0 != 0) goto L_0x0020
            java.lang.String r0 = "Invalid PCM encoding: "
            java.lang.String r1 = X.C12960it.A0W(r1, r0)
            java.lang.String r0 = "DefaultAudioSink"
            android.util.Log.w(r0, r1)
        L_0x001f:
            return r3
        L_0x0020:
            if (r1 == r2) goto L_0x002c
            r0 = 1
            return r0
        L_0x0024:
            X.3IX r0 = r4.A0c
            android.util.Pair r0 = A04(r5, r0)
            if (r0 == 0) goto L_0x001f
        L_0x002c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.AD7(X.4mC):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r25 == r2) goto L_0x000d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x0467  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0486  */
    @Override // X.AnonymousClass5XY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AHx(java.nio.ByteBuffer r25, int r26, long r27) {
        /*
        // Method dump skipped, instructions count: 1250
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106534vr.AHx(java.nio.ByteBuffer, int, long):boolean");
    }

    @Override // X.AnonymousClass5XY
    public boolean AIH() {
        if (this.A0D == null) {
            return false;
        }
        AnonymousClass4XV r5 = this.A0d;
        if (A07() <= r5.A00()) {
            return r5.A0S && r5.A0M.getPlayState() == 2 && r5.A00() == 0;
        }
        return true;
    }

    @Override // X.AnonymousClass5XY
    public void AcX(C94344be r5) {
        A0D(new C94344be(Math.max(0.1f, Math.min(r5.A01, 8.0f)), Math.max(0.1f, Math.min(r5.A00, 8.0f))), A08().A03);
    }

    @Override // X.AnonymousClass5XY
    public void flush() {
        if (this.A0D != null) {
            this.A0A = 0;
            this.A09 = 0;
            this.A0C = 0;
            this.A0B = 0;
            this.A0S = false;
            this.A04 = 0;
            this.A0L = new C91214Qw(A08().A02, 0, 0, A08().A03);
            this.A08 = 0;
            this.A0K = null;
            this.A0j.clear();
            this.A0O = null;
            this.A05 = 0;
            this.A0P = null;
            this.A0W = false;
            this.A0R = false;
            this.A03 = -1;
            this.A0N = null;
            this.A02 = 0;
            this.A0i.A04 = 0;
            int i = 0;
            while (true) {
                AnonymousClass5Xx[] r1 = this.A0Z;
                if (i >= r1.length) {
                    break;
                }
                AnonymousClass5Xx r0 = r1[i];
                r0.flush();
                this.A0a[i] = r0.AEn();
                i++;
            }
            AnonymousClass4XV r4 = this.A0d;
            if (r4.A0M.getPlayState() == 3) {
                this.A0D.pause();
            }
            AudioTrack audioTrack = this.A0D;
            int i2 = AnonymousClass3JZ.A01;
            if (i2 >= 29 && audioTrack.isOffloadedPlayback()) {
                this.A0M.A01(this.A0D);
            }
            AudioTrack audioTrack2 = this.A0D;
            this.A0D = null;
            if (i2 < 21 && !this.A0Q) {
                this.A01 = 0;
            }
            AnonymousClass4XS r02 = this.A0J;
            if (r02 != null) {
                this.A0I = r02;
                this.A0J = null;
            }
            r4.A0J = 0;
            r4.A05 = 0;
            r4.A02 = 0;
            r4.A0A = 0;
            r4.A0D = 0;
            r4.A0H = 0;
            r4.A0T = false;
            r4.A0M = null;
            r4.A0N = null;
            this.A0b.close();
            new AnonymousClass5HD(audioTrack2, this).start();
        }
        this.A0h.A01 = null;
        this.A0g.A01 = null;
    }

    @Override // X.AnonymousClass5XY
    public void reset() {
        flush();
        for (AnonymousClass5Xx r0 : this.A0l) {
            r0.reset();
        }
        for (AnonymousClass5Xx r02 : this.A0k) {
            r02.reset();
        }
        this.A0T = false;
    }
}
