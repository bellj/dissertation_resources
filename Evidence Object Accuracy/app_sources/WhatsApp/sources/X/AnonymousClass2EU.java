package X;

import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2EU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2EU extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ UserJid $bizJid;
    public final /* synthetic */ CatalogCategoryTabsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2EU(CatalogCategoryTabsViewModel catalogCategoryTabsViewModel, UserJid userJid) {
        super(1);
        this.this$0 = catalogCategoryTabsViewModel;
        this.$bizJid = userJid;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AnonymousClass4K6 r8 = (AnonymousClass4K6) obj;
        C16700pc.A0E(r8, 0);
        if (r8 instanceof C60252wM) {
            List list = ((C60252wM) r8).A01;
            AnonymousClass017 r6 = (AnonymousClass017) this.this$0.A04.getValue();
            UserJid userJid = this.$bizJid;
            ArrayList arrayList = new ArrayList();
            for (Object obj2 : list) {
                if (!((AnonymousClass4SX) obj2).A04) {
                    arrayList.add(obj2);
                }
            }
            ArrayList arrayList2 = new ArrayList(C16760pi.A0D(arrayList));
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass4SX r0 = (AnonymousClass4SX) it.next();
                String str = r0.A02;
                C16700pc.A0B(str);
                String str2 = r0.A01;
                C16700pc.A0B(str2);
                arrayList2.add(new AnonymousClass3F7(userJid, str, str2));
            }
            r6.A0A(arrayList2);
        }
        return AnonymousClass1WZ.A00;
    }
}
