package X;

/* renamed from: X.3wM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82893wM extends AbstractC111885Be {
    public int A00;

    public /* synthetic */ C82893wM(Object obj, int i) {
        super(obj);
        this.A00 = i;
    }

    @Override // X.AbstractC111885Be
    public int A00(AbstractC111885Be r3) {
        if (r3 instanceof C82893wM) {
            return AnonymousClass048.A00(((C82893wM) r3).A00, this.A00);
        }
        return super.compareTo(r3);
    }
}
