package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72613et extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass2CF A00;
    public final /* synthetic */ Runnable A01;

    public C72613et(AnonymousClass2CF r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2CF r1 = this.A00;
        r1.A03();
        Runnable runnable = this.A01;
        if (runnable != null) {
            r1.post(runnable);
        }
    }
}
