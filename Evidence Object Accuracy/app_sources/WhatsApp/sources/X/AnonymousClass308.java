package X;

/* renamed from: X.308  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass308 extends AbstractC16110oT {
    public String A00;

    public AnonymousClass308() {
        super(2190, new AnonymousClass00E(1, 20, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamLanguageNotRenderable {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "missingLanguage", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
