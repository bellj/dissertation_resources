package X;

import android.view.KeyEvent;

/* renamed from: X.0Y5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Y5 implements AbstractC001300o {
    public final /* synthetic */ AnonymousClass04T A00;

    public AnonymousClass0Y5(AnonymousClass04T r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC001300o
    public boolean AeZ(KeyEvent keyEvent) {
        return this.A00.A02(keyEvent);
    }
}
