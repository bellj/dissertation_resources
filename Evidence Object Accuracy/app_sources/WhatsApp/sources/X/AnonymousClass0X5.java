package X;

import android.widget.PopupWindow;

/* renamed from: X.0X5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X5 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ C05530Px A00;

    public AnonymousClass0X5(C05530Px r1) {
        this.A00 = r1;
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        this.A00.A02();
    }
}
