package X;

/* renamed from: X.0I7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I7 extends C06230Sr {
    public static final int A00(int i) {
        if (i >= 48 && i <= 57) {
            return i - 48;
        }
        int i2 = 65;
        if (i < 65 || i > 70) {
            i2 = 97;
            if (i < 97 || i > 102) {
                return -1;
            }
        }
        return (i - i2) + 10;
    }

    public AnonymousClass0I7(String str) {
        super(str.replaceAll("(?s)/\\*.*?\\*/", ""));
    }

    public String A0H() {
        char charAt;
        if (A0D() || ((charAt = this.A03.charAt(this.A01)) != '\'' && charAt != '\"')) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        this.A01++;
        loop0: while (true) {
            int intValue = A09().intValue();
            while (intValue != -1 && intValue != charAt) {
                if (intValue == 92) {
                    intValue = A09().intValue();
                    if (intValue == -1) {
                        break loop0;
                    } else if (!(intValue == 10 || intValue == 13 || intValue == 12)) {
                        int A00 = A00(intValue);
                        if (A00 != -1) {
                            int i = 1;
                            do {
                                intValue = A09().intValue();
                                int A002 = A00(intValue);
                                if (A002 != -1) {
                                    A00 = (A00 << 4) + A002;
                                    i++;
                                }
                                sb.append((char) A00);
                            } while (i <= 5);
                            sb.append((char) A00);
                        }
                    }
                }
                sb.append((char) intValue);
            }
        }
        return sb.toString();
    }

    public String A0I() {
        int i;
        boolean A0D = A0D();
        int i2 = this.A01;
        if (A0D) {
            i = i2;
        } else {
            int charAt = this.A03.charAt(i2);
            if (charAt == 45) {
                charAt = A05();
            }
            if ((charAt < 65 || charAt > 90) && ((charAt < 97 || charAt > 122) && charAt != 95)) {
                i = i2;
            } else {
                while (true) {
                    int A05 = A05();
                    if (A05 < 65 || A05 > 90) {
                        if (A05 < 97 || A05 > 122) {
                            if (A05 < 48 || A05 > 57) {
                                if (!(A05 == 45 || A05 == 95)) {
                                    break;
                                }
                            }
                        }
                    }
                }
                i = this.A01;
            }
            this.A01 = i2;
        }
        if (i == i2) {
            return null;
        }
        String substring = this.A03.substring(i2, i);
        this.A01 = i;
        return substring;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0227, code lost:
        if (A0F('-') == false) goto L_0x0229;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x02ff, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("Invalid or missing parameter section for pseudo class: ");
        r1.append(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0315, code lost:
        throw new X.C03960Jv(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0316, code lost:
        r25.A01 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x036d, code lost:
        if (A0F(')') != false) goto L_0x036f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x03e5, code lost:
        throw new X.C03960Jv("Invalid attribute simpleSelectors");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0413, code lost:
        r0 = r2.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0415, code lost:
        if (r0 == null) goto L_0x0420;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x041b, code lost:
        if (r0.isEmpty() != false) goto L_0x0420;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x041d, code lost:
        r14.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0420, code lost:
        return r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01b5, code lost:
        if (r4 == X.AnonymousClass0JV.nth_of_type) goto L_0x01b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01c1, code lost:
        if (r4 == X.AnonymousClass0JV.nth_last_of_type) goto L_0x01c3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x038c  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x03a5  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x03da A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x040f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A0J() {
        /*
        // Method dump skipped, instructions count: 1110
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0I7.A0J():java.util.List");
    }
}
