package X;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.5iK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121475iK extends AbstractActivityC119245dP {
    public View A00;
    public FrameLayout A01;
    public FrameLayout A02;
    public AnonymousClass18U A03;
    public TextEmojiLabel A04;
    public WaImageView A05;
    public WaTextView A06;
    public WaTextView A07;
    public WaTextView A08;
    public C15610nY A09;
    public C17050qB A0A;
    public C16590pI A0B;
    public AnonymousClass018 A0C;
    public C15650ng A0D;
    public C16120oU A0E;
    public C18650sn A0F;
    public C18600si A0G;
    public C18610sj A0H;
    public C22710zW A0I;
    public C18620sk A0J;
    public C17070qD A0K;
    public AnonymousClass60T A0L;
    public AnonymousClass60U A0M;
    public AnonymousClass61E A0N;
    public AnonymousClass605 A0O;
    public C130015yf A0P;
    public C25871Bd A0Q;
    public C123595nP A0R;
    public AnonymousClass65w A0S;
    public C18590sh A0T;
    public C252018m A0U;
    public String A0V;
    public String A0W = "";
    public String A0X = "";

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:84:0x0124 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v0, types: [android.widget.TextView, com.whatsapp.TextEmojiLabel] */
    /* JADX WARN: Type inference failed for: r8v0, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r8v5, types: [android.text.SpannableString] */
    /* JADX WARN: Type inference failed for: r8v6, types: [android.text.SpannableStringBuilder] */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x017f, code lost:
        if (r12 == null) goto L_0x0181;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x02cf, code lost:
        r1 = X.C12960it.A0h();
        r1.append("dyiReportManager/validate-state/report-message-missing for account type = ");
        com.whatsapp.util.Log.e(X.C12960it.A0d(r6, r1));
        r5.A0A.A0E(r6);
     */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x02c6  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02e7 A[Catch: all -> 0x0353, TryCatch #0 {, blocks: (B:46:0x0277, B:52:0x0283, B:54:0x028f, B:60:0x02ad, B:62:0x02b3, B:65:0x02c1, B:67:0x02c9, B:69:0x02cf, B:71:0x02e7, B:73:0x02f1, B:75:0x02f9, B:76:0x031c), top: B:82:0x0277 }] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r34) {
        /*
        // Method dump skipped, instructions count: 854
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121475iK.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        String str = this.A0X;
        if (str != null) {
            bundle.putString("random_password", str);
        }
    }
}
