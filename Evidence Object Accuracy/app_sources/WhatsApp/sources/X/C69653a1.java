package X;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.status.StatusesFragment;
import java.util.List;

/* renamed from: X.3a1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69653a1 implements AbstractC116185Ul {
    public View A00;
    public View A01;
    public final /* synthetic */ StatusesFragment A02;

    public /* synthetic */ C69653a1(StatusesFragment statusesFragment) {
        this.A02 = statusesFragment;
    }

    public final void A00(boolean z, boolean z2) {
        int measuredHeight = this.A01.getMeasuredHeight();
        if (this.A01.getVisibility() != 0) {
            this.A01.measure(View.MeasureSpec.makeMeasureSpec(this.A00.getMeasuredWidth(), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(0, 0));
            measuredHeight = this.A01.getMeasuredHeight();
        }
        StatusesFragment statusesFragment = this.A02;
        AnimatorSet animatorSet = statusesFragment.A01;
        if (animatorSet == null || (!animatorSet.isRunning() && !statusesFragment.A01.isStarted())) {
            statusesFragment.A01 = new AnimatorSet();
            int i = 8;
            float f = 100.0f;
            float f2 = 0.0f;
            if (z) {
                i = 0;
                f2 = 100.0f;
                f = 0.0f;
            }
            int i2 = 1;
            if (z2) {
                i2 = 500;
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(f2, f);
            ofFloat.setDuration((long) i2);
            ofFloat.setInterpolator(new AccelerateDecelerateInterpolator());
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(measuredHeight) { // from class: X.3Jq
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    C69653a1 r3 = C69653a1.this;
                    int A00 = (int) (((float) this.A00) * (C12960it.A00(valueAnimator) / 100.0f));
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) r3.A01.getLayoutParams();
                    layoutParams.topMargin = -A00;
                    r3.A01.setLayoutParams(layoutParams);
                }
            });
            ofFloat.addListener(new C72823fE(this, i, z));
            statusesFragment.A01.playSequentially(ofFloat);
            statusesFragment.A01.addListener(new IDxLAdapterShape0S0100000_1_I1(this, 8));
            if (z) {
                statusesFragment.A01.setStartDelay(700);
            }
            statusesFragment.A01.start();
        }
    }

    @Override // X.AbstractC116185Ul
    public View AHa(Context context, View view, ViewGroup viewGroup, AnonymousClass1J1 r8, List list, List list2, List list3, List list4, boolean z) {
        if (view != null) {
            this.A00 = view;
            this.A01 = view.findViewById(R.id.status_sharing_row_view);
        } else {
            View A0F = C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.statuses_row_cross_post);
            this.A00 = A0F;
            StatusesFragment statusesFragment = this.A02;
            A0F.setContentDescription(statusesFragment.A0I(R.string.share_to_facebook_story));
            AnonymousClass23N.A02(this.A00, R.string.share_to_facebook_story);
            C12960it.A11(this.A00, this, 22);
            C12960it.A11(this.A00.findViewById(R.id.status_row_share_to_third_party_button), this, 23);
            this.A01 = this.A00.findViewById(R.id.status_sharing_row_view);
            C64153El r1 = statusesFragment.A0h;
            r1.A00 = new AnonymousClass4OL(viewGroup, this);
            AnonymousClass4ON r0 = r1.A04;
            A00(r0.A00, r0.A01);
        }
        return this.A00;
    }
}
