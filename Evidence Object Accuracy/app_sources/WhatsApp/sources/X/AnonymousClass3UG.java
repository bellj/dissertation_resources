package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.3UG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UG implements AbstractC116455Vm {
    public final /* synthetic */ DialogC47362Ai A00;

    public AnonymousClass3UG(DialogC47362Ai r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A03.A04.A0B);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A03.A04.A0B, iArr, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    }
}
