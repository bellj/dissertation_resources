package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5gF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120475gF extends C126705tJ {
    public JSONObject A00;
    public JSONObject A01;
    public JSONObject A02;
    public JSONObject A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C17220qS A06;

    public C120475gF(C14830m7 r2, C14850m9 r3, C17220qS r4, C1308460e r5, C18610sj r6) {
        super(r5.A04, r6);
        this.A04 = r2;
        this.A05 = r3;
        this.A06 = r4;
    }

    public void A00(String str, List list) {
        String str2;
        String str3;
        if (this.A05.A07(635)) {
            switch (str.hashCode()) {
                case -120834421:
                    if (str.equals("SKIPPED_DEVICE_BINDING")) {
                        ArrayList A0l = C12960it.A0l();
                        C117295Zj.A1M("action", "upi-log-event", A0l);
                        C117295Zj.A1M("event-id", "SKIPPED_DEVICE_BINDING", A0l);
                        try {
                            this.A00.put("event-ts", System.currentTimeMillis());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        C117295Zj.A1M("event-info", this.A00.toString(), A0l);
                        C117295Zj.A1M("event-dl-info", this.A00.toString(), A0l);
                        super.A01.A0C(new C1330169d(this), C117295Zj.A0K(A0l));
                        return;
                    }
                    return;
                case 83413:
                    if (str.equals("U66") && this.A01 != null) {
                        try {
                            C17220qS r11 = this.A06;
                            String A01 = r11.A01();
                            long currentTimeMillis = System.currentTimeMillis();
                            JSONObject A0a = C117295Zj.A0a();
                            A0a.put("event-ts", currentTimeMillis);
                            JSONObject A0a2 = C117295Zj.A0a();
                            Iterator it = list.iterator();
                            while (it.hasNext()) {
                                AnonymousClass1W9 r4 = (AnonymousClass1W9) it.next();
                                String str4 = r4.A02;
                                switch (str4.hashCode()) {
                                    case -1413853096:
                                        if (str4.equals("amount")) {
                                            str2 = "txnAmount";
                                            break;
                                        } else {
                                            continue;
                                        }
                                    case -756180983:
                                        if (str4.equals("receiver-vpa")) {
                                            str2 = "payeeAddr";
                                            break;
                                        } else {
                                            continue;
                                        }
                                    case 996422991:
                                        if (str4.equals("sender-vpa")) {
                                            str2 = "payerAddr";
                                            break;
                                        } else {
                                            continue;
                                        }
                                    default:
                                        continue;
                                }
                                if (r4.A03.equals(this.A01.getString(str2))) {
                                    str3 = "Matches";
                                } else {
                                    str3 = "Mismatches";
                                }
                                A0a2.put(str4, C12960it.A0d(" in salt and request params", C12960it.A0j(str3)));
                            }
                            A0a.put("request-params", A0a2);
                            JSONObject A0a3 = C117295Zj.A0a();
                            A0a3.put("event-ts", currentTimeMillis);
                            JSONObject A0a4 = C117295Zj.A0a();
                            Iterator<String> keys = this.A01.keys();
                            while (keys.hasNext()) {
                                String A0x = C12970iu.A0x(keys);
                                A0a4.put(A0x, this.A01.get(A0x).toString());
                            }
                            A0a3.put("salt-params", A0a4);
                            JSONObject A0a5 = C117295Zj.A0a();
                            Iterator it2 = list.iterator();
                            while (it2.hasNext()) {
                                AnonymousClass1W9 r0 = (AnonymousClass1W9) it2.next();
                                A0a5.put(r0.A02, r0.A03);
                            }
                            A0a3.put("request-params", A0a5);
                            r11.A09(new AnonymousClass6DI(this), new C126545t3(new AnonymousClass3CT(A01), A0a.toString(), A0a3.toString()).A00, A01, 204, 0);
                            return;
                        } catch (JSONException unused) {
                            Log.w("PAY: IndiaUpiLogEventAction: exception while creating collecting U66 event info");
                            return;
                        }
                    } else {
                        return;
                    }
                case 1282366491:
                    if (str.equals("SIM_SWAP")) {
                        ArrayList A0l2 = C12960it.A0l();
                        C117295Zj.A1M("action", "upi-log-event", A0l2);
                        C117295Zj.A1M("event-id", "SIM_SWAP", A0l2);
                        long currentTimeMillis2 = System.currentTimeMillis();
                        try {
                            this.A03.put("event-ts", currentTimeMillis2);
                            this.A02.put("event-ts", currentTimeMillis2);
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        C117295Zj.A1M("event-info", this.A03.toString(), A0l2);
                        C117295Zj.A1M("event-dl-info", this.A02.toString(), A0l2);
                        super.A01.A0C(new C1330069c(this), C117295Zj.A0K(A0l2));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
