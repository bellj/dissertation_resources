package X;

import android.widget.CompoundButton;
import androidx.preference.SwitchPreference;

/* renamed from: X.0X1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X1 implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ SwitchPreference A00;

    public AnonymousClass0X1(SwitchPreference switchPreference) {
        this.A00 = switchPreference;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        SwitchPreference switchPreference = this.A00;
        if (!switchPreference.A0Q(Boolean.valueOf(z))) {
            compoundButton.setChecked(!z);
        } else {
            switchPreference.A0T(z);
        }
    }
}
