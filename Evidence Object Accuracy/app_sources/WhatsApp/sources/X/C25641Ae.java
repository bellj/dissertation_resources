package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25641Ae {
    public ScheduledFuture A00;
    public final C17170qN A01;
    public final AnonymousClass15Q A02;
    public final C44501z4 A03;
    public final RunnableC44511z5 A04;
    public final RunnableC44511z5 A05;
    public final ScheduledThreadPoolExecutor A06;

    public C25641Ae(AbstractC15710nm r18, C14830m7 r19, C17170qN r20, C16510p9 r21, C15650ng r22, C21410xN r23, AnonymousClass15Q r24, C21380xK r25, C26731Ep r26, C22830zi r27, C242414t r28) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.setKeepAliveTime(10, TimeUnit.MILLISECONDS);
        scheduledThreadPoolExecutor.allowCoreThreadTimeOut(true);
        this.A02 = r24;
        C44501z4 r11 = new C44501z4(this);
        this.A03 = r11;
        this.A04 = new RunnableC44511z5(r18, r19, r21, r22, r23, r24, r11, r25, r26, r27, r28, 100);
        this.A05 = new RunnableC44511z5(r18, r19, r21, r22, r23, r24, null, r25, r26, r27, r28, 0);
        this.A01 = r20;
        this.A06 = scheduledThreadPoolExecutor;
    }

    public void A00() {
        this.A06.execute(new RunnableBRunnable0Shape6S0100000_I0_6(this, 5));
    }

    public final synchronized void A01(RunnableC44511z5 r6, long j, boolean z) {
        long j2 = 0;
        if (j != 0) {
            j2 = Math.max((long) C26061Bw.A0L, j);
        }
        ScheduledFuture scheduledFuture = this.A00;
        if (scheduledFuture != null && !scheduledFuture.isDone() && this.A00.getDelay(TimeUnit.MILLISECONDS) > j2 && !this.A00.cancel(false)) {
            Log.e("EphemeralUpdateManager/scheduleRunnable/unable to cancel future");
        }
        if (j2 < 86400000) {
            ScheduledFuture<?> schedule = this.A06.schedule(r6, j2, TimeUnit.MILLISECONDS);
            this.A00 = schedule;
            if (z) {
                try {
                    schedule.get();
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("EphemeralUpdateManager/scheduleNextRun", e);
                }
            }
        }
    }

    public void A02(AbstractC14640lm r7, String str) {
        AnonymousClass15Q r5 = this.A02;
        if (r5.A00 == -1) {
            r5.A00 = r5.A01.A00();
        }
        Map map = r5.A03;
        Set set = (Set) map.get(r7);
        if (set == null) {
            set = new HashSet();
        }
        set.add(str);
        map.put(r7, set);
    }

    public void A03(AbstractC14640lm r5, String str) {
        AnonymousClass15Q r3 = this.A02;
        Map map = r3.A03;
        Set set = (Set) map.get(r5);
        if (set != null) {
            set.remove(str);
            if (set.isEmpty()) {
                map.remove(r5);
            }
        } else {
            StringBuilder sb = new StringBuilder("EphemeralSessionManager/null session: ");
            sb.append(r5);
            Log.e(sb.toString());
        }
        if (map.isEmpty()) {
            r3.A00 = -1;
        }
        if (r5 != null) {
            AnonymousClass1PE A06 = r3.A02.A04.A06(r5);
            if (A06 == null) {
                StringBuilder sb2 = new StringBuilder("msgstore/last/message/no chat for ");
                sb2.append(r5);
                Log.w(sb2.toString());
                return;
            }
            AbstractC15340mz r1 = A06.A0a;
            if (r1 == null) {
                return;
            }
            if (r1.A04 <= 0 && !C30041Vv.A0I(r1.A0y)) {
                return;
            }
        }
        A00();
    }
}
