package X;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5c6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118615c6 extends AnonymousClass02M {
    public int A00 = 0;
    public C30921Zi A01;
    public final C22540zF A02;
    public final C22460z7 A03;
    public final AnonymousClass6M4 A04;
    public final List A05 = C12960it.A0l();

    public C118615c6(C22540zF r2, C22460z7 r3, AnonymousClass6M4 r4) {
        this.A03 = r3;
        this.A02 = r2;
        this.A04 = r4;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A05.size();
    }

    public void A0E(List list) {
        list.size();
        List list2 = this.A05;
        AnonymousClass0SZ A00 = AnonymousClass0RD.A00(new C118505bv(list2, list));
        list2.clear();
        list2.addAll(list);
        A00.A02(this);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r4, int i) {
        C118855cU r42 = (C118855cU) r4;
        C129025x3 r2 = (C129025x3) this.A05.get(i);
        r42.A08(r2);
        r42.A0H.setOnClickListener(new View.OnClickListener(r2, r42, this, i) { // from class: X.64b
            public final /* synthetic */ int A00;
            public final /* synthetic */ C129025x3 A01;
            public final /* synthetic */ C118855cU A02;
            public final /* synthetic */ C118615c6 A03;

            {
                this.A03 = r3;
                this.A00 = r4;
                this.A02 = r2;
                this.A01 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C118615c6 r43 = this.A03;
                int i2 = this.A00;
                C118855cU r7 = this.A02;
                C129025x3 r6 = this.A01;
                List list = r43.A05;
                if (((C129025x3) list.get(i2)).A01) {
                    C129025x3 r22 = (C129025x3) list.get(i2);
                    C30921Zi r5 = r22.A03;
                    if (r5 != null) {
                        r7.A02.setVisibility(8);
                        r7.A03.setVisibility(0);
                        C22460z7 r44 = r7.A05;
                        AnonymousClass6B2 r3 = new AnonymousClass6B2(r22, r7);
                        if (TextUtils.isEmpty(r5.A05)) {
                            C12990iw.A1N(new C624137d(r5, r3, r44), r44.A0D);
                            return;
                        }
                        r44.A0D.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r44, r5, r3, 21));
                        return;
                    }
                    throw C12960it.A0U("Default theme should not have download failed state");
                } else if (r43.A00 != i2) {
                    ArrayList A0x = C12980iv.A0x(list);
                    C129025x3 r1 = new C129025x3(((C129025x3) list.get(r43.A00)).A03);
                    r1.A02 = false;
                    r1.A00 = ((C129025x3) list.get(r43.A00)).A00;
                    r1.A01 = ((C129025x3) list.get(r43.A00)).A01;
                    A0x.set(r43.A00, r1);
                    C30921Zi r23 = r6.A03;
                    C129025x3 r12 = new C129025x3(r23);
                    r12.A02 = true;
                    r12.A00 = r6.A00;
                    r12.A01 = r6.A01;
                    A0x.set(i2, r12);
                    r43.A01 = r23;
                    r43.A00 = i2;
                    r43.A04.AXS(r23);
                    r43.A0E(A0x);
                }
            }
        });
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C118855cU(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.expressive_theme_item_view), this.A02, this.A03);
    }
}
