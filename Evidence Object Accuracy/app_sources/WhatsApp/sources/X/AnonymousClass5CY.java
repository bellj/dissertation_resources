package X;

import java.util.Comparator;

/* renamed from: X.5CY  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass5CY implements Comparator {
    public final /* synthetic */ AnonymousClass5SM A00;

    public /* synthetic */ AnonymousClass5CY(AnonymousClass5SM r1) {
        this.A00 = r1;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        AnonymousClass5SM r0 = this.A00;
        return r0.AGR(obj2) - r0.AGR(obj);
    }
}
