package X;

import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

/* renamed from: X.1xW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43741xW implements AbstractC21730xt {
    public final C26631Ef A00;
    public final C14830m7 A01;
    public final C17220qS A02;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public C43741xW(C26631Ef r1, C14830m7 r2, C17220qS r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r8, String str) {
        long j;
        TimeUnit timeUnit;
        long j2;
        int A00 = C41151sz.A00(r8);
        C26631Ef r5 = this.A00;
        if (A00 == 404) {
            r5.A01(-1);
            return;
        }
        int i = r5.A00().getInt("biz_block_reasons_api_back_off_days", 0);
        if (i == 0) {
            r5.A01(1);
            j = this.A01.A00();
            timeUnit = TimeUnit.DAYS;
            j2 = 1;
        } else if (i > 0 && i < 16) {
            r5.A01(i << 1);
            j = r5.A00().getLong("biz_block_reasons_api_cooling_timestamp", 0);
            timeUnit = TimeUnit.DAYS;
            j2 = (long) i;
        } else {
            return;
        }
        r5.A00().edit().putLong("biz_block_reasons_api_cooling_timestamp", j + timeUnit.toMillis(j2)).apply();
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r12, String str) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        AnonymousClass1V8 A0E = r12.A0E("mobile_config");
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("list");
            if (!(A0E2 == null || A0E2.A03 == null || !"biz_block_reasons".equals(A0E2.A0I("name", null)))) {
                int A00 = C28421Nd.A00(A0E2.A0I("v", null), 0);
                String A0I = A0E2.A0I("language", null);
                if (A00 > 0 && A0I != null) {
                    for (AnonymousClass1V8 r2 : A0E2.A0J("item")) {
                        linkedHashMap.put(r2.A0I("id", null), r2.A0G());
                    }
                    String A0I2 = A0E2.A0I("country", null);
                    if (!linkedHashMap.isEmpty()) {
                        C26631Ef r6 = this.A00;
                        r6.A00().edit().putInt("biz_block_reasons_version", A00).apply();
                        r6.A00().edit().putString("biz_block_reasons_language", A0I).apply();
                        r6.A00().edit().putString("biz_block_reasons_country", A0I2).apply();
                        r6.A00().edit().putString("biz_block_reasons", new JSONObject(linkedHashMap).toString()).apply();
                    }
                }
            }
            C26631Ef r0 = this.A00;
            r0.A01(0);
            r0.A00().edit().putLong("biz_block_reasons_api_cooling_timestamp", 0).apply();
        }
    }
}
