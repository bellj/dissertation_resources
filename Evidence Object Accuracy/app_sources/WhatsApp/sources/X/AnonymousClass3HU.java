package X;

import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.3HU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HU {
    public C37471mS[] A00;
    public C37471mS[] A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final AnonymousClass210 A06;

    public /* synthetic */ AnonymousClass3HU() {
        this.A04 = 0;
        this.A01 = null;
        this.A02 = R.id.emoji_recent_btn;
        this.A03 = R.id.emoji_recent_marker;
        this.A05 = R.string.emoji_recents_title;
        this.A06 = null;
    }

    public /* synthetic */ AnonymousClass3HU(AnonymousClass210 r8, int i) {
        C37471mS[] r3;
        int length;
        this.A04 = i;
        EnumC452920z r6 = (EnumC452920z) r8;
        int[][] iArr = r6.emojiData;
        if (iArr == null || (length = iArr.length) == 0) {
            r3 = null;
        } else {
            r3 = new C37471mS[length];
            int i2 = 0;
            do {
                r3[i2] = new C37471mS(iArr[i2]);
                i2++;
            } while (i2 < length);
        }
        this.A01 = r3;
        this.A02 = r6.buttonId;
        this.A03 = r6.markerId;
        this.A05 = r6.titleResId;
        this.A06 = r8;
    }

    public int A00() {
        int length;
        if (!(this instanceof C58292oa)) {
            C37471mS[] r0 = this.A01;
            if (r0 == null) {
                return 0;
            }
            return r0.length;
        }
        C58292oa r3 = (C58292oa) this;
        C37471mS[] r02 = ((AnonymousClass3HU) r3).A01;
        if (r02 == null) {
            length = 0;
        } else {
            length = r02.length;
        }
        int A0A = r3.A01.A0A(((AnonymousClass3HU) r3).A00);
        if (A0A != length) {
            r3.A00 = true;
        }
        return A0A;
    }

    public void A01(C37471mS[] r8) {
        C37471mS[] r3;
        int length;
        if (!Arrays.equals(this.A00, r8)) {
            this.A00 = r8;
            AnonymousClass210 r0 = this.A06;
            if (r0 != null) {
                int[][] iArr = ((EnumC452920z) r0).emojiData;
                if (iArr == null || (length = iArr.length) == 0) {
                    r3 = null;
                } else {
                    r3 = new C37471mS[length];
                    int i = 0;
                    do {
                        r3[i] = new C37471mS(iArr[i]);
                        i++;
                    } while (i < length);
                }
                this.A01 = r3;
                if (r8 != null) {
                    int length2 = iArr.length;
                    ArrayList A0w = C12980iv.A0w(length2);
                    for (int[] iArr2 : iArr) {
                        C37471mS r1 = new C37471mS(iArr2);
                        if (!C37871n9.A01(r1, r8)) {
                            A0w.add(r1);
                        }
                    }
                    if (A0w.size() != length2) {
                        this.A01 = (C37471mS[]) A0w.toArray(new C37471mS[0]);
                    }
                }
            }
        }
    }

    public int[] A02(C16630pM r10, int i) {
        if (!(this instanceof C58292oa)) {
            C37471mS[] r0 = this.A01;
            if (r0 != null) {
                boolean A02 = AnonymousClass3JU.A02(r0[i].A00);
                C37471mS r02 = this.A01[i];
                if (A02) {
                    return AnonymousClass3JF.A03(r10, r02.A00);
                }
                int[] iArr = r02.A00;
                if (AnonymousClass3JU.A03(iArr)) {
                    return AnonymousClass3JU.A08(iArr, C12970iu.A01(r10.A01("emoji_modifiers"), AnonymousClass3JF.A00(iArr)));
                }
                return iArr;
            }
        } else {
            C58292oa r8 = (C58292oa) this;
            if (r8.A00) {
                C231510o r7 = r8.A01;
                C37471mS[] r6 = ((AnonymousClass3HU) r8).A00;
                int A0A = r7.A0A(r6);
                C37471mS[] r4 = new C37471mS[A0A];
                int i2 = 0;
                int i3 = 0;
                while (i2 < A0A) {
                    C37471mS r1 = new C37471mS((int[]) r7.A01(i3));
                    if (r6 == null || !C37871n9.A01(r1, r6)) {
                        r4[i2] = r1;
                        i2++;
                    }
                    i3++;
                }
                ((AnonymousClass3HU) r8).A01 = r4;
                r8.A00 = false;
            }
            C37471mS[] r03 = ((AnonymousClass3HU) r8).A01;
            if (r03 != null) {
                return r03[i].A00;
            }
        }
        return new int[0];
    }
}
