package X;

import android.content.Context;
import java.util.concurrent.Callable;

/* renamed from: X.0el  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10520el implements Callable {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public CallableC10520el(Context context, String str, String str2) {
        this.A00 = context;
        this.A02 = str;
        this.A01 = str2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return C06550Ub.A01(this.A00, this.A02, this.A01);
    }
}
