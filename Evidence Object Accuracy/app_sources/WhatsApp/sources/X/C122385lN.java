package X;

import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.5lN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122385lN extends AbstractC118825cR {
    public final LinearLayout A00;
    public final AnonymousClass018 A01;

    public C122385lN(View view, AnonymousClass018 r3) {
        super(view);
        this.A00 = C117305Zk.A07(view, R.id.payment_methods_list_container);
        this.A01 = r3;
    }
}
