package X;

import android.location.Location;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.32x  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32x extends AbstractC36001jA {
    public final AbstractC116405Vh A00 = new C1090450e(this);
    public final /* synthetic */ LocationPicker2 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass32x(AnonymousClass12P r34, AbstractC15710nm r35, C244615p r36, C14900mE r37, C15570nT r38, C18790t3 r39, C16170oZ r40, AnonymousClass130 r41, C22700zV r42, AnonymousClass131 r43, AnonymousClass01d r44, C14830m7 r45, C16590pI r46, C15890o4 r47, C14820m6 r48, AnonymousClass018 r49, C15650ng r50, AnonymousClass19M r51, C231510o r52, AnonymousClass193 r53, C14850m9 r54, C253719d r55, C18810t5 r56, LocationPicker2 locationPicker2, C16030oK r58, C244415n r59, AnonymousClass3DJ r60, WhatsAppLibLoader whatsAppLibLoader, C16630pM r62, C252018m r63, C252718t r64, AbstractC14440lR r65) {
        super(r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r58, r59, r60, whatsAppLibLoader, r62, r63, r64, r65);
        this.A01 = locationPicker2;
    }

    @Override // X.AbstractC36001jA, android.location.LocationListener
    public void onLocationChanged(Location location) {
        C35961j4 r1;
        if (location != null) {
            LocationPicker2 locationPicker2 = this.A01;
            if (locationPicker2.A0S.A06 == null && (r1 = locationPicker2.A02) != null) {
                r1.A0A(C65193Io.A01(AnonymousClass1U5.A01(location)));
            }
            if (locationPicker2.A0S.A0s && locationPicker2.A02 != null) {
                if (locationPicker2.A06 == null) {
                    A04();
                }
                LocationPicker2.A03(AnonymousClass1U5.A01(location), locationPicker2);
            }
            if (locationPicker2.A0S.A0r && locationPicker2.A02 != null) {
                locationPicker2.A02.A09(C65193Io.A01(AnonymousClass1U5.A01(location)));
            }
            locationPicker2.A0R.A06 = location;
            super.onLocationChanged(location);
        }
    }
}
