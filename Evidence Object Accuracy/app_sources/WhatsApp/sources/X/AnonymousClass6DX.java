package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrCodeScanActivity;

/* renamed from: X.6DX  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DX implements AnonymousClass27Z {
    public final /* synthetic */ IndiaUpiQrCodeScanActivity A00;

    public AnonymousClass6DX(IndiaUpiQrCodeScanActivity indiaUpiQrCodeScanActivity) {
        this.A00 = indiaUpiQrCodeScanActivity;
    }

    @Override // X.AnonymousClass27Z
    public void ANb(int i) {
        C14900mE r1;
        int i2;
        IndiaUpiQrCodeScanActivity indiaUpiQrCodeScanActivity = this.A00;
        if (((AbstractActivityC41101su) indiaUpiQrCodeScanActivity).A04.A03()) {
            r1 = ((ActivityC13810kN) indiaUpiQrCodeScanActivity).A05;
            i2 = R.string.error_camera_disabled_during_video_call;
        } else {
            if (i != 2) {
                r1 = ((ActivityC13810kN) indiaUpiQrCodeScanActivity).A05;
                i2 = R.string.cannot_start_camera;
            }
            indiaUpiQrCodeScanActivity.finish();
        }
        r1.A07(i2, 1);
        indiaUpiQrCodeScanActivity.finish();
    }

    @Override // X.AnonymousClass27Z
    public void AUF() {
        IndiaUpiQrCodeScanActivity indiaUpiQrCodeScanActivity = this.A00;
        indiaUpiQrCodeScanActivity.A01.A06("indiaupiqractivity/previewready");
        ((AbstractActivityC41101su) indiaUpiQrCodeScanActivity).A07 = true;
    }

    @Override // X.AnonymousClass27Z
    public void AUS(C49262Kb r2) {
        this.A00.A2g(r2);
    }
}
