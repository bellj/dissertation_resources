package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PK extends AnonymousClass2PA {
    public final AnonymousClass2PL A00;

    public AnonymousClass2PK(Jid jid, AnonymousClass2PL r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
