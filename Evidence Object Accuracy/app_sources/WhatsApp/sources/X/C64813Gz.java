package X;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.concurrent.locks.Lock;

/* renamed from: X.3Gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64813Gz {
    public static C64813Gz A03;
    public GoogleSignInAccount A00;
    public GoogleSignInOptions A01;
    public final C65263Iv A02;

    public C64813Gz(Context context) {
        C65263Iv A00 = C65263Iv.A00(context);
        this.A02 = A00;
        this.A00 = A00.A02();
        this.A01 = A00.A03();
    }

    public static synchronized C64813Gz A00(Context context) {
        C64813Gz r0;
        synchronized (C64813Gz.class) {
            Context applicationContext = context.getApplicationContext();
            r0 = A03;
            if (r0 == null) {
                r0 = new C64813Gz(applicationContext);
                A03 = r0;
            }
        }
        return r0;
    }

    public final synchronized void A01() {
        C65263Iv r0 = this.A02;
        Lock lock = r0.A01;
        lock.lock();
        r0.A00.edit().clear().apply();
        lock.unlock();
        this.A00 = null;
        this.A01 = null;
    }
}
