package X;

import com.whatsapp.Conversation;

/* renamed from: X.1Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26671Ej extends AbstractC16220oe {
    public void A05(String str) {
        AnonymousClass240 r1;
        AnonymousClass241 r0;
        for (C459823z r02 : A01()) {
            Conversation conversation = r02.A00;
            if (conversation.A2l.A01()) {
                conversation.A2l.A00(false);
            }
            C15260mp r2 = conversation.A2k;
            if (!(r2 == null || (r1 = r2.A07) == null)) {
                if (!r2.isShowing()) {
                    r2.A06();
                }
                r2.A0I(3);
                if (!(str == null || (r0 = r1.A03) == null)) {
                    r0.A04(str);
                }
            }
        }
    }

    public void A06(String str) {
        for (C459823z r0 : A01()) {
            Conversation conversation = r0.A00;
            if (conversation.A2l.A01()) {
                conversation.A2l.A00(false);
            }
            C15260mp r02 = conversation.A2k;
            if (r02 != null) {
                r02.A0L(str);
            }
        }
    }
}
