package X;

/* renamed from: X.5zK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130385zK {
    public static final String[] A00 = {"/system", "/system/bin", "/system/sbin", "/system/xbin", "/vendor/bin", "/sbin", "/etc"};

    /*  JADX ERROR: JadxRuntimeException in pass: AttachTryCatchVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Null type added to not empty exception handler: IOException | NoSuchElementException -> 0x0031
        	at jadx.core.dex.trycatch.ExceptionHandler.addCatchType(ExceptionHandler.java:54)
        	at jadx.core.dex.visitors.AttachTryCatchVisitor.createHandler(AttachTryCatchVisitor.java:130)
        	at jadx.core.dex.visitors.AttachTryCatchVisitor.attachHandlers(AttachTryCatchVisitor.java:118)
        	at jadx.core.dex.visitors.AttachTryCatchVisitor.initTryCatches(AttachTryCatchVisitor.java:54)
        	at jadx.core.dex.visitors.AttachTryCatchVisitor.visit(AttachTryCatchVisitor.java:42)
        */
    public static boolean A00() {
        /*
            r12 = 0
            java.lang.Runtime.getRuntime()     // Catch: all -> 0x0039
            r1 = move-result     // Catch: all -> 0x0039
            java.lang.String r0 = "mount"     // Catch: all -> 0x0039
            r1.exec(r0)     // Catch: all -> 0x0039
            r3 = move-result     // Catch: all -> 0x0039
            if (r3 == 0) goto L_0x0039     // Catch: all -> 0x0039
            r3.getInputStream()
            r2 = move-result
            if (r2 == 0) goto L_0x0031
            java.util.Scanner r1 = new java.util.Scanner
            r1.<init>(r2)
            java.lang.String r0 = "\\A"
            r1.useDelimiter(r0)
            r0 = move-result
            r0.next()
            r1 = move-result
            java.lang.String r0 = "\n"
            r1.split(r0)
            r0 = move-result
            r2.close()
            goto L_0x0035
            r0 = move-exception
            r2.close()
            throw r0
        L_0x0031:
            r3.destroy()
            goto L_0x0039
            r3.destroy()
            r12 = r0
        L_0x0039:
            r0 = 0
            if (r12 != 0) goto L_0x003d
            return r0
            int r11 = android.os.Build.VERSION.SDK_INT
            int r10 = r12.length
            r9 = 0
            r14 = 0
            if (r9 >= r10) goto L_0x00a5
            r1 = r12[r9]
            java.lang.String r0 = " "
            r1.split(r0)
            r3 = move-result
            r1 = 23
            if (r11 > r1) goto L_0x0054
            int r2 = r3.length
            r0 = 4
            if (r2 < r0) goto L_0x005a
            if (r11 <= r1) goto L_0x005d
            int r2 = r3.length
            r0 = 6
            if (r2 >= r0) goto L_0x005d
            int r9 = r9 + 1
            goto L_0x0042
            r0 = 1
            if (r11 <= r1) goto L_0x00a1
            r0 = 2
            r8 = r3[r0]
            r0 = 5
            r13 = r3[r0]
            java.lang.String[] r7 = X.C130385zK.A00
            int r6 = r7.length
            r5 = 0
            if (r5 >= r6) goto L_0x005a
            r0 = r7[r5]
            r8.equalsIgnoreCase(r0)
            r0 = move-result
            if (r0 == 0) goto L_0x0099
            if (r11 <= r1) goto L_0x0084
            java.lang.String r0 = "("
            java.lang.String r2 = ""
            r13.replace(r0, r2)
            r1 = move-result
            java.lang.String r0 = ")"
            r1.replace(r0, r2)
            r13 = move-result
            java.lang.String r0 = ","
            r13.split(r0)
            r4 = move-result
            int r3 = r4.length
            r2 = 0
            if (r2 >= r3) goto L_0x0099
            r1 = r4[r2]
            java.lang.String r0 = "rw"
            r1.equalsIgnoreCase(r0)
            r0 = move-result
            if (r0 == 0) goto L_0x009e
            r14 = 1
            int r5 = r5 + 1
            r1 = 23
            goto L_0x006a
            int r2 = r2 + 1
            goto L_0x008c
            r8 = r3[r0]
            r0 = 3
            goto L_0x0064
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130385zK.A00():boolean");
    }
}
