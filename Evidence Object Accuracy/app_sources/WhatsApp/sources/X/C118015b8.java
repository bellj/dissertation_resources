package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.payments.service.NoviVideoSelfieFgService;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.5b8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118015b8 extends AnonymousClass015 {
    public int A00;
    public C1316663q A01;
    public C27691It A02 = C13000ix.A03();
    public String A03;
    public String A04;
    public ArrayList A05;
    public final Bundle A06;
    public final C129675y7 A07;
    public final C128615wO A08;
    public final C22900zp A09;

    public C118015b8(Bundle bundle, C129675y7 r3, C128615wO r4, C22900zp r5) {
        this.A07 = r3;
        this.A09 = r5;
        this.A08 = r4;
        this.A06 = bundle;
    }

    public void A04(Context context, C126035sE r9) {
        C128155ve r2;
        C27691It r22;
        int i;
        int i2 = r9.A00;
        if (i2 == 0) {
            Bundle bundle = this.A06;
            String string = bundle.getString("video_selfie_challenge_id");
            AnonymousClass009.A05(string);
            this.A03 = string;
            String string2 = bundle.getString("disable_face_rec");
            AnonymousClass009.A05(string2);
            this.A04 = string2;
            C1316663q r0 = (C1316663q) bundle.getParcelable("step_up");
            this.A01 = r0;
            AnonymousClass009.A05(r0);
            this.A00 = bundle.getInt("step_up_origin_action", 1);
            ArrayList<String> stringArrayList = bundle.getStringArrayList("video_selfie_head_directions");
            AnonymousClass009.A05(stringArrayList);
            this.A05 = stringArrayList;
            r2 = new C128155ve(0);
            File A00 = this.A08.A00("selfie.mp4");
            AnonymousClass009.A05(A00);
            r2.A06 = A00.getAbsolutePath();
        } else if (i2 == 1) {
            r2 = new C128155ve(1);
        } else if (i2 != 2) {
            if (i2 == 3) {
                C128615wO r23 = this.A08;
                File A002 = r23.A00("selfie.mp4");
                File A003 = r23.A00("selfie.jpeg");
                if (A002 != null) {
                    A002.delete();
                }
                if (A003 != null) {
                    A003.delete();
                }
                r22 = this.A02;
                i = 7;
            } else if (i2 == 4) {
                r22 = this.A02;
                i = 9;
            } else {
                return;
            }
            r22.A0B(new C128155ve(i));
            return;
        } else {
            C22900zp r6 = this.A09;
            C1316663q r5 = this.A01;
            int i3 = this.A00;
            String str = this.A03;
            String str2 = this.A04;
            Log.i("PAY: NoviVideoSelfieFgService/start-service");
            Intent A0A = C12970iu.A0A();
            A0A.putExtra("extra_step_up", r5);
            A0A.putExtra("extra_step_up_origin_action", i3);
            A0A.putExtra("extra_step_up_challenge_id", str);
            A0A.putExtra("extra_disable_face_rec", str2);
            r6.A03(context, A0A, NoviVideoSelfieFgService.class);
            return;
        }
        r2.A07 = this.A05;
        this.A02.A0B(r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0055, code lost:
        if (r0 == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.content.Intent r12) {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C118015b8.A05(android.content.Intent):void");
    }
}
