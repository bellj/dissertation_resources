package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.2RL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2RL extends Drawable.ConstantState {
    public final /* synthetic */ C50702Qp A00;

    @Override // android.graphics.drawable.Drawable.ConstantState
    public int getChangingConfigurations() {
        return 0;
    }

    public /* synthetic */ AnonymousClass2RL(C50702Qp r1) {
        this.A00 = r1;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        return this.A00;
    }
}
