package X;

/* renamed from: X.1fD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33871fD extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Long A03;

    public C33871fD() {
        super(894, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamE2eRetryAfterDelivery {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceType", obj);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "msgRetryCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryRevoke", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
