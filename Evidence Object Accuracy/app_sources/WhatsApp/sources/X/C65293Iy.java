package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Iy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65293Iy {
    public static int A00(AnonymousClass1YT r4) {
        int i = r4.A00;
        if (r4.A0F == null && r4.A0B.A03) {
            return R.color.voipOutgoingCallIconTint;
        }
        if (i == 5) {
            return R.color.voipIncomingCallIconTint;
        }
        return R.color.voipMissedCallIconTint;
    }

    public static AnimationSet A01(View view) {
        Animation loadAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.up);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(view.getContext(), R.anim.down);
        Animation loadAnimation3 = AnimationUtils.loadAnimation(view.getContext(), R.anim.shake);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(loadAnimation);
        animationSet.addAnimation(loadAnimation3);
        animationSet.addAnimation(loadAnimation2);
        animationSet.setRepeatMode(1);
        animationSet.setRepeatCount(-1);
        animationSet.setStartOffset(750);
        animationSet.setAnimationListener(new C83353x6(view, animationSet));
        return animationSet;
    }

    public static AnonymousClass2OC A02(C15550nR r7, C15610nY r8, List list, boolean z) {
        Object[] objArr;
        int i;
        String A01;
        ArrayList A0l = C12960it.A0l();
        int i2 = 0;
        while (i2 < list.size()) {
            C15370n3 A0B = r7.A0B((AbstractC14640lm) list.get(i2));
            if (z) {
                A01 = r8.A04(A0B);
            } else {
                A01 = C15610nY.A01(r8, A0B);
            }
            A0l.add(A01);
            i2++;
            if (i2 >= 3) {
                break;
            }
        }
        if (list.size() > 3) {
            int A09 = C12990iw.A09(list, 1);
            Object[] objArr2 = new Object[2];
            C12990iw.A1U(A0l, objArr2, 0);
            C12960it.A1P(objArr2, C12990iw.A09(list, 1), 1);
            return new AnonymousClass2OB(objArr2, R.plurals.group_voip_call_participants_label, A09);
        }
        if (list.size() == 2) {
            objArr = new Object[2];
            C12990iw.A1U(A0l, objArr, 0);
            C12990iw.A1U(A0l, objArr, 1);
            i = R.string.voip_call_log_two_participants;
        } else if (list.size() == 3) {
            objArr = new Object[3];
            C12990iw.A1U(A0l, objArr, 0);
            C12990iw.A1U(A0l, objArr, 1);
            C12990iw.A1U(A0l, objArr, 2);
            i = R.string.voip_call_log_three_participants;
        } else if (list.size() == 1) {
            return new AnonymousClass2OE((String) A0l.get(0));
        } else {
            if (list.size() == 0) {
                objArr = new Object[0];
                i = R.string.voip_joinable_waiting_for_others;
            } else {
                AnonymousClass009.A0A("Number of names not supported", false);
                return null;
            }
        }
        return new AnonymousClass2OD(objArr, i);
    }

    public static void A03(View view, String str, String str2) {
        AnonymousClass028.A0g(view, new C53572ef(view, str, str2, false));
    }

    public static void A04(View view, boolean z) {
        view.setEnabled(z);
        float f = 0.4f;
        if (z) {
            f = 1.0f;
        }
        view.setAlpha(f);
    }
}
