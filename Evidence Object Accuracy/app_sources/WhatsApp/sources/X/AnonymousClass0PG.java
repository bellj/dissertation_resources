package X;

import java.util.Arrays;

/* renamed from: X.0PG  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PG {
    public AnonymousClass0NV A00;
    public final AnonymousClass0NV A01;
    public final String A02;

    public /* synthetic */ AnonymousClass0PG(String str) {
        AnonymousClass0NV r0 = new AnonymousClass0NV();
        this.A01 = r0;
        this.A00 = r0;
        this.A02 = str;
    }

    public final void A00(Object obj, String str) {
        AnonymousClass0NV r1 = new AnonymousClass0NV();
        this.A00.A00 = r1;
        this.A00 = r1;
        r1.A01 = obj;
        r1.A02 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.A02);
        sb.append('{');
        String str = "";
        for (AnonymousClass0NV r4 = this.A01.A00; r4 != null; r4 = r4.A00) {
            Object obj = r4.A01;
            sb.append(str);
            String str2 = r4.A02;
            if (str2 != null) {
                sb.append(str2);
                sb.append('=');
            }
            if (obj == null || !obj.getClass().isArray()) {
                sb.append(obj);
            } else {
                String deepToString = Arrays.deepToString(new Object[]{obj});
                sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
            }
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }
}
