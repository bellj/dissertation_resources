package X;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;

/* renamed from: X.0S1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0S1 {
    public Paint A00;
    public Paint A01;
    public AnonymousClass0SG A02;
    public AnonymousClass0SG A03;
    public C08900c2 A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final /* synthetic */ C06540Ua A08;

    public AnonymousClass0S1(AnonymousClass0S1 r4, C06540Ua r5) {
        this.A08 = r5;
        this.A05 = r4.A05;
        this.A06 = r4.A06;
        this.A00 = new Paint(r4.A00);
        this.A01 = new Paint(r4.A01);
        AnonymousClass0SG r1 = r4.A03;
        if (r1 != null) {
            this.A03 = new AnonymousClass0SG(r1);
        }
        AnonymousClass0SG r12 = r4.A02;
        if (r12 != null) {
            this.A02 = new AnonymousClass0SG(r12);
        }
        this.A07 = r4.A07;
        try {
            this.A04 = (C08900c2) r4.A04.clone();
        } catch (CloneNotSupportedException e) {
            Log.e("SVGAndroidRenderer", "Unexpected clone error", e);
            this.A04 = C08900c2.A00();
        }
    }

    public AnonymousClass0S1(C06540Ua r5) {
        this.A08 = r5;
        Paint paint = new Paint();
        this.A00 = paint;
        paint.setFlags(385);
        this.A00.setStyle(Paint.Style.FILL);
        Paint paint2 = this.A00;
        Typeface typeface = Typeface.DEFAULT;
        paint2.setTypeface(typeface);
        Paint paint3 = new Paint();
        this.A01 = paint3;
        paint3.setFlags(385);
        this.A01.setStyle(Paint.Style.STROKE);
        this.A01.setTypeface(typeface);
        this.A04 = C08900c2.A00();
    }
}
