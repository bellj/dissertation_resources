package X;

import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.3NP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NP implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ MessageReplyActivity A00;

    public AnonymousClass3NP(MessageReplyActivity messageReplyActivity) {
        this.A00 = messageReplyActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        MessageReplyActivity messageReplyActivity = this.A00;
        int i = 0;
        boolean A1V = C12960it.A1V(C12980iv.A0H(messageReplyActivity).orientation, 2);
        FrameLayout frameLayout = messageReplyActivity.A06;
        if (A1V) {
            i = 8;
        }
        frameLayout.setVisibility(i);
        if (!A1V && messageReplyActivity.A2m()) {
            C12980iv.A1E(messageReplyActivity.A05, this);
        }
    }
}
