package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.conversation.ConversationListView;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.util.Map;

/* renamed from: X.2An  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC47392An {
    public final int A00;
    public final Conversation A01;

    public AbstractC47392An(Conversation conversation, int i) {
        this.A01 = conversation;
        this.A00 = i;
    }

    public final void A00() {
        AbstractC47392An r0;
        C63553Ca r4 = this.A01.A1f;
        Class<?> cls = getClass();
        Map map = r4.A00;
        AbstractC47392An r2 = (AbstractC47392An) map.get(cls);
        if (r2 != null && r4.A01 != cls) {
            if (!(r4.A01 == null || (r0 = (AbstractC47392An) map.get(r4.A01)) == null || r0.A00 >= r2.A00)) {
                Class cls2 = r4.A01;
                AnonymousClass009.A05(cls2);
                AbstractC47392An r1 = (AbstractC47392An) map.get(cls2);
                if (cls2.equals(r4.A01) && r1 != null) {
                    r1.A01(new AnonymousClass4KL(r4), false);
                }
            }
            if (r4.A01 == null && r2.A03()) {
                if (!(r2 instanceof AnonymousClass2xW)) {
                    AnonymousClass2xV r22 = (AnonymousClass2xV) r2;
                    if (r22.A00 == null) {
                        r22.A00 = ((AbstractC47392An) r22).A01.getLayoutInflater().inflate(R.layout.change_number_notification, r22.A04).findViewById(R.id.change_number_notification);
                        r22.A05();
                        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                        translateAnimation.setAnimationListener(new C83213ws(r22));
                        translateAnimation.setDuration(400);
                        View view = r22.A00;
                        AnonymousClass009.A03(view);
                        view.startAnimation(translateAnimation);
                    }
                } else {
                    AnonymousClass2xW r23 = (AnonymousClass2xW) r2;
                    ViewGroup viewGroup = r23.A02;
                    if (viewGroup.getVisibility() != 0) {
                        boolean z = false;
                        if (viewGroup.getChildCount() > 0) {
                            z = true;
                        }
                        if (viewGroup.findViewById(R.id.group_description_text) == null) {
                            viewGroup.removeAllViews();
                            ((AbstractC47392An) r23).A01.getLayoutInflater().inflate(R.layout.conversation_group_description, viewGroup, true);
                            viewGroup.findViewById(R.id.group_description_close).setOnClickListener(new ViewOnClickCListenerShape18S0100000_I1_1(r23, 0));
                        }
                        r23.A05();
                        AbstractC005102i A1U = ((AbstractC47392An) r23).A01.A1U();
                        AnonymousClass009.A05(A1U);
                        A1U.A07(0.0f);
                        AnonymousClass028.A0V(viewGroup, r23.A01);
                        viewGroup.setVisibility(0);
                        if (z) {
                            viewGroup.postDelayed(new RunnableBRunnable0Shape15S0100000_I1_1(viewGroup, 23), 10);
                        }
                    }
                }
                r4.A01 = cls;
            }
        }
    }

    public void A01(AnonymousClass4KL r13, boolean z) {
        boolean A0A;
        if (!(this instanceof AnonymousClass2xW)) {
            AnonymousClass2xV r2 = (AnonymousClass2xV) this;
            if (z) {
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation.setAnimationListener(new AnonymousClass2o4(r2, r13));
                translateAnimation.setDuration(400);
                View view = r2.A00;
                AnonymousClass009.A03(view);
                view.startAnimation(translateAnimation);
                return;
            }
            View view2 = r2.A00;
            AnonymousClass009.A03(view2);
            view2.setVisibility(8);
            r2.A04.removeView(r2.A00);
            r2.A00 = null;
            r13.A00.A01 = null;
            return;
        }
        AnonymousClass2xW r22 = (AnonymousClass2xW) this;
        if (z) {
            Conversation conversation = ((AbstractC47392An) r22).A01;
            ConversationListView conversationListView = conversation.A1g;
            if (conversationListView == null) {
                A0A = false;
            } else {
                A0A = conversationListView.A0A();
            }
            if (A0A) {
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation2.setDuration(220);
                translateAnimation2.setInterpolator(new AccelerateInterpolator());
                r22.A02.startAnimation(translateAnimation2);
            } else {
                ViewGroup viewGroup = r22.A02;
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(viewGroup, "translationY", 0.0f, (float) (-viewGroup.getHeight()));
                ListView A2e = conversation.A2e();
                ObjectAnimator ofInt = ObjectAnimator.ofInt(A2e, "Top", A2e.getTop(), A2e.getTop() - viewGroup.getHeight());
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(ofFloat, ofInt);
                animatorSet.addListener(new C72553en(r13, r22));
                animatorSet.setDuration(220L).start();
                return;
            }
        }
        r22.A06(r13);
    }

    public final void A02(boolean z) {
        C63553Ca r3 = this.A01.A1f;
        Class<?> cls = getClass();
        AbstractC47392An r1 = (AbstractC47392An) r3.A00.get(cls);
        if (cls.equals(r3.A01) && r1 != null) {
            r1.A01(new AnonymousClass4KL(r3), z);
        }
    }

    public boolean A03() {
        AnonymousClass1PD r0;
        if (!(this instanceof AnonymousClass2xW)) {
            AnonymousClass2xV r5 = (AnonymousClass2xV) this;
            if (!r5.A0D && !r5.A0E) {
                AnonymousClass15L r2 = r5.A09;
                Jid A0B = r5.A01.A0B(UserJid.class);
                AnonymousClass009.A05(A0B);
                AnonymousClass1PE A06 = r2.A03.A06((AbstractC14640lm) A0B);
                if (!(A06 == null || A06.A0B == -1 || r5.A02 == null)) {
                    Jid A0B2 = r5.A01.A0B(UserJid.class);
                    AnonymousClass009.A05(A0B2);
                    if (!A0B2.equals(r5.A02) && r5.A06.A0B(r5.A02).A0C == null) {
                        return true;
                    }
                }
            }
        } else {
            AnonymousClass2xW r3 = (AnonymousClass2xW) this;
            if (r3.A09 && r3.A03.A02(AbstractC15460nI.A1P) > 0) {
                AnonymousClass1PE r02 = (AnonymousClass1PE) r3.A05.A0B().get(r3.A00.A0B(AbstractC14640lm.class));
                if (r02 != null && r02.A0h && (r0 = r3.A00.A0G) != null && !TextUtils.isEmpty(r0.A02)) {
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean A04() {
        C63553Ca r2 = this.A01.A1f;
        return r2.A01 != null && r2.A00.get(r2.A01) == this;
    }
}
