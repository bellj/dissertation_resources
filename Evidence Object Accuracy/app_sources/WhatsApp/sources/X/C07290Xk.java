package X;

import android.app.job.JobParameters;
import android.app.job.JobWorkItem;
import android.content.Intent;

/* renamed from: X.0Xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07290Xk implements AbstractC12330hk {
    public final JobWorkItem A00;
    public final /* synthetic */ job.JobServiceEngineC019209b A01;

    public C07290Xk(JobWorkItem jobWorkItem, job.JobServiceEngineC019209b r2) {
        this.A01 = r2;
        this.A00 = jobWorkItem;
    }

    @Override // X.AbstractC12330hk
    public void A7T() {
        job.JobServiceEngineC019209b r0 = this.A01;
        synchronized (r0.A02) {
            JobParameters jobParameters = r0.A00;
            if (jobParameters != null) {
                jobParameters.completeWork(this.A00);
            }
        }
    }

    @Override // X.AbstractC12330hk
    public Intent getIntent() {
        return this.A00.getIntent();
    }
}
