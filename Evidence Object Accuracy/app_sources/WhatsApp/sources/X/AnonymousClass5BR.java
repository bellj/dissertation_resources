package X;

import java.io.Serializable;

/* renamed from: X.5BR  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BR implements Serializable {
    public final Throwable exception;

    public AnonymousClass5BR(Throwable th) {
        this.exception = th;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass5BR) && C16700pc.A0O(this.exception, ((AnonymousClass5BR) obj).exception);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.exception.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Failure(");
        A0k.append(this.exception);
        return C12970iu.A0u(A0k);
    }
}
