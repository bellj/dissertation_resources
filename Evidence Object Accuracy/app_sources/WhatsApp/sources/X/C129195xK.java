package X;

import android.content.Context;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129195xK {
    public String A00;
    public final Context A01;
    public final C14900mE A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final AnonymousClass60T A05;
    public final C18590sh A06;

    public C129195xK(Context context, C14900mE r9, C18650sn r10, C18610sj r11, AnonymousClass60T r12, C18590sh r13, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        String str11 = str3;
        String str12 = str5;
        String str13 = str6;
        this.A01 = context;
        this.A02 = r9;
        this.A06 = r13;
        this.A04 = r11;
        this.A03 = r10;
        this.A05 = r12;
        if (str11.length() == 12) {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(str11.substring(0, 4));
            A0h.append("9");
            str11 = C12960it.A0d(str11.substring(4), A0h);
        }
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("fullName", str);
            A0a.put("personalID", str2);
            A0a.put("phone", str11);
            JSONObject A0a2 = C117295Zj.A0a();
            A0a2.put("address", str4);
            String str14 = "";
            A0a2.put("addressNumber", str5 == null ? str14 : str12);
            A0a2.put("extraLine", str6 == null ? str14 : str13);
            A0a2.put("neighborhood", str7 != null ? str7 : str14);
            A0a2.put("city", str8);
            A0a2.put("state", str9);
            A0a2.put("addressCode", str10);
            A0a2.put("country", "BR");
            this.A00 = C117305Zk.A0l(A0a2, "address", A0a);
        } catch (JSONException e) {
            Log.e(C12960it.A0b("PAY: BrazilSendKYCAction Exception: ", e));
        }
    }

    public final void A00(AnonymousClass6MT r16, AnonymousClass6B7 r17) {
        Context context = this.A01;
        C14900mE r10 = this.A02;
        C18590sh r14 = this.A06;
        C128065vV r8 = new C128065vV(context, r10, this.A03, this.A04, this.A05, r14);
        try {
            AnonymousClass1V8[] r4 = new AnonymousClass1V8[1];
            byte[] A9N = r17.A00.A9N(C117315Zl.A0a(this.A00), C003501n.A0D(16));
            C30931Zj r2 = r8.A05;
            StringBuilder A0h = C12960it.A0h();
            A0h.append("sendKyc Text Blob : ");
            r2.A06(C12960it.A0d(Base64.encodeToString(A9N, 2), A0h));
            AnonymousClass1W9[] r22 = new AnonymousClass1W9[1];
            boolean A1a = C12990iw.A1a("key-type", r17.A03, r22);
            r4[A1a ? 1 : 0] = new AnonymousClass1V8("text", A9N, r22);
            ArrayList A0l = C12960it.A0l();
            C117295Zj.A1M("action", "send-kyc-data", A0l);
            String str = r17.A05;
            C117295Zj.A1M("provider", str, A0l);
            C117295Zj.A1M("key-version", r17.A04, A0l);
            C117295Zj.A1M("device-id", r8.A06.A01(), A0l);
            C117305Zk.A1H(r8.A03, new C120245fr(r8.A00, r8.A01, r8.A02, r16, r8, "send-kyc-data", str), new AnonymousClass1V8("account", C117305Zk.A1b(A0l), r4));
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
}
