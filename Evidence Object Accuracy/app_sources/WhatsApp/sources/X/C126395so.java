package X;

/* renamed from: X.5so  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126395so {
    public final AnonymousClass1V8 A00;

    public C126395so(AnonymousClass3CT r7, C126405sp r8, Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-accept-collect");
        if (C117295Zj.A1W(str, 0, false)) {
            C41141sy.A01(A0N, "mpin", str);
        }
        if (C117295Zj.A1V(str2, 1, false)) {
            C41141sy.A01(A0N, "device-id", str2);
        }
        if (C117305Zk.A1Y(str3, false)) {
            C41141sy.A01(A0N, "credential-id", str3);
        }
        if (C117295Zj.A1W(str4, 1, false)) {
            C41141sy.A01(A0N, "id", str4);
        }
        if (str5 != null && C117295Zj.A1X(str5, true)) {
            C41141sy.A01(A0N, "sender-vpa", str5);
        }
        if (str6 != null && C117295Zj.A1X(str6, true)) {
            C41141sy.A01(A0N, "sender-vpa-id", str6);
        }
        if (str7 != null && C117295Zj.A1Y(str7, true)) {
            C41141sy.A01(A0N, "upi-bank-info", str7);
        }
        if (str8 != null && C117295Zj.A1X(str8, true)) {
            C41141sy.A01(A0N, "receiver-vpa", str8);
        }
        if (str9 != null && C117295Zj.A1X(str9, true)) {
            C41141sy.A01(A0N, "receiver-vpa-id", str9);
        }
        if (str10 != null && C117295Zj.A1X(str10, true)) {
            C41141sy.A01(A0N, "mandate-no", str10);
        }
        if (l != null && AnonymousClass3JT.A0D(l, 1, true)) {
            C117315Zl.A0X(A0N, "version", l.longValue());
        }
        A0N.A05(r8.A00);
        this.A00 = C117295Zj.A0J(A0N, A0M, r7);
    }
}
