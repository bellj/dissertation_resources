package X;

/* renamed from: X.43C  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43C extends AbstractC16110oT {
    public String A00;

    public AnonymousClass43C() {
        super(2768, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamSupportInteraction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "supportLogId", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
