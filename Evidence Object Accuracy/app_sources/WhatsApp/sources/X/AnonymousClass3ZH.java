package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.whatsapp.util.Log;

/* renamed from: X.3ZH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZH implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1ON A00;
    public final /* synthetic */ C244315m A01;
    public final /* synthetic */ byte[] A02;
    public final /* synthetic */ byte[] A03;

    public AnonymousClass3ZH(AnonymousClass1ON r1, C244315m r2, byte[] bArr, byte[] bArr2) {
        this.A01 = r2;
        this.A03 = bArr;
        this.A02 = bArr2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendBeginRegI/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r13, String str) {
        byte[] bArr = this.A03;
        byte[] bArr2 = this.A02;
        AnonymousClass1ON r7 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/beginRegOnSuccess id=")));
        byte[] A01 = C244315m.A01(r13, r7, "r2");
        byte[] A012 = C244315m.A01(r13, r7, "r2_sig");
        byte[] A013 = C244315m.A01(r13, r7, "opaque_c");
        if (A01 != null && A012 != null && A013 != null) {
            byte[] bArr3 = C244315m.A02;
            int length = bArr3.length;
            int length2 = A01.length;
            byte[] bArr4 = new byte[length + length2];
            System.arraycopy(bArr3, 0, bArr4, 0, length);
            System.arraycopy(A01, 0, bArr4, length, length2);
            if (!C16550pE.A03(bArr4, A012, bArr2)) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/beginLoginOnSuccess/r2 cannot be verified with r2_sig and ed_pub id=")));
                r7.APr("r2 cannot be verified with r2_sig and ed_pub", 2);
                return;
            }
            ((AnonymousClass1OF) r7).A00.A01();
            ((AnonymousClass1OF) r7).A01.Ab2(new RunnableBRunnable0Shape0S0400000_I0(r7, bArr, A01, A013, 6));
        }
    }
}
