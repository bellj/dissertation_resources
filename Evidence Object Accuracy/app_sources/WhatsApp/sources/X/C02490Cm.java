package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.0Cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02490Cm extends AbstractC05550Pz {
    public final AtomicReferenceFieldUpdater A00;
    public final AtomicReferenceFieldUpdater A01;
    public final AtomicReferenceFieldUpdater A02;
    public final AtomicReferenceFieldUpdater A03;
    public final AtomicReferenceFieldUpdater A04;

    public C02490Cm(AtomicReferenceFieldUpdater atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater5) {
        this.A03 = atomicReferenceFieldUpdater;
        this.A02 = atomicReferenceFieldUpdater2;
        this.A04 = atomicReferenceFieldUpdater3;
        this.A00 = atomicReferenceFieldUpdater4;
        this.A01 = atomicReferenceFieldUpdater5;
    }

    @Override // X.AbstractC05550Pz
    public void A00(C06320Td r2, C06320Td r3) {
        this.A02.lazySet(r2, r3);
    }

    @Override // X.AbstractC05550Pz
    public void A01(C06320Td r2, Thread thread) {
        this.A03.lazySet(r2, thread);
    }

    @Override // X.AbstractC05550Pz
    public boolean A02(C05910Rl r2, C05910Rl r3, AbstractC08750bn r4) {
        return AnonymousClass0KN.A00(r4, r2, r3, this.A00);
    }

    @Override // X.AbstractC05550Pz
    public boolean A03(C06320Td r2, C06320Td r3, AbstractC08750bn r4) {
        return AnonymousClass0KN.A00(r4, r2, r3, this.A04);
    }

    @Override // X.AbstractC05550Pz
    public boolean A04(AbstractC08750bn r3, Object obj, Object obj2) {
        return AnonymousClass0KN.A00(r3, null, obj2, this.A01);
    }
}
