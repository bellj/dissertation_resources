package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0JV  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JV {
    /* Fake field, exist only in values array */
    target,
    /* Fake field, exist only in values array */
    root,
    nth_child,
    /* Fake field, exist only in values array */
    nth_last_child,
    nth_of_type,
    nth_last_of_type,
    /* Fake field, exist only in values array */
    first_child,
    /* Fake field, exist only in values array */
    last_child,
    /* Fake field, exist only in values array */
    first_of_type,
    /* Fake field, exist only in values array */
    last_of_type,
    /* Fake field, exist only in values array */
    only_child,
    /* Fake field, exist only in values array */
    only_of_type,
    /* Fake field, exist only in values array */
    empty,
    /* Fake field, exist only in values array */
    not,
    /* Fake field, exist only in values array */
    lang,
    /* Fake field, exist only in values array */
    link,
    /* Fake field, exist only in values array */
    visited,
    /* Fake field, exist only in values array */
    hover,
    /* Fake field, exist only in values array */
    active,
    /* Fake field, exist only in values array */
    focus,
    /* Fake field, exist only in values array */
    enabled,
    /* Fake field, exist only in values array */
    disabled,
    /* Fake field, exist only in values array */
    checked,
    /* Fake field, exist only in values array */
    indeterminate,
    UNSUPPORTED;
    
    public static final Map A00 = new HashMap();

    static {
        AnonymousClass0JV[] values = values();
        for (AnonymousClass0JV r3 : values) {
            if (r3 != UNSUPPORTED) {
                A00.put(r3.name().replace('_', '-'), r3);
            }
        }
    }
}
