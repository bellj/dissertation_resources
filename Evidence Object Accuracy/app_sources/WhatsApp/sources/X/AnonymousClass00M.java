package X;

import android.content.Context;
import java.lang.Thread;

/* renamed from: X.00M  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00M implements Thread.UncaughtExceptionHandler {
    public final Thread.UncaughtExceptionHandler A00 = Thread.getDefaultUncaughtExceptionHandler();
    public final /* synthetic */ Context A01;
    public final /* synthetic */ AnonymousClass002 A02;

    public AnonymousClass00M(Context context, AnonymousClass002 r3) {
        this.A02 = r3;
        this.A01 = context;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        C003601o.A00(this.A01, th);
        C003601o.A03(this.A00, thread, th);
    }
}
