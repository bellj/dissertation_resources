package X;

import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;

/* renamed from: X.32T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32T extends RunnableC32531cJ {
    public final /* synthetic */ int A00;
    public final /* synthetic */ LeaveGroupsDialogFragment A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass32T(LeaveGroupsDialogFragment leaveGroupsDialogFragment, C21320xE r11, C20710wC r12, C15580nU r13, C14860mA r14, int i) {
        super(r11, r12, r13, null, r14, null, null, 16);
        this.A01 = leaveGroupsDialogFragment;
        this.A00 = i;
    }
}
