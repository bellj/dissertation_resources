package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/* renamed from: X.0Se  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06100Se {
    public final String A00;
    public final Map A01;
    public final Set A02;
    public final Set A03;

    public C06100Se(String str, Map map, Set set, Set set2) {
        Set unmodifiableSet;
        this.A00 = str;
        this.A01 = Collections.unmodifiableMap(map);
        this.A02 = Collections.unmodifiableSet(set);
        if (set2 == null) {
            unmodifiableSet = null;
        } else {
            unmodifiableSet = Collections.unmodifiableSet(set2);
        }
        this.A03 = unmodifiableSet;
    }

    /* JADX INFO: finally extract failed */
    public static C06100Se A00(AbstractC12920im r21, String str) {
        C05340Pe r4;
        StringBuilder sb = new StringBuilder("PRAGMA table_info(`");
        sb.append(str);
        sb.append("`)");
        AnonymousClass0ZE r2 = (AnonymousClass0ZE) r21;
        Cursor AZi = r2.AZi(new AnonymousClass0ZK(sb.toString()));
        HashMap hashMap = new HashMap();
        try {
            if (AZi.getColumnCount() > 0) {
                int columnIndex = AZi.getColumnIndex("name");
                int columnIndex2 = AZi.getColumnIndex("type");
                int columnIndex3 = AZi.getColumnIndex("notnull");
                int columnIndex4 = AZi.getColumnIndex("pk");
                int columnIndex5 = AZi.getColumnIndex("dflt_value");
                while (AZi.moveToNext()) {
                    String string = AZi.getString(columnIndex);
                    String string2 = AZi.getString(columnIndex2);
                    boolean z = false;
                    if (AZi.getInt(columnIndex3) != 0) {
                        z = true;
                    }
                    hashMap.put(string, new C05470Pr(string, string2, AZi.getString(columnIndex5), AZi.getInt(columnIndex4), 2, z));
                }
            }
            AZi.close();
            HashSet hashSet = new HashSet();
            StringBuilder sb2 = new StringBuilder("PRAGMA foreign_key_list(`");
            sb2.append(str);
            sb2.append("`)");
            Cursor AZi2 = r2.AZi(new AnonymousClass0ZK(sb2.toString()));
            try {
                int columnIndex6 = AZi2.getColumnIndex("id");
                int columnIndex7 = AZi2.getColumnIndex("seq");
                int columnIndex8 = AZi2.getColumnIndex("table");
                int columnIndex9 = AZi2.getColumnIndex("on_delete");
                int columnIndex10 = AZi2.getColumnIndex("on_update");
                int columnIndex11 = AZi2.getColumnIndex("id");
                int columnIndex12 = AZi2.getColumnIndex("seq");
                int columnIndex13 = AZi2.getColumnIndex("from");
                int columnIndex14 = AZi2.getColumnIndex("to");
                int count = AZi2.getCount();
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < count; i++) {
                    AZi2.moveToPosition(i);
                    arrayList.add(new C08930c5(AZi2.getString(columnIndex13), AZi2.getString(columnIndex14), AZi2.getInt(columnIndex11), AZi2.getInt(columnIndex12)));
                }
                Collections.sort(arrayList);
                int count2 = AZi2.getCount();
                for (int i2 = 0; i2 < count2; i2++) {
                    AZi2.moveToPosition(i2);
                    if (AZi2.getInt(columnIndex7) == 0) {
                        int i3 = AZi2.getInt(columnIndex6);
                        ArrayList arrayList2 = new ArrayList();
                        ArrayList arrayList3 = new ArrayList();
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            C08930c5 r8 = (C08930c5) it.next();
                            if (r8.A00 == i3) {
                                arrayList2.add(r8.A02);
                                arrayList3.add(r8.A03);
                            }
                        }
                        hashSet.add(new C05420Pm(AZi2.getString(columnIndex8), AZi2.getString(columnIndex9), AZi2.getString(columnIndex10), arrayList2, arrayList3));
                    }
                }
                AZi2.close();
                StringBuilder sb3 = new StringBuilder("PRAGMA index_list(`");
                sb3.append(str);
                sb3.append("`)");
                Cursor AZi3 = r2.AZi(new AnonymousClass0ZK(sb3.toString()));
                try {
                    int columnIndex15 = AZi3.getColumnIndex("name");
                    int columnIndex16 = AZi3.getColumnIndex("origin");
                    int columnIndex17 = AZi3.getColumnIndex("unique");
                    HashSet hashSet2 = null;
                    if (!(columnIndex15 == -1 || columnIndex16 == -1 || columnIndex17 == -1)) {
                        HashSet hashSet3 = new HashSet();
                        while (AZi3.moveToNext()) {
                            if ("c".equals(AZi3.getString(columnIndex16))) {
                                String string3 = AZi3.getString(columnIndex15);
                                boolean z2 = true;
                                if (AZi3.getInt(columnIndex17) != 1) {
                                    z2 = false;
                                }
                                StringBuilder sb4 = new StringBuilder("PRAGMA index_xinfo(`");
                                sb4.append(string3);
                                sb4.append("`)");
                                Cursor AZi4 = r2.AZi(new AnonymousClass0ZK(sb4.toString()));
                                int columnIndex18 = AZi4.getColumnIndex("seqno");
                                int columnIndex19 = AZi4.getColumnIndex("cid");
                                int columnIndex20 = AZi4.getColumnIndex("name");
                                if (columnIndex18 == -1 || columnIndex19 == -1 || columnIndex20 == -1) {
                                    r4 = null;
                                } else {
                                    TreeMap treeMap = new TreeMap();
                                    while (AZi4.moveToNext()) {
                                        if (AZi4.getInt(columnIndex19) >= 0) {
                                            int i4 = AZi4.getInt(columnIndex18);
                                            treeMap.put(Integer.valueOf(i4), AZi4.getString(columnIndex20));
                                        }
                                    }
                                    ArrayList arrayList4 = new ArrayList(treeMap.size());
                                    arrayList4.addAll(treeMap.values());
                                    r4 = new C05340Pe(string3, arrayList4, z2);
                                }
                                AZi4.close();
                                if (r4 != null) {
                                    hashSet3.add(r4);
                                }
                            }
                        }
                        AZi3.close();
                        hashSet2 = hashSet3;
                        return new C06100Se(str, hashMap, hashSet, hashSet2);
                    }
                    AZi3.close();
                    return new C06100Se(str, hashMap, hashSet, hashSet2);
                } catch (Throwable th) {
                    AZi3.close();
                    throw th;
                }
            } catch (Throwable th2) {
                AZi2.close();
                throw th2;
            }
        } catch (Throwable th3) {
            AZi.close();
            throw th3;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r1.equals(r0) == false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0049
            r2 = 0
            if (r5 == 0) goto L_0x0028
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0028
            X.0Se r5 = (X.C06100Se) r5
            java.lang.String r1 = r4.A00
            java.lang.String r0 = r5.A00
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0028
            java.util.Map r1 = r4.A01
            java.util.Map r0 = r5.A01
            if (r1 == 0) goto L_0x0029
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x002c
        L_0x0028:
            return r2
        L_0x0029:
            if (r0 == 0) goto L_0x002c
            return r2
        L_0x002c:
            java.util.Set r1 = r4.A02
            java.util.Set r0 = r5.A02
            if (r1 == 0) goto L_0x0039
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x003c
            return r2
        L_0x0039:
            if (r0 == 0) goto L_0x003c
            return r2
        L_0x003c:
            java.util.Set r1 = r4.A03
            if (r1 == 0) goto L_0x0049
            java.util.Set r0 = r5.A03
            if (r0 == 0) goto L_0x0049
            boolean r0 = r1.equals(r0)
            return r0
        L_0x0049:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06100Se.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int hashCode = this.A00.hashCode() * 31;
        Map map = this.A01;
        if (map != null) {
            i = map.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        Set set = this.A02;
        if (set != null) {
            i2 = set.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("TableInfo{name='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", columns=");
        sb.append(this.A01);
        sb.append(", foreignKeys=");
        sb.append(this.A02);
        sb.append(", indices=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
