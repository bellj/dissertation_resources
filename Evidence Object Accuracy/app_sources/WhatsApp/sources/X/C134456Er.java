package X;

import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.6Er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134456Er implements AbstractC136386Mi {
    public final AnonymousClass018 A00;

    public C134456Er(AnonymousClass018 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119415dw A8P(WaBloksActivity waBloksActivity) {
        return new C124465pY(this.A00, waBloksActivity);
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119405dv A8Q(WaBloksActivity waBloksActivity, C129125xD r4) {
        return new C124415pT(this.A00, waBloksActivity, r4);
    }
}
