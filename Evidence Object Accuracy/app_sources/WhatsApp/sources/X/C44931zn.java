package X;

/* renamed from: X.1zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44931zn extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Double A03;
    public Double A04;
    public Double A05;
    public Double A06;
    public Double A07;
    public Double A08;
    public Double A09;
    public Double A0A;
    public Double A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;

    public C44931zn() {
        super(484, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(23, this.A03);
        r3.Abe(27, this.A00);
        r3.Abe(17, this.A0C);
        r3.Abe(24, this.A0H);
        r3.Abe(10, this.A04);
        r3.Abe(22, this.A0I);
        r3.Abe(6, this.A0J);
        r3.Abe(21, this.A0K);
        r3.Abe(5, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A0L);
        r3.Abe(14, this.A05);
        r3.Abe(25, this.A0M);
        r3.Abe(11, this.A06);
        r3.Abe(15, this.A07);
        r3.Abe(1, this.A0D);
        r3.Abe(4, this.A0N);
        r3.Abe(7, this.A0E);
        r3.Abe(8, this.A0O);
        r3.Abe(9, this.A08);
        r3.Abe(13, this.A09);
        r3.Abe(12, this.A0A);
        r3.Abe(20, this.A0F);
        r3.Abe(26, this.A0B);
        r3.Abe(18, this.A0G);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        StringBuilder sb = new StringBuilder("WamBackup {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupDeletedMediaSize", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupIsTrimmed", this.A00);
        Integer num = this.A0C;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupNetworkSetting", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupNumberOfFilesDeleted", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreChatdbSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreEncryptionVersion", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreFinishedOverWifi", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreInSessionRetryCount", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIncludeVideos", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIsFull", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIsWifi", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreMediaFileCount", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreMediaRetentionInDays", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreMediaSize", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreNetworkRequestCount", this.A07);
        Integer num2 = this.A0D;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreResult", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreRetryCount", this.A0N);
        Integer num3 = this.A0E;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreStage", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreT", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTotalSize", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTransferFailedSize", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTransferSize", this.A0A);
        Integer num4 = this.A0F;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreType", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreUserSettingsSize", this.A0B);
        Integer num5 = this.A0G;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupSchedule", obj5);
        sb.append("}");
        return sb.toString();
    }
}
