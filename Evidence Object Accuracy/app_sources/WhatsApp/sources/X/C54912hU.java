package X;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2hU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54912hU extends AnonymousClass03U {
    public final CheckBox A00;
    public final TextView A01;

    public C54912hU(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.media_section);
        CheckBox checkBox = (CheckBox) AnonymousClass028.A0D(view, R.id.media_select_all_checkbox);
        this.A00 = checkBox;
        checkBox.setText(view.getResources().getText(R.string.select_all));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.4p6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                compoundButton.jumpDrawablesToCurrentState();
            }
        });
    }
}
