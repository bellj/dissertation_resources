package X;

import android.view.View;

/* renamed from: X.47Z  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47Z extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AnonymousClass35V A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ boolean A02;

    public AnonymousClass47Z(AnonymousClass35V r1, String str, boolean z) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = z;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        this.A00.A0F(this.A01, null, this.A02);
    }
}
