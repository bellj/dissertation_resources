package X;

import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;

/* renamed from: X.5bM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118155bM extends AnonymousClass015 {
    public AbstractC18860tB A00;
    public AbstractC35651iS A01;
    public final AnonymousClass017 A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass12H A04;
    public final C14850m9 A05;
    public final UserJid A06;
    public final C243515e A07;
    public final AnonymousClass2I6 A08;
    public final C130725zs A09 = new C130725zs(null, null, 1);
    public final AnonymousClass1IS A0A;
    public final AbstractC14440lR A0B;
    public final boolean A0C;

    public C118155bM(AnonymousClass12H r4, C14850m9 r5, UserJid userJid, C243515e r7, AnonymousClass2I6 r8, AnonymousClass1IS r9, AbstractC14440lR r10, boolean z) {
        C16700pc.A0E(r5, 1);
        C117295Zj.A1K(r10, r4, r7, r8);
        C16700pc.A0E(r9, 7);
        this.A05 = r5;
        this.A0B = r10;
        this.A04 = r4;
        this.A07 = r7;
        this.A08 = r8;
        this.A06 = userJid;
        this.A0A = r9;
        this.A0C = z;
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A03 = A0T;
        this.A02 = A0T;
        C119685ew r0 = new C119685ew(this);
        this.A00 = r0;
        r4.A03(r0);
        AnonymousClass69T r02 = new AnonymousClass69T(this);
        this.A01 = r02;
        r7.A03(r02);
    }

    public static final /* synthetic */ void A00(AnonymousClass1IR r3, C118155bM r4) {
        AnonymousClass60N r0;
        C16380ov r1;
        AnonymousClass617 r02 = r4.A09.A00;
        String str = null;
        if (r02 != null && (r0 = (AnonymousClass60N) r02.A01) != null && (r1 = r0.A03) != null) {
            AnonymousClass1IR r03 = r1.A0L;
            if (r03 != null) {
                str = r03.A0K;
            }
            if (C16700pc.A0O(str, r3.A0K)) {
                r1.A0L = r3;
                r4.A06(r1);
            }
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A04.A04(this.A00);
        this.A07.A04(this.A01);
    }

    public final void A04() {
        this.A03.A0A(this.A09.A00(this.A06, null, null, null, Boolean.valueOf(this.A0C)));
        this.A0B.Ab6(new Runnable() { // from class: X.6H0
            @Override // java.lang.Runnable
            public final void run() {
                C30821Yy r0;
                AbstractC30791Yv r02;
                AnonymousClass1ZD r03;
                AnonymousClass1ZD r04;
                C16470p4 r05;
                AnonymousClass1ZD r1;
                C16470p4 r06;
                AnonymousClass1ZD r07;
                String str;
                C118155bM r3 = C118155bM.this;
                C16700pc.A0E(r3, 0);
                AnonymousClass2I6 r2 = r3.A08;
                AnonymousClass1IS r12 = r3.A0A;
                C16700pc.A0E(r12, 0);
                C16380ov r5 = (C16380ov) r2.A00.A0K.A03(r12);
                AbstractC30791Yv r4 = null;
                if (!(r5 == null || (r06 = r5.A00) == null || (r07 = r06.A01) == null || (str = r07.A02) == null)) {
                    AnonymousClass1IR A0M = r2.A01.A0M(str);
                    if (A0M != null) {
                        r5.A0L = A0M;
                    } else {
                        Log.e("Pay: PaymentCheckoutOrderViewModel/loadData the paymentTransactionInfo fetched from PaymentTransactionStore is null");
                    }
                }
                r3.A06(r5);
                if (r5 == null || (r05 = r5.A00) == null || (r1 = r05.A01) == null) {
                    r0 = null;
                } else {
                    AnonymousClass1ZI r08 = r1.A05;
                    C16700pc.A0C(r08);
                    r0 = r1.A01(r08).A02;
                }
                C16700pc.A0C(r0);
                BigDecimal bigDecimal = r0.A00;
                C16700pc.A0B(bigDecimal);
                C94074bD r7 = new C94074bD();
                r7.A01 = 1000;
                C14850m9 r9 = r3.A05;
                r7.A02 = (long) r9.A02(1826);
                C16470p4 r09 = r5.A00;
                if (r09 == null || (r04 = r09.A01) == null) {
                    r02 = null;
                } else {
                    r02 = r04.A03;
                }
                r7.A03 = r02;
                AnonymousClass20C A00 = r7.A00();
                C94074bD r8 = new C94074bD();
                r8.A01 = 1000;
                r8.A02 = (long) r9.A02(1712);
                C16470p4 r010 = r5.A00;
                if (!(r010 == null || (r03 = r010.A01) == null)) {
                    r4 = r03.A03;
                }
                r8.A03 = r4;
                AnonymousClass20C A002 = r8.A00();
                if (bigDecimal.floatValue() < A00.A02.A00.floatValue() || bigDecimal.floatValue() > A002.A02.A00.floatValue()) {
                    r3.A03.A0A(r3.A09.A00(null, null, EnumC124545pi.A02, null, null));
                    return;
                }
                UserJid userJid = r3.A06;
                if (userJid != null) {
                    r2.A02.A00(userJid, new C133296Af(r3));
                }
            }
        });
    }

    public final void A05(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("save_order_detail_state_key");
        if (bundle2 != null) {
            boolean z = bundle2.getBoolean("should_show_shimmer_key");
            C1315963j r5 = (C1315963j) bundle2.getSerializable("checkout_error_code_key");
            this.A0B.Ab6(new Runnable((AnonymousClass1IR) bundle2.getParcelable("payment_transaction_key"), (UserJid) bundle2.getParcelable("merchant_jid_key"), this, r5, (EnumC124545pi) bundle2.getSerializable("merchant_status_key"), z) { // from class: X.6K6
                public final /* synthetic */ AnonymousClass1IR A00;
                public final /* synthetic */ UserJid A01;
                public final /* synthetic */ C118155bM A02;
                public final /* synthetic */ C1315963j A03;
                public final /* synthetic */ EnumC124545pi A04;
                public final /* synthetic */ boolean A05;

                {
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A05 = r6;
                    this.A03 = r4;
                    this.A04 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C118155bM r4 = this.A02;
                    AnonymousClass1IR r3 = this.A00;
                    UserJid userJid = this.A01;
                    boolean z2 = this.A05;
                    C1315963j r6 = this.A03;
                    EnumC124545pi r7 = this.A04;
                    C16700pc.A0E(r4, 0);
                    AnonymousClass1IS r1 = r4.A0A;
                    if (r1 != null) {
                        C16380ov r8 = (C16380ov) r4.A08.A00.A0K.A03(r1);
                        if (r8 != null) {
                            r8.A0L = r3;
                        }
                        r4.A03.A0A(r4.A09.A00(userJid, r6, r7, r8, Boolean.valueOf(z2)));
                    }
                }
            });
        }
    }

    public final void A06(C16380ov r12) {
        AnonymousClass617 A00;
        C130725zs r5 = this.A09;
        if (r12 == null) {
            EnumC124535ph r4 = EnumC124535ph.A03;
            int A01 = C117315Zl.A01(r4, C125305r2.A00);
            int i = R.string.order_details_order_details_not_available_title;
            int i2 = R.string.order_details_order_details_not_available_content;
            if (A01 != 1) {
                i = R.string.error_notification_title;
                i2 = R.string.something_went_wrong;
            }
            A00 = r5.A00(null, new C1315963j(r4, i, i2), null, null, null);
        } else {
            A00 = r5.A00(null, null, null, r12, null);
        }
        this.A03.A0A(A00);
    }
}
