package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.5oX  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oX extends AbstractC16350or {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AnonymousClass6CZ A01;

    public AnonymousClass5oX(ImageView imageView, AnonymousClass6CZ r2) {
        this.A01 = r2;
        this.A00 = imageView;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AnonymousClass6CZ r3 = this.A01;
        Context context = r3.A0D.A00;
        File file = new File(context.getFilesDir(), "");
        if (file.exists() || file.mkdirs()) {
            return r3.A0E.A03(new File(file, "002_invite_bottom.webp"), "invite", (int) context.getResources().getDimension(R.dimen.novi_invite_image_width), (int) context.getResources().getDimension(R.dimen.novi_invite_image_height));
        }
        Log.e(C12960it.A0d(file.getAbsolutePath(), C12960it.A0k("[PAY] : NoviInviteViewComponent/loadDownloadedInviteBubbleImage/Could not make directory ")));
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap != null) {
            ImageView imageView = this.A00;
            imageView.setImageDrawable(new BitmapDrawable(imageView.getResources(), bitmap));
            imageView.setVisibility(0);
        }
    }
}
