package X;

import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.TextView;
import com.whatsapp.registration.VerifyPhoneNumber;

/* renamed from: X.2Zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51932Zs extends CountDownTimer {
    public final /* synthetic */ AnonymousClass3FQ A00;
    public final /* synthetic */ boolean A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51932Zs(AnonymousClass3FQ r3, long j, boolean z) {
        super(j, 1000);
        this.A00 = r3;
        this.A01 = z;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        AnonymousClass3FQ r1 = this.A00;
        r1.A02(true);
        r1.A02 = null;
        AnonymousClass4L2 r0 = r1.A03;
        if (r0 != null) {
            VerifyPhoneNumber verifyPhoneNumber = r0.A00;
            if (AnonymousClass3J8.A01(verifyPhoneNumber.A2e())) {
                verifyPhoneNumber.A3A(1);
            }
        }
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        AnonymousClass3FQ r2 = this.A00;
        r2.A01 = j;
        if (!this.A01) {
            r2.A07.setText(r2.A00);
            r2.A08.setVisibility(8);
        } else if (j > 3600000) {
            int ceil = (int) Math.ceil(((double) j) / 3600000.0d);
            Button button = r2.A07;
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, ceil, 0);
            button.setText(r2.A09.A0I(A1b, r2.A05, (long) ceil));
        } else {
            r2.A07.setText(r2.A00);
            TextView textView = r2.A08;
            textView.setVisibility(0);
            C38131nZ.A0C(textView, r2.A09, C12980iv.A0D(j));
        }
    }
}
