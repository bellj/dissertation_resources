package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.biz.catalog.view.EllipsizedTextEmojiLabel;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3VO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VO implements AbstractC72383eV, AbstractC13960kc {
    public C30141Wg A00;
    public String A01;
    public boolean A02;
    public boolean A03;
    public final Context A04;
    public final AnonymousClass12P A05;
    public final C14900mE A06;
    public final C15570nT A07;
    public final C14650lo A08;
    public final C25821Ay A09;
    public final C19850um A0A;
    public final AnonymousClass19Q A0B;
    public final AnonymousClass19T A0C;
    public final CatalogMediaCard A0D;
    public final C253619c A0E;
    public final AnonymousClass17R A0F;
    public final AbstractC14440lR A0G;
    public final boolean A0H;

    public AnonymousClass3VO(AnonymousClass12P r2, C14900mE r3, C15570nT r4, C14650lo r5, C25821Ay r6, C19850um r7, AnonymousClass19Q r8, AnonymousClass19T r9, CatalogMediaCard catalogMediaCard, C253619c r11, AnonymousClass17R r12, AbstractC14440lR r13, boolean z) {
        this.A06 = r3;
        this.A07 = r4;
        this.A0F = r12;
        this.A05 = r2;
        this.A0E = r11;
        this.A0H = z;
        this.A0B = r8;
        this.A0G = r13;
        this.A08 = r5;
        this.A0C = r9;
        this.A0A = r7;
        this.A09 = r6;
        this.A0D = catalogMediaCard;
        this.A04 = catalogMediaCard.getContext();
        r6.A03(this);
    }

    public final void A00() {
        Activity A00 = AnonymousClass12P.A00(this.A04);
        if (A00 instanceof ProductDetailActivity) {
            AnonymousClass283 r2 = (AnonymousClass283) A00;
            r2.A0a.A01 = true;
            C12970iu.A1G(r2.A0W);
            EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel = r2.A0X;
            if (ellipsizedTextEmojiLabel != null) {
                ellipsizedTextEmojiLabel.A00 = Integer.MAX_VALUE;
            }
        }
    }

    @Override // X.AbstractC72383eV
    public void A5n() {
        if (!this.A03) {
            this.A0D.A0I.A06(3);
            this.A03 = true;
        }
    }

    @Override // X.AbstractC72383eV
    public void A7A() {
        this.A09.A04(this);
    }

    @Override // X.AbstractC72383eV
    public void A9s(UserJid userJid, int i) {
        this.A0C.A03(userJid, i);
    }

    @Override // X.AbstractC72383eV
    public int AFx(UserJid userJid) {
        return this.A0A.A00(userJid);
    }

    @Override // X.AbstractC72383eV
    public AbstractC116285Uv AHC(C44691zO r2, UserJid userJid, boolean z) {
        return new AbstractC116285Uv(r2, this, userJid, z) { // from class: X.3ac
            public final /* synthetic */ C44691zO A00;
            public final /* synthetic */ AnonymousClass3VO A01;
            public final /* synthetic */ UserJid A02;
            public final /* synthetic */ boolean A03;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A03 = r4;
                this.A02 = r3;
            }

            @Override // X.AbstractC116285Uv
            public final void AOB(View view, AnonymousClass4TJ r15) {
                AnonymousClass3VO r22 = this.A01;
                C44691zO r3 = this.A00;
                boolean z2 = this.A03;
                UserJid userJid2 = this.A02;
                if (view.getTag(R.id.loaded_image_url) != null) {
                    C19850um r1 = r22.A0A;
                    String str = r3.A0D;
                    if (r1.A05(null, str) == null) {
                        r22.A06.A07(R.string.catalog_error_missing_product, 0);
                        Log.w("CatalogMediaCard/MediaThumbnailOnClick/product no longer exists");
                        return;
                    }
                    CatalogMediaCard catalogMediaCard = r22.A0D;
                    AnonymousClass5RI r0 = catalogMediaCard.A0B;
                    if (r0 != null) {
                        AnonymousClass2VA.A01(((C1099753t) r0).A00, 7);
                    }
                    Context context = r22.A04;
                    Intent A0c = C14960mK.A0c(context, z2);
                    int thumbnailPixelSize = catalogMediaCard.A0I.getThumbnailPixelSize();
                    boolean A0F = r22.A07.A0F(userJid2);
                    Integer valueOf = Integer.valueOf(thumbnailPixelSize);
                    int i = 5;
                    if (r22.A01 == null) {
                        i = 4;
                    }
                    AnonymousClass283.A02(context, A0c, userJid2, valueOf, valueOf, str, i, A0F);
                    r22.A0B.A03(userJid2, 21, str, 2);
                }
            }
        };
    }

    @Override // X.AbstractC72383eV
    public boolean AIA(UserJid userJid) {
        return this.A0A.A0H(userJid);
    }

    @Override // X.AbstractC72383eV
    public void AIo(UserJid userJid) {
        if (this.A01 != null) {
            AnonymousClass36H r2 = this.A0D.A0I;
            Context context = this.A04;
            r2.setTitle(context.getString(R.string.carousel_from_product_message_title));
            r2.setTitleTextColor(AnonymousClass00T.A00(context, R.color.catalog_detail_description_color));
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.product_detail_container_padding_horizontal);
            r2.A07(dimensionPixelSize, dimensionPixelSize);
        }
        AnonymousClass36H r1 = this.A0D.A0I;
        r1.setSeeMoreClickListener(new AbstractC116275Uu(userJid) { // from class: X.3aZ
            public final /* synthetic */ UserJid A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC116275Uu
            public final void AO9() {
                AnonymousClass3VO r6 = AnonymousClass3VO.this;
                UserJid userJid2 = this.A01;
                AnonymousClass5RI r0 = r6.A0D.A0B;
                if (r0 != null) {
                    AnonymousClass2VA.A01(((C1099753t) r0).A00, 6);
                }
                AnonymousClass12P r4 = r6.A05;
                Context context2 = r6.A04;
                int i = 9;
                if (r6.A0H) {
                    i = 13;
                }
                r4.A06(context2, C14960mK.A0M(context2, userJid2, null, i));
                r6.A0B.A03(userJid2, 22, null, 3);
            }
        });
        r1.setCatalogBrandingDrawable(null);
    }

    @Override // X.AbstractC13960kc
    public void AQH(UserJid userJid, int i) {
        CatalogMediaCard catalogMediaCard = this.A0D;
        if (C29941Vi.A00(catalogMediaCard.A0G, userJid) && !this.A0A.A0J(catalogMediaCard.A0G)) {
            Log.w(C12960it.A0W(i, "CatalogMediaCard/requestCatalogProductsFromBeginning/FetchFailed/Error: "));
            int i2 = R.string.catalog_hidden;
            if (i != 406) {
                i2 = R.string.catalog_error_no_products;
                if (i != 404) {
                    i2 = R.string.catalog_server_error_retrieving_products;
                    if (i == -1) {
                        i2 = R.string.catalog_error_retrieving_products;
                    }
                }
            }
            catalogMediaCard.setError(i2);
        }
    }

    @Override // X.AbstractC13960kc
    public void AQI(UserJid userJid, boolean z, boolean z2) {
        if (C29941Vi.A00(this.A0D.A0G, userJid)) {
            AQR(userJid);
        }
    }

    @Override // X.AbstractC72383eV
    public void AQR(UserJid userJid) {
        C19850um r6 = this.A0A;
        int A00 = r6.A00(userJid);
        CatalogMediaCard catalogMediaCard = this.A0D;
        if (A00 != catalogMediaCard.A00) {
            catalogMediaCard.A00 = A00;
            boolean A0J = r6.A0J(userJid);
            C30141Wg r1 = this.A00;
            if (!A0J) {
                if (r1 != null && r1.A0I) {
                    C30151Wh r0 = new C30151Wh(r1);
                    r0.A0G = false;
                    this.A00 = r0.A00();
                    this.A0G.Ab2(new RunnableBRunnable0Shape10S0200000_I1(this, 43, userJid));
                }
                Log.w("CatalogMediaCard/onFetchCatalogSuccess/Error: no products");
                catalogMediaCard.A0I.setError(this.A04.getString(R.string.catalog_error_no_products));
                A00();
            } else {
                if (r1 != null && !r1.A0I) {
                    C30151Wh r02 = new C30151Wh(r1);
                    r02.A0G = true;
                    this.A00 = r02.A00();
                    this.A0G.Ab2(new RunnableBRunnable0Shape10S0200000_I1(this, 44, userJid));
                }
                List A01 = catalogMediaCard.A01(userJid, this.A04.getString(R.string.business_product_catalog_image_description), r6.A08(userJid), this.A0H);
                if (A01.isEmpty()) {
                    A00();
                }
                catalogMediaCard.A0I.A09(A01, 5);
            }
            C30141Wg r03 = this.A00;
            if (r03 == null || r03.A0I || r6.A0J(userJid)) {
                catalogMediaCard.setVisibility(0);
            } else {
                catalogMediaCard.setVisibility(8);
            }
            if (!this.A02) {
                this.A02 = true;
                this.A0B.A03(userJid, 20, null, 1);
            }
        }
    }

    @Override // X.AbstractC72383eV
    public boolean AdS() {
        C30141Wg r0 = this.A00;
        return r0 == null || !r0.A0I;
    }
}
