package X;

import android.view.MotionEvent;
import android.view.ViewGroup;

/* renamed from: X.2TX  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass2TX {
    int ABl(int i);

    int ADH();

    long ADI(int i);

    void ANF(AnonymousClass03U v, int i);

    AnonymousClass03U AOh(ViewGroup viewGroup);

    boolean AWm(MotionEvent motionEvent, AnonymousClass03U v, int i);
}
