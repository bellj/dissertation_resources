package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0PH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PH {
    public static int A03;
    public AbstractC07280Xj A00;
    public AbstractC07280Xj A01 = null;
    public ArrayList A02 = new ArrayList();

    public AnonymousClass0PH(AbstractC07280Xj r2) {
        A03++;
        this.A00 = r2;
        this.A01 = r2;
    }

    public final long A00(C07270Xi r10, long j) {
        AbstractC07280Xj r4 = r10.A06;
        if (r4 instanceof AnonymousClass0D0) {
            return j;
        }
        List list = r10.A07;
        int size = list.size();
        long j2 = j;
        for (int i = 0; i < size; i++) {
            AbstractC11720gk r8 = (AbstractC11720gk) list.get(i);
            if (r8 instanceof C07270Xi) {
                C07270Xi r82 = (C07270Xi) r8;
                if (r82.A06 != r4) {
                    j2 = Math.min(j2, A00(r82, ((long) r82.A00) + j));
                }
            }
        }
        if (r10 != r4.A04) {
            return j2;
        }
        long A05 = r4.A05();
        C07270Xi r42 = r4.A05;
        long j3 = j - A05;
        return Math.min(Math.min(j2, A00(r42, j3)), j3 - ((long) r42.A00));
    }

    public final long A01(C07270Xi r10, long j) {
        AbstractC07280Xj r4 = r10.A06;
        if (r4 instanceof AnonymousClass0D0) {
            return j;
        }
        List list = r10.A07;
        int size = list.size();
        long j2 = j;
        for (int i = 0; i < size; i++) {
            AbstractC11720gk r8 = (AbstractC11720gk) list.get(i);
            if (r8 instanceof C07270Xi) {
                C07270Xi r82 = (C07270Xi) r8;
                if (r82.A06 != r4) {
                    j2 = Math.max(j2, A01(r82, ((long) r82.A00) + j));
                }
            }
        }
        if (r10 != r4.A05) {
            return j2;
        }
        long A05 = r4.A05();
        C07270Xi r42 = r4.A04;
        long j3 = j + A05;
        return Math.max(Math.max(j2, A01(r42, j3)), j3 - ((long) r42.A00));
    }
}
