package X;

import android.widget.ImageView;

/* renamed from: X.1qH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39651qH extends AnonymousClass1qE {
    public final ImageView A00;
    public final AbstractC39661qJ A01;

    public C39651qH(ImageView imageView, AnonymousClass1KS r10, AbstractC39661qJ r11, String str, int i, int i2, int i3, boolean z, boolean z2) {
        super(r10, str, i, i2, i3, z, z2);
        this.A00 = imageView;
        this.A01 = r11;
    }
}
