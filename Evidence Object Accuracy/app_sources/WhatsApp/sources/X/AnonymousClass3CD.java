package X;

import android.os.Bundle;

/* renamed from: X.3CD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CD {
    public final Bundle A00;

    public AnonymousClass3CD(Bundle bundle) {
        if (bundle != null) {
            C65273Iw.A03("verifier", bundle);
            this.A00 = bundle;
            return;
        }
        throw C12970iu.A0f("Bundle is null");
    }

    public byte[] A00() {
        Bundle bundle = this.A00;
        if (bundle.containsKey("verifier")) {
            return bundle.getByteArray("verifier");
        }
        throw new IllegalStateException();
    }
}
