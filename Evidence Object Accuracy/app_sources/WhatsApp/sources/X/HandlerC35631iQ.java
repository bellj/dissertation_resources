package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.whatsapp.status.playback.MyStatusesActivity;
import java.util.ArrayList;
import org.chromium.net.UrlRequest;

/* renamed from: X.1iQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC35631iQ extends Handler {
    public final /* synthetic */ C21320xE A00;
    public final /* synthetic */ C21380xK A01;
    public final /* synthetic */ AnonymousClass12H A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC35631iQ(Looper looper, C21320xE r2, C21380xK r3, AnonymousClass12H r4) {
        super(looper);
        this.A01 = r3;
        this.A02 = r4;
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AbstractC15340mz r3;
        AbstractC15340mz r4;
        int i = message.what;
        Object obj = message.obj;
        if (i == 11) {
            Pair pair = (Pair) obj;
            r3 = (AbstractC15340mz) pair.first;
            r4 = (AbstractC15340mz) pair.second;
        } else {
            r3 = (AbstractC15340mz) obj;
            r4 = null;
            if (i != 2) {
                switch (i) {
                    case 10:
                        AnonymousClass12H r5 = this.A02;
                        for (AbstractC18860tB r42 : r5.A01()) {
                            r5.A00++;
                            if (r42 instanceof C35671iU) {
                                C35671iU r43 = (C35671iU) r42;
                                if (r3 != null) {
                                    AnonymousClass1IS r2 = r3.A0z;
                                    String str = r2.A01;
                                    C35681iV r44 = r43.A00;
                                    AbstractC15340mz r0 = ((AbstractC35561iG) r44).A09;
                                    AnonymousClass009.A05(r0);
                                    if (str.equals(r0.A0z.A01) && r2.A02) {
                                        C35691iW r1 = r44.A00;
                                        if (r1 != null) {
                                            r1.A03(true);
                                        }
                                        C35691iW r22 = new C35691iW(r44);
                                        r44.A00 = r22;
                                        r44.A0R.Aaz(r22, new Void[0]);
                                    }
                                }
                            } else if (r42 instanceof C35281hZ) {
                                C35281hZ r45 = (C35281hZ) r42;
                                if (r3 != null) {
                                    AnonymousClass1IS r12 = r3.A0z;
                                    if (C15380n4.A0N(r12.A00) && r12.A02) {
                                        MyStatusesActivity.A02(r3, r45.A00, true);
                                    }
                                }
                            } else if (r42 instanceof C35181hO) {
                                ((C35181hO) r42).A03(r3);
                            } else if (r42 instanceof C35351hg) {
                                ((C35351hg) r42).A00.A2o();
                            }
                        }
                        return;
                    case 11:
                        break;
                    case 12:
                        C21320xE r13 = this.A00;
                        AbstractC14640lm r46 = r3.A0z.A00;
                        AnonymousClass009.A05(r46);
                        AnonymousClass009.A05(r46);
                        for (C27151Gf r14 : r13.A01()) {
                            if (r14 instanceof C27391Hf) {
                                ((C27391Hf) r14).A01.A04(r46, 9, 0, 0);
                            }
                        }
                        return;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        C21320xE r15 = this.A00;
                        AbstractC14640lm r32 = r3.A0z.A00;
                        AnonymousClass009.A05(r32);
                        AnonymousClass009.A05(r32);
                        for (C27151Gf r16 : r15.A01()) {
                            if (r16 instanceof C27391Hf) {
                                ((C27391Hf) r16).A00.A03(r32);
                            }
                        }
                        return;
                    default:
                        return;
                }
            } else {
                this.A01.A01(r3, message.arg1);
                return;
            }
        }
        AnonymousClass12H r52 = this.A02;
        if (r4 != null) {
            for (AbstractC18860tB r7 : r52.A01()) {
                r52.A00++;
                if (r7 instanceof C22520zD) {
                    C22520zD r72 = (C22520zD) r7;
                    if (r3 != null && r4.A0y == 11) {
                        C22470z8 r11 = r72.A0h;
                        if (C30051Vw.A17(r3)) {
                            if (r11.A04.A02() && r11.A03()) {
                                ArrayList arrayList = new ArrayList(1);
                                arrayList.add(r3);
                                r11.A00(null, null, null, null, arrayList, null, 4, 4, false, false);
                            }
                            r72.A0I.A01(new RunnableBRunnable0Shape0S0200000_I0(r72, 15, r3), 11);
                        } else {
                            throw new IllegalArgumentException("message thumb not loaded");
                        }
                    }
                    r72.A0T.A09(r4, 3);
                }
            }
        }
    }
}
