package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewGroup;
import com.whatsapp.HomeActivity;

/* renamed from: X.2NB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NB extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass2LG A00;

    public AnonymousClass2NB(AnonymousClass2LG r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        HomeActivity homeActivity = this.A00.A00;
        ViewGroup viewGroup = homeActivity.A0F;
        if (viewGroup != null) {
            viewGroup.removeOnLayoutChangeListener(homeActivity.A1s);
            homeActivity.A06.removeAllListeners();
            homeActivity.A06 = null;
        }
    }
}
