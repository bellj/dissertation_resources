package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.53T  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass53T implements AnonymousClass1W3 {
    public final /* synthetic */ AnonymousClass3DH A00;

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
    }

    public AnonymousClass53T(AnonymousClass3DH r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e("CatalogSearchCatalogPageRequestFactory/reloadDCBusinessInfo/");
        this.A00.A00.AaV("product-search-enc-dc-refetch-failed", "", false);
    }
}
