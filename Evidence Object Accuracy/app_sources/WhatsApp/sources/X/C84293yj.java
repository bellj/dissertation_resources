package X;

/* renamed from: X.3yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84293yj extends AbstractC87994Dv {
    public final long A00;
    public final long A01;

    public C84293yj(long j, long j2) {
        this.A01 = j;
        this.A00 = j2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C84293yj r7 = (C84293yj) obj;
            if (!(this.A01 == r7.A01 && this.A00 == r7.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = Long.valueOf(this.A01);
        return C12960it.A06(Long.valueOf(this.A00), A1a);
    }
}
