package X;

import android.app.Activity;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiQrCodeUrlValidationActivity;

/* renamed from: X.5bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118435bo extends AnonymousClass0Yo {
    public final /* synthetic */ IndiaUpiQrCodeUrlValidationActivity A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public C118435bo(IndiaUpiQrCodeUrlValidationActivity indiaUpiQrCodeUrlValidationActivity, String str, String str2) {
        this.A00 = indiaUpiQrCodeUrlValidationActivity;
        this.A02 = str;
        this.A01 = str2;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C1310661b r0;
        if (cls.isAssignableFrom(C118135bK.class)) {
            IndiaUpiQrCodeUrlValidationActivity indiaUpiQrCodeUrlValidationActivity = this.A00;
            C118135bK r11 = new C118135bK(((ActivityC13810kN) indiaUpiQrCodeUrlValidationActivity).A06, indiaUpiQrCodeUrlValidationActivity.A00, indiaUpiQrCodeUrlValidationActivity.A03, indiaUpiQrCodeUrlValidationActivity.A04);
            IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(this, 61);
            C27691It r1 = r11.A01;
            r1.A05(indiaUpiQrCodeUrlValidationActivity, A0B);
            String str = this.A02;
            String str2 = this.A01;
            if (TextUtils.isEmpty(str)) {
                C127945vJ.A00(r1, 0);
                return r11;
            }
            r11.A02 = str2;
            AnonymousClass016 r12 = r11.A00;
            if (str == null || (r0 = C1310661b.A00(Uri.parse(str), str2)) == null) {
                r0 = null;
            } else {
                r0.A03 = str;
            }
            r12.A0B(r0);
            C1309360o r13 = r11.A07;
            AnonymousClass1ZR A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, r11.A04().A0C, "upiHandle");
            C126145sP r8 = new C126145sP(r11);
            C128255vo r14 = r13.A01;
            new C120485gG(r14.A01.A00, r14.A00, r14.A02, r14.A03, r14.A04, r14.A05, r14.A06, r14.A07, r14.A08, r14.A09).A00(A0I, null, new AnonymousClass6Ln(indiaUpiQrCodeUrlValidationActivity, A0I, r13, r8) { // from class: X.695
                public final /* synthetic */ Activity A00;
                public final /* synthetic */ AnonymousClass1ZR A01;
                public final /* synthetic */ C1309360o A02;
                public final /* synthetic */ C126145sP A03;

                {
                    this.A02 = r3;
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass6Ln
                public final void AVN(UserJid userJid, AnonymousClass1ZR r122, AnonymousClass1ZR r132, AnonymousClass1ZR r142, C452120p r15, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
                    String str5;
                    C1309360o r02 = this.A02;
                    C126145sP r2 = this.A03;
                    Activity activity = this.A00;
                    AnonymousClass1ZR r7 = this.A01;
                    if (!z || r15 != null) {
                        C127945vJ r16 = new C127945vJ(1);
                        r16.A01 = r15;
                        r2.A00.A01.A0B(r16);
                    } else if (z3) {
                        r02.A00.A00(activity, 
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: INVOKE  
                              (wrap: X.18S : 0x0010: IGET  (r3v1 X.18S A[REMOVE]) = (r0v0 'r02' X.60o) X.60o.A00 X.18S)
                              (r4v0 'activity' android.app.Activity)
                              (wrap: X.66w : 0x0016: CONSTRUCTOR  (r5v1 X.66w A[REMOVE]) = (r2v0 'r2' X.5sP), (r19v0 'z2' boolean) call: X.66w.<init>(X.5sP, boolean):void type: CONSTRUCTOR)
                              (r11v0 'userJid' com.whatsapp.jid.UserJid)
                              (r7v0 'r7' X.1ZR)
                              true
                              false
                             type: VIRTUAL call: X.18S.A00(android.app.Activity, X.1P3, com.whatsapp.jid.UserJid, X.1ZR, boolean, boolean):void in method: X.695.AVN(com.whatsapp.jid.UserJid, X.1ZR, X.1ZR, X.1ZR, X.20p, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, boolean):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0016: CONSTRUCTOR  (r5v1 X.66w A[REMOVE]) = (r2v0 'r2' X.5sP), (r19v0 'z2' boolean) call: X.66w.<init>(X.5sP, boolean):void type: CONSTRUCTOR in method: X.695.AVN(com.whatsapp.jid.UserJid, X.1ZR, X.1ZR, X.1ZR, X.20p, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, boolean):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 23 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.66w, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 29 more
                            */
                        /*
                            this = this;
                            X.60o r0 = r10.A02
                            X.5sP r2 = r10.A03
                            android.app.Activity r4 = r10.A00
                            X.1ZR r7 = r10.A01
                            if (r18 == 0) goto L_0x0089
                            if (r15 != 0) goto L_0x0089
                            r1 = r19
                            if (r20 == 0) goto L_0x001e
                            X.18S r3 = r0.A00
                            r8 = 1
                            r9 = 0
                            X.66w r5 = new X.66w
                            r5.<init>(r2, r1)
                            r6 = r11
                            r3.A00(r4, r5, r6, r7, r8, r9)
                            return
                        L_0x001e:
                            X.5bK r5 = r2.A00
                            X.61b r2 = r5.A04()
                            r2.A0D = r1
                            java.lang.Object r0 = X.C117295Zj.A0R(r12)
                            java.lang.String r0 = (java.lang.String) r0
                            r2.A04 = r0
                            java.lang.String r0 = r2.A05
                            boolean r0 = android.text.TextUtils.isEmpty(r0)
                            if (r0 != 0) goto L_0x0080
                            boolean r0 = r2.A0D
                            if (r0 != 0) goto L_0x0080
                            java.lang.String r1 = r2.A00
                            if (r1 == 0) goto L_0x0080
                            java.lang.String r0 = "DEEP_LINK"
                            boolean r0 = r1.contentEquals(r0)
                            if (r0 != 0) goto L_0x004e
                            java.lang.String r0 = "GALLERY_QR_CODE"
                            boolean r0 = r1.contentEquals(r0)
                            if (r0 == 0) goto L_0x0080
                        L_0x004e:
                            java.lang.String r0 = r2.A05
                            X.1Yv r4 = X.C30771Yt.A05
                            X.1Yy r1 = X.C117305Zk.A0E(r4, r0)
                            X.0nH r0 = r5.A04
                            int r0 = X.AnonymousClass614.A00(r0, r2)
                            java.math.BigDecimal r3 = new java.math.BigDecimal
                            r3.<init>(r0)
                            if (r1 == 0) goto L_0x0080
                            java.math.BigDecimal r0 = r1.A00
                            int r0 = r0.compareTo(r3)
                            if (r0 <= 0) goto L_0x0080
                            r0 = 7
                            X.5vJ r2 = new X.5vJ
                            r2.<init>(r0)
                            X.018 r1 = r5.A05
                            r0 = 0
                            java.lang.String r0 = r4.AAB(r1, r3, r0)
                            r2.A02 = r0
                            X.1It r0 = r5.A01
                            r0.A0B(r2)
                            return
                        L_0x0080:
                            X.016 r0 = r5.A00
                            r0.A0B(r2)
                            X.C118135bK.A00(r5)
                            return
                        L_0x0089:
                            r0 = 1
                            X.5vJ r1 = new X.5vJ
                            r1.<init>(r0)
                            r1.A01 = r15
                            X.5bK r0 = r2.A00
                            X.1It r0 = r0.A01
                            r0.A0B(r1)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass695.AVN(com.whatsapp.jid.UserJid, X.1ZR, X.1ZR, X.1ZR, X.20p, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, boolean):void");
                    }
                });
                return r11;
            }
            throw C12970iu.A0f("Invalid viewModel");
        }
    }
