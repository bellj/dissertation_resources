package X;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.R;
import com.whatsapp.identity.IdentityVerificationActivity;
import com.whatsapp.qrcode.WaQrScannerView;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/* renamed from: X.3Zh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69453Zh implements AnonymousClass27Z {
    public final /* synthetic */ IdentityVerificationActivity A00;

    public C69453Zh(IdentityVerificationActivity identityVerificationActivity) {
        this.A00 = identityVerificationActivity;
    }

    @Override // X.AnonymousClass27Z
    public void ANb(int i) {
        C14900mE r1;
        int i2;
        Log.e("idverification/cameraerror");
        IdentityVerificationActivity identityVerificationActivity = this.A00;
        if (identityVerificationActivity.A0S.A03()) {
            r1 = ((ActivityC13810kN) identityVerificationActivity).A05;
            i2 = R.string.error_camera_disabled_during_video_call;
        } else {
            if (i != 2) {
                r1 = ((ActivityC13810kN) identityVerificationActivity).A05;
                i2 = R.string.cannot_start_camera;
            }
            identityVerificationActivity.A2l(null);
        }
        r1.A07(i2, 1);
        identityVerificationActivity.A2l(null);
    }

    @Override // X.AnonymousClass27Z
    public void AUF() {
        IdentityVerificationActivity identityVerificationActivity = this.A00;
        WaQrScannerView waQrScannerView = identityVerificationActivity.A0Q;
        if (waQrScannerView != null && waQrScannerView.getVisibility() == 0) {
            if (identityVerificationActivity.findViewById(R.id.main_layout).getVisibility() != 8) {
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                identityVerificationActivity.A04 = translateAnimation;
                translateAnimation.setInterpolator(new AccelerateInterpolator());
                identityVerificationActivity.A04.setDuration((long) identityVerificationActivity.getResources().getInteger(17694721));
                identityVerificationActivity.findViewById(R.id.main_layout).startAnimation(identityVerificationActivity.A04);
                C12970iu.A1N(identityVerificationActivity, R.id.scan_code, 8);
                C12970iu.A1N(identityVerificationActivity, R.id.result, 8);
                identityVerificationActivity.A04.setAnimationListener(new C83253ww(this));
                C12970iu.A1N(identityVerificationActivity, R.id.main_layout, 8);
            }
            AlphaAnimation A0J = C12970iu.A0J();
            A0J.setDuration((long) identityVerificationActivity.getResources().getInteger(17694720));
            identityVerificationActivity.findViewById(R.id.verify_identity_qr_tip).startAnimation(A0J);
            identityVerificationActivity.findViewById(R.id.verify_identity_qr_tip).setVisibility(0);
        }
    }

    @Override // X.AnonymousClass27Z
    public void AUS(C49262Kb r8) {
        TextView textView;
        int i;
        int i2;
        IdentityVerificationActivity identityVerificationActivity = this.A00;
        Animation animation = identityVerificationActivity.A04;
        if (animation == null || animation.hasEnded()) {
            String str = r8.A02;
            if (str != null) {
                try {
                    byte[] bytes = str.getBytes("ISO-8859-1");
                    int A2e = identityVerificationActivity.A2e(bytes);
                    Arrays.toString(bytes);
                    String A01 = C15610nY.A01(identityVerificationActivity.A0E, identityVerificationActivity.A0J);
                    if (A2e != -4) {
                        if (A2e == -3) {
                            textView = identityVerificationActivity.A07;
                            i = R.string.verify_identity_result_wrong_you;
                        } else if (A2e != -2) {
                            if (A2e == 1) {
                                i2 = 8;
                            } else if (A2e == 2) {
                                i2 = 12;
                            } else {
                                return;
                            }
                            identityVerificationActivity.A2l(new RunnableBRunnable0Shape7S0100000_I0_7(identityVerificationActivity, i2));
                            return;
                        } else {
                            textView = identityVerificationActivity.A07;
                            i = R.string.verify_identity_result_wrong_contact;
                        }
                        textView.setText(C12960it.A0X(identityVerificationActivity, A01, new Object[1], 0, i));
                        identityVerificationActivity.A07.setVisibility(0);
                    }
                } catch (UnsupportedEncodingException unused) {
                    identityVerificationActivity.A0Q.Aab();
                    return;
                }
            }
            identityVerificationActivity.A0Q.Aab();
        }
    }
}
