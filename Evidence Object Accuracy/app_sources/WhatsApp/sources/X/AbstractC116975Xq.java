package X;

/* renamed from: X.5Xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116975Xq {
    public static final AnonymousClass1TK A00 = C72453ed.A13("2.5.4.3");
    public static final AnonymousClass1TK A01 = C72453ed.A13("2.5.4.6");
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03 = C72453ed.A13("1.3.14.3.2.26");
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07 = C72453ed.A13("2.5.4.41");
    public static final AnonymousClass1TK A08 = C72453ed.A13("2.5.4.97");
    public static final AnonymousClass1TK A09 = C72453ed.A13("2.5.4.20");
    public static final AnonymousClass1TK A0A = C72453ed.A12("2.5.29");
    public static final AnonymousClass1TK A0B = C72453ed.A13("2.5.8.1.1");
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I = C72453ed.A13("2.5.4.7");
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K = C72453ed.A13("2.5.4.10");
    public static final AnonymousClass1TK A0L = C72453ed.A13("2.5.4.11");
    public static final AnonymousClass1TK A0M = C72453ed.A13("1.3.36.3.2.1");
    public static final AnonymousClass1TK A0N = C72453ed.A13("1.3.36.3.3.1.2");
    public static final AnonymousClass1TK A0O = C72453ed.A13("2.5.4.8");

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.3.6.1.5.5.7");
        A0F = A12;
        A0G = AnonymousClass1TL.A02("6.30", A12);
        A0H = AnonymousClass1TL.A02("6.31", A12);
        A0C = AnonymousClass1TL.A02("6.32", A12);
        A0D = AnonymousClass1TL.A02("6.33", A12);
        A0E = AnonymousClass1TL.A02("1", A12);
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("48", A12);
        A04 = A022;
        AnonymousClass1TK A0B2 = AnonymousClass1TL.A02("2", A022).A0B();
        A05 = A0B2;
        AnonymousClass1TK A0B3 = AnonymousClass1TL.A02("1", A022).A0B();
        A06 = A0B3;
        A0J = A0B3;
        A02 = A0B2;
    }
}
