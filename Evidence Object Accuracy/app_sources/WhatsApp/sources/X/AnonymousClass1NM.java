package X;

import java.io.File;
import java.io.FileInputStream;

/* renamed from: X.1NM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1NM {
    public static byte[] A00(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            byte[] A04 = AnonymousClass1P1.A04(fileInputStream);
            fileInputStream.close();
            return A04;
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
