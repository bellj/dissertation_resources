package X;

import android.database.DataSetObserver;
import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;
import java.util.List;

/* renamed from: X.5Zt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117395Zt extends DataSetObserver {
    public final /* synthetic */ NoviWithdrawCashStoreLocatorActivity A00;

    public C117395Zt(NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity) {
        this.A00 = noviWithdrawCashStoreLocatorActivity;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        List list;
        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = this.A00;
        C52812bj r0 = noviWithdrawCashStoreLocatorActivity.A0D;
        if (r0 != null && (list = r0.A01) != null && !list.isEmpty()) {
            AnonymousClass60Y.A02(noviWithdrawCashStoreLocatorActivity.A0I, "LOCATION_BRANDS_LIST_LOADED", "WITHDRAW_MONEY", "SELECT_LOCATION", "SCREEN");
        }
    }
}
