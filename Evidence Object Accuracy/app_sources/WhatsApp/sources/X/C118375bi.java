package X;

import com.whatsapp.payments.ui.NoviSharedPaymentActivity;

/* renamed from: X.5bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118375bi extends AnonymousClass0Yo {
    public final /* synthetic */ NoviSharedPaymentActivity A00;
    public final /* synthetic */ C128375w0 A01;

    public C118375bi(NoviSharedPaymentActivity noviSharedPaymentActivity, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = noviSharedPaymentActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118145bL.class)) {
            NoviSharedPaymentActivity noviSharedPaymentActivity = this.A00;
            C128375w0 r0 = this.A01;
            C14830m7 r1 = r0.A0A;
            C14850m9 r12 = r0.A0J;
            C14900mE r13 = r0.A02;
            AbstractC14440lR r14 = r0.A0z;
            C129675y7 r15 = r0.A0p;
            AnonymousClass12P r16 = r0.A01;
            C125685re r17 = r0.A0Z;
            C15610nY r18 = r0.A08;
            AnonymousClass018 r19 = r0.A0C;
            C20320vZ r110 = r0.A0x;
            AnonymousClass60Y r111 = r0.A0c;
            C17070qD r112 = r0.A0V;
            C20920wX r113 = r0.A00;
            C15650ng r114 = r0.A0E;
            C130155yt r115 = r0.A0W;
            AnonymousClass61F r152 = r0.A0d;
            C129685y8 r142 = r0.A0l;
            C22120yY r132 = r0.A0w;
            C129175xI r122 = r0.A0r;
            C130125yq r11 = r0.A0a;
            C18610sj r10 = r0.A0R;
            C130105yo r9 = r0.A0h;
            C127875vC r8 = r0.A0i;
            AnonymousClass102 r7 = r0.A0H;
            C17900ra r6 = r0.A0T;
            C248217a r5 = r0.A0M;
            AnonymousClass61P r4 = r0.A0q;
            return new C118145bL(r113, r16, r13, noviSharedPaymentActivity, r18, r1, r19, r0.A0D, r114, r7, r12, r5, r10, r0.A0S, r6, r112, r115, r0.A0Y, r17, r11, r111, r152, r9, r8, r142, r0.A0o, r15, r4, r122, r132, r110, r14);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviSharedPaymentViewModel");
    }
}
