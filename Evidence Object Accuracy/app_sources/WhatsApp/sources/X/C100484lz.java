package X;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import org.chromium.net.UrlRequest;

/* renamed from: X.4lz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100484lz implements Parcelable.Creator {
    public static void A00(Parcel parcel, C78593pA r5, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, r5.A0C);
        C95654e8.A07(parcel, 2, r5.A0D);
        C95654e8.A07(parcel, 3, r5.A00);
        C95654e8.A0D(parcel, r5.A05, 4, false);
        C95654e8.A04(r5.A04, parcel, 5);
        C95654e8.A0I(parcel, r5.A0B, 6, i);
        C95654e8.A03(r5.A03, parcel, 7);
        C95654e8.A0B(parcel, r5.A02, 8, i, false);
        C95654e8.A0I(parcel, r5.A09, 10, i);
        C95654e8.A0I(parcel, r5.A0A, 11, i);
        C95654e8.A09(parcel, 12, r5.A07);
        C95654e8.A07(parcel, 13, r5.A01);
        C95654e8.A09(parcel, 14, r5.A08);
        C95654e8.A0D(parcel, r5.A06, 15, false);
        C95654e8.A06(parcel, A00);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        C78603pB[] r9 = null;
        C78603pB[] r10 = null;
        String str2 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    i3 = C95664e9.A02(parcel, readInt);
                    break;
                case 4:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 5:
                    iBinder = C95664e9.A06(parcel, readInt);
                    break;
                case 6:
                    scopeArr = (Scope[]) C95664e9.A0K(parcel, Scope.CREATOR, readInt);
                    break;
                case 7:
                    bundle = C95664e9.A05(parcel, readInt);
                    break;
                case '\b':
                    account = (Account) C95664e9.A07(parcel, Account.CREATOR, readInt);
                    break;
                case '\t':
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
                case '\n':
                    r9 = (C78603pB[]) C95664e9.A0K(parcel, C78603pB.CREATOR, readInt);
                    break;
                case 11:
                    r10 = (C78603pB[]) C95664e9.A0K(parcel, C78603pB.CREATOR, readInt);
                    break;
                case '\f':
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    i4 = C95664e9.A02(parcel, readInt);
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 15:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78593pA(account, bundle, iBinder, str, str2, r9, r10, scopeArr, i, i2, i3, i4, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78593pA[i];
    }
}
