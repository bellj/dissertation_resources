package X;

import com.whatsapp.R;
import java.util.List;

/* renamed from: X.1cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32721cd {
    public static String A00(AnonymousClass018 r9, List list, boolean z) {
        int i;
        Object[] objArr;
        Object obj;
        int i2;
        Object[] objArr2;
        Object obj2;
        int size = list.size();
        if (size == 0) {
            return "";
        }
        if (size == 1) {
            return r9.A0F((String) list.get(0));
        }
        if (size != 2) {
            String A0A = r9.A0A(235, r9.A0F((String) list.get(0)), r9.A0F((String) list.get(1)));
            for (int i3 = 2; i3 < list.size() - 1; i3++) {
                A0A = r9.A0A(234, A0A, r9.A0F((String) list.get(i3)));
            }
            if (z) {
                i2 = 233;
                objArr2 = new Object[2];
                objArr2[0] = A0A;
                obj2 = list.get(size - 1);
                objArr2[1] = r9.A0F((String) obj2);
                return r9.A0A(i2, objArr2);
            }
            i = R.string.list_end_short;
            objArr = new Object[2];
            objArr[0] = A0A;
            obj = list.get(size - 1);
            objArr[1] = r9.A0F((String) obj);
            return r9.A0B(i, objArr);
        } else if (z) {
            i2 = 236;
            objArr2 = new Object[2];
            objArr2[0] = r9.A0F((String) list.get(0));
            obj2 = list.get(1);
            objArr2[1] = r9.A0F((String) obj2);
            return r9.A0A(i2, objArr2);
        } else {
            i = R.string.list_two_items_short;
            objArr = new Object[2];
            objArr[0] = r9.A0F((String) list.get(0));
            obj = list.get(1);
            objArr[1] = r9.A0F((String) obj);
            return r9.A0B(i, objArr);
        }
    }
}
