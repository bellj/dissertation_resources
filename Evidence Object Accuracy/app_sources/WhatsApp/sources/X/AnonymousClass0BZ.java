package X;

import android.animation.Animator;
import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import com.facebook.rendercore.RootHostView;

/* renamed from: X.0BZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BZ extends FrameLayout {
    public int A00;
    public int A01;
    public int A02;
    public Interpolator A03;
    public Interpolator A04;
    public AbstractC12100hN A05;
    public AbstractC12110hO A06;
    public RootHostView A07;
    public C64893Hi A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final Animator.AnimatorListener A0C = new AnonymousClass09C(this);
    public final Animator.AnimatorListener A0D = new AnonymousClass09B(this);
    public final GestureDetector.OnGestureListener A0E;
    public final GestureDetector A0F;
    public final Runnable A0G = new RunnableC09430ct(this);

    public AnonymousClass0BZ(Context context, boolean z) {
        super(context, null);
        GestureDetector.OnGestureListener r1 = new AnonymousClass0Vz(this);
        this.A0E = r1;
        this.A0B = z;
        this.A0F = new GestureDetector(context, !z ? new C02300Au(this) : r1);
    }

    public final void A00() {
        if (this.A00 != 0 && !this.A09) {
            Runnable runnable = this.A0G;
            removeCallbacks(runnable);
            postDelayed(runnable, (long) this.A00);
        }
    }

    public void A01(int i) {
        ViewPropertyAnimator alpha;
        removeCallbacks(this.A0G);
        this.A09 = true;
        if (getContext() == null || !AnonymousClass028.A0q(this)) {
            AbstractC12100hN r0 = this.A05;
            if (r0 != null) {
                r0.API();
                return;
            }
            return;
        }
        clearAnimation();
        boolean z = this.A0B;
        clearAnimation();
        ViewPropertyAnimator duration = animate().setDuration((long) i);
        if (z) {
            alpha = duration.setInterpolator(this.A03).alpha(0.0f).scaleX(1.5f).scaleY(1.5f);
        } else {
            alpha = duration.translationY((float) getHeight()).setInterpolator(this.A03).alpha(0.0f);
        }
        alpha.setListener(this.A0C);
    }

    public void A02(Animator.AnimatorListener animatorListener, int i) {
        clearAnimation();
        animate().setDuration((long) i).translationY(0.0f).setInterpolator(this.A04).alpha(1.0f).setListener(animatorListener);
    }

    public final boolean A03() {
        if (!this.A0A) {
            if (getTranslationY() > ((float) getHeight()) / 2.0f) {
                A01(this.A01);
            } else {
                A02(null, this.A02);
                return true;
            }
        }
        return true;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C64893Hi r0 = this.A08;
        if (r0 != null) {
            r0.A03();
            this.A08 = null;
            this.A07 = null;
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            removeCallbacks(this.A0G);
        } else if (action == 1 || action == 3) {
            A00();
            A03();
        }
        return this.A0F.onTouchEvent(motionEvent);
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = this.A0F.onTouchEvent(motionEvent);
        int action = motionEvent.getAction();
        if (action == 0) {
            this.A0A = false;
            return true;
        } else if (action == 1 || action == 3) {
            A00();
            return A03();
        } else if (onTouchEvent || super.onTouchEvent(motionEvent)) {
            return true;
        } else {
            return false;
        }
    }

    public void setAutoDismissDurationMs(int i) {
        this.A00 = i;
    }

    public void setBloksContentView(C14260l7 r4, AnonymousClass3JI r5) {
        C64893Hi r0 = this.A08;
        if (r0 != null) {
            r0.A03();
            this.A08 = null;
        }
        RootHostView rootHostView = this.A07;
        if (rootHostView != null) {
            removeView(rootHostView);
        }
        Context A00 = r4.A00();
        if (!A00.equals(getContext())) {
            C28691Op.A00("bk.action.toast.ShowToast", "Different Android context for BloksHostingComponent and FoABloksPopoverView");
        }
        this.A07 = new RootHostView(A00);
        C64893Hi A002 = C64893Hi.A00(A00, r5, r4.A01()).A00();
        this.A08 = A002;
        A002.A05(this.A07);
        View A01 = this.A08.A01();
        if (A01 == null) {
            C28691Op.A00("bk.action.toast.ShowToast", "Cannot add null Bloks content view to PopoverView container.");
        } else {
            addView(A01);
        }
    }

    public void setDismissAnimationDurationMs(int i) {
        this.A01 = i;
    }

    public void setDismissAnimationEndListener(AbstractC12100hN r1) {
        this.A05 = r1;
    }

    public void setDismissAnimationInterpolator(Interpolator interpolator) {
        this.A03 = interpolator;
    }

    public void setIsLastGestureFling(boolean z) {
        this.A0A = z;
    }

    public void setShowAnimationDurationMs(int i) {
        this.A02 = i;
    }

    public void setShowAnimationEndListener(AbstractC12110hO r1) {
        this.A06 = r1;
    }

    public void setShowAnimationInterpolator(Interpolator interpolator) {
        this.A04 = interpolator;
    }
}
