package X;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

/* renamed from: X.1dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33211dd implements AbstractC33221de {
    public final File A00;

    public C33211dd(File file) {
        this.A00 = file;
    }

    @Override // X.AbstractC33221de
    public boolean A8j() {
        return this.A00.delete();
    }

    @Override // X.AbstractC33221de
    public boolean A9l() {
        return this.A00.exists();
    }

    @Override // X.AbstractC33221de
    public C32871cs ACp(C17050qB r2) {
        return r2.A00(this.A00);
    }

    @Override // X.AbstractC33221de
    public FileInputStream AD0() {
        return new FileInputStream(this.A00);
    }

    @Override // X.AbstractC33221de
    public String ADG(MessageDigest messageDigest, long j) {
        return C14350lI.A06(this.A00, messageDigest, j);
    }

    @Override // X.AbstractC33221de
    public InputStream ADW() {
        return new FileInputStream(this.A00);
    }

    @Override // X.AbstractC33221de
    public OutputStream AEq() {
        return new FileOutputStream(this.A00);
    }

    @Override // X.AbstractC33221de
    public long AKN() {
        return this.A00.lastModified();
    }

    @Override // X.AbstractC33221de
    public long AKR() {
        return this.A00.length();
    }
}
