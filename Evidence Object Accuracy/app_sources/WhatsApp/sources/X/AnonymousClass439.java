package X;

/* renamed from: X.439  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass439 extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass439() {
        super(2214, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamSettingsClick {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "settingsItem", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
