package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3d7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class ThreadFactoryC71553d7 implements ThreadFactory {
    public static final AtomicInteger A00 = new AtomicInteger();

    public /* synthetic */ ThreadFactoryC71553d7(C87574Ca r2) {
    }

    public ThreadFactoryC71553d7() {
    }

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        return new AnonymousClass5HA(runnable, C12960it.A0e("measurement-", C12980iv.A0t(23), A00.incrementAndGet()));
    }
}
