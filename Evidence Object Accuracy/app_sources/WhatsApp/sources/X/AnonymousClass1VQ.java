package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.1VQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VQ implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final C16590pI A01;
    public final C17220qS A02;
    public final AnonymousClass1JI A03;
    public volatile UserJid A04;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public AnonymousClass1VQ(AbstractC15710nm r1, C16590pI r2, C17220qS r3, AnonymousClass1JI r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A00(UserJid userJid, long j) {
        this.A04 = userJid;
        C17220qS r9 = this.A02;
        String A01 = r9.A01();
        ArrayList arrayList = new ArrayList();
        long j2 = j / 1000;
        arrayList.add(new AnonymousClass1V8("user", j2 == 0 ? new AnonymousClass1W9[]{new AnonymousClass1W9(userJid, "jid")} : new AnonymousClass1W9[]{new AnonymousClass1W9(userJid, "jid"), new AnonymousClass1W9("t", Long.toString(j2))}));
        r9.A0D(this, new AnonymousClass1V8(new AnonymousClass1V8("status", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "status"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), A01, 41, 0);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        this.A03.APn(this.A04, C41151sz.A00(r4));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        AnonymousClass1V8[] r2;
        AnonymousClass1V8 A0E = r9.A0E("status");
        if (A0E == null || (r2 = A0E.A03) == null || r2.length != 1) {
            this.A03.AT4(this.A04);
            return;
        }
        AnonymousClass1V8 r7 = r2[0];
        AnonymousClass1V8.A01(r7, "user");
        long A01 = C28421Nd.A01(r7.A0I("t", null), 0) * 1000;
        String A0I = r7.A0I("code", null);
        String A0I2 = r7.A0I("type", null);
        UserJid userJid = (UserJid) r7.A0B(this.A00, UserJid.class, "jid");
        String A0G = r7.A0G();
        if (A0I2 == null || !A0I2.equals("fail")) {
            if (TextUtils.isEmpty(A0G)) {
                A0G = this.A01.A00.getResources().getString(R.string.default_about_text);
            }
            this.A03.AWV(userJid, A0G, A01);
        } else if ("401".equals(A0I) || "403".equals(A0I) || "404".equals(A0I)) {
            this.A03.AOv(userJid);
        } else {
            this.A03.AT4(userJid);
        }
    }
}
