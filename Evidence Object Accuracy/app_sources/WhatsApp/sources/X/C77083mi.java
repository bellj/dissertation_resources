package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3mi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77083mi extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(15);
    public final String A00;
    public final String A01;
    public final String A02;
    public final byte[] A03;

    public C77083mi(Parcel parcel) {
        super("GEOB");
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
        this.A03 = parcel.createByteArray();
    }

    public C77083mi(String str, String str2, String str3, byte[] bArr) {
        super("GEOB");
        this.A02 = str;
        this.A01 = str2;
        this.A00 = str3;
        this.A03 = bArr;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77083mi.class != obj.getClass()) {
                return false;
            }
            C77083mi r5 = (C77083mi) obj;
            if (!AnonymousClass3JZ.A0H(this.A02, r5.A02) || !AnonymousClass3JZ.A0H(this.A01, r5.A01) || !AnonymousClass3JZ.A0H(this.A00, r5.A00) || !Arrays.equals(this.A03, r5.A03)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = 0;
        int A05 = (C72453ed.A05(C72453ed.A0E(this.A02)) + C72453ed.A0E(this.A01)) * 31;
        String str = this.A00;
        if (str != null) {
            i = str.hashCode();
        }
        return ((A05 + i) * 31) + Arrays.hashCode(this.A03);
    }

    @Override // X.AbstractC107404xH, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.A00);
        A0h.append(": mimeType=");
        A0h.append(this.A02);
        A0h.append(", filename=");
        A0h.append(this.A01);
        A0h.append(", description=");
        return C12960it.A0d(this.A00, A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
        parcel.writeByteArray(this.A03);
    }
}
