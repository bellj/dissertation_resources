package X;

/* renamed from: X.1lc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37171lc {
    public final AnonymousClass39o A00;

    public C37171lc(AnonymousClass39o r1) {
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((C37171lc) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.A00.ordinal();
    }
}
