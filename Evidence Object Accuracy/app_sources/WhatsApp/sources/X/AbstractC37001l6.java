package X;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: X.1l6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37001l6 extends C37011l7 {
    public boolean A00;

    public AbstractC37001l6(Context context) {
        super(context);
        A02();
    }

    public AbstractC37001l6(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AbstractC37001l6(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }
}
