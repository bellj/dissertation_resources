package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98914jS implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    @Deprecated
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new C100524m3(parcel);
    }

    @Override // android.os.Parcelable.Creator
    @Deprecated
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C100524m3[i];
    }
}
