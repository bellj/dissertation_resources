package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31581am {
    public static final Object A04 = new Object();
    public final C31191a9 A00;
    public final C31191a9 A01;
    public final C31651at A02;
    public final C31601ao A03;

    public C31581am(C31191a9 r7, C31191a9 r8, C31191a9 r9, C31701ay r10, C31601ao r11) {
        this.A01 = r7;
        this.A00 = r8;
        this.A03 = r11;
        this.A02 = new C31651at(r7, r8, r9, r10, r11);
    }

    public static final Cipher A00(SecretKeySpec secretKeySpec, int i, int i2) {
        try {
            Cipher instance = Cipher.getInstance("AES/CTR/NoPadding");
            byte[] bArr = new byte[16];
            bArr[3] = (byte) i2;
            bArr[2] = (byte) (i2 >> 8);
            bArr[1] = (byte) (i2 >> 16);
            bArr[0] = (byte) (i2 >> 24);
            instance.init(i, secretKeySpec, new IvParameterSpec(bArr));
            return instance;
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public final byte[] A01(C31591an r7, C31611ap r8) {
        LinkedList linkedList = r8.A00;
        Iterator it = linkedList.iterator();
        LinkedList linkedList2 = new LinkedList();
        try {
            C31661au r0 = new C31661au(r8.A01);
            byte[] A02 = A02(r7, r0);
            r8.A01 = r0;
            return A02;
        } catch (C31521ag e) {
            linkedList2.add(e);
            if (it.hasNext()) {
                C31661au r1 = new C31661au((C31661au) it.next());
                byte[] A022 = A02(r7, r1);
                it.remove();
                linkedList.addFirst(r8.A01);
                r8.A01 = r1;
                if (linkedList.size() > 40) {
                    linkedList.removeLast();
                }
                return A022;
            }
            throw new C31521ag(linkedList2);
        }
    }

    public final byte[] A02(C31591an r13, C31661au r14) {
        C31261aG r4;
        int i;
        C31831bB A00;
        Cipher cipher;
        C31321aM r42 = r14.A00;
        boolean z = false;
        if ((r42.A00 & 32) == 32) {
            z = true;
        }
        if (z) {
            int i2 = r13.A01;
            int i3 = r42.A04;
            if (i3 == 0) {
                i3 = 2;
            }
            if (i2 == i3) {
                C31491ad r5 = r13.A02;
                int i4 = r13.A00;
                try {
                    if (r14.A05(r5) != null) {
                        C31881bG r43 = (C31881bG) r14.A05(r5).A00;
                        if (r43 == null) {
                            r4 = null;
                        } else {
                            int i5 = r14.A00.A04;
                            if (i5 == 0) {
                                i5 = 2;
                            }
                            AbstractC31231aD A002 = AbstractC31231aD.A00(i5);
                            C31861bE r0 = r43.A04;
                            if (r0 == null) {
                                r0 = C31861bE.A03;
                            }
                            byte[] A042 = r0.A02.A04();
                            C31861bE r02 = r43.A04;
                            if (r02 == null) {
                                r02 = C31861bE.A03;
                            }
                            r4 = new C31261aG(A002, A042, r02.A01);
                        }
                    } else {
                        int i6 = r14.A00.A04;
                        if (i6 == 0) {
                            i6 = 2;
                        }
                        C31251aF r6 = new C31251aF(AbstractC31231aD.A00(i6), r14.A00.A08.A04());
                        C31491ad A03 = r14.A03();
                        C31881bG r03 = r14.A00.A0A;
                        if (r03 == null) {
                            r03 = C31881bG.A05;
                        }
                        C32021bU A003 = r6.A00(r5, new C31731b1(new C31721b0(r03.A01.A04()), A03));
                        C31731b1 A01 = C31481ac.A01();
                        C32021bU A004 = ((C31251aF) A003.A00).A00(r5, A01);
                        r14.A0D((C31251aF) A004.A00);
                        r4 = (C31261aG) A003.A01;
                        r14.A0B(r5, r4);
                        int max = Math.max(r14.A04().A00 - 1, 0);
                        AnonymousClass1G4 A0T = r14.A00.A0T();
                        A0T.A03();
                        C31321aM r2 = (C31321aM) A0T.A00;
                        r2.A00 |= 16;
                        r2.A02 = max;
                        r14.A00 = (C31321aM) A0T.A02();
                        r14.A0C(A01, (C31261aG) A004.A01);
                    }
                    int i7 = r4.A00;
                    if (i7 > i4) {
                        C31881bG r04 = (C31881bG) r14.A05(r5).A00;
                        if (r04 != null) {
                            for (AnonymousClass25C r05 : r04.A03) {
                                if (r05.A01 == i4) {
                                    C32021bU A05 = r14.A05(r5);
                                    C31881bG r8 = (C31881bG) A05.A00;
                                    A00 = null;
                                    if (r8 != null) {
                                        LinkedList linkedList = new LinkedList(r8.A03);
                                        Iterator it = linkedList.iterator();
                                        while (true) {
                                            if (!it.hasNext()) {
                                                break;
                                            }
                                            AnonymousClass25C r9 = (AnonymousClass25C) it.next();
                                            if (r9.A01 == i4) {
                                                A00 = new C31831bB(new IvParameterSpec(r9.A03.A04()), new SecretKeySpec(r9.A02.A04(), "AES"), new SecretKeySpec(r9.A04.A04(), DefaultCrypto.HMAC_SHA256), r9.A01);
                                                it.remove();
                                                break;
                                            }
                                        }
                                        AnonymousClass1G4 A0T2 = r8.A0T();
                                        A0T2.A03();
                                        ((C31881bG) A0T2.A00).A03 = AnonymousClass277.A01;
                                        A0T2.A03();
                                        C31881bG r22 = (C31881bG) A0T2.A00;
                                        AnonymousClass1K6 r1 = r22.A03;
                                        if (!((AnonymousClass1K7) r1).A00) {
                                            r1 = AbstractC27091Fz.A0G(r1);
                                            r22.A03 = r1;
                                        }
                                        AnonymousClass1G5.A01(linkedList, r1);
                                        C31901bI r12 = (C31901bI) r14.A00.A0T();
                                        r12.A05((C31881bG) A0T2.A02(), ((Number) A05.A01).intValue());
                                        r14.A00 = (C31321aM) r12.A02();
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder("Received message with old counter: ");
                        sb.append(i7);
                        sb.append(" , ");
                        sb.append(i4);
                        throw new C31531ah(sb.toString());
                    } else if (i4 - i7 > 2000) {
                        throw new C31521ag("Over 2000 messages into the future!");
                    } else {
                        while (true) {
                            i = r4.A00;
                            if (i >= i4) {
                                break;
                            }
                            C31831bB A005 = r4.A00();
                            C32021bU A052 = r14.A05(r5);
                            AnonymousClass1G4 A0T3 = AnonymousClass25C.A05.A0T();
                            byte[] encoded = A005.A02.getEncoded();
                            AbstractC27881Jp A012 = AbstractC27881Jp.A01(encoded, 0, encoded.length);
                            A0T3.A03();
                            AnonymousClass25C r23 = (AnonymousClass25C) A0T3.A00;
                            r23.A00 |= 2;
                            r23.A02 = A012;
                            byte[] encoded2 = A005.A03.getEncoded();
                            AbstractC27881Jp A013 = AbstractC27881Jp.A01(encoded2, 0, encoded2.length);
                            A0T3.A03();
                            AnonymousClass25C r24 = (AnonymousClass25C) A0T3.A00;
                            r24.A00 |= 4;
                            r24.A04 = A013;
                            int i8 = A005.A00;
                            A0T3.A03();
                            AnonymousClass25C r25 = (AnonymousClass25C) A0T3.A00;
                            r25.A00 |= 1;
                            r25.A01 = i8;
                            byte[] iv = A005.A01.getIV();
                            AbstractC27881Jp A014 = AbstractC27881Jp.A01(iv, 0, iv.length);
                            A0T3.A03();
                            AnonymousClass25C r26 = (AnonymousClass25C) A0T3.A00;
                            r26.A00 |= 8;
                            r26.A03 = A014;
                            AbstractC27091Fz A02 = A0T3.A02();
                            AnonymousClass1G4 A0T4 = ((AbstractC27091Fz) A052.A00).A0T();
                            A0T4.A03();
                            C31881bG r62 = (C31881bG) A0T4.A00;
                            AnonymousClass1K6 r27 = r62.A03;
                            if (!((AnonymousClass1K7) r27).A00) {
                                r27 = AbstractC27091Fz.A0G(r27);
                                r62.A03 = r27;
                            }
                            r27.add(A02);
                            if (((C31881bG) A0T4.A00).A03.size() > 2000) {
                                A0T4.A03();
                                C31881bG r92 = (C31881bG) A0T4.A00;
                                AnonymousClass1K6 r28 = r92.A03;
                                if (!((AnonymousClass1K7) r28).A00) {
                                    r28 = AbstractC27091Fz.A0G(r28);
                                    r92.A03 = r28;
                                }
                                r28.remove(0);
                            }
                            C31901bI r63 = (C31901bI) r14.A00.A0T();
                            r63.A05((C31881bG) A0T4.A02(), ((Number) A052.A01).intValue());
                            r14.A00 = (C31321aM) r63.A02();
                            r4 = new C31261aG(r4.A01, r4.A01(C31261aG.A03), r4.A00 + 1);
                        }
                        C31261aG r82 = new C31261aG(r4.A01, r4.A01(C31261aG.A03), i + 1);
                        C32021bU A053 = r14.A05(r5);
                        C31871bF r52 = (C31871bF) C31861bE.A03.A0T();
                        byte[] bArr = r82.A02;
                        r52.A06(AbstractC27881Jp.A01(bArr, 0, bArr.length));
                        r52.A05(r82.A00);
                        C31891bH r06 = (C31891bH) ((AbstractC27091Fz) A053.A00).A0T();
                        r06.A05((C31861bE) r52.A02());
                        C31901bI r15 = (C31901bI) r14.A00.A0T();
                        r15.A05((C31881bG) r06.A02(), ((Number) A053.A01).intValue());
                        r14.A00 = (C31321aM) r15.A02();
                        A00 = r4.A00();
                    }
                    C31641as A022 = r14.A02();
                    C31641as A015 = r14.A01();
                    SecretKeySpec secretKeySpec = A00.A03;
                    byte[] bArr2 = r13.A04;
                    byte[][] A016 = C31241aE.A01(bArr2, bArr2.length - 8, 8);
                    if (MessageDigest.isEqual(C31591an.A00(secretKeySpec, A022, A015, A016[0], i2), A016[1])) {
                        byte[] bArr3 = r13.A03;
                        try {
                            if (i2 >= 3) {
                                SecretKeySpec secretKeySpec2 = A00.A02;
                                IvParameterSpec ivParameterSpec = A00.A01;
                                try {
                                    cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                                    cipher.init(2, secretKeySpec2, ivParameterSpec);
                                } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
                                    throw new AssertionError(e);
                                }
                            } else {
                                cipher = A00(A00.A02, 2, A00.A00);
                            }
                            byte[] doFinal = cipher.doFinal(bArr3);
                            AnonymousClass1G4 A0T5 = r14.A00.A0T();
                            A0T5.A03();
                            C31321aM r16 = (C31321aM) A0T5.A00;
                            r16.A0C = null;
                            r16.A00 &= -129;
                            r14.A00 = (C31321aM) A0T5.A02();
                            return doFinal;
                        } catch (BadPaddingException | IllegalBlockSizeException e2) {
                            throw new C31521ag(e2);
                        }
                    } else {
                        throw new C31521ag("Bad Mac!");
                    }
                } catch (C31561ak e3) {
                    throw new C31521ag(e3);
                }
            } else {
                throw new C31521ag(String.format("Message version %d, but session version %d", Integer.valueOf(i2), Integer.valueOf(i3)));
            }
        } else {
            throw new C31521ag("Uninitialized session!");
        }
    }
}
