package X;

import java.util.Arrays;

/* renamed from: X.0Ss  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06240Ss {
    public static int A0E = 1000;
    public static boolean A0F = true;
    public static long A0G;
    public static long A0H;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public AbstractC11690gh A07;
    public AbstractC11690gh A08;
    public boolean A09;
    public C07240Xf[] A0A;
    public AnonymousClass0QC[] A0B;
    public boolean[] A0C;
    public final AnonymousClass0NX A0D;

    public C06240Ss() {
        AbstractC11690gh r0;
        this.A06 = 0;
        this.A00 = 32;
        this.A01 = 32;
        this.A0A = null;
        this.A09 = false;
        this.A0C = new boolean[32];
        this.A03 = 1;
        this.A04 = 0;
        this.A02 = 32;
        this.A0B = new AnonymousClass0QC[A0E];
        this.A05 = 0;
        this.A0A = new C07240Xf[32];
        A08();
        AnonymousClass0NX r1 = new AnonymousClass0NX();
        this.A0D = r1;
        this.A07 = new AnonymousClass0Cq(r1);
        if (A0F) {
            r0 = new C02520Cp(r1, this);
        } else {
            r0 = new C07240Xf(r1);
        }
        this.A08 = r0;
    }

    public static int A00(Object obj) {
        AnonymousClass0QC r0 = ((AnonymousClass0Q7) obj).A02;
        if (r0 != null) {
            return (int) (r0.A00 + 0.5f);
        }
        return 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v14, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r4v11 */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C07240Xf A01() {
        /*
            r8 = this;
            boolean r0 = X.C06240Ss.A0F
            r6 = 1
            X.0NX r5 = r8.A0D
            if (r0 == 0) goto L_0x0032
            X.0fx r3 = r5.A01
            X.0Xg r3 = (X.C07250Xg) r3
            int r0 = r3.A00
            r2 = 0
            if (r0 <= 0) goto L_0x0030
            int r1 = r0 + -1
            java.lang.Object[] r0 = r3.A01
            r4 = r0[r1]
            r0[r1] = r2
            r3.A00 = r1
        L_0x001b:
            X.0Xf r4 = (X.C07240Xf) r4
            if (r4 != 0) goto L_0x0056
            X.0Cp r4 = new X.0Cp
            r4.<init>(r5, r8)
            long r0 = X.C06240Ss.A0H
            long r0 = r0 + r6
            X.C06240Ss.A0H = r0
        L_0x0029:
            int r0 = X.AnonymousClass0QC.A0C
            int r0 = r0 + 1
            X.AnonymousClass0QC.A0C = r0
            return r4
        L_0x0030:
            r4 = r2
            goto L_0x001b
        L_0x0032:
            X.0fx r3 = r5.A00
            X.0Xg r3 = (X.C07250Xg) r3
            int r0 = r3.A00
            r2 = 0
            if (r0 <= 0) goto L_0x0054
            int r1 = r0 + -1
            java.lang.Object[] r0 = r3.A01
            r4 = r0[r1]
            r0[r1] = r2
            r3.A00 = r1
        L_0x0045:
            X.0Xf r4 = (X.C07240Xf) r4
            if (r4 != 0) goto L_0x0056
            X.0Xf r4 = new X.0Xf
            r4.<init>(r5)
            long r0 = X.C06240Ss.A0G
            long r0 = r0 + r6
            X.C06240Ss.A0G = r0
            goto L_0x0029
        L_0x0054:
            r4 = r2
            goto L_0x0045
        L_0x0056:
            r0 = 0
            r4.A02 = r0
            X.0iP r0 = r4.A01
            r0.clear()
            r0 = 0
            r4.A00 = r0
            r0 = 0
            r4.A04 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06240Ss.A01():X.0Xf");
    }

    public AnonymousClass0QC A02() {
        if (this.A03 + 1 >= this.A01) {
            A07();
        }
        AnonymousClass0QC A04 = A04(AnonymousClass0JQ.SLACK);
        int i = this.A06 + 1;
        this.A06 = i;
        this.A03++;
        A04.A02 = i;
        this.A0D.A03[i] = A04;
        return A04;
    }

    public AnonymousClass0QC A03(int i) {
        if (this.A03 + 1 >= this.A01) {
            A07();
        }
        AnonymousClass0QC A04 = A04(AnonymousClass0JQ.ERROR);
        int i2 = this.A06 + 1;
        this.A06 = i2;
        this.A03++;
        A04.A02 = i2;
        A04.A04 = i;
        this.A0D.A03[i2] = A04;
        AnonymousClass0Cq r3 = (AnonymousClass0Cq) this.A07;
        r3.A02.A01 = A04;
        float[] fArr = A04.A09;
        Arrays.fill(fArr, 0.0f);
        fArr[A04.A04] = 1.0f;
        r3.A06(A04);
        return A04;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v7, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass0QC A04(X.AnonymousClass0JQ r6) {
        /*
            r5 = this;
            X.0NX r0 = r5.A0D
            X.0fx r4 = r0.A02
            X.0Xg r4 = (X.C07250Xg) r4
            int r0 = r4.A00
            r2 = 0
            if (r0 <= 0) goto L_0x0043
            int r1 = r0 + -1
            java.lang.Object[] r0 = r4.A01
            r3 = r0[r1]
            r0[r1] = r2
            r4.A00 = r1
        L_0x0015:
            X.0QC r3 = (X.AnonymousClass0QC) r3
            if (r3 != 0) goto L_0x003f
            X.0QC r3 = new X.0QC
            r3.<init>(r6)
        L_0x001e:
            r3.A06 = r6
            int r1 = r5.A05
            int r0 = X.C06240Ss.A0E
            if (r1 < r0) goto L_0x0034
            int r1 = r0 << 1
            X.C06240Ss.A0E = r1
            X.0QC[] r0 = r5.A0B
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
            X.0QC[] r0 = (X.AnonymousClass0QC[]) r0
            r5.A0B = r0
        L_0x0034:
            X.0QC[] r2 = r5.A0B
            int r1 = r5.A05
            int r0 = r1 + 1
            r5.A05 = r0
            r2[r1] = r3
            return r3
        L_0x003f:
            r3.A00()
            goto L_0x001e
        L_0x0043:
            r3 = r2
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06240Ss.A04(X.0JQ):X.0QC");
    }

    public AnonymousClass0QC A05(Object obj) {
        AnonymousClass0QC r3 = null;
        if (obj != null) {
            if (this.A03 + 1 >= this.A01) {
                A07();
            }
            if (obj instanceof AnonymousClass0Q7) {
                AnonymousClass0Q7 r5 = (AnonymousClass0Q7) obj;
                r3 = r5.A02;
                if (r3 == null) {
                    r5.A02();
                    r3 = r5.A02;
                }
                int i = r3.A02;
                if (i != -1) {
                    if (i > this.A06 || this.A0D.A03[i] == null) {
                        if (i != -1) {
                            r3.A00();
                        }
                    }
                }
                int i2 = this.A06 + 1;
                this.A06 = i2;
                this.A03++;
                r3.A02 = i2;
                r3.A06 = AnonymousClass0JQ.UNRESTRICTED;
                this.A0D.A03[i2] = r3;
            }
        }
        return r3;
    }

    public void A06() {
        AnonymousClass0NX r5;
        AnonymousClass0QC[] r10;
        AbstractC11690gh r0;
        int i = 0;
        while (true) {
            r5 = this.A0D;
            r10 = r5.A03;
            if (i >= r10.length) {
                break;
            }
            AnonymousClass0QC r02 = r10[i];
            if (r02 != null) {
                r02.A00();
            }
            i++;
        }
        AbstractC11230fx r9 = r5.A02;
        AnonymousClass0QC[] r8 = this.A0B;
        int i2 = this.A05;
        C07250Xg r92 = (C07250Xg) r9;
        int length = r8.length;
        if (i2 > length) {
            i2 = length;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            AnonymousClass0QC r3 = r8[i3];
            int i4 = r92.A00;
            Object[] objArr = r92.A01;
            if (i4 < objArr.length) {
                objArr[i4] = r3;
                r92.A00 = i4 + 1;
            }
        }
        this.A05 = 0;
        Arrays.fill(r10, (Object) null);
        this.A06 = 0;
        AnonymousClass0Cq r1 = (AnonymousClass0Cq) this.A07;
        r1.A00 = 0;
        ((C07240Xf) r1).A00 = 0.0f;
        this.A03 = 1;
        for (int i5 = 0; i5 < this.A04; i5++) {
        }
        A08();
        this.A04 = 0;
        if (A0F) {
            r0 = new C02520Cp(r5, this);
        } else {
            r0 = new C07240Xf(r5);
        }
        this.A08 = r0;
    }

    public final void A07() {
        int i = this.A00 << 1;
        this.A00 = i;
        this.A0A = (C07240Xf[]) Arrays.copyOf(this.A0A, i);
        AnonymousClass0NX r2 = this.A0D;
        r2.A03 = (AnonymousClass0QC[]) Arrays.copyOf(r2.A03, this.A00);
        int i2 = this.A00;
        this.A0C = new boolean[i2];
        this.A01 = i2;
        this.A02 = i2;
    }

    public final void A08() {
        int i = 0;
        if (A0F) {
            while (true) {
                C07240Xf[] r5 = this.A0A;
                if (i < r5.length) {
                    C07240Xf r4 = r5[i];
                    if (r4 != null) {
                        C07250Xg r3 = (C07250Xg) this.A0D.A01;
                        int i2 = r3.A00;
                        Object[] objArr = r3.A01;
                        if (i2 < objArr.length) {
                            objArr[i2] = r4;
                            r3.A00 = i2 + 1;
                        }
                    }
                    r5[i] = null;
                    i++;
                } else {
                    return;
                }
            }
        } else {
            while (true) {
                C07240Xf[] r52 = this.A0A;
                if (i < r52.length) {
                    C07240Xf r42 = r52[i];
                    if (r42 != null) {
                        C07250Xg r32 = (C07250Xg) this.A0D.A00;
                        int i3 = r32.A00;
                        Object[] objArr2 = r32.A01;
                        if (i3 < objArr2.length) {
                            objArr2[i3] = r42;
                            r32.A00 = i3 + 1;
                        }
                    }
                    r52[i] = null;
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:128:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x017b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.C07240Xf r18) {
        /*
        // Method dump skipped, instructions count: 397
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06240Ss.A09(X.0Xf):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0A(X.C07240Xf r8) {
        /*
            r7 = this;
            boolean r0 = X.C06240Ss.A0F
            X.0Xf[] r6 = r7.A0A
            int r5 = r7.A04
            r4 = r6[r5]
            if (r0 == 0) goto L_0x002d
            if (r4 == 0) goto L_0x001f
            X.0NX r0 = r7.A0D
            X.0fx r3 = r0.A01
        L_0x0010:
            X.0Xg r3 = (X.C07250Xg) r3
            int r2 = r3.A00
            java.lang.Object[] r1 = r3.A01
            int r0 = r1.length
            if (r2 >= r0) goto L_0x001f
            r1[r2] = r4
            r0 = 1
            int r2 = r2 + r0
            r3.A00 = r2
        L_0x001f:
            r6[r5] = r8
            X.0QC r1 = r8.A02
            r1.A01 = r5
            int r0 = r5 + 1
            r7.A04 = r0
            r1.A03(r8)
            return
        L_0x002d:
            if (r4 == 0) goto L_0x001f
            X.0NX r0 = r7.A0D
            X.0fx r3 = r0.A00
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06240Ss.A0A(X.0Xf):void");
    }

    public final void A0B(AbstractC11690gh r10) {
        for (int i = 0; i < this.A03; i++) {
            this.A0C[i] = false;
        }
        int i2 = 0;
        while (true) {
            i2++;
            if (i2 < (this.A03 << 1)) {
                AnonymousClass0QC r0 = ((C07240Xf) r10).A02;
                if (r0 != null) {
                    this.A0C[r0.A02] = true;
                }
                boolean[] zArr = this.A0C;
                AnonymousClass0QC AFh = r10.AFh(this, zArr);
                if (AFh != null) {
                    int i3 = AFh.A02;
                    if (!zArr[i3]) {
                        zArr[i3] = true;
                        float f = Float.MAX_VALUE;
                        int i4 = -1;
                        for (int i5 = 0; i5 < this.A04; i5++) {
                            C07240Xf r1 = this.A0A[i5];
                            if (r1.A02.A06 != AnonymousClass0JQ.UNRESTRICTED && !r1.A04 && r1.A01.A7d(AFh)) {
                                float AAK = r1.A01.AAK(AFh);
                                if (AAK < 0.0f) {
                                    float f2 = (-r1.A00) / AAK;
                                    if (f2 < f) {
                                        i4 = i5;
                                        f = f2;
                                    }
                                }
                            }
                        }
                        if (i4 > -1) {
                            C07240Xf r12 = this.A0A[i4];
                            r12.A02.A01 = -1;
                            r12.A02(AFh);
                            AnonymousClass0QC r02 = r12.A02;
                            r02.A01 = i4;
                            r02.A03(r12);
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public void A0C(AnonymousClass0QC r5, int i) {
        C07240Xf r2;
        AbstractC12730iP r1;
        float f;
        int i2 = r5.A01;
        if (i2 == -1) {
            r5.A00 = (float) i;
            r5.A08 = true;
            int i3 = r5.A03;
            for (int i4 = 0; i4 < i3; i4++) {
                r5.A0B[i4].A05(r5, false);
            }
            r5.A03 = 0;
            return;
        }
        if (i2 != -1) {
            C07240Xf r12 = this.A0A[i2];
            if (!r12.A04) {
                if (r12.A01.ACD() == 0) {
                    r12.A04 = true;
                } else {
                    r2 = A01();
                    if (i < 0) {
                        r2.A00 = (float) (-i);
                        r1 = r2.A01;
                        f = 1.0f;
                    } else {
                        r2.A00 = (float) i;
                        r1 = r2.A01;
                        f = -1.0f;
                    }
                    r1.AZg(r5, f);
                }
            }
            r12.A00 = (float) i;
            return;
        }
        r2 = A01();
        r2.A02 = r5;
        float f2 = (float) i;
        r5.A00 = f2;
        r2.A00 = f2;
        r2.A04 = true;
        A09(r2);
    }

    public void A0D(AnonymousClass0QC r7, AnonymousClass0QC r8, int i, int i2) {
        if (i2 == 8 && r8.A08 && r7.A01 == -1) {
            r7.A00 = r8.A00 + ((float) i);
            r7.A08 = true;
            int i3 = r7.A03;
            for (int i4 = 0; i4 < i3; i4++) {
                r7.A0B[i4].A05(r7, false);
            }
            r7.A03 = 0;
            return;
        }
        C07240Xf A01 = A01();
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i = -i;
                z = true;
            }
            A01.A00 = (float) i;
        }
        AbstractC12730iP r0 = A01.A01;
        if (!z) {
            r0.AZg(r7, -1.0f);
            A01.A01.AZg(r8, 1.0f);
        } else {
            r0.AZg(r7, 1.0f);
            A01.A01.AZg(r8, -1.0f);
        }
        if (i2 != 8) {
            A01.A01.AZg(A03(i2), 1.0f);
            A01.A01.AZg(A03(i2), -1.0f);
        }
        A09(A01);
    }

    public void A0E(AnonymousClass0QC r5, AnonymousClass0QC r6, int i, int i2) {
        C07240Xf A01 = A01();
        AnonymousClass0QC A02 = A02();
        A02.A04 = 0;
        A01.A03(r5, r6, A02, i);
        if (i2 != 8) {
            int AAK = (int) (A01.A01.AAK(A02) * -1.0f);
            A01.A01.AZg(A03(i2), (float) AAK);
        }
        A09(A01);
    }

    public void A0F(AnonymousClass0QC r5, AnonymousClass0QC r6, int i, int i2) {
        C07240Xf A01 = A01();
        AnonymousClass0QC A02 = A02();
        A02.A04 = 0;
        A01.A04(r5, r6, A02, i);
        if (i2 != 8) {
            int AAK = (int) (A01.A01.AAK(A02) * -1.0f);
            A01.A01.AZg(A03(i2), (float) AAK);
        }
        A09(A01);
    }

    public void A0G(AnonymousClass0QC r7, AnonymousClass0QC r8, AnonymousClass0QC r9, AnonymousClass0QC r10, float f, int i, int i2, int i3) {
        float f2;
        int i4;
        C07240Xf A01 = A01();
        if (r8 == r9) {
            A01.A01.AZg(r7, 1.0f);
            A01.A01.AZg(r10, 1.0f);
            A01.A01.AZg(r8, -2.0f);
        } else {
            if (f == 0.5f) {
                A01.A01.AZg(r7, 1.0f);
                A01.A01.AZg(r8, -1.0f);
                A01.A01.AZg(r9, -1.0f);
                A01.A01.AZg(r10, 1.0f);
                if (i > 0 || i2 > 0) {
                    i4 = (-i) + i2;
                    f2 = (float) i4;
                }
            } else if (f <= 0.0f) {
                A01.A01.AZg(r7, -1.0f);
                A01.A01.AZg(r8, 1.0f);
                f2 = (float) i;
            } else if (f >= 1.0f) {
                A01.A01.AZg(r10, -1.0f);
                A01.A01.AZg(r9, 1.0f);
                i4 = -i2;
                f2 = (float) i4;
            } else {
                float f3 = 1.0f - f;
                A01.A01.AZg(r7, f3 * 1.0f);
                A01.A01.AZg(r8, f3 * -1.0f);
                A01.A01.AZg(r9, -1.0f * f);
                A01.A01.AZg(r10, 1.0f * f);
                if (i > 0 || i2 > 0) {
                    f2 = (((float) (-i)) * f3) + (((float) i2) * f);
                }
            }
            A01.A00 = f2;
        }
        if (i3 != 8) {
            A01.A01.AZg(A03(i3), 1.0f);
            A01.A01.AZg(A03(i3), -1.0f);
        }
        A09(A01);
    }
}
