package X;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53112cv extends LinearLayout implements AnonymousClass004 {
    public ProgressBar A00;
    public TextView A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public C53112cv(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        LinearLayout.inflate(context, R.layout.conversation_history_sync_progress_header, this);
        this.A01 = C12960it.A0I(this, R.id.history_sync_progress_text);
        this.A00 = (ProgressBar) AnonymousClass028.A0D(this, R.id.history_sync_progress_loader);
        this.A01.setTextSize(AnonymousClass1OY.A00(getResources()));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }
}
