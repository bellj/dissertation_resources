package X;

/* renamed from: X.1or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38881or implements AbstractC38871oq {
    public float A00;
    public final C38861op A01;

    public C38881or(C38861op r1, float f) {
        this.A00 = f;
        this.A01 = r1;
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ boolean A7R(Object obj) {
        String str = ((C38861op) obj).A01;
        AnonymousClass009.A05(str);
        return str.equals(this.A01.A01);
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ Object ADD() {
        return this.A01;
    }

    @Override // X.AbstractC38871oq
    public float AHi() {
        return this.A00;
    }

    @Override // X.AbstractC38871oq
    public void AdB(float f) {
        this.A00 = f;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("WeightedRecentStickerIdentifier{");
        stringBuffer.append("stickerIdentifier=");
        stringBuffer.append(this.A01);
        stringBuffer.append(", weight=");
        stringBuffer.append(this.A00);
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
