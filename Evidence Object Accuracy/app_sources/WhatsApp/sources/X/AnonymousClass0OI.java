package X;

import android.os.Handler;

/* renamed from: X.0OI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OI {
    public final Handler A00;
    public final AnonymousClass0MR A01;

    public AnonymousClass0OI(Handler handler, AnonymousClass0MR r2) {
        this.A01 = r2;
        this.A00 = handler;
    }

    public void A00(C05820Rc r5) {
        int i = r5.A00;
        if (i == 0) {
            this.A00.post(new RunnableC09810dY(r5.A01, this.A01, this));
            return;
        }
        this.A00.post(new RunnableC09820dZ(this.A01, this, i));
    }
}
