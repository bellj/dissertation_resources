package X;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3AK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AK {
    public static void A00(ImageView imageView, C37071lG r11, AnonymousClass3M0 r12) {
        AnonymousClass4Dz.A00(imageView);
        if (r12 != null) {
            String str = r12.A01;
            if (!TextUtils.isEmpty(str)) {
                r11.A01(imageView, new C44741zT(r12.A00, str, null, 0, 0), null, new AnonymousClass5TO() { // from class: X.3V6
                    @Override // X.AnonymousClass5TO
                    public final void ARz(C68203Um r4) {
                        ImageView imageView2 = (ImageView) r4.A09.get();
                        if (imageView2 != null) {
                            imageView2.setBackgroundResource(R.color.light_gray);
                            imageView2.setImageResource(R.drawable.product_broken_image);
                            AnonymousClass2GE.A05(imageView2.getContext(), imageView2, R.color.black_alpha_20);
                            imageView2.setScaleType(ImageView.ScaleType.CENTER);
                        }
                    }
                }, new AnonymousClass2E5() { // from class: X.3V7
                    @Override // X.AnonymousClass2E5
                    public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                        ImageView imageView2 = (ImageView) r4.A09.get();
                        if (imageView2 != null) {
                            imageView2.setBackgroundColor(0);
                            imageView2.setImageBitmap(bitmap);
                            C12990iw.A1E(imageView2);
                        }
                    }
                }, 2);
            }
        }
    }
}
