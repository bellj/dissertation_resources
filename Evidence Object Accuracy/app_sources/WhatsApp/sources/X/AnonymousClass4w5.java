package X;

/* renamed from: X.4w5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4w5 implements AnonymousClass5VX {
    public final C95304dT A00 = new C95304dT();
    public final AnonymousClass4YB A01;

    public /* synthetic */ AnonymousClass4w5(AnonymousClass4YB r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5VX
    public void AVj() {
        C95304dT r2 = this.A00;
        byte[] bArr = AnonymousClass3JZ.A0A;
        r2.A0U(bArr, bArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f7, code lost:
        return new X.C93534aK(0, -9223372036854775807L, r11 + r0);
     */
    @Override // X.AnonymousClass5VX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C93534aK AbM(X.AnonymousClass5Yf r22, long r23) {
        /*
        // Method dump skipped, instructions count: 264
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4w5.AbM(X.5Yf, long):X.4aK");
    }
}
