package X;

import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.List;

/* renamed from: X.23u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C459323u implements AbstractC14590lg {
    public int A00 = 0;
    public boolean A01 = false;
    public final /* synthetic */ C38421o4 A02;
    public final /* synthetic */ AnonymousClass109 A03;
    public final /* synthetic */ AnonymousClass1KC A04;
    public final /* synthetic */ C14370lK A05;

    public C459323u(C38421o4 r2, AnonymousClass109 r3, AnonymousClass1KC r4, C14370lK r5) {
        this.A03 = r3;
        this.A04 = r4;
        this.A05 = r5;
        this.A02 = r2;
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        String str = (String) obj;
        AnonymousClass109 r9 = this.A03;
        C14540lb r0 = r9.A0K;
        AnonymousClass1KC r2 = this.A04;
        C14560ld r3 = (C14560ld) r0.A00(r2);
        if (!r9.A01) {
            r9.A00 = r9.A0C.A07(1539);
            r9.A01 = true;
        }
        if (r9.A00 && r3 != null && this.A05.equals(C14370lK.A08) && !this.A01) {
            C29171Rd r02 = r3.A0X;
            String A05 = r02.A05();
            String A04 = r02.A04();
            long j = r2.A00().A02;
            if (A05 == null || A04 == null || j <= 0) {
                Log.e("sendmediamessagemanager/whenhttpconnected could not send medianotify since some params are invalid");
            } else {
                List unmodifiableList = Collections.unmodifiableList(this.A02.A01);
                if (unmodifiableList.size() == 1) {
                    AbstractC14640lm r10 = ((AbstractC15340mz) unmodifiableList.get(0)).A0z.A00;
                    if (C15380n4.A0L(r10) && r10 != null) {
                        C30291Wv r1 = new C30291Wv(r9.A0O.A07.A02(r10, true), r9.A06.A00());
                        r1.A03 = str;
                        r1.A02 = A05;
                        r1.A01 = A04;
                        r1.A00 = j;
                        r9.A08.A0S(r1);
                    }
                }
            }
            this.A01 = true;
        }
        this.A00++;
    }
}
