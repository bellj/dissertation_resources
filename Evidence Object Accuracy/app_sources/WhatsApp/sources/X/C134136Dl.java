package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.6Dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134136Dl implements AnonymousClass5Wu {
    public View A00;
    public ImageView A01;
    public ImageView A02;
    public TextView A03;
    public TextView A04;
    public final View.OnClickListener A05;
    public final C38721ob A06;

    public C134136Dl(View.OnClickListener onClickListener, C38721ob r2) {
        this.A06 = r2;
        this.A05 = onClickListener;
    }

    /* renamed from: A00 */
    public void A6Q(AnonymousClass4OZ r7) {
        TextView textView;
        TextUtils.TruncateAt truncateAt;
        if (r7 != null) {
            int i = r7.A00;
            int i2 = 8;
            if (!(i == -2 || i == -1)) {
                if (i == 0) {
                    this.A00.setVisibility(8);
                    return;
                } else if (!(i == 1 || i == 2)) {
                    return;
                }
            }
            this.A00.setVisibility(0);
            this.A00.setOnClickListener(this.A05);
            C1315663g r4 = (C1315663g) r7.A01;
            if (r4 != null) {
                this.A04.setText(r4.A03);
                this.A03.setText(r4.A01);
                boolean z = r4.A05;
                TextView textView2 = this.A03;
                if (z) {
                    textView2.setMaxLines(Integer.MAX_VALUE);
                    textView = this.A03;
                    truncateAt = null;
                } else {
                    textView2.setMaxLines(1);
                    textView = this.A03;
                    truncateAt = TextUtils.TruncateAt.END;
                }
                textView.setEllipsize(truncateAt);
                String str = r4.A02;
                if (str == null || TextUtils.isEmpty(str)) {
                    int i3 = r4.A00;
                    if (i3 != -1) {
                        this.A02.setImageResource(i3);
                    }
                } else {
                    this.A06.A01(this.A02, str);
                }
                ImageView imageView = this.A01;
                if (r4.A04) {
                    i2 = 0;
                }
                imageView.setVisibility(i2);
            }
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_withdraw_review_method;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A02 = C12970iu.A0K(view, R.id.novi_withdraw_review_method_icon);
        this.A04 = C12960it.A0I(view, R.id.novi_withdraw_review_method_title);
        this.A03 = C12960it.A0I(view, R.id.novi_withdraw_review_method_description);
        this.A01 = C12970iu.A0K(view, R.id.novi_withdraw_review_method_chevron_icon);
    }
}
