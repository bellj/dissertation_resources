package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2xS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xS extends AbstractC36601kB implements SectionIndexer {
    public List A00;
    public List A01;
    public List A02;
    public List A03 = C12960it.A0l();
    public List A04 = C12960it.A0l();
    public final Context A05;
    public final AnonymousClass130 A06;
    public final AnonymousClass1J1 A07;
    public final AnonymousClass018 A08;

    public AnonymousClass2xS(Context context, AnonymousClass130 r3, AnonymousClass1J1 r4, AnonymousClass018 r5, List list) {
        this.A01 = list;
        this.A05 = context;
        this.A06 = r3;
        this.A07 = r4;
        this.A02 = list;
        this.A08 = r5;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A01.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A01.get(i);
    }

    @Override // android.widget.SectionIndexer
    public int getPositionForSection(int i) {
        List list = this.A03;
        if (i >= list.size() || i < 0) {
            return -1;
        }
        return C12960it.A05(list.get(i));
    }

    @Override // android.widget.SectionIndexer
    public int getSectionForPosition(int i) {
        List list = this.A02;
        List list2 = this.A04;
        List list3 = this.A03;
        if (i < 0) {
            return 0;
        }
        if (i >= list.size()) {
            return C12980iv.A0C(list2);
        }
        int size = list3.size();
        do {
            size--;
            if (size < 0) {
                return 0;
            }
        } while (C12960it.A05(list3.get(size)) > i);
        return size;
    }

    @Override // android.widget.SectionIndexer
    public Object[] getSections() {
        return this.A04.toArray(C13000ix.A08());
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass3BS r3;
        String str;
        AbstractC116005Tt r6 = (AbstractC116005Tt) this.A01.get(i);
        AnonymousClass009.A05(r6);
        if (r6 instanceof AnonymousClass552) {
            if (view == null) {
                view = LayoutInflater.from(this.A05).inflate(R.layout.list_section, viewGroup, false);
                AnonymousClass028.A0a(view, 2);
            }
            TextView A0J = C12960it.A0J(view, R.id.title);
            C27531Hw.A06(A0J);
            A0J.setText(((AnonymousClass552) r6).A00);
            return view;
        }
        if (view == null) {
            view = LayoutInflater.from(this.A05).inflate(R.layout.phone_contact_row, viewGroup, false);
            r3 = new AnonymousClass3BS(view);
            view.setTag(r3);
        } else {
            r3 = (AnonymousClass3BS) view.getTag();
        }
        if (r6 instanceof AnonymousClass551) {
            AnonymousClass028.A0a(view, 2);
            r3.A01.setVisibility(4);
            r3.A02.setText(((AnonymousClass551) r6).A00);
            r3.A03.setVisibility(8);
            return view;
        }
        AnonymousClass3WI r62 = (AnonymousClass3WI) r6;
        ImageView imageView = r3.A01;
        imageView.setVisibility(0);
        this.A06.A05(imageView, R.drawable.avatar_contact);
        C15370n3 ABZ = r62.ABZ();
        this.A07.A06(imageView, ABZ);
        r3.A02.A0G(this.A00, r62.A00);
        TextEmojiLabel textEmojiLabel = r3.A03;
        textEmojiLabel.setVisibility(0);
        List list = r62.A01;
        if (list.size() > 1) {
            String[] strArr = new String[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                strArr[i2] = C248917h.A01((C15370n3) list.get(i2));
            }
            str = TextUtils.join(", ", strArr);
        } else {
            str = C248917h.A01(ABZ);
        }
        textEmojiLabel.setText(str);
        return view;
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Pair A00 = AnonymousClass3AW.A00(this.A08, this.A02);
        this.A04 = (List) A00.first;
        this.A03 = (List) A00.second;
    }
}
