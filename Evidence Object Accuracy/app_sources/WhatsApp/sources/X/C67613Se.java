package X;

import com.google.android.exoplayer2.Timeline;
import java.util.List;

/* renamed from: X.3Se  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67613Se implements AnonymousClass5VW {
    public int A00;
    public boolean A01;
    public final C56032kD A02;
    public final Object A03 = C12970iu.A0l();
    public final List A04 = C12960it.A0l();

    public C67613Se(AnonymousClass2CD r2, boolean z) {
        this.A02 = new C56032kD(r2, z);
    }

    @Override // X.AnonymousClass5VW
    public Timeline AHE() {
        return this.A02.A01;
    }

    @Override // X.AnonymousClass5VW
    public Object AHN() {
        return this.A03;
    }
}
