package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.3ST  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ST implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        AnonymousClass2k6 r5 = (AnonymousClass2k6) obj;
        C55992k9 r6 = (C55992k9) obj2;
        r5.setOnTouchListener(null);
        r5.A02 = null;
        View.OnClickListener onClickListener = r6.A06;
        if (onClickListener != null) {
            r5.setOnClickListener(onClickListener);
        }
        r5.setOnFocusChangeListener(null);
        r5.setFocusable(false);
        r5.setFocusableInTouchMode(false);
        r5.setEnabled(r6.A09);
        if (r6.A00 != -1) {
            r5.setClickable(false);
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        AnonymousClass2k6 r4 = (AnonymousClass2k6) obj;
        r4.setOnTouchListener(null);
        r4.A02 = null;
        r4.setOnClickListener(null);
        r4.setClickable(false);
        r4.setOnLongClickListener(null);
        r4.setLongClickable(false);
        r4.setOnFocusChangeListener(null);
        r4.setFocusable(false);
        r4.setFocusableInTouchMode(false);
    }
}
