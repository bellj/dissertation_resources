package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2mq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57322mq extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57322mq A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01 = 0;
    public Object A02;
    public String A03 = "";

    static {
        C57322mq r0 = new C57322mq();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0061, code lost:
        if (r8.A01 == 2) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0066, code lost:
        if (r8.A01 == 3) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0068, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0069, code lost:
        r8.A02 = r10.Afv(r8.A02, r11.A02, r5);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r9, java.lang.Object r10, java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57322mq.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A03, 0);
        }
        if (this.A01 == 2) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 2, i2);
        }
        if (this.A01 == 3) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 3, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 2);
        }
        if (this.A01 == 3) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 3);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
