package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32101bc extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C32101bc A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public AnonymousClass25E A03;
    public C31471ab A04;

    static {
        C32101bc r0 = new C32101bc();
        A05 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A01) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            AnonymousClass25E r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass25E.A03;
            }
            i += CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C31471ab r02 = this.A04;
            if (r02 == null) {
                r02 = C31471ab.A03;
            }
            i += CodedOutputStream.A0A(r02, 3);
        }
        for (int i4 = 0; i4 < this.A02.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A02.get(i4), 4);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass25E r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass25E.A03;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C31471ab r02 = this.A04;
            if (r02 == null) {
                r02 = C31471ab.A03;
            }
            codedOutputStream.A0L(r02, 3);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
