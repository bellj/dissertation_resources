package X;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99154jq implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        PointF[] pointFArr = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                pointFArr = (PointF[]) C95664e9.A0K(parcel, PointF.CREATOR, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78083oL(pointFArr, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78083oL[i];
    }
}
