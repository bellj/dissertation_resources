package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1Rf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29191Rf extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C29191Rf A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public long A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public AbstractC27881Jp A05;

    static {
        C29191Rf r0 = new C29191Rf();
        A06 = r0;
        r0.A0W();
    }

    public C29191Rf() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A04 = r0;
        this.A03 = r0;
        this.A05 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A04(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A04, 2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A03, 3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A05, 4);
        }
        if ((i3 & 16) == 16) {
            i2 += 9;
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A04, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A05, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0G(5, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
