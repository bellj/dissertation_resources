package X;

/* renamed from: X.4zR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108654zR implements AnonymousClass5XT {
    public final AnonymousClass4UP A00;
    public final AbstractC117125Yn A01;
    public final AnonymousClass4DR A02;

    public C108654zR(AnonymousClass4UP r1, AbstractC117125Yn r2, AnonymousClass4DR r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public static int A00(AnonymousClass4PN r2, byte[] bArr, int i, int i2, int i3) {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return C95504dq.A02(r2, bArr, i2);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return C95504dq.A01(r2, bArr, i2) + r2.A00;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = C95504dq.A01(r2, bArr, i2);
                    i6 = r2.A00;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = A00(r2, bArr, i6, i2, i3);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw new C868048x("Failed to parse the message.");
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw new C868048x("Protocol message contained an invalid tag (zero).");
            }
        } else {
            throw new C868048x("Protocol message contained an invalid tag (zero).");
        }
    }

    @Override // X.AnonymousClass5XT
    public final boolean A9Z(Object obj, Object obj2) {
        return ((AbstractC79123q5) obj).zzjp.equals(((AbstractC79123q5) obj2).zzjp);
    }

    @Override // X.AnonymousClass5XT
    public final int AIO(Object obj) {
        return ((AbstractC79123q5) obj).zzjp.hashCode();
    }

    @Override // X.AnonymousClass5XT
    public final Object ALd() {
        return ((AbstractC108634zP) ((AbstractC79123q5) this.A01).A05(5)).AhC();
    }

    @Override // X.AnonymousClass5XT
    public final void Agw(AbstractC115185Qn r2, Object obj) {
        throw C12980iv.A0n("zzjv");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0054 A[EDGE_INSN: B:48:0x0054->B:24:0x0054 ?: BREAK  , SYNTHETIC] */
    @Override // X.AnonymousClass5XT
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Agx(X.AnonymousClass4PN r14, java.lang.Object r15, byte[] r16, int r17, int r18) {
        /*
            r13 = this;
            r11 = r17
            X.4cr r8 = X.AbstractC108624zO.A06(r15)
        L_0x0006:
            r12 = r18
            if (r11 >= r12) goto L_0x005d
            r9 = r16
            r7 = r14
            int r11 = X.C95504dq.A01(r14, r9, r11)
            int r10 = r14.A00
            r0 = 11
            r6 = 2
            if (r10 == r0) goto L_0x0026
            r0 = r10 & 7
            if (r0 != r6) goto L_0x0021
            int r11 = X.C95504dq.A00(r7, r8, r9, r10, r11, r12)
            goto L_0x0006
        L_0x0021:
            int r11 = A00(r14, r9, r10, r11, r12)
            goto L_0x0006
        L_0x0026:
            r5 = 0
            r4 = 0
        L_0x0028:
            if (r11 >= r12) goto L_0x0054
            int r11 = X.C95504dq.A01(r14, r9, r11)
            int r3 = r14.A00
            int r2 = r3 >>> 3
            r1 = r3 & 7
            if (r2 == r6) goto L_0x0042
            r0 = 3
            if (r2 != r0) goto L_0x004b
            if (r1 != r6) goto L_0x004b
            int r11 = X.C95504dq.A03(r14, r9, r11)
            java.lang.Object r4 = r14.A02
            goto L_0x0028
        L_0x0042:
            if (r1 != 0) goto L_0x004b
            int r11 = X.C95504dq.A01(r14, r9, r11)
            int r5 = r14.A00
            goto L_0x0028
        L_0x004b:
            r0 = 12
            if (r3 == r0) goto L_0x0054
            int r11 = A00(r14, r9, r3, r11, r12)
            goto L_0x0028
        L_0x0054:
            if (r4 == 0) goto L_0x0006
            int r0 = r5 << 3
            r0 = r0 | r6
            r8.A01(r0, r4)
            goto L_0x0006
        L_0x005d:
            if (r11 != r12) goto L_0x0060
            return
        L_0x0060:
            java.lang.String r1 = "Failed to parse the message."
            X.48x r0 = new X.48x
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108654zR.Agx(X.4PN, java.lang.Object, byte[], int, int):void");
    }

    @Override // X.AnonymousClass5XT
    public final void AhF(Object obj) {
        ((AbstractC79123q5) obj).zzjp.A02 = false;
        throw C12980iv.A0n("zzjv");
    }

    @Override // X.AnonymousClass5XT
    public final void AhG(Object obj, Object obj2) {
        C95694eC.A0S(obj, obj2);
    }

    @Override // X.AnonymousClass5XT
    public final int AhO(Object obj) {
        C94984cr r7 = ((AbstractC79123q5) obj).zzjp;
        int i = r7.A01;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < r7.A00; i2++) {
                i += 2 + 1 + C72453ed.A07(r7.A03[i2] >>> 3) + AnonymousClass4UO.A05(((AbstractC111915Bh) r7.A04[i2]).A02(), C72453ed.A07(24));
            }
            r7.A01 = i;
        }
        return i;
    }

    @Override // X.AnonymousClass5XT
    public final boolean AhQ(Object obj) {
        throw C12980iv.A0n("zzjv");
    }
}
