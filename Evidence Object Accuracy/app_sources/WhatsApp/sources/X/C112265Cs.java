package X;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

/* renamed from: X.5Cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C112265Cs implements Comparator {
    public static final /* synthetic */ C112265Cs A00 = new C112265Cs();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ((Scope) obj).A01.compareTo(((Scope) obj2).A01);
    }
}
