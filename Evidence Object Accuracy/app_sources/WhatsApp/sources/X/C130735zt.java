package X;

import android.content.Context;
import android.content.ContextWrapper;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.5zt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130735zt {
    public static final Map A01 = new WeakHashMap();
    public final AnonymousClass01N A00;

    public C130735zt(AnonymousClass01N r1) {
        this.A00 = r1;
    }

    public synchronized C130435zP A00(Context context) {
        C130435zP r0;
        while (context instanceof ContextWrapper) {
            ContextWrapper contextWrapper = (ContextWrapper) context;
            if (contextWrapper.getBaseContext() == null) {
                break;
            }
            context = contextWrapper.getBaseContext();
        }
        Map map = A01;
        r0 = (C130435zP) map.get(context);
        if (r0 == null) {
            r0 = (C130435zP) this.A00.get();
            map.put(context, r0);
        }
        return r0;
    }
}
