package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106294vT implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setVisibility(((C55992k9) obj2).A02);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12980iv.A1V(((C55992k9) obj2).A02, ((C55992k9) obj).A02);
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setVisibility(0);
    }
}
