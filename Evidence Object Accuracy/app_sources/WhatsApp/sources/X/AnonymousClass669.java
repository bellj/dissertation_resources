package X;

import android.media.MediaRecorder;

/* renamed from: X.669  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass669 implements AbstractC136406Mk {
    public final /* synthetic */ AnonymousClass662 A00;

    @Override // X.AbstractC136406Mk
    public void AST(MediaRecorder mediaRecorder, int i, int i2, boolean z) {
    }

    public AnonymousClass669(AnonymousClass662 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136406Mk
    public void AVw(MediaRecorder mediaRecorder) {
        try {
            mediaRecorder.setVideoSource(2);
        } catch (Exception e) {
            AnonymousClass616.A01("Camera2Device.setVideoRecordingSource", e.getMessage());
        }
    }

    @Override // X.AbstractC136406Mk
    public void AWK(MediaRecorder mediaRecorder) {
        AnonymousClass662 r3 = this.A00;
        r3.A0d.A06("Method onStartMediaRecorder() must run on the Optic Background Thread.");
        AnonymousClass61Q r2 = r3.A0Y;
        C129805yK r1 = r2.A0H;
        r1.A01("Can only check if the prepared on the Optic thread");
        if (!r1.A00) {
            AnonymousClass616.A02("Camera2Device", "Can not start video recording, PreviewController is not prepared");
            return;
        }
        r3.A0Z.A0C = true;
        r2.A0B(mediaRecorder.getSurface());
    }
}
