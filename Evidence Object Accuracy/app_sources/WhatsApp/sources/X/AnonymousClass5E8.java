package X;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;

/* renamed from: X.5E8  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5E8 implements Executor, Closeable {
    public static final AnonymousClass4VA A07 = new AnonymousClass4VA("NOT_IN_STACK");
    public static final /* synthetic */ AtomicIntegerFieldUpdater A08 = AtomicIntegerFieldUpdater.newUpdater(AnonymousClass5E8.class, "_isTerminated");
    public static final /* synthetic */ AtomicLongFieldUpdater A09 = AtomicLongFieldUpdater.newUpdater(AnonymousClass5E8.class, "controlState");
    public static final /* synthetic */ AtomicLongFieldUpdater A0A = AtomicLongFieldUpdater.newUpdater(AnonymousClass5E8.class, "parkedWorkersStack");
    public final int A00;
    public final int A01;
    public final long A02;
    public final String A03 = "DefaultDispatcher";
    public final AtomicReferenceArray A04;
    public final AnonymousClass5LG A05;
    public final AnonymousClass5LG A06;
    public volatile /* synthetic */ int _isTerminated;
    public volatile /* synthetic */ long controlState;
    public volatile /* synthetic */ long parkedWorkersStack;

    public AnonymousClass5E8(int i, long j, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = j;
        if (i < 1) {
            StringBuilder A0k = C12960it.A0k("Core pool size ");
            A0k.append(i);
            throw C12970iu.A0f(C12960it.A0d(" should be at least 1", A0k));
        } else if (!C12990iw.A1X(i2, i)) {
            StringBuilder A0j = C12960it.A0j("Max pool size ");
            A0j.append(i2);
            throw C12970iu.A0f(C12960it.A0e(" should be greater than or equals to core pool size ", A0j, i));
        } else if (i2 > 2097150) {
            StringBuilder A0j2 = C12960it.A0j("Max pool size ");
            A0j2.append(i2);
            throw C12970iu.A0f(C12960it.A0d(" should not exceed maximal supported number of threads 2097150", A0j2));
        } else if (j > 0) {
            this.A06 = new AnonymousClass5LG();
            this.A05 = new AnonymousClass5LG();
            this.parkedWorkersStack = 0;
            this.A04 = new AtomicReferenceArray(i2 + 1);
            this.controlState = ((long) i) << 42;
            this._isTerminated = 0;
        } else {
            StringBuilder A0k2 = C12960it.A0k("Idle worker keep alive time ");
            A0k2.append(j);
            throw C12970iu.A0f(C12960it.A0d(" must be positive", A0k2));
        }
    }

    public static final void A00(AbstractRunnableC75993ks r2) {
        try {
            r2.run();
        } catch (Throwable th) {
            try {
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
            } catch (Throwable th2) {
                throw th2;
            }
        }
    }

    public final int A01() {
        int i;
        AtomicReferenceArray atomicReferenceArray = this.A04;
        synchronized (atomicReferenceArray) {
            if (this._isTerminated != 0) {
                i = -1;
            } else {
                long j = this.controlState;
                int i2 = (int) (j & 2097151);
                int i3 = i2 - ((int) ((j & 4398044413952L) >> 21));
                if (i3 < 0) {
                    i3 = 0;
                }
                if (i3 >= this.A00 || i2 >= this.A01) {
                    return 0;
                }
                int i4 = ((int) (this.controlState & 2097151)) + 1;
                if (i4 <= 0 || atomicReferenceArray.get(i4) != null) {
                    throw C12970iu.A0f("Failed requirement.");
                }
                AnonymousClass5HE r2 = new AnonymousClass5HE(this, i4);
                atomicReferenceArray.set(i4, r2);
                if (i4 == ((int) (2097151 & A09.incrementAndGet(this)))) {
                    r2.start();
                    i = i3 + 1;
                } else {
                    throw C12970iu.A0f("Failed requirement.");
                }
            }
            return i;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r2 != null) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(java.lang.Runnable r4, X.C89714La r5) {
        /*
            r3 = this;
            long r1 = java.lang.System.nanoTime()
            boolean r0 = r4 instanceof X.AbstractRunnableC75993ks
            if (r0 == 0) goto L_0x005f
            X.3ks r4 = (X.AbstractRunnableC75993ks) r4
            r4.A00 = r1
            r4.A01 = r5
        L_0x000e:
            java.lang.Thread r2 = java.lang.Thread.currentThread()
            boolean r0 = r2 instanceof X.AnonymousClass5HE
            if (r0 == 0) goto L_0x0032
            X.5HE r2 = (X.AnonymousClass5HE) r2
            if (r2 == 0) goto L_0x0032
            X.5E8 r0 = r2.A06
            boolean r0 = X.C16700pc.A0O(r0, r3)
            if (r0 == 0) goto L_0x0032
            X.4Ae r1 = r2.A03
            X.4Ae r0 = X.EnumC87094Ae.A05
            if (r1 == r0) goto L_0x0032
            X.4La r0 = r4.A01
            int r0 = r0.A00
            if (r0 != 0) goto L_0x0053
            X.4Ae r0 = X.EnumC87094Ae.A01
            if (r1 != r0) goto L_0x0053
        L_0x0032:
            r2 = r4
        L_0x0033:
            X.4La r0 = r2.A01
            int r1 = r0.A00
            r0 = 1
            if (r1 != r0) goto L_0x0050
            X.5LG r0 = r3.A05
        L_0x003c:
            boolean r0 = r0.A02(r2)
            if (r0 != 0) goto L_0x0066
            java.lang.String r1 = r3.A03
            java.lang.String r0 = " was terminated"
            java.lang.String r1 = X.C16700pc.A08(r1, r0)
            java.util.concurrent.RejectedExecutionException r0 = new java.util.concurrent.RejectedExecutionException
            r0.<init>(r1)
            throw r0
        L_0x0050:
            X.5LG r0 = r3.A06
            goto L_0x003c
        L_0x0053:
            r0 = 1
            r2.A04 = r0
            X.4bj r0 = r2.A05
            X.3ks r2 = r0.A02(r4)
            if (r2 == 0) goto L_0x0066
            goto L_0x0033
        L_0x005f:
            X.5LJ r0 = new X.5LJ
            r0.<init>(r4, r5, r1)
            r4 = r0
            goto L_0x000e
        L_0x0066:
            X.4La r0 = r4.A01
            int r0 = r0.A00
            if (r0 != 0) goto L_0x007e
            boolean r0 = r3.A04()
            if (r0 != 0) goto L_0x007d
            long r0 = r3.controlState
            boolean r0 = r3.A05(r0)
        L_0x0078:
            if (r0 != 0) goto L_0x007d
            r3.A04()
        L_0x007d:
            return
        L_0x007e:
            java.util.concurrent.atomic.AtomicLongFieldUpdater r2 = X.AnonymousClass5E8.A09
            r0 = 2097152(0x200000, double:1.0361308E-317)
            long r1 = r2.addAndGet(r3, r0)
            boolean r0 = r3.A04()
            if (r0 != 0) goto L_0x007d
            boolean r0 = r3.A05(r1)
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5E8.A02(java.lang.Runnable, X.4La):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0000, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(X.AnonymousClass5HE r10, int r11, int r12) {
        /*
            r9 = this;
        L_0x0000:
            r4 = r9
            long r5 = r9.parkedWorkersStack
            r0 = 2097151(0x1fffff, double:1.0361303E-317)
            long r0 = r0 & r5
            int r2 = (int) r0
            r7 = 2097152(0x200000, double:1.0361308E-317)
            long r7 = r7 + r5
            r0 = -2097152(0xffffffffffe00000, double:NaN)
            long r7 = r7 & r0
            if (r2 != r11) goto L_0x0031
            if (r12 != 0) goto L_0x0029
            r1 = r10
        L_0x0015:
            java.lang.Object r1 = r1.nextParkedWorker
            X.4VA r0 = X.AnonymousClass5E8.A07
            if (r1 == r0) goto L_0x0000
            if (r1 != 0) goto L_0x002b
            r2 = 0
        L_0x001e:
            java.util.concurrent.atomic.AtomicLongFieldUpdater r3 = X.AnonymousClass5E8.A0A
            long r0 = (long) r2
            long r7 = r7 | r0
            boolean r0 = r3.compareAndSet(r4, r5, r7)
            if (r0 == 0) goto L_0x0000
            return
        L_0x0029:
            r2 = r12
            goto L_0x0031
        L_0x002b:
            X.5HE r1 = (X.AnonymousClass5HE) r1
            int r2 = r1.indexInArray
            if (r2 == 0) goto L_0x0015
        L_0x0031:
            if (r2 >= 0) goto L_0x001e
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5E8.A03(X.5HE, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        if (X.AnonymousClass5E8.A0A.compareAndSet(r12, r8, ((long) r0) | r3) == false) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        r5.nextParkedWorker = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0000, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0000, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A04() {
        /*
            r12 = this;
        L_0x0000:
            r7 = r12
            long r8 = r12.parkedWorkersStack
            r2 = 2097151(0x1fffff, double:1.0361303E-317)
            long r2 = r2 & r8
            int r1 = (int) r2
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r12.A04
            java.lang.Object r5 = r0.get(r1)
            X.5HE r5 = (X.AnonymousClass5HE) r5
            if (r5 != 0) goto L_0x0025
            r5 = 0
        L_0x0013:
            r2 = 0
            if (r5 != 0) goto L_0x0017
            return r2
        L_0x0017:
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r1 = X.AnonymousClass5HE.A07
            r0 = -1
            boolean r0 = r1.compareAndSet(r5, r0, r2)
            if (r0 == 0) goto L_0x0000
            java.util.concurrent.locks.LockSupport.unpark(r5)
            r0 = 1
            return r0
        L_0x0025:
            r3 = 2097152(0x200000, double:1.0361308E-317)
            long r3 = r3 + r8
            r0 = -2097152(0xffffffffffe00000, double:NaN)
            long r3 = r3 & r0
            r2 = r5
        L_0x002e:
            java.lang.Object r2 = r2.nextParkedWorker
            X.4VA r1 = X.AnonymousClass5E8.A07
            if (r2 == r1) goto L_0x0000
            if (r2 != 0) goto L_0x0044
            r0 = 0
        L_0x0037:
            java.util.concurrent.atomic.AtomicLongFieldUpdater r6 = X.AnonymousClass5E8.A0A
            long r10 = (long) r0
            long r10 = r10 | r3
            boolean r0 = r6.compareAndSet(r7, r8, r10)
            if (r0 == 0) goto L_0x0000
            r5.nextParkedWorker = r1
            goto L_0x0013
        L_0x0044:
            X.5HE r2 = (X.AnonymousClass5HE) r2
            int r0 = r2.indexInArray
            if (r0 == 0) goto L_0x002e
            if (r0 >= 0) goto L_0x0037
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5E8.A04():boolean");
    }

    public final boolean A05(long j) {
        int i = ((int) (2097151 & j)) - ((int) ((j & 4398044413952L) >> 21));
        if (i < 0) {
            i = 0;
        }
        int i2 = this.A00;
        if (i < i2) {
            int A01 = A01();
            if (A01 == 1) {
                if (i2 > 1) {
                    A01();
                }
                return true;
            } else if (A01 > 0) {
                return true;
            }
        }
        return false;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        int i;
        AbstractRunnableC75993ks r0;
        AnonymousClass5HE r3;
        if (A08.compareAndSet(this, 0, 1)) {
            Thread currentThread = Thread.currentThread();
            AnonymousClass5HE r5 = null;
            if ((currentThread instanceof AnonymousClass5HE) && (r3 = (AnonymousClass5HE) currentThread) != null && C16700pc.A0O(r3.A06, this)) {
                r5 = r3;
            }
            AtomicReferenceArray atomicReferenceArray = this.A04;
            synchronized (atomicReferenceArray) {
                i = (int) (this.controlState & 2097151);
            }
            if (1 <= i) {
                int i2 = 1;
                while (true) {
                    int i3 = i2 + 1;
                    Object obj = atomicReferenceArray.get(i2);
                    C16700pc.A0C(obj);
                    AnonymousClass5HE r32 = (AnonymousClass5HE) obj;
                    if (r32 != r5) {
                        while (r32.isAlive()) {
                            LockSupport.unpark(r32);
                            r32.join(10000);
                        }
                        C94384bj r7 = r32.A05;
                        AnonymousClass5LG r4 = this.A05;
                        Object andSet = C94384bj.A04.getAndSet(r7, null);
                        if (andSet != null) {
                            r4.A02(andSet);
                        }
                        while (true) {
                            AbstractRunnableC75993ks A01 = r7.A01();
                            if (A01 == null) {
                                break;
                            }
                            r4.A02(A01);
                        }
                    }
                    if (i2 == i) {
                        break;
                    }
                    i2 = i3;
                }
            }
            AnonymousClass5LG r42 = this.A05;
            while (true) {
                C94454bq r2 = (C94454bq) r42._cur;
                if (r2.A03()) {
                    break;
                }
                AnonymousClass0KN.A00(r42, r2, r2.A02(), C94304ba.A00);
            }
            AnonymousClass5LG r33 = this.A06;
            while (true) {
                C94454bq r22 = (C94454bq) r33._cur;
                if (!r22.A03()) {
                    AnonymousClass0KN.A00(r33, r22, r22.A02(), C94304ba.A00);
                }
            }
            while (true) {
                if (r5 != null) {
                    r0 = r5.A01(true);
                    if (r0 != null) {
                        continue;
                        A00(r0);
                    }
                }
                r0 = (AbstractRunnableC75993ks) r33.A01();
                if (r0 == null && (r0 = (AbstractRunnableC75993ks) r42.A01()) == null) {
                    break;
                }
                A00(r0);
            }
            if (r5 != null) {
                r5.A04(EnumC87094Ae.A05);
            }
            this.parkedWorkersStack = 0;
            this.controlState = 0;
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        A02(runnable, C88864Hl.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb;
        char c;
        ArrayList A0l = C12960it.A0l();
        AtomicReferenceArray atomicReferenceArray = this.A04;
        int length = atomicReferenceArray.length();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 1;
        while (i6 < length) {
            int i7 = i6 + 1;
            AnonymousClass5HE r11 = (AnonymousClass5HE) atomicReferenceArray.get(i6);
            if (r11 != null) {
                C94384bj r0 = r11.A05;
                Object obj = r0.lastScheduledTask;
                int i8 = r0.producerIndex - r0.consumerIndex;
                if (obj != null) {
                    i8++;
                }
                switch (r11.A03.ordinal()) {
                    case 0:
                        i++;
                        sb = C12960it.A0h();
                        sb.append(i8);
                        c = 'c';
                        A0l.add(C72463ee.A0H(sb, c));
                        break;
                    case 1:
                        i2++;
                        sb = C12960it.A0h();
                        sb.append(i8);
                        c = 'b';
                        A0l.add(C72463ee.A0H(sb, c));
                        break;
                    case 2:
                        i3++;
                        break;
                    case 3:
                        i4++;
                        if (i8 <= 0) {
                            break;
                        } else {
                            sb = C12960it.A0h();
                            sb.append(i8);
                            c = 'd';
                            A0l.add(C72463ee.A0H(sb, c));
                            break;
                        }
                    case 4:
                        i5++;
                        break;
                }
            }
            i6 = i7;
        }
        long j = this.controlState;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A03);
        A0h.append('@');
        A0h.append(C72453ed.A0o(this));
        A0h.append("[Pool Size {core = ");
        int i9 = this.A00;
        A0h.append(i9);
        A0h.append(", max = ");
        A0h.append(this.A01);
        A0h.append("}, Worker States {CPU = ");
        A0h.append(i);
        A0h.append(", blocking = ");
        A0h.append(i2);
        A0h.append(", parked = ");
        A0h.append(i3);
        A0h.append(", dormant = ");
        A0h.append(i4);
        A0h.append(", terminated = ");
        A0h.append(i5);
        A0h.append("}, running workers queues = ");
        A0h.append(A0l);
        A0h.append(", global CPU queue size = ");
        A0h.append(this.A06.A00());
        A0h.append(", global blocking queue size = ");
        A0h.append(this.A05.A00());
        A0h.append(", Control State {created workers= ");
        A0h.append((int) (2097151 & j));
        A0h.append(", blocking tasks = ");
        A0h.append((int) ((4398044413952L & j) >> 21));
        A0h.append(", CPUs acquired = ");
        A0h.append(i9 - ((int) ((9223367638808264704L & j) >> 42)));
        return C12960it.A0d("}]", A0h);
    }
}
