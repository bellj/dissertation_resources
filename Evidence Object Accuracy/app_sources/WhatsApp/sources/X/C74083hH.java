package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* renamed from: X.3hH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74083hH extends BaseAdapter {
    public final ArrayList A00;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return 0;
    }

    public C74083hH(ArrayList arrayList) {
        this.A00 = arrayList;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A00.size();
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return this.A00.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        return (View) getItem(i);
    }
}
