package X;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0F6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0F6 extends AbstractC018308n implements AnonymousClass045 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public int A08 = 0;
    public int A09 = -1;
    public int A0A = -1;
    public int A0B;
    public int A0C;
    public long A0D;
    public Rect A0E;
    public VelocityTracker A0F;
    public View A0G = null;
    public AnonymousClass0MU A0H;
    public AbstractC06200So A0I;
    public AnonymousClass0Av A0J;
    public AbstractC11340g8 A0K = null;
    public AnonymousClass03U A0L = null;
    public RecyclerView A0M;
    public List A0N;
    public List A0O = new ArrayList();
    public List A0P;
    public final AbstractC12560i7 A0Q = new AnonymousClass0Z9(this);
    public final Runnable A0R = new RunnableC09290cf(this);
    public final List A0S = new ArrayList();
    public final float[] A0T = new float[2];

    @Override // X.AnonymousClass045
    public void ANv(View view) {
    }

    public AnonymousClass0F6(AbstractC06200So r4) {
        this.A0I = r4;
    }

    @Override // X.AbstractC018308n
    public void A00(Canvas canvas, C05480Ps r22, RecyclerView recyclerView) {
        float f;
        float f2;
        float f3;
        float f4;
        this.A0A = -1;
        if (this.A0L != null) {
            float[] fArr = this.A0T;
            A0D(fArr);
            f = fArr[0];
            f2 = fArr[1];
        } else {
            f = 0.0f;
            f2 = 0.0f;
        }
        AnonymousClass03U r3 = this.A0L;
        List list = this.A0O;
        int i = this.A08;
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            C06590Ug r7 = (C06590Ug) list.get(i2);
            float f5 = r7.A06;
            float f6 = r7.A08;
            if (f5 == f6) {
                f3 = r7.A0C.A0H.getTranslationX();
            } else {
                f3 = f5 + (r7.A00 * (f6 - f5));
            }
            r7.A01 = f3;
            float f7 = r7.A07;
            float f8 = r7.A09;
            if (f7 == f8) {
                f4 = r7.A0C.A0H.getTranslationY();
            } else {
                f4 = f7 + (r7.A00 * (f8 - f7));
            }
            r7.A02 = f4;
            int save = canvas.save();
            AnonymousClass03U r0 = r7.A0C;
            AnonymousClass0Z1.A00.APW(canvas, r0.A0H, recyclerView, r7.A01, r7.A02, r7.A0A, false);
            canvas.restoreToCount(save);
        }
        if (r3 != null) {
            int save2 = canvas.save();
            AnonymousClass0Z1.A00.APW(canvas, r3.A0H, recyclerView, f, f2, i, true);
            canvas.restoreToCount(save2);
        }
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r3, RecyclerView recyclerView) {
        rect.setEmpty();
    }

    @Override // X.AbstractC018308n
    public void A02(Canvas canvas, C05480Ps r8, RecyclerView recyclerView) {
        if (this.A0L != null) {
            A0D(this.A0T);
        }
        AnonymousClass03U r5 = this.A0L;
        List list = this.A0O;
        int size = list.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            list.get(i);
            canvas.restoreToCount(canvas.save());
        }
        if (r5 != null) {
            canvas.restoreToCount(canvas.save());
        }
        for (int i2 = size - 1; i2 >= 0; i2--) {
            C06590Ug r1 = (C06590Ug) list.get(i2);
            if (!r1.A03) {
                z = true;
            } else if (!r1.A04) {
                list.remove(i2);
            }
        }
        if (z) {
            recyclerView.invalidate();
        }
    }

    public final int A03(int i) {
        if ((i & 12) == 0) {
            return 0;
        }
        int i2 = 8;
        int i3 = 4;
        if (this.A00 > 0.0f) {
            i3 = 8;
        }
        VelocityTracker velocityTracker = this.A0F;
        if (velocityTracker != null && this.A09 > -1) {
            velocityTracker.computeCurrentVelocity(1000, this.A04);
            float xVelocity = this.A0F.getXVelocity(this.A09);
            float yVelocity = this.A0F.getYVelocity(this.A09);
            if (xVelocity <= 0.0f) {
                i2 = 4;
            }
            float abs = Math.abs(xVelocity);
            if ((i2 & i) != 0 && i3 == i2 && abs >= this.A07 && abs > Math.abs(yVelocity)) {
                return i2;
            }
        }
        float width = ((float) this.A0M.getWidth()) * 0.5f;
        if ((i & i3) == 0 || Math.abs(this.A00) <= width) {
            return 0;
        }
        return i3;
    }

    public final int A04(int i) {
        if ((i & 3) == 0) {
            return 0;
        }
        int i2 = 2;
        int i3 = 1;
        if (this.A01 > 0.0f) {
            i3 = 2;
        }
        VelocityTracker velocityTracker = this.A0F;
        if (velocityTracker != null && this.A09 > -1) {
            velocityTracker.computeCurrentVelocity(1000, this.A04);
            float xVelocity = this.A0F.getXVelocity(this.A09);
            float yVelocity = this.A0F.getYVelocity(this.A09);
            if (yVelocity <= 0.0f) {
                i2 = 1;
            }
            float abs = Math.abs(yVelocity);
            if ((i2 & i) != 0 && i2 == i3 && abs >= this.A07 && abs > Math.abs(xVelocity)) {
                return i2;
            }
        }
        float height = ((float) this.A0M.getHeight()) * 0.5f;
        if ((i & i3) == 0 || Math.abs(this.A01) <= height) {
            return 0;
        }
        return i3;
    }

    public View A05(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        AnonymousClass03U r0 = this.A0L;
        if (r0 != null) {
            View view = r0.A0H;
            float f = this.A05 + this.A00;
            float f2 = this.A06 + this.A01;
            if (x >= f && x <= f + ((float) view.getWidth()) && y >= f2 && y <= f2 + ((float) view.getHeight())) {
                return view;
            }
        }
        List list = this.A0O;
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                C06590Ug r1 = (C06590Ug) list.get(size);
                View view2 = r1.A0C.A0H;
                float f3 = r1.A01;
                float f4 = r1.A02;
                if (x >= f3 && x <= f3 + ((float) view2.getWidth()) && y >= f4 && y <= f4 + ((float) view2.getHeight())) {
                    return view2;
                }
            } else {
                RecyclerView recyclerView = this.A0M;
                int A00 = recyclerView.A0K.A00();
                while (true) {
                    A00--;
                    if (A00 < 0) {
                        return null;
                    }
                    View A03 = recyclerView.A0K.A03(A00);
                    float translationX = A03.getTranslationX();
                    float translationY = A03.getTranslationY();
                    if (x >= ((float) A03.getLeft()) + translationX && x <= ((float) A03.getRight()) + translationX && y >= ((float) A03.getTop()) + translationY && y <= ((float) A03.getBottom()) + translationY) {
                        return A03;
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x00c3 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.view.MotionEvent r10, int r11, int r12) {
        /*
            r9 = this;
            X.03U r0 = r9.A0L
            if (r0 != 0) goto L_0x0048
            r1 = 2
            if (r11 != r1) goto L_0x0048
            int r0 = r9.A08
            if (r0 == r1) goto L_0x0048
            X.0So r4 = r9.A0I
            boolean r0 = r4.A04()
            if (r0 == 0) goto L_0x0048
            androidx.recyclerview.widget.RecyclerView r1 = r9.A0M
            int r0 = r1.A0B
            r3 = 1
            if (r0 == r3) goto L_0x0048
            X.02H r6 = r1.getLayoutManager()
            int r1 = r9.A09
            r0 = -1
            if (r1 == r0) goto L_0x0048
            int r1 = r10.findPointerIndex(r1)
            float r2 = r10.getX(r1)
            float r0 = r9.A02
            float r2 = r2 - r0
            float r1 = r10.getY(r1)
            float r0 = r9.A03
            float r1 = r1 - r0
            float r5 = java.lang.Math.abs(r2)
            float r2 = java.lang.Math.abs(r1)
            int r0 = r9.A0C
            float r1 = (float) r0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0049
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0049
        L_0x0048:
            return
        L_0x0049:
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0054
            boolean r0 = r6.A14()
            if (r0 == 0) goto L_0x0054
            return
        L_0x0054:
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x005f
            boolean r0 = r6.A15()
            if (r0 == 0) goto L_0x005f
            return
        L_0x005f:
            android.view.View r1 = r9.A05(r10)
            if (r1 == 0) goto L_0x0048
            androidx.recyclerview.widget.RecyclerView r0 = r9.A0M
            X.03U r2 = r0.A0E(r1)
            if (r2 == 0) goto L_0x0048
            androidx.recyclerview.widget.RecyclerView r0 = r9.A0M
            int r1 = r4.A00(r2, r0)
            r0 = 65280(0xff00, float:9.1477E-41)
            r1 = r1 & r0
            int r8 = r1 >> 8
            if (r8 == 0) goto L_0x0048
            float r7 = r10.getX(r12)
            float r6 = r10.getY(r12)
            float r0 = r9.A02
            float r7 = r7 - r0
            float r0 = r9.A03
            float r6 = r6 - r0
            float r5 = java.lang.Math.abs(r7)
            float r4 = java.lang.Math.abs(r6)
            int r0 = r9.A0C
            float r1 = (float) r0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x009d
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x009d
            return
        L_0x009d:
            r1 = 0
            int r0 = (r5 > r4 ? 1 : (r5 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ab
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bb
            r0 = r8 & 4
            if (r0 != 0) goto L_0x00bb
            return
        L_0x00ab:
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00b4
            r0 = r8 & 1
            if (r0 != 0) goto L_0x00b4
            return
        L_0x00b4:
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c4
            r0 = r8 & 2
            goto L_0x00c1
        L_0x00bb:
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c4
            r0 = r8 & 8
        L_0x00c1:
            if (r0 != 0) goto L_0x00c4
            return
        L_0x00c4:
            r9.A01 = r1
            r9.A00 = r1
            r0 = 0
            int r0 = r10.getPointerId(r0)
            r9.A09 = r0
            r9.A0A(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0F6.A06(android.view.MotionEvent, int, int):void");
    }

    public void A07(MotionEvent motionEvent, int i, int i2) {
        float x = motionEvent.getX(i2);
        float y = motionEvent.getY(i2);
        float f = x - this.A02;
        this.A00 = f;
        float f2 = y - this.A03;
        this.A01 = f2;
        if ((i & 4) == 0) {
            f = Math.max(0.0f, f);
            this.A00 = f;
        }
        if ((i & 8) == 0) {
            this.A00 = Math.min(0.0f, f);
        }
        if ((i & 1) == 0) {
            f2 = Math.max(0.0f, f2);
            this.A01 = f2;
        }
        if ((i & 2) == 0) {
            this.A01 = Math.min(0.0f, f2);
        }
    }

    public void A08(View view) {
        if (view == this.A0G) {
            this.A0G = null;
            if (this.A0K != null) {
                this.A0M.setChildDrawingOrderCallback(null);
            }
        }
    }

    public void A09(AnonymousClass03U r22) {
        int i;
        int i2;
        int i3;
        int left;
        int bottom;
        if (!this.A0M.isLayoutRequested() && this.A08 == 2) {
            AbstractC06200So r0 = this.A0I;
            int i4 = (int) (this.A05 + this.A00);
            int i5 = (int) (this.A06 + this.A01);
            View view = r22.A0H;
            if (((float) Math.abs(i5 - view.getTop())) >= ((float) view.getHeight()) * 0.5f || ((float) Math.abs(i4 - view.getLeft())) >= ((float) view.getWidth()) * 0.5f) {
                List list = this.A0P;
                if (list == null) {
                    this.A0P = new ArrayList();
                    this.A0N = new ArrayList();
                } else {
                    list.clear();
                    this.A0N.clear();
                }
                int round = Math.round(this.A05 + this.A00) - 0;
                int round2 = Math.round(this.A06 + this.A01) - 0;
                int width = view.getWidth() + round + 0;
                int height = view.getHeight() + round2 + 0;
                int i6 = (round + width) >> 1;
                int i7 = (round2 + height) >> 1;
                AnonymousClass02H layoutManager = this.A0M.getLayoutManager();
                int A06 = layoutManager.A06();
                for (int i8 = 0; i8 < A06; i8++) {
                    View A0D = layoutManager.A0D(i8);
                    if (A0D != view && A0D.getBottom() >= round2 && A0D.getTop() <= height && A0D.getRight() >= round && A0D.getLeft() <= width) {
                        AnonymousClass03U A0E = this.A0M.A0E(A0D);
                        if (r0.A06(this.A0L, A0E, this.A0M)) {
                            int abs = Math.abs(i6 - ((A0D.getLeft() + A0D.getRight()) >> 1));
                            int abs2 = Math.abs(i7 - ((A0D.getTop() + A0D.getBottom()) >> 1));
                            int i9 = (abs * abs) + (abs2 * abs2);
                            int size = this.A0P.size();
                            int i10 = 0;
                            int i11 = 0;
                            while (i10 < size && i9 > ((Number) this.A0N.get(i10)).intValue()) {
                                i11++;
                                i10++;
                            }
                            this.A0P.add(i11, A0E);
                            this.A0N.add(i11, Integer.valueOf(i9));
                        }
                    }
                }
                List list2 = this.A0P;
                if (list2.size() != 0) {
                    int width2 = i4 + view.getWidth();
                    int height2 = i5 + view.getHeight();
                    int left2 = i4 - view.getLeft();
                    int top = i5 - view.getTop();
                    int size2 = list2.size();
                    AnonymousClass03U r3 = null;
                    int i12 = -1;
                    for (int i13 = 0; i13 < size2; i13++) {
                        AnonymousClass03U r2 = (AnonymousClass03U) list2.get(i13);
                        if (left2 <= 0 ? !(left2 >= 0 || (left = r2.A0H.getLeft() - i4) <= 0 || r2.A0H.getLeft() >= view.getLeft()) : !((left = r2.A0H.getRight() - width2) >= 0 || r2.A0H.getRight() <= view.getRight())) {
                            int abs3 = Math.abs(left);
                            if (abs3 > i12) {
                                r3 = r2;
                                i12 = abs3;
                            }
                        }
                        if (top >= 0 ? !(top <= 0 || (bottom = r2.A0H.getBottom() - height2) >= 0 || r2.A0H.getBottom() <= view.getBottom()) : !((bottom = r2.A0H.getTop() - i5) <= 0 || r2.A0H.getTop() >= view.getTop())) {
                            int abs4 = Math.abs(bottom);
                            if (abs4 > i12) {
                                r3 = r2;
                                i12 = abs4;
                            }
                        }
                    }
                    if (r3 == null) {
                        this.A0P.clear();
                        this.A0N.clear();
                        return;
                    }
                    int A00 = r3.A00();
                    r22.A00();
                    if (r0.A07(r22, r3, this.A0M)) {
                        RecyclerView recyclerView = this.A0M;
                        AnonymousClass02H layoutManager2 = recyclerView.getLayoutManager();
                        if (layoutManager2 instanceof AnonymousClass02J) {
                            View view2 = r3.A0H;
                            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) ((AnonymousClass02J) layoutManager2);
                            linearLayoutManager.A13("Cannot drop a view during a scroll or layout calculation");
                            linearLayoutManager.A1O();
                            linearLayoutManager.A1P();
                            int A02 = AnonymousClass02H.A02(view);
                            int A022 = AnonymousClass02H.A02(view2);
                            char c = 65535;
                            if (A02 < A022) {
                                c = 1;
                            }
                            boolean z = linearLayoutManager.A09;
                            AbstractC06220Sq r02 = linearLayoutManager.A06;
                            if (z) {
                                if (c == 1) {
                                    linearLayoutManager.A1R(A022, r02.A02() - (linearLayoutManager.A06.A0B(view2) + linearLayoutManager.A06.A09(view)));
                                    return;
                                } else {
                                    i = r02.A02();
                                    i2 = linearLayoutManager.A06.A08(view2);
                                }
                            } else if (c == 65535) {
                                i3 = r02.A0B(view2);
                                linearLayoutManager.A1R(A022, i3);
                                return;
                            } else {
                                i = r02.A08(view2);
                                i2 = linearLayoutManager.A06.A09(view);
                            }
                            i3 = i - i2;
                            linearLayoutManager.A1R(A022, i3);
                            return;
                        }
                        if (layoutManager2.A14()) {
                            View view3 = r3.A0H;
                            if (view3.getLeft() - ((AnonymousClass0B6) view3.getLayoutParams()).A03.left <= recyclerView.getPaddingLeft()) {
                                recyclerView.A0Y(A00);
                            }
                            if (view3.getRight() + ((AnonymousClass0B6) view3.getLayoutParams()).A03.right >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                                recyclerView.A0Y(A00);
                            }
                        }
                        if (layoutManager2.A15()) {
                            View view4 = r3.A0H;
                            if (view4.getTop() - ((AnonymousClass0B6) view4.getLayoutParams()).A03.top <= recyclerView.getPaddingTop()) {
                                recyclerView.A0Y(A00);
                            }
                            if (view4.getBottom() + ((AnonymousClass0B6) view4.getLayoutParams()).A03.bottom >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                                recyclerView.A0Y(A00);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a2, code lost:
        if (r1 > 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0198, code lost:
        if (r1 > 0) goto L_0x019a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b1 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0164  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(X.AnonymousClass03U r27, int r28) {
        /*
        // Method dump skipped, instructions count: 467
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0F6.A0A(X.03U, int):void");
    }

    public void A0B(AnonymousClass03U r5, boolean z) {
        C06590Ug r1;
        List list = this.A0O;
        int size = list.size();
        do {
            size--;
            if (size >= 0) {
                r1 = (C06590Ug) list.get(size);
            } else {
                return;
            }
        } while (r1.A0C != r5);
        r1.A05 |= z;
        if (!r1.A03) {
            r1.A0B.cancel();
        }
        list.remove(size);
    }

    public void A0C(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.A0M;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                recyclerView2.A0m(this);
                RecyclerView recyclerView3 = this.A0M;
                AbstractC12560i7 r1 = this.A0Q;
                recyclerView3.A14.remove(r1);
                if (recyclerView3.A0U == r1) {
                    recyclerView3.A0U = null;
                }
                List list = this.A0M.A0a;
                if (list != null) {
                    list.remove(this);
                }
                List list2 = this.A0O;
                int size = list2.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    AnonymousClass0Z1.A00.A7F(((C06590Ug) list2.get(0)).A0C.A0H);
                }
                list2.clear();
                this.A0G = null;
                this.A0A = -1;
                VelocityTracker velocityTracker = this.A0F;
                if (velocityTracker != null) {
                    velocityTracker.recycle();
                    this.A0F = null;
                }
                AnonymousClass0Av r2 = this.A0J;
                if (r2 != null) {
                    r2.A00 = false;
                    this.A0J = null;
                }
                if (this.A0H != null) {
                    this.A0H = null;
                }
            }
            this.A0M = recyclerView;
            if (recyclerView != null) {
                Resources resources = recyclerView.getResources();
                this.A07 = resources.getDimension(R.dimen.item_touch_helper_swipe_escape_velocity);
                this.A04 = resources.getDimension(R.dimen.item_touch_helper_swipe_escape_max_velocity);
                this.A0C = ViewConfiguration.get(this.A0M.getContext()).getScaledTouchSlop();
                this.A0M.A0l(this);
                RecyclerView recyclerView4 = this.A0M;
                recyclerView4.A14.add(this.A0Q);
                List list3 = recyclerView4.A0a;
                if (list3 == null) {
                    list3 = new ArrayList();
                    recyclerView4.A0a = list3;
                }
                list3.add(this);
                this.A0J = new AnonymousClass0Av(this);
                this.A0H = new AnonymousClass0MU(this.A0M.getContext(), this.A0J);
            }
        }
    }

    public final void A0D(float[] fArr) {
        float translationX;
        float translationY;
        if ((this.A0B & 12) != 0) {
            translationX = (this.A05 + this.A00) - ((float) this.A0L.A0H.getLeft());
        } else {
            translationX = this.A0L.A0H.getTranslationX();
        }
        fArr[0] = translationX;
        if ((this.A0B & 3) != 0) {
            translationY = (this.A06 + this.A01) - ((float) this.A0L.A0H.getTop());
        } else {
            translationY = this.A0L.A0H.getTranslationY();
        }
        fArr[1] = translationY;
    }

    @Override // X.AnonymousClass045
    public void ANw(View view) {
        A08(view);
        AnonymousClass03U A0E = this.A0M.A0E(view);
        if (A0E != null) {
            AnonymousClass03U r0 = this.A0L;
            if (r0 == null || A0E != r0) {
                A0B(A0E, false);
                List list = this.A0S;
                View view2 = A0E.A0H;
                if (list.remove(view2)) {
                    AnonymousClass0Z1.A00.A7F(view2);
                    return;
                }
                return;
            }
            A0A(null, 0);
        }
    }
}
