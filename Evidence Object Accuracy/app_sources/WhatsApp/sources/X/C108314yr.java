package X;

import android.os.Bundle;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108314yr implements AnonymousClass5XN {
    public final C108364yw A00;

    public C108314yr(C108364yw r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XN
    public final void AgZ(Bundle bundle) {
    }

    @Override // X.AnonymousClass5XN
    public final void Aga(C56492ky r1, AnonymousClass1UE r2, boolean z) {
    }

    @Override // X.AnonymousClass5XN
    public final void Agb(int i) {
    }

    @Override // X.AnonymousClass5XN
    public final boolean Agc() {
        return true;
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgM(AnonymousClass1UI r2) {
        this.A00.A05.A0J.add(r2);
        return r2;
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgO(AnonymousClass1UI r2) {
        throw C12960it.A0U("GoogleApiClient is not connected yet.");
    }

    @Override // X.AnonymousClass5XN
    public final void AgT() {
        C108364yw r2 = this.A00;
        Iterator A0o = C12960it.A0o(r2.A09);
        while (A0o.hasNext()) {
            ((AbstractC72443eb) A0o.next()).A8y();
        }
        r2.A05.A04 = Collections.emptySet();
    }

    @Override // X.AnonymousClass5XN
    public final void AgW() {
        C108364yw r4 = this.A00;
        Lock lock = r4.A0D;
        lock.lock();
        try {
            AnonymousClass3BW r5 = r4.A08;
            Map map = r4.A0B;
            r4.A0E = new C108324ys(r4.A02, r4.A03, r4.A04, r4, r5, map, lock);
            r4.A0E.AgT();
            r4.A0C.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
