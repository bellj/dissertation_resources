package X;

import com.whatsapp.chatinfo.ListChatInfo;
import java.util.Set;

/* renamed from: X.44W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44W extends AbstractC33331dp {
    public final /* synthetic */ ListChatInfo A00;

    public AnonymousClass44W(ListChatInfo listChatInfo) {
        this.A00 = listChatInfo;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        ListChatInfo.A02(this.A00);
    }
}
