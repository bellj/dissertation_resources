package X;

/* renamed from: X.3Tc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67843Tc implements AbstractC116415Vi {
    public AnonymousClass28D A00;
    public final C14250l6 A01;
    public final String A02;

    public /* synthetic */ C67843Tc(C14250l6 r1, String str) {
        this.A02 = str;
        this.A01 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r7.A0H().equals(r5) == false) goto L_0x0013;
     */
    @Override // X.AbstractC116415Vi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass28D A62(X.AnonymousClass28D r7) {
        /*
            r6 = this;
            java.lang.String r5 = r6.A02
            java.lang.String r0 = r7.A0H()
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = r7.A0H()
            boolean r1 = r0.equals(r5)
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            java.lang.String r4 = "Multiple components with the same id found during reflow"
            if (r0 == 0) goto L_0x001e
            X.28D r0 = r6.A00
            if (r0 != 0) goto L_0x0058
            r6.A00 = r7
        L_0x001e:
            java.util.LinkedList r0 = r7.A05
            if (r0 == 0) goto L_0x0057
            java.util.Iterator r3 = r0.iterator()
        L_0x0026:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0057
            java.lang.Object r1 = r3.next()
            X.4RF r1 = (X.AnonymousClass4RF) r1
            java.lang.String r0 = r1.A03
            if (r0 == 0) goto L_0x0042
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0042
            X.28D r0 = r6.A00
            if (r0 != 0) goto L_0x0052
            r6.A00 = r7
        L_0x0042:
            X.28D r0 = r6.A00
            if (r0 == 0) goto L_0x0026
            X.0l1 r2 = r1.A01
            if (r2 == 0) goto L_0x0026
            X.0l6 r1 = r6.A01
            X.0l3 r0 = X.C14220l3.A01
            r1.A01(r0, r2)
            goto L_0x0026
        L_0x0052:
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r4)
            throw r0
        L_0x0057:
            return r7
        L_0x0058:
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67843Tc.A62(X.28D):X.28D");
    }

    @Override // X.AbstractC116415Vi
    public void AY4(AnonymousClass28D r2) {
        if (this.A00 == r2) {
            this.A00 = null;
        }
    }
}
