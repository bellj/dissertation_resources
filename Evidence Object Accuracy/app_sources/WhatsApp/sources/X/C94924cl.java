package X;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94924cl {
    public static HandlerThread A05;
    public static C94924cl A06;
    public static final Object A07 = C12970iu.A0l();
    public final Context A00;
    public final C98204iJ A01;
    public final AnonymousClass3IW A02;
    public final HashMap A03 = C12970iu.A11();
    public volatile Handler A04;

    public C94924cl(Context context, Looper looper) {
        C98204iJ r1 = new C98204iJ(this);
        this.A01 = r1;
        this.A00 = context.getApplicationContext();
        this.A04 = new HandlerC73383g9(looper, r1);
        this.A02 = AnonymousClass3IW.A00();
    }

    public static C94924cl A00(Context context) {
        synchronized (A07) {
            if (A06 == null) {
                A06 = new C94924cl(context.getApplicationContext(), context.getMainLooper());
            }
        }
        return A06;
    }

    public final void A01(ServiceConnection serviceConnection, C65083Ib r7) {
        HashMap hashMap = this.A03;
        synchronized (hashMap) {
            ServiceConnectionC97804hf r0 = (ServiceConnectionC97804hf) hashMap.get(r7);
            if (r0 != null) {
                Map map = r0.A05;
                if (map.containsKey(serviceConnection)) {
                    map.remove(serviceConnection);
                    if (map.isEmpty()) {
                        this.A04.sendMessageDelayed(this.A04.obtainMessage(0, r7), 5000);
                    }
                } else {
                    String obj = r7.toString();
                    StringBuilder A0t = C12980iv.A0t(obj.length() + 76);
                    A0t.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                    throw C12960it.A0U(C12960it.A0d(obj, A0t));
                }
            } else {
                String obj2 = r7.toString();
                StringBuilder A0t2 = C12980iv.A0t(obj2.length() + 50);
                A0t2.append("Nonexistent connection status for service config: ");
                throw C12960it.A0U(C12960it.A0d(obj2, A0t2));
            }
        }
    }

    public final boolean A02(ServiceConnection serviceConnection, C65083Ib r6, String str) {
        boolean z;
        HashMap hashMap = this.A03;
        synchronized (hashMap) {
            ServiceConnectionC97804hf r2 = (ServiceConnectionC97804hf) hashMap.get(r6);
            if (r2 == null) {
                r2 = new ServiceConnectionC97804hf(r6, this);
                r2.A05.put(serviceConnection, serviceConnection);
                r2.A00(str);
                hashMap.put(r6, r2);
            } else {
                this.A04.removeMessages(0, r6);
                Map map = r2.A05;
                if (!map.containsKey(serviceConnection)) {
                    map.put(serviceConnection, serviceConnection);
                    int i = r2.A00;
                    if (i == 1) {
                        serviceConnection.onServiceConnected(r2.A01, r2.A02);
                    } else if (i == 2) {
                        r2.A00(str);
                    }
                } else {
                    String obj = r6.toString();
                    StringBuilder A0t = C12980iv.A0t(obj.length() + 81);
                    A0t.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    throw C12960it.A0U(C12960it.A0d(obj, A0t));
                }
            }
            z = r2.A03;
        }
        return z;
    }
}
