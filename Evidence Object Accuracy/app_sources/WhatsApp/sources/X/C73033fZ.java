package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView;

/* renamed from: X.3fZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73033fZ extends BitmapDrawable {
    public final /* synthetic */ ConversationRowImage$RowImageView A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73033fZ(Resources resources, Bitmap bitmap, ConversationRowImage$RowImageView conversationRowImage$RowImageView) {
        super(resources, bitmap);
        this.A00 = conversationRowImage$RowImageView;
    }

    @Override // android.graphics.drawable.BitmapDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.A00.A01.A06;
    }

    @Override // android.graphics.drawable.BitmapDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.A00.A01.A08;
    }
}
