package X;

/* renamed from: X.4Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C87394Bi extends Error {
    public C87394Bi() {
        super("An operation is not implemented.");
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C87394Bi(String str) {
        super(str);
        C16700pc.A0E(str, 1);
    }
}
