package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.polls.PollResultsViewModel;

/* renamed from: X.1J4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1J4 extends AnonymousClass02L {
    public final AnonymousClass1J2 A00;
    public final AnonymousClass1J3 A01;
    public final AnonymousClass1J1 A02;
    public final PollResultsViewModel A03;

    public AnonymousClass1J4(AnonymousClass02K r1, AnonymousClass1J2 r2, AnonymousClass1J3 r3, AnonymousClass1J1 r4, PollResultsViewModel pollResultsViewModel) {
        super(r1);
        this.A03 = pollResultsViewModel;
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r9, int i) {
        AnonymousClass1J1 r2;
        C15370n3 A0B;
        if ((r9 instanceof C75533k0) && (A0E(i) instanceof C1111558h)) {
            C75533k0 r92 = (C75533k0) r9;
            C1111558h r22 = (C1111558h) A0E(i);
            r92.A01.setText(r22.A03);
            WaTextView waTextView = r92.A00;
            AnonymousClass018 r6 = r92.A02;
            int i2 = r22.A00;
            waTextView.setText(r6.A0I(new Object[]{Integer.valueOf(i2)}, R.plurals.n_votes, (long) i2));
        } else if ((r9 instanceof C75603k7) && (A0E(i) instanceof C27721Iy)) {
            C75603k7 r93 = (C75603k7) r9;
            C27721Iy r23 = (C27721Iy) A0E(i);
            r93.A02.setText(r23.A00);
            C27711Iw r24 = r23.A01;
            if (r24 == null) {
                r93.A01.setVisibility(8);
                return;
            }
            WaImageView waImageView = r93.A01;
            waImageView.setVisibility(0);
            AnonymousClass1IS r1 = r24.A0z;
            if (r1.A02) {
                C15570nT r12 = r93.A00;
                r12.A08();
                if (r12.A01 != null) {
                    r2 = r93.A04;
                    r12.A08();
                    A0B = r12.A01;
                } else {
                    return;
                }
            } else {
                AbstractC14640lm r13 = r1.A00;
                if (C15380n4.A0J(r13)) {
                    r13 = r24.A0B();
                }
                AnonymousClass009.A05(r13);
                r2 = r93.A04;
                A0B = r93.A03.A0B(r13);
            }
            r2.A06(waImageView, A0B);
        } else if ((r9 instanceof C75463jt) && (A0E(i) instanceof C1111458g)) {
            ((C75463jt) r9).A00 = ((AbstractC27731Iz) A0E(i)).AGJ();
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C75533k0(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.poll_results_option, viewGroup, false), (AnonymousClass018) this.A00.A00.A03.ANb.get());
        }
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i != 1) {
            return new C75463jt(from.inflate(R.layout.poll_results_see_all, viewGroup, false), this.A03);
        }
        View inflate = from.inflate(R.layout.poll_results_user, viewGroup, false);
        AnonymousClass1J3 r0 = this.A01;
        AnonymousClass1J1 r3 = this.A02;
        AnonymousClass01J r1 = r0.A00.A03;
        return new C75603k7(inflate, (C15570nT) r1.AAr.get(), (C15550nR) r1.A45.get(), r3);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AbstractC27731Iz) A0E(i)).AHd();
    }
}
