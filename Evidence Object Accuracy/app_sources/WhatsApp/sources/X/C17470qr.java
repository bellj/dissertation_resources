package X;

import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.0qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17470qr implements AbstractC136386Mi {
    public final AnonymousClass018 A00;

    public C17470qr(AnonymousClass018 r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119415dw A8P(WaBloksActivity waBloksActivity) {
        return new AnonymousClass487(this.A00, waBloksActivity);
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119405dv A8Q(WaBloksActivity waBloksActivity, C129125xD r4) {
        return new C629039b(this.A00, waBloksActivity);
    }
}
