package X;

import android.view.View;
import java.util.Comparator;

/* renamed from: X.0eN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10290eN implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        float A01 = AnonymousClass028.A01((View) obj);
        float A012 = AnonymousClass028.A01((View) obj2);
        if (A01 <= A012) {
            return A01 < A012 ? 1 : 0;
        }
        return -1;
    }
}
