package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.util.Arrays;

/* renamed from: X.0BY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BY extends FrameLayout {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass0A2 A03;
    public AnonymousClass0A1 A04;
    public AnonymousClass0A1 A05;
    public boolean A06;
    public final int A07;
    public final EnumC03920Jq A08;
    public final EnumC03910Jp A09;

    public AnonymousClass0BY(Context context, AnonymousClass0BX r7, AnonymousClass0UU r8, C14260l7 r9) {
        super(context);
        EnumC03910Jp r4 = r8.A03;
        this.A09 = r4;
        this.A08 = r8.A00;
        EnumC03910Jp r3 = EnumC03910Jp.FULL_SCREEN;
        if (r4 == r3) {
            this.A07 = 0;
        } else {
            this.A07 = (int) AnonymousClass0LQ.A00(context, 4.0f);
            this.A00 = (int) AnonymousClass0LQ.A00(context, 18.0f);
            this.A02 = (int) AnonymousClass0LQ.A00(context, 6.0f);
            this.A01 = (int) AnonymousClass0LQ.A00(context, 10.0f);
            EnumC03900Jo r2 = r8.A02;
            boolean z = true;
            if (r2 != EnumC03900Jo.AUTO ? r2 != EnumC03900Jo.DISABLED : !(r4 == EnumC03910Jp.FULL_SHEET || r4 == r3)) {
                z = false;
            }
            this.A06 = !z;
            AnonymousClass0A1 r32 = new AnonymousClass0A1();
            this.A04 = r32;
            int A00 = AnonymousClass0TG.A00(context, EnumC03700Iu.A01, r9);
            Paint paint = r32.A01;
            if (A00 != paint.getColor()) {
                paint.setColor(A00);
                r32.invalidateSelf();
            }
            AnonymousClass0A1 r22 = this.A04;
            Arrays.fill(r22.A04, (float) ((int) AnonymousClass0LQ.A00(context, 2.0f)));
            r22.A00 = true;
            r22.invalidateSelf();
        }
        A01(context, r7, r9);
    }

    public final int A00(Context context, C14260l7 r5) {
        boolean A02 = AnonymousClass0TG.A02(context, r5);
        return this.A08.equals(EnumC03920Jq.STATIC) ? !A02 ? 20 : 13 : A02 ? 18 : 13;
    }

    public final void A01(Context context, AnonymousClass0BX r5, C14260l7 r6) {
        A03(context, r6);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -1);
        marginLayoutParams.setMargins(0, (int) AnonymousClass0LQ.A00(context, 16.0f), 0, 0);
        addView(r5, marginLayoutParams);
        A02(context, r6);
    }

    public final void A02(Context context, C14260l7 r5) {
        AnonymousClass0A1 r2 = new AnonymousClass0A1();
        this.A05 = r2;
        Arrays.fill(r2.A04, (float) this.A07);
        r2.A00 = true;
        r2.invalidateSelf();
        Color.alpha(AnonymousClass0TG.A00(context, EnumC03700Iu.A02, r5));
        if (this.A05 != null) {
            setForeground(null);
        }
    }

    public final void A03(Context context, C14260l7 r6) {
        EnumC03700Iu r0;
        EnumC03920Jq r3 = this.A08;
        if (r3.equals(EnumC03920Jq.DISABLED)) {
            int A00 = AnonymousClass0TG.A00(context, EnumC03700Iu.A00, r6);
            AnonymousClass0A1 r2 = new AnonymousClass0A1();
            Paint paint = r2.A01;
            if (A00 != paint.getColor()) {
                paint.setColor(A00);
                r2.invalidateSelf();
            }
            Arrays.fill(r2.A04, (float) this.A07);
            r2.A00 = true;
            r2.invalidateSelf();
            setBackground(r2);
            return;
        }
        if (AnonymousClass0TG.A02(context, r6)) {
            r0 = EnumC03700Iu.A00;
        } else {
            r0 = EnumC03700Iu.A0C;
        }
        AnonymousClass0A2 r1 = new AnonymousClass0A2(context, (float) this.A07, AnonymousClass0TG.A00(context, r0, r6));
        this.A03 = r1;
        if (r3.equals(EnumC03920Jq.ANIMATED)) {
            r1.A01(true);
        }
        this.A03.setAlpha(A00(context, r6));
        setBackground(this.A03);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        AnonymousClass0A1 r4;
        super.dispatchDraw(canvas);
        if (this.A09 != EnumC03910Jp.FULL_SCREEN && (r4 = this.A04) != null && this.A06) {
            int width = (int) (((float) getWidth()) / 2.0f);
            int i = this.A00;
            r4.setBounds(width - i, this.A02, width + i, this.A01);
            r4.draw(canvas);
        }
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int i3 = 1073741824;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 1073741824);
        int size = View.MeasureSpec.getSize(i2);
        if (this.A09 == EnumC03910Jp.FLEXIBLE_SHEET) {
            i3 = Integer.MIN_VALUE;
        }
        super.onMeasure(makeMeasureSpec, View.MeasureSpec.makeMeasureSpec(size, i3));
    }
}
