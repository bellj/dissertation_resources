package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1x4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC43461x4 extends Handler implements AnonymousClass1VL {
    public final /* synthetic */ HandlerThreadC26611Ed A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC43461x4(HandlerThreadC26611Ed r2) {
        super(r2.getLooper());
        this.A00 = r2;
    }

    @Override // X.AnonymousClass1VL
    public void Aba(UserJid userJid, AbstractC43501x8 r7, String str, boolean z, boolean z2) {
        String obj;
        String rawString;
        StringBuilder sb = new StringBuilder("xmpp/connection/send/connect/");
        if (z2) {
            obj = "active";
        } else {
            StringBuilder sb2 = new StringBuilder("passive ");
            sb2.append(this.A00.A14.A00());
            obj = sb2.toString();
        }
        sb.append(obj);
        Log.i(sb.toString());
        removeMessages(0);
        Message obtainMessage = obtainMessage(0);
        Bundle data = obtainMessage.getData();
        if (userJid == null) {
            rawString = null;
        } else {
            rawString = userJid.getRawString();
        }
        data.putString("jid", rawString);
        data.putString("ipaddress", str);
        data.putBoolean("available", z);
        data.putBoolean("active_connection", z2);
        obtainMessage.obj = null;
        obtainMessage.sendToTarget();
    }

    @Override // X.AnonymousClass1VL
    public void Abb(boolean z) {
        int i = 1;
        if (z) {
            i = 2;
        }
        hasMessages(0);
        hasMessages(i);
        removeMessages(i);
        removeMessages(0);
        Message obtainMessage = obtainMessage(i);
        obtainMessage.getData().putLong("requestTime", SystemClock.uptimeMillis());
        obtainMessage.sendToTarget();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:561:0x1245 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:562:0x1247 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:563:0x1249 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:673:0x13b8 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:683:0x13d1 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:736:0x14b8 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:839:? */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:770:0x1589 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:769:0x1587 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:672:0x13b6 */
    /* JADX DEBUG: Multi-variable search result rejected for r7v26, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r7v65, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r7v73, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r7v74, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r7v126, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Can't wrap try/catch for region: R(23:87|(7:806|88|89|123|(3:125|(2:127|866)(2:128|865)|129)|864|130)|(2:132|(1:134)(3:896|146|147))(2:136|(1:143)(10:140|483|159|842|160|(4:785|163|(1:165)(4:868|166|167|(19:169|188|191|804|192|845|193|(1:196)|198|199|(1:201)|202|(2:823|204)|208|(1:210)|211|(1:213)|214|(24:216|(2:792|218)|869|222|(1:224)|225|(1:227)|228|(1:230)|231|(1:233)|234|(1:238)|239|(1:241)|242|(5:244|(1:246)|247|(1:249)|250)|251|(6:253|(1:255)(2:259|(2:261|262))|256|(2:258|277)|278|(3:885|280|281)(11:282|(1:284)|285|(1:287)(1:295)|288|(1:290)(1:294)|291|9f0|a3c|313|(14:315|(2:844|317)(1:318)|319|(2:a8f|326)|332|333|(1:335)|336|(1:338)|339|(2:341|(1:(1:(3:345|(4:347|(1:349)|350|874)(1:875)|351)(3:873|352|353))(3:871|354|355))(3:870|356|(1:(43:362|(2:817|364)|367|(1:369)|370|(1:372)|373|(4:375|(1:dde)|389|(1:e18))|397|(1:399)|400|(1:402)|403|404|(2:406|(2:410|(1:412)))|413|414|(2:424|(1:426)(2:427|(1:431)))(1:418)|419|(4:421|f6f|434|438)|440|441|fae|445|(1:447)|449|(1:452)|(1:454)|455|(10:809|100e|101a|1026|467|(1:469)|470|(1:472)|473|(1:475))|(3:852|486|(1:488))(3:493|10b7|(1:498))|(1:501)|502|(1:504)(9:505|(1:507)|513|(1:516)|517|(6:519|(1:521)|522|(1:524)|525|(1:527))|528|628|(2:906|901)(6:900|630|(2:636|(1:638))(1:634)|635|907|901))|508|(1:512)|513|(1:516)|517|(0)|528|628|(0)(0))(3:883|360|361))(3:881|535|(2:537|(1:543)(2:541|542))(2:554|555))))|872|556|557)(4:886|558|559|560)))|263|256|(0)|278|(0)(0))(3:889|567|568))(5:170|176|810|177|(3:815|178|(1:180)(3:876|181|(19:187|188|191|804|192|845|193|(1:196)|198|199|(0)|202|(0)|208|(0)|211|(0)|214|(0)(0))))))|161)|867|166|167|(0)(0)))|135|(2:145|189)|190|191|804|192|845|193|(0)|198|199|(0)|202|(0)|208|(0)|211|(0)|214|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x05de, code lost:
        if (r22 == false) goto L_0x05e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:439:0x0fa7, code lost:
        if (r2 <= r1) goto L_0x0fa9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:448:0x0fc7, code lost:
        if (r37 < r7.A00()) goto L_0x0fc9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:569:0x1266, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:570:0x1267, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:571:0x1269, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:572:0x126a, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:573:0x126c, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:574:0x126d, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:618:0x1304, code lost:
        if (r1.A00.isClosed() == false) goto L_0x1306;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:624:0x1311, code lost:
        if (r0.A01 != null) goto L_0x1313;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:647:0x1360, code lost:
        if (android.text.TextUtils.isEmpty(r6.A06) == false) goto L_0x1362;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:660:0x1392, code lost:
        if (r3 >= r6.A07.size()) goto L_0x1394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:674:0x13ba, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:675:0x13bb, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:725:0x145a, code lost:
        r9 = 8;
        r6 = 6;
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:727:0x1462, code lost:
        if (r1.A00 != false) goto L_0x1473;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:728:0x1464, code lost:
        r4.edit().putInt("connection_sequence_attempts", r17).apply();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:729:0x1473, code lost:
        r4.edit().remove("connection_sequence_state").apply();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:730:0x1484, code lost:
        if (r1.A00 == false) goto L_0x1488;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:731:0x1486, code lost:
        r75 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:732:0x1488, code lost:
        r0.A05(r75, r17 - 1, r16 - 1, r28, r72, (long) -1, r18);
        r1 = r1.A00;
        r3 = (android.os.Handler) r0.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:733:0x14a5, code lost:
        if (r1 == false) goto L_0x14b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:734:0x14a7, code lost:
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r27 = r27 ? 1 : 0;
        r1 = r3.obtainMessage(3, r21, r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:735:0x14b1, code lost:
        r0 = r3.obtainMessage(4, 1, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:736:0x14b8, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:739:0x14bf, code lost:
        r3 = -1;
        r8 = 0;
        r7 = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0516 A[Catch: InterruptedException -> 0x0527, 2Uu -> 0x13fb, 2UV -> 0x13d3, IOException -> 0x12c9, 1V9 -> 0x1298, 2UW -> 0x128d, all -> 0x13cc, TryCatch #25 {all -> 0x13cc, blocks: (B:88:0x02b7, B:89:0x02bf, B:90:0x02c2, B:91:0x02d5, B:92:0x02d6, B:93:0x02dd, B:94:0x02de, B:95:0x02e5, B:96:0x02e6, B:97:0x0316, B:98:0x0324, B:99:0x0331, B:100:0x0338, B:103:0x0342, B:104:0x0347, B:107:0x0351, B:108:0x035a, B:111:0x0364, B:112:0x0369, B:114:0x0377, B:116:0x037b, B:117:0x0384, B:119:0x03a7, B:120:0x03ac, B:121:0x03b5, B:122:0x03b9, B:123:0x03bb, B:125:0x03d0, B:127:0x03da, B:128:0x03de, B:129:0x03e1, B:130:0x03e4, B:132:0x03ee, B:134:0x03f4, B:135:0x0414, B:136:0x043e, B:138:0x0444, B:140:0x044a, B:141:0x0483, B:143:0x0485, B:146:0x04ac, B:147:0x04b3, B:159:0x04d7, B:160:0x04ef, B:167:0x050f, B:169:0x0516, B:170:0x051c, B:172:0x0523, B:173:0x0526, B:175:0x0528, B:176:0x052d, B:177:0x053e, B:181:0x054d, B:183:0x0554, B:185:0x055a, B:187:0x055e, B:188:0x0563, B:189:0x0584, B:190:0x0597, B:191:0x059c, B:576:0x1270, B:577:0x1273, B:579:0x1275, B:580:0x127a, B:581:0x1281, B:584:0x1284, B:585:0x1285, B:586:0x128c), top: B:806:0x02b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x051c A[Catch: InterruptedException -> 0x0527, 2Uu -> 0x13fb, 2UV -> 0x13d3, IOException -> 0x12c9, 1V9 -> 0x1298, 2UW -> 0x128d, all -> 0x13cc, TryCatch #25 {all -> 0x13cc, blocks: (B:88:0x02b7, B:89:0x02bf, B:90:0x02c2, B:91:0x02d5, B:92:0x02d6, B:93:0x02dd, B:94:0x02de, B:95:0x02e5, B:96:0x02e6, B:97:0x0316, B:98:0x0324, B:99:0x0331, B:100:0x0338, B:103:0x0342, B:104:0x0347, B:107:0x0351, B:108:0x035a, B:111:0x0364, B:112:0x0369, B:114:0x0377, B:116:0x037b, B:117:0x0384, B:119:0x03a7, B:120:0x03ac, B:121:0x03b5, B:122:0x03b9, B:123:0x03bb, B:125:0x03d0, B:127:0x03da, B:128:0x03de, B:129:0x03e1, B:130:0x03e4, B:132:0x03ee, B:134:0x03f4, B:135:0x0414, B:136:0x043e, B:138:0x0444, B:140:0x044a, B:141:0x0483, B:143:0x0485, B:146:0x04ac, B:147:0x04b3, B:159:0x04d7, B:160:0x04ef, B:167:0x050f, B:169:0x0516, B:170:0x051c, B:172:0x0523, B:173:0x0526, B:175:0x0528, B:176:0x052d, B:177:0x053e, B:181:0x054d, B:183:0x0554, B:185:0x055a, B:187:0x055e, B:188:0x0563, B:189:0x0584, B:190:0x0597, B:191:0x059c, B:576:0x1270, B:577:0x1273, B:579:0x1275, B:580:0x127a, B:581:0x1281, B:584:0x1284, B:585:0x1285, B:586:0x128c), top: B:806:0x02b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x05da A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x05e8  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0647 A[Catch: 2Uu -> 0x13bd, 2UV -> 0x13ba, IOException -> 0x126c, 1V9 -> 0x1269, 2UW -> 0x1266, all -> 0x142b, TryCatch #58 {2Uu -> 0x13bd, all -> 0x142b, blocks: (B:193:0x05c6, B:199:0x05e2, B:202:0x05e9, B:204:0x05f3, B:206:0x060f, B:207:0x0629, B:208:0x062a, B:210:0x0647, B:211:0x0656, B:213:0x0687, B:214:0x0689, B:216:0x06ae, B:218:0x06b3, B:220:0x06bf, B:221:0x06d5, B:222:0x06d6, B:224:0x06de, B:225:0x06e0, B:227:0x071e, B:228:0x072f, B:230:0x074c, B:231:0x0776, B:233:0x07c4, B:234:0x07d3, B:236:0x07f6, B:238:0x07ff, B:239:0x080e, B:241:0x081a, B:242:0x0829, B:244:0x0837, B:246:0x083f, B:247:0x0841, B:249:0x086b, B:250:0x086d, B:251:0x08c7, B:253:0x08e4, B:255:0x08e8, B:256:0x08ea, B:259:0x090a, B:261:0x090e, B:262:0x0910, B:263:0x0913, B:264:0x0916, B:265:0x0919, B:266:0x091c, B:267:0x091f, B:268:0x0922, B:269:0x0925, B:270:0x0928, B:271:0x092b, B:272:0x092e, B:273:0x0931, B:274:0x0934, B:275:0x0937, B:276:0x093a, B:278:0x093e, B:280:0x096d, B:281:0x0977, B:282:0x0978, B:284:0x0983, B:285:0x09a1, B:287:0x09ad, B:288:0x09b8, B:290:0x09c7, B:291:0x09d2, B:292:0x09f0, B:294:0x09f2, B:295:0x09f9, B:309:0x0a3b, B:310:0x0a3c, B:311:0x0a3d, B:312:0x0a41, B:313:0x0a42, B:315:0x0a4a, B:319:0x0a87, B:332:0x0ac5, B:404:0x0e60, B:413:0x0eca, B:455:0x0fda, B:565:0x1250, B:566:0x1251, B:567:0x1252, B:568:0x1265), top: B:845:0x05c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x0687 A[Catch: 2Uu -> 0x13bd, 2UV -> 0x13ba, IOException -> 0x126c, 1V9 -> 0x1269, 2UW -> 0x1266, all -> 0x142b, TryCatch #58 {2Uu -> 0x13bd, all -> 0x142b, blocks: (B:193:0x05c6, B:199:0x05e2, B:202:0x05e9, B:204:0x05f3, B:206:0x060f, B:207:0x0629, B:208:0x062a, B:210:0x0647, B:211:0x0656, B:213:0x0687, B:214:0x0689, B:216:0x06ae, B:218:0x06b3, B:220:0x06bf, B:221:0x06d5, B:222:0x06d6, B:224:0x06de, B:225:0x06e0, B:227:0x071e, B:228:0x072f, B:230:0x074c, B:231:0x0776, B:233:0x07c4, B:234:0x07d3, B:236:0x07f6, B:238:0x07ff, B:239:0x080e, B:241:0x081a, B:242:0x0829, B:244:0x0837, B:246:0x083f, B:247:0x0841, B:249:0x086b, B:250:0x086d, B:251:0x08c7, B:253:0x08e4, B:255:0x08e8, B:256:0x08ea, B:259:0x090a, B:261:0x090e, B:262:0x0910, B:263:0x0913, B:264:0x0916, B:265:0x0919, B:266:0x091c, B:267:0x091f, B:268:0x0922, B:269:0x0925, B:270:0x0928, B:271:0x092b, B:272:0x092e, B:273:0x0931, B:274:0x0934, B:275:0x0937, B:276:0x093a, B:278:0x093e, B:280:0x096d, B:281:0x0977, B:282:0x0978, B:284:0x0983, B:285:0x09a1, B:287:0x09ad, B:288:0x09b8, B:290:0x09c7, B:291:0x09d2, B:292:0x09f0, B:294:0x09f2, B:295:0x09f9, B:309:0x0a3b, B:310:0x0a3c, B:311:0x0a3d, B:312:0x0a41, B:313:0x0a42, B:315:0x0a4a, B:319:0x0a87, B:332:0x0ac5, B:404:0x0e60, B:413:0x0eca, B:455:0x0fda, B:565:0x1250, B:566:0x1251, B:567:0x1252, B:568:0x1265), top: B:845:0x05c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x06ae A[Catch: 2Uu -> 0x13bd, 2UV -> 0x13ba, IOException -> 0x126c, 1V9 -> 0x1269, 2UW -> 0x1266, all -> 0x142b, TryCatch #58 {2Uu -> 0x13bd, all -> 0x142b, blocks: (B:193:0x05c6, B:199:0x05e2, B:202:0x05e9, B:204:0x05f3, B:206:0x060f, B:207:0x0629, B:208:0x062a, B:210:0x0647, B:211:0x0656, B:213:0x0687, B:214:0x0689, B:216:0x06ae, B:218:0x06b3, B:220:0x06bf, B:221:0x06d5, B:222:0x06d6, B:224:0x06de, B:225:0x06e0, B:227:0x071e, B:228:0x072f, B:230:0x074c, B:231:0x0776, B:233:0x07c4, B:234:0x07d3, B:236:0x07f6, B:238:0x07ff, B:239:0x080e, B:241:0x081a, B:242:0x0829, B:244:0x0837, B:246:0x083f, B:247:0x0841, B:249:0x086b, B:250:0x086d, B:251:0x08c7, B:253:0x08e4, B:255:0x08e8, B:256:0x08ea, B:259:0x090a, B:261:0x090e, B:262:0x0910, B:263:0x0913, B:264:0x0916, B:265:0x0919, B:266:0x091c, B:267:0x091f, B:268:0x0922, B:269:0x0925, B:270:0x0928, B:271:0x092b, B:272:0x092e, B:273:0x0931, B:274:0x0934, B:275:0x0937, B:276:0x093a, B:278:0x093e, B:280:0x096d, B:281:0x0977, B:282:0x0978, B:284:0x0983, B:285:0x09a1, B:287:0x09ad, B:288:0x09b8, B:290:0x09c7, B:291:0x09d2, B:292:0x09f0, B:294:0x09f2, B:295:0x09f9, B:309:0x0a3b, B:310:0x0a3c, B:311:0x0a3d, B:312:0x0a41, B:313:0x0a42, B:315:0x0a4a, B:319:0x0a87, B:332:0x0ac5, B:404:0x0e60, B:413:0x0eca, B:455:0x0fda, B:565:0x1250, B:566:0x1251, B:567:0x1252, B:568:0x1265), top: B:845:0x05c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0909  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0978 A[Catch: 2Uu -> 0x13bd, 2UV -> 0x13ba, IOException -> 0x126c, 1V9 -> 0x1269, 2UW -> 0x1266, all -> 0x142b, TryCatch #58 {2Uu -> 0x13bd, all -> 0x142b, blocks: (B:193:0x05c6, B:199:0x05e2, B:202:0x05e9, B:204:0x05f3, B:206:0x060f, B:207:0x0629, B:208:0x062a, B:210:0x0647, B:211:0x0656, B:213:0x0687, B:214:0x0689, B:216:0x06ae, B:218:0x06b3, B:220:0x06bf, B:221:0x06d5, B:222:0x06d6, B:224:0x06de, B:225:0x06e0, B:227:0x071e, B:228:0x072f, B:230:0x074c, B:231:0x0776, B:233:0x07c4, B:234:0x07d3, B:236:0x07f6, B:238:0x07ff, B:239:0x080e, B:241:0x081a, B:242:0x0829, B:244:0x0837, B:246:0x083f, B:247:0x0841, B:249:0x086b, B:250:0x086d, B:251:0x08c7, B:253:0x08e4, B:255:0x08e8, B:256:0x08ea, B:259:0x090a, B:261:0x090e, B:262:0x0910, B:263:0x0913, B:264:0x0916, B:265:0x0919, B:266:0x091c, B:267:0x091f, B:268:0x0922, B:269:0x0925, B:270:0x0928, B:271:0x092b, B:272:0x092e, B:273:0x0931, B:274:0x0934, B:275:0x0937, B:276:0x093a, B:278:0x093e, B:280:0x096d, B:281:0x0977, B:282:0x0978, B:284:0x0983, B:285:0x09a1, B:287:0x09ad, B:288:0x09b8, B:290:0x09c7, B:291:0x09d2, B:292:0x09f0, B:294:0x09f2, B:295:0x09f9, B:309:0x0a3b, B:310:0x0a3c, B:311:0x0a3d, B:312:0x0a41, B:313:0x0a42, B:315:0x0a4a, B:319:0x0a87, B:332:0x0ac5, B:404:0x0e60, B:413:0x0eca, B:455:0x0fda, B:565:0x1250, B:566:0x1251, B:567:0x1252, B:568:0x1265), top: B:845:0x05c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:519:0x1131 A[Catch: 2Uu -> 0x13b8, 2UV -> 0x13b6, IOException -> 0x1249, 1V9 -> 0x1247, 2UW -> 0x1245, all -> 0x13d1, TryCatch #28 {all -> 0x13d1, blocks: (B:458:0x100e, B:461:0x101a, B:464:0x1026, B:467:0x1032, B:469:0x1038, B:470:0x104a, B:472:0x1050, B:473:0x1062, B:475:0x1068, B:478:0x107d, B:481:0x1080, B:484:0x1083, B:486:0x1086, B:488:0x1090, B:490:0x10a4, B:492:0x10ac, B:493:0x10b3, B:494:0x10b7, B:498:0x10c3, B:501:0x10cc, B:502:0x10ea, B:505:0x10f7, B:507:0x1101, B:508:0x1107, B:510:0x110d, B:512:0x1113, B:513:0x111c, B:516:0x1126, B:517:0x1129, B:519:0x1131, B:521:0x1155, B:524:0x115d, B:527:0x1167, B:528:0x1172, B:531:0x1185, B:559:0x123a, B:560:0x1244, B:589:0x1291, B:592:0x129c, B:605:0x12cd), top: B:809:0x100e }] */
    /* JADX WARNING: Removed duplicated region for block: B:712:0x1438 A[Catch: 1xC -> 0x1456, all -> 0x1589, TryCatch #30 {1xC -> 0x1456, blocks: (B:710:0x1432, B:712:0x1438, B:714:0x143c, B:716:0x1444, B:717:0x1448, B:719:0x144c, B:721:0x1451, B:722:0x1455), top: B:812:0x1432 }] */
    /* JADX WARNING: Removed duplicated region for block: B:749:0x1510  */
    /* JADX WARNING: Removed duplicated region for block: B:750:0x1512  */
    /* JADX WARNING: Removed duplicated region for block: B:756:0x152a A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:760:0x153f  */
    /* JADX WARNING: Removed duplicated region for block: B:763:0x155a  */
    /* JADX WARNING: Removed duplicated region for block: B:776:0x15a1  */
    /* JADX WARNING: Removed duplicated region for block: B:779:0x15c1  */
    /* JADX WARNING: Removed duplicated region for block: B:782:0x15ce  */
    /* JADX WARNING: Removed duplicated region for block: B:823:0x05f3 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:885:0x096d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:889:0x1252 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:899:0x1318 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:900:0x131f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:905:0x027e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:906:0x027e A[SYNTHETIC] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r89) {
        /*
        // Method dump skipped, instructions count: 5738
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC43461x4.handleMessage(android.os.Message):void");
    }
}
