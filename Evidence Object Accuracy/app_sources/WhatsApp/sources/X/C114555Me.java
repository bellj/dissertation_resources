package X;

/* renamed from: X.5Me  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114555Me extends AnonymousClass1TM {
    public AnonymousClass5NG A00;
    public AnonymousClass5NH A01;
    public AnonymousClass5NH A02;
    public C114725Mv A03;

    public C114555Me(AnonymousClass5NG r1, AnonymousClass5NH r2, AnonymousClass5NH r3, C114725Mv r4) {
        this.A03 = r4;
        this.A02 = r2;
        this.A01 = r3;
        this.A00 = r1;
    }

    public C114555Me(AbstractC114775Na r2) {
        this.A03 = C114725Mv.A00(AbstractC114775Na.A00(r2));
        this.A02 = (AnonymousClass5NH) AbstractC114775Na.A01(r2);
        this.A01 = (AnonymousClass5NH) r2.A0D(2);
        this.A00 = (AnonymousClass5NG) r2.A0D(3);
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r1 = new C94954co(4);
        r1.A06(this.A03);
        r1.A06(this.A02);
        r1.A06(this.A01);
        return C94954co.A01(this.A00, r1);
    }
}
