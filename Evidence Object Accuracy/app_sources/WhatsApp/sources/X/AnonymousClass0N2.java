package X;

import java.util.Arrays;

/* renamed from: X.0N2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0N2 {
    public final long[] A00;
    public final boolean[] A01;

    public AnonymousClass0N2(int i) {
        long[] jArr = new long[i];
        this.A00 = jArr;
        boolean[] zArr = new boolean[i];
        this.A01 = zArr;
        Arrays.fill(jArr, 0L);
        Arrays.fill(zArr, false);
    }
}
