package X;

import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0fR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10930fR extends AbstractExecutorService {
    public final RunnableC09450cv A00;
    public final String A01 = "SerialExecutor";
    public final BlockingQueue A02;
    public final Executor A03;
    public final AtomicInteger A04;
    public final AtomicInteger A05;
    public volatile int A06;

    @Override // java.util.concurrent.ExecutorService
    public boolean isShutdown() {
        return false;
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isTerminated() {
        return false;
    }

    public C10930fR(BlockingQueue blockingQueue, Executor executor) {
        this.A03 = executor;
        this.A06 = 1;
        this.A02 = blockingQueue;
        this.A00 = new RunnableC09450cv(this);
        this.A05 = new AtomicInteger(0);
        this.A04 = new AtomicInteger(0);
    }

    public final void A00() {
        AtomicInteger atomicInteger = this.A05;
        while (true) {
            int i = atomicInteger.get();
            if (i < this.A06) {
                int i2 = i + 1;
                if (atomicInteger.compareAndSet(i, i2)) {
                    AnonymousClass0UN.A00(C10930fR.class, this.A01, Integer.valueOf(i2), Integer.valueOf(this.A06), "%s: starting worker %d of %d");
                    this.A03.execute(this.A00);
                    return;
                }
                AnonymousClass0UN.A02(C10930fR.class, this.A01, "%s: race in startWorkerIfNeeded; retrying");
            } else {
                return;
            }
        }
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        if (runnable != null) {
            BlockingQueue blockingQueue = this.A02;
            if (blockingQueue.offer(runnable)) {
                int size = blockingQueue.size();
                AtomicInteger atomicInteger = this.A04;
                int i = atomicInteger.get();
                if (size > i && atomicInteger.compareAndSet(i, size)) {
                    AnonymousClass0UN.A01(C10930fR.class, this.A01, Integer.valueOf(size), "%s: max pending work in queue = %d");
                }
                A00();
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(this.A01);
            sb.append(" queue is full, size=");
            sb.append(blockingQueue.size());
            throw new RejectedExecutionException(sb.toString());
        }
        throw new NullPointerException("runnable parameter is null");
    }

    @Override // java.util.concurrent.ExecutorService
    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.concurrent.ExecutorService
    public List shutdownNow() {
        throw new UnsupportedOperationException();
    }
}
