package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* renamed from: X.0CS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CS extends AnonymousClass0SU {
    public ColorStateList A00 = null;
    public PorterDuff.Mode A01 = null;
    public Drawable A02;
    public boolean A03 = false;
    public boolean A04 = false;
    public final SeekBar A05;

    public AnonymousClass0CS(SeekBar seekBar) {
        super(seekBar);
        this.A05 = seekBar;
    }

    @Override // X.AnonymousClass0SU
    public void A01(AttributeSet attributeSet, int i) {
        super.A01(attributeSet, i);
        SeekBar seekBar = this.A05;
        Context context = seekBar.getContext();
        int[] iArr = AnonymousClass07O.A06;
        C013406h A00 = C013406h.A00(context, attributeSet, iArr, i, 0);
        Context context2 = seekBar.getContext();
        TypedArray typedArray = A00.A02;
        AnonymousClass028.A0L(context2, typedArray, attributeSet, seekBar, iArr, i);
        Drawable A03 = A00.A03(0);
        if (A03 != null) {
            seekBar.setThumb(A03);
        }
        Drawable A02 = A00.A02(1);
        Drawable drawable = this.A02;
        if (drawable != null) {
            drawable.setCallback(null);
        }
        this.A02 = A02;
        if (A02 != null) {
            A02.setCallback(seekBar);
            C015607k.A0D(AnonymousClass028.A05(seekBar), A02);
            if (A02.isStateful()) {
                A02.setState(seekBar.getDrawableState());
            }
            A02();
        }
        seekBar.invalidate();
        if (typedArray.hasValue(3)) {
            this.A01 = C014706y.A00(this.A01, typedArray.getInt(3, -1));
            this.A04 = true;
        }
        if (typedArray.hasValue(2)) {
            this.A00 = A00.A01(2);
            this.A03 = true;
        }
        A00.A04();
        A02();
    }

    public final void A02() {
        Drawable drawable = this.A02;
        if (drawable == null) {
            return;
        }
        if (this.A03 || this.A04) {
            Drawable A03 = C015607k.A03(drawable.mutate());
            this.A02 = A03;
            if (this.A03) {
                C015607k.A04(this.A00, A03);
            }
            if (this.A04) {
                C015607k.A07(this.A01, this.A02);
            }
            if (this.A02.isStateful()) {
                this.A02.setState(this.A05.getDrawableState());
            }
        }
    }
}
