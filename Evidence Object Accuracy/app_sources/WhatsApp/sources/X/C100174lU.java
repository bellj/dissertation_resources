package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100174lU implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        C16700pc.A0E(parcel, 0);
        return new C100494m0(parcel.readString());
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C100494m0[i];
    }
}
