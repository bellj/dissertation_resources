package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.DeviceJid;
import java.util.Collections;
import java.util.HashSet;

/* renamed from: X.16f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246216f {
    public C28601Of A00;
    public final C14830m7 A01;
    public final AnonymousClass1YG A02;

    public C246216f(AbstractC15710nm r3, C14830m7 r4, C16590pI r5, C231410n r6, C14850m9 r7) {
        this.A01 = r4;
        this.A02 = new AnonymousClass1YG(r5.A00, r3, r6, r7);
    }

    public C28601Of A00() {
        C28601Of r1;
        synchronized (this) {
            if (this.A00 == null) {
                C16310on A01 = this.A02.get();
                Cursor A08 = A01.A03.A08("devices", null, null, null, AnonymousClass1YH.A00, null);
                try {
                    int columnIndexOrThrow = A08.getColumnIndexOrThrow("device_id");
                    int columnIndexOrThrow2 = A08.getColumnIndexOrThrow("platform_type");
                    int columnIndexOrThrow3 = A08.getColumnIndexOrThrow("device_os");
                    int columnIndexOrThrow4 = A08.getColumnIndexOrThrow("last_active");
                    int columnIndexOrThrow5 = A08.getColumnIndexOrThrow("login_time");
                    int columnIndexOrThrow6 = A08.getColumnIndexOrThrow("logout_time");
                    int columnIndexOrThrow7 = A08.getColumnIndexOrThrow("adv_key_index");
                    int columnIndexOrThrow8 = A08.getColumnIndexOrThrow("full_sync_required");
                    int columnIndexOrThrow9 = A08.getColumnIndexOrThrow("place_name");
                    AnonymousClass1YB r3 = new AnonymousClass1YB();
                    while (A08.moveToNext()) {
                        DeviceJid nullable = DeviceJid.getNullable(A08.getString(columnIndexOrThrow));
                        if (nullable != null) {
                            AnonymousClass1YI A00 = AnonymousClass1YI.A00(A08.getInt(columnIndexOrThrow2));
                            String string = A08.getString(columnIndexOrThrow3);
                            long j = A08.getLong(columnIndexOrThrow4);
                            long j2 = A08.getLong(columnIndexOrThrow5);
                            long j3 = A08.getLong(columnIndexOrThrow6);
                            int i = A08.getInt(columnIndexOrThrow7);
                            boolean z = false;
                            if (1 == A08.getInt(columnIndexOrThrow8)) {
                                z = true;
                            }
                            r3.A01(nullable, new AnonymousClass1JU(nullable, A00, string, A08.getString(columnIndexOrThrow9), i, j, j2, j3, z));
                        }
                    }
                    this.A00 = r3.A00();
                    A08.close();
                    A01.close();
                } catch (Throwable th) {
                    if (A08 != null) {
                        try {
                            A08.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            }
            r1 = this.A00;
        }
        return r1;
    }

    public void A01(AnonymousClass1JO r7) {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            synchronized (this) {
                String[] A0Q = C15380n4.A0Q(new HashSet(r7.A00));
                String join = TextUtils.join(", ", Collections.nCopies(A0Q.length, "?"));
                StringBuilder sb = new StringBuilder();
                sb.append("device_id IN (");
                sb.append(join);
                sb.append(")");
                A02.A03.A01("devices", sb.toString(), A0Q);
                A00.A00();
                this.A00 = null;
            }
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
