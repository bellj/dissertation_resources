package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.20n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC451920n extends AbstractC16350or {
    public final AbstractC451720l A00;
    public final C241414j A01;
    public final AbstractC248317b A02;
    public final AbstractC14440lR A03;

    public AbstractC451920n(AbstractC451720l r1, C241414j r2, AbstractC248317b r3, AbstractC14440lR r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public C90214Na A08(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl r3 = (AbstractC28901Pl) it.next();
            AnonymousClass5W2 r1 = null;
            AbstractC16830pp AGh = this.A02.AGh(r3.A07.A03, null);
            if (!(AGh == null || (r1 = AGh.ABr()) == null)) {
                r1.A6M(r3);
            }
            arrayList.add(r3);
            if (r1 != null && !hashMap.containsKey(r3.A07.A03)) {
                hashMap.put(r3.A07.A03, r1);
            }
        }
        C241414j r4 = this.A01;
        boolean A0M = r4.A0M(arrayList);
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        if (A0M && arrayList.size() > 0) {
            for (AnonymousClass5W2 r0 : hashMap.values()) {
                r0.A5s(arrayList);
            }
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                AbstractC28901Pl r8 = (AbstractC28901Pl) it2.next();
                AbstractC28901Pl A08 = r4.A08(r8.A0A);
                AnonymousClass009.A05(A08);
                arrayList2.add(A08);
                byte[] bArr = A08.A0D;
                if (bArr == null || bArr.length <= 0) {
                    this.A03.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, hashMap, r8, A08, 19));
                }
            }
        }
        return new C90214Na(arrayList2, A0M);
    }
}
