package X;

import android.content.Context;
import android.os.Parcelable;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3SU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SU implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        Parcelable parcelable;
        RecyclerView recyclerView = (RecyclerView) obj;
        C56002kA r5 = (C56002kA) obj2;
        recyclerView.setAdapter(r5.A08);
        recyclerView.setItemAnimator(null);
        AnonymousClass4IB r0 = r5.A07;
        if (r0 == null || (parcelable = r0.A00) == null) {
            int i = r5.A04;
            if (i >= 0) {
                recyclerView.A0Y(i);
                return;
            }
            return;
        }
        recyclerView.getLayoutManager().A0q(parcelable);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12960it.A1X(((C56002kA) obj).A08, ((C56002kA) obj2).A08);
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView = (RecyclerView) obj;
        AnonymousClass4IB r1 = ((C56002kA) obj2).A07;
        if (r1 != null) {
            r1.A00 = recyclerView.getLayoutManager().A0g();
        }
        recyclerView.setAdapter(null);
    }
}
