package X;

/* renamed from: X.4Vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92274Vf {
    public final String A00;
    public final String A01;
    public final String A02;

    public C92274Vf(String str, String str2, String str3) {
        this.A02 = str;
        this.A01 = str2;
        this.A00 = str3;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Bullet{text='");
        A0k.append(this.A02);
        A0k.append('\'');
        A0k.append(", iconLightUrl='");
        A0k.append(this.A01);
        A0k.append('\'');
        A0k.append(", iconDarkUrl='");
        A0k.append(this.A00);
        A0k.append('\'');
        return C12970iu.A0v(A0k);
    }
}
