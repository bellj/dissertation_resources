package X;

import android.view.View;

/* renamed from: X.5ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119565ee extends AbstractC17880rY {
    public C119565ee() {
        super(13642);
    }

    @Override // X.AbstractC17880rY
    public Object A00(C14260l7 r2, AnonymousClass28D r3) {
        return new C125145qm();
    }

    @Override // X.AbstractC17880rY
    public void A01(View view, C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7) {
        if (AnonymousClass3JV.A04(r5, r6) != null) {
            r6.A0I(38);
            AnonymousClass67B r2 = new AbstractC115815Ta() { // from class: X.67B
                @Override // X.AbstractC115815Ta
                public final AnonymousClass28D AAL() {
                    return AnonymousClass28D.this;
                }
            };
            AbstractC50222Op r1 = (AbstractC50222Op) r5.A00;
            if (((ActivityC13810kN) r1).A02 != null) {
                r1.AfS(r2);
                r1.AfT(r2, false);
            }
        }
    }

    @Override // X.AbstractC17880rY
    public void A02(View view, C14260l7 r3, AnonymousClass28D r4, AnonymousClass28D r5) {
        new C126195sU(r4);
        if (AnonymousClass3JV.A04(r3, r4) != null) {
            r4.A0I(38);
        }
    }
}
