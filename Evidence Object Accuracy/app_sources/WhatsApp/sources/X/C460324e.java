package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.24e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C460324e extends AbstractC460224d {
    public static final Parcelable.Creator CREATOR = new C100154lS();
    public final boolean A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C460324e(String str, boolean z) {
        super(str, "WEBVIEW");
        this.A00 = z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeByte(this.A00 ? (byte) 1 : 0);
    }
}
