package X;

import android.os.Handler;
import java.util.concurrent.Executor;

/* renamed from: X.5E1  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass5E1 implements Executor {
    public final /* synthetic */ Handler A00;

    public /* synthetic */ AnonymousClass5E1(Handler handler) {
        this.A00 = handler;
    }

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.A00.post(runnable);
    }
}
