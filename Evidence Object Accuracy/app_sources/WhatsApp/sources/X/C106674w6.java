package X;

/* renamed from: X.4w6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106674w6 implements AnonymousClass5VX {
    public final int A00;
    public final int A01;
    public final C95304dT A02 = new C95304dT();
    public final AnonymousClass4YB A03;

    public C106674w6(AnonymousClass4YB r2, int i, int i2) {
        this.A00 = i;
        this.A03 = r2;
        this.A01 = i2;
    }

    @Override // X.AnonymousClass5VX
    public void AVj() {
        C95304dT r2 = this.A02;
        byte[] bArr = AnonymousClass3JZ.A0A;
        r2.A0U(bArr, bArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0090, code lost:
        if (r11 == -9223372036854775807L) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0099, code lost:
        return new X.C93534aK(-2, r11, r13 + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009c, code lost:
        return X.C93534aK.A03;
     */
    @Override // X.AnonymousClass5VX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C93534aK AbM(X.AnonymousClass5Yf r24, long r25) {
        /*
            r23 = this;
            r4 = r24
            long r13 = r4.AFo()
            r7 = r23
            int r0 = r7.A01
            long r2 = (long) r0
            long r0 = r4.getLength()
            long r0 = r0 - r13
            long r0 = java.lang.Math.min(r2, r0)
            int r2 = (int) r0
            X.4dT r3 = r7.A02
            r3.A0Q(r2)
            byte[] r1 = r3.A02
            r0 = 0
            r4.AZ4(r1, r0, r2)
            int r8 = r3.A00
            r0 = -1
            r19 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r4 = -1
            r11 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x0030:
            int r6 = r3.A00
            int r2 = r3.A01
            int r6 = r6 - r2
            r9 = 188(0xbc, float:2.63E-43)
            if (r6 < r9) goto L_0x008e
            byte[] r10 = r3.A02
        L_0x003b:
            if (r2 >= r8) goto L_0x0046
            byte r9 = r10[r2]
            r6 = 71
            if (r9 == r6) goto L_0x0046
            int r2 = r2 + 1
            goto L_0x003b
        L_0x0046:
            int r6 = r2 + 188
            if (r6 > r8) goto L_0x008e
            int r0 = r7.A00
            long r0 = X.AnonymousClass4DH.A00(r3, r2, r0)
            int r9 = (r0 > r19 ? 1 : (r0 == r19 ? 0 : -1))
            if (r9 == 0) goto L_0x0089
            X.4YB r9 = r7.A03
            long r17 = r9.A03(r0)
            int r0 = (r17 > r25 ? 1 : (r17 == r25 ? 0 : -1))
            if (r0 <= 0) goto L_0x006f
            int r0 = (r11 > r19 ? 1 : (r11 == r19 ? 0 : -1))
            if (r0 != 0) goto L_0x006d
            r16 = -1
            X.4aK r9 = new X.4aK
            r15 = r9
            r19 = r13
            r15.<init>(r16, r17, r19)
            return r9
        L_0x006d:
            long r13 = r13 + r4
            goto L_0x007a
        L_0x006f:
            r4 = 100000(0x186a0, double:4.94066E-319)
            long r4 = r4 + r17
            int r0 = (r4 > r25 ? 1 : (r4 == r25 ? 0 : -1))
            if (r0 <= 0) goto L_0x0086
            long r0 = (long) r2
            long r13 = r13 + r0
        L_0x007a:
            X.4aK r9 = new X.4aK
            r18 = 0
            r17 = r9
            r21 = r13
            r17.<init>(r18, r19, r21)
            return r9
        L_0x0086:
            long r4 = (long) r2
            r11 = r17
        L_0x0089:
            r3.A0S(r6)
            long r0 = (long) r6
            goto L_0x0030
        L_0x008e:
            int r2 = (r11 > r19 ? 1 : (r11 == r19 ? 0 : -1))
            if (r2 == 0) goto L_0x009a
            long r13 = r13 + r0
            r10 = -2
            X.4aK r9 = new X.4aK
            r9.<init>(r10, r11, r13)
            return r9
        L_0x009a:
            X.4aK r9 = X.C93534aK.A03
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106674w6.AbM(X.5Yf, long):X.4aK");
    }
}
