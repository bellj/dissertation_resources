package X;

/* renamed from: X.4ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94914ck {
    public C94914ck A00;
    public byte[] A01;
    public final String A02;

    public C94914ck(String str) {
        this.A02 = str;
    }

    public static int A00(AnonymousClass4YW r2, int i, int i2) {
        int i3;
        if ((i & 4096) == 0 || r2.A03 >= 49) {
            i3 = 0;
        } else {
            r2.A02("Synthetic");
            i3 = 6;
        }
        if (i2 != 0) {
            r2.A02("Signature");
            i3 += 8;
        }
        if ((i & C25981Bo.A0F) == 0) {
            return i3;
        }
        r2.A02("Deprecated");
        return i3 + 6;
    }

    public static void A01(C95044cz r3, AnonymousClass4YW r4, int i, int i2) {
        if ((i & 4096) != 0 && r4.A03 < 49) {
            AnonymousClass4YW.A01("Synthetic", r3, r4);
            r3.A03(0);
        }
        if (i2 != 0) {
            AnonymousClass4YW.A01("Signature", r3, r4);
            r3.A03(2);
            r3.A04(i2);
        }
        if ((i & C25981Bo.A0F) != 0) {
            AnonymousClass4YW.A01("Deprecated", r3, r4);
            r3.A03(0);
        }
    }

    public final int A02(AnonymousClass4YW r5) {
        int i = 0;
        for (C94914ck r2 = this; r2 != null; r2 = r2.A00) {
            r5.A02(r2.A02);
            i += new C95044cz(r2.A01).A00 + 6;
        }
        return i;
    }

    public final void A03(C95044cz r6, AnonymousClass4YW r7) {
        for (C94914ck r4 = this; r4 != null; r4 = r4.A00) {
            C95044cz r3 = new C95044cz(r4.A01);
            AnonymousClass4YW.A01(r4.A02, r6, r7);
            r6.A03(r3.A00);
            r6.A0A(r3.A01, 0, r3.A00);
        }
    }
}
