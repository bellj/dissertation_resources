package X;

import java.io.File;
import java.io.IOException;

/* renamed from: X.3Gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64733Gr {
    public final File A00;

    public C64733Gr(File file) {
        this.A00 = file;
    }

    public static String A00(String str) {
        int length = str.length();
        StringBuilder A0t = C12980iv.A0t(length);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            A0t.append(((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z') && !((charAt >= '0' && charAt <= '9') || charAt == '-' || charAt == '_' || charAt == '.')) ? "_" : Character.valueOf(charAt));
        }
        return A0t.toString();
    }

    public String A01(String str) {
        File file = this.A00;
        if (!file.exists() && !file.mkdirs()) {
            return null;
        }
        try {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(file.getCanonicalPath());
            A0h.append(File.separator);
            A0h.append(str);
            return A0h.toString();
        } catch (IOException unused) {
            return null;
        }
    }
}
