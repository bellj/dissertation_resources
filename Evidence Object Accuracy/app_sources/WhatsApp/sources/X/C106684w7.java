package X;

/* renamed from: X.4w7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106684w7 implements AnonymousClass5VX {
    public final int A00;
    public final AnonymousClass4IE A01 = new AnonymousClass4IE();
    public final C95424dg A02;

    @Override // X.AnonymousClass5VX
    public /* synthetic */ void AVj() {
    }

    public /* synthetic */ C106684w7(C95424dg r2, int i) {
        this.A02 = r2;
        this.A00 = i;
    }

    public final long A00(AnonymousClass5Yf r19) {
        long length;
        long j;
        while (true) {
            long AFf = r19.AFf();
            length = r19.getLength();
            j = length - 6;
            if (AFf >= j) {
                break;
            }
            C95424dg r12 = this.A02;
            int i = this.A00;
            AnonymousClass4IE r10 = this.A01;
            byte[] bArr = new byte[2];
            r19.AZ4(bArr, 0, 2);
            if ((((bArr[0] & 255) << 8) | (bArr[1] & 255)) == i) {
                C95304dT A05 = C95304dT.A05(16);
                System.arraycopy(bArr, 0, A05.A02, 0, 2);
                byte[] bArr2 = A05.A02;
                int i2 = 0;
                do {
                    int AZ0 = r19.AZ0(bArr2, 2 + i2, 14 - i2);
                    if (AZ0 == -1) {
                        break;
                    }
                    i2 += AZ0;
                } while (i2 < 14);
                A05.A0R(i2);
                r19.Aaj();
                r19.A5r((int) (AFf - r19.AFo()));
                if (C92944Yc.A01(r10, r12, A05, i)) {
                    break;
                }
            } else {
                r19.Aaj();
                r19.A5r((int) (AFf - r19.AFo()));
            }
            r19.A5r(1);
        }
        long AFf2 = r19.AFf();
        if (AFf2 < j) {
            return this.A01.A00;
        }
        r19.A5r((int) (length - AFf2));
        return this.A02.A09;
    }

    @Override // X.AnonymousClass5VX
    public C93534aK AbM(AnonymousClass5Yf r15, long j) {
        long AFo = r15.AFo();
        long A00 = A00(r15);
        long AFf = r15.AFf();
        r15.A5r(Math.max(6, this.A02.A06));
        long A002 = A00(r15);
        long AFf2 = r15.AFf();
        if (A00 <= j && A002 > j) {
            return new C93534aK(0, -9223372036854775807L, AFf);
        }
        if (A002 <= j) {
            return new C93534aK(-2, A002, AFf2);
        }
        return new C93534aK(-1, A00, AFo);
    }
}
