package X;

/* renamed from: X.5In  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113675In extends AbstractC113685Io implements AnonymousClass5VI, AnonymousClass5VE {
    public final AnonymousClass5X4 collectContext;
    public final int collectContextSize;
    public final AnonymousClass5VI collector;
    public AnonymousClass5WO completion;
    public AnonymousClass5X4 lastEmissionContext;

    public C113675In(AnonymousClass5X4 r3, AnonymousClass5VI r4) {
        super(new C112655Ef(), C112775Er.A00);
        this.collector = r4;
        this.collectContext = r3;
        this.collectContextSize = C12960it.A05(r3.fold(C12980iv.A0i(), new AnonymousClass5KO()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a2, code lost:
        r5 = r6;
     */
    @Override // X.AnonymousClass5VI
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A9I(java.lang.Object r15, X.AnonymousClass5WO r16) {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113675In.A9I(java.lang.Object, X.5WO):java.lang.Object");
    }
}
