package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AM extends Enum {
    public static final /* synthetic */ AnonymousClass4AM[] A00;
    public static final AnonymousClass4AM A01;
    public static final AnonymousClass4AM A02;
    public static final AnonymousClass4AM A03;

    public static AnonymousClass4AM valueOf(String str) {
        return (AnonymousClass4AM) Enum.valueOf(AnonymousClass4AM.class, str);
    }

    public static AnonymousClass4AM[] values() {
        return (AnonymousClass4AM[]) A00.clone();
    }

    static {
        AnonymousClass4AM r4 = new AnonymousClass4AM("ICON", 0);
        A01 = r4;
        AnonymousClass4AM r3 = new AnonymousClass4AM("TEXT", 1);
        A03 = r3;
        AnonymousClass4AM r1 = new AnonymousClass4AM("ICON_TEXT", 2);
        A02 = r1;
        AnonymousClass4AM[] r0 = new AnonymousClass4AM[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4AM(String str, int i) {
    }
}
