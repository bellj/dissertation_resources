package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1300000_I1;
import java.io.IOException;

/* renamed from: X.3Xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68863Xa implements AbstractC44401yr {
    public final /* synthetic */ AnonymousClass19X A00;
    public final /* synthetic */ AnonymousClass5WG A01;
    public final /* synthetic */ String A02;

    public C68863Xa(AnonymousClass19X r1, AnonymousClass5WG r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r8) {
        this.A00.A03.A0H(new RunnableBRunnable0Shape1S1300000_I1(this, r8, this.A01, this.A02, 2));
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        APp(iOException);
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        this.A00.A03.A0H(new RunnableBRunnable0Shape1S1300000_I1(this, exc, this.A01, this.A02, 3));
    }
}
