package X;

import android.content.Context;
import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0pM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16630pM {
    public final Context A00;
    public final AnonymousClass167 A01;
    public final Map A02 = new HashMap();
    public final AtomicInteger A03 = new AtomicInteger();

    public C16630pM(Context context, AnonymousClass167 r3) {
        this.A00 = context;
        this.A01 = r3;
    }

    public final SharedPreferences A00(String str) {
        SharedPreferences sharedPreferences;
        StringBuilder sb;
        Map map = this.A02;
        SharedPreferences sharedPreferences2 = (SharedPreferences) map.get(str);
        if (sharedPreferences2 != null) {
            return sharedPreferences2;
        }
        String str2 = AnonymousClass01V.A07;
        if (str2.equals(str)) {
            return this.A00.getSharedPreferences(str2, 0);
        }
        Context context = this.A00;
        File file = new File(context.getFilesDir().getParent(), "shared_prefs");
        if (!file.exists()) {
            try {
                file.mkdir();
                if (!file.exists()) {
                    sb = new StringBuilder("SharedPreferencesFactory/Failed to create preference dir ");
                    sb.append(file.getAbsolutePath());
                } else if (!file.isDirectory() || !file.canRead() || !file.canWrite()) {
                    sb = new StringBuilder("SharedPreferencesFactory/Invalid preference dir ");
                    sb.append(file.getAbsolutePath());
                    sb.append(", isDirectory=");
                    sb.append(file.isDirectory());
                    sb.append(", canRead=");
                    sb.append(file.canRead());
                    sb.append(", canWrite=");
                    sb.append(file.canWrite());
                }
                Log.e(sb.toString());
                sharedPreferences = context.getSharedPreferences(str, 0);
            } catch (SecurityException e) {
                StringBuilder sb2 = new StringBuilder("SharedPreferencesFactory/Unable to create LightSharedPreferences: ");
                sb2.append(file.getAbsolutePath());
                Log.e(sb2.toString(), e);
                sharedPreferences = context.getSharedPreferences(str, 0);
            }
            map.put(str, sharedPreferences);
            return sharedPreferences;
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(".xml");
        sharedPreferences = new AnonymousClass1IL(new AnonymousClass1IK(new File(file, sb3.toString())), this.A01, this.A03.getAndIncrement());
        map.put(str, sharedPreferences);
        return sharedPreferences;
    }

    public synchronized SharedPreferences A01(String str) {
        return A00(str);
    }

    @Deprecated
    public synchronized SharedPreferences A02(String str) {
        return A00(str);
    }

    public boolean A03() {
        File file = new File(this.A00.getFilesDir().getParent(), "shared_prefs");
        StringBuilder sb = new StringBuilder();
        sb.append("ab-props-backup");
        sb.append(".xml");
        return new File(file, sb.toString()).exists();
    }
}
