package X;

import android.content.Context;

/* renamed from: X.4Vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92304Vj {
    public C93294Zw A00;
    public final int A01;
    public final Context A02;
    public final Object A03;

    public C92304Vj(Context context, C93294Zw r2, Object obj, int i) {
        this.A02 = context;
        this.A01 = i;
        this.A00 = r2;
        this.A03 = obj;
    }

    public Object A00() {
        return this.A03;
    }
}
