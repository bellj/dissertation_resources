package X;

import android.database.DataSetObserver;
import com.google.android.material.tabs.TabLayout;

/* renamed from: X.3fV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72993fV extends DataSetObserver {
    public final /* synthetic */ TabLayout A00;

    public C72993fV(TabLayout tabLayout) {
        this.A00 = tabLayout;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        this.A00.A05();
    }

    @Override // android.database.DataSetObserver
    public void onInvalidated() {
        this.A00.A05();
    }
}
