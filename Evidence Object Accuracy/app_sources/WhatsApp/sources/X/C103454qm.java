package X;

import android.content.Context;
import com.whatsapp.settings.SettingsNetworkUsage;

/* renamed from: X.4qm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103454qm implements AbstractC009204q {
    public final /* synthetic */ SettingsNetworkUsage A00;

    public C103454qm(SettingsNetworkUsage settingsNetworkUsage) {
        this.A00 = settingsNetworkUsage;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
