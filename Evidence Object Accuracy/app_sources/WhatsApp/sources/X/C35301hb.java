package X;

/* renamed from: X.1hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35301hb extends AbstractC18860tB {
    public final C35491i4 A00;
    public final C15580nU A01;

    public C35301hb(C35491i4 r1, C15580nU r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final boolean A03(AbstractC15340mz r3) {
        if (!(r3 instanceof C28581Od) || !r3.A0z.A02) {
            return false;
        }
        return this.A01.equals(((C28581Od) r3).A02);
    }
}
