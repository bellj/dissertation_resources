package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ToggleButton;

/* renamed from: X.0C1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C1 extends ToggleButton implements AnonymousClass012 {
    public final AnonymousClass085 A00;
    public final AnonymousClass086 A01;

    public AnonymousClass0C1(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842827);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A00 = r0;
        r0.A05(attributeSet, 16842827);
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A01 = r02;
        r02.A0A(attributeSet, 16842827);
    }

    @Override // android.widget.ToggleButton, android.widget.TextView, android.widget.CompoundButton, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A01;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    @Override // android.widget.ToggleButton, android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }
}
