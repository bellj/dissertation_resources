package X;

import android.media.MediaPlayer;

/* renamed from: X.4i6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C98074i6 implements MediaPlayer.OnCompletionListener {
    public static final /* synthetic */ C98074i6 A00 = new C98074i6();

    @Override // android.media.MediaPlayer.OnCompletionListener
    public final void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.release();
    }
}
