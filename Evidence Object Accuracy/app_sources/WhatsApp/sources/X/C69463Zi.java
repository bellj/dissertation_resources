package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.3Zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69463Zi implements AnonymousClass27Z {
    public final /* synthetic */ AbstractActivityC41101su A00;

    public C69463Zi(AbstractActivityC41101su r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass27Z
    public void ANb(int i) {
        C14900mE r1;
        int i2;
        AbstractActivityC41101su r3 = this.A00;
        if (r3.A04.A03()) {
            r1 = ((ActivityC13810kN) r3).A05;
            i2 = R.string.error_camera_disabled_during_video_call;
        } else {
            if (i != 2) {
                r1 = ((ActivityC13810kN) r3).A05;
                i2 = R.string.cannot_start_camera;
            }
            r3.finish();
        }
        r1.A07(i2, 1);
        r3.finish();
    }

    @Override // X.AnonymousClass27Z
    public void AUF() {
        Log.i("qractivity/previewready");
        this.A00.A07 = true;
    }

    @Override // X.AnonymousClass27Z
    public void AUS(C49262Kb r3) {
        Log.i("QrScannerActivity/onQrCodeDetected");
        AbstractActivityC41101su r1 = this.A00;
        if (!r1.A06) {
            r1.A2g(r3);
        }
    }
}
