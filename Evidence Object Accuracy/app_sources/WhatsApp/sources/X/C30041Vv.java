package X;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.List;

/* renamed from: X.1Vv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30041Vv extends C30051Vw {
    public static String A0A(byte b) {
        if (b != 0) {
            if (b == 7) {
                return "system";
            }
            if (b == 1 || b == 25 || b == 42) {
                return "image";
            }
            if (b == 2) {
                return "audio";
            }
            if (b == 3 || b == 28 || b == 43) {
                return "video";
            }
            if (b != 13) {
                if (b == 4) {
                    return "vcard";
                }
                if (b != 5) {
                    if (b == 16) {
                        return "livelocation";
                    }
                    if (b != 9) {
                        if (b == 20) {
                            return "sticker";
                        }
                        if (b == 23) {
                            return "product";
                        }
                        if (b == 37) {
                            return "catalog";
                        }
                        if (b == 24) {
                            return "invite";
                        }
                        if (b != 26) {
                            if (b != 29) {
                                if (b != 30) {
                                    if (b == 45) {
                                        return "list";
                                    }
                                    if (b == 46) {
                                        return "list_response";
                                    }
                                    if (b == 52) {
                                        return "product_list";
                                    }
                                }
                            }
                        }
                    }
                    return "document";
                }
                return "location";
            }
            return "gif";
        }
        return null;
    }

    public static boolean A0F(byte b) {
        return b == 13 || b == 29;
    }

    public static boolean A0G(byte b) {
        return b == 1 || b == 23 || b == 37 || b == 2 || b == 3 || b == 13 || b == 9 || b == 20 || b == 25 || b == 26 || b == 28 || b == 29;
    }

    public static boolean A0H(byte b) {
        return b == 3 || b == 28 || b == 62 || b == 43;
    }

    public static boolean A0I(byte b) {
        return b == 42 || b == 43;
    }

    public static boolean A0J(int i) {
        return i == 12 || i == 79 || i == 20 || i == 90 || i == 93;
    }

    public static int A00(AbstractC16130oV r1) {
        if (A12(r1)) {
            return A11(r1) ? 1 : 0;
        }
        if (A13(r1)) {
            return 2;
        }
        return A11(r1) ? 4 : 3;
    }

    public static long A01(AbstractC15340mz r5) {
        if (r5 == null || r5.A11 == 0) {
            return 1;
        }
        return r5.A11;
    }

    public static long A02(AbstractC15340mz r5) {
        if (r5 == null || r5.A11 == 0) {
            return Long.MIN_VALUE;
        }
        return r5.A12;
    }

    public static Bundle A03(AnonymousClass1IS r3) {
        if (r3 == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("message_key_jid", C15380n4.A03(r3.A00));
        bundle.putBoolean("message_key_from_me", r3.A02);
        bundle.putString("message_key_id", r3.A01);
        return bundle;
    }

    public static C15370n3 A04(C20830wO r2, AbstractC15340mz r3) {
        AbstractC14640lm r1 = r3.A0z.A00;
        if (C15380n4.A0J(r1) || C15380n4.A0N(r1)) {
            r1 = r3.A0B();
        }
        if (r1 != null) {
            return r2.A01(r1);
        }
        return null;
    }

    public static C15580nU A05(AbstractC15340mz r1) {
        if (r1 instanceof C30241Wq) {
            return C15580nU.A02(r1.A0z.A00);
        }
        return null;
    }

    public static AbstractC15340mz A06(C14830m7 r3, C22910zq r4, AbstractC15340mz r5) {
        AbstractC15340mz A07;
        List<AbstractC15340mz> list;
        AbstractC15340mz A072 = A07(r3, r4, r5);
        if (A072 == null && (A072 = A08(r3, r4, r5)) == null) {
            if ((r5 instanceof C27671Iq) && (list = ((C27671Iq) r5).A03) != null) {
                for (AbstractC15340mz r1 : list) {
                    if (!r1.A0z.A02 || (A072 = A07(r3, r4, r1)) == null) {
                    }
                }
            }
            C30311Wx r12 = r5.A18;
            if (r12 == null || !r12.A0z.A02 || (A07 = A07(r3, r4, r12)) == null) {
                return null;
            }
            return A07;
        }
        return A072;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0031, code lost:
        if (r0 != false) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC15340mz A07(X.C14830m7 r8, X.C22910zq r9, X.AbstractC15340mz r10) {
        /*
            boolean r0 = r10 instanceof X.C30311Wx
            if (r0 == 0) goto L_0x0024
            r6 = 30000(0x7530, double:1.4822E-319)
        L_0x0006:
            X.1IS r5 = r10.A0z
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x003d
            int r1 = r10.A0C
            r0 = 4
            int r0 = X.C37381mH.A00(r1, r0)
            if (r0 >= 0) goto L_0x0033
            long r3 = r10.A0I
            long r3 = r3 + r6
            long r1 = r8.A00()
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0033
            java.util.Map r1 = r9.A01
            monitor-enter(r1)
            goto L_0x0028
        L_0x0024:
            r6 = 86400000(0x5265c00, double:4.2687272E-316)
            goto L_0x0006
        L_0x0028:
            boolean r0 = r1.containsKey(r5)     // Catch: all -> 0x002e
            monitor-exit(r1)     // Catch: all -> 0x002e
            goto L_0x0031
        L_0x002e:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x002e
            throw r0
        L_0x0031:
            if (r0 == 0) goto L_0x003c
        L_0x0033:
            int r1 = r10.A0C
            r0 = 7
            if (r1 == r0) goto L_0x003c
            r0 = 20
            if (r1 != r0) goto L_0x003d
        L_0x003c:
            return r10
        L_0x003d:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A07(X.0m7, X.0zq, X.0mz):X.0mz");
    }

    public static AbstractC15340mz A08(C14830m7 r4, C22910zq r5, AbstractC15340mz r6) {
        AbstractC15340mz A07;
        C40481rf r0 = r6.A0V;
        if (r0 != null) {
            for (AbstractC15340mz r1 : r0.A02()) {
                if (r1.A0z.A02 && (A07 = A07(r4, r5, r1)) != null) {
                    return A07;
                }
            }
        }
        return null;
    }

    public static AnonymousClass1IS A09(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        AbstractC14640lm A01 = AbstractC14640lm.A01(bundle.getString("message_key_jid"));
        boolean z = bundle.getBoolean("message_key_from_me");
        String string = bundle.getString("message_key_id");
        if (A01 == null || string == null) {
            return null;
        }
        return new AnonymousClass1IS(A01, string, z);
    }

    public static String A0B(AnonymousClass018 r3, long j) {
        return j <= 0 ? "" : C44891zj.A03(r3, j);
    }

    public static String A0C(AbstractC15340mz r3) {
        StringBuilder sb = new StringBuilder("fmsg/status:");
        sb.append(r3.A0C);
        sb.append("/type:");
        sb.append((int) r3.A0y);
        AnonymousClass1IS r1 = r3.A0z;
        if (C15380n4.A0J(r1.A00) && (r3 instanceof AnonymousClass1XB)) {
            sb.append("/grp_action:");
            sb.append(((AnonymousClass1XB) r3).A00);
        }
        sb.append("/rmt-src:");
        sb.append(r3.A0B());
        sb.append(" ");
        sb.append(r1.toString());
        return sb.toString();
    }

    public static void A0D(C15570nT r0, C19990v2 r1, AbstractC15340mz r2) {
        AnonymousClass1PE A06;
        if (A0K(r0, r2) && (A06 = r1.A06(r2.A0z.A00)) != null) {
            synchronized (A06) {
                int i = A06.A04;
                if (i > 0) {
                    A06.A04 = i - 1;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("chatInfo/decrementUnseenImportantMessageCount/");
                sb.append(A06.A07());
                Log.i(sb.toString());
            }
        }
    }

    public static void A0E(AbstractC15340mz r2) {
        C38101nW A14;
        C38101nW A142;
        if (r2.A0G() != null) {
            r2.A0G().A01 = true;
        }
        if ((r2 instanceof AbstractC16130oV) && (A142 = ((AbstractC16130oV) r2).A14()) != null) {
            A142.A00 = true;
        }
        AbstractC15340mz A0E = r2.A0E();
        if (A0E != null) {
            if (A0E.A0G() != null) {
                r2.A0E().A0G().A01 = true;
            }
            AbstractC15340mz A0E2 = r2.A0E();
            if ((A0E2 instanceof AbstractC16130oV) && (A14 = ((AbstractC16130oV) A0E2).A14()) != null) {
                A14.A00 = true;
            }
        }
    }

    public static boolean A0K(C15570nT r2, AbstractC15340mz r3) {
        AnonymousClass1IS r1 = r3.A0z;
        if (r1.A02 || A0m(r3) || !C15380n4.A0J(r1.A00)) {
            return false;
        }
        List list = r3.A0o;
        if (list != null) {
            r2.A08();
            if (list.contains(r2.A05)) {
                return true;
            }
        }
        AbstractC15340mz A0E = r3.A0E();
        return A0E != null && A0E.A0z.A02;
    }

    public static boolean A0L(C15570nT r2, AbstractC15340mz r3) {
        if (!(r3 instanceof AnonymousClass1XB)) {
            return false;
        }
        int i = ((AnonymousClass1XB) r3).A00;
        if (A0J(i) || i == 52) {
            List list = ((C30461Xm) r3).A01;
            r2.A08();
            C27631Ih r0 = r2.A05;
            AnonymousClass009.A05(r0);
            return list.contains(r0);
        } else if (i == 4) {
            return r2.A0F(r3.A0B());
        } else {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003a, code lost:
        if (r1 != 12) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0M(X.C15570nT r4, X.AbstractC15340mz r5) {
        /*
            X.1IS r0 = r5.A0z
            boolean r0 = r0.A02
            r3 = 0
            if (r0 == 0) goto L_0x003c
            boolean r0 = r5 instanceof X.AnonymousClass1XB
            r2 = 1
            if (r0 == 0) goto L_0x0057
            r0 = r5
            X.1XB r0 = (X.AnonymousClass1XB) r0
            int r1 = r0.A00
            if (r1 == r2) goto L_0x004e
            r0 = 14
            if (r1 == r0) goto L_0x004e
            r0 = 17
            if (r1 == r0) goto L_0x004e
            r0 = 20
            if (r1 == r0) goto L_0x003d
            r0 = 27
            if (r1 == r0) goto L_0x004e
            r0 = 79
            if (r1 == r0) goto L_0x003d
            r0 = 90
            if (r1 == r0) goto L_0x004e
            r0 = 4
            if (r1 == r0) goto L_0x004e
            r0 = 5
            if (r1 == r0) goto L_0x004e
            r0 = 6
            if (r1 == r0) goto L_0x004e
            r0 = 11
            if (r1 == r0) goto L_0x004e
            r0 = 12
            if (r1 == r0) goto L_0x004e
        L_0x003c:
            return r3
        L_0x003d:
            X.1Xm r5 = (X.C30461Xm) r5
            java.util.List r1 = r5.A01
            r4.A08()
            X.1Ih r0 = r4.A05
            X.AnonymousClass009.A05(r0)
            boolean r0 = r1.contains(r0)
            return r0
        L_0x004e:
            X.0lm r0 = r5.A0B()
            boolean r0 = r4.A0F(r0)
            return r0
        L_0x0057:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A0M(X.0nT, X.0mz):boolean");
    }

    public static boolean A0N(C15570nT r2, AbstractC15340mz r3) {
        return !r3.A0z.A02 && r3.A0y == 64 && r2.A0F(((C30321Wy) r3).A00) && !r3.A1B;
    }

    public static boolean A0O(C15570nT r2, AbstractC15340mz r3) {
        return (r3 instanceof AnonymousClass1Y5) && ((AnonymousClass1XB) r3).A00 == 88 && !r2.A0F(r3.A0B());
    }

    public static boolean A0P(C15570nT r3, AbstractC15340mz r4) {
        AnonymousClass1IS r2 = r4.A0z;
        return r2.A02 && r4.A0C != 6 && r3.A0F(r2.A00);
    }

    public static boolean A0Q(C15570nT r2, C30331Wz r3) {
        boolean A0F;
        byte b = r3.A0y;
        if (b == 15) {
            A0F = r3.A0z.A02;
        } else if (b != 64) {
            return false;
        } else {
            A0F = r2.A0F(((C30321Wy) r3).A00);
        }
        if (!A0F || r3.A1B) {
            return true;
        }
        return false;
    }

    public static boolean A0R(C15450nH r7, C14850m9 r8, AbstractC15340mz r9) {
        C30061Vy r1;
        AnonymousClass1KB r0;
        String str;
        String str2;
        if ((!(r9 instanceof AnonymousClass1XV) || ((AnonymousClass1XV) r9).A01 != null) && !(r9 instanceof AnonymousClass1XF)) {
            if (r9 instanceof AbstractC16390ow) {
                C33711ex ACL = ((AbstractC16390ow) r9).ACL();
                if (ACL != null && ACL.A02() != AnonymousClass24E.A02) {
                    return true;
                }
            } else {
                byte b = r9.A0y;
                if (!(A0I(b) || b == 10 || b == 11 || b == 31 || b == 12 || A0m(r9))) {
                    r9.A0I();
                    if (r9.A0L == null) {
                        if (A0G(b)) {
                            AbstractC16130oV r12 = (AbstractC16130oV) r9;
                            C16150oX r3 = r12.A02;
                            if (r3 == null) {
                                str2 = "userActionForwardMessage/media_data is null";
                            } else {
                                if (!(r9 instanceof AnonymousClass1X2) || !A14((AnonymousClass1X2) r9)) {
                                    if (r3.A0F == null) {
                                        str2 = "userActionForwardMessage/media_data.file is null";
                                    } else if (!r3.A03()) {
                                        str2 = "userActionForwardMessage/media_data.file does not exist";
                                    } else {
                                        long j = r3.A0A;
                                        long j2 = 0;
                                        if (j != 0) {
                                            File file = r3.A0F;
                                            if (file != null) {
                                                j2 = file.length();
                                            }
                                            if (j != j2) {
                                                StringBuilder sb = new StringBuilder("userActionForwardMessage/original_size:");
                                                sb.append(r3.A0A);
                                                sb.append(" file_length:");
                                                sb.append(r3.A0F.length());
                                                str = sb.toString();
                                                Log.w(str);
                                                return false;
                                            }
                                        }
                                        if (r9.A0z.A02 && !r3.A0P && !r7.A05(AbstractC15460nI.A0k)) {
                                            str = "userActionForwardMessage/cannot forward partially uploaded message.";
                                            Log.w(str);
                                            return false;
                                        }
                                    }
                                } else if (r12.A04 == null) {
                                    return false;
                                }
                                if ((r9 instanceof C30061Vy) && (r0 = (r1 = (C30061Vy) r9).A01) != null && r0.A05 && !r1.A0z.A02) {
                                    return false;
                                }
                            }
                            Log.e(str2);
                            return false;
                        }
                        if (!A0V(r8, r9) && !A0l(r9) && ((r9 instanceof AbstractC16410oy) || (r9 instanceof AbstractC16130oV))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean A0S(C15550nR r3, C22700zV r4, C15600nX r5, C14850m9 r6, C20710wC r7, AbstractC15340mz r8, C23000zz r9) {
        boolean z;
        C15370n3 A0A;
        AbstractC14640lm r1 = r8.A0z.A00;
        if (C41861uH.A00(r6, r1)) {
            return false;
        }
        GroupJid of = GroupJid.of(r1);
        if (of == null || (((A0A = r3.A0A(of)) == null || !r7.A0Z(A0A, of)) && r5.A0C(of))) {
            z = true;
        } else {
            z = false;
        }
        if (!A0u(r8) || !z || C28081Kn.A00(r3, r4, r6, r8, r9) || C28061Kl.A00(r3, r4, r6, r8)) {
            return false;
        }
        return true;
    }

    public static boolean A0T(C16970q3 r3, C14820m6 r4, AbstractC15340mz r5) {
        if (!r3.A02.A00.getBoolean("archive_v2_enabled", false)) {
            if (r5.A0z.A02) {
                return true;
            }
            r3.A00.A08();
            Log.i("archiveutil/enableArchiveV2IfNeededForMessage/Enabling archive2.0");
            r3.A04();
        }
        return r4.A00.getBoolean("notify_new_message_for_archived_chats", false);
    }

    public static boolean A0U(C14850m9 r3, AbstractC15340mz r4) {
        if (!(r4 instanceof AnonymousClass1X7)) {
            return false;
        }
        AbstractC16130oV r1 = (AbstractC16130oV) r4;
        if (!C15380n4.A0N(r4.A0z.A00) || !r3.A07(252)) {
            return false;
        }
        C38101nW A14 = r1.A14();
        AnonymousClass009.A05(A14);
        C16150oX r0 = A14.A04.A02;
        AnonymousClass009.A05(r0);
        if (r0.A0M) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0V(X.C14850m9 r3, X.AbstractC15340mz r4) {
        /*
            boolean r0 = r4 instanceof X.C28851Pg
            if (r0 == 0) goto L_0x002a
            X.1Pg r4 = (X.C28851Pg) r4
            X.1Pk r0 = r4.A00
            java.util.List r0 = r0.A04
            if (r0 == 0) goto L_0x002a
            java.util.Iterator r2 = r0.iterator()
        L_0x0010:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x002a
            java.lang.Object r1 = r2.next()
            X.1Ys r1 = (X.C30761Ys) r1
            boolean r0 = A0Z(r3, r1)
            if (r0 != 0) goto L_0x0028
            boolean r0 = A0a(r3, r1)
            if (r0 == 0) goto L_0x0010
        L_0x0028:
            r0 = 1
            return r0
        L_0x002a:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A0V(X.0m9, X.0mz):boolean");
    }

    public static boolean A0W(C14850m9 r3, AbstractC15340mz r4) {
        byte b;
        AnonymousClass1IS r1 = r4.A0z;
        if (r1.A02 || C41861uH.A00(r3, r1.A00) || (b = r4.A0y) == 8 || b == 10 || b == 7 || b == 12 || A0m(r4) || b == 19 || C32401c6.A0M(r4)) {
            return false;
        }
        return true;
    }

    public static boolean A0X(C14850m9 r2, AbstractC15340mz r3) {
        int i;
        if (r3 instanceof C16440p1) {
            i = 253;
        } else if ((!(r3 instanceof AnonymousClass1X7) && !(r3 instanceof AnonymousClass1X3)) || !C15380n4.A0N(r3.A0z.A00)) {
            return true;
        } else {
            i = 252;
        }
        return !r2.A07(i);
    }

    public static boolean A0Y(C14850m9 r2, AbstractC15340mz r3, boolean z) {
        if (r3 instanceof AbstractC16130oV) {
            if ((r3 instanceof C16440p1) && r2.A07(247)) {
                return true;
            }
            if (r2.A07(246) && ((r3 instanceof AnonymousClass1X4) || (r3 instanceof AnonymousClass1X7))) {
                return z;
            }
        }
        return false;
    }

    public static boolean A0Z(C14850m9 r2, C30761Ys r3) {
        return !r2.A07(1023) && r3.A03 == 2 && r3.A05.startsWith(r2.A03(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
    }

    public static boolean A0a(C14850m9 r2, C30761Ys r3) {
        return !r2.A07(1544) && r3.A03 == 2 && r3.A05.startsWith(r2.A03(1547));
    }

    public static boolean A0b(AbstractC15340mz r1) {
        if (r1.A0r) {
            AnonymousClass1IS r12 = r1.A0z;
            if (r12.A02 && !C15380n4.A0F(r12.A00)) {
                return true;
            }
        }
        return false;
    }

    public static boolean A0c(AbstractC15340mz r6) {
        if (!(r6 instanceof AnonymousClass1XB)) {
            return false;
        }
        long j = (long) ((AnonymousClass1XB) r6).A00;
        if (j == 61 || j == 69) {
            return true;
        }
        return false;
    }

    public static boolean A0d(AbstractC15340mz r2) {
        return r2.A0z.A02 && A0p(r2) && ((AnonymousClass1XB) r2).A00 == 67;
    }

    public static boolean A0e(AbstractC15340mz r1) {
        if (!r1.A0z.A02 || !A0p(r1)) {
            return false;
        }
        int i = ((AnonymousClass1XB) r1).A00;
        return i == 28 || i == 10;
    }

    public static boolean A0f(AbstractC15340mz r2) {
        return r2.A0z.A02 && A0p(r2) && ((AnonymousClass1XB) r2).A00 == 57;
    }

    public static boolean A0g(AbstractC15340mz r2) {
        return r2.A0z.A02 && A0p(r2) && ((AnonymousClass1XB) r2).A00 == 71;
    }

    public static boolean A0h(AbstractC15340mz r2) {
        if (r2.A0y != 36) {
            return r2.A0z.A02 && A0p(r2) && (r2 instanceof AnonymousClass1XB) && ((AnonymousClass1XB) r2).A00 == 59;
        }
        return true;
    }

    public static boolean A0i(AbstractC15340mz r3) {
        if ((!A0r(r3) || !r3.A12(1)) && r3.A08 != 3) {
            return false;
        }
        return true;
    }

    public static boolean A0j(AbstractC15340mz r2) {
        return r2.A0z.A02 && A0p(r2) && ((AnonymousClass1XB) r2).A00 == 18;
    }

    public static boolean A0k(AbstractC15340mz r6) {
        boolean z;
        if ((r6 instanceof AnonymousClass1XV) || !(r6 instanceof AbstractC16130oV)) {
            return false;
        }
        boolean z2 = false;
        if (r6.A05 >= 127) {
            z2 = true;
        }
        if (r6 instanceof AnonymousClass1XH) {
            return false;
        }
        C16150oX r0 = ((AbstractC16130oV) r6).A02;
        if (r0 != null) {
            z = r0.A0P;
        } else {
            z = false;
        }
        boolean z3 = false;
        if (Build.VERSION.SDK_INT >= 29) {
            z3 = true;
        }
        if (!r6.A0z.A02 && !z) {
            return false;
        }
        if (!z3 || !z2) {
            return true;
        }
        return false;
    }

    public static boolean A0l(AbstractC15340mz r3) {
        List<AnonymousClass1ZN> list;
        if (C35011h5.A04(r3) && (list = r3.A0F().A00.A02) != null) {
            for (AnonymousClass1ZN r0 : list) {
                if (r0.A02 != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean A0m(AbstractC15340mz r2) {
        byte b = r2.A0y;
        return b == 15 || b == 64;
    }

    public static boolean A0n(AbstractC15340mz r2) {
        C38101nW A14;
        if (!(r2 instanceof AnonymousClass1X7) || (A14 = ((AbstractC16130oV) r2).A14()) == null) {
            return false;
        }
        C16150oX r0 = A14.A04.A02;
        AnonymousClass009.A05(r0);
        if (r0.A0M) {
            return true;
        }
        return false;
    }

    public static boolean A0o(AbstractC15340mz r1) {
        if (r1 instanceof AnonymousClass1X2) {
            C38101nW A14 = ((AbstractC16130oV) r1).A14();
            AnonymousClass009.A05(A14);
            C16150oX r0 = A14.A04.A02;
            AnonymousClass009.A05(r0);
            if (r0.A0M && (!C38241nl.A02())) {
                return true;
            }
        }
        return false;
    }

    public static boolean A0p(AbstractC15340mz r3) {
        byte b = r3.A0y;
        return b == 0 ? r3.A0C == 6 : b == 7;
    }

    public static boolean A0q(AbstractC15340mz r1) {
        if (!(r1 instanceof C28861Ph)) {
            return false;
        }
        C28861Ph r12 = (C28861Ph) r1;
        return !TextUtils.isEmpty(r12.A05) || !TextUtils.isEmpty(r12.A03);
    }

    public static boolean A0r(AbstractC15340mz r4) {
        if (!(r4 instanceof C16440p1)) {
            return false;
        }
        C16150oX r2 = ((AbstractC16130oV) r4).A02;
        if (r4.A08 != 7 || r2 == null || r2.A01 <= 0) {
            return false;
        }
        return true;
    }

    public static boolean A0s(AbstractC15340mz r6) {
        if (!(r6 instanceof AnonymousClass1XB)) {
            return false;
        }
        long j = (long) ((AnonymousClass1XB) r6).A00;
        if (j == 22 || j == 34 || j == 35 || j == 36 || j == 23 || j == 24 || j == 25 || j == 26 || j == 46 || j == 47 || j == 48 || j == 49 || j == 50 || j == 55) {
            return true;
        }
        return false;
    }

    public static boolean A0t(AbstractC15340mz r1) {
        return r1.A12(512) || r1.A12(4096);
    }

    public static boolean A0u(AbstractC15340mz r6) {
        boolean z;
        int i;
        AbstractC30891Zf r0;
        boolean z2 = r6.A0z.A02;
        if (!z2 || C37381mH.A00(r6.A0C, 4) >= 0) {
            byte b = r6.A0y;
            if (b == 0) {
                AnonymousClass1IR r3 = r6.A0L;
                if (r3 == null || !z2 || !((i = r3.A02) == 401 || i == 406 || i == 407 || i == 403 || i == 412 || (i == 408 && (r0 = r3.A0A) != null && r0.A06() == 403))) {
                    z = false;
                } else {
                    z = true;
                }
                return !z;
            } else if (b != 10 && b != 12 && !A0m(r6) && !C32401c6.A0M(r6)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        if (r1 >= 5) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0v(X.AbstractC15340mz r5) {
        /*
            int r4 = r5.A05
            r3 = 127(0x7f, float:1.78E-43)
            r0 = 0
            if (r4 < r3) goto L_0x0008
            r0 = 1
        L_0x0008:
            r2 = 0
            if (r0 != 0) goto L_0x0019
            X.1IS r0 = r5.A0z
            boolean r1 = r0.A02
            r0 = 1
            if (r1 == 0) goto L_0x001a
            int r1 = java.lang.Math.max(r0, r4)
        L_0x0016:
            if (r1 != r3) goto L_0x0019
        L_0x0018:
            r2 = 1
        L_0x0019:
            return r2
        L_0x001a:
            int r4 = r4 + r0
            int r1 = java.lang.Math.max(r0, r4)
            r0 = 5
            if (r1 < r0) goto L_0x0016
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A0v(X.0mz):boolean");
    }

    public static boolean A0w(AbstractC15340mz r4, long j) {
        if (r4.A04 <= 0) {
            return false;
        }
        Long l = r4.A0Y;
        AnonymousClass009.A05(l);
        if (l.longValue() >= j || r4.A06() == 1) {
            return false;
        }
        return true;
    }

    public static boolean A0x(AbstractC15340mz r5, boolean z) {
        AnonymousClass1IS r2 = r5.A0z;
        AbstractC14640lm r1 = r2.A00;
        return z && !(r5 instanceof AnonymousClass1XB) && r2.A02 && ((r1 instanceof UserJid) || (r1 instanceof AbstractC15590nW)) && r5.A0G == 0 && r5.A17 == null && r5.A0B == 0;
    }

    public static boolean A0y(AbstractC15340mz r3, boolean z) {
        AbstractC14640lm r1;
        int i;
        if (A0b(r3) && !z) {
            return false;
        }
        byte b = r3.A0y;
        if (b == 24) {
            if (r3.A0z.A02 && !z) {
                return false;
            }
        } else if (b == 8) {
            return false;
        }
        if (A0d(r3) || A0j(r3) || A0f(r3) || A0g(r3)) {
            return false;
        }
        AnonymousClass1IS r2 = r3.A0z;
        if ((r2.A02 && A0p(r3) && ((i = ((AnonymousClass1XB) r3).A00) == 50 || i == 49 || i == 47 || i == 48 || i == 46 || i == 55)) || A0s(r3)) {
            return false;
        }
        if ((A0e(r3) && (!z || (r1 = r2.A00) == null || !r1.equals(((C30511Xs) r3).A01))) || (r3 instanceof C30581Xz) || A0c(r3)) {
            return false;
        }
        if (!(r3 instanceof AnonymousClass1XB)) {
            return true;
        }
        long j = (long) ((AnonymousClass1XB) r3).A00;
        if (j == 62 || j == 63) {
            return false;
        }
        return true;
    }

    public static boolean A0z(AbstractC16130oV r6) {
        int[] A06;
        C38101nW A14 = r6.A14();
        if (A14 == null) {
            return false;
        }
        C16150oX r0 = A14.A04.A02;
        AnonymousClass009.A05(r0);
        if (!r0.A0M || (A06 = A14.A06()) == null || A06.length < 4) {
            return false;
        }
        long j = (long) (A06[0] + A06[1] + A06[2]);
        C16150oX r02 = r6.A02;
        AnonymousClass009.A05(r02);
        if (r02.A09 >= j) {
            return true;
        }
        return false;
    }

    public static boolean A10(AbstractC16130oV r3) {
        byte b = r3.A0y;
        if (b == 2) {
            if (((AbstractC15340mz) r3).A08 == 1) {
                return true;
            }
        } else if (b == 20) {
            return true;
        }
        if (!A0I(b)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r6.A0z.A02 != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A11(X.AbstractC16130oV r6) {
        /*
            boolean r0 = X.C63103Ah.A00(r6)
            r5 = 1
            if (r0 != 0) goto L_0x002a
            X.0oX r2 = r6.A02
            X.AnonymousClass009.A05(r2)
            boolean r0 = r2.A0X
            if (r0 == 0) goto L_0x0017
            X.1IS r0 = r6.A0z
            boolean r0 = r0.A02
            r1 = 1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r1 = 0
        L_0x0018:
            boolean r0 = r2.A0P
            if (r0 == 0) goto L_0x0026
            if (r1 != 0) goto L_0x0026
            long r3 = r6.A12
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x002b
        L_0x0026:
            java.lang.String r0 = r6.A08
            if (r0 == 0) goto L_0x002b
        L_0x002a:
            return r5
        L_0x002b:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A11(X.0oV):boolean");
    }

    public static boolean A12(AbstractC16130oV r1) {
        C16150oX r12 = r1.A02;
        AnonymousClass009.A05(r12);
        return r12.A0a && !r12.A0Y;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if (r4.A0z.A02 != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A13(X.AbstractC16130oV r4) {
        /*
            boolean r0 = X.C63103Ah.A00(r4)
            r3 = 0
            if (r0 != 0) goto L_0x0031
            X.0oX r2 = r4.A02
            X.AnonymousClass009.A05(r2)
            boolean r0 = r2.A0X
            if (r0 == 0) goto L_0x0017
            X.1IS r0 = r4.A0z
            boolean r0 = r0.A02
            r1 = 1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r1 = 0
        L_0x0018:
            boolean r0 = r2.A0P
            if (r0 == 0) goto L_0x001e
            if (r1 == 0) goto L_0x0030
        L_0x001e:
            boolean r0 = r4.A0r
            if (r0 == 0) goto L_0x0031
            X.1IS r1 = r4.A0z
            boolean r0 = r1.A02
            if (r0 == 0) goto L_0x0031
            X.0lm r0 = r1.A00
            boolean r0 = X.C15380n4.A0F(r0)
            if (r0 != 0) goto L_0x0031
        L_0x0030:
            r3 = 1
        L_0x0031:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30041Vv.A13(X.0oV):boolean");
    }

    public static boolean A14(AnonymousClass1X2 r5) {
        C16150oX r1 = ((AbstractC16130oV) r5).A02;
        return A0H(r5.A0y) && r1 != null && !r5.A0z.A02 && !r1.A0P && r1.A0C > 0;
    }

    public static boolean A15(AnonymousClass1X4 r3) {
        C16150oX r2 = ((AbstractC16130oV) r3).A02;
        if (!A0H(r3.A0y) || r2 == null || !r3.A0z.A02 || !r3.A12(1) || r2.A0P || ((AbstractC16130oV) r3).A08 == null) {
            return false;
        }
        return true;
    }

    public static boolean A16(AnonymousClass1X4 r5) {
        C16150oX r4 = ((AbstractC16130oV) r5).A02;
        A0o(r5);
        boolean z = r5.A0z.A02;
        if (r4 == null || !A0o(r5)) {
            return false;
        }
        if ((!z || A15(r5)) && !r4.A0P && r4.A07 == 0) {
            return true;
        }
        return false;
    }
}
