package X;

import android.speech.tts.TextToSpeech;
import com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView;
import com.whatsapp.util.Log;

/* renamed from: X.63t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C1316963t implements TextToSpeech.OnInitListener {
    public final /* synthetic */ NoviSelfieFaceAnimationView A00;
    public final /* synthetic */ String A01;

    public /* synthetic */ C1316963t(NoviSelfieFaceAnimationView noviSelfieFaceAnimationView, String str) {
        this.A00 = noviSelfieFaceAnimationView;
        this.A01 = str;
    }

    @Override // android.speech.tts.TextToSpeech.OnInitListener
    public final void onInit(int i) {
        String str;
        NoviSelfieFaceAnimationView noviSelfieFaceAnimationView = this.A00;
        String str2 = this.A01;
        if (i == 0) {
            int language = noviSelfieFaceAnimationView.A03.setLanguage(C12970iu.A14(noviSelfieFaceAnimationView.A0A));
            if (language == -1 || language == -2) {
                str = "PAY: NoviSelfieFaceAnimationView/Language is not supported";
            } else if (noviSelfieFaceAnimationView.A03.speak(str2, 0, null) == -1) {
                str = "PAY: NoviSelfieFaceAnimationView/Error converting Text to Speech";
            } else {
                return;
            }
        } else {
            str = "PAY: NoviSelfieFaceAnimationView/Text to speech initialization failed";
        }
        Log.e(str);
    }
}
