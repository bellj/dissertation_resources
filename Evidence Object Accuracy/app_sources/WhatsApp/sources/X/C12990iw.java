package X;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape0S0220102_I1;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.ViewOnClickCListenerShape1S0300000_I1;
import com.whatsapp.R;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C12990iw {
    public static char A00(String str, StringBuilder sb) {
        sb.append(str);
        sb.append('\'');
        return '\'';
    }

    public static float A01(float f, float f2, int i) {
        return i != 0 ? (f * f2) / 100.0f : f;
    }

    public static float A02(View view) {
        return (float) view.getWidth();
    }

    public static float A03(View view) {
        return (float) view.getHeight();
    }

    public static int A04(int i) {
        return i != 0 ? 1 : 2;
    }

    public static int A05(int i, float f) {
        return (i + Float.floatToIntBits(f)) * 31;
    }

    public static int A06(Cursor cursor, String str) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(str));
    }

    public static int A07(View view, int i) {
        return view.getResources().getDimensionPixelSize(i);
    }

    public static int A08(Object obj, int i) {
        return i + obj.hashCode();
    }

    public static int A09(List list, int i) {
        return list.size() - i;
    }

    public static int A0A(boolean z) {
        return z ? 0 : 8;
    }

    public static int A0B(byte[] bArr, int[] iArr, int i, int i2) {
        int A00 = AbstractC95434di.A00(bArr, i);
        iArr[i2] = A00;
        return A00;
    }

    public static int A0C(int[] iArr, int i, int i2, int i3, int i4) {
        iArr[i2] = i;
        int i5 = i3 ^ i;
        iArr[i4] = i5;
        return i5;
    }

    public static Intent A0D(Context context, Class cls) {
        return new Intent(context, cls);
    }

    public static Intent A0E(String str) {
        return new Intent(str);
    }

    public static Paint A0F() {
        return new Paint();
    }

    public static Paint A0G(int i) {
        return new Paint(i);
    }

    public static Bundle A0H(Activity activity) {
        return activity.getIntent().getExtras();
    }

    public static Parcelable A0I(Parcel parcel, Class cls) {
        return parcel.readParcelable(cls.getClassLoader());
    }

    public static SpannableStringBuilder A0J(CharSequence charSequence) {
        return new SpannableStringBuilder(charSequence);
    }

    public static DisplayMetrics A0K(View view) {
        return view.getResources().getDisplayMetrics();
    }

    public static Pair A0L(Object obj, Object obj2) {
        return new Pair(obj, obj2);
    }

    public static FrameLayout.LayoutParams A0M() {
        return new FrameLayout.LayoutParams(-1, -1);
    }

    public static TextView A0N(Activity activity, int i) {
        return (TextView) AnonymousClass00T.A05(activity, i);
    }

    public static AnonymousClass04S A0O(C004802e r1, Object obj, int i, int i2) {
        r1.setNegativeButton(i2, new IDxCListenerShape8S0100000_1_I1(obj, i));
        return r1.create();
    }

    public static AnonymousClass017 A0P(AbstractC16710pd r0) {
        return (AnonymousClass017) r0.getValue();
    }

    public static LinearLayoutManager A0Q(View view) {
        view.getContext();
        return new LinearLayoutManager(0);
    }

    public static RecyclerView A0R(View view, int i) {
        return (RecyclerView) AnonymousClass028.A0D(view, i);
    }

    public static AnonymousClass01J A0S(Context context) {
        return (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
    }

    public static AnonymousClass18U A0T(AnonymousClass01J r0) {
        return (AnonymousClass18U) r0.AAU.get();
    }

    public static C18790t3 A0U(AnonymousClass01J r0) {
        return (C18790t3) r0.AJw.get();
    }

    public static C252918v A0V(AnonymousClass01J r0) {
        return (C252918v) r0.A2x.get();
    }

    public static C16430p0 A0W(AnonymousClass01J r0) {
        return (C16430p0) r0.A5y.get();
    }

    public static CatalogSearchViewModel A0X(CatalogSearchFragment catalogSearchFragment) {
        return (CatalogSearchViewModel) catalogSearchFragment.A0a.getValue();
    }

    public static AnonymousClass130 A0Y(AnonymousClass01J r0) {
        return (AnonymousClass130) r0.A41.get();
    }

    public static AnonymousClass10S A0Z(AnonymousClass01J r0) {
        return (AnonymousClass10S) r0.A46.get();
    }

    public static C17170qN A0a(AnonymousClass01J r0) {
        return (C17170qN) r0.AMt.get();
    }

    public static AbstractC14640lm A0b(Iterator it) {
        return (AbstractC14640lm) it.next();
    }

    public static C17220qS A0c(AnonymousClass01J r0) {
        return (C17220qS) r0.ABt.get();
    }

    public static C20660w7 A0d(AnonymousClass01J r0) {
        return (C20660w7) r0.AIB.get();
    }

    public static C16630pM A0e(AnonymousClass01J r0) {
        return (C16630pM) r0.AIc.get();
    }

    public static AnonymousClass12F A0f(AnonymousClass01J r0) {
        return (AnonymousClass12F) r0.AJM.get();
    }

    public static AnonymousClass1YT A0g(AbstractList abstractList, int i) {
        return (AnonymousClass1YT) abstractList.get(i);
    }

    public static BufferedInputStream A0h(File file) {
        return new BufferedInputStream(new FileInputStream(file));
    }

    public static IOException A0i(String str) {
        return new IOException(str);
    }

    public static Integer A0j() {
        return 6;
    }

    public static Object A0k(List list, int i) {
        return ((Pair) list.get(i)).first;
    }

    public static Object A0l(Map map, int i) {
        return map.get(Integer.valueOf(i));
    }

    public static RuntimeException A0m(String str) {
        return new RuntimeException(str);
    }

    public static String A0n() {
        return UUID.randomUUID().toString();
    }

    public static String A0o(Resources resources, Object obj, Object[] objArr, int i, int i2) {
        objArr[i] = obj;
        return resources.getString(i2, objArr);
    }

    public static String A0p(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        return readString;
    }

    public static String A0q(View view, int i) {
        return view.getContext().getString(i);
    }

    public static String A0r(Map.Entry entry) {
        return (String) entry.getKey();
    }

    public static Iterator A0s(AbstractMap abstractMap) {
        return abstractMap.entrySet().iterator();
    }

    public static Iterator A0t(AbstractMap abstractMap) {
        return abstractMap.values().iterator();
    }

    public static Map A0u(List list, int i) {
        return (Map) list.get(i);
    }

    public static C113285Gx A0v() {
        return new C113285Gx();
    }

    public static void A0w(Animator animator, Object obj, int i) {
        animator.addListener(new IDxLAdapterShape1S0100000_2_I1(obj, i));
    }

    public static void A0x(Context context, ImageView imageView, int i) {
        imageView.setImageDrawable(AnonymousClass00T.A04(context, i));
    }

    public static void A0y(Context context, String str, StringBuilder sb, int i) {
        sb.append(context.getString(i));
        sb.append(str);
    }

    public static void A0z(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, C004802e r2, int i) {
        r2.setPositiveButton(i, onClickListener);
        r2.setNegativeButton(R.string.cancel, onClickListener2);
    }

    public static void A10(Intent intent, View view) {
        view.getContext().startActivity(intent);
    }

    public static void A11(SharedPreferences.Editor editor, String str) {
        editor.remove(str).apply();
    }

    public static void A12(Canvas canvas, RectF rectF, float f) {
        canvas.rotate(f, rectF.centerX(), rectF.centerY());
    }

    public static void A13(Paint paint) {
        paint.setStyle(Paint.Style.STROKE);
    }

    public static void A14(Paint paint, PorterDuff.Mode mode) {
        paint.setXfermode(new PorterDuffXfermode(mode));
    }

    public static void A15(Path path, View view, int i) {
        path.lineTo((float) i, (float) ((view.getHeight() * 9) / 10));
    }

    public static void A16(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    public static void A17(Handler handler) {
        handler.sendEmptyMessageDelayed(0, 50);
    }

    public static void A18(IBinder iBinder, Parcel parcel, Parcel parcel2, int i) {
        iBinder.transact(i, parcel, parcel2, 0);
        parcel2.readException();
    }

    public static void A19(View view, int i) {
        int i2 = 0;
        if (i != 0) {
            i2 = 8;
        }
        view.setVisibility(i2);
    }

    public static void A1A(View view, int i, int i2) {
        view.setPadding(i, i2, view.getPaddingRight(), view.getPaddingBottom());
    }

    public static void A1B(View view, Animation animation) {
        animation.setDuration(300);
        view.startAnimation(animation);
    }

    public static void A1C(View view, Object obj, Object obj2, Object obj3, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape1S0300000_I1(obj, obj2, obj3, i));
    }

    public static void A1D(ImageView imageView) {
        imageView.setImageDrawable(new ColorDrawable(-7829368));
    }

    public static void A1E(ImageView imageView) {
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    public static void A1F(TextView textView) {
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void A1G(TextView textView) {
        textView.setText("");
    }

    public static void A1H(TextView textView, AnonymousClass01E r2, int i) {
        textView.setText(r2.A0I(i));
    }

    public static void A1I(TextView textView, InputFilter[] inputFilterArr, int i) {
        inputFilterArr[0] = new C100654mG(i);
        textView.setFilters(inputFilterArr);
    }

    public static void A1J(AnonymousClass017 r1, boolean z) {
        r1.A0B(Boolean.valueOf(z));
    }

    public static void A1K(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager());
    }

    public static void A1L(RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I1) {
        runnableBRunnable0Shape0S0220102_I1.A05 = false;
        runnableBRunnable0Shape0S0220102_I1.A06 = true;
    }

    public static void A1M(C44931zn r1, int i) {
        r1.A0D = Integer.valueOf(i);
    }

    public static void A1N(AbstractC16350or r1, AbstractC14440lR r2) {
        r2.Aaz(r1, new Void[0]);
    }

    public static void A1O(AbstractC14440lR r1, Object obj, int i) {
        r1.Ab2(new RunnableBRunnable0Shape14S0100000_I1(obj, i));
    }

    public static void A1P(Object obj, Object obj2, Object[] objArr) {
        objArr[0] = obj;
        objArr[1] = obj2;
    }

    public static void A1Q(Object obj, AbstractMap abstractMap, Map.Entry entry) {
        abstractMap.put(obj, entry.getValue());
    }

    public static void A1R(String str, Locale locale, Object[] objArr) {
        Log.i(String.format(locale, str, objArr));
    }

    public static void A1S(String str, Mac mac, byte[] bArr) {
        mac.init(new SecretKeySpec(bArr, str));
    }

    public static void A1T(StringBuilder sb, String str) {
        sb.append(str);
        sb.append(": ");
    }

    public static void A1U(AbstractList abstractList, Object[] objArr, int i) {
        objArr[i] = abstractList.get(i);
    }

    public static void A1V(Object[] objArr, int i) {
        objArr[2] = Integer.valueOf(i);
    }

    public static boolean A1W(int i) {
        return i >= 0;
    }

    public static boolean A1X(int i, int i2) {
        return i >= i2;
    }

    public static boolean A1Y(int i, int i2) {
        return i < i2;
    }

    public static boolean A1Z(Object obj, boolean z) {
        return obj.equals(Boolean.valueOf(z));
    }

    public static boolean A1a(String str, String str2, Object[] objArr) {
        objArr[0] = new AnonymousClass1W9(str, str2);
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static String[] A1b(Object obj, Object obj2, int i) {
        String[] strArr = new String[i];
        strArr[0] = obj;
        strArr[1] = obj2;
        return strArr;
    }
}
