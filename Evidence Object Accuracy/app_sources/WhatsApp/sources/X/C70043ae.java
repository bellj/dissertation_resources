package X;

import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;

/* renamed from: X.3ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70043ae implements AnonymousClass23a {
    public final /* synthetic */ BanAppealViewModel A00;

    public C70043ae(BanAppealViewModel banAppealViewModel) {
        this.A00 = banAppealViewModel;
    }

    @Override // X.AnonymousClass23a
    public void AQB(Integer num) {
        int intValue = num.intValue();
        if (intValue == 4 || intValue == 3) {
            this.A00.A01.A0A(num);
        }
    }

    @Override // X.AnonymousClass23a
    public void AX3(C457723b r4) {
        BanAppealViewModel banAppealViewModel = this.A00;
        C12970iu.A1Q(banAppealViewModel.A0B, banAppealViewModel.A04(r4.A00, false));
    }
}
