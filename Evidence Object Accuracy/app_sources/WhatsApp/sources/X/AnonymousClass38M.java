package X;

import java.lang.ref.WeakReference;

/* renamed from: X.38M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38M extends AbstractC16350or {
    public final C14330lG A00;
    public final C14830m7 A01;
    public final AbstractC37361mF A02;
    public final WeakReference A03;

    public AnonymousClass38M(ActivityC13810kN r2, C14330lG r3, C14830m7 r4, AbstractC37361mF r5) {
        super(r2);
        this.A01 = r4;
        this.A00 = r3;
        this.A03 = C12970iu.A10(r2);
        this.A02 = r5;
    }
}
