package X;

/* renamed from: X.4wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107144wr implements AnonymousClass5X7 {
    public int A00 = 0;
    public int A01;
    public int A02 = 0;
    public long A03;
    public long A04;
    public C100614mC A05;
    public AnonymousClass5X6 A06;
    public String A07;
    public boolean A08 = false;
    public final C95054d0 A09;
    public final C95304dT A0A;
    public final String A0B;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107144wr(String str) {
        C95054d0 r0 = new C95054d0(new byte[16], 16);
        this.A09 = r0;
        this.A0A = new C95304dT(r0.A03);
        this.A0B = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00dc, code lost:
        r11.A02 = 1;
        r1 = r11.A0A.A02;
        r1[0] = -84;
        r0 = 64;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e8, code lost:
        if (r4 == false) goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ea, code lost:
        r0 = 65;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ec, code lost:
        r1[1] = (byte) r0;
        r11.A00 = 2;
     */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r12) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107144wr.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r2, C92824Xo r3) {
        r3.A03();
        this.A07 = r3.A02();
        this.A06 = C92824Xo.A00(r2, r3);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A04 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A02 = 0;
        this.A00 = 0;
        this.A08 = false;
    }
}
