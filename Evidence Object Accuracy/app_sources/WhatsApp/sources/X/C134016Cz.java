package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.6Cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134016Cz implements AbstractC136516Mv {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public C134016Cz(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AbstractC136516Mv
    public ActivityC000800j AAa() {
        return this.A00;
    }

    @Override // X.AbstractC136516Mv
    public String AFG() {
        Object obj;
        AnonymousClass1ZR r0 = ((AbstractActivityC121665jA) this.A00).A08;
        if (r0 == null) {
            obj = null;
        } else {
            obj = r0.A00;
        }
        return (String) obj;
    }

    @Override // X.AbstractC136516Mv
    public boolean AJr() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        return ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0k != null || ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0i == null;
    }

    @Override // X.AbstractC136516Mv
    public boolean AK3() {
        return this.A00.A3Y();
    }
}
