package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100144lR implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C32631cT(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C32631cT[i];
    }
}
