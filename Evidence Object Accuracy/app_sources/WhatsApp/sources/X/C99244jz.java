package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.wearable.internal.DataItemAssetParcelable;

/* renamed from: X.4jz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99244jz implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 2) {
                str2 = C95664e9.A09(parcel, str2, c, 3, readInt);
            } else {
                str = C95664e9.A08(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new DataItemAssetParcelable(str, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new DataItemAssetParcelable[i];
    }
}
