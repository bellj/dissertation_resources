package X;

import androidx.fragment.app.DialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.3RL  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3RL implements AnonymousClass02B {
    public final /* synthetic */ DialogFragment A00;
    public final /* synthetic */ AnonymousClass3FP A01;

    public /* synthetic */ AnonymousClass3RL(DialogFragment dialogFragment, AnonymousClass3FP r2) {
        this.A01 = r2;
        this.A00 = dialogFragment;
    }

    @Override // X.AnonymousClass02B
    public final void ANq(Object obj) {
        Runnable runnable;
        AnonymousClass3FP r3 = this.A01;
        DialogFragment dialogFragment = this.A00;
        int A05 = C12960it.A05(obj);
        if (A05 == 1) {
            Log.i("Start pay flow event received");
            dialogFragment.A1B();
            runnable = r3.A06;
        } else if (A05 != 2) {
            if (A05 == 3) {
                Log.i("Dismiss event received");
            } else if (A05 != 4) {
                Log.i("None event received");
                return;
            }
            dialogFragment.A1B();
            return;
        } else {
            Log.i("Invite sent event received");
            dialogFragment.A1B();
            runnable = r3.A05;
        }
        if (runnable != null) {
            runnable.run();
        }
    }
}
