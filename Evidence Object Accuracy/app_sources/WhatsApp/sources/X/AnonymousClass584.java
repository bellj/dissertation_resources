package X;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/* renamed from: X.584  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass584 implements AbstractC115365Rg {
    public byte A00;
    public int A01 = 0;
    public int A02;
    public int A03 = 0;
    public long A04 = 0;
    public long A05 = 0;
    public long A06 = 0;
    public AnonymousClass5UR A07;
    public AnonymousClass49F A08;
    public C63953Dp A09;
    public AbstractC92904Xx A0A;
    public AnonymousClass4WK A0B;
    public AnonymousClass1NQ A0C;
    public C92694Xb A0D;
    public AnonymousClass14H A0E;
    public AnonymousClass14N A0F;
    public C91104Ql A0G;
    public AnonymousClass14E A0H;
    public AnonymousClass14L A0I;
    public C90524Of A0J;
    public AnonymousClass4LX A0K;
    public AnonymousClass14K A0L;
    public InputStream A0M;
    public OutputStream A0N;
    public String A0O;
    public String A0P;
    public String A0Q;
    public List A0R;
    public List A0S;
    public List A0T;
    public Map A0U;
    public short A0V = 0;
    public short A0W = 0;
    public boolean A0X = false;
    public boolean A0Y = false;
    public boolean A0Z = false;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c = false;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f = false;
    public boolean A0g;
    public byte[] A0h;
    public byte[] A0i;
    public byte[] A0j;
    public byte[] A0k;
    public byte[] A0l;
    public byte[] A0m;
    public byte[] A0n;

    public static byte[] A00(C63953Dp r2, AnonymousClass584 r3, String str, byte[] bArr, byte[] bArr2) {
        return r2.A01(bArr2, AnonymousClass3JS.A08(str, bArr, r3.A02), r3.A02);
    }
}
