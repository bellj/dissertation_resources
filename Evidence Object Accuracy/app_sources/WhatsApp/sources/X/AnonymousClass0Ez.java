package X;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Ez  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Ez extends AbstractC06220Sq {
    public AnonymousClass0Ez(AnonymousClass02H r1) {
        super(r1);
    }

    @Override // X.AbstractC06220Sq
    public int A01() {
        return this.A02.A03;
    }

    @Override // X.AbstractC06220Sq
    public int A02() {
        AnonymousClass02H r0 = this.A02;
        return r0.A03 - r0.A0A();
    }

    @Override // X.AbstractC06220Sq
    public int A03() {
        return this.A02.A0A();
    }

    @Override // X.AbstractC06220Sq
    public int A04() {
        return this.A02.A04;
    }

    @Override // X.AbstractC06220Sq
    public int A05() {
        return this.A02.A01;
    }

    @Override // X.AbstractC06220Sq
    public int A06() {
        return this.A02.A09();
    }

    @Override // X.AbstractC06220Sq
    public int A07() {
        AnonymousClass02H r2 = this.A02;
        return (r2.A03 - r2.A09()) - r2.A0A();
    }

    @Override // X.AbstractC06220Sq
    public int A08(View view) {
        return view.getRight() + ((AnonymousClass0B6) view.getLayoutParams()).A03.right + ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A09(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        Rect rect = ((AnonymousClass0B6) view.getLayoutParams()).A03;
        return view.getMeasuredWidth() + rect.left + rect.right + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0A(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        Rect rect = ((AnonymousClass0B6) view.getLayoutParams()).A03;
        return view.getMeasuredHeight() + rect.top + rect.bottom + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0B(View view) {
        return (view.getLeft() - ((AnonymousClass0B6) view.getLayoutParams()).A03.left) - ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0C(View view) {
        AnonymousClass02H r1 = this.A02;
        Rect rect = this.A01;
        r1.A0H(rect, view);
        return rect.right;
    }

    @Override // X.AbstractC06220Sq
    public int A0D(View view) {
        AnonymousClass02H r1 = this.A02;
        Rect rect = this.A01;
        r1.A0H(rect, view);
        return rect.left;
    }

    @Override // X.AbstractC06220Sq
    public void A0E(int i) {
        this.A02.A0l(i);
    }
}
