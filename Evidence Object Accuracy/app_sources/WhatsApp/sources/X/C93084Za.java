package X;

import android.graphics.Color;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.4Za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93084Za {
    public static final Map A00;
    public static final Pattern A01 = Pattern.compile("^rgba\\((\\d{1,3}),(\\d{1,3}),(\\d{1,3}),(\\d*\\.?\\d*?)\\)$");
    public static final Pattern A02 = Pattern.compile("^rgba\\((\\d{1,3}),(\\d{1,3}),(\\d{1,3}),(\\d{1,3})\\)$");
    public static final Pattern A03 = Pattern.compile("^rgb\\((\\d{1,3}),(\\d{1,3}),(\\d{1,3})\\)$");

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put("aliceblue", -984833);
        A11.put("antiquewhite", -332841);
        A11.put("aqua", -16711681);
        A11.put("aquamarine", -8388652);
        A11.put("azure", -983041);
        A11.put("beige", -657956);
        A11.put("bisque", -6972);
        A11.put("black", -16777216);
        A11.put("blanchedalmond", -5171);
        A11.put("blue", -16776961);
        A11.put("blueviolet", -7722014);
        A11.put("brown", -5952982);
        A11.put("burlywood", -2180985);
        A11.put("cadetblue", -10510688);
        A11.put("chartreuse", -8388864);
        A11.put("chocolate", -2987746);
        A11.put("coral", -32944);
        A11.put("cornflowerblue", -10185235);
        A11.put("cornsilk", -1828);
        A11.put("crimson", -2354116);
        A11.put("cyan", -16711681);
        A11.put("darkblue", -16777077);
        A11.put("darkcyan", -16741493);
        A11.put("darkgoldenrod", -4684277);
        A11.put("darkgray", -5658199);
        A11.put("darkgreen", -16751616);
        A11.put("darkgrey", -5658199);
        A11.put("darkkhaki", -4343957);
        A11.put("darkmagenta", -7667573);
        A11.put("darkolivegreen", -11179217);
        A11.put("darkorange", -29696);
        A11.put("darkorchid", -6737204);
        A11.put("darkred", -7667712);
        A11.put("darksalmon", -1468806);
        A11.put("darkseagreen", -7357297);
        A11.put("darkslateblue", -12042869);
        A11.put("darkslategray", -13676721);
        A11.put("darkslategrey", -13676721);
        A11.put("darkturquoise", -16724271);
        A11.put("darkviolet", -7077677);
        A11.put("deeppink", -60269);
        A11.put("deepskyblue", -16728065);
        A11.put("dimgray", -9868951);
        A11.put("dimgrey", -9868951);
        A11.put("dodgerblue", -14774017);
        A11.put("firebrick", -5103070);
        A11.put("floralwhite", -1296);
        A11.put("forestgreen", -14513374);
        A11.put("fuchsia", -65281);
        A11.put("gainsboro", -2302756);
        A11.put("ghostwhite", -460545);
        A11.put("gold", -10496);
        A11.put("goldenrod", -2448096);
        A11.put("gray", -8355712);
        A11.put("green", -16744448);
        A11.put("greenyellow", -5374161);
        A11.put("grey", -8355712);
        A11.put("honeydew", -983056);
        A11.put("hotpink", -38476);
        A11.put("indianred", -3318692);
        A11.put("indigo", -11861886);
        A11.put("ivory", -16);
        A11.put("khaki", -989556);
        A11.put("lavender", -1644806);
        A11.put("lavenderblush", -3851);
        A11.put("lawngreen", -8586240);
        A11.put("lemonchiffon", -1331);
        A11.put("lightblue", -5383962);
        A11.put("lightcoral", -1015680);
        A11.put("lightcyan", -2031617);
        A11.put("lightgoldenrodyellow", -329006);
        A11.put("lightgray", -2894893);
        A11.put("lightgreen", -7278960);
        A11.put("lightgrey", -2894893);
        A11.put("lightpink", -18751);
        A11.put("lightsalmon", -24454);
        A11.put("lightseagreen", -14634326);
        A11.put("lightskyblue", -7876870);
        A11.put("lightslategray", -8943463);
        A11.put("lightslategrey", -8943463);
        A11.put("lightsteelblue", -5192482);
        A11.put("lightyellow", -32);
        A11.put("lime", -16711936);
        A11.put("limegreen", -13447886);
        A11.put("linen", -331546);
        A11.put("magenta", -65281);
        A11.put("maroon", -8388608);
        A11.put("mediumaquamarine", -10039894);
        A11.put("mediumblue", -16777011);
        A11.put("mediumorchid", -4565549);
        A11.put("mediumpurple", -7114533);
        A11.put("mediumseagreen", -12799119);
        A11.put("mediumslateblue", -8689426);
        A11.put("mediumspringgreen", -16713062);
        A11.put("mediumturquoise", -12004916);
        A11.put("mediumvioletred", -3730043);
        A11.put("midnightblue", -15132304);
        A11.put("mintcream", -655366);
        A11.put("mistyrose", -6943);
        A11.put("moccasin", -6987);
        A11.put("navajowhite", -8531);
        A11.put("navy", -16777088);
        A11.put("oldlace", -133658);
        A11.put("olive", -8355840);
        A11.put("olivedrab", -9728477);
        A11.put("orange", -23296);
        A11.put("orangered", -47872);
        A11.put("orchid", -2461482);
        A11.put("palegoldenrod", -1120086);
        A11.put("palegreen", -6751336);
        A11.put("paleturquoise", -5247250);
        A11.put("palevioletred", -2396013);
        A11.put("papayawhip", -4139);
        A11.put("peachpuff", -9543);
        A11.put("peru", -3308225);
        A11.put("pink", -16181);
        A11.put("plum", -2252579);
        A11.put("powderblue", -5185306);
        A11.put("purple", -8388480);
        A11.put("rebeccapurple", -10079335);
        A11.put("red", -65536);
        A11.put("rosybrown", -4419697);
        A11.put("royalblue", -12490271);
        A11.put("saddlebrown", -7650029);
        A11.put("salmon", -360334);
        A11.put("sandybrown", -744352);
        A11.put("seagreen", -13726889);
        A11.put("seashell", -2578);
        A11.put("sienna", -6270419);
        A11.put("silver", -4144960);
        A11.put("skyblue", -7876885);
        A11.put("slateblue", -9807155);
        A11.put("slategray", -9404272);
        A11.put("slategrey", -9404272);
        A11.put("snow", -1286);
        A11.put("springgreen", -16711809);
        A11.put("steelblue", -12156236);
        A11.put("tan", -2968436);
        A11.put("teal", -16744320);
        A11.put("thistle", -2572328);
        A11.put("tomato", -40121);
        A11.put("transparent", C12980iv.A0i());
        A11.put("turquoise", -12525360);
        A11.put("violet", -1146130);
        A11.put("wheat", -663885);
        A11.put("white", -1);
        A11.put("whitesmoke", -657931);
        A11.put("yellow", -256);
        A11.put("yellowgreen", -6632142);
    }

    public static int A00(String str, boolean z) {
        Pattern pattern;
        int parseInt;
        C95314dV.A03(!TextUtils.isEmpty(str));
        String replace = str.replace(" ", "");
        if (replace.charAt(0) == '#') {
            int parseLong = (int) Long.parseLong(replace.substring(1), 16);
            int length = replace.length();
            int i = -16777216;
            if (length != 7) {
                if (length == 9) {
                    i = (parseLong & 255) << 24;
                    parseLong >>>= 8;
                } else {
                    throw C72453ed.A0h();
                }
            }
            return i | parseLong;
        }
        if (replace.startsWith("rgba")) {
            if (z) {
                pattern = A01;
            } else {
                pattern = A02;
            }
            Matcher matcher = pattern.matcher(replace);
            if (matcher.matches()) {
                if (z) {
                    parseInt = (int) (Float.parseFloat(matcher.group(4)) * 255.0f);
                } else {
                    parseInt = Integer.parseInt(matcher.group(4), 10);
                }
                return Color.argb(parseInt, Integer.parseInt(matcher.group(1), 10), Integer.parseInt(matcher.group(2), 10), Integer.parseInt(matcher.group(3), 10));
            }
        } else if (replace.startsWith("rgb")) {
            Matcher matcher2 = A03.matcher(replace);
            if (matcher2.matches()) {
                return Color.rgb(Integer.parseInt(matcher2.group(1), 10), Integer.parseInt(matcher2.group(2), 10), Integer.parseInt(matcher2.group(3), 10));
            }
        } else {
            Number number = (Number) A00.get(replace.toLowerCase(Locale.US));
            if (number != null) {
                return number.intValue();
            }
        }
        throw C72453ed.A0h();
    }
}
