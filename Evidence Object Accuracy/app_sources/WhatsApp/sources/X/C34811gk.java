package X;

/* renamed from: X.1gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34811gk extends AbstractC30271Wt {
    public boolean A00;

    public C34811gk(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 47, j);
    }

    public C34811gk(AnonymousClass1IS r2, long j, boolean z) {
        super(r2, (byte) 47, j);
        this.A00 = z;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r7) {
        AnonymousClass1G3 r5 = r7.A03;
        C57692nT r0 = ((C27081Fy) r5.A00).A0W;
        if (r0 == null) {
            r0 = C57692nT.A0D;
        }
        C56882m6 r4 = (C56882m6) r0.A0T();
        C56982mH r02 = ((C57692nT) r4.A00).A09;
        if (r02 == null) {
            r02 = C56982mH.A02;
        }
        AnonymousClass1G4 A0T = r02.A0T();
        boolean z = this.A00;
        A0T.A03();
        C56982mH r1 = (C56982mH) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = z;
        r4.A05(EnumC87324Bb.A07);
        r4.A03();
        C57692nT r12 = (C57692nT) r4.A00;
        r12.A09 = (C56982mH) A0T.A02();
        r12.A00 |= 128;
        r5.A08(r4);
    }
}
