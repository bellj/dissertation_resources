package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56422kr extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98764jD();
    public final int A00;
    public final C56412kq A01;
    public final boolean A02;
    public final boolean A03;
    public final int[] A04;
    public final int[] A05;

    public C56422kr(C56412kq r1, int[] iArr, int[] iArr2, int i, boolean z, boolean z2) {
        this.A01 = r1;
        this.A02 = z;
        this.A03 = z2;
        this.A04 = iArr;
        this.A00 = i;
        this.A05 = iArr2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0B(parcel, this.A01, 1, i, false);
        C95654e8.A09(parcel, 2, this.A02);
        C95654e8.A09(parcel, 3, this.A03);
        C95654e8.A0H(parcel, this.A04, 4);
        C95654e8.A07(parcel, 5, this.A00);
        C95654e8.A0H(parcel, this.A05, 6);
        C95654e8.A06(parcel, A01);
    }
}
