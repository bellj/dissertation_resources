package X;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Enumeration;

/* renamed from: X.4Ys  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93034Ys {
    public static String A00(Object obj) {
        AnonymousClass1TL Aer;
        StringBuffer stringBuffer = new StringBuffer();
        if (obj instanceof AnonymousClass1TL) {
            Aer = (AnonymousClass1TL) obj;
        } else if (obj instanceof AnonymousClass1TN) {
            Aer = ((AnonymousClass1TN) obj).Aer();
        } else {
            return C12960it.A0d(obj.toString(), C12960it.A0k("unknown object type "));
        }
        A01("", stringBuffer, Aer);
        return stringBuffer.toString();
    }

    public static void A01(String str, StringBuffer stringBuffer, AnonymousClass1TL r16) {
        StringBuilder A0j;
        byte[] bArr;
        String str2;
        String obj;
        String A02;
        String A0B;
        byte[] bArr2;
        StringBuilder A0j2;
        int i;
        String str3;
        String str4;
        String str5 = AnonymousClass1T7.A00;
        if (r16 instanceof AbstractC114775Na) {
            Enumeration A0C = ((AbstractC114775Na) r16).A0C();
            String A0d = C12960it.A0d("    ", C12960it.A0j(str));
            stringBuffer.append(str);
            stringBuffer.append(r16 instanceof AnonymousClass5NX ? "BER Sequence" : r16 instanceof AnonymousClass5NZ ? "DER Sequence" : "Sequence");
            while (true) {
                stringBuffer.append(str5);
                while (A0C.hasMoreElements()) {
                    Object nextElement = A0C.nextElement();
                    if (nextElement == null || nextElement.equals(AnonymousClass5ME.A00)) {
                        stringBuffer.append(A0d);
                        stringBuffer.append("NULL");
                    } else {
                        A01(A0d, stringBuffer, nextElement instanceof AnonymousClass1TL ? (AnonymousClass1TL) nextElement : ((AnonymousClass1TN) nextElement).Aer());
                    }
                }
                return;
            }
        } else if (r16 instanceof AnonymousClass5NU) {
            String A0d2 = C12960it.A0d("    ", C12960it.A0j(str));
            stringBuffer.append(str);
            if (r16 instanceof C114815Ne) {
                str4 = "BER Tagged [";
            } else {
                str4 = "Tagged [";
            }
            stringBuffer.append(str4);
            AnonymousClass5NU r4 = (AnonymousClass5NU) r16;
            stringBuffer.append(Integer.toString(r4.A00));
            stringBuffer.append(']');
            if (!r4.A02) {
                stringBuffer.append(" IMPLICIT ");
            }
            stringBuffer.append(str5);
            A01(A0d2, stringBuffer, AnonymousClass5NU.A00(r4));
        } else if (r16 instanceof AnonymousClass5NV) {
            AnonymousClass5D0 r6 = new AnonymousClass5D0((AnonymousClass5NV) r16);
            String A0d3 = C12960it.A0d("    ", C12960it.A0j(str));
            stringBuffer.append(str);
            stringBuffer.append(r16 instanceof C114795Nc ? "BER Set" : r16 instanceof C114785Nb ? "DER Set" : "Set");
            while (true) {
                stringBuffer.append(str5);
                while (r6.hasMoreElements()) {
                    Object nextElement2 = r6.nextElement();
                    if (nextElement2 == null) {
                        break;
                    }
                    A01(A0d3, stringBuffer, nextElement2 instanceof AnonymousClass1TL ? (AnonymousClass1TL) nextElement2 : ((AnonymousClass1TN) nextElement2).Aer());
                }
                return;
                stringBuffer.append(A0d3);
                stringBuffer.append("NULL");
            }
        } else {
            if (r16 instanceof AnonymousClass5NH) {
                AnonymousClass5NH r3 = (AnonymousClass5NH) r16;
                boolean z = r16 instanceof AnonymousClass5N6;
                A0j2 = C12960it.A0h();
                if (z) {
                    A0j2.append(str);
                    str3 = "BER Constructed Octet String[";
                } else {
                    A0j2.append(str);
                    str3 = "DER Octet String[";
                }
                A0j2.append(str3);
                i = r3.A00.length;
            } else {
                if (r16 instanceof AnonymousClass1TK) {
                    A0j = C12960it.A0j(str);
                    A0j.append("ObjectIdentifier(");
                    A02 = ((AnonymousClass1TK) r16).A01;
                } else {
                    if (r16 instanceof AnonymousClass5NF) {
                        A0j = C12960it.A0j(str);
                        A0j.append("Boolean(");
                        A0j.append(C12960it.A1S(((AnonymousClass5NF) r16).A00));
                    } else {
                        if (r16 instanceof AnonymousClass5NG) {
                            A0j = C12960it.A0j(str);
                            A0j.append("Integer(");
                            bArr = ((AnonymousClass5NG) r16).A01;
                        } else if (r16 instanceof AnonymousClass5MA) {
                            AnonymousClass5NS r42 = (AnonymousClass5NS) r16;
                            A0j2 = C12960it.A0j(str);
                            A0j2.append("DER Bit String[");
                            A0j2.append(r42.A0B().length);
                            A0j2.append(", ");
                            i = r42.A00;
                        } else {
                            if (r16 instanceof AnonymousClass5NT) {
                                A0j = C12960it.A0j(str);
                                A0j.append("IA5String(");
                                bArr2 = ((AnonymousClass5NT) r16).A00;
                            } else {
                                if (r16 instanceof AnonymousClass5NP) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("UTF8String(");
                                    A0B = ((AnonymousClass5NP) r16).AGy();
                                } else if (r16 instanceof AnonymousClass5NN) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("PrintableString(");
                                    bArr2 = ((AnonymousClass5NN) r16).A00;
                                } else if (r16 instanceof AnonymousClass5NQ) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("VisibleString(");
                                    bArr2 = ((AnonymousClass5NQ) r16).A00;
                                } else if (r16 instanceof AnonymousClass5NK) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("BMPString(");
                                    A0B = new String(((AnonymousClass5NK) r16).A00);
                                } else if (r16 instanceof AnonymousClass5NO) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("T61String(");
                                    bArr2 = ((AnonymousClass5NO) r16).A00;
                                } else if (r16 instanceof AnonymousClass5NI) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("GraphicString(");
                                    bArr2 = ((AnonymousClass5NI) r16).A00;
                                } else if (r16 instanceof AnonymousClass5NJ) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("VideotexString(");
                                    bArr2 = ((AnonymousClass5NJ) r16).A00;
                                } else if (r16 instanceof AnonymousClass5NA) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("UTCTime(");
                                    A0B = ((AnonymousClass5NA) r16).A0B();
                                } else if (r16 instanceof AnonymousClass5NE) {
                                    A0j = C12960it.A0j(str);
                                    A0j.append("GeneralizedTime(");
                                    A0B = ((AnonymousClass5NE) r16).A0B();
                                } else {
                                    if (r16 instanceof AnonymousClass5M7) {
                                        str2 = "BER";
                                    } else if (r16 instanceof AnonymousClass5M8) {
                                        str2 = "";
                                    } else if (r16 instanceof AnonymousClass5ND) {
                                        A0j = C12960it.A0j(str);
                                        A0j.append("DER Enumerated(");
                                        bArr = ((AnonymousClass5ND) r16).A01;
                                    } else if (r16 instanceof AnonymousClass5NC) {
                                        AnonymousClass5NC r43 = (AnonymousClass5NC) r16;
                                        StringBuilder A0j3 = C12960it.A0j(str);
                                        A0j3.append("External ");
                                        stringBuffer.append(C12960it.A0d(str5, A0j3));
                                        String A0d4 = C12960it.A0d("    ", C12960it.A0j(str));
                                        AnonymousClass1TK r2 = r43.A02;
                                        if (r2 != null) {
                                            StringBuilder A0j4 = C12960it.A0j(A0d4);
                                            A0j4.append("Direct Reference: ");
                                            A0j4.append(r2.A01);
                                            stringBuffer.append(C12960it.A0d(str5, A0j4));
                                        }
                                        AnonymousClass5NG r22 = r43.A01;
                                        if (r22 != null) {
                                            StringBuilder A0j5 = C12960it.A0j(A0d4);
                                            A0j5.append("Indirect Reference: ");
                                            C12970iu.A1V(r22, A0j5);
                                            stringBuffer.append(C12960it.A0d(str5, A0j5));
                                        }
                                        AnonymousClass1TL r0 = r43.A03;
                                        if (r0 != null) {
                                            A01(A0d4, stringBuffer, r0);
                                        }
                                        StringBuilder A0j6 = C12960it.A0j(A0d4);
                                        A0j6.append("Encoding: ");
                                        A0j6.append(r43.A00);
                                        stringBuffer.append(C12960it.A0d(str5, A0j6));
                                        A01(A0d4, stringBuffer, r43.A04);
                                        return;
                                    } else {
                                        A0j = C12960it.A0j(str);
                                        C12970iu.A1V(r16, A0j);
                                        obj = C12960it.A0d(str5, A0j);
                                        stringBuffer.append(obj);
                                        return;
                                    }
                                    AnonymousClass5NB A00 = AnonymousClass5NB.A00(r16);
                                    StringBuffer stringBuffer2 = new StringBuffer();
                                    if (A00.A01) {
                                        try {
                                            byte[] A01 = A00.A01();
                                            int i2 = 1;
                                            if ((A01[0] & 31) == 31) {
                                                i2 = 2;
                                                int i3 = A01[1] & 255;
                                                if ((i3 & 127) == 0) {
                                                    throw C12990iw.A0i("corrupted stream - invalid high tag number found");
                                                }
                                                while ((i3 & 128) != 0) {
                                                    i2++;
                                                    i3 = A01[i2] & 255;
                                                }
                                            }
                                            int length = (A01.length - i2) + 1;
                                            byte[] bArr3 = new byte[length];
                                            System.arraycopy(A01, i2, bArr3, 1, length - 1);
                                            byte b = (byte) 16;
                                            bArr3[0] = b;
                                            if ((A01[0] & 32) != 0) {
                                                bArr3[0] = (byte) (b | 32);
                                            }
                                            AbstractC114775Na A04 = AbstractC114775Na.A04((Object) AnonymousClass1TL.A03(bArr3));
                                            StringBuilder A0j7 = C12960it.A0j(str);
                                            A0j7.append(str2);
                                            A0j7.append(" ApplicationSpecific[");
                                            A0j7.append(A00.A00);
                                            A0j7.append("]");
                                            stringBuffer2.append(C12960it.A0d(str5, A0j7));
                                            Enumeration A0C2 = A04.A0C();
                                            while (A0C2.hasMoreElements()) {
                                                A01(C12960it.A0d("    ", C12960it.A0j(str)), stringBuffer2, (AnonymousClass1TL) A0C2.nextElement());
                                            }
                                        } catch (IOException e) {
                                            stringBuffer2.append(e);
                                        }
                                        obj = stringBuffer2.toString();
                                        stringBuffer.append(obj);
                                        return;
                                    }
                                    A0j = C12960it.A0j(str);
                                    A0j.append(str2);
                                    A0j.append(" ApplicationSpecific[");
                                    A0j.append(A00.A00);
                                    A0j.append("] (");
                                    byte[] A022 = AnonymousClass1TT.A02(A00.A02);
                                    A02 = AnonymousClass1T7.A02(C95374db.A02(A022, 0, A022.length));
                                }
                                A0j.append(A0B);
                                A0j.append(") ");
                                obj = C12960it.A0d(str5, A0j);
                                stringBuffer.append(obj);
                                return;
                            }
                            A0B = AnonymousClass1T7.A02(bArr2);
                            A0j.append(A0B);
                            A0j.append(") ");
                            obj = C12960it.A0d(str5, A0j);
                            stringBuffer.append(obj);
                            return;
                        }
                        A0j.append(new BigInteger(bArr));
                    }
                    A0j.append(")");
                    obj = C12960it.A0d(str5, A0j);
                    stringBuffer.append(obj);
                    return;
                }
                A0j.append(A02);
                A0j.append(")");
                obj = C12960it.A0d(str5, A0j);
                stringBuffer.append(obj);
                return;
            }
            A0j2.append(i);
            stringBuffer.append(C12960it.A0d("] ", A0j2));
            stringBuffer.append(str5);
        }
    }
}
