package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0xJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21370xJ {
    public final C14900mE A00;
    public final C21290xB A01;
    public final C22330yu A02;
    public final C15550nR A03;
    public final C243615f A04;
    public final C19990v2 A05;
    public final C21320xE A06;
    public final C15680nj A07;
    public final C238213d A08;
    public final C22170ye A09;

    public C21370xJ(C14900mE r1, C21290xB r2, C22330yu r3, C15550nR r4, C243615f r5, C19990v2 r6, C21320xE r7, C15680nj r8, C238213d r9, C22170ye r10) {
        this.A00 = r1;
        this.A05 = r6;
        this.A09 = r10;
        this.A01 = r2;
        this.A03 = r4;
        this.A08 = r9;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A06 = r7;
    }

    public void A00() {
        this.A06.A05();
        this.A01.A01();
    }

    public void A01(AbstractC14640lm r5) {
        C19990v2 r1 = this.A05;
        if (r1.A0D(r5)) {
            C15680nj r3 = this.A07;
            long A05 = r1.A05(r5);
            r3.A00.A0B();
            r3.A0E(r5, r5, A05);
            this.A06.A05();
            UserJid of = UserJid.of(r5);
            if (of != null) {
                C243615f r12 = this.A04;
                C15370n3 A0B = this.A03.A0B(of);
                for (AnonymousClass1GQ r0 : r12.A01()) {
                    r0.A00.A08(A0B);
                }
            }
        }
    }

    public void A02(AbstractC14640lm r3, Collection collection) {
        this.A01.A01();
        this.A06.A07(r3);
        for (AnonymousClass2Dn r0 : this.A02.A01()) {
            r0.A01(r3);
        }
        if (collection != null && this.A08.A00(r3)) {
            this.A09.A0A(collection);
        }
    }

    public void A03(AbstractC14640lm r5, boolean z) {
        boolean z2;
        C19990v2 r1 = this.A05;
        boolean A0D = r1.A0D(r5);
        C15680nj r3 = this.A07;
        if (A0D) {
            long A05 = r1.A05(r5);
            r3.A00.A0B();
            z2 = r3.A0E(r5, r5, A05);
        } else {
            r3.A0C(r5);
            this.A01.A01();
            z2 = true;
        }
        C21320xE r0 = this.A06;
        if (z2) {
            r0.A05();
        } else {
            r0.A07(r5);
        }
        if (z) {
            r0.A08(r5);
        }
    }

    public void A04(List list) {
        C15680nj r7 = this.A07;
        C19990v2 r6 = r7.A00;
        r6.A0B();
        ArrayList arrayList = r7.A01;
        synchronized (arrayList) {
            arrayList.clear();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC14640lm r3 = (AbstractC14640lm) it.next();
                if (r6.A0D(r3)) {
                    AnonymousClass1W2 r2 = new AnonymousClass1W2();
                    r2.A01 = r3;
                    r2.A00 = r6.A05(r3);
                    if (r6.A03(GroupJid.of(r3)) != 1) {
                        arrayList.add(r2);
                    }
                }
            }
            Collections.sort(arrayList, r7.A02);
        }
        this.A00.A0I(new RunnableBRunnable0Shape4S0100000_I0_4(this.A06, 41));
    }
}
