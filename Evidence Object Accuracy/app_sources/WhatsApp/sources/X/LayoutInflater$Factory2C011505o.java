package X;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import java.lang.ref.WeakReference;

/* renamed from: X.05o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class LayoutInflater$Factory2C011505o extends AnonymousClass025 implements AbstractC011605p, LayoutInflater.Factory2 {
    public static final AnonymousClass00O A0o = new AnonymousClass00O();
    public static final boolean A0p;
    public static final boolean A0q;
    public static final boolean A0r = (!"robolectric".equals(Build.FINGERPRINT));
    public static final int[] A0s = {16842836};
    public int A00;
    public int A01 = -100;
    public int A02;
    public Rect A03;
    public Rect A04;
    public MenuInflater A05;
    public View A06;
    public ViewGroup A07;
    public Window A08;
    public PopupWindow A09;
    public TextView A0A;
    public AbstractC005102i A0B;
    public AnonymousClass0XI A0C;
    public C013206f A0D;
    public AnonymousClass0Q2 A0E;
    public AnonymousClass0Q2 A0F;
    public AnonymousClass08Y A0G;
    public AnonymousClass0XJ A0H;
    public AnonymousClass07P A0I;
    public AnonymousClass07Q A0J;
    public AbstractC009504t A0K;
    public ActionBarContextView A0L;
    public AbstractC12610iC A0M;
    public AnonymousClass0QQ A0N = null;
    public CharSequence A0O;
    public Runnable A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y = true;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public AnonymousClass08Y[] A0j;
    public final Context A0k;
    public final AbstractC002200x A0l;
    public final Object A0m;
    public final Runnable A0n = new RunnableC011805r(this);

    static {
        int i = Build.VERSION.SDK_INT;
        boolean z = false;
        boolean z2 = false;
        if (i < 21) {
            z2 = true;
        }
        A0p = z2;
        if (i >= 17) {
            z = true;
        }
        A0q = z;
        if (z2) {
            Thread.setDefaultUncaughtExceptionHandler(new C011705q(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }

    public LayoutInflater$Factory2C011505o(Context context, Window window, AbstractC002200x r6, Object obj) {
        this.A0k = context;
        this.A0l = r6;
        this.A0m = obj;
        if (obj instanceof Dialog) {
            while (true) {
                if (context != null) {
                    if (!(context instanceof ActivityC000800j)) {
                        if (!(context instanceof ContextWrapper)) {
                            break;
                        }
                        context = ((ContextWrapper) context).getBaseContext();
                    } else {
                        ActivityC000800j r4 = (ActivityC000800j) context;
                        if (r4 != null) {
                            this.A01 = ((LayoutInflater$Factory2C011505o) r4.A1V()).A01;
                        }
                    }
                } else {
                    break;
                }
            }
        }
        if (this.A01 == -100) {
            AnonymousClass00O r2 = A0o;
            String name = obj.getClass().getName();
            Number number = (Number) r2.get(name);
            if (number != null) {
                this.A01 = number.intValue();
                r2.remove(name);
            }
        }
        if (window != null) {
            A0S(window);
        }
        C011905s.A02();
    }

    public static final Configuration A02(Context context, Configuration configuration, int i) {
        int i2;
        if (i != 1) {
            i2 = i != 2 ? context.getApplicationContext().getResources().getConfiguration().uiMode & 48 : 32;
        } else {
            i2 = 16;
        }
        Configuration configuration2 = new Configuration();
        configuration2.fontScale = 0.0f;
        if (configuration != null) {
            configuration2.setTo(configuration);
        }
        configuration2.uiMode = i2 | (configuration2.uiMode & -49);
        return configuration2;
    }

    @Override // X.AnonymousClass025
    public Context A03(Context context) {
        this.A0S = true;
        int i = this.A01;
        if (i == -100) {
            i = AnonymousClass025.A00;
        }
        int A0I = A0I(context, i);
        Configuration configuration = null;
        if (A0q && (context instanceof ContextThemeWrapper)) {
            try {
                AnonymousClass0KB.A00(A02(context, null, A0I), (ContextThemeWrapper) context);
                return context;
            } catch (IllegalStateException unused) {
            }
        }
        if (context instanceof AnonymousClass062) {
            try {
                ((AnonymousClass062) context).A01(A02(context, null, A0I));
                return context;
            } catch (IllegalStateException unused2) {
            }
        }
        if (!A0r) {
            return context;
        }
        if (Build.VERSION.SDK_INT >= 17) {
            Configuration configuration2 = new Configuration();
            configuration2.uiMode = -1;
            configuration2.fontScale = 0.0f;
            Configuration configuration3 = AnonymousClass063.A00(context, configuration2).getResources().getConfiguration();
            Configuration configuration4 = context.getResources().getConfiguration();
            configuration3.uiMode = configuration4.uiMode;
            if (!configuration3.equals(configuration4)) {
                configuration = new Configuration();
                configuration.fontScale = 0.0f;
                if (configuration3.diff(configuration4) != 0) {
                    float f = configuration3.fontScale;
                    float f2 = configuration4.fontScale;
                    if (f != f2) {
                        configuration.fontScale = f2;
                    }
                    int i2 = configuration3.mcc;
                    int i3 = configuration4.mcc;
                    if (i2 != i3) {
                        configuration.mcc = i3;
                    }
                    int i4 = configuration3.mnc;
                    int i5 = configuration4.mnc;
                    if (i4 != i5) {
                        configuration.mnc = i5;
                    }
                    int i6 = Build.VERSION.SDK_INT;
                    if (i6 >= 24) {
                        AnonymousClass0K9.A00(configuration3, configuration4, configuration);
                    } else if (!C015407h.A01(configuration3.locale, configuration4.locale)) {
                        configuration.locale = configuration4.locale;
                    }
                    int i7 = configuration3.touchscreen;
                    int i8 = configuration4.touchscreen;
                    if (i7 != i8) {
                        configuration.touchscreen = i8;
                    }
                    int i9 = configuration3.keyboard;
                    int i10 = configuration4.keyboard;
                    if (i9 != i10) {
                        configuration.keyboard = i10;
                    }
                    int i11 = configuration3.keyboardHidden;
                    int i12 = configuration4.keyboardHidden;
                    if (i11 != i12) {
                        configuration.keyboardHidden = i12;
                    }
                    int i13 = configuration3.navigation;
                    int i14 = configuration4.navigation;
                    if (i13 != i14) {
                        configuration.navigation = i14;
                    }
                    int i15 = configuration3.navigationHidden;
                    int i16 = configuration4.navigationHidden;
                    if (i15 != i16) {
                        configuration.navigationHidden = i16;
                    }
                    int i17 = configuration3.orientation;
                    int i18 = configuration4.orientation;
                    if (i17 != i18) {
                        configuration.orientation = i18;
                    }
                    int i19 = configuration3.screenLayout & 15;
                    int i20 = configuration4.screenLayout & 15;
                    if (i19 != i20) {
                        configuration.screenLayout |= i20;
                    }
                    int i21 = configuration3.screenLayout & 192;
                    int i22 = configuration4.screenLayout & 192;
                    if (i21 != i22) {
                        configuration.screenLayout |= i22;
                    }
                    int i23 = configuration3.screenLayout & 48;
                    int i24 = configuration4.screenLayout & 48;
                    if (i23 != i24) {
                        configuration.screenLayout |= i24;
                    }
                    int i25 = configuration3.screenLayout & 768;
                    int i26 = configuration4.screenLayout & 768;
                    if (i25 != i26) {
                        configuration.screenLayout |= i26;
                    }
                    if (i6 >= 26) {
                        AnonymousClass0KA.A00(configuration3, configuration4, configuration);
                    }
                    int i27 = configuration3.uiMode & 15;
                    int i28 = configuration4.uiMode & 15;
                    if (i27 != i28) {
                        configuration.uiMode |= i28;
                    }
                    int i29 = configuration3.uiMode & 48;
                    int i30 = configuration4.uiMode & 48;
                    if (i29 != i30) {
                        configuration.uiMode |= i30;
                    }
                    int i31 = configuration3.screenWidthDp;
                    int i32 = configuration4.screenWidthDp;
                    if (i31 != i32) {
                        configuration.screenWidthDp = i32;
                    }
                    int i33 = configuration3.screenHeightDp;
                    int i34 = configuration4.screenHeightDp;
                    if (i33 != i34) {
                        configuration.screenHeightDp = i34;
                    }
                    int i35 = configuration3.smallestScreenWidthDp;
                    int i36 = configuration4.smallestScreenWidthDp;
                    if (i35 != i36) {
                        configuration.smallestScreenWidthDp = i36;
                    }
                    if (i6 >= 17) {
                        AnonymousClass063.A01(configuration3, configuration4, configuration);
                    }
                }
            }
        }
        Configuration A02 = A02(context, configuration, A0I);
        AnonymousClass062 r3 = new AnonymousClass062(context, 2131952414);
        r3.A01(A02);
        try {
            if (context.getTheme() != null) {
                Resources.Theme theme = r3.getTheme();
                int i37 = Build.VERSION.SDK_INT;
                if (i37 >= 29) {
                    C04020Kb.A00(theme);
                } else if (i37 >= 23) {
                    AnonymousClass0RO.A00(theme);
                    return r3;
                }
            }
        } catch (NullPointerException unused3) {
        }
        return r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b9, code lost:
        if ("include".equals(r4.getName()) == false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c7, code lost:
        if (((org.xmlpull.v1.XmlPullParser) r19).getDepth() > 1) goto L_0x00c9;
     */
    @Override // X.AnonymousClass025
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A04(android.view.View r16, java.lang.String r17, android.content.Context r18, android.util.AttributeSet r19) {
        /*
        // Method dump skipped, instructions count: 248
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A04(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fd, code lost:
        if (r1 == false) goto L_0x00ff;
     */
    @Override // X.AnonymousClass025
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC009504t A05(X.AnonymousClass02Q r11) {
        /*
        // Method dump skipped, instructions count: 409
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A05(X.02Q):X.04t");
    }

    @Override // X.AnonymousClass025
    public void A06() {
        LayoutInflater from = LayoutInflater.from(this.A0k);
        if (from.getFactory() == null) {
            C013106e.A01(this, from);
        } else if (!(from.getFactory2() instanceof LayoutInflater$Factory2C011505o)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @Override // X.AnonymousClass025
    public void A07() {
        A0O();
        AbstractC005102i r0 = this.A0B;
        if (r0 == null || !r0.A0S()) {
            this.A00 = (1 << 0) | this.A00;
            if (!this.A0a) {
                this.A08.getDecorView().postOnAnimation(this.A0n);
                this.A0a = true;
            }
        }
    }

    @Override // X.AnonymousClass025
    public void A08() {
        Object obj = this.A0m;
        boolean z = obj instanceof Activity;
        if (z) {
            synchronized (AnonymousClass025.A02) {
                AnonymousClass025.A01(this);
            }
        }
        if (this.A0a) {
            this.A08.getDecorView().removeCallbacks(this.A0n);
        }
        this.A0g = false;
        this.A0b = true;
        if (this.A01 == -100 || !z || !((Activity) obj).isChangingConfigurations()) {
            A0o.remove(obj.getClass().getName());
        } else {
            A0o.put(obj.getClass().getName(), Integer.valueOf(this.A01));
        }
        AbstractC005102i r0 = this.A0B;
        if (r0 != null) {
            r0.A05();
        }
        AnonymousClass0Q2 r02 = this.A0F;
        if (r02 != null) {
            r02.A02();
        }
        AnonymousClass0Q2 r03 = this.A0E;
        if (r03 != null) {
            r03.A02();
        }
    }

    @Override // X.AnonymousClass025
    public void A09() {
        this.A0g = false;
        A0O();
        AbstractC005102i r0 = this.A0B;
        if (r0 != null) {
            r0.A0Q(false);
        }
    }

    @Override // X.AnonymousClass025
    public void A0A(int i) {
        A0M();
        ViewGroup viewGroup = (ViewGroup) this.A07.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.A0k).inflate(i, viewGroup);
        ((Window$CallbackC013306g) this.A0D).A00.onContentChanged();
    }

    @Override // X.AnonymousClass025
    public void A0B(int i) {
        if (this.A01 != i) {
            this.A01 = i;
            if (this.A0S) {
                A0V(true);
            }
        }
    }

    @Override // X.AnonymousClass025
    public void A0C(Configuration configuration) {
        if (this.A0Z && this.A0h) {
            A0O();
            AbstractC005102i r0 = this.A0B;
            if (r0 != null) {
                r0.A0B(configuration);
            }
        }
        C011905s A01 = C011905s.A01();
        Context context = this.A0k;
        synchronized (A01) {
            C012005t r1 = A01.A00;
            synchronized (r1) {
                AnonymousClass036 r02 = (AnonymousClass036) r1.A06.get(context);
                if (r02 != null) {
                    r02.A05();
                }
            }
        }
        A0V(false);
    }

    @Override // X.AnonymousClass025
    public void A0D(Bundle bundle) {
        this.A0S = true;
        A0V(false);
        A0N();
        Object obj = this.A0m;
        if (obj instanceof Activity) {
            String str = null;
            try {
                Activity activity = (Activity) obj;
                try {
                    str = C014806z.A00(activity.getComponentName(), activity);
                } catch (PackageManager.NameNotFoundException e) {
                    throw new IllegalArgumentException(e);
                }
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                AbstractC005102i r0 = this.A0B;
                if (r0 == null) {
                    this.A0V = true;
                } else {
                    r0.A0L(true);
                }
            }
            synchronized (AnonymousClass025.A02) {
                AnonymousClass025.A01(this);
                AnonymousClass025.A01.add(new WeakReference(this));
            }
        }
        this.A0U = true;
    }

    @Override // X.AnonymousClass025
    public void A0E(View view) {
        A0M();
        ViewGroup viewGroup = (ViewGroup) this.A07.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        ((Window$CallbackC013306g) this.A0D).A00.onContentChanged();
    }

    @Override // X.AnonymousClass025
    public void A0F(View view, ViewGroup.LayoutParams layoutParams) {
        A0M();
        ((ViewGroup) this.A07.findViewById(16908290)).addView(view, layoutParams);
        ((Window$CallbackC013306g) this.A0D).A00.onContentChanged();
    }

    @Override // X.AnonymousClass025
    public void A0G(View view, ViewGroup.LayoutParams layoutParams) {
        A0M();
        ViewGroup viewGroup = (ViewGroup) this.A07.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        ((Window$CallbackC013306g) this.A0D).A00.onContentChanged();
    }

    @Override // X.AnonymousClass025
    public final void A0H(CharSequence charSequence) {
        this.A0O = charSequence;
        AbstractC12610iC r0 = this.A0M;
        if (r0 != null) {
            r0.setWindowTitle(charSequence);
            return;
        }
        AbstractC005102i r02 = this.A0B;
        if (r02 != null) {
            r02.A0J(charSequence);
            return;
        }
        TextView textView = this.A0A;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }

    public int A0I(Context context, int i) {
        AnonymousClass0Q2 A0K;
        if (i != -100) {
            if (i != -1) {
                if (i != 0) {
                    if (!(i == 1 || i == 2)) {
                        if (i == 3) {
                            A0K = this.A0E;
                            if (A0K == null) {
                                A0K = new AnonymousClass0C5(context, this);
                                this.A0E = A0K;
                            }
                        } else {
                            throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
                        }
                    }
                } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) context.getApplicationContext().getSystemService("uimode")).getNightMode() != 0) {
                    A0K = A0K(context);
                }
                return A0K.A00();
            }
            return i;
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        if (r10 != false) goto L_0x00ba;
     */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A0J(android.graphics.Rect r13, X.C018408o r14) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0J(android.graphics.Rect, X.08o):int");
    }

    public final AnonymousClass0Q2 A0K(Context context) {
        AnonymousClass0Q2 r0 = this.A0F;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0NW r2 = AnonymousClass0NW.A03;
        if (r2 == null) {
            Context applicationContext = context.getApplicationContext();
            r2 = new AnonymousClass0NW(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
            AnonymousClass0NW.A03 = r2;
        }
        AnonymousClass0C6 r02 = new AnonymousClass0C6(this, r2);
        this.A0F = r02;
        return r02;
    }

    public AnonymousClass08Y A0L(int i) {
        AnonymousClass08Y[] r3 = this.A0j;
        if (r3 == null || r3.length <= i) {
            AnonymousClass08Y[] r2 = new AnonymousClass08Y[i + 1];
            if (r3 != null) {
                System.arraycopy(r3, 0, r2, 0, r3.length);
            }
            this.A0j = r2;
            r3 = r2;
        }
        AnonymousClass08Y r0 = r3[i];
        if (r0 != null) {
            return r0;
        }
        AnonymousClass08Y r02 = new AnonymousClass08Y(i);
        r3[i] = r02;
        return r02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006e, code lost:
        if (r5 != null) goto L_0x0070;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0M() {
        /*
        // Method dump skipped, instructions count: 720
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0M():void");
    }

    public final void A0N() {
        if (this.A08 == null) {
            Object obj = this.A0m;
            if (obj instanceof Activity) {
                A0S(((Activity) obj).getWindow());
            }
        }
        if (this.A08 == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    public final void A0O() {
        AnonymousClass0C4 r1;
        A0M();
        if (this.A0Z && this.A0B == null) {
            Object obj = this.A0m;
            if (obj instanceof Activity) {
                r1 = new AnonymousClass0C4((Activity) obj, this.A0e);
            } else if (obj instanceof Dialog) {
                r1 = new AnonymousClass0C4((Dialog) obj);
            } else {
                return;
            }
            this.A0B = r1;
            r1.A0L(this.A0V);
        }
    }

    public final void A0P() {
        if (this.A0h) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    public void A0Q(int i) {
        AnonymousClass08Y A0L = A0L(i);
        if (A0L.A0A != null) {
            Bundle bundle = new Bundle();
            A0L.A0A.A0A(bundle);
            if (bundle.size() > 0) {
                A0L.A05 = bundle;
            }
            AnonymousClass07H r0 = A0L.A0A;
            r0.A07();
            r0.clear();
        }
        A0L.A0F = true;
        A0L.A0E = true;
        if ((i == 108 || i == 0) && this.A0M != null) {
            AnonymousClass08Y A0L2 = A0L(0);
            A0L2.A0D = false;
            A0X(null, A0L2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0076, code lost:
        if (r0.getCount() > 0) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0122, code lost:
        if (r0 != null) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0133, code lost:
        if (r0.width != -1) goto L_0x00b1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0R(android.view.KeyEvent r14, X.AnonymousClass08Y r15) {
        /*
        // Method dump skipped, instructions count: 429
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0R(android.view.KeyEvent, X.08Y):void");
    }

    public final void A0S(Window window) {
        if (this.A08 == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof C013206f)) {
                C013206f r0 = new C013206f(callback, this);
                this.A0D = r0;
                window.setCallback(r0);
                Context context = this.A0k;
                C013406h r1 = new C013406h(context, context.obtainStyledAttributes((AttributeSet) null, A0s));
                Drawable A03 = r1.A03(0);
                if (A03 != null) {
                    window.setBackgroundDrawable(A03);
                }
                r1.A04();
                this.A08 = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    public void A0T(AnonymousClass08Y r5, boolean z) {
        ViewGroup viewGroup;
        AbstractC12610iC r0;
        if (!z || r5.A01 != 0 || (r0 = this.A0M) == null || !r0.AJp()) {
            WindowManager windowManager = (WindowManager) this.A0k.getSystemService("window");
            if (!(windowManager == null || !r5.A0C || (viewGroup = r5.A08) == null)) {
                windowManager.removeView(viewGroup);
                if (z) {
                    int i = r5.A01;
                    AnonymousClass07H r1 = r5.A0A;
                    if (r5.A0C && !this.A0b) {
                        ((Window$CallbackC013306g) this.A0D).A00.onPanelClosed(i, r1);
                    }
                }
            }
            r5.A0D = false;
            r5.A0B = false;
            r5.A0C = false;
            r5.A07 = null;
            r5.A0E = true;
            if (this.A0G == r5) {
                this.A0G = null;
                return;
            }
            return;
        }
        A0U(r5.A0A);
    }

    public void A0U(AnonymousClass07H r3) {
        if (!this.A0T) {
            this.A0T = true;
            ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) this.A0M;
            actionBarOverlayLayout.A02();
            actionBarOverlayLayout.A0A.A93();
            Window.Callback callback = this.A08.getCallback();
            if (callback != null && !this.A0b) {
                callback.onPanelClosed(C43951xu.A03, r3);
            }
            this.A0T = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004d, code lost:
        if (r1 == 0) goto L_0x004f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:81:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0V(boolean r12) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0V(boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0108  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0W(android.view.KeyEvent r6) {
        /*
        // Method dump skipped, instructions count: 329
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0W(android.view.KeyEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        if (r7 == 108) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004f, code lost:
        if (r13.A0F != false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0103, code lost:
        if (r6 != null) goto L_0x00d3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0X(android.view.KeyEvent r12, X.AnonymousClass08Y r13) {
        /*
        // Method dump skipped, instructions count: 350
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.A0X(android.view.KeyEvent, X.08Y):boolean");
    }

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r9) {
        Window.Callback callback = this.A08.getCallback();
        if (callback == null || this.A0b) {
            return false;
        }
        AnonymousClass07H A01 = r9.A01();
        AnonymousClass08Y[] r4 = this.A0j;
        if (r4 == null) {
            return false;
        }
        for (AnonymousClass08Y r1 : r4) {
            if (r1 != null && r1.A0A == A01) {
                return callback.onMenuItemSelected(r1.A01, menuItem);
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0048, code lost:
        if (r1.A02() != false) goto L_0x004a;
     */
    @Override // X.AbstractC011605p
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ASi(X.AnonymousClass07H r7) {
        /*
            r6 = this;
            X.0iC r0 = r6.A0M
            r2 = 1
            r5 = 0
            if (r0 == 0) goto L_0x00cf
            androidx.appcompat.widget.ActionBarOverlayLayout r0 = (androidx.appcompat.widget.ActionBarOverlayLayout) r0
            r0.A02()
            X.08e r0 = r0.A0A
            X.08d r0 = (X.C017408d) r0
            androidx.appcompat.widget.Toolbar r1 = r0.A09
            int r0 = r1.getVisibility()
            if (r0 != 0) goto L_0x00cf
            androidx.appcompat.widget.ActionMenuView r0 = r1.A0O
            if (r0 == 0) goto L_0x00cf
            boolean r0 = r0.A0B
            if (r0 == 0) goto L_0x00cf
            android.content.Context r0 = r6.A0k
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            boolean r0 = r0.hasPermanentMenuKey()
            if (r0 == 0) goto L_0x004a
            X.0iC r0 = r6.A0M
            androidx.appcompat.widget.ActionBarOverlayLayout r0 = (androidx.appcompat.widget.ActionBarOverlayLayout) r0
            r0.A02()
            X.08e r0 = r0.A0A
            X.08d r0 = (X.C017408d) r0
            androidx.appcompat.widget.Toolbar r0 = r0.A09
            androidx.appcompat.widget.ActionMenuView r0 = r0.A0O
            if (r0 == 0) goto L_0x00cf
            X.0XQ r1 = r0.A08
            if (r1 == 0) goto L_0x00cf
            X.0cw r0 = r1.A0F
            if (r0 != 0) goto L_0x004a
            boolean r0 = r1.A02()
            if (r0 == 0) goto L_0x00cf
        L_0x004a:
            android.view.Window r0 = r6.A08
            android.view.Window$Callback r4 = r0.getCallback()
            X.0iC r0 = r6.A0M
            boolean r0 = r0.AJp()
            r3 = 108(0x6c, float:1.51E-43)
            if (r0 == 0) goto L_0x0080
            X.0iC r0 = r6.A0M
            androidx.appcompat.widget.ActionBarOverlayLayout r0 = (androidx.appcompat.widget.ActionBarOverlayLayout) r0
            r0.A02()
            X.08e r0 = r0.A0A
            X.08d r0 = (X.C017408d) r0
            androidx.appcompat.widget.Toolbar r0 = r0.A09
            androidx.appcompat.widget.ActionMenuView r0 = r0.A0O
            if (r0 == 0) goto L_0x0072
            X.0XQ r0 = r0.A08
            if (r0 == 0) goto L_0x0072
            r0.A01()
        L_0x0072:
            boolean r0 = r6.A0b
            if (r0 != 0) goto L_0x007f
            X.08Y r0 = r6.A0L(r5)
            X.07H r0 = r0.A0A
            r4.onPanelClosed(r3, r0)
        L_0x007f:
            return
        L_0x0080:
            if (r4 == 0) goto L_0x007f
            boolean r0 = r6.A0b
            if (r0 != 0) goto L_0x007f
            boolean r0 = r6.A0a
            if (r0 == 0) goto L_0x009d
            int r0 = r6.A00
            r0 = r0 & r2
            if (r0 == 0) goto L_0x009d
            android.view.Window r0 = r6.A08
            android.view.View r1 = r0.getDecorView()
            java.lang.Runnable r0 = r6.A0n
            r1.removeCallbacks(r0)
            r0.run()
        L_0x009d:
            X.08Y r2 = r6.A0L(r5)
            X.07H r1 = r2.A0A
            if (r1 == 0) goto L_0x007f
            boolean r0 = r2.A0F
            if (r0 != 0) goto L_0x007f
            android.view.View r0 = r2.A06
            boolean r0 = r4.onPreparePanel(r5, r0, r1)
            if (r0 == 0) goto L_0x007f
            X.07H r0 = r2.A0A
            r4.onMenuOpened(r3, r0)
            X.0iC r0 = r6.A0M
            androidx.appcompat.widget.ActionBarOverlayLayout r0 = (androidx.appcompat.widget.ActionBarOverlayLayout) r0
            r0.A02()
            X.08e r0 = r0.A0A
            X.08d r0 = (X.C017408d) r0
            androidx.appcompat.widget.Toolbar r0 = r0.A09
            androidx.appcompat.widget.ActionMenuView r0 = r0.A0O
            if (r0 == 0) goto L_0x007f
            X.0XQ r0 = r0.A08
            if (r0 == 0) goto L_0x007f
            r0.A03()
            return
        L_0x00cf:
            X.08Y r1 = r6.A0L(r5)
            r1.A0E = r2
            r6.A0T(r1, r5)
            r0 = 0
            r6.A0R(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C011505o.ASi(X.07H):void");
    }

    @Override // android.view.LayoutInflater.Factory2
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return A04(view, str, context, attributeSet);
    }

    @Override // android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
