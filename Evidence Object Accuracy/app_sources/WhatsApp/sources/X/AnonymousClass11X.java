package X;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.11X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11X extends AbstractC14550lc {
    public final C14850m9 A00;

    public AnonymousClass11X(C14850m9 r4, AbstractC14440lR r5) {
        super(new C002601e(null, new AnonymousClass01N() { // from class: X.1wR
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                ThreadPoolExecutor A8a = AbstractC14440lR.this.A8a("MediaDownload", new PriorityBlockingQueue(), 1, 1, 10, 60);
                A8a.allowCoreThreadTimeOut(true);
                return A8a;
            }
        }));
        this.A00 = r4;
    }

    /* renamed from: A06 */
    public synchronized C28921Pn A01(C28921Pn r5, AbstractC16130oV r6) {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) super.A00.get();
        int corePoolSize = threadPoolExecutor.getCorePoolSize();
        int max = Math.max(1, Math.min(10, this.A00.A02(49)));
        if (corePoolSize != max) {
            if (max > corePoolSize) {
                threadPoolExecutor.setMaximumPoolSize(max);
                threadPoolExecutor.setCorePoolSize(max);
            } else {
                threadPoolExecutor.setCorePoolSize(max);
                threadPoolExecutor.setMaximumPoolSize(max);
            }
        }
        return (C28921Pn) super.A01(r6, r5);
    }
}
