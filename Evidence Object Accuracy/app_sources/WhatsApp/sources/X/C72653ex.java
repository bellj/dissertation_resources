package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72653ex extends AnimatorListenerAdapter {
    public final /* synthetic */ float A00;
    public final /* synthetic */ C53232dO A01;
    public final /* synthetic */ AbstractC116775Wt A02;

    public C72653ex(C53232dO r1, AbstractC116775Wt r2, float f) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = f;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A02.setBackgroundScale(this.A00);
    }
}
