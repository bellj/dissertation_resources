package X;

import com.whatsapp.avatar.home.AvatarHomeActivity;
import com.whatsapp.avatar.home.AvatarHomeViewModel;

/* renamed from: X.3db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71843db extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AvatarHomeActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71843db(AvatarHomeActivity avatarHomeActivity) {
        super(0);
        this.this$0 = avatarHomeActivity;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AnonymousClass015 A00 = C13000ix.A02(this.this$0).A00(AvatarHomeViewModel.class);
        C16700pc.A0B(A00);
        return A00;
    }
}
