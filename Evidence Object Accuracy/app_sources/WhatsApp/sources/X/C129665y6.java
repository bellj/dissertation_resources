package X;

import com.facebook.redex.IDxAListenerShape0S0300000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.5y6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129665y6 {
    public final AnonymousClass018 A00;
    public final AnonymousClass102 A01;
    public final C130155yt A02;
    public final C130025yg A03;
    public final C129675y7 A04;

    public C129665y6(AnonymousClass018 r1, AnonymousClass102 r2, C130155yt r3, C130025yg r4, C129675y7 r5) {
        this.A04 = r5;
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    public final void A00(AnonymousClass3FE r7, C129865yQ r8, C1310460z r9, String str) {
        C129675y7 r5 = this.A04;
        C1316663q r2 = r5.A01;
        AnonymousClass009.A05(r2);
        AnonymousClass61S[] r4 = new AnonymousClass61S[3];
        r4[0] = AnonymousClass61S.A00("entry_flow", r2.A03);
        r4[1] = AnonymousClass61S.A00("metadata", r2.A04);
        C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("step", str), r4, 2));
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("action", "novi-answer-kyc-step-up-challenge"), new AnonymousClass61S[1], 0);
        ArrayList arrayList = A0F.A02;
        arrayList.add(A0B);
        arrayList.add(r9);
        C130155yt.A02(new AbstractC136196Lo(r7, r8, this) { // from class: X.69y
            public final /* synthetic */ AnonymousClass3FE A00;
            public final /* synthetic */ C129865yQ A01;
            public final /* synthetic */ C129665y6 A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r92) {
                Object obj;
                C129665y6 r52 = this.A02;
                AnonymousClass3FE r42 = this.A00;
                C129865yQ r6 = this.A01;
                if (r92.A06() && (obj = r92.A02) != null) {
                    try {
                        AnonymousClass1V8 r1 = (AnonymousClass1V8) obj;
                        if (C117295Zj.A0W(r1, "decision") != null) {
                            r52.A04.A03.A00.clear();
                            C130155yt.A00(new IDxAListenerShape0S0300000_3_I1(r42, r6, r52, 6), r52.A02, AnonymousClass61S.A01("novi-get-account-status"));
                            return;
                        }
                        AnonymousClass1V8 A0E = r1.A0E("next_step");
                        if (A0E != null) {
                            C121385hl A00 = C121385hl.A00(A0E);
                            r52.A04.A03.A00.push(A00);
                            HashMap A11 = C12970iu.A11();
                            A11.put("decision", "PENDING");
                            C117305Zk.A1G(r42, "stepUpType", A00.A01, A11);
                        }
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: KycStepUpManager/answerTKycPhotoIdStepUp unable to parse kyc step up");
                    }
                } else if (r92.A01 == null) {
                    C129865yQ.A00(r6, r92);
                    C117305Zk.A1E(r42);
                }
            }
        }, this.A02, A0F, r5.A00);
    }
}
