package X;

/* renamed from: X.42D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42D extends AnonymousClass1lQ {
    public int A00;
    public int A01;

    @Override // X.AnonymousClass1lQ
    public long A01(int i) {
        return 0;
    }

    public AnonymousClass42D(int i, int i2) {
        super(null, null);
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // X.AnonymousClass1lQ
    public int A00() {
        return this.A01;
    }

    @Override // X.AnonymousClass1lQ
    public AnonymousClass49N A02() {
        return AnonymousClass49N.A01;
    }
}
