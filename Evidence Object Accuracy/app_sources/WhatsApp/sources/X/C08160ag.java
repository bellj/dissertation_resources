package X;

import android.graphics.Path;

/* renamed from: X.0ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08160ag implements AbstractC12040hH {
    public final Path.FillType A00;
    public final AnonymousClass0H5 A01;
    public final AnonymousClass0HA A02;
    public final AnonymousClass0H6 A03;
    public final AnonymousClass0H6 A04;
    public final AnonymousClass0J4 A05;
    public final String A06;
    public final boolean A07;

    public C08160ag(Path.FillType fillType, AnonymousClass0H5 r2, AnonymousClass0HA r3, AnonymousClass0H6 r4, AnonymousClass0H6 r5, AnonymousClass0J4 r6, String str, boolean z) {
        this.A05 = r6;
        this.A00 = fillType;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r4;
        this.A03 = r5;
        this.A06 = str;
        this.A07 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C08060aW(r2, this, r3);
    }
}
