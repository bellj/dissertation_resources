package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.69E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69E implements AnonymousClass18M {
    public final AnonymousClass018 A00;
    public final C14850m9 A01;
    public final C129925yW A02;
    public final C1308460e A03;
    public final C1329668y A04;

    @Override // X.AnonymousClass18M
    public String ABs(int i) {
        return null;
    }

    @Override // X.AnonymousClass18M
    public int ABt(AnonymousClass1AA r2, C30141Wg r3, int i) {
        return -1;
    }

    @Override // X.AnonymousClass18M
    public String ABu(int i) {
        return null;
    }

    @Override // X.AnonymousClass18M
    public boolean AJb(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJc(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJd(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJf(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJw(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AKB(int i) {
        return i == 11455 || i == 11502;
    }

    @Override // X.AnonymousClass18M
    public int ALP() {
        return 100000;
    }

    @Override // X.AnonymousClass18M
    public int ALQ() {
        return 10;
    }

    public AnonymousClass69E(AnonymousClass018 r1, C14850m9 r2, C129925yW r3, C1308460e r4, C1329668y r5) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
    }

    public static int A00(C64513Fv r3, int i) {
        int i2;
        int i3;
        if (i <= 0 && r3 != null) {
            synchronized (r3) {
                i2 = r3.A01;
            }
            synchronized (r3) {
                i3 = r3.A02;
            }
            int i4 = r3.A00;
            if (i2 > 0) {
                return i2;
            }
            if (i3 > 0) {
                return i3;
            }
            if (i4 > 0) {
                return i4;
            }
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01(X.ActivityC13810kN r5, X.C125855rv r6, java.lang.Runnable r7, java.lang.String r8, int r9, boolean r10) {
        /*
            r0 = 404(0x194, float:5.66E-43)
            r3 = 0
            java.lang.String r4 = " op: "
            java.lang.String r1 = "PAY: "
            r2 = 1
            if (r9 == r0) goto L_0x0075
            r0 = 440(0x1b8, float:6.17E-43)
            if (r9 == r0) goto L_0x0047
            r0 = 442(0x1ba, float:6.2E-43)
            if (r9 == r0) goto L_0x00a1
            r0 = 443(0x1bb, float:6.21E-43)
            if (r9 != r0) goto L_0x00cf
            java.lang.StringBuilder r1 = X.C12960it.A0j(r1)
            r1.append(r5)
            r1.append(r4)
            r1.append(r8)
            java.lang.String r0 = " payment unsupported for client version"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.e(r0)
            if (r7 == 0) goto L_0x0031
            r7.run()
        L_0x0031:
            java.lang.Class<com.whatsapp.payments.ui.PaymentsUpdateRequiredActivity> r0 = com.whatsapp.payments.ui.PaymentsUpdateRequiredActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r5, r0)
            r0 = 335544320(0x14000000, float:6.4623485E-27)
            r1.addFlags(r0)
        L_0x003c:
            if (r6 == 0) goto L_0x0043
            X.5jA r0 = r6.A00
            r0.A2v(r1)
        L_0x0043:
            r5.A2G(r1, r2)
            return r2
        L_0x0047:
            java.lang.StringBuilder r1 = X.C12960it.A0j(r1)
            r1.append(r5)
            r1.append(r4)
            r1.append(r8)
            java.lang.String r0 = " tos not accepted; showTosAndFinish"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.e(r0)
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiPaymentsTosActivity> r0 = com.whatsapp.payments.ui.IndiaUpiPaymentsTosActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r5, r0)
            java.lang.String r0 = "extra_show_updated_tos"
            r1.putExtra(r0, r3)
            if (r6 == 0) goto L_0x006f
            X.5jA r0 = r6.A00
            r0.A2v(r1)
        L_0x006f:
            if (r7 == 0) goto L_0x0043
            r7.run()
            goto L_0x0043
        L_0x0075:
            if (r10 == 0) goto L_0x00cf
            java.lang.StringBuilder r1 = X.C12960it.A0j(r1)
            r1.append(r5)
            r1.append(r4)
            r1.append(r8)
            java.lang.String r0 = " payment account error: "
            r1.append(r0)
            r1.append(r9)
            java.lang.String r0 = "; restartPaymentsAccountSetupAndFinish"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.e(r0)
            if (r7 == 0) goto L_0x009a
            r7.run()
        L_0x009a:
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity> r0 = com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r5, r0)
            goto L_0x003c
        L_0x00a1:
            java.lang.StringBuilder r1 = X.C12960it.A0j(r1)
            r1.append(r5)
            r1.append(r4)
            r1.append(r8)
            java.lang.String r0 = " tos v2 not accepted; showTosAndFinish"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.e(r0)
            java.lang.Class<com.whatsapp.payments.ui.IndiaUpiPaymentsTosActivity> r0 = com.whatsapp.payments.ui.IndiaUpiPaymentsTosActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r5, r0)
            java.lang.String r0 = "extra_show_updated_tos"
            r1.putExtra(r0, r2)
            if (r6 == 0) goto L_0x00c9
            X.5jA r0 = r6.A00
            r0.A2v(r1)
        L_0x00c9:
            r0 = 1000(0x3e8, float:1.401E-42)
            r5.startActivityForResult(r1, r0)
            return r2
        L_0x00cf:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass69E.A01(X.0kN, X.5rv, java.lang.Runnable, java.lang.String, int, boolean):boolean");
    }

    public static boolean A02(AbstractActivityC121665jA r2, String str, int i, boolean z) {
        return A01(r2, new C125855rv(r2), new Runnable() { // from class: X.6G1
            @Override // java.lang.Runnable
            public final void run() {
                AbstractActivityC121665jA.this.A2q();
            }
        }, str, i, z);
    }

    public AnonymousClass60V A03(C64513Fv r2) {
        return A04(r2, 0);
    }

    public AnonymousClass60V A04(C64513Fv r6, int i) {
        String str;
        int i2;
        Object obj;
        int A00 = A00(r6, i);
        if (this.A01.A07(698)) {
            str = this.A02.A02(String.valueOf(A00));
        } else {
            str = null;
        }
        if (A00 == 443) {
            i2 = R.string.payments_upgrade_error;
        } else {
            if (!(A00 == 6 || A00 == 7)) {
                if (A00 == 405) {
                    i2 = R.string.payments_receiver_not_in_region;
                } else if (A00 == 406) {
                    i2 = R.string.payments_receiver_disabled_in_country;
                } else if (A00 == 409) {
                    i2 = R.string.payments_receiver_generic_error;
                } else if (A00 != 410) {
                    if (A00 != 21137 && A00 != 21138) {
                        switch (A00) {
                            case -2:
                                break;
                            case 400:
                            case 403:
                                i2 = R.string.payments_sender_generic_error;
                                break;
                            case 426:
                                i2 = R.string.payments_receiver_app_version_unsupported;
                                break;
                            case 460:
                                i2 = R.string.payments_receiver_not_in_supported_os;
                                break;
                            case 500:
                            case 4002:
                            case 11500:
                            case 2826004:
                                i2 = R.string.payments_generic_error;
                                break;
                            case 503:
                            case 10702:
                            case 11474:
                            case 11484:
                                i2 = R.string.payments_bank_generic_error;
                                break;
                            case 10780:
                            case 11497:
                            case 11537:
                            case 11540:
                                i2 = R.string.payments_outage_generic_error;
                                break;
                            case 17009:
                                i2 = R.string.upi_mandate_remove_payment_method_with_active_mandate_error;
                                break;
                            case 21157:
                            case 21164:
                                i2 = R.string.payment_invalid_upi_number_error_text;
                                break;
                            default:
                                i2 = 0;
                                break;
                        }
                    } else {
                        i2 = R.string.payment_invalid_upi_number_status_error_text;
                    }
                } else {
                    i2 = R.string.payments_receiver_not_in_group;
                }
            }
            i2 = R.string.no_internet_message;
        }
        StringBuilder A0k = C12960it.A0k("PAY: getErrorString errorCode: ");
        A0k.append(A00);
        A0k.append(" states last error: ");
        if (r6 != null) {
            obj = Integer.valueOf(r6.A00);
        } else {
            obj = "null";
        }
        A0k.append(obj);
        A0k.append(" resId returned: ");
        Log.i(C12960it.A0f(A0k, i2));
        return new AnonymousClass60V(i2, str);
    }

    @Override // X.AnonymousClass18M
    public String ABv(int i) {
        if (i != 2826003) {
            return null;
        }
        AnonymousClass018 r6 = this.A00;
        Object[] A1b = C12970iu.A1b();
        C12960it.A1P(A1b, 5, 0);
        return r6.A0I(A1b, R.plurals.payments_max_requests_reached, 5);
    }

    @Override // X.AnonymousClass18M
    public String ACl(int i, String str) {
        String A00 = this.A02.A00(i);
        return A00 != null ? A00 : str;
    }

    @Override // X.AnonymousClass18M
    public int ACm(C64513Fv r2, int i) {
        return A04(null, i).A00;
    }

    @Override // X.AnonymousClass18M
    public void AI2(String str) {
        if (str.equals("11456") || str.equals("11471")) {
            Log.i(C12960it.A0d(str, C12960it.A0k("PAY: IndiaUpiErrorHelper/handlePaymentTransactionError handle:")));
            this.A04.A0D();
            C1308460e r1 = this.A03;
            r1.A04.A01();
            r1.A09();
        }
    }

    @Override // X.AnonymousClass18M
    public boolean AJI(int i) {
        return C12960it.A1V(i, 11510);
    }

    @Override // X.AnonymousClass18M
    public boolean AJY(int i) {
        return C12960it.A1V(i, 11482);
    }

    @Override // X.AnonymousClass18M
    public boolean AJZ(int i) {
        return C12960it.A1V(i, 11459);
    }

    @Override // X.AnonymousClass18M
    public boolean AJa(int i) {
        return C12960it.A1V(i, 11504);
    }

    @Override // X.AnonymousClass18M
    public boolean AJe(int i) {
        return C12960it.A1V(i, 11503);
    }

    @Override // X.AnonymousClass18M
    public boolean AJk(int i) {
        return C12960it.A1V(i, 11468);
    }

    @Override // X.AnonymousClass18M
    public boolean AdY(int i) {
        return !AKB(i) && !C12960it.A1V(i, 11503) && !C12960it.A1V(i, 11504) && !C12960it.A1V(i, 11482) && !C12960it.A1V(i, 11468);
    }
}
