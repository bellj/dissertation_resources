package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.3Ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64043Ea {
    public final View A00;
    public final ImageView A01;
    public final TextEmojiLabel A02;
    public final C28801Pb A03;
    public final SelectionCheckView A04;
    public final /* synthetic */ AbstractActivityC36611kC A05;

    public C64043Ea(View view, C15610nY r4, AbstractActivityC36611kC r5, AnonymousClass12F r6) {
        this.A05 = r5;
        this.A00 = view.findViewById(R.id.row_container);
        this.A01 = C12970iu.A0L(view, R.id.contact_photo);
        C28801Pb r0 = new C28801Pb(view, r4, r6, (int) R.id.chat_able_contacts_row_name);
        this.A03 = r0;
        TextEmojiLabel textEmojiLabel = r0.A01;
        AnonymousClass028.A0a(textEmojiLabel, 2);
        C27531Hw.A06(textEmojiLabel);
        this.A02 = C12970iu.A0U(view, R.id.chat_able_contacts_row_status);
        this.A04 = (SelectionCheckView) view.findViewById(R.id.selection_check);
    }

    public void A00(String str, boolean z) {
        this.A00.setEnabled(z);
        TextEmojiLabel textEmojiLabel = this.A02;
        textEmojiLabel.setVisibility(0);
        textEmojiLabel.setText(str);
        AbstractActivityC36611kC r3 = this.A05;
        C12980iv.A14(r3.getResources(), textEmojiLabel, R.color.gray_alpha_100);
        textEmojiLabel.setTypeface(null, 2);
        this.A03.A04(r3.getResources().getColor(R.color.gray_alpha_100));
        this.A01.setAlpha(0.5f);
        this.A04.A04(false, false);
    }

    public void A01(boolean z) {
        this.A00.setEnabled(true);
        this.A01.setAlpha(1.0f);
        TextEmojiLabel textEmojiLabel = this.A02;
        textEmojiLabel.setTypeface(null, 0);
        AbstractActivityC36611kC r3 = this.A05;
        C12980iv.A14(r3.getResources(), textEmojiLabel, R.color.list_item_sub_title);
        this.A03.A04(r3.getResources().getColor(R.color.list_item_title));
        this.A04.A04(z, false);
    }
}
