package X;

/* renamed from: X.0t5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18810t5 {
    public C37581mf A00;
    public final C18790t3 A01;
    public final C19930uu A02;

    public C18810t5(C18790t3 r1, C19930uu r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    public C37581mf A00() {
        C37581mf r2 = this.A00;
        if (r2 != null) {
            return r2;
        }
        C37581mf r22 = new C37581mf(this.A01, this.A02);
        this.A00 = r22;
        return r22;
    }
}
