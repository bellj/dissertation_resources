package X;

import android.content.Context;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Pair;
import android.view.Surface;
import com.facebook.redex.RunnableBRunnable0Shape0S0101100_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3mT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76933mT extends AbstractC76533ln {
    public static boolean A0b;
    public static boolean A0c;
    public static final int[] A0d = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public long A0E;
    public long A0F;
    public long A0G;
    public long A0H;
    public long A0I;
    public long A0J;
    public Surface A0K;
    public Surface A0L;
    public AnonymousClass4PF A0M;
    public C98304iT A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public final int A0V = 50;
    public final long A0W = 5000;
    public final Context A0X;
    public final C94494bu A0Y;
    public final AnonymousClass4ME A0Z;
    public final boolean A0a;

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "MediaCodecVideoRenderer";
    }

    public C76933mT(Context context, Handler handler, AbstractC116825Xa r9, AbstractC117015Xu r10, AnonymousClass5XS r11) {
        super(r9, r10, 30.0f, 2);
        Context applicationContext = context.getApplicationContext();
        this.A0X = applicationContext;
        this.A0Y = new C94494bu(applicationContext);
        this.A0Z = new AnonymousClass4ME(handler, r11);
        this.A0a = "NVIDIA".equals(AnonymousClass3JZ.A04);
        this.A0G = -9223372036854775807L;
        this.A06 = -1;
        this.A04 = -1;
        this.A00 = -1.0f;
        this.A0B = 1;
        this.A0C = 0;
        this.A0A = -1;
        this.A08 = -1;
        this.A01 = -1.0f;
        this.A09 = -1;
    }

    public static int A00(C100614mC r6, C95494dp r7) {
        int i = r6.A0A;
        if (i == -1) {
            return A02(r7, r6.A0T, r6.A0I, r6.A09);
        }
        List list = r6.A0U;
        int size = list.size();
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += C72463ee.A0b(list, i3).length;
        }
        return i + i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A02(X.C95494dp r4, java.lang.String r5, int r6, int r7) {
        /*
            r3 = -1
            if (r6 == r3) goto L_0x000d
            if (r7 == r3) goto L_0x000d
            int r0 = r5.hashCode()
            r2 = 4
            switch(r0) {
                case -1851077871: goto L_0x0024;
                case -1664118616: goto L_0x0065;
                case -1662541442: goto L_0x001a;
                case 1187890754: goto L_0x0017;
                case 1331836730: goto L_0x0014;
                case 1599127256: goto L_0x0011;
                case 1599127257: goto L_0x000e;
                default: goto L_0x000d;
            }
        L_0x000d:
            return r3
        L_0x000e:
            java.lang.String r0 = "video/x-vnd.on2.vp9"
            goto L_0x001c
        L_0x0011:
            java.lang.String r0 = "video/x-vnd.on2.vp8"
            goto L_0x0067
        L_0x0014:
            java.lang.String r0 = "video/avc"
            goto L_0x0026
        L_0x0017:
            java.lang.String r0 = "video/mp4v-es"
            goto L_0x0067
        L_0x001a:
            java.lang.String r0 = "video/hevc"
        L_0x001c:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            int r6 = r6 * r7
            goto L_0x006f
        L_0x0024:
            java.lang.String r0 = "video/dolby-vision"
        L_0x0026:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            java.lang.String r2 = X.AnonymousClass3JZ.A05
            java.lang.String r0 = "BRAVIA 4K 2015"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x000d
            java.lang.String r1 = X.AnonymousClass3JZ.A04
            java.lang.String r0 = "Amazon"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0055
            java.lang.String r0 = "KFSOWI"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x000d
            java.lang.String r0 = "AFTS"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0055
            boolean r0 = r4.A06
            if (r0 == 0) goto L_0x0055
            return r3
        L_0x0055:
            r2 = 16
            int r6 = r6 + r2
            int r1 = r6 + -1
            int r1 = r1 / r2
            int r7 = r7 + r2
            int r0 = r7 + -1
            int r0 = r0 / r2
            int r1 = r1 * r0
            int r0 = r1 << 4
            int r6 = r0 << 4
            goto L_0x006e
        L_0x0065:
            java.lang.String r0 = "video/3gpp"
        L_0x0067:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            int r6 = r6 * r7
        L_0x006e:
            r2 = 2
        L_0x006f:
            int r1 = r6 * 3
            int r0 = r2 << 1
            int r1 = r1 / r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76933mT.A02(X.4dp, java.lang.String, int, int):int");
    }

    public static MediaFormat A03(C100614mC r5, AnonymousClass4PF r6, String str, float f, int i, boolean z) {
        Pair A01;
        int A05;
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", str);
        mediaFormat.setInteger("width", r5.A0I);
        mediaFormat.setInteger("height", r5.A09);
        List list = r5.A0U;
        for (int i2 = 0; i2 < list.size(); i2++) {
            mediaFormat.setByteBuffer(C12960it.A0W(i2, "csd-"), ByteBuffer.wrap(C72463ee.A0b(list, i2)));
        }
        float f2 = r5.A01;
        if (f2 != -1.0f) {
            mediaFormat.setFloat("frame-rate", f2);
        }
        int i3 = r5.A0E;
        if (i3 != -1) {
            mediaFormat.setInteger("rotation-degrees", i3);
        }
        C100604mB r4 = r5.A0M;
        if (r4 != null) {
            int i4 = r4.A03;
            if (i4 != -1) {
                mediaFormat.setInteger("color-transfer", i4);
            }
            int i5 = r4.A02;
            if (i5 != -1) {
                mediaFormat.setInteger("color-standard", i5);
            }
            int i6 = r4.A01;
            if (i6 != -1) {
                mediaFormat.setInteger("color-range", i6);
            }
            byte[] bArr = r4.A04;
            if (bArr != null) {
                mediaFormat.setByteBuffer("hdr-static-info", ByteBuffer.wrap(bArr));
            }
        }
        if (!(!"video/dolby-vision".equals(r5.A0T) || (A01 = C95604e3.A01(r5)) == null || (A05 = C12960it.A05(A01.first)) == -1)) {
            mediaFormat.setInteger("profile", A05);
        }
        mediaFormat.setInteger("max-width", r6.A02);
        mediaFormat.setInteger("max-height", r6.A00);
        int i7 = r6.A01;
        if (i7 != -1) {
            mediaFormat.setInteger("max-input-size", i7);
        }
        if (AnonymousClass3JZ.A01 >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (f != -1.0f) {
                mediaFormat.setFloat("operating-rate", f);
            }
        }
        if (z) {
            mediaFormat.setInteger("no-post-process", 1);
            mediaFormat.setInteger("auto-frc", 0);
        }
        if (i != 0) {
            mediaFormat.setFeatureEnabled("tunneled-playback", true);
            mediaFormat.setInteger("audio-session-id", i);
        }
        return mediaFormat;
    }

    public static List A04(C100614mC r4, AbstractC117015Xu r5, boolean z, boolean z2) {
        Pair A01;
        String str;
        String str2 = r4.A0T;
        if (str2 == null) {
            return Collections.emptyList();
        }
        ArrayList A0x = C12980iv.A0x(r5.ACR(str2, z, z2));
        Collections.sort(A0x, new AnonymousClass5CY(new C107384xF(r4)));
        if ("video/dolby-vision".equals(str2) && (A01 = C95604e3.A01(r4)) != null) {
            int A05 = C12960it.A05(A01.first);
            if (A05 == 16 || A05 == 256) {
                str = "video/hevc";
            } else if (A05 == 512) {
                str = "video/avc";
            }
            A0x.addAll(r5.ACR(str, z, z2));
        }
        return Collections.unmodifiableList(A0x);
    }

    public static void A05(Surface surface, AnonymousClass5XG r1) {
        r1.AcR(surface);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0039, code lost:
        if (r1.equals(r0) == false) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0275  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x027d  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A06(java.lang.String r3) {
        /*
        // Method dump skipped, instructions count: 1260
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76933mT.A06(java.lang.String):boolean");
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A07() {
        try {
            super.A07();
        } finally {
            Surface surface = this.A0K;
            if (surface != null) {
                if (this.A0L == surface) {
                    this.A0L = null;
                }
                surface.release();
                this.A0K = null;
            }
        }
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A08() {
        this.A0A = -1;
        this.A08 = -1;
        this.A01 = -1.0f;
        this.A09 = -1;
        A0X();
        this.A0Q = false;
        this.A0Y.A02();
        this.A0N = null;
        try {
            super.A08();
            AnonymousClass4ME r3 = this.A0Z;
            AnonymousClass4U3 r2 = super.A0L;
            synchronized (r2) {
            }
            Handler handler = r3.A00;
            if (handler != null) {
                C12980iv.A18(handler, r3, r2, 9);
            }
        } catch (Throwable th) {
            AnonymousClass4ME r32 = this.A0Z;
            AnonymousClass4U3 r22 = super.A0L;
            synchronized (r22) {
                Handler handler2 = r32.A00;
                if (handler2 != null) {
                    C12980iv.A18(handler2, r32, r22, 9);
                }
                throw th;
            }
        }
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A09(long j, boolean z) {
        long j2;
        super.A09(j, z);
        A0X();
        C94494bu r2 = this.A0Y;
        r2.A04 = 0;
        r2.A05 = -1;
        r2.A07 = -1;
        this.A0H = -9223372036854775807L;
        this.A0F = -9223372036854775807L;
        this.A03 = 0;
        if (z) {
            long j3 = this.A0W;
            if (j3 > 0) {
                j2 = SystemClock.elapsedRealtime() + j3;
            } else {
                j2 = -9223372036854775807L;
            }
            this.A0G = j2;
            return;
        }
        this.A0G = -9223372036854775807L;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r5.A0C != 0) goto L_0x000f;
     */
    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(boolean r6, boolean r7) {
        /*
            r5 = this;
            super.A0A(r6, r7)
            X.4bR r0 = r5.A04
            boolean r2 = r0.A00
            r4 = 0
            if (r2 == 0) goto L_0x000f
            int r1 = r5.A0C
            r0 = 0
            if (r1 == 0) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            X.C95314dV.A04(r0)
            boolean r0 = r5.A0U
            if (r0 == r2) goto L_0x001c
            r5.A0U = r2
            r5.A0E()
        L_0x001c:
            X.4ME r3 = r5.A0Z
            X.4U3 r2 = r5.A0L
            android.os.Handler r1 = r3.A00
            if (r1 == 0) goto L_0x0029
            r0 = 10
            X.C12980iv.A18(r1, r3, r2, r0)
        L_0x0029:
            X.4bu r0 = r5.A0Y
            r0.A03()
            r5.A0R = r7
            r5.A0S = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76933mT.A0A(boolean, boolean):void");
    }

    @Override // X.AbstractC76533ln
    public AnonymousClass4XN A0C(C89864Lr r7) {
        AnonymousClass4XN A0C = super.A0C(r7);
        AnonymousClass4ME r4 = this.A0Z;
        C100614mC r3 = r7.A00;
        Handler handler = r4.A00;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape3S0300000_I1(r4, r3, A0C, 2));
        }
        return A0C;
    }

    @Override // X.AbstractC76533ln
    public void A0G() {
        super.A0G();
        this.A02 = 0;
    }

    @Override // X.AbstractC76533ln
    public void A0N(long j) {
        super.A0N(j);
        if (!this.A0U) {
            this.A02--;
        }
    }

    @Override // X.AbstractC76533ln
    public void A0Q(C76763mA r9) {
        if (this.A0O) {
            ByteBuffer byteBuffer = r9.A02;
            if (byteBuffer.remaining() >= 7) {
                byte b = byteBuffer.get();
                short s = byteBuffer.getShort();
                short s2 = byteBuffer.getShort();
                byte b2 = byteBuffer.get();
                byte b3 = byteBuffer.get();
                byteBuffer.position(0);
                if (b == -75 && s == 60 && s2 == 1 && b2 == 4 && b3 == 0) {
                    byte[] bArr = new byte[byteBuffer.remaining()];
                    byteBuffer.get(bArr);
                    byteBuffer.position(0);
                    AnonymousClass5XG r2 = super.A0P;
                    Bundle A0D = C12970iu.A0D();
                    A0D.putByteArray("hdr10-plus-info", bArr);
                    r2.AcS(A0D);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0247, code lost:
        if (r4 < 50000) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0048, code lost:
        if (r9.A00 != 0) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b5, code lost:
        if (r10.A07[(int) ((r0 - 1) % 15)] != false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x013c, code lost:
        if (r9 > 100000) goto L_0x013e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0146, code lost:
        if (X.AnonymousClass3JZ.A01 >= 21) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0148, code lost:
        A0d(r25, r27, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0164, code lost:
        if (r11 != false) goto L_0x0166;
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01d3  */
    @Override // X.AbstractC76533ln
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0U(X.C100614mC r24, X.AnonymousClass5XG r25, java.nio.ByteBuffer r26, int r27, int r28, int r29, long r30, long r32, long r34, boolean r36, boolean r37) {
        /*
        // Method dump skipped, instructions count: 694
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76933mT.A0U(X.4mC, X.5XG, java.nio.ByteBuffer, int, int, int, long, long, long, boolean, boolean):boolean");
    }

    public void A0W() {
        this.A0S = true;
        if (!this.A0T) {
            this.A0T = true;
            AnonymousClass4ME r3 = this.A0Z;
            Surface surface = this.A0L;
            Handler handler = r3.A00;
            if (handler != null) {
                C12980iv.A18(handler, r3, surface, 8);
            }
            this.A0Q = true;
        }
    }

    public final void A0X() {
        AnonymousClass5XG r1;
        this.A0T = false;
        if (AnonymousClass3JZ.A01 >= 23 && this.A0U && (r1 = super.A0P) != null) {
            this.A0N = new C98304iT(r1, this);
        }
    }

    public final void A0Y() {
        int i = this.A07;
        if (i > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long j = elapsedRealtime - this.A0E;
            AnonymousClass4ME r4 = this.A0Z;
            Handler handler = r4.A00;
            if (handler != null) {
                handler.post(new RunnableBRunnable0Shape0S0101100_I1(r4, i, 0, j));
            }
            this.A07 = 0;
            this.A0E = elapsedRealtime;
        }
    }

    public final void A0Z() {
        int i = this.A06;
        if (i != -1 || this.A04 != -1) {
            if (this.A0A != i || this.A08 != this.A04 || this.A09 != this.A05 || this.A01 != this.A00) {
                AnonymousClass4ME r2 = this.A0Z;
                int i2 = this.A04;
                int i3 = this.A05;
                float f = this.A00;
                Handler handler = r2.A00;
                if (handler != null) {
                    handler.post(new RunnableC76233lJ(r2, f, i, i2, i3));
                }
                this.A0A = this.A06;
                this.A08 = this.A04;
                this.A09 = this.A05;
                this.A01 = this.A00;
            }
        }
    }

    public final void A0a() {
        int i = this.A0A;
        if (i != -1 || this.A08 != -1) {
            AnonymousClass4ME r2 = this.A0Z;
            int i2 = this.A08;
            int i3 = this.A09;
            float f = this.A01;
            Handler handler = r2.A00;
            if (handler != null) {
                handler.post(new RunnableC76233lJ(r2, f, i, i2, i3));
            }
        }
    }

    public void A0b(int i) {
        AnonymousClass4U3 r3 = super.A0L;
        r3.A02 += i;
        int i2 = this.A07 + i;
        this.A07 = i2;
        int i3 = this.A03 + i;
        this.A03 = i3;
        r3.A05 = Math.max(i3, r3.A05);
        int i4 = this.A0V;
        if (i4 > 0 && i2 >= i4) {
            A0Y();
        }
    }

    public void A0c(AnonymousClass5XG r3, int i) {
        C95074d5.A02("skipVideoBuffer");
        ((C107314x8) r3).A02.releaseOutputBuffer(i, false);
        C95074d5.A00();
        super.A0L.A08++;
    }

    public void A0d(AnonymousClass5XG r5, int i, long j) {
        A0Z();
        C95074d5.A02("releaseOutputBuffer");
        r5.Aa7(i, j);
        C95074d5.A00();
        this.A0I = SystemClock.elapsedRealtime() * 1000;
        super.A0L.A06++;
        this.A03 = 0;
        A0W();
    }

    public final boolean A0e(C95494dp r3) {
        if (AnonymousClass3JZ.A01 < 23 || this.A0U || A06(r3.A03)) {
            return false;
        }
        return !r3.A06 || C73663gb.A01(this.A0X);
    }

    @Override // X.AbstractC76533ln, X.AbstractC117055Yb
    public boolean AJx() {
        Surface surface;
        if (!super.AJx() || (!this.A0T && (((surface = this.A0K) == null || this.A0L != surface) && super.A0P != null && !this.A0U))) {
            long j = this.A0G;
            if (j != -9223372036854775807L) {
                if (SystemClock.elapsedRealtime() >= j) {
                    this.A0G = -9223372036854775807L;
                }
            }
            return false;
        }
        this.A0G = -9223372036854775807L;
        return true;
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi, X.AbstractC117055Yb
    public void AcY(float f, float f2) {
        super.AcY(f, f2);
        C94494bu r2 = this.A0Y;
        r2.A01 = f;
        r2.A04 = 0;
        r2.A05 = -1;
        r2.A07 = -1;
        r2.A06(false);
    }
}
