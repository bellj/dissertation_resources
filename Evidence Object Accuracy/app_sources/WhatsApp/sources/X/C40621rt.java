package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1rt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40621rt {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;

    public C40621rt(Long l, Long l2, Long l3, Long l4) {
        this.A02 = l;
        this.A00 = l2;
        this.A01 = l3;
        this.A03 = l4;
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(this.A02, this.A00, this.A01, this.A03));
    }
}
