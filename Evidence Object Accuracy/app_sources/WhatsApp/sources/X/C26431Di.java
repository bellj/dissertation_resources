package X;

import android.database.Cursor;
import java.util.List;

/* renamed from: X.1Di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26431Di extends AnonymousClass1DY {
    public final C14830m7 A00;
    public final C20650w6 A01;
    public final C19990v2 A02;
    public final C15680nj A03;
    public final C15650ng A04;
    public final C15620nZ A05;
    public final AnonymousClass13I A06;
    public final C15860o1 A07;

    public C26431Di(C14830m7 r1, C20650w6 r2, C19990v2 r3, C15680nj r4, C15650ng r5, C15620nZ r6, AnonymousClass13I r7, C15860o1 r8) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A07 = r8;
        this.A03 = r4;
        this.A06 = r7;
        this.A05 = r6;
    }

    public final void A00(AbstractC14640lm r17, List list, long j) {
        long A04 = this.A02.A04(r17);
        C15650ng r8 = this.A04;
        C14830m7 r3 = this.A00;
        Cursor cursor = r8.A0A(r17, 100, A04, r3.A00(), true).A00;
        for (boolean moveToFirst = cursor.moveToFirst(); moveToFirst; moveToFirst = cursor.moveToNext()) {
            AbstractC15340mz A02 = r8.A0K.A02(cursor, r17, false, true);
            if (A02 != null && A02.A11 != A04 && !A02.A0z.A02 && A02.A0G >= r3.A00() - j) {
                list.add(A02);
            }
        }
    }
}
