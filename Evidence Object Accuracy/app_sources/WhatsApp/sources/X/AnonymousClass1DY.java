package X;

import org.json.JSONObject;

/* renamed from: X.1DY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1DY {
    public static String A00(int i, String str) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("success", false);
        jSONObject.put("error_code", i);
        jSONObject.put("error_message", str);
        return jSONObject.toString();
    }

    public static String A01(Object obj) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("success", true);
        jSONObject.putOpt("result", obj);
        return jSONObject.toString();
    }
}
