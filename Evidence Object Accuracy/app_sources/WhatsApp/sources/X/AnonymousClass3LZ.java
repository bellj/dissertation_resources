package X;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import com.google.android.material.snackbar.BaseTransientBottomBar$Behavior;
import java.util.List;

/* renamed from: X.3LZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3LZ implements Handler.Callback {
    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            AbstractC15160mf r5 = (AbstractC15160mf) message.obj;
            C34291fu r6 = r5.A05;
            if (r6.getParent() == null) {
                ViewGroup.LayoutParams layoutParams = r6.getLayoutParams();
                if (layoutParams instanceof AnonymousClass0B5) {
                    AnonymousClass0B5 r3 = (AnonymousClass0B5) layoutParams;
                    BaseTransientBottomBar$Behavior baseTransientBottomBar$Behavior = new BaseTransientBottomBar$Behavior();
                    baseTransientBottomBar$Behavior.A00.A00 = r5.A07;
                    baseTransientBottomBar$Behavior.A04 = new AnonymousClass51A(r5);
                    r3.A00(baseTransientBottomBar$Behavior);
                    r3.A03 = 80;
                }
                r5.A03.addView(r6);
            }
            r6.A00 = new C15140md(r5);
            if (AnonymousClass028.A0r(r6)) {
                List<AccessibilityServiceInfo> enabledAccessibilityServiceList = r5.A04.getEnabledAccessibilityServiceList(1);
                if (enabledAccessibilityServiceList == null || !enabledAccessibilityServiceList.isEmpty()) {
                    r5.A02();
                    return true;
                }
                r5.A00();
                return true;
            }
            r6.A01 = new AnonymousClass51B(r5);
            return true;
        } else if (i != 1) {
            return false;
        } else {
            AbstractC15160mf r62 = (AbstractC15160mf) message.obj;
            int i2 = message.arg1;
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList2 = r62.A04.getEnabledAccessibilityServiceList(1);
            if (enabledAccessibilityServiceList2 != null && enabledAccessibilityServiceList2.isEmpty()) {
                C34291fu r1 = r62.A05;
                if (r1.getVisibility() == 0) {
                    ValueAnimator valueAnimator = new ValueAnimator();
                    int[] A07 = C13000ix.A07();
                    A07[0] = 0;
                    int height = r1.getHeight();
                    ViewGroup.LayoutParams layoutParams2 = r1.getLayoutParams();
                    if (layoutParams2 instanceof ViewGroup.MarginLayoutParams) {
                        height += ((ViewGroup.MarginLayoutParams) layoutParams2).bottomMargin;
                    }
                    A07[1] = height;
                    valueAnimator.setIntValues(A07);
                    valueAnimator.setInterpolator(C50732Qs.A02);
                    valueAnimator.setDuration(250L);
                    valueAnimator.addListener(new AnonymousClass2YP(r62, i2));
                    valueAnimator.addUpdateListener(new C65413Jm(r62));
                    valueAnimator.start();
                    return true;
                }
            }
            r62.A01();
            return true;
        }
    }
}
