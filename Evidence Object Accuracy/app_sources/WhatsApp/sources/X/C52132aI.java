package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.2aI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52132aI extends PrintDocumentAdapter {
    public Context A00;
    public PrintedPdfDocument A01;
    public final C63623Ch A02;
    public final AnonymousClass19M A03;
    public final String A04 = "join_whatsapp_group.pdf";
    public final String A05;

    public C52132aI(Context context, C63623Ch r3, AnonymousClass19M r4, String str) {
        this.A00 = context;
        this.A03 = r4;
        this.A05 = str;
        this.A02 = r3;
    }

    @Override // android.print.PrintDocumentAdapter
    public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
        this.A01 = new PrintedPdfDocument(this.A00, printAttributes2);
        if (cancellationSignal.isCanceled()) {
            layoutResultCallback.onLayoutCancelled();
        } else {
            layoutResultCallback.onLayoutFinished(new PrintDocumentInfo.Builder(this.A04).setContentType(0).setPageCount(1).build(), true);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:23:0x00ef */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:22:0x00a8 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v4, resolved type: android.print.pdf.PrintedPdfDocument */
    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: android.print.PrintDocumentAdapter$WriteResultCallback */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v3, types: [float] */
    /* JADX WARN: Type inference failed for: r4v2, types: [int] */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r4v4, types: [android.print.PrintDocumentAdapter$WriteResultCallback] */
    /* JADX WARN: Type inference failed for: r3v8 */
    @Override // android.print.PrintDocumentAdapter
    public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
        PdfDocument.Page startPage = this.A01.startPage(0);
        Canvas canvas = startPage.getCanvas();
        Context context = this.A00;
        TextView textView = new TextView(context);
        textView.setTextColor(-16777216);
        textView.setTextSize(0, (float) (canvas.getWidth() / 25));
        textView.setGravity(1);
        textView.setText(AbstractC36671kL.A03(context, textView.getPaint(), this.A03, this.A05));
        int width = canvas.getWidth() >> 3;
        textView.measure(C12980iv.A04(canvas.getWidth() - (width << 1)), View.MeasureSpec.makeMeasureSpec(canvas.getHeight(), Integer.MIN_VALUE));
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        canvas.translate((float) width, (float) (width >> 1));
        textView.draw(canvas);
        int i = -width;
        canvas.translate((float) i, (float) (i >> 1));
        C63623Ch r6 = this.A02;
        int i2 = r6.A01;
        int i3 = r6.A00;
        int min = Math.min(canvas.getWidth(), canvas.getHeight() - textView.getMeasuredHeight());
        int i4 = min >> 3;
        int i5 = min - (i4 << 1);
        PrintedPdfDocument printedPdfDocument = (((float) i5) * 1.0f) / ((float) i2);
        canvas.translate((float) i4, (float) (i4 + textView.getMeasuredHeight()));
        Paint A0F = C12990iw.A0F();
        A0F.setColor(-16777216);
        int i6 = 0;
        while (true) {
            if (i6 >= i2) {
                try {
                    int i7 = (i5 << 2) / 15;
                    PrintDocumentAdapter.WriteResultCallback writeResultCallback2 = i5 - i7;
                    int i8 = writeResultCallback2 >> 1;
                    Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_qr_walogo);
                    int i9 = i7 + i8;
                    Rect rect = new Rect(i8, i8, i9, i9);
                    printedPdfDocument = 0;
                    printedPdfDocument = 0;
                    canvas.drawBitmap(decodeResource, (Rect) null, rect, (Paint) null);
                    this.A01.finishPage(startPage);
                    try {
                        writeResultCallback2 = writeResultCallback;
                        this.A01.writeTo(new FileOutputStream(parcelFileDescriptor.getFileDescriptor()));
                        this.A01.close();
                        this.A01 = null;
                        writeResultCallback2.onWriteFinished(new PageRange[]{new PageRange(0, 0)});
                        return;
                    } catch (IOException e) {
                        writeResultCallback2.onWriteFailed(e.toString());
                        this.A01.close();
                        this.A01 = null;
                        return;
                    }
                } catch (Throwable th) {
                    this.A01.close();
                    this.A01 = printedPdfDocument;
                    throw th;
                }
            } else {
                for (int i10 = 0; i10 < i3; i10++) {
                    if (r6.A02[i10][i6] == 1) {
                        canvas.drawRect(printedPdfDocument * ((float) i6), printedPdfDocument * ((float) i10), printedPdfDocument * ((float) (i6 + 1)), printedPdfDocument * ((float) (i10 + 1)), A0F);
                    }
                }
                i6++;
            }
        }
    }
}
