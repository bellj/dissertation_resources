package X;

/* renamed from: X.1jL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC36111jL implements AbstractC36121jM {
    public final C15370n3 A00;
    public final boolean A01;

    public AbstractC36111jL(C15370n3 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AbstractC36121jM
    public boolean isEnabled() {
        return this.A01;
    }
}
