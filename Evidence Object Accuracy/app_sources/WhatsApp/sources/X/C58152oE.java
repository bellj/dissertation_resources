package X;

import android.view.View;
import android.view.animation.Animation;
import com.whatsapp.Conversation;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.2oE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58152oE extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC14670lq A01;

    public C58152oE(View view, AbstractC14670lq r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.clearAnimation();
        AbstractC14670lq r1 = this.A01;
        View view = r1.A0c;
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        AnonymousClass19A r12 = r1.A1H;
        Log.i("voicenote/voicenotepreviewcancelled");
        Iterator A00 = AbstractC16230of.A00(r12);
        while (A00.hasNext()) {
            AnonymousClass2SY r13 = (AnonymousClass2SY) A00.next();
            if (r13 instanceof AnonymousClass39I) {
                Conversation conversation = ((AnonymousClass39I) r13).A00;
                if (conversation.A0g != null) {
                    conversation.A09.setVisibility(8);
                }
                if (conversation.A0Q.getVisibility() == 0) {
                    conversation.A0Q.setVisibility(8);
                }
            }
        }
        view.requestFocus();
    }
}
