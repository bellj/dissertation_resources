package X;

import java.util.Collection;

/* renamed from: X.3tO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81053tO extends AnonymousClass4Y8 {
    @Override // X.AnonymousClass4Y8
    public C81073tQ build() {
        return C81073tQ.fromMapEntries(this.builderMap.entrySet(), null);
    }

    @Override // X.AnonymousClass4Y8
    public Collection newMutableValueCollection() {
        return C28371Mx.preservesInsertionOrderOnAddsSet();
    }

    @Override // X.AnonymousClass4Y8
    public C81053tO put(Object obj, Object obj2) {
        super.put(obj, obj2);
        return this;
    }

    @Override // X.AnonymousClass4Y8
    public C81053tO putAll(Object obj, Iterable iterable) {
        super.putAll(obj, iterable);
        return this;
    }
}
