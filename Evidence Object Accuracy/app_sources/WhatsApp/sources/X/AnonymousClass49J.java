package X;

/* renamed from: X.49J  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49J extends ClassCastException {
    public AnonymousClass49J() {
    }

    public AnonymousClass49J(String str) {
        super("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
