package X;

/* renamed from: X.5yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130025yg {
    public AnonymousClass619 A00;
    public AnonymousClass63Y A01;
    public C130645zk A02;
    public C130665zm A03;

    public synchronized C130665zm A00() {
        return this.A03;
    }

    public final synchronized C130665zm A01() {
        C130665zm r0;
        r0 = this.A03;
        if (r0 == null) {
            r0 = new C130665zm(null, null, null, null, null, null, null);
            this.A03 = r0;
        }
        return r0;
    }

    public synchronized void A02(C127375uO r3, C126835tW r4) {
        A01().A01 = new C126815tU(r3, r4);
    }

    public synchronized void A03(String str, String str2) {
        C130665zm A01 = A01();
        C125695rf r1 = A01.A00;
        if (r1 == null) {
            r1 = new C125695rf(C12970iu.A11());
            A01.A00 = r1;
        }
        r1.A00.put(str, str2);
    }
}
