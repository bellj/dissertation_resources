package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Set;

/* renamed from: X.0xj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21630xj {
    public C28601Of A00;
    public final AnonymousClass01H A01;

    public C21630xj(C16590pI r2) {
        this.A01 = new AnonymousClass01H() { // from class: X.1y0
            @Override // X.AnonymousClass01H
            public final Object get() {
                Set set = ((AnonymousClass01J) AnonymousClass01M.A00(C16590pI.this.A00, AnonymousClass01J.class)).A3E().A00;
                if (set != null) {
                    return AbstractC17940re.copyOf(set);
                }
                throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
            }
        };
    }

    public C28601Of A00() {
        synchronized (this) {
            if (this.A00 == null) {
                HashMap hashMap = new HashMap();
                for (AbstractC18500sY r3 : (Set) this.A01.get()) {
                    AnonymousClass009.A05(r3);
                    String str = r3.A0C;
                    AnonymousClass009.A05(str);
                    if (hashMap.containsKey(str)) {
                        StringBuilder sb = new StringBuilder("MigrationRegistry/addMigration/duplicate; name=");
                        sb.append(str);
                        Log.w(sb.toString());
                    }
                    hashMap.put(str, r3);
                }
                this.A00 = C28601Of.A00(hashMap);
            }
        }
        return this.A00;
    }

    public final AbstractC18500sY A01(String str) {
        return (AbstractC18500sY) A00().A00.get(str);
    }
}
