package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3ix  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74893ix extends AbstractC018308n {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractActivityC36611kC A01;

    public C74893ix(AbstractActivityC36611kC r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        int i = this.A00;
        rect.set(0, i, 0, i);
    }
}
