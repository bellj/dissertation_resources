package X;

import android.content.Context;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3SW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SW implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2 = (RecyclerView) obj;
        AnonymousClass0FB r3 = ((C56002kA) obj2).A06;
        if (r3 != null && (recyclerView = r3.A01) != recyclerView2) {
            if (recyclerView != null) {
                recyclerView.A0o(r3.A02);
                r3.A01.A0T = null;
            }
            r3.A01 = recyclerView2;
            if (recyclerView2 == null) {
                return;
            }
            if (recyclerView2.A0T == null) {
                recyclerView2.A0n(r3.A02);
                RecyclerView recyclerView3 = r3.A01;
                recyclerView3.A0T = r3;
                r3.A00 = new Scroller(recyclerView3.getContext(), new DecelerateInterpolator());
                r3.A03();
                return;
            }
            throw C12960it.A0U("An instance of OnFlingListener already set.");
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        AnonymousClass0FB r1 = ((C56002kA) obj).A06;
        AnonymousClass0FB r0 = ((C56002kA) obj2).A06;
        if (r1 != r0) {
            return r1 == null || !r1.equals(r0);
        }
        return false;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView;
        AnonymousClass0FB r3 = ((C56002kA) obj2).A06;
        if (r3 != null && (recyclerView = r3.A01) != null) {
            recyclerView.A0o(r3.A02);
            r3.A01.A0T = null;
            r3.A01 = null;
        }
    }
}
