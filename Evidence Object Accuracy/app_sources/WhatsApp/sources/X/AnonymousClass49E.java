package X;

import java.io.InputStream;

/* renamed from: X.49E  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49E extends InputStream {
    public AnonymousClass49F A00 = new AnonymousClass49F();
    public AbstractC115355Rf A01;

    public AnonymousClass49E(AbstractC115355Rf r2) {
        this.A01 = r2;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.A00.available();
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.close();
    }

    @Override // java.io.InputStream
    public synchronized void mark(int i) {
        this.A00.mark(i);
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    @Override // java.io.InputStream
    public int read() {
        byte[] bArr = new byte[1];
        int read = read(bArr);
        if (read > 1) {
            throw C12990iw.A0i("Read returned more than 1 byte");
        } else if (read == 1) {
            return (short) (((short) (bArr[0] & 255)) | 0);
        } else {
            return -1;
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        if (bArr != null) {
            return read(bArr, 0, bArr.length);
        }
        throw C12990iw.A0i("Buffer is null.");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:81:0x00fe */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:90:0x0148 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:93:0x00fe */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v1, types: [X.5Ib] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v2, types: [X.4Ks] */
    /* JADX WARN: Type inference failed for: r4v3 */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARN: Type inference failed for: r4v6 */
    /* JADX WARN: Type inference failed for: r4v7 */
    /* JADX WARN: Type inference failed for: r4v8 */
    /* JADX WARN: Type inference failed for: r4v9 */
    /* JADX WARN: Type inference failed for: r4v10 */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // java.io.InputStream
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r19, int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass49E.read(byte[], int, int):int");
    }

    @Override // java.io.InputStream
    public synchronized void reset() {
        this.A00.reset();
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        return this.A00.skip(j);
    }
}
