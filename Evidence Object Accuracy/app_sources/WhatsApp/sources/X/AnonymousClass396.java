package X;

import android.graphics.Bitmap;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Process;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.396  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass396 extends AnonymousClass1MS {
    public final /* synthetic */ AbstractC38761of A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass396(AbstractC38761of r2, String str) {
        super(C12960it.A0d(str, C12960it.A0k("PhotosNetwork-")));
        this.A00 = r2;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Pair pair;
        C232711a r14;
        AbstractC37701mr r6;
        Process.setThreadPriority(10);
        do {
            try {
                AbstractC38761of r4 = this.A00;
                Stack stack = r4.A0B;
                synchronized (stack) {
                    if (stack.size() == 0) {
                        stack.wait();
                    }
                }
                if (stack.size() != 0) {
                    AnonymousClass3BX r0 = null;
                    Object obj = r4.A05;
                    synchronized (obj) {
                        if (stack.size() != 0) {
                            r0 = (AnonymousClass3BX) stack.pop();
                        }
                    }
                    if (r0 != null) {
                        ConcurrentMap concurrentMap = r0.A05;
                        if (concurrentMap.size() != 0 && r0.A06.compareAndSet(false, true)) {
                            if (r4 instanceof C58952ti) {
                                C58952ti r62 = (C58952ti) r4;
                                String A01 = C003501n.A01(r0.A03);
                                AnonymousClass009.A05(A01);
                                StringBuilder sb = new StringBuilder("thumbloader/download ");
                                String str = r0.A04;
                                sb.append(str);
                                Log.i(sb.toString());
                                TrafficStats.setThreadStatsTag(11);
                                try {
                                    AbstractC37631mk A02 = r62.A01.A00().A02(str);
                                    try {
                                        try {
                                            InputStream AAZ = A02.AAZ(r62.A00, null, 30);
                                            try {
                                                r62.A03.A02(AAZ, A01);
                                                if (AAZ != null) {
                                                    AAZ.close();
                                                }
                                            } catch (Throwable th) {
                                                if (AAZ != null) {
                                                    try {
                                                        AAZ.close();
                                                    } catch (Throwable unused) {
                                                    }
                                                }
                                                throw th;
                                                break;
                                            }
                                        } catch (Throwable th2) {
                                            try {
                                                A02.close();
                                            } catch (Throwable unused2) {
                                            }
                                            throw th2;
                                            break;
                                        }
                                    } catch (IOException unused3) {
                                    }
                                    Bitmap A00 = r62.A03.A00(A01, r0.A01, r0.A00);
                                    if (A00 == null) {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("thumbloader/decode failed ");
                                        sb2.append(str);
                                        Log.e(sb2.toString());
                                    }
                                    pair = new Pair(Boolean.TRUE, A00);
                                    A02.close();
                                } catch (IOException e) {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("thumbloader/error downloading ");
                                    sb3.append(str);
                                    Log.e(sb3.toString(), e);
                                    pair = new Pair(Boolean.TRUE, null);
                                }
                                TrafficStats.clearThreadStatsTag();
                            } else if (!(r4 instanceof C38751oe)) {
                                C58962tj r13 = (C58962tj) r4;
                                String str2 = r0.A04;
                                Uri parse = Uri.parse(str2);
                                if (parse == null || !"static.whatsapp.net".equals(parse.getAuthority())) {
                                    r6 = new C37751mw(str2, "image");
                                } else {
                                    r6 = new C37711ms(str2);
                                }
                                C1109457m r5 = new C1109457m();
                                C90374Nq r12 = new C90374Nq(r5, r6);
                                C14850m9 r9 = r13.A02;
                                Bitmap bitmap = null;
                                byte[] bArr = null;
                                if (new CallableC71513d3(r13.A00, r13.A01, r9, r13.A04, r0.A02, r12, r13, r13.A05).A9A().A00.A00 == 0) {
                                    ByteArrayOutputStream byteArrayOutputStream = r5.A00;
                                    if (byteArrayOutputStream != null) {
                                        bArr = byteArrayOutputStream.toByteArray();
                                    }
                                    AnonymousClass009.A05(bArr);
                                    String A012 = C003501n.A01(r0.A03);
                                    AnonymousClass009.A05(A012);
                                    try {
                                        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                                        try {
                                            ((AbstractC38761of) r13).A03.A02(byteArrayInputStream, A012);
                                            byteArrayInputStream.close();
                                        } catch (Throwable th3) {
                                            try {
                                                byteArrayInputStream.close();
                                            } catch (Throwable unused4) {
                                            }
                                            throw th3;
                                            break;
                                        }
                                    } catch (IOException unused5) {
                                    }
                                    bitmap = ((AbstractC38761of) r13).A03.A00(A012, r0.A01, r0.A00);
                                }
                                pair = new Pair(Boolean.TRUE, bitmap);
                            } else {
                                C38751oe r122 = (C38751oe) r4;
                                String A013 = C003501n.A01(r0.A03);
                                AnonymousClass009.A05(A013);
                                StringBuilder sb4 = new StringBuilder("StickerThumbLoader/download ");
                                String str3 = r0.A04;
                                sb4.append(str3);
                                Log.i(sb4.toString());
                                TrafficStats.setThreadStatsTag(11);
                                Bitmap bitmap2 = null;
                                try {
                                    r14 = r122.A00;
                                } catch (IOException e2) {
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append("StickerThumbLoader/error downloading ");
                                    sb5.append(str3);
                                    Log.e(sb5.toString(), e2);
                                }
                                try {
                                    AbstractC37631mk A022 = r14.A08.A00().A02(str3);
                                    try {
                                        r14.A02(((C37621mj) A022).A01.getURL(), (long) A022.A7O());
                                        InputStream AAZ2 = A022.AAZ(r14.A03, null, 26);
                                        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                                        byte[] bArr2 = new byte[4096];
                                        while (true) {
                                            int read = AAZ2.read(bArr2);
                                            if (read == -1) {
                                                break;
                                            }
                                            byteArrayOutputStream2.write(bArr2, 0, read);
                                        }
                                        ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(byteArrayOutputStream2.toByteArray());
                                        AAZ2.close();
                                        A022.close();
                                        try {
                                            C64843Hc r63 = r122.A03;
                                            r63.A02(byteArrayInputStream2, A013);
                                            bitmap2 = r63.A00(A013, r0.A01, r0.A00);
                                            if (bitmap2 == null) {
                                                StringBuilder sb6 = new StringBuilder();
                                                sb6.append("StickerThumbLoader/decode failed ");
                                                sb6.append(str3);
                                                Log.e(sb6.toString());
                                            }
                                            byteArrayInputStream2.close();
                                            TrafficStats.clearThreadStatsTag();
                                            pair = new Pair(Boolean.TRUE, bitmap2);
                                        } catch (Throwable th4) {
                                            try {
                                                byteArrayInputStream2.close();
                                            } catch (Throwable unused6) {
                                            }
                                            throw th4;
                                        }
                                    } finally {
                                    }
                                } catch (IOException e3) {
                                    StringBuilder sb7 = new StringBuilder("StickerPackNetworkProvider/preview thumbnail decode failed ");
                                    sb7.append(str3);
                                    Log.e(sb7.toString(), e3);
                                    throw e3;
                                }
                            }
                            synchronized (obj) {
                                if (!C12970iu.A1Y(pair.first)) {
                                    concurrentMap.clear();
                                    r4.A09.remove(r0.A03);
                                } else {
                                    Bitmap bitmap3 = (Bitmap) pair.second;
                                    C64843Hc r3 = r4.A03;
                                    String str4 = r0.A03;
                                    Bitmap bitmap4 = bitmap3 != null ? bitmap3 : C64843Hc.A05;
                                    C006202y r32 = r3.A02;
                                    synchronized (r32) {
                                        r32.A08(str4, bitmap4);
                                        r32.A01();
                                        r32.A00();
                                    }
                                    r4.A09.remove(str4);
                                    if (concurrentMap.size() != 0) {
                                        ArrayList A0x = C12980iv.A0x(concurrentMap.values());
                                        concurrentMap.clear();
                                        r4.A02.A0H(new RunnableBRunnable0Shape0S0300000_I0(bitmap3, A0x, r4, 15));
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (InterruptedException unused7) {
                return;
            }
        } while (!Thread.interrupted());
    }
}
