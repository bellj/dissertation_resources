package X;

/* renamed from: X.3iR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74643iR extends AnonymousClass02K {
    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        return C29941Vi.A00(obj, obj2);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        return C12960it.A1V(((AbstractC89694Ky) obj).A00, ((AbstractC89694Ky) obj2).A00);
    }
}
