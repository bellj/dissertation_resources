package X;

import android.util.JsonReader;
import java.io.Closeable;
import java.util.NoSuchElementException;

/* renamed from: X.5BB  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5BB implements Closeable {
    public Object A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public final JsonReader A04;

    public abstract Object A01(JsonReader jsonReader);

    public abstract boolean A03(JsonReader jsonReader);

    public AnonymousClass5BB(JsonReader jsonReader) {
        this.A04 = jsonReader;
    }

    public Object A00() {
        Object obj;
        if (this.A01) {
            throw C12990iw.A0i("Closed.");
        } else if (!A02() || (obj = this.A00) == null) {
            throw new NoSuchElementException();
        } else {
            this.A00 = null;
            return obj;
        }
    }

    public boolean A02() {
        if (!this.A01) {
            JsonReader jsonReader = this.A04;
            if (this.A00 != null) {
                return true;
            }
            if (!this.A02) {
                if (!this.A03) {
                    if (A03(jsonReader)) {
                        this.A03 = true;
                    }
                    this.A02 = true;
                }
                while (jsonReader.hasNext()) {
                    Object A01 = A01(jsonReader);
                    this.A00 = A01;
                    if (A01 != null) {
                        return true;
                    }
                }
                this.A02 = true;
            }
            return false;
        }
        throw C12990iw.A0i("Closed.");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01 = true;
        this.A04.close();
    }
}
