package X;

import java.util.List;

/* renamed from: X.2OG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OG extends AnonymousClass1JW {
    public final int A00;
    public final List A01;

    public AnonymousClass2OG(AbstractC15710nm r2, C15450nH r3, List list, int i) {
        super(r2, r3);
        this.A05 = 17;
        this.A00 = i;
        this.A01 = list;
    }
}
