package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1xM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43641xM extends BroadcastReceiver {
    public final /* synthetic */ C43421wz A00;

    public C43641xM(C43421wz r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            this.A00.A00(false);
            return;
        }
        StringBuilder sb = new StringBuilder("unknown intent received in connectivity receiver ");
        sb.append(intent);
        Log.w(sb.toString());
    }
}
