package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/* renamed from: X.0WU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WU implements View.OnTouchListener {
    public float A00 = Float.MAX_VALUE;
    public float A01 = Float.MAX_VALUE;
    public boolean A02 = false;
    public final GestureDetector A03;
    public final ScaleGestureDetector A04;
    public final AnonymousClass0OT A05;
    public final AnonymousClass0Aw A06;

    public AnonymousClass0WU(GestureDetector gestureDetector, ScaleGestureDetector scaleGestureDetector, AnonymousClass0OT r4, AnonymousClass0Aw r5) {
        this.A06 = r5;
        this.A03 = gestureDetector;
        this.A04 = scaleGestureDetector;
        this.A05 = r4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A00(android.view.MotionEvent r7) {
        /*
            r6 = this;
            float r0 = r6.A00
            r1 = 2139095039(0x7f7fffff, float:3.4028235E38)
            r5 = 0
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x006b
            float r0 = r6.A01
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x006b
            int r0 = r7.getPointerCount()
            r3 = 1
            if (r0 <= r3) goto L_0x0022
            X.0Aw r1 = r6.A06
            r0 = 44
            X.0l1 r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x0022
        L_0x0021:
            return r3
        L_0x0022:
            X.0Aw r4 = r6.A06
            boolean r0 = r4.A02
            if (r0 != 0) goto L_0x002c
            boolean r0 = r4.A01
            if (r0 != 0) goto L_0x0035
        L_0x002c:
            r0 = 48
            X.0l1 r0 = r4.A00(r0)
            if (r0 == 0) goto L_0x0035
            return r3
        L_0x0035:
            float r2 = r7.getX()
            float r0 = r6.A00
            float r2 = r2 - r0
            float r1 = r7.getY()
            float r0 = r6.A01
            float r1 = r1 - r0
            float r1 = java.lang.Math.abs(r1)
            float r0 = java.lang.Math.abs(r2)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0060
            r0 = 38
            X.0l1 r0 = r4.A00(r0)
            if (r0 != 0) goto L_0x0021
            r0 = 42
        L_0x0059:
            X.0l1 r0 = r4.A00(r0)
            if (r0 == 0) goto L_0x006b
            return r3
        L_0x0060:
            r0 = 40
            X.0l1 r0 = r4.A00(r0)
            if (r0 != 0) goto L_0x0021
            r0 = 41
            goto L_0x0059
        L_0x006b:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WU.A00(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r1 != 3) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004a, code lost:
        if (r1 != false) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A01(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            android.view.ScaleGestureDetector r0 = r5.A04
            r0.onTouchEvent(r7)
            android.view.ViewParent r3 = r6.getParent()
            boolean r0 = r0.isInProgress()
            r2 = 1
            if (r0 == 0) goto L_0x0079
            X.0Aw r1 = r5.A06
            r0 = 44
            X.0l1 r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x0079
            if (r3 == 0) goto L_0x0021
            r3.requestDisallowInterceptTouchEvent(r2)
            r5.A02 = r2
        L_0x0021:
            r4 = 1
        L_0x0022:
            int r1 = r7.getActionMasked()
            if (r3 == 0) goto L_0x0032
            if (r1 == 0) goto L_0x0067
            if (r1 == r2) goto L_0x004d
            r0 = 2
            if (r1 == r0) goto L_0x0039
            r0 = 3
            if (r1 == r0) goto L_0x0055
        L_0x0032:
            r2 = r4
        L_0x0033:
            X.0Aw r1 = r5.A06
            r0 = 0
            r1.A01 = r0
            return r2
        L_0x0039:
            boolean r1 = r5.A00(r7)
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x004a
            if (r1 != 0) goto L_0x0032
            r0 = 0
            r3.requestDisallowInterceptTouchEvent(r0)
            r5.A02 = r0
            goto L_0x0032
        L_0x004a:
            if (r1 == 0) goto L_0x0032
            goto L_0x0073
        L_0x004d:
            X.0Aw r1 = r5.A06
            r0 = 0
            r1.A02 = r0
            r1.A01(r7)
        L_0x0055:
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x005f
            r0 = 0
            r3.requestDisallowInterceptTouchEvent(r0)
            r5.A02 = r0
        L_0x005f:
            r0 = 2139095039(0x7f7fffff, float:3.4028235E38)
            r5.A01 = r0
            r5.A00 = r0
            goto L_0x0033
        L_0x0067:
            float r0 = r7.getX()
            r5.A00 = r0
            float r0 = r7.getY()
            r5.A01 = r0
        L_0x0073:
            r3.requestDisallowInterceptTouchEvent(r2)
            r5.A02 = r2
            goto L_0x0032
        L_0x0079:
            android.view.GestureDetector r0 = r5.A03
            boolean r4 = r0.onTouchEvent(r7)
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WU.A01(android.view.View, android.view.MotionEvent):boolean");
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        AnonymousClass0OT r0 = this.A05;
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        View view2 = r0.A01;
        obtain.transform(view2.getMatrix());
        obtain.offsetLocation((float) view2.getLeft(), (float) view2.getTop());
        try {
            return A01(view, obtain);
        } finally {
            obtain.recycle();
        }
    }
}
