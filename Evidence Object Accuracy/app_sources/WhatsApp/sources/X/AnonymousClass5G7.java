package X;

/* renamed from: X.5G7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G7 implements AnonymousClass5X5 {
    public AnonymousClass5XE A00;
    public AnonymousClass20K A01;
    public boolean A02;
    public byte[] A03 = null;
    public byte[] A04;
    public byte[] A05;

    public AnonymousClass5G7(AnonymousClass5XE r2) {
        byte[] bArr = {-90, 89, 89, -90};
        this.A04 = bArr;
        this.A05 = bArr;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass5X5
    public String AAf() {
        return this.A00.AAf();
    }

    @Override // X.AnonymousClass5X5
    public void AIf(AnonymousClass20L r3, boolean z) {
        this.A02 = z;
        if (r3 instanceof C113025Fs) {
            r3 = ((C113025Fs) r3).A01;
        }
        if (r3 instanceof AnonymousClass20K) {
            this.A01 = (AnonymousClass20K) r3;
            this.A05 = this.A04;
        } else if (r3 instanceof C113075Fx) {
            C113075Fx r32 = (C113075Fx) r3;
            byte[] bArr = r32.A01;
            this.A05 = bArr;
            this.A01 = (AnonymousClass20K) r32.A00;
            if (bArr.length != 4) {
                throw C12970iu.A0f("IV length not equal to 4");
            }
        }
    }

    @Override // X.AnonymousClass5X5
    public byte[] AfE(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        if (!this.A02) {
            int i3 = i2 >> 3;
            if ((i3 << 3) != i2) {
                throw new C114965Nt("unwrap data must be a multiple of 8 bytes");
            } else if (i3 > 1) {
                byte[] bArr3 = new byte[i2];
                System.arraycopy(bArr, 0, bArr3, 0, i2);
                byte[] bArr4 = new byte[i2];
                if (i3 == 2) {
                    AnonymousClass5XE r2 = this.A00;
                    r2.AIf(this.A01, false);
                    for (int i4 = 0; i4 < i2; i4 += r2.AAt()) {
                        r2.AZY(bArr3, bArr4, i4, i4);
                    }
                    byte[] bArr5 = new byte[8];
                    this.A03 = bArr5;
                    System.arraycopy(bArr4, 0, bArr5, 0, 8);
                    int length = this.A03.length;
                    int i5 = i2 - length;
                    bArr2 = new byte[i5];
                    System.arraycopy(bArr4, length, bArr2, 0, i5);
                } else {
                    int i6 = i2 - 8;
                    bArr2 = new byte[i6];
                    byte[] bArr6 = new byte[8];
                    byte[] bArr7 = new byte[16];
                    System.arraycopy(bArr, 0, bArr6, 0, 8);
                    System.arraycopy(bArr, 8, bArr2, 0, i6);
                    AnonymousClass5XE r12 = this.A00;
                    r12.AIf(this.A01, false);
                    int i7 = (i2 / 8) - 1;
                    int i8 = 5;
                    do {
                        for (int i9 = i7; i9 >= 1; i9--) {
                            System.arraycopy(bArr6, 0, bArr7, 0, 8);
                            int i10 = (i9 - 1) << 3;
                            System.arraycopy(bArr2, i10, bArr7, 8, 8);
                            int i11 = (i7 * i8) + i9;
                            int i12 = 1;
                            while (i11 != 0) {
                                int i13 = 8 - i12;
                                C72463ee.A0P((byte) i11, bArr7, bArr7[i13], i13);
                                i11 >>>= 8;
                                i12++;
                            }
                            r12.AZY(bArr7, bArr7, 0, 0);
                            System.arraycopy(bArr7, 0, bArr6, 0, 8);
                            System.arraycopy(bArr7, 8, bArr2, i10, 8);
                        }
                        i8--;
                    } while (i8 >= 0);
                    this.A03 = bArr6;
                }
                int i14 = 4;
                byte[] bArr8 = new byte[4];
                byte[] bArr9 = new byte[4];
                System.arraycopy(this.A03, 0, bArr8, 0, 4);
                System.arraycopy(this.A03, 4, bArr9, 0, 4);
                int A0P = C72453ed.A0P(bArr9, bArr9[0] << 24, (bArr9[1] & 255) << 16, 1);
                boolean A01 = AnonymousClass1TT.A01(bArr8, this.A05);
                int length2 = bArr2.length;
                if (A0P <= length2 - 8) {
                    A01 = false;
                }
                if (A0P > length2) {
                    A01 = false;
                }
                int i15 = length2 - A0P;
                if (i15 >= 8 || i15 < 0) {
                    A01 = false;
                } else {
                    i14 = i15;
                }
                byte[] bArr10 = new byte[i14];
                System.arraycopy(bArr2, length2 - i14, bArr10, 0, i14);
                if (!AnonymousClass1TT.A01(bArr10, new byte[i14]) || !A01) {
                    throw new C114965Nt("checksum failed");
                }
                byte[] bArr11 = new byte[A0P];
                System.arraycopy(bArr2, 0, bArr11, 0, A0P);
                return bArr11;
            } else {
                throw new C114965Nt("unwrap data must be at least 16 bytes");
            }
        } else {
            throw C12960it.A0U("not set for unwrapping");
        }
    }

    @Override // X.AnonymousClass5X5
    public byte[] Ag9(byte[] bArr, int i, int i2) {
        if (this.A02) {
            byte[] bArr2 = new byte[8];
            byte[] bArr3 = new byte[4];
            AbstractC95434di.A01(bArr3, i2, 0);
            byte[] bArr4 = this.A05;
            int i3 = 0;
            System.arraycopy(bArr4, 0, bArr2, 0, bArr4.length);
            System.arraycopy(bArr3, 0, bArr2, this.A05.length, 4);
            byte[] bArr5 = new byte[i2];
            System.arraycopy(bArr, 0, bArr5, 0, i2);
            int i4 = (8 - (i2 % 8)) % 8;
            int i5 = i2 + i4;
            byte[] bArr6 = new byte[i5];
            System.arraycopy(bArr5, 0, bArr6, 0, i2);
            if (i4 != 0) {
                System.arraycopy(new byte[i4], 0, bArr6, i2, i4);
            }
            if (i5 == 8) {
                byte[] bArr7 = new byte[16];
                System.arraycopy(bArr2, 0, bArr7, 0, 8);
                System.arraycopy(bArr6, 0, bArr7, 8, i5);
                AnonymousClass5XE r1 = this.A00;
                r1.AIf(this.A01, true);
                do {
                    r1.AZY(bArr7, bArr7, i3, i3);
                    i3 += r1.AAt();
                } while (i3 < 16);
                return bArr7;
            }
            AnonymousClass5G6 r2 = new AnonymousClass5G6(this.A00);
            r2.AIf(new C113075Fx(this.A01, bArr2), true);
            return r2.Ag9(bArr6, 0, i5);
        }
        throw C12960it.A0U("not set for wrapping");
    }
}
