package X;

/* renamed from: X.2Cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47722Cd extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;

    public C47722Cd() {
        super(494, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A02);
        r3.Abe(9, this.A03);
        r3.Abe(3, this.A04);
        r3.Abe(5, this.A01);
        r3.Abe(2, this.A05);
        r3.Abe(6, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamCrashLog {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidAppStateMetadata", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidCrashedBuildVersion", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "crashContext", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "crashCount", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "crashReason", this.A05);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "crashType", obj);
        sb.append("}");
        return sb.toString();
    }
}
