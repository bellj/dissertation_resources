package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.254  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass254 {
    public static final AnonymousClass254 A01 = new AnonymousClass254(true);
    public final Map A00;

    static {
        try {
            Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
        }
    }

    public AnonymousClass254() {
        this.A00 = new HashMap();
    }

    public AnonymousClass254(boolean z) {
        this.A00 = Collections.emptyMap();
    }
}
