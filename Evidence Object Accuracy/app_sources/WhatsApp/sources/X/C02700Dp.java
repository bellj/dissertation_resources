package X;

import android.view.WindowInsets;

/* renamed from: X.0Dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02700Dp extends C02710Dq {
    public AnonymousClass0U7 A00 = null;

    public C02700Dp(C018408o r2, WindowInsets windowInsets) {
        super(r2, windowInsets);
    }

    @Override // X.C06250St
    public final AnonymousClass0U7 A01() {
        AnonymousClass0U7 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        WindowInsets windowInsets = this.A03;
        AnonymousClass0U7 A00 = AnonymousClass0U7.A00(windowInsets.getStableInsetLeft(), windowInsets.getStableInsetTop(), windowInsets.getStableInsetRight(), windowInsets.getStableInsetBottom());
        this.A00 = A00;
        return A00;
    }

    @Override // X.C06250St
    public C018408o A08() {
        return C018408o.A02(this.A03.consumeStableInsets());
    }

    @Override // X.C06250St
    public C018408o A09() {
        return C018408o.A02(this.A03.consumeSystemWindowInsets());
    }

    @Override // X.C06250St
    public void A0C(AnonymousClass0U7 r1) {
        this.A00 = r1;
    }

    @Override // X.C06250St
    public boolean A0E() {
        return this.A03.isConsumed();
    }
}
