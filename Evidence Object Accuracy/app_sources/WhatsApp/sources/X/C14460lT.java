package X;

/* renamed from: X.0lT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14460lT implements AbstractC14470lU {
    public final C14430lQ A00;
    public final C14450lS A01;

    public C14460lT(C14430lQ r1, C14450lS r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC14470lU
    public String ADi() {
        StringBuilder sb = new StringBuilder("ThumbnailMediaJob/");
        sb.append(this.A00.A0D);
        return sb.toString();
    }
}
