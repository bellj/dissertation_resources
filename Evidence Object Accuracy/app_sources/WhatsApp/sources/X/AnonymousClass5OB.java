package X;

/* renamed from: X.5OB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OB extends AnonymousClass5OF implements AbstractC115555Rz {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int[] A06;

    public AnonymousClass5OB() {
        this.A06 = new int[80];
        reset();
    }

    public AnonymousClass5OB(AnonymousClass5OB r2) {
        super(r2);
        this.A06 = new int[80];
        A0O(r2);
    }

    public final void A0O(AnonymousClass5OB r5) {
        this.A00 = r5.A00;
        this.A01 = r5.A01;
        this.A02 = r5.A02;
        this.A03 = r5.A03;
        this.A04 = r5.A04;
        int[] iArr = r5.A06;
        System.arraycopy(iArr, 0, this.A06, 0, iArr.length);
        this.A05 = r5.A05;
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OB(this);
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        A0K();
        AbstractC95434di.A01(bArr, this.A00, i);
        AbstractC95434di.A01(bArr, this.A01, i + 4);
        AbstractC95434di.A01(bArr, this.A02, i + 8);
        AbstractC95434di.A01(bArr, this.A03, i + 12);
        AbstractC95434di.A01(bArr, this.A04, i + 16);
        reset();
        return 20;
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "SHA-1";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 20;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        AnonymousClass5OB r12 = (AnonymousClass5OB) r1;
        super.A0M(r12);
        A0O(r12);
    }

    @Override // X.AnonymousClass5OF, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A00 = 1732584193;
        this.A01 = -271733879;
        this.A02 = -1732584194;
        this.A03 = 271733878;
        this.A04 = -1009589776;
        this.A05 = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.A06;
            if (i != iArr.length) {
                iArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public static int A00(int i) {
        return (i >>> 2) | (i << 30);
    }

    public static int A01(int i, int i2, int i3, int i4) {
        return ((i << 5) | (i >>> 27)) + ((i2 ^ i3) ^ i4);
    }

    public static int A02(int i, int i2, int i3, int i4) {
        return ((i << 5) | (i >>> 27)) + ((i2 & i4) | (i2 & i3) | (i3 & i4));
    }

    public static int A03(int[] iArr, int i, int i2, int i3, int i4) {
        return i4 + i2 + iArr[i] + i3;
    }

    @Override // X.AnonymousClass5OF
    public void A0L() {
        int[] iArr;
        int i = 16;
        do {
            iArr = this.A06;
            int i2 = ((iArr[i - 3] ^ iArr[i - 8]) ^ iArr[i - 14]) ^ iArr[i - 16];
            iArr[i] = (i2 >>> 31) | (i2 << 1);
            i++;
        } while (i < 80);
        int i3 = this.A00;
        int i4 = this.A01;
        int i5 = this.A02;
        int i6 = this.A03;
        int i7 = this.A04;
        int i8 = 0;
        int i9 = 0;
        do {
            int i10 = i9 + 1;
            int A0D = i7 + AnonymousClass5OF.A0D(i5, i4, i6, (i3 << 5) | (i3 >>> 27)) + iArr[i9] + 1518500249;
            int A00 = A00(i4);
            int i11 = i10 + 1;
            int A03 = A03(iArr, i10, AnonymousClass5OF.A0D(A00, i3, i5, (A0D << 5) | (A0D >>> 27)), 1518500249, i6);
            int A002 = A00(i3);
            int i12 = i11 + 1;
            int A032 = A03(iArr, i11, AnonymousClass5OF.A0D(A002, A0D, A00, (A03 << 5) | (A03 >>> 27)), 1518500249, i5);
            i7 = A00(A0D);
            int i13 = i12 + 1;
            i4 = A03(iArr, i12, AnonymousClass5OF.A0D(i7, A03, A002, (A032 << 5) | (A032 >>> 27)), 1518500249, A00);
            i6 = A00(A03);
            i9 = i13 + 1;
            i3 = A03(iArr, i13, AnonymousClass5OF.A0D(i6, A032, i7, (i4 << 5) | (i4 >>> 27)), 1518500249, A002);
            i5 = A00(A032);
            i8++;
        } while (i8 < 4);
        int i14 = 0;
        do {
            int i15 = i9 + 1;
            int A01 = i7 + A01(i3, i4, i5, i6) + iArr[i9] + 1859775393;
            int A003 = A00(i4);
            int i16 = i15 + 1;
            int A033 = A03(iArr, i15, A01(A01, i3, A003, i5), 1859775393, i6);
            int A004 = A00(i3);
            int i17 = i16 + 1;
            int A034 = A03(iArr, i16, A01(A033, A01, A004, A003), 1859775393, i5);
            i7 = A00(A01);
            int i18 = i17 + 1;
            i4 = A03(iArr, i17, A01(A034, A033, i7, A004), 1859775393, A003);
            i6 = A00(A033);
            i9 = i18 + 1;
            i3 = A03(iArr, i18, A01(i4, A034, i6, i7), 1859775393, A004);
            i5 = A00(A034);
            i14++;
        } while (i14 < 4);
        int i19 = 0;
        do {
            int i20 = i9 + 1;
            int A02 = i7 + ((A02(i3, i4, i5, i6) + iArr[i9]) - 1894007588);
            int A005 = A00(i4);
            int i21 = i20 + 1;
            int A035 = A03(iArr, i20, A02(A02, i3, A005, i5), -1894007588, i6);
            int A006 = A00(i3);
            int i22 = i21 + 1;
            int A036 = A03(iArr, i21, A02(A035, A02, A006, A005), -1894007588, i5);
            i7 = A00(A02);
            int i23 = i22 + 1;
            i4 = A03(iArr, i22, A02(A036, A035, i7, A006), -1894007588, A005);
            i6 = A00(A035);
            i9 = i23 + 1;
            i3 = A03(iArr, i23, A02(i4, A036, i6, i7), -1894007588, A006);
            i5 = A00(A036);
            i19++;
        } while (i19 < 4);
        int i24 = 0;
        do {
            int i25 = i9 + 1;
            int A012 = i7 + ((A01(i3, i4, i5, i6) + iArr[i9]) - 899497514);
            int A007 = A00(i4);
            int i26 = i25 + 1;
            int A037 = A03(iArr, i25, A01(A012, i3, A007, i5), -899497514, i6);
            int A008 = A00(i3);
            int i27 = i26 + 1;
            int A038 = A03(iArr, i26, A01(A037, A012, A008, A007), -899497514, i5);
            i7 = A00(A012);
            int i28 = i27 + 1;
            i4 = A03(iArr, i27, A01(A038, A037, i7, A008), -899497514, A007);
            i6 = A00(A037);
            i9 = i28 + 1;
            i3 = A03(iArr, i28, A01(i4, A038, i6, i7), -899497514, A008);
            i5 = A00(A038);
            i24++;
        } while (i24 <= 3);
        this.A00 = i3 + i3;
        this.A01 = i4 + i4;
        this.A02 = i5 + i5;
        this.A03 = i6 + i6;
        this.A04 = i7 + i7;
        this.A05 = 0;
        int i29 = 0;
        do {
            iArr[i29] = 0;
            i29++;
        } while (i29 < 16);
    }
}
