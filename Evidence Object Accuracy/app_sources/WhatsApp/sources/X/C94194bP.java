package X;

import java.lang.reflect.Field;

/* renamed from: X.4bP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94194bP {
    public final Field field;

    public C94194bP(Field field) {
        this.field = field;
        field.setAccessible(true);
    }

    public void set(Object obj, int i) {
        try {
            this.field.set(obj, Integer.valueOf(i));
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public void set(Object obj, Object obj2) {
        try {
            this.field.set(obj, obj2);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }
}
