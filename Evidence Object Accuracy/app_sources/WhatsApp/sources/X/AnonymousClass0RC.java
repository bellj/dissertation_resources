package X;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.0RC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RC {
    public static final Comparator A00 = new Comparator() { // from class: X.0eO
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            byte[] bArr = (byte[]) obj;
            byte[] bArr2 = (byte[]) obj2;
            int length = bArr.length;
            int length2 = bArr2.length;
            byte b = length2;
            if (length == length2) {
                for (int i = 0; i < length; i++) {
                    byte b2 = bArr[i];
                    byte b3 = bArr2[i];
                    if (b2 != b3) {
                        length = b2;
                        b = b3;
                    }
                }
                return 0;
            }
            return length - b;
        }
    };

    public static C04720Mu A00(Context context, C05060Oc r22) {
        int i;
        Cursor cursor;
        Uri uri;
        boolean z;
        PackageManager packageManager = context.getPackageManager();
        Resources resources = context.getResources();
        String str = r22.A01;
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(str, 0);
        if (resolveContentProvider != null) {
            String str2 = resolveContentProvider.packageName;
            String str3 = r22.A02;
            if (str2.equals(str3)) {
                Signature[] signatureArr = packageManager.getPackageInfo(str2, 64).signatures;
                ArrayList arrayList = new ArrayList();
                for (Signature signature : signatureArr) {
                    arrayList.add(signature.toByteArray());
                }
                Comparator comparator = A00;
                Collections.sort(arrayList, comparator);
                List list = r22.A04;
                if (list == null) {
                    list = AnonymousClass0KZ.A00(resources, 0);
                }
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ArrayList arrayList2 = new ArrayList((Collection) list.get(i2));
                    Collections.sort(arrayList2, comparator);
                    if (arrayList.size() == arrayList2.size()) {
                        for (int i3 = 0; i3 < arrayList.size(); i3++) {
                            if (Arrays.equals((byte[]) arrayList.get(i3), (byte[]) arrayList2.get(i3))) {
                            }
                        }
                        String str4 = resolveContentProvider.authority;
                        ArrayList arrayList3 = new ArrayList();
                        Uri build = new Uri.Builder().scheme("content").authority(str4).build();
                        Uri build2 = new Uri.Builder().scheme("content").authority(str4).appendPath("file").build();
                        String[] strArr = {"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"};
                        ContentResolver contentResolver = context.getContentResolver();
                        if (Build.VERSION.SDK_INT > 16) {
                            i = 1;
                            cursor = contentResolver.query(build, strArr, "query = ?", new String[]{r22.A03}, null, null);
                        } else {
                            i = 1;
                            cursor = contentResolver.query(build, strArr, "query = ?", new String[]{r22.A03}, null);
                        }
                        if (cursor != null) {
                            try {
                                if (cursor.getCount() > 0) {
                                    int columnIndex = cursor.getColumnIndex("result_code");
                                    arrayList3 = new ArrayList();
                                    int columnIndex2 = cursor.getColumnIndex("_id");
                                    int columnIndex3 = cursor.getColumnIndex("file_id");
                                    int columnIndex4 = cursor.getColumnIndex("font_ttc_index");
                                    int columnIndex5 = cursor.getColumnIndex("font_weight");
                                    int columnIndex6 = cursor.getColumnIndex("font_italic");
                                    while (cursor.moveToNext()) {
                                        int i4 = columnIndex != -1 ? cursor.getInt(columnIndex) : 0;
                                        int i5 = columnIndex4 != -1 ? cursor.getInt(columnIndex4) : 0;
                                        if (columnIndex3 == -1) {
                                            uri = ContentUris.withAppendedId(build, cursor.getLong(columnIndex2));
                                        } else {
                                            uri = ContentUris.withAppendedId(build2, cursor.getLong(columnIndex3));
                                        }
                                        int i6 = columnIndex5 != -1 ? cursor.getInt(columnIndex5) : 400;
                                        if (columnIndex6 != -1) {
                                            z = true;
                                            if (cursor.getInt(columnIndex6) == i) {
                                                arrayList3.add(new C04920No(uri, i5, i6, i4, z));
                                            }
                                        }
                                        z = false;
                                        arrayList3.add(new C04920No(uri, i5, i6, i4, z));
                                    }
                                }
                            } finally {
                                cursor.close();
                            }
                        }
                        return new C04720Mu((C04920No[]) arrayList3.toArray(new C04920No[0]), 0);
                    }
                }
                return new C04720Mu(null, 1);
            }
            StringBuilder sb = new StringBuilder("Found content provider ");
            sb.append(str);
            sb.append(", but package was not ");
            sb.append(str3);
            throw new PackageManager.NameNotFoundException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("No package found for authority: ");
        sb2.append(str);
        throw new PackageManager.NameNotFoundException(sb2.toString());
    }
}
