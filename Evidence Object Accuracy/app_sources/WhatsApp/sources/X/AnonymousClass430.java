package X;

/* renamed from: X.430  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass430 extends AbstractC16110oT {
    public String A00;

    public AnonymousClass430() {
        super(3290, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDirectorySearchQuery {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "queryString", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
