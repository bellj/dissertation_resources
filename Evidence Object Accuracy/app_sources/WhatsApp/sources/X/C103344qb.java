package X;

import android.content.Context;
import com.whatsapp.registration.ChangeNumberOverview;

/* renamed from: X.4qb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103344qb implements AbstractC009204q {
    public final /* synthetic */ ChangeNumberOverview A00;

    public C103344qb(ChangeNumberOverview changeNumberOverview) {
        this.A00 = changeNumberOverview;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
