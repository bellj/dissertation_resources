package X;

import android.content.Context;

/* renamed from: X.4zA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108484zA implements AbstractC115575Sc {
    @Override // X.AbstractC115575Sc
    public final AnonymousClass4PK AbV(Context context, AbstractC116365Vd r7, String str) {
        AnonymousClass4PK r4 = new AnonymousClass4PK();
        r4.A00 = r7.Agj(context, str);
        int Ah1 = r7.Ah1(context, str, true);
        r4.A01 = Ah1;
        int i = r4.A00;
        int i2 = 0;
        if (i == 0) {
            if (Ah1 != 0) {
                i = 0;
            }
            r4.A02 = i2;
            return r4;
        }
        if (Ah1 >= i) {
            r4.A02 = 1;
            return r4;
        }
        i2 = -1;
        r4.A02 = i2;
        return r4;
    }
}
