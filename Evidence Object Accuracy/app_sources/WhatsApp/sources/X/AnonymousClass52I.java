package X;

import java.util.List;

/* renamed from: X.52I  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass52I implements AnonymousClass5TA {
    public final C94394bk A00;

    public /* synthetic */ AnonymousClass52I(C94394bk r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5TA
    public boolean ALL(Object obj) {
        return obj instanceof List;
    }
}
