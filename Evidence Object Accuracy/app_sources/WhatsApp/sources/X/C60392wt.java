package X;

import android.content.Context;
import com.whatsapp.community.CommunitySubgroupsBottomSheet;

/* renamed from: X.2wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60392wt extends AnonymousClass3WV {
    public final /* synthetic */ CommunitySubgroupsBottomSheet A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60392wt(Context context, C14900mE r12, C22640zP r13, CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet, C14830m7 r15, C20040v7 r16, C21190x1 r17, C240514a r18, AnonymousClass12F r19, AbstractC14440lR r20) {
        super(context, r12, r13, r15, r16, r17, r18, r19, r20);
        this.A00 = communitySubgroupsBottomSheet;
    }
}
