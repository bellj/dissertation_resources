package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.3fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73123fi extends InsetDrawable {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    @Override // android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    public void draw(Canvas canvas) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73123fi(Drawable drawable, GroupAdminPickerActivity groupAdminPickerActivity) {
        super(drawable, 0);
        this.A00 = groupAdminPickerActivity;
    }
}
