package X;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

/* renamed from: X.4jl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99104jl implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        DataHolder dataHolder = null;
        ParcelFileDescriptor parcelFileDescriptor = null;
        byte[] bArr = null;
        long j = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c == 3) {
                dataHolder = (DataHolder) C95664e9.A07(parcel, DataHolder.CREATOR, readInt);
            } else if (c == 4) {
                parcelFileDescriptor = (ParcelFileDescriptor) C95664e9.A07(parcel, ParcelFileDescriptor.CREATOR, readInt);
            } else if (c == 5) {
                j = C95664e9.A04(parcel, readInt);
            } else if (c != 6) {
                C95664e9.A0D(parcel, readInt);
            } else {
                bArr = C95664e9.A0I(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78573p8(parcelFileDescriptor, dataHolder, str, bArr, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78573p8[i];
    }
}
