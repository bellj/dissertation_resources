package X;

import com.whatsapp.payments.ui.NoviSharedPaymentSettingsFragment;
import com.whatsapp.util.Log;

/* renamed from: X.68H  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68H implements AnonymousClass2DH {
    public final /* synthetic */ NoviSharedPaymentSettingsFragment A00;

    public AnonymousClass68H(NoviSharedPaymentSettingsFragment noviSharedPaymentSettingsFragment) {
        this.A00 = noviSharedPaymentSettingsFragment;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        Log.e("[PAY] : NoviSharedPaymentSettingsActivity Invite asset Fetch Failed");
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        Log.e("[PAY] : NoviSharedPaymentSettingsActivity Invite asset Fetch Failed");
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        this.A00.A05.A0k("novi_invite_asset_last_sync_timestamp");
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        Log.e("[PAY] : NoviSharedPaymentSettingsActivity Invite asset Fetch Timed Out");
    }
}
