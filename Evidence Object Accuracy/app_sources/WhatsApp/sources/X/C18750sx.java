package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteException;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0sx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18750sx {
    public C43971xw A00;
    public final AbstractC15710nm A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C14950mJ A04;
    public final C22320yt A05;
    public final C16370ot A06;
    public final C18740sw A07;
    public final C16510p9 A08;
    public final C19990v2 A09;
    public final C15650ng A0A;
    public final C21380xK A0B;
    public final C21620xi A0C;
    public final AnonymousClass12H A0D;
    public final AnonymousClass12I A0E;
    public final C16490p7 A0F;
    public final C21390xL A0G;
    public final C21660xm A0H;
    public final C16120oU A0I;
    public final C22140ya A0J;
    public final AnonymousClass12G A0K;
    public final ReentrantReadWriteLock A0L = new ReentrantReadWriteLock();

    public C18750sx(AbstractC15710nm r2, C15570nT r3, C14830m7 r4, C14950mJ r5, C22320yt r6, C16370ot r7, C18740sw r8, C16510p9 r9, C19990v2 r10, C15650ng r11, C21380xK r12, C21620xi r13, AnonymousClass12H r14, AnonymousClass12I r15, C16490p7 r16, C21390xL r17, C21660xm r18, C16120oU r19, C22140ya r20, AnonymousClass12G r21) {
        C43971xw r0 = new C43971xw();
        this.A03 = r4;
        this.A00 = r0;
        this.A02 = r3;
        this.A08 = r9;
        this.A01 = r2;
        this.A09 = r10;
        this.A0I = r19;
        this.A07 = r8;
        this.A04 = r5;
        this.A0B = r12;
        this.A0K = r21;
        this.A0A = r11;
        this.A0D = r14;
        this.A0G = r17;
        this.A05 = r6;
        this.A06 = r7;
        this.A0E = r15;
        this.A0C = r13;
        this.A0F = r16;
        this.A0J = r20;
        this.A0H = r18;
    }

    public static AnonymousClass1YT A00(AnonymousClass1YT r30) {
        boolean z = false;
        if (r30.A05 != null) {
            z = true;
        }
        AnonymousClass009.A0B("not a legacy/v1 call log", z);
        ArrayList arrayList = new ArrayList(r30.A04().size());
        for (AnonymousClass1YV r0 : r30.A04()) {
            arrayList.add(new AnonymousClass1YV(r0.A02, r0.A00, -1));
        }
        AnonymousClass1YU r15 = r30.A0B;
        long j = r30.A09;
        boolean z2 = r30.A0H;
        int i = r30.A01;
        int i2 = r30.A00;
        long j2 = r30.A02;
        return new AnonymousClass1YT(null, r30.A0A, r30.A04, null, r15, null, r30.A07, arrayList, i, i2, -1, j, j2, z2, false, false, r30.A0G);
    }

    public AnonymousClass1YT A01(long j) {
        AnonymousClass1YT r0;
        C006202y r1 = this.A00.A01;
        synchronized (r1) {
            r0 = (AnonymousClass1YT) r1.A04(Long.valueOf(j));
        }
        return r0;
    }

    public AnonymousClass1YT A02(long j) {
        AnonymousClass1YT r0;
        C43971xw r9 = this.A00;
        C006202y r1 = r9.A01;
        synchronized (r1) {
            r0 = (AnonymousClass1YT) r1.A04(Long.valueOf(j));
        }
        if (r0 == null) {
            C18740sw r8 = this.A07;
            C16310on A01 = r8.A03.get();
            try {
                C16330op r6 = A01.A03;
                String l = Long.toString(j);
                Cursor A09 = r6.A09("SELECT call_log._id, call_log.call_id, jid_row_id, from_me, transaction_id, timestamp, video_call, duration, call_result, bytes_transferred, call_log.group_jid_row_id, is_joinable_group_call, call_creator_device_jid_row_id, call_random_id, call_log_row_id, joinable_video_call, call_link._id AS call_link_id, token, creator_jid_row_id FROM call_log LEFT JOIN joinable_call_log ON joinable_call_log.call_log_row_id = call_log._id LEFT JOIN call_link ON call_link._id = call_link_row_id WHERE call_log._id = ?", new String[]{l});
                if (A09.moveToLast()) {
                    Cursor A092 = r6.A09("SELECT _id, jid_row_id, call_result FROM call_log_participant_v2 WHERE call_log_row_id = ? ORDER BY _id", new String[]{l});
                    r0 = r8.A01(A09, A092);
                    if (A092 != null) {
                        A092.close();
                    }
                    A09.close();
                    A01.close();
                    if (r0 != null) {
                        r9.A00(r0);
                        return r0;
                    }
                } else {
                    A09.close();
                    A01.close();
                    return null;
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return r0;
    }

    public final AnonymousClass1YT A03(DeviceJid deviceJid, UserJid userJid, String str, int i, long j, boolean z, boolean z2, boolean z3) {
        AnonymousClass1YU r9 = new AnonymousClass1YU(i, userJid, str, z);
        if (A04(r9) == null) {
            AnonymousClass1YT r4 = new AnonymousClass1YT(null, deviceJid, null, null, r9, null, null, Collections.emptyList(), 0, 0, -1, j, 0, z2, false, false, false);
            if (z3) {
                this.A05.A01(new RunnableBRunnable0Shape3S0200000_I0_3(this, 43, r4), 15);
                return r4;
            }
            A0A(r4);
            return r4;
        }
        StringBuilder sb = new StringBuilder("CallsMessageStore/addCallLogInternal already exists for this key=");
        sb.append(r9);
        throw new IllegalArgumentException(sb.toString());
    }

    public final AnonymousClass1YT A04(AnonymousClass1YU r4) {
        AnonymousClass1YT r0;
        C43971xw r2 = this.A00;
        C006202y r1 = r2.A00;
        synchronized (r1) {
            r0 = (AnonymousClass1YT) r1.A04(r4);
        }
        if (r0 == null && (r0 = this.A07.A02(r4)) != null) {
            r2.A00(r0);
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r14.A0D != false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1YT A05(X.AnonymousClass1YU r13, X.AnonymousClass1YT r14) {
        /*
            r12 = this;
            X.1YT r0 = r12.A04(r13)
            if (r0 != 0) goto L_0x00dc
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.A0L
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r3.writeLock()
            r0.lock()
            X.0sw r6 = r12.A07     // Catch: all -> 0x00d3
            monitor-enter(r6)     // Catch: all -> 0x00d3
            boolean r0 = r14.A0E     // Catch: all -> 0x00d0
            r2 = 1
            r11 = 0
            if (r0 != 0) goto L_0x001d
            boolean r0 = r14.A0D     // Catch: all -> 0x00d0
            r1 = 1
            if (r0 == 0) goto L_0x001e
        L_0x001d:
            r1 = 0
        L_0x001e:
            java.lang.String r0 = "Only regular call log is stored here"
            X.AnonymousClass009.A0B(r0, r1)     // Catch: all -> 0x00d0
            long r7 = r14.A02()     // Catch: all -> 0x00d0
            r4 = -1
            int r0 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
            r1 = 0
            if (r0 == 0) goto L_0x002f
            r1 = 1
        L_0x002f:
            java.lang.String r0 = "CallLog row_id is not set"
            X.AnonymousClass009.A0B(r0, r1)     // Catch: all -> 0x00d0
            X.0p7 r0 = r6.A03     // Catch: all -> 0x00d0
            X.0on r7 = r0.A02()     // Catch: all -> 0x00d0
            X.1Lx r10 = r7.A00()     // Catch: all -> 0x00cb
            android.content.ContentValues r9 = r6.A00(r13, r14)     // Catch: all -> 0x00c6
            X.0op r8 = r7.A03     // Catch: all -> 0x00c6
            java.lang.String r5 = "call_log"
            java.lang.String r4 = "_id = ?"
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch: all -> 0x00c6
            long r0 = r14.A02()     // Catch: all -> 0x00c6
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch: all -> 0x00c6
            r2[r11] = r0     // Catch: all -> 0x00c6
            r8.A00(r5, r9, r4, r2)     // Catch: all -> 0x00c6
            r10.A00()     // Catch: all -> 0x00c6
            X.1YU r1 = r14.A0B     // Catch: all -> 0x00c6
            java.lang.String r5 = "; new key="
            r10.close()     // Catch: all -> 0x00cb
            r7.close()     // Catch: all -> 0x00d0
            X.1YT r4 = r6.A02(r13)     // Catch: all -> 0x00d0
            monitor-exit(r6)     // Catch: all -> 0x00d3
            if (r4 == 0) goto L_0x00af
            X.1xw r0 = r12.A00     // Catch: all -> 0x00d3
            r0.A01(r14)     // Catch: all -> 0x00d3
            r0.A00(r4)     // Catch: all -> 0x00d3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: all -> 0x00d3
            r2.<init>()     // Catch: all -> 0x00d3
            java.lang.String r0 = "CallsMessageStore/updateCallLogOnCurrentThread; callLog.key="
            r2.append(r0)     // Catch: all -> 0x00d3
            r2.append(r1)     // Catch: all -> 0x00d3
            java.lang.String r0 = "; callLog.row_id="
            r2.append(r0)     // Catch: all -> 0x00d3
            long r0 = r14.A02()     // Catch: all -> 0x00d3
            r2.append(r0)     // Catch: all -> 0x00d3
            r2.append(r5)     // Catch: all -> 0x00d3
            r2.append(r13)     // Catch: all -> 0x00d3
            java.lang.String r0 = r2.toString()     // Catch: all -> 0x00d3
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x00d3
            X.0yt r2 = r12.A05     // Catch: all -> 0x00d3
            r0 = 40
            com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3 r1 = new com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3     // Catch: all -> 0x00d3
            r1.<init>(r12, r0, r4)     // Catch: all -> 0x00d3
            r0 = 16
            r2.A01(r1, r0)     // Catch: all -> 0x00d3
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r3.writeLock()
            r0.unlock()
            return r4
        L_0x00af:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x00d3
            r1.<init>()     // Catch: all -> 0x00d3
            java.lang.String r0 = "CallsMessageStore/updateCallLogOnCurrentThread error on creating new call log for this key="
            r1.append(r0)     // Catch: all -> 0x00d3
            r1.append(r13)     // Catch: all -> 0x00d3
            java.lang.String r1 = r1.toString()     // Catch: all -> 0x00d3
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch: all -> 0x00d3
            r0.<init>(r1)     // Catch: all -> 0x00d3
            throw r0     // Catch: all -> 0x00d3
        L_0x00c6:
            r0 = move-exception
            r10.close()     // Catch: all -> 0x00ca
        L_0x00ca:
            throw r0     // Catch: all -> 0x00cb
        L_0x00cb:
            r0 = move-exception
            r7.close()     // Catch: all -> 0x00cf
        L_0x00cf:
            throw r0     // Catch: all -> 0x00d0
        L_0x00d0:
            r0 = move-exception
            monitor-exit(r6)     // Catch: all -> 0x00d3
            throw r0     // Catch: all -> 0x00d3
        L_0x00d3:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r3.writeLock()
            r0.unlock()
            throw r1
        L_0x00dc:
            java.lang.String r1 = "CallsMessageStore/updateCallLogOnCurrentThread already exists for this key="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r13)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18750sx.A05(X.1YU, X.1YT):X.1YT");
    }

    public ArrayList A06(AbstractC38361nx r16, int i, int i2) {
        ReentrantReadWriteLock reentrantReadWriteLock;
        try {
            ArrayList arrayList = new ArrayList();
            reentrantReadWriteLock = this.A0L;
            reentrantReadWriteLock.readLock().lock();
            try {
                C18740sw r11 = this.A07;
                ArrayList arrayList2 = new ArrayList();
                String[] strArr = {Integer.toString(i), Integer.toString(i2)};
                C16310on A01 = r11.A03.get();
                try {
                    C16330op r7 = A01.A03;
                    Cursor A09 = r7.A09("SELECT call_log._id, call_log.call_id, jid_row_id, from_me, transaction_id, timestamp, video_call, duration, call_result, bytes_transferred, call_log.group_jid_row_id, is_joinable_group_call, call_creator_device_jid_row_id, call_random_id, call_log_row_id, joinable_video_call, call_link._id AS call_link_id, token, creator_jid_row_id FROM call_log LEFT JOIN joinable_call_log ON joinable_call_log.call_log_row_id = call_log._id LEFT JOIN call_link ON call_link._id = call_link_row_id ORDER BY call_log._id DESC LIMIT ?,?", strArr);
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                    while (A09.moveToNext() && !r16.Ada()) {
                        Cursor A092 = r7.A09("SELECT _id, jid_row_id, call_result FROM call_log_participant_v2 WHERE call_log_row_id = ? ORDER BY _id", new String[]{Long.toString(A09.getLong(columnIndexOrThrow))});
                        try {
                            AnonymousClass1YT A012 = r11.A01(A09, A092);
                            if (A012 != null) {
                                arrayList2.add(A012);
                            }
                            if (A092 != null) {
                                A092.close();
                            }
                        } catch (Throwable th) {
                            if (A092 != null) {
                                try {
                                    A092.close();
                                } catch (Throwable unused) {
                                }
                            }
                            throw th;
                        }
                    }
                    A09.close();
                    A01.close();
                    StringBuilder sb = new StringBuilder("CallLogStore/getCalls/size=");
                    sb.append(arrayList2.size());
                    Log.i(sb.toString());
                    arrayList.addAll(arrayList2);
                    reentrantReadWriteLock.readLock().unlock();
                    StringBuilder sb2 = new StringBuilder("CallsMessageStore/calls/size:");
                    sb2.append(arrayList.size());
                    Log.i(sb2.toString());
                    return arrayList;
                } catch (Throwable th2) {
                    try {
                        A01.close();
                    } catch (Throwable unused2) {
                    }
                    throw th2;
                }
            } catch (SQLiteException e) {
                Log.e("CallsMessageStore/getCalls/db/unavailable", e);
                reentrantReadWriteLock.readLock().unlock();
                return arrayList;
            }
        } catch (Throwable th3) {
            reentrantReadWriteLock.readLock().unlock();
            throw th3;
        }
    }

    public void A07() {
        boolean z;
        Integer num;
        ArrayList arrayList;
        String[] strArr;
        int i;
        C16490p7 r13 = this.A0F;
        r13.A04();
        if (!r13.A01) {
            Log.i("CallsMessageStore/convertCallLogToV2/database is not ready");
            return;
        }
        ReentrantReadWriteLock reentrantReadWriteLock = this.A0L;
        reentrantReadWriteLock.writeLock().lock();
        r13.A04();
        File file = r13.A07;
        long length = file.length();
        try {
            C16310on A02 = r13.A02();
            C28181Ma r9 = new C28181Ma(false);
            AnonymousClass1Lx A00 = A02.A00();
            r9.A04("CallsMessageStore/convertCallLogToV2");
            int i2 = 0;
            ArrayList arrayList2 = new ArrayList();
            try {
                arrayList = new ArrayList();
                strArr = new String[]{Integer.toString(0), Integer.toString(1000)};
            } catch (SQLiteException e) {
                Log.e("CallsMessageStore/getLegacyCalls/db/unavailable", e);
            }
            try {
                C16310on A01 = r13.get();
                try {
                    Cursor A09 = A01.A03.A09(C43981xx.A01, strArr);
                    if (A09 != null) {
                        int columnIndexOrThrow = A09.getColumnIndexOrThrow("transaction_id");
                        while (A09.moveToNext()) {
                            AbstractC14640lm A06 = this.A08.A06(A09);
                            if (UserJid.of(A06) == null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("CallsMessageStore/getLegacyCallsFromMessageTable/Userjid is null! Got: ");
                                sb.append(A06);
                                Log.w(sb.toString());
                            } else {
                                int i3 = A09.getInt(columnIndexOrThrow);
                                C30401Xg r0 = (C30401Xg) this.A06.A02(A09, A06, false, true);
                                if (r0 != null) {
                                    Iterator it = ((AbstractC30391Xf) r0).A02.iterator();
                                    while (true) {
                                        if (it.hasNext()) {
                                            AnonymousClass1YT r1 = (AnonymousClass1YT) it.next();
                                            if (r1.A0B.A00 == i3) {
                                                arrayList.add(r1);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        A09.close();
                    } else {
                        Log.e("CallsMessageStore/getLegacyCallsFromCallLogsDeprecatedTable/db/cursor is null");
                    }
                    A01.close();
                    StringBuilder sb2 = new StringBuilder("CallsMessageStore/getLegacyCallsFromCallLogsDeprecatedTable/size:");
                    sb2.append(arrayList.size());
                    Log.i(sb2.toString());
                    arrayList2.addAll(arrayList);
                    if (arrayList2.size() < 1000) {
                        int size = 1000 - arrayList2.size();
                        if (arrayList2.isEmpty()) {
                            A01 = this.A07.A03.get();
                            try {
                                Cursor A092 = A01.A03.A09("SELECT COUNT(1) as count, MIN(message_row_id) as first_id FROM call_logs", null);
                                if (A092.moveToLast()) {
                                    i = A092.getInt(A092.getColumnIndexOrThrow("count"));
                                    A092.close();
                                    A01.close();
                                } else {
                                    A092.close();
                                    A01.close();
                                    i = 0;
                                }
                                i2 = 0 - i;
                            } finally {
                            }
                        }
                        ArrayList arrayList3 = new ArrayList();
                        String str = C43981xx.A00;
                        try {
                            C16310on A012 = r13.get();
                            try {
                                Cursor A093 = A012.A03.A09(str, new String[]{Long.toString((long) i2), Integer.toString(size)});
                                while (A093.moveToNext()) {
                                    AbstractC14640lm A062 = this.A08.A06(A093);
                                    if (UserJid.of(A062) == null) {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("CallsMessageStore/getLegacyCallsFromMessageTable/Userjid is null! Got: ");
                                        sb3.append(A062);
                                        Log.w(sb3.toString());
                                    } else {
                                        C30401Xg r02 = (C30401Xg) this.A06.A02(A093, A062, false, true);
                                        if (r02 != null) {
                                            arrayList3.addAll(r02.A15());
                                        }
                                    }
                                }
                                A093.close();
                                A012.close();
                                arrayList2.addAll(arrayList3);
                            } finally {
                            }
                        } catch (SQLiteDiskIOException e2) {
                            this.A0E.A00(1);
                            throw e2;
                        }
                    }
                    Collections.reverse(arrayList2);
                    Iterator it2 = arrayList2.iterator();
                    int i4 = 0;
                    while (it2.hasNext()) {
                        AnonymousClass1YT A002 = A00((AnonymousClass1YT) it2.next());
                        this.A07.A06(A002);
                        i4++;
                        synchronized (A002) {
                        }
                    }
                    ArrayList arrayList4 = new ArrayList(this.A00.A00.A05().values());
                    Collections.sort(arrayList4, new Comparator() { // from class: X.1xy
                        @Override // java.util.Comparator
                        public final int compare(Object obj, Object obj2) {
                            return (((AnonymousClass1YT) obj).A09 > ((AnonymousClass1YT) obj2).A09 ? 1 : (((AnonymousClass1YT) obj).A09 == ((AnonymousClass1YT) obj2).A09 ? 0 : -1));
                        }
                    });
                    Iterator it3 = arrayList4.iterator();
                    while (it3.hasNext()) {
                        AnonymousClass1YT r12 = (AnonymousClass1YT) it3.next();
                        this.A07.A06(r12);
                        i4++;
                        synchronized (r12) {
                        }
                    }
                    if (this.A07.A04.A00("call_log_ready", 0) != 1) {
                        AnonymousClass1YX r2 = this.A0C.A01;
                        synchronized (r2) {
                            C006202y r6 = r2.A01;
                            Iterator it4 = new HashSet(r6.A05().values()).iterator();
                            while (it4.hasNext()) {
                                AbstractC15340mz r14 = (AbstractC15340mz) it4.next();
                                if (C30401Xg.class.isAssignableFrom(r14.getClass())) {
                                    r6.A07(r14.A0z);
                                }
                            }
                            ArrayList arrayList5 = new ArrayList();
                            Map map = r2.A02;
                            for (WeakReference weakReference : map.values()) {
                                AbstractC15340mz r15 = (AbstractC15340mz) weakReference.get();
                                if (r15 != null && C30401Xg.class.isAssignableFrom(r15.getClass())) {
                                    arrayList5.add(r15.A0z);
                                }
                            }
                            Iterator it5 = arrayList5.iterator();
                            while (it5.hasNext()) {
                                map.remove((AnonymousClass1IS) it5.next());
                            }
                        }
                        try {
                            C16310on A022 = r13.A02();
                            try {
                                AnonymousClass1Lx A003 = A022.A00();
                                C16330op r16 = A022.A03;
                                r16.A0B("DELETE FROM messages WHERE media_wa_type = 8");
                                r16.A0B(C29771Up.A00("call_logs"));
                                r16.A0B(C29771Up.A00("call_log_participant"));
                                this.A0G.A04("call_log_ready", 1);
                                A003.A00();
                                A003.close();
                                A022.close();
                            } finally {
                                try {
                                    A022.close();
                                } catch (Throwable unused) {
                                }
                            }
                        } catch (SQLiteException e3) {
                            Log.e("CallsMessageStore/clearLegacyCallLog", e3);
                            z = false;
                            this.A01.AaV("db-migration-call-log-failure", e3.toString(), false);
                            Log.i("CallsMessageStore/clearLegacyCallLog");
                        }
                    }
                    z = true;
                    A00.A00();
                    A00.close();
                    r9.A01();
                    r13.A04();
                    long length2 = file.length();
                    boolean z2 = false;
                    if (!z) {
                        z2 = true;
                    }
                    long A004 = r9.A00();
                    long j = (long) i4;
                    C44001xz r92 = new C44001xz();
                    C21660xm r11 = this.A0H;
                    List list = r11.A00;
                    r92.A01 = Double.valueOf((double) C21660xm.A00(list, (long) ((double) length)));
                    r92.A00 = Double.valueOf((double) C21660xm.A00(list, (long) ((double) length2)));
                    r92.A09 = "call_log";
                    r92.A02 = Double.valueOf((double) C21660xm.A00(list, (long) ((double) this.A04.A02())));
                    r92.A05 = Long.valueOf(A004);
                    r92.A07 = Long.valueOf(C21660xm.A00(r11.A02, j));
                    r92.A08 = 0L;
                    r92.A06 = 0L;
                    if (z2) {
                        num = 2;
                    } else {
                        num = 0;
                    }
                    r92.A04 = num;
                    int intValue = num.intValue();
                    C16120oU r03 = this.A0I;
                    if (intValue == 2) {
                        r03.A06(r92);
                    } else {
                        r03.A07(r92);
                    }
                    A02.close();
                } finally {
                    try {
                        A01.close();
                    } catch (Throwable unused2) {
                    }
                }
            } catch (SQLiteDiskIOException e4) {
                this.A0E.A00(1);
                throw e4;
            }
        } finally {
            reentrantReadWriteLock.writeLock().unlock();
        }
    }

    public void A08(AnonymousClass1YT r4) {
        StringBuilder sb = new StringBuilder("CallsMessageStore/updateCallLog; callLog.key=");
        sb.append(r4.A0B);
        sb.append("; callLog.row_id=");
        sb.append(r4.A02());
        Log.i(sb.toString());
        this.A05.A01(new RunnableBRunnable0Shape3S0200000_I0_3(this, 44, r4), 16);
    }

    public void A09(AnonymousClass1YT r4) {
        AnonymousClass009.A00();
        StringBuilder sb = new StringBuilder("CallsMessageStore/updateCallLogOnCurrentThread; callLog.key=");
        sb.append(r4.A0B);
        sb.append("; callLog.row_id=");
        sb.append(r4.A02());
        Log.i(sb.toString());
        A0B(r4);
    }

    public final void A0A(AnonymousClass1YT r5) {
        ReentrantReadWriteLock reentrantReadWriteLock = this.A0L;
        reentrantReadWriteLock.writeLock().lock();
        try {
            this.A07.A06(r5);
            StringBuilder sb = new StringBuilder();
            sb.append("CallsMessageStore/insertCallLog; callLog.key=");
            sb.append(r5.A0B);
            sb.append("; callLog.getRowId()=");
            sb.append(r5.A02());
            Log.i(sb.toString());
            this.A00.A00(r5);
        } finally {
            reentrantReadWriteLock.writeLock().unlock();
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final void A0B(X.AnonymousClass1YT r18) {
        /*
        // Method dump skipped, instructions count: 364
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18750sx.A0B(X.1YT):void");
    }

    public void A0C(Collection collection) {
        StringBuilder sb = new StringBuilder("CallsMessageStore/deleteCallLogs ");
        sb.append(collection.size());
        Log.i(sb.toString());
        this.A05.A01(new RunnableBRunnable0Shape3S0200000_I0_3(this, 46, collection), 17);
    }
}
