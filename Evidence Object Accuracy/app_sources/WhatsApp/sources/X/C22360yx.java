package X;

import android.content.SharedPreferences;

/* renamed from: X.0yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22360yx {
    public final C15450nH A00;
    public final C14830m7 A01;
    public final C14820m6 A02;
    public final C43281wj A03 = new C43281wj();

    public C22360yx(C15450nH r2, C14830m7 r3, C14820m6 r4) {
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
    }

    public boolean A00() {
        SharedPreferences sharedPreferences = this.A02.A00;
        return !this.A00.A05(AbstractC15460nI.A0T) && sharedPreferences.contains("c2dm_reg_id") && sharedPreferences.getInt("logins_with_messages", 0) < 3;
    }
}
