package X;

import android.graphics.BitmapFactory;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/* renamed from: X.1Dq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26481Dq {
    public final C15450nH A00;
    public final C26471Dp A01;

    public C26481Dq(C15450nH r1, C26471Dp r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean A00(File file, int i, boolean z) {
        AnonymousClass03k r4 = new AnonymousClass03k(new C37601mh(new BufferedInputStream(new FileInputStream(file)), (long) i), C22190yg.A09);
        try {
            C26471Dp r1 = this.A01;
            BitmapFactory.Options options = new BitmapFactory.Options();
            boolean z2 = true;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(r4, null, options);
            int i2 = r1.A01(z).A00;
            if (options.outHeight <= i2) {
                if (options.outWidth <= i2) {
                    z2 = false;
                }
            }
            r4.close();
            return z2;
        } catch (Throwable th) {
            try {
                r4.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
