package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31591an implements AbstractC31371aR {
    public final int A00;
    public final int A01;
    public final C31491ad A02;
    public final byte[] A03;
    public final byte[] A04;

    @Override // X.AbstractC31371aR
    public int getType() {
        return 2;
    }

    public C31591an(SecretKeySpec secretKeySpec, C31641as r10, C31641as r11, C31491ad r12, byte[] bArr, int i, int i2, int i3) {
        byte[] bArr2 = {(byte) (((i << 4) | 3) & 255)};
        AnonymousClass1G4 A0T = AnonymousClass25F.A05.A0T();
        byte[] A00 = r12.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        AnonymousClass25F r1 = (AnonymousClass25F) A0T.A00;
        r1.A00 |= 1;
        r1.A04 = A01;
        A0T.A03();
        AnonymousClass25F r13 = (AnonymousClass25F) A0T.A00;
        r13.A00 |= 2;
        r13.A01 = i2;
        A0T.A03();
        AnonymousClass25F r14 = (AnonymousClass25F) A0T.A00;
        r14.A00 |= 4;
        r14.A02 = i3;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        AnonymousClass25F r15 = (AnonymousClass25F) A0T.A00;
        r15.A00 |= 8;
        r15.A03 = A012;
        byte[] A02 = A0T.A02().A02();
        this.A04 = C31241aE.A00(bArr2, A02, A00(secretKeySpec, r10, r11, C31241aE.A00(bArr2, A02), i));
        this.A02 = r12;
        this.A00 = i2;
        this.A03 = bArr;
        this.A01 = i;
    }

    public C31591an(byte[] bArr) {
        try {
            byte[][] A02 = C31241aE.A02(bArr, 1, (bArr.length - 1) - 8, 8);
            byte b = A02[0][0];
            byte[] bArr2 = A02[1];
            int i = (b & 255) >> 4;
            if (i <= 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("Legacy message: ");
                sb.append(i);
                throw new C31571al(sb.toString());
            } else if (i <= 3) {
                AnonymousClass25F r3 = (AnonymousClass25F) AbstractC27091Fz.A0E(AnonymousClass25F.A05, bArr2);
                int i2 = r3.A00;
                if ((i2 & 8) == 8 && (i2 & 2) == 2 && (i2 & 1) == 1) {
                    this.A04 = bArr;
                    this.A02 = C31481ac.A00(r3.A04.A04());
                    this.A01 = (b & 255) >> 4;
                    this.A00 = r3.A01;
                    this.A03 = r3.A03.A04();
                    return;
                }
                throw new C31521ag("Incomplete message.");
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown version: ");
                sb2.append(i);
                throw new C31521ag(sb2.toString());
            }
        } catch (C28971Pt | C31561ak | ParseException e) {
            throw new C31521ag(e);
        }
    }

    public static final byte[] A00(SecretKeySpec secretKeySpec, C31641as r3, C31641as r4, byte[] bArr, int i) {
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(secretKeySpec);
            if (i >= 3) {
                instance.update(r3.A00.A00());
                instance.update(r4.A00.A00());
            }
            byte[] doFinal = instance.doFinal(bArr);
            byte[] bArr2 = new byte[8];
            System.arraycopy(doFinal, 0, bArr2, 0, 8);
            return bArr2;
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    @Override // X.AbstractC31371aR
    public byte[] Abf() {
        return this.A04;
    }
}
