package X;

import android.content.Intent;
import android.net.Uri;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviEditTransactionDescriptionFragment;

/* renamed from: X.5a2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117485a2 extends ClickableSpan {
    public final /* synthetic */ NoviEditTransactionDescriptionFragment A00;

    public C117485a2(NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment) {
        this.A00 = noviEditTransactionDescriptionFragment;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        C128365vz r4 = new AnonymousClass610("HELP_LINK_CLICK", "SEND_MONEY", "REVIEW_TRANSACTION", "LINK").A00;
        r4.A0i = "ADD_TRANSACTION_MESSAGE";
        r4.A0n = "P2P";
        NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment = this.A00;
        r4.A0L = noviEditTransactionDescriptionFragment.A02().getString(R.string.button_text_learn_more);
        Uri A01 = new AnonymousClass608(noviEditTransactionDescriptionFragment.A02, "632361481136723").A01();
        r4.A0R = A01.toString();
        noviEditTransactionDescriptionFragment.A0v(new Intent("android.intent.action.VIEW", A01));
        noviEditTransactionDescriptionFragment.A04.A06(r4);
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C117295Zj.A0k(this.A00.A02(), textPaint);
    }
}
