package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.098  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass098 extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewGroup A01;
    public final /* synthetic */ AnonymousClass0EA A02;
    public final /* synthetic */ AnonymousClass0EJ A03;
    public final /* synthetic */ AnonymousClass0Q9 A04;
    public final /* synthetic */ boolean A05;

    public AnonymousClass098(View view, ViewGroup viewGroup, AnonymousClass0EA r3, AnonymousClass0EJ r4, AnonymousClass0Q9 r5, boolean z) {
        this.A03 = r4;
        this.A01 = viewGroup;
        this.A00 = view;
        this.A05 = z;
        this.A04 = r5;
        this.A02 = r3;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        ViewGroup viewGroup = this.A01;
        View view = this.A00;
        viewGroup.endViewTransition(view);
        if (this.A05) {
            this.A04.A01.A02(view);
        }
        this.A02.A00();
    }
}
