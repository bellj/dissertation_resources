package X;

/* renamed from: X.1rr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40601rr {
    public static long A00(Long l, long j) {
        return (l == null ? 0 : l.longValue()) + j;
    }

    public static long A01(String[] strArr, int i) {
        if (strArr.length > i) {
            return C28421Nd.A01(strArr[i], 0);
        }
        return 0;
    }

    public static Boolean A02(String[] strArr, int i) {
        if (strArr.length > i) {
            String str = strArr[i];
            if (!"null".equals(str)) {
                return Boolean.valueOf(Boolean.parseBoolean(str));
            }
        }
        return null;
    }

    public static Long A03(String[] strArr, int i) {
        if (strArr.length > i) {
            String str = strArr[i];
            if (!"null".equals(str)) {
                return Long.valueOf(C28421Nd.A01(str, 0));
            }
        }
        return null;
    }
}
