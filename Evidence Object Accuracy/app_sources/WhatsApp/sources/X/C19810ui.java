package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.0ui  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19810ui {
    public final C14650lo A00;

    public C19810ui(C14650lo r1) {
        this.A00 = r1;
    }

    public void A00(AnonymousClass32K r3, C30141Wg r4, UserJid userJid) {
        if (r4 == null || !r4.A0H) {
            r3.A02();
            return;
        }
        String str = r4.A08;
        if (str != null) {
            this.A00.A06(userJid, str);
        }
        this.A00.A07.A02(r3, userJid, false);
    }

    public void A01(AnonymousClass32K r4, UserJid userJid) {
        C14650lo r2 = this.A00;
        C246816l r1 = r2.A07;
        if (r1.A01(userJid) == null || r1.A06(userJid)) {
            r2.A03(new AbstractC30111Wd(r4, userJid) { // from class: X.53P
                public final /* synthetic */ AnonymousClass32K A01;
                public final /* synthetic */ UserJid A02;

                {
                    this.A02 = r3;
                    this.A01 = r2;
                }

                @Override // X.AbstractC30111Wd
                public final void ANP(C30141Wg r7) {
                    C19810ui r5 = C19810ui.this;
                    UserJid userJid2 = this.A02;
                    AnonymousClass32K r3 = this.A01;
                    if (r7 == null) {
                        r5.A00.A04(new AnonymousClass53X(r5, r3, userJid2), userJid2, null);
                    } else {
                        r5.A00(r3, r7, userJid2);
                    }
                }
            }, userJid);
        } else {
            r4.A02();
        }
    }
}
