package X;

import com.whatsapp.util.Log;

/* renamed from: X.1X4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1X4 extends AbstractC16130oV {
    public AnonymousClass1X4(C16150oX r1, AnonymousClass1IS r2, AbstractC16130oV r3, byte b, long j, boolean z) {
        super(r1, r2, r3, b, j, z);
    }

    public AnonymousClass1X4(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public AnonymousClass1X4(C40851sR r1, AnonymousClass1IS r2, byte b, long j, boolean z, boolean z2) {
        super(r2, b, j);
        A1E(r1, z, z2);
    }

    @Override // X.AbstractC15340mz
    public C16460p3 A0G() {
        C16460p3 A0G = super.A0G();
        AnonymousClass009.A05(A0G);
        return A0G;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (r20 != false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        if (android.text.TextUtils.isEmpty(((X.AbstractC16130oV) r14).A06) == false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x026d, code lost:
        if (((X.AbstractC16130oV) r14).A00 > 0) goto L_0x00ce;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C82373vW A1B(X.C15570nT r15, X.C14850m9 r16, X.C82373vW r17, X.AnonymousClass1PG r18, byte[] r19, boolean r20, boolean r21) {
        /*
        // Method dump skipped, instructions count: 712
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X4.A1B(X.0nT, X.0m9, X.3vW, X.1PG, byte[], boolean, boolean):X.3vW");
    }

    public void A1C(C82373vW r11, C39971qq r12) {
        C14850m9 r4 = r12.A02;
        C15570nT r3 = r12.A00;
        boolean z = r12.A08;
        C82373vW A1B = A1B(r3, r4, r11, r12.A04, r12.A09, z, r12.A06);
        C16150oX r0 = ((AbstractC16130oV) this).A02;
        if (r0 == null || ((!z && r0.A0U == null) || A1B == null)) {
            StringBuilder sb = new StringBuilder("FMessageVideo/unable to send encrypted media message due to missing; media_wa_type=");
            sb.append((int) this.A0y);
            Log.w(sb.toString());
            return;
        }
        C38101nW A14 = A14();
        AnonymousClass009.A05(A14);
        byte[] A05 = A14.A05();
        if (A05 != null) {
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(A05, 0, A05.length);
            A1B.A03();
            C40851sR r2 = (C40851sR) A1B.A00;
            r2.A00 |= 32768;
            r2.A0B = A01;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1D(X.C40851sR r6) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.AnonymousClass1XR
            if (r0 != 0) goto L_0x0029
            X.0oX r3 = r5.A02
            X.AnonymousClass009.A05(r3)
            int r1 = r6.A00
            r0 = 32
            r1 = r1 & r0
            if (r1 != r0) goto L_0x0074
            X.1Jp r0 = r6.A0B
            byte[] r2 = r0.A04()
            int r0 = r2.length
            if (r0 <= 0) goto L_0x0074
            X.1nW r1 = r5.A14()
            X.AnonymousClass009.A05(r1)
            monitor-enter(r1)
            r0 = 0
            r1.A03(r2, r0)     // Catch: all -> 0x0026
            goto L_0x0073
        L_0x0026:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0029:
            X.0oX r4 = r5.A02
            X.AnonymousClass009.A05(r4)
            int r2 = r6.A00
            r1 = 32
            r0 = r2 & r1
            if (r0 != r1) goto L_0x0079
            r0 = 65536(0x10000, float:9.18355E-41)
            r2 = r2 & r0
            if (r2 != r0) goto L_0x0069
            int[] r3 = X.AnonymousClass4GT.A00
            int r1 = r6.A01
            if (r1 == 0) goto L_0x0070
            r0 = 1
            if (r1 == r0) goto L_0x006d
            r0 = 2
            if (r1 != r0) goto L_0x0070
            X.4B8 r2 = X.AnonymousClass4B8.A03
        L_0x0049:
            int r0 = r2.ordinal()
            r1 = r3[r0]
            r0 = 1
            if (r1 == r0) goto L_0x006a
            r0 = 2
            if (r1 == r0) goto L_0x006a
            r0 = 3
            if (r1 == r0) goto L_0x0069
            java.lang.String r1 = "MessageUtil/getGifAttribution/error: Unexpected gif attribution="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x0069:
            r0 = 0
        L_0x006a:
            r4.A05 = r0
            return
        L_0x006d:
            X.4B8 r2 = X.AnonymousClass4B8.A01
            goto L_0x0049
        L_0x0070:
            X.4B8 r2 = X.AnonymousClass4B8.A02
            goto L_0x0049
        L_0x0073:
            monitor-exit(r1)
        L_0x0074:
            X.1K6 r0 = r6.A0E
            X.C32411c7.A0R(r3, r0)
        L_0x0079:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X4.A1D(X.1sR):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        if (r13 != false) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0060, code lost:
        if ((r2 & 256) == 256) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0062, code lost:
        r4.A08 = r12.A04;
        r4.A06 = r12.A02;
     */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1E(X.C40851sR r12, boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 548
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X4.A1E(X.1sR, boolean, boolean):void");
    }
}
