package X;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;

/* renamed from: X.5lv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122725lv extends AbstractC118835cS {
    public final LinearLayout A00;
    public final TextView A01;
    public final TextView A02;
    public final ShimmerFrameLayout A03;

    public C122725lv(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.code);
        this.A02 = C12960it.A0I(view, R.id.expireTime);
        this.A00 = C117305Zk.A07(view, R.id.withdraw_code_static_shimmer);
        this.A03 = (ShimmerFrameLayout) AnonymousClass028.A0D(view, R.id.withdraw_code_shimmer);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r6, int i) {
        C123185mk r62 = (C123185mk) r6;
        TextView textView = this.A01;
        textView.setText(r62.A01);
        TextView textView2 = this.A02;
        textView2.setText(r62.A02);
        if (r62.A00 == 1) {
            textView.setVisibility(4);
            textView2.setVisibility(4);
            this.A03.A01();
            this.A00.setVisibility(0);
            return;
        }
        textView.setVisibility(0);
        textView2.setVisibility(0);
        this.A03.A00();
        this.A00.setVisibility(8);
    }
}
