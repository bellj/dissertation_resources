package X;

import android.content.ContentValues;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1Wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30091Wb extends AbstractC21570xd {
    public static final String[] A00 = {"wa_biz_profiles._id", "wa_biz_profiles.jid", "websites", "email", "business_description", "address", "tag", "latitude", "longitude", "vertical", "has_catalog", "address_postal_code", "address_city_id", "address_city_name", "commerce_experience", "shop_url", "cart_enabled", "commerce_manager_url", "direct_connection_enabled", "is_shop_banned", "default_postcode", "location_name", "galaxy_business_enabled", "cover_photo_url", "cover_photo_id"};

    public C30091Wb(C232010t r1) {
        super(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02cb, code lost:
        if (r2 != null) goto L_0x02cd;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C30141Wg A00(com.whatsapp.jid.UserJid r32) {
        /*
        // Method dump skipped, instructions count: 758
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30091Wb.A00(com.whatsapp.jid.UserJid):X.1Wg");
    }

    public final void A01(ContentValues contentValues, C30221Wo r4, C16310on r5, int i, long j) {
        contentValues.clear();
        contentValues.put("wa_biz_profile_id", Long.valueOf(j));
        contentValues.put("account_id", r4.A02);
        contentValues.put("account_type", Integer.valueOf(i));
        contentValues.put("account_display_name", r4.A01);
        contentValues.put("account_fan_count", Integer.valueOf(r4.A00));
        AbstractC21570xd.A00(contentValues, r5, "wa_biz_profiles_linked_accounts_table");
    }

    public void A02(UserJid userJid) {
        if (userJid == null) {
            Log.w("contact-mgr-db/cannot delete business profile details by null jid");
            return;
        }
        C16310on A02 = this.A00.A02();
        try {
            AbstractC21570xd.A02(A02, "wa_biz_profiles", "wa_biz_profiles.jid = ?", new String[]{userJid.getRawString()});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A03(java.util.Map r24) {
        /*
        // Method dump skipped, instructions count: 538
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30091Wb.A03(java.util.Map):void");
    }
}
