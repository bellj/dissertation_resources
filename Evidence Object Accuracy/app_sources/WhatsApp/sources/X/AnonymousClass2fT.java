package X;

/* renamed from: X.2fT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2fT extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass017 A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass1CR A03;
    public final AnonymousClass3EX A04;
    public final C27691It A05;

    public AnonymousClass2fT(AnonymousClass1CR r3, AnonymousClass3EX r4) {
        C16700pc.A0G(r4, r3);
        this.A04 = r4;
        this.A03 = r3;
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A02 = A0T;
        this.A00 = A0T;
        C27691It A03 = C13000ix.A03();
        this.A05 = A03;
        this.A01 = A03;
        r4.A00 = A0T;
    }
}
