package X;

/* renamed from: X.4VD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VD {
    public int A00;
    public int A01;

    public void A00(int i) {
        int i2;
        int i3 = this.A01;
        if (i3 < i || (i2 = this.A00) <= 0) {
            Object[] objArr = new Object[3];
            C12960it.A1P(objArr, i, 0);
            C12960it.A1P(objArr, i3, 1);
            C12960it.A1P(objArr, this.A00, 2);
            AnonymousClass0UN.A04("com.facebook.imagepipeline.memory.BasePool.Counter", "Unexpected decrement of %d. Current numBytes = %d, count = %d", objArr);
            return;
        }
        this.A00 = i2 - 1;
        this.A01 = i3 - i;
    }
}
