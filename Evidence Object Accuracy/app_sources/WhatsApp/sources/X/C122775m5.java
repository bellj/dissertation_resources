package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.5m5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122775m5 extends AbstractC118835cS {
    public View A00;
    public View A01;

    public C122775m5(View view) {
        super(view);
        this.A01 = AnonymousClass028.A0D(view, R.id.divider_space);
        this.A00 = AnonymousClass028.A0D(view, R.id.divider);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r8, int i) {
        int dimensionPixelSize;
        int dimensionPixelSize2;
        int dimensionPixelSize3;
        C123235mp r82 = (C123235mp) r8;
        boolean z = r82.A03;
        View view = this.A01;
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
        View view2 = this.A00;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view2);
        if (r82.A00 == 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = C12960it.A09(this.A0H).getDimensionPixelSize(r82.A00);
        }
        if (r82.A01 == 0) {
            dimensionPixelSize2 = 0;
        } else {
            dimensionPixelSize2 = C12960it.A09(this.A0H).getDimensionPixelSize(r82.A01);
        }
        if (r82.A02 == 0) {
            dimensionPixelSize3 = 0;
        } else {
            dimensionPixelSize3 = C12960it.A09(this.A0H).getDimensionPixelSize(r82.A02);
        }
        A0H.leftMargin = dimensionPixelSize;
        A0H.rightMargin = dimensionPixelSize2;
        A0H.topMargin = dimensionPixelSize3;
        A0H.bottomMargin = 0;
        view2.setLayoutParams(A0H);
    }
}
