package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0vr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20500vr {
    public final C18460sU A00;
    public final C16490p7 A01;

    public C20500vr(C18460sU r1, C16490p7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A00(ContentValues contentValues, AnonymousClass1XF r4, long j) {
        contentValues.put("message_row_id", Long.valueOf(j));
        C30021Vq.A04(contentValues, "order_id", r4.A06);
        C30021Vq.A04(contentValues, "order_title", r4.A07);
        contentValues.put("item_count", Integer.valueOf(r4.A00));
        contentValues.put("status", Integer.valueOf(r4.A01));
        contentValues.put("surface", Integer.valueOf(r4.A02));
        C30021Vq.A04(contentValues, "message", r4.A05);
        UserJid userJid = r4.A03;
        if (userJid != null) {
            contentValues.put("seller_jid", Long.valueOf(this.A00.A01(userJid)));
        }
        C30021Vq.A04(contentValues, "token", r4.A08);
        if (r4.A0G() != null) {
            C30021Vq.A06(contentValues, "thumbnail", r4.A0G().A07());
        }
        String str = r4.A04;
        if (str != null && r4.A09 != null) {
            contentValues.put("currency_code", str);
            contentValues.put("total_amount_1000", Long.valueOf(r4.A09.multiply(C30701Ym.A00).longValue()));
        }
    }

    public void A01(AnonymousClass1XF r7) {
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(r7.A11));
            C30021Vq.A04(contentValues, "order_id", r7.A06);
            C30021Vq.A04(contentValues, "order_title", r7.A07);
            contentValues.put("item_count", Integer.valueOf(r7.A00));
            contentValues.put("status", Integer.valueOf(r7.A01));
            contentValues.put("surface", Integer.valueOf(r7.A02));
            C30021Vq.A04(contentValues, "message", r7.A05);
            UserJid userJid = r7.A03;
            if (userJid != null) {
                contentValues.put("seller_jid", Long.valueOf(this.A00.A01(userJid)));
            }
            C30021Vq.A04(contentValues, "token", r7.A08);
            if (r7.A0G() != null) {
                C30021Vq.A06(contentValues, "thumbnail", r7.A0G().A07());
            }
            String str = r7.A04;
            if (!(str == null || r7.A09 == null)) {
                contentValues.put("currency_code", str);
                contentValues.put("total_amount_1000", Long.valueOf(r7.A09.multiply(C30701Ym.A00).longValue()));
            }
            boolean z = false;
            if (A02.A03.A03(contentValues, "message_order") == r7.A11) {
                z = true;
            }
            AnonymousClass009.A0C("OrderMessageStore/insertOrUpdateOrderMessage/inserted row should have same row_id", z);
            A02.close();
        } catch (SQLiteConstraintException e) {
            StringBuilder sb = new StringBuilder("OrderMessageStore/insertOrUpdateOrderMessage/fail to insert. Error message is: ");
            sb.append(e);
            Log.e(sb.toString());
        }
    }

    public void A02(AnonymousClass1XF r6, long j) {
        boolean z = true;
        boolean z2 = false;
        if (r6.A08() == 2) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessage/message in main storage; key=");
        sb.append(r6.A0z);
        AnonymousClass009.A0B(sb.toString(), z2);
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues();
            A00(contentValues, r6, j);
            if (A02.A03.A03(contentValues, "message_quoted_order") != j) {
                z = false;
            }
            AnonymousClass009.A0C("OrderMessageStore/insertOrUpdateQuotedOrderMessage/inserted row should have same row_id", z);
            A02.close();
        } catch (SQLiteConstraintException e) {
            StringBuilder sb2 = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessage/fail to insert. Error message is: ");
            sb2.append(e);
            Log.e(sb2.toString());
        }
    }

    public final void A03(AnonymousClass1XF r9, String str, boolean z) {
        boolean z2 = false;
        if (r9.A11 > 0) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("OrderMessageStore/fillOrderDataIfAvailable/message must have row_id set; key=");
        sb.append(r9.A0z);
        AnonymousClass009.A0B(sb.toString(), z2);
        String[] strArr = {String.valueOf(r9.A11)};
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09 != null) {
                if (A09.moveToLast()) {
                    C18460sU r6 = this.A00;
                    r9.A06 = A09.getString(A09.getColumnIndexOrThrow("order_id"));
                    r9.A07 = A09.getString(A09.getColumnIndexOrThrow("order_title"));
                    r9.A00 = A09.getInt(A09.getColumnIndexOrThrow("item_count"));
                    r9.A05 = A09.getString(A09.getColumnIndexOrThrow("message"));
                    r9.A01 = A09.getInt(A09.getColumnIndexOrThrow("status"));
                    r9.A02 = A09.getInt(A09.getColumnIndexOrThrow("surface"));
                    r9.A03 = (UserJid) r6.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("seller_jid")));
                    r9.A08 = A09.getString(A09.getColumnIndexOrThrow("token"));
                    String string = A09.getString(A09.getColumnIndexOrThrow("currency_code"));
                    r9.A04 = string;
                    if (!TextUtils.isEmpty(string)) {
                        try {
                            r9.A09 = C30701Ym.A00(new C30711Yn(r9.A04), A09.getLong(A09.getColumnIndexOrThrow("total_amount_1000")));
                        } catch (IllegalArgumentException unused) {
                            r9.A04 = null;
                        }
                    }
                    byte[] blob = A09.getBlob(A09.getColumnIndexOrThrow("thumbnail"));
                    if (blob != null && blob.length > 0) {
                        ((AbstractC15340mz) r9).A02 = 1;
                        C16460p3 A0G = r9.A0G();
                        if (A0G != null) {
                            A0G.A03(blob, z);
                        }
                    }
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }
}
