package X;

import java.util.Collections;

/* renamed from: X.0Gu  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gu extends AnonymousClass0QR {
    public final Object A00;

    @Override // X.AnonymousClass0QR
    public float A00() {
        return 1.0f;
    }

    public AnonymousClass0Gu(AnonymousClass0SF r2, Object obj) {
        super(Collections.emptyList());
        A08(r2);
        this.A00 = obj;
    }

    @Override // X.AnonymousClass0QR
    public Object A03() {
        AnonymousClass0SF r2 = this.A03;
        Object obj = this.A00;
        AnonymousClass0NB r0 = r2.A02;
        r0.A01 = obj;
        r0.A00 = obj;
        return r2.A01;
    }

    @Override // X.AnonymousClass0QR
    public Object A04(AnonymousClass0U8 r2, float f) {
        return A03();
    }

    @Override // X.AnonymousClass0QR
    public void A06() {
        if (this.A03 != null) {
            super.A06();
        }
    }

    @Override // X.AnonymousClass0QR
    public void A07(float f) {
        this.A02 = f;
    }
}
