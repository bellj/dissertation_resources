package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.location.WaMapView;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.2ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60792ye extends AnonymousClass2xh {
    public static final Set A0G;
    public C64343Fe A00;
    public C26171Ch A01;
    public C17990rj A02;
    public boolean A03;
    public final View A04 = findViewById(R.id.control_frame);
    public final View A05;
    public final View A06 = findViewById(R.id.progress_bar);
    public final View A07 = findViewById(R.id.thumb_button);
    public final FrameLayout A08;
    public final ImageView A09 = C12970iu.A0L(this, R.id.thumb);
    public final TextView A0A = C12960it.A0J(this, R.id.control_btn);
    public final TextView A0B;
    public final TextView A0C;
    public final TextEmojiLabel A0D;
    public final WaMapView A0E = ((WaMapView) findViewById(R.id.map_holder));
    public final AbstractC35401hl A0F;

    static {
        HashSet A12 = C12970iu.A12();
        A12.add("www.facebook.com");
        A12.add("maps.google.com");
        A12.add("foursquare.com");
        A0G = Collections.unmodifiableSet(A12);
    }

    public C60792ye(Context context, AbstractC13890kV r4, AnonymousClass1XO r5) {
        super(context, r4, r5);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.place_name);
        this.A0D = A0U;
        this.A0C = C12960it.A0J(this, R.id.place_address);
        this.A0B = C12960it.A0J(this, R.id.host_view);
        this.A05 = findViewById(R.id.message_info_holder);
        if (A0U != null) {
            A0U.setLongClickable(AbstractC28491Nn.A07(A0U));
        }
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.location_bubble_frame);
        this.A08 = frameLayout;
        if (frameLayout != null) {
            frameLayout.setForeground(getInnerFrameForegroundDrawable());
        }
        this.A0F = C65213Iq.A00(context);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0101, code lost:
        if (r15.A04 == false) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0146, code lost:
        if (r2 == 2) goto L_0x0148;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
        // Method dump skipped, instructions count: 538
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60792ye.A1M():void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return getIncomingLayoutId();
    }

    @Override // X.AbstractC28551Oa
    public AnonymousClass1XO getFMessage() {
        return (AnonymousClass1XO) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_location_left_large;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        FrameLayout frameLayout = this.A08;
        if (frameLayout != null) {
            innerFrameLayouts.add(frameLayout);
        }
        return innerFrameLayouts;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        if (this.A03) {
            return AnonymousClass3GD.A02(this);
        }
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_location_right_large;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XP);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
