package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.3k7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75603k7 extends AnonymousClass03U {
    public final C15570nT A00;
    public final WaImageView A01;
    public final WaTextView A02;
    public final C15550nR A03;
    public final AnonymousClass1J1 A04;

    public C75603k7(View view, C15570nT r3, C15550nR r4, AnonymousClass1J1 r5) {
        super(view);
        this.A00 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = (WaImageView) AnonymousClass028.A0D(view, R.id.poll_results_user_picture);
        this.A02 = (WaTextView) AnonymousClass028.A0D(view, R.id.poll_results_user_name);
    }
}
