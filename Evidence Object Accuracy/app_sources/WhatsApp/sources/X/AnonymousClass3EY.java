package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3EY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3EY {
    public String A00;
    public ArrayList A01 = C12960it.A0l();
    public boolean A02;
    public boolean A03;
    public final List A04 = C12960it.A0l();

    public void A00(String str, Collection collection, boolean z) {
        AnonymousClass009.A01();
        this.A02 = z;
        if (collection != null) {
            this.A04.addAll(collection);
        }
        this.A00 = str;
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            ((C54462gl) it.next()).A0E(this);
        }
        this.A03 = false;
    }
}
