package X;

import android.os.SystemClock;
import java.util.HashMap;

/* renamed from: X.1c9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32431c9 {
    public final long A00 = 60000;
    public final HashMap A01 = new HashMap();

    public synchronized void A00() {
        this.A01.clear();
    }

    public synchronized void A01(Object obj) {
        this.A01.remove(obj);
    }

    public synchronized boolean A02(Object obj) {
        boolean z;
        HashMap hashMap = this.A01;
        Long l = (Long) hashMap.get(obj);
        if (l == null || l.longValue() + this.A00 <= SystemClock.elapsedRealtime()) {
            hashMap.put(obj, Long.valueOf(SystemClock.elapsedRealtime()));
            z = true;
        } else {
            z = false;
        }
        return z;
    }
}
