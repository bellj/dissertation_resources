package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import com.whatsapp.group.GroupParticipantsSearchFragment;

/* renamed from: X.3fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73133fj extends InsetDrawable {
    public final /* synthetic */ GroupParticipantsSearchFragment A00;

    @Override // android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    public void draw(Canvas canvas) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73133fj(Drawable drawable, GroupParticipantsSearchFragment groupParticipantsSearchFragment) {
        super(drawable, 0);
        this.A00 = groupParticipantsSearchFragment;
    }
}
