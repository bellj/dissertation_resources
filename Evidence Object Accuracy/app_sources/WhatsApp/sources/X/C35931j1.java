package X;

import com.whatsapp.jobqueue.job.RotateSignedPreKeyJob;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1j1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35931j1 extends AbstractC35941j2 {
    public final /* synthetic */ RotateSignedPreKeyJob A00;
    public final /* synthetic */ AtomicInteger A01;
    public final /* synthetic */ AtomicReference A02;

    public C35931j1(RotateSignedPreKeyJob rotateSignedPreKeyJob, AtomicInteger atomicInteger, AtomicReference atomicReference) {
        this.A00 = rotateSignedPreKeyJob;
        this.A01 = atomicInteger;
        this.A02 = atomicReference;
    }
}
