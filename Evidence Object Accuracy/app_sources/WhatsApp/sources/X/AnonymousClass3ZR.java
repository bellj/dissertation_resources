package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.3ZR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZR implements AnonymousClass2EJ {
    public final /* synthetic */ AnonymousClass2EM A00;

    public AnonymousClass3ZR(AnonymousClass2EM r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2EJ
    public void APm(Pair pair) {
        AnonymousClass016 r1 = this.A00.A06;
        if (r1 != null) {
            r1.A0A(Boolean.TRUE);
        }
    }

    @Override // X.AnonymousClass2EJ
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        this.A00.A0L.Ab2(new RunnableBRunnable0Shape10S0200000_I1(this, 42, obj));
    }
}
