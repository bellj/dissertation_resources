package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.4zH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108554zH implements AbstractC115605Sf {
    @Override // X.AbstractC115605Sf
    public final /* synthetic */ AbstractC115135Qi Ah4(int i) {
        if (i == 100) {
            return EnumC87364Bf.A0M;
        }
        switch (i) {
            case 0:
                return EnumC87364Bf.A02;
            case 1:
                return EnumC87364Bf.A03;
            case 2:
                return EnumC87364Bf.A04;
            case 3:
                return EnumC87364Bf.A05;
            case 4:
                return EnumC87364Bf.A06;
            case 5:
                return EnumC87364Bf.A07;
            case 6:
                return EnumC87364Bf.A08;
            case 7:
                return EnumC87364Bf.A09;
            case 8:
                return EnumC87364Bf.A0A;
            case 9:
                return EnumC87364Bf.A0B;
            case 10:
                return EnumC87364Bf.A0C;
            case 11:
                return EnumC87364Bf.A0D;
            case 12:
                return EnumC87364Bf.A0E;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return EnumC87364Bf.A0F;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return EnumC87364Bf.A0G;
            case 15:
                return EnumC87364Bf.A0H;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return EnumC87364Bf.A0I;
            case 17:
                return EnumC87364Bf.A0J;
            case 18:
                return EnumC87364Bf.A0K;
            case 19:
                return EnumC87364Bf.A0L;
            default:
                return null;
        }
    }
}
