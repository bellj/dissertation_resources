package X;

/* renamed from: X.1f6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33801f6 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public Long A09;
    public Long A0A;

    public C33801f6() {
        super(476, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A02);
        r3.Abe(6, this.A08);
        r3.Abe(4, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(1, this.A00);
        r3.Abe(9, this.A09);
        r3.Abe(10, this.A01);
        r3.Abe(7, this.A06);
        r3.Abe(3, this.A0A);
        r3.Abe(11, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        StringBuilder sb = new StringBuilder("WamE2eMessageSend {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eCiphertextType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eCiphertextVersion", this.A08);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eDestination", obj2);
        Integer num3 = this.A04;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eFailureReason", obj3);
        Integer num4 = this.A05;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eReceiverType", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eSuccessful", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "encRetryCount", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsInvisible", this.A01);
        Integer num5 = this.A06;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryCount", this.A0A);
        Integer num6 = this.A07;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revokeType", obj6);
        sb.append("}");
        return sb.toString();
    }
}
