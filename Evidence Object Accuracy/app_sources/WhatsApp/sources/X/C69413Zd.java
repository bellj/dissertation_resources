package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/* renamed from: X.3Zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69413Zd implements AnonymousClass5WE {
    public final /* synthetic */ AnonymousClass024 A00;
    public final /* synthetic */ AnonymousClass1CS A01;

    public C69413Zd(AnonymousClass024 r1, AnonymousClass1CS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WE
    public void AUJ(AbstractC14640lm r1, String str, int i, long j) {
    }

    @Override // X.AnonymousClass5WE
    public void AUK(C41831uE r4, long j) {
        Bitmap decodeByteArray;
        byte[] bArr = r4.A00;
        if (bArr != null && (decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length)) != null) {
            this.A00.accept(decodeByteArray);
        }
    }
}
