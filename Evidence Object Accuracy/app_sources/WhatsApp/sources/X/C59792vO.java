package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59792vO extends AbstractC37191le {
    public final View A00;
    public final WaTextView A01;

    public C59792vO(View view) {
        super(view);
        this.A00 = view;
        this.A01 = (WaTextView) C16700pc.A02(view, R.id.update_location_button);
    }
}
