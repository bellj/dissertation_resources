package X;

import android.util.Pair;

/* renamed from: X.1sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41151sz {
    public static int A00(AnonymousClass1V8 r0) {
        Pair A01 = A01(r0);
        if (A01 != null) {
            return ((Number) A01.first).intValue();
        }
        return 0;
    }

    public static Pair A01(AnonymousClass1V8 r4) {
        for (AnonymousClass1V8 r3 : r4.A0J("error")) {
            if (r3 != null) {
                String A0I = r3.A0I("code", null);
                String A0I2 = r3.A0I("text", null);
                if (A0I != null) {
                    return new Pair(Integer.valueOf(C28421Nd.A00(A0I, 0)), A0I2);
                }
            }
        }
        return null;
    }
}
