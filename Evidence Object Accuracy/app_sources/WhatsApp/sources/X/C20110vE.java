package X;

import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.0vE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20110vE {
    public final C14850m9 A00;
    public final C19940uv A01;
    public final C18800t4 A02;
    public final C19930uu A03;

    public C20110vE(C14850m9 r1, C19940uv r2, C18800t4 r3, C19930uu r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    public AbstractC37631mk A00(C28481Nj r15, URL url, long j, long j2) {
        boolean A07;
        AnonymousClass1NW r8;
        boolean A072 = this.A00.A07(72);
        C18800t4 r5 = this.A02;
        String A00 = this.A03.A00();
        C19940uv r1 = this.A01;
        boolean A01 = r1.A01();
        if (!r1.A01()) {
            A07 = false;
        } else {
            A07 = r1.A03.A07(58);
        }
        try {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            boolean z = false;
            if (A01) {
                AnonymousClass1NX A012 = r5.A01(false);
                r8 = A012;
                if (A07) {
                    httpsURLConnection.setHostnameVerifier(new C37591mg(r15.A05, HttpsURLConnection.getDefaultHostnameVerifier()));
                    r8 = A012;
                }
            } else {
                r8 = r5.A02();
            }
            int ABY = r8.ABY();
            httpsURLConnection.setSSLSocketFactory(r8);
            httpsURLConnection.setConnectTimeout(15000);
            httpsURLConnection.setReadTimeout(30000);
            httpsURLConnection.setRequestProperty("User-Agent", A00);
            httpsURLConnection.setRequestProperty("Accept-Encoding", "identity");
            httpsURLConnection.setRequestProperty("Host", r15.A05);
            if (!(j == 0 && j2 == -1)) {
                StringBuilder sb = new StringBuilder("bytes=");
                sb.append(j);
                sb.append("-");
                String obj = sb.toString();
                if (j2 != -1) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(obj);
                    sb2.append(j2);
                    obj = sb2.toString();
                }
                httpsURLConnection.setRequestProperty("Range", obj);
            }
            if (A072) {
                httpsURLConnection.setRequestProperty("X-FB-Socket-Option", "TCP_CONGESTION=bbr");
            }
            try {
                int responseCode = httpsURLConnection.getResponseCode();
                if (r8.ABY() == ABY) {
                    z = true;
                }
                if (!(responseCode == 200 || responseCode == 206)) {
                    String str = null;
                    if (httpsURLConnection.getErrorStream() != null) {
                        try {
                            InputStream errorStream = httpsURLConnection.getErrorStream();
                            C37601mh r52 = new C37601mh(errorStream, 1024);
                            str = AnonymousClass1P1.A00(r52);
                            r52.close();
                            if (errorStream != null) {
                                errorStream.close();
                            }
                        } catch (IOException e) {
                            Log.e("MediaDownloadConnection/download/can't get string from error stream", e);
                        }
                    }
                    StringBuilder sb3 = new StringBuilder("MediaDownloadConnection/download failed; url=");
                    sb3.append(C37611mi.A01(url));
                    sb3.append(" responseCode=");
                    sb3.append(responseCode);
                    sb3.append(" responseBody=");
                    sb3.append(str);
                    Log.w(sb3.toString());
                    if (responseCode == 416) {
                        String headerField = httpsURLConnection.getHeaderField("Content-Range");
                        if (TextUtils.isEmpty(headerField) || !headerField.contains("*/")) {
                            throw new C37641ml(responseCode, str);
                        }
                    } else {
                        throw new C37641ml(responseCode, str);
                    }
                }
                Pair pair = new Pair(httpsURLConnection, Boolean.valueOf(z));
                return new C37621mj((Boolean) pair.second, (HttpURLConnection) pair.first);
            } catch (IOException e2) {
                Log.w("MediaDownloadConnection/exception while getting response code", e2);
                throw new C37661mn("failed with IOException while retrieving response", e2);
            } catch (IllegalArgumentException e3) {
                throw new C37661mn("failed with IllegalArgumentException while retrieving response", e3);
            }
        } catch (IOException e4) {
            throw new C37661mn("failed to open http url connection", e4);
        }
    }

    public final AbstractC37631mk A01(String str, String str2, URL url) {
        AnonymousClass1NW r3;
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
        C19940uv r4 = this.A01;
        boolean A01 = r4.A01();
        boolean z = false;
        C18800t4 r0 = this.A02;
        if (A01) {
            AnonymousClass1NX A012 = r0.A01(false);
            r3 = A012;
            if (r4.A01()) {
                r3 = A012;
                if (r4.A03.A07(58)) {
                    httpsURLConnection.setHostnameVerifier(new C37591mg(str, HttpsURLConnection.getDefaultHostnameVerifier()));
                    r3 = A012;
                }
            }
        } else {
            r3 = r0.A02();
        }
        int ABY = r3.ABY();
        httpsURLConnection.setSSLSocketFactory(r3);
        httpsURLConnection.setConnectTimeout(15000);
        httpsURLConnection.setReadTimeout(30000);
        httpsURLConnection.setRequestMethod(str2);
        httpsURLConnection.setRequestProperty("Host", str);
        httpsURLConnection.setRequestProperty("User-Agent", this.A03.A00());
        try {
            httpsURLConnection.connect();
            if (r3.ABY() == ABY) {
                z = true;
            }
            return new C37621mj(Boolean.valueOf(z), httpsURLConnection);
        } catch (IllegalArgumentException e) {
            throw new IOException(e);
        }
    }
}
