package X;

import android.text.TextUtils;

/* renamed from: X.1Xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30411Xh extends AbstractC15340mz implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public String A00;
    public String A01;
    public C30731Yp A02;

    public C30411Xh(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 4, j);
        super.A02 = 0;
    }

    public C30411Xh(AnonymousClass1IS r8, C30411Xh r9, long j, boolean z) {
        super(r9, r8, j, z);
        super.A02 = 0;
        this.A00 = r9.A00;
        this.A01 = r9.A14();
    }

    @Override // X.AbstractC15340mz
    public void A0l(String str) {
        synchronized (this.A10) {
            super.A0l(str);
            this.A01 = str;
        }
    }

    public String A14() {
        String str;
        synchronized (this.A10) {
            str = this.A01;
        }
        return str;
    }

    public C30731Yp A15(C14650lo r5, C15550nR r6, C16590pI r7, AnonymousClass018 r8) {
        C30731Yp r1;
        Object obj = this.A10;
        synchronized (obj) {
            String str = this.A01;
            C30731Yp r12 = this.A02;
            if (r12 != null || str == null) {
                return r12;
            }
            C30721Yo A05 = C30721Yo.A05(r5, r6, r7, r8, str);
            synchronized (obj) {
                r1 = A05 != null ? new C30731Yp(str, A05) : null;
                this.A02 = r1;
            }
            return r1;
        }
    }

    public void A16(String str) {
        synchronized (this.A10) {
            this.A01 = str;
            this.A02 = null;
            super.A0l(str);
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r11) {
        AnonymousClass1G3 r3 = r11.A03;
        C57292mn r0 = ((C27081Fy) r3.A00).A08;
        if (r0 == null) {
            r0 = C57292mn.A04;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        if (!TextUtils.isEmpty(this.A00)) {
            String str = this.A00;
            A0T.A03();
            C57292mn r1 = (C57292mn) A0T.A00;
            r1.A00 |= 1;
            r1.A02 = str;
        }
        String A14 = A14();
        if (!TextUtils.isEmpty(A14)) {
            A0T.A03();
            C57292mn r12 = (C57292mn) A0T.A00;
            r12.A00 |= 2;
            r12.A03 = A14;
        }
        AnonymousClass1PG r6 = r11.A04;
        byte[] bArr = r11.A09;
        if (C32411c7.A0U(r6, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r11.A00, r11.A02, r6, this, bArr, r11.A06);
            A0T.A03();
            C57292mn r13 = (C57292mn) A0T.A00;
            r13.A01 = A0P;
            r13.A00 |= 4;
        }
        r3.A03();
        C27081Fy r14 = (C27081Fy) r3.A00;
        r14.A08 = (C57292mn) A0T.A02();
        r14.A00 |= 8;
    }

    @Override // X.AbstractC16410oy
    public /* bridge */ /* synthetic */ AbstractC15340mz A7L(AnonymousClass1IS r7, long j) {
        return new C30411Xh(r7, this, j, false);
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r7) {
        return new C30411Xh(r7, this, this.A0I, true);
    }
}
