package X;

import java.security.cert.CRLException;

/* renamed from: X.5ON  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5ON extends AbstractC113435Ho {
    public AnonymousClass5OM A00;
    public final Object A01;
    public volatile int A02;
    public volatile boolean A03;

    public final AnonymousClass5OM A04() {
        byte[] bArr;
        AnonymousClass5OM r0;
        Object obj = this.A01;
        synchronized (obj) {
            AnonymousClass5OM r02 = this.A00;
            if (r02 != null) {
                return r02;
            }
            try {
                bArr = getEncoded();
            } catch (CRLException unused) {
                bArr = null;
            }
            AnonymousClass5S2 r5 = super.A02;
            AnonymousClass5OM r2 = new AnonymousClass5OM(super.A00, super.A01, r5, this.A04, bArr, super.A03);
            synchronized (obj) {
                r0 = this.A00;
                if (r0 == null) {
                    this.A00 = r2;
                    r0 = r2;
                }
            }
            return r0;
        }
    }

    @Override // java.security.cert.X509CRL, java.lang.Object
    public int hashCode() {
        if (!this.A03) {
            this.A02 = A04().hashCode();
            this.A03 = true;
        }
        return this.A02;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5ON(X.AnonymousClass5MU r9, X.AnonymousClass5S2 r10) {
        /*
            r8 = this;
            r4 = r9
            X.5Mv r0 = r9.A02     // Catch: Exception -> 0x0055
            java.lang.String r3 = X.C95444dj.A01(r0)     // Catch: Exception -> 0x0055
            X.1TN r0 = r0.A00     // Catch: Exception -> 0x0048
            if (r0 != 0) goto L_0x000d
            r6 = 0
            goto L_0x0011
        L_0x000d:
            byte[] r6 = X.C72463ee.A0c(r0)     // Catch: Exception -> 0x0048
        L_0x0011:
            X.1TK r0 = X.C114715Mu.A0K     // Catch: Exception -> 0x0041
            java.lang.String r2 = r0.A01     // Catch: Exception -> 0x0041
            X.5MN r0 = r9.A03     // Catch: Exception -> 0x0041
            X.5MX r1 = r0.A04     // Catch: Exception -> 0x0041
            if (r1 == 0) goto L_0x0034
            X.1TK r0 = X.C72453ed.A12(r2)     // Catch: Exception -> 0x0041
            X.5Mu r0 = X.AnonymousClass5MX.A00(r0, r1)     // Catch: Exception -> 0x0041
            if (r0 == 0) goto L_0x0034
            X.5NH r0 = r0.A01     // Catch: Exception -> 0x0041
            if (r0 == 0) goto L_0x0034
            byte[] r0 = r0.A00     // Catch: Exception -> 0x0041
            if (r0 == 0) goto L_0x0034
            X.5Ms r0 = X.C114695Ms.A00(r0)     // Catch: Exception -> 0x0041
            boolean r7 = r0.A03     // Catch: Exception -> 0x0041
            goto L_0x0035
        L_0x0034:
            r7 = 0
        L_0x0035:
            r2 = r8
            r5 = r10
            r2.<init>(r3, r4, r5, r6, r7)
            java.lang.Object r0 = X.C12970iu.A0l()
            r8.A01 = r0
            return
        L_0x0041:
            r1 = move-exception
            X.5Ha r0 = new X.5Ha
            r0.<init>(r1)
            throw r0
        L_0x0048:
            r1 = move-exception
            java.lang.String r0 = "CRL contents invalid: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CRLException r0 = new java.security.cert.CRLException
            r0.<init>(r1)
            throw r0
        L_0x0055:
            r1 = move-exception
            java.lang.String r0 = "CRL contents invalid: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CRLException r0 = new java.security.cert.CRLException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5ON.<init>(X.5MU, X.5S2):void");
    }

    @Override // java.security.cert.X509CRL, java.lang.Object
    public boolean equals(Object obj) {
        AnonymousClass5MA r1;
        if (this == obj) {
            return true;
        }
        if (obj instanceof AnonymousClass5ON) {
            AnonymousClass5ON r3 = (AnonymousClass5ON) obj;
            if (!this.A03 || !r3.A03) {
                if ((this.A00 == null || r3.A00 == null) && (r1 = super.A01.A01) != null && !r1.A04(((AbstractC113435Ho) r3).A01.A01)) {
                    return false;
                }
            } else if (this.A02 != r3.A02) {
                return false;
            }
        }
        return A04().equals(obj);
    }
}
