package X;

import java.util.concurrent.Callable;

/* renamed from: X.5Dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractCallableC112595Dz implements Callable {
    public final AnonymousClass02N A00 = new AnonymousClass02N();

    public void A00() {
        this.A00.A01();
    }

    public Object A01() {
        AnonymousClass42Q r2 = (AnonymousClass42Q) this;
        return r2.A01.A0D.A03(r2.A00);
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        AnonymousClass02N r2 = this.A00;
        if (!r2.A04()) {
            Object A01 = A01();
            if (!r2.A04()) {
                return A01;
            }
        }
        throw new AnonymousClass04U(null);
    }
}
