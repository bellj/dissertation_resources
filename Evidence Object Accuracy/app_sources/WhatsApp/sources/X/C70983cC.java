package X;

import java.util.Collections;

/* renamed from: X.3cC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70983cC implements AnonymousClass5WN {
    public final AnonymousClass016 A00;
    public final C64173En A01;

    public C70983cC(AnonymousClass016 r1, C64173En r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r5) {
        C64173En r3 = this.A01;
        if (r3 != null) {
            C28701Oq.A02(C65093Ic.A00().A00, r5, C14220l3.A01, r3, Collections.emptyMap());
        }
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r2) {
        this.A00.A0A(r2);
    }
}
