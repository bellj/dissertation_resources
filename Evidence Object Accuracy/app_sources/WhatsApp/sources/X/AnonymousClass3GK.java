package X;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.3GK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GK {
    public static void A00(Context context, int i) {
        float f;
        StringBuilder A0k = C12960it.A0k("android.resource://");
        A0k.append(context.getPackageName());
        A0k.append("/");
        Uri parse = Uri.parse(C12960it.A0f(A0k, R.raw.ptt_end_fast));
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(C98074i6.A00);
        mediaPlayer.setAudioStreamType(i);
        try {
            if (i == 3) {
                f = 0.35f;
            } else {
                if (i == 0) {
                    f = 0.2f;
                }
                mediaPlayer.setDataSource(context, parse);
                mediaPlayer.prepare();
                mediaPlayer.start();
                return;
            }
            mediaPlayer.setDataSource(context, parse);
            mediaPlayer.prepare();
            mediaPlayer.start();
            return;
        } catch (IOException e) {
            Log.e("SequentialVoiceMemoPlayer/playEndTone ", e);
            return;
        }
        mediaPlayer.setVolume(f, f);
    }

    public static void A01(Context context, C14900mE r4, Runnable runnable, int i) {
        float f;
        StringBuilder A0k = C12960it.A0k("android.resource://");
        A0k.append(context.getPackageName());
        A0k.append("/");
        Uri parse = Uri.parse(C12960it.A0f(A0k, R.raw.ptt_middle_fast));
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(C98074i6.A00);
        mediaPlayer.setAudioStreamType(i);
        try {
            if (i == 3) {
                f = 0.35f;
            } else {
                if (i == 0) {
                    f = 0.2f;
                }
                mediaPlayer.setDataSource(context, parse);
                mediaPlayer.prepare();
                mediaPlayer.start();
                r4.A0J(runnable, 600);
                return;
            }
            mediaPlayer.setDataSource(context, parse);
            mediaPlayer.prepare();
            mediaPlayer.start();
            r4.A0J(runnable, 600);
            return;
        } catch (IOException e) {
            Log.e("SequentialVoiceMemoPlayer/playMiddleTone ", e);
            return;
        }
        mediaPlayer.setVolume(f, f);
    }
}
