package X;

import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import com.whatsapp.Conversation;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.2zL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61062zL extends AbstractC60952zA {
    @Override // X.AbstractC60952zA
    public void A06(Activity activity, AbstractC17160qM r19, AnonymousClass018 r20, AnonymousClass1Z8 r21, C16660pY r22, String str, long j) {
        C65963Lt r11;
        long j2;
        super.A06(activity, r19, r20, r21, r22, str, j);
        Conversation conversation = (Conversation) AbstractC35731ia.A01(activity, Conversation.class);
        C91284Rd r4 = (C91284Rd) ((Map) r22.A01.getValue()).get("galaxy_message");
        if (r4 == null || r4.A03) {
            String str2 = r21.A01;
            Map A01 = C65053Hy.A01(str2);
            if (conversation != null && A01.containsKey("flow_id") && A01.containsKey("flow_version_id") && A01.containsKey("flow_data_endpoint") && A01.containsKey("flow_token") && A01.containsKey("flow_cta")) {
                try {
                    JSONObject A05 = C13000ix.A05(str2);
                    String A0t = C12970iu.A0t("flow_version_id", A01);
                    String A0t2 = C12970iu.A0t("flow_data_endpoint", A01);
                    String obj = A05.toString();
                    if (r4 != null) {
                        A0t = C12960it.A0d(r4.A02, C12960it.A0j(A0t));
                        j2 = r4.A00 * 1000;
                        if (j2 == 0) {
                            r11 = null;
                            Intent A0A = C12970iu.A0A();
                            A0A.setClassName(conversation.getPackageName(), "com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity");
                            A0A.putExtra("screen_name", "com.bloks.www.whatsapp.commerce.galaxy_message");
                            A0A.putExtra("screen_params", obj);
                            A0A.putExtra("screen_cache_config", r11);
                            A0A.putExtra("chat_id", C15380n4.A03(conversation.A2c.A0B(AbstractC14640lm.class)));
                            A0A.putExtra("message_id", str);
                            A0A.putExtra("action_name", "galaxy_message");
                            A0A.putExtra("message_row_id", j);
                            A0A.putExtra("user_locale", r20.A07());
                            A0A.putExtra("flow_data_endpoint", A0t2);
                            A0A.putExtra("flow_token", C12970iu.A0t("flow_token", A01));
                            KeyGenerator instance = KeyGenerator.getInstance("AES");
                            instance.init(128);
                            SecretKey generateKey = instance.generateKey();
                            byte[] bArr = new byte[16];
                            C002901h.A00().nextBytes(bArr);
                            ArrayList A0l = C12960it.A0l();
                            A0l.add(Base64.encodeToString(generateKey.getEncoded(), 2));
                            A0l.add(Base64.encodeToString(bArr, 2));
                            A0A.putExtra("aes_key", (String) A0l.get(0));
                            A0A.putExtra("initial_vector", (String) A0l.get(1));
                            activity.startActivity(A0A);
                        }
                    } else {
                        j2 = 3600000;
                    }
                    StringBuilder A0j = C12960it.A0j(A0t);
                    A0j.append(":");
                    r11 = new C65963Lt(C12960it.A0d(r20.A07(), A0j), j2, true);
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(conversation.getPackageName(), "com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity");
                    A0A.putExtra("screen_name", "com.bloks.www.whatsapp.commerce.galaxy_message");
                    A0A.putExtra("screen_params", obj);
                    A0A.putExtra("screen_cache_config", r11);
                    A0A.putExtra("chat_id", C15380n4.A03(conversation.A2c.A0B(AbstractC14640lm.class)));
                    A0A.putExtra("message_id", str);
                    A0A.putExtra("action_name", "galaxy_message");
                    A0A.putExtra("message_row_id", j);
                    A0A.putExtra("user_locale", r20.A07());
                    A0A.putExtra("flow_data_endpoint", A0t2);
                    A0A.putExtra("flow_token", C12970iu.A0t("flow_token", A01));
                    KeyGenerator instance = KeyGenerator.getInstance("AES");
                    instance.init(128);
                    SecretKey generateKey = instance.generateKey();
                    byte[] bArr = new byte[16];
                    C002901h.A00().nextBytes(bArr);
                    ArrayList A0l = C12960it.A0l();
                    A0l.add(Base64.encodeToString(generateKey.getEncoded(), 2));
                    A0l.add(Base64.encodeToString(bArr, 2));
                    A0A.putExtra("aes_key", (String) A0l.get(0));
                    A0A.putExtra("initial_vector", (String) A0l.get(1));
                    activity.startActivity(A0A);
                } catch (NoSuchAlgorithmException | JSONException e) {
                    e.getMessage();
                }
            }
        }
    }
}
