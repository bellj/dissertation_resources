package X;

/* renamed from: X.0kR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13850kR extends ActivityC000800j implements AnonymousClass004 {
    public boolean A00 = false;
    public final Object A01 = new Object();
    public volatile AnonymousClass2FH A02;

    public AbstractActivityC13850kR() {
        A1U();
    }

    private void A1U() {
        A0R(new C48562Gt(this));
    }

    /* renamed from: A1i */
    public final AnonymousClass2FH A1l() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    this.A02 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A02;
    }

    public void A1j() {
        new AnonymousClass2FH(this);
    }

    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            generatedComponent();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        return A1l().generatedComponent();
    }
}
