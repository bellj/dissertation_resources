package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

/* renamed from: X.2Ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48102Ei extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewPropertyAnimator A01;
    public final /* synthetic */ AnonymousClass03U A02;
    public final /* synthetic */ C48042Ec A03;

    public C48102Ei(View view, ViewPropertyAnimator viewPropertyAnimator, AnonymousClass03U r3, C48042Ec r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = view;
        this.A01 = viewPropertyAnimator;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        View view = this.A00;
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setListener(null);
        C48042Ec r2 = this.A03;
        AnonymousClass03U r1 = this.A02;
        r2.A03(r1);
        r2.A01.remove(r1);
        r2.A0H();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A03.A0C = true;
    }
}
