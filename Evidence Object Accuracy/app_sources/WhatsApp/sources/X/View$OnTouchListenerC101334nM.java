package X;

import android.view.MotionEvent;
import android.view.View;
import com.whatsapp.calling.views.VoipCallControlBottomSheetDragIndicator;

/* renamed from: X.4nM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnTouchListenerC101334nM implements View.OnTouchListener {
    public final /* synthetic */ VoipCallControlBottomSheetDragIndicator A00;

    public View$OnTouchListenerC101334nM(VoipCallControlBottomSheetDragIndicator voipCallControlBottomSheetDragIndicator) {
        this.A00 = voipCallControlBottomSheetDragIndicator;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.A00.A02(motionEvent.getAction());
        return true;
    }
}
