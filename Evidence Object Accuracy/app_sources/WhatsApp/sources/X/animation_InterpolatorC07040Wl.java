package X;

import android.view.animation.Interpolator;

/* renamed from: X.0Wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class animation.InterpolatorC07040Wl implements Interpolator {
    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        return f * f * f * f * f;
    }
}
