package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0CM  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0CM extends AnonymousClass0XP implements AbstractC12690iL, PopupWindow.OnDismissListener, View.OnKeyListener {
    public int A00 = 0;
    public int A01;
    public int A02 = 0;
    public int A03;
    public int A04;
    public View A05;
    public View A06;
    public ViewTreeObserver A07;
    public PopupWindow.OnDismissListener A08;
    public AbstractC12280hf A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public final int A0F;
    public final int A0G;
    public final int A0H;
    public final Context A0I;
    public final Handler A0J;
    public final View.OnAttachStateChangeListener A0K = new AnonymousClass0W5(this);
    public final ViewTreeObserver.OnGlobalLayoutListener A0L = new AnonymousClass0WY(this);
    public final AbstractC12310hi A0M = new AnonymousClass0XT(this);
    public final List A0N = new ArrayList();
    public final List A0O = new ArrayList();
    public final boolean A0P;

    @Override // X.AnonymousClass0XP
    public boolean A09() {
        return false;
    }

    @Override // X.AbstractC12690iL
    public boolean AA1() {
        return false;
    }

    public AnonymousClass0CM(Context context, View view, int i, int i2, boolean z) {
        this.A0I = context;
        this.A05 = view;
        this.A0G = i;
        this.A0H = i2;
        this.A0P = z;
        this.A0A = false;
        this.A01 = AnonymousClass028.A05(view) == 1 ? 0 : 1;
        Resources resources = context.getResources();
        this.A0F = Math.max(resources.getDisplayMetrics().widthPixels >> 1, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.A0J = new Handler();
    }

    @Override // X.AnonymousClass0XP
    public void A01(int i) {
        if (this.A02 != i) {
            this.A02 = i;
            this.A00 = C05660Ql.A00(i, AnonymousClass028.A05(this.A05));
        }
    }

    @Override // X.AnonymousClass0XP
    public void A02(int i) {
        this.A0B = true;
        this.A03 = i;
    }

    @Override // X.AnonymousClass0XP
    public void A03(int i) {
        this.A0C = true;
        this.A04 = i;
    }

    @Override // X.AnonymousClass0XP
    public void A04(View view) {
        if (this.A05 != view) {
            this.A05 = view;
            this.A00 = C05660Ql.A00(this.A02, AnonymousClass028.A05(view));
        }
    }

    @Override // X.AnonymousClass0XP
    public void A05(PopupWindow.OnDismissListener onDismissListener) {
        this.A08 = onDismissListener;
    }

    @Override // X.AnonymousClass0XP
    public void A06(AnonymousClass07H r2) {
        r2.A08(this.A0I, this);
        if (AK4()) {
            A0A(r2);
        } else {
            this.A0N.add(r2);
        }
    }

    @Override // X.AnonymousClass0XP
    public void A07(boolean z) {
        this.A0A = z;
    }

    @Override // X.AnonymousClass0XP
    public void A08(boolean z) {
        this.A0E = z;
    }

    public final void A0A(AnonymousClass07H r18) {
        AnonymousClass0NH r2;
        View view;
        int i;
        boolean z;
        int i2;
        int i3;
        int i4;
        AnonymousClass0BQ r14;
        int i5;
        int firstVisiblePosition;
        Context context = this.A0I;
        LayoutInflater from = LayoutInflater.from(context);
        AnonymousClass0BQ r10 = new AnonymousClass0BQ(from, r18, R.layout.abc_cascading_menu_item_layout, this.A0P);
        if (!AK4() && this.A0A) {
            r10.A02 = true;
        } else if (AK4()) {
            int size = r18.size();
            boolean z2 = false;
            int i6 = 0;
            while (true) {
                if (i6 >= size) {
                    break;
                }
                MenuItem item = r18.getItem(i6);
                if (item.isVisible() && item.getIcon() != null) {
                    z2 = true;
                    break;
                }
                i6++;
            }
            r10.A02 = z2;
        }
        int A00 = AnonymousClass0XP.A00(context, r10, this.A0F);
        C02410Ce r4 = new C02410Ce(context, this.A0G, this.A0H);
        r4.A00 = this.A0M;
        r4.A0B = this;
        PopupWindow popupWindow = r4.A0D;
        popupWindow.setOnDismissListener(this);
        r4.A0A = this.A05;
        ((AnonymousClass0XR) r4).A00 = this.A00;
        r4.A0H = true;
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(2);
        r4.Abi(r10);
        r4.A01(A00);
        ((AnonymousClass0XR) r4).A00 = this.A00;
        List list = this.A0O;
        if (list.size() > 0) {
            r2 = (AnonymousClass0NH) list.get(list.size() - 1);
            AnonymousClass07H r12 = r2.A01;
            int size2 = r12.size();
            int i7 = 0;
            while (true) {
                if (i7 >= size2) {
                    break;
                }
                MenuItem item2 = r12.getItem(i7);
                if (!item2.hasSubMenu() || r18 != item2.getSubMenu()) {
                    i7++;
                } else {
                    C02360Bs r13 = r2.A02.A0E;
                    ListAdapter adapter = r13.getAdapter();
                    int i8 = 0;
                    if (adapter instanceof HeaderViewListAdapter) {
                        HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                        i5 = headerViewListAdapter.getHeadersCount();
                        r14 = (AnonymousClass0BQ) headerViewListAdapter.getWrappedAdapter();
                    } else {
                        r14 = (AnonymousClass0BQ) adapter;
                        i5 = 0;
                    }
                    int count = r14.getCount();
                    while (true) {
                        if (i8 >= count) {
                            break;
                        } else if (item2 != r14.getItem(i8)) {
                            i8++;
                        } else if (i8 != -1 && (firstVisiblePosition = (i8 + i5) - r13.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < r13.getChildCount()) {
                            view = r13.getChildAt(firstVisiblePosition);
                        }
                    }
                }
            }
        } else {
            r2 = null;
        }
        view = null;
        if (view != null) {
            r4.A04();
            r4.A02();
            C02360Bs r142 = ((AnonymousClass0NH) list.get(list.size() - 1)).A02.A0E;
            int[] iArr = new int[2];
            r142.getLocationOnScreen(iArr);
            Rect rect = new Rect();
            this.A06.getWindowVisibleDisplayFrame(rect);
            if (this.A01 != 1 ? iArr[0] - A00 >= 0 : iArr[0] + r142.getWidth() + A00 > rect.right) {
                i = 0;
                z = false;
            } else {
                i = 1;
                z = true;
            }
            this.A01 = i;
            if (Build.VERSION.SDK_INT >= 26) {
                r4.A0A = view;
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr2 = new int[2];
                this.A05.getLocationOnScreen(iArr2);
                int[] iArr3 = new int[2];
                view.getLocationOnScreen(iArr3);
                if ((this.A00 & 7) == 5) {
                    iArr2[0] = iArr2[0] + this.A05.getWidth();
                    iArr3[0] = iArr3[0] + view.getWidth();
                }
                i2 = iArr3[0] - iArr2[0];
                i3 = iArr3[1] - iArr2[1];
            }
            if ((this.A00 & 5) == 5) {
                if (!z) {
                    A00 = view.getWidth();
                    i4 = i2 - A00;
                }
                i4 = i2 + A00;
            } else {
                if (z) {
                    A00 = view.getWidth();
                    i4 = i2 + A00;
                }
                i4 = i2 - A00;
            }
            r4.A02 = i4;
            r4.A0J = true;
            r4.A0I = true;
            r4.Ad7(i3);
        } else {
            if (this.A0B) {
                r4.A02 = this.A03;
            }
            if (this.A0C) {
                r4.Ad7(this.A04);
            }
            Rect rect2 = super.A00;
            r4.A09 = rect2 != null ? new Rect(rect2) : null;
        }
        list.add(new AnonymousClass0NH(r18, r4, this.A01));
        r4.Ade();
        C02360Bs r3 = r4.A0E;
        r3.setOnKeyListener(this);
        if (r2 == null && this.A0E && r18.A05 != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(R.layout.abc_popup_menu_header_item_layout, (ViewGroup) r3, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(r18.A05);
            r3.addHeaderView(frameLayout, null, false);
            r4.Ade();
        }
    }

    @Override // X.AbstractC12600iB
    public ListView ADv() {
        List list = this.A0O;
        if (list.isEmpty()) {
            return null;
        }
        return ((AnonymousClass0NH) list.get(list.size() - 1)).A02.A0E;
    }

    @Override // X.AbstractC12600iB
    public boolean AK4() {
        List list = this.A0O;
        if (list.size() <= 0 || !((AnonymousClass0NH) list.get(0)).A02.A0D.isShowing()) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC12690iL
    public void AOF(AnonymousClass07H r7, boolean z) {
        int i;
        List list = this.A0O;
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (r7 == ((AnonymousClass0NH) list.get(i2)).A01) {
                if (i2 >= 0) {
                    int i3 = i2 + 1;
                    if (i3 < list.size()) {
                        ((AnonymousClass0NH) list.get(i3)).A01.A0F(false);
                    }
                    AnonymousClass0NH r1 = (AnonymousClass0NH) list.remove(i2);
                    r1.A01.A0D(this);
                    if (this.A0D) {
                        C02410Ce r0 = r1.A02;
                        r0.A03();
                        r0.A0D.setAnimationStyle(0);
                    }
                    r1.A02.dismiss();
                    int size2 = list.size();
                    if (size2 > 0) {
                        i = ((AnonymousClass0NH) list.get(size2 - 1)).A00;
                    } else {
                        i = 1;
                        if (AnonymousClass028.A05(this.A05) == 1) {
                            i = 0;
                        }
                    }
                    this.A01 = i;
                    if (size2 == 0) {
                        dismiss();
                        AbstractC12280hf r12 = this.A09;
                        if (r12 != null) {
                            r12.AOF(r7, true);
                        }
                        ViewTreeObserver viewTreeObserver = this.A07;
                        if (viewTreeObserver != null) {
                            if (viewTreeObserver.isAlive()) {
                                this.A07.removeGlobalOnLayoutListener(this.A0L);
                            }
                            this.A07 = null;
                        }
                        this.A06.removeOnAttachStateChangeListener(this.A0K);
                        this.A08.onDismiss();
                        return;
                    } else if (z) {
                        ((AnonymousClass0NH) list.get(0)).A01.A0F(false);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC12690iL
    public boolean AWs(AnonymousClass0CK r5) {
        Iterator it = this.A0O.iterator();
        while (true) {
            if (it.hasNext()) {
                AnonymousClass0NH r1 = (AnonymousClass0NH) it.next();
                if (r5 == r1.A01) {
                    r1.A02.A0E.requestFocus();
                    break;
                }
            } else if (!r5.hasVisibleItems()) {
                return false;
            } else {
                A06(r5);
                AbstractC12280hf r0 = this.A09;
                if (r0 != null) {
                    r0.ATF(r5);
                    return true;
                }
            }
        }
        return true;
    }

    @Override // X.AbstractC12690iL
    public void Abr(AbstractC12280hf r1) {
        this.A09 = r1;
    }

    @Override // X.AbstractC12600iB
    public void Ade() {
        if (!AK4()) {
            List<AnonymousClass07H> list = this.A0N;
            for (AnonymousClass07H r0 : list) {
                A0A(r0);
            }
            list.clear();
            View view = this.A05;
            this.A06 = view;
            if (view != null) {
                boolean z = false;
                if (this.A07 == null) {
                    z = true;
                }
                ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
                this.A07 = viewTreeObserver;
                if (z) {
                    viewTreeObserver.addOnGlobalLayoutListener(this.A0L);
                }
                this.A06.addOnAttachStateChangeListener(this.A0K);
            }
        }
    }

    @Override // X.AbstractC12690iL
    public void AfQ(boolean z) {
        for (AnonymousClass0NH r0 : this.A0O) {
            ListAdapter adapter = r0.A02.A0E.getAdapter();
            if (adapter instanceof HeaderViewListAdapter) {
                adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            }
            ((AnonymousClass0BQ) adapter).notifyDataSetChanged();
        }
    }

    @Override // X.AbstractC12600iB
    public void dismiss() {
        List list = this.A0O;
        int size = list.size();
        if (size > 0) {
            AnonymousClass0NH[] r2 = (AnonymousClass0NH[]) list.toArray(new AnonymousClass0NH[size]);
            while (true) {
                size--;
                if (size >= 0) {
                    AnonymousClass0NH r1 = r2[size];
                    if (r1.A02.A0D.isShowing()) {
                        r1.A02.dismiss();
                    }
                } else {
                    return;
                }
            }
        }
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        List list = this.A0O;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass0NH r1 = (AnonymousClass0NH) list.get(i);
            if (!r1.A02.A0D.isShowing()) {
                r1.A01.A0F(false);
                return;
            }
        }
    }

    @Override // android.view.View.OnKeyListener
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }
}
