package X;

import android.icu.util.ULocale;
import java.util.Locale;

/* renamed from: X.0T3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T3 {
    public static ULocale A00(Object obj) {
        return ULocale.addLikelySubtags((ULocale) obj);
    }

    public static ULocale A01(Locale locale) {
        return ULocale.forLocale(locale);
    }

    public static String A02(Object obj) {
        return ((ULocale) obj).getScript();
    }
}
