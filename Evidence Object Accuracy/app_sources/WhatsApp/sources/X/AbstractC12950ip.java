package X;

import java.io.Closeable;
import java.io.Flushable;
import java.nio.channels.WritableByteChannel;

/* renamed from: X.0ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12950ip extends Closeable, WritableByteChannel, Flushable {
}
