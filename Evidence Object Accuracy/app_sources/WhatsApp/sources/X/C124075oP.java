package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.5oP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124075oP extends AbstractC16350or {
    public final /* synthetic */ C120615gT A00;
    public final /* synthetic */ AnonymousClass1V8 A01;

    public C124075oP(C120615gT r1, AnonymousClass1V8 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AnonymousClass1V8[] r7;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass1V8 A0c = C117305Zk.A0c(this.A01);
        if (!(A0c == null || (r7 = A0c.A03) == null)) {
            HashSet A12 = C12970iu.A12();
            AnonymousClass60I r5 = this.A00.A00;
            Iterator it = C117295Zj.A0Z(r5.A0F).iterator();
            while (it.hasNext()) {
                A12.add(C117305Zk.A0H(it).A0A);
            }
            for (AnonymousClass1V8 r9 : r7) {
                if (r9 != null && "upi".equals(r9.A00)) {
                    C119755f3 r2 = new C119755f3();
                    r2.A01(r5.A08, r9, 3);
                    if (!TextUtils.isEmpty(((AbstractC30851Zb) r2).A06)) {
                        r2.A0H = A12.contains(((AbstractC30851Zb) r2).A06);
                    }
                    A0l.add(r2);
                }
            }
        }
        return A0l;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        ArrayList arrayList = (ArrayList) obj;
        AnonymousClass6MR r1 = this.A00.A00.A02;
        if (r1 != null) {
            r1.AN8(null, arrayList);
        }
    }
}
