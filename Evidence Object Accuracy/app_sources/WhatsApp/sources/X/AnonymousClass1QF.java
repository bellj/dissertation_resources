package X;

import android.os.StrictMode;
import android.util.Log;
import com.facebook.soloader.Api18TraceUtils;
import com.facebook.soloader.SoLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.Arrays;

/* renamed from: X.1QF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QF extends AnonymousClass1QG {
    public final int A00;
    public final File A01;

    public AnonymousClass1QF(File file, int i) {
        this.A01 = file;
        this.A00 = i;
    }

    public static long A00(ByteBuffer byteBuffer, FileChannel fileChannel, long j) {
        A01(byteBuffer, fileChannel, 4, j);
        return ((long) byteBuffer.getInt()) & 4294967295L;
    }

    public static void A01(ByteBuffer byteBuffer, FileChannel fileChannel, int i, long j) {
        int read;
        byteBuffer.position(0);
        byteBuffer.limit(i);
        while (byteBuffer.remaining() > 0 && (read = fileChannel.read(byteBuffer, j)) != -1) {
            j += (long) read;
        }
        if (byteBuffer.remaining() <= 0) {
            byteBuffer.position(0);
            return;
        }
        throw new C113215Gq("ELF file truncated");
    }

    public int A05(StrictMode.ThreadPolicy threadPolicy, File file, String str, int i) {
        int i2;
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7;
        long j8;
        long j9;
        long j10;
        long j11;
        long j12;
        File file2 = new File(file, str);
        boolean exists = file2.exists();
        StringBuilder sb = new StringBuilder();
        if (!exists) {
            sb.append(str);
            sb.append(" not found on ");
            sb.append(file.getCanonicalPath());
            Log.d("SoLoader", sb.toString());
            return 0;
        }
        sb.append(str);
        sb.append(" found on ");
        sb.append(file.getCanonicalPath());
        Log.d("SoLoader", sb.toString());
        if ((i & 1) == 0 || (this.A00 & 2) == 0) {
            if ((this.A00 & 1) != 0) {
                boolean z = SoLoader.A0A;
                if (z) {
                    Api18TraceUtils.A01("SoLoader.getElfDependencies[", file2.getName());
                }
                try {
                    FileInputStream fileInputStream = new FileInputStream(file2);
                    FileChannel channel = fileInputStream.getChannel();
                    ByteBuffer allocate = ByteBuffer.allocate(8);
                    allocate.order(ByteOrder.LITTLE_ENDIAN);
                    if (A00(allocate, channel, 0) == 1179403647) {
                        A01(allocate, channel, 1, 4);
                        boolean z2 = true;
                        if (((short) (allocate.get() & 255)) != 1) {
                            z2 = false;
                        }
                        A01(allocate, channel, 1, 5);
                        if (((short) (allocate.get() & 255)) == 2) {
                            allocate.order(ByteOrder.BIG_ENDIAN);
                        }
                        if (z2) {
                            j2 = A00(allocate, channel, 28);
                            i2 = 2;
                            A01(allocate, channel, 2, 44);
                            j = (long) (allocate.getShort() & 65535);
                            j3 = 42;
                        } else {
                            A01(allocate, channel, 8, 32);
                            j2 = allocate.getLong();
                            i2 = 2;
                            A01(allocate, channel, 2, 56);
                            j = (long) (allocate.getShort() & 65535);
                            j3 = 54;
                        }
                        A01(allocate, channel, i2, j3);
                        int i3 = allocate.getShort() & 65535;
                        if (j == 65535) {
                            if (z2) {
                                j12 = A00(allocate, channel, 32) + 28;
                            } else {
                                A01(allocate, channel, 8, 40);
                                j12 = allocate.getLong() + 44;
                            }
                            j = A00(allocate, channel, j12);
                        }
                        long j13 = j2;
                        long j14 = 0;
                        while (true) {
                            if (j14 >= j) {
                                break;
                            } else if (A00(allocate, channel, j13 + 0) == 2) {
                                if (z2) {
                                    j4 = A00(allocate, channel, j13 + 4);
                                } else {
                                    A01(allocate, channel, 8, j13 + 8);
                                    j4 = allocate.getLong();
                                }
                                if (j4 != 0) {
                                    long j15 = j4;
                                    long j16 = 0;
                                    int i4 = 0;
                                    do {
                                        long j17 = j15 + 0;
                                        if (z2) {
                                            j5 = A00(allocate, channel, j17);
                                        } else {
                                            A01(allocate, channel, 8, j17);
                                            j5 = allocate.getLong();
                                        }
                                        if (j5 == 1) {
                                            if (i4 != Integer.MAX_VALUE) {
                                                i4++;
                                            } else {
                                                throw new C113215Gq("malformed DT_NEEDED section");
                                            }
                                        } else if (j5 == 5) {
                                            if (z2) {
                                                j16 = A00(allocate, channel, j15 + 4);
                                            } else {
                                                A01(allocate, channel, 8, j15 + 8);
                                                j16 = allocate.getLong();
                                            }
                                        }
                                        if (z2) {
                                            j6 = 8;
                                        } else {
                                            j6 = 16;
                                        }
                                        j15 += j6;
                                    } while (j5 != 0);
                                    if (j16 != 0) {
                                        int i5 = 0;
                                        while (true) {
                                            if (((long) i5) >= j) {
                                                break;
                                            }
                                            if (A00(allocate, channel, j2 + 0) == 1) {
                                                if (z2) {
                                                    j7 = A00(allocate, channel, j2 + 8);
                                                    j8 = A00(allocate, channel, j2 + 20);
                                                } else {
                                                    A01(allocate, channel, 8, j2 + 16);
                                                    j7 = allocate.getLong();
                                                    A01(allocate, channel, 8, j2 + 40);
                                                    j8 = allocate.getLong();
                                                }
                                                if (j7 <= j16 && j16 < j8 + j7) {
                                                    if (z2) {
                                                        j9 = A00(allocate, channel, j2 + 4);
                                                    } else {
                                                        A01(allocate, channel, 8, j2 + 8);
                                                        j9 = allocate.getLong();
                                                    }
                                                    long j18 = j9 + (j16 - j7);
                                                    if (j18 != 0) {
                                                        String[] strArr = new String[i4];
                                                        int i6 = 0;
                                                        do {
                                                            long j19 = j4 + 0;
                                                            if (z2) {
                                                                j10 = A00(allocate, channel, j19);
                                                            } else {
                                                                A01(allocate, channel, 8, j19);
                                                                j10 = allocate.getLong();
                                                            }
                                                            if (j10 == 1) {
                                                                if (z2) {
                                                                    j11 = A00(allocate, channel, j4 + 4);
                                                                } else {
                                                                    A01(allocate, channel, 8, j4 + 8);
                                                                    j11 = allocate.getLong();
                                                                }
                                                                long j20 = j11 + j18;
                                                                StringBuilder sb2 = new StringBuilder();
                                                                while (true) {
                                                                    long j21 = 1 + j20;
                                                                    A01(allocate, channel, 1, j20);
                                                                    short s = (short) (allocate.get() & 255);
                                                                    if (s == 0) {
                                                                        break;
                                                                    }
                                                                    sb2.append((char) s);
                                                                    j20 = j21;
                                                                }
                                                                strArr[i6] = sb2.toString();
                                                                if (i6 != Integer.MAX_VALUE) {
                                                                    i6++;
                                                                } else {
                                                                    throw new C113215Gq("malformed DT_NEEDED section");
                                                                }
                                                            }
                                                            j4 += z2 ? 8 : 16;
                                                        } while (j10 != 0);
                                                        if (i6 == i4) {
                                                            fileInputStream.close();
                                                            StringBuilder sb3 = new StringBuilder("Loading lib dependencies: ");
                                                            sb3.append(Arrays.toString(strArr));
                                                            Log.d("SoLoader", sb3.toString());
                                                            for (int i7 = 0; i7 < i4; i7++) {
                                                                String str2 = strArr[i7];
                                                                if (!str2.startsWith("/")) {
                                                                    SoLoader.A03(threadPolicy, str2, null, i | 1);
                                                                }
                                                            }
                                                        } else {
                                                            throw new C113215Gq("malformed DT_NEEDED section");
                                                        }
                                                    }
                                                }
                                            }
                                            j2 += (long) i3;
                                            i5++;
                                        }
                                        throw new C113215Gq("did not find file offset of DT_STRTAB table");
                                    }
                                    throw new C113215Gq("Dynamic section string-table not found");
                                }
                            } else {
                                j13 += (long) i3;
                                j14++;
                            }
                        }
                        throw new C113215Gq("ELF file does not contain dynamic linking information");
                    }
                    throw new C113215Gq("file is not ELF");
                } finally {
                    if (z) {
                        Api18TraceUtils.A00();
                    }
                }
            } else {
                StringBuilder sb4 = new StringBuilder("Not resolving dependencies for ");
                sb4.append(str);
                Log.d("SoLoader", sb4.toString());
            }
            try {
                SoLoader.A02.AKV(file2.getAbsolutePath(), i);
                return 1;
            } catch (UnsatisfiedLinkError e) {
                if (e.getMessage().contains("bad ELF magic")) {
                    Log.d("SoLoader", "Corrupted lib file detected");
                    return 3;
                }
                throw e;
            }
        } else {
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append(" loaded implicitly");
            Log.d("SoLoader", sb5.toString());
            return 2;
        }
    }

    public String toString() {
        String str;
        try {
            str = String.valueOf(this.A01.getCanonicalPath());
        } catch (IOException unused) {
            str = this.A01.getName();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append("[root = ");
        sb.append(str);
        sb.append(" flags = ");
        sb.append(this.A00);
        sb.append(']');
        return sb.toString();
    }
}
