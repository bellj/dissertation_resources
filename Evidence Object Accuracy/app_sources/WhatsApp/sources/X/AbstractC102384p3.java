package X;

import android.os.SystemClock;
import android.view.View;
import android.widget.AdapterView;
import com.whatsapp.status.SetStatus;

/* renamed from: X.4p3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC102384p3 implements AdapterView.OnItemClickListener {
    public long A00;

    public void A00(AdapterView adapterView, View view, int i, long j) {
        if (!(this instanceof AnonymousClass47X)) {
            ((AnonymousClass47Y) this).A00.A00 = i;
            return;
        }
        ((AnonymousClass47X) this).A00.A2g((String) SetStatus.A09.get(i));
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (elapsedRealtime - this.A00 > 1000) {
            this.A00 = elapsedRealtime;
            A00(adapterView, view, i, j);
        }
    }
}
