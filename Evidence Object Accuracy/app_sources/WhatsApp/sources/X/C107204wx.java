package X;

import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.Arrays;
import java.util.Collections;

/* renamed from: X.4wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107204wx implements AnonymousClass5X7 {
    public static final byte[] A0L = {73, 68, 51};
    public int A00 = 0;
    public int A01;
    public int A02 = -1;
    public int A03 = -1;
    public int A04 = 256;
    public int A05;
    public int A06 = 0;
    public long A07;
    public long A08 = -9223372036854775807L;
    public long A09;
    public AnonymousClass5X6 A0A;
    public AnonymousClass5X6 A0B;
    public AnonymousClass5X6 A0C;
    public String A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final C95054d0 A0H = new C95054d0(new byte[7], 7);
    public final C95304dT A0I = new C95304dT(Arrays.copyOf(A0L, 10));
    public final String A0J;
    public final boolean A0K;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107204wx(String str, boolean z) {
        this.A0K = z;
        this.A0J = str;
    }

    @Override // X.AnonymousClass5X7
    public void A7a(C95304dT r18) {
        while (true) {
            int i = r18.A00;
            int i2 = r18.A01;
            int i3 = i - i2;
            if (i3 > 0) {
                int i4 = this.A06;
                if (i4 == 0) {
                    byte[] bArr = r18.A02;
                    while (i2 < i) {
                        int i5 = i2 + 1;
                        int i6 = bArr[i2] & 255;
                        if (this.A04 == 512 && ((65280 | (((byte) i6) & 255)) & 65526) == 65520) {
                            if (!this.A0E) {
                                int i7 = i5 - 2;
                                r18.A0S(i7 + 1);
                                C95054d0 r8 = this.A0H;
                                byte[] bArr2 = r8.A03;
                                if (C95304dT.A00(r18) >= 1) {
                                    r18.A0V(bArr2, 0, 1);
                                    r8.A07(4);
                                    int A04 = r8.A04(1);
                                    int i8 = this.A03;
                                    if (i8 == -1 || A04 == i8) {
                                        if (this.A02 != -1) {
                                            byte[] bArr3 = r8.A03;
                                            if (C95304dT.A00(r18) >= 1) {
                                                r18.A0V(bArr3, 0, 1);
                                                r8.A07(2);
                                                if (r8.A04(4) == this.A02) {
                                                    r18.A0S(i7 + 2);
                                                }
                                            }
                                        }
                                        byte[] bArr4 = r8.A03;
                                        if (C95304dT.A00(r18) >= 4) {
                                            r18.A0V(bArr4, 0, 4);
                                            r8.A07(14);
                                            int A042 = r8.A04(13);
                                            if (A042 >= 7) {
                                                byte[] bArr5 = r18.A02;
                                                int i9 = r18.A00;
                                                int i10 = i7 + A042;
                                                if (i10 < i9) {
                                                    byte b = bArr5[i10];
                                                    if (b == -1) {
                                                        int i11 = i10 + 1;
                                                        if (i11 != i9) {
                                                            byte b2 = bArr5[i11];
                                                            if (((65280 | (b2 & 255)) & 65526) == 65520 && ((b2 & 8) >> 3) == A04) {
                                                            }
                                                        }
                                                    } else if (b == 73) {
                                                        int i12 = i10 + 1;
                                                        if (i12 != i9) {
                                                            if (bArr5[i12] == 68) {
                                                                int i13 = i10 + 2;
                                                                if (i13 != i9) {
                                                                    if (bArr5[i13] == 51) {
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    r18.A0S(i5);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            this.A01 = (i6 & 8) >> 3;
                            boolean z = true;
                            if ((i6 & 1) != 0) {
                                z = false;
                            }
                            this.A0F = z;
                            int i14 = 3;
                            if (!this.A0E) {
                                i14 = 1;
                            }
                            this.A06 = i14;
                            this.A00 = 0;
                            r18.A0S(i5);
                            break;
                            break;
                        }
                        int i15 = this.A04;
                        int i16 = i6 | i15;
                        int i17 = 768;
                        if (i16 != 329) {
                            if (i16 != 511) {
                                i17 = EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                if (i16 != 836) {
                                    if (i16 == 1075) {
                                        this.A06 = 2;
                                        this.A00 = A0L.length;
                                        this.A05 = 0;
                                        this.A0I.A0S(0);
                                        r18.A0S(i5);
                                        break;
                                        break;
                                    } else if (i15 != 256) {
                                        this.A04 = 256;
                                        i5--;
                                    }
                                }
                            } else {
                                this.A04 = 512;
                            }
                            i2 = i5;
                        }
                        this.A04 = i17;
                        i2 = i5;
                    }
                    r18.A0S(i2);
                } else if (i4 == 1) {
                    C95054d0 r4 = this.A0H;
                    C72463ee.A0X(r18.A02, r4.A03, i2, 0);
                    r4.A07(2);
                    int A043 = r4.A04(4);
                    int i18 = this.A02;
                    if (i18 == -1 || A043 == i18) {
                        if (!this.A0E) {
                            this.A0E = true;
                            this.A03 = this.A01;
                            this.A02 = A043;
                        }
                        this.A06 = 3;
                        this.A00 = 0;
                    } else {
                        this.A0E = false;
                        this.A06 = 0;
                        this.A00 = 0;
                        this.A04 = 256;
                    }
                } else if (i4 == 2) {
                    C95304dT r7 = this.A0I;
                    byte[] bArr6 = r7.A02;
                    int i19 = this.A00;
                    int A02 = C72463ee.A02(10, i19, i3);
                    r18.A0V(bArr6, i19, A02);
                    int i20 = this.A00 + A02;
                    this.A00 = i20;
                    if (i20 == 10) {
                        this.A0B.AbC(r7, 10);
                        r7.A0S(6);
                        AnonymousClass5X6 r5 = this.A0B;
                        this.A06 = 4;
                        this.A00 = 10;
                        this.A0A = r5;
                        this.A07 = 0;
                        this.A05 = r7.A0B() + 10;
                    }
                } else if (i4 == 3) {
                    int i21 = 5;
                    if (this.A0F) {
                        i21 = 7;
                    }
                    C95054d0 r9 = this.A0H;
                    byte[] bArr7 = r9.A03;
                    int i22 = this.A00;
                    int A022 = C72463ee.A02(i21, i22, i3);
                    r18.A0V(bArr7, i22, A022);
                    int i23 = this.A00 + A022;
                    this.A00 = i23;
                    if (i23 == i21) {
                        r9.A07(0);
                        if (!this.A0G) {
                            int A044 = r9.A04(2) + 1;
                            if (A044 != 2) {
                                StringBuilder A0k = C12960it.A0k("Detected audio object type: ");
                                A0k.append(A044);
                                Log.w("AdtsReader", C12960it.A0d(", but assuming AAC LC.", A0k));
                            }
                            r9.A08(5);
                            int A045 = r9.A04(3);
                            int i24 = this.A02;
                            byte[] bArr8 = new byte[2];
                            bArr8[0] = (byte) (16 | ((i24 >> 1) & 7));
                            C72463ee.A0O((i24 << 7) & 128, bArr8, (A045 << 3) & 120, 1);
                            C90704Ox A01 = C94674cK.A01(new C95054d0(bArr8, 2), false);
                            C93844ap A00 = C93844ap.A00();
                            A00.A0O = this.A0D;
                            A00.A0R = "audio/mp4a-latm";
                            A00.A0M = A01.A02;
                            A00.A04 = A01.A00;
                            A00.A0D = A01.A01;
                            A00.A0S = Collections.singletonList(bArr8);
                            A00.A0Q = this.A0J;
                            C100614mC r52 = new C100614mC(A00);
                            this.A08 = 1024000000 / ((long) r52.A0F);
                            this.A0C.AA6(r52);
                            this.A0G = true;
                        } else {
                            r9.A08(10);
                        }
                        r9.A08(4);
                        int A046 = (r9.A04(13) - 2) - 5;
                        if (this.A0F) {
                            A046 -= 2;
                        }
                        AnonymousClass5X6 r53 = this.A0C;
                        long j = this.A08;
                        this.A06 = 4;
                        this.A00 = 0;
                        this.A0A = r53;
                        this.A07 = j;
                        this.A05 = A046;
                    }
                } else if (i4 == 4) {
                    int A023 = C72463ee.A02(this.A05, this.A00, i3);
                    this.A0A.AbC(r18, A023);
                    int i25 = this.A00 + A023;
                    this.A00 = i25;
                    int i26 = this.A05;
                    if (i25 == i26) {
                        this.A0A.AbG(null, 1, i26, 0, this.A09);
                        this.A09 += this.A07;
                        this.A06 = 0;
                        this.A00 = 0;
                        this.A04 = 256;
                    }
                } else {
                    throw C72463ee.A0D();
                }
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r4, C92824Xo r5) {
        r5.A03();
        this.A0D = r5.A02();
        AnonymousClass5X6 A00 = C92824Xo.A00(r4, r5);
        this.A0C = A00;
        this.A0A = A00;
        if (this.A0K) {
            r5.A03();
            AnonymousClass5X6 Af4 = r4.Af4(r5.A01(), 5);
            this.A0B = Af4;
            C93844ap A002 = C93844ap.A00();
            A002.A0O = r5.A02();
            A002.A0R = "application/id3";
            C72453ed.A17(A002, Af4);
            return;
        }
        this.A0B = new C106984wb();
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A09 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A0E = false;
        this.A06 = 0;
        this.A00 = 0;
        this.A04 = 256;
    }
}
