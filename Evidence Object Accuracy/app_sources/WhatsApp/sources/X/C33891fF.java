package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1fF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33891fF extends AbstractC21570xd {
    public C33891fF(C232010t r1) {
        super(r1);
    }

    public long A00(UserJid userJid) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT sent_tc_token_timestamp FROM wa_trusted_contacts_send WHERE jid= ?", new String[]{userJid.getRawString()});
            long j = A09.moveToNext() ? A09.getLong(A09.getColumnIndexOrThrow("sent_tc_token_timestamp")) : -1;
            A09.close();
            A01.close();
            return j;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
