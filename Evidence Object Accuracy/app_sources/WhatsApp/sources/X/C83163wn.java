package X;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83163wn extends AnonymousClass4YR {
    public static final AnonymousClass5TA A00 = new AnonymousClass52G();

    public static void A00(AbstractC111885Be r14, C94394bk r15, AnonymousClass4YR r16, AnonymousClass5TA r17, Object obj, String str) {
        Object obj2;
        AbstractC117035Xw r9 = r15.A01.A00;
        if (obj instanceof Map) {
            if (r17.ALL(obj)) {
                r16.A02(r14, r15, obj, str);
            }
            Iterator it = r9.AFy(obj).iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                StringBuilder A0j = C12960it.A0j(str);
                A0j.append("['");
                A0j.append(A0x);
                String A0d = C12960it.A0d("']", A0j);
                Map map = (Map) obj;
                if (!map.containsKey(A0x)) {
                    obj2 = AbstractC117035Xw.A00;
                } else {
                    obj2 = map.get(A0x);
                }
                if (obj2 != AbstractC117035Xw.A00) {
                    A00(new C82883wL(obj, A0x), r15, r16, r17, obj2, A0d);
                }
            }
        } else if (obj instanceof List) {
            int i = 0;
            if (r17.ALL(obj)) {
                if (r16.A01 == null) {
                    r16.A02(r14, r15, obj, str);
                } else {
                    AnonymousClass4YR A002 = r16.A00();
                    int i2 = 0;
                    for (Object obj3 : r9.Aet(obj)) {
                        StringBuilder A0j2 = C12960it.A0j(str);
                        A0j2.append("[");
                        A0j2.append(i2);
                        String A0d2 = C12960it.A0d("]", A0j2);
                        A002.A00 = i2;
                        A002.A02(r14, r15, obj3, A0d2);
                        i2++;
                    }
                }
            }
            for (Object obj4 : r9.Aet(obj)) {
                StringBuilder A0j3 = C12960it.A0j(str);
                A0j3.append("[");
                A0j3.append(i);
                A00(new C82893wM(obj, i), r15, r16, r17, obj4, C12960it.A0d("]", A0j3));
                i++;
            }
        }
    }
}
