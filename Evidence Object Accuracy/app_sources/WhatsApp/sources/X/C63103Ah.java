package X;

import android.text.TextUtils;

/* renamed from: X.3Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63103Ah {
    public static boolean A00(AbstractC16130oV r2) {
        if (r2 != null) {
            C16150oX r1 = r2.A02;
            if (!TextUtils.isEmpty(r2.A04) && r1 != null && r1.A0U == null && AbstractC15340mz.A00(r2).A0F == null) {
                return true;
            }
        }
        return false;
    }
}
