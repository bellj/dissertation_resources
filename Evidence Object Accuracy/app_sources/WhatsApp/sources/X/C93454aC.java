package X;

/* renamed from: X.4aC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93454aC {
    public static final C93454aC A02 = new C93454aC(C113045Fu.class, "dhDefaultParams");
    public static final C93454aC A03 = new C93454aC(C113055Fv.class, "dsaDefaultParams");
    public final Class A00;
    public final String A01;

    public C93454aC(Class cls, String str) {
        this.A01 = str;
        this.A00 = cls;
    }
}
