package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;

/* renamed from: X.2CG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CG implements AnonymousClass1UU {
    public final /* synthetic */ AnonymousClass2CH A00;

    public AnonymousClass2CG(AnonymousClass2CH r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1UU
    public void AYT(int i) {
        this.A00.A01.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, i, 30));
    }

    @Override // X.AnonymousClass1UU
    public void AYU() {
        this.A00.A01.A0I(new RunnableBRunnable0Shape13S0100000_I0_13(this, 46));
    }
}
