package X;

import android.app.ProgressDialog;
import java.lang.ref.WeakReference;

/* renamed from: X.38Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38Y extends AbstractC16350or {
    public ProgressDialog A00;
    public final C18790t3 A01;
    public final C25771At A02;
    public final C18810t5 A03;
    public final String A04;
    public final String A05;
    public final WeakReference A06;

    public AnonymousClass38Y(ActivityC13810kN r2, C18790t3 r3, C25771At r4, C18810t5 r5, String str, String str2) {
        super(r2);
        this.A06 = C12970iu.A10(r2);
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = str;
        this.A04 = str2;
    }
}
