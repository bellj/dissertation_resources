package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout;
import com.whatsapp.conversation.conversationrow.TemplateRowContentLayout;
import java.util.List;

/* renamed from: X.2yD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60552yD extends C60792ye {
    public boolean A00;
    public final TemplateQuickReplyButtonsLayout A01 = ((TemplateQuickReplyButtonsLayout) findViewById(R.id.template_quick_reply_buttons));
    public final TemplateRowContentLayout A02 = ((TemplateRowContentLayout) findViewById(R.id.template_message_content));

    public C60552yD(Context context, AbstractC13890kV r3, AnonymousClass1XN r4) {
        super(context, r3, r4);
        A0Z();
        A1N();
    }

    @Override // X.AnonymousClass2xh, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            ((C60792ye) this).A02 = A08.A2e();
            ((C60792ye) this).A01 = (C26171Ch) A08.A2a.get();
        }
    }

    @Override // X.C60792ye, X.AnonymousClass1OY
    public void A0s() {
        A1N();
        super.A0s();
    }

    @Override // X.C60792ye, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1N();
        }
    }

    public final void A1N() {
        List list;
        this.A02.A02(this);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A01;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass4KN r1 = this.A1c;
            AbstractC13890kV r0 = ((AbstractC28551Oa) this).A0a;
            if (r0 == null || !r0.AdX()) {
                list = null;
            } else {
                list = ((AbstractC28871Pi) ((AnonymousClass1XO) ((AbstractC28551Oa) this).A0O)).AH7().A04;
            }
            templateQuickReplyButtonsLayout.A01(r1, list);
        }
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_template_title_location_left;
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_template_title_location_left;
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_template_title_location_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A01;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass1OY.A0G(templateQuickReplyButtonsLayout, this);
        }
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A01;
        if (templateQuickReplyButtonsLayout != null) {
            setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A05(this, templateQuickReplyButtonsLayout));
        }
    }
}
