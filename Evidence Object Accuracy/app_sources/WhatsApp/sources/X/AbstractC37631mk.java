package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.1mk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC37631mk extends Closeable {
    int A7O();

    InputStream AAZ(C18790t3 v, Integer num, Integer num2);

    String AIP(String str);

    long getContentLength();
}
