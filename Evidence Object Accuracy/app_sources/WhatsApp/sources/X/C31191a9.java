package X;

import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

@Deprecated
/* renamed from: X.1a9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31191a9 {
    public final C15450nH A00;
    public final C31401aU A01;
    public final C31331aN A02;
    public final C16040oL A03;
    public final AnonymousClass1RM A04;
    public final AnonymousClass1RX A05;
    public final C31701ay A06;
    public final C15990oG A07;
    public final C18240s8 A08;

    public C31191a9(C15450nH r2, C29251Rl r3, C16040oL r4, AnonymousClass1RM r5, C29291Rp r6, AnonymousClass1RX r7, AnonymousClass1RP r8, C15990oG r9, C18240s8 r10) {
        this.A00 = r2;
        this.A08 = r10;
        this.A07 = r9;
        this.A05 = r7;
        this.A02 = new C31331aN(r6, r9, r10);
        this.A06 = new C31701ay(r8, r10);
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = new C31401aU(r3, r9, r10);
    }

    public static C29211Rh A00(C31761b4 r5, int i) {
        byte[] A00 = r5.A00().A01.A00();
        int length = A00.length - 1;
        byte[] bArr = new byte[length];
        System.arraycopy(A00, 1, bArr, 0, length);
        return new C29211Rh(new byte[]{(byte) (i >> 16), (byte) (i >> 8), (byte) i}, bArr, null);
    }

    public C31741b2 A01() {
        try {
            C29331Rt A02 = this.A03.A02();
            C31641as r2 = new C31641as(A02.A01);
            C31721b0 r1 = new C31721b0(A02.A00);
            Log.i("axolotl loading identity key pair");
            return new C31741b2(r2, r1);
        } catch (C31561ak unused) {
            throw new SQLiteException("Invalid public key stored in identities table");
        }
    }

    public C31611ap A02(C31601ao r3) {
        try {
            return new C31611ap(this.A07.A0G(C29201Rg.A00(r3)).A00());
        } catch (IOException unused) {
            throw new AssertionError("serialize/deserialize failed from Session object");
        }
    }

    public void A03(C31641as r4, C31601ao r5) {
        AnonymousClass1JS r2;
        if (r4 != null) {
            try {
                r2 = new AnonymousClass1JS(C15940oB.A01(r4.A00.A00()));
            } catch (AnonymousClass1RY unused) {
                throw new AssertionError("Conversion between ECPublicKey and CurvePublicKey should never fail");
            }
        } else {
            r2 = null;
        }
        this.A07.A0U(r2, C29201Rg.A00(r5));
    }

    public void A04(C31601ao r8, C31611ap r9) {
        this.A08.A00();
        try {
            if (r9.A01.A00.A05.A04().length != 0) {
                C15990oG r5 = this.A07;
                C15950oC A00 = C29201Rg.A00(r8);
                LinkedList linkedList = new LinkedList();
                Iterator it = r9.A00.iterator();
                while (it.hasNext()) {
                    linkedList.add(((C31661au) it.next()).A00);
                }
                AnonymousClass1G4 A0T = C32071bZ.A03.A0T();
                C31321aM r0 = r9.A01.A00;
                A0T.A03();
                C32071bZ r1 = (C32071bZ) A0T.A00;
                r1.A02 = r0;
                r1.A00 |= 1;
                A0T.A03();
                C32071bZ r2 = (C32071bZ) A0T.A00;
                AnonymousClass1K6 r12 = r2.A01;
                if (!((AnonymousClass1K7) r12).A00) {
                    r12 = AbstractC27091Fz.A0G(r12);
                    r2.A01 = r12;
                }
                AnonymousClass1G5.A01(linkedList, r12);
                r5.A0a(A00, A0T.A02().A02());
                return;
            }
            throw new IOException("Alice base key missing from session");
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot store invalid session", e);
        }
    }
}
