package X;

/* renamed from: X.1un  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42181un {
    public int A00 = 1;
    public int A01 = 0;
    public int A02 = 1;
    public int A03 = 0;
    public C32141bg A04 = null;
    public C32141bg A05 = null;
    public String A06 = null;
    public String A07 = null;
    public boolean A08 = false;

    public C42181un() {
    }

    public C42181un(C32141bg r3, C32141bg r4, String str, String str2, int i, int i2, int i3, int i4) {
        this.A03 = i;
        this.A01 = i2;
        this.A07 = str;
        this.A06 = str2;
        this.A05 = r3;
        this.A04 = r4;
        this.A02 = i3;
        this.A00 = i4;
    }

    public C42181un(C38291nq r3) {
        this.A03 = r3.A03;
        this.A01 = r3.A01;
        this.A07 = r3.A07;
        this.A06 = r3.A06;
        this.A05 = r3.A05;
        this.A04 = r3.A04;
        this.A08 = r3.A08;
        this.A02 = r3.A02;
        this.A00 = r3.A00;
    }

    public static C42181un A00(AnonymousClass1M2 r2) {
        C42181un r1 = new C42181un();
        r1.A01 = r2.A03;
        r1.A06 = r2.A08;
        r1.A04 = r2.A00();
        r1.A00 = C38301nr.A00(r2);
        return r1;
    }

    public C38291nq A01() {
        int i = this.A03;
        int i2 = this.A01;
        String str = this.A07;
        String str2 = this.A06;
        return new C38291nq(this.A05, this.A04, str, str2, i, i2, this.A02, this.A00, this.A08);
    }
}
