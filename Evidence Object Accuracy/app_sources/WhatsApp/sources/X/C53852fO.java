package X;

import android.app.Application;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.2fO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53852fO extends AnonymousClass014 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final C14650lo A01;
    public final AnonymousClass2Cu A02;
    public final AnonymousClass10A A03;
    public final C22700zV A04;
    public final UserJid A05;
    public final AbstractC14440lR A06;

    public C53852fO(Application application, C14650lo r3, AnonymousClass10A r4, C22700zV r5, UserJid userJid, AbstractC14440lR r7) {
        super(application);
        C84343z9 r0 = new C84343z9(this);
        this.A02 = r0;
        this.A06 = r7;
        this.A04 = r5;
        this.A01 = r3;
        this.A03 = r4;
        this.A05 = userJid;
        r4.A03(r0);
    }

    public static Set A00(List list, List list2) {
        HashSet A12 = C12970iu.A12();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A12.add(((C92584Wm) it.next()).A01.A0D);
        }
        Iterator it2 = list2.iterator();
        while (it2.hasNext()) {
            A12.add(((C92584Wm) it2.next()).A01.A0D);
        }
        return A12;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A03.A04(this.A02);
    }

    public String A04(AnonymousClass018 r6, List list) {
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            i = (int) (((long) i) + ((C92584Wm) it.next()).A00);
        }
        if (i == 0) {
            return "";
        }
        if (i <= 999) {
            return r6.A0J().format((long) i);
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r6.A0J().format(999L));
        return C12960it.A0d("+", A0h);
    }

    public void A05() {
        C12990iw.A1O(this.A06, this, 23);
    }
}
