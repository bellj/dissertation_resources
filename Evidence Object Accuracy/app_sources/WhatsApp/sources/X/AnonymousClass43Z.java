package X;

/* renamed from: X.43Z  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43Z extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public AnonymousClass43Z() {
        super(2472, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A00);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamUserNotice {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeContentVersion", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeEvent", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeId", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
