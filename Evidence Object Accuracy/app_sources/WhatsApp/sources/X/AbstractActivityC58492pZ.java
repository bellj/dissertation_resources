package X;

import com.whatsapp.inappsupport.ui.SupportTopicsActivity;

/* renamed from: X.2pZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58492pZ extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58492pZ() {
        ActivityC13830kP.A1P(this, 79);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            SupportTopicsActivity supportTopicsActivity = (SupportTopicsActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, supportTopicsActivity);
            ActivityC13810kN.A10(A1M, supportTopicsActivity);
            ((ActivityC13790kL) supportTopicsActivity).A08 = ActivityC13790kL.A0S(r3, A1M, supportTopicsActivity, ActivityC13790kL.A0Y(A1M, supportTopicsActivity));
            supportTopicsActivity.A03 = (AnonymousClass19Y) A1M.AI6.get();
            supportTopicsActivity.A04 = (AnonymousClass11G) A1M.AKq.get();
        }
    }
}
