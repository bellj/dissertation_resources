package X;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.ImageReader;
import android.os.Handler;
import android.os.Looper;

/* renamed from: X.660  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass660 implements AbstractC1311461l {
    public int A00;
    public int A01;
    public Camera.PreviewCallback A02;
    public ImageReader.OnImageAvailableListener A03;
    public Handler A04;
    public AnonymousClass637 A05;
    public final Object A06 = C12970iu.A0l();

    public final void A02(Handler handler) {
        Looper myLooper;
        if (handler != null) {
            myLooper = handler.getLooper();
        } else {
            myLooper = Looper.myLooper();
        }
        if (myLooper != null) {
            Handler handler2 = this.A04;
            if (handler2 == null || handler2.getLooper() != myLooper) {
                this.A04 = new Handler(myLooper);
                return;
            }
            return;
        }
        throw C12970iu.A0f("handler is null but the current thread is not a looper");
    }

    @Override // X.AbstractC1311461l
    public SurfaceTexture ABC() {
        AnonymousClass637 r2 = this.A05;
        if (r2 == null) {
            r2 = new AnonymousClass637();
            this.A05 = r2;
        }
        return r2.A03(this.A01, this.A00);
    }

    @Override // X.AbstractC1311461l
    public void AcE(ImageReader.OnImageAvailableListener onImageAvailableListener, Handler handler) {
        synchronized (this.A06) {
            A02(handler);
            this.A03 = onImageAvailableListener;
        }
    }

    @Override // X.AbstractC1311461l
    public void Acb(SurfaceTexture surfaceTexture, int i, int i2) {
        AnonymousClass637 r0;
        this.A01 = i;
        this.A00 = i2;
        if (surfaceTexture != null || (r0 = this.A05) == null) {
            AnonymousClass637 r02 = this.A05;
            if (r02 == null) {
                r02 = new AnonymousClass637();
                this.A05 = r02;
            }
            r02.A03(i, i2);
            this.A05.A05(surfaceTexture, 0);
            return;
        }
        r0.A04();
        this.A05 = null;
    }

    @Override // X.AbstractC1311461l
    public void Acc(Camera.PreviewCallback previewCallback, Handler handler) {
        synchronized (this.A06) {
            A02(handler);
            this.A02 = previewCallback;
        }
    }

    @Override // android.media.ImageReader.OnImageAvailableListener
    public void onImageAvailable(ImageReader imageReader) {
        synchronized (this.A06) {
            Handler handler = this.A04;
            if (!(handler == null || this.A03 == null)) {
                handler.post(new Runnable(imageReader, this) { // from class: X.6He
                    public final /* synthetic */ ImageReader A00;
                    public final /* synthetic */ AnonymousClass660 A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AnonymousClass660.A01(this.A00, this.A01);
                    }
                });
            }
        }
    }

    @Override // android.hardware.Camera.PreviewCallback
    public void onPreviewFrame(byte[] bArr, Camera camera) {
        synchronized (this.A06) {
            Handler handler = this.A04;
            if (!(handler == null || this.A02 == null)) {
                handler.post(new Runnable(camera, this, bArr) { // from class: X.6Iz
                    public final /* synthetic */ Camera A00;
                    public final /* synthetic */ AnonymousClass660 A01;
                    public final /* synthetic */ byte[] A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AnonymousClass660.A00(this.A00, this.A01, this.A02);
                    }
                });
            }
        }
    }
}
