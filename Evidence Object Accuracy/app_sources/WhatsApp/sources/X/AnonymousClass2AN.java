package X;

import android.app.Activity;
import android.content.Intent;

/* renamed from: X.2AN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2AN extends AbstractDialogInterface$OnClickListenerC472029l {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Activity A01;
    public final /* synthetic */ Intent A02;

    public AnonymousClass2AN(Activity activity, Intent intent, int i) {
        this.A02 = intent;
        this.A01 = activity;
        this.A00 = i;
    }
}
