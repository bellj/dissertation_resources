package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5zD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130315zD {
    public static String A00(String str) {
        switch (str.hashCode()) {
            case -2049158756:
                if (str.equals("NATIONALITY")) {
                    return "nationality";
                }
                return null;
            case -1987897702:
                if (str.equals("PLACE_OF_BIRTH__CITY")) {
                    return "birth_city";
                }
                return null;
            case -1838336183:
                if (str.equals("NAME__LAST_NAME")) {
                    return "last";
                }
                return null;
            case -1683497011:
                if (str.equals("ADDRESS__ZIP")) {
                    return "zip";
                }
                return null;
            case -1169382806:
                if (str.equals("NAME__MIDDLE_NAME")) {
                    return "middle";
                }
                return null;
            case -649484769:
                if (str.equals("ADDRESS__CITY")) {
                    return "city";
                }
                return null;
            case -183691067:
                if (str.equals("NAME__FIRST_NAME")) {
                    return "first";
                }
                return null;
            case 67863:
                if (str.equals("DOB")) {
                    return "date_of_birth";
                }
                return null;
            case 1349113993:
                if (str.equals("ADDRESS__LINE1")) {
                    return "line1";
                }
                return null;
            case 1349113994:
                if (str.equals("ADDRESS__LINE2")) {
                    return "line2";
                }
                return null;
            case 2016058343:
                if (str.equals("PLACE_OF_BIRTH__COUNTRY")) {
                    return "birth_country";
                }
                return null;
            default:
                return null;
        }
    }

    public static Map A01(AbstractC129915yV r11) {
        String str;
        HashMap A11 = C12970iu.A11();
        String A00 = A00(r11.A02);
        String str2 = "";
        String str3 = A00;
        if (A00 == null) {
            str3 = str2;
        }
        A11.put("type", str3);
        String str4 = r11.A00;
        if (str4 != null) {
            String str5 = !str4.equals("KycTextQuestion") ? !str4.equals("KycDropdownQuestion") ? null : "dropdown" : "text";
            if (str5 == null) {
                str5 = str2;
            }
            A11.put("format", str5);
        }
        A11.put("title", r11.A01);
        String str6 = "0";
        String str7 = str6;
        if (r11.A03) {
            str7 = "1";
        }
        A11.put("editable", str7);
        if (r11.A04) {
            str6 = "1";
        }
        A11.put("optional", str6);
        if (r11 instanceof C121275ha) {
            str = ((C121275ha) r11).A00;
        } else if (!(r11 instanceof C121295hc)) {
            AnonymousClass60L r0 = ((C121285hb) r11).A01;
            if (r0 != null) {
                str = C1308860i.A00(r0.A02, r0.A01, r0.A00);
            } else {
                str = null;
            }
        } else {
            str = ((C121295hc) r11).A00;
        }
        if (str == null) {
            str = str2;
        }
        A11.put("value", str);
        if (r11 instanceof C121295hc) {
            try {
                JSONArray A0L = C117315Zl.A0L();
                List<C125745rk> list = ((C121295hc) r11).A01;
                for (C125745rk r02 : list) {
                    JSONObject put = C117295Zj.A0a().put("name", r02.A00);
                    String str8 = A00;
                    if (A00 == null) {
                        str8 = str2;
                    }
                    C117315Zl.A0Y("1", "is_supported", A0L, put.put("type", str8));
                }
                if (list.size() > 0) {
                    str2 = A0L.toString();
                }
                A11.put("dropdown_options", str2);
                return A11;
            } catch (JSONException unused) {
                Log.e("PAY: KycTypeMappers/getResponseMapForFields Error serializing dropdown question step");
            }
        }
        return A11;
    }
}
