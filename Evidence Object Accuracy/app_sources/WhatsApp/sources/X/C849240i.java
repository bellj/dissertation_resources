package X;

import android.content.Context;
import android.view.TextureView;

/* renamed from: X.40i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C849240i extends AnonymousClass5B1 {
    public final TextureView.SurfaceTextureListener A00;
    public final TextureView A01;

    public C849240i(TextureView textureView, boolean z, boolean z2) {
        super("voip/video/TextureViewVideoPort/", z, z2);
        TextureView$SurfaceTextureListenerC100914mg r1 = new TextureView$SurfaceTextureListenerC100914mg(this);
        this.A00 = r1;
        this.A01 = textureView;
        if (z) {
            textureView.setOpaque(false);
        }
        textureView.setSurfaceTextureListener(r1);
        A05();
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public Context getContext() {
        return this.A01.getContext();
    }

    @Override // X.AnonymousClass5B1, com.whatsapp.voipcalling.VideoPort
    public void release() {
        this.A01.setSurfaceTextureListener(null);
        super.release();
    }
}
