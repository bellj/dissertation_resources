package X;

import java.lang.reflect.Method;
import java.security.PrivilegedExceptionAction;
import java.security.spec.AlgorithmParameterSpec;

/* renamed from: X.5Bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112065Bx implements PrivilegedExceptionAction {
    public final /* synthetic */ AlgorithmParameterSpec A00;
    public final /* synthetic */ AnonymousClass20K A01;

    public C112065Bx(AlgorithmParameterSpec algorithmParameterSpec, AnonymousClass20K r2) {
        this.A01 = r2;
        this.A00 = algorithmParameterSpec;
    }

    @Override // java.security.PrivilegedExceptionAction
    public Object run() {
        AnonymousClass20K r5 = this.A01;
        Method method = C94694cN.A02;
        AlgorithmParameterSpec algorithmParameterSpec = this.A00;
        return new C113035Ft(r5, (byte[]) C94694cN.A01.invoke(algorithmParameterSpec, new Object[0]), null, C12960it.A05(method.invoke(algorithmParameterSpec, new Object[0])));
    }
}
