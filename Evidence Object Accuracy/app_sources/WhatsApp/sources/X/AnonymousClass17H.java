package X;

import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Map;
import org.json.JSONException;

/* renamed from: X.17H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17H {
    public final C20080vB A00;
    public final C14830m7 A01;
    public final AnonymousClass17E A02;
    public final AnonymousClass01H A03;

    public AnonymousClass17H(C20080vB r1, C14830m7 r2, AnonymousClass17E r3, AnonymousClass01H r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    public static /* synthetic */ void A00(AbstractC63833Dc r12, C16820po r13, AnonymousClass17H r14, Integer num, String str, String str2, String str3, String str4) {
        try {
            try {
                C20080vB r4 = r14.A00;
                X509Certificate A01 = r4.A01(str, new String[]{"CN=WhatsApp WWW Channel"});
                X509Certificate A012 = r4.A01(str2, new String[]{"CN=WhatsApp WWW Channel Signature"});
                Integer num2 = null;
                PublicKey A00 = str3 != null ? C63083Af.A00(str3) : null;
                if (str4 != null) {
                    num2 = Integer.valueOf(Integer.parseInt(str4));
                }
                AnonymousClass17E r1 = r14.A02;
                if (r1.A04.contains(r13)) {
                    if (num != null) {
                        int intValue = num.intValue();
                        long A002 = r14.A01.A00() / 1000;
                        synchronized (r1) {
                            try {
                                Map A003 = r1.A00();
                                A003.put(r13, new C64663Gk(num2, str3, A01, A012, intValue, A002));
                                r1.A01(A003);
                            } catch (CertificateEncodingException | CertificateException | JSONException e) {
                                AnonymousClass009.A0D(e);
                            }
                        }
                    } else {
                        throw new IllegalStateException("cacheable certs should have ttl");
                    }
                }
                r12.A00(num2, A00, A01);
            } catch (UnsupportedEncodingException | NumberFormatException | GeneralSecurityException e2) {
                Log.e("FBUserEntityManagement : On error response while sending the payload");
                r12.A00.APp(e2);
            }
        } catch (CertificateExpiredException e3) {
            throw e3;
        }
    }

    public void A01(AbstractC63833Dc r12, C16820po r13) {
        C64663Gk r7;
        AnonymousClass17E r9 = this.A02;
        C64663Gk r6 = null;
        PublicKey publicKey = null;
        r6 = null;
        r6 = null;
        if (r9.A04.contains(r13)) {
            try {
                r7 = (C64663Gk) r9.A00().get(r13);
                if (r7 != null) {
                    X509Certificate x509Certificate = r7.A04;
                    int i = r7.A00;
                    long j = r7.A01;
                    long A00 = this.A01.A00();
                    try {
                        x509Certificate.checkValidity(new Date(A00));
                        AnonymousClass009.A05(Integer.valueOf(i));
                        AnonymousClass009.A05(Long.valueOf(j));
                        if (A00 / 1000 < j + ((long) (i >> 1))) {
                            try {
                                String str = r7.A03;
                                if (str != null) {
                                    publicKey = C63083Af.A00(str);
                                }
                                r12.A00(r7.A02, publicKey, x509Certificate);
                                return;
                            } catch (GeneralSecurityException e) {
                                AnonymousClass009.A0D(e);
                            }
                        }
                    } catch (CertificateExpiredException | CertificateNotYetValidException unused) {
                    }
                    try {
                        Map A002 = r9.A00();
                        A002.remove(r13);
                        r9.A01(A002);
                    } catch (CertificateException | JSONException e2) {
                        AnonymousClass009.A0D(e2);
                    }
                }
            } catch (CertificateException | JSONException e3) {
                AnonymousClass009.A0D(e3);
                r7 = null;
            }
            r6 = r7;
        }
        Object obj = ((AnonymousClass17G) this.A03.get()).A00.get(r13);
        AnonymousClass009.A05(obj);
        ((AnonymousClass17F) ((AnonymousClass01N) obj).get()).A9q(new C68823Ww(r12, r6, r13, this));
    }
}
