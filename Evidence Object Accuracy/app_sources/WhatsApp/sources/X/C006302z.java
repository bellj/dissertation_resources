package X;

import java.nio.CharBuffer;

/* renamed from: X.02z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C006302z extends AnonymousClass030 {
    public final ThreadLocal A00 = new ThreadLocal();
    public final ThreadLocal A01 = new ThreadLocal();

    public static long A00(AnonymousClass033 r2) {
        r2.A07(CharBuffer.allocate(32));
        r2.A04();
        long A00 = r2.A00();
        r2.A03();
        return A00;
    }

    @Override // X.AnonymousClass030
    public AnonymousClass032 A01() {
        return new AnonymousClass031();
    }

    @Override // X.AnonymousClass030
    public /* bridge */ /* synthetic */ boolean A02(AnonymousClass032 r5) {
        AnonymousClass031 r52 = (AnonymousClass031) r5;
        if (r52 != null) {
            try {
                ThreadLocal threadLocal = this.A00;
                AnonymousClass033 r2 = (AnonymousClass033) threadLocal.get();
                if (r2 == null) {
                    r2 = new AnonymousClass033("/proc/self/io");
                    threadLocal.set(r2);
                }
                r2.A02();
                if (!r2.A05) {
                    return false;
                }
                r52.rcharBytes = A00(r2);
                r52.wcharBytes = A00(r2);
                r52.syscrCount = A00(r2);
                r52.syscwCount = A00(r2);
                r52.readBytes = A00(r2);
                r52.writeBytes = A00(r2);
                r52.cancelledWriteBytes = A00(r2);
                ThreadLocal threadLocal2 = this.A01;
                AnonymousClass033 r3 = (AnonymousClass033) threadLocal2.get();
                if (r3 == null) {
                    r3 = new AnonymousClass033("/proc/self/stat");
                    threadLocal2.set(r3);
                }
                r3.A02();
                if (!r3.A05) {
                    return false;
                }
                int i = 0;
                do {
                    r3.A04();
                    i++;
                } while (i < 11);
                r52.majorFaults = r3.A00();
                while (i < 41) {
                    r3.A04();
                    i++;
                }
                r52.blkIoTicks = r3.A00();
                return true;
            } catch (C10760fA unused) {
                return false;
            }
        } else {
            throw new IllegalArgumentException("Null value passed to getSnapshot!");
        }
    }
}
