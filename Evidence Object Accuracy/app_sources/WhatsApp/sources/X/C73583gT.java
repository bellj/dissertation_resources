package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.3gT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73583gT extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ AnonymousClass21U A00;

    public C73583gT(AnonymousClass21U r1) {
        this.A00 = r1;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        AnonymousClass21U r3 = this.A00;
        View view = r3.A0L;
        if (view == null || r3.A07() || r3.A0H) {
            return false;
        }
        r3.A07.A01(view, 1);
        r3.A08.A0M(4);
        r3.A0H = true;
        return true;
    }
}
