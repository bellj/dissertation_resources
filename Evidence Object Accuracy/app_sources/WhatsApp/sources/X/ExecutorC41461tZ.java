package X;

import java.util.concurrent.Executor;

/* renamed from: X.1tZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class ExecutorC41461tZ implements Executor {
    public final /* synthetic */ AbstractC14440lR A00;

    public /* synthetic */ ExecutorC41461tZ(AbstractC14440lR r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.A00.Ab2(runnable);
    }
}
