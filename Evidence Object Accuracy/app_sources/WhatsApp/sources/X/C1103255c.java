package X;

/* renamed from: X.55c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1103255c implements AbstractC33171dZ {
    public final C19990v2 A00;
    public final C14850m9 A01;

    public C1103255c(C19990v2 r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC33171dZ
    public boolean A9v(AbstractC14640lm r5) {
        boolean A07 = this.A01.A07(1608);
        C19990v2 r1 = this.A00;
        int A00 = r1.A00(r5);
        if (A07) {
            if (A00 == 0 || r1.A0E(r5)) {
                return false;
            }
            return true;
        } else if (A00 != 0) {
            return true;
        } else {
            return false;
        }
    }
}
