package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.07k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C015607k {
    public static Method A00;
    public static Method A01;
    public static boolean A02;
    public static boolean A03;

    public static int A00(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 19) {
            return AnonymousClass0UB.A00(drawable);
        }
        return 0;
    }

    public static int A01(Drawable drawable) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return C05640Qj.A00(drawable);
        }
        if (i >= 17) {
            if (!A02) {
                try {
                    Method declaredMethod = Drawable.class.getDeclaredMethod("getLayoutDirection", new Class[0]);
                    A00 = declaredMethod;
                    declaredMethod.setAccessible(true);
                } catch (NoSuchMethodException e) {
                    Log.i("DrawableCompat", "Failed to retrieve getLayoutDirection() method", e);
                }
                A02 = true;
            }
            Method method = A00;
            if (method != null) {
                try {
                    return ((Integer) method.invoke(drawable, new Object[0])).intValue();
                } catch (Exception e2) {
                    Log.i("DrawableCompat", "Failed to invoke getLayoutDirection() via reflection", e2);
                    A00 = null;
                }
            }
        }
        return 0;
    }

    public static ColorFilter A02(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0UZ.A00(drawable);
        }
        return null;
    }

    public static Drawable A03(Drawable drawable) {
        int i = Build.VERSION.SDK_INT;
        if (i < 23) {
            boolean z = drawable instanceof AbstractC013806l;
            if (i >= 21) {
                if (!z) {
                    return new AnonymousClass0DO(drawable);
                }
            } else if (!z) {
                return new C016607v(drawable);
            }
        }
        return drawable;
    }

    public static void A04(ColorStateList colorStateList, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A01(colorStateList, drawable);
        } else if (drawable instanceof AbstractC013806l) {
            ((AbstractC013806l) drawable).setTintList(colorStateList);
        }
    }

    public static void A05(Resources.Theme theme, Resources resources, Drawable drawable, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A02(theme, resources, drawable, attributeSet, xmlPullParser);
        } else {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        }
    }

    public static void A06(Resources.Theme theme, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A03(theme, drawable);
        }
    }

    public static void A07(PorterDuff.Mode mode, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A04(mode, drawable);
        } else if (drawable instanceof AbstractC013806l) {
            ((AbstractC013806l) drawable).setTintMode(mode);
        }
    }

    public static void A08(Drawable drawable) {
        DrawableContainer.DrawableContainerState drawableContainerState;
        Drawable drawable2;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23 || i < 21) {
            drawable.clearColorFilter();
            return;
        }
        drawable.clearColorFilter();
        if (drawable instanceof InsetDrawable) {
            drawable2 = AnonymousClass0UB.A02((InsetDrawable) drawable);
        } else if (drawable instanceof AbstractC016707w) {
            drawable2 = ((C016607v) ((AbstractC016707w) drawable)).A02;
        } else if ((drawable instanceof DrawableContainer) && (drawableContainerState = (DrawableContainer.DrawableContainerState) ((DrawableContainer) drawable).getConstantState()) != null) {
            int childCount = drawableContainerState.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                Drawable A012 = AnonymousClass0UB.A01(drawableContainerState, i2);
                if (A012 != null) {
                    A08(A012);
                }
            }
            return;
        } else {
            return;
        }
        A08(drawable2);
    }

    public static void A09(Drawable drawable, float f, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A05(drawable, f, f2);
        }
    }

    public static void A0A(Drawable drawable, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A06(drawable, i);
        } else if (drawable instanceof AbstractC013806l) {
            ((AbstractC013806l) drawable).setTint(i);
        }
    }

    public static void A0B(Drawable drawable, int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0UZ.A07(drawable, i, i2, i3, i4);
        }
    }

    public static void A0C(Drawable drawable, boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass0UB.A03(drawable, z);
        }
    }

    public static boolean A0D(int i, Drawable drawable) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            return C05640Qj.A01(i, drawable);
        }
        if (i2 >= 17) {
            if (!A03) {
                try {
                    Method declaredMethod = Drawable.class.getDeclaredMethod("setLayoutDirection", Integer.TYPE);
                    A01 = declaredMethod;
                    declaredMethod.setAccessible(true);
                } catch (NoSuchMethodException e) {
                    Log.i("DrawableCompat", "Failed to retrieve setLayoutDirection(int) method", e);
                }
                A03 = true;
            }
            Method method = A01;
            if (method != null) {
                try {
                    method.invoke(drawable, Integer.valueOf(i));
                    return true;
                } catch (Exception e2) {
                    Log.i("DrawableCompat", "Failed to invoke setLayoutDirection(int) via reflection", e2);
                    A01 = null;
                }
            }
        }
        return false;
    }

    public static boolean A0E(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0UZ.A08(drawable);
        }
        return false;
    }

    public static boolean A0F(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 19) {
            return AnonymousClass0UB.A04(drawable);
        }
        return false;
    }
}
