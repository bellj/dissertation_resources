package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06870Vk implements Parcelable {
    public static final C06870Vk A02 = new C06870Vk(new AnonymousClass03T(-90.0d, -180.0d), new AnonymousClass03T(90.0d, 180.0d));
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(31);
    public final AnonymousClass03T A00;
    public final AnonymousClass03T A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06870Vk(AnonymousClass03T r7, AnonymousClass03T r8) {
        double d = r7.A00;
        double d2 = r8.A00;
        if (d <= d2) {
            this.A01 = r7;
            this.A00 = r8;
            return;
        }
        StringBuilder sb = new StringBuilder("Southern latitude (");
        sb.append(d);
        sb.append(") exceeds Northern latitude (");
        sb.append(d2);
        sb.append(").");
        throw new IllegalArgumentException(sb.toString());
    }

    public /* synthetic */ C06870Vk(Parcel parcel) {
        this.A00 = (AnonymousClass03T) parcel.readParcelable(AnonymousClass03T.class.getClassLoader());
        this.A01 = (AnonymousClass03T) parcel.readParcelable(AnonymousClass03T.class.getClassLoader());
    }

    public AnonymousClass03T A00() {
        double d;
        AnonymousClass03T r3 = this.A01;
        double d2 = r3.A00;
        AnonymousClass03T r2 = this.A00;
        double d3 = (d2 + r2.A00) / 2.0d;
        double d4 = r3.A01;
        double d5 = r2.A01;
        int i = (d4 > d5 ? 1 : (d4 == d5 ? 0 : -1));
        double d6 = d4 + d5;
        if (i <= 0) {
            d = d6 / 2.0d;
        } else {
            double d7 = 360.0d;
            double d8 = (d6 + 360.0d) / 2.0d;
            if (d8 <= 180.0d) {
                d7 = 0.0d;
            }
            d = d8 - d7;
        }
        return new AnonymousClass03T(d3, d);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C06870Vk)) {
            return false;
        }
        C06870Vk r4 = (C06870Vk) obj;
        if (!this.A00.equals(r4.A00) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((527 + this.A00.hashCode()) * 31) + this.A01.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("LatLngBounds");
        sb.append("{northeast=");
        sb.append(this.A00);
        sb.append(", southwest=");
        sb.append(this.A01);
        sb.append("}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
    }
}
