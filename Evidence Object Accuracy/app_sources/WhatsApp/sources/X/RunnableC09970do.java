package X;

import android.app.Notification;
import androidx.work.impl.foreground.SystemForegroundService;

/* renamed from: X.0do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09970do implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Notification A01;
    public final /* synthetic */ SystemForegroundService A02;

    public RunnableC09970do(Notification notification, SystemForegroundService systemForegroundService, int i) {
        this.A02 = systemForegroundService;
        this.A00 = i;
        this.A01 = notification;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A02.A00.notify(this.A00, this.A01);
    }
}
