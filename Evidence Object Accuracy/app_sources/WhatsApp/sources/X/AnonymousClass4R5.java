package X;

import java.util.Set;

/* renamed from: X.4R5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R5 {
    public final int A00;
    public final String A01;
    public final String A02;
    public final Set A03;

    public AnonymousClass4R5(String str, String str2, Set set, int i) {
        this.A00 = i;
        this.A01 = str;
        this.A02 = str2;
        this.A03 = set;
    }
}
