package X;

/* renamed from: X.407  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass407 extends C37171lc {
    public final boolean A00;

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass407) && this.A00 == ((AnonymousClass407) obj).A00);
    }

    @Override // X.C37171lc
    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public AnonymousClass407(boolean z) {
        super(AnonymousClass39o.A0O);
        this.A00 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NearbyBusinessWidgetShimmerItem(showHeaderShimmer=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
