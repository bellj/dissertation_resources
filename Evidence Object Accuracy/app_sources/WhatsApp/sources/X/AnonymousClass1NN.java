package X;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.net.ssl.SSLSession;

/* renamed from: X.1NN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1NN extends LinkedHashMap<AnonymousClass1NP, SSLSession> {
    public final /* synthetic */ AnonymousClass14K this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1NN(AnonymousClass14K r4) {
        super(64, 0.75f, true);
        this.this$0 = r4;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.Map$Entry] */
    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry<AnonymousClass1NP, SSLSession> entry) {
        return size() > this.this$0.A02;
    }
}
