package X;

import com.whatsapp.util.Log;
import java.io.PrintStream;

/* renamed from: X.4ZF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZF {
    public static AnonymousClass4WJ A00 = new AnonymousClass45X();

    public static void A00(AnonymousClass4AG r4, String str) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        AnonymousClass4WJ r1 = A00;
        String obj = stackTrace[2].toString();
        if (!(r1 instanceof AnonymousClass45Y)) {
            PrintStream printStream = System.err;
            StringBuilder A0h = C12960it.A0h();
            A0h.append(r4);
            A0h.append(": ");
            A0h.append(obj);
            A0h.append(" : ");
            printStream.println(C12960it.A0d(str, A0h));
            return;
        }
        switch (r4.ordinal()) {
            case 2:
                StringBuilder A0j = C12960it.A0j(obj);
                A0j.append(" :");
                Log.i(C12960it.A0d(str, A0j));
                return;
            case 3:
                StringBuilder A0j2 = C12960it.A0j(obj);
                A0j2.append(" :");
                Log.w(C12960it.A0d(str, A0j2));
                return;
            case 4:
                StringBuilder A0j3 = C12960it.A0j(obj);
                A0j3.append(" :");
                Log.e(C12960it.A0d(str, A0j3));
                return;
            case 5:
                StringBuilder A0j4 = C12960it.A0j(obj);
                A0j4.append(" :");
                Log.a(C12960it.A0d(str, A0j4));
                return;
            default:
                return;
        }
    }
}
