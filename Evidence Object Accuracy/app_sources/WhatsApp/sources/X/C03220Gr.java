package X;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;

/* renamed from: X.0Gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03220Gr extends AbstractC08040aU {
    public AnonymousClass0Gu A00;
    public final int A01;
    public final RectF A02;
    public final AnonymousClass036 A03;
    public final AnonymousClass036 A04;
    public final AnonymousClass0QR A05;
    public final AnonymousClass0QR A06;
    public final AnonymousClass0QR A07;
    public final AnonymousClass0J4 A08;
    public final String A09;
    public final boolean A0A;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C03220Gr(X.AnonymousClass0AA r16, X.C08200ak r17, X.AbstractC08070aX r18) {
        /*
            r15 = this;
            r2 = r17
            X.0Iw r0 = r2.A08
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0099;
                case 1: goto L_0x0095;
                default: goto L_0x000b;
            }
        L_0x000b:
            android.graphics.Paint$Cap r6 = android.graphics.Paint.Cap.SQUARE
        L_0x000d:
            X.0Jg r0 = r2.A09
            android.graphics.Paint$Join r7 = r0.A00()
            float r14 = r2.A00
            X.0HA r11 = r2.A04
            X.0H9 r9 = r2.A02
            java.util.List r13 = r2.A0B
            X.0H9 r10 = r2.A01
            r5 = r15
            r12 = r18
            r8 = r16
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.036 r0 = new X.036
            r0.<init>()
            r15.A03 = r0
            X.036 r0 = new X.036
            r0.<init>()
            r15.A04 = r0
            android.graphics.RectF r0 = new android.graphics.RectF
            r0.<init>()
            r15.A02 = r0
            java.lang.String r0 = r2.A0A
            r15.A09 = r0
            X.0J4 r0 = r2.A07
            r15.A08 = r0
            boolean r0 = r2.A0C
            r15.A0A = r0
            X.0Py r3 = r8.A04
            float r1 = r3.A00
            float r0 = r3.A02
            float r1 = r1 - r0
            float r0 = r3.A01
            float r1 = r1 / r0
            r0 = 1148846080(0x447a0000, float:1000.0)
            float r1 = r1 * r0
            long r3 = (long) r1
            float r1 = (float) r3
            r0 = 1107296256(0x42000000, float:32.0)
            float r1 = r1 / r0
            int r0 = (int) r1
            r15.A01 = r0
            X.0H5 r0 = r2.A03
            java.util.List r0 = r0.A00
            X.0Gw r1 = new X.0Gw
            r1.<init>(r0)
            r15.A05 = r1
            java.util.List r0 = r1.A07
            r0.add(r15)
            r12.A03(r1)
            X.0H6 r0 = r2.A06
            java.util.List r0 = r0.A00
            X.0H2 r1 = new X.0H2
            r1.<init>(r0)
            r15.A07 = r1
            java.util.List r0 = r1.A07
            r0.add(r15)
            r12.A03(r1)
            X.0H6 r0 = r2.A05
            java.util.List r0 = r0.A00
            X.0H2 r1 = new X.0H2
            r1.<init>(r0)
            r15.A06 = r1
            java.util.List r0 = r1.A07
            r0.add(r15)
            r12.A03(r1)
            return
        L_0x0095:
            android.graphics.Paint$Cap r6 = android.graphics.Paint.Cap.ROUND
            goto L_0x000d
        L_0x0099:
            android.graphics.Paint$Cap r6 = android.graphics.Paint.Cap.BUTT
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03220Gr.<init>(X.0AA, X.0ak, X.0aX):void");
    }

    public final int A00() {
        float f = this.A07.A02;
        float f2 = (float) this.A01;
        int round = Math.round(f * f2);
        int round2 = Math.round(this.A06.A02 * f2);
        int round3 = Math.round(this.A05.A02 * f2);
        int i = 17;
        if (round != 0) {
            i = 527 * round;
        }
        if (round2 != 0) {
            i = i * 31 * round2;
        }
        return round3 != 0 ? i * 31 * round3 : i;
    }

    public final int[] A01(int[] iArr) {
        AnonymousClass0Gu r0 = this.A00;
        if (r0 != null) {
            Integer[] numArr = (Integer[]) r0.A03();
            int length = iArr.length;
            int length2 = numArr.length;
            int i = 0;
            if (length != length2) {
                iArr = new int[length2];
                while (i < length2) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            } else {
                while (i < length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            }
        }
        return iArr;
    }

    @Override // X.AbstractC08040aU, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        super.A5q(r3, obj);
        if (obj == AbstractC12810iX.A0V) {
            AnonymousClass0Gu r1 = this.A00;
            if (r1 != null) {
                super.A0A.A0O.remove(r1);
            }
            if (r3 == null) {
                this.A00 = null;
                return;
            }
            AnonymousClass0Gu r0 = new AnonymousClass0Gu(r3, null);
            this.A00 = r0;
            r0.A07.add(this);
            super.A0A.A03(this.A00);
        }
    }

    @Override // X.AbstractC08040aU, X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        Shader shader;
        if (!this.A0A) {
            AAy(matrix, this.A02, false);
            AnonymousClass0J4 r3 = this.A08;
            AnonymousClass0J4 r2 = AnonymousClass0J4.LINEAR;
            int A00 = A00();
            if (r3 == r2) {
                AnonymousClass036 r5 = this.A03;
                long j = (long) A00;
                shader = (Shader) r5.A04(j, null);
                if (shader == null) {
                    PointF pointF = (PointF) this.A07.A03();
                    PointF pointF2 = (PointF) this.A06.A03();
                    AnonymousClass0N8 r4 = (AnonymousClass0N8) this.A05.A03();
                    shader = new LinearGradient(pointF.x, pointF.y, pointF2.x, pointF2.y, A01(r4.A01), r4.A00, Shader.TileMode.CLAMP);
                    r5.A09(j, shader);
                }
            } else {
                AnonymousClass036 r7 = this.A04;
                long j2 = (long) A00;
                shader = (Shader) r7.A04(j2, null);
                if (shader == null) {
                    PointF pointF3 = (PointF) this.A07.A03();
                    PointF pointF4 = (PointF) this.A06.A03();
                    AnonymousClass0N8 r22 = (AnonymousClass0N8) this.A05.A03();
                    int[] A01 = A01(r22.A01);
                    float[] fArr = r22.A00;
                    float f = pointF3.x;
                    float f2 = pointF3.y;
                    shader = new RadialGradient(f, f2, (float) Math.hypot((double) (pointF4.x - f), (double) (pointF4.y - f2)), A01, fArr, Shader.TileMode.CLAMP);
                    r7.A09(j2, shader);
                }
            }
            shader.setLocalMatrix(matrix);
            super.A01.setShader(shader);
            super.A9C(canvas, matrix, i);
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A09;
    }
}
