package X;

import java.io.OutputStream;

/* renamed from: X.5NC  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NC extends AnonymousClass1TL {
    public int A00;
    public AnonymousClass5NG A01;
    public AnonymousClass1TK A02;
    public AnonymousClass1TL A03;
    public AnonymousClass1TL A04;

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        AnonymousClass1TK r2 = this.A02;
        return new AnonymousClass5MC(this.A01, r2, this.A03, this.A04, this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return true;
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r4) {
        AnonymousClass1TL r0;
        AnonymousClass5NG r02;
        AnonymousClass1TK r03;
        if (r4 instanceof AnonymousClass5NC) {
            if (this == r4) {
                return true;
            }
            AnonymousClass5NC r42 = (AnonymousClass5NC) r4;
            AnonymousClass1TK r1 = this.A02;
            if (r1 != null && ((r03 = r42.A02) == null || !r03.A04(r1))) {
                return false;
            }
            AnonymousClass5NG r12 = this.A01;
            if (r12 != null && ((r02 = r42.A01) == null || !r02.A04(r12))) {
                return false;
            }
            AnonymousClass1TL r13 = this.A03;
            if (r13 == null || ((r0 = r42.A03) != null && r0.A04(r13))) {
                return this.A04.A04(r42.A04);
            }
            return false;
        }
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        AnonymousClass1TK r0 = this.A02;
        int hashCode = r0 != null ? r0.hashCode() : 0;
        AnonymousClass5NG r02 = this.A01;
        if (r02 != null) {
            hashCode ^= r02.hashCode();
        }
        AnonymousClass1TL r03 = this.A03;
        if (r03 != null) {
            hashCode ^= r03.hashCode();
        }
        return hashCode ^ this.A04.hashCode();
    }

    public AnonymousClass5NC(C94954co r5) {
        int i = 0;
        AnonymousClass1TL A00 = A00(r5, 0);
        if (A00 instanceof AnonymousClass1TK) {
            this.A02 = (AnonymousClass1TK) A00;
            A00 = A00(r5, 1);
            i = 1;
        }
        if (A00 instanceof AnonymousClass5NG) {
            this.A01 = (AnonymousClass5NG) A00;
            i++;
            A00 = A00(r5, i);
        }
        if (!(A00 instanceof AnonymousClass5NU)) {
            this.A03 = A00;
            i++;
            A00 = A00(r5, i);
        }
        if (r5.A00 != i + 1) {
            throw C12970iu.A0f("input vector too large");
        } else if (A00 instanceof AnonymousClass5NU) {
            AnonymousClass5NU r2 = (AnonymousClass5NU) A00;
            int i2 = r2.A00;
            if (i2 < 0 || i2 > 2) {
                throw C12970iu.A0f(C12960it.A0W(i2, "invalid encoding value: "));
            }
            this.A00 = i2;
            this.A04 = AnonymousClass5NU.A00(r2);
        } else {
            throw C12970iu.A0f("No tagged object found in vector. Structure doesn't seem to be of type External");
        }
    }

    public AnonymousClass5NC(AnonymousClass5NG r2, AnonymousClass1TK r3, AnonymousClass1TL r4, AnonymousClass1TL r5, int i) {
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        if (i <= 2) {
            this.A00 = i;
            this.A04 = r5;
            return;
        }
        throw C12970iu.A0f(C12960it.A0W(i, "invalid encoding value: "));
    }

    public static final AnonymousClass1TL A00(C94954co r1, int i) {
        if (r1.A00 > i) {
            return r1.A05(i).Aer();
        }
        throw C12970iu.A0f("too few objects in input vector");
    }

    public static void A01(OutputStream outputStream, String str, AnonymousClass1TM r3) {
        if (r3 != null) {
            outputStream.write(r3.A02(str));
        }
    }
}
