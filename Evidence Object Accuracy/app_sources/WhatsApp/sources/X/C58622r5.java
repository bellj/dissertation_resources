package X;

import android.transition.Transition;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.2r5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58622r5 extends AbstractC100714mM {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C58622r5(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        super.onTransitionEnd(transition);
        ViewProfilePhoto viewProfilePhoto = this.A00;
        View findViewById = viewProfilePhoto.findViewById(R.id.picture);
        View findViewById2 = viewProfilePhoto.findViewById(R.id.picture_animation);
        findViewById.setVisibility(0);
        findViewById2.setVisibility(4);
        viewProfilePhoto.getWindow().setStatusBarColor(-16777216);
    }
}
