package X;

import android.util.Log;
import com.facebook.soloader.SoLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.1QH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1QH implements AnonymousClass12B {
    public final /* synthetic */ Runtime A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ Method A03;
    public final /* synthetic */ boolean A04;

    public AnonymousClass1QH(Runtime runtime, String str, String str2, Method method, boolean z) {
        this.A04 = z;
        this.A01 = str;
        this.A02 = str2;
        this.A00 = runtime;
        this.A03 = method;
    }

    @Override // X.AnonymousClass12B
    public void AKV(String str, int i) {
        String str2;
        MessageDigest instance;
        FileInputStream fileInputStream;
        Throwable e;
        if (this.A04) {
            String str3 = null;
            String str4 = (i & 4) == 4 ? this.A01 : this.A02;
            try {
                Runtime runtime = this.A00;
                try {
                    synchronized (runtime) {
                        try {
                            String str5 = (String) this.A03.invoke(runtime, str, SoLoader.class.getClassLoader(), str4);
                            if (str5 != null) {
                                throw new UnsatisfiedLinkError(str5);
                            }
                        } catch (Throwable th) {
                            th = th;
                            str3 = null;
                            try {
                                try {
                                    throw th;
                                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                                    e = e2;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Error: Cannot load ");
                                    sb.append(str);
                                    str3 = sb.toString();
                                    throw new RuntimeException(str3, e);
                                }
                            } catch (Throwable th2) {
                                if (str3 != null) {
                                    StringBuilder sb2 = new StringBuilder("Error when loading lib: ");
                                    sb2.append(str3);
                                    sb2.append(" lib hash: ");
                                    try {
                                        File file = new File(str);
                                        instance = MessageDigest.getInstance("MD5");
                                        fileInputStream = new FileInputStream(file);
                                    } catch (IOException | SecurityException | NoSuchAlgorithmException e3) {
                                        str2 = e3.toString();
                                    }
                                    try {
                                        byte[] bArr = new byte[4096];
                                        while (true) {
                                            int read = fileInputStream.read(bArr);
                                            if (read <= 0) {
                                                break;
                                            }
                                            instance.update(bArr, 0, read);
                                        }
                                        str2 = String.format("%32x", new BigInteger(1, instance.digest()));
                                        fileInputStream.close();
                                        sb2.append(str2);
                                        sb2.append(" search path is ");
                                        sb2.append(str4);
                                        Log.e("SoLoader", sb2.toString());
                                    } catch (Throwable th3) {
                                        try {
                                            throw th3;
                                        } catch (Throwable th4) {
                                            try {
                                                fileInputStream.close();
                                            } catch (Throwable unused) {
                                            }
                                            throw th4;
                                        }
                                    }
                                }
                                throw th2;
                            }
                        }
                    }
                } catch (Throwable th5) {
                    th = th5;
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e4) {
                e = e4;
            } catch (Throwable th6) {
                throw th6;
            }
        } else {
            System.load(str);
        }
    }
}
