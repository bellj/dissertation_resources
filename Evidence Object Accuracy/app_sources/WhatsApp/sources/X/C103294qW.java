package X;

import android.content.Context;
import com.whatsapp.qrcode.AuthenticationActivity;

/* renamed from: X.4qW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103294qW implements AbstractC009204q {
    public final /* synthetic */ AuthenticationActivity A00;

    public C103294qW(AuthenticationActivity authenticationActivity) {
        this.A00 = authenticationActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
