package X;

import android.view.View;
import android.view.Window;

/* renamed from: X.0Kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04230Kw {
    public static void A00(Window window, boolean z) {
        View decorView = window.getDecorView();
        int systemUiVisibility = decorView.getSystemUiVisibility();
        int i = systemUiVisibility | 1792;
        if (z) {
            i = systemUiVisibility & -1793;
        }
        decorView.setSystemUiVisibility(i);
    }
}
