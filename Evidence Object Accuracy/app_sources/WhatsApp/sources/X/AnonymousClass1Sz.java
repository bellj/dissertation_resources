package X;

import com.facebook.profilo.writer.NativeTraceWriterCallbacks;

/* renamed from: X.1Sz  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Sz implements NativeTraceWriterCallbacks {
    public final /* synthetic */ HandlerC29471Sw A00;
    public final /* synthetic */ C29441Sr A01;

    public AnonymousClass1Sz(HandlerC29471Sw r1, C29441Sr r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // com.facebook.profilo.writer.NativeTraceWriterCallbacks
    public void onTraceWriteAbort(long j, int i) {
        this.A00.A02.AXh(this.A01, i);
    }

    @Override // com.facebook.profilo.writer.NativeTraceWriterCallbacks
    public void onTraceWriteEnd(long j) {
        this.A00.A02.AXi(this.A01);
    }

    @Override // com.facebook.profilo.writer.NativeTraceWriterCallbacks
    public void onTraceWriteException(long j, Throwable th) {
        this.A00.A02.AXj(this.A01, th);
    }

    @Override // com.facebook.profilo.writer.NativeTraceWriterCallbacks
    public void onTraceWriteStart(long j, int i) {
        this.A00.A02.AXk(this.A01);
    }
}
