package X;

import java.lang.reflect.Method;

/* renamed from: X.4Qm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91114Qm {
    public final Method A00;
    public final Method A01;
    public final Method A02;

    public C91114Qm(Method method, Method method2, Method method3) {
        this.A01 = method;
        this.A00 = method2;
        this.A02 = method3;
    }
}
