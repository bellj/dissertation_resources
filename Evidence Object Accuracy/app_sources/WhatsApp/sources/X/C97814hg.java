package X;

import android.database.sqlite.SQLiteTransactionListener;

/* renamed from: X.4hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97814hg implements SQLiteTransactionListener {
    public final /* synthetic */ AnonymousClass1C1 A00;

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onBegin() {
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onCommit() {
    }

    public /* synthetic */ C97814hg(AnonymousClass1C1 r1) {
        this.A00 = r1;
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onRollback() {
        AnonymousClass1C1 r1 = this.A00;
        synchronized (r1) {
            r1.A00 = null;
        }
    }
}
