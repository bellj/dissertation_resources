package X;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.google.android.gms.common.SupportErrorDialogFragment;
import com.whatsapp.R;

/* renamed from: X.29i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C471729i extends C471929k {
    public static final C471729i A00 = new C471729i();
    public static final Object A01 = new Object();

    public static final Dialog A00(Context context, DialogInterface.OnCancelListener onCancelListener, AbstractDialogInterface$OnClickListenerC472029l r7, int i) {
        AlertDialog.Builder builder;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(C472129m.A01(context, i));
        builder.setOnCancelListener(onCancelListener);
        Resources resources = context.getResources();
        int i2 = R.string.common_google_play_services_install_button;
        if (i != 1) {
            i2 = R.string.common_google_play_services_update_button;
            if (i != 2) {
                i2 = R.string.common_google_play_services_enable_button;
                if (i != 3) {
                    i2 = 17039370;
                }
            }
        }
        String string = resources.getString(i2);
        if (string != null) {
            builder.setPositiveButton(string, r7);
        }
        String A02 = C472129m.A02(context, i);
        if (A02 != null) {
            builder.setTitle(A02);
        }
        Log.w("GoogleApiAvailability", String.format("Creating dialog for Google Play services availability issue. ConnectionResult=%s", Integer.valueOf(i)), new IllegalArgumentException());
        return builder.create();
    }

    public static final void A01(Activity activity, Dialog dialog, DialogInterface.OnCancelListener onCancelListener, String str) {
        try {
            if (activity instanceof ActivityC000900k) {
                AnonymousClass01F A0V = ((ActivityC000900k) activity).A0V();
                SupportErrorDialogFragment supportErrorDialogFragment = new SupportErrorDialogFragment();
                C13020j0.A02(dialog, "Cannot display null dialog");
                dialog.setOnCancelListener(null);
                dialog.setOnDismissListener(null);
                supportErrorDialogFragment.A00 = dialog;
                supportErrorDialogFragment.A02 = onCancelListener;
                supportErrorDialogFragment.A1F(A0V, str);
                return;
            }
        } catch (NoClassDefFoundError unused) {
        }
        FragmentManager fragmentManager = activity.getFragmentManager();
        AnonymousClass29o r1 = new AnonymousClass29o();
        C13020j0.A02(dialog, "Cannot display null dialog");
        dialog.setOnCancelListener(null);
        dialog.setOnDismissListener(null);
        r1.A00 = dialog;
        r1.A02 = onCancelListener;
        r1.show(fragmentManager, str);
    }

    public final C472229q A02(Context context, AnonymousClass29p r5) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        C472229q r1 = new C472229q(r5);
        context.registerReceiver(r1, intentFilter);
        r1.A00 = context;
        if (C472329r.A03(context)) {
            return r1;
        }
        r5.A00();
        r1.A00();
        return null;
    }

    public final void A03(PendingIntent pendingIntent, Context context, int i) {
        String A02;
        String format;
        int i2;
        Log.w("GoogleApiAvailability", String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", Integer.valueOf(i), null), new IllegalArgumentException());
        if (i == 18) {
            new HandlerC472429s(context, this).sendEmptyMessageDelayed(1, 120000);
        } else if (pendingIntent != null) {
            if (i == 6) {
                A02 = C472129m.A03(context, "common_google_play_services_resolution_required_title");
            } else {
                A02 = C472129m.A02(context, i);
            }
            if (A02 == null) {
                A02 = context.getResources().getString(R.string.common_google_play_services_notification_ticker);
            }
            if (i == 6 || i == 19) {
                String A002 = C472129m.A00(context);
                Resources resources = context.getResources();
                String A03 = C472129m.A03(context, "common_google_play_services_resolution_required_text");
                if (A03 == null) {
                    A03 = resources.getString(R.string.common_google_play_services_unknown_issue);
                }
                format = String.format(resources.getConfiguration().locale, A03, A002);
            } else {
                format = C472129m.A01(context, i);
            }
            Resources resources2 = context.getResources();
            Object systemService = context.getSystemService("notification");
            C13020j0.A01(systemService);
            NotificationManager notificationManager = (NotificationManager) systemService;
            C005602s r2 = new C005602s(context, null);
            r2.A0T = true;
            r2.A0D(true);
            r2.A0A(A02);
            NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
            notificationCompat$BigTextStyle.A09(format);
            r2.A08(notificationCompat$BigTextStyle);
            PackageManager packageManager = context.getPackageManager();
            Boolean bool = C472629u.A00;
            if (bool == null) {
                boolean z = false;
                if (C472729v.A01() && packageManager.hasSystemFeature("android.hardware.type.watch")) {
                    z = true;
                }
                bool = Boolean.valueOf(z);
                C472629u.A00 = bool;
            }
            if (!bool.booleanValue()) {
                r2.A07.icon = 17301642;
                r2.A0B(resources2.getString(R.string.common_google_play_services_notification_ticker));
                r2.A05(System.currentTimeMillis());
                r2.A09 = pendingIntent;
                r2.A09(format);
            } else if (C472729v.A01()) {
                r2.A07.icon = context.getApplicationInfo().icon;
                r2.A03 = 2;
                if (C472629u.A00(context)) {
                    r2.A04(R.drawable.common_full_open_on_phone, resources2.getString(R.string.common_open_on_phone), pendingIntent);
                } else {
                    r2.A09 = pendingIntent;
                }
            } else {
                throw new IllegalStateException();
            }
            if (C472729v.A03()) {
                synchronized (A01) {
                }
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
                String string = context.getResources().getString(R.string.common_google_play_services_notification_channel_name);
                if (notificationChannel == null) {
                    notificationChannel = new NotificationChannel("com.google.android.gms.availability", string, 4);
                } else {
                    if (!string.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(string);
                    }
                    r2.A0J = "com.google.android.gms.availability";
                }
                notificationManager.createNotificationChannel(notificationChannel);
                r2.A0J = "com.google.android.gms.availability";
            }
            Notification A012 = r2.A01();
            if (i == 1 || i == 2 || i == 3) {
                C472329r.A02.set(false);
                i2 = 10436;
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, A012);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
