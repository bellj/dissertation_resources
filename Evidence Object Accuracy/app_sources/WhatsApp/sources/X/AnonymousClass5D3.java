package X;

import java.util.Enumeration;

/* renamed from: X.5D3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5D3 implements Enumeration {
    public final Enumeration A00;
    public final /* synthetic */ AnonymousClass5MN A01;

    public AnonymousClass5D3(Enumeration enumeration, AnonymousClass5MN r2) {
        this.A01 = r2;
        this.A00 = enumeration;
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return this.A00.hasMoreElements();
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        Object nextElement = this.A00.nextElement();
        if (nextElement instanceof AnonymousClass5MS) {
            return nextElement;
        }
        if (nextElement != null) {
            return new AnonymousClass5MS(AbstractC114775Na.A04(nextElement));
        }
        return null;
    }
}
