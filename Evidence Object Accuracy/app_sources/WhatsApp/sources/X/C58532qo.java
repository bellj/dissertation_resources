package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaFrameLayout;

/* renamed from: X.2qo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58532qo extends WaFrameLayout {
    public final TextEmojiLabel A00;

    public C58532qo(Context context) {
        super(context, null);
        LayoutInflater.from(context).inflate(R.layout.conversation_row_im_header_select_list, (ViewGroup) this, true);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.im_header_title);
        this.A00 = A0T;
        A0T.setLongClickable(AbstractC28491Nn.A07(A0T));
    }
}
