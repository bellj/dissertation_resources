package X;

import com.google.android.exoplayer2.Timeline;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3Sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67623Sf implements AnonymousClass5XZ {
    public final /* synthetic */ AnonymousClass36Q A00;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AQ4(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARX(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARY(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ASS(AnonymousClass4XL r1, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATm(boolean z, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATo(C94344be r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATq(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATr(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATx(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AVk() {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AWU(List list) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXV(Timeline timeline, int i) {
        AnonymousClass4DD.A00(this, timeline, i);
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXW(Timeline timeline, Object obj, int i) {
    }

    public /* synthetic */ C67623Sf(AnonymousClass36Q r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XZ
    public void ATs(AnonymousClass3A1 r3) {
        Log.e(C12960it.A0d(r3.toString(), C12960it.A0k("exoaudioplayer/onPlayerError: ")));
    }

    @Override // X.AnonymousClass5XZ
    public void ATt(boolean z, int i) {
        if (i == 3) {
            AnonymousClass36Q r3 = this.A00;
            if (!r3.A05) {
                int ACb = (int) r3.A07.ACb();
                r3.A00 = ACb;
                AnonymousClass4KR r1 = r3.A04;
                if (r1 != null) {
                    r3.A05 = true;
                    r1.A00.A03 = ACb;
                }
            }
        }
    }

    @Override // X.AnonymousClass5XZ
    public void AXl(C100564m7 r6, C92524Wg r7) {
        C77353n9 r0;
        AnonymousClass36Q r4 = this.A00;
        if (r6 != r4.A01 && (r0 = r4.A08) != null) {
            AnonymousClass4W3 r02 = r0.A00;
            if (r02 != null && r02.A00(1) == 1) {
                AbstractC15710nm r3 = r4.A03;
                if (r3 != null) {
                    r3.AaV("exoaudioplayer/audio-track-not-playable", null, false);
                }
                Log.e("exoaudioplayer/onTracksChanged: Media includes audio tracks, but none are playable by this device");
            }
            r4.A01 = r6;
        }
    }
}
