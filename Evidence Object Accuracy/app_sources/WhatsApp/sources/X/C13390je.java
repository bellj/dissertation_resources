package X;

/* renamed from: X.0je  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13390je {
    public final Class A00;
    public final boolean A01;

    public /* synthetic */ C13390je(Class cls, boolean z) {
        this.A00 = cls;
        this.A01 = z;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C13390je)) {
            return false;
        }
        C13390je r4 = (C13390je) obj;
        if (!r4.A00.equals(this.A00) || r4.A01 != this.A01) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.A00.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.A01).hashCode();
    }
}
