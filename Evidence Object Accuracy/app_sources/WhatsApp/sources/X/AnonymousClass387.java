package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.387  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass387 extends AbstractC16350or {
    public final C254919p A00;
    public final C15610nY A01;
    public final C14950mJ A02;
    public final C255219s A03;
    public final C15370n3 A04;
    public final WeakReference A05;
    public final WeakReference A06;
    public final boolean A07;

    public AnonymousClass387(Context context, AbstractC13860kS r3, C254919p r4, C15610nY r5, C14950mJ r6, C255219s r7, C15370n3 r8, boolean z) {
        this.A05 = C12970iu.A10(context);
        this.A02 = r6;
        this.A06 = C12970iu.A10(r3);
        this.A07 = z;
        this.A03 = r7;
        this.A01 = r5;
        this.A04 = r8;
        this.A00 = r4;
    }

    /* JADX INFO: finally extract failed */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01ce A[Catch: all -> 0x0239, TryCatch #13 {SQLiteException -> 0x0247, blocks: (B:8:0x005e, B:127:0x023e, B:9:0x0064, B:126:0x0235, B:11:0x0089, B:13:0x008f, B:14:0x0096, B:16:0x00a4, B:44:0x00e3, B:46:0x00e9, B:47:0x00f1, B:49:0x00f5, B:50:0x00fb, B:52:0x0103, B:53:0x0109, B:55:0x011e, B:57:0x012f, B:58:0x0132, B:61:0x0139, B:63:0x013d, B:65:0x0146, B:80:0x018a, B:83:0x0197, B:85:0x0199, B:86:0x019e, B:90:0x01ab, B:92:0x01af, B:94:0x01b6, B:96:0x01ba, B:98:0x01be, B:101:0x01c8, B:103:0x01ce, B:104:0x01d2, B:106:0x01d6, B:107:0x01db, B:111:0x01fd, B:113:0x0209, B:115:0x020f, B:117:0x0215, B:119:0x0221, B:121:0x0227, B:123:0x022b, B:124:0x022f), top: B:380:0x005e }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x0235 A[ADDED_TO_REGION, EDGE_INSN: B:398:0x0235->B:126:0x0235 ?: BREAK  , SYNTHETIC] */
    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r38) {
        /*
        // Method dump skipped, instructions count: 2046
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass387.A05(java.lang.Object[]):java.lang.Object");
    }
}
