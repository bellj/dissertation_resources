package X;

import com.whatsapp.account.delete.DeleteAccountConfirmation;

/* renamed from: X.3UU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UU implements AnonymousClass108 {
    public final /* synthetic */ DeleteAccountConfirmation A00;

    public AnonymousClass3UU(DeleteAccountConfirmation deleteAccountConfirmation) {
        this.A00 = deleteAccountConfirmation;
    }

    @Override // X.AnonymousClass108
    public void AS9() {
        DeleteAccountConfirmation deleteAccountConfirmation = this.A00;
        C36021jC.A00(deleteAccountConfirmation, 1);
        deleteAccountConfirmation.A2G(C14960mK.A01(deleteAccountConfirmation), true);
    }

    @Override // X.AnonymousClass108
    public void ASA() {
        DeleteAccountConfirmation deleteAccountConfirmation = this.A00;
        C19550uI r0 = deleteAccountConfirmation.A07;
        C16820po r4 = C17750rK.A00;
        if (r0.A00(r4) != null) {
            C19550uI r3 = deleteAccountConfirmation.A07;
            C1107556t r2 = new C1107556t(deleteAccountConfirmation);
            ((AnonymousClass17H) r3.A02.get()).A01(new AnonymousClass302(r2, r2, r3, r4), r4);
        }
        deleteAccountConfirmation.A01.removeMessages(0);
    }
}
