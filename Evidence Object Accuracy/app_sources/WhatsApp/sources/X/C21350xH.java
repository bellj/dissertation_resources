package X;

import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0xH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21350xH {
    public final Map A00 = new HashMap();
    public final Map A01 = new C246516i(500);

    public void A00(UserJid userJid) {
        synchronized (this) {
            Map map = this.A00;
            AnonymousClass4JW r0 = (AnonymousClass4JW) map.get(userJid);
            if (r0 != null) {
                for (C44691zO r02 : r0.A00) {
                    this.A01.remove(r02.A0D);
                }
            }
            map.remove(userJid);
        }
    }
}
