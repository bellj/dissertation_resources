package X;

import com.facebook.redex.IDxObserverShape4S0100000_2_I1;

/* renamed from: X.4QG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QG {
    public boolean A00 = false;
    public final AnonymousClass016 A01;
    public final C92664Wv A02;

    public AnonymousClass4QG(AnonymousClass016 r3, C92664Wv r4) {
        this.A02 = r4;
        this.A01 = r3;
        r3.A08(new IDxObserverShape4S0100000_2_I1(this, 41));
    }
}
