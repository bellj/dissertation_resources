package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3ft  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73233ft extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73233ft() {
        attachInterface(this, "com.google.android.gms.safetynet.internal.ISafetyNetCallbacks");
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        AbstractBinderC79953rT r1 = (AbstractBinderC79953rT) this;
        if (i == 1) {
            Status status = (Status) C12970iu.A0F(parcel, Status.CREATOR);
            C77973oA r2 = (C77973oA) C12970iu.A0F(parcel, C77973oA.CREATOR);
            if (!(r1 instanceof BinderC79923rQ)) {
                throw C12970iu.A0z();
            }
            ((BinderC79923rQ) r1).A00.A05(new C1091150l(status, r2));
            return true;
        } else if (i == 2) {
            parcel.readString();
            throw C12970iu.A0z();
        } else if (i == 3) {
            C12970iu.A0F(parcel, Status.CREATOR);
            C12970iu.A0F(parcel, C78573p8.CREATOR);
            throw C12970iu.A0z();
        } else if (i == 4) {
            C12970iu.A0F(parcel, Status.CREATOR);
            parcel.readInt();
            throw C12970iu.A0z();
        } else if (i == 6) {
            C12970iu.A0F(parcel, Status.CREATOR);
            C12970iu.A0F(parcel, C77983oB.CREATOR);
            throw C12970iu.A0z();
        } else if (i == 8) {
            Status status2 = (Status) C12970iu.A0F(parcel, Status.CREATOR);
            C78393oq r22 = (C78393oq) C12970iu.A0F(parcel, C78393oq.CREATOR);
            if (!(r1 instanceof BinderC79933rR)) {
                throw C12970iu.A0z();
            }
            ((BinderC79933rR) r1).A00.A05(new C1091250m(status2, r22));
            return true;
        } else if (i == 10) {
            C12970iu.A0F(parcel, Status.CREATOR);
            parcel.readInt();
            throw C12970iu.A0z();
        } else if (i == 11) {
            C12970iu.A0F(parcel, Status.CREATOR);
            throw C12970iu.A0z();
        } else if (i == 15) {
            C12970iu.A0F(parcel, Status.CREATOR);
            C12970iu.A0F(parcel, C78063oJ.CREATOR);
            throw C12970iu.A0z();
        } else if (i != 16) {
            return false;
        } else {
            C12970iu.A0F(parcel, Status.CREATOR);
            parcel.readString();
            parcel.readInt();
            throw C12970iu.A0z();
        }
    }
}
