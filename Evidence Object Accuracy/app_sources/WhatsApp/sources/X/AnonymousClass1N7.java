package X;

import com.whatsapp.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Map;

/* renamed from: X.1N7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1N7 {
    public final AnonymousClass02W A00;
    public final AnonymousClass02Y A01;
    public final AnonymousClass15Y A02;
    public final AnonymousClass1N8 A03;

    public AnonymousClass1N7(AnonymousClass15Y r3, AnonymousClass1N8 r4) {
        AnonymousClass02Y r1 = new AnonymousClass02Y();
        AnonymousClass02W r0 = new AnonymousClass02W();
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r1;
        this.A00 = r0;
    }

    public int A00() {
        return this.A01.A02.size() + this.A00.A02.size();
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081 A[LOOP:0: B:24:0x007b->B:26:0x0081, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r10 = this;
            X.02W r0 = r10.A00
            r0.A07()
            X.1N8 r2 = r10.A03
            int r6 = r2.A04
            r4 = 2
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r0 = java.lang.System.currentTimeMillis()
            if (r6 != r4) goto L_0x009d
            long r3 = r3.toHours(r0)
            r0 = 3600(0xe10, double:1.7786E-320)
            long r3 = r3 * r0
        L_0x0019:
            X.15Y r5 = r10.A02
            r1 = 47
            java.lang.Long r0 = java.lang.Long.valueOf(r3)
            r5.A01(r0, r1, r6)
            r7 = 3433(0xd69, float:4.81E-42)
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r0 = java.lang.System.currentTimeMillis()
            long r3 = r3.toSeconds(r0)
            r0 = 86400(0x15180, double:4.26873E-319)
            long r3 = r3 / r0
            X.23W r8 = r2.A06
            long r0 = r8.A03
            int r9 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r9 == 0) goto L_0x0053
            int r1 = r2.A03
            if (r1 == 0) goto L_0x0099
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            int r0 = r0.nextInt(r1)
            if (r0 != 0) goto L_0x0099
            r0 = 0
            r8.A02 = r0
            r0 = 1
        L_0x004f:
            r8.A04 = r0
            r8.A03 = r3
        L_0x0053:
            boolean r0 = r8.A04
            if (r0 == 0) goto L_0x0062
            int r0 = r8.A02
            int r0 = r0 + 1
            if (r0 > 0) goto L_0x0092
            r0 = 0
            r8.A02 = r0
            r8.A04 = r0
        L_0x0062:
            r0 = 0
        L_0x0063:
            r5.A01(r0, r7, r6)
            r0 = 1
            if (r6 == r0) goto L_0x008f
            if (r6 == 0) goto L_0x008f
            X.1Md r0 = r5.A01
        L_0x006d:
            java.util.Map r0 = r0.A00
            java.util.Set r0 = r0.keySet()
            java.util.Collection r0 = java.util.Collections.unmodifiableCollection(r0)
            java.util.Iterator r1 = r0.iterator()
        L_0x007b:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00a3
            java.lang.Object r0 = r1.next()
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            r10.A02(r0)
            goto L_0x007b
        L_0x008f:
            X.1Md r0 = r5.A00
            goto L_0x006d
        L_0x0092:
            r8.A02 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0063
        L_0x0099:
            r0 = 0
            r8.A02 = r0
            goto L_0x004f
        L_0x009d:
            long r3 = r3.toSeconds(r0)
            goto L_0x0019
        L_0x00a3:
            X.1N9 r0 = r2.A00
            X.1NA r1 = r0.A8e()
            boolean r0 = r1.A04()
            if (r0 == 0) goto L_0x00d4
            X.1Md r0 = r1.A03
            java.util.Map r0 = r0.A00
            java.util.Set r0 = r0.keySet()
            java.util.Collection r0 = java.util.Collections.unmodifiableCollection(r0)
            java.util.Iterator r1 = r0.iterator()
        L_0x00bf:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00d3
            java.lang.Object r0 = r1.next()
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            r10.A02(r0)
            goto L_0x00bf
        L_0x00d3:
            return
        L_0x00d4:
            java.lang.String r1 = "No attribute codes available for rotated buffers"
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N7.A01():void");
    }

    public final void A02(int i) {
        C28211Md r0;
        C28221Me r02;
        Object obj;
        AnonymousClass15Y r3 = this.A02;
        AnonymousClass1N8 r2 = this.A03;
        int i2 = r2.A04;
        if (i2 == 1 || i2 == 0) {
            r0 = r3.A00;
        } else {
            r0 = r3.A01;
        }
        Map map = r0.A00;
        Integer valueOf = Integer.valueOf(i);
        if (!map.containsKey(valueOf)) {
            r02 = C28211Md.A01;
        } else {
            r02 = (C28221Me) map.get(valueOf);
        }
        AnonymousClass1NA A8e = r2.A00.A8e();
        if (A8e.A04()) {
            Map map2 = A8e.A03.A00;
            if (!map2.containsKey(valueOf)) {
                obj = C28211Md.A01;
            } else {
                obj = map2.get(valueOf);
            }
            AnonymousClass02W r4 = this.A00;
            Map map3 = r4.A00;
            if (!map3.containsKey(valueOf) && !r02.equals(obj)) {
                r4.A09(r02.A00, 0, i);
                map3.put(valueOf, r02);
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("No attribute value available for rotated buffers");
    }

    public void A03(AbstractC16110oT r5, int i) {
        AnonymousClass02Y r3 = this.A01;
        r3.A07();
        r3.A09(Integer.valueOf(i), 1, r5.code);
        r5.serialize(new AnonymousClass1N6() { // from class: X.2CR
            @Override // X.AnonymousClass1N6
            public final void Abe(int i2, Object obj) {
                AnonymousClass1N7 r0 = AnonymousClass1N7.this;
                if (obj != null) {
                    r0.A01.A09(obj, 2, i2);
                }
            }
        });
        r3.A08((byte) (r3.A02.A01()[r3.A00] | 4));
    }

    public void A04(byte[] bArr, int i, int i2) {
        AnonymousClass02Y r3 = this.A01;
        r3.A07();
        r3.A09(Integer.valueOf(i2), 1, i);
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        while (wrap.position() < wrap.limit()) {
            try {
                try {
                    AnonymousClass04W A05 = AnonymousClass02X.A05(wrap);
                    r3.A09(A05.A02, 2, A05.A00);
                } catch (BufferUnderflowException unused) {
                    throw new AnonymousClass2CC("Incomplete buffer");
                }
            } catch (AnonymousClass2CC e) {
                Log.e("EventSerialBuffer/putSerializedEvent Error adding serialized event to buffer", e);
            }
        }
        r3.A08((byte) (r3.A02.A01()[r3.A00] | 4));
    }
}
