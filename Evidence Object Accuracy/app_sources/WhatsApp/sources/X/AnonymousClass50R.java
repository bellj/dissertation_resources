package X;

import java.util.AbstractMap;

/* renamed from: X.50R  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50R implements AbstractC115665Sl {
    @Override // X.AbstractC115665Sl
    public final Object Agn(Object obj, Object obj2) {
        AnonymousClass5IN r0;
        AnonymousClass5IN r3 = (AnonymousClass5IN) obj;
        AbstractMap abstractMap = (AbstractMap) obj2;
        if (!abstractMap.isEmpty()) {
            if (!r3.zza) {
                if (r3.isEmpty()) {
                    r0 = new AnonymousClass5IN();
                } else {
                    r0 = new AnonymousClass5IN(r3);
                }
                r3 = r0;
            }
            if (!r3.zza) {
                throw C12970iu.A0z();
            } else if (!abstractMap.isEmpty()) {
                r3.putAll(abstractMap);
            }
        }
        return r3;
    }
}
