package X;

import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59482uo extends C37171lc {
    public final List A00;

    public C59482uo(BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel, List list) {
        super(AnonymousClass39o.A0X);
        ArrayList A0l = C12960it.A0l();
        this.A00 = A0l;
        A0l.add(new AnonymousClass40K(businessDirectorySearchQueryViewModel));
        int min = Math.min(list.size(), 14);
        for (int i = 0; i < min; i++) {
            A0l.add(new AnonymousClass40J((C48142Em) list.get(i), businessDirectorySearchQueryViewModel));
        }
        if (list.size() > 14) {
            A0l.add(new AnonymousClass40L(businessDirectorySearchQueryViewModel));
        }
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((C59482uo) obj).A00);
    }

    @Override // X.C37171lc
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.hashCode());
        return C12960it.A06(this.A00, A1a);
    }
}
