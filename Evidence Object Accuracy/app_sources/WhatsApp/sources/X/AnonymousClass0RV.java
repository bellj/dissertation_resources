package X;

import android.os.Handler;
import android.os.Looper;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: X.0RV  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0RV {
    public static final Handler A00 = new Handler(Looper.getMainLooper());
    public static final Field A01;
    public static final Field A02;
    public static final Method A03;
    public static final Method A04;
    public static final Method A05;

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0079, code lost:
        if (r2 == 27) goto L_0x007b;
     */
    static {
        /*
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            android.os.Handler r0 = new android.os.Handler
            r0.<init>(r1)
            X.AnonymousClass0RV.A00 = r0
            java.lang.String r0 = "android.app.ActivityThread"
            java.lang.Class r5 = java.lang.Class.forName(r0)     // Catch: all -> 0x0012
            goto L_0x0013
        L_0x0012:
            r5 = 0
        L_0x0013:
            java.lang.Class<android.app.Activity> r1 = android.app.Activity.class
            java.lang.String r0 = "mMainThread"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r0)     // Catch: all -> 0x0020
            r0 = 1
            r1.setAccessible(r0)     // Catch: all -> 0x0020
            goto L_0x0021
        L_0x0020:
            r1 = 0
        L_0x0021:
            X.AnonymousClass0RV.A01 = r1
            java.lang.Class<android.app.Activity> r1 = android.app.Activity.class
            java.lang.String r0 = "mToken"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r0)     // Catch: all -> 0x0030
            r0 = 1
            r1.setAccessible(r0)     // Catch: all -> 0x0030
            goto L_0x0031
        L_0x0030:
            r1 = 0
        L_0x0031:
            X.AnonymousClass0RV.A02 = r1
            r6 = 0
            if (r5 == 0) goto L_0x0052
            java.lang.String r4 = "performStopActivity"
            r0 = 3
            java.lang.Class[] r3 = new java.lang.Class[r0]     // Catch: all -> 0x0052
            r1 = 0
            java.lang.Class<android.os.IBinder> r0 = android.os.IBinder.class
            r3[r1] = r0     // Catch: all -> 0x0052
            java.lang.Class r0 = java.lang.Boolean.TYPE     // Catch: all -> 0x0052
            r2 = 1
            r3[r2] = r0     // Catch: all -> 0x0052
            r1 = 2
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r3[r1] = r0     // Catch: all -> 0x0052
            java.lang.reflect.Method r0 = r5.getDeclaredMethod(r4, r3)     // Catch: all -> 0x0052
            r0.setAccessible(r2)     // Catch: all -> 0x0052
            r6 = r0
        L_0x0052:
            X.AnonymousClass0RV.A04 = r6
            r4 = 0
            if (r5 == 0) goto L_0x006e
            java.lang.String r3 = "performStopActivity"
            r0 = 2
            java.lang.Class[] r2 = new java.lang.Class[r0]     // Catch: all -> 0x006e
            r1 = 0
            java.lang.Class<android.os.IBinder> r0 = android.os.IBinder.class
            r2[r1] = r0     // Catch: all -> 0x006e
            java.lang.Class r0 = java.lang.Boolean.TYPE     // Catch: all -> 0x006e
            r1 = 1
            r2[r1] = r0     // Catch: all -> 0x006e
            java.lang.reflect.Method r0 = r5.getDeclaredMethod(r3, r2)     // Catch: all -> 0x006e
            r0.setAccessible(r1)     // Catch: all -> 0x006e
            r4 = r0
        L_0x006e:
            X.AnonymousClass0RV.A03 = r4
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r2 == r0) goto L_0x007b
            r1 = 27
            r0 = 0
            if (r2 != r1) goto L_0x007c
        L_0x007b:
            r0 = 1
        L_0x007c:
            r7 = 0
            if (r0 == 0) goto L_0x00b5
            if (r5 == 0) goto L_0x00b5
            java.lang.String r6 = "requestRelaunchActivity"
            r0 = 9
            java.lang.Class[] r4 = new java.lang.Class[r0]     // Catch: all -> 0x00b5
            r1 = 0
            java.lang.Class<android.os.IBinder> r0 = android.os.IBinder.class
            r4[r1] = r0     // Catch: all -> 0x00b5
            java.lang.Class<java.util.List> r1 = java.util.List.class
            r3 = 1
            r4[r3] = r1     // Catch: all -> 0x00b5
            r0 = 2
            r4[r0] = r1     // Catch: all -> 0x00b5
            r1 = 3
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: all -> 0x00b5
            r4[r1] = r0     // Catch: all -> 0x00b5
            r0 = 4
            java.lang.Class r2 = java.lang.Boolean.TYPE     // Catch: all -> 0x00b5
            r4[r0] = r2     // Catch: all -> 0x00b5
            r0 = 5
            java.lang.Class<android.content.res.Configuration> r1 = android.content.res.Configuration.class
            r4[r0] = r1     // Catch: all -> 0x00b5
            r0 = 6
            r4[r0] = r1     // Catch: all -> 0x00b5
            r0 = 7
            r4[r0] = r2     // Catch: all -> 0x00b5
            r0 = 8
            r4[r0] = r2     // Catch: all -> 0x00b5
            java.lang.reflect.Method r0 = r5.getDeclaredMethod(r6, r4)     // Catch: all -> 0x00b5
            r0.setAccessible(r3)     // Catch: all -> 0x00b5
            r7 = r0
        L_0x00b5:
            X.AnonymousClass0RV.A05 = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0RV.<clinit>():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r5 == 27) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(android.app.Activity r11) {
        /*
            int r5 = android.os.Build.VERSION.SDK_INT
            r10 = 1
            r0 = 28
            if (r5 < r0) goto L_0x000b
            r11.recreate()
            return r10
        L_0x000b:
            r0 = 26
            if (r5 == r0) goto L_0x0014
            r1 = 27
            r0 = 0
            if (r5 != r1) goto L_0x0015
        L_0x0014:
            r0 = 1
        L_0x0015:
            r9 = 0
            if (r0 == 0) goto L_0x001d
            java.lang.reflect.Method r0 = X.AnonymousClass0RV.A05
            if (r0 != 0) goto L_0x001d
        L_0x001c:
            return r9
        L_0x001d:
            java.lang.reflect.Method r0 = X.AnonymousClass0RV.A03
            if (r0 != 0) goto L_0x0026
            java.lang.reflect.Method r0 = X.AnonymousClass0RV.A04
            if (r0 != 0) goto L_0x0026
            return r9
        L_0x0026:
            java.lang.reflect.Field r0 = X.AnonymousClass0RV.A02     // Catch: all -> 0x0095
            java.lang.Object r1 = r0.get(r11)     // Catch: all -> 0x0095
            if (r1 == 0) goto L_0x001c
            java.lang.reflect.Field r0 = X.AnonymousClass0RV.A01     // Catch: all -> 0x0095
            java.lang.Object r7 = r0.get(r11)     // Catch: all -> 0x0095
            if (r7 == 0) goto L_0x001c
            android.app.Application r4 = r11.getApplication()     // Catch: all -> 0x0095
            X.0Uz r3 = new X.0Uz     // Catch: all -> 0x0095
            r3.<init>(r11)     // Catch: all -> 0x0095
            r4.registerActivityLifecycleCallbacks(r3)     // Catch: all -> 0x0095
            android.os.Handler r2 = X.AnonymousClass0RV.A00     // Catch: all -> 0x0095
            X.0cy r0 = new X.0cy     // Catch: all -> 0x0095
            r0.<init>(r3, r1)     // Catch: all -> 0x0095
            r2.post(r0)     // Catch: all -> 0x0095
            r0 = 26
            if (r5 == r0) goto L_0x0058
            r0 = 27
            if (r5 == r0) goto L_0x0058
            r11.recreate()     // Catch: all -> 0x008b
            goto L_0x0082
        L_0x0058:
            java.lang.reflect.Method r8 = X.AnonymousClass0RV.A05     // Catch: all -> 0x008b
            r0 = 9
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch: all -> 0x008b
            r6[r9] = r1     // Catch: all -> 0x008b
            r5 = 0
            r6[r10] = r5     // Catch: all -> 0x008b
            r0 = 2
            r6[r0] = r5     // Catch: all -> 0x008b
            r1 = 3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch: all -> 0x008b
            r6[r1] = r0     // Catch: all -> 0x008b
            r0 = 4
            java.lang.Boolean r1 = java.lang.Boolean.FALSE     // Catch: all -> 0x008b
            r6[r0] = r1     // Catch: all -> 0x008b
            r0 = 5
            r6[r0] = r5     // Catch: all -> 0x008b
            r0 = 6
            r6[r0] = r5     // Catch: all -> 0x008b
            r0 = 7
            r6[r0] = r1     // Catch: all -> 0x008b
            r0 = 8
            r6[r0] = r1     // Catch: all -> 0x008b
            r8.invoke(r7, r6)     // Catch: all -> 0x008b
        L_0x0082:
            X.0cz r0 = new X.0cz     // Catch: all -> 0x0095
            r0.<init>(r4, r3)     // Catch: all -> 0x0095
            r2.post(r0)     // Catch: all -> 0x0095
            return r10
        L_0x008b:
            r1 = move-exception
            X.0cz r0 = new X.0cz     // Catch: all -> 0x0095
            r0.<init>(r4, r3)     // Catch: all -> 0x0095
            r2.post(r0)     // Catch: all -> 0x0095
            throw r1     // Catch: all -> 0x0095
        L_0x0095:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0RV.A00(android.app.Activity):boolean");
    }
}
