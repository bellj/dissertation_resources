package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3pF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78643pF extends AnonymousClass1U5 {
    public static final C78643pF A08;
    public static final byte[][] A09;
    public static final Parcelable.Creator CREATOR = new C99044jf();
    public final String A00;
    public final byte[] A01;
    public final int[] A02;
    public final byte[][] A03;
    public final byte[][] A04;
    public final byte[][] A05;
    public final byte[][] A06;
    public final byte[][] A07;

    static {
        byte[][] bArr = new byte[0];
        A09 = bArr;
        A08 = new C78643pF("", null, null, bArr, bArr, bArr, bArr, null);
    }

    public C78643pF(String str, byte[] bArr, int[] iArr, byte[][] bArr2, byte[][] bArr3, byte[][] bArr4, byte[][] bArr5, byte[][] bArr6) {
        this.A00 = str;
        this.A01 = bArr;
        this.A03 = bArr2;
        this.A04 = bArr3;
        this.A05 = bArr4;
        this.A06 = bArr5;
        this.A02 = iArr;
        this.A07 = bArr6;
    }

    public static void A03(String str, StringBuilder sb, byte[][] bArr) {
        String str2;
        sb.append(str);
        sb.append("=");
        if (bArr == null) {
            str2 = "null";
        } else {
            sb.append("(");
            int length = bArr.length;
            boolean z = true;
            int i = 0;
            while (i < length) {
                byte[] bArr2 = bArr[i];
                if (!z) {
                    sb.append(", ");
                }
                sb.append("'");
                sb.append(Base64.encodeToString(bArr2, 3));
                sb.append("'");
                i++;
                z = false;
            }
            str2 = ")";
        }
        sb.append(str2);
    }

    public static boolean A04(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static List A00(byte[][] bArr) {
        if (bArr == null) {
            return Collections.emptyList();
        }
        int length = bArr.length;
        ArrayList A0w = C12980iv.A0w(length);
        for (byte[] bArr2 : bArr) {
            A0w.add(Base64.encodeToString(bArr2, 3));
        }
        Collections.sort(A0w);
        return A0w;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        Collection collection;
        Collection collection2;
        if (obj instanceof C78643pF) {
            C78643pF r8 = (C78643pF) obj;
            if (A04(this.A00, r8.A00) && Arrays.equals(this.A01, r8.A01) && A04(A00(this.A03), A00(r8.A03)) && A04(A00(this.A04), A00(r8.A04)) && A04(A00(this.A05), A00(r8.A05)) && A04(A00(this.A06), A00(r8.A06))) {
                int[] iArr = this.A02;
                if (iArr == null) {
                    collection = Collections.emptyList();
                } else {
                    int length = iArr.length;
                    ArrayList A0w = C12980iv.A0w(length);
                    for (int i : iArr) {
                        C12980iv.A1R(A0w, i);
                    }
                    Collections.sort(A0w);
                    collection = A0w;
                }
                int[] iArr2 = r8.A02;
                if (iArr2 == null) {
                    collection2 = Collections.emptyList();
                } else {
                    int length2 = iArr2.length;
                    ArrayList A0w2 = C12980iv.A0w(length2);
                    for (int i2 : iArr2) {
                        C12980iv.A1R(A0w2, i2);
                    }
                    Collections.sort(A0w2);
                    collection2 = A0w2;
                }
                if (A04(collection, collection2) && A04(A00(this.A07), A00(r8.A07))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public String toString() {
        String A0d;
        StringBuilder A0k = C12960it.A0k("ExperimentTokens");
        A0k.append("(");
        String str = this.A00;
        if (str == null) {
            A0d = "null";
        } else {
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 2);
            A0t.append("'");
            A0t.append(str);
            A0d = C12960it.A0d("'", A0t);
        }
        A0k.append(A0d);
        A0k.append(", ");
        byte[] bArr = this.A01;
        A0k.append("direct");
        A0k.append("=");
        if (bArr == null) {
            A0k.append("null");
        } else {
            A0k.append("'");
            A0k.append(Base64.encodeToString(bArr, 3));
            A0k.append("'");
        }
        A0k.append(", ");
        A03("GAIA", A0k, this.A03);
        A0k.append(", ");
        A03("PSEUDO", A0k, this.A04);
        A0k.append(", ");
        A03("ALWAYS", A0k, this.A05);
        A0k.append(", ");
        A03("OTHER", A0k, this.A06);
        A0k.append(", ");
        int[] iArr = this.A02;
        A0k.append("weak");
        A0k.append("=");
        if (iArr == null) {
            A0k.append("null");
        } else {
            A0k.append("(");
            int length = iArr.length;
            boolean z = true;
            int i = 0;
            while (i < length) {
                int i2 = iArr[i];
                if (!z) {
                    A0k.append(", ");
                }
                A0k.append(i2);
                i++;
                z = false;
            }
            A0k.append(")");
        }
        A0k.append(", ");
        A03("directs", A0k, this.A07);
        return C12960it.A0d(")", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0G(parcel, this.A01, 3, C95654e8.A0K(parcel, this.A00));
        C95654e8.A0J(parcel, this.A03, 4);
        C95654e8.A0J(parcel, this.A04, 5);
        C95654e8.A0J(parcel, this.A05, 6);
        C95654e8.A0J(parcel, this.A06, 7);
        C95654e8.A0H(parcel, this.A02, 8);
        C95654e8.A0J(parcel, this.A07, 9);
        C95654e8.A06(parcel, A00);
    }
}
