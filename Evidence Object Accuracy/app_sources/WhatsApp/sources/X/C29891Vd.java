package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29891Vd extends UserJid implements Parcelable {
    public static final C29891Vd A00;
    public static final Parcelable.Creator CREATOR = new C99994lC();

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public int getType() {
        return 8;
    }

    static {
        try {
            A00 = new C29891Vd();
        } catch (AnonymousClass1MW e) {
            throw new IllegalStateException(e);
        }
    }

    public C29891Vd() {
        super("Server");
    }

    public C29891Vd(Parcel parcel) {
        super(parcel);
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        return getRawString();
    }
}
