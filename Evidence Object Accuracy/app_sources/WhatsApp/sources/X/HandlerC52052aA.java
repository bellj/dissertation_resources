package X;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.account.delete.DeleteAccountConfirmation;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.2aA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52052aA extends Handler {
    public final C15510nN A00;
    public final WeakReference A01;

    public HandlerC52052aA(DeleteAccountConfirmation deleteAccountConfirmation, C15510nN r3) {
        super(Looper.getMainLooper());
        this.A00 = r3;
        this.A01 = C12970iu.A10(deleteAccountConfirmation);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        Activity activity = (Activity) this.A01.get();
        if (activity == null) {
            Log.w("delete account confirmation was garbage collected with messages still enqueued");
        } else if (message.what == 0) {
            Log.i("deleteacctconfirm/timeout/expired");
            C36021jC.A00(activity, 1);
            if (this.A00.A00() != 0) {
                Log.w("deleteacctconfirm/dialog-delete-failed");
                C36021jC.A01(activity, 3);
            }
        }
    }
}
