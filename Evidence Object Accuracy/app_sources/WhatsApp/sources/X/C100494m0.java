package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4m0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100494m0 implements Parcelable {
    public static final C100174lU CREATOR = new C100174lU();
    public final String A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100494m0(String str) {
        this.A00 = str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeString(this.A00);
    }
}
