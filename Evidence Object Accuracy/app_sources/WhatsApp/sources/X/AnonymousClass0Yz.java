package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.0Yz  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yz implements AbstractC12550i6 {
    public final /* synthetic */ RecyclerView A00;

    public AnonymousClass0Yz(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public void A00(C05390Pj r6) {
        int i = r6.A00;
        if (i == 1) {
            RecyclerView recyclerView = this.A00;
            recyclerView.A0S.A0z(recyclerView, r6.A02, r6.A01);
        } else if (i == 2) {
            RecyclerView recyclerView2 = this.A00;
            recyclerView2.A0S.A10(recyclerView2, r6.A02, r6.A01);
        } else if (i == 4) {
            RecyclerView recyclerView3 = this.A00;
            recyclerView3.A0S.A12(recyclerView3, r6.A03, r6.A02, r6.A01);
        } else if (i == 8) {
            RecyclerView recyclerView4 = this.A00;
            recyclerView4.A0S.A11(recyclerView4, r6.A02, r6.A01, 1);
        }
    }

    @Override // X.AbstractC12550i6
    public void AKs(Object obj, int i, int i2) {
        int i3;
        int i4;
        RecyclerView recyclerView = this.A00;
        AnonymousClass0QT r3 = recyclerView.A0K;
        int A01 = r3.A01();
        int i5 = i + i2;
        for (int i6 = 0; i6 < A01; i6++) {
            View A04 = r3.A04(i6);
            AnonymousClass03U A012 = RecyclerView.A01(A04);
            if (A012 != null && !A012.A06() && (i4 = A012.A05) >= i && i4 < i5) {
                int i7 = 2 | A012.A00;
                A012.A00 = i7;
                if (obj == null) {
                    A012.A00 = 1024 | i7;
                } else if ((1024 & i7) == 0) {
                    if (A012.A0E == null) {
                        ArrayList arrayList = new ArrayList();
                        A012.A0E = arrayList;
                        A012.A0F = Collections.unmodifiableList(arrayList);
                    }
                    A012.A0E.add(obj);
                }
                ((AnonymousClass0B6) A04.getLayoutParams()).A01 = true;
            }
        }
        AnonymousClass0QS r5 = recyclerView.A0w;
        ArrayList arrayList2 = r5.A06;
        int size = arrayList2.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass03U r2 = (AnonymousClass03U) arrayList2.get(size);
                if (r2 != null && (i3 = r2.A05) >= i && i3 < i5) {
                    r2.A00 = 2 | r2.A00;
                    r5.A04(size);
                }
            } else {
                recyclerView.A0l = true;
                return;
            }
        }
    }

    @Override // X.AbstractC12550i6
    public void ALl(int i, int i2) {
        RecyclerView recyclerView = this.A00;
        AnonymousClass0QT r6 = recyclerView.A0K;
        int A01 = r6.A01();
        for (int i3 = 0; i3 < A01; i3++) {
            AnonymousClass03U A012 = RecyclerView.A01(r6.A04(i3));
            if (A012 != null && !A012.A06() && A012.A05 >= i) {
                A012.A04(i2, false);
                recyclerView.A0y.A0C = true;
            }
        }
        ArrayList arrayList = recyclerView.A0w.A06;
        int size = arrayList.size();
        for (int i4 = 0; i4 < size; i4++) {
            AnonymousClass03U r1 = (AnonymousClass03U) arrayList.get(i4);
            if (r1 != null && r1.A05 >= i) {
                r1.A04(i2, true);
            }
        }
        recyclerView.requestLayout();
        recyclerView.A0k = true;
    }

    @Override // X.AbstractC12550i6
    public void ALm(int i, int i2) {
        int i3;
        int i4;
        RecyclerView recyclerView = this.A00;
        AnonymousClass0QT r10 = recyclerView.A0K;
        int A01 = r10.A01();
        int i5 = i;
        int i6 = i2;
        int i7 = 1;
        if (i < i2) {
            i7 = -1;
            i6 = i;
            i5 = i2;
        }
        for (int i8 = 0; i8 < A01; i8++) {
            AnonymousClass03U A012 = RecyclerView.A01(r10.A04(i8));
            if (A012 != null && (i4 = A012.A05) >= i6 && i4 <= i5) {
                if (i4 == i) {
                    A012.A04(i2 - i, false);
                } else {
                    A012.A04(i7, false);
                }
                recyclerView.A0y.A0C = true;
            }
        }
        AnonymousClass0QS r0 = recyclerView.A0w;
        int i9 = 1;
        int i10 = i;
        int i11 = i2;
        if (i < i2) {
            i9 = -1;
            i11 = i;
            i10 = i2;
        }
        ArrayList arrayList = r0.A06;
        int size = arrayList.size();
        for (int i12 = 0; i12 < size; i12++) {
            AnonymousClass03U r1 = (AnonymousClass03U) arrayList.get(i12);
            if (r1 != null && (i3 = r1.A05) >= i11 && i3 <= i10) {
                if (i3 == i) {
                    r1.A04(i2 - i, false);
                } else {
                    r1.A04(i9, false);
                }
            }
        }
        recyclerView.requestLayout();
        recyclerView.A0k = true;
    }
}
