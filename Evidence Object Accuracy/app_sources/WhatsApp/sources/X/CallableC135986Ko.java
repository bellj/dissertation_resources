package X;

import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135986Ko implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ CameraManager A03;
    public final /* synthetic */ CaptureRequest.Builder A04;
    public final /* synthetic */ C129455xk A05;
    public final /* synthetic */ AnonymousClass60X A06;
    public final /* synthetic */ AnonymousClass66I A07;
    public final /* synthetic */ C124855qE A08;
    public final /* synthetic */ AnonymousClass60B A09;
    public final /* synthetic */ boolean A0A;

    public CallableC135986Ko(CameraManager cameraManager, CaptureRequest.Builder builder, C129455xk r3, AnonymousClass60X r4, AnonymousClass66I r5, C124855qE r6, AnonymousClass60B r7, int i, int i2, int i3, boolean z) {
        this.A06 = r4;
        this.A09 = r7;
        this.A03 = cameraManager;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A04 = builder;
        this.A08 = r6;
        this.A0A = z;
        this.A07 = r5;
        this.A05 = r3;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass60X r1 = this.A06;
        AnonymousClass60B r7 = this.A09;
        CameraManager cameraManager = this.A03;
        int i = this.A00;
        int i2 = this.A01;
        int i3 = this.A02;
        CaptureRequest.Builder builder = this.A04;
        C124855qE r6 = this.A08;
        boolean z = this.A0A;
        r1.A02(cameraManager, builder, this.A05, this.A07, r6, r7, i, i2, i3, z);
        return null;
    }
}
