package X;

/* renamed from: X.5xG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129155xG {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C18610sj A02;
    public final C30931Zj A03 = C117305Zk.A0V("PaymentFingerprintCoordinator", "network");
    public final AnonymousClass61E A04;
    public final String A05;

    public C129155xG(C15570nT r3, C14830m7 r4, C18610sj r5, AnonymousClass61E r6, String str) {
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
        this.A04 = r6;
        this.A05 = str;
    }
}
