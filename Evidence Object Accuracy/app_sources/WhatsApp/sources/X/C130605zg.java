package X;

/* renamed from: X.5zg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130605zg {
    public final AnonymousClass6F2 A00;
    public final AnonymousClass6F2 A01;
    public final C121035h9 A02;

    public C130605zg(AnonymousClass6F2 r1, AnonymousClass6F2 r2, C121035h9 r3) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = r3;
    }

    public static C130605zg A00(AnonymousClass102 r4, AnonymousClass1V8 r5) {
        AnonymousClass6F2 r2;
        AnonymousClass6F2 r3;
        AnonymousClass1V8 A0E = r5.A0E("balance");
        if (A0E != null) {
            r3 = AnonymousClass6F2.A00(r4, A0E.A0F("primary"));
            r2 = AnonymousClass6F2.A00(r4, A0E.A0F("local"));
        } else {
            r2 = null;
            r3 = null;
        }
        return new C130605zg(r2, r3, C121035h9.A00(r4, r5.A0F("transaction"), null));
    }
}
