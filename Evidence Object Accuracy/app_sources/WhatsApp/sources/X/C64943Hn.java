package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3Hn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64943Hn {
    public static int A00(String str, List list) {
        for (int i = 0; i < list.size(); i++) {
            AnonymousClass28D A0U = C12980iv.A0U(list, i);
            if (A0U.A0H() != null && A0U.A0H().equals(str)) {
                return i;
            }
        }
        return -1;
    }

    public static Pair A01(AnonymousClass28D r5, String str) {
        List A0K = r5.A0K();
        int A00 = A00(str, A0K);
        if (A00 < 0) {
            int[] A02 = C65093Ic.A02(r5);
            int length = A02.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    A0K = r5.A0L(A02[i]);
                    A00 = A00(str, A0K);
                    if (A00 >= 0) {
                        break;
                    }
                    i++;
                } else {
                    A0K = Collections.EMPTY_LIST;
                    A00 = -1;
                    break;
                }
            }
        }
        return C12960it.A0D(A0K, A00);
    }

    public static List A02(List list) {
        ArrayList A0y = C12980iv.A0y(list);
        for (int i = 0; i < list.size(); i++) {
            AnonymousClass28D A0U = C12980iv.A0U(list, i);
            if (A0U != null) {
                if (A0U.A01 == 13346) {
                    List A0M = A0U.A0M(32);
                    for (int i2 = 0; i2 < A0M.size(); i2++) {
                        A0y.add(A0M.get(i2));
                    }
                } else {
                    A0y.add(A0U);
                }
            }
        }
        return A0y;
    }
}
