package X;

import com.google.android.gms.common.api.Status;

/* renamed from: X.50m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1091250m implements AbstractC117105Yl {
    public final Status A00;
    public final C78393oq A01;

    public C1091250m(Status status, C78393oq r2) {
        this.A00 = status;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        return this.A00;
    }
}
