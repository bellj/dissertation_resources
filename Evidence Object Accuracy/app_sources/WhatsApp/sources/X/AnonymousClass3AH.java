package X;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.jid.Jid;
import com.whatsapp.mentions.MentionableEntry;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3AH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AH {
    public static Intent A00(Activity activity, MentionableEntry mentionableEntry, AbstractC15340mz r7, File file, List list) {
        Uri fromFile = Uri.fromFile(new File(file.getAbsolutePath()));
        ArrayList A0l = C12960it.A0l();
        A0l.add(fromFile);
        String A04 = AbstractC32741cf.A04(mentionableEntry.getStringText());
        C39341ph r1 = new C39341ph(fromFile);
        r1.A0D(A04);
        r1.A0E(AnonymousClass1Y6.A00(mentionableEntry.getMentions()));
        C453421e r3 = new C453421e(r1);
        AnonymousClass24W r2 = new AnonymousClass24W(activity);
        r2.A0C = A0l;
        r2.A01 = 9;
        r2.A0G = true;
        Bundle A0D = C12970iu.A0D();
        r3.A02(A0D);
        r2.A06 = A0D;
        if (list.size() == 1) {
            r2.A08 = C15380n4.A03((Jid) C12980iv.A0o(list));
        } else {
            r2.A0B = C15380n4.A06(list);
        }
        if (r7 != null) {
            r2.A04 = r7.A11;
            r2.A09 = C15380n4.A03(C30041Vv.A05(r7));
        }
        return r2.A00();
    }
}
