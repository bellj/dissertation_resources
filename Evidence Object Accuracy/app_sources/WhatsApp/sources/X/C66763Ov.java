package X;

import com.whatsapp.contact.picker.PhoneContactsSelector;
import java.util.ArrayList;

/* renamed from: X.3Ov  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66763Ov implements AnonymousClass07L {
    public final /* synthetic */ PhoneContactsSelector A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66763Ov(PhoneContactsSelector phoneContactsSelector) {
        this.A00 = phoneContactsSelector;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        PhoneContactsSelector phoneContactsSelector = this.A00;
        phoneContactsSelector.A0U = str;
        ArrayList A02 = C32751cg.A02(phoneContactsSelector.A0P, str);
        phoneContactsSelector.A0V = A02;
        if (A02.isEmpty()) {
            phoneContactsSelector.A0V = null;
        }
        PhoneContactsSelector.A03(phoneContactsSelector);
        return false;
    }
}
