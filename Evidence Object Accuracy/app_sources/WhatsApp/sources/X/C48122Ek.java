package X;

/* renamed from: X.2Ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48122Ek {
    public final Double A00;
    public final Double A01;
    public final Double A02;
    public final Double A03;
    public final Double A04;
    public final Double A05;
    public final String A06;
    public final String A07;

    public C48122Ek(Double d, Double d2, Double d3, Double d4, Double d5, Double d6, String str, String str2) {
        this.A05 = d;
        this.A03 = d2;
        this.A04 = d3;
        this.A01 = d4;
        this.A02 = d5;
        this.A06 = str;
        this.A07 = str2;
        this.A00 = d6;
    }

    public static C48122Ek A00() {
        return new C48122Ek(Double.valueOf(2800.0d), Double.valueOf(-23.533773d), Double.valueOf(-46.62529d), null, null, null, "São Paulo", "city_default");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x000d A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A01() {
        /*
            r6 = this;
            java.lang.String r5 = r6.A07
            int r0 = r5.hashCode()
            r4 = 0
            r3 = 3
            r2 = 2
            r1 = 1
            switch(r0) {
                case -1335157162: goto L_0x0023;
                case -1207360282: goto L_0x001a;
                case -1081415738: goto L_0x0011;
                case 1738549583: goto L_0x000e;
                default: goto L_0x000d;
            }
        L_0x000d:
            return r2
        L_0x000e:
            java.lang.String r0 = "nearest_neighborhood"
            goto L_0x0025
        L_0x0011:
            java.lang.String r0 = "manual"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            return r1
        L_0x001a:
            java.lang.String r0 = "pin_on_map"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            return r3
        L_0x0023:
            java.lang.String r0 = "device"
        L_0x0025:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x000d
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48122Ek.A01():int");
    }

    public Double A02() {
        if (!"device".equals(this.A07)) {
            return this.A03;
        }
        Double d = this.A01;
        AnonymousClass009.A05(d);
        return d;
    }

    public Double A03() {
        if (!"device".equals(this.A07)) {
            return this.A04;
        }
        Double d = this.A02;
        AnonymousClass009.A05(d);
        return d;
    }

    public boolean A04() {
        String str = this.A07;
        if ("pin_on_map".equals(str)) {
            return true;
        }
        if (!"device".equals(str)) {
            return false;
        }
        if (this.A05.doubleValue() - 800.0d <= 200.0d) {
            return true;
        }
        Double d = this.A00;
        return d != null && d.doubleValue() <= 200.0d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C48122Ek r5 = (C48122Ek) obj;
        if (this.A03.equals(r5.A03) && this.A04.equals(r5.A04)) {
            Double d = this.A01;
            Double d2 = r5.A01;
            if (d != null ? d.equals(d2) : d2 == null) {
                Double d3 = this.A02;
                Double d4 = r5.A02;
                if (d3 != null ? d3.equals(d4) : d4 == null) {
                    if (this.A07.equals(r5.A07)) {
                        Double d5 = this.A00;
                        Double d6 = r5.A00;
                        if (d5 == null) {
                            if (d6 == null) {
                                return true;
                            }
                        } else if (d5.equals(d6)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        int hashCode2;
        int hashCode3 = (this.A03.hashCode() * 43) + (this.A04.hashCode() * 43);
        Double d = this.A01;
        int i = 0;
        if (d == null) {
            hashCode = 0;
        } else {
            hashCode = d.hashCode() * 43;
        }
        int i2 = hashCode3 + hashCode;
        Double d2 = this.A02;
        if (d2 == null) {
            hashCode2 = 0;
        } else {
            hashCode2 = d2.hashCode() * 43;
        }
        int hashCode4 = i2 + hashCode2 + (this.A07.hashCode() * 43);
        Double d3 = this.A00;
        if (d3 != null) {
            i = d3.hashCode() * 43;
        }
        return hashCode4 + i;
    }
}
