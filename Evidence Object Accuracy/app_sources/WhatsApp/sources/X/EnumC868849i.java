package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC868849i extends Enum {
    public static final /* synthetic */ EnumC868849i[] A00;
    public static final EnumC868849i A01;
    public static final EnumC868849i A02;
    public static final EnumC868849i A03;
    public static final EnumC868849i A04;
    public static final EnumC868849i A05;
    public static final EnumC868849i A06;
    public static final EnumC868849i A07;
    public static final EnumC868849i A08;
    public static final EnumC868849i A09;
    public static final EnumC868849i A0A;
    public static final EnumC868849i A0B;
    public static final EnumC868849i A0C;

    static {
        EnumC868849i r14 = new EnumC868849i("FIXED_LINE", 0);
        A01 = r14;
        EnumC868849i r0 = new EnumC868849i("MOBILE", 1);
        A03 = r0;
        EnumC868849i r12 = new EnumC868849i("FIXED_LINE_OR_MOBILE", 2);
        A02 = r12;
        EnumC868849i r11 = new EnumC868849i("TOLL_FREE", 3);
        A08 = r11;
        EnumC868849i r10 = new EnumC868849i("PREMIUM_RATE", 4);
        A06 = r10;
        EnumC868849i r9 = new EnumC868849i("SHARED_COST", 5);
        A07 = r9;
        EnumC868849i r8 = new EnumC868849i("VOIP", 6);
        A0C = r8;
        EnumC868849i r7 = new EnumC868849i("PERSONAL_NUMBER", 7);
        A05 = r7;
        EnumC868849i r6 = new EnumC868849i("PAGER", 8);
        A04 = r6;
        EnumC868849i r5 = new EnumC868849i("UAN", 9);
        A09 = r5;
        EnumC868849i r4 = new EnumC868849i("VOICEMAIL", 10);
        A0B = r4;
        EnumC868849i r2 = new EnumC868849i("UNKNOWN", 11);
        A0A = r2;
        EnumC868849i[] r1 = new EnumC868849i[12];
        r1[0] = r14;
        r1[1] = r0;
        C12980iv.A1P(r12, r11, r10, r1);
        C12970iu.A1R(r9, r8, r7, r6, r1);
        r1[9] = r5;
        r1[10] = r4;
        r1[11] = r2;
        A00 = r1;
    }

    public EnumC868849i(String str, int i) {
    }

    public static EnumC868849i[] values() {
        return (EnumC868849i[]) A00.clone();
    }
}
