package X;

import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.2lO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56672lO extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115715Sq A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC56672lO(AbstractC115715Sq r2) {
        super("com.google.android.gms.maps.internal.IOnInfoWindowClickListener");
        this.A00 = r2;
    }

    @Override // X.AbstractBinderC73293fz
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        this.A00.ARO(new C36311jg(AbstractBinderC79903rO.A00(parcel.readStrongBinder())));
        parcel2.writeNoException();
        return true;
    }
}
