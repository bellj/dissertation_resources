package X;

import android.view.View;

/* renamed from: X.5d2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119175d2 extends AbstractC52172aN {
    public final Runnable A00;

    public C119175d2(Runnable runnable, int i) {
        super(i, -65536, 1711315404);
        this.A00 = runnable;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        this.A00.run();
    }
}
