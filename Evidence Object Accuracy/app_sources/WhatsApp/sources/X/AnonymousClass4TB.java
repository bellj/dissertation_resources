package X;

/* renamed from: X.4TB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TB {
    public final int A00;
    public final int A01;
    public final AnonymousClass3IM A02;
    public final short A03;
    public final short A04;
    public final byte[] A05;

    public AnonymousClass4TB(AnonymousClass3IM r1, byte[] bArr, int i, int i2, short s, short s2) {
        this.A02 = r1;
        this.A04 = s;
        this.A03 = s2;
        this.A01 = i;
        this.A05 = bArr;
        this.A00 = i2;
    }
}
