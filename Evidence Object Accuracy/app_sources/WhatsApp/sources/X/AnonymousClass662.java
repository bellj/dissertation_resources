package X;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.TypedValue;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.662 */
/* loaded from: classes4.dex */
public class AnonymousClass662 implements AbstractC1311761o {
    public static final Map A0q;
    public static volatile AnonymousClass662 A0r;
    public int A00;
    public int A01;
    public int A02;
    public Matrix A03;
    public Matrix A04;
    public Rect A05;
    public CaptureRequest.Builder A06;
    public C129535xs A07;
    public AnonymousClass61B A08;
    public C1308260c A09;
    public C124855qE A0A;
    public AbstractC1311561m A0B;
    public C119085cr A0C;
    public C119095cs A0D;
    public AbstractC130695zp A0E;
    public C129845yO A0F;
    public C129845yO A0G;
    public UUID A0H;
    public FutureTask A0I;
    public boolean A0J;
    public boolean A0K;
    public final int A0L;
    public final Context A0M;
    public final CameraManager A0N;
    public final AbstractC136406Mk A0O = new AnonymousClass669(this);
    public final C128445w7 A0P = new C128445w7(this);
    public final C125415rD A0Q = new C125415rD(this);
    public final C125425rE A0R = new C125425rE(this);
    public final C125435rF A0S = new C125435rF(this);
    public final C119045cn A0T = new C119045cn();
    public final C130175yv A0U;
    public final C130185yw A0V;
    public final C126625tB A0W = new C126625tB();
    public final AnonymousClass60X A0X;
    public final AnonymousClass61Q A0Y;
    public final C1308060a A0Z;
    public final C129775yH A0a = new C129775yH();
    public final C129775yH A0b = new C129775yH();
    public final C130055yj A0c;
    public final C1308560f A0d;
    public final Object A0e = C12970iu.A0l();
    public final Callable A0f = new IDxCallableShape15S0100000_3_I1(this, 12);
    public volatile int A0g;
    public volatile CameraDevice A0h;
    public volatile AnonymousClass66P A0i;
    public volatile AnonymousClass66I A0j;
    public volatile C124805q9 A0k;
    public volatile boolean A0l;
    public volatile boolean A0m;
    public volatile boolean A0n;
    public volatile boolean A0o;
    public volatile boolean A0p;

    static {
        HashMap A11 = C12970iu.A11();
        A0q = A11;
        Integer A0i = C12980iv.A0i();
        A11.put(A0i, A0i);
        A11.put(C12960it.A0V(), 90);
        A11.put(2, 180);
        A11.put(C12970iu.A0h(), 270);
    }

    public AnonymousClass662(Context context) {
        this.A0M = context.getApplicationContext();
        C1308560f r3 = new C1308560f();
        this.A0d = r3;
        C130055yj r2 = new C130055yj(r3);
        this.A0c = r2;
        CameraManager cameraManager = (CameraManager) context.getApplicationContext().getSystemService("camera");
        this.A0N = cameraManager;
        C130175yv r1 = new C130175yv(cameraManager, r2, r3);
        this.A0U = r1;
        this.A0X = new AnonymousClass60X(r2, r3);
        this.A0Z = new C1308060a(r1, r3);
        this.A0L = Math.round(TypedValue.applyDimension(1, 30.0f, context.getResources().getDisplayMetrics()));
        this.A0V = new C130185yw(r3);
        this.A0Y = new AnonymousClass61Q(r3);
    }

    public static AnonymousClass662 A03(Context context) {
        if (A0r == null) {
            synchronized (AnonymousClass662.class) {
                if (A0r == null) {
                    A0r = new AnonymousClass662(context);
                }
            }
        }
        return A0r;
    }

    public static /* synthetic */ void A05(AnonymousClass662 r14) {
        C1308260c r5 = r14.A09;
        if (r5 != null) {
            AbstractC130695zp r4 = r14.A0E;
            C119085cr r2 = r14.A0C;
            C119095cs r0 = r14.A0D;
            Rect rect = r14.A05;
            r5.A07 = r4;
            r5.A05 = r2;
            r5.A06 = r0;
            r5.A04 = rect;
            r5.A03 = new Rect(0, 0, rect.width(), rect.height());
            r5.A0A = C117295Zj.A1S(AbstractC130695zp.A0J, r5.A07);
            r5.A02 = C12960it.A05(r4.A01(AbstractC130695zp.A0c));
            r5.A08 = C117295Zj.A0Y(AbstractC130695zp.A0x, r4);
            r5.A09 = C117295Zj.A0Y(AbstractC130695zp.A0y, r4);
            r5.A01 = C12960it.A05(r4.A01(AbstractC130695zp.A0a));
            rect.width();
            rect.height();
            r5.A00 = C1308260c.A00(0.0f, (float) r5.A02, (float) r5.A01, -1.0f, 1.0f);
            if (r5.A06 != null) {
                C119095cs.A00(r5.A06, AbstractC130685zo.A0p, Float.valueOf(C1308260c.A00((float) r5.A02(), (float) r5.A02, (float) r5.A01, -1.0f, 1.0f)));
            }
        }
        C130185yw r52 = r14.A0V;
        C125405rC r10 = new C125405rC(r14);
        CameraManager cameraManager = r14.A0N;
        CameraDevice cameraDevice = r14.A0h;
        AbstractC130695zp r3 = r14.A0E;
        C119085cr r22 = r14.A0C;
        C1308260c r02 = r14.A09;
        AnonymousClass61Q r6 = r14.A0Y;
        r52.A04(cameraDevice, cameraManager, r10, r6, r02, r22, r3);
        C1308060a r7 = r14.A0Z;
        CameraDevice cameraDevice2 = r14.A0h;
        AbstractC130695zp r32 = r14.A0E;
        r7.A04(cameraDevice2, r52, r6, r14.A0B, r14.A0C, r32);
        AnonymousClass60X r23 = r14.A0X;
        CameraDevice cameraDevice3 = r14.A0h;
        AbstractC130695zp r11 = r14.A0E;
        r23.A01(cameraDevice3, r14.A0i, r52, r6, r7, r14.A09, r14.A0B, r14.A0C, r11, r14.A02);
    }

    public static /* synthetic */ void A06(AnonymousClass662 r7, String str) {
        C1308560f r6 = r7.A0d;
        r6.A06("Method openCamera() must run on the Optic Background Thread.");
        if (r7.A0h != null) {
            if (!r7.A0h.getId().equals(str)) {
                r7.A0C();
            } else {
                return;
            }
        }
        r7.A0Y.A0O.clear();
        CameraCharacteristics A01 = C1309160m.A01(r7.A0N, str);
        C117435Zx r4 = new C117435Zx(r7.A0P, r7.A0Q);
        CallableC135886Ke r3 = new CallableC135886Ke(r7, r4, str);
        synchronized (r6) {
            r6.A02.post(new AnonymousClass6LA(r6, "open_camera_on_camera_handler_thread", r6.A01, r3));
        }
        C130175yv r32 = r7.A0U;
        int A04 = r32.A04(str);
        r7.A00 = A04;
        C119065cp r0 = new C119065cp(r7.A0M, A01, A04);
        r7.A0E = r0;
        C119085cr r1 = new C119085cr(r0);
        r7.A0C = r1;
        r7.A0D = new C119095cs(r1);
        r7.A02 = r32.A01(r7.A00);
        r7.A05 = (Rect) A01.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        r4.A6e();
        r7.A0h = r4.AGH();
    }

    public static /* synthetic */ void A07(AnonymousClass662 r8, String str) {
        if (str == null) {
            throw new AnonymousClass6L0("Camera ID must be provided to setup camera params.");
        } else if (r8.A07 != null) {
            AbstractC1311561m r2 = r8.A0B;
            if (r2 != null) {
                AbstractC130695zp r1 = r8.A0E;
                if (r1 == null) {
                    throw C12960it.A0U("Trying to setup camera params without a Capabilities.");
                } else if (r8.A0C == null || r8.A0D == null) {
                    throw C12960it.A0U("Trying to setup camera params without instantiating CameraSettings.");
                } else if (r8.A0A != null) {
                    AnonymousClass61N r22 = ((AnonymousClass66J) r2).A00;
                    List A0Y = C117295Zj.A0Y(AbstractC130695zp.A0r, r1);
                    List A0Y2 = C117295Zj.A0Y(AbstractC130695zp.A0n, r8.A0E);
                    r8.A0E.A01(AbstractC130695zp.A0h);
                    List A0Y3 = C117295Zj.A0Y(AbstractC130695zp.A0v, r8.A0E);
                    C129535xs r0 = r8.A07;
                    int i = r0.A01;
                    int i2 = r0.A00;
                    r8.A0A();
                    C127235uA A04 = r22.A04(A0Y2, A0Y3, A0Y, i, i2);
                    C129845yO r4 = A04.A01;
                    if (r4 != null) {
                        C129845yO r23 = A04.A00;
                        if (r23 != null) {
                            r8.A0F = r4;
                            C119095cs r3 = r8.A0D;
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0m, r4);
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0g, r23);
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0u, null);
                            C125475rJ r24 = AbstractC130685zo.A0s;
                            C129845yO r12 = A04.A02;
                            if (r12 == null) {
                                r12 = r4;
                            }
                            ((AbstractC125485rK) r3).A00.A01(r24, r12);
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0R, Boolean.valueOf(r8.A0m));
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0h, null);
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0N, false);
                            ((AbstractC125485rK) r3).A00.A01(AbstractC130685zo.A0J, false);
                            C119095cs.A00(r3, AbstractC130685zo.A02, C12970iu.A11());
                            return;
                        }
                        throw C12990iw.A0m("Invalid picture size: 'null'");
                    }
                    throw C12990iw.A0m("Invalid preview size: 'null'");
                } else {
                    throw C12960it.A0U("Trying to setup camera params without instantiating PreviewSetupDelegate.");
                }
            } else {
                throw C12960it.A0U("Trying to setup camera params without a StartupSettings.");
            }
        } else {
            throw C12960it.A0U("Trying to setup camera params without a CameraDeviceConfig.");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0102, code lost:
        if (r24.A0F() != false) goto L_0x0104;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A08(X.AnonymousClass662 r24, java.lang.String r25) {
        /*
        // Method dump skipped, instructions count: 474
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass662.A08(X.662, java.lang.String):void");
    }

    public int A0A() {
        Number number = (Number) C12990iw.A0l(A0q, this.A01);
        if (number != null) {
            return ((this.A02 - number.intValue()) + 360) % 360;
        }
        throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Invalid display rotation value: "), this.A01));
    }

    public void A0B() {
        this.A0J = true;
    }

    public final void A0C() {
        this.A0d.A06("Method closeCamera() must run on the Optic Background Thread.");
        C1308060a r1 = this.A0Z;
        if (r1.A0D && (!this.A0p || r1.A0C)) {
            r1.A02();
        }
        A0E(false);
        this.A0V.A01();
        this.A0X.A00();
        r1.A03();
        if (this.A0h != null) {
            C119045cn r2 = this.A0T;
            r2.A00 = this.A0h.getId();
            r2.A02(0);
            this.A0h.close();
            r2.A00();
        }
        this.A0Y.A0O.clear();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006f, code lost:
        if (r6 == 180) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0071, code lost:
        r1 = -r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0072, code lost:
        r2 = (float) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0098, code lost:
        if (r6 == 180) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x009a, code lost:
        r1 = r1 - r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a7, code lost:
        if (r6 == 90) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a9, code lost:
        r2 = -r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00aa, code lost:
        r2 = (float) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b8, code lost:
        if (r6 == 270) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ba, code lost:
        r2 = r2 - r8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D() {
        /*
        // Method dump skipped, instructions count: 205
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass662.A0D():void");
    }

    public final void A0E(boolean z) {
        AnonymousClass61Q r3;
        C1308560f r5 = this.A0d;
        r5.A06("Method stopCameraPreview() must run on the Optic Background Thread.");
        synchronized (AnonymousClass61Q.A0S) {
            r3 = this.A0Y;
            r3.A0D(z);
            synchronized (this.A0e) {
                FutureTask futureTask = this.A0I;
                if (futureTask != null) {
                    r5.A08(futureTask);
                    this.A0I = null;
                }
            }
            this.A0j = null;
            this.A06 = null;
            this.A0G = null;
            this.A0X.A0G = false;
        }
        if (!r3.A0M.A00.isEmpty()) {
            AnonymousClass61K.A00(new AnonymousClass6FJ(r3));
        }
    }

    public final boolean A0F() {
        AnonymousClass61B r0 = this.A08;
        return r0 != null && (r0.A08.A00.isEmpty() ^ true);
    }

    @Override // X.AbstractC1311761o
    public void A5k(AbstractC136166Lg r4) {
        if (r4 == null) {
            throw C12970iu.A0f("Cannot add null OnPreviewFrameListener.");
        } else if (this.A08 != null) {
            boolean z = !A0F();
            boolean A01 = this.A08.A08.A01(r4);
            if (z && A01) {
                this.A0d.A07("restart_preview_to_resume_cpu_frames", new AnonymousClass6KM(this));
            }
        }
    }

    @Override // X.AbstractC1311761o
    public void A5l(C128425w5 r2) {
        if (r2 != null) {
            this.A0Y.A0L.A01(r2);
            return;
        }
        throw C12970iu.A0f("Cannot add null OnPreviewStartedListener.");
    }

    @Override // X.AbstractC1311761o
    public void A7Y(AbstractC129405xf r9, C129535xs r10, AbstractC1311561m r11, AnonymousClass6LQ r12, AnonymousClass6LR r13, String str, int i, int i2) {
        AnonymousClass616.A00();
        if (this.A0J) {
            this.A0H = this.A0c.A00(this.A0d.A00, str);
        }
        this.A0d.A00(r9, "connect", new CallableC135976Kn(r10, this, r11, i2, i));
    }

    @Override // X.AbstractC1311761o
    public boolean A8z(AbstractC129405xf r4) {
        AnonymousClass616.A00();
        AnonymousClass61Q r1 = this.A0Y;
        r1.A0L.A00();
        r1.A0M.A00();
        AnonymousClass61B r0 = this.A08;
        if (r0 != null) {
            r0.A08.A00();
            this.A08 = null;
        }
        this.A0b.A00();
        C1308260c r02 = this.A09;
        if (r02 != null) {
            r02.A0E.A00();
        }
        this.A0W.A01.clear();
        this.A0m = false;
        if (this.A0J) {
            this.A0c.A02(this.A0H);
            this.A0H = null;
        }
        this.A0d.A00(r4, "disconnect", new IDxCallableShape15S0100000_3_I1(this, 9));
        return true;
    }

    @Override // X.AbstractC1311761o
    public void AA3(int i, int i2) {
        Rect rect = new Rect(i, i2, i, i2);
        int i3 = -this.A0L;
        rect.inset(i3, i3);
        this.A0d.A00(new C118985ch(this), "focus", new AnonymousClass6KY(rect, this));
    }

    @Override // X.AbstractC1311761o
    public int ABB() {
        return this.A00;
    }

    @Override // X.AbstractC1311761o
    public AbstractC130695zp ABG() {
        AbstractC130695zp r0;
        if (isConnected() && (r0 = this.A0E) != null) {
            return r0;
        }
        throw new C136076Kx("Cannot get camera capabilities");
    }

    @Override // X.AbstractC1311761o
    public int AGc(int i) {
        if (this.A0h == null || i != this.A00) {
            return this.A0U.A01(i);
        }
        return this.A02;
    }

    @Override // X.AbstractC1311761o
    public int AHu() {
        C1308260c r0 = this.A09;
        if (r0 == null) {
            return -1;
        }
        return r0.A02();
    }

    @Override // X.AbstractC1311761o
    public boolean AI9(int i) {
        try {
            return this.A0U.A06(i) != null;
        } catch (Exception unused) {
            return false;
        }
    }

    @Override // X.AbstractC1311761o
    public void AIr(Matrix matrix, int i, int i2, int i3) {
        RectF rectF = new RectF(0.0f, 0.0f, (float) i, (float) i2);
        matrix.mapRect(rectF);
        Rect rect = this.A05;
        if (rect == null) {
            rect = (Rect) C1309160m.A01(this.A0N, this.A0U.A06(i3)).get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        }
        RectF rectF2 = new RectF(rect);
        int A0A = A0A();
        if (A0A == 90 || A0A == 270) {
            rectF2.set((float) rect.left, (float) rect.top, (float) rect.bottom, (float) rect.right);
        }
        Matrix matrix2 = new Matrix();
        matrix2.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
        float f = 1.0f;
        if (this.A00 == 1) {
            f = -1.0f;
        }
        matrix2.postScale(f, 1.0f, rectF2.width() / 2.0f, 0.0f);
        int abs = Math.abs(A0A / 90);
        Matrix matrix3 = new Matrix();
        for (int i4 = 0; i4 < abs; i4++) {
            Matrix matrix4 = new Matrix();
            float width = rectF2.width() / 2.0f;
            matrix4.setRotate(-90.0f, width, width);
            matrix4.mapRect(rectF2);
            matrix3.postConcat(matrix4);
        }
        matrix2.postConcat(matrix3);
        this.A04 = matrix2;
    }

    @Override // X.AbstractC1311761o
    public boolean AJz() {
        return this.A0Z.A0D;
    }

    @Override // X.AbstractC1311761o
    public boolean AK6() {
        return AI9(0) && AI9(1);
    }

    @Override // X.AbstractC1311761o
    public boolean AKp(float[] fArr) {
        Matrix matrix = this.A04;
        if (matrix == null) {
            return false;
        }
        matrix.mapPoints(fArr);
        return true;
    }

    @Override // X.AbstractC1311761o
    public void ALV(AbstractC129405xf r4, C128385w1 r5) {
        this.A0d.A00(r4, "modify_settings_on_background_thread", new AnonymousClass6KX(this, r5));
    }

    @Override // X.AbstractC1311761o
    public void ATI(int i) {
        this.A0g = i;
        AnonymousClass66P r1 = this.A0i;
        if (r1 != null) {
            r1.A00 = this.A0g;
        }
    }

    @Override // X.AbstractC1311761o
    public void AaL(AbstractC136166Lg r7) {
        AnonymousClass61B r0;
        if (r7 != null && (r0 = this.A08) != null && r0.A08.A02(r7) && !A0F()) {
            synchronized (this.A0e) {
                C1308560f r4 = this.A0d;
                r4.A08(this.A0I);
                this.A0I = r4.A02("restart_preview_if_to_stop_cpu_frames", this.A0f, 200);
            }
        }
    }

    @Override // X.AbstractC1311761o
    public void AaM(C128425w5 r2) {
        if (r2 != null) {
            this.A0Y.A0L.A02(r2);
        }
    }

    @Override // X.AbstractC1311761o
    public void Abs(Handler handler) {
        this.A0d.A00 = handler;
    }

    @Override // X.AbstractC1311761o
    public void Ac9(AbstractC136156Lf r2) {
        this.A0V.A02 = r2;
    }

    @Override // X.AbstractC1311761o
    public void AcO(C125385rA r3) {
        C130055yj r0 = this.A0c;
        synchronized (r0.A02) {
            r0.A00 = r3;
        }
    }

    @Override // X.AbstractC1311761o
    public void Acd(AbstractC129405xf r4, int i) {
        this.A01 = i;
        this.A0d.A00(r4, "set_rotation", new IDxCallableShape15S0100000_3_I1(this, 11));
    }

    @Override // X.AbstractC1311761o
    public void AdE(AbstractC129405xf r5, int i) {
        this.A0d.A00(null, "set_zoom_level", new AnonymousClass6KZ(this, i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r4 == 180) goto L_0x0019;
     */
    @Override // X.AbstractC1311761o
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AdG(android.graphics.Matrix r7, int r8, int r9, int r10, int r11, boolean r12) {
        /*
            r6 = this;
            X.5yO r0 = r6.A0F
            if (r0 == 0) goto L_0x007c
            r7.reset()
            float r1 = (float) r8
            float r0 = (float) r9
            r5 = 0
            android.graphics.RectF r3 = new android.graphics.RectF
            r3.<init>(r5, r5, r1, r0)
            int r4 = r6.A02
            if (r4 == 0) goto L_0x0019
            r2 = 180(0xb4, float:2.52E-43)
            float r1 = (float) r11
            float r0 = (float) r10
            if (r4 != r2) goto L_0x001b
        L_0x0019:
            float r1 = (float) r10
            float r0 = (float) r11
        L_0x001b:
            android.graphics.RectF r2 = new android.graphics.RectF
            r2.<init>(r5, r5, r1, r0)
            float r5 = r3.centerX()
            float r4 = r3.centerY()
            boolean r0 = r3.equals(r2)
            if (r0 != 0) goto L_0x0061
            float r0 = r2.centerX()
            float r1 = r5 - r0
            float r0 = r2.centerY()
            float r0 = r4 - r0
            r2.offset(r1, r0)
            android.graphics.Matrix$ScaleToFit r0 = android.graphics.Matrix.ScaleToFit.FILL
            r7.setRectToRect(r3, r2, r0)
            int r0 = java.lang.Math.max(r8, r9)
            float r2 = (float) r0
            int r0 = java.lang.Math.max(r10, r11)
            float r0 = (float) r0
            float r2 = r2 / r0
            int r0 = java.lang.Math.min(r8, r9)
            float r1 = (float) r0
            int r0 = java.lang.Math.min(r10, r11)
            float r0 = (float) r0
            float r1 = r1 / r0
            if (r12 == 0) goto L_0x0077
            float r0 = java.lang.Math.max(r2, r1)
        L_0x005e:
            r7.postScale(r0, r0, r5, r4)
        L_0x0061:
            int r3 = r6.A01
            r2 = 2
            r1 = 1
            if (r3 == r1) goto L_0x0072
            r0 = 3
            if (r3 == r0) goto L_0x0072
            if (r3 != r2) goto L_0x0071
            r0 = 1127481344(0x43340000, float:180.0)
        L_0x006e:
            r7.postRotate(r0, r5, r4)
        L_0x0071:
            return r1
        L_0x0072:
            int r3 = r3 - r2
            int r0 = r3 * 90
            float r0 = (float) r0
            goto L_0x006e
        L_0x0077:
            float r0 = java.lang.Math.min(r2, r1)
            goto L_0x005e
        L_0x007c:
            java.lang.String r0 = "Camera params need to be configured before invoking setupViewTransformMatrix()"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass662.AdG(android.graphics.Matrix, int, int, int, int, boolean):boolean");
    }

    @Override // X.AbstractC1311761o
    public void AeN(AbstractC129405xf r11, File file) {
        C1308060a r0 = this.A0Z;
        String absolutePath = file.getAbsolutePath();
        int i = this.A00;
        int i2 = this.A0g;
        AnonymousClass66P r2 = this.A0i;
        AbstractC136406Mk r4 = this.A0O;
        r0.A05(this.A06, r2, r11, r4, this.A0j, absolutePath, i, i2, A0F());
    }

    @Override // X.AbstractC1311761o
    public void AeV(AbstractC129405xf r4, boolean z) {
        C1308060a r2 = this.A0Z;
        CaptureRequest.Builder builder = this.A06;
        A0F();
        r2.A06(builder, r4, this.A0j);
    }

    @Override // X.AbstractC1311761o
    public void Aef(AbstractC129405xf r4) {
        AnonymousClass616.A00();
        this.A0d.A00(r4, "switch_camera", new IDxCallableShape15S0100000_3_I1(this, 10));
    }

    @Override // X.AbstractC1311761o
    public void Aeh(C129455xk r16, AnonymousClass60B r17) {
        int i;
        AnonymousClass60X r4 = this.A0X;
        CameraManager cameraManager = this.A0N;
        int i2 = this.A00;
        int i3 = (((this.A0g + 45) / 90) * 90) % 360;
        int i4 = this.A00;
        int i5 = this.A02;
        if (i4 == 1) {
            i = (i5 - i3) + 360;
        } else {
            i = i5 + i3;
        }
        int A0A = A0A();
        CaptureRequest.Builder builder = this.A06;
        C124855qE r9 = this.A0A;
        boolean A0F = A0F();
        r4.A03(cameraManager, builder, r16, this.A0j, r9, r17, i2, i % 360, A0A, A0F);
    }

    @Override // X.AbstractC1311761o
    public boolean isConnected() {
        return this.A0h != null && this.A0n;
    }
}
