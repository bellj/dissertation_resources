package X;

import com.whatsapp.base.WaFragment;
import com.whatsapp.businessdirectory.util.LocationUpdateListener;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment;
import com.whatsapp.payments.ui.IndiaUpiForgotPinDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiMyQrFragment;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiQrCodeScannedDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiScanQrCodeFragment;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment;
import com.whatsapp.payments.ui.NoviAddPaymentMethodFragment;
import com.whatsapp.payments.ui.NoviConfirmPaymentFragment;
import com.whatsapp.payments.ui.NoviEditTransactionDescriptionFragment;
import com.whatsapp.payments.ui.NoviGetStartedFragment;
import com.whatsapp.payments.ui.NoviTransactionMethodDetailsFragment;
import com.whatsapp.payments.ui.NoviTransactionReviewDetailsFragment;
import com.whatsapp.payments.ui.NoviWithdrawLocationDetailsSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.PaymentRailPickerFragment;
import com.whatsapp.payments.ui.fragment.NoviAddDebitCardSheet;
import com.whatsapp.payments.ui.fragment.NoviWithdrawCashReviewSheet;
import com.whatsapp.payments.ui.widget.MandateUpdateBottomSheetFragment;
import com.whatsapp.shops.ShopsProductPreviewFragment;
import com.whatsapp.wabloks.base.BkFragment;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragment;

/* renamed from: X.2Sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51112Sw extends AbstractC51092Su {
    public AnonymousClass01N A00;
    public AnonymousClass01N A01;
    public AnonymousClass01N A02;
    public AnonymousClass01N A03;
    public AnonymousClass01N A04;
    public AnonymousClass01N A05;
    public AnonymousClass01N A06;
    public AnonymousClass01N A07;
    public AnonymousClass01N A08;
    public AnonymousClass01N A09;
    public AnonymousClass01N A0A;
    public AnonymousClass01N A0B;
    public AnonymousClass01N A0C;
    public AnonymousClass01N A0D;
    public AnonymousClass01N A0E;
    public AnonymousClass01N A0F;
    public AnonymousClass01N A0G;
    public AnonymousClass01N A0H;
    public AnonymousClass01N A0I;
    public AnonymousClass01N A0J;
    public AnonymousClass01N A0K;
    public AnonymousClass01N A0L;
    public AnonymousClass01N A0M;
    public AnonymousClass01N A0N;
    public AnonymousClass01N A0O;
    public AnonymousClass01N A0P;
    public AnonymousClass01N A0Q;
    public AnonymousClass01N A0R;
    public AnonymousClass01N A0S;
    public AnonymousClass01N A0T;
    public AnonymousClass01N A0U;
    public final AnonymousClass2FL A0V;
    public final C48722Hj A0W;
    public final C51112Sw A0X = this;
    public final AnonymousClass01J A0Y;

    public /* synthetic */ C51112Sw(AnonymousClass2FL r7, C48722Hj r8, AnonymousClass01J r9) {
        this.A0Y = r9;
        this.A0W = r8;
        this.A0V = r7;
        this.A01 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 0));
        this.A0C = C19970uy.A00(new C71573d9(r7, r8, this, r9, 1));
        this.A0N = C19970uy.A00(new C71573d9(r7, r8, this, r9, 2));
        this.A0O = C19970uy.A00(new C71573d9(r7, r8, this, r9, 3));
        this.A0P = C19970uy.A00(new C71573d9(r7, r8, this, r9, 6));
        this.A0Q = C19970uy.A00(new C71573d9(r7, r8, this, r9, 5));
        this.A0R = C19970uy.A00(new C71573d9(r7, r8, this, r9, 7));
        this.A0S = C19970uy.A00(new C71573d9(r7, r8, this, r9, 8));
        this.A0T = C19970uy.A00(new C71573d9(r7, r8, this, r9, 9));
        this.A02 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 4));
        this.A03 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 10));
        this.A04 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 11));
        this.A05 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 13));
        this.A06 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 12));
        this.A07 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 14));
        this.A08 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 15));
        this.A09 = C19970uy.A00(new C71573d9(r7, r8, this, r9, 17));
        this.A0A = C19970uy.A00(new C71573d9(r7, r8, this, r9, 16));
        this.A0B = C19970uy.A00(new C71573d9(r7, r8, this, r9, 19));
        this.A0D = C19970uy.A00(new C71573d9(r7, r8, this, r9, 18));
        this.A0E = C19970uy.A00(new C71573d9(r7, r8, this, r9, 20));
        this.A0F = C19970uy.A00(new C71573d9(r7, r8, this, r9, 21));
        this.A0G = C19970uy.A00(new C71573d9(r7, r8, this, r9, 22));
        this.A0U = C19970uy.A00(new C71573d9(r7, r8, this, r9, 23));
        this.A0H = C19970uy.A00(new C71573d9(r7, r8, this, r9, 24));
        this.A0I = C19970uy.A00(new C71573d9(r7, r8, this, r9, 25));
        this.A0J = C19970uy.A00(new C71573d9(r7, r8, this, r9, 26));
        this.A0K = C19970uy.A00(new C71573d9(r7, r8, this, r9, 27));
        this.A0L = C19970uy.A00(new C71573d9(r7, r8, this, r9, 28));
        this.A0M = C19970uy.A00(new C71573d9(r7, r8, this, r9, 29));
        this.A00 = new C71573d9(r7, r8, this, r9, 30);
    }

    public final C63523Bx A00() {
        AnonymousClass01J r5 = this.A0Y;
        C14900mE r8 = (C14900mE) r5.A8X.get();
        C15570nT r9 = (C15570nT) r5.AAr.get();
        C14950mJ r10 = (C14950mJ) r5.AKf.get();
        C14840m8 r13 = (C14840m8) r5.ACi.get();
        C14840m8 r2 = (C14840m8) r5.ACi.get();
        C91684Sr r16 = new C91684Sr((C22100yW) r5.A3g.get(), (C14850m9) r5.A04.get(), (AnonymousClass139) r5.A9o.get(), r2, (C14860mA) r5.ANU.get());
        C22990zy r22 = (C22990zy) r5.AKn.get();
        C231910s r1 = (C231910s) r5.AKg.get();
        C22710zW r14 = (C22710zW) r5.AF7.get();
        return new C63523Bx(r8, r9, r10, (C22100yW) r5.A3g.get(), (C14850m9) r5.A04.get(), r13, r14, (C25871Bd) r5.ABZ.get(), r16, r1, (C21440xQ) r5.AFu.get(), r22, (AbstractC14440lR) r5.ANe.get(), (C232010t) r5.AMs.get());
    }

    public final AnonymousClass2EE A01() {
        AnonymousClass01J r1 = this.A0Y;
        AnonymousClass1CK r3 = (AnonymousClass1CK) r1.ADp.get();
        C19840ul r8 = (C19840ul) r1.A1Q.get();
        C17220qS r7 = (C17220qS) r1.ABt.get();
        AnonymousClass1CL r4 = (AnonymousClass1CL) r1.ADu.get();
        return new AnonymousClass2EE((C14650lo) r1.A2V.get(), r3, r4, (C16590pI) r1.AMg.get(), (C19870uo) r1.A8U.get(), r7, r8, (AbstractC14440lR) r1.ANe.get());
    }

    public final AnonymousClass67R A02() {
        AnonymousClass67R r1 = new AnonymousClass67R();
        r1.A00 = (C14900mE) this.A0Y.A8X.get();
        return r1;
    }

    public final LocationUpdateListener A03() {
        AnonymousClass01J r1 = this.A0Y;
        AnonymousClass018 r4 = (AnonymousClass018) r1.ANb.get();
        return new LocationUpdateListener((C244615p) r1.A8H.get(), (C14900mE) r1.A8X.get(), (C16590pI) r1.AMg.get(), r4, (AbstractC14440lR) r1.ANe.get());
    }

    public final C54142gF A04() {
        return new C54142gF((C48952Io) this.A0V.A0X.get(), (C89164Ix) this.A03.get(), (C89174Iy) this.A04.get(), (C89184Iz) this.A06.get(), (AnonymousClass4J1) this.A07.get(), (AnonymousClass4J2) this.A08.get(), (AnonymousClass4J3) this.A0A.get(), (AnonymousClass4J5) this.A0D.get(), (AnonymousClass4J8) this.A0E.get(), (AnonymousClass4J9) this.A0F.get(), (AnonymousClass4JA) this.A0G.get());
    }

    public final void A05(ConfirmPaymentFragment confirmPaymentFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) confirmPaymentFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) confirmPaymentFragment).A01 = (AnonymousClass180) r1.ALt.get();
        confirmPaymentFragment.A0E = (AnonymousClass01d) r1.ALI.get();
        confirmPaymentFragment.A0F = (AnonymousClass018) r1.ANb.get();
        confirmPaymentFragment.A0K = (C17070qD) r1.AFC.get();
        confirmPaymentFragment.A0J = (C22710zW) r1.AF7.get();
        confirmPaymentFragment.A0H = (AnonymousClass102) r1.AEL.get();
    }

    public final void A06(IndiaUpiEditTransactionDescriptionFragment indiaUpiEditTransactionDescriptionFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiEditTransactionDescriptionFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiEditTransactionDescriptionFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiEditTransactionDescriptionFragment.A00 = (C14900mE) r1.A8X.get();
        indiaUpiEditTransactionDescriptionFragment.A06 = (AnonymousClass19M) r1.A6R.get();
        indiaUpiEditTransactionDescriptionFragment.A04 = (AnonymousClass01d) r1.ALI.get();
        indiaUpiEditTransactionDescriptionFragment.A05 = (AnonymousClass018) r1.ANb.get();
        indiaUpiEditTransactionDescriptionFragment.A09 = (C16630pM) r1.AIc.get();
        indiaUpiEditTransactionDescriptionFragment.A07 = (AnonymousClass6BE) r1.A9X.get();
    }

    public final void A07(IndiaUpiForgotPinDialogFragment indiaUpiForgotPinDialogFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiForgotPinDialogFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiForgotPinDialogFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiForgotPinDialogFragment.A01 = (C14900mE) r1.A8X.get();
        indiaUpiForgotPinDialogFragment.A00 = (AnonymousClass12P) r1.A0H.get();
        indiaUpiForgotPinDialogFragment.A02 = (AnonymousClass01d) r1.ALI.get();
        indiaUpiForgotPinDialogFragment.A05 = (C1309960u) r1.A1b.get();
        indiaUpiForgotPinDialogFragment.A03 = (AnonymousClass6BE) r1.A9X.get();
    }

    public final void A08(IndiaUpiMyQrFragment indiaUpiMyQrFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiMyQrFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiMyQrFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiMyQrFragment.A04 = (C14900mE) r1.A8X.get();
        indiaUpiMyQrFragment.A05 = (C15570nT) r1.AAr.get();
        indiaUpiMyQrFragment.A08 = (C21270x9) r1.A4A.get();
        indiaUpiMyQrFragment.A06 = (AnonymousClass130) r1.A41.get();
        indiaUpiMyQrFragment.A0A = (AnonymousClass018) r1.ANb.get();
        indiaUpiMyQrFragment.A0F = (C25921Bi) r1.AIa.get();
        indiaUpiMyQrFragment.A09 = (C14820m6) r1.AN3.get();
        indiaUpiMyQrFragment.A0B = (C17900ra) r1.AF5.get();
        indiaUpiMyQrFragment.A0C = (C128355vy) r1.A9f.get();
    }

    public final void A09(IndiaUpiPinPrimerDialogFragment indiaUpiPinPrimerDialogFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiPinPrimerDialogFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiPinPrimerDialogFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiPinPrimerDialogFragment.A01 = (C14900mE) r1.A8X.get();
        indiaUpiPinPrimerDialogFragment.A00 = (AnonymousClass12P) r1.A0H.get();
        indiaUpiPinPrimerDialogFragment.A02 = (AnonymousClass01d) r1.ALI.get();
        indiaUpiPinPrimerDialogFragment.A03 = (AnonymousClass6BE) r1.A9X.get();
    }

    public final void A0A(IndiaUpiQrCodeScannedDialogFragment indiaUpiQrCodeScannedDialogFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiQrCodeScannedDialogFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiQrCodeScannedDialogFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiQrCodeScannedDialogFragment.A0C = (C14830m7) r1.ALb.get();
        indiaUpiQrCodeScannedDialogFragment.A0D = (C16590pI) r1.AMg.get();
        indiaUpiQrCodeScannedDialogFragment.A0B = (C15450nH) r1.AII.get();
        indiaUpiQrCodeScannedDialogFragment.A0E = (AnonymousClass018) r1.ANb.get();
        indiaUpiQrCodeScannedDialogFragment.A0I = (C21860y6) r1.AE6.get();
        indiaUpiQrCodeScannedDialogFragment.A0J = (C17900ra) r1.AF5.get();
        indiaUpiQrCodeScannedDialogFragment.A0K = (AnonymousClass6BE) r1.A9X.get();
        indiaUpiQrCodeScannedDialogFragment.A0H = (C1329668y) r1.A9d.get();
        indiaUpiQrCodeScannedDialogFragment.A0F = (C14850m9) r1.A04.get();
    }

    public final void A0B(IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiScanQrCodeFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiScanQrCodeFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiScanQrCodeFragment.A04 = (C14900mE) r1.A8X.get();
        indiaUpiScanQrCodeFragment.A08 = (C21280xA) r1.AMU.get();
        indiaUpiScanQrCodeFragment.A05 = (AnonymousClass01d) r1.ALI.get();
        indiaUpiScanQrCodeFragment.A06 = (C15890o4) r1.AN1.get();
    }

    public final void A0C(IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) indiaUpiSendPaymentToVpaFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) indiaUpiSendPaymentToVpaFragment).A01 = (AnonymousClass180) r1.ALt.get();
        indiaUpiSendPaymentToVpaFragment.A0U = (C252718t) r1.A9K.get();
        indiaUpiSendPaymentToVpaFragment.A04 = (C14900mE) r1.A8X.get();
        indiaUpiSendPaymentToVpaFragment.A0A = (C14850m9) r1.A04.get();
        indiaUpiSendPaymentToVpaFragment.A0D = (C17220qS) r1.ABt.get();
        indiaUpiSendPaymentToVpaFragment.A08 = (AnonymousClass018) r1.ANb.get();
        indiaUpiSendPaymentToVpaFragment.A0T = (C18590sh) r1.AES.get();
        indiaUpiSendPaymentToVpaFragment.A0O = (AnonymousClass69E) r1.A9W.get();
        indiaUpiSendPaymentToVpaFragment.A0Q = (AnonymousClass18O) r1.A9Y.get();
        indiaUpiSendPaymentToVpaFragment.A0F = (C1308460e) r1.A9c.get();
        indiaUpiSendPaymentToVpaFragment.A0J = (AnonymousClass18S) r1.AEw.get();
        indiaUpiSendPaymentToVpaFragment.A0S = (C1309960u) r1.A1b.get();
        indiaUpiSendPaymentToVpaFragment.A0K = (C18610sj) r1.AF0.get();
        indiaUpiSendPaymentToVpaFragment.A09 = (AnonymousClass102) r1.AEL.get();
        indiaUpiSendPaymentToVpaFragment.A0H = (AnonymousClass18T) r1.AE9.get();
        indiaUpiSendPaymentToVpaFragment.A0M = (AnonymousClass6BE) r1.A9X.get();
        indiaUpiSendPaymentToVpaFragment.A0E = (AnonymousClass68Z) r1.A9T.get();
        indiaUpiSendPaymentToVpaFragment.A07 = (C18640sm) r1.A3u.get();
        indiaUpiSendPaymentToVpaFragment.A0I = (C18650sn) r1.AEe.get();
        indiaUpiSendPaymentToVpaFragment.A0G = (C1329668y) r1.A9d.get();
        indiaUpiSendPaymentToVpaFragment.A0R = (C121265hX) r1.A9a.get();
    }

    public final void A0D(NoviAddPaymentMethodFragment noviAddPaymentMethodFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviAddPaymentMethodFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviAddPaymentMethodFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviAddPaymentMethodFragment.A00 = (AnonymousClass60Y) r1.ADK.get();
    }

    public final void A0E(NoviConfirmPaymentFragment noviConfirmPaymentFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviConfirmPaymentFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviConfirmPaymentFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviConfirmPaymentFragment.A02 = (C21270x9) r1.A4A.get();
        noviConfirmPaymentFragment.A09 = (C125685re) r1.ADE.get();
        noviConfirmPaymentFragment.A00 = (C15610nY) r1.AMe.get();
        noviConfirmPaymentFragment.A03 = (AnonymousClass018) r1.ANb.get();
        noviConfirmPaymentFragment.A0B = (AnonymousClass60Y) r1.ADK.get();
        noviConfirmPaymentFragment.A08 = (C17070qD) r1.AFC.get();
        noviConfirmPaymentFragment.A0C = (C130105yo) r1.ADZ.get();
        noviConfirmPaymentFragment.A04 = (C20830wO) r1.A4W.get();
    }

    public final void A0F(NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviEditTransactionDescriptionFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviEditTransactionDescriptionFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviEditTransactionDescriptionFragment.A04 = (AnonymousClass60Y) r1.ADK.get();
        noviEditTransactionDescriptionFragment.A03 = (AnonymousClass19M) r1.A6R.get();
        noviEditTransactionDescriptionFragment.A01 = (AnonymousClass01d) r1.ALI.get();
        noviEditTransactionDescriptionFragment.A02 = (AnonymousClass018) r1.ANb.get();
        noviEditTransactionDescriptionFragment.A05 = (C16630pM) r1.AIc.get();
    }

    public final void A0G(NoviGetStartedFragment noviGetStartedFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviGetStartedFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviGetStartedFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviGetStartedFragment.A00 = (C14850m9) r1.A04.get();
    }

    public final void A0H(NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviTransactionMethodDetailsFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviTransactionMethodDetailsFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviTransactionMethodDetailsFragment.A00 = (AnonymousClass018) r1.ANb.get();
        noviTransactionMethodDetailsFragment.A06 = (AnonymousClass60Y) r1.ADK.get();
        noviTransactionMethodDetailsFragment.A02 = (C17070qD) r1.AFC.get();
        noviTransactionMethodDetailsFragment.A07 = (AnonymousClass61F) r1.AD9.get();
        noviTransactionMethodDetailsFragment.A01 = (AnonymousClass102) r1.AEL.get();
    }

    public final void A0I(NoviTransactionReviewDetailsFragment noviTransactionReviewDetailsFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviTransactionReviewDetailsFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviTransactionReviewDetailsFragment).A01 = (AnonymousClass180) r1.ALt.get();
        noviTransactionReviewDetailsFragment.A00 = (C15610nY) r1.AMe.get();
        noviTransactionReviewDetailsFragment.A01 = (AnonymousClass018) r1.ANb.get();
        noviTransactionReviewDetailsFragment.A07 = (AnonymousClass60Y) r1.ADK.get();
        noviTransactionReviewDetailsFragment.A02 = (C20830wO) r1.A4W.get();
    }

    public final void A0J(NoviWithdrawLocationDetailsSheet noviWithdrawLocationDetailsSheet) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviWithdrawLocationDetailsSheet).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviWithdrawLocationDetailsSheet).A01 = (AnonymousClass180) r1.ALt.get();
        noviWithdrawLocationDetailsSheet.A00 = (C121885kQ) r1.ADN.get();
    }

    public final void A0K(PaymentMethodsListPickerFragment paymentMethodsListPickerFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) paymentMethodsListPickerFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) paymentMethodsListPickerFragment).A01 = (AnonymousClass180) r1.ALt.get();
        paymentMethodsListPickerFragment.A00 = (C14900mE) r1.A8X.get();
        paymentMethodsListPickerFragment.A01 = (AnonymousClass018) r1.ANb.get();
        paymentMethodsListPickerFragment.A05 = (C17070qD) r1.AFC.get();
        paymentMethodsListPickerFragment.A04 = (C248217a) r1.AE5.get();
        paymentMethodsListPickerFragment.A02 = (AnonymousClass102) r1.AEL.get();
    }

    public final void A0L(PaymentRailPickerFragment paymentRailPickerFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) paymentRailPickerFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) paymentRailPickerFragment).A01 = (AnonymousClass180) r1.ALt.get();
        paymentRailPickerFragment.A00 = (AnonymousClass018) r1.ANb.get();
    }

    public final void A0M(NoviAddDebitCardSheet noviAddDebitCardSheet) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviAddDebitCardSheet).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviAddDebitCardSheet).A01 = (AnonymousClass180) r1.ALt.get();
        noviAddDebitCardSheet.A00 = (AnonymousClass60Y) r1.ADK.get();
        noviAddDebitCardSheet.A03 = (C128375w0) r1.ADW.get();
    }

    public final void A0N(NoviWithdrawCashReviewSheet noviWithdrawCashReviewSheet) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) noviWithdrawCashReviewSheet).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) noviWithdrawCashReviewSheet).A01 = (AnonymousClass180) r1.ALt.get();
        noviWithdrawCashReviewSheet.A00 = (C15570nT) r1.AAr.get();
        noviWithdrawCashReviewSheet.A02 = (C16590pI) r1.AMg.get();
        noviWithdrawCashReviewSheet.A05 = (C121885kQ) r1.ADN.get();
        noviWithdrawCashReviewSheet.A01 = (C21270x9) r1.A4A.get();
        noviWithdrawCashReviewSheet.A03 = (AnonymousClass60Y) r1.ADK.get();
    }

    public final void A0O(MandateUpdateBottomSheetFragment mandateUpdateBottomSheetFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((WaFragment) mandateUpdateBottomSheetFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) mandateUpdateBottomSheetFragment).A01 = (AnonymousClass180) r1.ALt.get();
        mandateUpdateBottomSheetFragment.A04 = (C14900mE) r1.A8X.get();
        mandateUpdateBottomSheetFragment.A0H = (AbstractC14440lR) r1.ANe.get();
        mandateUpdateBottomSheetFragment.A06 = (AnonymousClass018) r1.ANb.get();
        mandateUpdateBottomSheetFragment.A0G = (C18590sh) r1.AES.get();
        mandateUpdateBottomSheetFragment.A0C = (C17070qD) r1.AFC.get();
        mandateUpdateBottomSheetFragment.A0F = (C1310060v) r1.A9R.get();
        mandateUpdateBottomSheetFragment.A0B = (C18610sj) r1.AF0.get();
        mandateUpdateBottomSheetFragment.A0D = (AnonymousClass6BE) r1.A9X.get();
        mandateUpdateBottomSheetFragment.A05 = (C18640sm) r1.A3u.get();
        mandateUpdateBottomSheetFragment.A09 = (C18650sn) r1.AEe.get();
        mandateUpdateBottomSheetFragment.A08 = (C1329668y) r1.A9d.get();
        mandateUpdateBottomSheetFragment.A0A = (C243515e) r1.AEt.get();
    }

    public final void A0P(ShopsProductPreviewFragment shopsProductPreviewFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((BkFragment) shopsProductPreviewFragment).A06 = C18000rk.A00(r1.AGe);
        shopsProductPreviewFragment.A07 = (C127795v4) r1.AIn.get();
        shopsProductPreviewFragment.A05 = (C16120oU) r1.ANE.get();
        shopsProductPreviewFragment.A03 = (AnonymousClass18U) r1.AAU.get();
        shopsProductPreviewFragment.A04 = (C253619c) r1.AId.get();
    }

    public final void A0Q(PrivacyNoticeFragment privacyNoticeFragment) {
        AnonymousClass01J r1 = this.A0Y;
        ((BkFragment) privacyNoticeFragment).A06 = C18000rk.A00(r1.AGe);
        privacyNoticeFragment.A03 = C18000rk.A00(this.A00);
        privacyNoticeFragment.A04 = C18000rk.A00(r1.ALs);
    }
}
