package X;

import java.util.List;

/* renamed from: X.4Wk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92564Wk {
    public final int A00;
    public final List A01;

    public C92564Wk(List list, int i) {
        this.A00 = i;
        this.A01 = list;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92564Wk r5 = (C92564Wk) obj;
            if (this.A00 != r5.A00 || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((217 + this.A00) * 31) + this.A01.hashCode();
    }
}
