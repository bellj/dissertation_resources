package X;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3Hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64913Hk {
    public static final C71123cQ A0M;
    public static final Pattern A0N = Pattern.compile("\\[([^\\[\\]])*\\]");
    public static final Pattern A0O = Pattern.compile(" ");
    public static final Pattern A0P = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]*(\\$\\d[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]*)+");
    public static final Pattern A0Q = Pattern.compile("[- ]");
    public static final Pattern A0R = Pattern.compile("\\d(?=[^,}][^,}])");
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public C71123cQ A03;
    public C71123cQ A04;
    public C92134Ur A05 = new C92134Ur(64);
    public String A06 = "";
    public String A07 = "";
    public String A08;
    public String A09 = "";
    public StringBuilder A0A = C12960it.A0h();
    public StringBuilder A0B = C12960it.A0h();
    public StringBuilder A0C = C12960it.A0h();
    public StringBuilder A0D = C12960it.A0h();
    public StringBuilder A0E = C12960it.A0h();
    public List A0F = C12960it.A0l();
    public boolean A0G = true;
    public boolean A0H = false;
    public boolean A0I = false;
    public boolean A0J = false;
    public boolean A0K = false;
    public final C20920wX A0L = C20920wX.A00();

    static {
        C71123cQ r2 = new C71123cQ();
        r2.hasInternationalPrefix = true;
        r2.internationalPrefix_ = "NA";
        A0M = r2;
    }

    public C64913Hk(String str) {
        this.A08 = str;
        C71123cQ A00 = A00(str);
        this.A03 = A00;
        this.A04 = A00;
    }

    public final C71123cQ A00(String str) {
        int i;
        C20920wX r4 = this.A0L;
        if (str == null || !r4.A08.contains(str)) {
            Logger logger = C20920wX.A0E;
            Level level = Level.WARNING;
            StringBuilder A0k = C12960it.A0k("Invalid or missing region code (");
            if (str == null) {
                str = "null";
            }
            A0k.append(str);
            logger.log(level, C12960it.A0d(") provided.", A0k));
            i = 0;
        } else {
            C71123cQ A0D = r4.A0D(str);
            if (A0D != null) {
                i = A0D.countryCode_;
            } else {
                throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("Invalid region code: ")));
            }
        }
        C71123cQ A0D2 = r4.A0D(r4.A0F(i));
        if (A0D2 == null) {
            return A0M;
        }
        return A0D2;
    }

    public final String A01() {
        C71123cQ r1;
        List<C71113cP> list;
        StringBuilder sb = this.A0D;
        if (sb.length() < 3) {
            return A06(sb.toString());
        }
        String substring = sb.substring(0, 3);
        if (!this.A0I || this.A03.intlNumberFormat_.size() <= 0) {
            r1 = this.A03;
            list = r1.numberFormat_;
        } else {
            r1 = this.A03;
            list = r1.intlNumberFormat_;
        }
        boolean z = r1.hasNationalPrefix;
        for (C71113cP r2 : list) {
            if (z && !this.A0I && !r2.nationalPrefixOptionalWhenFormatting_) {
                if (C20920wX.A0J.matcher(r2.nationalPrefixFormattingRule_).matches()) {
                }
            }
            if (A0P.matcher(r2.format_).matches()) {
                this.A0F.add(r2);
            }
        }
        A08(substring);
        return A0B() ? A02() : this.A0A.toString();
    }

    public final String A02() {
        StringBuilder sb;
        String A04;
        StringBuilder sb2 = this.A0D;
        int length = sb2.length();
        if (length > 0) {
            int i = 0;
            do {
                A04 = A04(sb2.charAt(i));
                i++;
            } while (i < length);
            if (this.A0G) {
                return A06(A04);
            }
            sb = this.A0A;
        } else {
            sb = this.A0E;
        }
        return sb.toString();
    }

    public final String A03() {
        boolean z = false;
        if (this.A03.countryCode_ == 1) {
            StringBuilder sb = this.A0D;
            if (!(sb.charAt(0) != '1' || sb.charAt(1) == '0' || sb.charAt(1) == '1')) {
                z = true;
            }
        }
        int i = 1;
        if (z) {
            StringBuilder sb2 = this.A0E;
            sb2.append('1');
            sb2.append(' ');
            this.A0I = true;
        } else {
            C71123cQ r2 = this.A03;
            if (r2.hasNationalPrefixForParsing) {
                Pattern A00 = this.A05.A00(r2.nationalPrefixForParsing_);
                StringBuilder sb3 = this.A0D;
                Matcher matcher = A00.matcher(sb3);
                if (matcher.lookingAt()) {
                    this.A0I = true;
                    i = matcher.end();
                    this.A0E.append(sb3.substring(0, i));
                }
            }
            i = 0;
        }
        StringBuilder sb4 = this.A0D;
        String substring = sb4.substring(0, i);
        sb4.delete(0, i);
        return substring;
    }

    public final String A04(char c) {
        Pattern pattern = A0O;
        StringBuilder sb = this.A0C;
        Matcher matcher = pattern.matcher(sb);
        if (matcher.find(this.A00)) {
            String replaceFirst = matcher.replaceFirst(Character.toString(c));
            sb.replace(0, replaceFirst.length(), replaceFirst);
            int start = matcher.start();
            this.A00 = start;
            return sb.substring(0, start + 1);
        }
        if (this.A0F.size() == 1) {
            this.A0G = false;
        }
        this.A06 = "";
        return this.A0A.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0044, code lost:
        if (A09() != false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A05(char r8, boolean r9) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64913Hk.A05(char, boolean):java.lang.String");
    }

    public final String A06(String str) {
        StringBuilder A0h;
        StringBuilder sb = this.A0E;
        int length = sb.length();
        if (!this.A0K || length <= 0 || sb.charAt(length - 1) == ' ') {
            A0h = C12960it.A0h();
            A0h.append((Object) sb);
        } else {
            A0h = C12960it.A0h();
            A0h.append(new String(sb));
            A0h.append(' ');
        }
        return C12960it.A0d(str, A0h);
    }

    public void A07() {
        this.A07 = "";
        this.A0A.setLength(0);
        this.A0B.setLength(0);
        this.A0C.setLength(0);
        this.A00 = 0;
        this.A06 = "";
        this.A0E.setLength(0);
        this.A09 = "";
        this.A0D.setLength(0);
        this.A0G = true;
        this.A0H = false;
        this.A02 = 0;
        this.A01 = 0;
        this.A0I = false;
        this.A0J = false;
        this.A0F.clear();
        this.A0K = false;
        if (!this.A03.equals(this.A04)) {
            this.A03 = A00(this.A08);
        }
    }

    public final void A08(String str) {
        int length = str.length() - 3;
        Iterator it = this.A0F.iterator();
        while (it.hasNext()) {
            C71113cP r2 = (C71113cP) it.next();
            if (r2.leadingDigitsPattern_.size() > length && !this.A05.A00(C12960it.A0g(r2.leadingDigitsPattern_, length)).matcher(str).lookingAt()) {
                it.remove();
            }
        }
    }

    public final boolean A09() {
        StringBuilder A0h;
        C20920wX r3;
        int A07;
        C71123cQ A00;
        StringBuilder sb = this.A0D;
        if (sb.length() == 0 || (A07 = (r3 = this.A0L).A07(sb, (A0h = C12960it.A0h()))) == 0) {
            return false;
        }
        sb.setLength(0);
        sb.append((CharSequence) A0h);
        String A0F = r3.A0F(A07);
        if ("001".equals(A0F)) {
            A00 = r3.A0C(A07);
        } else {
            if (!A0F.equals(this.A08)) {
                A00 = A00(A0F);
            }
            String num = Integer.toString(A07);
            StringBuilder sb2 = this.A0E;
            sb2.append(num);
            sb2.append(' ');
            return true;
        }
        this.A03 = A00;
        String num = Integer.toString(A07);
        StringBuilder sb2 = this.A0E;
        sb2.append(num);
        sb2.append(' ');
        return true;
    }

    public final boolean A0A() {
        Pattern A00 = this.A05.A00(C12960it.A0d(this.A03.internationalPrefix_, C12960it.A0k("\\+|")));
        StringBuilder sb = this.A0B;
        Matcher matcher = A00.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        this.A0I = true;
        int end = matcher.end();
        StringBuilder sb2 = this.A0D;
        sb2.setLength(0);
        sb2.append(sb.substring(end));
        StringBuilder sb3 = this.A0E;
        sb3.setLength(0);
        sb3.append(sb.substring(0, end));
        if (sb.charAt(0) != '+') {
            sb3.append(' ');
        }
        return true;
    }

    public final boolean A0B() {
        String replaceAll;
        Iterator it = this.A0F.iterator();
        while (true) {
            if (!it.hasNext()) {
                this.A0G = false;
                break;
            }
            C71113cP r5 = (C71113cP) it.next();
            String str = r5.pattern_;
            if (this.A06.equals(str)) {
                break;
            }
            if (str.indexOf(124) == -1) {
                String replaceAll2 = A0R.matcher(A0N.matcher(str).replaceAll("\\\\d")).replaceAll("\\\\d");
                StringBuilder sb = this.A0C;
                sb.setLength(0);
                String str2 = r5.format_;
                Matcher matcher = this.A05.A00(replaceAll2).matcher("999999999999999");
                matcher.find();
                String group = matcher.group();
                if (group.length() < this.A0D.length()) {
                    replaceAll = "";
                } else {
                    replaceAll = group.replaceAll(replaceAll2, str2).replaceAll("9", " ");
                }
                if (replaceAll.length() > 0) {
                    sb.append(replaceAll);
                    this.A06 = str;
                    this.A0K = A0Q.matcher(r5.nationalPrefixFormattingRule_).find();
                    this.A00 = 0;
                    return true;
                }
            }
            it.remove();
        }
        return false;
    }
}
