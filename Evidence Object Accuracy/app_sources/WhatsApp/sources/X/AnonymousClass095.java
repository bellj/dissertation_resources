package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.095  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass095 extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass0Bb A00;

    public AnonymousClass095(AnonymousClass0Bb r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass0Bb r1 = this.A00;
        if (r1.getChildCount() > 0) {
            r1.removeViewAt(0);
        }
    }
}
