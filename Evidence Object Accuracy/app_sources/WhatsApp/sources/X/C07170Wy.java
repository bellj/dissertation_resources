package X;

import android.view.View;
import android.widget.AdapterView;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0Wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07170Wy implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ SearchView A00;

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }

    public C07170Wy(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        this.A00.A0D(i);
    }
}
