package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.filter.FilterUtils;
import java.util.HashSet;

/* renamed from: X.3WH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WH implements AnonymousClass28F {
    public boolean A00;
    public final AnonymousClass1O4 A01;
    public final AnonymousClass130 A02;
    public final HashSet A03 = C12970iu.A12();

    public AnonymousClass3WH(AnonymousClass1O4 r2, AnonymousClass130 r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    public final void A00(Bitmap bitmap, ImageView imageView, String str) {
        if (this.A03.contains(Integer.valueOf(imageView.hashCode()))) {
            Bitmap bitmap2 = null;
            if (str != null && !this.A00) {
                bitmap2 = (Bitmap) this.A01.A00(str);
            }
            this.A00 = false;
            if (bitmap2 != null) {
                imageView.setImageBitmap(bitmap2);
                return;
            }
            try {
                Bitmap copy = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                if (copy != null) {
                    FilterUtils.blurNative(copy, 30, 2);
                    if (str != null) {
                        this.A01.A03(str, copy);
                    }
                    imageView.setImageBitmap(copy);
                }
            } catch (Throwable th) {
                th.getMessage();
            }
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        String str;
        if (bitmap != null) {
            if (imageView.getTag() instanceof String) {
                str = (String) imageView.getTag();
            } else {
                str = null;
            }
            A00(bitmap, imageView, str);
            return;
        }
        Adv(imageView);
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        A00(this.A02.A03(imageView.getContext(), R.drawable.avatar_contact_voip), imageView, "default_avatar");
    }
}
