package X;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2zf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61252zf extends AbstractCallableC112595Dz {
    public final int A00;
    public final int A01 = 3;
    public final Context A02;
    public final Paint A03;
    public final AnonymousClass01d A04;
    public final AnonymousClass018 A05;
    public final AnonymousClass19M A06;
    public final C16630pM A07;
    public final CharSequence A08;
    public final List A09;

    public C61252zf(Context context, Paint paint, AnonymousClass01d r4, AnonymousClass018 r5, AnonymousClass19M r6, C16630pM r7, CharSequence charSequence, List list, int i) {
        this.A02 = context;
        this.A03 = paint;
        this.A00 = i;
        this.A08 = charSequence;
        this.A09 = list;
        this.A06 = r6;
        this.A04 = r4;
        this.A05 = r5;
        this.A07 = r7;
    }

    public static List A00(BreakIterator breakIterator, List list, int i, int i2) {
        if (list.size() == 0) {
            return list;
        }
        ArrayList A0l = C12960it.A0l();
        int min = Math.min(list.size(), 5);
        for (int i3 = 0; i3 < min; i3++) {
            AnonymousClass01T r1 = (AnonymousClass01T) list.get(i3);
            Number number = (Number) r1.A00;
            AnonymousClass009.A05(number);
            Number number2 = (Number) r1.A01;
            AnonymousClass009.A05(number2);
            int max = Math.max(0, breakIterator.preceding(Math.max(0, number.intValue() - i)));
            int following = breakIterator.following(Math.min(i2, number2.intValue() + i)) - 1;
            if (following < 0) {
                following = i2;
            }
            AnonymousClass01T r7 = new AnonymousClass01T(Integer.valueOf(max), Integer.valueOf(following));
            if (A0l.size() != 0) {
                int size = A0l.size() - 1;
                AnonymousClass01T r10 = (AnonymousClass01T) A0l.get(size);
                Object obj = r7.A00;
                AnonymousClass009.A05(obj);
                int A05 = C12960it.A05(obj);
                Object obj2 = r7.A01;
                AnonymousClass009.A05(obj2);
                int A052 = C12960it.A05(obj2);
                Object obj3 = r10.A00;
                AnonymousClass009.A05(obj3);
                int A053 = C12960it.A05(obj3);
                Object obj4 = r10.A01;
                AnonymousClass009.A05(obj4);
                int A054 = C12960it.A05(obj4);
                if ((A05 <= A053 && A053 <= A052) || (A053 <= A05 && A05 <= A054)) {
                    int min2 = Math.min(A05, A053);
                    int max2 = Math.max(A052, A054);
                    A0l.remove(size);
                    r7 = new AnonymousClass01T(Integer.valueOf(min2), Integer.valueOf(max2));
                }
            }
            A0l.add(r7);
        }
        return A0l;
    }

    @Override // X.AbstractCallableC112595Dz
    public /* bridge */ /* synthetic */ Object A01() {
        CharSequence charSequence = this.A08;
        if (TextUtils.isEmpty(charSequence)) {
            return "";
        }
        CharSequence A03 = C42971wC.A03(this.A04, this.A07, charSequence.toString().replace("\n", " "));
        Context context = this.A02;
        List list = this.A09;
        AnonymousClass018 r12 = this.A05;
        AnonymousClass01T A00 = AnonymousClass3J9.A00(context, r12, AnonymousClass3J9.A02, A03, list, true);
        AnonymousClass02N r9 = super.A00;
        r9.A02();
        CharSequence charSequence2 = (CharSequence) A00.A00;
        Object obj = A00.A01;
        AnonymousClass009.A05(obj);
        List list2 = (List) obj;
        C32651cV r10 = new C32651cV(1, 460);
        if (TextUtils.isEmpty(charSequence2)) {
            return "";
        }
        if (!list2.isEmpty()) {
            Paint paint = this.A03;
            float measureText = paint.measureText(charSequence2, 0, charSequence2.length());
            float f = (float) (this.A00 * this.A01);
            if (measureText > f) {
                BreakIterator A01 = C32751cg.A01(r12);
                A01.setText(charSequence2.toString());
                List A002 = A00(A01, list2, 20, charSequence2.length());
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                A02(spannableStringBuilder, charSequence2, A002);
                r9.A02();
                int i = 10;
                while (paint.measureText(spannableStringBuilder, 0, spannableStringBuilder.length()) <= f && spannableStringBuilder.length() < charSequence2.length()) {
                    r9.A02();
                    spannableStringBuilder.clear();
                    A002 = A00(A01, A002, i, charSequence2.length());
                    A02(spannableStringBuilder, charSequence2, A002);
                    r9.A02();
                    i += 10;
                }
                return spannableStringBuilder;
            }
        }
        return AbstractC36671kL.A02(context, this.A03, this.A06, r10, charSequence2);
    }

    public final void A02(SpannableStringBuilder spannableStringBuilder, CharSequence charSequence, List list) {
        long length = (long) (charSequence.length() - 1);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass01T r6 = (AnonymousClass01T) it.next();
            super.A00.A02();
            Object obj = r6.A00;
            if (!(obj == null || C12960it.A05(obj) == 0)) {
                if (spannableStringBuilder.length() == 0) {
                    spannableStringBuilder.append(" ");
                    spannableStringBuilder.append("…");
                } else if (!"…".equals(String.valueOf(spannableStringBuilder.charAt(spannableStringBuilder.length() - 1)))) {
                    if (!Character.isWhitespace(spannableStringBuilder.charAt(spannableStringBuilder.length() - 1))) {
                        spannableStringBuilder.append(" ");
                    }
                    spannableStringBuilder.append("…");
                    spannableStringBuilder.append(" ");
                }
            }
            AnonymousClass009.A05(obj);
            int A05 = C12960it.A05(obj);
            Number number = (Number) r6.A01;
            AnonymousClass009.A05(number);
            spannableStringBuilder.append(charSequence.subSequence(A05, number.intValue()));
            if (((long) number.intValue()) < length) {
                spannableStringBuilder.append(" ");
                spannableStringBuilder.append("…");
            }
        }
    }
}
