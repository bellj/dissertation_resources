package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Ly  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C66013Ly implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(54);
    public final int A00;
    public final C66003Lx A01;
    public final C66003Lx A02;
    public final C66003Lx A03;
    public final String A04;
    public final boolean A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C66013Ly(C66003Lx r2, C66003Lx r3, C66003Lx r4, String str, int i) {
        this.A04 = str;
        this.A02 = r2;
        this.A03 = r3;
        this.A01 = r4;
        this.A00 = i;
        this.A05 = false;
    }

    public /* synthetic */ C66013Ly(Parcel parcel) {
        this.A04 = parcel.readString();
        this.A02 = (C66003Lx) C12990iw.A0I(parcel, C66003Lx.class);
        this.A03 = (C66003Lx) C12990iw.A0I(parcel, C66003Lx.class);
        this.A01 = (C66003Lx) C12990iw.A0I(parcel, C66003Lx.class);
        this.A00 = parcel.readInt();
        this.A05 = C12970iu.A1W(parcel.readInt());
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C66013Ly.class.getName());
        A0h.append("{id='");
        char A00 = C12990iw.A00(this.A04, A0h);
        A0h.append(", preview='");
        A0h.append(this.A02);
        A0h.append(A00);
        A0h.append(", staticPreview='");
        A0h.append(this.A03);
        A0h.append(A00);
        A0h.append(", content='");
        A0h.append(this.A01);
        A0h.append(A00);
        return C12970iu.A0v(A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeParcelable(this.A02, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A05 ? 1 : 0);
    }
}
