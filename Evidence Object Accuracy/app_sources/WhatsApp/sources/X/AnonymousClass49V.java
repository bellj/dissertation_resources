package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49V  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49V extends Enum {
    public static final AnonymousClass49V A00 = new AnonymousClass49V("AARCH64", "arm64-v8a", 4);
    public static final AnonymousClass49V A01 = new AnonymousClass49V("ARM", "armeabi-v7a", 2);
    public static final AnonymousClass49V A02 = new AnonymousClass49V("X86", "x86", 1);
    public static final AnonymousClass49V A03 = new AnonymousClass49V("X86_64", "x86_64", 3);
    public final String value;

    static {
        new AnonymousClass49V("NOT_SO", "not_so", 0);
        new AnonymousClass49V("OTHERS", "others", 5);
    }

    public AnonymousClass49V(String str, String str2, int i) {
        this.value = str2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.value;
    }
}
