package X;

import android.media.MediaCodecInfo;

/* renamed from: X.5X8  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5X8 {
    int ABS();

    MediaCodecInfo ABT(int i);

    boolean AJO(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2);

    boolean AJP(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2);

    boolean AbO();
}
