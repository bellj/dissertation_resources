package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C462625e extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C462625e A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01 = 0;
    public int A02;
    public Object A03;

    static {
        C462625e r0 = new C462625e();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.A01 == 1) {
            i2 = 0 + CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 1);
        }
        if (this.A01 == 2) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 2);
        }
        if (this.A01 == 3) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A04(4, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 1);
        }
        if (this.A01 == 2) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 2);
        }
        if (this.A01 == 3) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
