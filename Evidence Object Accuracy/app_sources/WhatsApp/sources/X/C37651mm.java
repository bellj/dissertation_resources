package X;

import java.io.IOException;

/* renamed from: X.1mm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37651mm extends Exception {
    public final int downloadStatus;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C37651mm(IOException iOException) {
        super(null, iOException);
        int A00 = AnonymousClass4EO.A00(iOException);
        this.downloadStatus = A00;
    }

    public /* synthetic */ C37651mm(Exception exc, String str, int i) {
        super(str, exc);
        this.downloadStatus = i;
    }
}
