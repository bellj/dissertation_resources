package X;

/* renamed from: X.2LM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LM extends AnonymousClass1V4 {
    public int A00 = 1;
    public long A01;
    public boolean A02;
    public final int A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;

    public AnonymousClass2LM(AbstractC15710nm r15, C14830m7 r16, C16120oU r17, C17230qT r18, Integer num, String str, int i, long j, long j2, boolean z, boolean z2, boolean z3) {
        super(r15, r16, r17, r18, num, str, 0, j, j2);
        this.A03 = i;
        this.A05 = z3;
        this.A04 = z;
        this.A06 = z2;
    }

    @Override // X.AnonymousClass1V4
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("; messageType = ");
        sb.append(this.A03);
        sb.append("; hasSenderKeyDistributionMessage = ");
        sb.append(this.A05);
        sb.append("; ephemeral = ");
        sb.append(this.A04);
        sb.append("; revoke = ");
        sb.append(this.A06);
        sb.append("; decryptionSuccess = ");
        sb.append(this.A02);
        sb.append("; mediaType = ");
        sb.append(this.A00);
        sb.append("; decryptQueueSize = ");
        sb.append(this.A01);
        return sb.toString();
    }
}
