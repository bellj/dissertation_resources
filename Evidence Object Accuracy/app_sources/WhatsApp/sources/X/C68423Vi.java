package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/* renamed from: X.3Vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68423Vi implements AbstractC115825Tb {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ C63743Ct A02;
    public final /* synthetic */ AnonymousClass1AH A03;

    public C68423Vi(Activity activity, Context context, C63743Ct r3, AnonymousClass1AH r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = context;
        this.A00 = activity;
    }

    @Override // X.AbstractC115825Tb
    public final void AVM(boolean z) {
        if (!z) {
            this.A02.A00(AnonymousClass49P.A02);
            return;
        }
        AnonymousClass1AH r5 = this.A03;
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(UUID.randomUUID(), A0h);
        File ACq = r5.ACq(C12960it.A0d(".jpg", A0h));
        try {
            Uri A01 = C14350lI.A01(this.A01, ACq);
            Intent A0E = C12990iw.A0E("android.media.action.IMAGE_CAPTURE");
            A0E.putExtra("output", A01);
            r5.Ae8(this.A00, A0E, new AnonymousClass3CY(this, ACq), 1);
        } catch (IOException unused) {
            this.A02.A00(AnonymousClass49P.A00);
            ACq.delete();
        }
    }
}
