package X;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;

/* renamed from: X.13q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239513q implements AbstractC15920o8 {
    public int A00;
    public final AbstractC15710nm A01;
    public final C233011d A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C20870wS A05;
    public final C22920zr A06;
    public final C20770wI A07;
    public final C15450nH A08;
    public final C22890zo A09;
    public final C20670w8 A0A;
    public final C16240og A0B;
    public final C238013b A0C;
    public final C20970wc A0D;
    public final C239313o A0E;
    public final C20720wD A0F;
    public final C18640sm A0G;
    public final C14830m7 A0H;
    public final C16590pI A0I;
    public final C14820m6 A0J;
    public final C15990oG A0K;
    public final C18240s8 A0L;
    public final C22320yt A0M;
    public final C15650ng A0N;
    public final C238113c A0O;
    public final C15600nX A0P;
    public final C21380xK A0Q;
    public final C236812p A0R;
    public final C15660nh A0S;
    public final C22340yv A0T;
    public final C238513g A0U;
    public final C22350yw A0V;
    public final C239013l A0W;
    public final C22830zi A0X;
    public final C21400xM A0Y;
    public final C239113m A0Z;
    public final C22100yW A0a;
    public final C22130yZ A0b;
    public final C18770sz A0c;
    public final C238813j A0d;
    public final C22090yV A0e;
    public final C14850m9 A0f;
    public final C16120oU A0g;
    public final C20710wC A0h;
    public final AnonymousClass109 A0i;
    public final AnonymousClass12J A0j;
    public final C22310ys A0k;
    public final C22400z1 A0l;
    public final AnonymousClass104 A0m;
    public final C238613h A0n;
    public final C19460u9 A0o;
    public final C22910zq A0p;
    public final C14840m8 A0q;
    public final C238913k A0r;
    public final C238213d A0s;
    public final C22170ye A0t;
    public final C20660w7 A0u;
    public final C22230yk A0v;
    public final C238713i A0w;
    public final C17230qT A0x;
    public final C238313e A0y;
    public final C17070qD A0z;
    public final C22850zk A10;
    public final C239413p A11;
    public final C238413f A12;
    public final C22140ya A13;
    public final C22900zp A14;
    public final C20810wM A15;
    public final AbstractC14440lR A16;
    public final C17140qK A17;
    public final Set A18 = new HashSet();
    public final Set A19 = new HashSet();

    public C239513q(AbstractC15710nm r2, C233011d r3, C14900mE r4, C15570nT r5, C20870wS r6, C22920zr r7, C20770wI r8, C15450nH r9, C22890zo r10, C20670w8 r11, C16240og r12, C238013b r13, C20970wc r14, C239313o r15, C20720wD r16, C18640sm r17, C14830m7 r18, C16590pI r19, C14820m6 r20, C15990oG r21, C18240s8 r22, C22320yt r23, C15650ng r24, C238113c r25, C15600nX r26, C21380xK r27, C236812p r28, C15660nh r29, C22340yv r30, C238513g r31, C22350yw r32, C239013l r33, C22830zi r34, C21400xM r35, C239113m r36, C22100yW r37, C22130yZ r38, C18770sz r39, C238813j r40, C22090yV r41, C14850m9 r42, C16120oU r43, C20710wC r44, AnonymousClass109 r45, AnonymousClass12J r46, C22310ys r47, C22400z1 r48, AnonymousClass104 r49, C238613h r50, C19460u9 r51, C22910zq r52, C14840m8 r53, C238913k r54, C238213d r55, C22170ye r56, C20660w7 r57, C22230yk r58, C238713i r59, C17230qT r60, C238313e r61, C17070qD r62, C22850zk r63, C239413p r64, C238413f r65, C22140ya r66, C22900zp r67, C20810wM r68, AbstractC14440lR r69, C17140qK r70) {
        this.A0I = r19;
        this.A0H = r18;
        this.A0f = r42;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A16 = r69;
        this.A0g = r43;
        this.A0u = r57;
        this.A08 = r9;
        this.A0t = r56;
        this.A0A = r11;
        this.A0Q = r27;
        this.A05 = r6;
        this.A09 = r10;
        this.A0v = r58;
        this.A0z = r62;
        this.A0C = r13;
        this.A0N = r24;
        this.A0O = r25;
        this.A14 = r67;
        this.A0h = r44;
        this.A0p = r52;
        this.A06 = r7;
        this.A0s = r55;
        this.A0B = r12;
        this.A0j = r46;
        this.A0F = r16;
        this.A0k = r47;
        this.A0S = r29;
        this.A0q = r53;
        this.A0e = r41;
        this.A0K = r21;
        this.A0c = r39;
        this.A0M = r23;
        this.A0T = r30;
        this.A0V = r32;
        this.A0x = r60;
        this.A0Y = r35;
        this.A0y = r61;
        this.A12 = r65;
        this.A0D = r14;
        this.A0J = r20;
        this.A0b = r38;
        this.A0R = r28;
        this.A0U = r31;
        this.A0X = r34;
        this.A13 = r66;
        this.A0n = r50;
        this.A07 = r8;
        this.A0l = r48;
        this.A17 = r70;
        this.A0a = r37;
        this.A0w = r59;
        this.A0m = r49;
        this.A0d = r40;
        this.A0r = r54;
        this.A0i = r45;
        this.A0P = r26;
        this.A15 = r68;
        this.A0G = r17;
        this.A0o = r51;
        this.A0W = r33;
        this.A0Z = r36;
        this.A02 = r3;
        this.A0E = r15;
        this.A0L = r22;
        this.A11 = r64;
        this.A10 = r63;
    }

    public final int A00(String str, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        String obj;
        if (bArr2 == null) {
            return 1;
        }
        if (bArr == null) {
            obj = "MessagingXmppHandler/validateServerErrorEncData/badmediadata;";
        } else if (bArr3 == null) {
            obj = "MessagingXmppHandler/validateServerErrorEncData/incomplete enc data";
        } else {
            AnonymousClass104.A00(bArr, bArr3);
            NativeHolder nativeHolder = (NativeHolder) JniBridge.jvidispatchOOOOO(1, str, bArr2, bArr, bArr3);
            if (nativeHolder != null) {
                AnonymousClass2Du r0 = new AnonymousClass2Du(nativeHolder);
                JniBridge.getInstance();
                NativeHolder nativeHolder2 = r0.A00;
                long j = (long) 28;
                if (str.equals(JniBridge.jvidispatchOIO(1, j, nativeHolder2))) {
                    return 1;
                }
                StringBuilder sb = new StringBuilder("MessagingXmppHandler/validateServerErrorEncData/incorrect stanza id; key=");
                sb.append(str);
                sb.append("; stanzaId=");
                JniBridge.getInstance();
                sb.append((String) JniBridge.jvidispatchOIO(1, j, nativeHolder2));
                obj = sb.toString();
            } else {
                Log.e("MessagingXmppHandler/validateServerErrorEncData/malformed enc data");
                return 3;
            }
        }
        Log.e(obj);
        return 0;
    }

    public Future A01(AbstractC47922Dh r6) {
        AnonymousClass1VC r4 = new AnonymousClass1VC();
        RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 = new RunnableBRunnable0Shape1S0300000_I0_1(this, r6, r4, 39);
        boolean A05 = this.A08.A05(AbstractC15460nI.A15);
        C22320yt r0 = this.A0M;
        if (A05) {
            Handler handler = r0.A01;
            Message obtain = Message.obtain(handler, runnableBRunnable0Shape1S0300000_I0_1);
            obtain.arg1 = 13;
            handler.sendMessage(obtain);
            return r4;
        }
        r0.A01(runnableBRunnable0Shape1S0300000_I0_1, 13);
        return r4;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r22v3, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x040e, code lost:
        if (r3 == null) goto L_0x0413;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
        // Method dump skipped, instructions count: 1064
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239513q.A02():void");
    }

    public final void A03(AbstractC14640lm r10, Jid jid, UserJid userJid, AbstractC16130oV r13, String str, int i, boolean z, boolean z2) {
        C16150oX r0;
        if (z) {
            C15570nT r1 = this.A04;
            r1.A08();
            if (r1.A05 != null && r1.A0E(DeviceJid.of(jid))) {
                if (r13 == null || !((r0 = r13.A02) == null || r0.A0U == null)) {
                    this.A0m.A01(r10, jid, userJid, r13, str, null, i, z2);
                }
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final void A04(X.AbstractC47922Dh r41, long r42) {
        /*
        // Method dump skipped, instructions count: 896
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239513q.A04(X.2Dh, long):void");
    }

    public final void A05(AbstractC15340mz r6, AnonymousClass1IS r7) {
        if (r6 instanceof AnonymousClass1Iv) {
            C239113m r4 = this.A0Z;
            AnonymousClass1IS r3 = r6.A0z;
            if (!r3.A02) {
                Log.e("MessageAddOnSendRecvManager/cant use this method for messageAddOn not from self");
            } else {
                r4.A05.A01(new RunnableBRunnable0Shape4S0200000_I0_4(r4, 38, r3), 57);
            }
        } else if (r6 != null) {
            r6.A0Y(7);
            this.A0N.A0a(r6, 23);
        } else {
            StringBuilder sb = new StringBuilder("MessagingXmppHandler/onMessageError/bounce unable to find message ");
            sb.append(r7);
            Log.w(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0035, code lost:
        if (r3.A02 == false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(X.AnonymousClass1IS r12, boolean r13) {
        /*
            r11 = this;
            X.0zi r0 = r11.A0X
            r4 = r12
            java.util.Set r0 = r0.A00(r12)
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>(r0)
            X.0nT r0 = r11.A04
            r0.A08()
            X.1Hq r0 = r0.A04
            r1.add(r0)
            X.0nm r0 = r11.A01
            java.util.Set r1 = X.C15380n4.A09(r0, r1)
            X.0wD r2 = r11.A0F
            r0 = 0
            com.whatsapp.jid.UserJid[] r0 = new com.whatsapp.jid.UserJid[r0]
            java.lang.Object[] r5 = r1.toArray(r0)
            com.whatsapp.jid.UserJid[] r5 = (com.whatsapp.jid.UserJid[]) r5
            if (r13 == 0) goto L_0x0066
            r8 = 0
        L_0x002b:
            X.0og r3 = r11.A0B
            int r1 = r3.A04
            r0 = 2
            if (r1 != r0) goto L_0x0037
            boolean r0 = r3.A02
            r10 = 1
            if (r0 != 0) goto L_0x0038
        L_0x0037:
            r10 = 0
        L_0x0038:
            int r0 = r5.length
            if (r0 != 0) goto L_0x004d
            java.lang.String r1 = "SyncDeviceAndResendMessageJob/empty recipients for "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r12)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.w(r0)
        L_0x004c:
            return
        L_0x004d:
            X.1E7 r0 = r2.A02
            boolean r0 = r0.A01(r12)
            if (r0 == 0) goto L_0x004c
            X.0w8 r1 = r2.A00
            X.0m7 r0 = r2.A03
            long r6 = r0.A00()
            com.whatsapp.jobqueue.job.SyncDeviceAndResendMessageJob r3 = new com.whatsapp.jobqueue.job.SyncDeviceAndResendMessageJob
            r3.<init>(r4, r5, r6, r8, r10)
            r1.A00(r3)
            return
        L_0x0066:
            X.0m7 r0 = r11.A0H
            long r8 = r0.A00()
            r0 = 300000(0x493e0, double:1.482197E-318)
            long r8 = r8 + r0
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239513q.A06(X.1IS, boolean):void");
    }

    public final void A07(C28941Pp r13) {
        C20660w7 r6 = this.A0u;
        new C453721i(this.A06, this.A0A, this.A0K, this.A0M, this.A0n, r6, r13, null, null, null, true).A00();
    }

    public final boolean A08(DeviceJid deviceJid, AbstractC15340mz r23, C32141bg r24, int i, long j) {
        boolean A03;
        this.A0M.A00();
        AnonymousClass1IS r6 = r23.A0z;
        String str = r6.A01;
        if (!C37381mH.A01(i)) {
            return this.A0T.A04(null, r6, i);
        }
        if (r23 instanceof AnonymousClass1Iv) {
            this.A0X.A01(deviceJid, r23, j);
            A03 = true;
        } else {
            C239013l r5 = this.A0W;
            r5.A02.A00();
            AbstractC15340mz A032 = r5.A03.A0K.A03(r6);
            if (A032 == null) {
                StringBuilder sb = new StringBuilder("PrivacyStateMessageManager/check privacy conflict on receipt/fMsg not found for messageKey=");
                sb.append(r6);
                Log.e(sb.toString());
            } else {
                C22440z5 r3 = r5.A04;
                C32351c1 A00 = r3.A00(A032.A11);
                C32141bg r2 = null;
                if (A00 != null) {
                    StringBuilder sb2 = new StringBuilder("PrivacyStateMessageManager/check privacy conflict on receipt/");
                    sb2.append(r6);
                    Log.i(sb2.toString());
                    r2 = A00.A00;
                } else if (r24 != null) {
                    StringBuilder sb3 = new StringBuilder("PrivacyStateMessageManager/check privacy conflict on receipt (ent upgrade)/");
                    sb3.append(r6);
                    Log.i(sb3.toString());
                }
                if (AnonymousClass23Y.A01(r2, r24)) {
                    StringBuilder sb4 = new StringBuilder("PrivacyStateMessageManager/check privacy conflict on receipt/privacy mismatch. jid=");
                    AbstractC14640lm r4 = A032.A0z.A00;
                    sb4.append(r4);
                    sb4.append(" privacy mode=");
                    sb4.append(r24);
                    sb4.append(" row id=");
                    sb4.append(A032.A11);
                    Log.w(sb4.toString());
                    if (r24 != null) {
                        C16310on A02 = r3.A02.A02();
                        try {
                            AnonymousClass1Lx A002 = A02.A00();
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("message_row_id", Long.valueOf(A032.A11));
                            contentValues.put("host_storage", Integer.valueOf(r24.hostStorage));
                            contentValues.put("actual_actors", Integer.valueOf(r24.actualActors));
                            contentValues.put("privacy_mode_ts", Long.valueOf(r24.privacyModeTs));
                            A02.A03.A06(contentValues, "message_privacy_state", 5);
                            A032.A0T(4096);
                            try {
                                r3.A01.A08(A032, false);
                                A002.A00();
                            } catch (IOException e) {
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("Failed to update msg privacy flag for ");
                                sb5.append(A032.A11);
                                Log.e(sb5.toString(), e);
                            }
                            A002.close();
                            A02.close();
                            if (r2 == null || r2.privacyModeTs < r24.privacyModeTs) {
                                StringBuilder sb6 = new StringBuilder("PrivacyStateMessageManager/check privacy conflict on receipt/GetVNameCertificateJob. UserJid=");
                                sb6.append(r4);
                                Log.w(sb6.toString());
                                r5.A01.A00(new GetVNameCertificateJob(UserJid.of(r4)));
                            }
                        } catch (Throwable th) {
                            try {
                                A02.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } else {
                        r3.A01(A032.A11);
                    }
                }
            }
            C22340yv r42 = this.A0T;
            AnonymousClass009.A05(deviceJid);
            AnonymousClass2RH r32 = new AbstractC43081wN() { // from class: X.2RH
                @Override // X.AbstractC43081wN
                public final void Ab1(Object obj) {
                    C239513q r33 = C239513q.this;
                    C43101wP r52 = (C43101wP) obj;
                    AnonymousClass009.A05(r52);
                    AnonymousClass1IS r22 = r52.A01.A0z;
                    AbstractC14640lm r1 = r22.A00;
                    if (C15380n4.A0F(r1) || C15380n4.A0J(r1)) {
                        r33.A0v.A0B(r22, r52.A00);
                    }
                }
            };
            r42.A04.A00();
            boolean z = r6.A02;
            if (r42.A0F.A05() || z) {
                r42.A0M.remove(r6);
                A03 = r42.A03(r32, deviceJid, r23, i, j);
            } else {
                StringBuilder sb7 = new StringBuilder("msgstore/updatetargetstatus/error ");
                sb7.append(r6.A00);
                sb7.append(" ");
                sb7.append(str);
                Log.e(sb7.toString());
                A03 = false;
            }
        }
        if (r6.A02 || C30041Vv.A0N(this.A04, r23)) {
            C15950oC A022 = C15940oB.A02(deviceJid);
            C18240s8 r22 = this.A0L;
            r22.A00.execute(new RunnableBRunnable0Shape1S0300000_I0_1(this, A022, r6, 41));
        }
        return A03;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{0, 1, 2, 19, 69, 86, 16, 15, 215, 227};
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractC15920o8
    public boolean AI8(android.os.Message r30, int r31) {
        /*
        // Method dump skipped, instructions count: 3077
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239513q.AI8(android.os.Message, int):boolean");
    }
}
