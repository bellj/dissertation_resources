package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/* renamed from: X.19o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254819o {
    public C627338j A00;
    public final AnonymousClass10G A01;
    public final C254919p A02;
    public final C17050qB A03;
    public final C14950mJ A04;
    public final C16120oU A05;
    public final AnonymousClass10D A06;
    public final AbstractC14440lR A07;

    public C254819o(AnonymousClass10G r1, C254919p r2, C17050qB r3, C14950mJ r4, C16120oU r5, AnonymousClass10D r6, AbstractC14440lR r7) {
        this.A07 = r7;
        this.A05 = r5;
        this.A02 = r2;
        this.A04 = r4;
        this.A01 = r1;
        this.A03 = r3;
        this.A06 = r6;
    }

    public Pair A00() {
        String obj = UUID.randomUUID().toString();
        AnonymousClass43C r1 = new AnonymousClass43C();
        r1.A00 = obj;
        this.A05.A07(r1);
        return new Pair("anid", obj);
    }

    public void A01(ActivityC13810kN r17, AnonymousClass1MJ r18, String str, String str2, String str3, String str4, ArrayList arrayList, List list, boolean z) {
        C627338j r0 = this.A00;
        if (r0 != null && r0.A00() == 1) {
            this.A00.A03(false);
        }
        C91694Ss r10 = new C91694Ss(r17, this, str2, str3, arrayList);
        C627338j r4 = new C627338j(A00(), this.A01, this.A03, this.A04, r18, r10, this.A06, str, str4, list, z);
        this.A00 = r4;
        this.A07.Aaz(r4, new Void[0]);
    }
}
