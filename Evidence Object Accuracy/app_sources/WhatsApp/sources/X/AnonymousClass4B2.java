package X;

/* renamed from: X.4B2  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4B2 {
    A02(0),
    A01(1),
    A03(2);
    
    public final int value;

    AnonymousClass4B2(int i) {
        this.value = i;
    }
}
