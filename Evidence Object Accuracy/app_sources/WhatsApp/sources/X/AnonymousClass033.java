package X;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.util.NoSuchElementException;

/* renamed from: X.033  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass033 {
    public char A00;
    public char A01;
    public int A02;
    public int A03 = -1;
    public RandomAccessFile A04;
    public boolean A05 = true;
    public boolean A06 = false;
    public final String A07;
    public final byte[] A08;

    public AnonymousClass033(String str) {
        this.A07 = str;
        this.A08 = new byte[512];
    }

    public long A00() {
        long j = 1;
        long j2 = 0;
        boolean z = true;
        while (true) {
            if (A08()) {
                A05();
                if (!Character.isDigit(this.A00)) {
                    if (!z) {
                        A06();
                        break;
                    } else if (this.A00 == '-') {
                        j = -1;
                    } else {
                        throw new C10760fA("Couldn't read number!");
                    }
                } else {
                    j2 = (j2 * 10) + ((long) (this.A00 - '0'));
                }
                z = false;
            } else if (z) {
                throw new C10760fA("Couldn't read number because the file ended!");
            }
        }
        return j * j2;
    }

    public void A01() {
        RandomAccessFile randomAccessFile = this.A04;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException unused) {
            } catch (Throwable th) {
                this.A04 = null;
                throw th;
            }
            this.A04 = null;
        }
    }

    public void A02() {
        this.A05 = true;
        RandomAccessFile randomAccessFile = this.A04;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.seek(0);
            } catch (IOException unused) {
                A01();
            }
        }
        if (this.A04 == null) {
            try {
                this.A04 = new RandomAccessFile(this.A07, "r");
            } catch (IOException unused2) {
                this.A05 = false;
                A01();
            }
        }
        if (this.A05) {
            this.A03 = -1;
            this.A02 = 0;
            this.A00 = 0;
            this.A01 = 0;
            this.A06 = false;
        }
    }

    public void A03() {
        boolean z = false;
        while (A08()) {
            A05();
            if (this.A00 == '\n') {
                z = true;
            } else if (z) {
                A06();
                return;
            }
        }
    }

    public void A04() {
        boolean z = false;
        while (A08()) {
            A05();
            if (this.A00 == ' ') {
                z = true;
            } else if (z) {
                A06();
                return;
            }
        }
    }

    public final void A05() {
        if (A08()) {
            int i = this.A03 + 1;
            this.A03 = i;
            this.A01 = this.A00;
            this.A00 = (char) this.A08[i];
            this.A06 = false;
            return;
        }
        throw new NoSuchElementException();
    }

    public final void A06() {
        if (!this.A06) {
            this.A03--;
            this.A00 = this.A01;
            this.A06 = true;
            return;
        }
        throw new C10760fA("Can only rewind one step!");
    }

    public void A07(CharBuffer charBuffer) {
        charBuffer.clear();
        boolean z = true;
        while (true) {
            if (A08()) {
                A05();
                if (!Character.isWhitespace(this.A00)) {
                    if (!charBuffer.hasRemaining()) {
                        CharBuffer allocate = CharBuffer.allocate(charBuffer.capacity() << 1);
                        charBuffer.flip();
                        allocate.put(charBuffer);
                        charBuffer = allocate;
                    }
                    charBuffer.put(this.A00);
                    z = false;
                } else if (!z) {
                    A06();
                } else {
                    throw new C10760fA("Couldn't read string!");
                }
            } else if (z) {
                throw new C10760fA("Couldn't read string because file ended!");
            }
        }
        charBuffer.flip();
    }

    public boolean A08() {
        RandomAccessFile randomAccessFile;
        if (this.A05 && (randomAccessFile = this.A04) != null) {
            int i = this.A03;
            int i2 = this.A02;
            if (i <= i2 - 1) {
                if (i < i2 - 1) {
                    return true;
                }
                try {
                    this.A02 = randomAccessFile.read(this.A08);
                    this.A03 = -1;
                } catch (IOException unused) {
                    this.A05 = false;
                    A01();
                }
                return A08();
            }
        }
        return false;
    }

    public void finalize() {
        A01();
    }
}
