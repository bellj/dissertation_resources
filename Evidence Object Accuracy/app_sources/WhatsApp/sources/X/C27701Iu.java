package X;

import android.util.Base64;
import java.security.MessageDigest;
import java.util.Arrays;

/* renamed from: X.1Iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27701Iu {
    public int A00;
    public long A01 = -1;
    public String A02;
    public final String A03;

    public C27701Iu(String str) {
        this.A03 = str;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes(AnonymousClass01V.A0A));
            this.A02 = Base64.encodeToString(instance.digest(), 2);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public C27701Iu(String str, String str2) {
        this.A03 = str;
        this.A02 = str2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C27701Iu)) {
            return false;
        }
        C27701Iu r7 = (C27701Iu) obj;
        if (this.A01 != r7.A01 || this.A00 != r7.A00 || !C29941Vi.A00(this.A02, r7.A02) || !C29941Vi.A00(this.A03, r7.A03)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, Integer.valueOf(this.A00), this.A02, Long.valueOf(this.A01)});
    }
}
