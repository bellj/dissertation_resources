package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Base64;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.List;

/* renamed from: X.0ji  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13430ji {
    public int A00;
    public int A01 = 0;
    public String A02;
    public String A03;
    public final Context A04;

    public C13430ji(Context context) {
        this.A04 = context;
    }

    public static String A00(C13030j1 r3) {
        r3.A02();
        C13050j3 r1 = r3.A01;
        String str = r1.A04;
        if (str != null) {
            return str;
        }
        r3.A02();
        String str2 = r1.A01;
        if (str2.startsWith("1:")) {
            String[] split = str2.split(":");
            if (split.length >= 2) {
                str2 = split[1];
                if (str2.isEmpty()) {
                }
            }
            return null;
        }
        return str2;
    }

    public static String A01(PublicKey publicKey) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(publicKey.getEncoded());
            digest[0] = (byte) ((digest[0] & 15) + 112);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException unused) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required algorithms");
            return null;
        }
    }

    public final synchronized int A02() {
        int i = this.A01;
        if (i == 0) {
            PackageManager packageManager = this.A04.getPackageManager();
            int i2 = 0;
            if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
                Log.e("FirebaseInstanceId", "Google Play services missing or without correct permission.");
            } else {
                if (!C472729v.A03()) {
                    Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                    intent.setPackage("com.google.android.gms");
                    List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
                    if (queryIntentServices != null && queryIntentServices.size() > 0) {
                        this.A01 = 1;
                        return 1;
                    }
                }
                Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
                intent2.setPackage("com.google.android.gms");
                List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
                i2 = 2;
                if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() <= 0) {
                    Log.w("FirebaseInstanceId", "Failed to resolve IID implementation package, falling back");
                    if (C472729v.A03()) {
                        this.A01 = 2;
                        i = 2;
                    } else {
                        this.A01 = 1;
                        i = 1;
                    }
                } else {
                    this.A01 = 2;
                }
            }
            return i2;
        }
        return i;
    }

    public final synchronized int A03() {
        PackageInfo A04;
        if (this.A00 == 0 && (A04 = A04("com.google.android.gms")) != null) {
            this.A00 = A04.versionCode;
        }
        return this.A00;
    }

    public final PackageInfo A04(String str) {
        try {
            return this.A04.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("Failed to find package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    public final synchronized String A05() {
        if (this.A02 == null) {
            A06();
        }
        return this.A02;
    }

    public final synchronized void A06() {
        PackageInfo A04 = A04(this.A04.getPackageName());
        if (A04 != null) {
            this.A02 = Integer.toString(A04.versionCode);
            this.A03 = A04.versionName;
        }
    }
}
