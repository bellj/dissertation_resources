package X;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1e8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33471e8 {
    public static long A00(C14330lG r4, C33461e7 r5) {
        File file = r4.A04().A0A;
        C14330lG.A03(file, false);
        long A00 = C14350lI.A00(null, file);
        r5.A01.A05("STORAGE_USAGE_MEDIA_SIZE", String.valueOf(A00));
        r5.A01("STORAGE_USAGE_MEDIA_SIZE_CACHE_TIME");
        return A00;
    }

    public static Cursor A01(C15660nh r7, AbstractC14640lm r8, int i, int i2) {
        C16310on r1;
        Cursor cursor;
        String str;
        if (i != 0) {
            if (i == 1) {
                if (i2 == 0) {
                    Log.i("mediamsgstore/getForwardedMediaAndDocMessagesOrderedByIDDescCursor:");
                    String str2 = AnonymousClass3B7.A04;
                    String[] strArr = {String.valueOf(5)};
                    r1 = r7.A0C.get();
                    try {
                        cursor = r1.A03.A09(str2, strArr);
                    } finally {
                        try {
                            r1.close();
                        } catch (Throwable unused) {
                        }
                    }
                } else if (i2 == 1) {
                    Log.i("mediamsgstore/getForwardedMediaAndDocMessagesOrderedByIDAscCursor:");
                    String str3 = AnonymousClass3B7.A03;
                    String[] strArr2 = {String.valueOf(5)};
                    r1 = r7.A0C.get();
                    try {
                        cursor = r1.A03.A09(str3, strArr2);
                    } finally {
                        try {
                            r1.close();
                        } catch (Throwable unused2) {
                        }
                    }
                } else if (i2 == 2) {
                    Log.i("mediamsgstore/getForwardedMediaAndDocMessagesOrderedBySizeCursor:");
                    r1 = r7.A0C.get();
                    try {
                        if (r7.A08.A0A()) {
                            str = AnonymousClass3B7.A01;
                        } else {
                            str = AnonymousClass3B7.A02;
                        }
                        return r1.A03.A09(str, new String[]{String.valueOf(5)});
                    } finally {
                        try {
                            r1.close();
                        } catch (Throwable unused3) {
                        }
                    }
                } else if (i2 == 3) {
                    Log.i("mediamsgstore/getForwardedMediaAndDocMessagesOrderedByForwardingScoreCursor_INTERNAL_ONLY:");
                    String str4 = AnonymousClass3B7.A00;
                    String[] strArr3 = {String.valueOf(3)};
                    r1 = r7.A0C.get();
                    try {
                        cursor = r1.A03.A09(str4, strArr3);
                    } finally {
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unknown sort type: ");
                    sb.append(i2);
                    throw new IllegalArgumentException(sb.toString());
                }
                r1.close();
                return cursor;
            } else if (i != 2) {
                StringBuilder sb2 = new StringBuilder("Unknown gallery type: ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
            } else if (i2 == 0) {
                return r7.A07(null, 5000000);
            } else {
                if (i2 == 1) {
                    return r7.A06(null, 5000000);
                }
                if (i2 == 2) {
                    return r7.A05(null, 5000000);
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unknown sort type: ");
                sb3.append(i2);
                throw new IllegalArgumentException(sb3.toString());
            }
        } else if (i2 == 0) {
            return r7.A07(r8, -1);
        } else {
            if (i2 == 1) {
                return r7.A06(r8, -1);
            }
            if (i2 == 2) {
                return r7.A05(r8, -1);
            }
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Unknown sort type: ");
            sb4.append(i2);
            throw new IllegalArgumentException(sb4.toString());
        }
    }

    public static AnonymousClass1M9 A02(C15650ng r6, AnonymousClass19O r7, Integer num, Long l, List list) {
        if (!(num == null || l == null)) {
            ArrayList arrayList = new ArrayList();
            if (list != null) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    AbstractC15340mz A00 = r6.A0K.A00(((Number) it.next()).longValue());
                    if (A00 instanceof AbstractC16130oV) {
                        AbstractC16130oV r1 = (AbstractC16130oV) A00;
                        if (r1.A1A()) {
                            arrayList.add(AnonymousClass3GG.A00(r1, r7));
                        }
                    }
                }
            }
            return new AnonymousClass1M9(arrayList, num.intValue(), l.longValue());
        }
        return null;
    }

    public static String A03(Context context, AnonymousClass018 r10) {
        return context.getString(R.string.storage_usage_large_files_title, r10.A0H(new Object[]{String.format(context.getResources().getConfiguration().locale, "%d", 5L)}, 280, 5));
    }

    public static void A04(Activity activity, AbstractC13860kS r7, C16120oU r8, int i) {
        r7.Adq(new C1096252k(activity, AnonymousClass3GM.A00(r8, i), i), null, R.string.insufficient_storage_dialog_title, R.string.insufficient_storage_dialog_subtitle, R.string.manage_storage_button_text);
    }
}
