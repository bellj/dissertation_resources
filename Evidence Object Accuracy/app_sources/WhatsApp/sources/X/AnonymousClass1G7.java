package X;

/* renamed from: X.1G7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1G7 extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public AnonymousClass1G7() {
        super(AnonymousClass1G6.A0k);
    }

    public void A05() {
        A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) this.A00;
        r1.A01 &= -17;
        r1.A0X = AnonymousClass1G6.A0k.A0X;
    }

    public void A06(long j) {
        A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) this.A00;
        r1.A01 |= 4;
        r1.A0A = j;
    }

    public void A07(AnonymousClass1G8 r3) {
        A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) this.A00;
        r1.A0M = r3;
        r1.A01 |= 1;
    }

    public void A08(EnumC40021qv r3) {
        A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) this.A00;
        r1.A01 |= 16384;
        r1.A06 = r3.value;
    }

    public void A09(String str) {
        A03();
        AnonymousClass1G6 r2 = (AnonymousClass1G6) this.A00;
        AnonymousClass1K6 r1 = r2.A0G;
        if (!((AnonymousClass1K7) r1).A00) {
            r1 = AbstractC27091Fz.A0G(r1);
            r2.A0G = r1;
        }
        r1.add(str);
    }

    public void A0A(String str) {
        A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) this.A00;
        r1.A01 |= 16;
        r1.A0X = str;
    }
}
