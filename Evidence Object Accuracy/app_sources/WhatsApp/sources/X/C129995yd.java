package X;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5yd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129995yd {
    public final C30931Zj A00 = C117305Zk.A0V("RsaKeyStoreHelper", "infra");
    public final KeyStore A01;

    public C129995yd(KeyStore keyStore) {
        this.A01 = keyStore;
    }

    public KeyPair A00(Context context, String str, Date date, Date date2) {
        try {
            KeyPairGeneratorSpec.Builder alias = new KeyPairGeneratorSpec.Builder(context).setAlias(str);
            StringBuilder A0h = C12960it.A0h();
            A0h.append("CN=");
            KeyPairGeneratorSpec build = alias.setSubject(new X500Principal(C12960it.A0d(str, A0h))).setSerialNumber(BigInteger.TEN).setStartDate(date).setEndDate(date2).build();
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
            instance.initialize(build);
            return instance.generateKeyPair();
        } catch (Exception e) {
            this.A00.A0A("", e);
            return null;
        }
    }

    public void A01(String str) {
        try {
            KeyStore keyStore = this.A01;
            if (keyStore.containsAlias(str)) {
                keyStore.deleteEntry(str);
            }
        } catch (KeyStoreException unused) {
            this.A00.A05("failed removing key");
        }
    }

    public byte[] A02(byte[] bArr, String str) {
        try {
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            instance.init(2, ((KeyStore.PrivateKeyEntry) this.A01.getEntry(str, null)).getPrivateKey());
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            CipherInputStream cipherInputStream = new CipherInputStream(byteArrayInputStream, instance);
            try {
                ArrayList A0l = C12960it.A0l();
                while (true) {
                    int read = cipherInputStream.read();
                    if (read == -1) {
                        break;
                    }
                    A0l.add(Byte.valueOf((byte) read));
                }
                int size = A0l.size();
                byte[] bArr2 = new byte[size];
                for (int i = 0; i < size; i++) {
                    bArr2[i] = ((Byte) A0l.get(i)).byteValue();
                }
                cipherInputStream.close();
                byteArrayInputStream.close();
                return bArr2;
            } catch (Throwable th) {
                try {
                    cipherInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception e) {
            this.A00.A0A("", e);
            return null;
        }
    }

    public byte[] A03(byte[] bArr, String str) {
        try {
            return C117295Zj.A0P("RSA/ECB/PKCS1Padding", (KeyStore.PrivateKeyEntry) this.A01.getEntry(str, null), bArr).toByteArray();
        } catch (Exception e) {
            this.A00.A0A("", e);
            return null;
        }
    }
}
