package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import com.whatsapp.WaImageView;
import com.whatsapp.util.ViewOnClickCListenerShape3S1100000_I1;
import java.util.List;
import java.util.Set;

/* renamed from: X.34a  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34a extends AbstractC863546x {
    public View A00;
    public AnonymousClass18U A01;
    public WaImageView A02;
    public AnonymousClass018 A03;
    public AnonymousClass1BK A04;
    public C53202d5 A05;
    public boolean A06;

    public AnonymousClass34a(Context context) {
        super(context);
        A00();
        A03();
    }

    @Override // X.AbstractC74143hO
    public void A00() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A01 = C12990iw.A0T(A00);
            this.A03 = C12960it.A0R(A00);
            this.A04 = (AnonymousClass1BK) A00.AFZ.get();
        }
    }

    @Override // X.AnonymousClass46z
    public View A01() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 16;
        linearLayout.setLayoutParams(layoutParams);
        int A06 = C12980iv.A06(this);
        C42941w9.A0A(linearLayout, this.A03, A06, 0, A06, 0);
        this.A00 = C12960it.A0E(this).inflate(R.layout.suspicious_link_indicator, (ViewGroup) linearLayout, false);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.topMargin = AnonymousClass3G9.A01(getContext(), 4.0f);
        layoutParams2.bottomMargin = AnonymousClass3G9.A01(getContext(), 4.0f);
        this.A00.setLayoutParams(layoutParams2);
        this.A00.setVisibility(8);
        this.A05 = new C53202d5(getContext());
        this.A05.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.addView(this.A05);
        linearLayout.addView(this.A00);
        return linearLayout;
    }

    @Override // X.AnonymousClass46z
    public View A02() {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_attachment_height_regular);
        this.A02 = new WaImageView(getContext());
        this.A02.setLayoutParams(new FrameLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
        return this.A02;
    }

    public void setMessage(C28861Ph r10, List list) {
        String str;
        Bitmap decodeByteArray;
        int i = 0;
        C64643Gi A00 = C64643Gi.A00(getContext(), this.A04, r10, 0);
        AnonymousClass4QJ r5 = A00.A00;
        String str2 = r5.A01;
        String str3 = A00.A03;
        if (str3 == null) {
            str3 = "";
        }
        Set set = r5.A02;
        setPreviewClickListener(str2, set);
        boolean A1W = C12960it.A1W(set);
        byte[] A17 = r10.A17();
        if (A17 == null || (decodeByteArray = BitmapFactory.decodeByteArray(A17, 0, A17.length)) == null || A1W) {
            this.A02.setImageDrawable(AnonymousClass2GE.A01(getContext(), R.drawable.ic_group_invite_link, R.color.search_link_icon));
            this.A02.setScaleType(ImageView.ScaleType.CENTER);
            this.A02.setScaleX(1.5f);
            this.A02.setScaleY(1.5f);
            C12970iu.A18(getContext(), this.A02, R.color.black_alpha_05);
        } else {
            this.A02.setImageBitmap(decodeByteArray);
            C12990iw.A1E(this.A02);
        }
        if (set != null) {
            str = null;
        } else {
            str = A00.A02;
        }
        this.A05.setTitleAndDescription(str3, str, list);
        this.A05.setSubText(r5.A00, list);
        View view = this.A00;
        if (set == null) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public final void setPreviewClickListener(String str, Set set) {
        if (set != null) {
            setOnClickListener(new View.OnClickListener(str, set) { // from class: X.2jl
                public final /* synthetic */ String A01;
                public final /* synthetic */ Set A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ((ActivityC13810kN) AnonymousClass12P.A02(AnonymousClass34a.this)).Adm(SuspiciousLinkWarningDialogFragment.A00(this.A01, this.A02));
                }
            });
        } else {
            setOnClickListener(new ViewOnClickCListenerShape3S1100000_I1(1, str, this));
        }
    }
}
