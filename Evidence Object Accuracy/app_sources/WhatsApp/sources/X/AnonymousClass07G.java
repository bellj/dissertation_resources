package X;

import android.content.res.ColorStateList;
import android.os.Build;
import android.view.MenuItem;

/* renamed from: X.07G  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass07G {
    public static void A00(ColorStateList colorStateList, MenuItem menuItem) {
        if (menuItem instanceof AbstractMenuItemC017808h) {
            ((AbstractMenuItemC017808h) menuItem).setIconTintList(colorStateList);
        } else if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass0UJ.A00(colorStateList, menuItem);
        }
    }
}
