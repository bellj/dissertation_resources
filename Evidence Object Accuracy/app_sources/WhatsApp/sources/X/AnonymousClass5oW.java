package X;

import android.content.Intent;
import com.whatsapp.payments.ui.IndiaUpiStepUpActivity;
import java.util.List;

/* renamed from: X.5oW  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oW extends AbstractC16350or {
    public final C17070qD A00;
    public final C126945th A01;

    public AnonymousClass5oW(ActivityC13790kL r1, C17070qD r2, C126945th r3) {
        super(r1);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A0Z = C117295Zj.A0Z(this.A00);
        if (!A0Z.isEmpty()) {
            return A0Z.get(C1311161i.A01(A0Z));
        }
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AbstractC28901Pl r5 = (AbstractC28901Pl) obj;
        C126945th r1 = this.A01;
        C122225l7 r0 = r1.A00;
        String str = r1.A01;
        if (r5 != null) {
            ActivityC13790kL r2 = r0.A05;
            Intent A0D = C12990iw.A0D(r2, IndiaUpiStepUpActivity.class);
            C117315Zl.A0M(A0D, r5);
            A0D.putExtra("extra_step_up_id", str);
            r2.startActivity(A0D);
            return;
        }
        r0.A01();
    }
}
