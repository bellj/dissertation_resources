package X;

/* renamed from: X.5ZU  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5ZU extends AnonymousClass5X4 {
    @Override // X.AnonymousClass5X4
    AnonymousClass5ZU get(AbstractC115495Rt v);

    AbstractC115495Rt getKey();
}
