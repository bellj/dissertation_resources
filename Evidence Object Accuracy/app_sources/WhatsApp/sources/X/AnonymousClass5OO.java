package X;

import java.security.cert.CertificateEncodingException;

/* renamed from: X.5OO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OO extends AbstractC113485Ht {
    public final byte[] encoding;

    public AnonymousClass5OO(String str, C114585Mh r9, C114565Mf r10, AnonymousClass5S2 r11, byte[] bArr, byte[] bArr2, boolean[] zArr) {
        super(str, r9, r10, r11, bArr, zArr);
        this.encoding = bArr2;
    }

    @Override // X.AbstractC113485Ht, java.security.cert.Certificate
    public byte[] getEncoded() {
        byte[] bArr = this.encoding;
        if (bArr != null) {
            return bArr;
        }
        throw new CertificateEncodingException();
    }
}
