package X;

/* renamed from: X.4R4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R4 {
    public final int A00;
    public final boolean A01;
    public final byte[] A02;
    public final byte[] A03;

    public AnonymousClass4R4(byte[] bArr, byte[] bArr2, int i, boolean z) {
        this.A00 = i;
        this.A01 = z;
        this.A03 = bArr;
        this.A02 = bArr2;
    }
}
