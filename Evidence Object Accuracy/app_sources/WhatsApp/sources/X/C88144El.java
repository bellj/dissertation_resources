package X;

import com.whatsapp.R;

/* renamed from: X.4El  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88144El {
    public static int A00(int i, boolean z) {
        if (i != 401) {
            if (i != 404) {
                return R.string.register_try_again_later;
            }
            if (z) {
                return R.string.failed_create_invite_link_no_parent_group;
            }
            return R.string.failed_create_invite_link_no_group;
        } else if (z) {
            return R.string.failed_create_invite_link_not_admin_parent_group;
        } else {
            return R.string.failed_create_invite_link_not_admin;
        }
    }
}
