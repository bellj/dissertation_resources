package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.14y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242914y {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15990oG A02;
    public final C15600nX A03;
    public final C22830zi A04;
    public final C18770sz A05;

    public C242914y(AbstractC15710nm r1, C15570nT r2, C15990oG r3, C15600nX r4, C22830zi r5, C18770sz r6) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
        this.A03 = r4;
    }

    public static final List A00(Set set) {
        ArrayList arrayList = new ArrayList(set.size());
        Iterator it = set.iterator();
        while (it.hasNext()) {
            arrayList.add(C15940oB.A02((DeviceJid) it.next()));
        }
        ArrayList arrayList2 = new ArrayList();
        int size = arrayList.size() / 100;
        int size2 = arrayList.size() % 100;
        int i = 0;
        while (i < size) {
            int i2 = i * 100;
            i++;
            arrayList2.add(arrayList.subList(i2, i * 100));
        }
        if (size2 > 0) {
            arrayList2.add(arrayList.subList(arrayList.size() - size2, arrayList.size()));
        }
        return arrayList2;
    }

    public final Set A01(AbstractC14640lm r5) {
        AbstractC15590nW r52;
        Set A0C;
        HashSet hashSet = new HashSet();
        if (r5 instanceof UserJid) {
            boolean z = r5 instanceof AnonymousClass1MU;
            C18770sz r1 = this.A05;
            if (z) {
                A0C = r1.A0B();
            } else {
                A0C = r1.A0C();
            }
            hashSet.addAll(A0C);
            if (!this.A01.A0F(r5)) {
                UserJid of = UserJid.of(r5);
                AnonymousClass009.A05(of);
                hashSet.addAll(r1.A0D(of));
            }
            return hashSet;
        }
        C15600nX r12 = this.A03;
        if (r5 instanceof AbstractC15590nW) {
            r52 = (AbstractC15590nW) r5;
        } else {
            r52 = null;
        }
        AnonymousClass009.A05(r52);
        Iterator it = r12.A02(r52).A07().iterator();
        while (it.hasNext()) {
            hashSet.addAll(new HashSet(AnonymousClass1JO.A00(((AnonymousClass1YO) it.next()).A04.keySet()).A00));
        }
        C15570nT r0 = this.A01;
        r0.A08();
        hashSet.remove(r0.A04);
        return hashSet;
    }

    public Set A02(AbstractC15340mz r7) {
        AnonymousClass1IS r1 = r7.A0z;
        AbstractC14640lm r5 = r1.A00;
        if ((r7 instanceof AnonymousClass1XB) || !r1.A02) {
            return null;
        }
        if (((r5 instanceof UserJid) || (r5 instanceof AbstractC15590nW)) && r7.A0G == 0 && r7.A17 == null) {
            return A01(r5);
        }
        return null;
    }

    public Set A03(AbstractC15340mz r8) {
        AnonymousClass1IS r0 = r8.A0z;
        AbstractC14640lm r6 = r0.A00;
        if (!r0.A02 && !C30041Vv.A0N(this.A01, r8)) {
            return null;
        }
        Set A04 = A04(r8);
        if (A04 != null && (r6 instanceof AbstractC15590nW) && (r8.A0G != 0 || C30041Vv.A0N(this.A01, r8))) {
            A04.addAll(A01(r6));
        }
        return A04;
    }

    public final Set A04(AbstractC15340mz r5) {
        AnonymousClass1IS r2 = r5.A0z;
        AbstractC14640lm r1 = r2.A00;
        if (r1 instanceof AbstractC15590nW) {
            Set A00 = this.A04.A00(r2);
            HashSet hashSet = new HashSet(A00);
            for (UserJid userJid : C15380n4.A09(this.A00, A00)) {
                hashSet.addAll(this.A05.A0D(userJid));
            }
            hashSet.addAll(new HashSet(this.A05.A06().A00));
            C15570nT r0 = this.A01;
            r0.A08();
            hashSet.remove(r0.A04);
            return hashSet;
        } else if (r1 instanceof UserJid) {
            return A01(r1);
        } else {
            return null;
        }
    }
}
