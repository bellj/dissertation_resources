package X;

import com.whatsapp.R;

/* renamed from: X.69b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329969b implements AnonymousClass5W8 {
    public final /* synthetic */ AbstractActivityC121755jw A00;
    public final /* synthetic */ String A01;

    public C1329969b(AbstractActivityC121755jw r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass5W8
    public void APo(C452120p r5) {
        AbstractActivityC121755jw r3 = this.A00;
        r3.AaN();
        int i = r5.A00;
        AnonymousClass04S A01 = AnonymousClass61M.A01(r3, null, null, r3.A02.A00(i), i);
        if (A01 != null) {
            A01.show();
            return;
        }
        C30931Zj r2 = r3.A0D;
        StringBuilder A0k = C12960it.A0k("verifyPaymentMethodButtonOnClickListener/get-method/credential-id=");
        A0k.append(this.A01);
        r2.A05(C12960it.A0Z(r5, ", unhandled error=", A0k));
        r3.Ado(R.string.payment_verify_card_error);
    }

    @Override // X.AnonymousClass5W8
    public void AQy(AbstractC28901Pl r5) {
        AbstractActivityC121755jw r3 = this.A00;
        r3.AaN();
        if (r5 == null) {
            C30931Zj r2 = r3.A0D;
            StringBuilder A0k = C12960it.A0k("get-method: credential-id=");
            A0k.append(this.A01);
            r2.A05(C12960it.A0d(" null method", A0k));
            r3.Ado(R.string.payment_verify_card_error);
            return;
        }
        r3.A2i(r5, C12980iv.A1X(((AbstractView$OnClickListenerC121765jx) r3).A09));
    }
}
