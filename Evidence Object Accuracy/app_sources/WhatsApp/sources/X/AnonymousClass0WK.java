package X;

import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;

/* renamed from: X.0WK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WK implements View.OnClickListener {
    public final /* synthetic */ AbstractC009504t A00;
    public final /* synthetic */ ActionBarContextView A01;

    public AnonymousClass0WK(AbstractC009504t r1, ActionBarContextView actionBarContextView) {
        this.A01 = actionBarContextView;
        this.A00 = r1;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        this.A00.A05();
    }
}
