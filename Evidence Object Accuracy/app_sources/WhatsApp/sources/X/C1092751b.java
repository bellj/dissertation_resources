package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.51b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1092751b implements AbstractC116415Vi {
    public final List A00;

    @Override // X.AbstractC116415Vi
    public void AY4(AnonymousClass28D r1) {
    }

    public C1092751b(List list) {
        this.A00 = C12980iv.A0x(list);
    }

    @Override // X.AbstractC116415Vi
    public AnonymousClass28D A62(AnonymousClass28D r8) {
        List list;
        ArrayList arrayList = null;
        int i = 0;
        AnonymousClass28D r4 = r8;
        while (true) {
            list = this.A00;
            if (i >= list.size()) {
                break;
            }
            Pair pair = (Pair) list.get(i);
            if (((AnonymousClass5T5) pair.first).Aej(r4)) {
                if (arrayList == null) {
                    arrayList = C12960it.A0l();
                }
                arrayList.add(pair);
                if (((AnonymousClass4WH) pair.second).A01(r4)) {
                    if (r4 == r8) {
                        r4 = new AnonymousClass28D(r8, null, r8.A06, r8.A00);
                    }
                    ((AnonymousClass4WH) pair.second).A00(r4);
                }
            }
            i++;
        }
        if (arrayList != null) {
            list.removeAll(arrayList);
        }
        return r4;
    }
}
