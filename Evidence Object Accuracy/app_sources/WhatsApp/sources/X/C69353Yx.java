package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.3Yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69353Yx implements AbstractC32461cC {
    public final /* synthetic */ C626838e A00;

    public C69353Yx(C626838e r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32461cC
    public void APl(int i) {
        this.A00.A00 = i;
    }

    @Override // X.AbstractC32461cC
    public void AR7(C15580nU r12, C15580nU r13, UserJid userJid, AnonymousClass1PD r15, String str, String str2, String str3, Map map, int i, int i2, long j, long j2) {
        this.A00.A08(r12, userJid, r15, str, str3, map.keySet(), i, i2, j);
    }
}
