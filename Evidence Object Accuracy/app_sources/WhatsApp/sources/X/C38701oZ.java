package X;

import android.util.Pair;
import java.util.Comparator;
import java.util.HashMap;

/* renamed from: X.1oZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38701oZ implements Comparator {
    public final /* synthetic */ C235512c A00;
    public final /* synthetic */ HashMap A01;

    public C38701oZ(C235512c r1, HashMap hashMap) {
        this.A00 = r1;
        this.A01 = hashMap;
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        Pair pair = (Pair) obj;
        Pair pair2 = (Pair) obj2;
        String A01 = AnonymousClass144.A01((String) pair.first, (String) pair.second);
        String A012 = AnonymousClass144.A01((String) pair2.first, (String) pair2.second);
        HashMap hashMap = this.A01;
        Object obj3 = hashMap.get(A01);
        AnonymousClass009.A05(obj3);
        int intValue = ((Number) obj3).intValue();
        Object obj4 = hashMap.get(A012);
        AnonymousClass009.A05(obj4);
        return ((Number) obj4).intValue() - intValue;
    }
}
