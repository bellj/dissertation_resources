package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

/* renamed from: X.2UQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UQ implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99914l4();
    public final byte A00;
    public final int A01;
    public final File A02;
    public final boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass2UQ(AnonymousClass2U0 r2) {
        this.A00 = r2.AHf();
        this.A02 = r2.ACy();
        this.A03 = r2.AJR();
        this.A01 = r2.AGI();
    }

    public AnonymousClass2UQ(Parcel parcel) {
        this.A00 = parcel.readByte();
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A02 = new File(readString);
        this.A03 = parcel.readByte() != 1 ? false : true;
        this.A01 = parcel.readInt();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.A00);
        parcel.writeString(this.A02.getAbsolutePath());
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
        parcel.writeInt(this.A01);
    }
}
