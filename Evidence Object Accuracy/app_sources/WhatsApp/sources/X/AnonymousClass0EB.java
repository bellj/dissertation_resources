package X;

/* renamed from: X.0EB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EB extends AnonymousClass0P6 {
    public final Object A00;
    public final Object A01;
    public final boolean A02;

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r2 != X.AnonymousClass01E.A0m) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0EB(X.AnonymousClass02N r5, X.AnonymousClass0Q9 r6, boolean r7, boolean r8) {
        /*
            r4 = this;
            r4.<init>(r5, r6)
            X.0Jr r1 = r6.A01
            X.0Jr r0 = X.EnumC03930Jr.VISIBLE
            X.01E r3 = r6.A04
            if (r1 != r0) goto L_0x003e
            X.0O9 r0 = r3.A0C
            if (r7 == 0) goto L_0x0039
            if (r0 == 0) goto L_0x0017
            java.lang.Object r2 = r0.A09
            java.lang.Object r0 = X.AnonymousClass01E.A0m
            if (r2 != r0) goto L_0x0018
        L_0x0017:
            r2 = 0
        L_0x0018:
            r4.A01 = r2
            r0 = 1
            r4.A02 = r0
            if (r8 == 0) goto L_0x0037
            if (r7 == 0) goto L_0x0030
            X.0O9 r2 = r3.A0C
            if (r2 == 0) goto L_0x0037
            java.lang.Object r1 = r2.A0C
            java.lang.Object r0 = X.AnonymousClass01E.A0m
            if (r1 != r0) goto L_0x002d
            java.lang.Object r1 = r2.A0B
        L_0x002d:
            r4.A00 = r1
            return
        L_0x0030:
            X.0O9 r0 = r3.A0C
            if (r0 == 0) goto L_0x0037
            java.lang.Object r1 = r0.A0B
            goto L_0x002d
        L_0x0037:
            r1 = 0
            goto L_0x002d
        L_0x0039:
            if (r0 == 0) goto L_0x0017
            java.lang.Object r2 = r0.A08
            goto L_0x0018
        L_0x003e:
            if (r7 == 0) goto L_0x0017
            X.0O9 r1 = r3.A0C
            if (r1 == 0) goto L_0x0017
            java.lang.Object r2 = r1.A0A
            java.lang.Object r0 = X.AnonymousClass01E.A0m
            if (r2 != r0) goto L_0x0018
            java.lang.Object r2 = r1.A08
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0EB.<init>(X.02N, X.0Q9, boolean, boolean):void");
    }

    public final AbstractC06480Tu A02(Object obj) {
        if (obj == null) {
            return null;
        }
        AbstractC06480Tu r1 = AnonymousClass0TQ.A00;
        if (r1 != null && r1.A0G(obj)) {
            return r1;
        }
        AbstractC06480Tu r12 = AnonymousClass0TQ.A01;
        if (r12 != null && r12.A0G(obj)) {
            return r12;
        }
        StringBuilder sb = new StringBuilder("Transition ");
        sb.append(obj);
        sb.append(" for fragment ");
        sb.append(super.A01.A04);
        sb.append(" is not a valid framework Transition or AndroidX Transition");
        throw new IllegalArgumentException(sb.toString());
    }
}
