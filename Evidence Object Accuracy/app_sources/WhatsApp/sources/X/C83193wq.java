package X;

import android.view.animation.Animation;

/* renamed from: X.3wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83193wq extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C48232Fc A00;

    public C83193wq(C48232Fc r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        C48232Fc r2 = this.A00;
        r2.A02.setIconified(true);
        r2.A06.setVisibility(4);
    }
}
