package X;

import java.util.Arrays;

/* renamed from: X.2Np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50002Np extends AnonymousClass1JW {
    public final int A00;
    public final int A01;
    public final boolean A02;

    public C50002Np(AbstractC15710nm r1, C15450nH r2, int i, int i2, boolean z) {
        super(r1, r2);
        this.A00 = i;
        this.A02 = z;
        this.A01 = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C50002Np)) {
            return false;
        }
        C50002Np r4 = (C50002Np) obj;
        if (this.A00 == r4.A00 && this.A02 == r4.A02 && this.A01 == r4.A01) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), Boolean.valueOf(this.A02), Integer.valueOf(this.A01)});
    }
}
