package X;

import com.whatsapp.registration.VerifyTwoFactorAuth;
import java.lang.ref.WeakReference;

/* renamed from: X.1yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44421yt extends AbstractC16350or {
    public final C16590pI A00;
    public final C14820m6 A01;
    public final C20800wL A02;
    public final String A03;
    public final String A04;
    public final WeakReference A05;
    public final boolean A06 = true;

    public C44421yt(C16590pI r3, C14820m6 r4, C20800wL r5, VerifyTwoFactorAuth verifyTwoFactorAuth, String str, String str2) {
        super(verifyTwoFactorAuth);
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = str;
        this.A04 = str2;
        this.A05 = new WeakReference(verifyTwoFactorAuth);
        this.A00 = r3;
    }
}
