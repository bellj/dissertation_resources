package X;

import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.security.identity.IdentityCredential;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* renamed from: X.0AD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AD extends BiometricPrompt.AuthenticationCallback {
    public final /* synthetic */ AnonymousClass0PV A00;

    @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
    public void onAuthenticationHelp(int i, CharSequence charSequence) {
    }

    public AnonymousClass0AD(AnonymousClass0PV r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
    public void onAuthenticationError(int i, CharSequence charSequence) {
        this.A00.A01(i, charSequence);
    }

    @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
    public void onAuthenticationFailed() {
        this.A00.A00();
    }

    @Override // android.hardware.biometrics.BiometricPrompt.AuthenticationCallback
    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult authenticationResult) {
        AnonymousClass0U4 r3;
        IdentityCredential A01;
        if (authenticationResult != null) {
            BiometricPrompt.CryptoObject cryptoObject = authenticationResult.getCryptoObject();
            r3 = null;
            if (cryptoObject != null) {
                Cipher cipher = cryptoObject.getCipher();
                if (cipher != null) {
                    r3 = new AnonymousClass0U4(cipher);
                } else {
                    Signature signature = cryptoObject.getSignature();
                    if (signature != null) {
                        r3 = new AnonymousClass0U4(signature);
                    } else {
                        Mac mac = cryptoObject.getMac();
                        if (mac != null) {
                            r3 = new AnonymousClass0U4(mac);
                        } else if (Build.VERSION.SDK_INT >= 30 && (A01 = AnonymousClass0QZ.A01(cryptoObject)) != null) {
                            r3 = new AnonymousClass0U4(A01);
                        }
                    }
                }
            }
        } else {
            r3 = null;
        }
        int i = Build.VERSION.SDK_INT;
        int i2 = -1;
        if (i >= 30) {
            if (authenticationResult != null) {
                i2 = AnonymousClass0KF.A00(authenticationResult);
            }
        } else if (i != 29) {
            i2 = 2;
        }
        this.A00.A02(new C04700Ms(r3, i2));
    }
}
