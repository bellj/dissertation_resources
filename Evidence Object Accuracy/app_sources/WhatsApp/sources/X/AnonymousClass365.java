package X;

import android.text.Editable;
import com.whatsapp.polls.PollCreatorViewModel;
import java.util.List;

/* renamed from: X.365  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass365 extends C469928m {
    public final /* synthetic */ AnonymousClass349 A00;
    public final /* synthetic */ PollCreatorViewModel A01;

    public AnonymousClass365(AnonymousClass349 r1, PollCreatorViewModel pollCreatorViewModel) {
        this.A00 = r1;
        this.A01 = pollCreatorViewModel;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        AnonymousClass349 r4 = this.A00;
        if (r4.A00() != -1) {
            String trim = editable.toString().trim();
            int A00 = r4.A00() - 1;
            PollCreatorViewModel pollCreatorViewModel = this.A01;
            List<C861846a> list = pollCreatorViewModel.A0F;
            if (list.size() < pollCreatorViewModel.A06.A02(1408) && trim.length() == 1) {
                if (C12990iw.A09(list, 1) != A00) {
                    for (C861846a r0 : list) {
                        if (r0.A00.isEmpty()) {
                            break;
                        }
                    }
                }
                list.add(new C861846a(((AbstractC89694Ky) list.get(C12980iv.A0C(list))).A00 + 1));
                pollCreatorViewModel.A04();
                pollCreatorViewModel.A0D.A0A(C12970iu.A0l());
            }
            if (pollCreatorViewModel.A05(trim, A00)) {
                int i = r4.A00;
                int length = trim.length();
                int i2 = pollCreatorViewModel.A00;
                if (i == i2) {
                    pollCreatorViewModel.A0B.A0B(Boolean.TRUE);
                    pollCreatorViewModel.A00 = -1;
                } else if (length < 2) {
                    C12990iw.A1J(pollCreatorViewModel.A0B, pollCreatorViewModel.A06(C12990iw.A1W(i2)));
                }
            }
        }
    }
}
