package X;

import android.net.Uri;
import android.text.TextUtils;

/* renamed from: X.4Eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88044Eb {
    public static String A00(String str, long j) {
        return !TextUtils.isEmpty(str) ? AbstractC37791n0.A00(Uri.parse(str).buildUpon(), "_nc_hot", String.valueOf(C12980iv.A0D(j))).build().toString() : str;
    }
}
