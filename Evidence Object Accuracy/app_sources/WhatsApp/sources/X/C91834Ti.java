package X;

import com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl;

/* renamed from: X.4Ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91834Ti {
    public static C91834Ti A07;
    public AnimatedFactoryV2Impl A00;
    public AnonymousClass4UM A01;
    public C105914up A02;
    public AbstractC94334bd A03;
    public final AnonymousClass4I1 A04;
    public final AnonymousClass4UG A05;
    public final C89784Lj A06;

    public C91834Ti(AnonymousClass4UG r3) {
        AnonymousClass4Yy.A00();
        this.A05 = r3;
        this.A06 = new C89784Lj(((C106074v5) r3.A0A).A03);
        this.A04 = new AnonymousClass4I1(r3.A0C);
        AnonymousClass4Yy.A00();
    }
}
