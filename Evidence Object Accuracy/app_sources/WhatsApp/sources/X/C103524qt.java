package X;

import android.content.Context;
import com.whatsapp.status.SetStatus;

/* renamed from: X.4qt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103524qt implements AbstractC009204q {
    public final /* synthetic */ SetStatus A00;

    public C103524qt(SetStatus setStatus) {
        this.A00 = setStatus;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
