package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.2ZA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZA extends BroadcastReceiver {
    public C14830m7 A00;
    public C14820m6 A01;
    public C26991Fp A02;
    public final Object A03 = C12970iu.A0l();
    public volatile boolean A04 = false;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A04) {
            synchronized (this.A03) {
                if (!this.A04) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = C12980iv.A0b(r1);
                    this.A01 = C12970iu.A0Z(r1);
                    this.A02 = (C26991Fp) r1.A5r.get();
                    this.A04 = true;
                }
            }
        }
        Log.i("ProcessProviderMigrationInfo/on-receive");
        Bundle resultExtras = getResultExtras(true);
        if (getResultCode() == -1 && resultExtras != null && intent != null) {
            boolean z = false;
            if ("com.whatsapp.registration.directmigration.initialMigrationInfoAction".equals(intent.getAction())) {
                Log.i("ProcessProviderMigrationInfo/received-phone-number");
                C14820m6 r12 = this.A01;
                C12970iu.A1D(C12960it.A08(r12), "registration_sibling_app_country_code", resultExtras.getString("me_country_code", null));
                C14820m6 r13 = this.A01;
                C12970iu.A1D(C12960it.A08(r13), "registration_sibling_app_phone_number", resultExtras.getString("phone_number", null));
                C14820m6 r3 = this.A01;
                C12960it.A0u(r3.A00, "direct_db_migration_timeout_in_secs", resultExtras.getInt("direct_db_migration_timeout_in_secs", 180));
                boolean z2 = resultExtras.getBoolean("sister_app_content_provider_enabled", false);
                C12960it.A0t(C12960it.A08(this.A01), "sister_app_content_provider_is_enabled", z2);
                StringBuilder A0k = C12960it.A0k("ProcessProviderMigrationInfo/sister-app-content-provider-is-enabled = ");
                A0k.append(z2);
                C12960it.A1F(A0k);
            } else if ("com.whatsapp.registration.directmigration.recoveryTokenAction".equals(intent.getAction())) {
                Log.i("ProcessProviderMigrationInfo/received-token");
                String string = this.A01.A00.getString("registration_sibling_app_country_code", null);
                String string2 = this.A01.A00.getString("registration_sibling_app_phone_number", null);
                String A00 = C32591cP.A00(C12960it.A0d(string2, C12960it.A0j(string)));
                byte[] byteArray = resultExtras.getByteArray("key_recovery_token");
                if (!TextUtils.isEmpty(A00) && byteArray != null) {
                    C003501n.A0A(context, A00, byteArray);
                    z = true;
                }
                byte[] byteArray2 = resultExtras.getByteArray("key_backup_token");
                if (!TextUtils.isEmpty(string2) && byteArray2 != null) {
                    C49352Kk.A00(context, this.A00, this.A01, string2, byteArray2);
                } else if (!z) {
                    return;
                }
                this.A02.A01.A00 = true;
            }
        }
    }
}
