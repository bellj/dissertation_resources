package X;

import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.38p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627938p extends AbstractC16350or {
    public final int A00;
    public final AnonymousClass1KZ A01;
    public final /* synthetic */ StickerStorePackPreviewActivity A02;

    public C627938p(AnonymousClass1KZ r1, StickerStorePackPreviewActivity stickerStorePackPreviewActivity, int i) {
        this.A02 = stickerStorePackPreviewActivity;
        this.A00 = i;
        this.A01 = r1;
    }
}
