package X;

import android.content.DialogInterface;
import com.whatsapp.calling.calllink.view.CallLinkActivity;
import com.whatsapp.calling.calllink.viewmodel.CallLinkViewModel;

/* renamed from: X.41u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852441u extends C50102Ob {
    public final /* synthetic */ CallLinkActivity A00;

    public C852441u(CallLinkActivity callLinkActivity) {
        this.A00 = callLinkActivity;
    }

    @Override // X.C50102Ob
    public void A00() {
        CallLinkViewModel callLinkViewModel = this.A00.A05;
        if (callLinkViewModel != null) {
            callLinkViewModel.A04(callLinkViewModel.A05());
        }
    }

    @Override // X.C50102Ob
    public void A01(DialogInterface dialogInterface) {
        this.A00.finish();
    }
}
