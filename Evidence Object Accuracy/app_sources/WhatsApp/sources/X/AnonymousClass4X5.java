package X;

import android.text.TextUtils;

/* renamed from: X.4X5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4X5 {
    public final String A00;
    public final boolean A01;
    public final boolean A02;

    public AnonymousClass4X5(String str, boolean z, boolean z2) {
        this.A00 = str;
        this.A01 = z;
        this.A02 = z2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || obj.getClass() != AnonymousClass4X5.class) {
                return false;
            }
            AnonymousClass4X5 r5 = (AnonymousClass4X5) obj;
            if (!(TextUtils.equals(this.A00, r5.A00) && this.A01 == r5.A01 && this.A02 == r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int hashCode = (this.A00.hashCode() + 31) * 31;
        int i = 1231;
        int i2 = 1237;
        if (this.A01) {
            i2 = 1231;
        }
        int i3 = (hashCode + i2) * 31;
        if (!this.A02) {
            i = 1237;
        }
        return i3 + i;
    }
}
