package X;

import org.json.JSONObject;

/* renamed from: X.4bD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94074bD {
    public int A00;
    public int A01;
    public long A02;
    public AbstractC30791Yv A03;

    public C94074bD() {
    }

    public C94074bD(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.A02 = jSONObject.optLong("value", -1);
            this.A01 = jSONObject.optInt("offset", -1);
            this.A00 = jSONObject.optInt("currencyType", -1);
            this.A03 = AnonymousClass102.A01(jSONObject.optJSONObject("currency"), this.A00);
        }
    }

    public AnonymousClass20C A00() {
        int i = this.A01;
        long j = this.A02;
        if (i <= 0) {
            return new AnonymousClass20C(this.A03, j);
        }
        return new AnonymousClass20C(this.A03, i, j);
    }
}
