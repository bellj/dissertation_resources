package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.1ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35751ig {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public Context A05;
    public boolean A06;
    public boolean A07;
    public int[] A08;
    public int[] A09;
    public int[] A0A;
    public String[] A0B;
    public String[] A0C;

    public C35751ig(Context context) {
        this.A05 = context;
    }

    public Intent A00() {
        Intent intent = new Intent();
        intent.setClassName(this.A05.getPackageName(), "com.whatsapp.RequestPermissionActivity");
        intent.putExtra("drawable_id", this.A01);
        intent.putExtra("drawable_ids", this.A09);
        intent.putExtra("message_id", this.A02);
        intent.putExtra("message_params_id", this.A0A);
        intent.putExtra("cancel_button_message_id", this.A00);
        intent.putExtra("perm_denial_message_id", this.A03);
        intent.putExtra("perm_denial_message_params_id", this.A08);
        intent.putExtra("permissions", this.A0C);
        intent.putExtra("force_ui", this.A06);
        intent.putExtra("minimal_partial_permissions", this.A0B);
        intent.putExtra("title_id", this.A04);
        intent.putExtra("hide_permissions_rationale", this.A07);
        return intent;
    }
}
