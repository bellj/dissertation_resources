package X;

import com.whatsapp.jid.DeviceJid;
import java.util.Comparator;

/* renamed from: X.5Ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C112195Ck implements Comparator {
    public final /* synthetic */ AnonymousClass2L8 A00;

    public /* synthetic */ C112195Ck(AnonymousClass2L8 r1) {
        this.A00 = r1;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        AnonymousClass1V8 r7 = (AnonymousClass1V8) obj;
        AnonymousClass1V8 r8 = (AnonymousClass1V8) obj2;
        try {
            AbstractC15710nm r2 = this.A00.A04;
            boolean isPrimary = ((DeviceJid) r7.A0B(r2, DeviceJid.class, "jid")).isPrimary();
            if (isPrimary != ((DeviceJid) r8.A0B(r2, DeviceJid.class, "jid")).isPrimary()) {
                return isPrimary ? -1 : 1;
            }
            return 0;
        } catch (AnonymousClass1V9 unused) {
            return 0;
        }
    }
}
