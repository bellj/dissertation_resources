package X;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

@Deprecated
/* renamed from: X.0GF  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0GF extends AnonymousClass01A {
    public C004902f A00 = null;
    public AnonymousClass01E A01 = null;
    public ArrayList A02 = new ArrayList();
    public ArrayList A03 = new ArrayList();
    public boolean A04;
    public final int A05;
    public final AnonymousClass01F A06;

    public abstract AnonymousClass01E A0F(int i);

    public AnonymousClass0GF(AnonymousClass01F r4) {
        this.A06 = r4;
        this.A05 = 1;
    }

    @Override // X.AnonymousClass01A
    public Parcelable A03() {
        Bundle bundle;
        ArrayList arrayList = this.A03;
        if (arrayList.size() > 0) {
            bundle = new Bundle();
            C06780Vb[] r1 = new C06780Vb[arrayList.size()];
            arrayList.toArray(r1);
            bundle.putParcelableArray("states", r1);
        } else {
            bundle = null;
        }
        int i = 0;
        while (true) {
            ArrayList arrayList2 = this.A02;
            if (i >= arrayList2.size()) {
                return bundle;
            }
            AnonymousClass01E r2 = (AnonymousClass01E) arrayList2.get(i);
            if (r2 != null && r2.A0c()) {
                if (bundle == null) {
                    bundle = new Bundle();
                }
                StringBuilder sb = new StringBuilder("f");
                sb.append(i);
                this.A06.A0P(bundle, r2, sb.toString());
            }
            i++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r3 != null) goto L_0x000e;
     */
    @Override // X.AnonymousClass01A
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A05(android.view.ViewGroup r6, int r7) {
        /*
            r5 = this;
            java.util.ArrayList r4 = r5.A02
            int r0 = r4.size()
            if (r0 <= r7) goto L_0x000f
            java.lang.Object r3 = r4.get(r7)
            if (r3 == 0) goto L_0x000f
        L_0x000e:
            return r3
        L_0x000f:
            X.02f r0 = r5.A00
            if (r0 != 0) goto L_0x001c
            X.01F r1 = r5.A06
            X.02f r0 = new X.02f
            r0.<init>(r1)
            r5.A00 = r0
        L_0x001c:
            X.01E r3 = r5.A0F(r7)
            java.util.ArrayList r1 = r5.A03
            int r0 = r1.size()
            if (r0 <= r7) goto L_0x003b
            java.lang.Object r1 = r1.get(r7)
            X.0Vb r1 = (X.C06780Vb) r1
            if (r1 == 0) goto L_0x003b
            X.01F r0 = r3.A0H
            if (r0 != 0) goto L_0x0068
            android.os.Bundle r0 = r1.A00
            if (r0 != 0) goto L_0x0039
            r0 = 0
        L_0x0039:
            r3.A06 = r0
        L_0x003b:
            int r0 = r4.size()
            if (r0 > r7) goto L_0x0046
            r0 = 0
            r4.add(r0)
            goto L_0x003b
        L_0x0046:
            r0 = 0
            r3.A0b(r0)
            int r2 = r5.A05
            if (r2 != 0) goto L_0x0051
            r3.A0n(r0)
        L_0x0051:
            r4.set(r7, r3)
            X.02f r1 = r5.A00
            int r0 = r6.getId()
            r1.A06(r3, r0)
            r0 = 1
            if (r2 != r0) goto L_0x000e
            X.02f r1 = r5.A00
            X.05I r0 = X.AnonymousClass05I.STARTED
            r1.A08(r3, r0)
            return r3
        L_0x0068:
            java.lang.String r1 = "Fragment already added"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GF.A05(android.view.ViewGroup, int):java.lang.Object");
    }

    @Override // X.AnonymousClass01A
    public void A09(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            ArrayList arrayList = this.A03;
            arrayList.clear();
            ArrayList arrayList2 = this.A02;
            arrayList2.clear();
            if (parcelableArray != null) {
                for (Parcelable parcelable2 : parcelableArray) {
                    arrayList.add(parcelable2);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    AnonymousClass01E A08 = this.A06.A08(bundle, str);
                    if (A08 == null) {
                        StringBuilder sb = new StringBuilder("Bad fragment at key ");
                        sb.append(str);
                        Log.w("FragmentStatePagerAdapt", sb.toString());
                    } else {
                        while (arrayList2.size() <= parseInt) {
                            arrayList2.add(null);
                        }
                        A08.A0b(false);
                        arrayList2.set(parseInt, A08);
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        C004902f r2 = this.A00;
        if (r2 != null) {
            if (!this.A04) {
                try {
                    this.A04 = true;
                    r2.A03();
                } finally {
                    this.A04 = false;
                }
            }
            this.A00 = null;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0B(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            StringBuilder sb = new StringBuilder("ViewPager with adapter ");
            sb.append(this);
            sb.append(" requires a view id");
            throw new IllegalStateException(sb.toString());
        }
    }

    @Override // X.AnonymousClass01A
    public void A0C(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01E r6 = (AnonymousClass01E) obj;
        AnonymousClass01E r0 = this.A01;
        if (r6 != r0) {
            if (r0 != null) {
                r0.A0b(false);
                if (this.A05 == 1) {
                    C004902f r2 = this.A00;
                    if (r2 == null) {
                        r2 = new C004902f(this.A06);
                        this.A00 = r2;
                    }
                    r2.A08(this.A01, AnonymousClass05I.STARTED);
                } else {
                    this.A01.A0n(false);
                }
            }
            r6.A0b(true);
            if (this.A05 == 1) {
                C004902f r1 = this.A00;
                if (r1 == null) {
                    r1 = new C004902f(this.A06);
                    this.A00 = r1;
                }
                r1.A08(r6, AnonymousClass05I.RESUMED);
            } else {
                r6.A0n(true);
            }
            this.A01 = r6;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        ArrayList arrayList;
        C06780Vb r0;
        AnonymousClass01E r5 = (AnonymousClass01E) obj;
        if (this.A00 == null) {
            this.A00 = new C004902f(this.A06);
        }
        while (true) {
            arrayList = this.A03;
            if (arrayList.size() > i) {
                break;
            }
            arrayList.add(null);
        }
        if (r5.A0c()) {
            r0 = this.A06.A06(r5);
        } else {
            r0 = null;
        }
        arrayList.set(i, r0);
        this.A02.set(i, null);
        this.A00.A05(r5);
        if (r5.equals(this.A01)) {
            this.A01 = null;
        }
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return ((AnonymousClass01E) obj).A0A == view;
    }
}
