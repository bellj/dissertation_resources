package X;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.19h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254119h {
    public final C14900mE A00;
    public final C16170oZ A01;
    public final C238013b A02;
    public final C15550nR A03;
    public final C20020v5 A04;
    public final C18640sm A05;
    public final C21320xE A06;
    public final C14850m9 A07;
    public final C20710wC A08;
    public final C20660w7 A09;
    public final C14860mA A0A;

    public C254119h(C14900mE r1, C16170oZ r2, C238013b r3, C15550nR r4, C20020v5 r5, C18640sm r6, C21320xE r7, C14850m9 r8, C20710wC r9, C20660w7 r10, C14860mA r11) {
        this.A07 = r8;
        this.A00 = r1;
        this.A0A = r11;
        this.A09 = r10;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A08 = r9;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
    }

    public void A00(Activity activity, AnonymousClass1MF r20, C15370n3 r21, String str, String str2, String str3, boolean z) {
        if (!r21.A0K()) {
            Jid A0B = r21.A0B(UserJid.class);
            AnonymousClass009.A05(A0B);
            UserJid userJid = (UserJid) A0B;
            this.A02.A0A(activity, r21, userJid, str, str2, str3);
            if (z) {
                this.A01.A0J(userJid, true, true);
            }
            if (r20 != null) {
                r20.AYA(r21);
                return;
            }
            return;
        }
        C20660w7 r1 = this.A09;
        C14860mA r9 = this.A0A;
        C20710wC r7 = this.A08;
        C21320xE r5 = this.A06;
        Jid A0B2 = r21.A0B(C15580nU.class);
        AnonymousClass009.A05(A0B2);
        r1.A06(new AnonymousClass32W(r20, this, r5, r21, r7, (C15580nU) A0B2, r9, z));
    }

    public void A01(C15370n3 r14, String str, List list) {
        Jid A0B = r14.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        AbstractC14640lm r8 = (AbstractC14640lm) A0B;
        C20020v5 r6 = this.A04;
        synchronized (r6) {
            if (r6.A0M.A07(1034)) {
                SharedPreferences A04 = r6.A04();
                String rawString = r8.getRawString();
                StringBuilder sb = new StringBuilder();
                sb.append(rawString);
                sb.append("_integrity");
                String obj = sb.toString();
                C40631ru A00 = C40631ru.A00(A04.getString(obj, "0,null,null"));
                A00.A00++;
                A04.edit().putString(obj, A00.toString()).apply();
            }
        }
        this.A01.A0G(r8, null, str, list, !r14.A0K());
        r14.A0c = true;
        C15550nR r4 = this.A03;
        r14.A0c = true;
        C21580xe r2 = r4.A06;
        C28181Ma r5 = new C28181Ma(true);
        r5.A03();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("is_spam_reported", Boolean.valueOf(r14.A0c));
        r2.A0E(contentValues, r14.A0D);
        StringBuilder sb2 = new StringBuilder("updated is reported spam for jid=");
        sb2.append(r14.A0D);
        sb2.append(' ');
        sb2.append(contentValues);
        sb2.append(" | time: ");
        sb2.append(r5.A00());
        Log.i(sb2.toString());
        r4.A04.A00(r14);
    }

    public boolean A02(Context context) {
        if (this.A05.A0B()) {
            return true;
        }
        Log.w("spamreportmanager/spam/report/no-network-cannot-block-report");
        boolean A03 = C18640sm.A03(context);
        int i = R.string.no_network_cannot_block;
        if (A03) {
            i = R.string.no_network_cannot_block_airplane;
        }
        this.A00.A07(i, 0);
        return false;
    }
}
