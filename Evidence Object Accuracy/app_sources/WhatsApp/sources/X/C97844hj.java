package X;

import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.view.Surface;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/* renamed from: X.4hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97844hj implements SurfaceTexture.OnFrameAvailableListener {
    public SurfaceTexture A00;
    public Surface A01;
    public C94274bX A02;
    public EGL10 A03;
    public EGLContext A04;
    public EGLDisplay A05;
    public EGLSurface A06;
    public boolean A07;
    public final Object A08 = C12970iu.A0l();

    public C97844hj() {
        A02();
    }

    public C97844hj(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            throw C72453ed.A0h();
        }
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        this.A03 = egl10;
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        this.A05 = eglGetDisplay;
        if (this.A03.eglInitialize(eglGetDisplay, null)) {
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            if (this.A03.eglChooseConfig(this.A05, new int[]{12324, 8, 12323, 8, 12322, 8, 12339, 1, 12352, 4, 12344}, eGLConfigArr, 1, new int[1])) {
                this.A04 = this.A03.eglCreateContext(this.A05, eGLConfigArr[0], EGL10.EGL_NO_CONTEXT, new int[]{12440, 2, 12344});
                A01();
                if (this.A04 != null) {
                    this.A06 = this.A03.eglCreatePbufferSurface(this.A05, eGLConfigArr[0], new int[]{12375, i, 12374, i2, 12344});
                    A01();
                    if (this.A06 != null) {
                        A00();
                        A02();
                        return;
                    }
                    throw C12990iw.A0m("surface was null");
                }
                throw C12990iw.A0m("null context");
            }
            throw C12990iw.A0m("unable to find RGB888+pbuffer egl config");
        }
        throw C12990iw.A0m("unable to initialize EGL10");
    }

    public void A00() {
        EGL10 egl10 = this.A03;
        if (egl10 != null) {
            A01();
            EGLDisplay eGLDisplay = this.A05;
            EGLSurface eGLSurface = this.A06;
            if (!egl10.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, this.A04)) {
                throw C12990iw.A0m("eglMakeCurrent failed");
            }
            return;
        }
        throw C12990iw.A0m("not configured for makeCurrent");
    }

    public final void A01() {
        boolean z = false;
        while (this.A03.eglGetError() != 12288) {
            z = true;
        }
        if (z) {
            throw C12990iw.A0m("EGL error encountered (see log)");
        }
    }

    public final void A02() {
        int A01;
        C94274bX r4 = new C94274bX();
        this.A02 = r4;
        int A012 = r4.A01(35633, "uniform mat4 uMVPMatrix;\nuniform mat4 uSTMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n}\n");
        int i = 0;
        if (!(A012 == 0 || (A01 = r4.A01(35632, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n  gl_FragColor = texture2D(sTexture, vTextureCoord);\n}\n")) == 0)) {
            int glCreateProgram = GLES20.glCreateProgram();
            C94274bX.A00("glCreateProgram");
            GLES20.glAttachShader(glCreateProgram, A012);
            C94274bX.A00("glAttachShader");
            GLES20.glAttachShader(glCreateProgram, A01);
            C94274bX.A00("glAttachShader");
            GLES20.glLinkProgram(glCreateProgram);
            int[] iArr = new int[1];
            GLES20.glGetProgramiv(glCreateProgram, 35714, iArr, 0);
            if (iArr[0] != 1) {
                GLES20.glDeleteProgram(glCreateProgram);
            } else {
                i = glCreateProgram;
            }
        }
        r4.A02 = i;
        if (i != 0) {
            r4.A00 = GLES20.glGetAttribLocation(i, "aPosition");
            C94274bX.A00("glGetAttribLocation aPosition");
            if (r4.A00 != -1) {
                r4.A01 = GLES20.glGetAttribLocation(r4.A02, "aTextureCoord");
                C94274bX.A00("glGetAttribLocation aTextureCoord");
                if (r4.A01 != -1) {
                    r4.A04 = GLES20.glGetUniformLocation(r4.A02, "uMVPMatrix");
                    C94274bX.A00("glGetUniformLocation uMVPMatrix");
                    if (r4.A04 != -1) {
                        r4.A05 = GLES20.glGetUniformLocation(r4.A02, "uSTMatrix");
                        C94274bX.A00("glGetUniformLocation uSTMatrix");
                        if (r4.A05 != -1) {
                            int[] iArr2 = new int[1];
                            GLES20.glGenTextures(1, iArr2, 0);
                            int i2 = iArr2[0];
                            r4.A03 = i2;
                            GLES20.glBindTexture(36197, i2);
                            C94274bX.A00("glBindTexture textureID");
                            GLES20.glTexParameterf(36197, 10241, 9729.0f);
                            GLES20.glTexParameterf(36197, 10240, 9729.0f);
                            GLES20.glTexParameteri(36197, 10242, 33071);
                            GLES20.glTexParameteri(36197, 10243, 33071);
                            C94274bX.A00("glTexParameter");
                            SurfaceTexture surfaceTexture = new SurfaceTexture(this.A02.A03);
                            this.A00 = surfaceTexture;
                            surfaceTexture.setOnFrameAvailableListener(this);
                            this.A01 = new Surface(this.A00);
                            return;
                        }
                        throw C12990iw.A0m("Could not get attrib location for uSTMatrix");
                    }
                    throw C12990iw.A0m("Could not get attrib location for uMVPMatrix");
                }
                throw C12990iw.A0m("Could not get attrib location for aTextureCoord");
            }
            throw C12990iw.A0m("Could not get attrib location for aPosition");
        }
        throw C12990iw.A0m("failed creating program");
    }

    @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        Object obj = this.A08;
        synchronized (obj) {
            if (!this.A07) {
                this.A07 = true;
                obj.notifyAll();
            } else {
                throw C12990iw.A0m("frameAvailable already set, frame could be dropped");
            }
        }
    }
}
