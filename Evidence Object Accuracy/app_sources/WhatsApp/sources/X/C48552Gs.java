package X;

import java.util.Comparator;

/* renamed from: X.2Gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C48552Gs implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ((AbstractC48542Gr) obj2).AFu() - ((AbstractC48542Gr) obj).AFu();
    }
}
