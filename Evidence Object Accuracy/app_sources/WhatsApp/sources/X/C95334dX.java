package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.4dX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95334dX {
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:97:0x0146 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.util.LinkedHashMap, java.util.AbstractMap] */
    public static final Object A00(AnonymousClass4O5 r11, Object obj) {
        Object obj2;
        int i;
        Object obj3;
        Object key;
        if (obj instanceof Map) {
            obj2 = new LinkedHashMap();
            Iterator A0n = C12960it.A0n((Map) obj);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                if (A15.getKey() instanceof String) {
                    Object key2 = A15.getKey();
                    if (key2 != null) {
                        String str = ".$";
                        if (AnonymousClass03C.A0K((String) key2, str)) {
                            Object value = A15.getValue();
                            if (value != null) {
                                String str2 = (String) value;
                                i = 0;
                                C16700pc.A0E(str2, 0);
                                C93934az r1 = r11.A00;
                                if (r1 == null) {
                                    Map map = r11.A01;
                                    C89104Ir r12 = new C89104Ir();
                                    C95094d8.A03(map, "json object can not be null");
                                    r1 = new C93934az(r12.A00, map);
                                    r11.A00 = r1;
                                }
                                obj3 = null;
                                try {
                                    obj3 = r1.A01(str2, new AnonymousClass5T6[0]);
                                } catch (C82803wD unused) {
                                }
                                if (obj3 == null) {
                                    StringBuilder A0k = C12960it.A0k("FcsStateIoUtils/evaluate/required path failed to evaluate. Found a null value for '");
                                    Object key3 = A15.getKey();
                                    if (key3 == null) {
                                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                                    }
                                    A0k.append((String) key3);
                                    throw new AssertionError(C12960it.A0d("'.", A0k));
                                }
                                key = A15.getKey();
                                if (key == null) {
                                    throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                                }
                            } else {
                                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                            }
                        } else {
                            Object key4 = A15.getKey();
                            if (key4 != null) {
                                str = ".$?";
                                if (AnonymousClass03C.A0K((String) key4, str)) {
                                    Object value2 = A15.getValue();
                                    if (value2 != null) {
                                        String str3 = (String) value2;
                                        i = 0;
                                        C16700pc.A0E(str3, 0);
                                        C93934az r13 = r11.A00;
                                        if (r13 == null) {
                                            Map map2 = r11.A01;
                                            C89104Ir r14 = new C89104Ir();
                                            C95094d8.A03(map2, "json object can not be null");
                                            r13 = new C93934az(r14.A00, map2);
                                            r11.A00 = r13;
                                        }
                                        obj3 = null;
                                        try {
                                            obj3 = r13.A01(str3, new AnonymousClass5T6[0]);
                                        } catch (C82803wD unused2) {
                                        }
                                        if (obj3 == null) {
                                            continue;
                                        } else {
                                            key = A15.getKey();
                                            if (key == null) {
                                                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                                            }
                                        }
                                    } else {
                                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                                    }
                                } else {
                                    Object key5 = A15.getKey();
                                    if (key5 != null) {
                                        obj2.put(key5, A00(r11, A15.getValue()));
                                    } else {
                                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                                    }
                                }
                            } else {
                                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                            }
                        }
                        String str4 = (String) key;
                        C16700pc.A0E(str4, i);
                        if (AnonymousClass03C.A0K(str4, str)) {
                            str4 = str4.substring(i, str4.length() - str.length());
                            C16700pc.A0B(str4);
                        }
                        obj2.put(str4, obj3);
                    } else {
                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                    }
                } else {
                    throw new AssertionError("FcsStateIoUtils/evaluate/key in map is not string");
                }
            }
        } else if (!(obj instanceof Object[])) {
            return obj;
        } else {
            obj2 = C12960it.A0l();
            Object[] objArr = (Object[]) obj;
            int i2 = 0;
            int length = objArr.length;
            while (i2 < length) {
                Object obj4 = objArr[i2];
                i2++;
                obj2.add(A00(r11, obj4));
            }
        }
        return obj2;
    }

    public static final Object A01(Object obj, String str, Map map) {
        if (str != null) {
            if (str.equals("$")) {
                return obj;
            }
            if (!str.equals("#")) {
                String str2 = str;
                if (!str.startsWith("$.")) {
                    throw new AssertionError("FcsStateIoUtils/createPath/path should start with '$.'");
                } else if (!AnonymousClass03B.A0G(str, "[")) {
                    LinkedHashMap linkedHashMap = new LinkedHashMap(map);
                    if (AnonymousClass03C.A0L(str, "$.")) {
                        str2 = str.substring("$.".length());
                        C16700pc.A0B(str2);
                    }
                    Map map2 = linkedHashMap;
                    for (Object obj2 : AnonymousClass03B.A09(str2, new String[]{"."}, 0)) {
                        if (!map2.containsKey(obj2) || !(map2.get(obj2) instanceof Map)) {
                            map2.put(obj2, new LinkedHashMap());
                        }
                        Object obj3 = map2.get(obj2);
                        if (obj3 == null) {
                            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.MutableMap<kotlin.String, kotlin.Any?>");
                        } else if (obj3 instanceof AbstractC16910px) {
                            C94564c6.A01(obj3, "kotlin.collections.MutableMap");
                            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                        } else {
                            try {
                                map2 = (Map) obj3;
                            } catch (ClassCastException e) {
                                C16700pc.A0L(C94564c6.class.getName(), e);
                                throw e;
                            }
                        }
                    }
                    C89104Ir r1 = new C89104Ir();
                    C95094d8.A03(linkedHashMap, "json object can not be null");
                    C93934az r6 = new C93934az(r1.A00, linkedHashMap);
                    C94734cS A00 = C93934az.A00(str, new AnonymousClass5T6[0]);
                    Object obj4 = r6.A01;
                    C92364Vp r7 = r6.A00;
                    EnumC87084Ad r8 = EnumC87084Ad.AS_PATH_LIST;
                    EnumSet noneOf = EnumSet.noneOf(EnumC87084Ad.class);
                    noneOf.addAll(r7.A03);
                    noneOf.addAll(Arrays.asList(r8));
                    C92354Vo r12 = new C92354Vo();
                    r12.A00 = r7.A00;
                    r12.A01 = r7.A01;
                    r12.A03.addAll(noneOf);
                    Collection collection = r7.A02;
                    if (collection == null) {
                        collection = Collections.emptyList();
                    }
                    r12.A02 = collection;
                    C92364Vp A002 = r12.A00();
                    C95094d8.A03(obj4, "json can not be null");
                    C95094d8.A03(A002, "configuration can not be null");
                    C92574Wl r3 = A00.A00;
                    C94394bk r72 = new C94394bk(A002, r3, obj4, true);
                    try {
                        r3.A00.A02(new C82863wJ(obj4), r72, obj4, "");
                    } catch (AnonymousClass5H1 unused) {
                    }
                    if (r72.A01().isEmpty()) {
                        EnumC87084Ad r0 = EnumC87084Ad.SUPPRESS_EXCEPTIONS;
                        Set set = A002.A03;
                        if (set.contains(r0)) {
                            boolean contains = set.contains(r8);
                            boolean contains2 = set.contains(EnumC87084Ad.ALWAYS_RETURN_LIST);
                            if (contains || contains2 || !r3.A00.A05()) {
                                ((AnonymousClass52M) A002.A00).A01.A01();
                            }
                        } else {
                            throw new C82803wD();
                        }
                    } else {
                        List list = r72.A07;
                        Collections.sort(list);
                        for (AbstractC111885Be r10 : Collections.unmodifiableCollection(list)) {
                            if (r10 instanceof C82863wJ) {
                                throw new C82813wE();
                            } else if (r10 instanceof C82883wL) {
                                C82883wL r102 = (C82883wL) r10;
                                A002.A00.Acf(((AbstractC111885Be) r102).A00, r102.A00, obj);
                            } else if (r10 instanceof C82873wK) {
                                C82873wK r103 = (C82873wK) r10;
                                for (Object obj5 : r103.A00) {
                                    A002.A00.Acf(((AbstractC111885Be) r103).A00, obj5, obj);
                                }
                            } else if (r10 instanceof C82893wM) {
                                C82893wM r104 = (C82893wM) r10;
                                A002.A00.Abk(((AbstractC111885Be) r104).A00, r104.A00, obj);
                            }
                        }
                        if (A002.A03.contains(r8)) {
                            r72.A01();
                        }
                    }
                    return r6.A01("$", new AnonymousClass5T6[0]);
                } else {
                    throw new AssertionError("FcsStateIoUtils/createPath/currently lacking support for arrays, filters, or multiple targets");
                }
            } else if (obj != null) {
                if (obj instanceof Map) {
                    Map map3 = (Map) obj;
                    C16700pc.A0E(map3, 1);
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap(map);
                    linkedHashMap2.putAll(map3);
                    return linkedHashMap2;
                }
                throw new AssertionError("FcsStateIoUtils/flatMerge/can't merge with non-map result value");
            }
        }
        return map;
    }
}
