package X;

import com.whatsapp.mediacomposer.doodle.ColorPickerView;

/* renamed from: X.3YH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YH implements AnonymousClass5W4 {
    public final /* synthetic */ C48792Hu A00;
    public final /* synthetic */ AnonymousClass2Ab A01;
    public final /* synthetic */ C47342Ag A02;

    public AnonymousClass3YH(C48792Hu r1, AnonymousClass2Ab r2, C47342Ag r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r0.A0K() == false) goto L_0x0013;
     */
    @Override // X.AnonymousClass5W4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOJ(float r5, int r6) {
        /*
            r4 = this;
            X.2Hu r0 = r4.A00
            r0.A00 = r6
            X.2Ab r1 = r4.A01
            X.21t r0 = r1.A0O
            X.21u r0 = r0.A01
            if (r0 == 0) goto L_0x0013
            boolean r0 = r0.A0K()
            r3 = 1
            if (r0 != 0) goto L_0x0014
        L_0x0013:
            r3 = 0
        L_0x0014:
            X.2Ag r2 = r4.A02
            com.whatsapp.mediacomposer.doodle.ColorPickerComponent r0 = r1.A0E
            com.whatsapp.mediacomposer.doodle.ColorPickerView r0 = r0.A05
            float r1 = r0.A00
            boolean r0 = r0.A0B
            r2.A08(r1, r6, r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3YH.AOJ(float, int):void");
    }

    @Override // X.AnonymousClass5W4
    public void AY2() {
        C48792Hu r5 = this.A00;
        AnonymousClass2Ab r2 = this.A01;
        ColorPickerView colorPickerView = r2.A0E.A05;
        r5.A00 = colorPickerView.A02;
        r2.A04();
        this.A02.A08(colorPickerView.A00, r5.A00, colorPickerView.A0B, false);
    }
}
