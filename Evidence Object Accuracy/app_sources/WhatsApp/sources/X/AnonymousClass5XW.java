package X;

/* renamed from: X.5XW  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XW {
    boolean AIK();

    void AZq(byte[] bArr);

    long AaC();

    void AcZ(long j);

    void close();

    long position();

    byte readByte();

    int readInt();

    long readLong();

    short readShort();
}
