package X;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* renamed from: X.0et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ExecutorC10600et implements Executor {
    public Runnable A00;
    public final ArrayDeque A01 = new ArrayDeque();
    public final Executor A02;

    public ExecutorC10600et(Executor executor) {
        this.A02 = executor;
    }

    public synchronized void A00() {
        Runnable runnable = (Runnable) this.A01.poll();
        this.A00 = runnable;
        if (runnable != null) {
            this.A02.execute(runnable);
        }
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        this.A01.offer(new RunnableC09600dD(this, runnable));
        if (this.A00 == null) {
            A00();
        }
    }
}
