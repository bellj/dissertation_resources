package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2nW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57722nW extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57722nW A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01 = 0;
    public Object A02;
    public String A03 = "";
    public String A04 = "";
    public boolean A05;

    static {
        C57722nW r0 = new C57722nW();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009f, code lost:
        if (r15.A01 == 4) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a4, code lost:
        if (r15.A01 == 3) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a9, code lost:
        if (r15.A01 == 7) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ab, code lost:
        r10 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ac, code lost:
        r0 = r9.Afv(r15.A02, r8.A02, r10);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r16, java.lang.Object r17, java.lang.Object r18) {
        /*
        // Method dump skipped, instructions count: 480
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57722nW.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public AnonymousClass39p A0b() {
        int i = this.A01;
        if (i == 0) {
            return AnonymousClass39p.A04;
        }
        if (i == 3) {
            return AnonymousClass39p.A01;
        }
        if (i == 4) {
            return AnonymousClass39p.A02;
        }
        if (i == 6) {
            return AnonymousClass39p.A03;
        }
        if (i != 7) {
            return null;
        }
        return AnonymousClass39p.A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int A0A;
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A04, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A03, i2);
        }
        if (this.A01 == 3) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 3, i2);
        }
        if (this.A01 == 4) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 4, i2);
        }
        if ((this.A00 & 64) == 64) {
            i2 += CodedOutputStream.A00(5);
        }
        int i3 = this.A01;
        if (i3 == 6) {
            A0A = CodedOutputStream.A09((AbstractC27881Jp) this.A02, 6);
        } else {
            if (i3 == 7) {
                A0A = CodedOutputStream.A0A((AnonymousClass1G0) this.A02, 7);
            }
            return AbstractC27091Fz.A07(this, i2);
        }
        i2 += A0A;
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A04);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A03);
        }
        if (this.A01 == 3) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 3);
        }
        if (this.A01 == 4) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 4);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0J(5, this.A05);
        }
        if (this.A01 == 6) {
            codedOutputStream.A0K((AbstractC27881Jp) this.A02, 6);
        }
        if (this.A01 == 7) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 7);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
