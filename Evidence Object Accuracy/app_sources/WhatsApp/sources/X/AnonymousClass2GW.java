package X;

/* renamed from: X.2GW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GW implements AbstractC009404s {
    public final /* synthetic */ ActivityC13810kN A00;

    public AnonymousClass2GW(ActivityC13810kN r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(AnonymousClass2GX.class)) {
            return new AnonymousClass2GX();
        }
        throw new IllegalArgumentException("Invalid UIModeViewModel for DialogActivity");
    }
}
