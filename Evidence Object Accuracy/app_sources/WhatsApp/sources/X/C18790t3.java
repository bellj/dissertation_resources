package X;

import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.Statistics$Data;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0t3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18790t3 {
    public AnonymousClass1N1 A00;
    public final C18640sm A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C14850m9 A04;
    public final C21780xy A05;
    public final AnonymousClass16B A06;

    public C18790t3(C18640sm r1, C14830m7 r2, C16590pI r3, C14850m9 r4, C21780xy r5, AnonymousClass16B r6) {
        this.A03 = r3;
        this.A02 = r2;
        this.A04 = r4;
        this.A06 = r6;
        this.A01 = r1;
        this.A05 = r5;
    }

    public Statistics$Data A00() {
        Statistics$Data statistics$Data;
        AnonymousClass1N1 r1 = this.A00;
        boolean z = false;
        if (r1 != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        try {
            r1.A03.await();
        } catch (InterruptedException e) {
            Log.e("statistics/waitForStatsInit exception waiting", e);
        }
        AnonymousClass1N1 r2 = this.A00;
        synchronized (r2) {
            try {
                statistics$Data = new Statistics$Data(new JSONObject(r2.A00.A00()));
            } catch (JSONException e2) {
                throw new RuntimeException(e2);
            }
        }
        return statistics$Data;
    }

    public void A01() {
        Log.i("statistics/init");
        boolean z = false;
        if (this.A00 == null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        HandlerThread handlerThread = new HandlerThread("stat-save", 10);
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        AnonymousClass1N1 r1 = new AnonymousClass1N1(looper, this, this.A01);
        this.A00 = r1;
        r1.sendEmptyMessage(0);
        AnonymousClass16B r3 = this.A06;
        r3.A00 = new AnonymousClass1N0(looper, r3.A01, r3.A03);
    }

    public final void A02() {
        this.A00.removeMessages(1);
        this.A00.sendEmptyMessageDelayed(1, 1000);
    }

    public void A03(long j, int i) {
        AnonymousClass1N1 r4 = this.A00;
        boolean z = false;
        if (r4 != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        if (j >= 0) {
            Message obtain = Message.obtain(r4, 5, i, 0);
            obtain.getData().putLong("bytes", j);
            obtain.sendToTarget();
            A02();
        }
    }

    public void A04(long j, int i) {
        AnonymousClass1N1 r4 = this.A00;
        boolean z = false;
        if (r4 != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        if (j >= 0) {
            Message obtain = Message.obtain(r4, 4, i, 0);
            obtain.getData().putLong("bytes", j);
            obtain.sendToTarget();
            A02();
        }
    }

    public void A05(long j, int i, boolean z) {
        AnonymousClass1N1 r1 = this.A00;
        boolean z2 = false;
        if (r1 != null) {
            z2 = true;
        }
        AnonymousClass009.A0F(z2);
        Message obtain = Message.obtain(r1, 7);
        Bundle data = obtain.getData();
        data.putInt("messageType", i);
        data.putLong("timestamp", j);
        data.putBoolean("isPayment", z);
        obtain.sendToTarget();
        A02();
    }

    public void A06(boolean z) {
        AnonymousClass1N1 r2 = this.A00;
        boolean z2 = false;
        if (r2 != null) {
            z2 = true;
        }
        AnonymousClass009.A0F(z2);
        Message.obtain(r2, 8, z ? 1 : 0, 0).sendToTarget();
        A02();
    }
}
