package X;

/* renamed from: X.0vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20580vz {
    public final AbstractC15710nm A00;
    public final C14830m7 A01;
    public final C16510p9 A02;
    public final C20290vW A03;
    public final C20850wQ A04;
    public final C16490p7 A05;
    public final C242214r A06;

    public C20580vz(AbstractC15710nm r1, C14830m7 r2, C16510p9 r3, C20290vW r4, C20850wQ r5, C16490p7 r6, C242214r r7) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (X.C15380n4.A0N(r2) != false) goto L_0x0013;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C39921ql A00(X.AbstractC15340mz r13) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20580vz.A00(X.0mz):X.1ql");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r20v0, resolved type: X.1IS */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (X.C15380n4.A0N(r4) != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C39921ql A01(X.AnonymousClass1IS r20) {
        /*
            r19 = this;
            long r10 = android.os.SystemClock.uptimeMillis()
            r5 = r20
            X.0lm r4 = r5.A00
            boolean r0 = X.C15380n4.A0J(r4)
            r8 = 0
            r3 = 1
            if (r0 != 0) goto L_0x0017
            boolean r0 = X.C15380n4.A0N(r4)
            r2 = 0
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r2 = 1
        L_0x0018:
            java.lang.String r1 = "jid="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            X.AnonymousClass009.A0B(r0, r2)
            X.AnonymousClass009.A05(r4)
            java.lang.String r0 = r4.getRawString()
            X.1ql r4 = new X.1ql
            r4.<init>()
            r2 = 2
            java.lang.String[] r7 = new java.lang.String[r2]
            r7[r8] = r0
            java.lang.String r0 = r5.A01
            r7[r3] = r0
            r5 = r19
            X.0p7 r0 = r5.A05     // Catch: SQLiteDatabaseCorruptException -> 0x00a7
            X.0on r6 = r0.get()     // Catch: SQLiteDatabaseCorruptException -> 0x00a7
            X.0op r1 = r6.A03     // Catch: all -> 0x00a2
            java.lang.String r0 = "SELECT remote_resource, receipt_device_timestamp, read_device_timestamp, played_device_timestamp FROM receipts WHERE key_remote_jid = ? AND key_id = ?"
            android.database.Cursor r7 = r1.A09(r0, r7)     // Catch: all -> 0x00a2
            if (r7 == 0) goto L_0x0086
        L_0x0050:
            boolean r0 = r7.moveToNext()     // Catch: all -> 0x009b
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = r7.getString(r8)     // Catch: all -> 0x009b
            if (r0 != 0) goto L_0x005f
            X.1Uv r1 = X.C29831Uv.A00     // Catch: all -> 0x009b
            goto L_0x0063
        L_0x005f:
            com.whatsapp.jid.UserJid r1 = com.whatsapp.jid.UserJid.getNullable(r0)     // Catch: all -> 0x009b
        L_0x0063:
            r9 = 0
            if (r1 == 0) goto L_0x0067
            r9 = 1
        L_0x0067:
            java.lang.String r0 = "receiptsmsgstore/invalid participant jid when getting receipts for group or status message"
            X.AnonymousClass009.A0A(r0, r9)     // Catch: all -> 0x009b
            if (r1 == 0) goto L_0x0050
            long r13 = r7.getLong(r3)     // Catch: all -> 0x009b
            long r15 = r7.getLong(r2)     // Catch: all -> 0x009b
            r0 = 3
            long r17 = r7.getLong(r0)     // Catch: all -> 0x009b
            X.1qm r12 = new X.1qm     // Catch: all -> 0x009b
            r12.<init>(r13, r15, r17)     // Catch: all -> 0x009b
            java.util.concurrent.ConcurrentHashMap r0 = r4.A00     // Catch: all -> 0x009b
            r0.put(r1, r12)     // Catch: all -> 0x009b
            goto L_0x0050
        L_0x0086:
            X.0vW r3 = r5.A03     // Catch: all -> 0x009b
            java.lang.String r2 = "ReceiptsMessageStore/getMessageReceiptsForGroupOrStatusMessage"
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch: all -> 0x009b
            long r0 = r0 - r10
            r3.A00(r2, r0)     // Catch: all -> 0x009b
            if (r7 == 0) goto L_0x0097
            r7.close()     // Catch: all -> 0x00a2
        L_0x0097:
            r6.close()     // Catch: SQLiteDatabaseCorruptException -> 0x00a7
            return r4
        L_0x009b:
            r0 = move-exception
            if (r7 == 0) goto L_0x00a1
            r7.close()     // Catch: all -> 0x00a1
        L_0x00a1:
            throw r0     // Catch: all -> 0x00a2
        L_0x00a2:
            r0 = move-exception
            r6.close()     // Catch: all -> 0x00a6
        L_0x00a6:
            throw r0     // Catch: SQLiteDatabaseCorruptException -> 0x00a7
        L_0x00a7:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            X.0wQ r0 = r5.A04
            r0.A02()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20580vz.A01(X.1IS):X.1ql");
    }
}
