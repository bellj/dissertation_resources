package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape2S0101000_I1;
import java.util.List;

/* renamed from: X.2bX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52702bX extends ArrayAdapter {
    public int A00 = 0;
    public final List A01;

    public C52702bX(Context context, List list) {
        super(context, (int) R.layout.item_phone_number_selection, list);
        this.A01 = list;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        boolean z = false;
        if (view == null) {
            view = C12960it.A0E(viewGroup).inflate(R.layout.item_phone_number_selection, viewGroup, false);
        }
        AnonymousClass4Q4 r6 = new AnonymousClass4Q4();
        r6.A02 = C12960it.A0I(view, R.id.title);
        r6.A01 = C12960it.A0I(view, R.id.subtitle);
        r6.A00 = (RadioButton) AnonymousClass028.A0D(view, R.id.phone_number_selection_radio_button);
        ViewOnClickCListenerShape2S0101000_I1 viewOnClickCListenerShape2S0101000_I1 = new ViewOnClickCListenerShape2S0101000_I1(this, i, 3);
        C65983Lv r3 = (C65983Lv) this.A01.get(i);
        String str = r3.A01;
        boolean isEmpty = TextUtils.isEmpty(str);
        TextView textView = r6.A02;
        if (isEmpty) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
            r6.A02.setText(str);
        }
        r6.A01.setText(r3.A00);
        RadioButton radioButton = r6.A00;
        if (i == this.A00) {
            z = true;
        }
        radioButton.setChecked(z);
        r6.A00.setOnClickListener(viewOnClickCListenerShape2S0101000_I1);
        view.setOnClickListener(viewOnClickCListenerShape2S0101000_I1);
        return view;
    }
}
