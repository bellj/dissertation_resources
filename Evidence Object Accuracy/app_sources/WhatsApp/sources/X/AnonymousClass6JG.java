package X;

import java.util.Collections;

/* renamed from: X.6JG  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6JG implements Runnable {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ C130095yn A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ AnonymousClass6JG(AnonymousClass016 r1, C130095yn r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C130095yn r6 = this.A01;
        String str = this.A02;
        AnonymousClass016 r5 = this.A00;
        C1310460z A0B = C117315Zl.A0B("transaction", C12980iv.A0x(Collections.singleton(AnonymousClass61S.A00("id", str))));
        C1310460z A0Q = C117305Zk.A0Q(Collections.singleton(AnonymousClass61S.A00("action", "novi-get-transaction")));
        A0Q.A02.add(A0B);
        r6.A07.A0B(C117305Zk.A09(r5, r6, 14), A0Q, "get", 3);
    }
}
