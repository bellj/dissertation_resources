package X;

/* renamed from: X.28R  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28R extends AbstractC16110oT {
    public String A00;
    public String A01;
    public String A02;

    public AnonymousClass28R() {
        super(2812, new AnonymousClass00E(1, 1, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamSamsungBatteryWarning {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "data", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "extras", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "extrasJson", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
