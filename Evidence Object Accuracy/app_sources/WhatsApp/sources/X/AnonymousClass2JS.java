package X;

import android.os.Handler;
import android.os.SystemClock;
import java.lang.ref.WeakReference;

/* renamed from: X.2JS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JS extends AbstractC16350or {
    public Handler A00;
    public Runnable A01;
    public final long A02 = SystemClock.uptimeMillis();
    public final C15610nY A03;
    public final AbstractC15590nW A04;
    public final WeakReference A05;
    public final boolean A06;

    public AnonymousClass2JS(C15610nY r3, AnonymousClass2JT r4, AbstractC15590nW r5, boolean z) {
        this.A03 = r3;
        this.A05 = new WeakReference(r4);
        this.A04 = r5;
        this.A06 = z;
    }
}
