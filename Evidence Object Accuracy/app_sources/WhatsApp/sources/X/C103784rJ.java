package X;

import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.4rJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103784rJ implements AnonymousClass07L {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C103784rJ(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A00 = groupAdminPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        this.A00.A2h(str);
        return false;
    }
}
