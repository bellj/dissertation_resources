package X;

import com.google.android.exoplayer2.Timeline;
import java.util.List;

/* renamed from: X.5XZ  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XZ {
    void AQ4(boolean z);

    void ARX(boolean z);

    void ARY(boolean z);

    void ASS(AnonymousClass4XL v, int i);

    void ATm(boolean z, int i);

    void ATo(C94344be v);

    void ATq(int i);

    void ATr(int i);

    void ATs(AnonymousClass3A1 v);

    @Deprecated
    void ATt(boolean z, int i);

    void ATx(int i);

    @Deprecated
    void AVk();

    void AWU(List list);

    void AXV(Timeline timeline, int i);

    @Deprecated
    void AXW(Timeline timeline, Object obj, int i);

    void AXl(C100564m7 v, C92524Wg v2);
}
