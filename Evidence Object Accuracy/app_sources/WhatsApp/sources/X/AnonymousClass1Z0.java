package X;

import android.content.Context;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.1Z0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z0 {
    public static final String A0A;
    public static final Pattern A0B = Pattern.compile("[#0,.-]+");
    public String A00;
    public String A01;
    public final AnonymousClass1Z2 A02;
    public final C94014b7 A03;
    public final C94014b7 A04;
    public final C94014b7 A05;
    public final C94014b7 A06;
    public final AnonymousClass1Z1 A07;
    public final String A08;
    public final String A09;

    static {
        StringBuilder sb = new StringBuilder("¤");
        sb.append(AnonymousClass01V.A06);
        sb.append("#,##0.00");
        A0A = sb.toString();
    }

    public AnonymousClass1Z0(AnonymousClass1Z2 r3, AnonymousClass1Z1 r4, AnonymousClass018 r5) {
        this.A02 = r3;
        this.A07 = r4;
        if (r3.A02) {
            this.A05 = new C94014b7(r5.A08(9));
            this.A06 = new C94014b7(r5.A08(11));
            this.A09 = r5.A08(10);
            this.A03 = new C94014b7(r5.A08(6));
            this.A04 = new C94014b7(r5.A08(8));
            this.A08 = r5.A08(7);
            return;
        }
        C94014b7 r0 = C94014b7.A02;
        this.A04 = r0;
        this.A03 = r0;
        this.A06 = r0;
        this.A05 = r0;
        this.A08 = "";
        this.A09 = "";
    }

    public AnonymousClass1Z0(Context context, AnonymousClass1Z2 r4, AnonymousClass1Z1 r5, Locale locale) {
        this.A02 = r4;
        this.A07 = r5;
        this.A05 = new C94014b7(AnonymousClass1MZ.A01(context, locale, 9));
        this.A06 = new C94014b7(AnonymousClass1MZ.A01(context, locale, 11));
        this.A09 = AnonymousClass1MZ.A01(context, locale, 10);
        this.A03 = new C94014b7(AnonymousClass1MZ.A01(context, locale, 6));
        this.A04 = new C94014b7(AnonymousClass1MZ.A01(context, locale, 8));
        this.A08 = AnonymousClass1MZ.A01(context, locale, 7);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a A[ORIG_RETURN, RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r4) {
        /*
            int r0 = r4.hashCode()
            r3 = 3
            r2 = 2
            r1 = 1
            switch(r0) {
                case 1632: goto L_0x0029;
                case 1776: goto L_0x001f;
                case 2406: goto L_0x0016;
                case 2534: goto L_0x000c;
                default: goto L_0x000a;
            }
        L_0x000a:
            r1 = 5
        L_0x000b:
            return r1
        L_0x000c:
            java.lang.String r0 = "০"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x000a
            return r3
        L_0x0016:
            java.lang.String r0 = "०"
            boolean r0 = r4.equals(r0)
            r1 = 4
            goto L_0x0030
        L_0x001f:
            java.lang.String r0 = "۰"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x000a
            return r2
        L_0x0029:
            java.lang.String r0 = "٠"
            boolean r0 = r4.equals(r0)
        L_0x0030:
            if (r0 != 0) goto L_0x000b
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Z0.A00(java.lang.String):int");
    }

    public final String A01(String str, boolean z) {
        C91304Rf r2;
        String replaceFirst;
        String str2;
        AnonymousClass1Z2 r0 = this.A02;
        if (z) {
            r2 = r0.A00;
        } else {
            r2 = r0.A01;
        }
        if (r2.A03) {
            replaceFirst = r2.A01;
            str2 = A0B.matcher(r2.A02).replaceFirst(str);
        } else {
            replaceFirst = A0B.matcher(r2.A01).replaceFirst(str);
            str2 = r2.A02;
        }
        if (!replaceFirst.isEmpty() && this.A06.A00(replaceFirst.codePointBefore(replaceFirst.length())) && this.A05.A00(this.A01.codePointAt(0))) {
            StringBuilder sb = new StringBuilder();
            sb.append(replaceFirst);
            sb.append(this.A09);
            replaceFirst = sb.toString();
        }
        if (!str2.isEmpty() && this.A04.A00(str2.codePointAt(0))) {
            C94014b7 r22 = this.A03;
            String str3 = this.A01;
            if (r22.A00(str3.codePointBefore(str3.length()))) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.A08);
                sb2.append(str2);
                str2 = sb2.toString();
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(replaceFirst);
        sb3.append(this.A01);
        sb3.append(str2);
        return sb3.toString();
    }
}
