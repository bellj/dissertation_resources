package X;

/* renamed from: X.5y2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129625y2 {
    public final /* synthetic */ C91064Qh A00;
    public final /* synthetic */ AnonymousClass5WN A01;
    public final /* synthetic */ C124395pK A02;
    public final /* synthetic */ boolean A03;

    public C129625y2(C91064Qh r1, AnonymousClass5WN r2, C124395pK r3, boolean z) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = z;
        this.A01 = r2;
    }

    public void A00(Exception exc) {
        C91064Qh r3 = this.A00;
        r3.A02 = exc;
        r3.A00 = 4;
        this.A02.A01(r3, this.A01, exc.getLocalizedMessage());
    }

    public void A01(String str) {
        this.A02.A00(this.A00, this.A01, str, this.A03);
    }
}
