package X;

/* renamed from: X.0QO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QO {
    public long A00 = 0;
    public AnonymousClass0QO A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r7 >= 64) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(int r7) {
        /*
            r6 = this;
            X.0QO r1 = r6.A01
            r0 = 64
            r4 = 1
            if (r1 != 0) goto L_0x0011
            long r2 = r6.A00
            if (r7 < r0) goto L_0x0015
        L_0x000c:
            int r2 = java.lang.Long.bitCount(r2)
            return r2
        L_0x0011:
            if (r7 >= r0) goto L_0x001a
            long r2 = r6.A00
        L_0x0015:
            long r0 = r4 << r7
            long r0 = r0 - r4
            long r2 = r2 & r0
            goto L_0x000c
        L_0x001a:
            int r7 = r7 - r0
            int r2 = r1.A00(r7)
            long r0 = r6.A00
            int r0 = java.lang.Long.bitCount(r0)
            int r2 = r2 + r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QO.A00(int):int");
    }

    public void A01() {
        this.A00 = 0;
        AnonymousClass0QO r0 = this.A01;
        if (r0 != null) {
            r0.A01();
        }
    }

    public final void A02() {
        if (this.A01 == null) {
            this.A01 = new AnonymousClass0QO();
        }
    }

    public void A03(int i) {
        if (i >= 64) {
            AnonymousClass0QO r0 = this.A01;
            if (r0 != null) {
                r0.A03(i - 64);
                return;
            }
            return;
        }
        this.A00 &= (1 << i) ^ -1;
    }

    public void A04(int i) {
        if (i >= 64) {
            A02();
            this.A01.A04(i - 64);
            return;
        }
        this.A00 |= 1 << i;
    }

    public void A05(int i, boolean z) {
        if (i >= 64) {
            A02();
            this.A01.A05(i - 64, z);
            return;
        }
        long j = this.A00;
        boolean z2 = false;
        if ((Long.MIN_VALUE & j) != 0) {
            z2 = true;
        }
        long j2 = (1 << i) - 1;
        this.A00 = ((j & (j2 ^ -1)) << 1) | (j & j2);
        if (z) {
            A04(i);
        } else {
            A03(i);
        }
        if (z2 || this.A01 != null) {
            A02();
            this.A01.A05(0, z2);
        }
    }

    public boolean A06(int i) {
        if (i < 64) {
            return (this.A00 & (1 << i)) != 0;
        }
        A02();
        return this.A01.A06(i - 64);
    }

    public boolean A07(int i) {
        if (i >= 64) {
            A02();
            return this.A01.A07(i - 64);
        }
        long j = 1 << i;
        long j2 = this.A00;
        boolean z = false;
        if ((j2 & j) != 0) {
            z = true;
        }
        long j3 = j2 & (j ^ -1);
        this.A00 = j3;
        long j4 = j - 1;
        this.A00 = (j3 & j4) | Long.rotateRight((j4 ^ -1) & j3, 1);
        AnonymousClass0QO r0 = this.A01;
        if (r0 != null) {
            if (r0.A06(0)) {
                A04(63);
            }
            this.A01.A07(0);
        }
        return z;
    }

    public String toString() {
        AnonymousClass0QO r0 = this.A01;
        if (r0 == null) {
            return Long.toBinaryString(this.A00);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(r0.toString());
        sb.append("xx");
        sb.append(Long.toBinaryString(this.A00));
        return sb.toString();
    }
}
