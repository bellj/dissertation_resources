package X;

/* renamed from: X.4b2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93964b2 {
    public final int A00;
    public final String A01;

    public C93964b2(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    public C93964b2(AbstractC69213Yj r2, int i) {
        this.A00 = i;
        this.A01 = r2.getId();
    }

    public boolean A00(int i) {
        boolean z = this instanceof AnonymousClass47G;
        int i2 = this.A00;
        if (!z) {
            return C12960it.A1V(i, i2);
        }
        return i >= i2 && i < i2 + 4;
    }
}
