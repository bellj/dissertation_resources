package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.3Vl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68453Vl implements AnonymousClass2J4 {
    public final /* synthetic */ C44341yl A00;

    public C68453Vl(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J4
    public C59412ug A84(AnonymousClass2K1 r14, AnonymousClass1B4 r15, Jid jid) {
        AnonymousClass01J r1 = this.A00.A03;
        C14900mE A0R = C12970iu.A0R(r1);
        r1.AMg.get();
        AbstractC15710nm A0Q = C12970iu.A0Q(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        AnonymousClass018 A0R2 = C12960it.A0R(r1);
        C14850m9 A0S = C12960it.A0S(r1);
        C17170qN A0a = C12990iw.A0a(r1);
        return new C59412ug(A0Q, A0R, C12990iw.A0W(r1), r14, r15, (C16340oq) r1.A5z.get(), A0a, A0R2, A0S, jid, A0T);
    }
}
