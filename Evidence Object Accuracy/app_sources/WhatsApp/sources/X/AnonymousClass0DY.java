package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.0DY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DY extends AnonymousClass0Q6 {
    public AnonymousClass0DY() {
        super(Boolean.class, R.id.tag_screen_reader_focusable, 0, 28);
    }

    @Override // X.AnonymousClass0Q6
    public Object A01(View view) {
        return Boolean.valueOf(AnonymousClass0US.A06(view));
    }

    @Override // X.AnonymousClass0Q6
    public void A03(View view, Object obj) {
        AnonymousClass0US.A04(view, ((Boolean) obj).booleanValue());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r5.booleanValue() == false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        if (r6.booleanValue() == false) goto L_0x0018;
     */
    @Override // X.AnonymousClass0Q6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ boolean A04(java.lang.Object r5, java.lang.Object r6) {
        /*
            r4 = this;
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            r3 = 1
            if (r5 == 0) goto L_0x000e
            boolean r0 = r5.booleanValue()
            r2 = 1
            if (r0 != 0) goto L_0x000f
        L_0x000e:
            r2 = 0
        L_0x000f:
            if (r6 == 0) goto L_0x0018
            boolean r1 = r6.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            if (r2 == r0) goto L_0x001c
            r3 = 0
        L_0x001c:
            r0 = r3 ^ 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DY.A04(java.lang.Object, java.lang.Object):boolean");
    }
}
