package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73893gy extends Animation {
    public final int A00;
    public final int A01;
    public final View A02;

    public C73893gy(View view, int i) {
        this.A00 = i;
        this.A01 = view.getTop();
        this.A02 = view;
        setDuration(300);
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = this.A01;
        int i2 = (int) (((float) i) + (((float) (this.A00 - i)) * f));
        View view = this.A02;
        AnonymousClass028.A0Y(view, i2 - view.getTop());
    }
}
