package X;

/* renamed from: X.4BF  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BF {
    A03(0),
    A01(1),
    A02(2),
    A04(3);
    
    public final int value;

    AnonymousClass4BF(int i) {
        this.value = i;
    }
}
