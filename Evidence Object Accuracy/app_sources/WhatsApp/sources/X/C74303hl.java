package X;

import android.view.View;
import com.google.android.material.internal.NavigationMenuItemView;

/* renamed from: X.3hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74303hl extends AnonymousClass04v {
    public final /* synthetic */ NavigationMenuItemView A00;

    public C74303hl(NavigationMenuItemView navigationMenuItemView) {
        this.A00 = navigationMenuItemView;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        r4.A02.setCheckable(this.A00.A06);
    }
}
