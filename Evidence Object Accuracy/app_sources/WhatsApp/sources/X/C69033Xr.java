package X;

import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69033Xr implements AnonymousClass13P {
    public final /* synthetic */ GroupChatInfo A00;

    @Override // X.AnonymousClass13P
    public void AUg(C30751Yr r1) {
    }

    public C69033Xr(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AnonymousClass13P
    public void AUh(AbstractC14640lm r4, UserJid userJid) {
        GroupChatInfo groupChatInfo = this.A00;
        if (r4.equals(groupChatInfo.A1C)) {
            C14900mE.A01(((ActivityC13810kN) groupChatInfo).A05, groupChatInfo, 37);
        }
    }

    @Override // X.AnonymousClass13P
    public void AUi(AbstractC14640lm r4, UserJid userJid) {
        GroupChatInfo groupChatInfo = this.A00;
        if (r4.equals(groupChatInfo.A1C)) {
            C14900mE.A01(((ActivityC13810kN) groupChatInfo).A05, groupChatInfo, 38);
        }
    }
}
