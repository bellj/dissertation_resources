package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

/* renamed from: X.06z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C014806z {
    public static String A00(ComponentName componentName, Context context) {
        String string;
        PackageManager packageManager = context.getPackageManager();
        int i = Build.VERSION.SDK_INT;
        int i2 = 640;
        if (i >= 29) {
            i2 = 269222528;
        } else if (i >= 24) {
            i2 = 787072;
        }
        ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, i2);
        String str = activityInfo.parentActivityName;
        if (str == null) {
            Bundle bundle = activityInfo.metaData;
            str = null;
            if (!(bundle == null || (string = bundle.getString("android.support.PARENT_ACTIVITY")) == null)) {
                if (string.charAt(0) != '.') {
                    return string;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(context.getPackageName());
                sb.append(string);
                return sb.toString();
            }
        }
        return str;
    }
}
