package X;

import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Build;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0sm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18640sm extends AbstractC16220oe {
    public AnonymousClass1I1 A00;
    public final Handler A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;
    public final Object A05 = new Object();
    public final AtomicBoolean A06 = new AtomicBoolean(false);
    public volatile C51882Zm A07;
    public volatile C22050yP A08;
    public volatile C21940yE A09;

    public C18640sm(AnonymousClass01H r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6) {
        super(r6);
        this.A03 = r3;
        this.A02 = r4;
        this.A04 = r5;
        this.A01 = new Handler(Looper.getMainLooper());
    }

    public static int A01(Context context) {
        if (A03(context)) {
            return R.string.network_required_airplane_on;
        }
        return R.string.network_required;
    }

    public static boolean A02() {
        Throwable th;
        HttpURLConnection httpURLConnection;
        String str = AnonymousClass029.A0I;
        HttpURLConnection httpURLConnection2 = null;
        try {
            TrafficStats.setThreadStatsTag(3);
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        } catch (IOException unused) {
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setConnectTimeout(SearchActionVerificationClientService.NOTIFICATION_ID);
            httpURLConnection.setReadTimeout(SearchActionVerificationClientService.NOTIFICATION_ID);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.getInputStream();
        } catch (IOException unused2) {
            httpURLConnection2 = httpURLConnection;
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
            TrafficStats.clearThreadStatsTag();
            return false;
        } catch (Throwable th3) {
            th = th3;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            TrafficStats.clearThreadStatsTag();
            throw th;
        }
        if (httpURLConnection.getResponseCode() == 204) {
            httpURLConnection.disconnect();
            TrafficStats.clearThreadStatsTag();
            return false;
        }
        Log.i("captive portal");
        httpURLConnection.disconnect();
        TrafficStats.clearThreadStatsTag();
        return true;
    }

    public static boolean A03(Context context) {
        int i;
        int i2 = Build.VERSION.SDK_INT;
        ContentResolver contentResolver = context.getContentResolver();
        if (i2 < 17) {
            i = Settings.System.getInt(contentResolver, "airplane_mode_on", 0);
        } else {
            i = Settings.Global.getInt(contentResolver, "airplane_mode_on", 0);
        }
        if (i == 0) {
            return false;
        }
        return true;
    }

    public int A05(boolean z) {
        if (A0D()) {
            return A06().A00();
        }
        return ((C247716u) this.A02.get()).A00(z);
    }

    public final C51882Zm A06() {
        if (this.A07 == null) {
            synchronized (this) {
                if (this.A07 == null) {
                    this.A07 = new C51882Zm(this, (C15890o4) this.A04.get());
                }
            }
        }
        return this.A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0029, code lost:
        if (r2 != 2) goto L_0x002b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1I0 A07() {
        /*
        // Method dump skipped, instructions count: 306
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18640sm.A07():X.1I0");
    }

    public void A08() {
        AnonymousClass1I0 A07 = A07();
        this.A01.post(new RunnableBRunnable0Shape0S0300000_I0(this, AnonymousClass1I1.A00(A07, ((C14830m7) this.A03.get()).A00()), A07, 37));
    }

    public void A09(AnonymousClass1I0 r3, C22050yP r4, C21940yE r5) {
        this.A06.set(true);
        this.A08 = r4;
        this.A09 = r5;
        A06().A03(r3);
    }

    public void A0A(AnonymousClass1I1 r3) {
        synchronized (this.A05) {
            this.A00 = r3;
        }
        for (AbstractC20260vT r0 : A01()) {
            r0.AOa(r3);
        }
    }

    public boolean A0B() {
        if (A0D()) {
            return A06().A05();
        }
        C247716u r2 = (C247716u) this.A02.get();
        ConnectivityManager A0H = r2.A01.A0H();
        if (A0H == null) {
            return false;
        }
        try {
            NetworkInfo activeNetworkInfo = A0H.getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return false;
            }
            if (activeNetworkInfo.isConnected()) {
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                r2.A00.AaV("networkstatemanager/deadOS", null, false);
                return false;
            }
            throw e;
        }
    }

    public boolean A0C() {
        if (A0D()) {
            return A06().A04();
        }
        return ((C247716u) this.A02.get()).A02();
    }

    public boolean A0D() {
        return Build.VERSION.SDK_INT >= 29 && this.A06.get();
    }

    public boolean A0E(ConnectivityManager connectivityManager, TelephonyManager telephonyManager) {
        if (connectivityManager != null) {
            try {
                C51882Zm A06 = A06();
                connectivityManager.registerDefaultNetworkCallback(A06);
                A06.A02(connectivityManager, telephonyManager);
                return true;
            } catch (RuntimeException e) {
                Log.e("ConnectivityStateProvider/registerForNetworkCallbacks", e);
            }
        }
        return false;
    }
}
