package X;

/* renamed from: X.66G  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66G implements AnonymousClass6Lj {
    public final /* synthetic */ AnonymousClass66H A00;

    public AnonymousClass66G(AnonymousClass66H r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6Lj
    public void AXX() {
        AnonymousClass66H r2 = this.A00;
        r2.A08 = Boolean.FALSE;
        r2.A06 = new AnonymousClass6L0("Photo capture failed. Still capture timed out.");
    }
}
