package X;

import java.util.Iterator;

/* renamed from: X.5Bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C111895Bf implements Iterable {
    public final /* synthetic */ long A00 = Long.MAX_VALUE;
    public final /* synthetic */ AnonymousClass39u A01;
    public final /* synthetic */ C20920wX A02;
    public final /* synthetic */ CharSequence A03;
    public final /* synthetic */ String A04;

    public C111895Bf(AnonymousClass39u r3, C20920wX r4, CharSequence charSequence, String str) {
        this.A02 = r4;
        this.A03 = charSequence;
        this.A04 = str;
        this.A01 = r3;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new C71353cn(this.A01, this.A02, this.A03, this.A04, this.A00);
    }
}
