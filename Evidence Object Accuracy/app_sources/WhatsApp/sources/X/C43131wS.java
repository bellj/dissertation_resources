package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1wS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43131wS extends HashMap<AnonymousClass1IS, AbstractC16130oV> {
    public final /* synthetic */ C22370yy this$0;

    public /* synthetic */ C43131wS(C22370yy r1) {
        this.this$0 = r1;
    }

    /* renamed from: A00 */
    public AbstractC16130oV remove(Object obj) {
        C16150oX r0;
        C28921Pn A00;
        AbstractC16130oV r02 = (AbstractC16130oV) super.get(obj);
        if (!(r02 == null || (r0 = r02.A02) == null)) {
            C22370yy r4 = this.this$0;
            AnonymousClass009.A05(r0);
            AnonymousClass1CH r1 = r4.A0V;
            C28921Pn A002 = r1.A00(r0);
            if (A002 != null) {
                A002.A0t = false;
                HashMap hashMap = r4.A0q;
                synchronized (hashMap) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        r4.A08(null, (AbstractC16130oV) entry.getKey(), 1, ((Long) entry.getValue()).longValue(), true, true);
                    }
                    hashMap.clear();
                }
                synchronized (r4.A0W) {
                    Iterator it = r4.A04().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            r4.A01.open();
                            break;
                        }
                        C16150oX r03 = ((AbstractC16130oV) it.next()).A02;
                        if (!(r03 == null || (A00 = r1.A00(r03)) == null || !A00.A0t)) {
                            break;
                        }
                    }
                }
            }
        }
        return (AbstractC16130oV) super.remove(obj);
    }
}
