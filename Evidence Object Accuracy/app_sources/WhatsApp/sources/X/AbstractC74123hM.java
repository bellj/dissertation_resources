package X;

import android.content.Context;
import android.widget.FrameLayout;

/* renamed from: X.3hM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC74123hM extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC74123hM(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
