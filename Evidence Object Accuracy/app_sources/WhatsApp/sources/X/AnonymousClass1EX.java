package X;

/* renamed from: X.1EX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EX {
    public final C15570nT A00;
    public final C14850m9 A01;

    public AnonymousClass1EX(C15570nT r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C39941qn A00(X.AbstractC15340mz r10) {
        /*
        // Method dump skipped, instructions count: 642
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1EX.A00(X.0mz):X.1qn");
    }

    public AnonymousClass1V8 A01(AbstractC15340mz r15) {
        AnonymousClass1G7 r2 = (AnonymousClass1G7) AnonymousClass1G6.A0k.A0T();
        AnonymousClass1G9 r3 = (AnonymousClass1G9) AnonymousClass1G8.A05.A0T();
        AnonymousClass1IS r1 = r15.A0z;
        r3.A07(C15380n4.A03(r1.A00));
        r3.A08(r1.A02);
        r3.A05(r1.A01);
        r2.A07((AnonymousClass1G8) r3.A02());
        AnonymousClass1G3 A00 = C27081Fy.A00();
        C32411c7.A0S(r15, new C39971qq(this.A00, null, this.A01, A00, null, null, null, true, false, false));
        r2.A03();
        AnonymousClass1G6 r12 = (AnonymousClass1G6) r2.A00;
        r12.A0L = (C27081Fy) A00.A02();
        r12.A01 |= 2;
        A02(r2, r15);
        return new AnonymousClass1V8("message", r2.A02().A02(), (AnonymousClass1W9[]) null);
    }

    public void A02(AnonymousClass1G7 r4, AbstractC15340mz r5) {
        AnonymousClass1IR r0;
        AnonymousClass1IR r02 = r5.A0L;
        if (!(r02 == null || r02.A08 == null)) {
            C39941qn A00 = A00(r5);
            r4.A03();
            AnonymousClass1G6 r2 = (AnonymousClass1G6) r4.A00;
            r2.A0Q = A00;
            r2.A01 |= C25981Bo.A0F;
        }
        AbstractC15340mz A0E = r5.A0E();
        if (A0E != null && (r0 = A0E.A0L) != null && r0.A08 != null) {
            C39941qn A002 = A00(A0E);
            r4.A03();
            AnonymousClass1G6 r22 = (AnonymousClass1G6) r4.A00;
            r22.A0R = A002;
            r22.A01 |= 524288;
        }
    }
}
