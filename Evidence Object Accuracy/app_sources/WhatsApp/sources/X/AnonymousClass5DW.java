package X;

import java.util.concurrent.Callable;

/* renamed from: X.5DW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DW implements Callable {
    public final /* synthetic */ C56592lG A00;

    public AnonymousClass5DW(C56592lG r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ Object call() {
        return C56592lG.A00(this.A00);
    }
}
