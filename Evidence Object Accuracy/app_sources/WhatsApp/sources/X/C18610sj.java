package X;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.0sj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18610sj {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18790t3 A03;
    public final C14830m7 A04;
    public final C16590pI A05;
    public final C15650ng A06;
    public final C241414j A07;
    public final C17220qS A08;
    public final C21860y6 A09;
    public final C241314i A0A;
    public final C18650sn A0B;
    public final C18660so A0C;
    public final C18600si A0D;
    public final C17900ra A0E;
    public final C22710zW A0F;
    public final C17070qD A0G;
    public final C20360vd A0H;
    public final C30931Zj A0I = C30931Zj.A00("PaymentsActionManager", "network", "COMMON");
    public final AnonymousClass1FL A0J;
    public final AnonymousClass15O A0K;
    public final C22980zx A0L;
    public final C20320vZ A0M;
    public final C18800t4 A0N;

    public C18610sj(AbstractC15710nm r4, C14900mE r5, C15570nT r6, C18790t3 r7, C14830m7 r8, C16590pI r9, C15650ng r10, C241414j r11, C17220qS r12, C21860y6 r13, C241314i r14, C18650sn r15, C18660so r16, C18600si r17, C17900ra r18, C22710zW r19, C17070qD r20, C20360vd r21, AnonymousClass1FL r22, AnonymousClass15O r23, C22980zx r24, C20320vZ r25, C18800t4 r26) {
        this.A05 = r9;
        this.A04 = r8;
        this.A01 = r5;
        this.A00 = r4;
        this.A02 = r6;
        this.A03 = r7;
        this.A07 = r11;
        this.A08 = r12;
        this.A0M = r25;
        this.A0G = r20;
        this.A06 = r10;
        this.A0N = r26;
        this.A0K = r23;
        this.A0D = r17;
        this.A09 = r13;
        this.A0J = r22;
        this.A0L = r24;
        this.A0F = r19;
        this.A0E = r18;
        this.A0A = r14;
        this.A0H = r21;
        this.A0B = r15;
        this.A0C = r16;
    }

    public AnonymousClass20C A00(AbstractC30791Yv r6, C30821Yy r7) {
        int AC0 = A03(r6).AC0(((AbstractC30781Yu) r6).A04);
        long doubleValue = (long) ((int) (r7.A00.doubleValue() * ((double) AC0)));
        if (AC0 <= 0) {
            return new AnonymousClass20C(r6, doubleValue);
        }
        return new AnonymousClass20C(r6, AC0, doubleValue);
    }

    public final AnonymousClass20C A01(AbstractC30791Yv r6, C30821Yy r7) {
        int i;
        AbstractC16830pp A03 = A03(r6);
        if (A03 != null) {
            i = A03.AC0(((AbstractC30781Yu) r6).A04);
        } else {
            i = 1000;
        }
        long doubleValue = (long) ((int) (r7.A00.doubleValue() * ((double) i)));
        if (i <= 0) {
            return new AnonymousClass20C(r6, doubleValue);
        }
        return new AnonymousClass20C(r6, i, doubleValue);
    }

    public AnonymousClass1IR A02(AbstractC30791Yv r5, C30821Yy r6, AbstractC28901Pl r7, AbstractC30891Zf r8, C30921Zi r9, AbstractC15340mz r10, String str, String str2, boolean z) {
        if (!A0H(r5, r6, r7, r8, r9, r10, str, str2, z)) {
            return null;
        }
        this.A06.A0p(r10);
        C241314i r3 = this.A0A;
        String str3 = r10.A0z.A01;
        AnonymousClass1IR r1 = r10.A0L;
        synchronized (r3) {
            if (r1 != null) {
                r3.A03.put(str3, r1);
            }
        }
        this.A0H.A00(r9, r10);
        AnonymousClass1IR r0 = r10.A0L;
        AnonymousClass009.A05(r0);
        return r0;
    }

    public final AbstractC16830pp A03(AbstractC30791Yv r3) {
        return this.A0G.A01(this.A0E.A01().A03).AFX(((AbstractC30781Yu) r3).A04);
    }

    public AnonymousClass1V8 A04(AbstractC30791Yv r11, C30821Yy r12, String str) {
        int AC0 = A03(r11).AC0(((AbstractC30781Yu) r11).A04);
        return A05(r11, str, AC0, (long) ((int) (r12.A00.doubleValue() * ((double) AC0))));
    }

    public AnonymousClass1V8 A05(AbstractC30791Yv r4, String str, int i, long j) {
        AnonymousClass20C r0;
        AbstractC16830pp A03 = A03(r4);
        if (i <= 0) {
            r0 = new AnonymousClass20C(r4, j);
        } else {
            r0 = new AnonymousClass20C(r4, i, j);
        }
        return new AnonymousClass1V8(A03 != null ? A03.AEY(r0) : null, str, (AnonymousClass1W9[]) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b0, code lost:
        if (r1 != null) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AbstractC451720l r9, X.AnonymousClass1V8 r10, boolean r11) {
        /*
            r8 = this;
            java.lang.String r0 = "account"
            X.1V8 r1 = r10.A0E(r0)
            X.0zx r0 = r8.A0L
            java.util.ArrayList r1 = r0.A07(r1)
            r3 = r9
            if (r1 == 0) goto L_0x0092
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0092
            r2 = 0
            boolean r0 = X.C241414j.A04(r1)
            if (r0 == 0) goto L_0x008b
            X.0qD r0 = r8.A0G
            r0.A03()
            X.1nR r0 = r0.A00
            X.AnonymousClass009.A05(r0)
            X.AnonymousClass009.A05(r0)
            r0.A05(r9, r1)
        L_0x002c:
            int r0 = r1.size()
            if (r0 <= 0) goto L_0x00b4
            java.util.Iterator r2 = r1.iterator()
        L_0x0036:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00b4
            java.lang.Object r1 = r2.next()
            X.1Pl r1 = (X.AbstractC28901Pl) r1
            boolean r0 = r1 instanceof X.C30881Ze
            if (r0 == 0) goto L_0x0036
            int r1 = r1.A01
            r0 = 2
            if (r1 != r0) goto L_0x0036
            X.0si r5 = r8.A0D
            r2 = 1
        L_0x004e:
            android.content.SharedPreferences r0 = r5.A01()
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r0 = "payments_card_can_receive_payment"
            android.content.SharedPreferences$Editor r0 = r1.putBoolean(r0, r2)
            r0.apply()
            X.0m7 r0 = r5.A01
            long r3 = r0.A00()
            android.content.SharedPreferences r0 = r5.A01()
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r0 = "payments_methods_last_sync_time"
            android.content.SharedPreferences$Editor r0 = r1.putLong(r0, r3)
            r0.apply()
            X.1Zj r2 = r5.A02
            java.lang.String r1 = "updateMethodsLastSyncTimeMilli to: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.A06(r0)
        L_0x008a:
            return
        L_0x008b:
            if (r11 == 0) goto L_0x008a
            r0 = 2
            r8.A08(r2, r0)
            return
        L_0x0092:
            X.0qD r0 = r8.A0G
            r0.A03()
            X.1nR r4 = r0.A00
            X.AnonymousClass009.A05(r4)
            X.AnonymousClass009.A05(r4)
            X.0lR r7 = r4.A03
            X.14j r5 = r4.A01
            X.17b r6 = r4.A02
            X.20m r2 = new X.20m
            r2.<init>(r3, r4, r5, r6, r7)
            r0 = 0
            java.lang.Void[] r0 = new java.lang.Void[r0]
            r7.Aaz(r2, r0)
            if (r1 == 0) goto L_0x00b4
            goto L_0x002c
        L_0x00b4:
            X.0si r5 = r8.A0D
            r2 = 0
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18610sj.A06(X.20l, X.1V8, boolean):void");
    }

    public void A07(C30821Yy r2, C30921Zi r3, UserJid userJid, AbstractC15340mz r5) {
        if (A0I(r2, r3, userJid, r5)) {
            this.A06.A0p(r5);
        }
    }

    public void A08(AnonymousClass1FK r14, int i) {
        SharedPreferences sharedPreferences;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("version", i));
        arrayList.add(new AnonymousClass1W9("action", "get-methods"));
        C451220g r7 = new C451220g(new C451120f((AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0])));
        AnonymousClass1FL r3 = this.A0J;
        synchronized (r3) {
            sharedPreferences = r3.A00;
            if (sharedPreferences == null) {
                sharedPreferences = r3.A01.A01("com.whatsapp_payment_sync_preferences");
                r3.A00 = sharedPreferences;
            }
        }
        String string = sharedPreferences.getString(r7.A01.A00(), null);
        if (string != null) {
            arrayList.add(new AnonymousClass1W9("instance-id", string));
        }
        AnonymousClass1V8 r9 = new AnonymousClass1V8("account", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0]));
        AbstractC16870pt ACx = this.A0G.A02().ACx();
        if (ACx != null) {
            ACx.AeG();
        }
        A0F(new C451320h(this.A05.A00, this.A01, r14, this.A0B, this, r7, string), r9, "get", 0);
    }

    public void A09(AnonymousClass1FK r12, AnonymousClass1V8 r13) {
        A0F(new C450920d(this.A05.A00, this.A01, r12, this.A0B, this), r13, "set", C26061Bw.A0L);
    }

    public void A0A(AnonymousClass1FK r12, AnonymousClass1V8 r13) {
        A0F(new C451620k(this.A05.A00, this.A01, r12, this.A0B, this), r13, "set", C26061Bw.A0L);
    }

    public void A0B(AnonymousClass1FK r6, AnonymousClass1V8 r7, String str, String str2) {
        AnonymousClass1V8 r0;
        if (TextUtils.isEmpty(str2)) {
            byte[] A01 = AnonymousClass15O.A01(this.A02, this.A04, false);
            AnonymousClass009.A05(A01);
            str2 = C003501n.A03(A01);
        }
        AnonymousClass1W9[] r4 = {new AnonymousClass1W9("action", "remove-credential"), new AnonymousClass1W9("credential-id", str), new AnonymousClass1W9("version", "2"), new AnonymousClass1W9("nonce", str2)};
        if (r7 == null) {
            r0 = new AnonymousClass1V8("account", r4);
        } else {
            r0 = new AnonymousClass1V8(r7, "account", r4);
        }
        A09(r6, r0);
    }

    public void A0C(AbstractC451420i r13, AnonymousClass1V8 r14) {
        A0G(new C451520j(this.A05.A00, this.A01, this.A0B, r13, this), r14, "set", "w:pay", C26061Bw.A0L);
    }

    public void A0D(AbstractC15340mz r4) {
        C30931Zj r2;
        String str;
        if (!this.A0F.A03()) {
            r2 = this.A0I;
            StringBuilder sb = new StringBuilder("decline/cancelPaymentRequest is not enabled for country: ");
            sb.append(this.A0E.A01());
            str = sb.toString();
        } else if (r4.A0z.A00 == null) {
            r2 = this.A0I;
            str = "requestPayment found null or empty args jid";
        } else {
            this.A06.A0p(r4);
            return;
        }
        r2.A06(str);
    }

    public void A0E(AbstractC21730xt r8, AnonymousClass1V8 r9, String str) {
        A0G(r8, r9, str, "w:pay", C26061Bw.A0L);
    }

    public void A0F(AbstractC21730xt r8, AnonymousClass1V8 r9, String str, long j) {
        A0G(r8, r9, str, "w:pay", j);
    }

    public void A0G(AbstractC21730xt r12, AnonymousClass1V8 r13, String str, String str2, long j) {
        C17220qS r4 = this.A08;
        String A01 = r4.A01();
        r4.A09(r12, new AnonymousClass1V8(r13, "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("type", str), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", str2)}), A01, 204, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0179 A[Catch: Exception -> 0x030a, TryCatch #0 {Exception -> 0x030a, blocks: (B:24:0x009b, B:26:0x00a9, B:27:0x00ad, B:30:0x00d0, B:32:0x00fb, B:33:0x0110, B:34:0x0118, B:36:0x011e, B:40:0x012c, B:42:0x0134, B:44:0x0141, B:45:0x0167, B:47:0x0179, B:48:0x0184, B:50:0x018a, B:51:0x019f, B:53:0x01a5, B:55:0x01ab, B:57:0x01bb, B:58:0x01c0, B:59:0x01c3, B:61:0x01c9, B:62:0x01d0, B:64:0x01d8, B:65:0x01e0, B:68:0x01ec, B:70:0x01f2, B:72:0x01fa, B:74:0x020a, B:76:0x021d, B:77:0x0237, B:78:0x024b, B:81:0x0255, B:83:0x025b, B:85:0x0263, B:87:0x0273, B:89:0x029e, B:90:0x02ad, B:91:0x02b3, B:92:0x02c6), top: B:107:0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01c9 A[Catch: Exception -> 0x030a, TryCatch #0 {Exception -> 0x030a, blocks: (B:24:0x009b, B:26:0x00a9, B:27:0x00ad, B:30:0x00d0, B:32:0x00fb, B:33:0x0110, B:34:0x0118, B:36:0x011e, B:40:0x012c, B:42:0x0134, B:44:0x0141, B:45:0x0167, B:47:0x0179, B:48:0x0184, B:50:0x018a, B:51:0x019f, B:53:0x01a5, B:55:0x01ab, B:57:0x01bb, B:58:0x01c0, B:59:0x01c3, B:61:0x01c9, B:62:0x01d0, B:64:0x01d8, B:65:0x01e0, B:68:0x01ec, B:70:0x01f2, B:72:0x01fa, B:74:0x020a, B:76:0x021d, B:77:0x0237, B:78:0x024b, B:81:0x0255, B:83:0x025b, B:85:0x0263, B:87:0x0273, B:89:0x029e, B:90:0x02ad, B:91:0x02b3, B:92:0x02c6), top: B:107:0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x021d A[Catch: Exception -> 0x030a, TryCatch #0 {Exception -> 0x030a, blocks: (B:24:0x009b, B:26:0x00a9, B:27:0x00ad, B:30:0x00d0, B:32:0x00fb, B:33:0x0110, B:34:0x0118, B:36:0x011e, B:40:0x012c, B:42:0x0134, B:44:0x0141, B:45:0x0167, B:47:0x0179, B:48:0x0184, B:50:0x018a, B:51:0x019f, B:53:0x01a5, B:55:0x01ab, B:57:0x01bb, B:58:0x01c0, B:59:0x01c3, B:61:0x01c9, B:62:0x01d0, B:64:0x01d8, B:65:0x01e0, B:68:0x01ec, B:70:0x01f2, B:72:0x01fa, B:74:0x020a, B:76:0x021d, B:77:0x0237, B:78:0x024b, B:81:0x0255, B:83:0x025b, B:85:0x0263, B:87:0x0273, B:89:0x029e, B:90:0x02ad, B:91:0x02b3, B:92:0x02c6), top: B:107:0x009b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0H(X.AbstractC30791Yv r34, X.C30821Yy r35, X.AbstractC28901Pl r36, X.AbstractC30891Zf r37, X.C30921Zi r38, X.AbstractC15340mz r39, java.lang.String r40, java.lang.String r41, boolean r42) {
        /*
        // Method dump skipped, instructions count: 788
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18610sj.A0H(X.1Yv, X.1Yy, X.1Pl, X.1Zf, X.1Zi, X.0mz, java.lang.String, java.lang.String, boolean):boolean");
    }

    public boolean A0I(C30821Yy r22, C30921Zi r23, UserJid userJid, AbstractC15340mz r25) {
        C30931Zj r4;
        StringBuilder sb;
        if (!this.A0F.A03()) {
            r4 = this.A0I;
            sb = new StringBuilder("requestPayment is not enabled for country: ");
            sb.append(this.A0E.A01());
        } else {
            AbstractC14640lm r3 = r25.A0z.A00;
            if (r3 == null || ((C15380n4.A0J(r3) && userJid == null) || r22 == null)) {
                r4 = this.A0I;
                sb = new StringBuilder("requestPayment found null or empty args jid: ");
                sb.append(r3);
                sb.append(" receiver: ");
                sb.append(userJid);
            } else {
                C17900ra r5 = this.A0E;
                C17930rd A01 = r5.A01();
                AnonymousClass009.A05(A01);
                AbstractC30791Yv r2 = A01.A02;
                AbstractC16830pp A03 = A03(r2);
                C15570nT r32 = this.A02;
                r32.A08();
                C27621Ig r33 = r32.A01;
                AnonymousClass009.A05(r33);
                String str = ((AbstractC30781Yu) r5.A00()).A04;
                AbstractC30791Yv A00 = r5.A00();
                String str2 = this.A0K.A02(userJid, true).A01;
                String str3 = r5.A01().A03;
                AnonymousClass1IR A02 = C31001Zq.A02(A00, r22, userJid, (UserJid) r33.A0D, str, str2, str3, 10, 11, AnonymousClass20N.A00(str3), A03.AGi(), 0, -1);
                A02.A05(r23);
                AbstractC30891Zf AIk = A03.AIk();
                AIk.A01 = A01(r2, r22);
                A02.A0A = AIk;
                long A002 = this.A04.A00();
                r25.A0I = A002;
                r25.A0m = "UNSET";
                r25.A0L = A02;
                A02.A05 = A002;
                A02.A02 = 12;
                A02.A0K = A02.A0K;
                AbstractC30891Zf r42 = A02.A0A;
                this.A0G.A02().AF8();
                A02.A03(r42, A002 + 604800000);
                return true;
            }
        }
        r4.A06(sb.toString());
        return false;
    }

    public boolean A0J(C450720b r7, C28941Pp r8, AnonymousClass1V8 r9, AnonymousClass1OT r10) {
        AbstractC16830pp AFZ;
        String A0I = r9.A0I("service", null);
        if (!TextUtils.isEmpty(A0I)) {
            C17070qD r0 = this.A0G;
            r0.A03();
            AbstractC248317b r02 = r0.A01;
            if (!(r02 == null || (AFZ = r02.AFZ(A0I)) == null || (r9 = AFZ.AZQ(r9)) != null)) {
                return false;
            }
        }
        AnonymousClass1IR A05 = this.A0L.A05(null, r9);
        if (r8 != null) {
            r8.A07 = A05;
            return true;
        }
        StringBuilder sb = new StringBuilder("xmpp/reader/on-recv-payment-transaction-update: stanzaKey:");
        sb.append(r10);
        sb.append(" paymentTransactionInfo id:");
        sb.append(A05.A0K);
        Log.i(sb.toString());
        AbstractC450820c r3 = r7.A00;
        Message obtain = Message.obtain(null, 0, 133, 0);
        Bundle data = obtain.getData();
        data.putParcelable("stanzaKey", r10);
        data.putParcelable("paymentTransactionInfo", A05);
        r3.AYY(obtain);
        return true;
    }
}
