package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Q5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Q5 {
    public C05310Pb A00 = new C05310Pb();
    public C03370Hn A01 = null;
    public Map A02 = new HashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        if (r7 != null) goto L_0x0006;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Picture A00(X.AnonymousClass0SI r7) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x0025
            X.0SG r5 = r7.A02
            if (r5 == 0) goto L_0x0025
        L_0x0006:
            X.0SG r3 = r7.A03
            if (r3 == 0) goto L_0x002c
            float r1 = r3.A01
            float r0 = r3.A03
            float r1 = r1 + r0
            float r2 = r3.A02
            float r0 = r3.A00
            float r2 = r2 + r0
        L_0x0014:
            double r0 = (double) r1
            double r0 = java.lang.Math.ceil(r0)
            int r3 = (int) r0
            double r0 = (double) r2
            double r1 = java.lang.Math.ceil(r0)
            int r0 = (int) r1
            android.graphics.Picture r0 = r6.A01(r7, r3, r0)
            return r0
        L_0x0025:
            X.0Hn r0 = r6.A01
            X.0SG r5 = r0.A00
            if (r7 == 0) goto L_0x002c
            goto L_0x0006
        L_0x002c:
            X.0Hn r4 = r6.A01
            X.0c3 r3 = r4.A01
            if (r3 == 0) goto L_0x005c
            X.0JP r0 = r3.A01
            X.0JP r1 = X.AnonymousClass0JP.percent
            if (r0 == r1) goto L_0x004d
            X.0c3 r2 = r4.A00
            if (r2 == 0) goto L_0x004d
            X.0JP r0 = r2.A01
            if (r0 == r1) goto L_0x004d
            r1 = 1119879168(0x42c00000, float:96.0)
            r0 = 1119879168(0x42c00000, float:96.0)
            float r1 = r3.A00(r1)
            float r2 = r2.A00(r0)
            goto L_0x0014
        L_0x004d:
            if (r5 == 0) goto L_0x005c
            r0 = 1119879168(0x42c00000, float:96.0)
            float r1 = r3.A00(r0)
            float r2 = r5.A00
            float r2 = r2 * r1
            float r0 = r5.A03
            float r2 = r2 / r0
            goto L_0x0014
        L_0x005c:
            X.0c3 r1 = r4.A00
            if (r1 == 0) goto L_0x006f
            if (r5 == 0) goto L_0x006f
            r0 = 1119879168(0x42c00000, float:96.0)
            float r2 = r1.A00(r0)
            float r1 = r5.A03
            float r1 = r1 * r2
            float r0 = r5.A00
            float r1 = r1 / r0
            goto L_0x0014
        L_0x006f:
            r0 = 512(0x200, float:7.175E-43)
            android.graphics.Picture r0 = r6.A01(r7, r0, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Q5.A00(X.0SI):android.graphics.Picture");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Picture A01(X.AnonymousClass0SI r11, int r12, int r13) {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Q5.A01(X.0SI, int, int):android.graphics.Picture");
    }

    public final AnonymousClass0I1 A02(AbstractC12490i0 r5, String str) {
        AnonymousClass0I1 r3 = (AnonymousClass0I1) r5;
        if (!str.equals(r3.A03)) {
            for (AnonymousClass0OO r1 : r5.ABO()) {
                if (r1 instanceof AnonymousClass0I1) {
                    r3 = (AnonymousClass0I1) r1;
                    if (!str.equals(r3.A03)) {
                        if ((r1 instanceof AbstractC12490i0) && (r3 = A02((AbstractC12490i0) r1, str)) != null) {
                        }
                    }
                }
            }
            return null;
        }
        return r3;
    }

    public AnonymousClass0I1 A03(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        C03370Hn r1 = this.A01;
        if (str.equals(((AnonymousClass0I1) r1).A03)) {
            return r1;
        }
        Map map = this.A02;
        if (map.containsKey(str)) {
            return (AnonymousClass0I1) map.get(str);
        }
        AnonymousClass0I1 A02 = A02(this.A01, str);
        map.put(str, A02);
        return A02;
    }

    public AnonymousClass0OO A04(String str) {
        String substring;
        String str2;
        String replace;
        if (str != null) {
            String str3 = "\"";
            if (!str.startsWith(str3) || !str.endsWith(str3)) {
                str3 = "'";
                if (str.startsWith(str3) && str.endsWith(str3)) {
                    substring = str.substring(1, str.length() - 1);
                    str2 = "\\'";
                }
                replace = str.replace("\\\n", "").replace("\\A", "\n");
                if (replace.length() > 1 && replace.startsWith("#")) {
                    return A03(replace.substring(1));
                }
            } else {
                substring = str.substring(1, str.length() - 1);
                str2 = "\\\"";
            }
            str = substring.replace(str2, str3);
            replace = str.replace("\\\n", "").replace("\\A", "\n");
            if (replace.length() > 1) {
                return A03(replace.substring(1));
            }
        }
        return null;
    }
}
