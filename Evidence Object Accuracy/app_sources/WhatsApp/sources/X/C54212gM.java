package X;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.calling.PeerAvatarLayout;
import com.whatsapp.components.button.ThumbnailButton;
import java.util.List;

/* renamed from: X.2gM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54212gM extends AnonymousClass02M {
    public final List A00 = C12960it.A0l();
    public final /* synthetic */ PeerAvatarLayout A01;

    public C54212gM(PeerAvatarLayout peerAvatarLayout) {
        this.A01 = peerAvatarLayout;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r11, int i) {
        int max;
        C53192d4 r8 = ((C75413jo) r11).A00;
        r8.clearAnimation();
        List list = this.A00;
        int A00 = r8.A00(list.size());
        ThumbnailButton thumbnailButton = r8.A02;
        ViewGroup.LayoutParams layoutParams = thumbnailButton.getLayoutParams();
        layoutParams.height = A00;
        layoutParams.width = A00;
        thumbnailButton.setLayoutParams(layoutParams);
        thumbnailButton.A02 = (float) ((A00 + 1) >> 1);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        PeerAvatarLayout peerAvatarLayout = this.A01;
        View view = (View) peerAvatarLayout.getParent();
        if (i != 0) {
            int size = list.size();
            int width = view.getWidth();
            if (size <= 1) {
                max = 0;
            } else {
                int A002 = r8.A00(size) + (r8.A00 << 1);
                max = Math.max(A002 - ((width - A002) / (size - 1)), (int) (((double) A002) * 0.4d));
            }
            int i2 = -max;
            if (C28141Kv.A00(peerAvatarLayout.A08)) {
                layoutParams2.rightMargin = i2;
            } else {
                layoutParams2.leftMargin = i2;
            }
        }
        r8.setLayoutParams(layoutParams2);
        if (Build.VERSION.SDK_INT >= 21) {
            float f = (float) (peerAvatarLayout.A00 - (peerAvatarLayout.A01 * i));
            r8.setElevation(f);
            r8.setElevation(f);
        }
        AnonymousClass28F r3 = peerAvatarLayout.A04;
        if (r3 != null) {
            peerAvatarLayout.A05.A02(thumbnailButton, r3, (C15370n3) list.get(i), true);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75413jo(new C53192d4(this.A01.getContext()), this);
    }
}
