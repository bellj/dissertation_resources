package X;

import java.util.Iterator;
import java.util.LinkedHashMap;
import org.json.JSONObject;

/* renamed from: X.1WH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WH extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AnonymousClass0t9 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1WH(AnonymousClass0t9 r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AnonymousClass0t9 r0 = this.this$0;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        String A03 = r0.A05.A03(1320);
        if (A03 != null) {
            try {
                JSONObject jSONObject = new JSONObject(A03);
                Iterator<String> keys = jSONObject.keys();
                C16700pc.A0B(keys);
                AnonymousClass1WR r3 = new AnonymousClass1WR(new AnonymousClass1WQ(new AnonymousClass1WP(jSONObject), AnonymousClass1WM.A06(keys), true));
                while (r3.hasNext()) {
                    JSONObject optJSONObject = jSONObject.optJSONObject((String) r3.next());
                    if (optJSONObject != null) {
                        String optString = optJSONObject.optString("app_id");
                        String optString2 = optJSONObject.optString("version", "");
                        C16700pc.A0B(optString);
                        C16700pc.A0B(optString2);
                        linkedHashMap.put(optString, optString2);
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return linkedHashMap;
    }
}
