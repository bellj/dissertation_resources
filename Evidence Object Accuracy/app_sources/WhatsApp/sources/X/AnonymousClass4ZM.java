package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.cert.Extension;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.4ZM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZM {
    public static Map A00 = Collections.synchronizedMap(new WeakHashMap());

    public static AnonymousClass5MI A00(URI uri, X509Certificate x509Certificate, List list, C114555Me r25, AnonymousClass4TK r26, AnonymousClass5S2 r27) {
        Map map;
        AnonymousClass5MI r9;
        AnonymousClass5NE r12;
        Map map2 = A00;
        Reference reference = (Reference) map2.get(uri);
        if (!(reference == null || (map = (Map) reference.get()) == null || (r9 = (AnonymousClass5MI) map.get(r25)) == null)) {
            byte[] A05 = AnonymousClass5NH.A05(r9.A01.A01);
            C114665Mp r0 = (A05 instanceof C114545Md ? (C114545Md) A05 : A05 != null ? new C114545Md(AbstractC114775Na.A04(A05)) : null).A02;
            if (r0 == null) {
                r0 = null;
            }
            AbstractC114775Na r11 = r0.A02;
            for (int i = 0; i != r11.A0B(); i++) {
                AnonymousClass1TN A0D = r11.A0D(i);
                AnonymousClass5ML r1 = A0D instanceof AnonymousClass5ML ? (AnonymousClass5ML) A0D : A0D != null ? new AnonymousClass5ML(AbstractC114775Na.A04(A0D)) : null;
                if (r25.equals(r1.A02) && (r12 = r1.A00) != null) {
                    try {
                    } catch (ParseException unused) {
                        map.remove(r25);
                    }
                    if (new Date(r26.A04.getTime()).after(r12.A0D())) {
                        map.remove(r25);
                        r9 = null;
                    }
                }
            }
            if (r9 != null) {
                return r9;
            }
        }
        try {
            URL url = uri.toURL();
            C94954co r112 = new C94954co(10);
            r112.A06(new AnonymousClass5MH(r25));
            C94954co r10 = new C94954co(10);
            byte[] bArr = null;
            for (int i2 = 0; i2 != list.size(); i2++) {
                Extension extension = (Extension) list.get(i2);
                byte[] value = extension.getValue();
                if ("1.3.6.1.5.5.7.48.1.2".equals(extension.getId())) {
                    bArr = value;
                }
                r10.A06(new C114715Mu(C72453ed.A12(extension.getId()), value, extension.isCritical()));
            }
            try {
                byte[] A01 = new AnonymousClass5MF(new C114525Mb(new AnonymousClass5NZ(r112), AnonymousClass5MX.A01(new AnonymousClass5NZ(r10)))).A01();
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-type", "application/ocsp-request");
                httpURLConnection.setRequestProperty("Content-length", String.valueOf(A01.length));
                OutputStream outputStream = httpURLConnection.getOutputStream();
                outputStream.write(A01);
                outputStream.flush();
                InputStream inputStream = httpURLConnection.getInputStream();
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength < 0) {
                    contentLength = 32768;
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                long j = (long) contentLength;
                byte[] bArr2 = new byte[4096];
                long j2 = 0;
                while (true) {
                    int read = inputStream.read(bArr2, 0, 4096);
                    if (read >= 0) {
                        long j3 = (long) read;
                        if (j - j2 >= j3) {
                            j2 += j3;
                            byteArrayOutputStream.write(bArr2, 0, read);
                        } else {
                            throw new C867848v();
                        }
                    } else {
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        AnonymousClass5MI r4 = byteArray instanceof AnonymousClass5MI ? (AnonymousClass5MI) byteArray : byteArray != null ? new AnonymousClass5MI(AbstractC114775Na.A04(byteArray)) : null;
                        AnonymousClass5ND r13 = r4.A00.A00;
                        if (r13.A0B() == 0) {
                            AnonymousClass5MV r5 = r4.A01;
                            if (r5 == null) {
                                r5 = null;
                            }
                            if (r5.A00.A04(AbstractC116925Xl.A02)) {
                                byte[] bArr3 = r5.A01.A00;
                                if (AnonymousClass5GK.A00(x509Certificate, bArr3 instanceof C114545Md ? (C114545Md) bArr3 : bArr3 != null ? new C114545Md(AbstractC114775Na.A04(bArr3)) : null, r26, r27, bArr)) {
                                    Reference reference2 = (Reference) map2.get(uri);
                                    if (reference2 != null) {
                                        ((Map) reference2.get()).put(r25, r4);
                                        return r4;
                                    }
                                    HashMap A11 = C12970iu.A11();
                                    A11.put(r25, r4);
                                    map2.put(uri, C12970iu.A10(A11));
                                    return r4;
                                }
                            }
                            throw AnonymousClass4TK.A00("OCSP response failed to validate", null, r26);
                        }
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("OCSP responder failed: ");
                        throw AnonymousClass4TK.A00(C12970iu.A0s(new BigInteger(r13.A01), A0h), null, r26);
                    }
                }
            } catch (IOException e) {
                throw AnonymousClass4TK.A00(C12960it.A0d(e.getMessage(), C12960it.A0j("configuration error: ")), e, r26);
            }
        } catch (MalformedURLException e2) {
            throw AnonymousClass4TK.A00(C12960it.A0d(e2.getMessage(), C12960it.A0j("configuration error: ")), e2, r26);
        }
    }
}
