package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.0Nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C04890Nl {
    public final ImageView A00;
    public final ImageView A01;
    public final ImageView A02;
    public final TextView A03;
    public final TextView A04;

    public C04890Nl(View view) {
        this.A03 = (TextView) view.findViewById(16908308);
        this.A04 = (TextView) view.findViewById(16908309);
        this.A00 = (ImageView) view.findViewById(16908295);
        this.A01 = (ImageView) view.findViewById(16908296);
        this.A02 = (ImageView) view.findViewById(R.id.edit_query);
    }
}
