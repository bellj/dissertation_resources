package X;

import android.content.Context;
import com.whatsapp.chatinfo.ContactInfoActivity;

/* renamed from: X.4pt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102904pt implements AbstractC009204q {
    public final /* synthetic */ ContactInfoActivity A00;

    public C102904pt(ContactInfoActivity contactInfoActivity) {
        this.A00 = contactInfoActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
