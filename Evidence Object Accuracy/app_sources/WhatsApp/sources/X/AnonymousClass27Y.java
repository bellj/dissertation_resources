package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

/* renamed from: X.27Y  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass27Y extends SurfaceView implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass27Y(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass27X r2 = (AnonymousClass27X) this;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            r2.A0G = (AnonymousClass01d) r1.ALI.get();
            r2.A0I = (C21200x2) r1.A2q.get();
            r2.A0J = (C16630pM) r1.AIc.get();
            r2.A0H = (C15890o4) r1.AN1.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
