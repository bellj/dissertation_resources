package X;

import com.whatsapp.R;

/* renamed from: X.5os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124265os extends AbstractC16350or {
    public final /* synthetic */ C129165xH A00;
    public final /* synthetic */ Runnable A01;
    public final /* synthetic */ String A02;

    public C124265os(C129165xH r1, Runnable runnable, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = runnable;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return this.A00.A02.A0M(this.A02);
    }

    @Override // X.AbstractC16350or
    public void A06() {
        ((ActivityC13810kN) this.A00.A03).A2C(R.string.register_wait_message);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1IR r7 = (AnonymousClass1IR) obj;
        C129165xH r5 = this.A00;
        AbstractC136316Mb r4 = r5.A03;
        ((ActivityC13810kN) r4).AaN();
        if (r7 == null || !r4.AdW(r7.A02)) {
            this.A01.run();
        } else {
            C16380ov r0 = r5.A00;
            AnonymousClass009.A05(r0);
            r4.Ads(r5.A00.A0z.A00, r7.A02, r0.A11);
        }
        r5.A01 = null;
    }
}
