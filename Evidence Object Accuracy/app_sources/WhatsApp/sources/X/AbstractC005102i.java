package X;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.View;

/* renamed from: X.02i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC005102i {
    public abstract float A00();

    public abstract int A01();

    public abstract Context A02();

    public abstract View A03();

    public AbstractC009504t A04(AnonymousClass02Q r2) {
        return null;
    }

    public void A05() {
    }

    public abstract void A06();

    public abstract void A07(float f);

    public abstract void A08(int i);

    public abstract void A09(int i);

    public abstract void A0A(int i);

    public void A0B(Configuration configuration) {
    }

    public abstract void A0C(Drawable drawable);

    public abstract void A0D(Drawable drawable);

    public abstract void A0E(Drawable drawable);

    public abstract void A0F(View view);

    public abstract void A0G(View view, C009604u v);

    public abstract void A0H(CharSequence charSequence);

    public abstract void A0I(CharSequence charSequence);

    public abstract void A0J(CharSequence charSequence);

    public abstract void A0K(boolean z);

    public abstract void A0L(boolean z);

    public abstract void A0M(boolean z);

    public abstract void A0N(boolean z);

    public abstract void A0O(boolean z);

    public abstract void A0P(boolean z);

    public abstract void A0Q(boolean z);

    public boolean A0R() {
        return false;
    }

    public boolean A0S() {
        return false;
    }

    public boolean A0T() {
        return false;
    }

    public abstract boolean A0U();

    public abstract boolean A0V(int i, KeyEvent keyEvent);

    public boolean A0W(KeyEvent keyEvent) {
        return false;
    }
}
