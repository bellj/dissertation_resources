package X;

import com.whatsapp.Conversation;

/* renamed from: X.55C  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass55C implements AbstractC116055Ty {
    public final /* synthetic */ Conversation A00;
    public final /* synthetic */ C35191hP A01;
    public final /* synthetic */ C30421Xi A02;

    public /* synthetic */ AnonymousClass55C(Conversation conversation, C35191hP r2, C30421Xi r3) {
        this.A00 = conversation;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC116055Ty
    public final void ATn(int i) {
        Conversation conversation = this.A00;
        C30421Xi r3 = this.A02;
        C35191hP r2 = this.A01;
        if (conversation.A4J && conversation.A7g(r3, i, r2.A0T, r2.A0Y)) {
            r2.A0S = true;
        }
    }
}
