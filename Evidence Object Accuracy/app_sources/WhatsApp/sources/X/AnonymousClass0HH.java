package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0HH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HH extends AbstractC08070aX {
    public AnonymousClass0QR A00;
    public AnonymousClass0QR A01;
    public AnonymousClass0QR A02;
    public AnonymousClass0QR A03;
    public AnonymousClass0QR A04;
    public AnonymousClass0QR A05;
    public AnonymousClass0QR A06;
    public AnonymousClass0QR A07;
    public AnonymousClass0QR A08;
    public final Matrix A09 = new Matrix();
    public final Paint A0A = new C020409r(this);
    public final Paint A0B = new C020509s(this);
    public final RectF A0C = new RectF();
    public final AnonymousClass036 A0D = new AnonymousClass036();
    public final C05540Py A0E;
    public final AnonymousClass0AA A0F;
    public final AnonymousClass0Gv A0G;
    public final StringBuilder A0H = new StringBuilder(2);
    public final Map A0I = new HashMap();

    public AnonymousClass0HH(AnonymousClass0AA r4, AnonymousClass0PU r5) {
        super(r4, r5);
        this.A0F = r4;
        this.A0E = r5.A09;
        AnonymousClass0Gv r1 = new AnonymousClass0Gv(r5.A0B.A00);
        this.A0G = r1;
        r1.A07.add(this);
        A03(r1);
        C04850Nh r2 = r5.A0C;
        if (r2 != null) {
            AnonymousClass0H4 r0 = r2.A00;
            if (r0 != null) {
                C03230Gz r02 = new C03230Gz(r0.A00);
                this.A00 = r02;
                r02.A07.add(this);
                A03(this.A00);
            }
            AnonymousClass0H4 r03 = r2.A01;
            if (r03 != null) {
                C03230Gz r04 = new C03230Gz(r03.A00);
                this.A02 = r04;
                r04.A07.add(this);
                A03(this.A02);
            }
            AnonymousClass0H9 r05 = r2.A02;
            if (r05 != null) {
                AnonymousClass0H1 r06 = new AnonymousClass0H1(r05.A00);
                this.A04 = r06;
                r06.A07.add(this);
                A03(this.A04);
            }
            AnonymousClass0H9 r07 = r2.A03;
            if (r07 != null) {
                AnonymousClass0H1 r08 = new AnonymousClass0H1(r07.A00);
                this.A07 = r08;
                r08.A07.add(this);
                A03(this.A07);
            }
        }
    }

    public static final void A00(Canvas canvas, Paint paint, Path path) {
        if (paint.getColor() == 0) {
            return;
        }
        if (paint.getStyle() != Paint.Style.STROKE || paint.getStrokeWidth() != 0.0f) {
            canvas.drawPath(path, paint);
        }
    }

    public static final void A01(Canvas canvas, Paint paint, String str) {
        if (paint.getColor() == 0) {
            return;
        }
        if (paint.getStyle() != Paint.Style.STROKE || paint.getStrokeWidth() != 0.0f) {
            canvas.drawText(str, 0, str.length(), 0.0f, 0.0f, paint);
        }
    }

    public static final void A02(Canvas canvas, EnumC03730Ix r4, float f) {
        float f2;
        switch (r4.ordinal()) {
            case 1:
                f2 = -f;
                break;
            case 2:
                f2 = (-f) / 2.0f;
                break;
            default:
                return;
        }
        canvas.translate(f2, 0.0f);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v6, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x02d7, code lost:
        if (r5 == null) goto L_0x02d9;
     */
    @Override // X.AbstractC08070aX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.graphics.Canvas r27, android.graphics.Matrix r28, int r29) {
        /*
        // Method dump skipped, instructions count: 1092
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HH.A06(android.graphics.Canvas, android.graphics.Matrix, int):void");
    }

    @Override // X.AbstractC08070aX, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r4, Object obj) {
        AnonymousClass0QR r0;
        super.A5q(r4, obj);
        if (obj == AbstractC12810iX.A0R) {
            AnonymousClass0QR r1 = this.A01;
            if (r1 != null) {
                this.A0O.remove(r1);
            }
            if (r4 == null) {
                this.A01 = null;
                return;
            }
            AnonymousClass0Gu r02 = new AnonymousClass0Gu(r4, null);
            this.A01 = r02;
            r02.A07.add(this);
            r0 = this.A01;
        } else if (obj == AbstractC12810iX.A0T) {
            AnonymousClass0QR r12 = this.A03;
            if (r12 != null) {
                this.A0O.remove(r12);
            }
            if (r4 == null) {
                this.A03 = null;
                return;
            }
            AnonymousClass0Gu r03 = new AnonymousClass0Gu(r4, null);
            this.A03 = r03;
            r03.A07.add(this);
            r0 = this.A03;
        } else if (obj == AbstractC12810iX.A0G) {
            AnonymousClass0QR r13 = this.A05;
            if (r13 != null) {
                this.A0O.remove(r13);
            }
            if (r4 == null) {
                this.A05 = null;
                return;
            }
            AnonymousClass0Gu r04 = new AnonymousClass0Gu(r4, null);
            this.A05 = r04;
            r04.A07.add(this);
            r0 = this.A05;
        } else if (obj == AbstractC12810iX.A0I) {
            AnonymousClass0QR r14 = this.A08;
            if (r14 != null) {
                this.A0O.remove(r14);
            }
            if (r4 == null) {
                this.A08 = null;
                return;
            }
            AnonymousClass0Gu r05 = new AnonymousClass0Gu(r4, null);
            this.A08 = r05;
            r05.A07.add(this);
            r0 = this.A08;
        } else if (obj == AbstractC12810iX.A0H) {
            AnonymousClass0QR r15 = this.A06;
            if (r15 != null) {
                this.A0O.remove(r15);
            }
            if (r4 == null) {
                this.A06 = null;
                return;
            }
            AnonymousClass0Gu r06 = new AnonymousClass0Gu(r4, null);
            this.A06 = r06;
            r06.A07.add(this);
            r0 = this.A06;
        } else {
            return;
        }
        A03(r0);
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        C05540Py r1 = this.A0E;
        rectF.set(0.0f, 0.0f, (float) r1.A04.width(), (float) r1.A04.height());
    }
}
