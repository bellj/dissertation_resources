package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.1Ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27581Ic {
    public ServiceConnectionC97784hd A00;
    public AnonymousClass5Y3 A01;
    public boolean A02;
    public final Context A03;
    public final Object A04 = new Object();
    public final boolean A05;

    public C27581Ic(Context context, boolean z, boolean z2) {
        Context applicationContext;
        C13020j0.A01(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.A03 = context;
        this.A02 = false;
        this.A05 = z2;
    }

    public final void A01() {
        C13020j0.A06("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            Context context = this.A03;
            if (!(context == null || this.A00 == null)) {
                if (this.A02) {
                    AnonymousClass3IW.A00().A01(context, this.A00);
                }
                this.A02 = false;
                this.A01 = null;
                this.A00 = null;
            }
        }
    }

    public void finalize() {
        A01();
        super.finalize();
    }

    public static C27591Id A00(Context context) {
        boolean z;
        float f;
        boolean z2;
        C27591Id r11;
        AnonymousClass4IN r6 = new AnonymousClass4IN(context);
        try {
            SharedPreferences sharedPreferences = r6.A00;
            z = sharedPreferences == null ? false : sharedPreferences.getBoolean("gads:ad_id_app_context:enabled", false);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            z = false;
        }
        try {
            SharedPreferences sharedPreferences2 = r6.A00;
            f = sharedPreferences2 == null ? 0.0f : sharedPreferences2.getFloat("gads:ad_id_app_context:ping_ratio", 0.0f);
        } catch (Throwable th2) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th2);
            f = 0.0f;
        }
        String str = "";
        try {
            SharedPreferences sharedPreferences3 = r6.A00;
            if (sharedPreferences3 != null) {
                str = sharedPreferences3.getString("gads:ad_id_use_shared_preference:experiment_id", str);
            }
        } catch (Throwable th3) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th3);
        }
        try {
            SharedPreferences sharedPreferences4 = r6.A00;
            z2 = sharedPreferences4 == null ? false : sharedPreferences4.getBoolean("gads:ad_id_use_persistent_service:enabled", false);
        } catch (Throwable th4) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th4);
            z2 = false;
        }
        C27581Ic r10 = new C27581Ic(context, z, z2);
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            C13020j0.A06("Calling this from your main thread can lead to deadlock");
            synchronized (r10) {
                if (r10.A02) {
                }
                Context context2 = r10.A03;
                boolean z3 = r10.A05;
                try {
                    context2.getPackageManager().getPackageInfo("com.android.vending", 0);
                    int A00 = C471929k.A00.A00(context2, 12451000);
                    if (A00 == 0 || A00 == 2) {
                        String str2 = z3 ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                        ServiceConnectionC97784hd r62 = new ServiceConnectionC97784hd();
                        Intent intent = new Intent(str2);
                        intent.setPackage("com.google.android.gms");
                        if (AnonymousClass3IW.A00().A02(context2, intent, r62, context2.getClass().getName(), 1)) {
                            r10.A00 = r62;
                            try {
                                TimeUnit timeUnit = TimeUnit.MILLISECONDS;
                                C13020j0.A06("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
                                if (!r62.A00) {
                                    r62.A00 = true;
                                    IBinder iBinder = (IBinder) r62.A01.poll(10000, timeUnit);
                                    if (iBinder != null) {
                                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                                        r10.A01 = queryLocalInterface instanceof AnonymousClass5Y3 ? (AnonymousClass5Y3) queryLocalInterface : new C98434ig(iBinder);
                                        r10.A02 = true;
                                    } else {
                                        throw new TimeoutException("Timed out waiting for the service connection");
                                    }
                                } else {
                                    throw new IllegalStateException("Cannot call get on this connection more than once");
                                }
                            } catch (InterruptedException unused) {
                                throw new IOException("Interrupted exception");
                            } catch (Throwable th5) {
                                throw new IOException(th5);
                            }
                        } else {
                            throw new IOException("Connection failure");
                        }
                    } else {
                        throw new IOException("Google Play services not available");
                    }
                } catch (PackageManager.NameNotFoundException unused2) {
                    throw new AnonymousClass29w(9);
                }
            }
            C13020j0.A06("Calling this from your main thread can lead to deadlock");
            synchronized (r10) {
                if (!r10.A02) {
                    synchronized (r10.A04) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                C13020j0.A01(r10.A00);
                AnonymousClass5Y3 r3 = r10.A01;
                C13020j0.A01(r3);
                try {
                    C98434ig r32 = (C98434ig) r3;
                    Parcel obtain = Parcel.obtain();
                    obtain.writeInterfaceToken(r32.A01);
                    Parcel A002 = r32.A00(1, obtain);
                    String readString = A002.readString();
                    A002.recycle();
                    C98434ig r33 = (C98434ig) r10.A01;
                    Parcel obtain2 = Parcel.obtain();
                    obtain2.writeInterfaceToken(r33.A01);
                    obtain2.writeInt(1);
                    Parcel A003 = r33.A00(2, obtain2);
                    boolean z4 = false;
                    if (A003.readInt() != 0) {
                        z4 = true;
                    }
                    A003.recycle();
                    r11 = new C27591Id(readString, z4);
                } catch (RemoteException e) {
                    Log.i("AdvertisingIdClient", "GMS remote exception ", e);
                    throw new IOException("Remote exception");
                }
            }
            synchronized (r10.A04) {
            }
            r10.A02(r11, str, null, f, SystemClock.elapsedRealtime() - elapsedRealtime, z);
            return r11;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final void A02(C27591Id r6, String str, Throwable th, float f, long j, boolean z) {
        if (Math.random() <= ((double) f)) {
            HashMap hashMap = new HashMap();
            String str2 = "1";
            String str3 = "0";
            if (z) {
                str3 = str2;
            }
            hashMap.put("app_context", str3);
            if (r6 != null) {
                if (!r6.A01) {
                    str2 = "0";
                }
                hashMap.put("limit_ad_tracking", str2);
                String str4 = r6.A00;
                if (str4 != null) {
                    hashMap.put("ad_id_size", Integer.toString(str4.length()));
                }
            }
            if (th != null) {
                hashMap.put("error", th.getClass().getName());
            }
            if (str != null && !str.isEmpty()) {
                hashMap.put("experiment_id", str);
            }
            hashMap.put("tag", "AdvertisingIdClient");
            hashMap.put("time_spent", Long.toString(j));
            new C71663dJ(hashMap).start();
        }
    }
}
