package X;

import android.view.View;

/* renamed from: X.3Eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64103Eg {
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;

    public C64103Eg(View view) {
        int[] A07 = C13000ix.A07();
        view.getLocationInWindow(A07);
        this.A06 = A07[0];
        this.A07 = A07[1];
        this.A02 = view.getTranslationX();
        this.A03 = view.getTranslationY();
        this.A00 = view.getScaleX();
        this.A01 = view.getScaleY();
        this.A05 = view.getWidth();
        this.A04 = view.getHeight();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C64103Eg)) {
            return false;
        }
        C64103Eg r4 = (C64103Eg) obj;
        if (r4.A02 == this.A02 && r4.A03 == this.A03 && r4.A00 == this.A00 && r4.A01 == this.A01 && r4.A05 == this.A05 && r4.A04 == this.A04 && r4.A06 == this.A06 && r4.A07 == this.A07) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object[] objArr = new Object[8];
        objArr[0] = Float.valueOf(this.A02);
        objArr[1] = Float.valueOf(this.A03);
        objArr[2] = Float.valueOf(this.A00);
        objArr[3] = Float.valueOf(this.A01);
        objArr[4] = Integer.valueOf(this.A05);
        objArr[5] = Integer.valueOf(this.A04);
        objArr[6] = Integer.valueOf(this.A06);
        return C12980iv.A0B(Integer.valueOf(this.A07), objArr, 7);
    }
}
