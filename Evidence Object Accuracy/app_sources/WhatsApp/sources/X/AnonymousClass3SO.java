package X;

import android.graphics.Bitmap;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

/* renamed from: X.3SO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SO implements AnonymousClass5YZ {
    public final Set A00 = Collections.newSetFromMap(new IdentityHashMap());

    @Override // X.AbstractC12900ik, X.AbstractC12240hb
    public /* bridge */ /* synthetic */ void Aa4(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        this.A00.remove(bitmap);
        bitmap.recycle();
    }

    @Override // X.AbstractC12900ik
    public /* bridge */ /* synthetic */ Object get(int i) {
        Bitmap createBitmap = Bitmap.createBitmap(1, (int) Math.ceil(((double) i) / 2.0d), Bitmap.Config.RGB_565);
        this.A00.add(createBitmap);
        return createBitmap;
    }
}
