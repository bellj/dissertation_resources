package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0kB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13700kB {
    public static Object A00(C13600jz r4, TimeUnit timeUnit, long j) {
        C13020j0.A06("Must not be called on the main application thread");
        C13020j0.A02(r4, "Task must not be null");
        C13020j0.A02(timeUnit, "TimeUnit must not be null");
        if (!r4.A09()) {
            C13610k0 r3 = new C13610k0(null);
            Executor executor = C13650k5.A01;
            r4.A06(r3, executor);
            r4.A05(r3, executor);
            r4.A03.A00(new AnonymousClass0k7(r3, executor));
            r4.A04();
            if (!r3.A00.await(j, timeUnit)) {
                throw new TimeoutException("Timed out waiting for Task");
            }
        }
        if (r4.A0A()) {
            return r4.A01();
        }
        if (r4.A05) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(r4.A00());
    }
}
