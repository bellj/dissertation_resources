package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.19S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19S extends AbstractC16220oe {
    public void A05(UserJid userJid) {
        for (AnonymousClass3VN r1 : A01()) {
            if (C29941Vi.A00(r1.A06.A0G, userJid)) {
                r1.AQR(userJid);
            }
        }
    }
}
