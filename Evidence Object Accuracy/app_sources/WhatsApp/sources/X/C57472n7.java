package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57472n7 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57472n7 A05;
    public static volatile AnonymousClass255 A06;
    public double A00;
    public double A01;
    public int A02;
    public int A03;
    public int A04;

    static {
        C57472n7 r0 = new C57472n7();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57472n7 r3 = (C57472n7) obj2;
                int i = this.A02;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A03;
                int i3 = r3.A02;
                this.A03 = r7.Afp(i2, r3.A03, A1R, C12960it.A1R(i3));
                this.A04 = r7.Afp(this.A04, r3.A04, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A00 = r7.Afn(this.A00, r3.A00, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A01 = r7.Afn(this.A01, r3.A01, C12960it.A1V(i & 8, 8), C12960it.A1V(i3 & 8, 8));
                if (r7 == C463025i.A00) {
                    this.A02 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                this.A02 |= 1;
                                this.A03 = r72.A02();
                            } else if (A03 == 16) {
                                this.A02 |= 2;
                                this.A04 = r72.A02();
                            } else if (A03 == 25) {
                                this.A02 |= 4;
                                this.A00 = Double.longBitsToDouble(r72.A05());
                            } else if (A03 == 33) {
                                this.A02 |= 8;
                                this.A01 = Double.longBitsToDouble(r72.A05());
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57472n7();
            case 5:
                return new C82423vb();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57472n7.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A02;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A03(1, this.A03);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A03(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i2 += 9;
        }
        if ((i3 & 8) == 8) {
            i2 += 9;
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0E(1, this.A03);
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0E(2, this.A04);
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0G(3, Double.doubleToRawLongBits(this.A00));
        }
        if ((this.A02 & 8) == 8) {
            codedOutputStream.A0G(4, Double.doubleToRawLongBits(this.A01));
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
