package X;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0zL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22600zL extends AbstractC16230of implements AbstractC22060yS {
    public C37691mq A00;
    public boolean A01;
    public boolean A02;
    public final long A03;
    public final Handler A04 = new HandlerC37681mp(Looper.getMainLooper(), this);
    public final AbstractC15710nm A05;
    public final C15450nH A06;
    public final C16240og A07;
    public final C14830m7 A08;
    public final C14850m9 A09;
    public final C19940uv A0A;
    public final AnonymousClass156 A0B;
    public final C16630pM A0C;
    public final C37671mo A0D;
    public final AnonymousClass157 A0E;
    public final AbstractC14440lR A0F;
    public final AnonymousClass14O A0G;
    public final Object A0H = new Object();
    public final Object A0I = new Object();
    public final AtomicBoolean A0J = new AtomicBoolean(false);
    public volatile C27231Gn A0K;

    public C22600zL(C16210od r6, AbstractC15710nm r7, C15450nH r8, C16240og r9, C14830m7 r10, C14850m9 r11, C19940uv r12, AnonymousClass156 r13, C16630pM r14, AnonymousClass157 r15, AbstractC14440lR r16, AnonymousClass14O r17) {
        C37671mo r4 = new C37671mo();
        this.A08 = r10;
        this.A09 = r11;
        this.A0G = r17;
        this.A05 = r7;
        this.A0E = r15;
        this.A0F = r16;
        this.A06 = r8;
        this.A07 = r9;
        this.A0A = r12;
        this.A0C = r14;
        this.A0B = r13;
        this.A0D = r4;
        this.A03 = 1000;
        r6.A03(this);
    }

    public static String A01(String str, JSONObject jSONObject) {
        if (jSONObject.has(str)) {
            return jSONObject.getString(str);
        }
        return null;
    }

    public static Set A02(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            hashSet.add(jSONArray.getString(i));
        }
        return hashSet;
    }

    public C37741mv A05(AbstractC37701mr r21, int i) {
        AnonymousClass009.A0F(true);
        if (r21 instanceof C37711ms) {
            return new C37741mv(new C37721mt(((C37711ms) r21).A00, this.A03));
        }
        if (r21 instanceof C37751mw) {
            C37751mw r2 = (C37751mw) r21;
            C37691mq A08 = A08();
            C14830m7 r6 = this.A08;
            C14850m9 r7 = this.A09;
            AnonymousClass14O r11 = this.A0G;
            AbstractC15710nm r3 = this.A05;
            C15450nH r4 = this.A06;
            C16240og r5 = this.A07;
            C19940uv r8 = this.A0A;
            long j = this.A03;
            return new C37741mv(new C37771my(C37761mx.A02(r3, r4, r5, r6, r7, r8, this, A08, r11, r2.A02, null, null, i, 0, j, false), r2.A01, j));
        } else if (r21 instanceof C37781mz) {
            C37781mz r22 = (C37781mz) r21;
            return A06(((AbstractC37791n0) r22).A01, r22.A02, r22.A00, i, false);
        } else {
            StringBuilder sb = new StringBuilder("Unknown url generator type: ");
            sb.append(r21);
            throw new AssertionError(sb.toString());
        }
    }

    public final C37741mv A06(String str, String str2, String str3, int i, boolean z) {
        C37691mq A08 = A08();
        return new C37741mv(C37761mx.A02(this.A05, this.A06, this.A07, this.A08, this.A09, this.A0A, this, A08, this.A0G, str, str2, str3, i, 0, this.A03, z));
    }

    public AbstractC37731mu A07(String str, String str2, String str3, int i, int i2) {
        C37691mq A08 = A08();
        return C37761mx.A02(this.A05, this.A06, this.A07, this.A08, this.A09, this.A0A, this, A08, this.A0G, str, str2, str3, i, i2, this.A03, false);
    }

    public C37691mq A08() {
        C37691mq r0;
        synchronized (this.A0H) {
            r0 = this.A00;
        }
        return r0;
    }

    public void A09() {
        boolean z;
        Log.i("routeselector/requestroutesandwaitforauth");
        if (A0E()) {
            C37691mq r0 = this.A00;
            if (r0 == null || r0.A02 <= SystemClock.elapsedRealtime()) {
                Log.i("routeselector/requestroutesandwaitforauth/waiting for response");
                AnonymousClass009.A00();
                long uptimeMillis = SystemClock.uptimeMillis();
                while (true) {
                    C37691mq r02 = this.A00;
                    if (r02 == null || r02.A02 <= SystemClock.elapsedRealtime()) {
                        C27231Gn r6 = this.A0K;
                        synchronized (r6) {
                            z = false;
                            if (r6.A00 > 0) {
                                z = true;
                            }
                        }
                        if (!z) {
                            Log.w("routeselector/waitforroutingresponse/giving up because no request in flight");
                            return;
                        }
                        Object obj = this.A0I;
                        synchronized (obj) {
                            try {
                                obj.wait(1000);
                            } catch (InterruptedException e) {
                                Log.w("routeselector/waitforroutingresponse/interrupted while waiting on route selection", e);
                            }
                            C37691mq r03 = this.A00;
                            if (r03 == null || r03.A02 <= SystemClock.elapsedRealtime()) {
                                Log.w("routeselector/waitforroutingresponse/routing response still not available");
                            }
                            if (20000 + uptimeMillis < SystemClock.uptimeMillis()) {
                                Log.w("routeselector/waitforroutingresponse/waited too long for routing response! Give up");
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final void A0A() {
        boolean z;
        String str;
        AnonymousClass156 r6 = this.A0B;
        synchronized (r6) {
            z = false;
            if (r6.A01.A00() <= r6.A00) {
                z = true;
            }
        }
        if (z) {
            Log.i("routeselector/requestupdatedroutinginfo throttled");
            return;
        }
        C27231Gn r8 = this.A0K;
        C37691mq A08 = A08();
        if (A08 == null) {
            str = null;
        } else {
            str = A08.A09;
        }
        synchronized (r8) {
            Log.i("routeselector/requestupdatedroutinginfo");
            if (r8.A00 == 0) {
                C17220qS r7 = r8.A02;
                String A01 = r7.A01();
                if (r7.A0D(r8, new AnonymousClass1V8(new AnonymousClass1V8("media_conn", str != null ? new AnonymousClass1W9[]{new AnonymousClass1W9("last_id", str)} : null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "w:m"), new AnonymousClass1W9("type", "set")}), A01, 124, 32000)) {
                    r8.A00 = SystemClock.elapsedRealtime();
                } else {
                    Log.i("app/sendgetmediaroutinginfo not sent");
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("routeselector/requestupdatedroutinginfo/not sending request; inFlightMediaRoutingRequestTime=");
            sb.append(r8.A00);
            Log.w(sb.toString());
        }
    }

    public final void A0B() {
        Log.i("routeselector/setuprouterequesttimer");
        C37691mq A08 = A08();
        if (A08 != null) {
            Handler handler = this.A04;
            handler.removeMessages(0);
            long elapsedRealtime = (A08.A02 - SystemClock.elapsedRealtime()) - 60000;
            StringBuilder sb = new StringBuilder("routeselector/settimerorupdateroutes/creating timer task with delay ");
            sb.append(elapsedRealtime);
            Log.i(sb.toString());
            handler.sendEmptyMessageDelayed(0, elapsedRealtime);
        }
    }

    public final void A0C() {
        if ((!this.A09.A07(149) || this.A01) && this.A02) {
            this.A0F.Ab2(new RunnableBRunnable0Shape11S0100000_I0_11(this, 21));
        }
    }

    public final void A0D(C37691mq r19) {
        StringBuilder sb = new StringBuilder("routeselector/setroutinginfo/got a RoutingResponse with ");
        sb.append(r19.A0A.size());
        sb.append(" route classes");
        Log.i(sb.toString());
        this.A0D.A00.A02();
        synchronized (this.A0H) {
            C37691mq r4 = this.A00;
            if (r4 == null || r19.A0B) {
                this.A00 = r19;
                for (AbstractC37811n3 r2 : A01()) {
                    r2.AT2(r19);
                }
            } else {
                List list = r4.A0A;
                this.A00 = new C37691mq(r19.A08, r19.A09, list, r19.A00, r19.A01, r19.A05, r19.A03, r19.A06, r19.A07, false);
                Log.i("routeselector/setroutinginfo/previous hosts retained");
            }
        }
        Object obj = this.A0I;
        synchronized (obj) {
            obj.notifyAll();
        }
        A0B();
    }

    public final boolean A0E() {
        Log.i("routeselector/requestroutinginfoifnulloralmostexpired");
        if (this.A09.A07(149) && this.A0J.compareAndSet(false, true)) {
            C37691mq r4 = null;
            String string = this.A0C.A01("route_selector_prefs").getString("media_conn", null);
            C14830m7 r9 = this.A08;
            if (string != null) {
                try {
                    JSONObject jSONObject = new JSONObject(string);
                    String string2 = jSONObject.getString("auth_token");
                    long j = jSONObject.getLong("conn_ttl");
                    long j2 = jSONObject.getLong("auth_ttl");
                    long j3 = jSONObject.getLong("max_buckets");
                    JSONArray jSONArray = jSONObject.getJSONArray("hosts");
                    int length = jSONArray.length();
                    ArrayList arrayList = new ArrayList(length);
                    for (int i = 0; i < length; i++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        arrayList.add(new AnonymousClass1n2(jSONObject2.getString("hostname"), A01("ip4", jSONObject2), A01("ip6", jSONObject2), A01("class", jSONObject2), A01("fallback_hostname", jSONObject2), A01("fallback_ip4", jSONObject2), A01("fallback_ip6", jSONObject2), A01("fallback_class", jSONObject2), A01("type", jSONObject2), A02(jSONObject2.optJSONArray("upload")), A02(jSONObject2.optJSONArray("download")), A02(jSONObject2.optJSONArray("download_buckets")), jSONObject2.optBoolean("force_ip")));
                    }
                    String A01 = A01("last_id", jSONObject);
                    boolean z = jSONObject.getBoolean("is_new");
                    r4 = new C37691mq(string2, A01, arrayList, jSONObject.getInt("max_autodownload_retry"), jSONObject.getInt("max_manual_retry"), j, j2, j3, (jSONObject.getLong("send_time_abs_ms") - r9.A00()) + SystemClock.elapsedRealtime(), z);
                } catch (JSONException e) {
                    Log.e("routingresponse/can't parse json", e);
                }
            }
            synchronized (this.A0H) {
                if (this.A00 == null && r4 != null && r4.A02 > SystemClock.elapsedRealtime()) {
                    A0D(r4);
                }
            }
        }
        C37691mq A08 = A08();
        if (A08 != null) {
            StringBuilder sb = new StringBuilder("routeselector/isroutinginfonulloralmostexpired/expiring at ");
            long j4 = A08.A04;
            sb.append(j4);
            sb.append(" (");
            sb.append(j4 - SystemClock.elapsedRealtime());
            sb.append("ms from now)");
            Log.i(sb.toString());
            if (j4 > SystemClock.elapsedRealtime() + 120000) {
                if (this.A04.hasMessages(0)) {
                    return false;
                }
                A0B();
                return false;
            }
        }
        A0A();
        return true;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        this.A01 = false;
    }

    @Override // X.AbstractC22060yS
    public void AMG() {
        this.A01 = true;
        if (this.A09.A07(149)) {
            A0C();
        }
    }
}
