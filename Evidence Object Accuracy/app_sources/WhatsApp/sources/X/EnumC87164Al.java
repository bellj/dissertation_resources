package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87164Al extends Enum {
    public static final /* synthetic */ EnumC87164Al[] A00;
    public static final EnumC87164Al A01;
    public final int value = 1;

    static {
        EnumC87164Al r1 = new EnumC87164Al();
        A01 = r1;
        A00 = new EnumC87164Al[]{r1};
    }

    public static EnumC87164Al valueOf(String str) {
        return (EnumC87164Al) Enum.valueOf(EnumC87164Al.class, str);
    }

    public static EnumC87164Al[] values() {
        return (EnumC87164Al[]) A00.clone();
    }
}
