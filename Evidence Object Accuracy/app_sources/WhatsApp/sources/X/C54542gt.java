package X;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.emoji.search.EmojiSearchContainer;

/* renamed from: X.2gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54542gt extends AnonymousClass02M implements AnonymousClass1KY {
    public AnonymousClass4VM A00;
    public final int A01;
    public final LayoutInflater A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass19M A04;
    public final AnonymousClass5UC A05;
    public final C16630pM A06;
    public final /* synthetic */ EmojiSearchContainer A07;

    public C54542gt(Activity activity, AnonymousClass018 r3, AnonymousClass19M r4, EmojiSearchContainer emojiSearchContainer, AnonymousClass5UC r6, C16630pM r7, int i) {
        this.A07 = emojiSearchContainer;
        this.A02 = activity.getLayoutInflater();
        this.A04 = r4;
        this.A03 = r3;
        this.A06 = r7;
        this.A05 = r6;
        this.A01 = i;
    }

    @Override // X.AnonymousClass02M
    public void A08(AnonymousClass03U r1) {
        ((C55192hw) r1).A08();
    }

    @Override // X.AnonymousClass02M
    public synchronized int A0D() {
        int i;
        AnonymousClass4VM r0 = this.A00;
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.A01.size();
        }
        return i;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r3, int i) {
        C37471mS r0;
        C55192hw r32 = (C55192hw) r3;
        synchronized (this) {
            if (i < this.A00.A01.size()) {
                r0 = (C37471mS) this.A00.A01.get(i);
            } else {
                r0 = null;
            }
            r32.A01 = r0;
            r32.A00 = i;
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C55192hw(this.A02, viewGroup, this.A04, this.A05, this.A06, this.A01);
    }

    @Override // X.AnonymousClass1KY
    public void AVQ(AnonymousClass4VM r5) {
        if (r5.equals(this.A00)) {
            A02();
        }
        EmojiSearchContainer emojiSearchContainer = this.A07;
        int i = 8;
        emojiSearchContainer.A01.setVisibility(8);
        View view = emojiSearchContainer.A02;
        if (emojiSearchContainer.A08.A0D() == 0) {
            i = 0;
        }
        view.setVisibility(i);
    }
}
