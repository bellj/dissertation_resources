package X;

import android.graphics.BitmapFactory;

/* renamed from: X.3kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75933kj extends AbstractC94334bd {
    public C75933kj(AnonymousClass0DQ r1, AnonymousClass5YZ r2, int i) {
        super(r1, r2, i);
    }

    @Override // X.AbstractC94334bd
    public int A00(BitmapFactory.Options options, int i, int i2) {
        return i * i2 * C94594c9.A00(options.inPreferredConfig);
    }
}
