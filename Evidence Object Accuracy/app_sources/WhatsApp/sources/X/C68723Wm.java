package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import com.whatsapp.emoji.EmojiDescriptor;

/* renamed from: X.3Wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68723Wm implements AbstractC39501q0 {
    public final Resources A00;
    public final Handler A01 = C12970iu.A0E();
    public final int[] A02;
    public final /* synthetic */ AnonymousClass1BR A03;

    @Override // X.AbstractC39501q0
    public void APk() {
    }

    public C68723Wm(Resources resources, AnonymousClass1BR r3, int[] iArr) {
        this.A03 = r3;
        this.A02 = iArr;
        this.A00 = resources;
    }

    @Override // X.AbstractC39501q0
    public /* bridge */ /* synthetic */ void AUc(Object obj) {
        C39511q1 r3 = new C39511q1(this.A02);
        long A00 = EmojiDescriptor.A00(r3, false);
        this.A01.post(new Runnable(this.A03.A0A.A03(this.A00, null, r3, A00), this, A00) { // from class: X.3l6
            public final /* synthetic */ long A00;
            public final /* synthetic */ Drawable A01;
            public final /* synthetic */ C68723Wm A02;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A00 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C68723Wm r0 = this.A02;
                Drawable drawable = this.A01;
                long j = this.A00;
                C74493i9 r32 = r0.A03.A04;
                if (r32 == null) {
                    return;
                }
                if (drawable != null) {
                    r32.A04(drawable, 0);
                    return;
                }
                int i = 2;
                if (!C12960it.A1S((j > -1 ? 1 : (j == -1 ? 0 : -1)))) {
                    i = 1;
                }
                r32.A04(null, i);
            }
        });
    }
}
