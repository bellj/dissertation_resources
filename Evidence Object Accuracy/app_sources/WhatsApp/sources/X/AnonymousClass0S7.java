package X;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;

/* renamed from: X.0S7  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0S7 {
    public final PointerIcon A00;

    public AnonymousClass0S7(PointerIcon pointerIcon) {
        this.A00 = pointerIcon;
    }

    public static AnonymousClass0S7 A00(Context context) {
        PointerIcon pointerIcon;
        if (Build.VERSION.SDK_INT >= 24) {
            pointerIcon = C04180Kr.A00(context, 1002);
        } else {
            pointerIcon = null;
        }
        return new AnonymousClass0S7(pointerIcon);
    }

    public Object A01() {
        return this.A00;
    }
}
