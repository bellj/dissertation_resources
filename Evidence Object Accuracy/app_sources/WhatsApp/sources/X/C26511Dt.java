package X;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;
import com.whatsapp.util.DocumentWarningDialogFragment;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* renamed from: X.1Dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26511Dt {
    public static final AnonymousClass00E A02 = new AnonymousClass00E(1, 1);
    public static final Object A03 = new Object();
    public static final Set A04 = new HashSet(Arrays.asList("wmv", "aif", "cda", "mpa", "opus", "ogg", "wlp", "amr", "mp3", "m4a", "aac", "wav", "wma"));
    public static final Set A05 = new HashSet(Arrays.asList("7z", "arj", "deb", "pkg", "rar", "rpm", "gz", "z", "zip"));
    public static final Set A06 = new HashSet(Arrays.asList("pdf", "doc", "docx", "ppt", "pptx", "xls", "xlsx", "txt", "rtf", "tex", "csv", "wpd"));
    public static final Set A07 = new HashSet(Arrays.asList("apk", "bat", "bin", "cgi", "pl", "com", "exe", "gadget", "jar", "msi", "py", "wsf"));
    public static final Set A08 = new HashSet(Arrays.asList("ai", "ico", "jpeg", "jpg", "png", "ps", "psd", "svg", "tif", "tiff"));
    public static final Set A09 = new HashSet(Arrays.asList("3g2", "3gp", "avi", "flv", "h264", "m4v", "mkv", "mov", "mp4", "mpg", "mpeg", "rm", "vob"));
    public final C14850m9 A00;
    public final C26521Du A01;

    public C26511Dt(C14850m9 r1, C26521Du r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static int A00(String str, ZipFile zipFile) {
        ZipEntry entry = zipFile.getEntry("docProps/app.xml");
        int i = 0;
        if (entry != null) {
            StringBuilder sb = new StringBuilder("<");
            sb.append(str);
            sb.append("[^>]*>(.*)</");
            sb.append(str);
            sb.append(">");
            Pattern compile = Pattern.compile(sb.toString(), 34);
            InputStream inputStream = zipFile.getInputStream(entry);
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, AnonymousClass01V.A08);
                StringWriter stringWriter = new StringWriter();
                char[] cArr = new char[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
                while (true) {
                    int read = inputStreamReader.read(cArr);
                    if (read == -1) {
                        break;
                    }
                    stringWriter.write(cArr, 0, read);
                }
                String obj = stringWriter.toString();
                inputStreamReader.close();
                Matcher matcher = compile.matcher(obj);
                if (matcher.find()) {
                    String group = matcher.group(1);
                    AnonymousClass009.A05(group);
                    try {
                        i = Integer.parseInt(group.trim());
                    } catch (NumberFormatException e) {
                        Log.i("documentutils/count ", e);
                    }
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
        return i;
    }

    public static Bitmap A01(String str, int i, int i2, boolean z) {
        ParcelFileDescriptor parcelFileDescriptor;
        PdfRenderer pdfRenderer;
        Matrix matrix;
        synchronized (A03) {
            Bitmap bitmap = null;
            try {
                parcelFileDescriptor = ParcelFileDescriptor.open(new File(str), 268435456);
            } catch (FileNotFoundException e) {
                Log.w(e);
                parcelFileDescriptor = null;
            }
            if (parcelFileDescriptor == null) {
                return null;
            }
            try {
                pdfRenderer = new PdfRenderer(parcelFileDescriptor);
            } catch (IOException | SecurityException e2) {
                Log.w(e2);
                pdfRenderer = null;
            }
            if (pdfRenderer != null) {
                if (pdfRenderer.getPageCount() > 0) {
                    PdfRenderer.Page openPage = pdfRenderer.openPage(0);
                    if (i <= 0 || i2 <= 0) {
                        int width = openPage.getWidth();
                        int height = openPage.getHeight();
                        if (width > height) {
                            i2 = (height * 480) / width;
                            i = 480;
                        } else {
                            i = (width * 480) / height;
                            i2 = 480;
                        }
                    }
                    Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
                    new Canvas(createBitmap).drawColor(-1);
                    if (z) {
                        int width2 = openPage.getWidth();
                        matrix = new Matrix();
                        float f = (((float) i) * 1.0f) / ((float) width2);
                        matrix.setScale(f, f);
                    } else {
                        matrix = null;
                    }
                    openPage.render(createBitmap, null, matrix, 1);
                    openPage.close();
                    bitmap = createBitmap;
                }
                pdfRenderer.close();
            }
            try {
                parcelFileDescriptor.close();
            } catch (IOException unused) {
            }
            return bitmap;
        }
    }

    public static Drawable A02(Context context, C16440p1 r4) {
        String A00 = AnonymousClass14P.A00(((AbstractC16130oV) r4).A06);
        Locale locale = Locale.US;
        String upperCase = A00.toUpperCase(locale);
        if (TextUtils.isEmpty(upperCase) && !TextUtils.isEmpty(r4.A16())) {
            upperCase = C14350lI.A07(r4.A16()).toUpperCase(locale);
        }
        return A03(context, ((AbstractC16130oV) r4).A06, upperCase, false);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        if (android.text.TextUtils.isEmpty(r6) == false) goto L_0x009b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable A03(android.content.Context r4, java.lang.String r5, java.lang.String r6, boolean r7) {
        /*
            if (r5 != 0) goto L_0x0023
            r3 = 2131232265(0x7f080609, float:1.8080634E38)
            if (r7 == 0) goto L_0x000a
            r3 = 2131232266(0x7f08060a, float:1.8080636E38)
        L_0x000a:
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x009b
            java.lang.String r2 = ""
        L_0x0012:
            android.content.res.Resources r1 = r4.getResources()
            r0 = 2131167011(0x7f070723, float:1.7948284E38)
            int r1 = r1.getDimensionPixelSize(r0)
            X.1oy r0 = new X.1oy
            r0.<init>(r4, r2, r3, r1)
            return r0
        L_0x0023:
            int r0 = r5.hashCode()
            switch(r0) {
                case -1248334925: goto L_0x008a;
                case -1248332507: goto L_0x0075;
                case -1073633483: goto L_0x0064;
                case -1071817359: goto L_0x0061;
                case -1050893613: goto L_0x005e;
                case -1004732798: goto L_0x005a;
                case -366307023: goto L_0x0049;
                case 904647503: goto L_0x0046;
                case 1993842850: goto L_0x0043;
                default: goto L_0x002a;
            }
        L_0x002a:
            r3 = 2131232265(0x7f080609, float:1.8080634E38)
            if (r7 == 0) goto L_0x0032
            r3 = 2131232266(0x7f08060a, float:1.8080636E38)
        L_0x0032:
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x009b
        L_0x0038:
            java.lang.String r1 = X.AnonymousClass14P.A00(r5)
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r2 = r1.toUpperCase(r0)
            goto L_0x0012
        L_0x0043:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            goto L_0x004b
        L_0x0046:
            java.lang.String r0 = "application/msword"
            goto L_0x0077
        L_0x0049:
            java.lang.String r0 = "application/vnd.ms-excel"
        L_0x004b:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r0 = 2131232267(0x7f08060b, float:1.8080638E38)
            if (r7 == 0) goto L_0x0085
            r0 = 2131232268(0x7f08060c, float:1.808064E38)
            goto L_0x0085
        L_0x005a:
            java.lang.String r0 = "text/rtf"
            goto L_0x0077
        L_0x005e:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            goto L_0x0077
        L_0x0061:
            java.lang.String r0 = "application/vnd.ms-powerpoint"
            goto L_0x0066
        L_0x0064:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        L_0x0066:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r0 = 2131232263(0x7f080607, float:1.808063E38)
            if (r7 == 0) goto L_0x0085
            r0 = 2131232264(0x7f080608, float:1.8080632E38)
            goto L_0x0085
        L_0x0075:
            java.lang.String r0 = "application/rtf"
        L_0x0077:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r0 = 2131232259(0x7f080603, float:1.8080622E38)
            if (r7 == 0) goto L_0x0085
            r0 = 2131232260(0x7f080604, float:1.8080624E38)
        L_0x0085:
            android.graphics.drawable.Drawable r0 = X.AnonymousClass00T.A04(r4, r0)
            return r0
        L_0x008a:
            java.lang.String r0 = "application/pdf"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r3 = 2131232261(0x7f080605, float:1.8080626E38)
            if (r7 == 0) goto L_0x0038
            r3 = 2131232262(0x7f080606, float:1.8080628E38)
            goto L_0x0038
        L_0x009b:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r2 = r6.toUpperCase(r0)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26511Dt.A03(android.content.Context, java.lang.String, java.lang.String, boolean):android.graphics.drawable.Drawable");
    }

    public static String A04(Uri uri, AnonymousClass01d r8) {
        File A032 = C14350lI.A03(uri);
        if (A032 != null) {
            return A032.getName();
        }
        String[] strArr = {"_display_name", "_size"};
        ContentResolver A0C = r8.A0C();
        if (A0C == null) {
            Log.w("document-utils/get-document-title cr=null");
            return null;
        }
        try {
            Cursor query = A0C.query(uri, strArr, null, null, null);
            if (query == null) {
                return null;
            }
            try {
                if (query.getColumnCount() <= 0 || !query.moveToFirst()) {
                    query.close();
                    return null;
                }
                String string = query.getString(0);
                query.close();
                return string;
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IllegalArgumentException | UnsupportedOperationException e) {
            Log.w("document-utils/get-document-title/unexpected exception", e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A05(X.AnonymousClass018 r7, X.C16440p1 r8) {
        /*
            java.lang.String r3 = r8.A06
            int r6 = r8.A00
            java.lang.String r2 = ""
            if (r3 == 0) goto L_0x0013
            if (r6 == 0) goto L_0x0013
            int r0 = r3.hashCode()
            r1 = 1
            r5 = 0
            switch(r0) {
                case -1248334925: goto L_0x0038;
                case -1073633483: goto L_0x002c;
                case -1071817359: goto L_0x0029;
                case -1050893613: goto L_0x0026;
                case -366307023: goto L_0x001a;
                case 904647503: goto L_0x0017;
                case 1993842850: goto L_0x0014;
                default: goto L_0x0013;
            }
        L_0x0013:
            return r2
        L_0x0014:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            goto L_0x001c
        L_0x0017:
            java.lang.String r0 = "application/msword"
            goto L_0x003a
        L_0x001a:
            java.lang.String r0 = "application/vnd.ms-excel"
        L_0x001c:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            r4 = 2131755210(0x7f1000ca, float:1.9141293E38)
            goto L_0x0043
        L_0x0026:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            goto L_0x003a
        L_0x0029:
            java.lang.String r0 = "application/vnd.ms-powerpoint"
            goto L_0x002e
        L_0x002c:
            java.lang.String r0 = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        L_0x002e:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            r4 = 2131755211(0x7f1000cb, float:1.9141295E38)
            goto L_0x0043
        L_0x0038:
            java.lang.String r0 = "application/pdf"
        L_0x003a:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            r4 = 2131755208(0x7f1000c8, float:1.9141289E38)
        L_0x0043:
            long r2 = (long) r6
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r1[r5] = r0
            java.lang.String r2 = r7.A0I(r1, r4, r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26511Dt.A05(X.018, X.0p1):java.lang.String");
    }

    public static void A06(AnonymousClass12P r4, AbstractC15710nm r5, ActivityC13810kN r6, C14900mE r7, C15670ni r8, C16440p1 r9, AnonymousClass1CY r10, AbstractC14440lR r11) {
        long j;
        int i;
        C16150oX r2 = ((AbstractC16130oV) r9).A02;
        AnonymousClass009.A05(r2);
        File file = r2.A0F;
        if (file == null || !file.exists()) {
            r10.A01(r6);
            return;
        }
        if (!r9.A0z.A02 && "apk".equalsIgnoreCase(C14350lI.A07(r2.A0F.getAbsolutePath()))) {
            j = r9.A11;
            i = R.string.warning_opening_apk;
        } else if (r2.A07 == 3) {
            j = r9.A11;
            i = R.string.warning_opening_document;
        } else {
            WeakReference weakReference = new WeakReference(r6);
            r7.A06(0, R.string.loading_spinner);
            C38961p1 r22 = new C38961p1(r4, r7, r9, weakReference);
            C38971p2 r1 = new C38971p2(r5, r8, r9);
            r1.A01(r22, r7.A06);
            r11.Ab2(r1);
            return;
        }
        DocumentWarningDialogFragment.A00(i, j).A1F(r6.A0V(), null);
    }

    public static boolean A07(String str) {
        return "image/jpeg".equals(str) || "image/jpg".equals(str) || "image/gif".equals(str) || "image/png".equals(str) || "image/webp".equals(str);
    }

    public static byte[] A08(Bitmap bitmap) {
        byte[] byteArray;
        int i = 80;
        do {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, i, byteArrayOutputStream);
            byteArray = byteArrayOutputStream.toByteArray();
            StringBuilder sb = new StringBuilder("documentutils/docthumb ");
            int length = byteArray.length;
            sb.append(length);
            sb.append(" ");
            sb.append(i);
            Log.i(sb.toString());
            i -= 5;
            if (length <= 20480) {
                break;
            }
        } while (i > 0);
        return byteArray;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if ("video/x.looping_mp4".equals(r16) != false) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] A09(java.io.File r15, java.lang.String r16) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26511Dt.A09(java.io.File, java.lang.String):byte[]");
    }
}
