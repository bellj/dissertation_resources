package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.widget.CompoundButton;

/* renamed from: X.0Qy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05780Qy {
    public static void A00(ColorStateList colorStateList, CompoundButton compoundButton) {
        compoundButton.setButtonTintList(colorStateList);
    }

    public static void A01(PorterDuff.Mode mode, CompoundButton compoundButton) {
        compoundButton.setButtonTintMode(mode);
    }
}
