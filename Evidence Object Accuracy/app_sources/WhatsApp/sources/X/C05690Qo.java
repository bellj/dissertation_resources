package X;

import android.view.View;
import android.view.WindowInsets;

/* renamed from: X.0Qo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05690Qo {
    public static C018408o A00(View view) {
        WindowInsets rootWindowInsets = view.getRootWindowInsets();
        if (rootWindowInsets == null) {
            return null;
        }
        C018408o A02 = C018408o.A02(rootWindowInsets);
        C06250St r1 = A02.A00;
        r1.A0D(A02);
        r1.A0B(view.getRootView());
        return A02;
    }

    public static void A01(View view, int i, int i2) {
        view.setScrollIndicators(i, i2);
    }
}
