package X;

import java.util.Iterator;
import java.util.Set;

/* renamed from: X.4Y9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Y9 {
    public C95534du A00 = new C95534du();

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass4Y9)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass4Y9) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return this.A00.toString();
    }

    public void A00(AnonymousClass5N1 r5) {
        try {
            C95534du r3 = this.A00;
            int i = r5.A00;
            if (i == 0) {
                Set<Object> set = r3.A04;
                AnonymousClass5MZ A00 = AnonymousClass5MZ.A00(r5.A01);
                if (!set.isEmpty()) {
                    for (Object obj : set) {
                        if (AnonymousClass5MZ.A00(obj).equals(A00)) {
                            throw new C87514Bu("OtherName is from an excluded subtree.");
                        }
                    }
                }
            } else if (i == 1) {
                Set set2 = r3.A02;
                String A002 = AnonymousClass5N1.A00(r5);
                if (!set2.isEmpty()) {
                    Iterator it = set2.iterator();
                    while (it.hasNext()) {
                        if (r3.A09(A002, C12970iu.A0x(it))) {
                            throw new C87514Bu("Email address is from an excluded subtree.");
                        }
                    }
                }
            } else if (i == 2) {
                Set set3 = r3.A01;
                String A003 = AnonymousClass5N1.A00(r5);
                if (!set3.isEmpty()) {
                    Iterator it2 = set3.iterator();
                    while (it2.hasNext()) {
                        String A0x = C12970iu.A0x(it2);
                        if (C95534du.A03(A003, A0x) || A003.equalsIgnoreCase(A0x)) {
                            throw new C87514Bu("DNS is from an excluded subtree.");
                        }
                    }
                }
            } else if (i == 4) {
                r3.A07(AnonymousClass5N2.A00(r5.A01));
            } else if (i == 6) {
                Set set4 = r3.A05;
                String A004 = AnonymousClass5N1.A00(r5);
                if (!set4.isEmpty()) {
                    Iterator it3 = set4.iterator();
                    while (it3.hasNext()) {
                        if (r3.A0A(A004, C12970iu.A0x(it3))) {
                            throw new C87514Bu("URI is from an excluded subtree.");
                        }
                    }
                }
            } else if (i == 7) {
                Set<byte[]> set5 = r3.A03;
                byte[] A05 = AnonymousClass5NH.A05(r5.A01);
                if (!set5.isEmpty()) {
                    for (byte[] bArr : set5) {
                        if (C95534du.A05(A05, bArr)) {
                            throw new C87514Bu("IP is from an excluded subtree.");
                        }
                    }
                }
            }
        } catch (C87514Bu e) {
            throw new AnonymousClass4C7(e.getMessage(), e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0096 A[Catch: 4Bu -> 0x011a, TryCatch #0 {4Bu -> 0x011a, blocks: (B:2:0x0000, B:14:0x0015, B:16:0x001f, B:17:0x0023, B:19:0x0029, B:22:0x0036, B:24:0x0039, B:27:0x0040, B:28:0x0047, B:29:0x0048, B:31:0x0050, B:32:0x0054, B:34:0x005a, B:37:0x0065, B:39:0x006b, B:42:0x0072, B:43:0x0079, B:44:0x007a, B:46:0x0084, B:48:0x008c, B:49:0x0090, B:51:0x0096, B:53:0x00a0, B:56:0x00a7, B:58:0x00ad, B:61:0x00b4, B:62:0x00bb, B:63:0x00bc, B:65:0x00c4, B:66:0x00c8, B:68:0x00ce, B:71:0x00d9, B:73:0x00df, B:76:0x00e6, B:77:0x00ed, B:78:0x00ee, B:80:0x00f8, B:81:0x00fc, B:83:0x0102, B:86:0x0111, B:87:0x0118), top: B:92:0x0000 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass5N1 r6) {
        /*
        // Method dump skipped, instructions count: 293
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4Y9.A01(X.5N1):void");
    }
}
