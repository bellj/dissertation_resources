package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.1wO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43091wO extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;

    public C43091wO() {
        super(2170, new AnonymousClass00E(1, SearchActionVerificationClientService.NOTIFICATION_ID, 20000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamAndroidMessageTargetPerf {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "durationReceiptT", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "targetStage", obj2);
        sb.append("}");
        return sb.toString();
    }
}
