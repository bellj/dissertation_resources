package X;

import java.io.File;
import java.util.concurrent.Callable;

/* renamed from: X.1ta  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41471ta {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final long A06;
    public final long A07;
    public final AbstractC14640lm A08;
    public final C14370lK A09;
    public final File A0A;
    public final File A0B;
    public final File A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final String A0K;
    public final Callable A0L;
    public final boolean A0M;
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    public final boolean A0Q;
    public final boolean A0R;
    public final boolean A0S;
    public final boolean A0T;
    public final boolean A0U;
    public final boolean A0V;
    public final boolean A0W;
    public final byte[] A0X;
    public final byte[] A0Y;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0081, code lost:
        if (r7 != X.C14370lK.A08) goto L_0x0083;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C41471ta(X.AbstractC14640lm r6, X.C14370lK r7, java.io.File r8, java.io.File r9, java.io.File r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.util.concurrent.Callable r19, byte[] r20, byte[] r21, int r22, int r23, int r24, int r25, int r26, int r27, long r28, long r30, boolean r32, boolean r33, boolean r34, boolean r35, boolean r36, boolean r37, boolean r38, boolean r39, boolean r40, boolean r41) {
        /*
            r5 = this;
            r5.<init>()
            r5.A09 = r7
            r0 = r22
            r5.A00 = r0
            r0 = r20
            r5.A0X = r0
            r5.A0C = r8
            r5.A0B = r10
            r5.A0A = r9
            r0 = r28
            r5.A07 = r0
            r2 = r30
            r5.A06 = r2
            r2 = r19
            r5.A0L = r2
            r5.A0J = r11
            r5.A0F = r12
            r5.A0I = r13
            r5.A0G = r14
            r5.A0E = r15
            r2 = r16
            r5.A0H = r2
            r2 = r17
            r5.A0D = r2
            r5.A08 = r6
            r2 = r23
            r5.A04 = r2
            r2 = r24
            r5.A01 = r2
            r2 = r25
            r5.A05 = r2
            r2 = r26
            r5.A02 = r2
            r2 = r32
            r5.A0U = r2
            r2 = r33
            r5.A0T = r2
            r2 = r34
            r5.A0P = r2
            r2 = r35
            r5.A0S = r2
            r2 = r36
            r5.A0Q = r2
            r2 = r37
            r5.A0R = r2
            r2 = r38
            r5.A0N = r2
            r2 = r39
            r5.A0O = r2
            r2 = r21
            r5.A0Y = r2
            r2 = r18
            r5.A0K = r2
            r2 = r40
            r5.A0W = r2
            r2 = r41
            r5.A0V = r2
            r2 = r27
            r5.A03 = r2
            r3 = 52428800(0x3200000, double:2.5903269E-316)
            int r2 = (r28 > r3 ? 1 : (r28 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x0083
            X.0lK r1 = X.C14370lK.A08
            r0 = 1
            if (r7 == r1) goto L_0x0084
        L_0x0083:
            r0 = 0
        L_0x0084:
            r5.A0M = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41471ta.<init>(X.0lm, X.0lK, java.io.File, java.io.File, java.io.File, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.concurrent.Callable, byte[], byte[], int, int, int, int, int, int, long, long, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean):void");
    }
}
