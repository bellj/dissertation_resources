package X;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.168  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass168 implements AbstractC14440lR {
    public static AbstractC15710nm A03;
    public static AnonymousClass1IA A04;
    public static final AnonymousClass1HB A05;
    public static final BlockingQueue A06;
    public static final Executor A07 = new Executor() { // from class: X.1I7
        @Override // java.util.concurrent.Executor
        public final void execute(Runnable runnable) {
            new AnonymousClass1MS(runnable, "AnomalyExecutorThread").start();
        }
    };
    public static final ThreadPoolExecutor A08;
    public Handler A00;
    public final Set A01 = new HashSet();
    public final Set A02 = new HashSet();

    static {
        BlockingQueue r4;
        if (Build.VERSION.SDK_INT >= 21) {
            r4 = new AnonymousClass1I6();
        } else {
            r4 = new AnonymousClass1IC();
        }
        A06 = r4;
        AnonymousClass1HB r3 = new AnonymousClass1HB();
        A05 = r3;
        TimeUnit timeUnit = TimeUnit.SECONDS;
        AnonymousClass1I9 r1 = new AnonymousClass1I9(r4, new AnonymousClass1I8("WhatsApp Worker", 10), timeUnit);
        A04 = r1;
        r1.setRejectedExecutionHandler(new AnonymousClass1IB());
        r3.A00(A04);
        A08 = new AnonymousClass1IA("High Pri Worker", new SynchronousQueue(), new AnonymousClass1I8("High Pri Worker", 0), timeUnit, 1, Integer.MAX_VALUE, 120);
    }

    @Override // X.AbstractC14440lR
    public ThreadPoolExecutor A8a(String str, BlockingQueue blockingQueue, int i, int i2, int i3, long j) {
        AnonymousClass1ID r1 = new AnonymousClass1ID(this, str, blockingQueue, new AnonymousClass1I8(str, i3), TimeUnit.SECONDS, i, i2, j, true);
        A05.A00(r1);
        return r1;
    }

    @Override // X.AbstractC14440lR
    public synchronized void AaP(Runnable runnable) {
        Handler handler = this.A00;
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override // X.AbstractC14440lR
    public final void Aaz(AbstractC16350or r3, Object... objArr) {
        r3.A02.executeOnExecutor(A04, objArr);
    }

    @Override // X.AbstractC14440lR
    public void Ab2(Runnable runnable) {
        A04.execute(runnable);
    }

    @Override // X.AbstractC14440lR
    public void Ab4(Runnable runnable, String str) {
        Set set = this.A02;
        synchronized (set) {
            if (set.add(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("WaWorkers/runIfNotRunning/");
                sb.append(str);
                Ab2(new AnonymousClass1IE(this, runnable, sb.toString(), str));
            }
        }
    }

    @Override // X.AbstractC14440lR
    public final void Ab5(AbstractC16350or r3, Object... objArr) {
        r3.A02.executeOnExecutor(A08, objArr);
    }

    @Override // X.AbstractC14440lR
    public void Ab6(Runnable runnable) {
        A08.execute(runnable);
    }

    @Override // X.AbstractC14440lR
    public synchronized Runnable AbK(Runnable runnable, String str, long j) {
        RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8;
        Handler handler = this.A00;
        if (handler == null) {
            HandlerThread handlerThread = new HandlerThread("WhatsApp Worker Scheduler", 10);
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper());
            this.A00 = handler;
        }
        runnableBRunnable0Shape8S0200000_I0_8 = new RunnableBRunnable0Shape8S0200000_I0_8(this, 36, runnable);
        handler.postDelayed(runnableBRunnable0Shape8S0200000_I0_8, j);
        return runnableBRunnable0Shape8S0200000_I0_8;
    }
}
