package X;

/* renamed from: X.276  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass276 implements AbstractC462925h {
    public int A00 = 0;

    @Override // X.AbstractC462925h
    public boolean Afl(boolean z, boolean z2, boolean z3, boolean z4) {
        int i = this.A00 * 53;
        int i2 = 1237;
        if (z2) {
            i2 = 1231;
        }
        this.A00 = i + i2;
        return z2;
    }

    @Override // X.AbstractC462925h
    public AbstractC27881Jp Afm(AbstractC27881Jp r3, AbstractC27881Jp r4, boolean z, boolean z2) {
        this.A00 = (this.A00 * 53) + r3.hashCode();
        return r3;
    }

    @Override // X.AbstractC462925h
    public double Afn(double d, double d2, boolean z, boolean z2) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        this.A00 = (this.A00 * 53) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)));
        return d;
    }

    @Override // X.AbstractC462925h
    public float Afo(float f, float f2, boolean z, boolean z2) {
        this.A00 = (this.A00 * 53) + Float.floatToIntBits(f);
        return f;
    }

    @Override // X.AbstractC462925h
    public int Afp(int i, int i2, boolean z, boolean z2) {
        this.A00 = (this.A00 * 53) + i;
        return i;
    }

    @Override // X.AbstractC462925h
    public AbstractC41941uP Afq(AbstractC41941uP r3, AbstractC41941uP r4) {
        this.A00 = (this.A00 * 53) + r3.hashCode();
        return r3;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1K6 Afr(AnonymousClass1K6 r3, AnonymousClass1K6 r4) {
        this.A00 = (this.A00 * 53) + r3.hashCode();
        return r3;
    }

    @Override // X.AbstractC462925h
    public long Afs(long j, long j2, boolean z, boolean z2) {
        this.A00 = (this.A00 * 53) + ((int) (j ^ (j >>> 32)));
        return j;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1G1 Aft(AnonymousClass1G1 r5, AnonymousClass1G1 r6) {
        int i;
        if (r5 == null) {
            i = 37;
        } else if (r5 instanceof AbstractC27091Fz) {
            AbstractC27091Fz r3 = (AbstractC27091Fz) r5;
            i = ((AnonymousClass1G0) r3).A00;
            if (i == 0) {
                int i2 = this.A00;
                this.A00 = 0;
                r3.A0Y(this, r3);
                i = this.A00;
                ((AnonymousClass1G0) r3).A00 = i;
                this.A00 = i2;
            }
        } else {
            i = r5.hashCode();
        }
        this.A00 = (this.A00 * 53) + i;
        return r5;
    }

    @Override // X.AbstractC462925h
    public Object Afu(Object obj, Object obj2, boolean z) {
        this.A00 = (this.A00 * 53) + obj.hashCode();
        return obj;
    }

    @Override // X.AbstractC462925h
    public Object Afv(Object obj, Object obj2, boolean z) {
        AnonymousClass1G1 r1 = (AnonymousClass1G1) obj;
        Aft(r1, (AnonymousClass1G1) obj2);
        return r1;
    }

    @Override // X.AbstractC462925h
    public void Afw(boolean z) {
        if (z) {
            throw new IllegalStateException();
        }
    }

    @Override // X.AbstractC462925h
    public Object Afx(Object obj, Object obj2, boolean z) {
        this.A00 = (this.A00 * 53) + obj.hashCode();
        return obj;
    }

    @Override // X.AbstractC462925h
    public String Afy(String str, String str2, boolean z, boolean z2) {
        this.A00 = (this.A00 * 53) + str.hashCode();
        return str;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass256 Afz(AnonymousClass256 r3, AnonymousClass256 r4) {
        this.A00 = (this.A00 * 53) + r3.hashCode();
        return r3;
    }
}
