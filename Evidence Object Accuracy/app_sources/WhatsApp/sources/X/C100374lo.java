package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100374lo implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1Z8(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1Z8[i];
    }
}
