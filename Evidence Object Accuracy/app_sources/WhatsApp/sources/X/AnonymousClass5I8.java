package X;

import java.util.AbstractSet;
import java.util.Collection;

/* renamed from: X.5I8  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5I8<E> extends AbstractSet<E> {
    public AnonymousClass5I8() {
    }

    public /* synthetic */ AnonymousClass5I8(AbstractC81283tl r2) {
        this();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final boolean add(Object obj) {
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final boolean addAll(Collection collection) {
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final void clear() {
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final boolean remove(Object obj) {
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractSet, java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final boolean removeAll(Collection collection) {
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    @Deprecated
    public final boolean retainAll(Collection collection) {
        throw C12970iu.A0z();
    }
}
