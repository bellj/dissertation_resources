package X;

import android.content.Context;

/* renamed from: X.5gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120795gl extends C120895gv {
    public final /* synthetic */ C120375g5 A00;
    public final /* synthetic */ C126135sO A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120795gl(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120375g5 r11, C126135sO r12) {
        super(context, r8, r9, r10, "upi-verify-qr-code");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        C118135bK r0 = this.A01.A00;
        if (r3 != null) {
            C127945vJ.A00(r0.A01, 4);
        } else {
            r0.A05();
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        C118135bK r0 = this.A01.A00;
        if (r3 != null) {
            C127945vJ.A00(r0.A01, 4);
        } else {
            r0.A05();
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r2) {
        super.A04(r2);
        this.A01.A00.A05();
    }
}
