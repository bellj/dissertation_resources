package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;

/* renamed from: X.01i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C003001i {
    public final AnonymousClass12I A00;
    public final C16490p7 A01;

    public C003001i(AnonymousClass12I r1, C16490p7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public int A00(String str) {
        try {
            C16310on A01 = this.A01.get();
            int i = 0;
            Cursor A09 = A01.A03.A09("SELECT ref_count FROM media_refs WHERE path = ?", new String[]{str});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    i = A09.getInt(A09.getColumnIndexOrThrow("ref_count"));
                }
                A09.close();
            }
            A01.close();
            return i;
        } catch (SQLiteDiskIOException e) {
            this.A00.A00(1);
            throw e;
        }
    }

    public int A01(String str, int i) {
        int i2;
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        C16310on A02 = this.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            int A002 = A00(str);
            if (A002 <= i) {
                A02.A03.A01("media_refs", "path = ?", new String[]{str});
            } else {
                AnonymousClass1YE A0A = A02.A02().A0A("UPDATE media_refs SET ref_count = ref_count + ? WHERE path = ?");
                A0A.A01(1, (long) (-i));
                A0A.A02(2, str);
                if (A0A.A00() == 0) {
                    i2 = -1;
                    A00.A00();
                    A00.close();
                    A02.close();
                    return i2;
                }
            }
            i2 = A002 - i;
            A00.A00();
            A00.close();
            A02.close();
            return i2;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(String str, int i) {
        if (i != 0) {
            boolean z = false;
            if (i > 0) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            C16310on A02 = this.A01.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                AnonymousClass1YE A0A = A02.A02().A0A("UPDATE media_refs SET ref_count = ref_count + ? WHERE path = ?");
                A0A.A01(1, (long) i);
                A0A.A02(2, str);
                if (A0A.A00() == 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("path", str);
                    contentValues.put("ref_count", Integer.valueOf(i));
                    A02.A03.A02(contentValues, "media_refs");
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
