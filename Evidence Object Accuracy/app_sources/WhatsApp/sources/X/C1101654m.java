package X;

import com.whatsapp.camera.CameraActivity;

/* renamed from: X.54m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1101654m implements AbstractC49682Lt {
    public final /* synthetic */ CameraActivity A00;

    public C1101654m(CameraActivity cameraActivity) {
        this.A00 = cameraActivity;
    }

    @Override // X.AbstractC49682Lt
    public int AEl() {
        return this.A00.getIntent().getIntExtra("origin", 1);
    }

    @Override // X.AbstractC49682Lt
    public void ANa() {
        this.A00.finish();
    }

    @Override // X.AbstractC49682Lt
    public void AVn() {
        CameraActivity cameraActivity = this.A00;
        cameraActivity.setResult(-1);
        cameraActivity.finish();
    }
}
