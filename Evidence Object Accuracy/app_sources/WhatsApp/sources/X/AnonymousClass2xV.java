package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S1100000_I1;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2xV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xV extends AbstractC47392An {
    public View A00;
    public C15370n3 A01;
    public UserJid A02;
    public UserJid A03;
    public final ViewGroup A04;
    public final C15570nT A05;
    public final C15550nR A06;
    public final C15610nY A07;
    public final AnonymousClass018 A08;
    public final AnonymousClass15L A09;
    public final UserJid A0A;
    public final C22230yk A0B;
    public final AbstractC14440lR A0C;
    public final boolean A0D;
    public final boolean A0E;

    public AnonymousClass2xV(ViewGroup viewGroup, Conversation conversation, C15570nT r4, C15550nR r5, C15610nY r6, AnonymousClass018 r7, AnonymousClass15L r8, C15370n3 r9, UserJid userJid, C22230yk r11, AbstractC14440lR r12, boolean z, boolean z2) {
        super(conversation, 30);
        this.A05 = r4;
        this.A0C = r12;
        this.A06 = r5;
        this.A07 = r6;
        this.A0B = r11;
        this.A08 = r7;
        this.A09 = r8;
        this.A0A = userJid;
        this.A04 = viewGroup;
        this.A0D = z;
        this.A0E = z2;
        this.A01 = r9;
    }

    public final void A05() {
        View view = this.A00;
        AnonymousClass009.A03(view);
        TextView A0J = C12960it.A0J(view, R.id.change_number_text);
        this.A05.A08();
        boolean equals = this.A0A.equals(this.A03);
        int i = R.string.change_number_notification_alert_new;
        if (equals) {
            i = R.string.change_number_notification_alert_old;
        }
        String A04 = this.A07.A04(C15550nR.A00(this.A06, this.A03));
        A0J.setText(C12960it.A0X(super.A01, A04, C12970iu.A1b(), 0, i));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape1S1100000_I1(2, A04, this));
        C12960it.A0y(this.A00.findViewById(R.id.change_number_dismiss), this, 31);
    }
}
