package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.2ld  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56762ld extends AnonymousClass2UH {
    public final /* synthetic */ int A00;
    public final /* synthetic */ GroupAdminPickerActivity A01;

    public C56762ld(GroupAdminPickerActivity groupAdminPickerActivity, int i) {
        this.A01 = groupAdminPickerActivity;
        this.A00 = i;
    }

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
        int i = ((int) (f * 127.0f)) << 24;
        GroupAdminPickerActivity groupAdminPickerActivity = this.A01;
        groupAdminPickerActivity.A00.setColor(i);
        if (Build.VERSION.SDK_INT >= 21) {
            groupAdminPickerActivity.getWindow().setStatusBarColor(C016907y.A03(1.0f, this.A00, i));
        }
    }
}
