package X;

import androidx.recyclerview.widget.GridLayoutManager;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;

/* renamed from: X.3iY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74713iY extends AbstractC05290Oz {
    public final /* synthetic */ GridLayoutManager A00;
    public final /* synthetic */ AnonymousClass1KX A01;
    public final /* synthetic */ ShapePickerRecyclerView A02;

    public C74713iY(GridLayoutManager gridLayoutManager, AnonymousClass1KX r2, ShapePickerRecyclerView shapePickerRecyclerView) {
        this.A02 = shapePickerRecyclerView;
        this.A01 = r2;
        this.A00 = gridLayoutManager;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        int i2 = this.A01.A0E(i).A00;
        if (i2 == 0) {
            return this.A02.A01;
        }
        if (i2 == 1) {
            return this.A00.A00;
        }
        throw C12960it.A0U("shapepickerrecyclerview/invalid grid size");
    }
}
