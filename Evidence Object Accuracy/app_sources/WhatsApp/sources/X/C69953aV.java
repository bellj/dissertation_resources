package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

/* renamed from: X.3aV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69953aV implements AbstractC116265Ut {
    @Override // X.AbstractC116265Ut
    public Layout A8M(TextPaint textPaint, CharSequence charSequence, int i) {
        return StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), textPaint, i).setAlignment(Layout.Alignment.ALIGN_CENTER).setBreakStrategy(1).build();
    }
}
