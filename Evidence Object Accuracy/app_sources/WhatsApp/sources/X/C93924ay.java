package X;

import android.os.SystemClock;

/* renamed from: X.4ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93924ay {
    public long A00;
    public final AbstractC115095Qe A01;

    public C93924ay(AbstractC115095Qe r1) {
        C13020j0.A01(r1);
        this.A01 = r1;
    }

    public C93924ay(AbstractC115095Qe r1, long j) {
        C13020j0.A01(r1);
        this.A01 = r1;
        this.A00 = j;
    }

    public final boolean A00(long j) {
        long j2 = this.A00;
        if (j2 == 0 || SystemClock.elapsedRealtime() - j2 > j) {
            return true;
        }
        return false;
    }
}
