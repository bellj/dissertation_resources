package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.RectF;
import androidx.cardview.widget.CardView;

/* renamed from: X.0Xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07190Xa implements AbstractC12750iR {
    public final RectF A00 = new RectF();

    @Override // X.AbstractC12750iR
    public void AOK(AbstractC11670gf r1) {
    }

    public void A00(AbstractC11670gf r9) {
        Rect rect = new Rect();
        AnonymousClass0XY r5 = (AnonymousClass0XY) r9;
        ((AnonymousClass0A6) r5.A00).getPadding(rect);
        int ceil = (int) Math.ceil((double) AEW(r9));
        int ceil2 = (int) Math.ceil((double) AEU(r9));
        CardView cardView = r5.A01;
        if (ceil > cardView.A01) {
            CardView.A00(cardView, ceil);
        }
        if (ceil2 > cardView.A00) {
            CardView.A01(cardView, ceil2);
        }
        int i = rect.left;
        int i2 = rect.top;
        int i3 = rect.right;
        int i4 = rect.bottom;
        cardView.A05.set(i, i2, i3, i4);
        Rect rect2 = cardView.A04;
        CardView.A02(cardView, i + rect2.left, i2 + rect2.top, i3 + rect2.right, i4 + rect2.bottom);
    }

    @Override // X.AbstractC12750iR
    public ColorStateList AAn(AbstractC11670gf r2) {
        return ((AnonymousClass0A6) ((AnonymousClass0XY) r2).A00).A04;
    }

    @Override // X.AbstractC12750iR
    public float ACg(AbstractC11670gf r2) {
        return ((AnonymousClass0A6) ((AnonymousClass0XY) r2).A00).A02;
    }

    @Override // X.AbstractC12750iR
    public float AE8(AbstractC11670gf r2) {
        return ((AnonymousClass0A6) ((AnonymousClass0XY) r2).A00).A01;
    }

    @Override // X.AbstractC12750iR
    public float AEU(AbstractC11670gf r7) {
        AnonymousClass0A6 r0 = (AnonymousClass0A6) ((AnonymousClass0XY) r7).A00;
        float f = r0.A01;
        float f2 = r0.A00;
        float f3 = (float) r0.A0C;
        float f4 = f * 1.5f;
        return (Math.max(f, f2 + f3 + (f4 / 2.0f)) * 2.0f) + ((f4 + f3) * 2.0f);
    }

    @Override // X.AbstractC12750iR
    public float AEW(AbstractC11670gf r6) {
        AnonymousClass0A6 r0 = (AnonymousClass0A6) ((AnonymousClass0XY) r6).A00;
        float f = r0.A01;
        float f2 = r0.A00;
        float f3 = (float) r0.A0C;
        return (Math.max(f, f2 + f3 + (f / 2.0f)) * 2.0f) + ((f + f3) * 2.0f);
    }

    @Override // X.AbstractC12750iR
    public float AG3(AbstractC11670gf r2) {
        return ((AnonymousClass0A6) ((AnonymousClass0XY) r2).A00).A00;
    }

    @Override // X.AbstractC12750iR
    public void AIq() {
        AnonymousClass0A6.A0G = new C07210Xc(this);
    }

    @Override // X.AbstractC12750iR
    public void AIw(Context context, ColorStateList colorStateList, AbstractC11670gf r12, float f, float f2, float f3) {
        AnonymousClass0A6 r3 = new AnonymousClass0A6(colorStateList, context.getResources(), f, f2, f3);
        AnonymousClass0XY r2 = (AnonymousClass0XY) r12;
        CardView cardView = r2.A01;
        r3.A09 = cardView.A03;
        r3.invalidateSelf();
        r2.A00 = r3;
        cardView.setBackgroundDrawable(r3);
        A00(r12);
    }

    @Override // X.AbstractC12750iR
    public void AUD(AbstractC11670gf r3) {
        AnonymousClass0XY r0 = (AnonymousClass0XY) r3;
        AnonymousClass0A6 r1 = (AnonymousClass0A6) r0.A00;
        r1.A09 = r0.A01.A03;
        r1.invalidateSelf();
        A00(r3);
    }

    @Override // X.AbstractC12750iR
    public void Abm(ColorStateList colorStateList, AbstractC11670gf r6) {
        AnonymousClass0A6 r3 = (AnonymousClass0A6) ((AnonymousClass0XY) r6).A00;
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        r3.A04 = colorStateList;
        r3.A07.setColor(colorStateList.getColorForState(r3.getState(), r3.A04.getDefaultColor()));
        r3.invalidateSelf();
    }

    @Override // X.AbstractC12750iR
    public void Ac6(AbstractC11670gf r3, float f) {
        AnonymousClass0A6 r1 = (AnonymousClass0A6) ((AnonymousClass0XY) r3).A00;
        r1.A01(f, r1.A01);
    }

    @Override // X.AbstractC12750iR
    public void AcJ(AbstractC11670gf r3, float f) {
        AnonymousClass0A6 r1 = (AnonymousClass0A6) ((AnonymousClass0XY) r3).A00;
        r1.A01(r1.A02, f);
        A00(r3);
    }

    @Override // X.AbstractC12750iR
    public void Aci(AbstractC11670gf r4, float f) {
        AnonymousClass0A6 r2 = (AnonymousClass0A6) ((AnonymousClass0XY) r4).A00;
        if (f >= 0.0f) {
            float f2 = (float) ((int) (f + 0.5f));
            if (r2.A00 != f2) {
                r2.A00 = f2;
                r2.A0A = true;
                r2.invalidateSelf();
            }
            A00(r4);
            return;
        }
        StringBuilder sb = new StringBuilder("Invalid radius ");
        sb.append(f);
        sb.append(". Must be >= 0");
        throw new IllegalArgumentException(sb.toString());
    }
}
