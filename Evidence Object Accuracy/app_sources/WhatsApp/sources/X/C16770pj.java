package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0pj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16770pj extends C16780pk {
    public static final Collection A0F(Object[] objArr) {
        return new C71213cZ(objArr, false);
    }

    public static final List A0G() {
        return AnonymousClass1WF.A00;
    }

    public static final List A0H(List list) {
        int size = list.size();
        if (size == 0) {
            return AnonymousClass1WF.A00;
        }
        if (size != 1) {
            return list;
        }
        List singletonList = Collections.singletonList(list.get(0));
        C16700pc.A0B(singletonList);
        return singletonList;
    }

    public static final List A0I(Object... objArr) {
        if (objArr.length > 0) {
            return C10980fW.A04(objArr);
        }
        return AnonymousClass1WF.A00;
    }

    public static final List A0J(Object... objArr) {
        C16700pc.A0E(objArr, 0);
        return objArr.length == 0 ? new ArrayList() : new ArrayList(new C71213cZ(objArr, true));
    }
}
