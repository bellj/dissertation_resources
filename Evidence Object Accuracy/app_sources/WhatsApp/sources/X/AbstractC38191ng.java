package X;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.whatsapp.R;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1ng  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC38191ng {
    public final int A00;
    public final AnonymousClass12P A01;
    public final C21740xu A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C14830m7 A05;
    public final AnonymousClass018 A06;
    public final C14850m9 A07;
    public final AnonymousClass14X A08;

    public abstract int A00();

    public abstract int A01();

    public abstract int A02();

    public abstract int A03();

    public abstract int A04();

    public abstract int A05();

    public abstract int A06();

    public abstract String A09();

    public AbstractC38191ng(AnonymousClass12P r1, C21740xu r2, C15550nR r3, C15610nY r4, C14830m7 r5, AnonymousClass018 r6, C14850m9 r7, AnonymousClass14X r8, int i) {
        this.A05 = r5;
        this.A07 = r7;
        this.A02 = r2;
        this.A01 = r1;
        this.A08 = r8;
        this.A04 = r4;
        this.A06 = r6;
        this.A03 = r3;
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003b, code lost:
        if (android.text.TextUtils.equals(r3, (java.lang.String) r0.A00.get(java.lang.Integer.valueOf(r2))) != false) goto L_0x003d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A07(X.AnonymousClass1ZO r8, com.whatsapp.jid.UserJid r9, X.C50942Ry r10) {
        /*
            r7 = this;
            X.0nR r0 = r7.A03
            X.0n3 r0 = r0.A0B(r9)
            boolean r0 = r0.A0I()
            if (r0 == 0) goto L_0x0065
            X.2SA r6 = r10.A0A
            boolean r0 = r6.A01
            r5 = 3
            r4 = 1
            if (r0 == 0) goto L_0x003d
            if (r8 == 0) goto L_0x003e
            X.2Rz r0 = r10.A08
            long r0 = r0.A01
            java.lang.String r3 = java.lang.String.valueOf(r0)
            int r2 = r7.A00
            X.1Zh r0 = r8.A03
            if (r0 != 0) goto L_0x002b
            X.1Zh r0 = new X.1Zh
            r0.<init>()
            r8.A03 = r0
        L_0x002b:
            java.util.Map r1 = r0.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = android.text.TextUtils.equals(r3, r0)
            if (r0 == 0) goto L_0x003e
        L_0x003d:
            r5 = 1
        L_0x003e:
            int r3 = r6.A00
            if (r5 != r4) goto L_0x0064
            if (r3 <= 0) goto L_0x0064
            if (r8 == 0) goto L_0x0064
            X.1Zo r2 = r8.A04
            if (r2 == 0) goto L_0x0064
            X.2Rz r0 = r10.A08
            long r0 = r0.A01
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.util.HashMap r0 = r2.A00
            java.lang.Object r0 = r0.get(r1)
            java.util.AbstractCollection r0 = (java.util.AbstractCollection) r0
            if (r0 == 0) goto L_0x0064
            int r0 = r0.size()
            if (r0 < r3) goto L_0x0063
            r4 = 2
        L_0x0063:
            return r4
        L_0x0064:
            return r5
        L_0x0065:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC38191ng.A07(X.1ZO, com.whatsapp.jid.UserJid, X.2Ry):int");
    }

    public AnonymousClass04S A08(Context context) {
        C004802e r2 = new C004802e(context);
        r2.A07(R.string.incentive_app_update_dialog_title);
        r2.A06(R.string.incentive_app_update_dialog_description);
        r2.setNegativeButton(R.string.not_now, null);
        r2.setPositiveButton(R.string.upgrade, new DialogInterface.OnClickListener(context, this) { // from class: X.4gg
            public final /* synthetic */ Context A00;
            public final /* synthetic */ AbstractC38191ng A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AbstractC38191ng r4 = this.A01;
                r4.A01.A06(this.A00, new Intent("android.intent.action.VIEW", r4.A02.A01()));
            }
        });
        return r2.create();
    }

    public boolean A0A(C50942Ry r7, C50932Rx r8) {
        if (r7 == null || r8 == null || r7.A08.A01 != r8.A03) {
            return true;
        }
        int A02 = this.A07.A02(988);
        return A02 >= 1 && Math.abs(this.A05.A00() - r8.A02) >= TimeUnit.HOURS.toMillis((long) A02);
    }
}
