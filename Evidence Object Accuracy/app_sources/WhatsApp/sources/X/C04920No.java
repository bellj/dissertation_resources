package X;

import android.net.Uri;

/* renamed from: X.0No  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04920No {
    public final int A00;
    public final int A01;
    public final int A02;
    public final Uri A03;
    public final boolean A04;

    @Deprecated
    public C04920No(Uri uri, int i, int i2, int i3, boolean z) {
        this.A03 = uri;
        this.A01 = i;
        this.A02 = i2;
        this.A04 = z;
        this.A00 = i3;
    }
}
