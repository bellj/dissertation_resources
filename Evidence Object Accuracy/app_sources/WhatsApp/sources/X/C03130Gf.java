package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0Gf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03130Gf extends AnonymousClass0Zt {
    public static final String A00 = C06390Tk.A01("NetworkNotRoamingCtrlr");

    public C03130Gf(Context context, AbstractC11500gO r3) {
        super(C05990Rt.A00(context, r3).A02);
    }

    @Override // X.AnonymousClass0Zt
    public boolean A01(C004401z r4) {
        return r4.A09.A03 == EnumC004001t.NOT_ROAMING;
    }

    @Override // X.AnonymousClass0Zt
    public boolean A02(Object obj) {
        C05410Pl r6 = (C05410Pl) obj;
        if (Build.VERSION.SDK_INT < 24) {
            C06390Tk.A00().A02(A00, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
            return !r6.A00;
        } else if (!r6.A00 || !r6.A02) {
            return true;
        } else {
            return false;
        }
    }
}
