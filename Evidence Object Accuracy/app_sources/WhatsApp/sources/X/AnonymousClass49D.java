package X;

import java.io.InputStream;

/* renamed from: X.49D  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49D extends InputStream {
    public final /* synthetic */ AnonymousClass5FW A00;

    public AnonymousClass49D(AnonymousClass5FW r1) {
        this.A00 = r1;
    }

    @Override // java.io.InputStream
    public int available() {
        AnonymousClass5FW r1 = this.A00;
        if (!r1.A00) {
            return (int) Math.min(r1.A01.A00, (long) Integer.MAX_VALUE);
        }
        throw C12990iw.A0i("closed");
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.close();
    }

    @Override // java.io.InputStream
    public int read() {
        AnonymousClass5FW r6 = this.A00;
        if (!r6.A00) {
            C10730f6 r5 = r6.A01;
            if (r5.A00 == 0 && AnonymousClass5FW.A00(r5, r6) == -1) {
                return -1;
            }
            return r5.readByte() & 255;
        }
        throw C12990iw.A0i("closed");
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        C16700pc.A0D(bArr, 0);
        AnonymousClass5FW r5 = this.A00;
        if (!r5.A00) {
            AnonymousClass4F1.A00((long) bArr.length, (long) i, (long) i2);
            C10730f6 r6 = r5.A01;
            if (r6.A00 == 0 && AnonymousClass5FW.A00(r6, r5) == -1) {
                return -1;
            }
            return r6.A02(bArr, i, i2);
        }
        throw C12990iw.A0i("closed");
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A00);
        return C12960it.A0d(".inputStream()", A0h);
    }
}
