package X;

/* renamed from: X.1Mu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28341Mu<E> extends AbstractC17940re<E> {
    public final transient Object element;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return false;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return 1;
    }

    public C28341Mu(Object obj) {
        this.element = obj;
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf
    public AnonymousClass1Mr asList() {
        return AnonymousClass1Mr.of(this.element);
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return this.element.equals(obj);
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        objArr[i] = this.element;
        return i + 1;
    }

    @Override // X.AbstractC17940re, java.util.Collection, java.lang.Object, java.util.Set
    public final int hashCode() {
        return this.element.hashCode();
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return AnonymousClass1I4.singletonIterator(this.element);
    }

    @Override // java.util.AbstractCollection, java.lang.Object
    public String toString() {
        String obj = this.element.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }
}
