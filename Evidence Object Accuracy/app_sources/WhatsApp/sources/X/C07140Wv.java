package X;

import android.view.View;
import android.widget.AdapterView;
import androidx.appcompat.widget.AppCompatSpinner;

/* renamed from: X.0Wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07140Wv implements AdapterView.OnItemClickListener {
    public final /* synthetic */ C02400Cd A00;
    public final /* synthetic */ AppCompatSpinner A01;

    public C07140Wv(C02400Cd r1, AppCompatSpinner appCompatSpinner) {
        this.A00 = r1;
        this.A01 = appCompatSpinner;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        C02400Cd r3 = this.A00;
        AppCompatSpinner appCompatSpinner = r3.A04;
        appCompatSpinner.setSelection(i);
        if (appCompatSpinner.getOnItemClickListener() != null) {
            appCompatSpinner.performItemClick(view, i, r3.A01.getItemId(i));
        }
        r3.dismiss();
    }
}
