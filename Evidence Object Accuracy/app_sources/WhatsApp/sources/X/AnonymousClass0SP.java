package X;

/* renamed from: X.0SP  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0SP {
    public abstract AnonymousClass0SP A01(String str, AnonymousClass1J7 v);

    public abstract Object A02();

    public static final String A00(String str, Object obj) {
        C16700pc.A0E(obj, 0);
        C16700pc.A0E(str, 1);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" value: ");
        sb.append(obj);
        return sb.toString();
    }
}
