package X;

import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2hM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54832hM extends AbstractC05270Ox {
    public final /* synthetic */ AnonymousClass242 A00;

    public C54832hM(AnonymousClass242 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        A02(recyclerView);
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        A02(recyclerView);
    }

    public final void A02(RecyclerView recyclerView) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass242 r5 = this.A00;
            if (r5.A04 != null) {
                float f = (C12960it.A09(recyclerView).getDisplayMetrics().density * 0.8f) + 0.5f;
                float min = Math.min(((float) recyclerView.computeVerticalScrollOffset()) / (((float) C12960it.A09(recyclerView).getDimensionPixelSize(R.dimen.emoji_picker_item)) / 3.0f), 1.0f);
                if (min >= 0.0f && min <= 1.0f) {
                    f *= min;
                }
                r5.A04.AHZ().setBackgroundColor(C016907y.A05(C016907y.A06(r5.A06, (int) (min * 13.0f)), r5.A05));
                AnonymousClass028.A0V(r5.A04.AHZ(), f);
            }
        }
    }
}
