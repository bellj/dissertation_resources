package X;

import android.view.WindowInsets;

/* renamed from: X.0Dn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02680Dn extends C02690Do {
    public AnonymousClass0U7 A00 = null;
    public AnonymousClass0U7 A01 = null;
    public AnonymousClass0U7 A02 = null;

    @Override // X.C02700Dp, X.C06250St
    public void A0C(AnonymousClass0U7 r1) {
    }

    public C02680Dn(C018408o r2, WindowInsets windowInsets) {
        super(r2, windowInsets);
    }

    @Override // X.C06250St
    public AnonymousClass0U7 A00() {
        AnonymousClass0U7 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0U7 A01 = AnonymousClass0U7.A01(this.A03.getMandatorySystemGestureInsets());
        this.A00 = A01;
        return A01;
    }

    @Override // X.C06250St
    public AnonymousClass0U7 A02() {
        AnonymousClass0U7 r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0U7 A01 = AnonymousClass0U7.A01(this.A03.getSystemGestureInsets());
        this.A01 = A01;
        return A01;
    }

    @Override // X.C06250St
    public AnonymousClass0U7 A04() {
        AnonymousClass0U7 r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0U7 A01 = AnonymousClass0U7.A01(this.A03.getTappableElementInsets());
        this.A02 = A01;
        return A01;
    }

    @Override // X.C02710Dq, X.C06250St
    public C018408o A0A(int i, int i2, int i3, int i4) {
        return C018408o.A02(this.A03.inset(i, i2, i3, i4));
    }
}
