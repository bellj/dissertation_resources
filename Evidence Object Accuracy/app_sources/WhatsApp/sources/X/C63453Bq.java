package X;

import android.app.ProgressDialog;
import com.whatsapp.R;
import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;
import com.whatsapp.deviceauth.BiometricAuthPlugin;

/* renamed from: X.3Bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63453Bq {
    public ProgressDialog A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final ActivityC13790kL A03;
    public final C54402gf A04;
    public final LinkedDevicesSharedViewModel A05;
    public final AnonymousClass01d A06;
    public final C245716a A07;
    public final BiometricAuthPlugin A08;
    public final C14850m9 A09;
    public final AnonymousClass12U A0A;

    public C63453Bq(AbstractC15710nm r11, C14900mE r12, ActivityC13790kL r13, C54402gf r14, AnonymousClass01d r15, C245716a r16, C14850m9 r17, AnonymousClass12U r18) {
        this.A09 = r17;
        this.A02 = r12;
        this.A01 = r11;
        this.A0A = r18;
        this.A06 = r15;
        this.A07 = r16;
        this.A03 = r13;
        this.A05 = (LinkedDevicesSharedViewModel) C13000ix.A02(r13).A00(LinkedDevicesSharedViewModel.class);
        this.A08 = new BiometricAuthPlugin(r13, r11, r12, r15, new AnonymousClass5U7() { // from class: X.56O
            @Override // X.AnonymousClass5U7
            public final void AMd(int i) {
                LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = C63453Bq.this.A05;
                if (i == -1 || i == 4) {
                    linkedDevicesSharedViewModel.A0Q.A0B(null);
                }
            }
        }, r17, R.string.linked_device_unlock_to_link, 0);
        this.A04 = r14;
    }
}
