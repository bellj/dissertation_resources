package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/* renamed from: X.5pK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124395pK extends AbstractC16850pr {
    public C17170qN A00;
    public AnonymousClass018 A01;
    public C17120qI A02;
    public final AnonymousClass17J A03;
    public final C19550uI A04;
    public final AbstractC14440lR A05;
    public final C18840t8 A06;
    public final C126165sR A07;
    public final C129035x4 A08;

    public C124395pK(AnonymousClass17J r1, C19550uI r2, AbstractC14440lR r3, C18840t8 r4, C126165sR r5, C129035x4 r6, Set set) {
        super(set);
        this.A06 = r4;
        this.A05 = r3;
        this.A03 = r1;
        this.A04 = r2;
        this.A07 = r5;
        this.A08 = r6;
    }

    public static /* synthetic */ void A00(C124395pK r4, int i) {
        HashMap A11 = C12970iu.A11();
        A11.put("error_code", "-1");
        C130355zH.A01(r4.A02, C130355zH.A00(Integer.valueOf(i)), "prefetchResponse", A11);
    }

    @Override // X.AbstractC16850pr
    public void A01(C91064Qh r3, AnonymousClass5WN r4, String str) {
        if (r3.A00 != 5) {
            r4.AVH(r3);
            return;
        }
        throw C12960it.A0U("Successful responses should not be processed as errors");
    }

    @Override // X.AbstractC16850pr
    public void A02(C65963Lt r23, AnonymousClass5WN r24, Boolean bool, String str, String str2, String str3, boolean z) {
        AnonymousClass009.A05(str);
        C91064Qh r5 = new C91064Qh();
        Iterator A0n = C12960it.A0n(this.A07.A00);
        while (true) {
            if (!A0n.hasNext()) {
                break;
            }
            Map.Entry A15 = C12970iu.A15(A0n);
            if (((Pattern) A15.getKey()).matcher(str).matches() && ((C127185u5) A15.getValue()).A01.AJM(str, str2)) {
                AnonymousClass6MD r1 = ((C127185u5) A15.getValue()).A00;
                if (r1 != null) {
                    this.A05.Ab2(new Runnable(r5, r23, r24, this, r1, str, str2, z) { // from class: X.6KA
                        public final /* synthetic */ C91064Qh A00;
                        public final /* synthetic */ C65963Lt A01;
                        public final /* synthetic */ AnonymousClass5WN A02;
                        public final /* synthetic */ C124395pK A03;
                        public final /* synthetic */ AnonymousClass6MD A04;
                        public final /* synthetic */ String A05;
                        public final /* synthetic */ String A06;
                        public final /* synthetic */ boolean A07;

                        {
                            this.A03 = r4;
                            this.A04 = r5;
                            this.A05 = r6;
                            this.A06 = r7;
                            this.A01 = r2;
                            this.A00 = r1;
                            this.A07 = r8;
                            this.A02 = r3;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            C124395pK r8 = this.A03;
                            AnonymousClass6MD r7 = this.A04;
                            String str4 = this.A05;
                            String str5 = this.A06;
                            r7.AZP(this.A01, new C129625y2(this.A00, this.A02, r8, this.A07), str4, str5);
                        }
                    });
                    return;
                }
            }
        }
        A03(new AnonymousClass4VZ(), r5, r23, r24, bool, str, str2, str3, z);
    }

    public final void A03(AnonymousClass4VZ r21, C91064Qh r22, C65963Lt r23, AnonymousClass5WN r24, Boolean bool, String str, String str2, String str3, boolean z) {
        HashMap hashMap;
        AnonymousClass19W r4;
        C129035x4 r2 = this.A08;
        C127535ue A00 = r2.A00(str);
        C125925s2 r0 = A00.A00;
        if (r0 != null) {
            hashMap = C12970iu.A11();
            hashMap.put("X_NOVI_COMPOSITE_HEADER", r0.A00.A00());
            hashMap.put("X_NOVI_CLIENT_REQUEST_ID", C12990iw.A0n());
        } else {
            hashMap = null;
        }
        C127195u6 r1 = A00.A02;
        C16820po r3 = r1.A01;
        if (r3 == null) {
            AbstractC126175sS r02 = A00.A01;
            r4 = r02.A00.A8U(null, str, str2, hashMap, r1.A00);
        } else {
            AnonymousClass178 r03 = r2.A00;
            AnonymousClass68S r25 = new AnonymousClass5UI(str, str2, hashMap) { // from class: X.68S
                public final /* synthetic */ String A01;
                public final /* synthetic */ String A02;
                public final /* synthetic */ Map A03;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A03 = r4;
                }

                @Override // X.AnonymousClass5UI
                public final AnonymousClass19W A7x(String str4) {
                    C127535ue r04 = C127535ue.this;
                    String str5 = this.A01;
                    String str6 = this.A02;
                    Map map = this.A03;
                    AbstractC126175sS r12 = r04.A01;
                    return r12.A00.A8U(str4, str5, str6, map, r04.A02.A00);
                }
            };
            AnonymousClass01J r04 = r03.A00.A01;
            r4 = new C68883Xc(C12980iv.A0b(r04), r04.A3K(), r3, r25);
        }
        this.A05.Ab2(new Runnable(r21, r4, r22, r23, r24, this, bool, str, str3, str2, r4.hashCode(), z) { // from class: X.6KC
            public final /* synthetic */ int A00;
            public final /* synthetic */ AnonymousClass4VZ A01;
            public final /* synthetic */ AnonymousClass19W A02;
            public final /* synthetic */ C91064Qh A03;
            public final /* synthetic */ C65963Lt A04;
            public final /* synthetic */ AnonymousClass5WN A05;
            public final /* synthetic */ C124395pK A06;
            public final /* synthetic */ Boolean A07;
            public final /* synthetic */ String A08;
            public final /* synthetic */ String A09;
            public final /* synthetic */ String A0A;
            public final /* synthetic */ boolean A0B;

            {
                this.A06 = r6;
                this.A04 = r4;
                this.A09 = r8;
                this.A07 = r7;
                this.A0A = r9;
                this.A03 = r3;
                this.A0B = r12;
                this.A05 = r5;
                this.A00 = r11;
                this.A02 = r2;
                this.A01 = r1;
                this.A08 = r10;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C124395pK r9 = this.A06;
                C65963Lt r7 = this.A04;
                String str4 = this.A09;
                Boolean bool2 = this.A07;
                String str5 = this.A0A;
                C91064Qh r6 = this.A03;
                boolean z2 = this.A0B;
                AnonymousClass5WN r8 = this.A05;
                int i = this.A00;
                AnonymousClass19W r32 = this.A02;
                AnonymousClass4VZ r5 = this.A01;
                String str6 = this.A08;
                if (r7 != null) {
                    String str7 = (String) r9.A06.A01(r7.A01, r9.A03.A00(str4));
                    if (str7 != null) {
                        if (bool2 == null || !bool2.booleanValue()) {
                            int length = str7.length();
                            HashMap A11 = C12970iu.A11();
                            A11.put("size", String.valueOf(length));
                            C130355zH.A01(r9.A02, str5, "bloksCacheHit", A11);
                            r9.A00(r6, r8, str7, z2);
                            return;
                        }
                        return;
                    } else if (Boolean.TRUE.equals(bool2)) {
                        HashMap A112 = C12970iu.A11();
                        A112.put("app_id", str4);
                        C130355zH.A01(r9.A02, C130355zH.A00(Integer.valueOf(i)), "startPrefetch", A112);
                    }
                }
                r32.AZO(new AnonymousClass68T(r5, r6, r7, r8, r9, bool2, str5, str4, str6, i, z2));
            }
        });
    }
}
