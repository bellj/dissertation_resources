package X;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.4vP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106254vP implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView = (RecyclerView) obj;
        C56002kA r6 = (C56002kA) obj2;
        AnonymousClass02H ADp = r6.A0E.ADp(context);
        if (ADp instanceof LinearLayoutManager) {
            ((LinearLayoutManager) ADp).A1a(r6.A0C);
        }
        recyclerView.setLayoutManager(ADp);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        C56002kA r4 = (C56002kA) obj;
        C56002kA r5 = (C56002kA) obj2;
        return (r4.A02 == r5.A02 && r4.A0C == r5.A0C) ? false : true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((RecyclerView) obj).setLayoutManager(null);
    }
}
