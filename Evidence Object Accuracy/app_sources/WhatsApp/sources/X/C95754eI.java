package X;

import android.animation.TimeInterpolator;

/* renamed from: X.4eI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95754eI implements TimeInterpolator {
    public final C28151Kw A00;

    public /* synthetic */ C95754eI(C28151Kw r1) {
        this.A00 = r1;
    }

    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        if (((double) f) > 0.5d) {
            return Math.min(f, (float) Double.longBitsToDouble(this.A00.A00.get()));
        }
        return 0.0f;
    }
}
