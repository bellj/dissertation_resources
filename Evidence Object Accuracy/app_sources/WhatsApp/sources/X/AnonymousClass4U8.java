package X;

import android.content.Context;

/* renamed from: X.4U8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4U8 {
    public Context A00;
    public AnonymousClass01c A01;
    public C87884Dk A02 = new C87884Dk();
    public AbstractC17450qp A03;
    public C18530sb A04;
    public AnonymousClass4N1 A05;
    public final AnonymousClass4ZA A06;
    public final AnonymousClass4ZB A07;
    public final C18520sa A08;
    public final C64723Gq A09;
    public final C18550sd A0A;
    public final C18570sf A0B;

    public AnonymousClass4U8(Context context, AnonymousClass4ZA r3, AnonymousClass4ZB r4, C18520sa r5, C64723Gq r6, C18550sd r7, AbstractC17450qp r8, C18530sb r9, C18570sf r10) {
        this.A00 = context;
        this.A03 = r8;
        this.A04 = r9;
        this.A08 = r5;
        this.A07 = r4;
        this.A0A = r7;
        this.A09 = r6;
        this.A06 = r3;
        this.A0B = r10;
    }
}
