package X;

import android.animation.ValueAnimator;
import android.os.Build;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

/* renamed from: X.0QQ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0QQ {
    public final WeakReference A00;

    public AnonymousClass0QQ(View view) {
        this.A00 = new WeakReference(view);
    }

    public void A00() {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public void A01() {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().start();
        }
    }

    public void A02(float f) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().alpha(f);
        }
    }

    public void A03(float f) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().scaleX(f);
        }
    }

    public void A04(float f) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().scaleY(f);
        }
    }

    public void A05(float f) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().translationX(f);
        }
    }

    public void A06(float f) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().translationY(f);
        }
    }

    public void A07(long j) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
    }

    public void A08(Interpolator interpolator) {
        View view = (View) this.A00.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
    }

    public void A09(AbstractC12530i4 r4) {
        AnonymousClass09K r0;
        View view = (View) this.A00.get();
        if (view != null) {
            ViewPropertyAnimator animate = view.animate();
            if (r4 != null) {
                r0 = new AnonymousClass09K(view, this, r4);
            } else {
                r0 = null;
            }
            animate.setListener(r0);
        }
    }

    public void A0A(AbstractC11270g1 r4) {
        View view = (View) this.A00.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            C04220Kv.A00(new ValueAnimator.AnimatorUpdateListener(view, r4) { // from class: X.0Ut
                public final /* synthetic */ View A00;
                public final /* synthetic */ AbstractC11270g1 A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ((View) ((AnonymousClass0YB) this.A01).A00.A09.getParent()).invalidate();
                }
            }, view.animate());
        }
    }
}
