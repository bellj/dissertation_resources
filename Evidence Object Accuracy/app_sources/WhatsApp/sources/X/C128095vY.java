package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5vY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128095vY {
    public AnonymousClass63X A00;
    public String A01;
    public final UserJid A02;
    public final C1315463e A03;
    public final C1315863i A04;
    public final C1315163b A05;
    public final C126845tX A06;

    public /* synthetic */ C128095vY(UserJid userJid, C1315463e r2, C1315863i r3, C1315163b r4, AnonymousClass63X r5, C126845tX r6, String str) {
        this.A02 = userJid;
        this.A06 = r6;
        this.A03 = r2;
        this.A00 = r5;
        this.A04 = r3;
        this.A01 = str;
        this.A05 = r4;
    }
}
