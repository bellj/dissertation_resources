package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;

/* renamed from: X.1vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42651vb extends AbstractC42661vc {
    public int A00 = 0;
    public ImageView A01;
    public final View.OnClickListener A02 = new ViewOnClickCListenerShape1S0100000_I0_1(this, 38);
    public final Runnable A03 = new RunnableBRunnable0Shape5S0100000_I0_5(this, 7);

    public C42651vb(Context context, AbstractC13890kV r4, AbstractC16130oV r5) {
        super(context, r4, r5);
        A1R();
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        AnonymousClass009.A07("ConversationRowViewOnceMedia/senders can not view their own media");
    }

    @Override // X.AnonymousClass1OY
    public Drawable getPopupDrawable() {
        return this.A10.A05(getResources(), new C39511q1(new int[]{129323}), -1);
    }
}
