package X;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3wT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82963wT extends AbstractC94534c0 {
    public final C92574Wl A00;
    public final boolean A01;
    public final boolean A02;

    public C82963wT(C92574Wl r1, boolean z, boolean z2) {
        this.A00 = r1;
        this.A01 = z;
        this.A02 = z2;
    }

    public AbstractC94534c0 A08(AnonymousClass4RG r7) {
        Object A00;
        if (this.A01) {
            try {
                C92354Vo r4 = new C92354Vo();
                r4.A00 = r7.A00.A00;
                r4.A03.addAll(Arrays.asList(EnumC87084Ad.REQUIRE_PROPERTIES));
                return this.A00.A00(r4.A00(), r7.A01, r7.A02).A00() == AbstractC117035Xw.A00 ? AbstractC116885Xh.A00 : AbstractC116885Xh.A01;
            } catch (C82803wD unused) {
                return AbstractC116885Xh.A00;
            }
        } else {
            try {
                C92574Wl r5 = this.A00;
                if (r5.A01) {
                    HashMap hashMap = r7.A03;
                    if (hashMap.containsKey(r5)) {
                        r5.toString();
                        A00 = hashMap.get(r5);
                    } else {
                        Object obj = r7.A02;
                        A00 = r5.A00(r7.A00, obj, obj).A00();
                        hashMap.put(r5, A00);
                    }
                } else {
                    A00 = r5.A00(r7.A00, r7.A01, r7.A02).A00();
                }
                C92364Vp r1 = r7.A00;
                if (A00 instanceof Number) {
                    return new C83013wY(A00.toString());
                }
                if (A00 instanceof String) {
                    return new C82973wU(A00.toString(), false);
                }
                if (A00 instanceof Boolean) {
                    return Boolean.parseBoolean(A00.toString().toString()) ? AbstractC116885Xh.A01 : AbstractC116885Xh.A00;
                }
                if (A00 instanceof OffsetDateTime) {
                    return new C82983wV(A00.toString());
                }
                if (A00 == null) {
                    return AbstractC116885Xh.A02;
                }
                if (A00 instanceof List) {
                    return new C83003wX(r1.A01.A01(List.class, A00));
                }
                if (A00 instanceof Map) {
                    return new C83003wX(r1.A01.A01(Map.class, A00));
                }
                StringBuilder A0h = C12960it.A0h();
                A0h.append("Could not convert ");
                C12970iu.A1V(A00.getClass(), A0h);
                A0h.append(":");
                C12970iu.A1V(A00, A0h);
                throw new AnonymousClass5H8(C12960it.A0d(" to a ValueNode", A0h));
            } catch (C82803wD unused2) {
                return AbstractC116885Xh.A03;
            }
        }
    }

    public String toString() {
        return (!this.A01 || this.A02) ? this.A00.toString() : C95094d8.A02("!", this.A00.toString());
    }
}
