package X;

import javax.net.ssl.SSLException;

/* renamed from: X.3Yb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69133Yb implements AbstractC116735Wp {
    public AnonymousClass5GB A00;
    public byte[] A01;
    public byte[] A02;

    public static byte[] A00(byte[] bArr, long j) {
        byte[] bArr2 = {(byte) ((int) (j >> 56)), (byte) ((int) (j >> 48)), (byte) ((int) (j >> 40)), (byte) ((int) (j >> 32)), (byte) ((int) (j >> 24)), (byte) ((int) (j >> 16)), (byte) ((int) (j >> 8)), (byte) ((int) j)};
        int length = bArr.length;
        byte[] bArr3 = new byte[length];
        int i = length - 8;
        System.arraycopy(bArr, 0, bArr3, 0, i);
        for (int i2 = i; i2 < length; i2++) {
            bArr3[i2] = (byte) (bArr[i2] ^ bArr2[i2 - i]);
        }
        return bArr3;
    }

    public final byte[] A01(byte[] bArr, int i) {
        AnonymousClass5GB r3 = this.A00;
        int AEp = r3.AEp(i);
        byte[] bArr2 = new byte[AEp];
        int AZZ = r3.AZZ(bArr, 0, i, bArr2, 0);
        int A97 = AZZ + this.A00.A97(bArr2, AZZ);
        if (A97 >= AEp) {
            return bArr2;
        }
        byte[] bArr3 = new byte[A97];
        System.arraycopy(bArr2, 0, bArr3, 0, A97);
        return bArr3;
    }

    @Override // X.AbstractC116735Wp
    public byte[] A8i(byte[] bArr, byte[] bArr2, int i, int i2, long j) {
        byte[] A00 = A00(this.A01, j);
        AnonymousClass5GB r3 = this.A00;
        byte[] bArr3 = this.A02;
        r3.AIf(new C113075Fx(new AnonymousClass20K(bArr3, bArr3.length), A00), false);
        this.A00.AZX(bArr, 0, bArr.length);
        try {
            return A01(bArr2, i2);
        } catch (C114965Nt e) {
            throw new AnonymousClass1NR(new SSLException(e), (byte) 51, true);
        }
    }

    @Override // X.AbstractC116735Wp
    public byte[] A9O(byte[] bArr, byte[] bArr2, int i, int i2, long j) {
        byte[] A00 = A00(this.A01, j);
        AnonymousClass5GB r3 = this.A00;
        byte[] bArr3 = this.A02;
        r3.AIf(new C113075Fx(new AnonymousClass20K(bArr3, bArr3.length), A00), true);
        this.A00.AZX(bArr, 0, bArr.length);
        try {
            return A01(bArr2, i2);
        } catch (C114965Nt e) {
            throw new AnonymousClass1NR(new SSLException(e), (byte) 80);
        }
    }

    @Override // X.AbstractC116735Wp
    public void AId(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr.length != 16) {
            throw AnonymousClass1NR.A00("Invalid key length.", (byte) 80);
        } else if (bArr2 == null || bArr2.length != 12) {
            throw AnonymousClass1NR.A00("Invalid iv length.", (byte) 80);
        } else {
            this.A00 = new AnonymousClass5GB(new C71653dH());
            this.A02 = bArr;
            this.A01 = bArr2;
        }
    }
}
