package X;

import android.view.View;
import android.widget.Space;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122635lm extends AbstractC118835cS {
    public Space A00;
    public TextView A01;
    public TextView A02;

    public C122635lm(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.payment_amount_header);
        this.A02 = C12960it.A0I(view, R.id.payment_amount_text);
        this.A00 = (Space) AnonymousClass028.A0D(view, R.id.space);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r5, int i) {
        C122805m8 r52 = (C122805m8) r5;
        String str = r52.A00;
        TextView textView = this.A01;
        if (str != null) {
            textView.setText(str);
            textView.setVisibility(0);
            this.A00.setVisibility(8);
        } else {
            textView.setVisibility(8);
            this.A00.setVisibility(0);
        }
        TextView textView2 = this.A02;
        textView2.setText(r52.A02);
        if (r52.A01) {
            C92984Yl.A00(textView2);
        } else {
            C92984Yl.A01(textView2);
        }
    }
}
