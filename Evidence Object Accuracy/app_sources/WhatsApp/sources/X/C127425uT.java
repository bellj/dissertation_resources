package X;

import android.content.Context;

/* renamed from: X.5uT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C127425uT {
    public final Context A00;
    public final C14830m7 A01;
    public final AbstractActivityC121495iO A02;

    public C127425uT(Context context, C14830m7 r3) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(context, 2);
        this.A01 = r3;
        this.A00 = context;
        this.A02 = (AbstractActivityC121495iO) context;
    }
}
