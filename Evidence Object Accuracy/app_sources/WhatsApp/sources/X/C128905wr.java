package X;

import android.text.TextUtils;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128905wr {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ C128905wr(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, String str) {
        this.A01 = brazilPayBloksActivity;
        this.A02 = str;
        this.A00 = r1;
    }

    public final void A00(C452120p r9, C119775f5 r10) {
        int i;
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        String str = this.A02;
        AnonymousClass3FE r4 = this.A00;
        if (r9 == null) {
            HashMap A11 = C12970iu.A11();
            String str2 = "1";
            A11.put("remaining_validates", str2);
            C14830m7 r1 = ((ActivityC13790kL) brazilPayBloksActivity).A05;
            if (!TextUtils.isEmpty(str)) {
                i = Integer.parseInt(str);
            } else {
                i = 60;
            }
            A11.put("next_resend_ts", String.valueOf((long) Math.ceil(((double) (r1.A00() + ((long) (i * 1000)))) / 1000.0d)));
            if (r10 != null) {
                if (!r10.A0A()) {
                    str2 = "0";
                }
                A11.put("verified_state", str2);
                ((AbstractActivityC121705jc) brazilPayBloksActivity).A0G.A00().A03(new AbstractC451720l(A11) { // from class: X.67q
                    public final /* synthetic */ HashMap A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC451720l
                    public final void AM7(List list) {
                        AnonymousClass3FE.this.A01("on_success", this.A01);
                    }
                }, r10.A05());
                return;
            }
            r4.A01("on_success", A11);
            return;
        }
        AbstractActivityC121705jc.A0l(r4, null, r9.A00);
    }
}
