package X;

import android.os.RemoteException;

/* renamed from: X.5Gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113245Gt extends RuntimeException {
    public C113245Gt(RemoteException remoteException) {
        super(remoteException);
    }
}
