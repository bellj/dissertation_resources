package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133366Am implements AnonymousClass6MV {
    public final /* synthetic */ C129195xK A00;
    public final /* synthetic */ AnonymousClass6MT A01;

    public C133366Am(C129195xK r1, AnonymousClass6MT r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        Log.e("PAY: BrazilPayBloksActivity/provider key iq returned null");
        this.A01.ARg(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r3) {
        this.A00.A00(this.A01, r3);
    }
}
