package X;

import android.text.TextUtils;

/* renamed from: X.3Za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC69383Za implements AbstractC33751f1 {
    public final AnonymousClass1ZL A00;

    public AbstractC69383Za(AnonymousClass1ZL r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC33751f1
    public void A6l(C39971qq r13, AnonymousClass1XE r14) {
        if (r14.A00 != null) {
            AnonymousClass1G3 r2 = r13.A03;
            C57452n4 r0 = ((C27081Fy) r2.A00).A0L;
            if (r0 == null) {
                r0 = C57452n4.A05;
            }
            AnonymousClass1G4 A0T = r0.A0T();
            String str = r14.A00.A03;
            if (!TextUtils.isEmpty(str)) {
                AnonymousClass1G4 A0T2 = C57012mK.A02.A0T();
                C57012mK r1 = (C57012mK) AnonymousClass1G4.A00(A0T2);
                r1.A00 |= 1;
                r1.A01 = str;
                C57452n4 r12 = (C57452n4) AnonymousClass1G4.A00(A0T);
                r12.A03 = (C57012mK) A0T2.A02();
                r12.A00 |= 1;
            }
            AnonymousClass1PG r8 = r13.A04;
            byte[] bArr = r13.A09;
            if (C32411c7.A0U(r8, r14, bArr)) {
                C43261wh A0P = C32411c7.A0P(r13.A00, r13.A02, r8, r14, bArr, r13.A06);
                C57452n4 r15 = (C57452n4) AnonymousClass1G4.A00(A0T);
                r15.A02 = A0P;
                r15.A00 |= 4;
            }
            C27081Fy r16 = (C27081Fy) AnonymousClass1G4.A00(r2);
            r16.A0L = (C57452n4) A0T.A02();
            r16.A01 |= 32;
        }
    }

    @Override // X.AbstractC33751f1
    public int ADz() {
        return 36;
    }
}
