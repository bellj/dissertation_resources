package X;

import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.QrScanCodeFragment;

/* renamed from: X.3Zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69473Zj implements AnonymousClass27Z {
    public final /* synthetic */ QrScanCodeFragment A00;

    public C69473Zj(QrScanCodeFragment qrScanCodeFragment) {
        this.A00 = qrScanCodeFragment;
    }

    @Override // X.AnonymousClass27Z
    public void ANb(int i) {
        QrScanCodeFragment qrScanCodeFragment = this.A00;
        if (qrScanCodeFragment.A07.A03()) {
            qrScanCodeFragment.A02.A07(R.string.error_camera_disabled_during_video_call, 1);
        }
        ActivityC000900k A0B = qrScanCodeFragment.A0B();
        if (A0B instanceof AnonymousClass34P) {
            AnonymousClass34P r1 = (AnonymousClass34P) A0B;
            r1.A02.A0F(!C28141Kv.A01(r1.A0G) ? 1 : 0, true);
        }
    }

    @Override // X.AnonymousClass27Z
    public void AUF() {
        this.A00.A1B();
    }

    @Override // X.AnonymousClass27Z
    public void AUS(C49262Kb r6) {
        QrScanCodeFragment qrScanCodeFragment = this.A00;
        if (!qrScanCodeFragment.A09) {
            String str = r6.A02;
            if (str != null && !str.equals(qrScanCodeFragment.A08)) {
                qrScanCodeFragment.A08 = str;
                if (((AnonymousClass34P) qrScanCodeFragment.A0C()).A2h(str, true, 2)) {
                    qrScanCodeFragment.A0B = true;
                    qrScanCodeFragment.A02.A0G(qrScanCodeFragment.A0D);
                    return;
                }
                qrScanCodeFragment.A02.A07(R.string.contact_qr_scan_toast_no_valid_code, 1);
            }
            qrScanCodeFragment.A06.Aab();
        }
    }
}
