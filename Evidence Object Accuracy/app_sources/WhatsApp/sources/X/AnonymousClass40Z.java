package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.40Z  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass40Z extends AbstractC37191le {
    public final WaImageView A00;
    public final WaTextView A01;

    public AnonymousClass40Z(View view) {
        super(view);
        this.A01 = (WaTextView) AnonymousClass028.A0D(view, R.id.category_name);
        this.A00 = (WaImageView) AnonymousClass028.A0D(view, R.id.category_icon);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        throw C12980iv.A0n("displayName");
    }
}
