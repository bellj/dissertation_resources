package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

/* renamed from: X.3fQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class RunnableC72943fQ extends BroadcastReceiver implements Runnable {
    public final Handler A00;
    public final AbstractC115045Pq A01;
    public final /* synthetic */ C89844Lp A02;

    public RunnableC72943fQ(Handler handler, AbstractC115045Pq r2, C89844Lp r3) {
        this.A02 = r3;
        this.A00 = handler;
        this.A01 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
            this.A00.post(this);
        }
    }

    @Override // java.lang.Runnable
    public void run() {
    }
}
