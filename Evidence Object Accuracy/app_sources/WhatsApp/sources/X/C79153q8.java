package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3q8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79153q8 extends AbstractC113535Hy<Boolean> implements AnonymousClass5Z5<Boolean>, RandomAccess {
    public static final C79153q8 A02;
    public int A00;
    public boolean[] A01;

    public C79153q8(boolean[] zArr, int i) {
        this.A01 = zArr;
        this.A00 = i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C79153q8)) {
            return super.addAll(collection);
        }
        C79153q8 r7 = (C79153q8) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.A01;
            if (i3 > zArr.length) {
                zArr = Arrays.copyOf(zArr, i3);
                this.A01 = zArr;
            }
            System.arraycopy(r7.A01, 0, zArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C79153q8)) {
                return super.equals(obj);
            }
            C79153q8 r8 = (C79153q8) obj;
            int i = this.A00;
            if (i == r8.A00) {
                boolean[] zArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == zArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean remove(Object obj) {
        A02();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Boolean.valueOf(this.A01[i]))) {
                boolean[] zArr = this.A01;
                System.arraycopy(zArr, i + 1, zArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            boolean[] zArr = this.A01;
            System.arraycopy(zArr, i2, zArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }

    static {
        C79153q8 r0 = new C79153q8(new boolean[10], 0);
        A02 = r0;
        ((AbstractC113535Hy) r0).A00 = false;
    }

    public final void A03(int i, boolean z) {
        int i2;
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        if (i2 < zArr.length) {
            C72463ee.A0T(zArr, i, i2);
        } else {
            boolean[] zArr2 = new boolean[((i2 * 3) >> 1) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.A01, i, zArr2, i + 1, this.A00 - i);
            this.A01 = zArr2;
        }
        this.A01[i] = z;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= this.A00) {
            return new C79153q8(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A03(i, C12970iu.A1Y(obj));
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return Boolean.valueOf(this.A01[i]);
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            int i3 = i * 31;
            int i4 = 1237;
            if (this.A01[i2]) {
                i4 = 1231;
            }
            i = i3 + i4;
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        boolean z = zArr[i];
        AbstractC113535Hy.A01(zArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        boolean A1Y = C12970iu.A1Y(obj);
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        boolean z = zArr[i];
        zArr[i] = A1Y;
        return Boolean.valueOf(z);
    }
}
