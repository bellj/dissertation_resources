package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1G6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1G6 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1G6 A0k;
    public static volatile AnonymousClass255 A0l;
    public byte A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07 = 1;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public AbstractC27881Jp A0C;
    public AbstractC27881Jp A0D;
    public AbstractC27881Jp A0E;
    public AnonymousClass1K6 A0F;
    public AnonymousClass1K6 A0G;
    public AnonymousClass1K6 A0H;
    public AnonymousClass1K6 A0I;
    public AnonymousClass1K6 A0J;
    public C35771ii A0K;
    public C27081Fy A0L;
    public AnonymousClass1G8 A0M;
    public AnonymousClass278 A0N;
    public AnonymousClass25L A0O;
    public AnonymousClass25L A0P;
    public C39941qn A0Q;
    public C39941qn A0R;
    public AnonymousClass25K A0S;
    public C40811sM A0T;
    public AnonymousClass25J A0U;
    public String A0V;
    public String A0W;
    public String A0X = "";
    public String A0Y = "";
    public String A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;

    static {
        AnonymousClass1G6 r0 = new AnonymousClass1G6();
        A0k = r0;
        r0.A0W();
    }

    public AnonymousClass1G6() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A0D = r1;
        AnonymousClass277 r0 = AnonymousClass277.A01;
        this.A0G = r0;
        this.A0F = r0;
        this.A0Z = "";
        this.A0J = r0;
        this.A0I = r0;
        this.A0C = r1;
        this.A0H = r0;
        this.A0V = "";
        this.A0E = r1;
        this.A0W = "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:356:0x0895, code lost:
        if (r1.A0Z() == false) goto L_0x0897;
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r18, java.lang.Object r19, java.lang.Object r20) {
        /*
        // Method dump skipped, instructions count: 2438
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1G6.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A01 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A0M;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i = CodedOutputStream.A0A(r0, 1) + 0;
        } else {
            i = 0;
        }
        if ((this.A01 & 2) == 2) {
            C27081Fy r02 = this.A0L;
            if (r02 == null) {
                r02 = C27081Fy.A0i;
            }
            i += CodedOutputStream.A0A(r02, 2);
        }
        int i3 = this.A01;
        if ((i3 & 4) == 4) {
            i += CodedOutputStream.A06(3, this.A0A);
        }
        if ((i3 & 8) == 8) {
            i += CodedOutputStream.A02(4, this.A07);
        }
        if ((i3 & 16) == 16) {
            i += CodedOutputStream.A07(5, this.A0X);
        }
        int i4 = this.A01;
        if ((i4 & 32) == 32) {
            i += CodedOutputStream.A06(6, this.A09);
        }
        if ((i4 & 64) == 64) {
            i += CodedOutputStream.A00(16);
        }
        if ((i4 & 128) == 128) {
            i += CodedOutputStream.A00(17);
        }
        if ((i4 & 256) == 256) {
            i += CodedOutputStream.A00(18);
        }
        if ((i4 & 512) == 512) {
            i += CodedOutputStream.A07(19, this.A0Y);
        }
        int i5 = this.A01;
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i += CodedOutputStream.A09(this.A0D, 20);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i += CodedOutputStream.A00(21);
        }
        if ((i5 & 4096) == 4096) {
            i += CodedOutputStream.A00(22);
        }
        if ((i5 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i += CodedOutputStream.A00(23);
        }
        if ((i5 & 16384) == 16384) {
            i += CodedOutputStream.A02(24, this.A06);
        }
        if ((i5 & 32768) == 32768) {
            i += CodedOutputStream.A00(25);
        }
        int i6 = 0;
        for (int i7 = 0; i7 < this.A0G.size(); i7++) {
            i6 += CodedOutputStream.A0B((String) this.A0G.get(i7));
        }
        int size = i + i6 + (this.A0G.size() << 1);
        if ((this.A01 & 65536) == 65536) {
            size += CodedOutputStream.A04(27, this.A04);
        }
        int i8 = 0;
        for (int i9 = 0; i9 < this.A0F.size(); i9++) {
            i8 += CodedOutputStream.A0B((String) this.A0F.get(i9));
        }
        int size2 = size + i8 + (this.A0F.size() << 1);
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            C39941qn r03 = this.A0Q;
            if (r03 == null) {
                r03 = C39941qn.A0E;
            }
            size2 += CodedOutputStream.A0A(r03, 29);
        }
        if ((this.A01 & 262144) == 262144) {
            C35771ii r04 = this.A0K;
            if (r04 == null) {
                r04 = C35771ii.A0B;
            }
            size2 += CodedOutputStream.A0A(r04, 30);
        }
        if ((this.A01 & 524288) == 524288) {
            C39941qn r05 = this.A0R;
            if (r05 == null) {
                r05 = C39941qn.A0E;
            }
            size2 += CodedOutputStream.A0A(r05, 31);
        }
        int i10 = this.A01;
        if ((i10 & 1048576) == 1048576) {
            size2 += CodedOutputStream.A06(32, this.A08);
        }
        if ((i10 & 2097152) == 2097152) {
            size2 += CodedOutputStream.A04(33, this.A05);
        }
        if ((i10 & 4194304) == 4194304) {
            size2 += CodedOutputStream.A00(34);
        }
        if ((i10 & 8388608) == 8388608) {
            size2 += CodedOutputStream.A00(35);
        }
        if ((i10 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            size2 += CodedOutputStream.A02(36, this.A03);
        }
        if ((i10 & 33554432) == 33554432) {
            size2 += CodedOutputStream.A07(37, this.A0Z);
        }
        if ((this.A01 & 67108864) == 67108864) {
            AnonymousClass25L r06 = this.A0O;
            if (r06 == null) {
                r06 = AnonymousClass25L.A02;
            }
            size2 += CodedOutputStream.A0A(r06, 38);
        }
        if ((this.A01 & 134217728) == 134217728) {
            AnonymousClass25K r07 = this.A0S;
            if (r07 == null) {
                r07 = AnonymousClass25K.A04;
            }
            size2 += CodedOutputStream.A0A(r07, 39);
        }
        for (int i11 = 0; i11 < this.A0J.size(); i11++) {
            size2 += CodedOutputStream.A0A((AnonymousClass1G1) this.A0J.get(i11), 40);
        }
        for (int i12 = 0; i12 < this.A0I.size(); i12++) {
            size2 += CodedOutputStream.A0A((AnonymousClass1G1) this.A0I.get(i12), 41);
        }
        if ((this.A01 & 268435456) == 268435456) {
            AnonymousClass25L r08 = this.A0P;
            if (r08 == null) {
                r08 = AnonymousClass25L.A02;
            }
            size2 += CodedOutputStream.A0A(r08, 42);
        }
        int i13 = this.A01;
        if ((i13 & 536870912) == 536870912) {
            size2 += CodedOutputStream.A09(this.A0C, 43);
        }
        if ((i13 & 1073741824) == 1073741824) {
            AnonymousClass25J r09 = this.A0U;
            if (r09 == null) {
                r09 = AnonymousClass25J.A04;
            }
            size2 += CodedOutputStream.A0A(r09, 44);
        }
        for (int i14 = 0; i14 < this.A0H.size(); i14++) {
            size2 += CodedOutputStream.A0A((AnonymousClass1G1) this.A0H.get(i14), 45);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            C40811sM r010 = this.A0T;
            if (r010 == null) {
                r010 = C40811sM.A02;
            }
            size2 += CodedOutputStream.A0A(r010, 46);
        }
        if ((this.A02 & 1) == 1) {
            size2 += CodedOutputStream.A07(47, this.A0V);
        }
        int i15 = this.A02;
        if ((i15 & 2) == 2) {
            size2 += CodedOutputStream.A00(48);
        }
        if ((i15 & 4) == 4) {
            size2 += CodedOutputStream.A09(this.A0E, 49);
        }
        if ((i15 & 8) == 8) {
            AnonymousClass278 r011 = this.A0N;
            if (r011 == null) {
                r011 = AnonymousClass278.A05;
            }
            size2 += CodedOutputStream.A0A(r011, 50);
        }
        if ((this.A02 & 16) == 16) {
            size2 += CodedOutputStream.A07(51, this.A0W);
        }
        if ((this.A02 & 32) == 32) {
            size2 += CodedOutputStream.A06(52, this.A0B);
        }
        int A00 = size2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A0M;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A01 & 2) == 2) {
            C27081Fy r02 = this.A0L;
            if (r02 == null) {
                r02 = C27081Fy.A0i;
            }
            codedOutputStream.A0L(r02, 2);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0H(3, this.A0A);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0E(4, this.A07);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0X);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0H(6, this.A09);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0J(16, this.A0e);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0J(17, this.A0g);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0J(18, this.A0a);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0I(19, this.A0Y);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0K(this.A0D, 20);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0J(21, this.A0f);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0J(22, this.A0j);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0J(23, this.A0i);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0E(24, this.A06);
        }
        if ((this.A01 & 32768) == 32768) {
            codedOutputStream.A0J(25, this.A0b);
        }
        for (int i = 0; i < this.A0G.size(); i++) {
            codedOutputStream.A0I(26, (String) this.A0G.get(i));
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0F(27, this.A04);
        }
        for (int i2 = 0; i2 < this.A0F.size(); i2++) {
            codedOutputStream.A0I(28, (String) this.A0F.get(i2));
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            C39941qn r03 = this.A0Q;
            if (r03 == null) {
                r03 = C39941qn.A0E;
            }
            codedOutputStream.A0L(r03, 29);
        }
        if ((this.A01 & 262144) == 262144) {
            C35771ii r04 = this.A0K;
            if (r04 == null) {
                r04 = C35771ii.A0B;
            }
            codedOutputStream.A0L(r04, 30);
        }
        if ((this.A01 & 524288) == 524288) {
            C39941qn r05 = this.A0R;
            if (r05 == null) {
                r05 = C39941qn.A0E;
            }
            codedOutputStream.A0L(r05, 31);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0H(32, this.A08);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0F(33, this.A05);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0J(34, this.A0c);
        }
        if ((this.A01 & 8388608) == 8388608) {
            codedOutputStream.A0J(35, this.A0d);
        }
        if ((this.A01 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            codedOutputStream.A0E(36, this.A03);
        }
        if ((this.A01 & 33554432) == 33554432) {
            codedOutputStream.A0I(37, this.A0Z);
        }
        if ((this.A01 & 67108864) == 67108864) {
            AnonymousClass25L r06 = this.A0O;
            if (r06 == null) {
                r06 = AnonymousClass25L.A02;
            }
            codedOutputStream.A0L(r06, 38);
        }
        if ((this.A01 & 134217728) == 134217728) {
            AnonymousClass25K r07 = this.A0S;
            if (r07 == null) {
                r07 = AnonymousClass25K.A04;
            }
            codedOutputStream.A0L(r07, 39);
        }
        for (int i3 = 0; i3 < this.A0J.size(); i3++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0J.get(i3), 40);
        }
        for (int i4 = 0; i4 < this.A0I.size(); i4++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0I.get(i4), 41);
        }
        if ((this.A01 & 268435456) == 268435456) {
            AnonymousClass25L r08 = this.A0P;
            if (r08 == null) {
                r08 = AnonymousClass25L.A02;
            }
            codedOutputStream.A0L(r08, 42);
        }
        if ((this.A01 & 536870912) == 536870912) {
            codedOutputStream.A0K(this.A0C, 43);
        }
        if ((this.A01 & 1073741824) == 1073741824) {
            AnonymousClass25J r09 = this.A0U;
            if (r09 == null) {
                r09 = AnonymousClass25J.A04;
            }
            codedOutputStream.A0L(r09, 44);
        }
        for (int i5 = 0; i5 < this.A0H.size(); i5++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0H.get(i5), 45);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            C40811sM r010 = this.A0T;
            if (r010 == null) {
                r010 = C40811sM.A02;
            }
            codedOutputStream.A0L(r010, 46);
        }
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0I(47, this.A0V);
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0J(48, this.A0h);
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0K(this.A0E, 49);
        }
        if ((this.A02 & 8) == 8) {
            AnonymousClass278 r011 = this.A0N;
            if (r011 == null) {
                r011 = AnonymousClass278.A05;
            }
            codedOutputStream.A0L(r011, 50);
        }
        if ((this.A02 & 16) == 16) {
            codedOutputStream.A0I(51, this.A0W);
        }
        if ((this.A02 & 32) == 32) {
            codedOutputStream.A0H(52, this.A0B);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
