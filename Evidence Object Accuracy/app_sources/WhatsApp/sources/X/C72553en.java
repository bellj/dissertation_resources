package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3en  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72553en extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass4KL A00;
    public final /* synthetic */ AnonymousClass2xW A01;

    public C72553en(AnonymousClass4KL r1, AnonymousClass2xW r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2xW r1 = this.A01;
        r1.A06(this.A00);
        r1.A02.setTranslationY(0.0f);
    }
}
