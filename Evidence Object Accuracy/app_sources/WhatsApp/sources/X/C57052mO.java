package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57052mO extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57052mO A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public String A01 = "";
    public String A02 = "";

    static {
        C57052mO r0 = new C57052mO();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57052mO r9 = (C57052mO) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A01;
                int i2 = r9.A00;
                this.A01 = r8.Afy(str, r9.A01, A1R, C12960it.A1R(i2));
                this.A02 = r8.Afy(this.A02, r9.A02, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r82.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            String A0A = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A01 = A0A;
                        } else if (A032 == 18) {
                            String A0A2 = r82.A0A();
                            this.A00 |= 2;
                            this.A02 = A0A2;
                        } else if (!A0a(r82, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57052mO();
            case 5:
                return new C81513u8();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57052mO.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A01, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A02, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
