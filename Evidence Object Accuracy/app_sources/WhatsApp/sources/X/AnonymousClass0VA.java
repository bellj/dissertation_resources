package X;

import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;
import java.util.List;

/* renamed from: X.0VA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VA implements DatabaseErrorHandler {
    public final /* synthetic */ AnonymousClass0SX A00;
    public final /* synthetic */ AnonymousClass0ZE[] A01;

    public AnonymousClass0VA(AnonymousClass0SX r1, AnonymousClass0ZE[] r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.database.DatabaseErrorHandler
    public void onCorruption(SQLiteDatabase sQLiteDatabase) {
        AnonymousClass0ZE[] r2 = this.A01;
        AnonymousClass0ZE r0 = r2[0];
        if (r0 == null || r0.A00 != sQLiteDatabase) {
            r2[0] = new AnonymousClass0ZE(sQLiteDatabase);
        }
        AnonymousClass0ZE r22 = r2[0];
        StringBuilder sb = new StringBuilder("Corruption reported by sqlite on database: ");
        SQLiteDatabase sQLiteDatabase2 = r22.A00;
        sb.append(sQLiteDatabase2.getPath());
        Log.e("SupportSQLite", sb.toString());
        if (sQLiteDatabase2.isOpen()) {
            List<Pair<String, String>> list = null;
            try {
                try {
                    list = sQLiteDatabase2.getAttachedDbs();
                } catch (SQLiteException unused) {
                }
                try {
                    r22.close();
                } catch (IOException unused2) {
                }
            } finally {
                if (list != null) {
                    for (Pair<String, String> next : list) {
                        AnonymousClass0SX.A00((String) next.second);
                    }
                } else {
                    AnonymousClass0SX.A00(sQLiteDatabase2.getPath());
                }
            }
        }
    }
}
