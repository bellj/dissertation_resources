package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.3jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75373jk extends AnonymousClass03U {
    public final WaImageView A00;

    public C75373jk(View view) {
        super(view);
        this.A00 = (WaImageView) AnonymousClass028.A0D(view, R.id.search_icon);
    }
}
