package X;

/* renamed from: X.5oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC124175oj extends AbstractC16350or {
    public final C18640sm A00;
    public final C18600si A01;
    public final C18610sj A02;
    public final AnonymousClass1BY A03;
    public final C22120yY A04;

    public AbstractC124175oj(C18640sm r1, C18600si r2, C18610sj r3, AnonymousClass1BY r4, C22120yY r5) {
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:54:0x01ee */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v5, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r2v14, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r3v18 */
    /* JADX WARNING: Can't wrap try/catch for region: R(15:6|(2:8|(13:10|(2:12|(1:14)(2:34|35))(3:25|(1:27)(2:28|(1:30))|35)|15|(1:17)|18|(6:21|75|22|80|79|19)|78|36|69|37|77|38|(8:40|73|41|(9:43|(1:45)|82|46|71|47|59|67|68)(1:50)|51|59|67|68)(3:53|67|68))(1:32))(1:31)|33|35|15|(0)|18|(1:19)|78|36|69|37|77|38|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01ee, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01f0, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01f1, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0202, code lost:
        if (r5 == null) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x020a, code lost:
        if (r3 != 0) goto L_0x020c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x020c, code lost:
        r3.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x020f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0210, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0211, code lost:
        r6.A0I.A0A("httpRequestFBToken threw while building url ", r2);
        r1 = r2.toString();
        r4.A00 = 3;
        r4.A09 = r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ec A[Catch: IOException -> 0x01f0, all -> 0x01ee, TRY_LEAVE, TryCatch #7 {IOException -> 0x01f0, all -> 0x01ee, blocks: (B:38:0x00e4, B:40:0x00ec, B:53:0x01df), top: B:77:0x00e4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01df A[Catch: IOException -> 0x01f0, all -> 0x01ee, TRY_ENTER, TRY_LEAVE, TryCatch #7 {IOException -> 0x01f0, all -> 0x01ee, blocks: (B:38:0x00e4, B:40:0x00ec, B:53:0x01df), top: B:77:0x00e4 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r12) {
        /*
        // Method dump skipped, instructions count: 550
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC124175oj.A05(java.lang.Object[]):java.lang.Object");
    }
}
