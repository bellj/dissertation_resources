package X;

import android.util.Log;
import java.util.Locale;

/* renamed from: X.3Cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63703Cp {
    public final int A00;
    public final AnonymousClass4IS A01;
    public final String A02;
    public final String A03;

    public C63703Cp(String str, String... strArr) {
        String A0d;
        int length = strArr.length;
        if (length == 0) {
            A0d = "";
        } else {
            StringBuilder A0k = C12960it.A0k("[");
            int i = 0;
            do {
                String str2 = strArr[i];
                if (A0k.length() > 1) {
                    A0k.append(",");
                }
                A0k.append(str2);
                i++;
            } while (i < length);
            A0d = C12960it.A0d("] ", A0k);
        }
        this.A03 = A0d;
        this.A02 = str;
        this.A01 = new AnonymousClass4IS(str, null);
        int i2 = 2;
        while (!Log.isLoggable(this.A02, i2) && (i2 = i2 + 1) <= 7) {
        }
        this.A00 = i2;
    }

    public void A00(String str, Object... objArr) {
        if (this.A00 <= 3) {
            String str2 = this.A02;
            if (objArr.length > 0) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.d(str2, this.A03.concat(str));
        }
    }
}
