package X;

import android.graphics.Rect;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spanned;
import android.widget.TextView;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.2eq  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2eq extends AnonymousClass0DW {
    public final Rect A00 = C12980iv.A0J();
    public final TextView A01;
    public final AnonymousClass01d A02;

    public AnonymousClass2eq(TextView textView, AnonymousClass01d r3) {
        super(textView);
        this.A02 = r3;
        this.A01 = textView;
    }

    @Override // X.AnonymousClass0DW
    public int A07(float f, float f2) {
        TextView textView = this.A01;
        CharSequence text = textView.getText();
        if (!(text instanceof Spanned)) {
            return Integer.MIN_VALUE;
        }
        Spanned spanned = (Spanned) text;
        int offsetForPosition = textView.getOffsetForPosition(f, f2);
        AbstractC116465Vn[] r2 = (AbstractC116465Vn[]) spanned.getSpans(offsetForPosition, offsetForPosition, AbstractC116465Vn.class);
        if (r2.length == 1) {
            return spanned.getSpanStart(r2[0]);
        }
        return Integer.MIN_VALUE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0056  */
    @Override // X.AnonymousClass0DW
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0C(X.AnonymousClass04Z r6, int r7) {
        /*
            r5 = this;
            android.widget.TextView r3 = r5.A01
            java.lang.CharSequence r1 = r3.getText()
            boolean r0 = r1 instanceof android.text.Spanned
            if (r0 == 0) goto L_0x0064
            android.text.Spanned r1 = (android.text.Spanned) r1
            java.lang.Class<X.5Vn> r0 = X.AbstractC116465Vn.class
            java.lang.Object[] r2 = r1.getSpans(r7, r7, r0)
            X.5Vn[] r2 = (X.AbstractC116465Vn[]) r2
            int r1 = r2.length
            r0 = 1
            if (r1 != r0) goto L_0x0064
            r0 = 0
            r4 = r2[r0]
            if (r4 == 0) goto L_0x0065
            java.lang.CharSequence r2 = r3.getText()
            boolean r0 = r2 instanceof android.text.Spanned
            if (r0 == 0) goto L_0x0033
            android.text.Spanned r2 = (android.text.Spanned) r2
            int r1 = r2.getSpanStart(r4)
            int r0 = r2.getSpanEnd(r4)
            java.lang.CharSequence r2 = r2.subSequence(r1, r0)
        L_0x0033:
            android.view.accessibility.AccessibilityNodeInfo r3 = r6.A02
            r3.setContentDescription(r2)
            r2 = 1
            r3.setFocusable(r2)
            r3.setClickable(r2)
            android.graphics.Rect r1 = r5.A00
            r5.A0K(r1, r4)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0056
            r5.A0K(r1, r4)
        L_0x004d:
            r3.setBoundsInParent(r1)
            r0 = 16
            r3.addAction(r0)
            return
        L_0x0056:
            java.lang.String r0 = "LinkAccessibilityHelper/LinkSpan bounds is empty for: "
            java.lang.String r0 = X.C12960it.A0W(r7, r0)
            com.whatsapp.util.Log.e(r0)
            r0 = 0
            r1.set(r0, r0, r2, r2)
            goto L_0x004d
        L_0x0064:
            r4 = 0
        L_0x0065:
            java.lang.String r0 = "LinkAccessibilityHelper/TouchableSpan is null for offset: "
            java.lang.String r0 = X.C12960it.A0W(r7, r0)
            com.whatsapp.util.Log.e(r0)
            java.lang.CharSequence r2 = r3.getText()
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2eq.A0C(X.04Z, int):void");
    }

    @Override // X.AnonymousClass0DW
    public void A0D(List list) {
        if (!this.A02.A0S("android.hardware.type.featurephone")) {
            CharSequence text = this.A01.getText();
            if (text instanceof Spanned) {
                Spanned spanned = (Spanned) text;
                for (AbstractC116465Vn r0 : (AbstractC116465Vn[]) spanned.getSpans(0, spanned.length(), AbstractC116465Vn.class)) {
                    list.add(Integer.valueOf(spanned.getSpanStart(r0)));
                }
            }
        }
    }

    @Override // X.AnonymousClass0DW
    public boolean A0G(int i, int i2, Bundle bundle) {
        AbstractC116465Vn r0;
        if (i2 != 16) {
            return false;
        }
        TextView textView = this.A01;
        CharSequence text = textView.getText();
        if (text instanceof Spanned) {
            AbstractC116465Vn[] r2 = (AbstractC116465Vn[]) ((Spanned) text).getSpans(i, i, AbstractC116465Vn.class);
            if (r2.length == 1 && (r0 = r2[0]) != null) {
                r0.onClick(textView);
                return true;
            }
        }
        Log.e(C12960it.A0W(i, "LinkAccessibilityHelper/LinkSpan is null for offset: "));
        return false;
    }

    public final void A0K(Rect rect, AbstractC116465Vn r8) {
        Layout layout;
        TextView textView = this.A01;
        CharSequence text = textView.getText();
        rect.setEmpty();
        if ((text instanceof Spanned) && (layout = textView.getLayout()) != null) {
            Spanned spanned = (Spanned) text;
            int spanStart = spanned.getSpanStart(r8);
            int spanEnd = spanned.getSpanEnd(r8);
            int lineForOffset = layout.getLineForOffset(spanStart);
            int lineForOffset2 = layout.getLineForOffset(spanEnd);
            layout.getLineBounds(lineForOffset, rect);
            if (lineForOffset2 != lineForOffset) {
                Rect A0J = C12980iv.A0J();
                while (true) {
                    lineForOffset++;
                    if (lineForOffset > lineForOffset2) {
                        break;
                    }
                    layout.getLineBounds(lineForOffset, A0J);
                    rect.union(A0J);
                }
            } else {
                rect.left = (int) layout.getPrimaryHorizontal(spanStart);
                rect.right = (int) layout.getPrimaryHorizontal(spanEnd);
            }
            rect.offset(textView.getTotalPaddingLeft(), textView.getTotalPaddingTop());
        }
    }
}
