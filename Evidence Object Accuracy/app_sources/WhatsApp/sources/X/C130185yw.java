package X;

import android.graphics.Rect;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import java.util.concurrent.FutureTask;

/* renamed from: X.5yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130185yw {
    public CameraDevice A00;
    public CameraManager A01;
    public AbstractC136156Lf A02;
    public C125405rC A03;
    public AnonymousClass61Q A04;
    public C1308260c A05;
    public C119085cr A06;
    public AbstractC130695zp A07;
    public FutureTask A08;
    public boolean A09;
    public final C129805yK A0A;
    public final C1308560f A0B;
    public volatile boolean A0C;
    public volatile boolean A0D;
    public volatile boolean A0E;

    public C130185yw(C1308560f r2) {
        C129805yK r0 = new C129805yK(r2);
        this.A0B = r2;
        this.A0A = r0;
    }

    public Integer A00(CaptureRequest.Builder builder, AnonymousClass66I r5, C1310360y r6) {
        this.A0A.A01("Method lockFocusForCapture() must run on the Optic Background Thread.");
        if (r5 != null) {
            r5.A0E = 2;
            r5.A0D.A02(300);
            this.A0B.A04("lock_focus_for_capture_on_camera_handler_thread", new CallableC135946Kk(builder, this, r5, r6));
            return r5.A0A;
        }
        throw new AnonymousClass6L0("Preview closed while processing capture request.");
    }

    public void A01() {
        this.A0A.A02("Failed to release PreviewController.", false);
        this.A03 = null;
        this.A01 = null;
        this.A00 = null;
        this.A07 = null;
        this.A06 = null;
        this.A05 = null;
        this.A04 = null;
    }

    public synchronized void A02() {
        FutureTask futureTask = this.A08;
        if (futureTask != null) {
            this.A0B.A08(futureTask);
            this.A08 = null;
        }
    }

    public void A03(Rect rect, CaptureRequest.Builder builder, AnonymousClass66I r17, C124855qE r18, float[] fArr, boolean z) {
        AnonymousClass61Q r0;
        C1310360y r4;
        long j;
        Rect rect2;
        C129805yK r1 = this.A0A;
        r1.A01("Cannot perform focus, not on Optic thread.");
        r1.A01("Can only check if the prepared on the Optic thread");
        if (r1.A00 && this.A03.A00.isConnected() && (r0 = this.A04) != null && r0.A0Q && builder != null && r17 != null) {
            if (C117295Zj.A1S(AbstractC130695zp.A0O, this.A07) && r18 != null && this.A05 != null && this.A0D && (r4 = this.A04.A09) != null) {
                A02();
                A09(EnumC124565pk.FOCUSING, fArr);
                MeteringRectangle[] meteringRectangleArr = new MeteringRectangle[1];
                C1308260c r5 = this.A05;
                if (!(r5.A04 == null || (rect2 = r5.A03) == null)) {
                    float width = ((float) rect2.width()) / ((float) r5.A04.width());
                    float height = ((float) r5.A03.height()) / ((float) r5.A04.height());
                    int width2 = (r5.A04.width() - r5.A03.width()) >> 1;
                    int height2 = (r5.A04.height() - r5.A03.height()) >> 1;
                    int centerX = (int) ((((float) rect.centerX()) * width) + ((float) width2));
                    int centerY = (int) ((((float) rect.centerY()) * height) + ((float) height2));
                    Rect rect3 = new Rect(centerX, centerY, centerX, centerY);
                    rect3.inset((-rect.width()) >> 1, (-rect.height()) >> 1);
                    rect = rect3;
                }
                meteringRectangleArr[0] = new MeteringRectangle(rect, 1000);
                r17.A04 = null;
                r17.A06 = new AnonymousClass66B(builder, this, r17, fArr, z);
                builder.set(CaptureRequest.CONTROL_AF_MODE, 1);
                builder.set(CaptureRequest.CONTROL_AF_REGIONS, meteringRectangleArr);
                this.A0C = true;
                CaptureRequest.Key key = CaptureRequest.CONTROL_AF_TRIGGER;
                builder.set(key, 2);
                r4.A04(builder.build(), r17);
                builder.set(key, 0);
                r4.A05(builder.build(), r17);
                builder.set(key, 1);
                r4.A04(builder.build(), r17);
                if (z) {
                    j = 6000;
                } else {
                    j = 4000;
                }
                A08(builder, r17, j);
            }
        }
    }

    public void A04(CameraDevice cameraDevice, CameraManager cameraManager, C125405rC r6, AnonymousClass61Q r7, C1308260c r8, C119085cr r9, AbstractC130695zp r10) {
        C129805yK r2 = this.A0A;
        r2.A01("Can only prepare the FocusController on the Optic thread.");
        this.A03 = r6;
        this.A01 = cameraManager;
        this.A00 = cameraDevice;
        this.A07 = r10;
        this.A06 = r9;
        this.A05 = r8;
        this.A04 = r7;
        this.A0E = false;
        this.A0D = true;
        r2.A02("Failed to prepare FocusController.", true);
    }

    public void A05(CaptureRequest.Builder builder, AnonymousClass66I r14) {
        C1310360y r2;
        this.A0A.A01("Can only reset focus on the Optic thread.");
        if (this.A04 != null && this.A05 != null && builder != null && this.A07 != null && this.A0D && (r2 = this.A04.A09) != null) {
            this.A0E = false;
            this.A0C = false;
            float A01 = this.A05.A01();
            C1308260c r0 = this.A05;
            AnonymousClass61Q.A01(r0.A03, builder, this.A07, r0.A06(), this.A05.A05(), A01);
            builder.set(CaptureRequest.CONTROL_AF_TRIGGER, 2);
            r2.A04(builder.build(), r14);
            int A00 = C1308660g.A00(this.A01, builder, this.A06, this.A07, this.A00.getId(), 0);
            builder.set(CaptureRequest.CONTROL_AF_TRIGGER, 0);
            r2.A05(builder.build(), r14);
            if (A00 == 1) {
                CaptureRequest.Key key = CaptureRequest.CONTROL_AF_TRIGGER;
                builder.set(key, 1);
                r2.A04(builder.build(), r14);
                builder.set(key, 0);
            }
        }
    }

    public void A06(CaptureRequest.Builder builder, AnonymousClass66I r6) {
        AnonymousClass61Q r1;
        C1310360y r2;
        int i;
        this.A0B.A06("Method setFocusModeForVideo() must run on the Optic Background Thread.");
        if (this.A01 != null && this.A00 != null && (r1 = this.A04) != null && builder != null && this.A07 != null && (r2 = r1.A09) != null) {
            this.A0E = true;
            if (this.A0C) {
                A02();
                return;
            }
            if (C117295Zj.A1S(AbstractC130695zp.A09, this.A07)) {
                i = 3;
            } else if (C117295Zj.A1S(AbstractC130695zp.A08, this.A07)) {
                i = 4;
            } else {
                return;
            }
            builder.set(CaptureRequest.CONTROL_AF_TRIGGER, 2);
            r2.A04(builder.build(), r6);
            builder.set(CaptureRequest.CONTROL_AF_MODE, Integer.valueOf(i));
            builder.set(CaptureRequest.CONTROL_AF_TRIGGER, C12980iv.A0i());
            r2.A05(builder.build(), r6);
        }
    }

    public synchronized void A07(CaptureRequest.Builder builder, AnonymousClass66I r5, long j) {
        CallableC135906Kg r2 = new CallableC135906Kg(builder, this, r5);
        A02();
        this.A08 = this.A0B.A02("monitor_auto_exposure", r2, j);
    }

    public synchronized void A08(CaptureRequest.Builder builder, AnonymousClass66I r5, long j) {
        CallableC135896Kf r2 = new CallableC135896Kf(builder, this, r5);
        A02();
        this.A08 = this.A0B.A02("reset_focus", r2, j);
    }

    public void A09(EnumC124565pk r2, float[] fArr) {
        if (this.A02 != null) {
            AnonymousClass61K.A00(new AnonymousClass6J2(r2, this, fArr));
        }
    }

    public void A0A(AnonymousClass66I r3) {
        C119085cr r1;
        if (C117295Zj.A1S(AbstractC130695zp.A04, this.A07)) {
            if (C117295Zj.A1S(AbstractC130695zp.A03, this.A07) && (r1 = this.A06) != null && C12970iu.A1Y(r1.A03(AbstractC130685zo.A0N))) {
                this.A09 = true;
                r3.A06 = new AnonymousClass66A(this);
                return;
            }
        }
        r3.A06 = null;
        this.A09 = false;
    }
}
