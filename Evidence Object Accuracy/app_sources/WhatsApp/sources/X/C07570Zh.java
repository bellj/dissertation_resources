package X;

import android.content.Context;
import androidx.work.impl.WorkDatabase;
import java.util.UUID;

/* renamed from: X.0Zh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07570Zh implements AbstractC11950h8 {
    public final AbstractC11450gJ A00;
    public final AbstractC12700iM A01;
    public final AbstractC11500gO A02;

    static {
        C06390Tk.A01("WMFgUpdater");
    }

    public C07570Zh(WorkDatabase workDatabase, AbstractC11450gJ r3, AbstractC11500gO r4) {
        this.A00 = r3;
        this.A02 = r4;
        this.A01 = workDatabase.A0B();
    }

    @Override // X.AbstractC11950h8
    public AbstractFutureC44231yX AcA(Context context, C05360Pg r9, UUID uuid) {
        AnonymousClass040 A00 = AnonymousClass040.A00();
        AbstractC11500gO r0 = this.A02;
        ((C07760a2) r0).A01.execute(new RunnableC10090e1(context, r9, this, A00, uuid));
        return A00;
    }
}
