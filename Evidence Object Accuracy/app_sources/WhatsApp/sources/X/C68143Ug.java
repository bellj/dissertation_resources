package X;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

/* renamed from: X.3Ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68143Ug implements AbstractC117175Yu {
    public final URL A00;
    public final HttpEntity A01;
    public final HttpResponse A02;

    public C68143Ug(URL url, HttpResponse httpResponse) {
        this.A02 = httpResponse;
        this.A01 = httpResponse.getEntity();
        this.A00 = url;
    }

    @Override // X.AbstractC37631mk
    public int A7O() {
        return this.A02.getStatusLine().getStatusCode();
    }

    @Override // X.AbstractC37631mk
    public InputStream AAZ(C18790t3 r2, Integer num, Integer num2) {
        HttpEntity httpEntity = this.A01;
        if (httpEntity != null) {
            return httpEntity.getContent();
        }
        throw C12990iw.A0i("Entity is null");
    }

    @Override // X.AbstractC117175Yu
    public String ABg() {
        HttpEntity httpEntity = this.A01;
        if (httpEntity != null) {
            return EntityUtils.toString(httpEntity);
        }
        return null;
    }

    @Override // X.AbstractC117175Yu
    public String ACk() {
        return ABg();
    }

    @Override // X.AbstractC117175Yu
    public String AGG() {
        return this.A02.getStatusLine().getReasonPhrase();
    }

    @Override // X.AbstractC37631mk
    public String AIP(String str) {
        List AIQ = AIQ(str);
        if (AIQ == null || AIQ.isEmpty()) {
            return null;
        }
        return C12960it.A0g(AIQ, 0);
    }

    @Override // X.AbstractC117175Yu
    public List AIQ(String str) {
        Header[] headers = this.A02.getHeaders(str);
        if (headers == null) {
            return null;
        }
        int length = headers.length;
        ArrayList A0w = C12980iv.A0w(length);
        for (Header header : headers) {
            A0w.add(header.getValue());
        }
        return A0w;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        C65163Ij.A02(this.A01);
    }

    @Override // X.AbstractC37631mk
    public long getContentLength() {
        HttpEntity httpEntity = this.A01;
        if (httpEntity != null) {
            return httpEntity.getContentLength();
        }
        return 0;
    }
}
