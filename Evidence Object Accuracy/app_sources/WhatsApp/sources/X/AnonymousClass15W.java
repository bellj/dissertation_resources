package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.15W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15W implements AbstractC21180x0 {
    public final C14830m7 A00;
    public final AnonymousClass15S A01;
    public final AbstractC20460vn A02;
    public final C20480vp A03;
    public final AnonymousClass15V A04;
    public final AnonymousClass15V A05;
    public final AnonymousClass15R A06;
    public final AnonymousClass15U A07;
    public final AnonymousClass15T A08;
    public final AbstractC14440lR A09;
    public final AbstractC14440lR A0A;
    public final AnonymousClass01H A0B;
    public final ConcurrentHashMap A0C = new ConcurrentHashMap();
    public final ConcurrentLinkedQueue A0D = new ConcurrentLinkedQueue();
    public final ConcurrentNavigableMap A0E = new ConcurrentSkipListMap();
    public final CopyOnWriteArrayList A0F = new CopyOnWriteArrayList();
    public final AtomicInteger A0G = new AtomicInteger(0);

    public AnonymousClass15W(C14830m7 r3, AnonymousClass15S r4, AbstractC20460vn r5, C20480vp r6, AnonymousClass15V r7, AnonymousClass15R r8, AnonymousClass15U r9, AnonymousClass15T r10, AbstractC14440lR r11, AnonymousClass01H r12) {
        this.A00 = r3;
        this.A09 = r11;
        this.A06 = r8;
        this.A02 = r5;
        this.A01 = r4;
        this.A08 = r10;
        this.A07 = r9;
        this.A04 = r7;
        this.A0A = r11;
        this.A0B = r12;
        this.A03 = r6;
        this.A05 = r7;
    }

    public C28631Oi A00(Integer num, int i) {
        Object obj;
        if (num == null) {
            obj = this.A0C.get(Integer.valueOf(i));
        } else {
            obj = this.A0E.get(Long.valueOf((((long) num.intValue()) & 4294967295L) | (((long) i) << 32)));
        }
        return (C28631Oi) obj;
    }

    public C28631Oi A01(Integer num, int i, long j, short s) {
        Object remove;
        if (num == null) {
            remove = this.A0C.remove(Integer.valueOf(i));
        } else {
            remove = this.A0E.remove(Long.valueOf((((long) num.intValue()) & 4294967295L) | (((long) i) << 32)));
        }
        C28631Oi r1 = (C28631Oi) remove;
        if (r1 != null) {
            r1.A01(j, s);
            this.A0G.decrementAndGet();
        }
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if ((r8 % r1) == 0) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r10.A00(r26) == false) goto L_0x0021;
     */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0197  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C28631Oi A02(java.lang.Integer r25, int r26, long r27, boolean r29) {
        /*
        // Method dump skipped, instructions count: 454
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass15W.A02(java.lang.Integer, int, long, boolean):X.1Oi");
    }

    public final void A03(ConcurrentMap concurrentMap, long j) {
        C28631Oi r1;
        if (!concurrentMap.isEmpty()) {
            for (Map.Entry entry : concurrentMap.entrySet()) {
                C28631Oi r6 = (C28631Oi) entry.getValue();
                if (r6 != null) {
                    if (r6.A03 + TimeUnit.MILLISECONDS.toNanos((long) 300000) < j && (r1 = (C28631Oi) concurrentMap.remove(entry.getKey())) != null) {
                        r1.A01(j, 113);
                        this.A0D.add(r1);
                        this.A0G.decrementAndGet();
                    }
                }
            }
            AgE();
        }
    }

    public final void A04(ConcurrentMap concurrentMap, long j) {
        C28631Oi r1;
        if (!concurrentMap.isEmpty()) {
            Iterator it = new HashSet(concurrentMap.keySet()).iterator();
            while (it.hasNext()) {
                Object next = it.next();
                C28631Oi r0 = (C28631Oi) concurrentMap.get(next);
                if (!(r0 == null || !r0.A0G || (r1 = (C28631Oi) concurrentMap.remove(next)) == null)) {
                    r1.A01(j, 630);
                    this.A0D.add(r1);
                    this.A0G.decrementAndGet();
                }
            }
            AgE();
        }
    }

    @Override // X.AbstractC21180x0
    public void A9P(short s, boolean z) {
        if (this.A01.A03()) {
            long nanoTime = System.nanoTime();
            A04(this.A0E, nanoTime);
            A04(this.A0C, nanoTime);
        }
    }

    @Override // X.AbstractC21180x0
    public void A9Q(int i, short s) {
        if (this.A01.A03()) {
            long nanoTime = System.nanoTime();
            A03(this.A0C, nanoTime);
            A03(this.A0E, nanoTime);
        }
    }

    @Override // X.AbstractC21180x0
    public Collection AAh() {
        HashSet hashSet = new HashSet(this.A0C.keySet());
        for (Number number : this.A0E.keySet()) {
            hashSet.add(Integer.valueOf((int) (number.longValue() >> 32)));
        }
        return hashSet;
    }

    @Override // X.AbstractC21180x0
    public boolean AID() {
        return !this.A0D.isEmpty();
    }

    @Override // X.AbstractC21180x0
    public boolean AJj(int i) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            return A00.A0D.get() == -1 || A00.A0F.get() == -1;
        }
        return false;
    }

    @Override // X.AbstractC21180x0
    public void AKu(int i, String str, int i2) {
        C28631Oi A00 = A00(null, 689639794);
        if (A00 != null) {
            A00.A00(1, "scrollSurface", Long.valueOf((long) i2));
        }
    }

    @Override // X.AbstractC21180x0
    public void AKv(int i, String str, long j) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            A00.A00(1, str, Long.valueOf(j));
        }
    }

    @Override // X.AbstractC21180x0
    public void AKw(int i, String str, String str2) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            A00.A00(str2.length(), str, str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKx(int i, String str, boolean z) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            A00.A00(1, str, Boolean.valueOf(z));
        }
    }

    @Override // X.AbstractC21180x0
    public void AKy(String str, int i, int i2, int i3) {
        C28631Oi A00 = A00(Integer.valueOf(i2), i);
        if (A00 != null) {
            A00.A00(1, str, Long.valueOf((long) i3));
        }
    }

    @Override // X.AbstractC21180x0
    public void AKz(String str, int i, int i2, long j) {
        C28631Oi A00 = A00(Integer.valueOf(i2), i);
        if (A00 != null) {
            A00.A00(1, str, Long.valueOf(j));
        }
    }

    @Override // X.AbstractC21180x0
    public void AL0(String str, String str2, int i, int i2) {
        C28631Oi A00 = A00(Integer.valueOf(i2), i);
        if (A00 != null) {
            A00.A00(str2.length(), str, str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL1(String str, int i, int i2, boolean z) {
        C28631Oi A00 = A00(Integer.valueOf(i2), i);
        if (A00 != null) {
            A00.A00(1, str, Boolean.valueOf(z));
        }
    }

    @Override // X.AbstractC21180x0
    public void AL2(String str, double d, int i) {
        AbstractC20460vn r1 = this.A02;
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            r1.AIV(str, d, 689639794);
            return;
        }
        C28631Oi A00 = A00(null, 689639794);
        if (A00 != null) {
            A00.A00(1, str, Double.valueOf(d));
        }
    }

    @Override // X.AbstractC21180x0
    public void AL3(String str, String[] strArr, int i) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            int i2 = 0;
            for (String str2 : strArr) {
                if (str2 == null) {
                    i2++;
                } else {
                    i2 += str2.length();
                }
            }
            A00.A00(i2, "inflated_rows", strArr);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL4(AbstractC29031Pz r6, int i) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            AnonymousClass15V r3 = this.A05;
            r3.A01(A00.A01);
            r3.A05.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r3, r6, A00, 42));
        }
    }

    @Override // X.AbstractC21180x0
    public void AL5(int i, int i2) {
        Integer valueOf = Integer.valueOf(i2);
        C28631Oi r2 = (C28631Oi) this.A0E.remove(Long.valueOf((((long) valueOf.intValue()) & 4294967295L) | (((long) 926875649) << 32)));
        this.A0G.decrementAndGet();
        if (r2 != null) {
            this.A04.A08.remove(Integer.valueOf(r2.A01));
        }
    }

    @Override // X.AbstractC21180x0
    public void AL6(int i, int i2, short s) {
        C28631Oi A01 = A01(Integer.valueOf(i2), i, System.nanoTime(), s);
        if (A01 != null) {
            this.A0D.add(A01);
            AgE();
        }
    }

    @Override // X.AbstractC21180x0
    public void AL7(int i, short s) {
        C28631Oi A01 = A01(null, i, System.nanoTime(), s);
        if (A01 != null) {
            this.A0D.add(A01);
            AgE();
        }
    }

    @Override // X.AbstractC21180x0
    public void AL8(String str, int i, int i2, short s) {
        C28631Oi A01 = A01(Integer.valueOf(i2), i, System.nanoTime(), 213);
        if (A01 != null) {
            AtomicInteger atomicInteger = A01.A0E;
            int i3 = atomicInteger.get() - 1;
            while (true) {
                if (i3 < 0) {
                    this.A02.AZJ(i, "start_foreground_service");
                    break;
                }
                ConcurrentHashMap concurrentHashMap = A01.A0B;
                AnonymousClass1Q0 r1 = (AnonymousClass1Q0) concurrentHashMap.get(Integer.valueOf(i3));
                if (r1 == null || !r1.A01.equals("start_foreground_service")) {
                    i3--;
                } else {
                    long j = r1.A00;
                    A01.A0F.set(j);
                    for (int i4 = atomicInteger.get() - 1; i4 >= 0; i4--) {
                        AnonymousClass1Q0 r0 = (AnonymousClass1Q0) concurrentHashMap.get(Integer.valueOf(i4));
                        if (r0 != null && r0.A00 > j) {
                            concurrentHashMap.remove(Integer.valueOf(i4));
                        }
                    }
                }
            }
            this.A0D.add(A01);
            AgE();
        }
    }

    @Override // X.AbstractC21180x0
    public boolean ALA(int i) {
        C243215b r3 = (C243215b) this.A07;
        AnonymousClass15S r1 = r3.A02;
        if (!r1.A03()) {
            return false;
        }
        if (r1.A01(689639794).A02) {
            return r3.A00(689639794);
        }
        return true;
    }

    @Override // X.AbstractC21180x0
    public void ALB(int i, String str) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            A00.A03(str, System.nanoTime());
        }
    }

    @Override // X.AbstractC21180x0
    public void ALC(int i, String str, int i2) {
        C28631Oi A00 = A00(Integer.valueOf(i2), i);
        if (A00 != null) {
            A00.A03(str, System.nanoTime());
        }
    }

    @Override // X.AbstractC21180x0
    public void ALD(String str, TimeUnit timeUnit, int i, long j) {
        C28631Oi A00 = A00(null, i);
        if (A00 != null) {
            A00.A03(str, timeUnit.toNanos(j));
        }
    }

    @Override // X.AbstractC21180x0
    public void ALE(int i) {
        A02(null, i, System.nanoTime(), true);
    }

    @Override // X.AbstractC21180x0
    public void ALF(int i, int i2) {
        A02(Integer.valueOf(i2), i, System.nanoTime(), true);
    }

    @Override // X.AbstractC21180x0
    public void ALG(int i, String str, String str2) {
        C28631Oi A02 = A02(null, i, System.nanoTime(), true);
        if (A02 != null) {
            A02.A00(str2.length(), str, str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALH(String str, String str2, int i, int i2, boolean z) {
        C28631Oi A02 = A02(Integer.valueOf(i2), i, System.nanoTime(), z);
        if (A02 != null) {
            A02.A00(str2.length(), "perf_origin", str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALI(String str, String str2, TimeUnit timeUnit, int i, long j) {
        C28631Oi A02 = A02(null, i, timeUnit.toNanos(j), true);
        if (A02 != null) {
            A02.A00(str2.length(), "perf_origin", str2);
        }
    }

    @Override // X.AbstractC21180x0
    public Long AZ2() {
        C28631Oi r0 = (C28631Oi) this.A0D.peek();
        if (r0 == null) {
            return null;
        }
        long j = r0.A0F.get();
        if (j != -1) {
            return Long.valueOf(j);
        }
        return null;
    }

    @Override // X.AbstractC21180x0
    public Integer AZ3() {
        C28631Oi r0 = (C28631Oi) this.A0D.peek();
        if (r0 != null) {
            return Integer.valueOf(r0.A01);
        }
        return null;
    }

    @Override // X.AbstractC21180x0
    public String AZK() {
        Object obj;
        Object string;
        boolean z;
        boolean z2;
        String str;
        C20480vp r7 = this.A03;
        C28631Oi r6 = (C28631Oi) this.A0D.poll();
        if (r6 != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                int i = r6.A00;
                jSONObject.put("marker_id", i);
                ConcurrentHashMap concurrentHashMap = r6.A09;
                if (concurrentHashMap.get("subType") != null) {
                    jSONObject.put("da_type", concurrentHashMap.get("subType"));
                }
                Object obj2 = r6.A07;
                if (obj2 != null) {
                    jSONObject.put("instance_id", obj2);
                }
                jSONObject.put("action_id", (short) r6.A0D.get());
                if (r6.A0H) {
                    obj = "per_user";
                } else {
                    obj = "random_sampling";
                }
                jSONObject.put("method", obj);
                jSONObject.put("sample_rate", r6.A02);
                long j = r6.A0F.get();
                long j2 = r6.A03;
                jSONObject.put("duration_ns", j - j2);
                C21680xo r9 = r7.A00.A03;
                synchronized (r9) {
                    string = r9.A01.getString("ab_props:sys:config_key", null);
                }
                jSONObject.put("wa_ab_key2", string);
                jSONObject.put("wa_ab_expo_key", r9.A01());
                ArrayList arrayList = new ArrayList();
                for (Object obj3 : r6.A0B.values()) {
                    if (obj3 != null) {
                        arrayList.add(obj3);
                    }
                }
                Collections.sort(arrayList);
                if (!arrayList.isEmpty()) {
                    JSONArray jSONArray = new JSONArray();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1Q0 r92 = (AnonymousClass1Q0) it.next();
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("name", r92.A01);
                        jSONObject2.put("time_since_start_ns", r92.A00 - j2);
                        jSONArray.put(jSONObject2);
                    }
                    jSONObject.put("points", jSONArray);
                }
                HashMap hashMap = new HashMap();
                ConcurrentHashMap concurrentHashMap2 = r6.A08;
                for (Object obj4 : concurrentHashMap2.keySet()) {
                    Object obj5 = concurrentHashMap2.get(obj4);
                    if (obj5 != null) {
                        hashMap.put(obj4, obj5);
                    }
                }
                if (!hashMap.isEmpty()) {
                    HashMap hashMap2 = new HashMap();
                    for (Map.Entry entry : hashMap.entrySet()) {
                        Class<?> cls = entry.getValue().getClass();
                        List list = (List) hashMap2.get(cls);
                        if (list == null) {
                            list = new ArrayList();
                            hashMap2.put(cls, list);
                        }
                        list.add(entry);
                    }
                    for (Map.Entry entry2 : hashMap2.entrySet()) {
                        Object key = entry2.getKey();
                        if (key == String.class) {
                            str = "annotations";
                        } else if (key == Double.class) {
                            str = "annotations_double";
                        } else if (key == Boolean.class) {
                            str = "annotations_bool";
                        } else if (key == Long.class) {
                            str = "annotations_int";
                        } else if (key == String[].class) {
                            str = "annotations_string_array";
                        } else if (key == double[].class) {
                            str = "annotations_double_array";
                        } else if (key == boolean[].class) {
                            str = "annotations_bool_array";
                        } else if (key == long[].class) {
                            str = "annotations_int_array";
                        } else {
                            StringBuilder sb = new StringBuilder("Unknown class: ");
                            sb.append(key);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        JSONObject jSONObject3 = new JSONObject();
                        boolean isArray = ((Class) entry2.getKey()).isArray();
                        Iterator it2 = ((List) entry2.getValue()).iterator();
                        if (isArray) {
                            while (it2.hasNext()) {
                                Map.Entry entry3 = (Map.Entry) it2.next();
                                jSONObject3.put((String) entry3.getKey(), C20480vp.A00((Class) entry2.getKey(), entry3.getValue()));
                            }
                        } else {
                            while (it2.hasNext()) {
                                Map.Entry entry4 = (Map.Entry) it2.next();
                                Object value = entry4.getValue();
                                C20480vp.A01((Class) entry2.getKey(), value, (String) entry4.getKey(), jSONObject3);
                            }
                        }
                        jSONObject.put(str, jSONObject3);
                    }
                }
                HashMap hashMap3 = new HashMap();
                ConcurrentHashMap concurrentHashMap3 = r6.A0A;
                for (Pair pair : concurrentHashMap3.keySet()) {
                    Object obj6 = concurrentHashMap3.get(pair);
                    if (obj6 != null) {
                        Object obj7 = pair.first;
                        Object obj8 = pair.second;
                        Map map = (Map) hashMap3.get(obj7);
                        if (map == null) {
                            map = new HashMap();
                            hashMap3.put(obj7, map);
                        }
                        map.put(obj8, obj6);
                    }
                }
                if (!hashMap3.isEmpty()) {
                    JSONObject jSONObject4 = new JSONObject();
                    for (Map.Entry entry5 : hashMap3.entrySet()) {
                        if (entry5.getValue() != null) {
                            JSONObject jSONObject5 = new JSONObject();
                            for (Map.Entry entry6 : ((Map) entry5.getValue()).entrySet()) {
                                if (entry6.getValue() != null) {
                                    Class<?> cls2 = entry6.getValue().getClass();
                                    if (cls2 == Double.class) {
                                        AbstractC20460vn r14 = r7.A01;
                                        String str2 = (String) entry6.getKey();
                                        double doubleValue = ((Number) entry6.getValue()).doubleValue();
                                        if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                                            r14.AIV(str2, doubleValue, i);
                                            z2 = false;
                                        } else {
                                            z2 = true;
                                        }
                                        if (!z2) {
                                        }
                                    }
                                    if (cls2 == double[].class) {
                                        AbstractC20460vn r0 = r7.A01;
                                        String str3 = (String) entry6.getKey();
                                        double[] dArr = (double[]) entry6.getValue();
                                        int length = dArr.length;
                                        int i2 = 0;
                                        while (i2 < length) {
                                            double d = dArr[i2];
                                            if (!Double.isNaN(d)) {
                                                i2++;
                                                if (Double.isInfinite(d)) {
                                                }
                                            }
                                            r0.AIV(str3, d, i);
                                            z = false;
                                            break;
                                        }
                                        z = true;
                                        if (!z) {
                                        }
                                    }
                                    if (cls2.isArray()) {
                                        jSONObject5.put((String) entry6.getKey(), C20480vp.A00(cls2, entry6.getValue()));
                                    } else {
                                        C20480vp.A01(cls2, entry6.getValue(), (String) entry6.getKey(), jSONObject5);
                                    }
                                }
                            }
                            jSONObject4.put((String) entry5.getKey(), jSONObject5);
                        }
                    }
                    jSONObject.put("metadata", jSONObject4);
                }
                return jSONObject.toString();
            } catch (JSONException e) {
                r7.A01.AKL(r6.A00, e.getMessage());
            }
        }
        return null;
    }

    @Override // X.AbstractC21180x0
    public void AgE() {
        this.A0A.Ab4((Runnable) this.A0B.get(), "qpl_fs_writer");
    }
}
