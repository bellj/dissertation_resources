package X;

import android.util.Pair;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.ui.NoviPayBloksActivity;

/* renamed from: X.5dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119445dz extends AbstractC58672rA {
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ FingerprintBottomSheet A01;
    public final /* synthetic */ AnonymousClass3FE A02;
    public final /* synthetic */ AnonymousClass61D A03;
    public final /* synthetic */ NoviPayBloksActivity A04;
    public final /* synthetic */ String A05;

    public C119445dz(Pair pair, FingerprintBottomSheet fingerprintBottomSheet, AnonymousClass3FE r3, AnonymousClass61D r4, NoviPayBloksActivity noviPayBloksActivity, String str) {
        this.A04 = noviPayBloksActivity;
        this.A03 = r4;
        this.A05 = str;
        this.A00 = pair;
        this.A02 = r3;
        this.A01 = fingerprintBottomSheet;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        this.A01.A1B();
    }

    @Override // X.AbstractC58672rA
    public void A02() {
        this.A04.A30(this.A01);
    }

    @Override // X.AbstractC58672rA
    public void A04(AnonymousClass02N r3, AnonymousClass21K r4) {
        ((AbstractActivityC121705jc) this.A04).A0N.A08(r3, r4, new byte[1]);
    }

    @Override // X.AbstractC58672rA
    public void A06(byte[] bArr) {
        this.A04.A2w(this.A00, this.A02, this.A03, this.A05);
    }
}
