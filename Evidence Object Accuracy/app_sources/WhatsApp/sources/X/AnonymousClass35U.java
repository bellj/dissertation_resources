package X;

import com.whatsapp.R;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.35U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35U extends AbstractC33641ei {
    public AnonymousClass2VE A00;
    public boolean A01;
    public boolean A02;
    public final C14850m9 A03;
    public final C244415n A04;
    public final PhotoView A05;
    public final AnonymousClass1X7 A06;
    public final C92784Xk A07;
    public final AnonymousClass19O A08;

    public AnonymousClass35U(AnonymousClass12P r14, C14900mE r15, AnonymousClass01d r16, AnonymousClass018 r17, C14850m9 r18, C244415n r19, AnonymousClass1CH r20, AbstractC15340mz r21, C48242Fd r22, AnonymousClass19O r23) {
        super(r14, r15, r16, r17, r20, r22);
        long j;
        double sqrt;
        this.A03 = r18;
        this.A04 = r19;
        this.A08 = r23;
        AnonymousClass009.A05(r21);
        AnonymousClass1X7 r2 = (AnonymousClass1X7) r21;
        this.A06 = r2;
        String A15 = r2.A15();
        if (C15380n4.A0M(r2.A0B())) {
            j = 6750;
        } else if (!r2.A0z.A02) {
            int A00 = AnonymousClass2VC.A00(A15);
            if (A00 >= 89) {
                sqrt = 1.0d;
            } else {
                sqrt = Math.sqrt((double) (A00 / 89));
            }
            j = (long) ((sqrt * 3000.0d) + 4500.0d);
        } else {
            j = 4500;
        }
        this.A07 = new C92784Xk(j);
        C619033a r1 = new C619033a(A02(), this, r22);
        this.A05 = r1;
        ((PhotoView) r1).A01 = 0.0f;
        r1.A0Y = true;
        r1.A0V = false;
        r1.setIsLongpressEnabled(false);
        r1.setId(R.id.status_playback_image);
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        return this.A07.A03;
    }

    @Override // X.AbstractC33641ei
    public void A07() {
        AbstractC33641ei.A00(this.A07, this);
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        this.A07.A02();
        this.A02 = false;
    }
}
