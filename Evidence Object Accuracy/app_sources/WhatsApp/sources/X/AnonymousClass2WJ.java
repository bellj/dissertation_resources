package X;

/* renamed from: X.2WJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2WJ extends AnonymousClass2Vu implements AbstractC51662Vw {
    public final AbstractC14640lm A00;

    public AnonymousClass2WJ(AbstractC14640lm r2) {
        super(r2, 2);
        this.A00 = r2;
    }

    @Override // X.AbstractC51662Vw
    public AbstractC14640lm ADg() {
        return this.A00;
    }
}
