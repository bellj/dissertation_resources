package X;

import android.util.Log;

/* renamed from: X.3cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C70973cB implements AnonymousClass5WN {
    public final /* synthetic */ AnonymousClass5TH A00;
    public final /* synthetic */ C64173En A01;

    public C70973cB(AnonymousClass5TH r1, C64173En r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r3) {
        this.A00.AOM(5);
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r3) {
        C16700pc.A0E(r3, 0);
        if (r3.A00 == 5) {
            Log.e("AvatarEditorLauncher", "should launch as an async action");
        }
        this.A00.AOM(r3.A00);
    }
}
