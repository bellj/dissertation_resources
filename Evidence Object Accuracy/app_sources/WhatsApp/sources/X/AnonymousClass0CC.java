package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.StateSet;

/* renamed from: X.0CC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CC extends AnonymousClass0CD {
    public AnonymousClass036 A00;
    public C013506i A01;

    public AnonymousClass0CC(Resources resources, AnonymousClass0CC r3, AnonymousClass0CE r4) {
        super(resources, r3, r4);
        C013506i r0;
        if (r3 != null) {
            this.A00 = r3.A00;
            r0 = r3.A01;
        } else {
            this.A00 = new AnonymousClass036();
            r0 = new C013506i();
        }
        this.A01 = r0;
    }

    @Override // X.AnonymousClass0CD, X.AnonymousClass09z
    public void A04() {
        this.A00 = this.A00.clone();
        this.A01 = this.A01.clone();
    }

    public int A09(int i) {
        Object obj;
        if (i < 0) {
            return 0;
        }
        C013506i r3 = this.A01;
        int i2 = 0;
        int A00 = AnonymousClass00R.A00(r3.A01, r3.A00, i);
        if (A00 >= 0 && (obj = r3.A02[A00]) != C013506i.A03) {
            i2 = obj;
        }
        return i2.intValue();
    }

    public int A0A(int[] iArr) {
        int A08 = super.A08(iArr);
        if (A08 < 0) {
            return super.A08(StateSet.WILD_CARD);
        }
        return A08;
    }

    @Override // X.AnonymousClass0CD, android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        return new AnonymousClass0CE(null, this);
    }

    @Override // X.AnonymousClass0CD, android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources) {
        return new AnonymousClass0CE(resources, this);
    }
}
