package X;

/* renamed from: X.51e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093051e implements AnonymousClass5T3 {
    public final /* synthetic */ AnonymousClass4PX A00;
    public final /* synthetic */ AnonymousClass4PX A01;

    public C1093051e(AnonymousClass4PX r1, AnonymousClass4PX r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass5T3
    public final boolean Afk(AnonymousClass28D r6) {
        AnonymousClass4PX r4 = this.A01;
        AnonymousClass4PX r3 = this.A00;
        int i = r6.A00;
        Object obj = r4.A02.get(i);
        if (obj != null) {
            r3.A02.put(i, obj);
        }
        Object obj2 = r4.A00.get(i);
        if (obj2 != null) {
            r3.A00.put(i, obj2);
        }
        Object obj3 = r4.A01.get(i);
        if (obj3 == null) {
            return false;
        }
        r3.A01.put(i, obj3);
        return false;
    }
}
