package X;

import com.whatsapp.biz.BusinessProfileExtraFieldsActivity;

/* renamed from: X.41O  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41O extends AnonymousClass2Dn {
    public final /* synthetic */ BusinessProfileExtraFieldsActivity A00;

    public AnonymousClass41O(BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity) {
        this.A00 = businessProfileExtraFieldsActivity;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        this.A00.A2e();
    }
}
