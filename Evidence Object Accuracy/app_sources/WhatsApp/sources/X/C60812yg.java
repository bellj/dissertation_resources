package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60812yg extends AnonymousClass2xj {
    public final TextEmojiLabel A00;

    public C60812yg(Context context, AbstractC13890kV r4, C30331Wz r5) {
        super(context, r4, r5);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.message_text);
        this.A00 = A0U;
        A0U.setText(getMessageString());
        A0U.setLongClickable(AbstractC28491Nn.A07(A0U));
    }

    @Override // X.AnonymousClass1OY
    public int A0m(int i) {
        if (!getFMessage().A0z.A02) {
            return 0;
        }
        return R.drawable.message_unsent;
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        if (!getFMessage().A0z.A02) {
            return 0;
        }
        return R.color.msgStatusTint;
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean A1X = C12960it.A1X(r3, getFMessage());
        super.A1D(r3, z);
        if (z || A1X) {
            TextEmojiLabel textEmojiLabel = this.A00;
            textEmojiLabel.setText(getMessageString());
            textEmojiLabel.setLongClickable(AbstractC28491Nn.A07(textEmojiLabel));
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_revoked_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_revoked_left;
    }

    public String getMessageString() {
        boolean z = getFMessage().A0z.A02;
        int i = R.string.revoked_msg_incoming;
        if (z) {
            i = R.string.revoked_msg_outgoing;
        }
        return C12990iw.A0q(this, i);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_revoked_right;
    }
}
