package X;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0Zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07680Zs implements AbstractC12570i8, AbstractC11960h9, AbstractC12450hw {
    public static final String A08 = C06390Tk.A01("GreedyScheduler");
    public C05930Rn A00;
    public Boolean A01;
    public boolean A02;
    public final Context A03;
    public final AnonymousClass022 A04;
    public final AnonymousClass0Zu A05;
    public final Object A06;
    public final Set A07 = new HashSet();

    @Override // X.AbstractC12570i8
    public boolean AIE() {
        return false;
    }

    public C07680Zs(Context context, C05180Oo r4, AnonymousClass022 r5, AbstractC11500gO r6) {
        this.A03 = context;
        this.A04 = r5;
        this.A05 = new AnonymousClass0Zu(context, this, r6);
        this.A00 = new C05930Rn(r4.A03, this);
        this.A06 = new Object();
    }

    @Override // X.AbstractC12570i8
    public void A73(String str) {
        Boolean bool = this.A01;
        if (bool == null) {
            bool = Boolean.valueOf(AnonymousClass0TK.A00(this.A03));
            this.A01 = bool;
        }
        if (!bool.booleanValue()) {
            C06390Tk.A00().A04(A08, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        if (!this.A02) {
            this.A04.A03.A02(this);
            this.A02 = true;
        }
        C06390Tk.A00().A02(A08, String.format("Cancelling work ID %s", str), new Throwable[0]);
        C05930Rn r2 = this.A00;
        Runnable runnable = (Runnable) r2.A02.remove(str);
        if (runnable != null) {
            ((C07600Zk) r2.A00).A00.removeCallbacks(runnable);
        }
        this.A04.A09(str);
    }

    @Override // X.AbstractC12450hw
    public void AM8(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            C06390Tk.A00().A02(A08, String.format("Constraints met: Scheduling work ID %s", str), new Throwable[0]);
            AnonymousClass022 r3 = this.A04;
            ((C07760a2) r3.A06).A01.execute(new RunnableC09980dp(null, r3, str));
        }
    }

    @Override // X.AbstractC12450hw
    public void AM9(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            C06390Tk.A00().A02(A08, String.format("Constraints not met: Cancelling work ID %s", str), new Throwable[0]);
            this.A04.A09(str);
        }
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        synchronized (this.A06) {
            Set set = this.A07;
            Iterator it = set.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C004401z r5 = (C004401z) it.next();
                if (r5.A0E.equals(str)) {
                    C06390Tk.A00().A02(A08, String.format("Stopping tracking for %s", str), new Throwable[0]);
                    set.remove(r5);
                    this.A05.A01(set);
                    break;
                }
            }
        }
    }

    @Override // X.AbstractC12570i8
    public void AbJ(C004401z... r14) {
        C06390Tk A00;
        String str;
        String str2;
        Boolean bool = this.A01;
        if (bool == null) {
            bool = Boolean.valueOf(AnonymousClass0TK.A00(this.A03));
            this.A01 = bool;
        }
        if (!bool.booleanValue()) {
            C06390Tk.A00().A04(A08, "Ignoring schedule request in a secondary process", new Throwable[0]);
            return;
        }
        if (!this.A02) {
            this.A04.A03.A02(this);
            this.A02 = true;
        }
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (C004401z r10 : r14) {
            long A002 = r10.A00();
            long currentTimeMillis = System.currentTimeMillis();
            if (r10.A0D == EnumC03840Ji.ENQUEUED) {
                if (currentTimeMillis < A002) {
                    C05930Rn r11 = this.A00;
                    Map map = r11.A02;
                    Runnable runnable = (Runnable) map.remove(r10.A0E);
                    if (runnable != null) {
                        ((C07600Zk) r11.A00).A00.removeCallbacks(runnable);
                    }
                    RunnableC09630dG r4 = new RunnableC09630dG(r11, r10);
                    map.put(r10.A0E, r4);
                    ((C07600Zk) r11.A00).A00.postDelayed(r4, r10.A00() - System.currentTimeMillis());
                } else if (!C004101u.A08.equals(r10.A09)) {
                    int i = Build.VERSION.SDK_INT;
                    if (i >= 23 && r10.A09.A04()) {
                        A00 = C06390Tk.A00();
                        str = A08;
                        str2 = "Ignoring WorkSpec %s, Requires device idle.";
                    } else if (i < 24 || !r10.A09.A03()) {
                        hashSet.add(r10);
                        hashSet2.add(r10.A0E);
                    } else {
                        A00 = C06390Tk.A00();
                        str = A08;
                        str2 = "Ignoring WorkSpec %s, Requires ContentUri triggers.";
                    }
                    A00.A02(str, String.format(str2, r10), new Throwable[0]);
                } else {
                    C06390Tk.A00().A02(A08, String.format("Starting work for %s", r10.A0E), new Throwable[0]);
                    AnonymousClass022 r42 = this.A04;
                    ((C07760a2) r42.A06).A01.execute(new RunnableC09980dp(null, r42, r10.A0E));
                }
            }
        }
        synchronized (this.A06) {
            if (!hashSet.isEmpty()) {
                C06390Tk.A00().A02(A08, String.format("Starting tracking for [%s]", TextUtils.join(",", hashSet2)), new Throwable[0]);
                Set set = this.A07;
                set.addAll(hashSet);
                this.A05.A01(set);
            }
        }
    }
}
