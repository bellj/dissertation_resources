package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2Ep  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48172Ep extends C48142Em implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99614ka();
    public String A00;
    public String A01;
    public String A02;

    public C48172Ep(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
    }

    public C48172Ep(String str, String str2, String str3, long j) {
        super(j);
        this.A00 = str;
        this.A01 = str2;
        this.A02 = str3;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A02.equals(((C48172Ep) obj).A02);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A02.hashCode();
    }

    @Override // X.C48142Em, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
    }
}
