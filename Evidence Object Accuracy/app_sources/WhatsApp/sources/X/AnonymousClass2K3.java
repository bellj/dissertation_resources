package X;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.2K3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2K3 {
    public final Integer A00;
    public final List A01;
    public final boolean A02;

    public AnonymousClass2K3(Integer num, List list, boolean z) {
        this.A01 = list;
        this.A00 = num;
        this.A02 = z;
    }

    public JSONObject A00() {
        JSONObject jSONObject = new JSONObject();
        if (this.A02) {
            jSONObject.put("has_catalog", 1);
        }
        Integer num = this.A00;
        if (num != null) {
            jSONObject.put("opening_hours", num);
        }
        List list = this.A01;
        if (list != null) {
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                jSONArray.put(list.get(i));
            }
            jSONObject.put("subcategories", jSONArray);
        }
        if (jSONObject.length() == 0) {
            return null;
        }
        return jSONObject;
    }
}
