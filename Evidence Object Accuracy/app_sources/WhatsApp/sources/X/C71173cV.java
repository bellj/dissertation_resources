package X;

import java.util.Arrays;

/* renamed from: X.3cV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71173cV implements Cloneable {
    public int A00;
    public int[] A01;

    public C71173cV() {
        this.A00 = 0;
        this.A01 = new int[1];
    }

    public C71173cV(int[] iArr, int i) {
        this.A01 = iArr;
        this.A00 = i;
    }

    public final void A00(int i) {
        int[] iArr = this.A01;
        int length = iArr.length;
        if (i > (length << 5)) {
            int[] iArr2 = new int[(i + 31) >> 5];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            this.A01 = iArr2;
        }
    }

    public void A01(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw C12970iu.A0f("Num bits must be between 0 and 32");
        }
        A00(this.A00 + i2);
        while (i2 > 0) {
            i2--;
            A02(C12960it.A1R(i >> i2));
        }
    }

    public void A02(boolean z) {
        A00(this.A00 + 1);
        if (z) {
            int[] iArr = this.A01;
            int i = this.A00;
            int i2 = i >> 5;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.A00++;
    }

    public boolean A03(int i) {
        return ((1 << (i & 31)) & this.A01[i >> 5]) != 0;
    }

    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() {
        return new C71173cV((int[]) this.A01.clone(), this.A00);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof C71173cV) {
            C71173cV r4 = (C71173cV) obj;
            if (this.A00 == r4.A00 && Arrays.equals(this.A01, r4.A01)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.A00 * 31) + Arrays.hashCode(this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        int i = this.A00;
        StringBuilder A0t = C12980iv.A0t((i >> 3) + i + 1);
        for (int i2 = 0; i2 < i; i2++) {
            if ((i2 & 7) == 0) {
                A0t.append(' ');
            }
            char c = '.';
            if (A03(i2)) {
                c = 'X';
            }
            A0t.append(c);
        }
        return A0t.toString();
    }
}
