package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.14Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14Q {
    public final AbstractC15710nm A00;

    public AnonymousClass14Q(AbstractC15710nm r1) {
        this.A00 = r1;
    }

    public String A00(String str) {
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            if (parse.getQueryParameter("oe") == null || parse.getQueryParameter("oh") == null) {
                StringBuilder sb = new StringBuilder("DirectPathUtils/direct_path missing signature or expiry ");
                sb.append(str);
                Log.e(sb.toString());
                this.A00.AaV("DirectPathUtils/verifyDirectPath", null, false);
                return null;
            }
        }
        return str;
    }
}
