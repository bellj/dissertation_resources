package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Base64;
import android.util.JsonWriter;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0yV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22090yV {
    public final C21910yB A00;

    public C22090yV(C21910yB r1) {
        this.A00 = r1;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public static final X.AbstractC30271Wt A00(android.database.Cursor r9) {
        /*
        // Method dump skipped, instructions count: 1194
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22090yV.A00(android.database.Cursor):X.1Wt");
    }

    public long A01(AbstractC30271Wt r13) {
        String str;
        StringWriter stringWriter;
        IOException e;
        String str2;
        JsonWriter jsonWriter;
        StringWriter stringWriter2;
        AnonymousClass009.A00();
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT INTO peer_messages (message_type,key_remote_jid,key_from_me, key_id, timestamp, device_id, data, acked) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            SQLiteStatement sQLiteStatement = A0A.A00;
            sQLiteStatement.clearBindings();
            A0A.A01(1, (long) r13.A0y);
            AnonymousClass1IS r3 = r13.A0z;
            AbstractC14640lm r0 = r3.A00;
            AnonymousClass009.A05(r0);
            A0A.A02(2, r0.getRawString());
            A0A.A01(3, r3.A02 ? 1 : 0);
            A0A.A02(4, r3.A01);
            A0A.A01(5, r13.A0I);
            DeviceJid deviceJid = r13.A00;
            if (deviceJid != null) {
                A0A.A02(6, deviceJid.getRawString());
            }
            if (!(r13 instanceof C34811gk)) {
                if (!(r13 instanceof C34771gg)) {
                    if (r13 instanceof C34821gl) {
                        C34821gl r32 = (C34821gl) r13;
                        stringWriter = new StringWriter();
                        try {
                            JsonWriter jsonWriter2 = new JsonWriter(stringWriter);
                            try {
                                jsonWriter2.beginObject();
                                C34871gq r02 = r32.A00;
                                AnonymousClass009.A05(r02);
                                jsonWriter2.name("appStateSyncKeyShareProtoString").value(Base64.encodeToString(r02.A02(), 2));
                                jsonWriter2.name("isNewlyGeneratedKey").value(r32.A01);
                                jsonWriter2.endObject();
                                jsonWriter2.flush();
                                jsonWriter2.close();
                            } finally {
                                try {
                                    jsonWriter2.close();
                                } catch (Throwable unused) {
                                }
                            }
                        } catch (IOException e2) {
                            e = e2;
                            str2 = "FMessageAppStateSyncKeyShare/writeData failed";
                        }
                    } else if (!(r13 instanceof C34831gm)) {
                        C34841gn r11 = (C34841gn) r13;
                        stringWriter2 = new StringWriter();
                        try {
                            jsonWriter = new JsonWriter(stringWriter2);
                            try {
                                jsonWriter.beginObject();
                                jsonWriter.name("collection_names").beginArray();
                                for (String str3 : r11.A01) {
                                    jsonWriter.value(str3);
                                }
                                jsonWriter.endArray();
                                jsonWriter.name("timestamp").value(r11.A0I);
                                jsonWriter.endObject();
                                jsonWriter.flush();
                                jsonWriter.close();
                            } finally {
                            }
                        } catch (IOException e3) {
                            e = e3;
                            str2 = "FMessageAppStateFatalExceptionNotification/writeData failed";
                        }
                    } else {
                        C34831gm r1 = (C34831gm) r13;
                        stringWriter2 = new StringWriter();
                        try {
                            jsonWriter = new JsonWriter(stringWriter2);
                            try {
                                jsonWriter.beginObject();
                                jsonWriter.name("key-ids").beginArray();
                                for (AnonymousClass1JR r03 : r1.A00) {
                                    jsonWriter.value(Base64.encodeToString(r03.A02().A02(), 2));
                                }
                                jsonWriter.endArray();
                                jsonWriter.endObject();
                                jsonWriter.flush();
                                jsonWriter.close();
                            } finally {
                            }
                        } catch (IOException e4) {
                            e = e4;
                            str2 = "FMessageAppStateSyncKeyRequest/writeData failed";
                        }
                    }
                    Log.e(str2, e);
                    str = null;
                    A0A.A02(7, str);
                    A0A.A01(8, 0);
                    r13.A11 = sQLiteStatement.executeInsert();
                    long j = r13.A11;
                    A02.close();
                    return j;
                }
                C34771gg r33 = (C34771gg) r13;
                stringWriter2 = new StringWriter();
                try {
                    JsonWriter jsonWriter3 = new JsonWriter(stringWriter2);
                    try {
                        jsonWriter3.beginObject();
                        if (!TextUtils.isEmpty(r33.A0B)) {
                            jsonWriter3.name("direct_path").value(r33.A0B);
                        }
                        if (!TextUtils.isEmpty(r33.A0D)) {
                            jsonWriter3.name("media_hash").value(r33.A0D);
                        }
                        if (!TextUtils.isEmpty(r33.A0C)) {
                            jsonWriter3.name("enc_media_hash").value(r33.A0C);
                        }
                        if (!TextUtils.isEmpty(r33.A0E)) {
                            jsonWriter3.name("original-msg-id").value(r33.A0E);
                        }
                        if (!TextUtils.isEmpty(r33.A0G)) {
                            jsonWriter3.name("session_id").value(r33.A0G);
                        }
                        if (!TextUtils.isEmpty(r33.A0F)) {
                            jsonWriter3.name("reg_attempt_id").value(r33.A0F);
                        }
                        jsonWriter3.name("file_length").value(r33.A05).name("sync_type").value((long) r33.A03).name("chunk_order").value((long) r33.A00).name("progress").value((long) r33.A01).name("retries").value((long) r33.A02).name("latest_msg_id").value(r33.A06).name("oldest_msg_id").value(r33.A08).name("oldest_msg_id_to_sync").value(r33.A09).name("chats_count").value(r33.A04).name("messages_count").value(r33.A07).name("oldest_msg_to_sync_timestamp").value(r33.A0A);
                        byte[] bArr = r33.A0H;
                        if (bArr != null) {
                            jsonWriter3.name("key_data").value(Base64.encodeToString(bArr, 2));
                        }
                        jsonWriter3.endObject();
                        jsonWriter3.flush();
                        jsonWriter3.close();
                    } finally {
                        try {
                            jsonWriter3.close();
                        } catch (Throwable unused2) {
                        }
                    }
                } catch (IOException e5) {
                    e = e5;
                    str2 = "FMessageHistorySyncNotification/writeData failed";
                }
                str = stringWriter2.toString();
                A0A.A02(7, str);
                A0A.A01(8, 0);
                r13.A11 = sQLiteStatement.executeInsert();
                long j = r13.A11;
                A02.close();
                return j;
            }
            C34811gk r34 = (C34811gk) r13;
            stringWriter = new StringWriter();
            try {
                jsonWriter = new JsonWriter(stringWriter);
                try {
                    jsonWriter.beginObject().name("security_notification_enabled").value(r34.A00).endObject();
                    jsonWriter.close();
                } finally {
                    try {
                        jsonWriter.close();
                    } catch (Throwable unused3) {
                    }
                }
            } catch (IOException e6) {
                e = e6;
                str2 = "FMessageInitialSecurityNotificationSettingSync/writeData failed";
            }
            str = stringWriter.toString();
            A0A.A02(7, str);
            A0A.A01(8, 0);
            r13.A11 = sQLiteStatement.executeInsert();
            long j = r13.A11;
            A02.close();
            return j;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused4) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        if (r1 != null) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC30271Wt A02(long r7) {
        /*
            r6 = this;
            X.AnonymousClass009.A00()
            X.0yB r0 = r6.A00
            X.0on r5 = r0.get()
            X.0op r4 = r5.A03     // Catch: all -> 0x0037
            java.lang.String r3 = "SELECT _id, message_type, key_remote_jid, key_from_me, key_id, timestamp, device_id, data, acked FROM peer_messages WHERE _id = ?"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0037
            r1 = 0
            java.lang.String r0 = java.lang.String.valueOf(r7)     // Catch: all -> 0x0037
            r2[r1] = r0     // Catch: all -> 0x0037
            android.database.Cursor r1 = r4.A09(r3, r2)     // Catch: all -> 0x0037
            if (r1 == 0) goto L_0x002d
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0028
            if (r0 == 0) goto L_0x002d
            X.1Wt r0 = A00(r1)     // Catch: all -> 0x0028
            goto L_0x0030
        L_0x0028:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x002c
        L_0x002c:
            throw r0     // Catch: all -> 0x0037
        L_0x002d:
            r0 = 0
            if (r1 == 0) goto L_0x0033
        L_0x0030:
            r1.close()     // Catch: all -> 0x0037
        L_0x0033:
            r5.close()
            return r0
        L_0x0037:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x003b
        L_0x003b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22090yV.A02(long):X.1Wt");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
        if (r1 != null) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC30271Wt A03(com.whatsapp.jid.DeviceJid r7, java.lang.String r8) {
        /*
            r6 = this;
            X.AnonymousClass009.A00()
            X.0yB r0 = r6.A00
            X.0on r4 = r0.get()
            X.0op r5 = r4.A03     // Catch: all -> 0x0041
            java.lang.String r3 = "SELECT _id, message_type, key_remote_jid, key_from_me, key_id, timestamp, device_id, data, acked FROM peer_messages WHERE device_id = ? AND key_from_me = ? AND key_id = ?"
            r0 = 3
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0041
            java.lang.String r1 = r7.getRawString()     // Catch: all -> 0x0041
            r0 = 0
            r2[r0] = r1     // Catch: all -> 0x0041
            r1 = 1
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch: all -> 0x0041
            r2[r1] = r0     // Catch: all -> 0x0041
            r0 = 2
            r2[r0] = r8     // Catch: all -> 0x0041
            android.database.Cursor r1 = r5.A09(r3, r2)     // Catch: all -> 0x0041
            if (r1 == 0) goto L_0x0037
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0032
            if (r0 == 0) goto L_0x0037
            X.1Wt r0 = A00(r1)     // Catch: all -> 0x0032
            goto L_0x003a
        L_0x0032:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0036
        L_0x0036:
            throw r0     // Catch: all -> 0x0041
        L_0x0037:
            r0 = 0
            if (r1 == 0) goto L_0x003d
        L_0x003a:
            r1.close()     // Catch: all -> 0x0041
        L_0x003d:
            r4.close()
            return r0
        L_0x0041:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0045
        L_0x0045:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22090yV.A03(com.whatsapp.jid.DeviceJid, java.lang.String):X.1Wt");
    }

    public List A04(byte b) {
        AnonymousClass009.A00();
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id, message_type, key_remote_jid, key_from_me, key_id, timestamp, device_id, data, acked FROM peer_messages WHERE message_type = ? ", new String[]{String.valueOf((int) b)});
            while (A09.moveToNext()) {
                AbstractC30271Wt A00 = A00(A09);
                if (A00 != null) {
                    arrayList.add(A00);
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(DeviceJid deviceJid) {
        AnonymousClass009.A00();
        C16310on A02 = this.A00.A02();
        try {
            A02.A03.A01("peer_messages", "device_id = ?", new String[]{deviceJid.getRawString()});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A06(List list) {
        if (list.size() != 0) {
            AnonymousClass009.A00();
            String[] strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                strArr[i] = String.valueOf(list.get(i));
            }
            C29841Uw r1 = new C29841Uw(strArr, 975);
            C16310on A02 = this.A00.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                Iterator it = r1.iterator();
                while (it.hasNext()) {
                    String[] strArr2 = (String[]) it.next();
                    C16330op r3 = A02.A03;
                    int length = strArr2.length;
                    StringBuilder sb = new StringBuilder("DELETE FROM peer_messages WHERE _id IN ( ");
                    sb.append(TextUtils.join(",", Collections.nCopies(length, "?")));
                    sb.append(" )");
                    r3.A0C(sb.toString(), strArr2);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
