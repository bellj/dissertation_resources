package X;

/* renamed from: X.10a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C230110a extends AbstractC17250qV {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15550nR A02;
    public final AnonymousClass10T A03;
    public final C20730wE A04;
    public final C15650ng A05;
    public final AnonymousClass10Y A06;
    public final AnonymousClass10Z A07;
    public final C22140ya A08;

    public C230110a(AbstractC15710nm r9, C15570nT r10, C15550nR r11, AnonymousClass10T r12, C20730wE r13, C15650ng r14, AnonymousClass10Y r15, C17220qS r16, C17230qT r17, AnonymousClass10Z r18, C22140ya r19, AbstractC14440lR r20) {
        super(r9, r16, r17, r20, new int[]{189}, true);
        this.A00 = r9;
        this.A01 = r10;
        this.A02 = r11;
        this.A05 = r14;
        this.A06 = r15;
        this.A03 = r12;
        this.A04 = r13;
        this.A07 = r18;
        this.A08 = r19;
    }
}
