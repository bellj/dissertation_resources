package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3V2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3V2 implements AnonymousClass285 {
    public final /* synthetic */ int A00 = 6;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ Intent A02;
    public final /* synthetic */ C14900mE A03;
    public final /* synthetic */ C14580lf A04;
    public final /* synthetic */ AnonymousClass19T A05;
    public final /* synthetic */ UserJid A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ boolean A08;

    public AnonymousClass3V2(Context context, Intent intent, C14900mE r4, C14580lf r5, AnonymousClass19T r6, UserJid userJid, String str, boolean z) {
        this.A07 = str;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = userJid;
        this.A08 = z;
        this.A01 = context;
        this.A02 = intent;
        this.A04 = r5;
    }

    @Override // X.AnonymousClass285
    public void AQP(String str, int i) {
        if (this.A07.equals(str)) {
            C14900mE r3 = this.A03;
            r3.A02.post(new RunnableBRunnable0Shape10S0200000_I1(this, 47, this.A05));
            this.A04.A02(Boolean.FALSE);
        }
    }

    @Override // X.AnonymousClass285
    public void AQQ(AnonymousClass4TA r9, String str) {
        String str2 = this.A07;
        if (str2.equals(str)) {
            C14900mE r3 = this.A03;
            r3.A02.post(new RunnableBRunnable0Shape10S0200000_I1(this, 48, this.A05));
            AnonymousClass283.A02(this.A01, this.A02, this.A06, null, null, str2, this.A00, this.A08);
            this.A04.A02(Boolean.TRUE);
        }
    }
}
