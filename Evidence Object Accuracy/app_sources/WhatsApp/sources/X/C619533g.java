package X;

import android.util.JsonReader;
import com.whatsapp.util.Log;
import java.util.zip.ZipInputStream;

/* renamed from: X.33g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619533g extends AnonymousClass5BB {
    public final C15740np A00;
    public final ZipInputStream A01;

    public C619533g(JsonReader jsonReader, C15740np r2, ZipInputStream zipInputStream) {
        super(jsonReader);
        this.A01 = zipInputStream;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass5BB
    public /* bridge */ /* synthetic */ Object A01(JsonReader jsonReader) {
        String str;
        C15740np r5 = this.A00;
        AnonymousClass009.A05(r5);
        jsonReader.beginObject();
        String str2 = null;
        String str3 = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("iv")) {
                str3 = jsonReader.nextString();
            } else if (!nextName.equals("path")) {
                jsonReader.skipValue();
            } else {
                str2 = jsonReader.nextString();
            }
        }
        jsonReader.endObject();
        if (str2 == null) {
            str = "EncFileInfo/fromJson; file path is null, skipping...";
        } else if (str3 != null) {
            return new C90444Nx(r5.A02(str2), str3);
        } else {
            str = "EncFileInfo/fromJson; file IV is null, skipping...";
        }
        Log.e(str);
        return null;
    }

    @Override // X.AnonymousClass5BB
    public boolean A03(JsonReader jsonReader) {
        AnonymousClass009.A05(this.A01);
        AnonymousClass009.A05(this.A00);
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if ("files".equals(jsonReader.nextName())) {
                jsonReader.beginArray();
                return true;
            }
            jsonReader.skipValue();
        }
        return false;
    }

    @Override // X.AnonymousClass5BB, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        super.close();
        this.A01.close();
    }
}
