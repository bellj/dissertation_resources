package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03020Fu extends AnonymousClass0ZM {
    @Override // X.AbstractC12410hs
    public float ADC(View view, ViewGroup viewGroup) {
        return view.getTranslationY() - ((float) viewGroup.getHeight());
    }
}
