package X;

import android.hardware.camera2.CameraCaptureSession;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;

/* renamed from: X.5Zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117425Zw extends CameraCaptureSession.StateCallback {
    public C1310360y A00;
    public final /* synthetic */ AnonymousClass66N A01;

    public C117425Zw(AnonymousClass66N r1) {
        this.A01 = r1;
    }

    public static void A00(AnonymousClass66N r1, C1310360y r2, int i, int i2) {
        if (i == i2) {
            r1.A03 = 0;
            r1.A05 = Boolean.TRUE;
            r1.A04 = r2;
            r1.A02.A01();
        }
    }

    public final C1310360y A01(CameraCaptureSession cameraCaptureSession) {
        C1310360y r1 = this.A00;
        if (r1 != null && r1.A00 == cameraCaptureSession) {
            return r1;
        }
        C1310360y r12 = new C1310360y(cameraCaptureSession);
        this.A00 = r12;
        return r12;
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onActive(CameraCaptureSession cameraCaptureSession) {
        super.onActive(cameraCaptureSession);
        AnonymousClass66N r0 = this.A01;
        A01(cameraCaptureSession);
        C125445rG r1 = r0.A00;
        if (r1 != null) {
            r1.A00.A0N.A00(new C118905cZ(), "camera_session_active", new IDxCallableShape15S0100000_3_I1(r1, 15));
        }
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onClosed(CameraCaptureSession cameraCaptureSession) {
        super.onClosed(cameraCaptureSession);
        AnonymousClass66N r3 = this.A01;
        A00(r3, A01(cameraCaptureSession), r3.A03, 2);
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
        AnonymousClass66N r2 = this.A01;
        A01(cameraCaptureSession);
        if (r2.A03 == 1) {
            r2.A03 = 0;
            r2.A05 = Boolean.FALSE;
            r2.A02.A01();
        }
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onConfigured(CameraCaptureSession cameraCaptureSession) {
        AnonymousClass66N r3 = this.A01;
        A00(r3, A01(cameraCaptureSession), r3.A03, 1);
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onReady(CameraCaptureSession cameraCaptureSession) {
        super.onReady(cameraCaptureSession);
        AnonymousClass66N r3 = this.A01;
        A00(r3, A01(cameraCaptureSession), r3.A03, 3);
    }
}
