package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.WaEditText;

/* renamed from: X.1lA  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1lA extends WaEditText {
    public AnonymousClass1lA(Context context) {
        super(context);
        A02();
    }

    public AnonymousClass1lA(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AnonymousClass1lA(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }
}
