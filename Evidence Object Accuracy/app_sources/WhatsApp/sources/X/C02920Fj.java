package X;

/* renamed from: X.0Fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02920Fj extends AnonymousClass0OL {
    public C02920Fj() {
        super(6, 7);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        ((AnonymousClass0ZE) r3).A00.execSQL("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
    }
}
