package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1HM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HM implements AbstractC20260vT, AnonymousClass01p {
    public final AtomicReference A00 = new AtomicReference("D");

    public AnonymousClass1HM(C18640sm r3) {
        r3.A03(this);
    }

    @Override // X.AnonymousClass01p
    public String AAR() {
        return (String) this.A00.get();
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r3) {
        String str = "D";
        if (r3.A01) {
            if (r3.A03) {
                str = "M";
            } else if (r3.A04) {
                str = "W";
            }
        }
        this.A00.set(str);
    }
}
