package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.32b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617532b extends C36351jk {
    public final /* synthetic */ C20710wC A00;
    public final /* synthetic */ File A01;
    public final /* synthetic */ File A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C617532b(C14830m7 r10, C21320xE r11, C15650ng r12, C20710wC r13, C20710wC r14, C63323Bd r15, C22140ya r16, C14860mA r17, File file, File file2) {
        super(r10, r11, r12, r14, r15, r16, r17);
        this.A00 = r13;
        this.A01 = file;
        this.A02 = file2;
    }

    @Override // X.C36351jk, X.AbstractC36361jl
    public void AWy(C15580nU r10, C47572Bl r11) {
        super.AWy(r10, r11);
        C20710wC r6 = this.A00;
        C15370n3 A0B = r6.A0B.A0B(r10);
        AnonymousClass10T r8 = r6.A0E;
        File A00 = r8.A00(A0B);
        AnonymousClass009.A05(A00);
        File file = this.A01;
        if (!file.renameTo(A00)) {
            StringBuilder A0j = C12960it.A0j("group/create again, failed to rename ");
            A0j.append(file.getAbsolutePath());
            A0j.append(" to ");
            Log.w(C12960it.A0d(A00.getAbsolutePath(), A0j));
        }
        File A01 = r8.A01(A0B);
        AnonymousClass009.A05(A01);
        File file2 = this.A02;
        if (!file2.renameTo(A01)) {
            StringBuilder A0j2 = C12960it.A0j("group/create again, failed to rename ");
            A0j2.append(file2.getAbsolutePath());
            A0j2.append(" to ");
            Log.w(C12960it.A0d(A01.getAbsolutePath(), A0j2));
        }
        r6.A0q.A09(A0B);
    }
}
