package X;

/* renamed from: X.59Z  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass59Z implements AbstractC116245Ur {
    public final /* synthetic */ AnonymousClass341 A00;

    public AnonymousClass59Z(AnonymousClass341 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r2, Integer num, int i) {
        AbstractC116245Ur r0 = this.A00.A06;
        if (r0 != null) {
            r0.AWl(r2, num, i);
        }
    }
}
