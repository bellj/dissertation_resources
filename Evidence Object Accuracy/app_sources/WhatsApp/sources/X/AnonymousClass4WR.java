package X;

/* renamed from: X.4WR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4WR {
    public final boolean A00;

    public AnonymousClass4WR(boolean z) {
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((AnonymousClass4WR) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return C12980iv.A0B(Boolean.valueOf(this.A00), new Object[1], 0);
    }
}
