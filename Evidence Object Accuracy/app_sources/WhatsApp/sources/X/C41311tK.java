package X;

import java.util.Map;
import java.util.Set;

/* renamed from: X.1tK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41311tK {
    public final AnonymousClass1JO A00;
    public final AnonymousClass1V8 A01;
    public final String A02;
    public final Map A03;
    public final Map A04;

    public /* synthetic */ C41311tK(AnonymousClass1V8 r3, String str, Map map, Map map2, Set set) {
        this.A02 = str;
        this.A01 = r3;
        AnonymousClass1YJ r1 = new AnonymousClass1YJ();
        Set set2 = r1.A00;
        AnonymousClass009.A05(set2);
        set2.addAll(set);
        this.A00 = r1.A00();
        this.A04 = map;
        this.A03 = map2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C41311tK)) {
            return false;
        }
        C41311tK r4 = (C41311tK) obj;
        if (!this.A02.equals(r4.A02) || !this.A00.equals(r4.A00) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A02.hashCode() ^ this.A00.hashCode()) ^ this.A01.hashCode();
    }
}
