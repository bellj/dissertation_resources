package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.5lk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122615lk extends AbstractC118835cS {
    public final ImageView A00;
    public final LinearLayout A01;

    public C122615lk(View view) {
        super(view);
        this.A01 = C117305Zk.A07(view, R.id.payment_support_container);
        this.A00 = C12970iu.A0K(view, R.id.payment_support_icon);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        this.A01.setOnClickListener(((C123105mc) r4).A00);
        ImageView imageView = this.A00;
        AnonymousClass2GE.A05(imageView.getContext(), imageView, R.color.settings_icon);
    }
}
