package X;

import com.whatsapp.payments.ui.NoviAmountEntryActivity;

/* renamed from: X.5bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118395bk extends AnonymousClass0Yo {
    public final /* synthetic */ NoviAmountEntryActivity A00;
    public final /* synthetic */ C128375w0 A01;

    public C118395bk(NoviAmountEntryActivity noviAmountEntryActivity, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = noviAmountEntryActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123475nD.class)) {
            C128375w0 r0 = this.A01;
            C14900mE r3 = r0.A02;
            C16590pI r4 = r0.A0B;
            C129675y7 r13 = r0.A0p;
            AnonymousClass018 r5 = r0.A0C;
            AnonymousClass60Y r8 = r0.A0c;
            C17070qD r6 = r0.A0V;
            AnonymousClass61F r9 = r0.A0d;
            C129685y8 r12 = r0.A0l;
            C129705yA r11 = r0.A0j;
            return new C123475nD(r3, r4, r5, r6, new C129865yQ(r0.A01, this.A00, r0.A0S), r8, r9, r0.A0h, r11, r12, r13);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviDepositAmountEntryViewModel");
    }
}
