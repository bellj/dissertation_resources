package X;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30801Yw extends AbstractC30781Yu {
    public static final Parcelable.Creator CREATOR = new C468627z();
    public C30821Yy A00;
    public final C30821Yy A01;
    public final String A02;
    public final boolean A03;
    public final String[] A04;

    @Override // X.AbstractC30791Yv
    public int AH1(AnonymousClass018 r2) {
        return 2;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C30801Yw(Parcel parcel) {
        super(1, parcel);
        boolean z = true;
        this.A03 = parcel.readInt() != 1 ? false : z;
        this.A02 = parcel.readString();
        String[] strArr = new String[parcel.readInt()];
        parcel.readStringArray(strArr);
        this.A04 = strArr;
        this.A00 = (C30821Yy) parcel.readParcelable(C30821Yy.class.getClassLoader());
        this.A01 = (C30821Yy) parcel.readParcelable(C30821Yy.class.getClassLoader());
    }

    public C30801Yw(String str, String str2, String str3, BigDecimal bigDecimal, BigDecimal bigDecimal2, String[] strArr, int i, int i2, int i3, boolean z) {
        super(str, str2, 1, i, i2, i3);
        this.A03 = z;
        this.A02 = str3;
        this.A04 = strArr;
        this.A00 = new C30821Yy(bigDecimal, i2);
        this.A01 = new C30821Yy(bigDecimal2, i2);
    }

    public C30801Yw(JSONObject jSONObject) {
        super(jSONObject);
        String[] strArr;
        this.A03 = jSONObject.optBoolean("isStable");
        this.A02 = jSONObject.optString("defaultMatchingFiat");
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("matchingFiats");
            strArr = new String[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                strArr[i] = jSONArray.getString(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            strArr = new String[0];
        }
        this.A04 = strArr;
        JSONObject optJSONObject = jSONObject.optJSONObject("maxValue");
        int i2 = super.A01;
        this.A00 = C30821Yy.A00(optJSONObject.optString("amount", ""), i2);
        this.A01 = C30821Yy.A00(jSONObject.optJSONObject("minValue").optString("amount", ""), i2);
    }

    public boolean A00(AbstractC30791Yv r3) {
        return this.A03 && this.A02.equals(((AbstractC30781Yu) r3).A04);
    }

    @Override // X.AbstractC30791Yv
    public String AA8(AnonymousClass018 r4, C30821Yy r5) {
        return C30831Yz.A03(r4, this.A05, r5.A00, false);
    }

    @Override // X.AbstractC30791Yv
    public String AA9(AnonymousClass018 r3, BigDecimal bigDecimal) {
        return C30831Yz.A03(r3, this.A05, bigDecimal, false);
    }

    @Override // X.AbstractC30791Yv
    public String AAA(AnonymousClass018 r4, C30821Yy r5, int i) {
        return C30831Yz.A03(r4, this.A05, r5.A00, true);
    }

    @Override // X.AbstractC30791Yv
    public String AAB(AnonymousClass018 r3, BigDecimal bigDecimal, int i) {
        return C30831Yz.A03(r3, this.A05, bigDecimal, true);
    }

    @Override // X.AbstractC30791Yv
    public BigDecimal AAD(AnonymousClass018 r4, String str) {
        DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance(AnonymousClass018.A00(r4.A00));
        try {
            return new BigDecimal(decimalFormat.parse(str.replace(String.valueOf(decimalFormat.getDecimalFormatSymbols().getGroupingSeparator()), "")).toString());
        } catch (ParseException unused) {
            Log.e("PAY: CryptoCurrency/fromString: Currency parse threw: ");
            try {
                return new BigDecimal(str);
            } catch (NumberFormatException unused2) {
                Log.e("PAY: CryptoCurrency/fromString: Backup currency parse threw: ");
                return null;
            }
        }
    }

    @Override // X.AbstractC30791Yv
    public CharSequence ABy(Context context) {
        return ABz(context, 0);
    }

    @Override // X.AbstractC30791Yv
    public CharSequence ABz(Context context, int i) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("@");
        Typeface A02 = AnonymousClass00X.A02(context);
        if (A02 != null) {
            spannableStringBuilder.setSpan(new AnonymousClass1Z3(A02), 0, "@".length(), 0);
        }
        return spannableStringBuilder;
    }

    @Override // X.AbstractC30791Yv
    public C30821Yy AEA() {
        return this.A00;
    }

    @Override // X.AbstractC30791Yv
    public C30821Yy AEV() {
        return this.A01;
    }

    @Override // X.AbstractC30791Yv
    public void AcK(C30821Yy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC30781Yu, X.AbstractC30791Yv
    public JSONObject Aew() {
        JSONObject Aew = super.Aew();
        try {
            Aew.put("isStable", this.A03);
            Aew.put("defaultMatchingFiat", this.A02);
            String[] strArr = this.A04;
            int length = strArr.length;
            Aew.put("matchingFiatsLength", length);
            JSONArray jSONArray = new JSONArray();
            for (String str : strArr) {
                jSONArray.put(str);
            }
            Aew.put("matchingFiats", jSONArray);
            Aew.put("maxValue", this.A00.A01());
            Aew.put("minValue", this.A01.A01());
            return Aew;
        } catch (JSONException e) {
            Log.e("PAY: CryptoCurrency toJsonObject threw: ", e);
            return Aew;
        }
    }

    @Override // X.AbstractC30781Yu, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C30801Yw)) {
            return false;
        }
        C30801Yw r4 = (C30801Yw) obj;
        if (!super.equals(r4) || this.A03 != r4.A03 || !this.A01.equals(r4.A01) || !this.A00.equals(r4.A00) || !this.A02.equals(r4.A02) || !Arrays.equals(this.A04, r4.A04)) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC30781Yu, java.lang.Object
    public int hashCode() {
        return super.hashCode() + ((!this.A03 ? 1 : 0) * 31) + (this.A02.hashCode() * 31) + (Arrays.hashCode(this.A04) * 31) + (this.A01.hashCode() * 31) + (this.A00.hashCode() * 31);
    }

    @Override // X.AbstractC30781Yu, X.AbstractC30791Yv, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeString(this.A02);
        String[] strArr = this.A04;
        parcel.writeInt(strArr.length);
        parcel.writeStringArray(strArr);
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
    }
}
