package X;

/* renamed from: X.0FV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FV extends AbstractC05330Pd {
    public final /* synthetic */ C07730Zz A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "DELETE from WorkProgress where work_spec_id=?";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FV(AnonymousClass0QN r1, C07730Zz r2) {
        super(r1);
        this.A00 = r2;
    }
}
