package X;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0AA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AA extends Drawable implements Animatable, Drawable.Callback {
    public float A00;
    public int A01;
    public AnonymousClass0K0 A02;
    public AbstractC11510gP A03;
    public C05540Py A04;
    public AnonymousClass0K1 A05;
    public C04980Nu A06;
    public C06000Ru A07;
    public AnonymousClass0HG A08;
    public String A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final ValueAnimator.AnimatorUpdateListener A0I;
    public final Matrix A0J = new Matrix();
    public final AnonymousClass09T A0K;
    public final ArrayList A0L;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public AnonymousClass0AA() {
        AnonymousClass09T r3 = new AnonymousClass09T();
        this.A0K = r3;
        this.A00 = 1.0f;
        this.A0H = true;
        this.A0B = false;
        this.A0G = false;
        this.A0L = new ArrayList();
        C06670Uo r1 = new C06670Uo(this);
        this.A0I = r1;
        this.A01 = 255;
        this.A0D = false;
        r3.addUpdateListener(r1);
    }

    public void A00() {
        AnonymousClass09T r1 = this.A0K;
        if (r1.A07) {
            r1.cancel();
        }
        this.A04 = null;
        this.A08 = null;
        this.A07 = null;
        r1.A06 = null;
        r1.A02 = -2.14748365E9f;
        r1.A01 = 2.14748365E9f;
        invalidateSelf();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r2.getRepeatCount() == 0) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r3 = this;
            X.0HG r0 = r3.A08
            if (r0 != 0) goto L_0x000f
            java.util.ArrayList r1 = r3.A0L
            X.0a3 r0 = new X.0a3
            r0.<init>(r3)
            r1.add(r0)
        L_0x000e:
            return
        L_0x000f:
            boolean r0 = r3.A0H
            if (r0 != 0) goto L_0x001f
            boolean r0 = r3.A0B
            if (r0 != 0) goto L_0x001f
            X.09T r2 = r3.A0K
            int r0 = r2.getRepeatCount()
            if (r0 != 0) goto L_0x0024
        L_0x001f:
            X.09T r2 = r3.A0K
            r2.A04()
        L_0x0024:
            boolean r0 = r3.A0H
            if (r0 != 0) goto L_0x000e
            boolean r0 = r3.A0B
            if (r0 != 0) goto L_0x000e
            float r1 = r2.A03
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x003f
            float r0 = r2.A01()
        L_0x0037:
            int r0 = (int) r0
            r3.A05(r0)
            r2.A02()
            return
        L_0x003f:
            float r0 = r2.A00()
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AA.A01():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r2.getRepeatCount() == 0) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r3 = this;
            X.0HG r0 = r3.A08
            if (r0 != 0) goto L_0x000f
            java.util.ArrayList r1 = r3.A0L
            X.0a4 r0 = new X.0a4
            r0.<init>(r3)
            r1.add(r0)
        L_0x000e:
            return
        L_0x000f:
            boolean r0 = r3.A0H
            if (r0 != 0) goto L_0x001f
            boolean r0 = r3.A0B
            if (r0 != 0) goto L_0x001f
            X.09T r2 = r3.A0K
            int r0 = r2.getRepeatCount()
            if (r0 != 0) goto L_0x0042
        L_0x001f:
            X.09T r2 = r3.A0K
            r0 = 1
            r2.A07 = r0
            r2.A05()
            r0 = 0
            r2.A05 = r0
            float r1 = r2.A03
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            float r1 = r2.A00
            if (r0 >= 0) goto L_0x0062
            float r0 = r2.A01()
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0042
            float r0 = r2.A00()
        L_0x0040:
            r2.A00 = r0
        L_0x0042:
            boolean r0 = r3.A0H
            if (r0 != 0) goto L_0x000e
            boolean r0 = r3.A0B
            if (r0 != 0) goto L_0x000e
            float r1 = r2.A03
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x005d
            float r0 = r2.A01()
        L_0x0055:
            int r0 = (int) r0
            r3.A05(r0)
            r2.A02()
            return
        L_0x005d:
            float r0 = r2.A00()
            goto L_0x0055
        L_0x0062:
            float r0 = r2.A00()
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0042
            float r0 = r2.A01()
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AA.A02():void");
    }

    public final void A03() {
        C05540Py r5 = this.A04;
        Rect rect = r5.A04;
        List emptyList = Collections.emptyList();
        AnonymousClass0JI r10 = AnonymousClass0JI.PRE_COMP;
        List emptyList2 = Collections.emptyList();
        C08170ah r9 = new C08170ah(null, null, null, null, null, null, null, null, null);
        int width = rect.width();
        int height = rect.height();
        AnonymousClass0PU r4 = new AnonymousClass0PU(r5, null, null, null, r9, r10, AnonymousClass0J6.NONE, "__container", null, emptyList, emptyList2, Collections.emptyList(), 0.0f, 0.0f, 0, 0, 0, width, height, -1, -1, false);
        C05540Py r3 = this.A04;
        AnonymousClass0HG r1 = new AnonymousClass0HG(r3, this, r4, r3.A07);
        this.A08 = r1;
        if (this.A0E) {
            r1.A05(true);
        }
    }

    public void A04(float f) {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            this.A0L.add(new C07810a7(this, f));
            return;
        }
        AnonymousClass09T r2 = this.A0K;
        float f2 = r0.A02;
        r2.A06(f2 + (f * (r0.A00 - f2)));
        AnonymousClass0MI.A00();
    }

    public void A05(int i) {
        if (this.A04 == null) {
            this.A0L.add(new C07800a6(this, i));
        } else {
            this.A0K.A06((float) i);
        }
    }

    public void A06(int i) {
        if (this.A04 == null) {
            this.A0L.add(new C07840aA(this, i));
            return;
        }
        AnonymousClass09T r2 = this.A0K;
        r2.A07(r2.A02, ((float) i) + 0.99f);
    }

    public void A07(int i) {
        if (this.A04 == null) {
            this.A0L.add(new C07820a8(this, i));
            return;
        }
        AnonymousClass09T r2 = this.A0K;
        r2.A07((float) i, (float) ((int) r2.A01));
    }

    public void A08(int i, int i2) {
        if (this.A04 == null) {
            this.A0L.add(new C07880aE(this, i, i2));
        } else {
            this.A0K.A07((float) i, ((float) i2) + 0.99f);
        }
    }

    public final void A09(Canvas canvas) {
        float f;
        C05540Py r2 = this.A04;
        if (r2 != null && !getBounds().isEmpty()) {
            Rect bounds = getBounds();
            Rect rect = r2.A04;
            if (((float) bounds.width()) / ((float) bounds.height()) != ((float) rect.width()) / ((float) rect.height())) {
                if (this.A08 != null) {
                    int i = -1;
                    Rect bounds2 = getBounds();
                    float width = ((float) bounds2.width()) / ((float) this.A04.A04.width());
                    float height = ((float) bounds2.height()) / ((float) this.A04.A04.height());
                    float min = Math.min(width, height);
                    if (min < 1.0f) {
                        float f2 = 1.0f / min;
                        width /= f2;
                        height /= f2;
                        if (f2 > 1.0f) {
                            i = canvas.save();
                            float width2 = ((float) bounds2.width()) / 2.0f;
                            float height2 = ((float) bounds2.height()) / 2.0f;
                            float f3 = width2 * min;
                            float f4 = min * height2;
                            canvas.translate(width2 - f3, height2 - f4);
                            canvas.scale(f2, f2, f3, f4);
                        }
                    }
                    Matrix matrix = this.A0J;
                    matrix.reset();
                    matrix.preScale(width, height);
                    this.A08.A9C(canvas, matrix, this.A01);
                    if (i > 0) {
                        canvas.restoreToCount(i);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        if (this.A08 != null) {
            float f5 = this.A00;
            float min2 = Math.min(((float) canvas.getWidth()) / ((float) this.A04.A04.width()), ((float) canvas.getHeight()) / ((float) this.A04.A04.height()));
            if (f5 > min2) {
                f = this.A00 / min2;
            } else {
                min2 = f5;
                f = 1.0f;
            }
            int i2 = -1;
            if (f > 1.0f) {
                i2 = canvas.save();
                float width3 = ((float) this.A04.A04.width()) / 2.0f;
                float height3 = ((float) this.A04.A04.height()) / 2.0f;
                float f6 = width3 * min2;
                float f7 = height3 * min2;
                float f8 = this.A00;
                canvas.translate((f8 * width3) - f6, (f8 * height3) - f7);
                canvas.scale(f, f, f6, f7);
            }
            Matrix matrix2 = this.A0J;
            matrix2.reset();
            matrix2.preScale(min2, min2);
            this.A08.A9C(canvas, matrix2, this.A01);
            if (i2 > 0) {
                canvas.restoreToCount(i2);
            }
        }
    }

    public void A0A(C06430To r7, AnonymousClass0SF r8, Object obj) {
        float f;
        AnonymousClass0HG r5 = this.A08;
        if (r5 == null) {
            this.A0L.add(new C07890aF(this, r7, r8, obj));
            return;
        }
        if (r7 == C06430To.A02) {
            r5.A5q(r8, obj);
        } else {
            AbstractC12480hz r0 = r7.A00;
            if (r0 != null) {
                r0.A5q(r8, obj);
            } else {
                ArrayList arrayList = new ArrayList();
                r5.Aan(r7, new C06430To(new String[0]), arrayList, 0);
                for (int i = 0; i < arrayList.size(); i++) {
                    ((C06430To) arrayList.get(i)).A00.A5q(r8, obj);
                }
                if (!(true ^ arrayList.isEmpty())) {
                    return;
                }
            }
        }
        invalidateSelf();
        if (obj == AbstractC12810iX.A0J) {
            AnonymousClass09T r1 = this.A0K;
            C05540Py r02 = r1.A06;
            if (r02 == null) {
                f = 0.0f;
            } else {
                float f2 = r1.A00;
                float f3 = r02.A02;
                f = (f2 - f3) / (r02.A00 - f3);
            }
            A04(f);
        }
    }

    public void A0B(String str) {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            this.A0L.add(new C07870aD(this, str));
            return;
        }
        AnonymousClass0NQ A00 = r0.A00(str);
        if (A00 != null) {
            A06((int) (A00.A01 + A00.A00));
            return;
        }
        StringBuilder sb = new StringBuilder("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }

    public void A0C(String str) {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            this.A0L.add(new C07790a5(this, str));
            return;
        }
        AnonymousClass0NQ A00 = r0.A00(str);
        if (A00 != null) {
            int i = (int) A00.A01;
            A08(i, ((int) A00.A00) + i);
            return;
        }
        StringBuilder sb = new StringBuilder("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }

    public void A0D(String str) {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            this.A0L.add(new C07860aC(this, str));
            return;
        }
        AnonymousClass0NQ A00 = r0.A00(str);
        if (A00 != null) {
            A07((int) A00.A01);
            return;
        }
        StringBuilder sb = new StringBuilder("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }

    public void A0E(boolean z) {
        if (this.A0A == z) {
            return;
        }
        if (Build.VERSION.SDK_INT < 19) {
            AnonymousClass0R5.A00("Merge paths are not supported pre-Kit Kat.");
            return;
        }
        this.A0A = z;
        if (this.A04 != null) {
            A03();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.A0D = false;
        if (this.A0G) {
            try {
                A09(canvas);
            } catch (Throwable unused) {
            }
        } else {
            A09(canvas);
        }
        AnonymousClass0MI.A00();
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A01;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            return -1;
        }
        return (int) (((float) r0.A04.height()) * this.A00);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        C05540Py r0 = this.A04;
        if (r0 == null) {
            return -1;
        }
        return (int) (((float) r0.A04.width()) * this.A00);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        if (!this.A0D) {
            this.A0D = true;
            Drawable.Callback callback = getCallback();
            if (callback != null) {
                callback.invalidateDrawable(this);
            }
        }
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        AnonymousClass09T r0 = this.A0K;
        if (r0 == null) {
            return false;
        }
        return r0.A07;
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A01 = i;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        AnonymousClass0R5.A00("Use addColorFilter instead.");
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        Drawable.Callback callback = getCallback();
        if ((callback instanceof View) && !((View) callback).isInEditMode()) {
            A01();
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        this.A0L.clear();
        this.A0K.A02();
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }
}
