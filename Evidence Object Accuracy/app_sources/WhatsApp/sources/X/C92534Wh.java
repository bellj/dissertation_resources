package X;

import java.util.Arrays;

/* renamed from: X.4Wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92534Wh {
    public int A00;
    public long[] A01 = new long[32];

    public long A00(int i) {
        if (i >= 0 && i < this.A00) {
            return this.A01[i];
        }
        StringBuilder A0k = C12960it.A0k("Invalid index ");
        A0k.append(i);
        A0k.append(", size is ");
        throw new IndexOutOfBoundsException(C12960it.A0f(A0k, this.A00));
    }

    public void A01(long j) {
        int i = this.A00;
        long[] jArr = this.A01;
        if (i == jArr.length) {
            jArr = Arrays.copyOf(jArr, i << 1);
            this.A01 = jArr;
        }
        int i2 = this.A00;
        this.A00 = i2 + 1;
        jArr[i2] = j;
    }
}
