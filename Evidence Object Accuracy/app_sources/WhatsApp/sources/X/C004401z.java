package X;

/* renamed from: X.01z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C004401z {
    public static final AnonymousClass02O A0I = new AnonymousClass0XU();
    public static final String A0J = C06390Tk.A01("WorkSpec");
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public EnumC007503u A08;
    public C004101u A09;
    public C006503b A0A;
    public C006503b A0B;
    public EnumC006603c A0C;
    public EnumC03840Ji A0D = EnumC03840Ji.ENQUEUED;
    public String A0E;
    public String A0F;
    public String A0G;
    public boolean A0H;

    public C004401z(C004401z r3) {
        C006503b r0 = C006503b.A01;
        this.A0A = r0;
        this.A0B = r0;
        this.A09 = C004101u.A08;
        this.A08 = EnumC007503u.EXPONENTIAL;
        this.A01 = C26061Bw.A0L;
        this.A07 = -1;
        this.A0C = EnumC006603c.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.A0E = r3.A0E;
        this.A0G = r3.A0G;
        this.A0D = r3.A0D;
        this.A0F = r3.A0F;
        this.A0A = new C006503b(r3.A0A);
        this.A0B = new C006503b(r3.A0B);
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A02 = r3.A02;
        this.A09 = new C004101u(r3.A09);
        this.A00 = r3.A00;
        this.A08 = r3.A08;
        this.A01 = r3.A01;
        this.A06 = r3.A06;
        this.A05 = r3.A05;
        this.A07 = r3.A07;
        this.A0H = r3.A0H;
        this.A0C = r3.A0C;
    }

    public C004401z(String str, String str2) {
        C006503b r0 = C006503b.A01;
        this.A0A = r0;
        this.A0B = r0;
        this.A09 = C004101u.A08;
        this.A08 = EnumC007503u.EXPONENTIAL;
        this.A01 = C26061Bw.A0L;
        this.A07 = -1;
        this.A0C = EnumC006603c.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.A0E = str;
        this.A0G = str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r15.A00 <= 0) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A00() {
        /*
            r15 = this;
            X.0Ji r1 = r15.A0D
            X.0Ji r0 = X.EnumC03840Ji.ENQUEUED
            if (r1 != r0) goto L_0x000b
            int r1 = r15.A00
            r0 = 1
            if (r1 > 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            r14 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0034
            X.03u r1 = r15.A08
            X.03u r0 = X.EnumC007503u.LINEAR
            if (r1 != r0) goto L_0x0017
            r14 = 1
        L_0x0017:
            long r4 = r15.A01
            if (r14 == 0) goto L_0x002a
            int r0 = r15.A00
            long r0 = (long) r0
            long r4 = r4 * r0
        L_0x001f:
            long r2 = r15.A06
            r0 = 18000000(0x112a880, double:8.8931816E-317)
            long r0 = java.lang.Math.min(r0, r4)
        L_0x0028:
            long r2 = r2 + r0
            return r2
        L_0x002a:
            float r1 = (float) r4
            int r0 = r15.A00
            int r0 = r0 - r2
            float r0 = java.lang.Math.scalb(r1, r0)
            long r4 = (long) r0
            goto L_0x001f
        L_0x0034:
            long r4 = r15.A04
            r12 = 0
            int r0 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x0065
            r10 = 0
            long r8 = java.lang.System.currentTimeMillis()
            long r6 = r15.A06
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x0063
            long r0 = r15.A03
            long r8 = r8 + r0
        L_0x004b:
            long r2 = r15.A02
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0052
            r14 = 1
        L_0x0052:
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r14 == 0) goto L_0x005f
            if (r0 != 0) goto L_0x005c
            r0 = -1
            long r10 = r2 * r0
        L_0x005c:
            long r8 = r8 + r4
        L_0x005d:
            long r8 = r8 + r10
            return r8
        L_0x005f:
            if (r0 == 0) goto L_0x005d
            r10 = r4
            goto L_0x005d
        L_0x0063:
            r8 = r6
            goto L_0x004b
        L_0x0065:
            long r2 = r15.A06
            int r0 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x006f
            long r2 = java.lang.System.currentTimeMillis()
        L_0x006f:
            long r0 = r15.A03
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C004401z.A00():long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0078, code lost:
        if (r1.equals(r0) == false) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 1
            if (r7 == r8) goto L_0x00a9
            r5 = 0
            if (r8 == 0) goto L_0x007a
            java.lang.Class<X.01z> r1 = X.C004401z.class
            java.lang.Class r0 = r8.getClass()
            if (r1 != r0) goto L_0x007a
            X.01z r8 = (X.C004401z) r8
            long r3 = r7.A03
            long r1 = r8.A03
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r3 = r7.A04
            long r1 = r8.A04
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r3 = r7.A02
            long r1 = r8.A02
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            int r1 = r7.A00
            int r0 = r8.A00
            if (r1 != r0) goto L_0x007a
            long r3 = r7.A01
            long r1 = r8.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r3 = r7.A06
            long r1 = r8.A06
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r3 = r7.A05
            long r1 = r8.A05
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            long r3 = r7.A07
            long r1 = r8.A07
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x007a
            boolean r1 = r7.A0H
            boolean r0 = r8.A0H
            if (r1 != r0) goto L_0x007a
            java.lang.String r1 = r7.A0E
            java.lang.String r0 = r8.A0E
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x007a
            X.0Ji r1 = r7.A0D
            X.0Ji r0 = r8.A0D
            if (r1 != r0) goto L_0x007a
            java.lang.String r1 = r7.A0G
            java.lang.String r0 = r8.A0G
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x007a
            java.lang.String r1 = r7.A0F
            java.lang.String r0 = r8.A0F
            if (r1 == 0) goto L_0x007b
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x007e
        L_0x007a:
            return r5
        L_0x007b:
            if (r0 == 0) goto L_0x007e
            return r5
        L_0x007e:
            X.03b r1 = r7.A0A
            X.03b r0 = r8.A0A
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x007a
            X.03b r1 = r7.A0B
            X.03b r0 = r8.A0B
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x007a
            X.01u r1 = r7.A09
            X.01u r0 = r8.A09
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x007a
            X.03u r1 = r7.A08
            X.03u r0 = r8.A08
            if (r1 != r0) goto L_0x007a
            X.03c r1 = r7.A0C
            X.03c r0 = r8.A0C
            if (r1 == r0) goto L_0x00a9
            r6 = 0
        L_0x00a9:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C004401z.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int hashCode = ((((this.A0E.hashCode() * 31) + this.A0D.hashCode()) * 31) + this.A0G.hashCode()) * 31;
        String str = this.A0F;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        long j = this.A03;
        long j2 = this.A04;
        long j3 = this.A02;
        long j4 = this.A01;
        long j5 = this.A06;
        long j6 = this.A05;
        long j7 = this.A07;
        return ((((((((((((((((((((((((((((hashCode + i) * 31) + this.A0A.hashCode()) * 31) + this.A0B.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.A09.hashCode()) * 31) + this.A00) * 31) + this.A08.hashCode()) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + (this.A0H ? 1 : 0)) * 31) + this.A0C.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("{WorkSpec: ");
        sb.append(this.A0E);
        sb.append("}");
        return sb.toString();
    }
}
