package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.status.playback.widget.AudioVolumeView;
import com.whatsapp.status.playback.widget.StatusPlaybackProgressView;

/* renamed from: X.1en  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33651en {
    public Button A00;
    public final View A01;
    public final View A02;
    public final View A03;
    public final View A04;
    public final View A05;
    public final View A06;
    public final ViewGroup A07;
    public final ViewStub A08;
    public final FrameLayout A09;
    public final ImageView A0A;
    public final ImageView A0B;
    public final TextView A0C;
    public final AudioVolumeView A0D;
    public final StatusPlaybackProgressView A0E;

    public C33651en(View view) {
        this.A09 = (FrameLayout) AnonymousClass028.A0D(view, R.id.root_view);
        this.A0E = (StatusPlaybackProgressView) AnonymousClass028.A0D(view, R.id.playback_progress);
        this.A07 = (ViewGroup) AnonymousClass028.A0D(view, R.id.status_container);
        this.A0D = (AudioVolumeView) AnonymousClass028.A0D(view, R.id.volume);
        this.A0A = (ImageView) AnonymousClass028.A0D(view, R.id.back);
        this.A0C = (TextView) AnonymousClass028.A0D(view, R.id.date);
        this.A06 = AnonymousClass028.A0D(view, R.id.title_bar);
        this.A05 = AnonymousClass028.A0D(view, R.id.title_protection);
        this.A01 = AnonymousClass028.A0D(view, R.id.header);
        this.A04 = AnonymousClass028.A0D(view, R.id.status_header);
        this.A03 = AnonymousClass028.A0D(view, R.id.menu);
        this.A02 = AnonymousClass028.A0D(view, R.id.progress);
        this.A0B = (ImageView) AnonymousClass028.A0D(view, R.id.profile_picture);
        ViewStub viewStub = (ViewStub) AnonymousClass028.A0D(view, R.id.action_link_button_view_stub);
        this.A08 = viewStub;
        viewStub.setVisibility(8);
    }
}
