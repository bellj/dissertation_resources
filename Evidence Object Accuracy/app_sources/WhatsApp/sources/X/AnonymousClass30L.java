package X;

/* renamed from: X.30L  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30L extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;

    public AnonymousClass30L() {
        super(3426, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(4, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCadminDemote {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cadminDemoteOrigin", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cadminDemoteResult", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isLastCadminOrCreator", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
