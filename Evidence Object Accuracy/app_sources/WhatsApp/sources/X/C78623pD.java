package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3pD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78623pD extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98904jR();
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;

    public C78623pD(String str, String str2, String str3, String str4, int i, int i2, int i3, boolean z, boolean z2) {
        this.A03 = str;
        this.A01 = i;
        this.A02 = i2;
        this.A05 = str2;
        this.A06 = str3;
        this.A07 = z;
        this.A04 = str4;
        this.A08 = z2;
        this.A00 = i3;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C78623pD) {
                C78623pD r5 = (C78623pD) obj;
                if (!(C13300jT.A00(this.A03, r5.A03) && this.A01 == r5.A01 && this.A02 == r5.A02 && C13300jT.A00(this.A04, r5.A04) && C13300jT.A00(this.A05, r5.A05) && C13300jT.A00(this.A06, r5.A06) && this.A07 == r5.A07 && this.A08 == r5.A08 && this.A00 == r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public C78623pD(EnumC87344Bd r3, String str, String str2, int i, int i2) {
        C13020j0.A01(str);
        this.A03 = str;
        this.A01 = i;
        this.A02 = i2;
        this.A04 = str2;
        this.A05 = null;
        this.A06 = null;
        this.A07 = true;
        this.A08 = false;
        this.A00 = r3.value;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        Object[] objArr = new Object[9];
        objArr[0] = this.A03;
        C12980iv.A1T(objArr, this.A01);
        C12990iw.A1V(objArr, this.A02);
        objArr[3] = this.A04;
        objArr[4] = this.A05;
        objArr[5] = this.A06;
        objArr[6] = Boolean.valueOf(this.A07);
        objArr[7] = Boolean.valueOf(this.A08);
        return C12980iv.A0B(Integer.valueOf(this.A00), objArr, 8);
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder A0k = C12960it.A0k("PlayLoggerContext[");
        A0k.append("package=");
        A0k.append(this.A03);
        A0k.append(',');
        A0k.append("packageVersionCode=");
        A0k.append(this.A01);
        A0k.append(',');
        A0k.append("logSource=");
        A0k.append(this.A02);
        A0k.append(',');
        A0k.append("logSourceName=");
        A0k.append(this.A04);
        A0k.append(',');
        A0k.append("uploadAccount=");
        A0k.append(this.A05);
        A0k.append(',');
        A0k.append("loggingId=");
        A0k.append(this.A06);
        A0k.append(',');
        A0k.append("logAndroidId=");
        A0k.append(this.A07);
        A0k.append(',');
        A0k.append("isAnonymous=");
        A0k.append(this.A08);
        A0k.append(',');
        A0k.append("qosTier=");
        A0k.append(this.A00);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        boolean A0K = C95654e8.A0K(parcel, this.A03);
        C95654e8.A07(parcel, 3, this.A01);
        C95654e8.A07(parcel, 4, this.A02);
        C95654e8.A0D(parcel, this.A05, 5, A0K);
        C95654e8.A0D(parcel, this.A06, 6, A0K);
        C95654e8.A09(parcel, 7, this.A07);
        C95654e8.A0D(parcel, this.A04, 8, A0K);
        C95654e8.A09(parcel, 9, this.A08);
        C95654e8.A07(parcel, 10, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
