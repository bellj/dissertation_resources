package X;

import java.util.HashMap;

/* renamed from: X.5wJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128565wJ {
    public final /* synthetic */ AnonymousClass3FE A00;

    public /* synthetic */ C128565wJ(AnonymousClass3FE r1) {
        this.A00 = r1;
    }

    public final void A00(AnonymousClass1ZW r7, C452120p r8) {
        String str;
        String str2;
        AnonymousClass3FE r5 = this.A00;
        HashMap A11 = C12970iu.A11();
        if (r8 == null) {
            AnonymousClass1ZX r0 = (AnonymousClass1ZX) r7.A08;
            if (r0 != null) {
                int i = r0.A00;
                boolean z = true;
                if ((i & 1) <= 0) {
                    z = false;
                }
                if (!z) {
                    str2 = "sell_pending";
                } else if ((i & 2) <= 0) {
                    str2 = "payout_pending";
                }
                A11.put(str2, "1");
            }
            str = "on_success";
        } else {
            C117295Zj.A1D(r8, A11);
            str = "on_failure";
        }
        r5.A01(str, A11);
    }
}
