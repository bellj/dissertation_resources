package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import java.util.Locale;

/* renamed from: X.08F  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08F {
    public static int A00(View view) {
        return view.getLayoutDirection();
    }

    public static int A01(View view) {
        return view.getTextDirection();
    }

    public static Locale A02(TextView textView) {
        return textView.getTextLocale();
    }

    public static void A03(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, TextView textView) {
        textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
    }

    public static void A04(View view, int i) {
        view.setTextDirection(i);
    }

    public static Drawable[] A05(TextView textView) {
        return textView.getCompoundDrawablesRelative();
    }
}
