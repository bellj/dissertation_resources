package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

/* renamed from: X.5fD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119855fD extends AbstractC18830t7 {
    public final C18790t3 A00;

    @Override // X.AbstractC18830t7
    public boolean A09(String str, byte[] bArr) {
        return true;
    }

    public C119855fD(C18790t3 r8, C16590pI r9, C18810t5 r10, C18800t4 r11, AbstractC14440lR r12) {
        super(r8, r9, r10, r11, r12, 14);
        this.A00 = r8;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ String A01(Object obj) {
        return null;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ void A04(Object obj, String str) {
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A07(InputStream inputStream, Object obj) {
        File A00 = A00("001_invite_bubble.webp");
        if (A00 != null) {
            C14350lI.A0M(A00);
        }
        File A002 = A00("");
        if (A002 == null) {
            Log.e("PAY: NoviInviteAssetManager/storeAssets/ Could not prepare resource directory");
            return false;
        }
        try {
            ZipInputStream zipInputStream = new ZipInputStream(inputStream);
            try {
                new C119995fR(A002).A02(zipInputStream);
                zipInputStream.close();
                return true;
            } catch (Throwable th) {
                try {
                    zipInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (FileNotFoundException | IOException unused2) {
            Log.e("PAY: NoviInviteAssetManager/unzipBatchBackgrounds");
            return false;
        }
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A08(Object obj) {
        return !A06(A00("001_invite_bubble.webp"));
    }

    public void A0A(AnonymousClass2DH r3) {
        if (!(!A06(A00("001_invite_bubble.webp")))) {
            super.A03(r3, null, null, C39461pw.A00("invite", null, null, null));
        }
    }
}
