package X;

import android.os.Handler;
import android.os.PowerManager;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.util.Log;

/* renamed from: X.0y1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21810y1 {
    public boolean A00;
    public final C14900mE A01;
    public final C16240og A02;
    public final AnonymousClass01d A03;
    public final C19890uq A04;
    public final C22280yp A05;
    public final C14860mA A06;

    public C21810y1(C14900mE r1, C16240og r2, AnonymousClass01d r3, C19890uq r4, C22280yp r5, C14860mA r6) {
        this.A01 = r1;
        this.A06 = r6;
        this.A04 = r4;
        this.A03 = r3;
        this.A05 = r5;
        this.A02 = r2;
    }

    public final void A00() {
        C14860mA r1 = this.A06;
        r1.A0N();
        if ((this.A02.A06 && this.A00 && !r1.A0N()) || (r1.A0N() && !this.A00)) {
            C19890uq r2 = this.A04;
            Log.i("session active");
            AnonymousClass1VL r12 = r2.A09;
            AnonymousClass009.A05(r12);
            ((Handler) r12).obtainMessage(7).sendToTarget();
            r2.A06();
            r2.A0n.A02();
            r2.A0J(true, false, false);
            r2.A0X.A00();
        }
    }

    public final void A01() {
        C14860mA r1 = this.A06;
        r1.A0N();
        if (this.A02.A06 && !this.A00 && !r1.A0N()) {
            PowerManager A0I = this.A03.A0I();
            if (A0I == null) {
                Log.w("app/send/inactive pm=null");
            } else {
                PowerManager.WakeLock A00 = C39151pN.A00(A0I, "sendinactive", 1);
                if (A00 != null) {
                    A00.acquire(3000);
                    Log.i("app/sendinactive/wl");
                }
            }
            C19890uq r12 = this.A04;
            Log.i("session inactive");
            AnonymousClass1VL r13 = r12.A09;
            AnonymousClass009.A05(r13);
            ((Handler) r13).obtainMessage(6).sendToTarget();
        }
        if (!this.A00) {
            this.A01.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this.A05, 30));
        }
    }
}
