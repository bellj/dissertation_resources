package X;

import android.transition.Transition;
import android.view.animation.AlphaAnimation;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.2r6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58632r6 extends AbstractC100714mM {
    public final /* synthetic */ MediaViewBaseFragment A00;
    public final /* synthetic */ AbstractC35501i8 A01;

    public C58632r6(MediaViewBaseFragment mediaViewBaseFragment, AbstractC35501i8 r2) {
        this.A00 = mediaViewBaseFragment;
        this.A01 = r2;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        MediaViewBaseFragment mediaViewBaseFragment = this.A00;
        ActivityC000900k A0B = mediaViewBaseFragment.A0B();
        if (A0B != null && !mediaViewBaseFragment.A0g && !A0B.isFinishing()) {
            mediaViewBaseFragment.A03.setVisibility(0);
            AlphaAnimation A0J = C12970iu.A0J();
            A0J.setDuration(600);
            mediaViewBaseFragment.A03.startAnimation(A0J);
            mediaViewBaseFragment.A1M(true, true);
            PhotoView A19 = mediaViewBaseFragment.A19(mediaViewBaseFragment.A1C(mediaViewBaseFragment.A09.getCurrentItem()));
            if (A19 != null) {
                A19.A08(true);
            }
            this.A01.AXp(true);
        }
    }
}
