package X;

/* renamed from: X.4CW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CW extends IndexOutOfBoundsException {
    public static final long serialVersionUID = 6807380416709738314L;
    public final String className;
    public final int codeSize;
    public final String descriptor;
    public final String methodName;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4CW(java.lang.String r3, java.lang.String r4, java.lang.String r5, int r6) {
        /*
            r2 = this;
            java.lang.String r0 = "Method too large: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r3)
            java.lang.String r0 = "."
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r5, r1)
            r2.<init>(r0)
            r2.className = r3
            r2.methodName = r4
            r2.descriptor = r5
            r2.codeSize = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4CW.<init>(java.lang.String, java.lang.String, java.lang.String, int):void");
    }
}
