package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.6Bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133566Bg implements AbstractC17760rL {
    public Map A00 = new LinkedHashMap();

    @Override // X.AbstractC17760rL
    public void A6A(String str) {
        C16700pc.A0E(str, 0);
        for (AbstractC17770rM r1 : this.A00.values()) {
            if (r1 instanceof AbstractC17920rc) {
                ((AbstractC17920rc) r1).AIu(str);
            }
        }
    }

    @Override // X.AbstractC17760rL
    public AbstractC17770rM AGE(String str) {
        C16700pc.A0E(str, 0);
        return (AbstractC17770rM) this.A00.get(str);
    }

    @Override // X.AbstractC17760rL
    public void AWR() {
        for (AbstractC17770rM r0 : this.A00.values()) {
            r0.A01();
        }
    }

    @Override // X.AbstractC17760rL
    public void AWS() {
        for (AbstractC17770rM r0 : this.A00.values()) {
            r0.A02();
        }
    }
}
