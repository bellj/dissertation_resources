package X;

import java.io.File;

/* renamed from: X.4Q0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Q0 {
    public final int A00;
    public final File A01;
    public final boolean A02;

    public AnonymousClass4Q0(File file, int i, boolean z) {
        this.A01 = file;
        this.A02 = z;
        this.A00 = i;
    }
}
