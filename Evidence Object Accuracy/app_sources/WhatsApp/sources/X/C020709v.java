package X;

import android.graphics.drawable.Animatable2;
import android.graphics.drawable.Drawable;

/* renamed from: X.09v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020709v extends Animatable2.AnimationCallback {
    public final /* synthetic */ AnonymousClass03J A00;

    public C020709v(AnonymousClass03J r1) {
        this.A00 = r1;
    }

    @Override // android.graphics.drawable.Animatable2.AnimationCallback
    public void onAnimationEnd(Drawable drawable) {
        this.A00.A01(drawable);
    }

    @Override // android.graphics.drawable.Animatable2.AnimationCallback
    public void onAnimationStart(Drawable drawable) {
    }
}
