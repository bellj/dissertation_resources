package X;

import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.68J  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68J implements AnonymousClass2K5 {
    public final C37891nB A00;

    public AnonymousClass68J(C37891nB r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2K5
    public InputStream A8h(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        C629539h r0 = new C629539h(this.A00, byteArrayOutputStream, (long) bArr.length);
        try {
            r0.write(bArr);
            if (r0.A01) {
                return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            }
            Log.e("NoviResourceDecrypter/decrypt/ could not verify hmac");
            return null;
        } catch (IOException unused) {
            Log.e("NoviResourceDecrypter/decrypt/ could not decrypt response");
            return null;
        }
    }
}
