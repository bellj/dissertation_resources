package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.5kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122115kw extends AbstractC118815cQ {
    public final WaTextView A00;

    public C122115kw(View view) {
        super(view);
        this.A00 = C12960it.A0N(view, R.id.order_saving);
    }
}
