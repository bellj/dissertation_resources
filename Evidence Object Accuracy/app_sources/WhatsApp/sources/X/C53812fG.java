package X;

import android.app.Application;

/* renamed from: X.2fG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53812fG extends AnonymousClass014 {
    public final AnonymousClass02P A00 = new AnonymousClass02P();
    public final AnonymousClass02P A01 = new AnonymousClass02P();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final C22680zT A04;
    public final C14650lo A05;
    public final C15550nR A06;
    public final C16590pI A07;
    public final AnonymousClass018 A08;
    public final AnonymousClass1AW A09;

    public C53812fG(Application application, C22680zT r3, C14650lo r4, C15550nR r5, C16590pI r6, AnonymousClass018 r7, AnonymousClass1AW r8) {
        super(application);
        this.A07 = r6;
        this.A06 = r5;
        this.A09 = r8;
        this.A08 = r7;
        this.A04 = r3;
        this.A05 = r4;
    }
}
