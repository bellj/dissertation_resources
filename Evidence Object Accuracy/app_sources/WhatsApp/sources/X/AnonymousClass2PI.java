package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2PI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PI extends AnonymousClass2PA {
    public final UserJid A00;
    public final boolean A01;

    public AnonymousClass2PI(Jid jid, UserJid userJid, String str, long j, boolean z) {
        super(jid, str, j);
        this.A00 = userJid;
        this.A01 = z;
    }
}
