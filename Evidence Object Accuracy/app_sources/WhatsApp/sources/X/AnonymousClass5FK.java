package X;

/* renamed from: X.5FK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FK implements AnonymousClass5VN {
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x003c A[SYNTHETIC] */
    @Override // X.AnonymousClass5VN
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AgH(java.lang.Appendable r7, java.lang.Object r8, X.C94884ch r9) {
        /*
            r6 = this;
            java.util.Map r8 = (java.util.Map) r8
            r0 = 123(0x7b, float:1.72E-43)
            r7.append(r0)
            java.util.Iterator r5 = X.C12960it.A0n(r8)
            r4 = 1
        L_0x000c:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0062
            java.util.Map$Entry r1 = X.C12970iu.A15(r5)
            java.lang.Object r3 = r1.getValue()
            if (r3 != 0) goto L_0x0021
            boolean r0 = r9.A03
            if (r0 == 0) goto L_0x0021
            goto L_0x000c
        L_0x0021:
            if (r4 == 0) goto L_0x0058
            r4 = 0
        L_0x0024:
            java.lang.Object r0 = r1.getKey()
            java.lang.String r2 = r0.toString()
            if (r2 != 0) goto L_0x0042
            java.lang.String r2 = "null"
        L_0x0030:
            r7.append(r2)
        L_0x0033:
            r0 = 58
            r7.append(r0)
            boolean r0 = r3 instanceof java.lang.String
            if (r0 == 0) goto L_0x005e
            java.lang.String r3 = (java.lang.String) r3
            r9.A00(r7, r3)
            goto L_0x000c
        L_0x0042:
            X.5VL r0 = r9.A00
            boolean r0 = r0.ALX(r2)
            if (r0 == 0) goto L_0x0030
            r1 = 34
            r7.append(r1)
            X.5VM r0 = r9.A02
            r0.A9f(r7, r2)
            r7.append(r1)
            goto L_0x0033
        L_0x0058:
            r0 = 44
            r7.append(r0)
            goto L_0x0024
        L_0x005e:
            X.AnonymousClass4ZZ.A00(r7, r3, r9)
            goto L_0x000c
        L_0x0062:
            r0 = 125(0x7d, float:1.75E-43)
            r7.append(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5FK.AgH(java.lang.Appendable, java.lang.Object, X.4ch):void");
    }
}
