package X;

import android.content.Context;
import com.whatsapp.backup.google.SettingsGoogleDrive;

/* renamed from: X.4pm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102834pm implements AbstractC009204q {
    public final /* synthetic */ SettingsGoogleDrive A00;

    public C102834pm(SettingsGoogleDrive settingsGoogleDrive) {
        this.A00 = settingsGoogleDrive;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
