package X;

/* renamed from: X.4e2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95594e2 {
    public static final int[] A00 = {-19, -1, -1, -1, -1, -1, -1, Integer.MAX_VALUE};

    public static int A00(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        return (bArr[i3 + 1] << 24) | (bArr[i] & 255) | ((bArr[i2] & 255) << 8) | ((bArr[i3] & 255) << 16);
    }

    public static void A01(int i, int i2, int[] iArr, int[] iArr2) {
        int i3 = iArr[i];
        int i4 = iArr[i + 1];
        int i5 = iArr[i + 2];
        int i6 = iArr[i + 3];
        iArr2[i2] = i3 & 67108863;
        iArr2[i2 + 1] = ((i3 >>> 26) | (i4 << 6)) & 67108863;
        iArr2[i2 + 2] = ((i5 << 12) | (i4 >>> 20)) & 33554431;
        iArr2[i2 + 3] = ((i6 << 19) | (i5 >>> 13)) & 67108863;
        iArr2[i2 + 4] = i6 >>> 7;
    }

    public static void A02(int i, int i2, int[] iArr, int[] iArr2) {
        int i3 = iArr[i];
        int i4 = iArr[i + 1];
        int i5 = iArr[i + 2];
        int i6 = iArr[i + 3];
        int i7 = iArr[i + 4];
        iArr2[i2] = i3 | (i4 << 26);
        iArr2[i2 + 1] = (i4 >>> 6) | (i5 << 20);
        iArr2[i2 + 2] = (i5 >>> 12) | (i6 << 13);
        iArr2[i2 + 3] = (i7 << 7) | (i6 >>> 19);
    }

    public static void A04(byte[] bArr, int[] iArr, int i, int i2) {
        int A002 = A00(bArr, i);
        int A003 = A00(bArr, i + 4);
        int A004 = A00(bArr, i + 8);
        int A005 = A00(bArr, i + 12);
        iArr[i2] = A002 & 67108863;
        iArr[i2 + 1] = ((A002 >>> 26) | (A003 << 6)) & 67108863;
        iArr[i2 + 2] = ((A004 << 12) | (A003 >>> 20)) & 33554431;
        iArr[i2 + 3] = ((A005 << 19) | (A004 >>> 13)) & 67108863;
        iArr[i2 + 4] = A005 >>> 7;
    }

    public static void A05(byte[] bArr, int[] iArr, int i, int i2) {
        int i3 = iArr[i];
        int i4 = iArr[i + 1];
        int i5 = iArr[i + 2];
        int i6 = iArr[i + 3];
        int i7 = iArr[i + 4];
        A03(bArr, (i4 << 26) | i3, i2);
        A03(bArr, (i4 >>> 6) | (i5 << 20), i2 + 4);
        A03(bArr, (i5 >>> 12) | (i6 << 13), i2 + 8);
        A03(bArr, (i7 << 7) | (i6 >>> 19), i2 + 12);
    }

    public static void A06(int[] iArr, int i) {
        int i2 = iArr[9];
        long j = ((long) (((i2 >> 24) + i) * 19)) + ((long) iArr[0]);
        iArr[0] = ((int) j) & 67108863;
        long j2 = (j >> 26) + ((long) iArr[1]);
        iArr[1] = ((int) j2) & 67108863;
        long j3 = (j2 >> 26) + ((long) iArr[2]);
        iArr[2] = ((int) j3) & 33554431;
        long j4 = (j3 >> 25) + ((long) iArr[3]);
        iArr[3] = ((int) j4) & 67108863;
        long j5 = (j4 >> 26) + ((long) iArr[4]);
        iArr[4] = ((int) j5) & 33554431;
        long j6 = (j5 >> 25) + ((long) iArr[5]);
        iArr[5] = ((int) j6) & 67108863;
        long j7 = (j6 >> 26) + ((long) iArr[6]);
        iArr[6] = ((int) j7) & 67108863;
        long j8 = (j7 >> 26) + ((long) iArr[7]);
        iArr[7] = 33554431 & ((int) j8);
        long j9 = (j8 >> 25) + ((long) iArr[8]);
        iArr[8] = 67108863 & ((int) j9);
        iArr[9] = (16777215 & i2) + ((int) (j9 >> 26));
    }

    public static void A07(int[] iArr, int[] iArr2) {
        int i = iArr[0];
        int i2 = iArr[1];
        int i3 = iArr[2];
        int i4 = iArr[3];
        int i5 = iArr[4];
        int i6 = iArr[5];
        int i7 = iArr[6];
        int i8 = iArr[7];
        int i9 = iArr[8];
        long j = (long) 121666;
        long j2 = ((long) i3) * j;
        int i10 = ((int) j2) & 33554431;
        long j3 = ((long) i5) * j;
        int i11 = ((int) j3) & 33554431;
        long j4 = ((long) i8) * j;
        int i12 = ((int) j4) & 33554431;
        long j5 = ((long) iArr[9]) * j;
        int i13 = ((int) j5) & 33554431;
        long j6 = ((j5 >> 25) * 38) + (((long) i) * j);
        iArr2[0] = ((int) j6) & 67108863;
        long j7 = (j3 >> 25) + (((long) i6) * j);
        iArr2[5] = ((int) j7) & 67108863;
        long j8 = (j6 >> 26) + (((long) i2) * j);
        iArr2[1] = ((int) j8) & 67108863;
        long j9 = (j2 >> 25) + (((long) i4) * j);
        iArr2[3] = ((int) j9) & 67108863;
        long j10 = (j7 >> 26) + (((long) i7) * j);
        iArr2[6] = ((int) j10) & 67108863;
        long j11 = (j4 >> 25) + (((long) i9) * j);
        iArr2[8] = ((int) j11) & 67108863;
        iArr2[2] = i10 + ((int) (j8 >> 26));
        iArr2[4] = i11 + ((int) (j9 >> 26));
        iArr2[7] = i12 + ((int) (j10 >> 26));
        iArr2[9] = i13 + ((int) (j11 >> 26));
    }

    public static void A08(int[] iArr, int[] iArr2) {
        int i = iArr[0];
        int i2 = iArr[1];
        int i3 = iArr[2];
        int i4 = iArr[3];
        int i5 = iArr[4];
        int i6 = iArr[5];
        int i7 = iArr[6];
        int i8 = iArr[7];
        int i9 = iArr[8];
        int i10 = iArr[9];
        long j = (long) i;
        long j2 = j * j;
        long j3 = (long) (i2 << 1);
        long j4 = j * j3;
        long j5 = (long) (i3 << 1);
        long j6 = (long) i2;
        long j7 = (j * j5) + (j6 * j6);
        long j8 = j3 * j5;
        long j9 = (long) (i4 << 1);
        long j10 = j8 + (j * j9);
        long j11 = (long) (i5 << 1);
        long j12 = (((long) i3) * j5) + (j * j11) + (j6 * j9);
        long j13 = (j3 * j11) + (j9 * j5);
        long j14 = (long) i4;
        long j15 = (j5 * j11) + (j14 * j14);
        long j16 = j14 * j11;
        long j17 = ((long) i5) * j11;
        long j18 = (long) i6;
        long j19 = (long) (i7 << 1);
        long j20 = j18 * j19;
        long j21 = (long) (i8 << 1);
        long j22 = (long) i7;
        long j23 = (j18 * j21) + (j22 * j22);
        long j24 = (long) (i9 << 1);
        long j25 = (j19 * j21) + (j18 * j24);
        long j26 = (long) (i10 << 1);
        long j27 = (((long) i8) * j21) + (j18 * j26) + (j22 * j24);
        long j28 = (j19 * j26) + (j24 * j21);
        long j29 = (long) i9;
        long j30 = j2 - (j28 * 38);
        long j31 = j4 - (((j21 * j26) + (j29 * j29)) * 38);
        long j32 = j7 - ((j29 * j26) * 38);
        long j33 = j10 - ((((long) i10) * j26) * 38);
        long j34 = j13 - (j18 * j18);
        long j35 = j15 - j20;
        long j36 = j16 - j23;
        long j37 = j17 - j25;
        int i11 = i2 + i7;
        int i12 = i3 + i8;
        int i13 = i4 + i9;
        int i14 = i5 + i10;
        long j38 = (long) (i + i6);
        long j39 = j38 * j38;
        long j40 = (long) (i11 << 1);
        long j41 = j38 * j40;
        long j42 = (long) (i12 << 1);
        long j43 = (long) i11;
        long j44 = (j38 * j42) + (j43 * j43);
        long j45 = (long) (i13 << 1);
        long j46 = (j40 * j42) + (j38 * j45);
        long j47 = (long) (i14 << 1);
        long j48 = (((long) i12) * j42) + (j38 * j47) + (j43 * j45);
        long j49 = (j40 * j47) + (j45 * j42);
        long j50 = (long) i13;
        long j51 = (j42 * j47) + (j50 * j50);
        long j52 = j37 + (j46 - j33);
        int i15 = ((int) j52) & 67108863;
        long j53 = (j52 >> 26) + ((j48 - j12) - j27);
        long j54 = j30 + ((((j53 >> 25) + j49) - j34) * 38);
        iArr2[0] = ((int) j54) & 67108863;
        long j55 = (j54 >> 26) + j31 + ((j51 - j35) * 38);
        iArr2[1] = ((int) j55) & 67108863;
        long j56 = (j55 >> 26) + j32 + (((j50 * j47) - j36) * 38);
        iArr2[2] = ((int) j56) & 33554431;
        long j57 = (j56 >> 25) + j33 + (((((long) i14) * j47) - j37) * 38);
        iArr2[3] = ((int) j57) & 67108863;
        long j58 = (j57 >> 26) + j12 + (38 * j27);
        iArr2[4] = ((int) j58) & 33554431;
        long j59 = (j58 >> 25) + j34 + (j39 - j30);
        iArr2[5] = ((int) j59) & 67108863;
        long j60 = (j59 >> 26) + j35 + (j41 - j31);
        iArr2[6] = ((int) j60) & 67108863;
        long j61 = (j60 >> 26) + j36 + (j44 - j32);
        iArr2[7] = ((int) j61) & 33554431;
        long j62 = (j61 >> 25) + ((long) i15);
        iArr2[8] = ((int) j62) & 67108863;
        iArr2[9] = (((int) j53) & 33554431) + ((int) (j62 >> 26));
    }

    public static void A09(int[] iArr, int[] iArr2, int[] iArr3) {
        int i = iArr[0];
        int i2 = iArr2[0];
        int i3 = iArr[1];
        int i4 = iArr2[1];
        int i5 = iArr[2];
        int i6 = iArr2[2];
        int i7 = iArr[3];
        int i8 = iArr2[3];
        int i9 = iArr[4];
        int i10 = iArr2[4];
        int i11 = iArr[5];
        int i12 = iArr2[5];
        int i13 = iArr[6];
        int i14 = iArr2[6];
        int i15 = iArr[7];
        int i16 = iArr2[7];
        int i17 = iArr[8];
        int i18 = iArr2[8];
        int i19 = iArr[9];
        int i20 = iArr2[9];
        long j = (long) i;
        long j2 = (long) i2;
        long j3 = j * j2;
        long j4 = (long) i4;
        long j5 = (long) i3;
        long j6 = (j * j4) + (j5 * j2);
        long j7 = (long) i6;
        long j8 = (j * j7) + (j5 * j4);
        long j9 = (long) i5;
        long j10 = j8 + (j9 * j2);
        long j11 = (long) i8;
        long j12 = (long) i7;
        long j13 = (((j5 * j7) + (j9 * j4)) << 1) + (j * j11) + (j12 * j2);
        long j14 = (long) i10;
        long j15 = (j * j14) + (j5 * j11) + (j12 * j4);
        long j16 = (long) i9;
        long j17 = ((j9 * j7) << 1) + j15 + (j2 * j16);
        long j18 = (((j9 * j14) + (j16 * j7)) << 1) + (j12 * j11);
        long j19 = (j12 * j14) + (j16 * j11);
        long j20 = (long) i11;
        long j21 = (long) i12;
        long j22 = j20 * j21;
        long j23 = (long) i14;
        long j24 = (long) i13;
        long j25 = (j20 * j23) + (j24 * j21);
        long j26 = (long) i16;
        long j27 = (long) i15;
        long j28 = (j20 * j26) + (j24 * j23) + (j27 * j21);
        long j29 = (long) i18;
        long j30 = (long) i17;
        long j31 = (((j24 * j26) + (j27 * j23)) << 1) + (j20 * j29) + (j30 * j21);
        long j32 = (long) i20;
        long j33 = (j20 * j32) + (j24 * j29) + (j30 * j23);
        long j34 = (long) i19;
        long j35 = ((j27 * j26) << 1) + j33 + (j21 * j34);
        long j36 = j3 - (((((j24 * j32) + (j27 * j29)) + (j30 * j26)) + (j34 * j23)) * 76);
        long j37 = j6 - (((((j27 * j32) + (j34 * j26)) << 1) + (j30 * j29)) * 38);
        long j38 = j10 - (((j30 * j32) + (j29 * j34)) * 38);
        long j39 = j13 - ((j34 * j32) * 76);
        long j40 = (((((j5 * j14) + (j9 * j11)) + (j12 * j7)) + (j16 * j4)) << 1) - j22;
        long j41 = j18 - j25;
        long j42 = j19 - j28;
        long j43 = ((j16 * j14) << 1) - j31;
        int i21 = i9 + i19;
        long j44 = (long) (i + i11);
        long j45 = (long) (i2 + i12);
        long j46 = j44 * j45;
        long j47 = (long) (i4 + i14);
        long j48 = (long) (i3 + i13);
        long j49 = (j44 * j47) + (j48 * j45);
        long j50 = (long) (i6 + i16);
        long j51 = (long) (i5 + i15);
        long j52 = (j44 * j50) + (j48 * j47) + (j51 * j45);
        long j53 = (long) (i8 + i18);
        long j54 = (long) (i7 + i17);
        long j55 = (((j48 * j50) + (j51 * j47)) << 1) + (j44 * j53) + (j54 * j45);
        long j56 = (long) (i10 + i20);
        long j57 = (j44 * j56) + (j48 * j53) + (j54 * j47);
        long j58 = (long) i21;
        long j59 = ((j51 * j50) << 1) + j57 + (j45 * j58);
        long j60 = (((j51 * j56) + (j58 * j50)) << 1) + (j54 * j53);
        long j61 = j43 + (j55 - j39);
        long j62 = (j61 >> 26) + ((j59 - j17) - j35);
        long j63 = j36 + ((((j62 >> 25) + (((((j48 * j56) + (j51 * j53)) + (j54 * j50)) + (j58 * j47)) << 1)) - j40) * 38);
        iArr3[0] = ((int) j63) & 67108863;
        long j64 = (j63 >> 26) + j37 + ((j60 - j41) * 38);
        iArr3[1] = ((int) j64) & 67108863;
        long j65 = (j64 >> 26) + j38 + ((((j54 * j56) + (j58 * j53)) - j42) * 38);
        iArr3[2] = ((int) j65) & 33554431;
        long j66 = (j65 >> 25) + j39 + ((((j58 * j56) << 1) - j43) * 38);
        iArr3[3] = ((int) j66) & 67108863;
        long j67 = (j66 >> 26) + j17 + (j35 * 38);
        iArr3[4] = ((int) j67) & 33554431;
        long j68 = (j67 >> 25) + j40 + (j46 - j36);
        iArr3[5] = ((int) j68) & 67108863;
        long j69 = (j68 >> 26) + j41 + (j49 - j37);
        iArr3[6] = ((int) j69) & 67108863;
        long j70 = (j69 >> 26) + j42 + (j52 - j38);
        iArr3[7] = ((int) j70) & 33554431;
        long j71 = (j70 >> 25) + ((long) (((int) j61) & 67108863));
        iArr3[8] = ((int) j71) & 67108863;
        iArr3[9] = (((int) j62) & 33554431) + ((int) (j71 >> 26));
    }

    public static void A03(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) i;
        int i3 = i2 + 1;
        C72463ee.A0W(bArr, i, i3);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >>> 16);
        bArr[i4 + 1] = (byte) (i >>> 24);
    }
}
