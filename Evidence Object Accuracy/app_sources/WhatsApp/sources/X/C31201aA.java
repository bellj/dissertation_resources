package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/* renamed from: X.1aA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31201aA {
    public static final Random A03 = new Random();
    public final C31191a9 A00;
    public final C14830m7 A01;
    public final C18240s8 A02;

    public C31201aA(C31191a9 r1, C14830m7 r2, C18240s8 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public static final C16020oJ A00(Exception exc, byte[] bArr, int i) {
        int length;
        String str;
        if (exc != null) {
            Log.w("axolotl", exc);
        }
        if (i == 0) {
            if (bArr == null || (length = bArr.length) == 0) {
                Log.w("SignalCoordinator/createDecryptionResult axolotl derived null or empty plaintext from message");
                i = -1000;
            } else {
                int i2 = bArr[length - 1] & 255;
                if (i2 == 0) {
                    str = "MessageUtil/removePadding/ axolotl derived plaintext has invalid padding";
                } else if (i2 >= length) {
                    str = "MessageUtil/removePadding/ axolotl derived entire plaintext as padding";
                } else {
                    int i3 = length - i2;
                    byte[] bArr2 = new byte[i3];
                    System.arraycopy(bArr, 0, bArr2, 0, i3);
                    if (bArr2.length != 0) {
                        return new C16020oJ(bArr2, 0);
                    }
                    i = -10000;
                }
                Log.w(str);
                i = -10000;
            }
        }
        return new C16020oJ(null, i);
    }

    public static final boolean A01(C31941bM r2, C31941bM r3) {
        C31961bO r0 = r2.A02;
        if (r0 == null) {
            r0 = C31961bO.A03;
        }
        byte[] A02 = r0.A02();
        C31961bO r02 = r3.A02;
        if (r02 == null) {
            r02 = C31961bO.A03;
        }
        if (MessageDigest.isEqual(A02, r02.A02())) {
            C31961bO r03 = r2.A03;
            if (r03 == null) {
                r03 = C31961bO.A03;
            }
            byte[] A022 = r03.A02();
            C31961bO r04 = r3.A03;
            if (r04 == null) {
                r04 = C31961bO.A03;
            }
            if (MessageDigest.isEqual(A022, r04.A02())) {
                return true;
            }
        }
        return false;
    }

    public static final boolean A02(C31941bM r2, C31941bM r3) {
        C31961bO r0 = r2.A02;
        if (r0 == null) {
            r0 = C31961bO.A03;
        }
        byte[] A02 = r0.A02();
        C31961bO r02 = r3.A03;
        if (r02 == null) {
            r02 = C31961bO.A03;
        }
        if (MessageDigest.isEqual(A02, r02.A02())) {
            C31961bO r03 = r2.A03;
            if (r03 == null) {
                r03 = C31961bO.A03;
            }
            byte[] A022 = r03.A02();
            C31961bO r04 = r3.A02;
            if (r04 == null) {
                r04 = C31961bO.A03;
            }
            if (MessageDigest.isEqual(A022, r04.A02())) {
                return true;
            }
        }
        return false;
    }

    public static byte[] A03(byte[] bArr) {
        int nextInt = A03.nextInt(16) + 1;
        int length = bArr.length;
        int i = length + nextInt;
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        Arrays.fill(bArr2, length, i, (byte) nextInt);
        return bArr2;
    }

    public C31951bN A04() {
        C31741b2 A01 = this.A00.A01();
        byte b = (byte) 5;
        return new C31951bN(new C32051bX(A01.A01.A00, b), new AnonymousClass1JS(new AnonymousClass1PA(A01.A00.A00.A00, b)));
    }

    public C31581am A05(C15950oC r8) {
        C31191a9 r2 = this.A00;
        return new C31581am(r2, r2, r2, r2.A06, new C31601ao(C29201Rg.A01(r8), r8.A00));
    }

    public void A06() {
        C31191a9 r7 = this.A00;
        int A02 = r7.A00.A02(AbstractC15460nI.A1K);
        int A00 = r7.A04.A00();
        if (A00 >= A02) {
            StringBuilder sb = new StringBuilder("skipping key generation because already more than ");
            sb.append(A02);
            sb.append(" are unsent");
            Log.i(sb.toString());
            return;
        }
        while (true) {
            A02 -= A00;
            if (A02 > 0) {
                A00 = Math.min(A02, 50);
                int A002 = r7.A03.A00();
                StringBuilder sb2 = new StringBuilder("axolotl generating ");
                sb2.append(A00);
                sb2.append(" new prekeys starting from ");
                sb2.append(A002);
                sb2.append(" and recording them in the db");
                Log.i(sb2.toString());
                ArrayList arrayList = new ArrayList();
                LinkedList linkedList = new LinkedList();
                int i = A002 - 1;
                for (int i2 = 0; i2 < A00; i2++) {
                    linkedList.add(new C31761b4(C31481ac.A01(), ((i + i2) % 16777214) + 1));
                }
                Iterator it = linkedList.iterator();
                while (it.hasNext()) {
                    C31281aI r0 = ((C31761b4) it.next()).A00;
                    arrayList.add(new AnonymousClass1RW(r0.A01, r0.A02()));
                }
                r7.A07.A0e(arrayList, ((A002 + A00) % 16777214) + 1);
            } else {
                return;
            }
        }
    }

    public boolean A07() {
        C31191a9 r1 = this.A00;
        r1.A08.A00();
        C16310on A01 = r1.A04.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM prekeys WHERE sent_to_server = 0 AND direct_distribution = 0", null);
            if (A09.moveToNext()) {
                boolean z = false;
                if (A09.getInt(0) != 0) {
                    z = true;
                }
                A09.close();
                A01.close();
                StringBuilder sb = new StringBuilder("axolotl has unsent prekeys: ");
                sb.append(z);
                Log.i(sb.toString());
                return z;
            }
            throw new SQLiteException("Unable to count unsent entries in prekeys table");
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
