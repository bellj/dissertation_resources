package X;

import java.util.concurrent.Executor;

/* renamed from: X.5E0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5E0 implements Executor {
    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
