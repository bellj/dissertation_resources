package X;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.31e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616331e extends AbstractC35601iM {
    @Override // X.AbstractC35611iN
    public String AES() {
        return "image/*";
    }

    @Override // X.AbstractC35611iN
    public int getType() {
        return 0;
    }

    public C616331e(AbstractC16130oV r1, File file, long j) {
        super(r1, file, j);
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        try {
            ParcelFileDescriptor open = ParcelFileDescriptor.open(this.A04, 268435456);
            long j = (long) i;
            try {
                Bitmap A02 = AnonymousClass3Il.A02(open, i, j * j * 2);
                if (open == null) {
                    return A02;
                }
                open.close();
                return A02;
            } catch (Throwable th) {
                if (open != null) {
                    try {
                        open.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (IOException | SecurityException e) {
            Log.e("got exception decoding bitmap ", e);
            return null;
        }
    }
}
