package X;

import android.app.Activity;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.view.Display;
import com.whatsapp.payments.ui.IndiaUpiPaymentsBlockScreenShareActivity;

/* renamed from: X.60u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309960u {
    public DisplayManager.DisplayListener A00;
    public AnonymousClass6MA A01;
    public final C16590pI A02;
    public final C14850m9 A03;

    public C1309960u(C16590pI r1, C14850m9 r2) {
        this.A03 = r2;
        this.A02 = r1;
    }

    public static void A01(Activity activity) {
        if (activity != null && Build.VERSION.SDK_INT >= 17) {
            Intent A0D = C12990iw.A0D(activity, IndiaUpiPaymentsBlockScreenShareActivity.class);
            A0D.addFlags(536870912);
            activity.finish();
            activity.startActivity(A0D);
        }
    }

    public void A02(AnonymousClass6MA r5) {
        if (this.A03.A07(1734) && Build.VERSION.SDK_INT >= 17) {
            if (A03()) {
                r5.AVY();
                return;
            }
            this.A01 = r5;
            DisplayManager displayManager = (DisplayManager) this.A02.A00.getSystemService("display");
            DisplayManager.DisplayListener displayListener = this.A00;
            if (displayListener == null && Build.VERSION.SDK_INT >= 17) {
                displayListener = new AnonymousClass63I(displayManager, this);
                this.A00 = displayListener;
            }
            displayManager.registerDisplayListener(displayListener, null);
        }
    }

    public boolean A03() {
        Display[] displays;
        int length;
        if (Build.VERSION.SDK_INT < 17 || (length = (displays = ((DisplayManager) this.A02.A00.getSystemService("display")).getDisplays()).length) <= 1) {
            return false;
        }
        for (int i = 1; i < length; i++) {
            if ((displays[i].getFlags() & 2) > 0) {
                return true;
            }
        }
        return false;
    }
}
