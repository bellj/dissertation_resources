package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100384lp implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return C14370lK.A01(parcel.readByte(), parcel.readInt());
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C14370lK[i];
    }
}
