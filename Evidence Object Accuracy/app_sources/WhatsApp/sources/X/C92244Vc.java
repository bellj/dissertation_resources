package X;

/* renamed from: X.4Vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92244Vc {
    public final C16590pI A00;
    public final C14850m9 A01;
    public final AnonymousClass1CF A02;

    public C92244Vc(C16590pI r1, C14850m9 r2, AnonymousClass1CF r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public final boolean A00() {
        AnonymousClass1CF r2 = this.A02;
        r2.A00.A08();
        return r2.A02.A07(808) && this.A01.A07(1687);
    }
}
