package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.212  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass212 extends AbstractC16280ok {
    public final Context A00;
    public final C231410n A01;

    public AnonymousClass212(Context context, AbstractC15710nm r9, C231410n r10, C14850m9 r11) {
        super(context, r9, "emojidictionary.db", null, 1, r11.A07(781));
        this.A00 = context;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        return AnonymousClass1Tx.A01(super.A00(), this.A01);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE emoji_search_tag (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type INTEGER NOT NULL, symbol TEXT NOT NULL, tag TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS emoji_search_pack_index ON emoji_search_tag (type, symbol, tag)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS emoji_search_tag");
        onCreate(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS emoji_search_tag");
        onCreate(sQLiteDatabase);
    }
}
