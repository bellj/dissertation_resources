package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1Jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27901Jr extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27901Jr A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public AbstractC27881Jp A01 = AbstractC27881Jp.A01;

    static {
        C27901Jr r0 = new C27901Jr();
        A02 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A01, 1);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A01, 1);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
