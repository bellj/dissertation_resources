package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99604kZ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass2VB(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass2VB[i];
    }
}
