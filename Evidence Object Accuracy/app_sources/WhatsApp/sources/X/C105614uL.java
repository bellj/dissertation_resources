package X;

/* renamed from: X.4uL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105614uL implements AbstractC009404s {
    public final /* synthetic */ AnonymousClass2IM A00;
    public final /* synthetic */ AbstractC14640lm A01;

    public C105614uL(AnonymousClass2IM r1, AbstractC14640lm r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new C74523iD(this.A01, (C16030oK) this.A00.A00.A03.AAd.get());
    }
}
