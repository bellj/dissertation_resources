package X;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.util.Log;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.2ko */
/* loaded from: classes2.dex */
public final class C56392ko extends AbstractC77963o9 {
    public final AnonymousClass4ST A00;
    public final AbstractC115225Qr A01;
    public final String A02 = "locationServices";

    @Override // X.AbstractC95064d1
    public final String A05() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @Override // X.AbstractC95064d1
    public final String A06() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    @Override // X.AbstractC95064d1
    public final boolean A0A() {
        return true;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 11717000;
    }

    public C56392ko(Context context, Looper looper, AbstractC14980mM r12, AbstractC15000mO r13, AnonymousClass3BW r14) {
        super(context, looper, r12, r13, r14, 23);
        AnonymousClass50G r1 = new AnonymousClass50G(this);
        this.A01 = r1;
        this.A00 = new AnonymousClass4ST(context, r1);
    }

    public static /* synthetic */ void A00(C56392ko r0) {
        if (!r0.isConnected()) {
            throw C12960it.A0U("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @Override // X.AbstractC95064d1
    public final Bundle A02() {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("client_name", this.A02);
        return A0D;
    }

    @Override // X.AbstractC95064d1
    public final /* bridge */ /* synthetic */ IInterface A04(IBinder iBinder) {
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return !(queryLocalInterface instanceof AnonymousClass5YQ) ? new C56642lL(iBinder) : queryLocalInterface;
    }

    @Override // X.AbstractC95064d1
    public final C78603pB[] A0B() {
        return C88794Hd.A05;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final void A8y() {
        AnonymousClass4ST r3 = this.A00;
        synchronized (r3) {
            if (isConnected()) {
                try {
                    Map map = r3.A02;
                    synchronized (map) {
                        Iterator A0o = C12960it.A0o(map);
                        while (A0o.hasNext()) {
                            BinderC79763rA r6 = (BinderC79763rA) A0o.next();
                            if (r6 != null) {
                                ((AnonymousClass5YQ) ((AnonymousClass50G) r3.A01).A00.A03()).AhP(new C78413os(null, r6, null, null, null, 2));
                            }
                        }
                        map.clear();
                    }
                    Map map2 = r3.A04;
                    synchronized (map2) {
                        Iterator A0o2 = C12960it.A0o(map2);
                        while (A0o2.hasNext()) {
                            A0o2.next();
                        }
                        map2.clear();
                    }
                    Map map3 = r3.A03;
                    synchronized (map3) {
                        Iterator A0o3 = C12960it.A0o(map3);
                        while (A0o3.hasNext()) {
                            A0o3.next();
                        }
                        map3.clear();
                    }
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.A8y();
        }
    }
}
