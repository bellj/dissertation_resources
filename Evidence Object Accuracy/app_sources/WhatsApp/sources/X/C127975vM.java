package X;

/* renamed from: X.5vM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127975vM {
    public final C452120p A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public C127975vM(AnonymousClass1V8 r5) {
        String A0I = r5.A0I("owner-name", null);
        AnonymousClass009.A05(A0I);
        this.A04 = A0I;
        String A0I2 = r5.A0I("business-name", null);
        AnonymousClass009.A05(A0I2);
        this.A02 = A0I2;
        String A0I3 = r5.A0I("verify-type", null);
        AnonymousClass009.A05(A0I3);
        this.A05 = A0I3;
        this.A01 = r5.A0I("bank-name", null);
        AnonymousClass1V8 A0E = r5.A0E("merchant");
        AnonymousClass009.A05(A0E);
        String A0I4 = A0E.A0I("credential-id", null);
        AnonymousClass009.A05(A0I4);
        this.A03 = A0I4;
        if (r5.A0I("error-code", null) != null) {
            C452120p A0L = C117305Zk.A0L();
            this.A00 = A0L;
            int A00 = C28421Nd.A00(r5.A0I("error-code", null), 0);
            String A0I5 = r5.A0I("error-text", null);
            A0L.A00 = A00;
            A0L.A09 = A0I5;
        }
    }
}
