package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

/* renamed from: X.05l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C011205l implements AbstractC009204q {
    public final /* synthetic */ ActivityC000900k A00;

    public C011205l(ActivityC000900k r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        ActivityC000900k r3 = this.A00;
        AnonymousClass05V r1 = r3.A03.A00;
        r1.A03.A0Z(null, r1, r1);
        Bundle A00 = r3.A07.A00.A00(ActivityC000900k.A05);
        if (A00 != null) {
            Parcelable parcelable = A00.getParcelable(ActivityC000900k.A05);
            AnonymousClass05V r12 = r3.A03.A00;
            if (r12 instanceof AbstractC001400p) {
                r12.A03.A0Q(parcelable);
                return;
            }
            throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
        }
    }
}
