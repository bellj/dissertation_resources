package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.2hZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54962hZ extends AnonymousClass03U {
    public final View A00;
    public final View A01;
    public final ImageView A02;

    public C54962hZ(Context context, ViewGroup viewGroup) {
        super(C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.sticker_pack_preview));
        View view = this.A0H;
        this.A02 = C12970iu.A0L(view, R.id.icon);
        this.A00 = view.findViewById(R.id.badge);
        this.A01 = view.findViewById(R.id.sel_marker);
    }
}
