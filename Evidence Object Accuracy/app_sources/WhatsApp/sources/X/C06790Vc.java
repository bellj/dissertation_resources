package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06790Vc implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(15);
    public int A00;
    public String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06790Vc(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readInt();
    }

    public C06790Vc(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeInt(this.A00);
    }
}
