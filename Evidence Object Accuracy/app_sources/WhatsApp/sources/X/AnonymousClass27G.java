package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.27G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27G implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29831Uv(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29831Uv[i];
    }
}
