package X;

/* renamed from: X.0bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08690bh implements AbstractC72393eW {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final AbstractC72393eW A05;
    public final AbstractC65073Ia A06;
    public final Object A07;

    @Override // X.AbstractC72393eW
    public int ABP() {
        return 1;
    }

    @Override // X.AbstractC72393eW
    public int AEr() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEs() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEt() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEu() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AHs(int i) {
        return 0;
    }

    public C08690bh(AbstractC72393eW r10, AbstractC65073Ia r11) {
        this(r10, r11, null, r10.getWidth(), r10.getHeight(), r10.AHm(), r10.ADK(), 0);
    }

    public C08690bh(AbstractC72393eW r1, AbstractC65073Ia r2, Object obj, int i, int i2, int i3, int i4, int i5) {
        this.A06 = r2;
        this.A05 = r1;
        this.A03 = i;
        this.A01 = i2;
        this.A04 = i3;
        this.A02 = i4;
        this.A00 = i5;
        this.A07 = obj;
    }

    @Override // X.AbstractC72393eW
    public AbstractC72393eW ABK(int i) {
        return this.A05;
    }

    @Override // X.AbstractC72393eW
    public int ADK() {
        return this.A02;
    }

    @Override // X.AbstractC72393eW
    public Object ADo() {
        return this.A07;
    }

    @Override // X.AbstractC72393eW
    public AbstractC65073Ia AG8() {
        return this.A06;
    }

    @Override // X.AbstractC72393eW
    public int AHm() {
        return this.A04;
    }

    @Override // X.AbstractC72393eW
    public int AHt(int i) {
        return this.A00;
    }

    @Override // X.AbstractC72393eW
    public int getHeight() {
        return this.A01;
    }

    @Override // X.AbstractC72393eW
    public int getWidth() {
        return this.A03;
    }
}
