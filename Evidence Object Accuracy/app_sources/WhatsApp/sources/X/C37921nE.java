package X;

import java.util.List;

/* renamed from: X.1nE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37921nE {
    public final /* synthetic */ AbstractC37911nD A00;

    public C37921nE(AbstractC37911nD r1) {
        this.A00 = r1;
    }

    public byte[] A00() {
        AbstractC37911nD r2 = this.A00;
        AnonymousClass009.A0F(r2.A02);
        List<Object> list = r2.A07;
        if (list.size() == 0) {
            return null;
        }
        int size = list.size();
        int i = r2.A03;
        byte[] bArr = new byte[size * i];
        int i2 = 0;
        for (Object obj : list) {
            System.arraycopy(obj, 0, bArr, i2, i);
            i2 += i;
        }
        return bArr;
    }
}
