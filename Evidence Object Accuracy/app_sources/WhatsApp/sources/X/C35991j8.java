package X;

import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1j8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35991j8 {
    public final int A00;
    public final int A01;
    public final C30751Yr A02;
    public final String A03;
    public final List A04;

    public C35991j8(C244415n r7, List list, int i) {
        this.A04 = new ArrayList(list);
        C30751Yr r0 = (C30751Yr) list.get(0);
        this.A02 = r0;
        this.A01 = r7.A03.A00() - r0.A05 >= 600000 ? 1 : 0;
        this.A00 = i;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((C30751Yr) it.next()).A06.getRawString());
        }
        this.A03 = TextUtils.join("|", arrayList);
    }

    public C35991j8(C30751Yr r2, int i) {
        this.A04 = Collections.singletonList(r2);
        this.A02 = r2;
        this.A01 = 2;
        this.A00 = i;
        this.A03 = r2.A06.getRawString();
    }

    public LatLng A00() {
        List<C30751Yr> list = this.A04;
        double d = 0.0d;
        double d2 = 0.0d;
        for (C30751Yr r6 : list) {
            d += r6.A00;
            d2 += r6.A01;
        }
        return new LatLng(d / ((double) list.size()), d2 / ((double) list.size()));
    }
}
