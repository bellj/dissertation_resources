package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.CompoundButton;

/* renamed from: X.0Po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05440Po {
    public ColorStateList A00 = null;
    public PorterDuff.Mode A01 = null;
    public boolean A02 = false;
    public boolean A03 = false;
    public boolean A04;
    public final CompoundButton A05;

    public C05440Po(CompoundButton compoundButton) {
        this.A05 = compoundButton;
    }

    public int A00(int i) {
        Drawable A00;
        return (Build.VERSION.SDK_INT >= 17 || (A00 = AnonymousClass0TP.A00(this.A05)) == null) ? i : i + A00.getIntrinsicWidth();
    }

    public void A01() {
        CompoundButton compoundButton = this.A05;
        Drawable A00 = AnonymousClass0TP.A00(compoundButton);
        if (A00 == null) {
            return;
        }
        if (this.A02 || this.A03) {
            Drawable mutate = C015607k.A03(A00).mutate();
            if (this.A02) {
                C015607k.A04(this.A00, mutate);
            }
            if (this.A03) {
                C015607k.A07(this.A01, mutate);
            }
            if (mutate.isStateful()) {
                mutate.setState(compoundButton.getDrawableState());
            }
            compoundButton.setButtonDrawable(mutate);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0058 A[Catch: all -> 0x0078, TryCatch #1 {all -> 0x0078, blocks: (B:3:0x0019, B:5:0x001f, B:7:0x0026, B:8:0x0036, B:10:0x003c, B:12:0x0042, B:13:0x0051, B:15:0x0058, B:16:0x005f, B:18:0x0066), top: B:27:0x0019 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0066 A[Catch: all -> 0x0078, TRY_LEAVE, TryCatch #1 {all -> 0x0078, blocks: (B:3:0x0019, B:5:0x001f, B:7:0x0026, B:8:0x0036, B:10:0x003c, B:12:0x0042, B:13:0x0051, B:15:0x0058, B:16:0x005f, B:18:0x0066), top: B:27:0x0019 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.util.AttributeSet r12, int r13) {
        /*
            r11 = this;
            android.widget.CompoundButton r8 = r11.A05
            android.content.Context r0 = r8.getContext()
            int[] r9 = X.AnonymousClass07O.A0B
            r4 = 0
            r7 = r12
            r10 = r13
            X.06h r2 = X.C013406h.A00(r0, r12, r9, r13, r4)
            android.content.Context r5 = r8.getContext()
            android.content.res.TypedArray r6 = r2.A02
            X.AnonymousClass028.A0L(r5, r6, r7, r8, r9, r10)
            r0 = 1
            boolean r0 = r6.hasValue(r0)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0036
            r0 = 1
            int r3 = r6.getResourceId(r0, r4)     // Catch: all -> 0x0078
            if (r3 == 0) goto L_0x0036
            android.content.Context r1 = r8.getContext()     // Catch: NotFoundException -> 0x0036, all -> 0x0078
            X.05t r0 = X.C012005t.A01()     // Catch: NotFoundException -> 0x0036, all -> 0x0078
            android.graphics.drawable.Drawable r0 = r0.A04(r1, r3)     // Catch: NotFoundException -> 0x0036, all -> 0x0078
            r8.setButtonDrawable(r0)     // Catch: NotFoundException -> 0x0036, all -> 0x0078
            goto L_0x0051
        L_0x0036:
            boolean r0 = r6.hasValue(r4)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0051
            int r3 = r6.getResourceId(r4, r4)     // Catch: all -> 0x0078
            if (r3 == 0) goto L_0x0051
            android.content.Context r1 = r8.getContext()     // Catch: all -> 0x0078
            X.05t r0 = X.C012005t.A01()     // Catch: all -> 0x0078
            android.graphics.drawable.Drawable r0 = r0.A04(r1, r3)     // Catch: all -> 0x0078
            r8.setButtonDrawable(r0)     // Catch: all -> 0x0078
        L_0x0051:
            r1 = 2
            boolean r0 = r6.hasValue(r1)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x005f
            android.content.res.ColorStateList r0 = r2.A01(r1)     // Catch: all -> 0x0078
            X.AnonymousClass0TP.A01(r0, r8)     // Catch: all -> 0x0078
        L_0x005f:
            r0 = 3
            boolean r0 = r6.hasValue(r0)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0074
            r1 = 3
            r0 = -1
            int r1 = r6.getInt(r1, r0)     // Catch: all -> 0x0078
            r0 = 0
            android.graphics.PorterDuff$Mode r0 = X.C014706y.A00(r0, r1)     // Catch: all -> 0x0078
            X.AnonymousClass0TP.A02(r0, r8)     // Catch: all -> 0x0078
        L_0x0074:
            r2.A04()
            return
        L_0x0078:
            r0 = move-exception
            r2.A04()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05440Po.A02(android.util.AttributeSet, int):void");
    }
}
