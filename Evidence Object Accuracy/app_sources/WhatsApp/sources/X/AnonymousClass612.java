package X;

import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.612  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass612 {
    public static C122935mL A00(View.OnClickListener onClickListener, String str, int i) {
        C122935mL A00 = C122935mL.A00(onClickListener, null, str, i, true);
        A00.A06 = false;
        return A00;
    }

    public static List A01(List list) {
        boolean z;
        ArrayList A0l = C12960it.A0l();
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC28901Pl A0H = C117305Zk.A0H(it);
                AnonymousClass1ZY r1 = A0H.A08;
                if (r1 instanceof C119765f4) {
                    z = ((C119765f4) r1).A06;
                } else if (r1 instanceof C119735f1) {
                    z = ((C119735f1) r1).A05;
                }
                A0l.add(new C127025tp(A0H, z));
            }
        }
        return A0l;
    }
}
