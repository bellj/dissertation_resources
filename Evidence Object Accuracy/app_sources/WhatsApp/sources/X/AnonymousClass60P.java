package X;

import com.whatsapp.R;

/* renamed from: X.60P  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60P {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final AnonymousClass609 A05;
    public final AbstractC136356Mf A06;
    public final C1310260x A07;
    public final C1310260x A08;
    public final C1310260x A09;

    public AnonymousClass60P() {
        this(new AnonymousClass609(-1, -1, R.dimen.nux_icon_size, R.dimen.nux_icon_size), null, new C1310260x(new Object[]{""}, 0), new C1310260x(new Object[]{""}, 0), new C1310260x(new Object[]{""}, 0), -1, 8, -1, -1, 0);
    }

    public AnonymousClass60P(AnonymousClass609 r1, AbstractC136356Mf r2, C1310260x r3, C1310260x r4, C1310260x r5, int i, int i2, int i3, int i4, int i5) {
        this.A07 = r3;
        this.A09 = r4;
        this.A08 = r5;
        this.A01 = i5;
        this.A05 = r1;
        this.A00 = i;
        this.A02 = i2;
        this.A04 = i3;
        this.A03 = i4;
        this.A06 = r2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass60P.class != obj.getClass()) {
            return false;
        }
        AnonymousClass60P r5 = (AnonymousClass60P) obj;
        if (this.A00 == r5.A00 && this.A02 == r5.A02 && this.A04 == r5.A04 && this.A01 == r5.A01 && this.A07.equals(r5.A07) && this.A09.equals(r5.A09) && this.A08.equals(r5.A08)) {
            AnonymousClass609 r1 = this.A05;
            AnonymousClass609 r0 = r5.A05;
            if (r1 == null) {
                if (r0 == null) {
                    return true;
                }
            } else if (r0 != null && r1.equals(r0)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return ((((((((((this.A02 + 31) * 31) + this.A04) * 31) + this.A01) * 31) + this.A07.hashCode()) * 31) + this.A09.hashCode()) * 31) + this.A08.hashCode();
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("PaymentBannerConfiguration{bannerVisibility=");
        A0k.append(this.A02);
        A0k.append(", ctaButtonVisibility=");
        A0k.append(this.A04);
        A0k.append(", bannerType=");
        A0k.append(this.A01);
        A0k.append(", cta=");
        A0k.append(this.A07);
        A0k.append(", title=");
        A0k.append(this.A09);
        A0k.append(", description=");
        A0k.append(this.A08);
        A0k.append(", bannerOnClickListener=");
        A0k.append(this.A06);
        return C12970iu.A0v(A0k);
    }
}
