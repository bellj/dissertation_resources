package X;

import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;
import java.util.Date;

/* renamed from: X.2US  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2US implements AbstractC35611iN, AnonymousClass2U0 {
    public final long A00;
    public final Uri A01;
    public final File A02;

    @Override // X.AbstractC35611iN
    public String AES() {
        return "video/*";
    }

    @Override // X.AnonymousClass2U0
    public int AGI() {
        return 0;
    }

    @Override // X.AnonymousClass2U0
    public byte AHf() {
        return 3;
    }

    @Override // X.AnonymousClass2U0
    public boolean AJR() {
        return false;
    }

    @Override // X.AbstractC35611iN
    public int getType() {
        return 1;
    }

    public AnonymousClass2US(File file) {
        Uri fromFile = Uri.fromFile(file);
        long length = file.length();
        this.A01 = fromFile;
        this.A00 = length;
        this.A02 = file;
    }

    @Override // X.AbstractC35611iN
    public Uri AAE() {
        return this.A01;
    }

    @Override // X.AbstractC35611iN
    public long ACQ() {
        return new Date(this.A02.lastModified()).getTime();
    }

    @Override // X.AbstractC35611iN
    public /* synthetic */ long ACb() {
        return 0;
    }

    @Override // X.AnonymousClass2U0
    public File ACy() {
        return this.A02;
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        File file;
        String path = this.A01.getPath();
        if (path == null) {
            file = null;
        } else {
            file = new File(path);
        }
        return C26521Du.A01(file);
    }

    @Override // X.AbstractC35611iN
    public long getContentLength() {
        return this.A00;
    }
}
