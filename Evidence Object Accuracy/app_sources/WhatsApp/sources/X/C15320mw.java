package X;

import android.app.Activity;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.0mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15320mw extends C15330mx {
    public AbstractC14000kg A00;
    public final C16120oU A01;
    public final GifSearchContainer A02;
    public final AbstractC253919f A03;

    public C15320mw(Activity activity, AnonymousClass01d r22, C14820m6 r23, AnonymousClass018 r24, AnonymousClass19M r25, C231510o r26, EmojiSearchContainer emojiSearchContainer, C14850m9 r28, C16120oU r29, C15260mp r30, C253719d r31, GifSearchContainer gifSearchContainer, AbstractC253919f r33, C16630pM r34, C252718t r35) {
        super(activity, r24, r25, r30, r26, emojiSearchContainer, r34);
        this.A01 = r29;
        this.A03 = r33;
        this.A02 = gifSearchContainer;
        C63463Br r1 = new C63463Br(activity, r22, r23, r28, r29, r30, this, r31, gifSearchContainer, r34, r35);
        AnonymousClass3XR r0 = r30.A09;
        if (r0 != null) {
            r0.A02 = r1;
        }
    }
}
