package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57192md extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57192md A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AnonymousClass1K6 A01 = AnonymousClass277.A01;
    public String A02 = "";

    static {
        C57192md r0 = new C57192md();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        switch (r5.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57192md r7 = (C57192md) obj2;
                this.A02 = r6.Afy(this.A02, r7.A02, C12960it.A1R(this.A00), C12960it.A1R(r7.A00));
                this.A01 = r6.Afr(this.A01, r7.A01);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A032 = r62.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                String A0A = r62.A0A();
                                this.A00 = 1 | this.A00;
                                this.A02 = A0A;
                            } else if (A032 == 18) {
                                AnonymousClass1K6 r1 = this.A01;
                                if (!((AnonymousClass1K7) r1).A00) {
                                    r1 = AbstractC27091Fz.A0G(r1);
                                    this.A01 = r1;
                                }
                                r1.add((C57022mL) AbstractC27091Fz.A0H(r62, r72, C57022mL.A02));
                            } else if (!A0a(r62, A032)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A01);
                return null;
            case 4:
                return new C57192md();
            case 5:
                return new C82123v7();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57192md.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A02) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A01.size(); i3++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A01.get(i3), 2, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A02);
        }
        int i = 0;
        while (i < this.A01.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A01, i, 2);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
