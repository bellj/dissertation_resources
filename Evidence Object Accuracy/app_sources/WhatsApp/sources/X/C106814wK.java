package X;

import android.util.Log;
import android.util.SparseArray;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/* renamed from: X.4wK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106814wK implements AbstractC116785Ww {
    public static final Map A0r;
    public static final UUID A0s = new UUID(72057594037932032L, -9223371306706625679L);
    public static final byte[] A0t = "Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text".getBytes(C88814Hf.A05);
    public static final byte[] A0u = {68, 105, 97, 108, 111, 103, 117, 101, 58, 32, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44};
    public static final byte[] A0v = {49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};
    public byte A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public long A0D;
    public long A0E;
    public long A0F = -9223372036854775807L;
    public long A0G = -1;
    public long A0H = -9223372036854775807L;
    public long A0I = -9223372036854775807L;
    public long A0J;
    public long A0K = -1;
    public long A0L = -1;
    public long A0M;
    public long A0N = -9223372036854775807L;
    public AbstractC14070ko A0O;
    public AnonymousClass4WE A0P;
    public C92534Wh A0Q;
    public C92534Wh A0R;
    public ByteBuffer A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public int[] A0c;
    public final SparseArray A0d;
    public final AnonymousClass5Q7 A0e;
    public final C94054bB A0f;
    public final C95304dT A0g;
    public final C95304dT A0h;
    public final C95304dT A0i;
    public final C95304dT A0j;
    public final C95304dT A0k;
    public final C95304dT A0l;
    public final C95304dT A0m;
    public final C95304dT A0n;
    public final C95304dT A0o;
    public final C95304dT A0p;
    public final boolean A0q;

    static {
        HashMap A11 = C12970iu.A11();
        A11.put("htc_video_rotA-000", C12980iv.A0i());
        A11.put("htc_video_rotA-090", 90);
        A11.put("htc_video_rotA-180", 180);
        A11.put("htc_video_rotA-270", 270);
        A0r = Collections.unmodifiableMap(A11);
    }

    public C106814wK() {
        C107014we r4 = new C107014we();
        this.A0e = r4;
        r4.A03 = new C107004wd(this);
        this.A0q = true;
        this.A0f = new C94054bB();
        this.A0d = new SparseArray();
        this.A0m = C95304dT.A05(4);
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(-1);
        this.A0p = new C95304dT(allocate.array());
        this.A0n = C95304dT.A05(4);
        this.A0k = new C95304dT(C95464dl.A02);
        this.A0j = C95304dT.A05(4);
        this.A0l = new C95304dT();
        this.A0o = new C95304dT();
        this.A0h = C95304dT.A05(8);
        this.A0i = new C95304dT();
        this.A0g = new C95304dT();
        this.A0c = new int[1];
    }

    public static AnonymousClass4WE A00(C106814wK r0, int i) {
        r0.A06(i);
        return r0.A0P;
    }

    public static void A01(ByteBuffer byteBuffer, float f) {
        byteBuffer.putShort((short) ((int) ((f * 50000.0f) + 0.5f)));
    }

    public static byte[] A02(String str, long j, long j2) {
        C95314dV.A03(C12960it.A1S((j > -9223372036854775807L ? 1 : (j == -9223372036854775807L ? 0 : -1))));
        int i = (int) (j / 3600000000L);
        long j3 = j - (((long) (i * 3600)) * SearchActionVerificationClientService.MS_TO_NS);
        int i2 = (int) (j3 / 60000000);
        long j4 = j3 - (((long) (i2 * 60)) * SearchActionVerificationClientService.MS_TO_NS);
        int i3 = (int) (j4 / SearchActionVerificationClientService.MS_TO_NS);
        Locale locale = Locale.US;
        Object[] objArr = new Object[4];
        C12960it.A1P(objArr, i, 0);
        C12960it.A1P(objArr, i2, 1);
        C12960it.A1P(objArr, i3, 2);
        C12960it.A1P(objArr, (int) ((j4 - (((long) i3) * SearchActionVerificationClientService.MS_TO_NS)) / j2), 3);
        return String.format(locale, str, objArr).getBytes(C88814Hf.A05);
    }

    public final int A03(AnonymousClass5Yf r18, AnonymousClass4WE r19, int i) {
        int AbF;
        int AbF2;
        int i2;
        byte[] bArr;
        String str = r19.A0e;
        if ("S_TEXT/UTF8".equals(str)) {
            bArr = A0v;
        } else if ("S_TEXT/ASS".equals(str)) {
            bArr = A0u;
        } else {
            AnonymousClass5X6 r6 = r19.A0c;
            boolean z = true;
            if (!this.A0V) {
                if (r19.A0k) {
                    this.A02 &= -1073741825;
                    int i3 = 128;
                    if (!this.A0Y) {
                        C95304dT r7 = this.A0m;
                        r18.readFully(r7.A02, 0, 1);
                        this.A08++;
                        byte b = r7.A02[0];
                        if ((b & 128) != 128) {
                            this.A00 = b;
                            this.A0Y = true;
                        } else {
                            throw AnonymousClass496.A00("Extension bit is set in signal byte");
                        }
                    }
                    byte b2 = this.A00;
                    if ((b2 & 1) == 1) {
                        boolean A1V = C12960it.A1V(b2 & 2, 2);
                        this.A02 |= 1073741824;
                        if (!this.A0W) {
                            C95304dT r12 = this.A0h;
                            r18.readFully(r12.A02, 0, 8);
                            this.A08 += 8;
                            this.A0W = true;
                            C95304dT r72 = this.A0m;
                            byte[] bArr2 = r72.A02;
                            if (!A1V) {
                                i3 = 0;
                            }
                            C72463ee.A0O(i3, bArr2, 8, 0);
                            r72.A0S(0);
                            r6.AbD(r72, 1, 1);
                            this.A09++;
                            r12.A0S(0);
                            r6.AbD(r12, 8, 1);
                            this.A09 += 8;
                        }
                        if (A1V) {
                            if (!this.A0X) {
                                C95304dT r73 = this.A0m;
                                r18.readFully(r73.A02, 0, 1);
                                this.A08++;
                                r73.A0S(0);
                                this.A0B = r73.A0C();
                                this.A0X = true;
                            }
                            int i4 = this.A0B << 2;
                            C95304dT r14 = this.A0m;
                            r14.A0Q(i4);
                            r18.readFully(r14.A02, 0, i4);
                            this.A08 += i4;
                            short s = (short) ((this.A0B / 2) + 1);
                            int i5 = (s * 6) + 2;
                            ByteBuffer byteBuffer = this.A0S;
                            if (byteBuffer == null || byteBuffer.capacity() < i5) {
                                this.A0S = ByteBuffer.allocate(i5);
                            }
                            this.A0S.position(0);
                            this.A0S.putShort(s);
                            int i6 = 0;
                            int i7 = 0;
                            while (true) {
                                i2 = this.A0B;
                                if (i6 >= i2) {
                                    break;
                                }
                                int A0E = r14.A0E();
                                int i8 = i6 % 2;
                                ByteBuffer byteBuffer2 = this.A0S;
                                int i9 = A0E - i7;
                                if (i8 == 0) {
                                    byteBuffer2.putShort((short) i9);
                                } else {
                                    byteBuffer2.putInt(i9);
                                }
                                i6++;
                                i7 = A0E;
                            }
                            int i10 = (i - this.A08) - i7;
                            int i11 = i2 % 2;
                            ByteBuffer byteBuffer3 = this.A0S;
                            if (i11 == 1) {
                                byteBuffer3.putInt(i10);
                            } else {
                                byteBuffer3.putShort((short) i10);
                                this.A0S.putInt(0);
                            }
                            C95304dT r74 = this.A0i;
                            r74.A0U(this.A0S.array(), i5);
                            r6.AbD(r74, i5, 1);
                            this.A09 += i5;
                        }
                    }
                } else {
                    byte[] bArr3 = r19.A0o;
                    if (bArr3 != null) {
                        this.A0l.A0U(bArr3, bArr3.length);
                    }
                }
                if (r19.A0O > 0) {
                    this.A02 |= 268435456;
                    this.A0g.A0Q(0);
                    C95304dT r11 = this.A0m;
                    r11.A0Q(4);
                    byte[] bArr4 = r11.A02;
                    C72463ee.A0V(bArr4, i >> 24, 0);
                    C72463ee.A0V(bArr4, i >> 16, 1);
                    C72463ee.A0V(bArr4, i >> 8, 2);
                    C72463ee.A0V(bArr4, i, 3);
                    r6.AbD(r11, 4, 2);
                    this.A09 += 4;
                }
                this.A0V = true;
            }
            C95304dT r75 = this.A0l;
            int i12 = r75.A00;
            int i13 = i + i12;
            String str2 = r19.A0e;
            if (!"V_MPEG4/ISO/AVC".equals(str2) && !"V_MPEGH/ISO/HEVC".equals(str2)) {
                AnonymousClass4W2 r2 = r19.A0d;
                if (r2 != null) {
                    if (i12 != 0) {
                        z = false;
                    }
                    C95314dV.A04(z);
                    if (!r2.A05) {
                        byte[] bArr5 = r2.A06;
                        r18.AZ4(bArr5, 0, 10);
                        r18.Aaj();
                        boolean z2 = false;
                        if (bArr5[4] == -8 && bArr5[5] == 114 && bArr5[6] == 111) {
                            byte b3 = bArr5[7];
                            if ((b3 & 254) == 186) {
                                if ((b3 & 255) == 187) {
                                    z2 = true;
                                }
                                char c = '\b';
                                if (z2) {
                                    c = '\t';
                                }
                                if ((40 << ((bArr5[c] >> 4) & 7)) != 0) {
                                    r2.A05 = true;
                                }
                            }
                        }
                    }
                }
                while (true) {
                    int i14 = this.A08;
                    if (i14 >= i13) {
                        break;
                    }
                    int i15 = i13 - i14;
                    int A00 = C95304dT.A00(r75);
                    if (A00 > 0) {
                        AbF2 = Math.min(i15, A00);
                        r6.AbC(r75, AbF2);
                    } else {
                        AbF2 = r6.AbF(r18, i15, 0, false);
                    }
                    this.A08 += AbF2;
                    this.A09 += AbF2;
                }
            } else {
                C95304dT r13 = this.A0j;
                byte[] bArr6 = r13.A02;
                bArr6[0] = 0;
                bArr6[1] = 0;
                bArr6[2] = 0;
                int i16 = r19.A0R;
                int i17 = 4 - i16;
                while (this.A08 < i13) {
                    int i18 = this.A0A;
                    int A002 = C95304dT.A00(r75);
                    if (i18 == 0) {
                        int min = Math.min(i16, A002);
                        r18.readFully(bArr6, i17 + min, i16 - min);
                        if (min > 0) {
                            r75.A0V(bArr6, i17, min);
                        }
                        this.A08 += i16;
                        this.A0A = C95304dT.A02(r13, 0);
                        C95304dT r0 = this.A0k;
                        r0.A0S(0);
                        r6.AbC(r0, 4);
                        this.A09 += 4;
                    } else {
                        if (A002 > 0) {
                            AbF = Math.min(i18, A002);
                            r6.AbC(r75, AbF);
                        } else {
                            AbF = r6.AbF(r18, i18, 0, false);
                        }
                        this.A08 += AbF;
                        this.A09 += AbF;
                        this.A0A -= AbF;
                    }
                }
            }
            if ("A_VORBIS".equals(r19.A0e)) {
                C95304dT r02 = this.A0p;
                r02.A0S(0);
                r6.AbC(r02, 4);
                this.A09 += 4;
            }
            int i19 = this.A09;
            this.A08 = 0;
            this.A09 = 0;
            this.A0A = 0;
            this.A0V = false;
            this.A0Y = false;
            this.A0X = false;
            this.A0B = 0;
            this.A00 = 0;
            this.A0W = false;
            this.A0l.A0Q(0);
            return i19;
        }
        int length = bArr.length;
        int i20 = length + i;
        C95304dT r3 = this.A0o;
        byte[] bArr7 = r3.A02;
        if (bArr7.length < i20) {
            byte[] copyOf = Arrays.copyOf(bArr, i20 + i);
            r3.A0U(copyOf, copyOf.length);
        } else {
            System.arraycopy(bArr, 0, bArr7, 0, length);
        }
        r18.readFully(r3.A02, length, i);
        r3.A0S(0);
        r3.A0R(i20);
        int i19 = this.A09;
        this.A08 = 0;
        this.A09 = 0;
        this.A0A = 0;
        this.A0V = false;
        this.A0Y = false;
        this.A0X = false;
        this.A0B = 0;
        this.A00 = 0;
        this.A0W = false;
        this.A0l.A0Q(0);
        return i19;
    }

    public final long A04(long j) {
        long j2 = this.A0N;
        if (j2 != -9223372036854775807L) {
            return AnonymousClass3JZ.A07(j, j2, 1000);
        }
        throw AnonymousClass496.A00("Can't scale timecode prior to timecodeScale being set.");
    }

    public final void A05(int i) {
        if (this.A0R == null || this.A0Q == null) {
            StringBuilder A0k = C12960it.A0k("Element ");
            A0k.append(i);
            throw AnonymousClass496.A00(C12960it.A0d(" must be in a Cues", A0k));
        }
    }

    public final void A06(int i) {
        if (this.A0P == null) {
            StringBuilder A0k = C12960it.A0k("Element ");
            A0k.append(i);
            throw AnonymousClass496.A00(C12960it.A0d(" must be in a TrackEntry", A0k));
        }
    }

    public final void A07(AnonymousClass5Yf r5, int i) {
        C95304dT r3 = this.A0m;
        if (r3.A00 < i) {
            int length = r3.A02.length;
            if (length < i) {
                r3.A0P(Math.max(length << 1, i));
            }
            byte[] bArr = r3.A02;
            int i2 = r3.A00;
            r5.readFully(bArr, i2, i - i2);
            r3.A0R(i);
        }
    }

    public final void A08(AnonymousClass4WE r18, int i, int i2, int i3, long j) {
        byte[] A02;
        int i4;
        String str;
        int i5 = i;
        int i6 = i2;
        AnonymousClass4W2 r2 = r18.A0d;
        if (r2 == null) {
            String str2 = r18.A0e;
            if ("S_TEXT/UTF8".equals(str2) || "S_TEXT/ASS".equals(str2)) {
                if (this.A03 > 1) {
                    str = "Skipping subtitle sample in laced block.";
                } else {
                    long j2 = this.A0D;
                    if (j2 == -9223372036854775807L) {
                        str = "Skipping subtitle sample with no duration.";
                    } else {
                        C95304dT r8 = this.A0o;
                        byte[] bArr = r8.A02;
                        if (str2.equals("S_TEXT/ASS")) {
                            A02 = A02("%01d:%02d:%02d:%02d", j2, 10000);
                            i4 = 21;
                        } else if (str2.equals("S_TEXT/UTF8")) {
                            A02 = A02("%02d:%02d:%02d,%03d", j2, 1000);
                            i4 = 19;
                        } else {
                            throw C72453ed.A0h();
                        }
                        System.arraycopy(A02, 0, bArr, i4, A02.length);
                        int i7 = r8.A01;
                        while (true) {
                            if (i7 >= r8.A00) {
                                break;
                            } else if (r8.A02[i7] == 0) {
                                r8.A0R(i7);
                                break;
                            } else {
                                i7++;
                            }
                        }
                        r18.A0c.AbC(r8, r8.A00);
                        i6 = i2 + r8.A00;
                    }
                }
                Log.w("MatroskaExtractor", str);
            }
            if ((268435456 & i) != 0) {
                if (this.A03 > 1) {
                    i5 = i & -268435457;
                } else {
                    C95304dT r3 = this.A0g;
                    int i8 = r3.A00;
                    r18.A0c.AbD(r3, i8, 2);
                    i6 += i8;
                }
            }
            r18.A0c.AbG(r18.A0b, i5, i6, i3, j);
        } else if (r2.A05) {
            int i9 = r2.A02;
            int i10 = i9 + 1;
            r2.A02 = i10;
            if (i9 == 0) {
                r2.A04 = j;
                r2.A00 = i5;
                r2.A03 = 0;
            }
            r2.A03 += i2;
            r2.A01 = i3;
            if (i10 >= 16) {
                r2.A00(r18);
            }
        }
        this.A0U = true;
    }

    @Override // X.AbstractC116785Ww
    public final void AIa(AbstractC14070ko r1) {
        this.A0O = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0321, code lost:
        if (r0 != false) goto L_0x01d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:333:0x07fc, code lost:
        if (java.lang.Float.compare(r1, -90.0f) != 0) goto L_0x07fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:445:0x09ce, code lost:
        if (r1 == 2) goto L_0x0d0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:447:0x09d1, code lost:
        if (r1 == 3) goto L_0x0c96;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:448:0x09d3, code lost:
        if (r1 == 4) goto L_0x0a0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:450:0x09d6, code lost:
        if (r1 != 5) goto L_0x10c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:451:0x09d8, code lost:
        r0 = r7.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:452:0x09de, code lost:
        if (r0 == 4) goto L_0x09f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:454:0x09e2, code lost:
        if (r0 == 8) goto L_0x09f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:456:0x09f2, code lost:
        throw X.AnonymousClass496.A00(X.C12970iu.A0w(X.C12960it.A0k("Invalid float size: "), r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:457:0x09f3, code lost:
        r9 = (int) r0;
        r11 = r7.A06;
        r10 = 0;
        r29.readFully(r11, 0, r9);
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:458:0x09fe, code lost:
        if (r10 >= r9) goto L_0x0f69;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:459:0x0a00, code lost:
        r0 = (r0 << 8) | ((long) (r11[r10] & 255));
        r10 = r10 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:460:0x0a0c, code lost:
        r11 = (int) r7.A02;
        r9 = ((X.C107004wd) r8).A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:461:0x0a19, code lost:
        if (r6 == 161) goto L_0x0aca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:462:0x0a1b, code lost:
        if (r6 == 163) goto L_0x0aca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:464:0x0a1f, code lost:
        if (r6 == 165) goto L_0x0a6f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:466:0x0a23, code lost:
        if (r6 == 16877) goto L_0x0a5a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:468:0x0a27, code lost:
        if (r6 == 16981) goto L_0x0a51;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:470:0x0a2b, code lost:
        if (r6 == 18402) goto L_0x0aaf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:472:0x0a2f, code lost:
        if (r6 == 21419) goto L_0x0a93;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:474:0x0a33, code lost:
        if (r6 == 25506) goto L_0x0a48;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:476:0x0a37, code lost:
        if (r6 != 30322) goto L_0x10d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:477:0x0a39, code lost:
        r1 = new byte[r11];
        A00(r9, r6).A0n = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:478:0x0a41, code lost:
        r29.readFully(r1, 0, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:479:0x0a48, code lost:
        r1 = new byte[r11];
        A00(r9, r6).A0l = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:480:0x0a51, code lost:
        r1 = new byte[r11];
        A00(r9, r6).A0o = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:481:0x0a5a, code lost:
        r2 = A00(r9, 16877);
        r1 = r2.A0E;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:482:0x0a63, code lost:
        if (r1 == 1685485123) goto L_0x0a6a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:484:0x0a68, code lost:
        if (r1 != 1685480259) goto L_0x0ac3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:485:0x0a6a, code lost:
        r1 = new byte[r11];
        r2.A0m = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:487:0x0a71, code lost:
        if (r9.A05 != 2) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:488:0x0a73, code lost:
        r1 = (X.AnonymousClass4WE) r9.A0d.get(r9.A06);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:489:0x0a7f, code lost:
        if (r9.A01 != 4) goto L_0x0ac3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:491:0x0a89, code lost:
        if ("V_VP9".equals(r1.A0e) == false) goto L_0x0ac3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:492:0x0a8b, code lost:
        r0 = r9.A0g;
        r0.A0Q(r11);
        r1 = r0.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:493:0x0a93, code lost:
        r3 = r9.A0n;
        java.util.Arrays.fill(r3.A02, (byte) 0);
        r29.readFully(r3.A02, 4 - r11, r11);
        r3.A0S(0);
        r9.A0C = (int) r3.A0I();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:494:0x0aaf, code lost:
        r2 = new byte[r11];
        r29.readFully(r2, 0, r11);
        A00(r9, 18402).A0b = new X.AnonymousClass4XD(1, r2, 0, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:495:0x0ac3, code lost:
        r29.Ae3(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:496:0x0aca, code lost:
        r2 = r9.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:497:0x0ace, code lost:
        if (r2 != 0) goto L_0x0af0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:498:0x0ad0, code lost:
        r3 = r9.A0f;
        r9.A06 = (int) r3.A00(r29, 8, false, true);
        r9.A07 = r3.A00;
        r9.A0D = -9223372036854775807L;
        r9.A05 = 1;
        r2 = 1;
        r9.A0m.A0Q(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:499:0x0af0, code lost:
        r8 = (X.AnonymousClass4WE) r9.A0d.get(r9.A06);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:500:0x0afa, code lost:
        if (r8 != null) goto L_0x0b08;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:501:0x0afc, code lost:
        r29.Ae3(r11 - r9.A07);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:502:0x0b04, code lost:
        r9.A05 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:503:0x0b08, code lost:
        if (r2 != 1) goto L_0x0b59;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:504:0x0b0a, code lost:
        r9.A07(r29, 3);
        r14 = r9.A0m;
        r3 = (r14.A02[2] & 6) >> 1;
        r0 = 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:505:0x0b1b, code lost:
        if (r3 != 0) goto L_0x0b95;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:506:0x0b1d, code lost:
        r9.A03 = 1;
        r2 = r9.A0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:507:0x0b21, code lost:
        if (r2 != null) goto L_0x0b89;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:508:0x0b23, code lost:
        r2 = new int[1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:509:0x0b25, code lost:
        r9.A0c = r2;
        r2[0] = (r11 - r9.A07) - 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:510:0x0b2d, code lost:
        r10 = r14.A02;
        r9.A0E = r9.A0F + r9.A04((long) ((r10[0] << 8) | (r10[1] & r0)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:511:0x0b44, code lost:
        if (r8.A0W == 2) goto L_0x0b51;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:513:0x0b48, code lost:
        if (r6 != 163) goto L_0x0b87;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:515:0x0b4f, code lost:
        if ((r10[2] & 128) != 128) goto L_0x0b87;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:516:0x0b51, code lost:
        r0 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:517:0x0b52, code lost:
        r9.A02 = r0;
        r9.A05 = 2;
        r9.A04 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:519:0x0b5b, code lost:
        if (r6 != 163) goto L_0x0c7e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:520:0x0b5d, code lost:
        r1 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:521:0x0b61, code lost:
        if (r1 >= r9.A03) goto L_0x0b04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:522:0x0b63, code lost:
        r9.A08(r8, r9.A02, r9.A03(r29, r8, r9.A0c[r1]), 0, ((long) ((r9.A04 * r8.A0J) / 1000)) + r9.A0E);
        r9.A04++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:523:0x0b87, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:524:0x0b89, code lost:
        r1 = r2.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:525:0x0b8a, code lost:
        if (r1 >= 1) goto L_0x0b25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:526:0x0b8c, code lost:
        r2 = new int[java.lang.Math.max(r1 << 1, 1)];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:527:0x0b95, code lost:
        r13 = 4;
        r9.A07(r29, 4);
        r15 = (r14.A02[3] & 255) + 1;
        r9.A03 = r15;
        r2 = r9.A0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:528:0x0ba5, code lost:
        if (r2 != null) goto L_0x0bb8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:529:0x0ba7, code lost:
        r2 = new int[r15];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:530:0x0ba9, code lost:
        r9.A0c = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:531:0x0bac, code lost:
        if (r3 != 2) goto L_0x0bc4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:532:0x0bae, code lost:
        java.util.Arrays.fill(r2, 0, r15, ((r11 - r9.A07) - 4) / r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:533:0x0bb8, code lost:
        r1 = r2.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:534:0x0bb9, code lost:
        if (r1 >= r15) goto L_0x0ba9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:535:0x0bbb, code lost:
        r2 = new int[java.lang.Math.max(r1 << 1, r15)];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:536:0x0bc4, code lost:
        if (r3 != 1) goto L_0x0bf4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:537:0x0bc6, code lost:
        r10 = 0;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:538:0x0bc8, code lost:
        r3 = r9.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:539:0x0bcc, code lost:
        if (r10 >= (r3 - 1)) goto L_0x0bec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:540:0x0bce, code lost:
        r2[r10] = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:541:0x0bd0, code lost:
        r13 = r13 + 1;
        r9.A07(r29, r13);
        r3 = r14.A02[r13 - 1] & 255;
        r2 = r9.A0c;
        r2[r10] = r2[r10] + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:542:0x0be4, code lost:
        if (r3 == 255) goto L_0x0bd0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:543:0x0be6, code lost:
        r12 = r12 + r2[r10];
        r10 = r10 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:544:0x0bec, code lost:
        r3 = r3 - 1;
        r11 = ((r11 - r9.A07) - r13) - r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:545:0x0bf4, code lost:
        if (r3 != 3) goto L_0x10e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:546:0x0bf6, code lost:
        r15 = 0;
        r19 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:547:0x0bf9, code lost:
        r3 = r9.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:548:0x0bfd, code lost:
        if (r15 >= (r3 - 1)) goto L_0x0c73;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:549:0x0bff, code lost:
        r2[r15] = 0;
        r13 = r13 + 1;
        r9.A07(r29, r13);
        r10 = r14.A02;
        r17 = r13 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:550:0x0c0e, code lost:
        if (r10[r17] == 0) goto L_0x10e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:551:0x0c10, code lost:
        r2 = 0;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:552:0x0c13, code lost:
        r16 = 1 << (7 - r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:553:0x0c1b, code lost:
        if ((r10[r17] & r16) == 0) goto L_0x0c3e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:554:0x0c1d, code lost:
        r13 = r13 + r12;
        r9.A07(r29, r13);
        r10 = r14.A02;
        r1 = r17 + 1;
        r2 = (long) ((r10[r17] & r0) & (r16 ^ -1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:555:0x0c2e, code lost:
        if (r1 >= r13) goto L_0x0c45;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:556:0x0c30, code lost:
        r1 = r1 + 1;
        r2 = (r2 << 8) | ((long) (r10[r1] & r0));
        r0 = 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:557:0x0c3e, code lost:
        r12 = r12 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:558:0x0c42, code lost:
        if (r12 >= 8) goto L_0x0c60;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:559:0x0c45, code lost:
        if (r15 <= 0) goto L_0x0c52;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:560:0x0c47, code lost:
        r2 = r2 - ((1 << ((r12 * 7) + 6)) - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:562:0x0c57, code lost:
        if (r2 < -2147483648L) goto L_0x10db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:564:0x0c5e, code lost:
        if (r2 > 2147483647L) goto L_0x10db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:565:0x0c60, code lost:
        r1 = (int) r2;
        r2 = r9.A0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:566:0x0c63, code lost:
        if (r15 == 0) goto L_0x0c6a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:567:0x0c65, code lost:
        r1 = r1 + r2[r15 - 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:568:0x0c6a, code lost:
        r2[r15] = r1;
        r19 = r19 + r1;
        r15 = r15 + 1;
        r0 = 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:569:0x0c73, code lost:
        r3 = r3 - 1;
        r11 = ((r11 - r9.A07) - r13) - r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:570:0x0c7a, code lost:
        r2[r3] = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:571:0x0c7e, code lost:
        r3 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:572:0x0c82, code lost:
        if (r3 >= r9.A03) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:573:0x0c84, code lost:
        r2 = r9.A0c;
        r2[r3] = r9.A03(r29, r8, r2[r3]);
        r9.A04++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:574:0x0c96, code lost:
        r0 = r7.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:575:0x0c9d, code lost:
        if (r0 > 2147483647L) goto L_0x10f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:576:0x0c9f, code lost:
        r9 = (int) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:577:0x0ca0, code lost:
        if (r9 != 0) goto L_0x0cc1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:578:0x0ca2, code lost:
        r3 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:579:0x0ca4, code lost:
        r1 = ((X.C107004wd) r8).A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:580:0x0caa, code lost:
        if (r6 == 134) goto L_0x0d04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:582:0x0cae, code lost:
        if (r6 == 17026) goto L_0x0ce0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:584:0x0cb2, code lost:
        if (r6 == 21358) goto L_0x0cd8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:586:0x0cb7, code lost:
        if (r6 != 2274716) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:587:0x0cb9, code lost:
        A00(r1, 2274716).A0f = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:588:0x0cc1, code lost:
        r2 = new byte[r9];
        r29.readFully(r2, 0, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:589:0x0cc8, code lost:
        if (r9 <= 0) goto L_0x0cd2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:590:0x0cca, code lost:
        r1 = r9 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:591:0x0cce, code lost:
        if (r2[r1] != 0) goto L_0x0cd2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:592:0x0cd0, code lost:
        r9 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:593:0x0cd2, code lost:
        r3 = new java.lang.String(r2, 0, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:594:0x0cd8, code lost:
        A00(r1, 21358).A0g = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:596:0x0ce6, code lost:
        if ("webm".equals(r3) != false) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:598:0x0cee, code lost:
        if ("matroska".equals(r3) != false) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:599:0x0cf0, code lost:
        r1 = X.C12960it.A0k("DocType ");
        r1.append(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:600:0x0d03, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:601:0x0d04, code lost:
        A00(r1, 134).A0e = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:602:0x0d0c, code lost:
        r0 = r7.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:603:0x0d10, code lost:
        if (r0 > 8) goto L_0x1115;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:604:0x0d12, code lost:
        r9 = (int) r0;
        r12 = r7.A06;
        r11 = 0;
        r29.readFully(r12, 0, r9);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:605:0x0d1d, code lost:
        if (r11 >= r9) goto L_0x0d2b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:606:0x0d1f, code lost:
        r2 = (r2 << 8) | ((long) (r12[r11] & 255));
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:607:0x0d2b, code lost:
        r8 = ((X.C107004wd) r8).A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:608:0x0d33, code lost:
        if (r6 == 20529) goto L_0x0f51;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:610:0x0d39, code lost:
        if (r6 == 20530) goto L_0x0f3b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:611:0x0d3b, code lost:
        r12 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:612:0x0d3e, code lost:
        switch(r6) {
            case 131: goto L_0x0eac;
            case 136: goto L_0x0eb5;
            case 155: goto L_0x0db8;
            case 159: goto L_0x0ec2;
            case 176: goto L_0x0ecb;
            case 179: goto L_0x0dc0;
            case 186: goto L_0x0ed4;
            case 215: goto L_0x0edd;
            case 231: goto L_0x0dce;
            case 238: goto L_0x0dd6;
            case 241: goto L_0x0ddb;
            case 251: goto L_0x0deb;
            case 16871: goto L_0x0ee6;
            case 16980: goto L_0x0def;
            case 17029: goto L_0x0e07;
            case 17143: goto L_0x0e13;
            case 18401: goto L_0x0e29;
            case 18408: goto L_0x0e41;
            case 21420: goto L_0x0e57;
            case 21432: goto L_0x0e5e;
            case 21680: goto L_0x0eef;
            case 21682: goto L_0x0ef8;
            case 21690: goto L_0x0f01;
            case 21930: goto L_0x0f09;
            case 21998: goto L_0x0f15;
            case 22186: goto L_0x0f1d;
            case 22203: goto L_0x0f24;
            case 25188: goto L_0x0f2b;
            case 30321: goto L_0x0e84;
            case 2352003: goto L_0x0f33;
            case 2807729: goto L_0x0ea8;
            default: goto L_0x0d41;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:614:0x0d43, code lost:
        switch(r6) {
            case 21945: goto L_0x0d48;
            case 21946: goto L_0x0d5c;
            case 21947: goto L_0x0d82;
            case 21948: goto L_0x0da6;
            case 21949: goto L_0x0daf;
            default: goto L_0x0d46;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:615:0x0d48, code lost:
        r8.A06(r6);
        r0 = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:616:0x0d4c, code lost:
        if (r0 == 1) goto L_0x0d56;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:617:0x0d4e, code lost:
        if (r0 != 2) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:618:0x0d50, code lost:
        r8.A0P.A0G = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:619:0x0d56, code lost:
        r8.A0P.A0G = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:620:0x0d5c, code lost:
        r8.A06(r6);
        r1 = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:621:0x0d60, code lost:
        if (r1 == 1) goto L_0x0d7c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:623:0x0d64, code lost:
        if (r1 == 16) goto L_0x0d76;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:625:0x0d68, code lost:
        if (r1 == 18) goto L_0x0d70;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:626:0x0d6a, code lost:
        if (r1 == 6) goto L_0x0d7c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:627:0x0d6c, code lost:
        if (r1 == 7) goto L_0x0d7c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:628:0x0d70, code lost:
        r8.A0P.A0I = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:629:0x0d76, code lost:
        r8.A0P.A0I = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:630:0x0d7c, code lost:
        r8.A0P.A0I = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:631:0x0d82, code lost:
        r6 = A00(r8, r6);
        r6.A0j = true;
        r1 = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:632:0x0d89, code lost:
        if (r1 == 1) goto L_0x0da2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:634:0x0d8d, code lost:
        if (r1 == 9) goto L_0x0d9e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:635:0x0d8f, code lost:
        if (r1 == 4) goto L_0x0d9a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:637:0x0d92, code lost:
        if (r1 == 5) goto L_0x0d9a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:638:0x0d94, code lost:
        if (r1 == 6) goto L_0x0d9a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:639:0x0d96, code lost:
        if (r1 == 7) goto L_0x0d9a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:640:0x0d9a, code lost:
        r6.A0H = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:641:0x0d9e, code lost:
        r6.A0H = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:642:0x0da2, code lost:
        r6.A0H = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:643:0x0da6, code lost:
        A00(r8, r6).A0P = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:644:0x0daf, code lost:
        A00(r8, r6).A0Q = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:645:0x0db8, code lost:
        r8.A0D = r8.A04(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:646:0x0dc0, code lost:
        r8.A05(r6);
        r8.A0R.A01(r8.A04(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:647:0x0dce, code lost:
        r8.A0F = r8.A04(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:648:0x0dd6, code lost:
        r8.A01 = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:650:0x0ddd, code lost:
        if (r8.A0a != false) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:651:0x0ddf, code lost:
        r8.A05(r6);
        r8.A0Q.A01(r2);
        r8.A0a = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:652:0x0deb, code lost:
        r8.A0T = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:654:0x0df3, code lost:
        if (r2 == 3) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:655:0x0df5, code lost:
        r0 = X.C12960it.A0k("ContentCompAlgo ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:656:0x0e06, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:658:0x0e09, code lost:
        if (r2 < 1) goto L_0x1103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:660:0x0e0f, code lost:
        if (r2 > 2) goto L_0x1103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:662:0x0e15, code lost:
        if (r2 == 1) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:663:0x0e17, code lost:
        r0 = X.C12960it.A0k("EBMLReadVersion ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:664:0x0e28, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:666:0x0e2d, code lost:
        if (r2 == 5) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:667:0x0e2f, code lost:
        r0 = X.C12960it.A0k("ContentEncAlgo ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:668:0x0e40, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:670:0x0e43, code lost:
        if (r2 == 1) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:671:0x0e45, code lost:
        r0 = X.C12960it.A0k("AESSettingsCipherMode ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:672:0x0e56, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:673:0x0e57, code lost:
        r8.A0J = r2 + r8.A0L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:674:0x0e5e, code lost:
        r1 = (int) r2;
        r8.A06(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:675:0x0e62, code lost:
        if (r1 == 0) goto L_0x0e7e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:676:0x0e64, code lost:
        if (r1 == 1) goto L_0x0e78;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:677:0x0e66, code lost:
        if (r1 == 3) goto L_0x0e72;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:679:0x0e6a, code lost:
        if (r1 != 15) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:680:0x0e6c, code lost:
        r8.A0P.A0V = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:681:0x0e72, code lost:
        r8.A0P.A0V = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:682:0x0e78, code lost:
        r8.A0P.A0V = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:683:0x0e7e, code lost:
        r8.A0P.A0V = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:684:0x0e84, code lost:
        r8.A06(r6);
        r0 = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:685:0x0e88, code lost:
        if (r0 == 0) goto L_0x0ea2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:686:0x0e8a, code lost:
        if (r0 == 1) goto L_0x0e9c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:687:0x0e8c, code lost:
        if (r0 == 2) goto L_0x0e96;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:688:0x0e8e, code lost:
        if (r0 != 3) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:689:0x0e90, code lost:
        r8.A0P.A0T = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:690:0x0e96, code lost:
        r8.A0P.A0T = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:691:0x0e9c, code lost:
        r8.A0P.A0T = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:692:0x0ea2, code lost:
        r8.A0P.A0T = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:693:0x0ea8, code lost:
        r8.A0N = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:694:0x0eac, code lost:
        A00(r8, r6).A0W = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:695:0x0eb5, code lost:
        r1 = A00(r8, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:696:0x0ebb, code lost:
        if (r2 != 1) goto L_0x0ebe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:697:0x0ebd, code lost:
        r12 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:698:0x0ebe, code lost:
        r1.A0h = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:699:0x0ec2, code lost:
        A00(r8, r6).A0F = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:700:0x0ecb, code lost:
        A00(r8, r6).A0X = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:701:0x0ed4, code lost:
        A00(r8, r6).A0N = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:702:0x0edd, code lost:
        A00(r8, r6).A0S = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:703:0x0ee6, code lost:
        A00(r8, r6).A0E = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:704:0x0eef, code lost:
        A00(r8, r6).A0M = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:705:0x0ef8, code lost:
        A00(r8, r6).A0L = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:706:0x0f01, code lost:
        A00(r8, r6).A0K = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:707:0x0f09, code lost:
        r1 = A00(r8, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:708:0x0f0f, code lost:
        if (r2 != 1) goto L_0x0f12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:709:0x0f11, code lost:
        r12 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:710:0x0f12, code lost:
        r1.A0i = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:711:0x0f15, code lost:
        A00(r8, r6).A0O = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:712:0x0f1d, code lost:
        A00(r8, r6).A0Y = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:713:0x0f24, code lost:
        A00(r8, r6).A0Z = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:714:0x0f2b, code lost:
        A00(r8, r6).A0D = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:715:0x0f33, code lost:
        A00(r8, r6).A0J = (int) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:717:0x0f3d, code lost:
        if (r2 == 1) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:718:0x0f3f, code lost:
        r0 = X.C12960it.A0k("ContentEncodingScope ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:719:0x0f50, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:721:0x0f55, code lost:
        if (r2 == 0) goto L_0x0f84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:722:0x0f57, code lost:
        r0 = X.C12960it.A0k("ContentEncodingOrder ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:723:0x0f68, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:725:0x0f6a, code lost:
        if (r9 != 4) goto L_0x0ffc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:726:0x0f6c, code lost:
        r0 = (double) java.lang.Float.intBitsToFloat((int) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:727:0x0f72, code lost:
        r8 = ((X.C107004wd) r8).A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:728:0x0f78, code lost:
        if (r6 == 181) goto L_0x0ff4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:730:0x0f7c, code lost:
        if (r6 == 17545) goto L_0x0ff0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:731:0x0f7e, code lost:
        switch(r6) {
            case 21969: goto L_0x0fa0;
            case 21970: goto L_0x0fa8;
            case 21971: goto L_0x0fb0;
            case 21972: goto L_0x0fb8;
            case 21973: goto L_0x0fc0;
            case 21974: goto L_0x0fc8;
            case 21975: goto L_0x0fd0;
            case 21976: goto L_0x0fd8;
            case 21977: goto L_0x0fe0;
            case 21978: goto L_0x0fe8;
            default: goto L_0x0f81;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:732:0x0f81, code lost:
        switch(r6) {
            case 30323: goto L_0x0f88;
            case 30324: goto L_0x0f90;
            case 30325: goto L_0x0f98;
            default: goto L_0x0f84;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:733:0x0f84, code lost:
        r7.A01 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:734:0x0f88, code lost:
        A00(r8, r6).A0A = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:735:0x0f90, code lost:
        A00(r8, r6).A08 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:736:0x0f98, code lost:
        A00(r8, r6).A09 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:737:0x0fa0, code lost:
        A00(r8, r6).A06 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:738:0x0fa8, code lost:
        A00(r8, r6).A07 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:739:0x0fb0, code lost:
        A00(r8, r6).A04 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:740:0x0fb8, code lost:
        A00(r8, r6).A05 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:741:0x0fc0, code lost:
        A00(r8, r6).A02 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:742:0x0fc8, code lost:
        A00(r8, r6).A03 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:743:0x0fd0, code lost:
        A00(r8, r6).A0B = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:744:0x0fd8, code lost:
        A00(r8, r6).A0C = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:745:0x0fe0, code lost:
        A00(r8, r6).A00 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:746:0x0fe8, code lost:
        A00(r8, r6).A01 = (float) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:747:0x0ff0, code lost:
        r8.A0H = (long) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:748:0x0ff4, code lost:
        A00(r8, 181).A0U = (int) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:749:0x0ffc, code lost:
        r0 = java.lang.Double.longBitsToDouble(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:756:0x104b, code lost:
        r1 = r29.AFo();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:757:0x1055, code lost:
        if (r28.A0Z == false) goto L_0x1064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:758:0x1057, code lost:
        r28.A0K = r1;
        r30.A00 = r28.A0G;
        r28.A0Z = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:759:0x1063, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:761:0x1068, code lost:
        if (r28.A0b == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:762:0x106a, code lost:
        r6 = r28.A0K;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:763:0x1072, code lost:
        if (r6 == -1) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:764:0x1074, code lost:
        r30.A00 = r6;
        r28.A0K = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:765:0x107a, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:791:0x10cf, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0W(r1, "Invalid element type "));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:793:0x10da, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0W(r6, "Unexpected id: "));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:795:0x10e1, code lost:
        throw X.AnonymousClass496.A00("EBML lacing sample size out of range.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:797:0x10e8, code lost:
        throw X.AnonymousClass496.A00("No valid varint length mask found");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:799:0x10f3, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0W(r3, "Unexpected lacing value: "));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:801:0x1102, code lost:
        throw X.AnonymousClass496.A00(X.C12970iu.A0w(X.C12960it.A0k("String element size: "), r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:802:0x1103, code lost:
        r0 = X.C12960it.A0k("DocTypeReadVersion ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:803:0x1114, code lost:
        throw X.AnonymousClass496.A00(X.C12960it.A0d(" not supported", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:805:0x1123, code lost:
        throw X.AnonymousClass496.A00(X.C12970iu.A0w(X.C12960it.A0k("Invalid integer size: "), r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:806:0x1124, code lost:
        r1 = r28.A0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:807:0x112c, code lost:
        if (r5 >= r1.size()) goto L_0x113e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:808:0x112e, code lost:
        r1 = (X.AnonymousClass4WE) r1.valueAt(r5);
        r0 = r1.A0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:809:0x1136, code lost:
        if (r0 == null) goto L_0x113b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:810:0x1138, code lost:
        r0.A00(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:811:0x113b, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:812:0x113e, code lost:
        return -1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x05d3  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x05ec  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x05f8  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x060d  */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x0658  */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int AZn(X.AnonymousClass5Yf r29, X.AnonymousClass4IG r30) {
        /*
        // Method dump skipped, instructions count: 5286
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106814wK.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A0F = -9223372036854775807L;
        int i = 0;
        this.A05 = 0;
        C107014we r1 = (C107014we) this.A0e;
        r1.A01 = 0;
        r1.A05.clear();
        C94054bB r0 = r1.A04;
        r0.A01 = 0;
        r0.A00 = 0;
        C94054bB r02 = this.A0f;
        r02.A01 = 0;
        r02.A00 = 0;
        this.A08 = 0;
        this.A09 = 0;
        this.A0A = 0;
        this.A0V = false;
        this.A0Y = false;
        this.A0X = false;
        this.A0B = 0;
        this.A00 = 0;
        this.A0W = false;
        this.A0l.A0Q(0);
        while (true) {
            SparseArray sparseArray = this.A0d;
            if (i < sparseArray.size()) {
                AnonymousClass4W2 r12 = ((AnonymousClass4WE) sparseArray.valueAt(i)).A0d;
                if (r12 != null) {
                    r12.A05 = false;
                    r12.A02 = 0;
                }
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC116785Ww
    public final boolean Ae5(AnonymousClass5Yf r21) {
        AnonymousClass4VE r10 = new AnonymousClass4VE();
        long length = r21.getLength();
        long j = 1024;
        if (length != -1 && length <= 1024) {
            j = length;
        }
        int i = (int) j;
        C95304dT r2 = r10.A01;
        r21.AZ4(r2.A02, 0, 4);
        r10.A00 = 4;
        for (long A0I = r2.A0I(); A0I != 440786851; A0I = ((A0I << 8) & -256) | ((long) (r2.A02[0] & 255))) {
            int i2 = r10.A00 + 1;
            r10.A00 = i2;
            if (i2 == i) {
                return false;
            }
            C95304dT.A06(r21, r2, 1);
        }
        long A00 = r10.A00(r21);
        long j2 = (long) r10.A00;
        if (A00 == Long.MIN_VALUE) {
            return false;
        }
        if (length != -1 && j2 + A00 >= length) {
            return false;
        }
        while (true) {
            long j3 = (long) r10.A00;
            long j4 = j2 + A00;
            if (j3 < j4) {
                if (r10.A00(r21) == Long.MIN_VALUE) {
                    return false;
                }
                long A002 = r10.A00(r21);
                if (A002 < 0 || A002 > 2147483647L) {
                    return false;
                }
                if (A002 != 0) {
                    int i3 = (int) A002;
                    r21.A5r(i3);
                    r10.A00 += i3;
                }
            } else if (j3 == j4) {
                return true;
            } else {
                return false;
            }
        }
    }
}
