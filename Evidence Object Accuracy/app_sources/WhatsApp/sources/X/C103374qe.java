package X;

import android.content.Context;
import com.whatsapp.registration.RegisterPhone;

/* renamed from: X.4qe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103374qe implements AbstractC009204q {
    public final /* synthetic */ RegisterPhone A00;

    public C103374qe(RegisterPhone registerPhone) {
        this.A00 = registerPhone;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
