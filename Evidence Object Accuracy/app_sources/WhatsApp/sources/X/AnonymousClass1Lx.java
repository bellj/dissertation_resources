package X;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteTransactionListener;
import com.whatsapp.util.Log;
import java.io.Closeable;
import java.util.AbstractMap;

/* renamed from: X.1Lx  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Lx implements Closeable {
    public boolean A00 = false;
    public boolean A01;
    public final C16330op A02;

    public AnonymousClass1Lx(SQLiteTransactionListener sQLiteTransactionListener, C29621Ty r6, C16330op r7) {
        this.A02 = r7;
        ThreadLocal threadLocal = r6.A00;
        Object obj = threadLocal.get();
        AnonymousClass009.A05(obj);
        if (!((Boolean) obj).booleanValue()) {
            SQLiteDatabase sQLiteDatabase = r7.A00;
            AnonymousClass009.A0A("OuterTransactionManager/already-in-transaction", !sQLiteDatabase.inTransaction());
            sQLiteDatabase.beginTransactionWithListener(r6);
            threadLocal.set(Boolean.TRUE);
        } else {
            r7.A00.beginTransaction();
        }
        if (sQLiteTransactionListener != null) {
            Object obj2 = new Object();
            Object obj3 = r6.A01.get();
            AnonymousClass009.A05(obj3);
            ((AbstractMap) obj3).put(obj2, sQLiteTransactionListener);
            sQLiteTransactionListener.onBegin();
        }
    }

    public void A00() {
        this.A01 = true;
        this.A02.A00.setTransactionSuccessful();
    }

    public boolean A01() {
        return this.A02.A00.inTransaction() && !this.A00 && !this.A01;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.A00) {
            if (!this.A01) {
                Log.w("DatabaseTransaction/close/was not set successful");
            }
            this.A02.A00.endTransaction();
            this.A00 = true;
        }
    }
}
