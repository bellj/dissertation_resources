package X;

import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.google.android.material.snackbar.SnackbarContentLayout;
import com.whatsapp.R;

/* renamed from: X.1fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34271fr extends AbstractC15160mf {
    public static final int[] A02 = {R.attr.snackbarButtonStyle};
    public boolean A00;
    public final AccessibilityManager A01;

    public C34271fr(View view, ViewGroup viewGroup, AnonymousClass5R7 r5) {
        super(view, viewGroup, r5);
        this.A01 = (AccessibilityManager) viewGroup.getContext().getSystemService("accessibility");
    }

    public static C34271fr A00(View view, CharSequence charSequence, int i) {
        ViewGroup viewGroup;
        ViewGroup viewGroup2 = null;
        while (!(view instanceof CoordinatorLayout)) {
            if (!(view instanceof FrameLayout)) {
                if (view == null) {
                    viewGroup = viewGroup2;
                    break;
                }
            } else if (view.getId() == 16908290) {
                break;
            } else {
                viewGroup2 = (ViewGroup) view;
            }
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View) parent;
                if (view == null) {
                }
            }
            viewGroup = viewGroup2;
        }
        viewGroup = (ViewGroup) view;
        if (viewGroup != null) {
            LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
            TypedArray obtainStyledAttributes = viewGroup.getContext().obtainStyledAttributes(A02);
            int resourceId = obtainStyledAttributes.getResourceId(0, -1);
            obtainStyledAttributes.recycle();
            int i2 = R.layout.design_layout_snackbar_include;
            if (resourceId != -1) {
                i2 = R.layout.mtrl_layout_snackbar_include;
            }
            SnackbarContentLayout snackbarContentLayout = (SnackbarContentLayout) from.inflate(i2, viewGroup, false);
            C34271fr r1 = new C34271fr(snackbarContentLayout, viewGroup, snackbarContentLayout);
            ((SnackbarContentLayout) r1.A05.getChildAt(0)).A03.setText(charSequence);
            ((AbstractC15160mf) r1).A00 = i;
            return r1;
        }
        throw new IllegalArgumentException("No suitable parent found from the given view. Please provide a valid view.");
    }

    public void A06(int i) {
        ((SnackbarContentLayout) this.A05.getChildAt(0)).A02.setTextColor(i);
    }

    public void A07(CharSequence charSequence, View.OnClickListener onClickListener) {
        Button button = ((SnackbarContentLayout) this.A05.getChildAt(0)).A02;
        if (!TextUtils.isEmpty(charSequence)) {
            this.A00 = true;
            button.setVisibility(0);
            button.setText(charSequence);
            button.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 0, onClickListener));
            return;
        }
        button.setVisibility(8);
        button.setOnClickListener(null);
        this.A00 = false;
    }
}
