package X;

import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.1yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44301yg implements AbstractC44311yh {
    public final /* synthetic */ BusinessActivityReportViewModel A00;

    public C44301yg(BusinessActivityReportViewModel businessActivityReportViewModel) {
        this.A00 = businessActivityReportViewModel;
    }

    @Override // X.AbstractC44311yh
    public void APk() {
        Log.i("BusinessActivityReportViewModel/export-report/on-error");
        this.A00.A01.A0A(6);
    }

    @Override // X.AbstractC44311yh
    public void AUd(String str) {
        StringBuilder sb = new StringBuilder("BusinessActivityReportViewModel/export-report/on-ready-to-export :: ");
        sb.append(str);
        Log.i(sb.toString());
        this.A00.A00.A0A(str);
    }
}
