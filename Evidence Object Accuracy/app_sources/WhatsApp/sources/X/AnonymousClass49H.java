package X;

import java.io.OutputStream;
import java.io.RandomAccessFile;

/* renamed from: X.49H  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49H extends OutputStream {
    public final /* synthetic */ C64443Fo A00;
    public final /* synthetic */ RandomAccessFile A01;

    public AnonymousClass49H(C64443Fo r1, RandomAccessFile randomAccessFile) {
        this.A00 = r1;
        this.A01 = randomAccessFile;
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        this.A01.write(i);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        this.A01.write(bArr);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        this.A01.write(bArr, i, i2);
    }
}
