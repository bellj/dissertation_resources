package X;

/* renamed from: X.1sC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40711sC {
    public final AbstractC14640lm A00;
    public final AnonymousClass1IS A01;

    public C40711sC(AbstractC14640lm r1, AnonymousClass1IS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (obj instanceof C40711sC) {
            C40711sC r4 = (C40711sC) obj;
            if (this.A01.equals(r4.A01)) {
                AbstractC14640lm r1 = this.A00;
                AbstractC14640lm r0 = r4.A00;
                if (r1 == null) {
                    if (r0 == null) {
                        return true;
                    }
                } else if (r1.equals(r0)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = (this.A01.hashCode() + 31) * 31;
        AbstractC14640lm r0 = this.A00;
        return hashCode + (r0 == null ? 0 : r0.hashCode());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01.toString());
        sb.append(". [sender_jid=");
        sb.append(this.A00);
        sb.append("]");
        return sb.toString();
    }
}
