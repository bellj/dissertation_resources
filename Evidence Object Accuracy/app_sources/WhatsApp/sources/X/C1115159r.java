package X;

/* renamed from: X.59r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1115159r implements AnonymousClass1Kp {
    public final /* synthetic */ AnonymousClass01F A00;
    public final /* synthetic */ C42581vS A01;
    public final /* synthetic */ AbstractC14640lm A02;

    public C1115159r(AnonymousClass01F r1, C42581vS r2, AbstractC14640lm r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass1Kp
    public void A7o() {
        C42581vS.A00(this.A00, this.A01, this.A02, true);
    }

    @Override // X.AnonymousClass1Kp
    public void AHv(boolean z) {
        C42581vS.A00(this.A00, this.A01, this.A02, z);
    }
}
