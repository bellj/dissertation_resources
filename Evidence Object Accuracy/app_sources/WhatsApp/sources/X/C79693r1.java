package X;

/* renamed from: X.3r1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79693r1 extends AbstractC14150kw {
    public final /* synthetic */ C15100mZ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C79693r1(C14160kx r1, C15100mZ r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC14150kw
    public final void A01() {
        C15100mZ r1 = this.A00;
        C14170ky.A00();
        if (r1.A0I()) {
            r1.A09("Inactivity, disconnecting from device AnalyticsService");
            r1.A0H();
        }
    }
}
