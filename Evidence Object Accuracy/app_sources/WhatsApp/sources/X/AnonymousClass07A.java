package X;

import android.os.Bundle;
import androidx.lifecycle.SavedStateHandleController;

/* renamed from: X.07A  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass07A extends AnonymousClass05F {
    public final Bundle A00;
    public final AbstractC009904y A01;
    public final AnonymousClass058 A02;

    public abstract AnonymousClass015 A02(AnonymousClass07E v, Class cls, String str);

    public AnonymousClass07A(Bundle bundle, AbstractC001500q r3) {
        this.A02 = r3.AGO();
        this.A01 = r3.ADr();
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass05G
    public void A00(AnonymousClass015 r3) {
        SavedStateHandleController.A00(this.A01, r3, this.A02);
    }

    @Override // X.AnonymousClass05F
    public final AnonymousClass015 A01(Class cls, String str) {
        AnonymousClass058 r3 = this.A02;
        AbstractC009904y r2 = this.A01;
        SavedStateHandleController savedStateHandleController = new SavedStateHandleController(AnonymousClass07E.A00(r3.A00(str), this.A00), str);
        savedStateHandleController.A02(r2, r3);
        SavedStateHandleController.A01(r2, r3);
        AnonymousClass015 A02 = A02(savedStateHandleController.A01, cls, str);
        A02.A00(savedStateHandleController);
        return A02;
    }

    @Override // X.AnonymousClass05F, X.AbstractC009404s
    public final AnonymousClass015 A7r(Class cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return A01(cls, canonicalName);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
