package X;

import android.text.TextUtils;
import java.util.Collections;

/* renamed from: X.1Ux  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Ux {
    public static String A00(int i) {
        StringBuilder sb = new StringBuilder("(");
        sb.append(TextUtils.join(",", Collections.nCopies(i, "?")));
        sb.append(")");
        return sb.toString();
    }

    public static String A01(String str, String str2) {
        String[] split = str2.replaceAll(" ", "").split(",");
        for (int i = 0; i < split.length; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".");
            String str3 = split[i];
            sb.append(str3);
            sb.append(" AS ");
            sb.append(str3);
            split[i] = sb.toString();
        }
        return TextUtils.join(", ", split);
    }
}
