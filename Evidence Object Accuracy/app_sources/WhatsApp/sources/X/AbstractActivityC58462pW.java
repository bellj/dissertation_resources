package X;

import com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity;

/* renamed from: X.2pW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58462pW extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58462pW() {
        ActivityC13830kP.A1P(this, 25);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            BusinessDirectoryActivity businessDirectoryActivity = (BusinessDirectoryActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, businessDirectoryActivity);
            ActivityC13810kN.A10(A1M, businessDirectoryActivity);
            ((ActivityC13790kL) businessDirectoryActivity).A08 = ActivityC13790kL.A0S(r3, A1M, businessDirectoryActivity, ActivityC13790kL.A0Y(A1M, businessDirectoryActivity));
            businessDirectoryActivity.A0A = (C22660zR) A1M.AAk.get();
            businessDirectoryActivity.A02 = C12970iu.A0V(A1M);
            businessDirectoryActivity.A05 = (AnonymousClass1B0) A1M.A2K.get();
            businessDirectoryActivity.A09 = (C18360sK) A1M.AN0.get();
            businessDirectoryActivity.A06 = (AnonymousClass1B1) A1M.A2I.get();
            businessDirectoryActivity.A04 = C12990iw.A0W(A1M);
            businessDirectoryActivity.A0B = (C252618s) A1M.A2M.get();
            businessDirectoryActivity.A03 = (AnonymousClass1B2) A1M.A2G.get();
        }
    }
}
