package X;

/* renamed from: X.4U2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4U2 {
    public int A00;
    public C95474dn A01;
    public C95474dn A02;
    public C95474dn A03;
    public C95474dn A04;
    public C94914ck A05;
    public AnonymousClass4U2 A06 = null;
    public final int A07;
    public final int A08;
    public final AnonymousClass4YW A09;

    public AnonymousClass4U2(String str, String str2, String str3, AnonymousClass4YW r5) {
        this.A09 = r5;
        this.A08 = r5.A02(str);
        this.A07 = r5.A02(str2);
        if (str3 != null) {
            this.A00 = r5.A02(str3);
        }
    }
}
