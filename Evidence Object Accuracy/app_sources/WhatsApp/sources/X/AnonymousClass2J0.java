package X;

/* renamed from: X.2J0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J0 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J0(C48302Fl r1) {
        this.A00 = r1;
    }

    public AnonymousClass3DS A00(C36051jF r10) {
        AnonymousClass01J r1 = this.A00.A03;
        C15650ng r3 = (C15650ng) r1.A4m.get();
        C242114q r6 = (C242114q) r1.AJq.get();
        C15600nX r4 = (C15600nX) r1.A8x.get();
        return new AnonymousClass3DS((AnonymousClass1A9) r1.A4d.get(), (C19990v2) r1.A3M.get(), r3, r4, (AnonymousClass134) r1.AJg.get(), r6, r10, (C15860o1) r1.A3H.get());
    }
}
