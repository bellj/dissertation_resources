package X;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30771Yt extends AbstractC30781Yu {
    public static AbstractC30791Yv A04;
    public static AbstractC30791Yv A05;
    public static AbstractC30791Yv A06;
    public static final BigDecimal A07;
    public static final Parcelable.Creator CREATOR = new C30811Yx();
    public C30821Yy A00;
    public final C30821Yy A01;
    public final String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        BigDecimal bigDecimal = new BigDecimal(1);
        A07 = bigDecimal;
        BigDecimal bigDecimal2 = BigDecimal.ZERO;
        A06 = new C30771Yt("XXX", "XXX", "#", "#", bigDecimal2, bigDecimal2, -1, 10, 1, 0);
        A05 = new C30771Yt("INR", "₹", "R", "r", BigDecimal.valueOf(5000L), bigDecimal, 0, 100, 2, 0);
        A04 = new C30771Yt("BRL", "R$", "B", "b", BigDecimal.valueOf(1000L), bigDecimal, 0, 100, 2, 0);
    }

    public C30771Yt(Parcel parcel) {
        super(0, parcel);
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A00 = (C30821Yy) parcel.readParcelable(C30821Yy.class.getClassLoader());
        this.A01 = (C30821Yy) parcel.readParcelable(C30821Yy.class.getClassLoader());
    }

    public C30771Yt(String str, String str2, String str3, String str4, BigDecimal bigDecimal, BigDecimal bigDecimal2, int i, int i2, int i3, int i4) {
        super(str, str2, i, i2, i3, i4);
        int log10 = (int) Math.log10((double) i2);
        this.A00 = new C30821Yy(bigDecimal, log10);
        this.A01 = new C30821Yy(bigDecimal2, log10);
        this.A02 = str3;
        this.A03 = str4;
    }

    public C30771Yt(JSONObject jSONObject) {
        super(jSONObject);
        this.A02 = jSONObject.optString("currencyIconText");
        this.A03 = jSONObject.optString("requestCurrencyIconText");
        JSONObject optJSONObject = jSONObject.optJSONObject("maxValue");
        int i = super.A01;
        this.A00 = C30821Yy.A00(optJSONObject.optString("amount", ""), i);
        this.A01 = C30821Yy.A00(jSONObject.optJSONObject("minValue").optString("amount", ""), i);
    }

    @Override // X.AbstractC30791Yv
    public String AA8(AnonymousClass018 r7, C30821Yy r8) {
        String str = this.A04;
        BigDecimal bigDecimal = r8.A00;
        return C30831Yz.A01(r7, str, this.A05, bigDecimal, bigDecimal.scale(), false);
    }

    @Override // X.AbstractC30791Yv
    public String AA9(AnonymousClass018 r4, BigDecimal bigDecimal) {
        return C30831Yz.A02(r4, this.A04, this.A05, bigDecimal, false);
    }

    @Override // X.AbstractC30791Yv
    public String AAA(AnonymousClass018 r10, C30821Yy r11, int i) {
        String str;
        String str2;
        BigDecimal bigDecimal;
        int scale;
        boolean z = true;
        if (i == 1) {
            str = this.A04;
            str2 = this.A05;
            bigDecimal = r11.A00;
            scale = bigDecimal.scale();
        } else if (i == 2) {
            str = this.A04;
            str2 = this.A05;
            bigDecimal = r11.A00;
            scale = bigDecimal.scale();
            z = false;
        } else {
            String str3 = this.A04;
            BigDecimal bigDecimal2 = r11.A00;
            return C30831Yz.A01(r10, str3, this.A05, bigDecimal2, bigDecimal2.scale(), true);
        }
        C30711Yn A00 = C30831Yz.A00(str);
        StringBuilder sb = new StringBuilder();
        if (!z) {
            str2 = "";
        }
        sb.append(str2);
        AnonymousClass1Z0 A01 = A00.A01(r10, scale, false);
        String A02 = A01.A07.A02(bigDecimal);
        if (A01.A02.A02) {
            boolean z2 = false;
            if (bigDecimal.compareTo(BigDecimal.ZERO) < 0) {
                z2 = true;
            }
            A02 = A01.A01(A02, z2);
        }
        sb.append(A02);
        sb.append(" ");
        sb.append(str);
        return sb.toString();
    }

    @Override // X.AbstractC30791Yv
    public String AAB(AnonymousClass018 r5, BigDecimal bigDecimal, int i) {
        String str;
        C30711Yn A00;
        StringBuilder sb;
        String str2;
        if (i == 1) {
            str = this.A04;
            str2 = this.A05;
            A00 = C30831Yz.A00(str);
            sb = new StringBuilder();
        } else {
            str = this.A04;
            if (i != 2) {
                return C30831Yz.A02(r5, str, this.A05, bigDecimal, true);
            }
            A00 = C30831Yz.A00(str);
            sb = new StringBuilder();
            str2 = "";
        }
        sb.append(str2);
        sb.append(A00.A03(r5, bigDecimal, false));
        sb.append(" ");
        sb.append(str);
        return sb.toString();
    }

    @Override // X.AbstractC30791Yv
    public BigDecimal AAD(AnonymousClass018 r5, String str) {
        C30711Yn A00 = C30831Yz.A00(this.A04);
        try {
            AnonymousClass1Z0 A01 = A00.A01(r5, C30711Yn.A00(A00.A00), false);
            return new BigDecimal(A01.A07.A00(str.replace(A01.A01, "").replace(A01.A00, "").replace(AnonymousClass01V.A06, "").trim()).toString());
        } catch (Exception e) {
            Log.w("Currency parse threw: ", e);
            try {
                return new BigDecimal(str);
            } catch (Exception e2) {
                Log.w("Currency parse fallback threw: ", e2);
                return null;
            }
        }
    }

    @Override // X.AbstractC30791Yv
    public CharSequence ABy(Context context) {
        return ABz(context, 0);
    }

    @Override // X.AbstractC30791Yv
    public CharSequence ABz(Context context, int i) {
        String str;
        if (i == 1) {
            str = this.A03;
        } else {
            str = this.A02;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        Typeface A02 = AnonymousClass00X.A02(context);
        if (A02 != null) {
            spannableStringBuilder.setSpan(new AnonymousClass1Z3(A02), 0, this.A02.length(), 0);
        }
        return spannableStringBuilder;
    }

    @Override // X.AbstractC30791Yv
    public C30821Yy AEA() {
        return this.A00;
    }

    @Override // X.AbstractC30791Yv
    public C30821Yy AEV() {
        return this.A01;
    }

    @Override // X.AbstractC30791Yv
    public int AH1(AnonymousClass018 r8) {
        C30711Yn A00 = C30831Yz.A00(this.A04);
        AnonymousClass1Z0 A01 = A00.A01(r8, C30711Yn.A00(A00.A00), true);
        String A012 = A01.A07.A01(1.0d);
        if (A01.A02.A02) {
            A012 = A01.A01(A012, false);
        }
        String A02 = A00.A02(r8);
        int length = A012.length();
        int length2 = A02.length();
        if (length < length2 || !A012.substring(0, length2).equals(A02)) {
            return 2;
        }
        return 1;
    }

    @Override // X.AbstractC30791Yv
    public void AcK(C30821Yy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC30781Yu, X.AbstractC30791Yv
    public JSONObject Aew() {
        JSONObject Aew = super.Aew();
        try {
            Aew.put("currencyIconText", this.A02);
            Aew.put("requestCurrencyIconText", this.A03);
            Aew.put("maxValue", this.A00.A01());
            Aew.put("minValue", this.A01.A01());
            return Aew;
        } catch (JSONException e) {
            Log.e("PAY: PaymentCurrency toJsonObject threw: ", e);
            return Aew;
        }
    }

    @Override // X.AbstractC30781Yu, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C30771Yt)) {
            return false;
        }
        C30771Yt r4 = (C30771Yt) obj;
        if (!super.equals(r4) || !this.A02.equals(r4.A02) || !this.A03.equals(r4.A03) || !this.A01.equals(r4.A01) || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC30781Yu, java.lang.Object
    public int hashCode() {
        return super.hashCode() + (this.A02.hashCode() * 31) + (this.A03.hashCode() * 31) + (this.A01.hashCode() * 31) + (this.A00.hashCode() * 31);
    }

    @Override // X.AbstractC30781Yu, X.AbstractC30791Yv, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
    }
}
