package X;

/* renamed from: X.1MK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MK {
    public static boolean A00(String str) {
        return "payments:settings".equals(str) || "payments:transaction".equals(str) || "payments:account-details".equals(str) || "payments:request".equals(str);
    }
}
