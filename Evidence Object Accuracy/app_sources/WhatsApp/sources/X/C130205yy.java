package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.text.MessageFormat;
import java.util.List;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.5yy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130205yy {
    public final int A00 = R.string.transaction_payment_method_id;
    public final Context A01;
    public final C15550nR A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final AnonymousClass1IR A06;
    public final AbstractC1316063k A07;
    public final C123565nM A08;
    public final C127915vG A09;
    public final C129835yN A0A;
    public final AnonymousClass14X A0B;

    public C130205yy(Context context, C15550nR r3, C14830m7 r4, C16590pI r5, AnonymousClass018 r6, AnonymousClass1IR r7, AbstractC1316063k r8, C123565nM r9, C127915vG r10, C129835yN r11, AnonymousClass14X r12) {
        this.A03 = r4;
        this.A05 = r6;
        this.A01 = context;
        this.A04 = r5;
        this.A0B = r12;
        this.A02 = r3;
        this.A07 = r8;
        this.A06 = r7;
        this.A08 = r9;
        this.A09 = r10;
        this.A0A = r11;
    }

    public CharSequence A00(long j) {
        Context context = this.A01;
        Object[] A1b = C12970iu.A1b();
        AnonymousClass018 r8 = this.A05;
        C14830m7 r2 = this.A03;
        String A02 = AnonymousClass1MY.A02(r8, r2.A02(j));
        String A00 = AnonymousClass3JK.A00(r8, r2.A02(j));
        String A08 = r8.A08(178);
        Object[] A1a = C12980iv.A1a();
        C12990iw.A1P(A00, A02, A1a);
        return C12960it.A0X(context, MessageFormat.format(A08, A1a), A1b, 0, R.string.time_and_date);
    }

    public String A01(C121035h9 r8) {
        AbstractC128515wE r6 = r8.A00.A02;
        int i = r6.A00;
        if (i == 0) {
            Log.e("NoviTransactionCommonModelHelper/getDepositMethodInfo case BANK not valid");
            return null;
        } else if (i != 1) {
            return null;
        } else {
            C120945h0 r62 = (C120945h0) r6;
            Context context = this.A01;
            Object[] A1a = C12980iv.A1a();
            A1a[0] = C30881Ze.A08(r62.A00);
            return C12960it.A0X(context, r62.A03, A1a, 1, R.string.novi_payment_transaction_details_sender_card_method_debit_label);
        }
    }

    public void A02(AbstractC130195yx r5, List list, int i, boolean z) {
        A08(list);
        String A05 = r5.A05();
        C15370n3 r1 = r5.A00;
        String string = this.A0A.A00.getString(i);
        C123315mx r2 = new C123315mx();
        r2.A05 = r1;
        r2.A09 = string;
        r2.A08 = A05.toString();
        if (r1 != null && z) {
            r2.A04 = new IDxCListenerShape2S0200000_3_I1(this, 50, r2);
        }
        list.add(r2);
    }

    public void A03(CharSequence charSequence, CharSequence charSequence2, List list) {
        C123285mu r2 = new C123285mu(charSequence, this.A0B.A0D(this.A06, charSequence), charSequence2, this.A0A.A00.getString(R.string.novi_payment_transaction_details_button_title_cancel_transaction), null, false, false);
        r2.A00 = C117305Zk.A0A(this, MediaCodecVideoEncoder.MIN_ENCODER_WIDTH);
        list.add(r2);
    }

    public void A04(CharSequence charSequence, String str, List list, int i) {
        A08(list);
        CharSequence A00 = A00(this.A06.A06);
        Context context = this.A01;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("@");
        if (AnonymousClass1Z4.A00 == null) {
            try {
                AnonymousClass1Z4.A00 = AnonymousClass00X.A02(context);
            } catch (Resources.NotFoundException unused) {
                Log.e("PAY: PaymentsTypeface/loadTypefaceSync could not load font R.font.payment_icons_regular");
            }
        }
        Typeface typeface = AnonymousClass1Z4.A00;
        if (typeface != null) {
            spannableStringBuilder.setSpan(new AnonymousClass1Z3(typeface), 0, "@".length(), 0);
        } else {
            Log.e("PAY: PaymentsTypeface/applyFont Could not load payment_icons_regular typeface, call loadTypeface() before applying font.");
        }
        String string = context.getString(R.string.payment);
        C123325my r0 = new C123325my();
        r0.A01 = i;
        r0.A03 = spannableStringBuilder;
        r0.A04 = string;
        r0.A05 = charSequence;
        r0.A0B = str;
        r0.A07 = A00;
        list.add(r0);
    }

    public void A05(String str, List list) {
        C123235mp r0 = new C123235mp();
        r0.A00 = R.dimen.novi_payment_transaction_detail_view_transaction_margin_left;
        r0.A01 = R.dimen.payment_settings_default_margin;
        r0.A03 = false;
        list.add(r0);
        C122945mM r1 = new C122945mM(null, null, this.A01.getString(R.string.novi_payment_transaction_details_breakdown_view_transaction_title), 1, true);
        r1.A00 = new View.OnClickListener(str) { // from class: X.64A
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C130205yy r02 = C130205yy.this;
                String str2 = this.A01;
                C123565nM r3 = r02.A08;
                r3.A0h.Ab2(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: INVOKE  
                      (wrap: X.0lR : 0x000a: IGET  (r1v0 X.0lR A[REMOVE]) = (r3v0 'r3' X.5nM) X.5bP.A0h X.0lR)
                      (wrap: X.6Jh : 0x000e: CONSTRUCTOR  (r0v1 X.6Jh A[REMOVE]) = 
                      (wrap: android.content.Context : 0x0006: INVOKE  (r2v0 android.content.Context A[REMOVE]) = (r6v0 'view' android.view.View) type: VIRTUAL call: android.view.View.getContext():android.content.Context)
                      (r3v0 'r3' X.5nM)
                      (r4v0 'str2' java.lang.String)
                     call: X.6Jh.<init>(android.content.Context, X.5nM, java.lang.String):void type: CONSTRUCTOR)
                     type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.64A.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: CONSTRUCTOR  (r0v1 X.6Jh A[REMOVE]) = 
                      (wrap: android.content.Context : 0x0006: INVOKE  (r2v0 android.content.Context A[REMOVE]) = (r6v0 'view' android.view.View) type: VIRTUAL call: android.view.View.getContext():android.content.Context)
                      (r3v0 'r3' X.5nM)
                      (r4v0 'str2' java.lang.String)
                     call: X.6Jh.<init>(android.content.Context, X.5nM, java.lang.String):void type: CONSTRUCTOR in method: X.64A.onClick(android.view.View):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Jh, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    X.5yy r0 = X.C130205yy.this
                    java.lang.String r4 = r5.A01
                    X.5nM r3 = r0.A08
                    android.content.Context r2 = r6.getContext()
                    X.0lR r1 = r3.A0h
                    X.6Jh r0 = new X.6Jh
                    r0.<init>(r2, r3, r4)
                    r1.Ab2(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass64A.onClick(android.view.View):void");
            }
        };
        list.add(r1);
    }

    public final void A06(List list) {
        A07(list);
        list.add(new C123265ms(this.A01.getString(R.string.novi_payment_transaction_details_powered_by_label), R.dimen.payment_settings_default_margin, R.dimen.payment_settings_default_margin, 1, R.color.novi_payments_Powered_by_Novi_text_color));
    }

    public final void A07(List list) {
        C123235mp r0 = new C123235mp();
        r0.A00 = 0;
        r0.A01 = 0;
        r0.A03 = false;
        list.add(r0);
    }

    public final void A08(List list) {
        C123235mp r0 = new C123235mp();
        r0.A00 = R.dimen.payment_settings_default_margin;
        r0.A01 = R.dimen.payment_settings_default_margin;
        r0.A03 = false;
        list.add(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (X.C31001Zq.A09(r2.A0F) != false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09(java.util.List r4) {
        /*
            r3 = this;
            X.1IR r2 = r3.A06
            boolean r0 = r2.A0F()
            if (r0 != 0) goto L_0x0011
            java.lang.String r0 = r2.A0F
            boolean r0 = X.C31001Zq.A09(r0)
            r1 = 1
            if (r0 == 0) goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            boolean r0 = r2.A0F()
            if (r0 != 0) goto L_0x0044
            java.lang.String r0 = r2.A0F
            boolean r0 = X.C31001Zq.A09(r0)
            if (r0 != 0) goto L_0x0044
            java.lang.String r2 = r2.A0K
        L_0x0022:
            boolean r0 = X.C31001Zq.A09(r2)
            if (r0 == 0) goto L_0x0040
            if (r1 == 0) goto L_0x0041
            r1 = 2131892282(0x7f12183a, float:1.9419308E38)
        L_0x002d:
            X.5yN r0 = r3.A0A
            X.5mM r1 = r0.A00(r2, r1)
            X.64s r0 = new X.64s
            r0.<init>(r2)
            r1.A01 = r0
            r3.A08(r4)
            r4.add(r1)
        L_0x0040:
            return
        L_0x0041:
            int r1 = r3.A00
            goto L_0x002d
        L_0x0044:
            java.lang.String r2 = r2.A0F
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130205yy.A09(java.util.List):void");
    }

    public final void A0A(List list) {
        String str = this.A07.A04;
        if (TextUtils.isEmpty(str)) {
            str = this.A01.getString(R.string.novi_payment_transaction_details_view_disclosure_link);
            Log.e("NoviTransactionDetailViewModelHelper/insertViewDisclosure link is empty or null");
        }
        A08(list);
        C123125me r1 = new C123125me(this.A0A.A00.getString(R.string.novi_payment_transaction_details_view_disclosure_title));
        r1.A00 = new View.OnClickListener(str) { // from class: X.64B
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                C130205yy r0 = C130205yy.this;
                String str2 = this.A01;
                C123565nM r6 = r0.A08;
                Context context = view.getContext();
                C128365vz r12 = new AnonymousClass610("CONSUMER_DISCLOSURE_CLICK", "PAYMENT_HISTORY", "REVIEW_TRANSACTION", "LINK").A00;
                r12.A0L = str2;
                C118185bP.A00(r12, r6, ((C118185bP) r6).A0B);
                r6.A09.A06(r12);
                r6.A06.A06(AnonymousClass12P.A00(context), C117295Zj.A04(str2));
            }
        };
        list.add(r1);
    }

    public void A0B(List list, CharSequence charSequence) {
        Context context = this.A01;
        String string = context.getString(R.string.learn_more);
        Object[] A1a = C12980iv.A1a();
        A1a[0] = charSequence;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(AnonymousClass1Z4.A00(context, C12960it.A0X(context, string, A1a, 1, R.string.novi_payment_transaction_details_sender_debit_description_with_link)));
        int length = charSequence.length();
        int length2 = string.length();
        spannableStringBuilder.setSpan(new C117505a4(this), charSequence.length() + 1, length + length2 + 1, 0);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.payments_link_highlight)), charSequence.length() + 1, charSequence.length() + length2 + 1, 0);
        A08(list);
        list.add(new C123265ms(spannableStringBuilder, R.dimen.product_margin_16dp, R.dimen.product_margin_16dp, 8388611, R.color.payments_desc_font_color));
    }

    public final void A0C(List list, boolean z) {
        A07(list);
        list.add(new C123065mY(this.A0A.A00.getString(R.string.novi_transaction_details_support_title)));
        if (z) {
            list.add(AnonymousClass612.A00(C117305Zk.A0A(this, 178), this.A05.A09(R.string.novi_payment_transaction_details_report_transaction_row_label), R.drawable.ic_report));
            A08(list);
        }
        C123055mX r1 = new C123055mX();
        r1.A00 = C117305Zk.A0A(this, 179);
        list.add(r1);
    }
}
