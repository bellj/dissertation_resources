package X;

import android.widget.ImageView;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.jid.UserJid;

/* renamed from: X.4RH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4RH {
    public ImageView A00;
    public C28801Pb A01;
    public SelectionCheckView A02;
    public UserJid A03;
}
