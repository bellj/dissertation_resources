package X;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import com.facebook.redex.RunnableBRunnable0Shape1S0500000_I1;
import com.whatsapp.R;

/* renamed from: X.0sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18550sd {
    public static boolean A00(int i) {
        switch (i) {
            case 13313:
            case 13337:
            case 13566:
            case 13656:
            case 13688:
            case 13768:
            case 13774:
            case 13914:
            case 13981:
            case 14001:
            case 15833:
            case 16310:
                return true;
            default:
                return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b8, code lost:
        if (r1.endsWith("%") != false) goto L_0x00ba;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A01(X.C14260l7 r7, X.AnonymousClass28D r8) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18550sd.A01(X.0l7, X.28D):java.lang.Object");
    }

    public void A02(C14260l7 r16, AnonymousClass28D r17, AnonymousClass28D r18, Object obj) {
        AnonymousClass3MF r0;
        String str;
        String str2;
        boolean z;
        int i = r17.A01;
        if (A00(i)) {
            View view = (View) obj;
            if (i == 13337) {
                EditText editText = (EditText) view;
                String A0I = r17.A0I(35);
                if (A0I != null) {
                    AnonymousClass3MK r2 = (AnonymousClass3MK) AnonymousClass3JV.A04(r16, r17);
                    r2.A00 = editText;
                    String str3 = r2.A02;
                    if (str3 == null || !str3.equals(A0I)) {
                        r2.A02 = A0I;
                        r2.A00();
                        EditText editText2 = r2.A00;
                        if (!(editText2 == null || editText2.getEditableText() == null)) {
                            r2.afterTextChanged(r2.A00.getEditableText());
                        }
                    }
                    AnonymousClass3C4 r1 = (AnonymousClass3C4) AnonymousClass3JV.A04(r16, r18);
                    r1.A0M.A04.remove(r2);
                    r1.A0M.A04.add(r2);
                }
            } else if (i == 13313) {
                C89984Md r12 = (C89984Md) AnonymousClass3JV.A04(r16, r17);
                if (r12 != null) {
                    r12.A00 = Integer.valueOf(view.getImportantForAccessibility());
                    r12.A01 = view.isFocusable();
                    boolean z2 = false;
                    if (!r17.A0O(35, true)) {
                        view.setImportantForAccessibility(2);
                        z = false;
                    } else {
                        z = true;
                    }
                    String A0I2 = r17.A0I(36);
                    String A0I3 = r17.A0I(38);
                    if (!(A0I2 == null && A0I3 == null)) {
                        z2 = true;
                    }
                    if (z || z2) {
                        view.setImportantForAccessibility(1);
                        view.setFocusable(true);
                    }
                    AnonymousClass028.A0g(view, new C53442eS(r17));
                    C64953Ho.A02(r18, r17.A0M(45));
                }
            } else if (i == 14001) {
                AnonymousClass3IE.A00(view, r16, r17, r18);
            } else if (i == 13656) {
                RunnableBRunnable0Shape1S0500000_I1 runnableBRunnable0Shape1S0500000_I1 = new RunnableBRunnable0Shape1S0500000_I1(view, r18, r17, r16, r16.A01.get(R.id.bloks_surface_on_data_rendered_runnable), 0);
                view.setTag(R.id.render_lifecycle_extension_runnable, runnableBRunnable0Shape1S0500000_I1);
                view.post(runnableBRunnable0Shape1S0500000_I1);
            } else {
                if (i == 13688) {
                    C67833Tb r22 = (C67833Tb) AnonymousClass3JV.A04(r16, r17);
                    if (r22 == null) {
                        str = "ViewTransformsExtensionBinderUtils";
                        str2 = "Null controller while binding ViewTransformsExtension";
                    } else {
                        r22.A08 = view;
                        view.setAlpha(r22.A00);
                        view.setRotation(r22.A03);
                        view.setScaleX(r22.A04);
                        view.setScaleY(r22.A05);
                        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66423Nn(view, r22));
                        return;
                    }
                } else if (i == 13566) {
                    view.setTag(R.id.testing_id_view_tag_key, r17.A0I(35));
                    return;
                } else if (i == 16310) {
                    C05100Og r6 = (C05100Og) AnonymousClass3JV.A04(r16, r17);
                    C04460Lu.A00.removeCallbacks(r6.A04);
                    Object obj2 = r17.A02.get(41);
                    boolean A0O = r17.A0O(42, true);
                    if ((r6.A01 || A0O) && !C87834De.A00(obj2, r6.A00)) {
                        r6.A00();
                        AbstractC14200l1 A0G = r17.A0G(35);
                        if (A0G != null) {
                            C14210l2 r13 = new C14210l2();
                            r13.A04(r16, 0);
                            r13.A04(r6.A00, 1);
                            r13.A04(obj2, 2);
                            C28701Oq.A01(r16, r17, new C14220l3(r13.A00), A0G);
                        }
                        r6.A00 = obj2;
                    }
                    r6.A01 = true;
                    return;
                } else if (i == 15833) {
                    AnonymousClass0OT r11 = new AnonymousClass0OT(view);
                    AnonymousClass0Aw r9 = new AnonymousClass0Aw(view, r11, r16, r17, r18);
                    Context context = r16.A00;
                    GestureDetector gestureDetector = new GestureDetector(context, r9);
                    ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(context, r9);
                    int i2 = Build.VERSION.SDK_INT;
                    if (i2 >= 19) {
                        scaleGestureDetector.setQuickScaleEnabled(false);
                    }
                    if (i2 >= 23) {
                        scaleGestureDetector.setStylusScaleEnabled(false);
                    }
                    view.setOnTouchListener(new AnonymousClass0WU(gestureDetector, scaleGestureDetector, r11, r9));
                    return;
                } else if (i == 13774) {
                    AnonymousClass0NE r3 = (AnonymousClass0NE) AnonymousClass3JV.A04(r16, r17);
                    if (r3 != null && r3.A00 == null) {
                        ViewTreeObserver$OnGlobalLayoutListenerC06970We r23 = new ViewTreeObserver$OnGlobalLayoutListenerC06970We(view.getRootView(), r3.A01);
                        r23.A03.add(new C05370Ph(r16, r17, r18));
                        r3.A00 = r23;
                        return;
                    }
                    return;
                } else if (i == 13914) {
                    C04610Mj r14 = (C04610Mj) AnonymousClass3JV.A04(r16, r17);
                    if (!r14.A00) {
                        r14.A00 = true;
                        AbstractC14200l1 A0G2 = r17.A0G(41);
                        if (A0G2 != null) {
                            C14210l2 r02 = new C14210l2();
                            r02.A05(r18, 0);
                            r02.A05(r16, 1);
                            C28701Oq.A01(r16, r17, new C14220l3(r02.A00), A0G2);
                        }
                    }
                    AbstractC14200l1 A0G3 = r17.A0G(42);
                    if (A0G3 != null) {
                        C14210l2 r03 = new C14210l2();
                        r03.A05(r18, 0);
                        r03.A05(r16, 1);
                        C28701Oq.A01(r16, r17, new C14220l3(r03.A00), A0G3);
                        return;
                    }
                    return;
                } else if (i == 13981) {
                    view.setOnTouchListener(new AnonymousClass0WT(r16, r17, r18));
                    return;
                } else if (i != 13768) {
                    throw new IllegalArgumentException(String.format("No implementation bound to key: %s", Integer.valueOf(i)));
                } else if (view instanceof EditText) {
                    EditText editText3 = (EditText) view;
                    AbstractC14200l1 A0G4 = r17.A0G(35);
                    if (r18.A01 == 13336 && A0G4 != null) {
                        RunnableC10100e2 r92 = new RunnableC10100e2(editText3, r16, (AnonymousClass3C4) AnonymousClass3JV.A04(r16, r18), r18, A0G4);
                        Thread currentThread = Thread.currentThread();
                        Handler handler = C04480Lw.A00;
                        if (currentThread == handler.getLooper().getThread()) {
                            r92.run();
                        } else {
                            handler.post(r92);
                        }
                    }
                    AbstractC14200l1 A0G5 = r17.A0G(36);
                    if (A0G5 != null) {
                        AnonymousClass0Vr r24 = (AnonymousClass0Vr) AnonymousClass3JV.A04(r16, r17);
                        if (r24 == null) {
                            str = "TextInputFormatterExtensionUtils";
                            str2 = "Unexpected null ExpressionMask in TextInputFormatterExtension";
                        } else {
                            r24.A02 = r18;
                            r24.A00 = editText3;
                            r24.A03 = A0G5;
                            r24.A01 = r16;
                            AnonymousClass3C4 r15 = (AnonymousClass3C4) AnonymousClass3JV.A04(r16, r18);
                            if (r15 != null && (r0 = r15.A0M) != null) {
                                r0.A04.remove(r24);
                                r15.A0M.A04.add(r24);
                                return;
                            }
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    StringBuilder sb = new StringBuilder("Text Input Formatter extension attached to non-text-input component with style ID: ");
                    sb.append(r18.A01);
                    throw new IllegalStateException(sb.toString());
                }
                C28691Op.A00(str, str2);
            }
        }
    }

    public void A03(C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7, Object obj) {
        AnonymousClass3MF r0;
        int i = r6.A01;
        if (A00(i)) {
            View view = (View) obj;
            if (i == 13337) {
                if (r6.A0I(35) != null) {
                    AnonymousClass3MK r1 = (AnonymousClass3MK) AnonymousClass3JV.A04(r5, r6);
                    r1.A00 = null;
                    ((AnonymousClass3C4) AnonymousClass3JV.A04(r5, r7)).A0M.A04.remove(r1);
                }
            } else if (i == 13313) {
                C89984Md r12 = (C89984Md) AnonymousClass3JV.A04(r5, r6);
                if (r12 != null) {
                    AnonymousClass028.A0g(view, null);
                    view.setImportantForAccessibility(r12.A00.intValue());
                    view.setFocusable(r12.A01);
                    C64953Ho.A01(r7, r6.A0M(45));
                }
            } else if (i == 14001) {
                AnonymousClass4U5 r3 = (AnonymousClass4U5) AnonymousClass3JV.A04(r5, r6);
                if (r3 != null) {
                    AnonymousClass028.A0g(view, null);
                    view.setContentDescription(r3.A03);
                    view.setImportantForAccessibility(r3.A00.intValue());
                    AnonymousClass028.A0Z(view, r3.A01.intValue());
                    view.setFocusable(r3.A05);
                    view.setSelected(r3.A08);
                    view.setEnabled(r3.A04);
                    AnonymousClass028.A0l(view, r3.A06);
                    new AnonymousClass0DY().A02(view, Boolean.valueOf(r3.A07));
                    if (r3.A09) {
                        view.setOnClickListener(null);
                    }
                    if (r3.A0A) {
                        view.setOnClickListener(null);
                    }
                    C64953Ho.A01(r7, r6.A0M(56));
                    AnonymousClass028.A0b(view, -1);
                }
            } else if (i == 13656) {
                Runnable runnable = (Runnable) view.getTag(R.id.render_lifecycle_extension_runnable);
                if (runnable != null) {
                    view.removeCallbacks(runnable);
                }
            } else if (i == 13688) {
                C67833Tb r2 = (C67833Tb) AnonymousClass3JV.A04(r5, r6);
                if (r2 == null) {
                    C28691Op.A00("ViewTransformsExtensionBinderUtils", "Null controller while binding ViewTransformsExtension");
                    return;
                }
                r2.A08 = null;
                view.setAlpha(1.0f);
                view.setRotation(0.0f);
                view.setTranslationX(0.0f);
                view.setTranslationY(0.0f);
                view.setScaleX(1.0f);
                view.setScaleY(1.0f);
                if (!r2.A09) {
                    return;
                }
                if (Build.VERSION.SDK_INT >= 28) {
                    view.resetPivot();
                    return;
                }
                view.setPivotX((((float) view.getWidth()) * 50.0f) / 100.0f);
                view.setPivotY((((float) view.getHeight()) * 50.0f) / 100.0f);
            } else if (i == 13566) {
                view.setTag(R.id.testing_id_view_tag_key, null);
            } else if (i == 16310) {
                C04460Lu.A00.post(((C05100Og) AnonymousClass3JV.A04(r5, r6)).A04);
            } else {
                if (i != 15833) {
                    if (i == 13774) {
                        AnonymousClass0NE r22 = (AnonymousClass0NE) AnonymousClass3JV.A04(r5, r6);
                        if (r22 != null) {
                            ViewTreeObserver$OnGlobalLayoutListenerC06970We r13 = r22.A00;
                            if (r13 == null) {
                                r22.A01 = false;
                                return;
                            }
                            r22.A01 = r13.A00;
                            r13.A03.clear();
                            ViewTreeObserver viewTreeObserver = r13.A02.getViewTreeObserver();
                            if (viewTreeObserver != null) {
                                viewTreeObserver.removeOnGlobalLayoutListener(r13);
                            }
                            r22.A00 = null;
                            return;
                        }
                        return;
                    } else if (i == 13914) {
                        AbstractC14200l1 A0G = r6.A0G(43);
                        if (A0G != null) {
                            C14210l2 r14 = new C14210l2();
                            r14.A05(r7, 0);
                            C28701Oq.A01(r5, r6, new C14220l3(r14.A00), A0G);
                            return;
                        }
                        return;
                    } else if (i != 13981) {
                        if (i == 13768) {
                            AnonymousClass0Vr r15 = (AnonymousClass0Vr) AnonymousClass3JV.A04(r5, r6);
                            if (r15 != null) {
                                AnonymousClass3C4 r02 = (AnonymousClass3C4) AnonymousClass3JV.A04(r5, r7);
                                if (!(r02 == null || (r0 = r02.A0M) == null)) {
                                    r0.A04.remove(r15);
                                }
                                r15.A02 = null;
                                r15.A00 = null;
                                r15.A03 = null;
                                r15.A01 = null;
                                return;
                            }
                            return;
                        }
                        throw new IllegalArgumentException(String.format("No implementation bound to key: %s", Integer.valueOf(i)));
                    }
                }
                view.setOnTouchListener(null);
            }
        }
    }
}
