package X;

import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import java.util.List;

/* renamed from: X.2gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54382gd extends AnonymousClass02M {
    public int A00 = -1;
    public CharSequence A01 = "";
    public final AnonymousClass01d A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass19M A04;
    public final C16630pM A05;
    public final List A06;
    public final AnonymousClass1J7 A07;

    public C54382gd(AnonymousClass01d r2, AnonymousClass018 r3, AnonymousClass19M r4, C16630pM r5, List list, AnonymousClass1J7 r7) {
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
        this.A06 = list;
        this.A05 = r5;
        this.A07 = r7;
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r3) {
        C16700pc.A0E(r3, 0);
        if (r3 instanceof C55062hj) {
            C55062hj r32 = (C55062hj) r3;
            C469928m r1 = r32.A01;
            if (r1 != null) {
                r32.A03.removeTextChangedListener(r1);
            }
            AnonymousClass367 r12 = r32.A00;
            if (r12 != null) {
                r32.A03.removeTextChangedListener(r12);
            }
            r32.A01 = null;
            r32.A00 = null;
        }
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A06.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r14, int i) {
        C16700pc.A0E(r14, 0);
        int i2 = r14.A02;
        boolean z = false;
        if (i2 == 0) {
            C75223jV r142 = (C75223jV) r14;
            String str = ((AnonymousClass2Er) this.A06.get(i)).A01;
            if (i == this.A00) {
                z = true;
            }
            C72063dx r2 = new C72063dx(this, i);
            C16700pc.A0E(str, 0);
            AppCompatRadioButton appCompatRadioButton = r142.A00;
            appCompatRadioButton.setText(str);
            appCompatRadioButton.setChecked(z);
            C12960it.A0z(appCompatRadioButton, r2, 5);
        } else if (i2 == 1) {
            C55062hj r143 = (C55062hj) r14;
            String str2 = ((AnonymousClass2Er) this.A06.get(i)).A01;
            boolean A1V = C12960it.A1V(i, this.A00);
            CharSequence charSequence = this.A01;
            C72073dy r4 = new C72073dy(this, i);
            AnonymousClass5K6 r3 = new AnonymousClass5K6(this);
            C16700pc.A0E(str2, 0);
            C16700pc.A0E(charSequence, 2);
            AppCompatRadioButton appCompatRadioButton2 = r143.A02;
            appCompatRadioButton2.setText(str2);
            appCompatRadioButton2.setChecked(A1V);
            C12960it.A0z(appCompatRadioButton2, r4, 4);
            WaEditText waEditText = r143.A03;
            C469928m r0 = r143.A01;
            if (r0 != null) {
                waEditText.removeTextChangedListener(r0);
            }
            r143.A01 = new AnonymousClass47J(r3);
            AnonymousClass367 r02 = r143.A00;
            if (r02 != null) {
                waEditText.removeTextChangedListener(r02);
            }
            AnonymousClass19M r8 = r143.A07;
            r143.A00 = new AnonymousClass367(waEditText, r143.A04, r143.A05, r143.A06, r8, r143.A08, 30, 30, false);
            waEditText.setText(charSequence);
            waEditText.addTextChangedListener(r143.A00);
            waEditText.addTextChangedListener(r143.A01);
            if (charSequence.length() > 0) {
                waEditText.requestFocus();
                waEditText.setSelection(waEditText.length());
            }
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        if (i == 0) {
            return new C75223jV(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.block_reason_item));
        }
        if (i == 1) {
            return new C55062hj(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.block_reason_text_item), this.A02, this.A03, this.A04, this.A05);
        }
        throw C12960it.A0U("Unsupported view type");
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return (!"other".equalsIgnoreCase(((AnonymousClass2Er) this.A06.get(i)).A00) || this.A00 != i) ? 0 : 1;
    }
}
