package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75503jx extends AnonymousClass03U {
    public final View A00;
    public final TextView A01;

    public /* synthetic */ C75503jx(View view) {
        super(view);
        this.A00 = view.findViewById(R.id.content);
        this.A01 = C12960it.A0J(view, R.id.title_tv);
    }
}
