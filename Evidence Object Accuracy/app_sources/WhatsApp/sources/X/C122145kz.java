package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.5kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122145kz extends AbstractC118815cQ {
    public final View A00;
    public final WaTextView A01;
    public final C247116o A02;

    public C122145kz(View view, C247116o r3) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.payment_option);
        this.A00 = AnonymousClass028.A0D(view, R.id.payment_options_container);
        this.A02 = r3;
    }
}
