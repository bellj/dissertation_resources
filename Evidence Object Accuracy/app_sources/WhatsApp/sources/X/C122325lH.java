package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122325lH extends AbstractC118825cR {
    public TextView A00;

    public C122325lH(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.header_text);
    }
}
