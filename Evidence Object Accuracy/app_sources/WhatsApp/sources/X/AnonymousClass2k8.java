package X;

import android.content.Context;
import com.facebook.rendercore.text.RCTextView;

/* renamed from: X.2k8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2k8 extends AbstractC65073Ia implements AnonymousClass5SC {
    public static AnonymousClass5WW A01 = new AnonymousClass3SZ();
    public long A00;

    public AnonymousClass2k8(long j) {
        super(EnumC869849t.VIEW);
        this.A00 = j;
        A04(new C93304Zx(A01, this));
    }

    @Override // X.AnonymousClass5SC
    public Object A8B(Context context) {
        return new RCTextView(context);
    }
}
