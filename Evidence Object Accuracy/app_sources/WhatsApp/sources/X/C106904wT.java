package X;

/* renamed from: X.4wT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106904wT implements AnonymousClass5WY {
    public final long A00;
    public final C92684Xa A01;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return false;
    }

    public C106904wT(long j, long j2) {
        C94324bc r1;
        this.A00 = j;
        if (j2 == 0) {
            r1 = C94324bc.A02;
        } else {
            r1 = new C94324bc(0, j2);
        }
        this.A01 = new C92684Xa(r1, r1);
    }

    public static void A00(AbstractC14070ko r3, long j) {
        r3.AbR(new C106904wT(j, 0));
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        return this.A01;
    }
}
