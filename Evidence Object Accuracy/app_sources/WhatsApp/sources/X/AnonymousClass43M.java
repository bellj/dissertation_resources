package X;

/* renamed from: X.43M  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43M extends AbstractC16110oT {
    public Long A00;
    public Long A01;

    public AnonymousClass43M() {
        super(2866, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamLdpExampleEvent1 {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "rapporValue1", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "rapporValue2", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
