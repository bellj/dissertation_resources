package X;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78413os extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98944jV();
    public final int A00;
    public final PendingIntent A01;
    public final AnonymousClass5YA A02;
    public final C56452ku A03;
    public final AnonymousClass5YD A04;
    public final AnonymousClass5YE A05;

    public C78413os(PendingIntent pendingIntent, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, C56452ku r8, int i) {
        AnonymousClass5YE r1;
        AnonymousClass5YD r12;
        this.A00 = i;
        this.A03 = r8;
        AnonymousClass5YA r2 = null;
        if (iBinder == null) {
            r1 = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationListener");
            if (queryLocalInterface instanceof AnonymousClass5YE) {
                r1 = (AnonymousClass5YE) queryLocalInterface;
            } else {
                r1 = new C79743r8(iBinder);
            }
        }
        this.A05 = r1;
        this.A01 = pendingIntent;
        if (iBinder2 == null) {
            r12 = null;
        } else {
            IInterface queryLocalInterface2 = iBinder2.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
            if (queryLocalInterface2 instanceof AnonymousClass5YD) {
                r12 = (AnonymousClass5YD) queryLocalInterface2;
            } else {
                r12 = new C79733r7(iBinder2);
            }
        }
        this.A04 = r12;
        if (iBinder3 != null) {
            IInterface queryLocalInterface3 = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            if (queryLocalInterface3 instanceof AnonymousClass5YA) {
                r2 = (AnonymousClass5YA) queryLocalInterface3;
            } else {
                r2 = new C79723r6(iBinder3);
            }
        }
        this.A02 = r2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        IBinder asBinder;
        IBinder asBinder2;
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0B(parcel, this.A03, 2, i, false);
        AnonymousClass5YE r0 = this.A05;
        IBinder iBinder = null;
        if (r0 == null) {
            asBinder = null;
        } else {
            asBinder = r0.asBinder();
        }
        C95654e8.A04(asBinder, parcel, 3);
        C95654e8.A0B(parcel, this.A01, 4, i, false);
        AnonymousClass5YD r02 = this.A04;
        if (r02 == null) {
            asBinder2 = null;
        } else {
            asBinder2 = r02.asBinder();
        }
        C95654e8.A04(asBinder2, parcel, 5);
        AnonymousClass5YA r03 = this.A02;
        if (r03 != null) {
            iBinder = r03.asBinder();
        }
        C95654e8.A04(iBinder, parcel, 6);
        C95654e8.A06(parcel, A00);
    }
}
