package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;

/* renamed from: X.1nr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38301nr {
    public int A00;
    public C32141bg A01;
    public boolean A02;

    public C38301nr(C22700zV r4, UserJid userJid) {
        AnonymousClass1M2 A00 = r4.A00(userJid);
        if (A00 != null) {
            A02(A00.A00(), A00(A00), A00.A03);
            return;
        }
        A02(null, 1, 1);
    }

    public C38301nr(C32141bg r1, int i, int i2) {
        A02(r1, i, i2);
    }

    public static int A00(AnonymousClass1M2 r3) {
        if (r3 != null) {
            String str = r3.A07;
            if (str != null && str.startsWith("smb:")) {
                return 2;
            }
            if (r3.A01()) {
                return 3;
            }
        }
        return 1;
    }

    public int A01() {
        if (!A05(1, 1, 1, false)) {
            if (A05(2, 1, 1, false)) {
                return 1;
            }
            if (!A05(2, 1, 1, true)) {
                if (!A05(3, 1, 1, false)) {
                    if (!A05(3, 1, 1, true)) {
                        if (A05(3, 1, 2, false)) {
                            return 5;
                        }
                        if (A05(3, 1, 2, true)) {
                            return 6;
                        }
                        if (A05(3, 2, 1, false)) {
                            return 7;
                        }
                        if (A05(3, 2, 1, true)) {
                            return 8;
                        }
                        if (A05(3, 2, 2, false)) {
                            return 9;
                        }
                        if (A05(3, 2, 2, true)) {
                            return 10;
                        }
                        if (!A05(1, 0, 0, false)) {
                            if (A05(2, 0, 0, false)) {
                                return 1;
                            }
                            if (!A05(2, 0, 0, true)) {
                                if (!A05(3, 0, 0, false)) {
                                    if (A05(3, 0, 0, true)) {
                                    }
                                }
                            }
                        }
                    }
                    return 4;
                }
                return 3;
            }
            return 2;
        }
        return 0;
    }

    public final void A02(C32141bg r3, int i, int i2) {
        if (r3 == null) {
            r3 = new C32141bg();
        }
        this.A01 = r3;
        this.A00 = i;
        boolean z = false;
        if (i2 == 3) {
            z = true;
        }
        this.A02 = z;
    }

    public boolean A03() {
        int A01 = A01();
        return A01 == 5 || A01 == 6 || A01 == 9 || A01 == 10;
    }

    public boolean A04() {
        int A01 = A01();
        return A01 == 0 || A01 == 1 || A01 == 2 || A01 == 3 || A01 == 4;
    }

    public final boolean A05(int i, int i2, int i3, boolean z) {
        if (this.A00 == i) {
            C32141bg r1 = this.A01;
            if (r1.actualActors == i2 && r1.hostStorage == i3 && this.A02 == z) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C38301nr r5 = (C38301nr) obj;
            if (!(this.A00 == r5.A00 && this.A02 == r5.A02 && this.A01.equals(r5.A01))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, Integer.valueOf(this.A00), Boolean.valueOf(this.A02)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BusinessState{privacyMode=");
        sb.append(this.A01);
        sb.append(", client=");
        sb.append(this.A00);
        sb.append(", isVerified=");
        sb.append(this.A02);
        sb.append(", stateId=");
        sb.append(A01());
        sb.append('}');
        return sb.toString();
    }
}
