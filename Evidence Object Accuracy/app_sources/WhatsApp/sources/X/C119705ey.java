package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119705ey extends AnonymousClass1ZO {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(4);
    public int A00 = 1;
    public AnonymousClass1ZR A01;
    public AnonymousClass1ZR A02;
    public String A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;

    @Override // X.AnonymousClass1ZO
    public int A05() {
        return 1;
    }

    @Override // X.AnonymousClass1ZO
    public void A09(int i) {
    }

    @Override // X.AnonymousClass1ZO
    public void A0B(boolean z) {
    }

    @Override // X.AnonymousClass1ZO
    public boolean A0C() {
        return false;
    }

    public C119705ey() {
    }

    public /* synthetic */ C119705ey(Parcel parcel) {
        super(parcel);
        this.A02 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
        this.A03 = parcel.readString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x005d, code lost:
        if (r1 != false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x004b, code lost:
        if (r1 == false) goto L_0x004d;
     */
    @Override // X.AnonymousClass1ZP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass102 r6, X.AnonymousClass1V8 r7, int r8) {
        /*
            r5 = this;
            java.lang.Class<java.lang.String> r3 = java.lang.String.class
            java.lang.String r0 = "user"
            r4 = 0
            java.lang.String r0 = r7.A0I(r0, r4)
            com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.getNullable(r0)
            r5.A05 = r0
            X.2SM r2 = X.C117305Zk.A0J()
            java.lang.String r0 = "vpa"
            java.lang.String r1 = r7.A0I(r0, r4)
            java.lang.String r0 = "upiHandle"
            X.1ZR r0 = X.C117305Zk.A0I(r2, r3, r1, r0)
            r5.A02 = r0
            java.lang.String r0 = "vpa-id"
            java.lang.String r0 = r7.A0I(r0, r4)
            r5.A03 = r0
            X.2SM r2 = X.C117305Zk.A0J()
            java.lang.String r0 = "user-name"
            java.lang.String r1 = r7.A0I(r0, r4)
            java.lang.String r0 = "accountHolderName"
            X.1ZR r0 = X.C117305Zk.A0I(r2, r3, r1, r0)
            r5.A01 = r0
            java.lang.String r0 = "nodal"
            java.lang.String r0 = r7.A0I(r0, r4)
            r3 = 1
            java.lang.String r2 = "1"
            if (r0 == 0) goto L_0x004d
            boolean r1 = r0.equals(r2)
            r0 = 1
            if (r1 != 0) goto L_0x004e
        L_0x004d:
            r0 = 0
        L_0x004e:
            r5.A04 = r0
            java.lang.String r0 = "nodal-allowed"
            java.lang.String r0 = r7.A0I(r0, r4)
            if (r0 == 0) goto L_0x005f
            boolean r1 = r0.equals(r2)
            r0 = 0
            if (r1 == 0) goto L_0x0060
        L_0x005f:
            r0 = 1
        L_0x0060:
            r5.A05 = r0
            java.lang.String r0 = "notif-allowed"
            java.lang.String r0 = r7.A0I(r0, r4)
            if (r0 == 0) goto L_0x0071
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0071
            r3 = 0
        L_0x0071:
            r5.A06 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119705ey.A01(X.102, X.1V8, int):void");
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("IndiaUpiContactData toNetwork not supported");
    }

    @Override // X.AnonymousClass1ZO, X.AnonymousClass1ZP
    public String A03() {
        JSONObject A0a;
        try {
            String A03 = super.A03();
            if (A03 != null) {
                A0a = C13000ix.A05(A03);
            } else {
                A0a = C117295Zj.A0a();
            }
            A0a.put("v", this.A00);
            if (!AnonymousClass1ZS.A02(this.A02)) {
                C117315Zl.A0U(this.A02, "vpaHandle", A0a);
            }
            String str = this.A03;
            if (str != null) {
                A0a.put("vpaId", str);
            }
            if (!AnonymousClass1ZS.A02(this.A01)) {
                C117315Zl.A0U(this.A01, "legalName", A0a);
            }
            return A0a.toString();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiContactData toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AnonymousClass1ZO, X.AnonymousClass1ZP
    public void A04(String str) {
        super.A04(str);
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                int optInt = A05.optInt("v", 1);
                this.A00 = optInt;
                if (optInt == 1) {
                    this.A02 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("vpaHandle", null), "upiHandle");
                    this.A03 = A05.optString("vpaId", null);
                    this.A01 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("legalName", null), "accountHolderName");
                }
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiContactData fromDBString threw: ", e);
            }
        }
    }

    @Override // X.AnonymousClass1ZO
    public String A08() {
        Object obj;
        AnonymousClass1ZR r0 = this.A02;
        if (r0 == null) {
            obj = null;
        } else {
            obj = r0.A00;
        }
        return (String) obj;
    }

    @Override // X.AnonymousClass1ZO
    public void A0A(String str) {
        this.A02 = C117295Zj.A0E(str);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ ver: ");
        A0k.append(this.A00);
        A0k.append(" jid: ");
        A0k.append(super.A05);
        A0k.append(" vpaHandle: ");
        A0k.append(this.A02);
        A0k.append(" nodal: ");
        A0k.append(this.A04);
        A0k.append(" nodalAllowed: ");
        A0k.append(this.A05);
        A0k.append(" notifAllowed: ");
        A0k.append(this.A06);
        return C12960it.A0d(" ]", A0k);
    }

    @Override // X.AnonymousClass1ZO, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A03);
    }
}
