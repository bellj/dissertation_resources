package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A1 extends Enum {
    public static final /* synthetic */ AnonymousClass4A1[] A00;
    public static final AnonymousClass4A1 A01;
    public static final AnonymousClass4A1 A02;

    public static AnonymousClass4A1 valueOf(String str) {
        return (AnonymousClass4A1) Enum.valueOf(AnonymousClass4A1.class, str);
    }

    public static AnonymousClass4A1[] values() {
        return (AnonymousClass4A1[]) A00.clone();
    }

    static {
        AnonymousClass4A1 r3 = new AnonymousClass4A1("START", 0);
        A02 = r3;
        AnonymousClass4A1 r1 = new AnonymousClass4A1("CONTINUE", 1);
        A01 = r1;
        AnonymousClass4A1[] r0 = new AnonymousClass4A1[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public AnonymousClass4A1(String str, int i) {
    }
}
