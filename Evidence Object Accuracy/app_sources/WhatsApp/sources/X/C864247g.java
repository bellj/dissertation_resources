package X;

import android.graphics.Bitmap;

/* renamed from: X.47g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864247g extends AbstractC16350or {
    public Bitmap[] A00;

    public C864247g(Bitmap[] bitmapArr) {
        this.A00 = bitmapArr;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        int i = 0;
        while (true) {
            Bitmap[] bitmapArr = this.A00;
            if (i >= bitmapArr.length) {
                return null;
            }
            if (bitmapArr[i] != null) {
                bitmapArr[i].recycle();
                bitmapArr[i] = null;
            }
            i++;
        }
    }
}
