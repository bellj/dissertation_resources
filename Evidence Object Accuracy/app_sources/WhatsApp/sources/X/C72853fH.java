package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3fH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72853fH extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass2CF A00;

    public C72853fH(AnonymousClass2CF r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00.A0c = false;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.A0c = false;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A00.A0c = true;
    }
}
