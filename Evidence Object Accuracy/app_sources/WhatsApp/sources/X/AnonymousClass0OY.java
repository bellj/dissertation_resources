package X;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;

/* renamed from: X.0OY  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0OY {
    public final AnonymousClass00O A00 = new AnonymousClass00O();
    public final AbstractC12350hm A01 = new C07420Xx(10);
    public final ArrayList A02 = new ArrayList();
    public final HashSet A03 = new HashSet();

    public final void A00(Object obj, ArrayList arrayList, HashSet hashSet) {
        if (arrayList.contains(obj)) {
            return;
        }
        if (!hashSet.contains(obj)) {
            hashSet.add(obj);
            AbstractList abstractList = (AbstractList) this.A00.get(obj);
            if (abstractList != null) {
                int size = abstractList.size();
                for (int i = 0; i < size; i++) {
                    A00(abstractList.get(i), arrayList, hashSet);
                }
            }
            hashSet.remove(obj);
            arrayList.add(obj);
            return;
        }
        throw new RuntimeException("This graph contains cyclic dependencies");
    }
}
