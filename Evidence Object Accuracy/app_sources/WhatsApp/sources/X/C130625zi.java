package X;

/* renamed from: X.5zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130625zi {
    public String A00;
    public String A01;
    public String A02;
    public String A03;

    public C130625zi(AnonymousClass1V8 r3) {
        this.A03 = AnonymousClass619.A01(r3, "title").A00;
        this.A00 = AnonymousClass619.A01(r3, "description").A00;
        this.A01 = r3.A0F("primary_action").A0H("text");
        AnonymousClass1V8 A0E = r3.A0E("secondary_action");
        if (A0E != null) {
            this.A02 = A0E.A0H("text");
        }
    }

    public C130625zi(String str, String str2, String str3, String str4) {
        this.A03 = str;
        this.A00 = str2;
        this.A01 = str3;
        this.A02 = str4;
    }
}
