package X;

import android.content.Context;

/* renamed from: X.0uH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19540uH {
    public AnonymousClass113 A00;
    public final Context A01;
    public final AnonymousClass01N A02;

    public C19540uH(Context context, AnonymousClass01N r2) {
        this.A01 = context;
        this.A02 = r2;
    }

    public C16310on A00() {
        AnonymousClass113 r0;
        synchronized (this) {
            r0 = this.A00;
            if (r0 == null) {
                r0 = (AnonymousClass113) this.A02.get();
                this.A00 = r0;
            }
        }
        return r0.get();
    }
}
