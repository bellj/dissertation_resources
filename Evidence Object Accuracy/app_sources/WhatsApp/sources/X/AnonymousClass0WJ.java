package X;

import android.view.View;

/* renamed from: X.0WJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WJ implements View.OnClickListener {
    public final /* synthetic */ C08720bk A00;

    public AnonymousClass0WJ(C08720bk r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        C08720bk r1 = this.A00;
        AnonymousClass28D r4 = r1.A01;
        AbstractC14200l1 r3 = r1.A02;
        C14210l2 r2 = new C14210l2();
        r2.A05(r4, 0);
        C14260l7 r12 = r1.A00;
        r2.A05(r12, 1);
        C28701Oq.A01(r12, r4, r2.A03(), r3);
    }
}
