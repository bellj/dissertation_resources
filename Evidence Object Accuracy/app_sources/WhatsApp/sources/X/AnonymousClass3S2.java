package X;

/* renamed from: X.3S2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S2 implements AbstractC11900h3 {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ AnonymousClass28D A01;
    public final /* synthetic */ C57882nm A02;
    public final /* synthetic */ AnonymousClass51Y A03;

    public AnonymousClass3S2(C14260l7 r1, AnonymousClass28D r2, C57882nm r3, AnonymousClass51Y r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC11900h3
    public void AUm() {
        this.A03.A01 = true;
        AnonymousClass28D r3 = this.A01;
        AbstractC14200l1 A0G = r3.A0G(36);
        if (A0G != null) {
            C28701Oq.A01(this.A00, r3, C14210l2.A02(r3), A0G);
        }
    }
}
