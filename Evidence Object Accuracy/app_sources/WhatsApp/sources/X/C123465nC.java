package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5nC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123465nC extends AbstractC118095bG {
    public int A00;
    public C30861Zc A01;
    public C1316163l A02;
    public final C130075yl A03;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C123465nC(X.C16590pI r16, X.AnonymousClass018 r17, X.C17070qD r18, X.C129865yQ r19, X.AnonymousClass60Y r20, X.AnonymousClass61F r21, X.C130105yo r22, X.C129685y8 r23, X.C130075yl r24, X.C129675y7 r25) {
        /*
            r15 = this;
            r2 = r16
            android.content.Context r1 = r2.A00
            r0 = 2131889995(0x7f120f4b, float:1.941467E38)
            java.lang.String r11 = r1.getString(r0)
            r0 = 2131889991(0x7f120f47, float:1.9414661E38)
            java.lang.String r12 = r1.getString(r0)
            r0 = 2131889982(0x7f120f3e, float:1.9414643E38)
            java.lang.String r13 = r1.getString(r0)
            java.lang.String r14 = "WITHDRAW_MONEY"
            r5 = r19
            r4 = r18
            r3 = r17
            r1 = r15
            r6 = r20
            r10 = r25
            r9 = r23
            r8 = r22
            r7 = r21
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r24
            r15.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123465nC.<init>(X.0pI, X.018, X.0qD, X.5yQ, X.60Y, X.61F, X.5yo, X.5y8, X.5yl, X.5y7):void");
    }

    public static String A01(C130125yq r2, C129585xx r3, String str, String str2, Object[] objArr) {
        objArr[2] = new AnonymousClass61S("quote_id", str);
        objArr[3] = new AnonymousClass61S("nonce", str2);
        KeyPair A02 = r2.A02();
        AnonymousClass009.A05(A02);
        String A01 = r3.A01(A02);
        AnonymousClass009.A05(A01);
        return A01;
    }

    public static void A02(AnonymousClass603 r4, Object obj, String str, JSONObject jSONObject) {
        jSONObject.put(str, obj);
        AnonymousClass6F2 r1 = r4.A02;
        C30821Yy r0 = r1.A01;
        AbstractC30791Yv r2 = r1.A00;
        jSONObject.put("trading_amount", C130325zE.A00(r2, r0));
        jSONObject.put("trading_currency", ((AbstractC30781Yu) r2).A04);
        AnonymousClass6F2 r12 = r4.A03;
        C30821Yy r02 = r12.A01;
        AbstractC30791Yv r22 = r12.A00;
        jSONObject.put("local_amount", C130325zE.A00(r22, r02));
        jSONObject.put("local_currency", ((AbstractC30781Yu) r22).A04);
        jSONObject.put("quote_id", r4.A04);
    }

    @Override // X.AbstractC118095bG
    public AnonymousClass017 A05() {
        String A0n = C12990iw.A0n();
        C1309460p r2 = this.A04;
        AnonymousClass009.A05(r2);
        int i = this.A00;
        if (i == 1) {
            C130075yl r5 = this.A03;
            C1316163l r0 = this.A02;
            AnonymousClass009.A05(r0);
            String str = r0.A04;
            AnonymousClass603 r8 = r2.A02;
            AnonymousClass016 A0T = C12980iv.A0T();
            String str2 = AnonymousClass600.A03;
            C130155yt r3 = r5.A05;
            String A05 = r3.A05();
            long A00 = r5.A02.A00();
            String A0n2 = C12990iw.A0n();
            JSONObject A0a = C117295Zj.A0a();
            try {
                C117295Zj.A1L(str2, A05, A0a, A00);
                A02(r8, A0n2, "client_idempotency_key", A0a);
                if (!TextUtils.isEmpty(str)) {
                    A0a.put("location_id", str);
                }
                if (!TextUtils.isEmpty(null)) {
                    A0a.put("brand_id", (Object) null);
                }
            } catch (JSONException unused) {
                Log.e("PAY: IntentPayloadHelper/getCashWithdrawalIntentPayload/toJson can't construct json");
            }
            C130125yq r11 = r5.A06;
            C129585xx r6 = new C129585xx(r11, "CASH_WITHDRAWAL", A0a);
            AnonymousClass61S[] r22 = new AnonymousClass61S[5];
            AnonymousClass61S.A04("action", "novi-submit-cash-withdrawal", r22);
            AnonymousClass61S.A05("store_id", str, r22, 1);
            C130155yt.A03(C117305Zk.A09(A0T, r5, 19), r3, C117295Zj.A0F(AnonymousClass61S.A00("cash_withdrawal_signed_intent", A01(r11, r6, r8.A04, A0n, r22)), r22, 4), 14);
            return A0T;
        } else if (i != 2) {
            return null;
        } else {
            C130075yl r52 = this.A03;
            C30861Zc r02 = this.A01;
            AnonymousClass009.A05(r02);
            String str3 = r02.A0A;
            AnonymousClass603 r82 = r2.A02;
            AnonymousClass016 A0T2 = C12980iv.A0T();
            String str4 = AnonymousClass600.A03;
            C130155yt r32 = r52.A05;
            String A052 = r32.A05();
            long A002 = r52.A02.A00();
            String A0n3 = C12990iw.A0n();
            JSONObject A0a2 = C117295Zj.A0a();
            try {
                C117295Zj.A1L(str4, A052, A0a2, A002);
                A0a2.put("client_idempotency_key", A0n3);
                A02(r82, str3, "financial_instrument_id", A0a2);
            } catch (JSONException unused2) {
                Log.e("PAY: IntentPayloadHelper/getBankWithdrawalIntentPayload/toJson can't construct json");
            }
            C130125yq r112 = r52.A06;
            C129585xx r62 = new C129585xx(r112, "BANK_WITHDRAWAL", A0a2);
            AnonymousClass61S[] r23 = new AnonymousClass61S[5];
            AnonymousClass61S.A04("action", "novi-submit-bank-withdrawal", r23);
            AnonymousClass61S.A05("credential_id", str3, r23, 1);
            C130155yt.A03(C117305Zk.A09(A0T2, r52, 15), r32, C117295Zj.A0F(AnonymousClass61S.A00("bank_withdrawal_signed_intent", A01(r112, r62, r82.A04, A0n, r23)), r23, 4), 15);
            return A0T2;
        }
    }

    @Override // X.AbstractC118095bG
    public void A07(AbstractC001200n r3) {
        this.A03.A00.A06(-1);
        super.A07(r3);
    }

    public final void A0E(C130785zy r7, int i) {
        AbstractC121025h8 r4;
        C30861Zc r0;
        String str;
        String str2;
        C126765tP r02;
        C1309260n r03;
        C130615zh r3 = (C130615zh) r7.A02;
        if (r7.A06() && r3 != null && (r4 = r3.A03) != null) {
            this.A05 = null;
            AnonymousClass603 r5 = r4.A04;
            AnonymousClass009.A05(r5);
            AnonymousClass6F2 r1 = r5.A03;
            HashMap A11 = C12970iu.A11();
            A11.put("withdraw_amount_fiat", r1.A06(this.A0G));
            A11.put("withdraw_quote_id", r5.A04);
            A11.put("withdraw_transaction_id", ((AbstractC1316063k) r4).A05);
            if (i == 1) {
                C1316163l r04 = this.A02;
                if (r04 != null) {
                    str = r04.A04;
                    str2 = "withdraw_store_id";
                    A11.put(str2, str);
                }
                A11.put("withdraw_status", r4.A07);
                r02 = r3.A00;
                if (!(r02 == null || (r03 = r02.A00) == null)) {
                    A11.put("withdraw_instructions", TextUtils.join("\n\n", r03.A00));
                }
                this.A08.A0A(A11);
            }
            if (i == 2 && (r0 = this.A01) != null) {
                str = r0.A0A;
                str2 = "withdraw_fi_model_id";
                A11.put(str2, str);
            }
            A11.put("withdraw_status", r4.A07);
            r02 = r3.A00;
            if (r02 == null) {
                A11.put("withdraw_instructions", TextUtils.join("\n\n", r03.A00));
            }
            this.A08.A0A(A11);
        } else if (r7.A01 == null || r3 == null) {
            A0A(r7.A00);
        } else {
            this.A05 = r3.A03;
        }
    }
}
