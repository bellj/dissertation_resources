package X;

import dalvik.system.DexFile;
import java.io.File;
import java.lang.reflect.Constructor;
import java.util.zip.ZipFile;

/* renamed from: X.0Ys  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ys implements AbstractC11800gt {
    public final Constructor A00;

    public AnonymousClass0Ys(Class cls) {
        Constructor constructor = cls.getConstructor(File.class, ZipFile.class, DexFile.class);
        this.A00 = constructor;
        constructor.setAccessible(true);
    }

    @Override // X.AbstractC11800gt
    public Object ALe(DexFile dexFile, File file) {
        return this.A00.newInstance(file, new ZipFile(file), dexFile);
    }
}
