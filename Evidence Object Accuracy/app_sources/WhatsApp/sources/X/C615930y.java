package X;

/* renamed from: X.30y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615930y extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;

    public C615930y() {
        super(3112, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(14, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(7, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(9, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamOfflineResume {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isOfflineCompleteMissed", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "lastStanzaT", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mailboxAge", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offlineMessageCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offlineNotificationCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offlinePreviewT", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offlineReceiptCount", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
