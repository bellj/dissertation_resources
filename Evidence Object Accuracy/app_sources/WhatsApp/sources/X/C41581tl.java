package X;

import android.view.View;

/* renamed from: X.1tl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41581tl {
    public final View A00;
    public final AbstractC15340mz A01;
    public final C41561tj A02;
    public final AbstractC41521tf A03;
    public final Object A04;
    public final boolean A05;
    public final /* synthetic */ C41491tc A06;

    public C41581tl(View view, AbstractC15340mz r2, C41561tj r3, AbstractC41521tf r4, C41491tc r5, Object obj, boolean z) {
        this.A06 = r5;
        this.A01 = r2;
        this.A00 = view;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = obj;
        this.A05 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A01.A0z.equals(((C41581tl) obj).A01.A0z);
    }

    public int hashCode() {
        return this.A01.A0z.hashCode();
    }
}
