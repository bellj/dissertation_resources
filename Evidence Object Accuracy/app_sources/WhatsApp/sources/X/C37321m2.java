package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1m2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37321m2 extends AbstractC35911iz {
    public final C16210od A00;
    public final AnonymousClass13A A01;
    public final AnonymousClass13K A02;
    public final AnonymousClass13D A03;
    public final AnonymousClass13J A04;
    public final AnonymousClass13I A05;
    public final AnonymousClass13G A06;
    public final C15490nL A07;
    public final String A08 = "com.facebook.stella";

    public C37321m2(C16210od r2, AnonymousClass13A r3, AnonymousClass13K r4, AnonymousClass13D r5, AnonymousClass13J r6, AnonymousClass13I r7, AnonymousClass13G r8, C15490nL r9) {
        this.A01 = r3;
        this.A03 = r5;
        this.A07 = r9;
        this.A00 = r2;
        this.A06 = r8;
        this.A05 = r7;
        this.A04 = r6;
        this.A02 = r4;
    }

    public final void A01(AnonymousClass1m3 r5) {
        if (r5 != null) {
            try {
                AnonymousClass13A r3 = this.A01;
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("action", r5.A00);
                jSONObject.putOpt("payload", r5.A01);
                r3.A00(jSONObject.toString(), this.A08, true);
            } catch (JSONException unused) {
                Log.e("StellaEventHandler/failed to create event");
            }
        }
    }
}
