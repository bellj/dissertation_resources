package X;

/* renamed from: X.5Nb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114785Nb extends AnonymousClass5NV {
    public int A00 = -1;

    public C114785Nb() {
    }

    public C114785Nb(C94954co r2) {
        super(r2, true);
    }

    public C114785Nb(AnonymousClass1TN[] r2) {
        super(r2, true);
    }

    @Override // X.AnonymousClass5NV, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return super.A00 ? this : super.A06();
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int i = this.A00;
        if (i < 0) {
            AnonymousClass1TN[] r2 = this.A01;
            int length = r2.length;
            i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                i = C72453ed.A0R(r2, i2, i);
            }
            this.A00 = i;
        }
        return AnonymousClass1TQ.A00(i) + 1 + i;
    }
}
