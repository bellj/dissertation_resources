package X;

import java.util.Arrays;

/* renamed from: X.0ST  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ST {
    public final Object A00;
    public final Throwable A01;

    public AnonymousClass0ST(Object obj) {
        this.A00 = obj;
        this.A01 = null;
    }

    public AnonymousClass0ST(Throwable th) {
        this.A01 = th;
        this.A00 = null;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass0ST) {
                AnonymousClass0ST r5 = (AnonymousClass0ST) obj;
                Object obj2 = this.A00;
                if (obj2 == null || !obj2.equals(r5.A00)) {
                    Throwable th = this.A01;
                    if (!(th == null || r5.A01 == null)) {
                        return th.toString().equals(th.toString());
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01});
    }
}
