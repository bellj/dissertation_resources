package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0p9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16510p9 {
    public final AbstractC15710nm A00;
    public final C19990v2 A01;
    public final C18460sU A02;
    public final C20850wQ A03;
    public final C16490p7 A04;
    public final C21390xL A05;
    public final Map A06 = new HashMap();
    public final Map A07 = new HashMap();

    public C16510p9(AbstractC15710nm r2, C19990v2 r3, C18460sU r4, C20850wQ r5, C16490p7 r6, C21390xL r7) {
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
        this.A05 = r7;
        this.A04 = r6;
        this.A03 = r5;
    }

    public int A00(ContentValues contentValues, AbstractC14640lm r11) {
        C16310on A02 = this.A04.A02();
        try {
            contentValues.put("hidden", (Integer) 0);
            int A00 = A02.A03.A00("chat", contentValues, "jid_row_id = ?", new String[]{String.valueOf(this.A02.A01(r11))});
            A02.close();
            return A00;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public long A01(ContentValues contentValues) {
        C16310on A02 = this.A04.A02();
        try {
            contentValues.put("hidden", (Integer) 0);
            long A022 = A02.A03.A02(contentValues, "chat");
            A02.close();
            return A022;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public long A02(AbstractC14640lm r7) {
        synchronized (this) {
            Map map = this.A06;
            Long l = (Long) map.get(r7);
            if (l != null) {
                return l.longValue();
            }
            long A03 = A03(r7);
            if (A03 == -1) {
                return A03;
            }
            synchronized (this) {
                Long valueOf = Long.valueOf(A03);
                map.put(r7, valueOf);
                this.A07.put(valueOf, r7);
            }
            return A03;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r1 > 0) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long A03(X.AbstractC14640lm r12) {
        /*
            r11 = this;
            X.0v2 r0 = r11.A01
            X.1PE r0 = r0.A06(r12)
            r9 = 0
            java.lang.String r4 = "; rowId="
            if (r0 == 0) goto L_0x0013
            long r1 = r0.A0V
            int r0 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x0013
        L_0x0012:
            return r1
        L_0x0013:
            X.0sU r0 = r11.A02
            long r0 = r0.A01(r12)
            r7 = -1
            int r2 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            if (r2 >= 0) goto L_0x0031
            java.lang.String r3 = "ChatStore/getRowIdForChat/invalid jidRowId="
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.whatsapp.util.Log.e(r0)
            return r7
        L_0x0031:
            X.0p7 r2 = r11.A04
            X.0on r5 = r2.get()
            X.0op r6 = r5.A03     // Catch: all -> 0x0087
            java.lang.String r3 = "SELECT _id FROM chat WHERE jid_row_id = ?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch: all -> 0x0087
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch: all -> 0x0087
            r1 = 0
            r2[r1] = r0     // Catch: all -> 0x0087
            android.database.Cursor r3 = r6.A09(r3, r2)     // Catch: all -> 0x0087
            boolean r0 = r3.moveToNext()     // Catch: all -> 0x0080
            if (r0 == 0) goto L_0x0054
            long r1 = r3.getLong(r1)     // Catch: all -> 0x0080
            goto L_0x0056
        L_0x0054:
            r1 = -1
        L_0x0056:
            r3.close()     // Catch: all -> 0x0087
            r5.close()
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0012
            long r1 = r11.A04(r12)
            int r0 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r0 > 0) goto L_0x0012
            java.lang.String r3 = "ChatStore/getRowIdForChat/error inserting a hidden chat; jid="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r3)
            r0.append(r12)
            r0.append(r4)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            return r1
        L_0x0080:
            r0 = move-exception
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch: all -> 0x0086
        L_0x0086:
            throw r0     // Catch: all -> 0x0087
        L_0x0087:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x008b
        L_0x008b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16510p9.A03(X.0lm):long");
    }

    public final long A04(AbstractC14640lm r9) {
        long A01 = this.A02.A01(r9);
        if (A01 == -1) {
            StringBuilder sb = new StringBuilder("ChatStore/insertHiddenChat/jid row id not found; jid=");
            sb.append(r9);
            Log.e(sb.toString());
            return -1;
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("jid_row_id", Long.valueOf(A01));
        contentValues.put("hidden", (Integer) 1);
        try {
            C16310on A02 = this.A04.A02();
            long A03 = A02.A03.A03(contentValues, "chat");
            A02.close();
            return A03;
        } catch (SQLiteConstraintException e) {
            StringBuilder sb2 = new StringBuilder("ChatStore/insertHiddenChat/row already exists but can't be read; jid=");
            sb2.append(r9);
            Log.e(sb2.toString(), e);
            return -1;
        }
    }

    public AbstractC14640lm A05(long j) {
        AbstractC14640lm r1 = null;
        if (j <= 0) {
            return null;
        }
        synchronized (this) {
            Map map = this.A07;
            Long valueOf = Long.valueOf(j);
            if (map.containsKey(valueOf)) {
                return (AbstractC14640lm) map.get(valueOf);
            }
            C16310on A01 = this.A04.get();
            try {
                Cursor A09 = A01.A03.A09("SELECT jid_row_id FROM chat WHERE _id = ?", new String[]{Long.toString(j)});
                if (A09.moveToLast()) {
                    Jid A03 = this.A02.A03(A09.getLong(0));
                    if (A03 instanceof AbstractC14640lm) {
                        r1 = (AbstractC14640lm) A03;
                        if (r1 != null) {
                            synchronized (this) {
                                map.put(valueOf, r1);
                                this.A06.put(r1, valueOf);
                            }
                        }
                    } else {
                        r1 = null;
                    }
                }
                A09.close();
                A01.close();
                return r1;
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public AbstractC14640lm A06(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("chat_row_id");
        if (columnIndex >= 0) {
            return A05(cursor.getLong(columnIndex));
        }
        int columnIndex2 = cursor.getColumnIndex("key_remote_jid");
        if (columnIndex2 >= 0) {
            return AbstractC14640lm.A01(cursor.getString(columnIndex2));
        }
        return null;
    }

    public Map A07() {
        long j;
        C28181Ma r55 = new C28181Ma("ChatsStore/getChats");
        HashMap hashMap = new HashMap();
        try {
            C16310on A01 = this.A04.get();
            Cursor A09 = A01.A03.A09(AnonymousClass1Uz.A00, null);
            if (A09 != null) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("raw_string_jid");
                int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("display_message_row_id");
                int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("display_message_sort_id");
                int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("last_read_message_row_id");
                int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("last_read_message_sort_id");
                int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("last_read_receipt_sent_message_row_id");
                int columnIndexOrThrow8 = A09.getColumnIndexOrThrow("last_read_receipt_sent_message_sort_id");
                int columnIndexOrThrow9 = A09.getColumnIndexOrThrow("archived");
                int columnIndexOrThrow10 = A09.getColumnIndexOrThrow("sort_timestamp");
                int columnIndexOrThrow11 = A09.getColumnIndexOrThrow("mod_tag");
                int columnIndexOrThrow12 = A09.getColumnIndexOrThrow("spam_detection");
                int columnIndexOrThrow13 = A09.getColumnIndexOrThrow("plaintext_disabled");
                int columnIndexOrThrow14 = A09.getColumnIndexOrThrow("vcard_ui_dismissed");
                int columnIndexOrThrow15 = A09.getColumnIndexOrThrow("change_number_notified_message_row_id");
                int columnIndexOrThrow16 = A09.getColumnIndexOrThrow("subject");
                int columnIndexOrThrow17 = A09.getColumnIndexOrThrow("last_message_row_id");
                int columnIndexOrThrow18 = A09.getColumnIndexOrThrow("last_message_sort_id");
                int columnIndexOrThrow19 = A09.getColumnIndexOrThrow("last_important_message_row_id");
                int columnIndexOrThrow20 = A09.getColumnIndexOrThrow("unseen_earliest_message_received_time");
                int columnIndexOrThrow21 = A09.getColumnIndexOrThrow("unseen_message_count");
                int columnIndexOrThrow22 = A09.getColumnIndexOrThrow("unseen_missed_calls_count");
                int columnIndexOrThrow23 = A09.getColumnIndexOrThrow("unseen_row_count");
                int columnIndexOrThrow24 = A09.getColumnIndexOrThrow("unseen_message_reaction_count");
                int columnIndexOrThrow25 = A09.getColumnIndexOrThrow("last_message_reaction_row_id");
                int columnIndexOrThrow26 = A09.getColumnIndexOrThrow("last_seen_message_reaction_row_id");
                int columnIndexOrThrow27 = A09.getColumnIndexOrThrow("deleted_message_row_id");
                int columnIndexOrThrow28 = A09.getColumnIndexOrThrow("deleted_starred_message_row_id");
                int columnIndexOrThrow29 = A09.getColumnIndexOrThrow("deleted_categories_message_row_id");
                int columnIndexOrThrow30 = A09.getColumnIndexOrThrow("deleted_categories_starred_message_row_id");
                int columnIndexOrThrow31 = A09.getColumnIndexOrThrow("deleted_message_categories");
                int columnIndexOrThrow32 = A09.getColumnIndexOrThrow("show_group_description");
                int columnIndexOrThrow33 = A09.getColumnIndexOrThrow("ephemeral_expiration");
                int columnIndexOrThrow34 = A09.getColumnIndexOrThrow("ephemeral_setting_timestamp");
                int columnIndexOrThrow35 = A09.getColumnIndexOrThrow("ephemeral_disappearing_messages_initiator");
                int columnIndexOrThrow36 = A09.getColumnIndexOrThrow("unseen_important_message_count");
                int columnIndexOrThrow37 = A09.getColumnIndexOrThrow("group_type");
                int columnIndexOrThrow38 = A09.getColumnIndexOrThrow("growth_lock_level");
                int columnIndexOrThrow39 = A09.getColumnIndexOrThrow("growth_lock_expiration_ts");
                int columnIndexOrThrow40 = A09.getColumnIndexOrThrow("has_new_community_admin_dialog_been_acknowledged");
                int columnIndexOrThrow41 = A09.getColumnIndexOrThrow("history_sync_progress");
                int columnIndexOrThrow42 = A09.getColumnIndexOrThrow("hidden");
                while (A09.moveToNext()) {
                    AbstractC14640lm A012 = AbstractC14640lm.A01(A09.getString(columnIndexOrThrow2));
                    if (A012 == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("msgstore-manager/initialize/chats/could not parse raw chat jid: ");
                        sb.append(A09.getString(columnIndexOrThrow2));
                        Log.w(sb.toString());
                    } else if (!C15380n4.A0N(A012) && A09.getInt(columnIndexOrThrow42) != 1) {
                        AnonymousClass1PE r4 = new AnonymousClass1PE(A012);
                        if (A0G()) {
                            j = A09.getLong(columnIndexOrThrow);
                        } else {
                            j = -1;
                        }
                        r4.A0V = j;
                        r4.A0T = A09.getLong(columnIndexOrThrow3);
                        r4.A0U = A09.getLong(columnIndexOrThrow4);
                        r4.A0O = A09.getLong(columnIndexOrThrow5);
                        r4.A0P = A09.getLong(columnIndexOrThrow6);
                        r4.A0Q = A09.getLong(columnIndexOrThrow7);
                        r4.A0R = A09.getLong(columnIndexOrThrow8);
                        boolean z = true;
                        if (A09.getInt(columnIndexOrThrow9) != 1) {
                            z = false;
                        }
                        r4.A0f = z;
                        r4.A0W = A09.getLong(columnIndexOrThrow10);
                        r4.A0A = A09.getInt(columnIndexOrThrow11);
                        r4.A03 = A09.getInt(columnIndexOrThrow12);
                        r4.A00 = A09.getInt(columnIndexOrThrow13);
                        r4.A09 = A09.getInt(columnIndexOrThrow14);
                        r4.A0B = A09.getLong(columnIndexOrThrow15);
                        r4.A0e = A09.getString(columnIndexOrThrow16);
                        r4.A0M = A09.getLong(columnIndexOrThrow17);
                        r4.A0N = A09.getLong(columnIndexOrThrow18);
                        long j2 = A09.getLong(columnIndexOrThrow19);
                        r4.A0K = j2;
                        if (j2 == 0) {
                            r4.A0K = 1;
                        }
                        r4.A0X = A09.getLong(columnIndexOrThrow20);
                        r4.A06 = A09.getInt(columnIndexOrThrow21);
                        r4.A07 = A09.getInt(columnIndexOrThrow22);
                        r4.A08 = A09.getInt(columnIndexOrThrow23);
                        r4.A05 = A09.getInt(columnIndexOrThrow24);
                        r4.A0L = (long) A09.getInt(columnIndexOrThrow25);
                        r4.A0S = (long) A09.getInt(columnIndexOrThrow26);
                        long j3 = A09.getLong(columnIndexOrThrow27);
                        r4.A0E = j3;
                        if (j3 == 0) {
                            r4.A0E = Long.MIN_VALUE;
                        }
                        long j4 = A09.getLong(columnIndexOrThrow28);
                        r4.A0F = j4;
                        if (j4 == 0) {
                            r4.A0F = Long.MIN_VALUE;
                        }
                        r4.A0d = A09.getString(columnIndexOrThrow31);
                        long j5 = A09.getLong(columnIndexOrThrow29);
                        r4.A0C = j5;
                        if (j5 == 0) {
                            r4.A0C = Long.MIN_VALUE;
                        }
                        long j6 = A09.getLong(columnIndexOrThrow30);
                        r4.A0D = j6;
                        if (j6 == 0) {
                            r4.A0D = Long.MIN_VALUE;
                        }
                        boolean z2 = true;
                        if (A09.getInt(columnIndexOrThrow32) != 1) {
                            z2 = false;
                        }
                        r4.A0h = z2;
                        r4.A02 = A09.getInt(columnIndexOrThrow41);
                        r4.A0Y = new AnonymousClass1PG(A09.getInt(columnIndexOrThrow33), A09.getLong(columnIndexOrThrow34), A09.getInt(columnIndexOrThrow35));
                        r4.A04 = A09.getInt(columnIndexOrThrow36);
                        boolean z3 = true;
                        if (A09.getInt(columnIndexOrThrow40) != 1) {
                            z3 = false;
                        }
                        r4.A0g = z3;
                        r4.A01 = A09.getInt(columnIndexOrThrow37);
                        r4.A0b = new AnonymousClass1V0(A09.getInt(columnIndexOrThrow38), A09.getLong(columnIndexOrThrow39));
                        hashMap.put(A012, r4);
                    }
                }
            }
            synchronized (this) {
                for (Map.Entry entry : hashMap.entrySet()) {
                    this.A07.put(Long.valueOf(((AnonymousClass1PE) entry.getValue()).A0V), (AbstractC14640lm) entry.getKey());
                    this.A06.put((AbstractC14640lm) entry.getKey(), Long.valueOf(((AnonymousClass1PE) entry.getValue()).A0V));
                }
            }
            if (A09 != null) {
                A09.close();
            }
            A01.close();
            return hashMap;
        } finally {
            r55.A01();
        }
    }

    public void A08(AnonymousClass1PE r5) {
        ContentValues contentValues;
        synchronized (r5) {
            contentValues = new ContentValues(2);
            contentValues.put("change_number_notified_message_row_id", Long.valueOf(r5.A0B));
        }
        A00(contentValues, r5.A0i);
    }

    public void A09(AnonymousClass1PE r5) {
        ContentValues contentValues;
        try {
            try {
                synchronized (r5) {
                    contentValues = new ContentValues();
                    contentValues.put("archived", Boolean.valueOf(r5.A0f));
                }
                AbstractC14640lm r2 = r5.A0i;
                if (A00(contentValues, r2) == 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("msgstore/archive/did not update ");
                    sb.append(r2);
                    Log.e(sb.toString());
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
                this.A03.A02();
            }
        } catch (Error | RuntimeException e2) {
            Log.e(e2);
            throw e2;
        }
    }

    public void A0A(AnonymousClass1PE r5) {
        ContentValues contentValues;
        synchronized (r5) {
            contentValues = new ContentValues();
            contentValues.put("unseen_message_reaction_count", Integer.valueOf(r5.A05));
            contentValues.put("last_message_reaction_row_id", Long.valueOf(r5.A0L));
            contentValues.put("last_seen_message_reaction_row_id", Long.valueOf(r5.A0S));
        }
        AbstractC14640lm r1 = r5.A0i;
        int A00 = A00(contentValues, r1);
        StringBuilder sb = new StringBuilder("msgstore/updateChatLastMessageReactionInfo/");
        sb.append(r1);
        sb.append("/");
        sb.append(r5.A07());
        sb.append("/");
        sb.append(A00);
        Log.i(sb.toString());
    }

    public void A0B(AnonymousClass1PE r7) {
        ContentValues A03;
        try {
            try {
                synchronized (r7) {
                    A03 = r7.A03();
                    A03.put("last_read_message_row_id", Long.valueOf(r7.A0O));
                    A03.put("last_read_message_sort_id", Long.valueOf(r7.A0P));
                    A03.put("last_message_row_id", Long.valueOf(r7.A0M));
                    A03.put("last_message_sort_id", Long.valueOf(r7.A0N));
                    A03.put("last_important_message_row_id", Long.valueOf(r7.A0K));
                    A03.put("unseen_important_message_count", Integer.valueOf(r7.A04));
                    A03.put("unseen_message_reaction_count", Integer.valueOf(r7.A05));
                    A03.put("last_message_reaction_row_id", Long.valueOf(r7.A0L));
                    A03.put("last_seen_message_reaction_row_id", Long.valueOf(r7.A0S));
                }
                AbstractC14640lm r3 = r7.A0i;
                int A00 = A00(A03, r3);
                StringBuilder sb = new StringBuilder();
                sb.append("msgstore/setchatseen/");
                sb.append(r3);
                sb.append("/");
                sb.append(r7.A07());
                sb.append("/");
                sb.append(A00);
                Log.i(sb.toString());
            } catch (Error | RuntimeException e) {
                Log.e(e);
                throw e;
            }
        } catch (SQLiteDatabaseCorruptException e2) {
            Log.e(e2);
            this.A03.A02();
        }
    }

    public void A0C(AnonymousClass1PE r6) {
        try {
            ContentValues A03 = r6.A03();
            AbstractC14640lm r3 = r6.A0i;
            int A00 = A00(A03, r3);
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore/setchatunseen/");
            sb.append(r3);
            sb.append("/");
            sb.append(r6.A07());
            sb.append("/");
            sb.append(A00);
            Log.i(sb.toString());
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e(e);
            this.A03.A02();
        } catch (Error | RuntimeException e2) {
            Log.e(e2);
            throw e2;
        }
    }

    public void A0D(AnonymousClass1PE r5) {
        ContentValues contentValues;
        synchronized (r5) {
            contentValues = new ContentValues();
            contentValues.put("ephemeral_expiration", Integer.valueOf(r5.A0Y.expiration));
            contentValues.put("ephemeral_setting_timestamp", Long.valueOf(r5.A0Y.ephemeralSettingTimestamp));
            contentValues.put("ephemeral_disappearing_messages_initiator", Integer.valueOf(r5.A0Y.disappearingMessagesInitiator));
        }
        A00(contentValues, r5.A0i);
    }

    public void A0E(AbstractC14640lm r10) {
        C16310on A02 = this.A04.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A0C("DELETE FROM chat WHERE jid_row_id = ?", new String[]{String.valueOf(this.A02.A01(r10))});
            C19990v2 r1 = this.A01;
            synchronized (r1) {
                if (r10 != null) {
                    r1.A0B().remove(r10);
                }
            }
            synchronized (this) {
                Long l = (Long) this.A06.remove(r10);
                if (l != null) {
                    this.A07.remove(l);
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0F(AbstractC14640lm r5, long j) {
        AnonymousClass1PE A06 = this.A01.A06(r5);
        if (A06 != null && A06.A0S <= j && j >= 1) {
            A06.A0S = j;
            if (A06.A0L < j) {
                A06.A0L = j;
            }
            A06.A05 = 0;
            A0A(A06);
        }
    }

    public boolean A0G() {
        return this.A05.A01("chat_ready", 0) == 2;
    }

    public boolean A0H(ContentValues contentValues, AnonymousClass1PE r8) {
        AbstractC14640lm r1 = r8.A0i;
        if (A00(contentValues, r1) != 0) {
            return true;
        }
        contentValues.put("jid_row_id", Long.valueOf(this.A02.A01(r1)));
        long A01 = A01(contentValues);
        r8.A0V = A01;
        if (A01 == -1) {
            return false;
        }
        return true;
    }

    public boolean A0I(AnonymousClass1PE r4) {
        ContentValues contentValues;
        synchronized (r4) {
            contentValues = new ContentValues(3);
            contentValues.put("mod_tag", Integer.valueOf(r4.A0A));
        }
        return A0H(contentValues, r4);
    }
}
