package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60372wr extends AbstractC75673kE {
    public final Runnable A00;

    public C60372wr(View view, Runnable runnable) {
        super(view);
        this.A00 = runnable;
    }

    @Override // X.AbstractC75673kE
    public void A08(Object obj, int i) {
        View view = this.A0H;
        AbstractView$OnClickListenerC34281fs.A00(AnonymousClass028.A0D(view, R.id.view_community), this, 42);
        C27531Hw.A06(C12960it.A0I(view, R.id.view_community));
    }
}
