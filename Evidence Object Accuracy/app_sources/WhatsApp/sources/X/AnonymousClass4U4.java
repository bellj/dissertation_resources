package X;

/* renamed from: X.4U4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4U4 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final long A04;
    public final long A05;
    public final long A06;
    public final C100614mC A07;
    public final long[] A08;
    public final long[] A09;
    public final AnonymousClass4SP[] A0A;

    public AnonymousClass4U4(C100614mC r1, long[] jArr, long[] jArr2, AnonymousClass4SP[] r4, int i, int i2, int i3, int i4, long j, long j2, long j3) {
        this.A00 = i;
        this.A03 = i2;
        this.A06 = j;
        this.A05 = j2;
        this.A04 = j3;
        this.A07 = r1;
        this.A02 = i3;
        this.A0A = r4;
        this.A01 = i4;
        this.A08 = jArr;
        this.A09 = jArr2;
    }
}
