package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5m2  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5m2 extends AbstractC118835cS {
    public final Context A00;
    public final View A01;
    public final ImageView A02;
    public final ProgressBar A03;
    public final RelativeLayout A04;
    public final TextView A05;
    public final TextView A06;
    public final TextView A07;
    public final AnonymousClass130 A08;
    public final C21270x9 A09;

    public AnonymousClass5m2(View view, AnonymousClass130 r3, C21270x9 r4) {
        super(view);
        this.A09 = r4;
        this.A08 = r3;
        this.A00 = view.getContext();
        this.A07 = C12960it.A0I(view, R.id.title);
        this.A05 = C12960it.A0I(view, R.id.subtitle);
        this.A04 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.root);
        this.A02 = C12970iu.A0K(view, R.id.icon);
        this.A03 = (ProgressBar) AnonymousClass028.A0D(view, R.id.progress_bar);
        this.A01 = AnonymousClass028.A0D(view, R.id.open_indicator);
        this.A06 = C12960it.A0I(view, R.id.secondary_subtitle);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r8, int i) {
        ImageView imageView;
        C123315mx r82 = (C123315mx) r8;
        boolean z = true;
        int i2 = 8;
        if (!TextUtils.isEmpty(r82.A09)) {
            this.A07.setText(r82.A09);
            this.A05.setText(r82.A08);
            C15370n3 r0 = r82.A05;
            if (r0 != null && TextUtils.isEmpty(r0.A0K) && !TextUtils.isEmpty(r82.A05.A0U)) {
                String A0X = C12960it.A0X(this.A0H.getContext(), r82.A05.A0U, new Object[1], 0, R.string.novi_payment_transaction_details_other_party_push_name_label);
                TextView textView = this.A06;
                textView.setText(A0X);
                textView.setVisibility(0);
            }
        } else {
            this.A04.setVisibility(8);
        }
        if (r82.A05 != null) {
            AnonymousClass1J1 A04 = this.A09.A04(this.A00, "novi-pay-transaction-detail-view-holder");
            C15370n3 r02 = r82.A05;
            imageView = this.A02;
            A04.A06(imageView, r02);
        } else {
            AnonymousClass130 r1 = this.A08;
            imageView = this.A02;
            r1.A05(imageView, R.drawable.avatar_contact);
        }
        RelativeLayout relativeLayout = this.A04;
        relativeLayout.setOnClickListener(r82.A04);
        if (r82.A04 == null) {
            z = false;
        }
        relativeLayout.setEnabled(z);
        View view = this.A01;
        if (relativeLayout.isEnabled()) {
            i2 = 0;
        }
        view.setVisibility(i2);
        imageView.setVisibility(r82.A01);
        this.A03.setVisibility(r82.A02);
    }
}
