package X;

import android.content.Context;
import android.content.res.ColorStateList;

/* renamed from: X.0iR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12750iR {
    ColorStateList AAn(AbstractC11670gf v);

    float ACg(AbstractC11670gf v);

    float AE8(AbstractC11670gf v);

    float AEU(AbstractC11670gf v);

    float AEW(AbstractC11670gf v);

    float AG3(AbstractC11670gf v);

    void AIq();

    void AIw(Context context, ColorStateList colorStateList, AbstractC11670gf v, float f, float f2, float f3);

    void AOK(AbstractC11670gf v);

    void AUD(AbstractC11670gf v);

    void Abm(ColorStateList colorStateList, AbstractC11670gf v);

    void Ac6(AbstractC11670gf v, float f);

    void AcJ(AbstractC11670gf v, float f);

    void Aci(AbstractC11670gf v, float f);
}
