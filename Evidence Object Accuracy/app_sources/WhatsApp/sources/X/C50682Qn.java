package X;

import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.2Qn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50682Qn implements AbstractC28771Oy {
    public final /* synthetic */ C28921Pn A00;
    public final /* synthetic */ C22370yy A01;
    public final /* synthetic */ String A02;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C50682Qn(C28921Pn r1, C22370yy r2, String str) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = str;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        C28921Pn r2;
        C22370yy r5 = this.A01;
        HashMap hashMap = r5.A0s;
        synchronized (hashMap) {
            StringBuilder sb = new StringBuilder();
            sb.append("mediadownloadmanager/queueexpresspathdownload Download canceled for media job: ");
            r2 = this.A00;
            sb.append(r2);
            sb.append(" enc hash: ");
            String str = this.A02;
            sb.append(str);
            Log.i(sb.toString());
            hashMap.remove(str);
        }
        C22370yy.A01(r5, r2.A0a);
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r6, C28781Oz r7) {
        C28921Pn r1;
        C22370yy r4 = this.A01;
        HashMap hashMap = r4.A0s;
        synchronized (hashMap) {
            StringBuilder sb = new StringBuilder();
            sb.append("mediadownloadmanager/queueexpresspathdownload Download finishes for media job: ");
            r1 = this.A00;
            sb.append(r1);
            Log.i(sb.toString());
            hashMap.remove(this.A02);
        }
        C22370yy.A01(r4, r1.A0a);
    }
}
