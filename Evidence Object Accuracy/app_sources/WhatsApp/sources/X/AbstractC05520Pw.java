package X;

import android.graphics.PointF;
import android.util.Log;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0Pw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC05520Pw {
    public int A00 = -1;
    public View A01;
    public AnonymousClass02H A02;
    public RecyclerView A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public final C05160Om A07 = new C05160Om();

    public abstract void A03(View view, C05160Om v, C05480Ps v2);

    public PointF A00(int i) {
        AnonymousClass02H r1 = this.A02;
        if (r1 instanceof AnonymousClass02I) {
            return ((AnonymousClass02I) r1).A7U(i);
        }
        StringBuilder sb = new StringBuilder("You should override computeScrollVectorForPosition when the LayoutManager does not implement ");
        sb.append(AnonymousClass02I.class.getCanonicalName());
        Log.w("RecyclerView", sb.toString());
        return null;
    }

    public final void A01() {
        if (this.A05) {
            this.A05 = false;
            AnonymousClass0FE r0 = (AnonymousClass0FE) this;
            r0.A01 = 0;
            r0.A00 = 0;
            r0.A02 = null;
            this.A03.A0y.A06 = -1;
            this.A01 = null;
            this.A00 = -1;
            this.A04 = false;
            AnonymousClass02H r1 = this.A02;
            if (r1.A06 == this) {
                r1.A06 = null;
            }
            this.A02 = null;
            this.A03 = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(int r10, int r11) {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC05520Pw.A02(int, int):void");
    }
}
