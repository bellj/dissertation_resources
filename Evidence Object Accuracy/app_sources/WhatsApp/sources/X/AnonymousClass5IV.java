package X;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.5IV  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5IV extends CipherSpi implements AnonymousClass5S1 {
    public int A00;
    public AlgorithmParameters A01;
    public AnonymousClass5X5 A02;
    public C866948l A03;
    public boolean A04;
    public byte[] A05;
    public Class[] A06;
    public final AnonymousClass5S2 A07 = A01(this);

    @Override // javax.crypto.CipherSpi
    public int engineGetBlockSize() {
        return 0;
    }

    @Override // javax.crypto.CipherSpi
    public byte[] engineGetIV() {
        return AnonymousClass1TT.A02(this.A05);
    }

    @Override // javax.crypto.CipherSpi
    public int engineGetKeySize(Key key) {
        return key.getEncoded().length << 3;
    }

    @Override // javax.crypto.CipherSpi
    public int engineGetOutputSize(int i) {
        return -1;
    }

    @Override // javax.crypto.CipherSpi
    public void engineInit(int i, Key key, SecureRandom secureRandom) {
        try {
            engineInit(i, key, (AlgorithmParameterSpec) null, secureRandom);
        } catch (InvalidAlgorithmParameterException e) {
            throw new AnonymousClass5HX(e.getMessage(), e);
        }
    }

    @Override // javax.crypto.CipherSpi
    public byte[] engineWrap(Key key) {
        byte[] encoded = key.getEncoded();
        if (encoded != null) {
            try {
                AnonymousClass5X5 r2 = this.A02;
                return r2 == null ? engineDoFinal(encoded, 0, encoded.length) : r2.Ag9(encoded, 0, encoded.length);
            } catch (BadPaddingException e) {
                throw new IllegalBlockSizeException(e.getMessage());
            }
        } else {
            throw new InvalidKeyException("Cannot wrap key, null encoding.");
        }
    }

    public AnonymousClass5IV() {
    }

    public AnonymousClass5IV(AnonymousClass5X5 r2, int i) {
        this.A02 = r2;
        this.A00 = i;
    }

    public static C112955Fl A00() {
        return new C112955Fl(new C71643dG());
    }

    public static AnonymousClass5GT A01(AnonymousClass5IV r3) {
        r3.A06 = new Class[]{AnonymousClass5C3.class, PBEParameterSpec.class, RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class};
        r3.A01 = null;
        r3.A02 = null;
        r3.A03 = null;
        return new AnonymousClass5GT();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e A[Catch: all -> 0x005a, TRY_LEAVE, TryCatch #2 {all -> 0x005a, blocks: (B:5:0x0007, B:7:0x000c, B:9:0x001e, B:10:0x0027, B:11:0x0028, B:12:0x0038, B:14:0x003e, B:17:0x0047, B:18:0x004e, B:20:0x0050, B:21:0x0059), top: B:31:0x0007, inners: #0, #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047 A[Catch: all -> 0x005a, TRY_ENTER, TryCatch #2 {all -> 0x005a, blocks: (B:5:0x0007, B:7:0x000c, B:9:0x001e, B:10:0x0027, B:11:0x0028, B:12:0x0038, B:14:0x003e, B:17:0x0047, B:18:0x004e, B:20:0x0050, B:21:0x0059), top: B:31:0x0007, inners: #0, #1 }] */
    @Override // javax.crypto.CipherSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int engineDoFinal(byte[] r6, int r7, int r8, byte[] r9, int r10) {
        /*
            r5 = this;
            X.48l r0 = r5.A03
            if (r0 == 0) goto L_0x0061
            r0.write(r6, r7, r8)
            boolean r0 = r5.A04     // Catch: all -> 0x005a
            r4 = 0
            if (r0 == 0) goto L_0x0028
            X.5X5 r2 = r5.A02     // Catch: Exception -> 0x001d, all -> 0x005a
            X.48l r0 = r5.A03     // Catch: Exception -> 0x001d, all -> 0x005a
            byte[] r1 = r0.A01()     // Catch: Exception -> 0x001d, all -> 0x005a
            int r0 = r0.size()     // Catch: Exception -> 0x001d, all -> 0x005a
            byte[] r3 = r2.Ag9(r1, r4, r0)     // Catch: Exception -> 0x001d, all -> 0x005a
            goto L_0x0038
        L_0x001d:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()     // Catch: all -> 0x005a
            javax.crypto.IllegalBlockSizeException r0 = new javax.crypto.IllegalBlockSizeException     // Catch: all -> 0x005a
            r0.<init>(r1)     // Catch: all -> 0x005a
            throw r0     // Catch: all -> 0x005a
        L_0x0028:
            X.5X5 r2 = r5.A02     // Catch: 5Nt -> 0x004f, all -> 0x005a
            X.48l r0 = r5.A03     // Catch: 5Nt -> 0x004f, all -> 0x005a
            byte[] r1 = r0.A01()     // Catch: 5Nt -> 0x004f, all -> 0x005a
            int r0 = r0.size()     // Catch: 5Nt -> 0x004f, all -> 0x005a
            byte[] r3 = r2.AfE(r1, r4, r0)     // Catch: 5Nt -> 0x004f, all -> 0x005a
        L_0x0038:
            int r2 = r3.length     // Catch: all -> 0x005a
            int r1 = r2 + r10
            int r0 = r9.length     // Catch: all -> 0x005a
            if (r1 > r0) goto L_0x0047
            java.lang.System.arraycopy(r3, r4, r9, r10, r2)     // Catch: all -> 0x005a
            X.48l r0 = r5.A03
            r0.A00()
            return r2
        L_0x0047:
            java.lang.String r1 = "output buffer too short for input."
            javax.crypto.ShortBufferException r0 = new javax.crypto.ShortBufferException     // Catch: all -> 0x005a
            r0.<init>(r1)     // Catch: all -> 0x005a
            throw r0     // Catch: all -> 0x005a
        L_0x004f:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()     // Catch: all -> 0x005a
            javax.crypto.BadPaddingException r0 = new javax.crypto.BadPaddingException     // Catch: all -> 0x005a
            r0.<init>(r1)     // Catch: all -> 0x005a
            throw r0     // Catch: all -> 0x005a
        L_0x005a:
            r1 = move-exception
            X.48l r0 = r5.A03
            r0.A00()
            throw r1
        L_0x0061:
            java.lang.String r0 = "not supported in a wrapping mode"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5IV.engineDoFinal(byte[], int, int, byte[], int):int");
    }

    @Override // javax.crypto.CipherSpi
    public byte[] engineDoFinal(byte[] bArr, int i, int i2) {
        byte[] Ag9;
        C866948l r0 = this.A03;
        if (r0 != null) {
            if (bArr != null) {
                r0.write(bArr, i, i2);
            }
            try {
                if (this.A04) {
                    try {
                        AnonymousClass5X5 r2 = this.A02;
                        C866948l r02 = this.A03;
                        Ag9 = r2.Ag9(r02.A01(), 0, r02.size());
                        return Ag9;
                    } catch (Exception e) {
                        throw new IllegalBlockSizeException(e.getMessage());
                    }
                } else {
                    try {
                        AnonymousClass5X5 r22 = this.A02;
                        C866948l r03 = this.A03;
                        Ag9 = r22.AfE(r03.A01(), 0, r03.size());
                        return Ag9;
                    } catch (C114965Nt e2) {
                        throw new BadPaddingException(e2.getMessage());
                    }
                }
            } finally {
                this.A03.A00();
            }
        } else {
            throw C12960it.A0U("not supported in a wrapping mode");
        }
    }

    @Override // javax.crypto.CipherSpi
    public AlgorithmParameters engineGetParameters() {
        if (this.A01 == null && this.A05 != null) {
            String AAf = this.A02.AAf();
            int indexOf = AAf.indexOf(47);
            if (indexOf >= 0) {
                AAf = AAf.substring(0, indexOf);
            }
            try {
                AnonymousClass5PQ.A02(AAf, this).init(new IvParameterSpec(this.A05));
            } catch (Exception e) {
                throw C12990iw.A0m(e.toString());
            }
        }
        return this.A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        if (r0 != null) goto L_0x0021;
     */
    @Override // javax.crypto.CipherSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void engineInit(int r4, java.security.Key r5, java.security.AlgorithmParameters r6, java.security.SecureRandom r7) {
        /*
            r3 = this;
            if (r6 == 0) goto L_0x001d
            java.lang.Class[] r2 = r3.A06
            java.lang.Class<java.security.spec.AlgorithmParameterSpec> r0 = java.security.spec.AlgorithmParameterSpec.class
            java.security.spec.AlgorithmParameterSpec r0 = r6.getParameterSpec(r0)     // Catch: Exception -> 0x000b
            goto L_0x001f
        L_0x000b:
            r1 = 0
        L_0x000c:
            int r0 = r2.length
            if (r1 == r0) goto L_0x0027
            r0 = r2[r1]
            if (r0 == 0) goto L_0x001a
            r0 = r2[r1]     // Catch: Exception -> 0x001a
            java.security.spec.AlgorithmParameterSpec r0 = r6.getParameterSpec(r0)     // Catch: Exception -> 0x001a
            goto L_0x001f
        L_0x001a:
            int r1 = r1 + 1
            goto L_0x000c
        L_0x001d:
            r0 = 0
            goto L_0x0021
        L_0x001f:
            if (r0 == 0) goto L_0x0027
        L_0x0021:
            r3.A01 = r6
            r3.engineInit(r4, r5, r0, r7)
            return
        L_0x0027:
            java.lang.String r0 = "can't handle parameter "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r6.toString()
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            java.security.InvalidAlgorithmParameterException r0 = X.C72463ee.A0J(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5IV.engineInit(int, java.security.Key, java.security.AlgorithmParameters, java.security.SecureRandom):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0056, code lost:
        if (r10 != null) goto L_0x0043;
     */
    @Override // javax.crypto.CipherSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void engineInit(int r7, java.security.Key r8, java.security.spec.AlgorithmParameterSpec r9, java.security.SecureRandom r10) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof X.AnonymousClass5ED
            if (r0 == 0) goto L_0x0066
            X.5ED r8 = (X.AnonymousClass5ED) r8
            boolean r0 = r9 instanceof javax.crypto.spec.PBEParameterSpec
            if (r0 == 0) goto L_0x0059
            X.5X5 r0 = r6.A02
            java.lang.String r0 = r0.AAf()
            X.20L r3 = X.C95114dA.A00(r0, r9, r8)
        L_0x0014:
            boolean r0 = r9 instanceof javax.crypto.spec.IvParameterSpec
            if (r0 == 0) goto L_0x0026
            javax.crypto.spec.IvParameterSpec r9 = (javax.crypto.spec.IvParameterSpec) r9
            byte[] r1 = r9.getIV()
            r6.A05 = r1
            X.5Fx r0 = new X.5Fx
            r0.<init>(r3, r1)
            r3 = r0
        L_0x0026:
            boolean r0 = r3 instanceof X.AnonymousClass20K
            r5 = 3
            r4 = 1
            if (r0 == 0) goto L_0x0056
            int r0 = r6.A00
            if (r0 == 0) goto L_0x0056
            if (r7 == r5) goto L_0x0034
            if (r7 != r4) goto L_0x0056
        L_0x0034:
            byte[] r0 = new byte[r0]
            r6.A05 = r0
            r10.nextBytes(r0)
            byte[] r1 = r6.A05
            X.5Fx r0 = new X.5Fx
            r0.<init>(r3, r1)
            r3 = r0
        L_0x0043:
            X.5Fs r0 = new X.5Fs
            r0.<init>(r10, r3)
            r3 = r0
        L_0x0049:
            if (r7 == r4) goto L_0x008f
            r0 = 2
            r2 = 0
            if (r7 == r0) goto L_0x0078
            r1 = 0
            if (r7 == r5) goto L_0x009c
            r0 = 4
            if (r7 != r0) goto L_0x0070
            goto L_0x0085
        L_0x0056:
            if (r10 == 0) goto L_0x0049
            goto L_0x0043
        L_0x0059:
            X.AnonymousClass5ED.A00(r8)
            X.20L r0 = r8.param
            if (r0 == 0) goto L_0x00b1
            X.AnonymousClass5ED.A00(r8)
            X.20L r3 = r8.param
            goto L_0x0014
        L_0x0066:
            byte[] r0 = r8.getEncoded()
            X.20K r3 = new X.20K
            r3.<init>(r0)
            goto L_0x0014
        L_0x0070:
            java.lang.String r1 = "Unknown mode parameter passed to init."
            java.security.InvalidParameterException r0 = new java.security.InvalidParameterException     // Catch: Exception -> 0x00a6
            r0.<init>(r1)     // Catch: Exception -> 0x00a6
            throw r0     // Catch: Exception -> 0x00a6
        L_0x0078:
            X.5X5 r0 = r6.A02     // Catch: Exception -> 0x00a6
            r0.AIf(r3, r2)     // Catch: Exception -> 0x00a6
            X.48l r0 = new X.48l     // Catch: Exception -> 0x00a6
            r0.<init>()     // Catch: Exception -> 0x00a6
            r6.A03 = r0     // Catch: Exception -> 0x00a6
            goto L_0x008c
        L_0x0085:
            X.5X5 r0 = r6.A02     // Catch: Exception -> 0x00a6
            r0.AIf(r3, r2)     // Catch: Exception -> 0x00a6
            r6.A03 = r1     // Catch: Exception -> 0x00a6
        L_0x008c:
            r6.A04 = r2     // Catch: Exception -> 0x00a6
            return
        L_0x008f:
            X.5X5 r0 = r6.A02     // Catch: Exception -> 0x00a6
            r0.AIf(r3, r4)     // Catch: Exception -> 0x00a6
            X.48l r0 = new X.48l     // Catch: Exception -> 0x00a6
            r0.<init>()     // Catch: Exception -> 0x00a6
            r6.A03 = r0     // Catch: Exception -> 0x00a6
            goto L_0x00a3
        L_0x009c:
            X.5X5 r0 = r6.A02     // Catch: Exception -> 0x00a6
            r0.AIf(r3, r4)     // Catch: Exception -> 0x00a6
            r6.A03 = r1     // Catch: Exception -> 0x00a6
        L_0x00a3:
            r6.A04 = r4     // Catch: Exception -> 0x00a6
            return
        L_0x00a6:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()
            X.5HX r0 = new X.5HX
            r0.<init>(r1, r2)
            throw r0
        L_0x00b1:
            java.lang.String r0 = "PBE requires PBE parameters to be set."
            java.security.InvalidAlgorithmParameterException r0 = X.C72463ee.A0J(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5IV.engineInit(int, java.security.Key, java.security.spec.AlgorithmParameterSpec, java.security.SecureRandom):void");
    }

    @Override // javax.crypto.CipherSpi
    public void engineSetMode(String str) {
        throw new NoSuchAlgorithmException(C12960it.A0d(str, C12960it.A0k("can't support mode ")));
    }

    @Override // javax.crypto.CipherSpi
    public void engineSetPadding(String str) {
        StringBuilder A0k = C12960it.A0k("Padding ");
        A0k.append(str);
        throw new NoSuchPaddingException(C12960it.A0d(" unknown.", A0k));
    }

    @Override // javax.crypto.CipherSpi
    public Key engineUnwrap(byte[] bArr, String str, int i) {
        AnonymousClass5MM r4;
        try {
            AnonymousClass5X5 r2 = this.A02;
            byte[] engineDoFinal = r2 == null ? engineDoFinal(bArr, 0, bArr.length) : r2.AfE(bArr, 0, bArr.length);
            if (i == 3) {
                return new SecretKeySpec(engineDoFinal, str);
            }
            if (!str.equals("") || i != 2) {
                try {
                    KeyFactory instance = KeyFactory.getInstance(str, ((AnonymousClass5GT) this.A07).A00);
                    if (i == 1) {
                        return instance.generatePublic(new X509EncodedKeySpec(engineDoFinal));
                    }
                    if (i == 2) {
                        return instance.generatePrivate(new PKCS8EncodedKeySpec(engineDoFinal));
                    }
                    throw new InvalidKeyException(C12960it.A0e("Unknown key type ", C12960it.A0h(), i));
                } catch (NoSuchProviderException e) {
                    throw new InvalidKeyException(C12960it.A0d(e.getMessage(), C12960it.A0j("Unknown key type ")));
                } catch (InvalidKeySpecException e2) {
                    throw new InvalidKeyException(C12960it.A0d(e2.getMessage(), C12960it.A0j("Unknown key type ")));
                }
            } else {
                try {
                    if (engineDoFinal instanceof AnonymousClass5MM) {
                        r4 = (AnonymousClass5MM) engineDoFinal;
                    } else {
                        r4 = new AnonymousClass5MM(AbstractC114775Na.A04(engineDoFinal));
                    }
                    AnonymousClass1TK r22 = r4.A04.A01;
                    Map map = C27511Hu.A00;
                    synchronized (map) {
                        map.get(r22);
                    }
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("algorithm ");
                    A0h.append(r22);
                    throw new InvalidKeyException(C12960it.A0d(" not supported", A0h));
                } catch (Exception unused) {
                    throw new InvalidKeyException("Invalid key encoding.");
                }
            }
        } catch (C114965Nt e3) {
            throw new InvalidKeyException(e3.getMessage());
        } catch (BadPaddingException e4) {
            throw new InvalidKeyException(e4.getMessage());
        } catch (IllegalBlockSizeException e5) {
            throw new InvalidKeyException(e5.getMessage());
        }
    }

    @Override // javax.crypto.CipherSpi
    public int engineUpdate(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        C866948l r0 = this.A03;
        if (r0 != null) {
            r0.write(bArr, i, i2);
            return 0;
        }
        throw C12960it.A0U("not supported in a wrapping mode");
    }

    @Override // javax.crypto.CipherSpi
    public byte[] engineUpdate(byte[] bArr, int i, int i2) {
        C866948l r0 = this.A03;
        if (r0 != null) {
            r0.write(bArr, i, i2);
            return null;
        }
        throw C12960it.A0U("not supported in a wrapping mode");
    }
}
