package X;

import android.content.SharedPreferences;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Dr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26491Dr {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C26491Dr(C16630pM r1) {
        this.A01 = r1;
    }

    public static String A00(int i, int i2, int i3, long j, boolean z) {
        if (!(i == 2 || i == 3 || i == 5)) {
            i = 1;
        }
        return String.format(Locale.US, "%d_%d_%d_%d_%b", Long.valueOf(j), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Boolean.valueOf(z));
    }

    public C41441tX A01(int i, int i2, int i3, long j, boolean z) {
        C41441tX A00;
        int i4 = i;
        String A002 = A00(i4, i2, i3, j, z);
        SharedPreferences sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("media_daily_usage_preferences_v1");
            this.A00 = sharedPreferences;
        }
        String string = sharedPreferences.getString(A002, null);
        if (string != null && !string.isEmpty() && (A00 = C41441tX.A00(string)) != null) {
            return A00;
        }
        if (!(i == 2 || i == 3 || i == 5)) {
            i4 = 1;
        }
        return new C41441tX(i4, i2, i3, j, z);
    }

    public void A02(C41441tX r7, int i, int i2, int i3, long j, boolean z) {
        String A00 = A00(i, i2, i3, j, z);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("bytesSent", r7.A01);
            jSONObject.put("bytesReceived", r7.A00);
            jSONObject.put("countMessageSent", r7.A05);
            jSONObject.put("countMessageReceived", r7.A04);
            jSONObject.put("countUploaded", r7.A07);
            jSONObject.put("countDownloaded", r7.A02);
            jSONObject.put("countForward", r7.A03);
            jSONObject.put("countShared", r7.A06);
            jSONObject.put("countViewed", r7.A08);
            jSONObject.put("transferDate", r7.A0C);
            jSONObject.put("mediaType", r7.A0A);
            jSONObject.put("transferRadio", r7.A0B);
            jSONObject.put("mediaTransferOrigin", r7.A09);
            jSONObject.put("isAutoDownload", r7.A0D);
            String obj = jSONObject.toString();
            SharedPreferences sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A01.A01("media_daily_usage_preferences_v1");
                this.A00 = sharedPreferences;
            }
            sharedPreferences.edit().putString(A00, obj).apply();
        } catch (JSONException e) {
            e.getMessage();
        }
    }
}
