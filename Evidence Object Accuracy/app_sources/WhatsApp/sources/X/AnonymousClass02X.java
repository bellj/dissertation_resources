package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

/* renamed from: X.02X  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass02X {
    public int A00 = -1;
    public int A01;
    public final C47602Bq A02 = new C47602Bq();

    public static int A00(C47602Bq r5, double d) {
        long j = (long) d;
        if (((double) j) == d) {
            return A01(r5, j);
        }
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        r5.write((byte) ((int) doubleToRawLongBits));
        r5.write((byte) ((int) (doubleToRawLongBits >> 8)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 16)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 24)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 32)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 40)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 48)));
        r5.write((byte) ((int) (doubleToRawLongBits >> 56)));
        return 7;
    }

    public static int A01(C47602Bq r4, long j) {
        if (j == 0) {
            return 1;
        }
        if (j == 1) {
            return 2;
        }
        r4.write((byte) ((int) j));
        if (-128 <= j && j <= 127) {
            return 3;
        }
        r4.write((byte) ((int) (j >> 8)));
        if (-32768 <= j && j <= 32767) {
            return 4;
        }
        r4.write((byte) ((int) (j >> 16)));
        r4.write((byte) ((int) (j >> 24)));
        if (-2147483648L <= j && j <= 2147483647L) {
            return 5;
        }
        r4.write((byte) ((int) (j >> 32)));
        r4.write((byte) ((int) (j >> 40)));
        r4.write((byte) ((int) (j >> 48)));
        r4.write((byte) ((int) (j >> 56)));
        return 6;
    }

    public static int A02(C47602Bq r3, long j) {
        if (j < 0 || j > 4294967295L) {
            throw new IllegalArgumentException("Value is not an unsigned integer");
        }
        r3.write((byte) ((int) j));
        if (j <= 255) {
            return 1;
        }
        r3.write((byte) ((int) (j >> 8)));
        if (j <= 65535) {
            return 2;
        }
        r3.write((byte) ((int) (j >> 16)));
        r3.write((byte) ((int) (j >> 24)));
        return 4;
    }

    public static int A03(C47602Bq r2, Object obj) {
        if (obj == null) {
            return 0;
        }
        if (obj instanceof Boolean) {
            return A01(r2, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Long) {
            return A01(r2, ((Number) obj).longValue());
        } else {
            if (obj instanceof Number) {
                return A00(r2, ((Number) obj).doubleValue());
            }
            if (obj instanceof String) {
                return A04(r2, (String) obj);
            }
            StringBuilder sb = new StringBuilder("Expected class Boolean, Number, or String, got ");
            sb.append(obj.getClass().getName());
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public static int A04(C47602Bq r8, String str) {
        try {
            byte[] bytes = str.getBytes(AnonymousClass01V.A08);
            int length = bytes.length;
            if (length > 1024) {
                Log.w(String.format(Locale.US, "wam/serialize: string length is limited to %d UTF-8 bytes", Integer.valueOf((int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)));
            }
            int min = Math.min(length, (int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            int A02 = A02(r8, (long) min);
            r8.write(bytes, 0, min);
            if (A02 == 1) {
                return 8;
            }
            if (A02 == 2) {
                return 9;
            }
            if (A02 == 4) {
                return 10;
            }
            throw new Error("Impossible");
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    public static AnonymousClass04W A05(ByteBuffer byteBuffer) {
        long j;
        long j2;
        int i;
        int i2;
        long j3;
        int i3;
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int position = byteBuffer.position();
        byte b = byteBuffer.get();
        int i4 = b & 3;
        if (i4 <= 2) {
            if ((b & 8) == 0) {
                j2 = 255;
                j = 0 | ((((long) byteBuffer.get()) & 255) << 0);
            } else {
                j = 0;
                int i5 = 0;
                do {
                    j2 = 255;
                    j |= (((long) byteBuffer.get()) & 255) << (i5 << 3);
                    i5++;
                } while (i5 < 2);
            }
            int i6 = (int) j;
            int i7 = (b >> 4) & 15;
            if (i7 <= 10) {
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                switch (i7) {
                    case 0:
                        i = null;
                        break;
                    case 1:
                        i = 0;
                        break;
                    case 2:
                        i = 1;
                        break;
                    case 3:
                        i = Byte.valueOf(byteBuffer.get());
                        break;
                    case 4:
                        i = Short.valueOf(byteBuffer.getShort());
                        break;
                    case 5:
                        i = Integer.valueOf(byteBuffer.getInt());
                        break;
                    case 6:
                        i = Long.valueOf(byteBuffer.getLong());
                        break;
                    case 7:
                        i = Double.valueOf(byteBuffer.getDouble());
                        break;
                    case 8:
                        i = A06(byteBuffer, (int) (0 | ((((long) byteBuffer.get()) & j2) << 0)));
                        break;
                    case 9:
                        i2 = 2;
                        j3 = 0;
                        i3 = 0;
                        do {
                            j3 |= (((long) byteBuffer.get()) & j2) << (i3 << 3);
                            i3++;
                        } while (i3 < i2);
                        i = A06(byteBuffer, (int) j3);
                        break;
                    case 10:
                        i2 = 4;
                        j3 = 0;
                        i3 = 0;
                        do {
                            j3 |= (((long) byteBuffer.get()) & j2) << (i3 << 3);
                            i3++;
                        } while (i3 < i2);
                        i = A06(byteBuffer, (int) j3);
                        break;
                    default:
                        throw new Error("Invalid value type");
                }
                return new AnonymousClass04W(i, i4, i6);
            }
            try {
                StringBuilder sb = new StringBuilder("Invalid value type ");
                sb.append(i7);
                throw new IllegalArgumentException(sb.toString());
            } catch (IllegalArgumentException e) {
                String format = String.format(Locale.US, "%02X ", Byte.valueOf(b));
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e);
                sb2.append(" at ");
                sb2.append(position);
                sb2.append(", tag: ");
                sb2.append(format);
                throw new AnonymousClass2CC(sb2.toString());
            }
        } else {
            try {
                throw new IllegalArgumentException("Invalid record type");
            } catch (IllegalArgumentException e2) {
                String format2 = String.format(Locale.US, "%02X ", Byte.valueOf(b));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(e2);
                sb3.append(" at ");
                sb3.append(position);
                sb3.append(", tag: ");
                sb3.append(format2);
                throw new AnonymousClass2CC(sb3.toString());
            }
        }
    }

    public static String A06(ByteBuffer byteBuffer, int i) {
        byte[] bArr = new byte[i];
        byteBuffer.get(bArr);
        try {
            return new String(bArr, AnonymousClass01V.A08);
        } catch (UnsupportedEncodingException e) {
            StringBuilder sb = new StringBuilder("UnsupportedEncoding: ");
            sb.append(e);
            throw new AnonymousClass2CC(sb.toString());
        }
    }

    public void A07() {
        this.A02.reset();
        this.A00 = -1;
        this.A01 = 0;
    }

    public final void A08(byte b) {
        this.A02.A01()[this.A00] = b;
    }

    public final void A09(Object obj, int i, int i2) {
        int i3;
        C47602Bq r4 = this.A02;
        this.A00 = r4.size();
        r4.write(0);
        int A02 = A02(r4, (long) i2);
        if (A02 == 1) {
            i3 = 0;
        } else if (A02 == 2) {
            i3 = 1;
        } else {
            throw new Error("Id too big to fit in 2 bytes");
        }
        A08((byte) (i | (i3 << 3) | (A03(r4, obj) << 4)));
        this.A01++;
    }
}
