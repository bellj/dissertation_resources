package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.2kT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56182kT extends AbstractC64703Go {
    public final Map A00 = new HashMap(4);

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(this.A00);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            String valueOf = String.valueOf(A15.getKey());
            StringBuilder A0t = C12980iv.A0t(valueOf.length() + 9);
            A0t.append("dimension");
            C12990iw.A1Q(C12960it.A0d(valueOf, A0t), A11, A15);
        }
        return AbstractC64703Go.A00(A11, 0);
    }
}
