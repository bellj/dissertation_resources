package X;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.community.SubgroupPileView;

/* renamed from: X.1PZ */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1PZ extends C28791Pa {
    public View A00;
    public View A01;
    public ViewGroup A02;
    public ViewGroup A03;
    public ViewStub A04;
    public ImageView A05;
    public ProgressBar A06;
    public TextView A07;
    public TextView A08;
    public RunnableBRunnable0Shape0S0400000_I0 A09;
    public C28801Pb A0A;
    public C28801Pb A0B;
    public WaImageView A0C;
    public SubgroupPileView A0D;
    public AnonymousClass381 A0E;
    public C15370n3 A0F;
    public boolean A0G = false;
    public final ActivityC000800j A0H;
    public final C253519b A0I;
    public final C14900mE A0J;
    public final AnonymousClass2TT A0K;
    public final AnonymousClass2Cu A0L = new C84373zC(this);
    public final AnonymousClass10A A0M;
    public final AnonymousClass2Dn A0N = new C851241g(this);
    public final C22330yu A0O;
    public final AnonymousClass130 A0P;
    public final C27131Gd A0Q = new C36481jx(this);
    public final AnonymousClass10S A0R;
    public final C15610nY A0S;
    public final AnonymousClass1J1 A0T;
    public final AnonymousClass131 A0U;
    public final AnonymousClass018 A0V;
    public final C17720rH A0W;
    public final C19990v2 A0X;
    public final C20830wO A0Y;
    public final C15600nX A0Z;
    public final AnonymousClass19M A0a;
    public final C14850m9 A0b;
    public final C20710wC A0c;
    public final AbstractC33331dp A0d = new C857644b(this);
    public final C244215l A0e;
    public final AbstractC14640lm A0f;
    public final AnonymousClass12F A0g;
    public final AbstractC14440lR A0h;

    public AnonymousClass1PZ(ActivityC000800j r2, C253519b r3, C14900mE r4, AnonymousClass2TT r5, AnonymousClass10A r6, C22330yu r7, AnonymousClass130 r8, AnonymousClass10S r9, C15610nY r10, AnonymousClass1J1 r11, AnonymousClass131 r12, AnonymousClass018 r13, C17720rH r14, C19990v2 r15, C20830wO r16, C15600nX r17, C15370n3 r18, AnonymousClass19M r19, C14850m9 r20, C20710wC r21, C244215l r22, AbstractC14640lm r23, AnonymousClass12F r24, AbstractC14440lR r25) {
        this.A0H = r2;
        this.A0b = r20;
        this.A0J = r4;
        this.A0h = r25;
        this.A0X = r15;
        this.A0a = r19;
        this.A0P = r8;
        this.A0I = r3;
        this.A0S = r10;
        this.A0V = r13;
        this.A0K = r5;
        this.A0R = r9;
        this.A0c = r21;
        this.A0T = r11;
        this.A0g = r24;
        this.A0O = r7;
        this.A0M = r6;
        this.A0Z = r17;
        this.A0U = r12;
        this.A0Y = r16;
        this.A0e = r22;
        this.A0f = r23;
        this.A0F = r18;
        this.A0W = r14;
    }

    public static /* synthetic */ boolean A00(AnonymousClass1PZ r0, AbstractC14640lm r1) {
        return r1 != null && r1.equals(r0.A0f);
    }

    public ViewGroup A01(Context context) {
        return (ViewGroup) LayoutInflater.from(context).inflate(R.layout.conversation_actionbar, (ViewGroup) null, false);
    }

    public void A02() {
        C15370n3 A01 = this.A0Y.A01(this.A0f);
        this.A0F = A01;
        this.A0B.A06(A01);
        TextView textView = (TextView) this.A02.findViewById(R.id.conversation_contact_name);
        WaImageView waImageView = this.A0C;
        if (waImageView != null && waImageView.getVisibility() == 0 && textView != null && !TextUtils.isEmpty(textView.getText())) {
            textView.setContentDescription(textView.getContext().getString(R.string.tb_ephemeral_chat_has_disappearing_messages_on, textView.getText()));
        }
        AnonymousClass381 r0 = this.A0E;
        if (r0 != null) {
            r0.A03(true);
        }
        C15580nU A02 = C15580nU.A02(this.A0F.A0D);
        C19990v2 r1 = this.A0X;
        int A022 = r1.A02(A02);
        if (SubgroupPileView.A00(r1, this.A0Z, A02)) {
            SubgroupPileView subgroupPileView = this.A0D;
            int i = R.drawable.subgroup_facepile_circle_bottom_titlebar;
            int i2 = R.drawable.subgroup_facepile_circle_middle_titlebar;
            if (A022 == 3) {
                i = R.drawable.subgroup_facepile_circle_bottom_cag_titlebar;
                i2 = R.drawable.subgroup_facepile_circle_middle_cag_titlebar;
            }
            Context context = subgroupPileView.getContext();
            Drawable A04 = AnonymousClass00T.A04(context, i);
            AnonymousClass009.A05(A04);
            subgroupPileView.A00.setImageDrawable(A04);
            Drawable A042 = AnonymousClass00T.A04(context, i2);
            AnonymousClass009.A05(A042);
            subgroupPileView.A01.setImageDrawable(A042);
            this.A0D.setSubgroupProfilePhoto(this.A0F, this.A0T);
            this.A0D.setVisibility(0);
            View view = (View) this.A0D.getParent();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = -2;
            view.setLayoutParams(layoutParams);
            this.A05.setVisibility(8);
        } else {
            this.A0D.setVisibility(8);
            this.A05.setVisibility(0);
            AnonymousClass381 r4 = new AnonymousClass381(this.A05, this.A0P, this.A0U, this.A0F, this.A0c);
            this.A0E = r4;
            this.A0h.Aaz(r4, new Void[0]);
        }
        A03();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r10.A0K(r9.A0F) != false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 546
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1PZ.A03():void");
    }

    public void A04(Activity activity) {
        ActivityC000800j r3 = this.A0H;
        AbstractC005102i A1U = r3.A1U();
        AnonymousClass009.A05(A1U);
        ViewGroup A01 = A01(A1U.A02());
        this.A02 = A01;
        View A0D = AnonymousClass028.A0D(A01, R.id.back);
        AnonymousClass23N.A01(A0D);
        int i = Build.VERSION.SDK_INT;
        if (i > 21) {
            int paddingLeft = A0D.getPaddingLeft();
            int paddingRight = A0D.getPaddingRight();
            AnonymousClass018 r4 = this.A0V;
            AbstractC005102i A1U2 = r3.A1U();
            AnonymousClass009.A05(A1U2);
            A0D.setBackground(new AnonymousClass2GF(AnonymousClass00T.A04(A1U2.A02(), R.drawable.conversation_navigate_up_background), r4));
            C42941w9.A08(A0D, r4, paddingLeft, paddingRight);
        }
        A0D.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 18, activity));
        this.A03 = (ViewGroup) this.A02.findViewById(R.id.conversation_contact);
        this.A0C = (WaImageView) AnonymousClass028.A0D(this.A02, R.id.ephemeral_status);
        ViewGroup viewGroup = this.A03;
        C15610nY r6 = this.A0S;
        AnonymousClass12F r5 = this.A0g;
        this.A0B = new C28801Pb(viewGroup, r6, r5, (int) R.id.conversation_contact_name);
        View findViewById = this.A03.findViewById(R.id.conversation_contact_status_holder);
        this.A01 = findViewById;
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC42691vf(this));
        this.A00 = this.A03.findViewById(R.id.business_holder);
        this.A08 = (TextView) this.A03.findViewById(R.id.conversation_contact_status);
        this.A0A = new C28801Pb(this.A01, r6, r5, (int) R.id.business_name);
        this.A07 = (TextView) this.A03.findViewById(R.id.business_separator);
        this.A05 = (ImageView) this.A02.findViewById(R.id.conversation_contact_photo);
        this.A0D = (SubgroupPileView) this.A02.findViewById(R.id.subgroup_facepile_toolbar_photo);
        if (this.A0V.A04().A06 && i < 19) {
            this.A03.setLayoutTransition(null);
        }
        this.A03.setClickable(true);
        this.A04 = (ViewStub) this.A02.findViewById(R.id.change_photo_progress_stub);
        AbstractC005102i A1U3 = r3.A1U();
        AnonymousClass009.A05(A1U3);
        A1U3.A0N(true);
        AbstractC005102i A1U4 = r3.A1U();
        AnonymousClass009.A05(A1U4);
        A1U4.A0F(this.A02);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        A04(activity);
        this.A0R.A03(this.A0Q);
        this.A0O.A03(this.A0N);
        this.A0M.A03(this.A0L);
        this.A0e.A03(this.A0d);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        AnonymousClass381 r1 = this.A0E;
        if (r1 != null) {
            r1.A03(true);
            this.A0E = null;
        }
        this.A0R.A04(this.A0Q);
        this.A0O.A04(this.A0N);
        this.A0M.A04(this.A0L);
        this.A0e.A04(this.A0d);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        A02();
        this.A08.setSelected(true);
    }
}
