package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57252mj extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57252mj A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01 = 0;
    public int A02;
    public Object A03;

    static {
        C57252mj r0 = new C57252mj();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0065, code lost:
        if (r8.A01 == 2) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006a, code lost:
        if (r8.A01 == 1) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006f, code lost:
        if (r8.A01 == 3) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0071, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0072, code lost:
        r8.A03 = r10.Afv(r8.A03, r11.A03, r5);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r9, java.lang.Object r10, java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 384
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57252mj.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.A01 == 1) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A03, 1, 0);
        }
        if (this.A01 == 2) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A03, 2, i2);
        }
        if (this.A01 == 3) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A03, 3, i2);
        }
        if ((this.A00 & 8) == 8) {
            i2 = AbstractC27091Fz.A02(4, this.A02, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A03, 1);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A03, 2);
        }
        if (this.A01 == 3) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
