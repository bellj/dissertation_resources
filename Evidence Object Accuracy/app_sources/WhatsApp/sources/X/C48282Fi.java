package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48282Fi extends AnonymousClass019 {
    public final List A00 = new ArrayList();

    public C48282Fi(AnonymousClass01F r2) {
        super(r2, 1);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        return (AnonymousClass01E) this.A00.get(i);
    }
}
