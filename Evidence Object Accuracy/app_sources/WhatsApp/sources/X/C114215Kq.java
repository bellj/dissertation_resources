package X;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.5Kq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114215Kq extends AnonymousClass5LK implements AnonymousClass5WO, AnonymousClass5VE {
    public static final /* synthetic */ AtomicIntegerFieldUpdater A03 = AtomicIntegerFieldUpdater.newUpdater(C114215Kq.class, "_decision");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A04 = AtomicReferenceFieldUpdater.newUpdater(C114215Kq.class, Object.class, "_state");
    public AnonymousClass5VG A00;
    public final AnonymousClass5WO A01;
    public final AnonymousClass5X4 A02;
    public volatile /* synthetic */ int _decision = 0;
    public volatile /* synthetic */ Object _state = AnonymousClass5F4.A00;

    public C114215Kq(AnonymousClass5WO r3, int i) {
        super(i);
        this.A01 = r3;
        this.A02 = r3.ABi();
    }

    public static final void A00(Object obj, AnonymousClass1J7 r3) {
        StringBuilder A0k = C12960it.A0k("It's prohibited to register multiple handlers, tried to register ");
        A0k.append(r3);
        throw C12960it.A0U(C12960it.A0Z(obj, ", already has ", A0k));
    }

    public static final void A01(AnonymousClass5WO r5, AnonymousClass5LK r6, boolean z) {
        Object obj;
        if (!(r6 instanceof C114205Kp)) {
            obj = ((C114215Kq) r6)._state;
        } else {
            C114205Kp r1 = (C114205Kp) r6;
            obj = r1.A00;
            r1.A00 = AnonymousClass4HG.A01;
        }
        Throwable A02 = r6.A02(obj);
        if (A02 != null) {
            obj = new AnonymousClass5BR(A02);
        } else if ((r6 instanceof C114215Kq) && (obj instanceof C92844Xq)) {
            obj = ((C92844Xq) obj).A01;
        }
        if (z) {
            C114205Kp r52 = (C114205Kp) r5;
            AnonymousClass5WO r3 = r52.A02;
            Object obj2 = r52.A01;
            AnonymousClass5X4 ABi = r3.ABi();
            Object A00 = C94704cP.A00(obj2, ABi);
            if (A00 != C94704cP.A03) {
                C88224Et.A00(r3, ABi);
            }
            try {
                r3.Aas(obj);
            } finally {
                C94704cP.A01(A00, ABi);
            }
        } else {
            r5.Aas(obj);
        }
    }

    @Override // X.AnonymousClass5LK
    public Throwable A02(Object obj) {
        Throwable A02 = super.A02(obj);
        if (A02 == null) {
            return null;
        }
        return A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (((X.C114205Kp) r4.A01)._reusableCancellableContinuation == null) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A03() {
        /*
            r4 = this;
            int r1 = r4.A00
            r0 = 2
            if (r1 != r0) goto L_0x000e
            X.5WO r0 = r4.A01
            X.5Kp r0 = (X.C114205Kp) r0
            java.lang.Object r0 = r0._reusableCancellableContinuation
            r3 = 1
            if (r0 != 0) goto L_0x000f
        L_0x000e:
            r3 = 0
        L_0x000f:
            int r1 = r4._decision
            r2 = 0
            if (r1 == 0) goto L_0x0052
            r0 = 2
            if (r1 != r0) goto L_0x007f
            if (r3 == 0) goto L_0x001c
            r4.A06()
        L_0x001c:
            java.lang.Object r2 = r4._state
            boolean r0 = r2 instanceof X.C94894ci
            if (r0 == 0) goto L_0x0027
            X.4ci r2 = (X.C94894ci) r2
            java.lang.Throwable r0 = r2.A00
            throw r0
        L_0x0027:
            int r1 = r4.A00
            r0 = 1
            if (r1 == r0) goto L_0x002f
            r0 = 2
            if (r1 != r0) goto L_0x0049
        L_0x002f:
            X.5X4 r1 = r4.A02
            X.5Ep r0 = X.AbstractC02760Dw.A00
            X.5ZU r1 = r1.get(r0)
            X.0Dw r1 = (X.AbstractC02760Dw) r1
            if (r1 == 0) goto L_0x0049
            boolean r0 = r1.AJD()
            if (r0 != 0) goto L_0x0049
            java.util.concurrent.CancellationException r0 = r1.ABF()
            r4.A00(r0)
            throw r0
        L_0x0049:
            boolean r0 = r2 instanceof X.C92844Xq
            if (r0 == 0) goto L_0x007e
            X.4Xq r2 = (X.C92844Xq) r2
            java.lang.Object r2 = r2.A01
            return r2
        L_0x0052:
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r1 = X.C114215Kq.A03
            r0 = 1
            boolean r0 = r1.compareAndSet(r4, r2, r0)
            if (r0 == 0) goto L_0x000f
            X.5VG r0 = r4.A00
            if (r0 != 0) goto L_0x0077
            X.5X4 r1 = r4.A02
            X.5Ep r0 = X.AbstractC02760Dw.A00
            X.5ZU r2 = r1.get(r0)
            X.0Dw r2 = (X.AbstractC02760Dw) r2
            if (r2 == 0) goto L_0x0077
            X.5Kw r1 = new X.5Kw
            r1.<init>(r4)
            r0 = 1
            X.5VG r0 = r2.AJB(r1, r0, r0)
            r4.A00 = r0
        L_0x0077:
            if (r3 == 0) goto L_0x007c
            r4.A06()
        L_0x007c:
            X.4A6 r2 = X.AnonymousClass4A6.A01
        L_0x007e:
            return r2
        L_0x007f:
            java.lang.String r0 = "Already suspended"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C114215Kq.A03():java.lang.Object");
    }

    public final void A04() {
        AnonymousClass5VG r0 = this.A00;
        if (r0 != null) {
            r0.dispose();
            this.A00 = AnonymousClass5F0.A00;
        }
    }

    public final void A05() {
        if (((AnonymousClass5LK) this).A00 != 2 || ((C114205Kp) this.A01)._reusableCancellableContinuation == null) {
            A04();
        }
    }

    public final void A06() {
        C114205Kp r4;
        AnonymousClass4VA r2;
        AnonymousClass5WO r42 = this.A01;
        if ((r42 instanceof C114205Kp) && (r4 = (C114205Kp) r42) != null) {
            do {
                Object obj = r4._reusableCancellableContinuation;
                r2 = AnonymousClass4HG.A00;
                if (obj != r2) {
                    if (!(obj instanceof Throwable)) {
                        throw C12960it.A0U(C16700pc.A08("Inconsistent state ", obj));
                    } else if (AnonymousClass0KN.A00(r4, obj, null, C114205Kp.A04)) {
                        Throwable th = (Throwable) obj;
                        if (th != null) {
                            A04();
                            A0A(th);
                            return;
                        }
                        return;
                    } else {
                        throw C12970iu.A0f("Failed requirement.");
                    }
                }
            } while (!AnonymousClass0KN.A00(r4, r2, this, C114205Kp.A04));
        }
    }

    /* JADX INFO: finally extract failed */
    public final void A07(int i) {
        do {
            int i2 = this._decision;
            boolean z = true;
            if (i2 != 0) {
                if (i2 == 1) {
                    boolean z2 = true;
                    AnonymousClass5WO r6 = this.A01;
                    if (i != 4) {
                        z2 = false;
                        if (r6 instanceof C114205Kp) {
                            boolean z3 = true;
                            if (!(i == 1 || i == 2)) {
                                z3 = false;
                            }
                            int i3 = ((AnonymousClass5LK) this).A00;
                            if (!(i3 == 1 || i3 == 2)) {
                                z = false;
                            }
                            if (z3 == z) {
                                AbstractC10990fX r2 = ((C114205Kp) r6).A03;
                                AnonymousClass5X4 ABi = r6.ABi();
                                if (r2.A03(ABi)) {
                                    r2.A04(this, ABi);
                                    return;
                                }
                                AbstractC114165Kl A11 = C72453ed.A11();
                                long j = A11.A00;
                                if (j >= 4294967296L) {
                                    A11.A08(this);
                                    return;
                                }
                                A11.A00 = j + 4294967296L;
                                try {
                                    A01(r6, this, true);
                                    do {
                                    } while (A11.A0A());
                                } catch (Throwable th) {
                                    try {
                                        A01(th, null);
                                    } finally {
                                        A11.A06();
                                    }
                                }
                                return;
                            }
                        }
                    }
                    A01(r6, this, z2);
                    return;
                }
                throw C12960it.A0U("Already resumed");
            }
        } while (!A03.compareAndSet(this, 0, 2));
    }

    public void A08(Object obj) {
        A09(obj, ((AnonymousClass5LK) this).A00);
    }

    public final void A09(Object obj, int i) {
        Object obj2;
        Object obj3;
        do {
            obj2 = this._state;
            if (obj2 instanceof AbstractC115535Rx) {
                AbstractC115535Rx r9 = (AbstractC115535Rx) obj2;
                obj3 = obj;
                if (!(obj instanceof C94894ci) && ((i == 1 || i == 2) && (r9 instanceof AbstractC114125Kh) && !(r9 instanceof AbstractC114115Kg))) {
                    obj3 = new C92844Xq(obj, null, null, null, (AbstractC114125Kh) r9);
                }
            } else if (!(obj2 instanceof C114135Ki) || !C114135Ki.A00.compareAndSet(obj2, 0, 1)) {
                throw C12960it.A0U(C16700pc.A08("Already resumed, but proposed with update ", obj));
            } else {
                return;
            }
        } while (!AnonymousClass0KN.A00(this, obj2, obj3, A04));
        A05();
        A07(i);
    }

    public void A0A(Throwable th) {
        Object obj;
        boolean z;
        AbstractC114125Kh r3;
        do {
            obj = this._state;
            if (obj instanceof AbstractC115535Rx) {
                z = obj instanceof AbstractC114125Kh;
            } else {
                return;
            }
        } while (!AnonymousClass0KN.A00(this, obj, new C114135Ki(th, z), A04));
        if (z && (r3 = (AbstractC114125Kh) obj) != null) {
            A0D(r3);
        }
        A05();
        A07(((AnonymousClass5LK) this).A00);
    }

    public final void A0B(Throwable th, AnonymousClass1J7 r6) {
        try {
            r6.AJ4(th);
        } catch (Throwable th2) {
            C88234Eu.A00(this.A02, new C113295Gy(C16700pc.A08("Exception in invokeOnCancellation handler for ", this), th2));
        }
    }

    public void A0C(AnonymousClass1J7 r14) {
        boolean A00;
        AbstractC114125Kh r6 = (AbstractC114125Kh) r14;
        do {
            Object obj = this._state;
            if (obj instanceof AnonymousClass5F4) {
                A00 = AnonymousClass0KN.A00(this, obj, r6, A04);
                continue;
            } else if (obj instanceof AbstractC114125Kh) {
                A00(obj, r14);
                throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
            } else if (obj instanceof C94894ci) {
                C94894ci r1 = (C94894ci) obj;
                if (!r1.A00()) {
                    A00(obj, r14);
                    throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                } else if (obj instanceof C114135Ki) {
                    A0B(r1.A00, r14);
                    return;
                } else {
                    return;
                }
            } else if (obj instanceof C92844Xq) {
                C92844Xq r12 = (C92844Xq) obj;
                if (r12.A04 != null) {
                    A00(obj, r14);
                    throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                } else if (!(r6 instanceof AbstractC114115Kg)) {
                    Throwable th = r12.A02;
                    if (th != null) {
                        A0B(th, r14);
                        return;
                    }
                    A00 = AnonymousClass0KN.A00(this, obj, new C92844Xq(r12.A01, r12.A00, th, r12.A03, r6), A04);
                    continue;
                } else {
                    return;
                }
            } else if (!(r6 instanceof AbstractC114115Kg)) {
                A00 = AnonymousClass0KN.A00(this, obj, new C92844Xq(obj, null, null, null, r6), A04);
                continue;
            } else {
                return;
            }
        } while (!A00);
    }

    public final void A0D(AbstractC114125Kh r5) {
        try {
            ((C114105Kf) r5).A00.A06();
        } catch (Throwable th) {
            C88234Eu.A00(this.A02, new C113295Gy(C16700pc.A08("Exception in invokeOnCancellation handler for ", this), th));
        }
    }

    @Override // X.AnonymousClass5VE
    public AnonymousClass5VE ABA() {
        AnonymousClass5WO r1 = this.A01;
        if (r1 instanceof AnonymousClass5VE) {
            return (AnonymousClass5VE) r1;
        }
        return null;
    }

    @Override // X.AnonymousClass5WO
    public AnonymousClass5X4 ABi() {
        return this.A02;
    }

    @Override // X.AnonymousClass5WO
    public void Aas(Object obj) {
        Throwable A00 = AnonymousClass5BU.A00(obj);
        if (A00 != null) {
            obj = new C94894ci(A00, false);
        }
        A09(obj, ((AnonymousClass5LK) this).A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("CancellableContinuation");
        A0k.append('(');
        A0k.append(C88244Ev.A00(this.A01));
        A0k.append("){");
        Object obj = this._state;
        if (obj instanceof AbstractC115535Rx) {
            str = "Active";
        } else {
            str = obj instanceof C114135Ki ? "Cancelled" : "Completed";
        }
        A0k.append(str);
        A0k.append("}@");
        return C12960it.A0d(C72453ed.A0o(this), A0k);
    }
}
