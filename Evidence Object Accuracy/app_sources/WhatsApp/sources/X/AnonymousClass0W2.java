package X;

import android.view.InflateException;
import android.view.MenuItem;
import java.lang.reflect.Method;

/* renamed from: X.0W2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W2 implements MenuItem.OnMenuItemClickListener {
    public static final Class[] A02 = {MenuItem.class};
    public Object A00;
    public Method A01;

    public AnonymousClass0W2(Object obj, String str) {
        this.A00 = obj;
        Class<?> cls = obj.getClass();
        try {
            this.A01 = cls.getMethod(str, A02);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("Couldn't resolve menu item onClick handler ");
            sb.append(str);
            sb.append(" in class ");
            sb.append(cls.getName());
            InflateException inflateException = new InflateException(sb.toString());
            inflateException.initCause(e);
            throw inflateException;
        }
    }

    @Override // android.view.MenuItem.OnMenuItemClickListener
    public boolean onMenuItemClick(MenuItem menuItem) {
        try {
            Method method = this.A01;
            if (method.getReturnType() == Boolean.TYPE) {
                return ((Boolean) method.invoke(this.A00, menuItem)).booleanValue();
            }
            method.invoke(this.A00, menuItem);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
