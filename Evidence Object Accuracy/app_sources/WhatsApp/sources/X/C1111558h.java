package X;

/* renamed from: X.58h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1111558h implements AbstractC27731Iz {
    public final int A00;
    public final int A01;
    public final long A02;
    public final String A03;

    @Override // X.AbstractC27731Iz
    public int AHd() {
        return 0;
    }

    public C1111558h(String str, int i, int i2, long j) {
        this.A02 = j;
        this.A03 = str;
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // X.AbstractC27731Iz
    public boolean A9Y(AbstractC27731Iz r4) {
        if (r4 instanceof C1111558h) {
            C1111558h r42 = (C1111558h) r4;
            if (AnonymousClass1US.A0D(this.A03, r42.A03) && this.A00 == r42.A00 && this.A01 == r42.A01) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC27731Iz
    public long AGJ() {
        return this.A02;
    }
}
