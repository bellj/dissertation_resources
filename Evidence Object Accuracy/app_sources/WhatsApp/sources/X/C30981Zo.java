package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;

/* renamed from: X.1Zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30981Zo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99794ks();
    public HashMap A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30981Zo() {
        this.A00 = new HashMap();
    }

    public C30981Zo(Parcel parcel) {
        HashMap hashMap = (HashMap) parcel.readSerializable();
        this.A00 = hashMap == null ? new HashMap() : hashMap;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.A00);
    }
}
