package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A7  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A7 extends Enum {
    public static final /* synthetic */ AnonymousClass4A7[] A00;
    public static final AnonymousClass4A7 A01;
    public static final AnonymousClass4A7 A02;

    public static AnonymousClass4A7 valueOf(String str) {
        return (AnonymousClass4A7) Enum.valueOf(AnonymousClass4A7.class, str);
    }

    public static AnonymousClass4A7[] values() {
        return (AnonymousClass4A7[]) A00.clone();
    }

    static {
        AnonymousClass4A7 r4 = new AnonymousClass4A7("SUSPEND", 0);
        A02 = r4;
        AnonymousClass4A7 r3 = new AnonymousClass4A7("DROP_OLDEST", 1);
        A01 = r3;
        AnonymousClass4A7 r1 = new AnonymousClass4A7("DROP_LATEST", 2);
        AnonymousClass4A7[] r0 = new AnonymousClass4A7[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4A7(String str, int i) {
    }
}
