package X;

import android.view.View;

/* renamed from: X.2ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59532ut extends C37171lc {
    public final View.OnClickListener A00;
    public final String A01;

    public C59532ut(View.OnClickListener onClickListener, String str) {
        super(AnonymousClass39o.A0E);
        this.A01 = str;
        this.A00 = onClickListener;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A01.equals(((C59532ut) obj).A01);
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A01.hashCode();
    }
}
