package X;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import org.chromium.net.UrlRequest;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.14X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14X {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C22700zV A02;
    public final C15610nY A03;
    public final C14830m7 A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;
    public final C15600nX A07;
    public final AnonymousClass102 A08;
    public final C14850m9 A09;
    public final C17900ra A0A;
    public final C22710zW A0B;
    public final C17070qD A0C;
    public final C30931Zj A0D = C30931Zj.A00("PaymentsUtils", "infra", "COMMON");

    public AnonymousClass14X(C15570nT r4, C15550nR r5, C22700zV r6, C15610nY r7, C14830m7 r8, C16590pI r9, AnonymousClass018 r10, C15600nX r11, AnonymousClass102 r12, C14850m9 r13, C17900ra r14, C22710zW r15, C17070qD r16) {
        this.A05 = r9;
        this.A04 = r8;
        this.A09 = r13;
        this.A00 = r4;
        this.A01 = r5;
        this.A03 = r7;
        this.A06 = r10;
        this.A0C = r16;
        this.A02 = r6;
        this.A0B = r15;
        this.A08 = r12;
        this.A0A = r14;
        this.A07 = r11;
    }

    public static int A00(C17930rd r5) {
        AbstractC30791Yv r1;
        if (r5 != null) {
            LinkedHashSet linkedHashSet = r5.A05;
            Iterator it = linkedHashSet.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                AbstractC30791Yv r12 = (AbstractC30791Yv) it.next();
                if (((AbstractC30781Yu) r12).A00 == 0) {
                    if (r12 != null) {
                        Iterator it2 = linkedHashSet.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                r1 = null;
                                break;
                            }
                            r1 = (AbstractC30791Yv) it2.next();
                            if (((AbstractC30781Yu) r1).A00 == 0) {
                                break;
                            }
                        }
                        String str = ((AbstractC30781Yu) r1).A04;
                        switch (str.hashCode()) {
                            case 66044:
                                if (str.equals("BRL")) {
                                    return R.drawable.ic_attachment_payment_brl;
                                }
                                break;
                            case 70916:
                                if (str.equals("GTQ")) {
                                    return R.drawable.ic_attachment_payment_gtq;
                                }
                                break;
                            case 72653:
                                if (str.equals("INR")) {
                                    return R.drawable.ic_attachment_payment_inr;
                                }
                                break;
                            case 84326:
                                if (str.equals("USD")) {
                                    return R.drawable.ic_attachment_payment_usd;
                                }
                                break;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public static int A01(AnonymousClass1IR r0) {
        int i = r0.A02;
        switch (i) {
            case 11:
            case 12:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 18:
            case 19:
            case C43951xu.A01 /* 20 */:
                return R.color.payments_status_gray;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return R.color.payments_status_red;
            case 17:
                return R.color.payments_status_green;
            default:
                switch (i) {
                    case 101:
                    case 102:
                    case 103:
                    case 104:
                    case 109:
                    case 112:
                        return R.color.payments_status_gray;
                    case 105:
                    case 107:
                    case C43951xu.A03 /* 108 */:
                    case 110:
                    case 111:
                        return R.color.payments_status_red;
                    case 106:
                        return R.color.payments_status_green;
                    default:
                        switch (i) {
                            case 401:
                            case 402:
                            case 403:
                            case 410:
                            case 415:
                            case 417:
                            case 418:
                                return R.color.payments_status_gray;
                            case 404:
                            case 406:
                            case 407:
                            case 408:
                            case 409:
                            case 411:
                            case 412:
                            case 413:
                            case 414:
                            case 416:
                                return R.color.payments_status_red;
                            case 405:
                                return R.color.payments_status_green;
                            default:
                                switch (i) {
                                    case 420:
                                    case 421:
                                        return R.color.payments_status_gray;
                                    case 422:
                                    case 423:
                                    case 424:
                                        return R.color.payments_status_red;
                                    default:
                                        switch (i) {
                                            case 601:
                                            case 602:
                                            case 603:
                                            case 606:
                                            case 607:
                                            case 608:
                                                return R.color.payments_status_gray;
                                            case 604:
                                                return R.color.payments_status_green;
                                            case 605:
                                                return R.color.payments_status_red;
                                            default:
                                                switch (i) {
                                                    case 703:
                                                        return R.color.payments_status_green;
                                                    case 704:
                                                        return R.color.payments_status_red;
                                                    default:
                                                        return R.color.payments_status_gray;
                                                }
                                        }
                                }
                        }
                }
        }
    }

    public static SpannableStringBuilder A02(Context context, AnonymousClass018 r7, AbstractC30791Yv r8, C30821Yy r9) {
        return A03(context, r7, r8, r9, 0, true);
    }

    public static SpannableStringBuilder A03(Context context, AnonymousClass018 r2, AbstractC30791Yv r3, C30821Yy r4, int i, boolean z) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A05(r2, r3, r4, i, z));
        if (AnonymousClass1Z4.A00 == null) {
            try {
                AnonymousClass1Z4.A00 = AnonymousClass00X.A02(context);
            } catch (Resources.NotFoundException unused) {
                Log.e("PAY: PaymentsTypeface/loadTypefaceSync could not load font R.font.payment_icons_regular");
            }
        }
        Map map = AnonymousClass1Z4.A01;
        Iterator it = map.keySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String str = (String) it.next();
            if (map.get(str) != null) {
                String str2 = (String) map.get(str);
                if (str2 != null && str != null) {
                    if (AnonymousClass1Z4.A00 != null) {
                        int indexOf = spannableStringBuilder.toString().indexOf(str);
                        if (indexOf >= 0) {
                            spannableStringBuilder.replace(indexOf, str.length() + indexOf, (CharSequence) str2);
                            spannableStringBuilder.setSpan(new AnonymousClass1Z3(AnonymousClass1Z4.A00), indexOf, indexOf + 1, 0);
                        }
                    } else {
                        Log.e("PAY: PaymentsTypeface/format Could not load payment_icons_regular typeface, call loadTypeface() before applying font.");
                        return spannableStringBuilder;
                    }
                }
            }
        }
        return spannableStringBuilder;
    }

    public static CharSequence A04(Context context, AnonymousClass1IR r3) {
        AbstractC30791Yv A00 = r3.A00();
        return A00 != C30771Yt.A06 ? A00.ABz(context, r3.A0F() ? 1 : 0) : "";
    }

    public static String A05(AnonymousClass018 r3, AbstractC30791Yv r4, C30821Yy r5, int i, boolean z) {
        String AAA = r4.AAA(r3, r5, i);
        String AA8 = r4.AA8(r3, r5);
        BigDecimal bigDecimal = r5.A00;
        int scale = bigDecimal.scale();
        StringBuilder sb = new StringBuilder(AAA);
        int indexOf = AAA.indexOf(AA8);
        int length = AA8.length();
        int i2 = scale + 1;
        if (scale <= 0) {
            i2 = 0;
        }
        int i3 = (length - i2) + indexOf;
        int i4 = indexOf + length;
        if ((bigDecimal.signum() == 0 || bigDecimal.scale() <= 0 || bigDecimal.stripTrailingZeros().scale() <= 0) && z) {
            sb.delete(i3, i4);
        }
        return sb.toString();
    }

    public static String A06(C30821Yy r2, String str) {
        if (r2 == null) {
            return "";
        }
        return TextUtils.join(";", Arrays.asList(str, Long.toString(r2.A00.scaleByPowerOfTen(3).longValue())));
    }

    public static boolean A07(AnonymousClass1IR r2) {
        int i = r2.A02;
        return i == 405 || i == 106 || i == 604;
    }

    public int A08(AbstractC14640lm r7) {
        C22710zW r3 = this.A0B;
        if (!r3.A03()) {
            return 0;
        }
        C15570nT r4 = this.A00;
        if (r4.A0F(r7) || C15380n4.A0F(r7)) {
            return 0;
        }
        if (!C15380n4.A0J(r7)) {
            return r3.A00(UserJid.of(r7));
        }
        if (r3.A04()) {
            return 5;
        }
        r3.A06.A01();
        int i = 4;
        Iterator it = this.A07.A02((AbstractC15590nW) r7).A07().iterator();
        while (it.hasNext()) {
            UserJid userJid = ((AnonymousClass1YO) it.next()).A03;
            if (!r4.A0F(userJid)) {
                i = 3;
                if (r3.A00(userJid) == 2) {
                    return 4;
                }
            }
        }
        return i;
    }

    public Pair A09(long j) {
        Integer valueOf;
        AnonymousClass018 r1;
        int i;
        if (j <= 0) {
            return null;
        }
        int i2 = (int) (j / 86400000);
        if (i2 > 0) {
            valueOf = Integer.valueOf(i2);
            r1 = this.A06;
            i = 3;
        } else {
            i2 = (int) (j / 3600000);
            if (i2 > 0) {
                valueOf = Integer.valueOf(i2);
                r1 = this.A06;
                i = 2;
            } else {
                i2 = (int) (j / 60000);
                if (i2 <= 0) {
                    return null;
                }
                valueOf = Integer.valueOf(i2);
                r1 = this.A06;
                i = 1;
            }
        }
        return new Pair(valueOf, C38131nZ.A02(r1, i2, i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004f, code lost:
        if (r2 != false) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.Pair A0A(X.AnonymousClass1IR r9) {
        /*
            r8 = this;
            java.lang.String r7 = r8.A0N(r9)
            java.lang.String r6 = r8.A0O(r9)
            X.018 r0 = r8.A06
            java.lang.String r1 = r0.A06()
            java.lang.String r0 = "en"
            boolean r2 = r0.equals(r1)
            X.0nT r1 = r8.A00
            com.whatsapp.jid.UserJid r0 = r9.A0D
            boolean r0 = r1.A0F(r0)
            r5 = 0
            r4 = 1
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x0039
            if (r2 != 0) goto L_0x0039
            X.0pI r0 = r8.A05
            android.content.Context r2 = r0.A00
            r1 = 2131890554(0x7f12117a, float:1.9415803E38)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r5] = r7
        L_0x002f:
            java.lang.String r1 = r2.getString(r1, r0)
            android.util.Pair r0 = new android.util.Pair
            r0.<init>(r3, r1)
            return r0
        L_0x0039:
            com.whatsapp.jid.UserJid r0 = r9.A0E
            boolean r0 = r1.A0F(r0)
            if (r0 == 0) goto L_0x004f
            if (r2 != 0) goto L_0x0051
            X.0pI r0 = r8.A05
            android.content.Context r2 = r0.A00
            r1 = 2131890553(0x7f121179, float:1.9415801E38)
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r5] = r6
            goto L_0x002f
        L_0x004f:
            if (r2 == 0) goto L_0x0052
        L_0x0051:
            r3 = r6
        L_0x0052:
            X.0pI r0 = r8.A05
            android.content.Context r2 = r0.A00
            r1 = 2131890552(0x7f121178, float:1.94158E38)
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r5] = r6
            r0[r4] = r7
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass14X.A0A(X.1IR):android.util.Pair");
    }

    public C38151nb A0B(Context context, AbstractC14640lm r6, boolean z) {
        String string;
        AbstractC38141na AFL;
        String A08 = this.A03.A08(this.A01.A0B(r6));
        if (!this.A0B.A07() || (AFL = this.A0C.A02().AFL()) == null) {
            int i = R.string.payment_invite_status_text_inbound;
            if (z) {
                i = R.string.payment_invite_status_text_outbound;
            }
            string = context.getString(i, A08);
        } else {
            string = AFL.AFM(context, A08, z);
        }
        return new C38151nb(A08, string);
    }

    public CharSequence A0C(Context context, int i) {
        AbstractC30791Yv r0;
        if (i == 1 || i == 2) {
            C15570nT r02 = this.A00;
            r02.A08();
            C27631Ih r03 = r02.A05;
            AnonymousClass009.A05(r03);
            r0 = C17930rd.A00(C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(r03))).A03).A02;
        } else if (i != 3) {
            return null;
        } else {
            r0 = C30771Yt.A05;
        }
        return r0.ABy(context);
    }

    public CharSequence A0D(AnonymousClass1IR r6, CharSequence charSequence) {
        String A0J = A0J(r6);
        if (TextUtils.isEmpty(A0J)) {
            A0J = this.A05.A00.getString(R.string.transaction_detail_status_label);
        }
        return this.A05.A00.getString(R.string.transaction_detail_payment_amount_content_description, charSequence, A0J);
    }

    public Long A0E(AnonymousClass1IR r5) {
        AbstractC30891Zf r0 = r5.A0A;
        if (r0 == null) {
            return null;
        }
        return Long.valueOf(r0.A07() - this.A04.A00());
    }

    public String A0F(long j) {
        int i = 2;
        if (j <= 86400000) {
            i = 1;
        }
        return A0G(j, i);
    }

    public final String A0G(long j, int i) {
        AnonymousClass018 r2;
        String str = "";
        if (j > 0) {
            long j2 = 86400000;
            int i2 = (int) (j / 86400000);
            if (i2 > 0) {
                r2 = this.A06;
                str = C38131nZ.A02(r2, i2, 3);
            } else {
                j2 = 3600000;
                int i3 = (int) (j / 3600000);
                if (i3 > 0) {
                    r2 = this.A06;
                    str = C38131nZ.A02(r2, i3, 2);
                } else {
                    int i4 = (int) (j / 60000);
                    if (i4 > 0) {
                        return C38131nZ.A02(this.A06, i4, 1);
                    }
                }
            }
            long j3 = j % j2;
            if (!(i == 1 || j3 == 0)) {
                return r2.A0A(243, str, A0G(j3, i - 1));
            }
        }
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        if (r1 != 200) goto L_0x0056;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0H(X.AnonymousClass1IR r22) {
        /*
        // Method dump skipped, instructions count: 281
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass14X.A0H(X.1IR):java.lang.String");
    }

    public String A0I(AnonymousClass1IR r5) {
        if (C31001Zq.A08(r5) || TextUtils.isEmpty(r5.A0I) || r5.A08 == null) {
            return "";
        }
        return r5.A00().AAA(this.A06, r5.A08, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0055 A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0065  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0J(X.AnonymousClass1IR r5) {
        /*
            r4 = this;
            int r0 = r5.A02
            if (r0 == 0) goto L_0x0095
            switch(r0) {
                case 11: goto L_0x004d;
                case 12: goto L_0x0045;
                case 13: goto L_0x005d;
                case 14: goto L_0x005d;
                case 15: goto L_0x006d;
                case 16: goto L_0x003d;
                case 17: goto L_0x0055;
                case 18: goto L_0x0065;
                case 19: goto L_0x0080;
                case 20: goto L_0x0045;
                default: goto L_0x0007;
            }
        L_0x0007:
            switch(r0) {
                case 101: goto L_0x004d;
                case 102: goto L_0x0045;
                case 103: goto L_0x004d;
                case 104: goto L_0x004d;
                case 105: goto L_0x005d;
                case 106: goto L_0x0055;
                case 107: goto L_0x003d;
                case 108: goto L_0x005d;
                case 109: goto L_0x004d;
                case 110: goto L_0x0045;
                case 111: goto L_0x005d;
                case 112: goto L_0x0065;
                default: goto L_0x000a;
            }
        L_0x000a:
            switch(r0) {
                case 401: goto L_0x004d;
                case 402: goto L_0x0045;
                case 403: goto L_0x004d;
                case 404: goto L_0x005d;
                case 405: goto L_0x0055;
                case 406: goto L_0x005d;
                case 407: goto L_0x005d;
                case 408: goto L_0x005d;
                case 409: goto L_0x005d;
                case 410: goto L_0x004d;
                case 411: goto L_0x005d;
                case 412: goto L_0x005d;
                case 413: goto L_0x005d;
                case 414: goto L_0x005d;
                case 415: goto L_0x0065;
                case 416: goto L_0x003d;
                case 417: goto L_0x0075;
                case 418: goto L_0x0035;
                case 419: goto L_0x0045;
                case 420: goto L_0x004d;
                case 421: goto L_0x0065;
                case 422: goto L_0x005d;
                case 423: goto L_0x005d;
                case 424: goto L_0x005d;
                default: goto L_0x000d;
            }
        L_0x000d:
            switch(r0) {
                case 601: goto L_0x004d;
                case 602: goto L_0x004d;
                case 603: goto L_0x002d;
                case 604: goto L_0x0055;
                case 605: goto L_0x005d;
                case 606: goto L_0x0065;
                case 607: goto L_0x003d;
                case 608: goto L_0x0045;
                default: goto L_0x0010;
            }
        L_0x0010:
            switch(r0) {
                case 701: goto L_0x004d;
                case 702: goto L_0x002d;
                case 703: goto L_0x0055;
                case 704: goto L_0x005d;
                case 705: goto L_0x0065;
                default: goto L_0x0013;
            }
        L_0x0013:
            switch(r0) {
                case 801: goto L_0x004d;
                case 802: goto L_0x0055;
                case 803: goto L_0x004d;
                case 804: goto L_0x0055;
                default: goto L_0x0016;
            }
        L_0x0016:
            switch(r0) {
                case 901: goto L_0x0025;
                case 902: goto L_0x0055;
                case 903: goto L_0x0025;
                case 904: goto L_0x0055;
                default: goto L_0x0019;
            }
        L_0x0019:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890686(0x7f1211fe, float:1.941607E38)
        L_0x0020:
            java.lang.String r0 = r1.getString(r0)
            return r0
        L_0x0025:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890687(0x7f1211ff, float:1.9416073E38)
            goto L_0x0020
        L_0x002d:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890689(0x7f121201, float:1.9416077E38)
            goto L_0x0020
        L_0x0035:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890673(0x7f1211f1, float:1.9416044E38)
            goto L_0x0020
        L_0x003d:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890677(0x7f1211f5, float:1.9416053E38)
            goto L_0x0020
        L_0x0045:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890684(0x7f1211fc, float:1.9416067E38)
            goto L_0x0020
        L_0x004d:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890685(0x7f1211fd, float:1.9416069E38)
            goto L_0x0020
        L_0x0055:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890674(0x7f1211f2, float:1.9416046E38)
            goto L_0x0020
        L_0x005d:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890678(0x7f1211f6, float:1.9416055E38)
            goto L_0x0020
        L_0x0065:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890688(0x7f121200, float:1.9416075E38)
            goto L_0x0020
        L_0x006d:
            X.0pI r0 = r4.A05
            android.content.Context r1 = r0.A00
            r0 = 2131890561(0x7f121181, float:1.9415817E38)
            goto L_0x0020
        L_0x0075:
            X.0qD r0 = r4.A0C
            X.0pp r0 = r0.A02()
            java.lang.String r0 = r0.AGP(r5)
            return r0
        L_0x0080:
            X.0pI r0 = r4.A05
            android.content.Context r3 = r0.A00
            int r2 = r5.A03
            r1 = 10
            r0 = 2131890564(0x7f121184, float:1.9415823E38)
            if (r2 != r1) goto L_0x0090
            r0 = 2131890572(0x7f12118c, float:1.941584E38)
        L_0x0090:
            java.lang.String r0 = r3.getString(r0)
            return r0
        L_0x0095:
            java.lang.String r0 = ""
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass14X.A0J(X.1IR):java.lang.String");
    }

    public final String A0K(AnonymousClass1IR r6) {
        boolean A0F = this.A00.A0F(r6.A0D);
        C16590pI r0 = this.A05;
        if (A0F) {
            return r0.A00.getString(R.string.payment_data_localized_preview_to_me);
        }
        return r0.A00.getString(R.string.payment_data_localized_preview_to_other, A0M(r6));
    }

    public synchronized String A0L(AnonymousClass1IR r2) {
        return A0S(r2, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r3 != 1000) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String A0M(X.AnonymousClass1IR r8) {
        /*
            r7 = this;
            r6 = r7
            monitor-enter(r6)
            com.whatsapp.jid.UserJid r1 = r8.A0D     // Catch: all -> 0x008f
            if (r1 == 0) goto L_0x001d
            X.0nR r0 = r7.A01     // Catch: all -> 0x008f
            X.0n3 r5 = r0.A0B(r1)     // Catch: all -> 0x008f
        L_0x000c:
            X.0pI r0 = r7.A05     // Catch: all -> 0x008f
            android.content.Context r2 = r0.A00     // Catch: all -> 0x008f
            r0 = 2131892451(0x7f1218e3, float:1.941965E38)
            java.lang.String r4 = r2.getString(r0)     // Catch: all -> 0x008f
            int r3 = r8.A03     // Catch: all -> 0x008f
            r1 = 1
            if (r3 == r1) goto L_0x001f
            goto L_0x0029
        L_0x001d:
            r5 = 0
            goto L_0x000c
        L_0x001f:
            if (r5 == 0) goto L_0x0022
            goto L_0x0045
        L_0x0022:
            java.lang.String r0 = r7.A0R(r8, r1)     // Catch: all -> 0x008f
            if (r0 == 0) goto L_0x0043
            goto L_0x008d
        L_0x0029:
            r0 = 2
            if (r3 == r0) goto L_0x0086
            r0 = 3
            if (r3 == r0) goto L_0x004c
            r0 = 10
            if (r3 == r0) goto L_0x0086
            r0 = 20
            if (r3 == r0) goto L_0x001f
            r0 = 100
            if (r3 == r0) goto L_0x001f
            r0 = 200(0xc8, float:2.8E-43)
            if (r3 == r0) goto L_0x0086
            r0 = 1000(0x3e8, float:1.401E-42)
            if (r3 == r0) goto L_0x0055
        L_0x0043:
            monitor-exit(r6)
            return r4
        L_0x0045:
            X.0nY r0 = r7.A03     // Catch: all -> 0x008f
            java.lang.String r0 = r0.A08(r5)     // Catch: all -> 0x008f
            goto L_0x008d
        L_0x004c:
            if (r5 == 0) goto L_0x0055
            X.0nY r0 = r7.A03     // Catch: all -> 0x008f
            java.lang.String r0 = r0.A08(r5)     // Catch: all -> 0x008f
            goto L_0x008d
        L_0x0055:
            X.0nT r1 = r7.A00     // Catch: all -> 0x0084
            com.whatsapp.jid.UserJid r0 = r8.A0D     // Catch: all -> 0x0084
            boolean r0 = r1.A0F(r0)     // Catch: all -> 0x0084
            if (r0 == 0) goto L_0x0067
            r0 = 2131893184(0x7f121bc0, float:1.9421137E38)
            java.lang.String r0 = r2.getString(r0)     // Catch: all -> 0x0084
            goto L_0x008d
        L_0x0067:
            com.whatsapp.jid.UserJid r1 = r8.A0D     // Catch: all -> 0x0084
            if (r1 == 0) goto L_0x0072
            X.0nR r0 = r7.A01     // Catch: all -> 0x0084
            X.0n3 r1 = r0.A0B(r1)     // Catch: all -> 0x0084
            goto L_0x0073
        L_0x0072:
            r1 = 0
        L_0x0073:
            if (r1 == 0) goto L_0x007c
            X.0nY r0 = r7.A03     // Catch: all -> 0x0084
            java.lang.String r0 = r0.A08(r1)     // Catch: all -> 0x0084
            goto L_0x008d
        L_0x007c:
            r0 = 2131892451(0x7f1218e3, float:1.941965E38)
            java.lang.String r0 = r2.getString(r0)     // Catch: all -> 0x0084
            goto L_0x008d
        L_0x0084:
            r0 = move-exception
            throw r0     // Catch: all -> 0x008f
        L_0x0086:
            r0 = 2131893184(0x7f121bc0, float:1.9421137E38)
            java.lang.String r0 = r2.getString(r0)     // Catch: all -> 0x008f
        L_0x008d:
            monitor-exit(r6)
            return r0
        L_0x008f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass14X.A0M(X.1IR):java.lang.String");
    }

    public synchronized String A0N(AnonymousClass1IR r4) {
        String str;
        AnonymousClass009.A0F(r4.A0F());
        UserJid userJid = r4.A0E;
        C15370n3 A0B = userJid != null ? this.A01.A0B(userJid) : null;
        if (A0B == null) {
            str = A0R(r4, false);
            if (str == null) {
                str = this.A05.A00.getString(R.string.unknown);
            }
        } else if (this.A00.A0F(A0B.A0D)) {
            str = this.A05.A00.getString(R.string.you);
        } else {
            str = this.A03.A08(A0B);
        }
        return str;
    }

    public synchronized String A0O(AnonymousClass1IR r4) {
        String str;
        AnonymousClass009.A0F(r4.A0F());
        UserJid userJid = r4.A0D;
        C15370n3 A0B = userJid != null ? this.A01.A0B(userJid) : null;
        if (A0B == null) {
            str = A0R(r4, true);
            if (str == null) {
                str = this.A05.A00.getString(R.string.unknown);
            }
        } else if (this.A00.A0F(A0B.A0D)) {
            str = this.A05.A00.getString(R.string.you);
        } else {
            str = this.A03.A08(A0B);
        }
        return str;
    }

    public synchronized String A0P(AnonymousClass1IR r3) {
        String str;
        UserJid userJid = r3.A0E;
        C15370n3 A0B = userJid != null ? this.A01.A0B(userJid) : null;
        if (A0B != null) {
            str = this.A03.A08(A0B);
        } else {
            str = A0R(r3, false);
            if (str == null) {
                str = this.A05.A00.getString(R.string.unknown);
            }
        }
        return str;
    }

    public synchronized String A0Q(AnonymousClass1IR r8, AbstractC15340mz r9) {
        String str;
        C15550nR r1 = this.A01;
        UserJid A0C = r9.A0C();
        AnonymousClass009.A05(A0C);
        C15370n3 A0B = r1.A0B(A0C);
        if (r9 instanceof AnonymousClass1XY) {
            if (r8.A08 != null) {
                Context context = this.A05.A00;
                boolean z = r9.A0z.A02;
                int i = R.string.payment_system_event_my_request_declined;
                if (z) {
                    i = R.string.payment_system_event_request_declined;
                }
                str = context.getString(i, this.A03.A04(A0B), A0I(r8));
            } else {
                Context context2 = this.A05.A00;
                boolean z2 = r9.A0z.A02;
                int i2 = R.string.payment_system_event_my_request_declined_no_amount;
                if (z2) {
                    i2 = R.string.payment_system_event_request_declined_no_amount;
                }
                str = context2.getString(i2, this.A03.A04(A0B), A0I(r8));
            }
        } else if (!(r9 instanceof AnonymousClass1XW)) {
            throw new IllegalStateException(C30931Zj.A01("PaymentsUtils", "Request message is not cancelled or rejected"));
        } else if (r8.A08 != null) {
            Context context3 = this.A05.A00;
            boolean z3 = r9.A0z.A02;
            int i3 = R.string.payment_system_event_request_canceled;
            if (z3) {
                i3 = R.string.payment_system_event_request_canceled_by_me;
            }
            str = context3.getString(i3, this.A03.A04(A0B), A0I(r8));
        } else {
            Context context4 = this.A05.A00;
            boolean z4 = r9.A0z.A02;
            int i4 = R.string.payment_system_event_request_canceled_no_amount;
            if (z4) {
                i4 = R.string.payment_system_event_request_canceled_by_me_no_amount;
            }
            str = context4.getString(i4, this.A03.A04(A0B), A0I(r8));
        }
        return str;
    }

    public final String A0R(AnonymousClass1IR r5, boolean z) {
        AnonymousClass1ZR A0D;
        Object obj;
        boolean z2;
        if (r5.A0A == null) {
            return null;
        }
        C14850m9 r1 = this.A09;
        if (r1.A07(1611)) {
            AbstractC30891Zf r0 = r5.A0A;
            if (z) {
                A0D = r0.A0C();
            } else {
                A0D = r0.A0D();
            }
            if (A0D == null) {
                obj = null;
            } else {
                obj = A0D.A00;
            }
            String str = (String) obj;
            if (str != null) {
                if (!AnonymousClass1US.A0C(str)) {
                    try {
                        JSONArray jSONArray = new JSONArray(r1.A03(1940));
                        for (int i = 0; i < jSONArray.length(); i++) {
                            if (str.equalsIgnoreCase(jSONArray.getString(i))) {
                                z2 = false;
                                break;
                            }
                        }
                    } catch (JSONException unused) {
                        Log.e("PaymentsUtils failed to parse json in abprop");
                    }
                }
                z2 = true;
                if (z2) {
                    return str;
                }
            }
        }
        AbstractC30891Zf r02 = r5.A0A;
        if (z) {
            return r02.A0F();
        }
        return r02.A0G();
    }

    public synchronized String A0S(AnonymousClass1IR r5, boolean z) {
        String str;
        UserJid userJid;
        int i = r5.A03;
        if (i != 20) {
            if (i != 30) {
                if (!(i == 40 || i == 100)) {
                    if (i != 200) {
                        if (i != 1000) {
                            switch (i) {
                                case 1:
                                    break;
                                case 2:
                                case 10:
                                    break;
                                case 3:
                                case 4:
                                case 5:
                                    break;
                                case 6:
                                    str = this.A05.A00.getString(R.string.payment_transaction_type_deposit);
                                    break;
                                case 7:
                                    str = this.A05.A00.getString(this.A0C.A02().AFC());
                                    break;
                                case 8:
                                    str = this.A05.A00.getString(R.string.payment_transaction_type_withdrawal);
                                    break;
                                case 9:
                                    str = A0R(r5, false);
                                    if (str != null) {
                                        break;
                                    }
                                    str = this.A05.A00.getString(R.string.unknown);
                                    break;
                                default:
                                    str = this.A05.A00.getString(R.string.unknown);
                                    break;
                            }
                        }
                    }
                    UserJid userJid2 = r5.A0E;
                    if (userJid2 != null) {
                        C15370n3 A0B = this.A01.A0B(userJid2);
                        if (z) {
                            str = this.A03.A08(A0B);
                        } else {
                            str = this.A03.A04(A0B);
                        }
                    } else {
                        str = A0R(r5, false);
                        if (str != null) {
                        }
                        str = this.A05.A00.getString(R.string.unknown);
                    }
                }
            }
            UserJid userJid3 = r5.A0D;
            if (userJid3 == null || r5.A0E == null) {
                str = this.A05.A00.getString(R.string.unknown);
            } else {
                if (this.A00.A0F(userJid3)) {
                    userJid = r5.A0E;
                } else {
                    userJid = r5.A0D;
                }
                C15370n3 A0B2 = this.A01.A0B(userJid);
                if (z) {
                    str = this.A03.A08(A0B2);
                } else {
                    str = this.A03.A04(A0B2);
                }
            }
        }
        UserJid userJid4 = r5.A0D;
        if (userJid4 != null) {
            C15370n3 A0B3 = this.A01.A0B(userJid4);
            if (z) {
                str = this.A03.A08(A0B3);
            } else {
                str = this.A03.A04(A0B3);
            }
        } else {
            str = A0R(r5, true);
            if (str != null) {
            }
            str = this.A05.A00.getString(R.string.unknown);
        }
        return str;
    }

    public String A0T(AnonymousClass18M r7, AbstractC15340mz r8) {
        String str;
        Context context;
        int i;
        if (!C31001Zq.A08(r8.A0L)) {
            AnonymousClass1IR r3 = r8.A0L;
            int i2 = r3.A02;
            if (i2 != 12) {
                if (i2 != 102) {
                    if (i2 != 105) {
                        if (i2 != 402) {
                            if (i2 == 406 || i2 == 407) {
                                String str2 = r3.A0J;
                                if (!(str2 == null || r7 == null)) {
                                    int A00 = C28421Nd.A00(str2, -1);
                                    if (r7.AKB(A00) || r7.AJa(A00) || r7.AJe(A00)) {
                                        return r7.ACl(A00, this.A05.A00.getString(R.string.payments_message_bubble_status_label_desc_daily_limit_exceeded));
                                    }
                                    if (r7.AJZ(A00)) {
                                        Context context2 = this.A05.A00;
                                        return context2.getString(R.string.payments_transaction_incorrect_pin, context2.getString(this.A0C.A02().AFS()));
                                    } else if (r7.AJY(A00)) {
                                        return r7.ACl(A00, this.A05.A00.getString(R.string.payments_transaction_insufficient_balance));
                                    } else {
                                        if (r7.AJk(A00)) {
                                            Context context3 = this.A05.A00;
                                            return context3.getString(R.string.payments_transaction_max_pin_retries, context3.getString(this.A0C.A02().AFS()));
                                        }
                                    }
                                }
                            } else {
                                if (i2 != 420) {
                                    if (i2 != 421) {
                                        switch (i2) {
                                            case 109:
                                                context = this.A05.A00;
                                                i = R.string.transaction_status_receiver_withdrawal_processing;
                                                break;
                                            case 110:
                                                context = this.A05.A00;
                                                i = R.string.transaction_status_receiver_withdrawal_failure;
                                                break;
                                            case 111:
                                                context = this.A05.A00;
                                                i = R.string.transaction_status_receiver_withdrawal_permanent_failed;
                                                break;
                                        }
                                    }
                                    context = this.A05.A00;
                                    i = R.string.payments_message_bubble_status_label_user_canceled;
                                } else {
                                    context = this.A05.A00;
                                    i = R.string.transaction_status_pending_processing;
                                }
                                return context.getString(i);
                            }
                        }
                    }
                    String str3 = r3.A0J;
                    if (!(str3 == null || r7 == null || !r7.AJw(C28421Nd.A00(str3, -1)))) {
                        context = this.A05.A00;
                        i = R.string.transaction_status_incoming_payment_limit_reached;
                        return context.getString(i);
                    }
                }
                Long A0E = A0E(r3);
                if (A0E == null) {
                    return "";
                }
                long longValue = A0E.longValue();
                return A0X(A0F(longValue), longValue);
            }
            int i3 = r3.A03;
            if (i3 == 10 || i3 == 20) {
                Long A0E2 = A0E(r3);
                if (A0E2 == null) {
                    return "";
                }
                long longValue2 = A0E2.longValue();
                Pair A09 = A09(longValue2);
                if (A09 == null) {
                    str = null;
                } else {
                    str = (String) A09.second;
                }
                return A0X(str, longValue2);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0154, code lost:
        if (r17 == false) goto L_0x0156;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String A0U(X.AbstractC15340mz r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 652
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass14X.A0U(X.0mz, boolean):java.lang.String");
    }

    public String A0V(String str) {
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(";");
            if (split.length == 2) {
                AbstractC30791Yv A02 = this.A08.A02(split[0]);
                try {
                    return A02.AAA(this.A06, new C30821Yy(new BigDecimal(split[1]).movePointLeft(3), ((AbstractC30781Yu) A02).A01), 0);
                } catch (NumberFormatException unused) {
                    return "";
                }
            }
        }
        return "";
    }

    public final String A0W(String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, long j) {
        int i12 = i9;
        if (j <= 0) {
            return this.A05.A00.getString(i, str);
        }
        int A00 = C38121nY.A00(this.A04.A00(), j);
        if (A00 == 0) {
            return this.A05.A00.getString(i2, str);
        }
        if (A00 == 1) {
            return this.A05.A00.getString(i3, str);
        }
        if (A00 >= 7) {
            return this.A05.A00.getString(i11, str, AnonymousClass1MY.A00(this.A06, j));
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        switch (instance.get(7)) {
            case 1:
                i12 = i10;
                break;
            case 2:
                i12 = i4;
                break;
            case 3:
                i12 = i5;
                break;
            case 4:
                i12 = i6;
                break;
            case 5:
                i12 = i7;
                break;
            case 6:
                i12 = i8;
                break;
            case 7:
                break;
            default:
                i12 = 0;
                break;
        }
        return this.A05.A00.getString(i12, str);
    }

    public final String A0X(String str, long j) {
        if (!TextUtils.isEmpty(str)) {
            return this.A05.A00.getResources().getQuantityString(R.plurals.payments_message_bubble_status_label_desc_expiration, 1, str);
        }
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        Context context = this.A05.A00;
        int i2 = R.string.payments_message_bubble_status_label_desc_expiring;
        if (i > 0) {
            i2 = R.string.payments_message_bubble_status_label_desc_expiration_sub_minute;
        }
        return context.getString(i2);
    }

    public String A0Y(String str, String str2, String str3, int i, int i2, int i3, long j, long j2, boolean z) {
        int i4;
        Object[] objArr;
        if (z) {
            if (i == 406 || i == 407 || i == 412) {
                return A0W(str, R.string.payments_transaction_system_message_from_me_failed_no_timestamp, R.string.payments_transaction_system_message_from_me_failed_today, R.string.payments_transaction_system_message_from_me_failed_yesterday, R.string.payments_transaction_system_message_from_me_failed_on_monday, R.string.payments_transaction_system_message_from_me_failed_on_tuesday, R.string.payments_transaction_system_message_from_me_failed_on_wednesday, R.string.payments_transaction_system_message_from_me_failed_on_thursday, R.string.payments_transaction_system_message_from_me_failed_on_friday, R.string.payments_transaction_system_message_from_me_failed_on_saturday, R.string.payments_transaction_system_message_from_me_failed_on_sunday, R.string.payments_transaction_system_message_from_me_failed_day_and_month, j);
            }
            if (i == 408 || i == 404 || i == 411) {
                return A0W(str, R.string.payments_transaction_system_message_from_me_refunded_no_timestamp, R.string.payments_transaction_system_message_from_me_refunded_today, R.string.payments_transaction_system_message_from_me_refunded_yesterday, R.string.payments_transaction_system_message_from_me_refunded_on_monday, R.string.payments_transaction_system_message_from_me_refunded_on_tuesday, R.string.payments_transaction_system_message_from_me_refunded_on_wednesday, R.string.payments_transaction_system_message_from_me_refunded_on_thursday, R.string.payments_transaction_system_message_from_me_refunded_on_friday, R.string.payments_transaction_system_message_from_me_refunded_on_saturday, R.string.payments_transaction_system_message_from_me_refunded_on_sunday, R.string.payments_transaction_system_message_from_me_refunded_day_and_month, j2);
            }
            if (i == 409) {
                int i5 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                Context context = this.A05.A00;
                if (i5 > 0) {
                    i4 = R.string.payments_transaction_system_message_from_me_refunded_failure;
                    objArr = new Object[]{str, context.getString(R.string.transaction_timestamp_format, AnonymousClass1MY.A02(this.A06, j))};
                } else {
                    i4 = R.string.payments_transaction_system_message_from_me_refunded_failure_no_timestamp;
                    objArr = new Object[]{str};
                }
                return context.getString(i4, objArr);
            }
        } else if (i == 102) {
            return this.A05.A00.getResources().getQuantityString(R.plurals.payments_transaction_system_message_to_me_pending_setup, i3, str2, str3);
        } else {
            if (i == 106) {
                if (i2 == 104 || i2 == 103 || i2 == 102) {
                    return A0W(str2, R.string.payments_transaction_system_message_to_me_success_with_hiccup_no_timestamp, R.string.payments_transaction_system_message_to_me_success_with_hiccup_today, R.string.payments_transaction_system_message_to_me_success_with_hiccup_yesterday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_monday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_tuesday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_wednesday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_thursday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_friday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_saturday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_on_sunday, R.string.payments_transaction_system_message_to_me_success_with_hiccup_day_and_month, j);
                }
            } else if (i == 420) {
                return this.A05.A00.getString(R.string.transaction_status_pending_processing);
            }
        }
        return "";
    }

    public void A0Z(View view, C15370n3 r7) {
        C34271fr.A00(view, this.A05.A00.getString(R.string.payment_invite_sending_status_text_outbound, this.A03.A08(r7)), 0).A03();
    }

    public boolean A0a(Context context, UserJid userJid, int i) {
        AbstractC30791Yv A00;
        if (i == 0 || ((userJid != null && this.A02.A02(userJid)) || (A00 = this.A0A.A00()) == null || TextUtils.isEmpty(A00.ABy(context)))) {
            return false;
        }
        return true;
    }

    public boolean A0b(AnonymousClass1IR r5, AnonymousClass18M r6, AnonymousClass17T r7) {
        int i;
        int A00 = C28421Nd.A00(r5.A0J, -1);
        int i2 = r5.A03;
        if (i2 != 1 && i2 != 100) {
            return false;
        }
        AbstractC30891Zf r0 = r5.A0A;
        if ((r0 != null && r0.A02 != null && (!this.A0B.A05() || r7 == null)) || !this.A00.A0F(r5.A0E) || !r5.A0A() || (i = r5.A02) == 419 || i == 420 || i == 409 || i == 405 || i == 407 || i == 0 || A00 == 441 || A00 == 410 || A00 == 11455 || A00 == 2826008) {
            return false;
        }
        if (r6 == null || r6.AdY(A00)) {
            return true;
        }
        return false;
    }
}
