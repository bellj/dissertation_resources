package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.util.FloatingChildLayout;

/* renamed from: X.3f0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72683f0 extends AnimatorListenerAdapter {
    public final /* synthetic */ FloatingChildLayout A00;
    public final /* synthetic */ Runnable A01;
    public final /* synthetic */ boolean A02;

    public C72683f0(FloatingChildLayout floatingChildLayout, Runnable runnable, boolean z) {
        this.A00 = floatingChildLayout;
        this.A02 = z;
        this.A01 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        int i;
        FloatingChildLayout floatingChildLayout = this.A00;
        floatingChildLayout.A09.setLayerType(0, null);
        boolean z = this.A02;
        int i2 = floatingChildLayout.A03;
        if (z) {
            if (i2 == 3) {
                i = 4;
            } else {
                return;
            }
        } else if (i2 == 1) {
            i = 2;
        } else {
            return;
        }
        floatingChildLayout.A03 = i;
        Runnable runnable = this.A01;
        if (runnable != null) {
            runnable.run();
        }
    }
}
