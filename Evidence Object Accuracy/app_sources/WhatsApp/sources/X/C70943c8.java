package X;

import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.voipcalling.VideoPort;

/* renamed from: X.3c8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70943c8 implements AnonymousClass5X3 {
    public final /* synthetic */ C60112vx A00;

    public C70943c8(C60112vx r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5X3
    public void AOY(VideoPort videoPort) {
        C60112vx r2 = this.A00;
        AnonymousClass009.A05(((AbstractC55202hx) r2).A05);
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r2.A07);
        C12960it.A1N(A0h, "onConnected ", videoPort);
        A0h.append(((AbstractC55202hx) r2).A05);
        C12960it.A1F(A0h);
        if (r2.A04 == null) {
            videoPort.setCornerRadius((float) r2.A00);
        }
        AbstractC116665Wi r1 = r2.A05;
        if (r1 != null) {
            r1.AWM(((AbstractC55202hx) r2).A05, videoPort);
        }
    }

    @Override // X.AnonymousClass5X3
    public void APE(VideoPort videoPort) {
        C60112vx r2 = this.A00;
        AnonymousClass009.A05(((AbstractC55202hx) r2).A05);
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r2.A07);
        C12960it.A1N(A0h, "onDisconnecting ", videoPort);
        A0h.append(((AbstractC55202hx) r2).A05);
        C12960it.A1F(A0h);
        AbstractC116665Wi r1 = r2.A05;
        if (r1 != null) {
            r1.AWq(((AbstractC55202hx) r2).A05, videoPort);
        }
    }

    @Override // X.AnonymousClass5X3
    public void ATu(VideoPort videoPort) {
        C60112vx r2 = this.A00;
        AnonymousClass009.A05(((AbstractC55202hx) r2).A05);
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r2.A07);
        C12960it.A1N(A0h, "onPortWindowSizeChanged ", videoPort);
        A0h.append(((AbstractC55202hx) r2).A05);
        C12960it.A1F(A0h);
        AbstractC116665Wi r1 = r2.A05;
        if (r1 != null) {
            r1.AYX(((AbstractC55202hx) r2).A05, videoPort);
        }
    }

    @Override // X.AnonymousClass5X3
    public void AUu(VideoPort videoPort) {
        StringBuilder A0h = C12960it.A0h();
        C60112vx r1 = this.A00;
        A0h.append(r1.A07);
        C12960it.A1N(A0h, "onRenderStarted ", videoPort);
        A0h.append(((AbstractC55202hx) r1).A05);
        C12960it.A1F(A0h);
        r1.A0D.post(new RunnableBRunnable0Shape14S0100000_I1(this, 45));
    }
}
