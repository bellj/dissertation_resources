package X;

import android.os.Handler;
import java.util.concurrent.Callable;

/* renamed from: X.0db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09840db implements Runnable {
    public Handler A00;
    public AnonymousClass024 A01;
    public Callable A02;

    public RunnableC09840db(Handler handler, AnonymousClass024 r2, Callable callable) {
        this.A02 = callable;
        this.A01 = r2;
        this.A00 = handler;
    }

    @Override // java.lang.Runnable
    public void run() {
        Object obj;
        try {
            obj = this.A02.call();
        } catch (Exception unused) {
            obj = null;
        }
        this.A00.post(new RunnableC09830da(this, this.A01, obj));
    }
}
