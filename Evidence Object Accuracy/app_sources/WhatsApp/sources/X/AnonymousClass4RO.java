package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4RO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4RO {
    public final /* synthetic */ AnonymousClass19T A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass4RO(AnonymousClass19T r1, UserJid userJid, String str, boolean z) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = userJid;
        this.A03 = z;
    }
}
