package X;

import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.4Xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92704Xc {
    public final CopyOnWriteArraySet A00 = new CopyOnWriteArraySet();
    public final /* synthetic */ VoipPhysicalCamera A01;

    public C92704Xc(VoipPhysicalCamera voipPhysicalCamera) {
        this.A01 = voipPhysicalCamera;
    }

    public void A00() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass2NP) it.next()).AOD(this.A01);
        }
    }

    public void A01() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass2NP) it.next()).AQ0(this.A01);
        }
    }

    public void A02() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass2NP) it.next()).AXz(this.A01);
        }
    }
}
