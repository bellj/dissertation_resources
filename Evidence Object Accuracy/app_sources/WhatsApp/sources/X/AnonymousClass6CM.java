package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import com.whatsapp.util.Log;

/* renamed from: X.6CM  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CM implements AnonymousClass6N0 {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    @Override // X.AnonymousClass6N0
    public void ATW(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void ATZ(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    @Override // X.AnonymousClass6N0
    public void ATc(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void ATg(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void AXm(PaymentBottomSheet paymentBottomSheet) {
    }

    public AnonymousClass6CM(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AnonymousClass6N0
    public void AOW(View view, View view2, AnonymousClass1ZO r30, AbstractC28901Pl r31, PaymentBottomSheet paymentBottomSheet) {
        String A0O;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        indiaUpiSendPaymentActivity.A2C(R.string.register_wait_message);
        C119835fB r3 = new C119835fB();
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0G = r3;
        r3.A0F = AnonymousClass15O.A00(((ActivityC13790kL) indiaUpiSendPaymentActivity).A01, ((ActivityC13790kL) indiaUpiSendPaymentActivity).A05, false);
        C119835fB r1 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0G;
        if (!TextUtils.isEmpty(((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0n)) {
            A0O = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0n;
        } else {
            A0O = AbstractActivityC119235dO.A0O(indiaUpiSendPaymentActivity);
        }
        r1.A0N = A0O;
        AnonymousClass1ZY r32 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B.A08;
        AnonymousClass009.A06(r32, ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A02("IndiaUpiPaymentActivity onRequestPayment: Cannot get IndiaUpiMethodData"));
        C119755f3 r33 = (C119755f3) r32;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0G.A09 = r33.A06;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0b = AnonymousClass4ES.A00(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0Q, ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0a);
        C120555gN r7 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0M;
        AnonymousClass1ZR r8 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08;
        String str = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0M;
        AnonymousClass1ZR A04 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0B.A04();
        String A0B = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0B.A0B();
        AnonymousClass1ZR r12 = r33.A06;
        C30821Yy r13 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0A;
        C119835fB r0 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0G;
        String str2 = r0.A0N;
        String str3 = r0.A0F;
        String str4 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B.A0A;
        String str5 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0b;
        AnonymousClass1ZR r34 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A07;
        C128595wM r6 = new C128595wM(indiaUpiSendPaymentActivity);
        Log.i("PAY: collectFromVpa called");
        C17220qS r5 = r7.A03;
        String A01 = r5.A01();
        String A012 = r7.A05.A01();
        C126495sy r14 = new C126495sy(C120555gN.A00(((C126705tJ) r7).A01.A00(C30771Yt.A05, r13)));
        C126485sx r132 = new C126485sx(new AnonymousClass3CT(A01), r14, (String) AnonymousClass1ZS.A01(r8), str, (String) C117295Zj.A0R(r34), (String) A04.A00, A0B, (String) C117295Zj.A0R(r12), str2, str4, str5, str3, A012);
        C64513Fv r122 = ((C126705tJ) r7).A00;
        if (r122 != null) {
            r122.A04("upi-collect-from-vpa");
        }
        C117325Zm.A05(r5, new C120685ga(r7.A00, r7.A01, r7.A04, r122, r7, r6), r132.A00, A01);
    }

    @Override // X.AnonymousClass6N0
    public void AXn(String str) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0a = str;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0V.A01(str);
    }

    @Override // X.AnonymousClass6N0
    public void AXo(PaymentBottomSheet paymentBottomSheet) {
        IndiaUpiEditTransactionDescriptionFragment A00 = IndiaUpiEditTransactionDescriptionFragment.A00(((AbstractActivityC121525iS) this.A00).A0a);
        A00.A08 = new C133996Cx(this);
        paymentBottomSheet.A1L(A00);
    }
}
