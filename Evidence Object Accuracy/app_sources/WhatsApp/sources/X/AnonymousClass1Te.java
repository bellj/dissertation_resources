package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Te  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1Te {
    public static Map A00 = new HashMap();

    static {
        A02(new AnonymousClass2H7(), "bm bo dz id ig ii in ja jbo jv jw kde kea km ko lkt lo ms my nqo osa root sah ses sg su th to vi wo yo yue zh");
        A02(new AnonymousClass2H8(), "am as bn fa gu hi kn zu");
        A02(new AnonymousClass2H9(), "ff fr hy kab");
        A01(new AnonymousClass2HA(), "pt");
        A02(new AnonymousClass2HB(), "ast ca de en et fi fy gl ia io it ji nl pt_PT sc scn sv sw ur yi");
        A01(new AnonymousClass2HC(), "si");
        A02(new AnonymousClass2HD(), "ak bho guw ln mg nso pa ti wa");
        A01(new AnonymousClass2HE(), "tzm");
        A02(new AnonymousClass2HF(), "af an asa az bem bez bg brx ce cgg chr ckb dv ee el eo es eu fo fur gsw ha haw hu jgo jmc ka kaj kcg kk kkj kl ks ksb ku ky lb lg mas mgo ml mn mr nah nb nd ne nn nnh no nr ny nyn om or os pap ps rm rof rwk saq sd sdh seh sn so sq ss ssy st syr ta te teo tig tk tn tr ts ug uz ve vo vun wae xh xog");
        A01(new AnonymousClass2HG(), "da");
        A01(new AnonymousClass2HH(), "is");
        A01(new AnonymousClass2HI(), "mk");
        A02(new AnonymousClass2HJ(), "ceb fil tl");
        A02(new AnonymousClass2HK(), "lv prg");
        A01(new AnonymousClass2HL(), "lag");
        A01(new AnonymousClass2HM(), "ksh");
        A02(new AnonymousClass2HN(), "iu naq se sma smi smj smn sms");
        A01(new AnonymousClass2HO(), "shi");
        A02(new AnonymousClass2HP(), "mo ro");
        A02(new AnonymousClass2HQ(), "bs hr sh sr");
        A01(new AnonymousClass2HR(), "gd");
        A01(new AnonymousClass2HS(), "sl");
        A02(new AnonymousClass2HT(), "dsb hsb");
        A02(new AnonymousClass2HU(), "he iw");
        A02(new AnonymousClass2HV(), "cs sk");
        A01(new AnonymousClass2HW(), "pl");
        A01(new AnonymousClass2HX(), "be");
        A01(new AnonymousClass2HY(), "lt");
        A01(new AnonymousClass2HZ(), "mt");
        A02(new C48632Ha(), "ru uk");
        A01(new C48642Hb(), "br");
        A01(new C48652Hc(), "ga");
        A01(new C48662Hd(), "gv");
        A01(new C48672He(), "kw");
        A02(new C48682Hf(), "ar ars");
        A01(new C48692Hg(), "cy");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002a, code lost:
        if (r0 != null) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1Te A00(java.util.Locale r5) {
        /*
            java.lang.String r4 = r5.getLanguage()
            java.lang.String r3 = r5.getCountry()
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x002d
            java.util.Map r2 = X.AnonymousClass1Te.A00
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r4)
            java.lang.String r0 = "_"
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            java.lang.Object r0 = r2.get(r0)
            X.1Te r0 = (X.AnonymousClass1Te) r0
            if (r0 == 0) goto L_0x002d
        L_0x002c:
            return r0
        L_0x002d:
            java.util.Map r1 = X.AnonymousClass1Te.A00
            java.lang.Object r0 = r1.get(r4)
            X.1Te r0 = (X.AnonymousClass1Te) r0
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = "root"
            java.lang.Object r0 = r1.get(r0)
            X.1Te r0 = (X.AnonymousClass1Te) r0
            if (r0 != 0) goto L_0x002c
            java.lang.String r1 = "No plural rule found for 'root' locale."
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Te.A00(java.util.Locale):X.1Te");
    }

    public static void A01(AnonymousClass1Te r5, String str) {
        Map map = A00;
        map.put(str, r5);
        if ("pt_PT".equals(str)) {
            Iterator it = AnonymousClass1Th.A00.iterator();
            while (it.hasNext()) {
                StringBuilder sb = new StringBuilder("pt_");
                sb.append((String) it.next());
                map.put(sb.toString(), r5);
            }
        }
    }

    public static void A02(AnonymousClass1Te r4, String str) {
        for (String str2 : str.split(" ")) {
            A01(r4, str2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:292:0x035e, code lost:
        if (r3 <= 19.0d) goto L_0x0362;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:347:0x0423, code lost:
        if (r5 != 91.0d) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:595:0x0752, code lost:
        if (r24 != 0) goto L_0x0754;
     */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:610:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:611:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:614:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:625:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:683:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:694:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:700:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:709:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:710:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:725:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:742:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:745:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:746:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A03(double r21, int r23, long r24, long r26, long r28) {
        /*
        // Method dump skipped, instructions count: 1896
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Te.A03(double, int, long, long, long):int");
    }

    public int A04(Object obj) {
        long j;
        if (obj instanceof String) {
            String str = (String) obj;
            int indexOf = str.indexOf(46);
            try {
                if (indexOf == -1) {
                    long parseLong = Long.parseLong(str);
                    return A03((double) parseLong, 0, parseLong, 0, 0);
                }
                double parseDouble = Double.parseDouble(str);
                long j2 = 0;
                if (indexOf == 0) {
                    j = 0;
                } else {
                    j = Long.parseLong(str.substring(0, indexOf));
                }
                String substring = str.substring(indexOf + 1);
                int length = substring.length();
                int i = length;
                while (i > 0 && substring.charAt(i - 1) == '0') {
                    i--;
                }
                long parseLong2 = Long.parseLong(substring);
                if (i != 0) {
                    j2 = Long.parseLong(substring.substring(0, i));
                }
                return A03(parseDouble, length, j, parseLong2, j2);
            } catch (NumberFormatException unused) {
                return 0;
            }
        } else if (!(obj instanceof Long)) {
            return 0;
        } else {
            long longValue = ((Number) obj).longValue();
            return A03((double) longValue, 0, longValue, 0, 0);
        }
    }
}
