package X;

/* renamed from: X.11w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C234911w {
    public final C16120oU A00;

    public C234911w(C16120oU r1) {
        this.A00 = r1;
    }

    public void A00(int i) {
        AnonymousClass43G r1 = new AnonymousClass43G();
        r1.A01 = Integer.valueOf(i);
        r1.A00 = 1;
        this.A00.A07(r1);
    }

    public void A01(int i, boolean z) {
        AnonymousClass30B r1 = new AnonymousClass30B();
        r1.A01 = Integer.valueOf(i);
        r1.A00 = Boolean.valueOf(z);
        this.A00.A07(r1);
    }

    public void A02(long j, long j2, boolean z) {
        AnonymousClass30I r3 = new AnonymousClass30I();
        r3.A02 = Long.valueOf(j / 3600);
        r3.A01 = Long.valueOf(j2 / 3600);
        r3.A00 = Boolean.valueOf(z);
        this.A00.A07(r3);
    }

    public void A03(long j, long j2, boolean z) {
        AnonymousClass30J r3 = new AnonymousClass30J();
        r3.A02 = Long.valueOf(j / 3600);
        r3.A01 = Long.valueOf(j2 / 3600);
        r3.A00 = Boolean.valueOf(z);
        this.A00.A07(r3);
    }

    public void A04(boolean z) {
        C854342u r1 = new C854342u();
        r1.A00 = Boolean.valueOf(z);
        this.A00.A07(r1);
    }
}
