package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* renamed from: X.0Gj  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Gj extends AbstractC06170Sl {
    public static final String A01 = C06390Tk.A01("BrdcstRcvrCnstrntTrckr");
    public final BroadcastReceiver A00 = new C019409d(this);

    public abstract IntentFilter A05();

    public abstract void A06(Context context, Intent intent);

    public AnonymousClass0Gj(Context context, AbstractC11500gO r3) {
        super(context, r3);
    }

    @Override // X.AbstractC06170Sl
    public void A01() {
        C06390Tk.A00().A02(A01, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.A01.registerReceiver(this.A00, A05());
    }

    @Override // X.AbstractC06170Sl
    public void A02() {
        C06390Tk.A00().A02(A01, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.A01.unregisterReceiver(this.A00);
    }
}
