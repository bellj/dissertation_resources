package X;

import java.util.List;

/* renamed from: X.1cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32451cB {
    public final String A00;
    public final String A01;
    public final List A02;

    public C32451cB(String str, String str2, List list) {
        this.A01 = str;
        this.A00 = str2;
        this.A02 = list;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C32451cB r5 = (C32451cB) obj;
            if (this.A01.equals(r5.A01) && this.A00.equals(r5.A00)) {
                List list = this.A02;
                List list2 = r5.A02;
                if (list != null) {
                    return list.equals(list2);
                }
                if (list2 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = ((this.A01.hashCode() * 31) + this.A00.hashCode()) * 31;
        List list = this.A02;
        return hashCode + (list != null ? list.hashCode() : 0);
    }
}
