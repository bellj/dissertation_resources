package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout;
import com.whatsapp.conversation.conversationrow.TemplateRowContentLayout;
import java.util.List;

/* renamed from: X.2xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60512xz extends AnonymousClass1OY {
    public boolean A00;
    public final TextEmojiLabel A01 = C12970iu.A0U(this, R.id.title_text_message);
    public final TemplateQuickReplyButtonsLayout A02 = ((TemplateQuickReplyButtonsLayout) findViewById(R.id.template_quick_reply_buttons));
    public final TemplateRowContentLayout A03 = ((TemplateRowContentLayout) findViewById(R.id.template_message_content));

    public C60512xz(Context context, AbstractC13890kV r3, C28851Pg r4) {
        super(context, r3, r4);
        A0Z();
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public final void A1M() {
        boolean z;
        int i;
        List list;
        int i2;
        C28851Pg r5 = (C28851Pg) getFMessage();
        List list2 = r5.A00.A04;
        if (list2 == null || list2.isEmpty()) {
            z = false;
            i = -2;
        } else {
            z = true;
            i = getResources().getDimensionPixelSize(R.dimen.conversation_row_document_width);
        }
        View view = ((AbstractC28551Oa) this).A0D;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = i;
        view.setLayoutParams(layoutParams);
        TemplateRowContentLayout templateRowContentLayout = this.A03;
        templateRowContentLayout.A02(this);
        if (!TextUtils.isEmpty(r5.A0I())) {
            String A0I = r5.A0I();
            TextEmojiLabel textEmojiLabel = this.A01;
            A17(textEmojiLabel, getFMessage(), A0I, false, true);
            ViewGroup.LayoutParams layoutParams2 = textEmojiLabel.getLayoutParams();
            if (z) {
                i2 = i;
            } else {
                i2 = -1;
                if (A1N(r5)) {
                    i2 = -2;
                }
            }
            layoutParams2.width = i2;
            textEmojiLabel.setLayoutParams(layoutParams2);
            textEmojiLabel.setVisibility(0);
        } else {
            this.A01.setVisibility(8);
        }
        ViewGroup.LayoutParams layoutParams3 = templateRowContentLayout.getLayoutParams();
        if (!z) {
            i = -2;
            if (A1N(r5)) {
                i = -1;
            }
        }
        layoutParams3.width = i;
        templateRowContentLayout.setLayoutParams(layoutParams3);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A02;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass4KN r1 = this.A1c;
            AbstractC13890kV r0 = ((AbstractC28551Oa) this).A0a;
            if (r0 == null || !r0.AdX()) {
                list = null;
            } else {
                list = ((AbstractC28871Pi) getFMessage()).AH7().A04;
            }
            templateQuickReplyButtonsLayout.A01(r1, list);
        }
    }

    public final boolean A1N(C28851Pg r7) {
        float f;
        String A0I = r7.A0I();
        if (TextUtils.isEmpty(A0I)) {
            return false;
        }
        C28891Pk r0 = r7.A00;
        String str = r0.A01;
        String str2 = r0.A02;
        float measureText = this.A01.getPaint().measureText(A0I);
        TemplateRowContentLayout templateRowContentLayout = this.A03;
        float measureText2 = templateRowContentLayout.getContentTextView().getPaint().measureText(str);
        if (!TextUtils.isEmpty(str2)) {
            f = templateRowContentLayout.A02.getPaint().measureText(str2);
        } else {
            f = 0.0f;
        }
        if (measureText <= measureText2 || measureText <= f) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_template_title_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_template_title_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_template_title_text_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A02;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass1OY.A0G(templateQuickReplyButtonsLayout, this);
        }
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A02;
        if (templateQuickReplyButtonsLayout != null) {
            setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A05(this, templateQuickReplyButtonsLayout));
        }
    }
}
