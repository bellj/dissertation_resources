package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.Comparator;

/* renamed from: X.4aY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93674aY {
    public static final Comparator A04 = new IDxComparatorShape3S0000000_2_I1(7);
    public static final Comparator A05 = new IDxComparatorShape3S0000000_2_I1(6);
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;

    public /* synthetic */ C93674aY(String str, String str2, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A03 = str;
        this.A02 = str2;
    }
}
