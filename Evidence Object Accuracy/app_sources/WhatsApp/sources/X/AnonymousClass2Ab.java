package X;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.mediacomposer.doodle.ColorPickerComponent;
import com.whatsapp.mediacomposer.doodle.ColorPickerView;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerView;
import com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView;
import java.util.List;

/* renamed from: X.2Ab  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ab implements AbstractC47302Ac {
    public int A00 = 0;
    public Activity A01;
    public View A02;
    public C64453Fp A03;
    public AnonymousClass5Yv A04;
    public DialogC51702Ye A05;
    public DialogC51712Yf A06;
    public boolean A07;
    public boolean A08 = false;
    public final Rect A09 = new Rect();
    public final Handler A0A;
    public final AnonymousClass018 A0B;
    public final C14850m9 A0C;
    public final MediaComposerFragment A0D;
    public final ColorPickerComponent A0E;
    public final C48792Hu A0F;
    public final C454921v A0G;
    public final DoodleView A0H;
    public final C64243Eu A0I;
    public final C47322Ae A0J;
    public final AnonymousClass3DK A0K;
    public final C63583Cd A0L;
    public final AnonymousClass3MO A0M;
    public final AbstractC47302Ac A0N;
    public final C454721t A0O;
    public final C93694aa A0P;
    public final C47342Ag A0Q;
    public final C64073Ed A0R;
    public final C252718t A0S;
    public final C002601e A0T;
    public final boolean A0U;
    public final boolean A0V;

    public AnonymousClass2Ab(Activity activity, GestureDetector.OnGestureListener onGestureListener, View view, AbstractC001200n r35, AbstractC001400p r36, AnonymousClass01d r37, AnonymousClass018 r38, AnonymousClass19M r39, C14850m9 r40, C64453Fp r41, MediaComposerFragment mediaComposerFragment, C48792Hu r43, AnonymousClass1BT r44, AnonymousClass1BU r45, AbstractC47302Ac r46, AnonymousClass1CV r47, C47342Ag r48, AnonymousClass1AB r49, AnonymousClass146 r50, C235512c r51, C252718t r52, AbstractC14440lR r53, boolean z) {
        Handler handler = new Handler(Looper.getMainLooper());
        this.A0A = handler;
        this.A0C = r40;
        this.A01 = activity;
        this.A0S = r52;
        this.A0B = r38;
        this.A02 = view;
        this.A03 = r41;
        this.A0N = r46;
        this.A0Q = r48;
        this.A0U = z;
        this.A0D = mediaComposerFragment;
        this.A0F = r43;
        AnonymousClass028.A0D(view, R.id.doodle_decoration).setVisibility(0);
        Resources resources = activity.getResources();
        AbstractC454821u.A03 = resources.getDimension(R.dimen.doodle_min_shape_size);
        AbstractC454821u.A05 = resources.getDimension(R.dimen.doodle_min_text_size);
        AbstractC454821u.A08 = resources.getDimension(R.dimen.doodle_max_text_size);
        AbstractC454821u.A04 = resources.getDimension(R.dimen.doodle_min_stroke);
        AbstractC454821u.A07 = resources.getDimension(R.dimen.doodle_max_stroke);
        AbstractC454821u.A06 = resources.getDimension(R.dimen.doodle_hit_test_distance);
        DoodleView doodleView = (DoodleView) this.A02.findViewById(R.id.doodle_view);
        this.A0H = doodleView;
        C64243Eu r5 = doodleView.A0F;
        this.A0I = r5;
        C454721t r4 = doodleView.A0H;
        this.A0O = r4;
        boolean A07 = r40.A07(926);
        this.A0V = A07;
        C454921v r13 = doodleView.A0E;
        this.A0G = r13;
        C93694aa r15 = new C93694aa(new C89534Ki(this));
        this.A0P = r15;
        C47322Ae r2 = new C47322Ae(r13, doodleView.A0G, r4, r15, doodleView.getResources().getDisplayMetrics().density, A07);
        this.A0J = r2;
        this.A0L = new C63583Cd(r13, r4);
        C64073Ed r0 = new C64073Ed(handler, this.A02.findViewById(R.id.trash), r37, r38, new AnonymousClass2ZR());
        this.A0R = r0;
        AnonymousClass3DK r1 = new AnonymousClass3DK(new C89524Kh(this), r5, new C64213Er(handler, (ViewGroup) this.A02.findViewById(R.id.media_guidelines), r37), r0);
        this.A0K = r1;
        ColorPickerComponent colorPickerComponent = (ColorPickerComponent) this.A02.findViewById(R.id.color_picker_component);
        this.A0E = colorPickerComponent;
        if (A07) {
            colorPickerComponent.setPadding(0, 0, colorPickerComponent.getResources().getDimensionPixelSize(R.dimen.doodle_wave2_color_picker_side_margin), 0);
        }
        colorPickerComponent.A04(r41, new AnonymousClass3YH(r43, this, r48), doodleView);
        AnonymousClass3YI r3 = new AnonymousClass3YI(this, new RunnableBRunnable0Shape1S0300000_I0_1(this, r48, r41, 10));
        this.A04 = r3;
        AnonymousClass3MO r02 = new AnonymousClass3MO(onGestureListener, r3, doodleView, r1, new C89544Kj(), r4);
        this.A0M = r02;
        doodleView.setControllers(r02, r2);
        doodleView.setDoodleViewListener(this.A04);
        this.A0T = new C002601e(null, new AnonymousClass01N(activity, r35, r36, r38, r39, mediaComposerFragment, this, r44, r45, r47, r48, r49, r50, r51, r52, r53) { // from class: X.3d8
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ AbstractC001200n A01;
            public final /* synthetic */ AbstractC001400p A02;
            public final /* synthetic */ AnonymousClass018 A03;
            public final /* synthetic */ AnonymousClass19M A04;
            public final /* synthetic */ MediaComposerFragment A05;
            public final /* synthetic */ AnonymousClass2Ab A06;
            public final /* synthetic */ AnonymousClass1BT A07;
            public final /* synthetic */ AnonymousClass1BU A08;
            public final /* synthetic */ AnonymousClass1CV A09;
            public final /* synthetic */ C47342Ag A0A;
            public final /* synthetic */ AnonymousClass1AB A0B;
            public final /* synthetic */ AnonymousClass146 A0C;
            public final /* synthetic */ C235512c A0D;
            public final /* synthetic */ C252718t A0E;
            public final /* synthetic */ AbstractC14440lR A0F;

            {
                this.A06 = r8;
                this.A00 = r2;
                this.A0E = r16;
                this.A0F = r17;
                this.A04 = r6;
                this.A07 = r9;
                this.A03 = r5;
                this.A0C = r14;
                this.A0D = r15;
                this.A09 = r11;
                this.A08 = r10;
                this.A0B = r13;
                this.A01 = r3;
                this.A02 = r4;
                this.A0A = r12;
                this.A05 = r7;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                AnonymousClass2Ab r12 = this.A06;
                Activity activity2 = this.A00;
                C252718t r03 = this.A0E;
                AbstractC14440lR r14 = this.A0F;
                AnonymousClass19M r132 = this.A04;
                AnonymousClass1BT r11 = this.A07;
                AnonymousClass018 r10 = this.A03;
                AnonymousClass146 r9 = this.A0C;
                C235512c r8 = this.A0D;
                AnonymousClass1CV r7 = this.A09;
                AnonymousClass1BU r6 = this.A08;
                AnonymousClass1AB r54 = this.A0B;
                AbstractC001200n r42 = this.A01;
                AbstractC001400p r32 = this.A02;
                return new AnonymousClass1KW(activity2, this.A0A.A0H.A07, r42, r32, r10, r132, this.A05, r11, r6, r12, (ShapePickerView) r12.A02.findViewById(R.id.shape_picker), r7, r54, r9, r8, r03, r14);
            }
        });
        this.A07 = false;
    }

    public void A00() {
        DoodleView doodleView = this.A0H;
        if (doodleView.A04()) {
            C47322Ae r6 = this.A0J;
            r6.A02 = true;
            C64453Fp r3 = this.A03;
            r3.A03();
            A02();
            this.A0O.A01 = null;
            if (this.A0V) {
                this.A0E.A05(false);
                r3.A01();
                int[] iArr = new int[2];
                if (doodleView.getScaleX() == 1.0f && doodleView.getScaleY() == 1.0f) {
                    doodleView.getLocationOnScreen(iArr);
                }
                Activity activity = this.A01;
                C48792Hu r4 = this.A0F;
                DialogC51702Ye r2 = new DialogC51702Ye(activity, r4, new C89514Kg(doodleView), r6, this.A0L, iArr, this.A0U);
                this.A05 = r2;
                r2.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: X.3L7
                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        AnonymousClass2Ab r62 = AnonymousClass2Ab.this;
                        C64453Fp r5 = r62.A03;
                        r5.A03();
                        r62.A0J.A02 = false;
                        ColorPickerComponent colorPickerComponent = r62.A0E;
                        ColorPickerView colorPickerView = colorPickerComponent.A05;
                        colorPickerView.A01();
                        colorPickerView.invalidate();
                        r62.A0O.A01 = null;
                        DoodleView doodleView2 = r62.A0H;
                        C48792Hu r22 = r62.A0F;
                        doodleView2.A03 = r22.A00;
                        doodleView2.invalidate();
                        colorPickerComponent.setColorAndInvalidate(r22.A00);
                        C47342Ag r1 = r62.A0Q;
                        r1.A09(0);
                        r1.A01 = r22.A00;
                        r5.A02();
                        r62.A04();
                        r1.A04();
                    }
                });
                this.A0Q.A0A(r4.A00, 0.0f);
                this.A05.setOnShowListener(new DialogInterface.OnShowListener() { // from class: X.4hW
                    @Override // android.content.DialogInterface.OnShowListener
                    public final void onShow(DialogInterface dialogInterface) {
                        AnonymousClass2Ab.this.A0Q.A03();
                    }
                });
                return;
            }
            A04();
            doodleView.A03 = this.A0F.A00;
            ColorPickerComponent colorPickerComponent = this.A0E;
            ColorPickerView colorPickerView = colorPickerComponent.A05;
            colorPickerView.A01();
            colorPickerView.invalidate();
            colorPickerComponent.A00();
        }
    }

    public void A01() {
        if (this.A0H.A04()) {
            if (this.A0V) {
                A04();
                C47342Ag r2 = this.A0Q;
                r2.A04();
                r2.A09(0);
                this.A03.A02();
                int i = 4;
                if (!this.A0O.A03.A00.isEmpty()) {
                    i = 0;
                }
                r2.A0H.setUndoButtonVisibility(i);
            }
            C64453Fp r22 = this.A03;
            r22.A03();
            A02();
            this.A0J.A02 = false;
            ColorPickerComponent colorPickerComponent = this.A0E;
            colorPickerComponent.A05(true);
            r22.A00();
            ColorPickerView colorPickerView = colorPickerComponent.A05;
            colorPickerView.A01();
            colorPickerView.invalidate();
            this.A0O.A01 = null;
        }
    }

    public final void A02() {
        if (A08()) {
            AnonymousClass1KW r2 = (AnonymousClass1KW) this.A0T.get();
            ShapePickerView shapePickerView = r2.A0Q;
            shapePickerView.setVisibility(8);
            r2.A0X.A01(shapePickerView);
            if (shapePickerView.A02()) {
                shapePickerView.invalidate();
            }
            if (r2.A04) {
                r2.A0D.A1C();
            }
            TitleBarView titleBarView = this.A0Q.A0H;
            titleBarView.setToolbarExtraVisibility(8);
            int i = 4;
            if (!this.A0O.A03.A00.isEmpty()) {
                i = 0;
            }
            titleBarView.setUndoButtonVisibility(i);
            this.A03.A02();
            A04();
        }
    }

    public final void A03() {
        if (A08()) {
            AnonymousClass1KW r4 = (AnonymousClass1KW) this.A0T.get();
            boolean z = this.A07;
            r4.A0T.A03(z);
            r4.A0S.A03(z);
            r4.A0Y.A0B(Boolean.valueOf(z));
            ShapePickerRecyclerView shapePickerRecyclerView = r4.A0P;
            boolean z2 = false;
            if (r4.A03.A06.getVisibility() == 0) {
                z2 = true;
            }
            shapePickerRecyclerView.A11(z, z2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04() {
        /*
            r5 = this;
            X.2Ae r0 = r5.A0J
            boolean r0 = r0.A02
            r3 = 0
            if (r0 == 0) goto L_0x0075
            boolean r0 = r5.A0V
            if (r0 != 0) goto L_0x0075
        L_0x000b:
            com.whatsapp.mediacomposer.doodle.ColorPickerComponent r0 = r5.A0E
            r0.A00()
        L_0x0010:
            boolean r0 = r5.A08()
            X.2Ag r2 = r5.A0Q
            if (r0 == 0) goto L_0x0060
            boolean r0 = r2.A0I
            if (r0 != 0) goto L_0x0035
            com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView r4 = r2.A0H
            android.widget.ImageView r0 = r4.A05
            r1 = 1056964608(0x3f000000, float:0.5)
            r0.setAlpha(r1)
            com.whatsapp.WaTextView r0 = r4.A09
            r0.setAlpha(r1)
            android.widget.ImageView r0 = r4.A03
            r0.setAlpha(r1)
            android.view.View r1 = r4.A01
            r0 = 4
            r1.setVisibility(r0)
        L_0x0035:
            com.whatsapp.mediacomposer.doodle.ColorPickerComponent r1 = r5.A0E
            com.whatsapp.mediacomposer.doodle.ColorPickerView r0 = r1.A05
            r0.clearAnimation()
            r1.A05(r3)
        L_0x003f:
            X.018 r0 = r5.A0B
            X.1Kv r0 = r0.A04()
            boolean r1 = r0.A06
            com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView r0 = r2.A0H
            android.widget.RelativeLayout r3 = r0.A07
            android.widget.ImageView r0 = r0.A02
            android.view.ViewGroup$LayoutParams r2 = r3.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r2 = (android.widget.RelativeLayout.LayoutParams) r2
            r1 = r1 ^ 1
            int r0 = r0.getId()
            r2.addRule(r1, r0)
            r3.setLayoutParams(r2)
            return
        L_0x0060:
            X.21t r0 = r5.A0O
            X.3Df r0 = r0.A03
            java.util.LinkedList r0 = r0.A00
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x006f
            r3 = 4
        L_0x006f:
            com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView r0 = r2.A0H
            r0.setUndoButtonVisibility(r3)
            goto L_0x003f
        L_0x0075:
            X.2Ag r2 = r5.A0Q
            int r1 = r2.A00()
            r0 = 2
            if (r1 != r0) goto L_0x0010
            X.21t r0 = r5.A0O
            X.21u r1 = r0.A01
            if (r1 == 0) goto L_0x0092
            boolean r0 = r1.A0K()
            if (r0 != 0) goto L_0x000b
            boolean r0 = r1.A0J()
            if (r0 == 0) goto L_0x0092
            goto L_0x000b
        L_0x0092:
            com.whatsapp.mediacomposer.doodle.ColorPickerComponent r1 = r5.A0E
            r0 = 1
            r1.A05(r0)
            X.3Fp r0 = r5.A03
            r0.A00()
            r2.A09(r3)
            r5.A01()
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Ab.A04():void");
    }

    public void A05(RectF rectF) {
        C64243Eu r2 = this.A0I;
        r2.A07 = rectF;
        C63113Ai.A00(r2.A09, rectF, (float) r2.A02);
        DoodleView doodleView = this.A0H;
        r2.A08 = doodleView.getResources().getDisplayMetrics();
        C454921v r0 = this.A0G;
        r0.A02();
        doodleView.requestLayout();
        r0.A01();
    }

    public void A06(AbstractC454821u r4) {
        this.A0H.A03(r4);
        if (!A08()) {
            boolean A0J = r4.A0J();
            C47342Ag r1 = this.A0Q;
            int i = 0;
            if (A0J) {
                i = 2;
            }
            r1.A09(i);
            r1.A01 = this.A0F.A00;
        }
    }

    public final void A07(AnonymousClass33L r17) {
        String str;
        float textSize;
        int color;
        int i;
        A02();
        this.A03.A03();
        this.A0J.A02 = false;
        C47342Ag r4 = this.A0Q;
        TitleBarView titleBarView = r4.A0H;
        titleBarView.A0C.A01(0);
        titleBarView.A0B.A01(0);
        ColorPickerComponent colorPickerComponent = this.A0E;
        colorPickerComponent.A05(false);
        C252718t r0 = this.A0S;
        DoodleView doodleView = this.A0H;
        r0.A02(doodleView);
        int[] iArr = {titleBarView.getPaddingLeft(), titleBarView.getPaddingTop(), titleBarView.getPaddingRight(), titleBarView.getPaddingBottom()};
        boolean A07 = this.A0C.A07(926);
        int i2 = R.layout.doodle_text_entry_legacy;
        if (A07) {
            i2 = R.layout.doodle_text_entry_wave2;
        }
        AbstractC471028y r13 = (AbstractC471028y) LayoutInflater.from(this.A02.getContext()).inflate(i2, (ViewGroup) null).findViewById(R.id.main);
        if (r17 == null) {
            str = "";
            textSize = 0.0f;
            color = this.A0F.A00;
            i = this.A00;
        } else {
            str = r17.A05;
            textSize = r17.A08.getTextSize();
            color = ((AbstractC454821u) r17).A01.getColor();
            i = r17.A03;
        }
        C91494Ry r14 = new C91494Ry(str, textSize, color, i);
        this.A00 = r14.A02;
        DialogC51712Yf r10 = new DialogC51712Yf(this.A01, this, r13, r14, iArr);
        this.A06 = r10;
        ColorPickerView colorPickerView = colorPickerComponent.A05;
        r10.A01.A03.A00 = colorPickerView.getHeight();
        DialogC51712Yf r6 = this.A06;
        boolean z = false;
        if (colorPickerView.getVisibility() == 0) {
            z = true;
        }
        r6.A01.A03.A09 = !z;
        if (r17 != null) {
            this.A0O.A04(r17);
            doodleView.invalidate();
        }
        this.A06.setOnShowListener(new DialogInterface.OnShowListener(r14, A07) { // from class: X.4hc
            public final /* synthetic */ C91494Ry A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                AnonymousClass2Ab r1 = AnonymousClass2Ab.this;
                C91494Ry r02 = this.A01;
                boolean z2 = this.A02;
                C47342Ag r2 = r1.A0Q;
                r2.A0H.setFont(r02.A02);
                if (z2) {
                    r2.A03();
                }
            }
        });
        if (A07) {
            r4.A0A(r14.A01, r14.A00);
        } else {
            this.A06.show();
        }
        this.A06.setOnDismissListener(new DialogInterface.OnDismissListener(r17, r14, A07) { // from class: X.3L9
            public final /* synthetic */ AnonymousClass33L A01;
            public final /* synthetic */ C91494Ry A02;
            public final /* synthetic */ boolean A03;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
            }

            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                C454921v r02;
                AnonymousClass2Ab r9 = AnonymousClass2Ab.this;
                AnonymousClass33L r8 = this.A01;
                C91494Ry r7 = this.A02;
                boolean z2 = this.A03;
                boolean isEmpty = TextUtils.isEmpty(r7.A03);
                if (r8 != null) {
                    if (isEmpty) {
                        r02 = r9.A0G;
                    } else {
                        C454721t r2 = r9.A0O;
                        C63863Df r03 = r2.A03;
                        List list = r2.A04;
                        r03.A00(list);
                        AbstractC454821u r04 = r2.A01;
                        if (r04 != null && !list.contains(r04)) {
                            r2.A01 = null;
                        }
                        DoodleView doodleView2 = r9.A0H;
                        String str2 = r7.A03;
                        int i3 = r7.A01;
                        int i4 = r7.A02;
                        if (!(str2.equals(r8.A05) && ((AbstractC454821u) r8).A01.getColor() == i3 && i4 == r8.A03)) {
                            C454721t r22 = doodleView2.A0H;
                            r22.A03.A00.add(new AnonymousClass33O(r8.A03(), r8));
                            r8.A0S(i4);
                            r8.A0T(str2, i4);
                            r8.A09(i3);
                            doodleView2.invalidate();
                            if (r8 != r22.A01) {
                                r02 = doodleView2.A0E;
                            }
                        }
                    }
                    r02.A01();
                } else if (!isEmpty) {
                    DoodleView doodleView3 = r9.A0H;
                    String str3 = r7.A03;
                    int i5 = r7.A01;
                    int i6 = r7.A02;
                    AnonymousClass33L r05 = new AnonymousClass33L(doodleView3.getContext(), doodleView3.A04, doodleView3.A05);
                    r05.A0T(str3, i6);
                    r05.A09(i5);
                    doodleView3.A03(r05);
                }
                C48792Hu r06 = r9.A0F;
                int i7 = r7.A01;
                r06.A00 = i7;
                r9.A0E.setColorAndInvalidate(i7);
                DoodleView doodleView4 = r9.A0H;
                doodleView4.A03 = r7.A01;
                doodleView4.invalidate();
                C47342Ag r1 = r9.A0Q;
                r1.A09(0);
                r1.A01 = r7.A01;
                r9.A03.A02();
                r9.A04();
                if (z2) {
                    r1.A04();
                }
            }
        });
    }

    public final boolean A08() {
        C002601e r1 = this.A0T;
        if (!r1.A00() || ((AnonymousClass1KW) r1.get()).A0Q.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public boolean A09(float f, float f2) {
        if (A08()) {
            return true;
        }
        DoodleView doodleView = this.A0H;
        if (doodleView.A0F.A07 == null) {
            return false;
        }
        if (doodleView.A07.A02) {
            return true;
        }
        C454721t r1 = doodleView.A0H;
        return (r1.A02 == null && r1.A00(doodleView.A0G.A00(f, f2)) == null) ? false : true;
    }

    @Override // X.AbstractC47302Ac
    public void AVx(AbstractC454821u r3) {
        if (r3 instanceof AnonymousClass33N) {
            this.A0Q.A09(0);
            this.A0N.AVx(r3);
            return;
        }
        A06(r3);
    }
}
