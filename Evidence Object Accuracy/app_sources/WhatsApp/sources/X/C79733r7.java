package X;

import android.os.IBinder;

/* renamed from: X.3r7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79733r7 extends C98334iW implements AnonymousClass5YD {
    public C79733r7(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }
}
