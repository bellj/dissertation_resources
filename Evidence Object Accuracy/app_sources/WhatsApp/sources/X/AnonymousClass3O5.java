package X;

import android.widget.AbsListView;
import com.whatsapp.Conversation;

/* renamed from: X.3O5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3O5 implements AbsListView.OnScrollListener {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public final /* synthetic */ Conversation A04;

    public AnonymousClass3O5(Conversation conversation) {
        this.A04 = conversation;
    }

    public final void A00(int i, int i2) {
        AbstractC15340mz A05;
        Conversation conversation = this.A04;
        C15350n0 conversationCursorAdapter = conversation.A1g.getConversationCursorAdapter();
        int count = conversationCursorAdapter.getCount();
        while (i <= i2) {
            int headerViewsCount = i - conversation.A1g.getHeaderViewsCount();
            if (headerViewsCount >= 0 && headerViewsCount <= count - 1 && (A05 = conversationCursorAdapter.getItem(headerViewsCount)) != null && A05.A0y == 13) {
                conversation.A8w(A05.A0z);
            }
            i++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0171, code lost:
        if (r12.A12 < r4.A12) goto L_0x023c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x022e, code lost:
        if (r30 <= r5) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0239, code lost:
        if (r31 != 0) goto L_0x01dd;
     */
    @Override // android.widget.AbsListView.OnScrollListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onScroll(android.widget.AbsListView r29, int r30, int r31, int r32) {
        /*
        // Method dump skipped, instructions count: 578
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3O5.onScroll(android.widget.AbsListView, int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0069, code lost:
        if (X.C28111Kr.A00 != false) goto L_0x006b;
     */
    @Override // android.widget.AbsListView.OnScrollListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onScrollStateChanged(android.widget.AbsListView r10, int r11) {
        /*
            r9 = this;
            r1 = 1
            if (r11 == 0) goto L_0x0023
            if (r11 != r1) goto L_0x000c
            com.whatsapp.Conversation r0 = r9.A04
            X.185 r0 = r0.A3G
            r0.A01(r1)
        L_0x000c:
            com.whatsapp.Conversation r4 = r9.A04
            android.os.Handler r1 = r4.A4c
            java.lang.Runnable r0 = r4.A50
            r1.removeCallbacks(r0)
        L_0x0015:
            r4.A00 = r11
            if (r11 == 0) goto L_0x0020
            long r0 = java.lang.System.currentTimeMillis()
        L_0x001d:
            X.AnonymousClass223.A00 = r0
            return
        L_0x0020:
            r0 = 0
            goto L_0x001d
        L_0x0023:
            com.whatsapp.Conversation r4 = r9.A04
            X.185 r0 = r4.A3G
            r0.A00()
            com.whatsapp.conversation.ConversationListView r3 = r4.A1g
            java.util.HashSet r8 = r4.A0s
            int r7 = r3.getChildCount()
            r6 = 0
        L_0x0033:
            if (r6 >= r7) goto L_0x006b
            android.view.View r1 = r3.getChildAt(r6)
            boolean r0 = r1 instanceof X.AbstractC42481vH
            if (r0 == 0) goto L_0x0082
            boolean r0 = r1 instanceof X.AnonymousClass1OY
            if (r0 == 0) goto L_0x0082
            X.1vH r1 = (X.AbstractC42481vH) r1
            X.1Oa r1 = (X.AbstractC28551Oa) r1
            X.0mz r5 = r1.getFMessage()
            X.1IS r0 = r5.A0z
            boolean r0 = r8.contains(r0)
            if (r0 != 0) goto L_0x0082
            X.0n0 r2 = r3.getConversationCursorAdapter()
            int r1 = r2.A03(r5)
            int r0 = r2.A01()
            if (r1 > r0) goto L_0x0067
            java.util.ArrayList r0 = r2.A0Q
            boolean r0 = r0.contains(r5)
            if (r0 == 0) goto L_0x0082
        L_0x0067:
            boolean r0 = X.C28111Kr.A00
            if (r0 == 0) goto L_0x0085
        L_0x006b:
            r2 = 0
        L_0x006c:
            int r0 = r3.getChildCount()
            if (r2 >= r0) goto L_0x0085
            android.view.View r1 = r3.getChildAt(r2)
            boolean r0 = r1 instanceof X.AbstractC42481vH
            if (r0 == 0) goto L_0x007f
            X.1vH r1 = (X.AbstractC42481vH) r1
            r1.Ae9()
        L_0x007f:
            int r2 = r2 + 1
            goto L_0x006c
        L_0x0082:
            int r6 = r6 + 1
            goto L_0x0033
        L_0x0085:
            android.os.Handler r3 = r4.A4c
            java.lang.Runnable r2 = r4.A50
            r0 = 1000(0x3e8, double:4.94E-321)
            r3.postDelayed(r2, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3O5.onScrollStateChanged(android.widget.AbsListView, int):void");
    }
}
