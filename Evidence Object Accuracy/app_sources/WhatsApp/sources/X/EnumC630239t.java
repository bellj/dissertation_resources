package X;

/* renamed from: X.39t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC630239t {
    UPTO_DATE(0, true, true),
    FETCH_ERROR(1, false, false),
    NETWORK_ERROR(2, false, false),
    LANGUAGE_UNAVAILABLE(3, false, true);
    
    public final boolean fetchSuccessful;
    public final String fieldStatString;
    public final boolean gotDictionary;

    EnumC630239t(int i, boolean z, boolean z2) {
        this.gotDictionary = z;
        this.fetchSuccessful = z2;
        this.fieldStatString = r2;
    }
}
