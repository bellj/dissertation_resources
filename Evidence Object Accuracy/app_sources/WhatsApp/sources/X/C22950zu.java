package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0zu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22950zu {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C237913a A02;
    public final C20770wI A03;
    public final C238013b A04;
    public final C20720wD A05;
    public final C14830m7 A06;
    public final C14820m6 A07;
    public final C20660w7 A08;
    public final C20750wG A09;
    public final Map A0A = new HashMap();

    public C22950zu(C14900mE r2, C15570nT r3, C237913a r4, C20770wI r5, C238013b r6, C20720wD r7, C14830m7 r8, C14820m6 r9, C20660w7 r10, C20750wG r11) {
        this.A06 = r8;
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A08 = r10;
        this.A04 = r6;
        this.A05 = r7;
        this.A09 = r11;
        this.A07 = r9;
        this.A03 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006e, code lost:
        if (A01(1, r3, r23) != false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007b, code lost:
        if (A01(2, r3, r23) != false) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0088, code lost:
        if (A01(3, r3, r23) != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0094, code lost:
        if (A01(4, r3, r23) != false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x009f, code lost:
        if (A01(5, r3, r23) != false) goto L_0x00a1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A00(X.C454021l r20, boolean r21, boolean r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 495
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22950zu.A00(X.21l, boolean, boolean, boolean):void");
    }

    public final boolean A01(int i, long j, boolean z) {
        if (z) {
            Map map = this.A0A;
            Integer valueOf = Integer.valueOf(i);
            if (map.containsKey(valueOf) && j - ((Number) map.get(valueOf)).longValue() <= 60000) {
                return true;
            }
        }
        return false;
    }
}
