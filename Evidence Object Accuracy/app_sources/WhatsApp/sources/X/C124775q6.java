package X;

import android.app.Activity;
import android.content.Context;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.5q6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124775q6 extends Exception {
    public C124775q6(Context context, String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("PAY: ")));
        Activity A00 = AnonymousClass12P.A00(context);
        String string = A00.getString(R.string.npci_error_msg);
        A00.findViewById(R.id.error_layout).setVisibility(0);
        ((TextView) A00.findViewById(R.id.error_message)).setText(string);
    }
}
