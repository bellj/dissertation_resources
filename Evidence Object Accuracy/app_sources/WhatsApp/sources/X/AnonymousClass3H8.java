package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;

/* renamed from: X.3H8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3H8 {
    public static final byte[] A02 = {2};
    public static final byte[] A03 = {1};
    public final int A00;
    public final byte[] A01;

    public AnonymousClass3H8(int i, byte[] bArr) {
        this.A00 = i;
        this.A01 = bArr;
    }

    public AnonymousClass3H8 A00() {
        int i = this.A00 + 1;
        byte[] bArr = A02;
        byte[] bArr2 = this.A01;
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            C12990iw.A1S(DefaultCrypto.HMAC_SHA256, instance, bArr2);
            return new AnonymousClass3H8(i, instance.doFinal(bArr));
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public C31511af A01() {
        int i = this.A00;
        byte[] bArr = A03;
        byte[] bArr2 = this.A01;
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            C12990iw.A1S(DefaultCrypto.HMAC_SHA256, instance, bArr2);
            return new C31511af(i, instance.doFinal(bArr));
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
