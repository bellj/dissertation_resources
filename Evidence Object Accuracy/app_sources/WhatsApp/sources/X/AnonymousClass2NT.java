package X;

/* renamed from: X.2NT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NT {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final boolean A06;

    public AnonymousClass2NT(int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        this.A05 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A01 = i4;
        this.A06 = z;
        this.A04 = i5;
        this.A03 = i6;
    }
}
