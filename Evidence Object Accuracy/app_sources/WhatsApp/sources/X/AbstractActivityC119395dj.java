package X;

import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;

/* renamed from: X.5dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119395dj extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119395dj() {
        C117295Zj.A0p(this, 71);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiQrTabActivity indiaUpiQrTabActivity = (IndiaUpiQrTabActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiQrTabActivity);
            ActivityC13810kN.A10(A1M, indiaUpiQrTabActivity);
            ((ActivityC13790kL) indiaUpiQrTabActivity).A08 = ActivityC13790kL.A0S(r3, A1M, indiaUpiQrTabActivity, ActivityC13790kL.A0Y(A1M, indiaUpiQrTabActivity));
            indiaUpiQrTabActivity.A05 = (C21860y6) A1M.AE6.get();
            indiaUpiQrTabActivity.A04 = (C1329668y) A1M.A9d.get();
            indiaUpiQrTabActivity.A02 = C12970iu.A0Y(A1M);
            indiaUpiQrTabActivity.A0C = (C22190yg) A1M.AB6.get();
            indiaUpiQrTabActivity.A0A = (C1309960u) A1M.A1b.get();
            indiaUpiQrTabActivity.A06 = C117305Zk.A0T(A1M);
        }
    }
}
