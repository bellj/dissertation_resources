package X;

import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;

/* renamed from: X.5oG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123985oG extends AbstractC16350or {
    public final /* synthetic */ IndiaUpiDeviceBindStepActivity A00;

    public /* synthetic */ C123985oG(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity) {
        this.A00 = indiaUpiDeviceBindStepActivity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = this.A00;
        indiaUpiDeviceBindStepActivity.A32();
        IndiaUpiDeviceBindStepActivity.A02(indiaUpiDeviceBindStepActivity);
        return null;
    }
}
