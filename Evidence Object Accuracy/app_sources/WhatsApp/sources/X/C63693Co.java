package X;

import android.graphics.Bitmap;

/* renamed from: X.3Co  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63693Co {
    public AnonymousClass3HI A00;
    public AnonymousClass3FZ A01;
    public final AnonymousClass5XK A02;
    public final AnonymousClass5S6 A03;

    public C63693Co(AnonymousClass5XK r3, AnonymousClass3HI r4) {
        C106034v1 r1 = new C106034v1(this);
        this.A03 = r1;
        this.A02 = r3;
        this.A00 = r4;
        this.A01 = new AnonymousClass3FZ(r4, r1);
    }

    public boolean A00(Bitmap bitmap, int i) {
        try {
            this.A01.A00(i, bitmap);
            return true;
        } catch (IllegalStateException e) {
            Object[] objArr = {Integer.valueOf(i)};
            AbstractC12710iN r2 = AnonymousClass0UN.A00;
            if (r2.AJi(6)) {
                r2.A9H(C63693Co.class.getSimpleName(), String.format(null, "Rendering of frame unsuccessful. Frame number: %d", objArr), e);
            }
            return false;
        }
    }
}
