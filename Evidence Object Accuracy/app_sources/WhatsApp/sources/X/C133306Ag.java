package X;

/* renamed from: X.6Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133306Ag implements AnonymousClass6MU {
    public final /* synthetic */ C14580lf A00;
    public final /* synthetic */ AbstractActivityC121685jC A01;

    public C133306Ag(C14580lf r1, AbstractActivityC121685jC r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MU
    public void APo(C452120p r3) {
        this.A00.A02(Boolean.FALSE);
    }

    @Override // X.AnonymousClass6MU
    public void AX1(EnumC124545pi r3) {
        this.A00.A02(Boolean.valueOf(C12970iu.A1Z(r3, EnumC124545pi.A01)));
    }
}
