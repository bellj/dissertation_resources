package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2GE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GE {
    public static Drawable A00(Context context, int i) {
        Drawable A04 = AnonymousClass00T.A04(context, i);
        AnonymousClass009.A05(A04);
        return A04;
    }

    public static Drawable A01(Context context, int i, int i2) {
        Integer num;
        Drawable A04 = AnonymousClass00T.A04(context, i);
        AnonymousClass009.A05(A04);
        Drawable mutate = A04.mutate();
        try {
            num = Integer.valueOf(AnonymousClass00T.A00(context, i2));
        } catch (Resources.NotFoundException unused) {
            num = null;
        }
        return (num == null || num.intValue() == 0) ? mutate : A04(mutate, AnonymousClass00T.A00(context, i2));
    }

    public static Drawable A02(Context context, Drawable drawable) {
        ColorStateList A03 = AnonymousClass00T.A03(context, R.color.ripple_tint);
        if (Build.VERSION.SDK_INT >= 21 && A03 != null) {
            return new RippleDrawable(A03, drawable, null);
        }
        if (!(drawable instanceof AbstractC013806l)) {
            drawable = C015607k.A03(drawable);
        }
        C015607k.A07(PorterDuff.Mode.MULTIPLY, drawable);
        C015607k.A04(new ColorStateList(new int[][]{new int[]{16842919}, new int[0]}, new int[]{AnonymousClass00T.A00(context, R.color.ripple_compat_tint), AnonymousClass00T.A00(context, R.color.multiply_none)}), drawable);
        return drawable;
    }

    public static Drawable A03(Context context, Drawable drawable, int i) {
        return A04(drawable, AnonymousClass00T.A00(context, i));
    }

    public static Drawable A04(Drawable drawable, int i) {
        if (i == 0) {
            return drawable;
        }
        Drawable A03 = C015607k.A03(drawable);
        C015607k.A0A(A03, i);
        return A03;
    }

    public static void A05(Context context, ImageView imageView, int i) {
        A07(imageView, AnonymousClass00T.A00(context, i));
    }

    public static void A06(View view, ImageView imageView) {
        imageView.setBackgroundColor(view.getResources().getColor(R.color.image_placeholder_background_color));
        imageView.setImageDrawable(A01(view.getContext(), R.drawable.camera, R.color.image_placeholder_icon_color));
    }

    public static void A07(ImageView imageView, int i) {
        ColorStateList valueOf;
        C016307r.A01(PorterDuff.Mode.SRC_IN, imageView);
        if (i == 0) {
            valueOf = null;
        } else {
            valueOf = ColorStateList.valueOf(i);
        }
        C016307r.A00(valueOf, imageView);
    }

    public static void A08(TextView textView, int i) {
        if (i != 0) {
            Drawable[] compoundDrawables = textView.getCompoundDrawables();
            for (Drawable drawable : compoundDrawables) {
                if (drawable != null) {
                    drawable.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN));
                }
            }
        }
    }
}
