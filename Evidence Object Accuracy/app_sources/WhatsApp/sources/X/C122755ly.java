package X;

import android.content.Context;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.whatsapp.R;

/* renamed from: X.5ly  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122755ly extends AbstractC118835cS {
    public View A00;
    public ListView A01;
    public boolean A02 = false;
    public final Context A03;
    public final AnonymousClass01d A04;
    public final AbstractC16870pt A05;

    public C122755ly(View view, AnonymousClass01d r3, AbstractC16870pt r4) {
        super(view);
        this.A04 = r3;
        this.A05 = r4;
        this.A03 = view.getContext();
        this.A00 = AnonymousClass028.A0D(view, R.id.view_more_row);
        this.A01 = (ListView) AnonymousClass028.A0D(view, R.id.timeline_list_view);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r6, int i) {
        C123145mg r62 = (C123145mg) r6;
        C117625aI r0 = new C117625aI(this.A03, r62);
        ListView listView = this.A01;
        listView.setAdapter((ListAdapter) r0);
        if (!r62.A01 || this.A02) {
            this.A00.setVisibility(8);
            listView.setVisibility(0);
            return;
        }
        View view = this.A00;
        view.setVisibility(0);
        listView.setVisibility(8);
        C117295Zj.A0n(view, this, 145);
    }
}
