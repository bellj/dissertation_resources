package X;

import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.0kD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13720kD {
    public C15170mg A00;
    public Boolean A01;
    public boolean A02;
    public boolean A03;
    public final C13270jQ A04;
    public final /* synthetic */ FirebaseInstanceId A05;

    public C13720kD(C13270jQ r1, FirebaseInstanceId firebaseInstanceId) {
        this.A05 = firebaseInstanceId;
        this.A04 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e0, code lost:
        if (((X.C15700nl) r0.A03.get()).A03.get() == false) goto L_0x00e2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean A00() {
        /*
        // Method dump skipped, instructions count: 234
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13720kD.A00():boolean");
    }
}
