package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.3Lq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65933Lq implements Parcelable {
    public static final C65893Lm CREATOR = new C65893Lm();
    public final String A00;
    public final List A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C65933Lq) {
                C65933Lq r5 = (C65933Lq) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.A00;
        int i = 0;
        int hashCode = (str == null ? 0 : str.hashCode()) * 31;
        List list = this.A01;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public C65933Lq(String str, List list) {
        this.A00 = str;
        this.A01 = list;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarGetStickersEntity(id=");
        A0k.append((Object) this.A00);
        A0k.append(", stickers=");
        return C12960it.A0a(this.A01, A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeString(this.A00);
        parcel.writeTypedList(this.A01);
    }
}
