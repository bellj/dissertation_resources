package X;

import java.security.AlgorithmParameterGeneratorSpi;
import java.security.SecureRandom;

/* renamed from: X.5HU  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5HU extends AlgorithmParameterGeneratorSpi {
    public SecureRandom A00;
    public final AnonymousClass5S2 A01 = new AnonymousClass5GT();

    @Override // java.security.AlgorithmParameterGeneratorSpi
    public void engineInit(int i, SecureRandom secureRandom) {
        this.A00 = secureRandom;
    }
}
