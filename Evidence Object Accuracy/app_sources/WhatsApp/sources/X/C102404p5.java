package X;

import android.view.View;
import android.widget.AdapterView;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;

/* renamed from: X.4p5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102404p5 implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ GoogleDriveNewUserSetupActivity A00;

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }

    public C102404p5(GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity) {
        this.A00 = googleDriveNewUserSetupActivity;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity = this.A00;
        if (googleDriveNewUserSetupActivity.A04.getVisibility() == 0) {
            googleDriveNewUserSetupActivity.A2o(null, String.valueOf(adapterView.getItemAtPosition(i)));
        }
    }
}
