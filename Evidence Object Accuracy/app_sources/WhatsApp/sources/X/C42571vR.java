package X;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.MenuItem;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1vR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42571vR implements AbstractC13940ka {
    public AbstractC14640lm A00;
    public final int A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C16170oZ A04;
    public final C18330sH A05;
    public final C14650lo A06;
    public final C15550nR A07;
    public final C20730wE A08;
    public final C250417w A09;
    public final C42581vS A0A;
    public final C14830m7 A0B;
    public final C14820m6 A0C;
    public final AnonymousClass018 A0D;
    public final C20650w6 A0E;
    public final C19990v2 A0F;
    public final C15680nj A0G;
    public final C15600nX A0H;
    public final C20710wC A0I;
    public final C20220vP A0J;
    public final C15860o1 A0K;
    public final AnonymousClass12F A0L;
    public final C253018w A0M;
    public final AnonymousClass12U A0N;
    public final AnonymousClass198 A0O;
    public final C254219i A0P;
    public final C255719x A0Q;
    public final AbstractC14440lR A0R;
    public final WeakReference A0S;
    public final WeakReference A0T;

    @Override // X.AbstractC13940ka
    public void AR0() {
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
    }

    public C42571vR(Context context, AnonymousClass01F r3, C14900mE r4, C15570nT r5, C16170oZ r6, C18330sH r7, C14650lo r8, C15550nR r9, C20730wE r10, C250417w r11, C42581vS r12, C14830m7 r13, C14820m6 r14, AnonymousClass018 r15, C20650w6 r16, C19990v2 r17, C15680nj r18, C15600nX r19, C20710wC r20, C20220vP r21, C15860o1 r22, AnonymousClass12F r23, C253018w r24, AnonymousClass12U r25, AnonymousClass198 r26, C254219i r27, C255719x r28, AbstractC14440lR r29, int i) {
        this.A0S = new WeakReference(context);
        this.A0T = new WeakReference(r3);
        this.A0B = r13;
        this.A0M = r24;
        this.A02 = r4;
        this.A03 = r5;
        this.A0R = r29;
        this.A0F = r17;
        this.A0N = r25;
        this.A0E = r16;
        this.A04 = r6;
        this.A07 = r9;
        this.A0D = r15;
        this.A0I = r20;
        this.A0L = r23;
        this.A0O = r26;
        this.A0K = r22;
        this.A05 = r7;
        this.A0P = r27;
        this.A08 = r10;
        this.A0J = r21;
        this.A0C = r14;
        this.A0G = r18;
        this.A06 = r8;
        this.A09 = r11;
        this.A0Q = r28;
        this.A0H = r19;
        this.A0A = r12;
        this.A01 = i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0130  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.view.ContextMenu r8, X.AbstractC14640lm r9, boolean r10, boolean r11) {
        /*
        // Method dump skipped, instructions count: 370
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42571vR.A00(android.view.ContextMenu, X.0lm, boolean, boolean):void");
    }

    public void A01(Set set) {
        boolean z;
        AnonymousClass01F r3 = (AnonymousClass01F) this.A0T.get();
        Context context = (Context) this.A0S.get();
        if (context != null && r3 != null) {
            Iterator it = set.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!this.A0G.A0D((AbstractC14640lm) it.next())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z) {
                AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], R.string.unpin_all_dialog_message);
                A01.A05 = R.string.unpin_all_dialog_title;
                A01.A0B = new Object[0];
                A01.A02(new DialogInterface.OnClickListener(set) { // from class: X.4gY
                    public final /* synthetic */ Set A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        C42571vR r4 = C42571vR.this;
                        r4.A0R.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r4, 27, this.A01));
                    }
                }, R.string.unpin_all_dialog_positive_button);
                DialogInterface$OnClickListenerC97494hA r1 = DialogInterface$OnClickListenerC97494hA.A00;
                A01.A04 = R.string.unpin_all_dialog_cancel_button;
                A01.A07 = r1;
                A01.A01().A1F(r3, null);
                return;
            }
            this.A02.A0E(context.getResources().getQuantityString(R.plurals.cannot_pin, 3, 3), 0);
        }
    }

    public boolean A02(int i) {
        UserJid userJid;
        if (this.A00 == null || i != this.A01) {
            return false;
        }
        this.A08.A06();
        C15370n3 A0B = this.A07.A0B(this.A00);
        if (A0B.A0J() && (userJid = (UserJid) A0B.A0B(UserJid.class)) != null) {
            this.A06.A04(this, userJid, null);
        }
        this.A0O.A00();
        return true;
    }

    public boolean A03(MenuItem menuItem, AnonymousClass01E r13, ActivityC000900k r14) {
        AbstractC14440lR r3;
        int i;
        AnonymousClass01F r4 = (AnonymousClass01F) this.A0T.get();
        if (!(r4 == null || this.A00 == null)) {
            if (menuItem.getItemId() == R.id.menuitem_conversations_contact_info) {
                C15370n3 A0B = this.A07.A0B(this.A00);
                if (A0B.A0C != null) {
                    r14.startActivity(new C14960mK().A0f(r14, A0B));
                }
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_conversations_delete) {
                this.A0Q.A06(r14).A00(new AbstractC14590lg(r4, this) { // from class: X.5AL
                    public final /* synthetic */ AnonymousClass01F A00;
                    public final /* synthetic */ C42571vR A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        C42571vR r1 = this.A01;
                        AnonymousClass01F r32 = this.A00;
                        C42581vS r0 = r1.A0A;
                        AbstractC14640lm r2 = r1.A00;
                        r0.A0E.A07(r2, new C1115159r(r32, r0, r2));
                    }
                });
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_conversations_add_new_contact) {
                Intent A00 = this.A0P.A00(this.A07.A0B(this.A00), this.A00, true);
                AnonymousClass009.A05(r14);
                A00.setComponent(A00.resolveActivity(r14.getPackageManager()));
                if (A00.getComponent() != null) {
                    r13.startActivityForResult(A00, this.A01);
                    this.A0O.A02(true, 7);
                    return true;
                }
                Log.w("conversations/context system contact list could not found");
                this.A02.A07(R.string.unimplemented, 0);
                return true;
            } else if (menuItem.getItemId() == R.id.menuitem_conversations_add_to_existing_contact) {
                try {
                    r13.startActivityForResult(this.A0P.A00(this.A07.A0B(this.A00), this.A00, false), this.A01);
                    this.A0O.A02(false, 7);
                    return true;
                } catch (ActivityNotFoundException unused) {
                    this.A02.A07(R.string.activity_not_found, 0);
                    return true;
                }
            } else {
                if (menuItem.getItemId() == R.id.menuitem_conversations_pin) {
                    Set A0D = this.A0K.A0D();
                    if (A0D.size() >= 3) {
                        A01(A0D);
                        return true;
                    }
                    r3 = this.A0R;
                    i = 35;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_unpin) {
                    r3 = this.A0R;
                    i = 34;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_mute) {
                    MuteDialogFragment.A01(Collections.singleton(this.A00)).A1F(r4, null);
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_unmute) {
                    AbstractC14640lm r32 = this.A00;
                    if (C16970q3.A03(this.A0C, this.A0F, r32)) {
                        C16970q3.A00(r14, r14.findViewById(R.id.result_list), this.A04, this.A00);
                        return true;
                    }
                    r3 = this.A0R;
                    i = 36;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_archive) {
                    C42581vS r42 = this.A0A;
                    AbstractC14640lm r6 = this.A00;
                    r42.A07.A05(r6, true);
                    r42.A0C.A04(r6, 3, 0, 0);
                    r42.A0F.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r42, 26, r6));
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_unarchive) {
                    C42581vS r2 = this.A0A;
                    AbstractC14640lm r62 = this.A00;
                    r2.A07.A05(r62, false);
                    r2.A0C.A04(r62, 4, 0, 0);
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_mark_read) {
                    this.A09.A02(this.A00, true, true);
                    this.A0J.A07();
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_mark_unread) {
                    this.A09.A01(this.A00, true);
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_create_shortcuit) {
                    this.A05.A05(this.A07.A0B(this.A00));
                    return true;
                } else if (menuItem.getItemId() == R.id.menuitem_conversations_group_info) {
                    C15370n3 A0B2 = this.A07.A0B(this.A00);
                    if (C15380n4.A0F(A0B2.A0D)) {
                        AnonymousClass009.A05(r14);
                        r14.startActivity(C14960mK.A0K(r14, A0B2.A0D), null);
                        return true;
                    }
                    Intent A0O = C14960mK.A0O(r14, A0B2.A0D, true, false, true);
                    C35741ib.A00(A0O, r14.getClass().getSimpleName());
                    r14.startActivity(A0O, null);
                    return true;
                }
                r3.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this, i));
                return true;
            }
        }
        return false;
    }
}
