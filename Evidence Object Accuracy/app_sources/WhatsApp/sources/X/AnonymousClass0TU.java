package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0TU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TU {
    public static final List A00 = new ArrayList();
    public static volatile AbstractC12710iN A01 = C08630bX.A01;

    static {
        ((C08630bX) A01).A00 = 5;
        AnonymousClass0UN.A00 = A01;
    }

    public static void A00(Object obj, String str) {
        if (A01.AJi(4)) {
            String formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe(str, obj);
            if (A01.AJi(4)) {
                A01.AIU("OpticE2EConfig", formatStrLocaleSafe);
            }
        }
    }

    public static void A01(Throwable th, Object... objArr) {
        if (A01.AJi(6)) {
            String formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe("%s hit fixed orientation exception", objArr);
            if (A01.AJi(6)) {
                A01.AgK("FixedOrientationCompat", formatStrLocaleSafe, th);
            }
        }
    }
}
