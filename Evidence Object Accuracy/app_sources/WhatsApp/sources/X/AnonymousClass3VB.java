package X;

import android.graphics.Bitmap;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.biz.catalog.CatalogMediaViewFragment;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3VB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VB implements AnonymousClass2E5 {
    public boolean A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ CatalogMediaViewFragment A02;
    public final /* synthetic */ PhotoView A03;

    public AnonymousClass3VB(CatalogMediaViewFragment catalogMediaViewFragment, PhotoView photoView, int i) {
        this.A02 = catalogMediaViewFragment;
        this.A03 = photoView;
        this.A01 = i;
    }

    @Override // X.AnonymousClass2E5
    public void AS6(Bitmap bitmap, C68203Um r7, boolean z) {
        if (this.A00) {
            CatalogMediaViewFragment catalogMediaViewFragment = this.A02;
            AnonymousClass3YP r1 = new AbstractC35501i8(bitmap, this, this.A03) { // from class: X.3YP
                public final /* synthetic */ Bitmap A00;
                public final /* synthetic */ AnonymousClass3VB A01;
                public final /* synthetic */ PhotoView A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                }

                @Override // X.AbstractC35501i8
                public final void AXp(boolean z2) {
                    ActivityC000900k A0B;
                    AnonymousClass3VB r0 = this.A01;
                    PhotoView photoView = this.A02;
                    Bitmap bitmap2 = this.A00;
                    if (z2 && (A0B = r0.A02.A0B()) != null && !A0B.isFinishing()) {
                        photoView.A05(bitmap2);
                    }
                }
            };
            if (!((MediaViewBaseFragment) catalogMediaViewFragment).A0E) {
                r1.AXp(true);
            } else {
                ((MediaViewBaseFragment) catalogMediaViewFragment).A0A = r1;
            }
        } else {
            this.A00 = true;
            PhotoView photoView = this.A03;
            photoView.A05(bitmap);
            CatalogMediaViewFragment catalogMediaViewFragment2 = this.A02;
            String str = catalogMediaViewFragment2.A08;
            if (str != null) {
                if (str.equals(AnonymousClass19N.A00(this.A01, catalogMediaViewFragment2.A02.A0D))) {
                    photoView.post(new RunnableBRunnable0Shape14S0100000_I1(catalogMediaViewFragment2.A0C(), 24));
                }
            }
        }
    }
}
