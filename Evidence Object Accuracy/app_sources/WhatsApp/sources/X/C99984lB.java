package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99984lB implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29981Vm(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29981Vm[i];
    }
}
