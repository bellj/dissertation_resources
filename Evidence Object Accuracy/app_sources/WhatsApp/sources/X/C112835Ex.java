package X;

import java.util.Iterator;

/* renamed from: X.5Ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112835Ex implements AnonymousClass1WO {
    public final AnonymousClass1J7 A00;
    public final AnonymousClass1WO A01;

    public C112835Ex(AnonymousClass1J7 r1, AnonymousClass1WO r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        return new AnonymousClass5DN(this);
    }
}
