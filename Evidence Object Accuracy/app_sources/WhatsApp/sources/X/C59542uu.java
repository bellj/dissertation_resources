package X;

/* renamed from: X.2uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59542uu extends C37171lc {
    public final AbstractView$OnClickListenerC34281fs A00;
    public final String A01;

    public C59542uu(AbstractView$OnClickListenerC34281fs r2, String str) {
        super(AnonymousClass39o.A0I);
        this.A00 = r2;
        this.A01 = str;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        String str = this.A01;
        String str2 = ((C59542uu) obj).A01;
        if (str == null) {
            if (str2 == null) {
                return true;
            }
        } else if (str.equals(str2)) {
            return true;
        }
        return false;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return super.A00.hashCode();
    }
}
