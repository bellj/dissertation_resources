package X;

import java.util.concurrent.ThreadFactory;

/* renamed from: X.0jj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class ThreadFactoryC13440jj implements ThreadFactory {
    public static final ThreadFactory A00 = new ThreadFactoryC13440jj();

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "firebase-iid-executor");
    }
}
