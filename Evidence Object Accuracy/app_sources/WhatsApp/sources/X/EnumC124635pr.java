package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124635pr extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124635pr[] A00;
    public static final EnumC124635pr A01;
    public static final EnumC124635pr A02;
    public static final EnumC124635pr A03;
    public static final EnumC124635pr A04;
    public static final EnumC124635pr A05;
    public static final EnumC124635pr A06;
    public final String fieldName;

    public static EnumC124635pr valueOf(String str) {
        return (EnumC124635pr) Enum.valueOf(EnumC124635pr.class, str);
    }

    public static EnumC124635pr[] values() {
        return (EnumC124635pr[]) A00.clone();
    }

    static {
        EnumC124635pr r9 = new EnumC124635pr("REFERENCE_ID", "reference_id", 0);
        A02 = r9;
        EnumC124635pr r8 = new EnumC124635pr("PAYMENT_CONFIGURATION", "payment_configuration", 1);
        A01 = r8;
        EnumC124635pr r7 = new EnumC124635pr("THUMB_IMAGE_BLOB", "thumb_image_blob", 2);
        A04 = r7;
        EnumC124635pr r6 = new EnumC124635pr("TITLE", "title", 3);
        A05 = r6;
        EnumC124635pr r5 = new EnumC124635pr("TOTAL_AMOUNT", "total_amount", 4);
        A06 = r5;
        EnumC124635pr r4 = new EnumC124635pr("STATUS", "status", 5);
        A03 = r4;
        EnumC124635pr r1 = new EnumC124635pr("TYPE", "type", 6);
        EnumC124635pr[] r0 = new EnumC124635pr[7];
        C72453ed.A1F(r9, r8, r7, r6, r0);
        C117305Zk.A1M(r5, r4, r0);
        r0[6] = r1;
        A00 = r0;
    }

    public EnumC124635pr(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
