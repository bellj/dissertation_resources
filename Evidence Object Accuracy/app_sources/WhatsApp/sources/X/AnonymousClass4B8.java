package X;

/* renamed from: X.4B8  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4B8 {
    A02(0),
    A01(1),
    A03(2);
    
    public final int value;

    AnonymousClass4B8(int i) {
        this.value = i;
    }
}
