package X;

/* renamed from: X.4Z0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Z0 {
    public static final int[] A00 = {1769172845, 1769172786, 1769172787, 1769172788, 1769172789, 1769172790, 1769172793, 1635148593, 1752589105, 1751479857, 1635135537, 1836069937, 1836069938, 862401121, 862401122, 862417462, 862417718, 862414134, 862414646, 1295275552, 1295270176, 1714714144, 1801741417, 1295275600, 1903435808, 1297305174, 1684175153, 1769172332, 1885955686};

    public static boolean A00(AnonymousClass5Yf r21, boolean z) {
        boolean z2;
        long length = r21.getLength();
        long j = 4096;
        if (length != -1 && length <= 4096) {
            j = length;
        }
        int i = (int) j;
        C95304dT A05 = C95304dT.A05(64);
        boolean z3 = false;
        int i2 = 0;
        boolean z4 = false;
        while (i2 < i) {
            A05.A0Q(8);
            if (!r21.AZ5(A05.A02, 0, 8, true)) {
                break;
            }
            long A0I = A05.A0I();
            int A07 = A05.A07();
            int i3 = 16;
            if (A0I == 1) {
                r21.AZ4(A05.A02, 8, 8);
                A05.A0R(16);
                A0I = A05.A0H();
            } else {
                if (A0I == 0 && length != -1) {
                    A0I = ((long) 8) + (length - r21.AFf());
                }
                i3 = 8;
            }
            long j2 = (long) i3;
            if (A0I < j2) {
                return z3;
            }
            i2 += i3;
            if (A07 != 1836019574) {
                if (A07 == 1836019558 || A07 == 1836475768) {
                    z2 = true;
                    break;
                }
                if ((((long) i2) + A0I) - j2 >= ((long) i)) {
                    break;
                }
                int i4 = (int) (A0I - j2);
                i2 += i4;
                if (A07 == 1718909296) {
                    if (i4 < 8) {
                        return false;
                    }
                    A05.A0Q(i4);
                    C95304dT.A06(r21, A05, i4);
                    int i5 = i4 >> 2;
                    for (int i6 = 0; i6 < i5; i6++) {
                        if (i6 != 1) {
                            int A072 = A05.A07();
                            if ((A072 >>> 8) != 3368816) {
                                for (int i7 : A00) {
                                    if (i7 != A072) {
                                    }
                                }
                                continue;
                            }
                            z4 = true;
                            break;
                        }
                        A05.A0T(4);
                    }
                    if (!z4) {
                        return false;
                    }
                } else if (i4 != 0) {
                    r21.A5r(i4);
                }
                z3 = false;
            } else {
                i += (int) A0I;
                if (length != -1 && ((long) i) > length) {
                    i = (int) length;
                }
            }
        }
        z2 = false;
        return z4 && z == z2;
    }
}
