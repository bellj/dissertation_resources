package X;

import android.view.DisplayCutout;

/* renamed from: X.0Tz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06530Tz {
    public static final int A00(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetBottom();
    }

    public static final int A01(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetLeft();
    }

    public static final int A02(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetRight();
    }

    public static final int A03(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetTop();
    }
}
