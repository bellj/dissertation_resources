package X;

import android.content.Context;
import android.os.Looper;
import android.util.Base64;

/* renamed from: X.3o3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77903o3 extends AbstractC77963o9 {
    public final C108194ye A00;

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12800000;
    }

    public C77903o3(Context context, Looper looper, C108194ye r13, AbstractC14980mM r14, AbstractC15000mO r15, AnonymousClass3BW r16) {
        super(context, looper, r14, r15, r16, 68);
        C93334a0 r2 = new C93334a0(r13 == null ? C108194ye.A02 : r13);
        byte[] bArr = new byte[16];
        C88334Fg.A00.nextBytes(bArr);
        r2.A01 = Base64.encodeToString(bArr, 11);
        this.A00 = new C108194ye(r2);
    }
}
