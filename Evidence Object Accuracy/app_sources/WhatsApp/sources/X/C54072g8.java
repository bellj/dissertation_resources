package X;

/* renamed from: X.2g8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54072g8 extends AnonymousClass02K {
    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        C16700pc.A0F(obj, obj2);
        return obj.equals(obj2);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        AbstractC87964Ds r5 = (AbstractC87964Ds) obj;
        AbstractC87964Ds r6 = (AbstractC87964Ds) obj2;
        C16700pc.A0I(r5, r6);
        if (!C16700pc.A0O(r5.getClass(), r6.getClass())) {
            return false;
        }
        if (!(r5 instanceof C58722rH) || !(r6 instanceof C58722rH)) {
            if ((r5 instanceof C58702rF) && (r6 instanceof C58702rF)) {
                return C16700pc.A0O(((C58702rF) r5).A01, ((C58702rF) r6).A01);
            }
            if (!(r5 instanceof C58712rG) || !(r6 instanceof C58712rG)) {
                return r5.equals(r6);
            }
            return C16700pc.A0O(((C58712rG) r5).A00, ((C58712rG) r6).A00);
        } else if (((C58722rH) r5).A00 == ((C58722rH) r6).A00) {
            return true;
        } else {
            return false;
        }
    }
}
