package X;

import android.content.Context;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2Pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50452Pp implements AbstractC50412Pl {
    public AnonymousClass1AA A00;
    public AnonymousClass18T A01;
    public C22710zW A02;

    @Override // X.AbstractC50412Pl
    public void AZA(Context context, AbstractC15340mz r15, C16470p4 r16, int i) {
        AnonymousClass1ZD r2 = r16.A01;
        if (r2 != null) {
            C63593Ce r3 = new C63593Ce(this.A01, this.A02);
            UserJid A0C = r15.A0C();
            AnonymousClass009.A05(A0C);
            String str = r2.A06;
            AnonymousClass009.A05(str);
            AnonymousClass1IS r6 = r15.A0z;
            String str2 = r2.A09;
            String str3 = r2.A07;
            String str4 = r2.A04.A07;
            boolean z = false;
            if (i == 1) {
                z = true;
            }
            r3.A00(context, A0C, r6, str, str2, str3, str4, 0, z);
        }
    }
}
