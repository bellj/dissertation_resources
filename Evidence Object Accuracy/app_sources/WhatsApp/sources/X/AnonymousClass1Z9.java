package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Z9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z9 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100264ld();
    public boolean A00;
    public final AnonymousClass1Z8 A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1Z9(AnonymousClass1Z8 r1, boolean z) {
        this.A00 = z;
        this.A01 = r1;
    }

    public AnonymousClass1Z9(Parcel parcel) {
        this.A01 = (AnonymousClass1Z8) parcel.readParcelable(AnonymousClass1Z8.class.getClassLoader());
        this.A00 = parcel.readByte() != 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A01, i);
        parcel.writeByte(this.A00 ? (byte) 1 : 0);
    }
}
