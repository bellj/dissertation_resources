package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0BI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BI extends Animation {
    public final /* synthetic */ AnonymousClass0BD A00;

    public AnonymousClass0BI(AnonymousClass0BD r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass0BD r2 = this.A00;
        int i = r2.A09;
        r2.setTargetOffsetTopAndBottom((i + ((int) (((float) (r2.A0B - i)) * f))) - r2.A0K.getTop());
    }
}
