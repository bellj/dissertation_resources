package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.whatsapp.calling.CallDetailsLayout;

/* renamed from: X.3hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74343hp extends AnonymousClass04v {
    public final /* synthetic */ CallDetailsLayout A00;

    public C74343hp(CallDetailsLayout callDetailsLayout) {
        this.A00 = callDetailsLayout;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() != 2048) {
            super.A01(view, accessibilityEvent);
        }
    }
}
