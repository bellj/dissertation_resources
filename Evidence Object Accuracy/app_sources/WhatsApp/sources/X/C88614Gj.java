package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4Gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88614Gj {
    public static Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put(AnonymousClass1TJ.A1Q, "MD2");
        A00.put(AnonymousClass1TJ.A1S, "MD4");
        A00.put(AnonymousClass1TJ.A1U, "MD5");
        A00.put(AnonymousClass1TW.A07, "SHA-1");
        A00.put(AnonymousClass1TY.A0n, "SHA-224");
        A00.put(AnonymousClass1TY.A0o, "SHA-256");
        A00.put(AnonymousClass1TY.A0p, "SHA-384");
        A00.put(AnonymousClass1TY.A0u, "SHA-512");
        A00.put(AbstractC72423eZ.A0J, "RIPEMD-128");
        A00.put(AbstractC72423eZ.A0K, "RIPEMD-160");
        A00.put(AbstractC72423eZ.A0L, "RIPEMD-128");
        A00.put(AbstractC116915Xk.A05, "RIPEMD-128");
        A00.put(AbstractC116915Xk.A06, "RIPEMD-160");
        A00.put(AbstractC116985Xr.A0I, "GOST3411");
        A00.put(AbstractC116965Xp.A0J, "Tiger");
        A00.put(AbstractC116915Xk.A07, "Whirlpool");
        A00.put(AnonymousClass1TY.A0q, "SHA3-224");
        A00.put(AnonymousClass1TY.A0r, "SHA3-256");
        A00.put(AnonymousClass1TY.A0s, "SHA3-384");
        A00.put(AnonymousClass1TY.A0t, "SHA3-512");
        A00.put(AbstractC117005Xt.A0Z, "SM3");
    }
}
