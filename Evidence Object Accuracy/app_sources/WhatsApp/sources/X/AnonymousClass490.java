package X;

import java.io.IOException;

/* renamed from: X.490  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass490 extends IOException {
    public AnonymousClass490(IOException iOException) {
        super(iOException);
    }

    public AnonymousClass490(String str) {
        super(str);
    }
}
