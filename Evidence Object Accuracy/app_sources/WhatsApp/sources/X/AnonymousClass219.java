package X;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.219  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass219 implements AnonymousClass21A {
    public final int A00;
    public final AnonymousClass21E A01;
    public final Class A02;
    public final String A03;
    public final Set A04;

    public AnonymousClass219(int i, String str, Object obj) {
        this.A00 = i;
        this.A03 = str;
        this.A02 = String.class;
        this.A01 = new AnonymousClass21E(String.class, obj);
        this.A04 = null;
    }

    public AnonymousClass219(List list) {
        this.A00 = 10;
        this.A03 = "platform";
        this.A02 = String.class;
        this.A04 = new HashSet(list.size());
        for (Object obj : list) {
            this.A04.add(new AnonymousClass21E(String.class, obj));
        }
        this.A01 = null;
    }

    @Override // X.AnonymousClass21A
    public boolean AJL(C22020yM r6) {
        try {
            String str = this.A03;
            Map map = r6.A00;
            if (map.containsKey(str)) {
                AnonymousClass21E r3 = new AnonymousClass21E(this.A02, map.get(str));
                int i = this.A00;
                switch (i) {
                    case 3:
                    case 4:
                        return r3.equals(this.A01);
                    case 5:
                        return !r3.equals(this.A01);
                    case 6:
                        if (r3.compareTo(this.A01) >= 0) {
                            return false;
                        }
                        break;
                    case 7:
                        if (r3.compareTo(this.A01) <= 0) {
                            return false;
                        }
                        break;
                    case 8:
                        if (r3.compareTo(this.A01) > 0) {
                            return false;
                        }
                        break;
                    case 9:
                        if (r3.compareTo(this.A01) < 0) {
                            return false;
                        }
                        break;
                    case 10:
                        Set set = this.A04;
                        AnonymousClass009.A05(set);
                        return set.contains(r3);
                    default:
                        StringBuilder sb = new StringBuilder("Operator with code ");
                        sb.append(i);
                        sb.append(" is not currently supported");
                        throw new IllegalStateException(sb.toString());
                }
                return true;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" has not been set on UserInfo");
            throw new IllegalArgumentException(sb2.toString());
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass219)) {
            return false;
        }
        AnonymousClass219 r4 = (AnonymousClass219) obj;
        if (!this.A03.equals(r4.A03) || this.A00 != r4.A00) {
            return false;
        }
        AnonymousClass21E r1 = this.A01;
        if (r1 == null) {
            if (r4.A01 != null) {
                return false;
            }
        } else if (!r1.equals(r4.A01)) {
            return false;
        }
        Set set = this.A04;
        if (set == null) {
            if (r4.A04 != null) {
                return false;
            }
        } else if (!set.equals(r4.A04)) {
            return false;
        }
        if (this.A02.equals(r4.A02)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, Integer.valueOf(this.A00), this.A01, this.A04, this.A02});
    }
}
