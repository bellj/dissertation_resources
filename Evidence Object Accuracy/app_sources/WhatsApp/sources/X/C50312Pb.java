package X;

/* renamed from: X.2Pb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50312Pb implements AnonymousClass01N {
    public final int A00;
    public final AnonymousClass2FL A01;
    public final C48722Hj A02;
    public final AnonymousClass2P6 A03;
    public final AnonymousClass01J A04;

    public C50312Pb(AnonymousClass2FL r1, C48722Hj r2, AnonymousClass2P6 r3, AnonymousClass01J r4, int i) {
        this.A04 = r4;
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A00 = i;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        int i = this.A00;
        if (i == 0) {
            return new AnonymousClass2Q0(this);
        }
        if (i == 1) {
            return new C50542Py(this);
        }
        if (i == 2) {
            return new C50532Px(this);
        }
        throw new AssertionError(i);
    }
}
