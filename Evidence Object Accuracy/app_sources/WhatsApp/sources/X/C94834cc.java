package X;

/* renamed from: X.4cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94834cc {
    public int A00;
    public int A01;
    public int A02;
    public AbstractC470728v A03;
    public String A04;

    public C94834cc(int i) {
        this.A03 = null;
        this.A04 = null;
        this.A02 = i;
        this.A00 = 1;
        this.A01 = 0;
    }

    public C94834cc(AbstractC470728v r3, int i) {
        this.A03 = r3;
        this.A04 = null;
        this.A02 = 0;
        this.A00 = 0;
        this.A01 = i;
    }

    public C94834cc(String str, int i) {
        this.A03 = null;
        this.A04 = str;
        this.A02 = i;
        this.A00 = 1;
        this.A01 = 0;
    }
}
