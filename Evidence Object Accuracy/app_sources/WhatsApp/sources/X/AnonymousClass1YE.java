package X;

import android.database.sqlite.SQLiteStatement;

/* renamed from: X.1YE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YE {
    public final SQLiteStatement A00;

    public AnonymousClass1YE(SQLiteStatement sQLiteStatement) {
        this.A00 = sQLiteStatement;
    }

    public int A00() {
        return this.A00.executeUpdateDelete();
    }

    public void A01(int i, long j) {
        this.A00.bindLong(i, j);
    }

    public void A02(int i, String str) {
        this.A00.bindString(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass1YE) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
