package X;

import android.graphics.Bitmap;
import com.whatsapp.storage.StorageUsageMediaPreviewView;

/* renamed from: X.3XG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XG implements AnonymousClass23E {
    public final /* synthetic */ AbstractC35601iM A00;
    public final /* synthetic */ AnonymousClass2T3 A01;
    public final /* synthetic */ AnonymousClass23D A02;
    public final /* synthetic */ StorageUsageMediaPreviewView A03;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XG(AbstractC35601iM r1, AnonymousClass2T3 r2, AnonymousClass23D r3, StorageUsageMediaPreviewView storageUsageMediaPreviewView) {
        this.A03 = storageUsageMediaPreviewView;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        AnonymousClass2T3 r1 = this.A01;
        r1.setBackgroundColor(this.A03.A07);
        r1.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        Bitmap bitmap2 = bitmap;
        AnonymousClass2T3 r5 = this.A01;
        if (r5.getTag() == this.A02) {
            AbstractC35601iM r4 = this.A00;
            if (bitmap == StorageUsageMediaPreviewView.A0B) {
                bitmap2 = null;
            }
            StorageUsageMediaPreviewView storageUsageMediaPreviewView = this.A03;
            AnonymousClass3GG.A01(bitmap2, storageUsageMediaPreviewView.A09, r4, r5, storageUsageMediaPreviewView.A07, !z);
        }
    }
}
