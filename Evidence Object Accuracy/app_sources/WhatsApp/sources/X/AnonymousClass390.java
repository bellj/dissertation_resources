package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.390  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass390 extends AbstractC16350or {
    public final C242114q A00;
    public final AnonymousClass1Kp A01;
    public final Set A02;

    public AnonymousClass390(C242114q r2, AbstractC14640lm r3, AnonymousClass1Kp r4) {
        this.A00 = r2;
        HashSet A12 = C12970iu.A12();
        this.A02 = A12;
        A12.add(r3);
        this.A01 = r4;
    }

    public AnonymousClass390(C242114q r2, AnonymousClass1Kp r3, Set set) {
        this.A00 = r2;
        this.A02 = new HashSet(set);
        this.A01 = r3;
    }
}
