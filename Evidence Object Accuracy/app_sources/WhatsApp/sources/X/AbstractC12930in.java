package X;

import java.io.Closeable;

/* renamed from: X.0in  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12930in extends Closeable {
    byte AZm(int i);

    int AZr(byte[] bArr, int i, int i2, int i3);

    boolean isClosed();

    int size();
}
