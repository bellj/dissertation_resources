package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.5x9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129085x9 {
    public final C128885wp A00;
    public final C128885wp A01;
    public final C128885wp A02;
    public final C120955h1 A03;
    public final List A04;

    public C129085x9(C128885wp r2, C128885wp r3, C128885wp r4, C120955h1 r5, List list) {
        this.A04 = Collections.unmodifiableList(list);
        this.A02 = r2;
        this.A01 = r3;
        this.A00 = r4;
        this.A03 = r5;
    }

    public Map A00() {
        HashMap A11 = C12970iu.A11();
        ArrayList A0l = C12960it.A0l();
        for (C127655uq r5 : this.A04) {
            HashMap A112 = C12970iu.A11();
            String str = r5.A02;
            if (str != null) {
                A112.put("card_network", str.toLowerCase(Locale.US));
            }
            A112.put("detection_regex", r5.A03);
            A112.put("cvv_length", Integer.valueOf(r5.A01));
            A112.put("card_number_length", Integer.valueOf(r5.A00));
            A0l.add(A112);
        }
        A11.put("card_properties", A0l);
        A11.put("card_number", this.A02.A00());
        A11.put("card_expiry", this.A01.A00());
        A11.put("card_cvv", this.A00.A00());
        C120955h1 r0 = this.A03;
        if (r0 != null) {
            A11.put("card_postal_code", r0.A00());
        }
        return A11;
    }
}
