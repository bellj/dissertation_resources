package X;

/* renamed from: X.69o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1331269o implements AnonymousClass6MN {
    public final /* synthetic */ AnonymousClass6M0 A00;
    public final /* synthetic */ AnonymousClass605 A01;
    public final /* synthetic */ C128545wH A02;

    public C1331269o(AnonymousClass6M0 r1, AnonymousClass605 r2, C128545wH r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MN
    public void APo(C452120p r2) {
        this.A00.AVD(r2);
    }

    @Override // X.AnonymousClass6MN
    public void AX8(String[] strArr) {
        AnonymousClass605 r11 = this.A01;
        C128545wH r12 = this.A02;
        String str = strArr[0];
        String str2 = strArr[1];
        AnonymousClass6M0 r10 = this.A00;
        C130775zx r1 = r11.A01;
        C121225hS r6 = new C121225hS(r11.A04.A00, r11.A02, r11.A05, r10, r11, r12);
        byte[] A00 = C130775zx.A00(Boolean.TRUE, str, "CHANGE", str2, null, new Object[0], C117295Zj.A03(r1.A01));
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[1];
        C12960it.A1M("action", "change-payment-pin", r3, 0);
        C128545wH.A01(r12, r1, r6, A00, r3);
    }
}
