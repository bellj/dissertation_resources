package X;

import android.os.CountDownTimer;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;

/* renamed from: X.2Zq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51912Zq extends CountDownTimer {
    public final /* synthetic */ EncBackupViewModel A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51912Zq(EncBackupViewModel encBackupViewModel, long j) {
        super(j, 60000);
        this.A00 = encBackupViewModel;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        EncBackupViewModel encBackupViewModel = this.A00;
        encBackupViewModel.A00 = null;
        encBackupViewModel.A08.A0A(0L);
        encBackupViewModel.A04.A0A(C12960it.A0V());
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        AnonymousClass016 r3 = this.A00.A08;
        if (r3.A01() == null || C12980iv.A0G(r3.A01()) != j) {
            r3.A0A(Long.valueOf(j));
        }
    }
}
