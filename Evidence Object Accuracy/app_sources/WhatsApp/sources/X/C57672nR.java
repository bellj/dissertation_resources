package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57672nR extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57672nR A0D;
    public static volatile AnonymousClass255 A0E;
    public double A00;
    public double A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public AbstractC27881Jp A06 = AbstractC27881Jp.A01;
    public C43261wh A07;
    public String A08 = "";
    public String A09 = "";
    public String A0A = "";
    public String A0B = "";
    public boolean A0C;

    static {
        C57672nR r0 = new C57672nR();
        A0D = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C81603uH r2;
        switch (r15.ordinal()) {
            case 0:
                return A0D;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57672nR r1 = (C57672nR) obj2;
                int i = this.A04;
                boolean A1R = C12960it.A1R(i);
                double d = this.A00;
                int i2 = r1.A04;
                this.A00 = r7.Afn(d, r1.A00, A1R, C12960it.A1R(i2));
                this.A01 = r7.Afn(this.A01, r1.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A0A = r7.Afy(this.A0A, r1.A0A, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A08 = r7.Afy(this.A08, r1.A08, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A0B = r7.Afy(this.A0B, r1.A0B, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A0C = r7.Afl(C12960it.A1V(i & 32, 32), this.A0C, C12960it.A1V(i2 & 32, 32), r1.A0C);
                this.A03 = r7.Afp(this.A03, r1.A03, C12960it.A1V(i & 64, 64), C12960it.A1V(i2 & 64, 64));
                this.A02 = r7.Afo(this.A02, r1.A02, C12960it.A1V(i & 128, 128), C12960it.A1V(i2 & 128, 128));
                this.A05 = r7.Afp(this.A05, r1.A05, C12960it.A1V(i & 256, 256), C12960it.A1V(i2 & 256, 256));
                this.A09 = r7.Afy(this.A09, r1.A09, C12960it.A1V(i & 512, 512), C12960it.A1V(i2 & 512, 512));
                this.A06 = r7.Afm(this.A06, r1.A06, C12960it.A1V(i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), C12960it.A1V(i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
                this.A07 = (C43261wh) r7.Aft(this.A07, r1.A07);
                if (r7 == C463025i.A00) {
                    this.A04 |= r1.A04;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 9:
                                this.A04 |= 1;
                                this.A00 = Double.longBitsToDouble(r72.A05());
                                break;
                            case 17:
                                this.A04 |= 2;
                                this.A01 = Double.longBitsToDouble(r72.A05());
                                break;
                            case 26:
                                String A0A = r72.A0A();
                                this.A04 |= 4;
                                this.A0A = A0A;
                                break;
                            case 34:
                                String A0A2 = r72.A0A();
                                this.A04 |= 8;
                                this.A08 = A0A2;
                                break;
                            case 42:
                                String A0A3 = r72.A0A();
                                this.A04 |= 16;
                                this.A0B = A0A3;
                                break;
                            case 48:
                                this.A04 |= 32;
                                this.A0C = r72.A0F();
                                break;
                            case 56:
                                this.A04 |= 64;
                                this.A03 = r72.A02();
                                break;
                            case 69:
                                this.A04 |= 128;
                                this.A02 = Float.intBitsToFloat(r72.A01());
                                break;
                            case C43951xu.A02:
                                this.A04 |= 256;
                                this.A05 = r72.A02();
                                break;
                            case 90:
                                String A0A4 = r72.A0A();
                                this.A04 |= 512;
                                this.A09 = A0A4;
                                break;
                            case 130:
                                this.A04 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A06 = r72.A08();
                                break;
                            case 138:
                                if ((this.A04 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                    r2 = (C81603uH) this.A07.A0T();
                                } else {
                                    r2 = null;
                                }
                                C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r72, r12, C43261wh.A0O);
                                this.A07 = r0;
                                if (r2 != null) {
                                    this.A07 = (C43261wh) AbstractC27091Fz.A0C(r2, r0);
                                }
                                this.A04 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                break;
                            default:
                                if (A0a(r72, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57672nR();
            case 5:
                return new C82173vC();
            case 6:
                break;
            case 7:
                if (A0E == null) {
                    synchronized (C57672nR.class) {
                        if (A0E == null) {
                            A0E = AbstractC27091Fz.A09(A0D);
                        }
                    }
                }
                return A0E;
            default:
                throw C12970iu.A0z();
        }
        return A0D;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A04;
        if ((i3 & 1) == 1) {
            i2 = 9;
        }
        if ((i3 & 2) == 2) {
            i2 += 9;
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A04(3, this.A0A, i2);
        }
        if ((this.A04 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A08, i2);
        }
        if ((this.A04 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A0B, i2);
        }
        int i4 = this.A04;
        if ((i4 & 32) == 32) {
            i2 += CodedOutputStream.A00(6);
        }
        if ((i4 & 64) == 64) {
            i2 = AbstractC27091Fz.A02(7, this.A03, i2);
        }
        if ((i4 & 128) == 128) {
            i2 += 5;
        }
        if ((i4 & 256) == 256) {
            i2 = AbstractC27091Fz.A02(9, this.A05, i2);
        }
        if ((i4 & 512) == 512) {
            i2 = AbstractC27091Fz.A04(11, this.A09, i2);
        }
        int i5 = this.A04;
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 = AbstractC27091Fz.A05(this.A06, 16, i2);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C43261wh r0 = this.A07;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 17, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A04 & 1) == 1) {
            codedOutputStream.A0G(1, Double.doubleToRawLongBits(this.A00));
        }
        if ((this.A04 & 2) == 2) {
            codedOutputStream.A0G(2, Double.doubleToRawLongBits(this.A01));
        }
        if ((this.A04 & 4) == 4) {
            codedOutputStream.A0I(3, this.A0A);
        }
        if ((this.A04 & 8) == 8) {
            codedOutputStream.A0I(4, this.A08);
        }
        if ((this.A04 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0B);
        }
        if ((this.A04 & 32) == 32) {
            codedOutputStream.A0J(6, this.A0C);
        }
        if ((this.A04 & 64) == 64) {
            codedOutputStream.A0F(7, this.A03);
        }
        if ((this.A04 & 128) == 128) {
            codedOutputStream.A0D(8, Float.floatToRawIntBits(this.A02));
        }
        if ((this.A04 & 256) == 256) {
            codedOutputStream.A0F(9, this.A05);
        }
        if ((this.A04 & 512) == 512) {
            codedOutputStream.A0I(11, this.A09);
        }
        if ((this.A04 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0K(this.A06, 16);
        }
        if ((this.A04 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C43261wh r0 = this.A07;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
