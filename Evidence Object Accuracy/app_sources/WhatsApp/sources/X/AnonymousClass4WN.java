package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.util.List;

/* renamed from: X.4WN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WN {
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x00d7, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass5I3 A00(X.C08960c8... r26) {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4WN.A00(X.0c8[]):X.5I3");
    }

    public final void A01(List list, List list2, C10730f6 r25, int i, int i2, int i3, long j) {
        int i4;
        int i5 = i2;
        int i6 = i;
        if (C12990iw.A1Y(i5, i3)) {
            for (int i7 = i5; i7 < i3; i7++) {
                if (((C08960c8) list.get(i7)).A01() < i6) {
                    throw C12970iu.A0f("Failed requirement.");
                }
            }
            C08960c8 r9 = (C08960c8) list.get(i5);
            C08960c8 r11 = (C08960c8) list.get(i3 - 1);
            if (i6 == r9.A01()) {
                i4 = C72453ed.A0G(list2, i5);
                i5 = i2 + 1;
                r9 = (C08960c8) list.get(i5);
            } else {
                i4 = -1;
            }
            if (r9.A00(i6) != r11.A00(i6)) {
                int i8 = 1;
                for (int i9 = i5 + 1; i9 < i3; i9++) {
                    if (((C08960c8) list.get(i9 - 1)).A00(i6) != ((C08960c8) list.get(i9)).A00(i6)) {
                        i8++;
                    }
                }
                long j2 = (long) 4;
                long j3 = j + (r25.A00 / j2) + ((long) 2) + ((long) (i8 << 1));
                r25.A0D(i8);
                r25.A0D(i4);
                for (int i10 = i5; i10 < i3; i10++) {
                    byte A00 = ((C08960c8) list.get(i10)).A00(i6);
                    if (i10 == i5 || A00 != ((C08960c8) list.get(i10 - 1)).A00(i6)) {
                        r25.A0D(A00 & 255);
                    }
                }
                C10730f6 r92 = new C10730f6();
                while (i5 < i3) {
                    byte A002 = ((C08960c8) list.get(i5)).A00(i6);
                    int i11 = i5 + 1;
                    int i12 = i11;
                    while (true) {
                        if (i12 >= i3) {
                            i12 = i3;
                            break;
                        }
                        if (A002 != ((C08960c8) list.get(i12)).A00(i6)) {
                            break;
                        }
                        i12++;
                    }
                    if (i11 == i12 && i + 1 == ((C08960c8) list.get(i5)).A01()) {
                        r25.A0D(C72453ed.A0G(list2, i5));
                    } else {
                        r25.A0D(-((int) (j3 + (r92.A00 / j2))));
                        A01(list, list2, r92, i + 1, i5, i12, j3);
                    }
                    i5 = i12;
                }
                do {
                } while (r92.AZp(r25, (long) DefaultCrypto.BUFFER_SIZE) != -1);
                return;
            }
            int min = Math.min(r9.A01(), r11.A01());
            int i13 = i6;
            int i14 = 0;
            while (i13 < min && r9.A00(i13) == r11.A00(i13)) {
                i14++;
                i13++;
            }
            long j4 = (long) 4;
            long j5 = j + (r25.A00 / j4) + ((long) 2) + ((long) i14) + 1;
            r25.A0D(-i14);
            r25.A0D(i4);
            int i15 = i + i14;
            while (i6 < i15) {
                r25.A0D(r9.A00(i6) & 255);
                i6++;
            }
            if (i5 + 1 != i3) {
                C10730f6 r5 = new C10730f6();
                r25.A0D(-((int) ((r5.A00 / j4) + j5)));
                A01(list, list2, r5, i15, i5, i3, j5);
                do {
                } while (r5.AZp(r25, (long) DefaultCrypto.BUFFER_SIZE) != -1);
            } else if (i15 == ((C08960c8) list.get(i5)).A01()) {
                r25.A0D(C72453ed.A0G(list2, i5));
            } else {
                throw C12960it.A0U("Check failed.");
            }
        } else {
            throw C12970iu.A0f("Failed requirement.");
        }
    }
}
