package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.4jU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98934jU implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        List list = C56452ku.A0B;
        LocationRequest locationRequest = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j = Long.MAX_VALUE;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                switch (c) {
                    case 5:
                        list = C95664e9.A0B(parcel, C78473oy.CREATOR, readInt);
                        continue;
                    case 6:
                        str = C95664e9.A08(parcel, readInt);
                        continue;
                    case 7:
                        z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                        continue;
                    case '\b':
                        z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                        continue;
                    case '\t':
                        z3 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                        continue;
                    case '\n':
                        str2 = C95664e9.A08(parcel, readInt);
                        continue;
                    case 11:
                        z4 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                        continue;
                    case '\f':
                        z5 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                        continue;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        str3 = C95664e9.A08(parcel, readInt);
                        continue;
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        j = C95664e9.A04(parcel, readInt);
                        continue;
                    default:
                        C95664e9.A0D(parcel, readInt);
                        continue;
                }
            } else {
                locationRequest = (LocationRequest) C95664e9.A07(parcel, LocationRequest.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56452ku(locationRequest, str, str2, str3, list, j, z, z2, z3, z4, z5);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C56452ku[i];
    }
}
