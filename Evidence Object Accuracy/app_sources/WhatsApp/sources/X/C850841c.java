package X;

import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.41c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C850841c extends AnonymousClass2Dn {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C850841c(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        ViewProfilePhoto.A02(this.A00);
    }
}
