package X;

import android.widget.BaseAdapter;
import com.whatsapp.profile.WebImagePicker;

/* renamed from: X.2be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52772be extends BaseAdapter {
    public AnonymousClass36n A00;
    public boolean A01;
    public final /* synthetic */ WebImagePicker A02;

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return null;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public /* synthetic */ C52772be(WebImagePicker webImagePicker) {
        this.A02 = webImagePicker;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        WebImagePicker webImagePicker = this.A02;
        int size = webImagePicker.A0J.size();
        int i = webImagePicker.A00;
        return ((size + i) - 1) / i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 != r2.A00) goto L_0x0013;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r19, android.view.View r20, android.view.ViewGroup r21) {
        /*
        // Method dump skipped, instructions count: 287
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52772be.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
