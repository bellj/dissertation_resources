package X;

import android.hardware.biometrics.BiometricPrompt;

/* renamed from: X.0KF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KF {
    public static int A00(BiometricPrompt.AuthenticationResult authenticationResult) {
        return authenticationResult.getAuthenticationType();
    }
}
