package X;

import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathChecker;

/* renamed from: X.5GI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GI implements AnonymousClass5WR {
    public final PKIXCertPathChecker A00;

    public AnonymousClass5GI(PKIXCertPathChecker pKIXCertPathChecker) {
        this.A00 = pKIXCertPathChecker;
    }

    @Override // X.AnonymousClass5WR
    public void AIv(AnonymousClass4TK r3) {
        this.A00.init(false);
    }

    @Override // X.AnonymousClass5WR
    public void check(Certificate certificate) {
        this.A00.check(certificate);
    }
}
