package X;

import com.whatsapp.util.Log;

/* renamed from: X.68w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329468w implements AnonymousClass17Y {
    public final C130155yt A00;
    public final AnonymousClass61F A01;
    public final AbstractC14440lR A02;

    @Override // X.AnonymousClass17Y
    public boolean A8l(String str, boolean z) {
        return false;
    }

    @Override // X.AnonymousClass17Y
    public boolean AdJ(AbstractC30891Zf r2) {
        return false;
    }

    @Override // X.AnonymousClass17Y
    public boolean AfM() {
        return false;
    }

    @Override // X.AnonymousClass17Y
    public boolean AfV(AnonymousClass1ZY r2) {
        return false;
    }

    public C1329468w(C130155yt r1, AnonymousClass61F r2, AbstractC14440lR r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass17Y
    public void A8k() {
        this.A02.Ab2(new Runnable() { // from class: X.6Fc
            @Override // java.lang.Runnable
            public final void run() {
                C1329468w r1 = C1329468w.this;
                r1.A01.A08();
                r1.A00.A06();
                Log.w("PAY: CountryAccountHelper/deleteAccountInfo() account removed");
            }
        });
    }
}
