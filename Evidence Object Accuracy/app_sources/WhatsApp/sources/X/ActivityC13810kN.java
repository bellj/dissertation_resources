package X;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.community.CommunityAdminDialogFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0kN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC13810kN extends AbstractActivityC13820kO implements AbstractC13860kS, AbstractC13870kT {
    public static final int A0P = -1;
    public static final long A0Q = 500;
    public static final String A0R = "screenshot.jpg";
    public View A00;
    public ViewGroup A01;
    public Toolbar A02;
    public AbstractC15710nm A03;
    public C14330lG A04;
    public C14900mE A05;
    public C15450nH A06;
    public C18640sm A07;
    public AnonymousClass01d A08;
    public C14820m6 A09;
    public C18470sV A0A;
    public AnonymousClass19M A0B;
    public C14850m9 A0C;
    public C18810t5 A0D;
    public boolean A0E = true;
    public int A0F;
    public long A0G;
    public Intent A0H;
    public View A0I;
    public ViewGroup A0J;
    public AnonymousClass2GX A0K;
    public Integer A0L;
    public List A0M = new ArrayList();
    public boolean A0N;
    public final C29661Ud A0O = new C29661Ud(this);

    public void A2A(int i) {
    }

    private AnonymousClass2GX A0n() {
        return (AnonymousClass2GX) new AnonymousClass02A(new AnonymousClass2GW(this), this).A00(AnonymousClass2GX.class);
    }

    public static Iterator A0o(ActivityC13810kN r0) {
        return r0.A23().iterator();
    }

    private void A0p() {
        Intent intent = this.A0H;
        if (intent != null) {
            Integer num = this.A0L;
            if (num != null) {
                startActivityForResult(intent, num.intValue());
            } else {
                startActivity(intent);
            }
            if (this.A0N) {
                finish();
            }
            this.A0H = null;
            this.A0L = null;
            this.A0N = false;
        }
    }

    public static void A0q(long j, long j2) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        if (elapsedRealtime < j2) {
            SystemClock.sleep(j2 - elapsedRealtime);
        }
    }

    public static void A0x(Bundle bundle, Parcelable parcelable, DialogFragment dialogFragment, AbstractC69213Yj r4) {
        bundle.putParcelable("sticker", parcelable);
        dialogFragment.A0U(bundle);
        ((ActivityC13810kN) AnonymousClass12P.A00(r4.A08)).Adm(dialogFragment);
    }

    public static void A0y(Bundle bundle, ActivityC13810kN r3, AnonymousClass4KE r4) {
        CommunityAdminDialogFragment communityAdminDialogFragment = new CommunityAdminDialogFragment();
        communityAdminDialogFragment.A0U(bundle);
        AnonymousClass009.A05(r4);
        communityAdminDialogFragment.A01 = r4;
        r3.Adl(communityAdminDialogFragment, null);
    }

    public static void A0z(AbstractActivityC36551k4 r1, int i) {
        if (i != 0) {
            r1.A2g();
            return;
        }
        r1.finish();
        ((ActivityC13810kN) r1).A05.A07(R.string.failed_to_update_privacy_settings, 1);
    }

    public static void A10(AnonymousClass01J r1, ActivityC13810kN r2) {
        r2.A0C = (C14850m9) r1.A04.get();
        r2.A05 = (C14900mE) r1.A8X.get();
        r2.A03 = (AbstractC15710nm) r1.A4o.get();
        r2.A04 = (C14330lG) r1.A7B.get();
        r2.A0B = (AnonymousClass19M) r1.A6R.get();
        r2.A0A = (C18470sV) r1.AK8.get();
        r2.A06 = (C15450nH) r1.AII.get();
        r2.A08 = (AnonymousClass01d) r1.ALI.get();
        r2.A0D = (C18810t5) r1.AMu.get();
        r2.A09 = (C14820m6) r1.AN3.get();
        r2.A07 = (C18640sm) r1.A3u.get();
    }

    public static void A13(ActivityC13810kN r1, Boolean bool) {
        r1.A05.A03();
        if (bool.booleanValue()) {
            r1.setResult(-1);
        } else {
            r1.A05.A07(R.string.coldsync_no_network, 1);
        }
    }

    public static /* synthetic */ void A15(ActivityC13810kN r2, Integer num) {
        if (num.intValue() == 1) {
            StringBuilder sb = new StringBuilder();
            sb.append(r2.getClass().getCanonicalName());
            sb.append(" ActivityLifecycleCallbacks: Recreating");
            Log.i(sb.toString());
            r2.recreate();
        }
    }

    public static boolean A1I(ActivityC13810kN r1) {
        return r1.A0C.A07(931);
    }

    @Override // X.ActivityC000900k
    public void A1T(AnonymousClass01E r3) {
        this.A0M.add(new WeakReference(r3));
    }

    @Override // X.ActivityC000800j
    public AbstractC009504t A1W(AnonymousClass02Q r4) {
        AbstractC009504t A1W = super.A1W(r4);
        if (A1W != null) {
            A1W.A06();
        }
        View findViewById = findViewById(R.id.action_mode_close_button);
        if (findViewById != null) {
            AnonymousClass028.A0g(findViewById, new C48482Gl(this));
        }
        return A1W;
    }

    @Override // X.ActivityC000800j
    public void A1e(Toolbar toolbar) {
        super.A1e(toolbar);
        this.A02 = toolbar;
    }

    @Override // X.ActivityC000800j
    public void A1g(boolean z) {
        AbstractC005102i A1U;
        int i = 0;
        if (this.A0I == null) {
            View inflate = getLayoutInflater().inflate(R.layout.actionbar_progress, (ViewGroup) null, false);
            View findViewById = inflate.findViewById(R.id.progress_bar);
            this.A0I = findViewById;
            if (!(findViewById == null || (A1U = A1U()) == null)) {
                A1U.A0N(true);
                A1U.A0G(inflate, new C009604u());
            }
        }
        View view = this.A0I;
        if (view != null) {
            if (!z) {
                i = 8;
            }
            view.setVisibility(i);
        }
    }

    public Uri A22() {
        Throwable e;
        StringBuilder sb;
        String str;
        View rootView = getWindow().getDecorView().getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
        File A0M = this.A04.A0M(A0R);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(A0M);
            createBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e2) {
            e = e2;
            sb = new StringBuilder();
            str = "File not found: ";
            sb.append(str);
            sb.append(e.getMessage());
            Log.e(sb.toString());
            return C14350lI.A01(this, A0M);
        } catch (IOException e3) {
            e = e3;
            sb = new StringBuilder();
            str = "IOException: ";
            sb.append(str);
            sb.append(e.getMessage());
            Log.e(sb.toString());
            return C14350lI.A01(this, A0M);
        }
        return C14350lI.A01(this, A0M);
    }

    public List A23() {
        ArrayList arrayList = new ArrayList();
        for (Reference reference : this.A0M) {
            AnonymousClass01E r1 = (AnonymousClass01E) reference.get();
            if (r1 != null && r1.A0c()) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public void A24() {
    }

    public void A25() {
    }

    public void A26() {
    }

    public void A27() {
    }

    public void A28() {
        View view;
        if (!isFinishing() && (view = this.A00) != null) {
            view.postDelayed(new RunnableBRunnable0Shape0S0100000_I0(this, 40), 300);
        }
    }

    public void A29() {
        A2B(R.layout.toolbar);
    }

    /* JADX INFO: finally extract failed */
    public void A2B(int i) {
        boolean z = false;
        Toolbar toolbar = (Toolbar) getLayoutInflater().inflate(i, (ViewGroup) null, false);
        toolbar.setFitsSystemWindows(true);
        if (Build.VERSION.SDK_INT >= 21) {
            TypedValue typedValue = new TypedValue();
            getTheme().resolveAttribute(R.attr.actionBarStyle, typedValue, true);
            TypedArray obtainStyledAttributes = getTheme().obtainStyledAttributes(typedValue.resourceId, AnonymousClass2GZ.A00);
            try {
                float dimension = obtainStyledAttributes.getDimension(12, 0.0f);
                obtainStyledAttributes.recycle();
                toolbar.setElevation(dimension);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        }
        TypedValue typedValue2 = new TypedValue();
        getTheme().resolveAttribute(R.attr.windowActionBarOverlay, typedValue2, true);
        if (typedValue2.type == 18 && typedValue2.data != 0) {
            z = true;
        }
        if (z) {
            this.A01 = new FrameLayout(this);
            FrameLayout frameLayout = new FrameLayout(this);
            this.A0J = frameLayout;
            this.A01.addView(frameLayout, -1, -1);
        } else {
            LinearLayout linearLayout = new LinearLayout(this);
            this.A01 = linearLayout;
            this.A0J = linearLayout;
            linearLayout.setOrientation(1);
        }
        this.A01.addView(toolbar, -1, getResources().getDimensionPixelSize(R.dimen.actionbar_height));
        A1e(toolbar);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 7));
    }

    public void A2C(int i) {
        if (!AJN()) {
            Ady(0, i);
        }
    }

    public void A2D(Intent intent) {
        A2G(intent, false);
    }

    public void A2E(Intent intent, int i) {
        A2F(intent, i, false);
    }

    public void A2F(Intent intent, int i, boolean z) {
        if (!this.A0E) {
            this.A0H = intent;
            this.A0L = Integer.valueOf(i);
            this.A0N = z;
            return;
        }
        startActivityForResult(intent, i);
        if (z) {
            finish();
        }
    }

    public void A2G(Intent intent, boolean z) {
        boolean z2;
        if (!this.A0E) {
            this.A0H = intent;
            z2 = false;
        } else {
            startActivity(intent);
            z2 = true;
        }
        if (!z) {
            return;
        }
        if (z2) {
            finish();
        } else {
            this.A0N = z;
        }
    }

    public void A2H(Configuration configuration) {
        this.A0K.A04(configuration);
    }

    public void A2I(AnonymousClass2GV r4, int i, int i2, int i3) {
        if (!AJN()) {
            AnonymousClass2AC r1 = new AnonymousClass2AC(new Object[0], i2);
            r1.A05 = i;
            r1.A0B = new Object[0];
            r1.A00 = i2;
            r1.A02(new DialogInterface.OnClickListener() { // from class: X.2Gg
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i4) {
                    AnonymousClass2GV.this.AO1();
                }
            }, i3);
            r1.A01().A1F(A0V(), null);
        }
    }

    public void A2J(AnonymousClass2GV r4, int i, int i2, int i3) {
        if (!AJN()) {
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], i);
            A01.A00 = i;
            A01.A02(new DialogInterface.OnClickListener() { // from class: X.2Gj
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i4) {
                    dialogInterface.dismiss();
                }
            }, i2);
            DialogInterface$OnClickListenerC48472Gk r0 = new DialogInterface.OnClickListener() { // from class: X.2Gk
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i4) {
                    AnonymousClass2GV.this.AO1();
                }
            };
            A01.A04 = i3;
            A01.A07 = r0;
            A01.A01().A1F(A0V(), null);
        }
    }

    public void A2K(AnonymousClass2GV r4, int i, int i2, int i3, int i4) {
        if (!AJN()) {
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], i2);
            A01.A05 = i;
            A01.A0B = new Object[0];
            A01.A00 = i2;
            A01.A02(new DialogInterface.OnClickListener() { // from class: X.2Gc
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i5) {
                    AnonymousClass2GV.this.AO1();
                }
            }, i3);
            DialogInterface$OnClickListenerC48412Gd r0 = new DialogInterface.OnClickListener() { // from class: X.2Gd
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i5) {
                    dialogInterface.dismiss();
                }
            };
            A01.A04 = i4;
            A01.A07 = r0;
            A01.A01().A1F(A0V(), null);
        }
    }

    public void A2L(AnonymousClass2GV r4, AnonymousClass2GV r5, int i, int i2, int i3, int i4) {
        if (!AJN()) {
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], i2);
            A01.A05 = i;
            A01.A0B = new Object[0];
            A01.A00 = i2;
            A01.A02(new DialogInterface.OnClickListener() { // from class: X.2Ge
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i5) {
                    AnonymousClass2GV.this.AO1();
                }
            }, i3);
            DialogInterface$OnClickListenerC48432Gf r0 = new DialogInterface.OnClickListener() { // from class: X.2Gf
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i5) {
                    AnonymousClass2GV.this.AO1();
                }
            };
            A01.A04 = i4;
            A01.A07 = r0;
            A01.A01().A1F(A0V(), null);
        }
    }

    public void A2M(String str) {
        if (!AJN()) {
            AnonymousClass01F A0V = A0V();
            C004902f r1 = new C004902f(A0V);
            AnonymousClass01E A0A = A0V.A0A(str);
            if (A0A != null) {
                r1.A05(A0A);
                r1.A02();
            }
        }
    }

    public void A2N(String str) {
        TextPaint textPaint = new TextPaint();
        textPaint.setTextSize(getResources().getDimension(R.dimen.subtitle_text_size));
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0H(AbstractC36671kL.A03(this, textPaint, this.A0B, str));
    }

    public void A2O(String str) {
        TextPaint textPaint = new TextPaint();
        textPaint.setTextSize(getResources().getDimension(R.dimen.title_text_size));
        setTitle(AbstractC36671kL.A03(this, textPaint, this.A0B, str));
    }

    public void A2P(String str) {
        if (!AJN()) {
            C29661Ud r1 = this.A0O;
            if (r1.A00 == null) {
                ProgressDialogFragment A01 = ProgressDialogFragment.A01(str);
                r1.A00 = A01;
                A01.A1F(r1.A01.A0V(), C29661Ud.A03);
            }
            C29661Ud.A02 = true;
        }
    }

    public void A2Q(String str, String str2) {
        if (!AJN()) {
            AnonymousClass2AC r0 = new AnonymousClass2AC(str2);
            r0.A09 = str;
            r0.A01().A1F(A0V(), null);
        }
    }

    public boolean A2R() {
        if (this.A07.A0B()) {
            return false;
        }
        boolean A03 = C18640sm.A03((Context) this);
        int i = R.string.network_required;
        if (A03) {
            i = R.string.network_required_airplane_on;
        }
        Ado(i);
        return true;
    }

    public boolean A2S(int i) {
        if (this.A07.A0B()) {
            return false;
        }
        Ado(i);
        return true;
    }

    @Override // X.AbstractC13860kS
    public boolean AJN() {
        return C36021jC.A03(this);
    }

    @Override // X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r3) {
        Toolbar toolbar = this.A02;
        if (toolbar != null) {
            AnonymousClass028.A0a(toolbar, 0);
        }
    }

    @Override // X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r3) {
        Toolbar toolbar = this.A02;
        if (toolbar != null) {
            AnonymousClass028.A0a(toolbar, 4);
        }
    }

    @Override // X.AbstractC13860kS
    public void AaN() {
        C29661Ud r1 = this.A0O;
        C29661Ud.A02 = false;
        if (!C36021jC.A03(r1.A01)) {
            DialogFragment dialogFragment = r1.A00;
            if (dialogFragment != null) {
                dialogFragment.A1C();
            }
            r1.A00 = null;
        }
    }

    @Override // X.AbstractC13860kS
    public void Adl(DialogFragment dialogFragment, String str) {
        if (!AJN()) {
            C004902f r0 = new C004902f(A0V());
            r0.A09(dialogFragment, str);
            r0.A02();
        }
    }

    @Override // X.AbstractC13860kS
    public void Adm(DialogFragment dialogFragment) {
        if (!AJN()) {
            C42791vs.A00(dialogFragment, A0V());
        }
    }

    @Override // X.AbstractC13860kS
    public void Ado(int i) {
        if (!AJN()) {
            AnonymousClass2AC r0 = new AnonymousClass2AC(new Object[0], i);
            r0.A00 = i;
            r0.A01().A1F(A0V(), null);
        }
    }

    @Override // X.AbstractC13860kS
    @Deprecated
    public void Adp(String str) {
        if (!AJN()) {
            new AnonymousClass2AC(str).A01().A1F(A0V(), null);
        }
    }

    @Override // X.AbstractC13860kS
    public void Adq(AnonymousClass2GV r4, Object[] objArr, int i, int i2, int i3) {
        if (!AJN()) {
            AnonymousClass2AC r2 = new AnonymousClass2AC(objArr, i2);
            r2.A05 = i;
            r2.A0B = new Object[0];
            r2.A00 = i2;
            r2.A02(new DialogInterface.OnClickListener() { // from class: X.2Gh
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i4) {
                    AnonymousClass2GV.this.AO1();
                }
            }, i3);
            DialogInterface$OnClickListenerC48452Gi r1 = new DialogInterface.OnClickListener() { // from class: X.2Gi
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i4) {
                    dialogInterface.dismiss();
                }
            };
            r2.A04 = R.string.cancel;
            r2.A07 = r1;
            r2.A01().A1F(A0V(), null);
        }
    }

    @Override // X.AbstractC13860kS
    public void Adr(Object[] objArr, int i, int i2) {
        if (!AJN()) {
            AnonymousClass2AC r1 = new AnonymousClass2AC(objArr, i2);
            r1.A05 = i;
            r1.A0B = new Object[0];
            r1.A00 = i2;
            r1.A01().A1F(A0V(), null);
        }
    }

    @Override // X.AbstractC13860kS
    public void Ady(int i, int i2) {
        if (!AJN()) {
            C29661Ud r1 = this.A0O;
            if (r1.A00 == null) {
                ProgressDialogFragment A00 = ProgressDialogFragment.A00(i, i2);
                r1.A00 = A00;
                A00.A1F(r1.A01.A0V(), C29661Ud.A03);
            }
            C29661Ud.A02 = true;
        }
    }

    @Override // X.AbstractC13860kS
    public void AfX(String str) {
        StringBuilder sb;
        String str2;
        if (!AJN()) {
            DialogFragment dialogFragment = this.A0O.A00;
            if (dialogFragment == null) {
                sb = new StringBuilder();
                str2 = "dialogtoast/update-progress-message/progress-spinner-not-shown \"";
            } else {
                Dialog dialog = dialogFragment.A03;
                if (dialog == null) {
                    sb = new StringBuilder();
                    str2 = "dialogtoast/update-progress-message/null-dialog/ \"";
                } else if (!(dialog instanceof ProgressDialog)) {
                    sb = new StringBuilder();
                    str2 = "dialogtoast/update-progress-message/dialog-type-not-progress-dialog/ \"";
                } else {
                    ((AlertDialog) dialog).setMessage(str);
                    return;
                }
            }
            sb.append(str2);
            sb.append(str);
            sb.append("\"");
            Log.w(sb.toString());
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return (this.A0E || SystemClock.elapsedRealtime() - this.A0G > 500 || !(motionEvent.getActionMasked() == 0 || motionEvent.getActionMasked() == 2)) && super.dispatchTouchEvent(motionEvent);
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT < 29) {
            onStateNotSaved();
        }
        if (this.A0E) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A2H(configuration);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006b, code lost:
        if (r2.data != 0) goto L_0x006d;
     */
    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            int r0 = X.AnonymousClass025.A00
            r5.A0F = r0
            X.018 r1 = r5.A01
            android.view.Window r0 = r5.getWindow()
            X.C42941w9.A0B(r0, r1)
            X.2GX r1 = r5.A0n()
            r5.A0K = r1
            android.content.res.Resources r0 = r5.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.uiMode
            r0 = r0 & 48
            r1.A00 = r0
            X.2GX r0 = r5.A0K
            X.016 r1 = r0.A01
            X.2GY r0 = new X.2GY
            r0.<init>()
            r1.A05(r5, r0)
            super.onCreate(r6)
            X.018 r0 = r5.A01
            X.1Kv r0 = r0.A04()
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x0079
            android.content.res.Resources$Theme r4 = r5.getTheme()
            android.util.TypedValue r1 = new android.util.TypedValue
            r1.<init>()
            r0 = 2130969384(0x7f040328, float:1.7547448E38)
            r3 = 1
            r4.resolveAttribute(r0, r1, r3)
            int r0 = r1.type
            if (r0 != r3) goto L_0x007a
            int r0 = r1.data
        L_0x0050:
            r4.applyStyle(r0, r3)
            boolean r0 = X.C42941w9.A01
            if (r0 != 0) goto L_0x0079
            android.util.TypedValue r2 = new android.util.TypedValue
            r2.<init>()
            r0 = 2130969744(0x7f040490, float:1.7548179E38)
            r4.resolveAttribute(r0, r2, r3)
            int r1 = r2.type
            r0 = 18
            if (r1 != r0) goto L_0x006d
            int r0 = r2.data
            r1 = 1
            if (r0 == 0) goto L_0x006e
        L_0x006d:
            r1 = 0
        L_0x006e:
            r0 = 2131952063(0x7f1301bf, float:1.9540558E38)
            r4.applyStyle(r0, r3)
            if (r1 != 0) goto L_0x0079
            r5.A29()
        L_0x0079:
            return
        L_0x007a:
            r0 = 2131952170(0x7f13022a, float:1.9540775E38)
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: X.ActivityC13810kN.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C29661Ud r1 = this.A0O;
        DialogFragment dialogFragment = r1.A00;
        if (dialogFragment != null) {
            dialogFragment.A1C();
        }
        r1.A00 = null;
        this.A0H = null;
        this.A0N = false;
        super.onDestroy();
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPause() {
        this.A05.A09(this);
        super.onPause();
        this.A0E = false;
        this.A0G = SystemClock.elapsedRealtime();
    }

    @Override // android.app.Activity
    public void onRestart() {
        super.onRestart();
        if (AnonymousClass025.A00 != this.A0F) {
            recreate();
        }
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A05.A0B(this);
        this.A0E = true;
        A0p();
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0K.A04(getResources().getConfiguration());
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void setContentView(int i) {
        setContentView(getLayoutInflater().inflate(i, (ViewGroup) null, false));
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void setContentView(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass028.A0S(view);
        }
        if (this.A01 != null) {
            this.A0J.addView(view, -1, -1);
            ViewGroup viewGroup = this.A01;
            this.A00 = viewGroup;
            super.setContentView(viewGroup);
            return;
        }
        this.A00 = view;
        super.setContentView(view);
    }
}
