package X;

/* renamed from: X.3ES  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ES {
    public final int A00;
    public final int A01;
    public final C92604Wp A02;
    public final AnonymousClass4WR A03;

    public AnonymousClass3ES(C92604Wp r1, AnonymousClass4WR r2, int i, int i2) {
        this.A01 = i;
        this.A03 = r2;
        this.A02 = r1;
        this.A00 = i2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3ES r5 = (AnonymousClass3ES) obj;
            if (this.A01 != r5.A01 || this.A00 != r5.A00 || !C29941Vi.A00(this.A03, r5.A03) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[4];
        C12960it.A1O(objArr, this.A01);
        C12980iv.A1T(objArr, this.A00);
        objArr[2] = this.A03;
        return C12980iv.A0B(this.A02, objArr, 3);
    }
}
