package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1Jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27941Jv extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27941Jv A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01;
    public C27931Ju A02;

    static {
        C27941Jv r0 = new C27941Jv();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        C464125t r1;
        switch (r5.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C27941Jv r7 = (C27941Jv) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                int i = this.A01;
                boolean z2 = true;
                if ((r7.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r6.Afp(i, r7.A01, z, z2);
                this.A02 = (C27931Ju) r6.Aft(this.A02, r7.A02);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A032 = r62.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 8) {
                                int A02 = r62.A02();
                                if (A02 == 0 || A02 == 1) {
                                    this.A00 = 1 | this.A00;
                                    this.A01 = A02;
                                } else {
                                    super.A0X(1, A02);
                                }
                            } else if (A032 == 18) {
                                if ((this.A00 & 2) == 2) {
                                    r1 = (C464125t) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                C27931Ju r0 = (C27931Ju) r62.A09(r72, C27931Ju.A04.A0U());
                                this.A02 = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A02 = (C27931Ju) r1.A01();
                                }
                                this.A00 |= 2;
                            } else if (!A0a(r62, A032)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C27941Jv();
            case 5:
                return new C82453ve();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C27941Jv.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            C27931Ju r0 = this.A02;
            if (r0 == null) {
                r0 = C27931Ju.A04;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            C27931Ju r0 = this.A02;
            if (r0 == null) {
                r0 = C27931Ju.A04;
            }
            codedOutputStream.A0L(r0, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
