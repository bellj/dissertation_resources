package X;

import java.io.Serializable;
import java.util.Comparator;

/* renamed from: X.5Cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112305Cw implements Comparator, Serializable {
    public final float average;

    public /* synthetic */ C112305Cw(float f) {
        this.average = f;
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        C82593vs r4 = (C82593vs) obj;
        C82593vs r5 = (C82593vs) obj2;
        int A00 = AnonymousClass048.A00(r5.A01, r4.A01);
        if (A00 != 0) {
            return A00;
        }
        float f = r4.A00;
        float f2 = this.average;
        return Float.compare(Math.abs(f - f2), Math.abs(r5.A00 - f2));
    }
}
