package X;

import java.util.List;

/* renamed from: X.0D3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0D3 extends AbstractC07280Xj {
    public static int[] A00 = new int[2];

    public AnonymousClass0D3(AnonymousClass0QV r3) {
        super(r3);
        this.A05.A04 = EnumC03760Ja.LEFT;
        this.A04.A04 = EnumC03760Ja.RIGHT;
        this.A01 = 0;
    }

    public static final void A00(int[] iArr, float f, int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i2 - i;
        int i9 = i4 - i3;
        if (i5 == -1) {
            i6 = (int) ((((float) i9) * f) + 0.5f);
            i7 = (int) ((((float) i8) / f) + 0.5f);
            if (i6 > i8) {
                if (i7 > i9) {
                    return;
                }
                iArr[0] = i8;
                iArr[1] = i7;
            }
            iArr[0] = i6;
            iArr[1] = i9;
        } else if (i5 == 0) {
            i6 = (int) ((((float) i9) * f) + 0.5f);
            iArr[0] = i6;
            iArr[1] = i9;
        } else if (i5 == 1) {
            i7 = (int) ((((float) i8) * f) + 0.5f);
            iArr[0] = i8;
            iArr[1] = i7;
        }
    }

    @Override // X.AbstractC07280Xj
    public void A06() {
        C07270Xi r2;
        C07270Xi r1;
        int i;
        AnonymousClass0QV r0;
        AnonymousClass0Q7 r22;
        C07270Xi r12;
        int A002;
        List list;
        Object obj;
        List list2;
        C07270Xi r4;
        C07270Xi r13;
        int i2;
        C07270Xi r14;
        int i3;
        AnonymousClass0QV r5;
        AnonymousClass0QV r42;
        AnonymousClass0QV r52;
        AnonymousClass0QV r23 = this.A03;
        if (r23.A0i) {
            this.A06.A01(r23.A01());
        }
        C02580Cy r3 = this.A06;
        if (!r3.A0B) {
            AnonymousClass0QV r6 = this.A03;
            AnonymousClass0JR r43 = r6.A0o[0];
            this.A02 = r43;
            if (r43 != AnonymousClass0JR.MATCH_CONSTRAINT) {
                AnonymousClass0JR r24 = AnonymousClass0JR.MATCH_PARENT;
                if (r43 == r24 && (((r52 = r6.A0Z) != null && r52.A0o[0] == AnonymousClass0JR.FIXED) || r52.A0o[0] == r24)) {
                    int A01 = r52.A01();
                    int A003 = r6.A0W.A00();
                    int A004 = (A01 - A003) - r6.A0X.A00();
                    AbstractC07280Xj.A03(this.A05, r52.A0c.A05, A003);
                    AbstractC07280Xj.A03(this.A04, r52.A0c.A04, -this.A03.A0X.A00());
                    r3.A01(A004);
                    return;
                } else if (r43 == AnonymousClass0JR.FIXED) {
                    r3.A01(r6.A01());
                }
            }
        } else {
            AnonymousClass0JR r02 = this.A02;
            AnonymousClass0JR r25 = AnonymousClass0JR.MATCH_PARENT;
            if (r02 == r25 && (((r42 = (r5 = this.A03).A0Z) != null && r42.A0o[0] == AnonymousClass0JR.FIXED) || r42.A0o[0] == r25)) {
                AbstractC07280Xj.A03(this.A05, r42.A0c.A05, r5.A0W.A00());
                AbstractC07280Xj.A03(this.A04, r42.A0c.A04, -this.A03.A0X.A00());
                return;
            }
        }
        if (r3.A0B) {
            AnonymousClass0QV r44 = this.A03;
            if (r44.A0i) {
                AnonymousClass0Q7[] r03 = r44.A0n;
                AnonymousClass0Q7 r62 = r03[0];
                AnonymousClass0Q7 r15 = r62.A03;
                r22 = r03[1];
                AnonymousClass0Q7 r04 = r22.A03;
                if (r15 != null) {
                    if (r04 == null) {
                        r14 = AbstractC07280Xj.A01(r62);
                        if (r14 != null) {
                            r4 = this.A05;
                            i3 = r62.A00();
                        } else {
                            return;
                        }
                    } else if (r44.A0E()) {
                        r12 = this.A05;
                        A002 = r62.A00();
                        r12.A00 = A002;
                        this.A04.A00 = -r22.A00();
                        return;
                    } else {
                        C07270Xi A012 = AbstractC07280Xj.A01(r62);
                        if (A012 != null) {
                            AbstractC07280Xj.A03(this.A05, A012, r62.A00());
                        }
                        AnonymousClass0Q7 r05 = this.A03.A0n[1];
                        C07270Xi A013 = AbstractC07280Xj.A01(r05);
                        if (A013 != null) {
                            AbstractC07280Xj.A03(this.A04, A013, -r05.A00());
                        }
                        this.A05.A09 = true;
                        this.A04.A09 = true;
                        return;
                    }
                } else if (r04 != null) {
                    C07270Xi A014 = AbstractC07280Xj.A01(r22);
                    if (A014 != null) {
                        r4 = this.A04;
                        AbstractC07280Xj.A03(r4, A014, -r22.A00());
                        r13 = this.A05;
                        i2 = -r3.A02;
                        AbstractC07280Xj.A03(r13, r4, i2);
                        return;
                    }
                    return;
                } else if (!(r44 instanceof AbstractC11700gi) && r44.A0Z != null && r44.A04(AnonymousClass0JZ.CENTER).A03 == null) {
                    AnonymousClass0QV r26 = this.A03;
                    r14 = r26.A0Z.A0c.A05;
                    r4 = this.A05;
                    i3 = r26.A02();
                } else {
                    return;
                }
                AbstractC07280Xj.A03(r4, r14, i3);
                r13 = this.A04;
                i2 = r3.A02;
                AbstractC07280Xj.A03(r13, r4, i2);
                return;
            }
        }
        if (this.A02 == AnonymousClass0JR.MATCH_CONSTRAINT) {
            AnonymousClass0QV r63 = this.A03;
            int i4 = r63.A0D;
            if (i4 == 2) {
                AnonymousClass0QV r06 = r63.A0Z;
                if (r06 != null) {
                    C02580Cy r16 = r06.A0d.A06;
                    r3.A08.add(r16);
                    r16.A07.add(r3);
                    r3.A09 = true;
                    list = r3.A07;
                    list.add(this.A05);
                    obj = this.A04;
                    list.add(obj);
                }
            } else if (i4 == 3) {
                if (r63.A0C == 3) {
                    this.A05.A03 = this;
                    this.A04.A03 = this;
                    AnonymousClass0D2 r45 = r63.A0d;
                    r45.A05.A03 = this;
                    r45.A04.A03 = this;
                    r3.A03 = this;
                    if (r63.A0F()) {
                        List list3 = r3.A08;
                        list3.add(r45.A06);
                        this.A03.A0d.A06.A07.add(r3);
                        AnonymousClass0D2 r17 = this.A03.A0d;
                        r17.A06.A03 = this;
                        list3.add(r17.A05);
                        list3.add(this.A03.A0d.A04);
                        this.A03.A0d.A05.A07.add(r3);
                        list2 = this.A03.A0d.A04.A07;
                    } else {
                        boolean A0E = r63.A0E();
                        list2 = r45.A06.A08;
                        if (A0E) {
                            list2.add(r3);
                            list = r3.A07;
                            obj = this.A03.A0d.A06;
                            list.add(obj);
                        }
                    }
                } else {
                    C02580Cy r18 = r63.A0d.A06;
                    r3.A08.add(r18);
                    r18.A07.add(r3);
                    this.A03.A0d.A05.A07.add(r3);
                    this.A03.A0d.A04.A07.add(r3);
                    r3.A09 = true;
                    List list4 = r3.A07;
                    C07270Xi r07 = this.A05;
                    list4.add(r07);
                    C07270Xi r19 = this.A04;
                    list4.add(r19);
                    r07.A08.add(r3);
                    list2 = r19.A08;
                }
                list2.add(r3);
            }
            r12.A00 = A002;
            this.A04.A00 = -r22.A00();
            return;
        }
        AnonymousClass0QV r64 = this.A03;
        AnonymousClass0Q7[] r110 = r64.A0n;
        AnonymousClass0Q7 r46 = r110[0];
        if (r46.A03 != null) {
            r22 = r110[1];
            if (r22.A03 == null) {
                r2 = AbstractC07280Xj.A01(r46);
                if (r2 != null) {
                    r1 = this.A05;
                    i = r46.A00();
                } else {
                    return;
                }
            } else if (r64.A0E()) {
                r12 = this.A05;
                A002 = r46.A00();
                r12.A00 = A002;
                this.A04.A00 = -r22.A00();
                return;
            } else {
                C07270Xi A015 = AbstractC07280Xj.A01(r46);
                C07270Xi A016 = AbstractC07280Xj.A01(r22);
                A015.A02(this);
                A016.A02(this);
                this.A08 = AnonymousClass0J2.CENTER;
                return;
            }
        } else {
            AnonymousClass0Q7 r47 = r110[1];
            if (r47.A03 != null) {
                C07270Xi A017 = AbstractC07280Xj.A01(r47);
                if (A017 != null) {
                    C07270Xi r27 = this.A04;
                    AbstractC07280Xj.A03(r27, A017, -r47.A00());
                    A0A(this.A05, r27, r3, -1);
                    return;
                }
                return;
            } else if (!(r64 instanceof AbstractC11700gi) && (r0 = r64.A0Z) != null) {
                r2 = r0.A0c.A05;
                r1 = this.A05;
                i = r64.A02();
            } else {
                return;
            }
        }
        AbstractC07280Xj.A03(r1, r2, i);
        A0A(this.A04, r1, r3, 1);
    }

    @Override // X.AbstractC07280Xj
    public void A07() {
        C07270Xi r2 = this.A05;
        if (r2.A0B) {
            this.A03.A0P = r2.A02;
        }
    }

    @Override // X.AbstractC07280Xj
    public void A08() {
        this.A07 = null;
        this.A05.A00();
        this.A04.A00();
        this.A06.A00();
        this.A09 = false;
    }

    @Override // X.AbstractC07280Xj
    public boolean A0B() {
        if (this.A02 != AnonymousClass0JR.MATCH_CONSTRAINT || this.A03.A0D == 0) {
            return true;
        }
        return false;
    }

    public void A0C() {
        this.A09 = false;
        C07270Xi r0 = this.A05;
        r0.A00();
        r0.A0B = false;
        C07270Xi r02 = this.A04;
        r02.A00();
        r02.A0B = false;
        this.A06.A0B = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0277, code lost:
        if (r8 != false) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02da, code lost:
        if (r11 != false) goto L_0x02dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0306, code lost:
        if (r2 != 1) goto L_0x0039;
     */
    @Override // X.AbstractC07280Xj, X.AbstractC11720gk
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AfH(X.AbstractC11720gk r23) {
        /*
        // Method dump skipped, instructions count: 861
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0D3.AfH(X.0gk):void");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HorizontalRun ");
        sb.append(this.A03.A0f);
        return sb.toString();
    }
}
