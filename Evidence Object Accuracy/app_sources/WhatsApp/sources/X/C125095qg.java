package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.util.Map;

/* renamed from: X.5qg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C125095qg {
    public static void A00(Context context, AnonymousClass619 r10, AnonymousClass018 r11, AnonymousClass63Y r12, C130645zk r13, Map map) {
        if (r12 != null) {
            map.put("account_fiat_currency", ((AbstractC30781Yu) r12.A01).A04);
            map.put("account_crypto_currency", r12.A02.AC1(r11));
        }
        if (r13 != null) {
            C127325uJ r8 = r13.A01;
            if (!(r8 == null || r12 == null)) {
                Object[] objArr = new Object[2];
                AbstractC30791Yv r0 = r12.A02;
                BigDecimal bigDecimal = BigDecimal.ONE;
                int i = 0;
                objArr[0] = r0.AAB(r11, bigDecimal, 0);
                AbstractC30791Yv r2 = r12.A01;
                BigDecimal bigDecimal2 = r8.A02;
                if (!bigDecimal.equals(bigDecimal2)) {
                    i = 4;
                }
                map.put("country_exchange_quote", C12960it.A0X(context, C117305Zk.A0k(r11, r2, bigDecimal2, i), objArr, 1, R.string.novi_onboarding_country_exchange_rate));
            }
            if (r10 != null) {
                map.put("account_digital_currency_description-text", r10.A00);
                map.put("account_digital_currency_description-colors", AnonymousClass619.A03(r10.A01));
                map.put("account_digital_currency_description-links", AnonymousClass619.A03(r10.A02));
                map.put("account_digital_currency_description-styles", AnonymousClass619.A03(r10.A04));
                map.put("account_digital_currency_description-scales", AnonymousClass619.A03(r10.A03));
            }
            map.put("country_code_alpha2", r13.A04);
        }
    }
}
