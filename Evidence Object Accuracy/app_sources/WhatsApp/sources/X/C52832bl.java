package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.documentpicker.DocumentPickerActivity;
import java.io.File;
import java.util.List;

/* renamed from: X.2bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52832bl extends BaseAdapter implements Filterable {
    public final C52872bp A00;
    public final /* synthetic */ DocumentPickerActivity A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean isEmpty() {
        return false;
    }

    public /* synthetic */ C52832bl(DocumentPickerActivity documentPickerActivity) {
        this.A01 = documentPickerActivity;
        this.A00 = new C52872bp(documentPickerActivity);
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return C12970iu.A09(this.A01.A0J);
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        return this.A00;
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        List list = this.A01.A0J;
        if (list == null) {
            return null;
        }
        return list.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C91604Sj r7;
        String A07;
        int i2 = 0;
        if (view != null) {
            r7 = (C91604Sj) view.getTag();
        } else {
            view = this.A01.getLayoutInflater().inflate(R.layout.document_picker_item, (ViewGroup) null, false);
            r7 = new C91604Sj(view);
            view.setTag(r7);
        }
        DocumentPickerActivity documentPickerActivity = this.A01;
        List list = documentPickerActivity.A0J;
        if (list != null) {
            AnonymousClass4XI r5 = (AnonymousClass4XI) list.get(i);
            ImageView imageView = r7.A01;
            Context context = view.getContext();
            File file = r5.A02;
            if (file == null) {
                A07 = "";
            } else {
                A07 = C14350lI.A07(file.getAbsolutePath());
            }
            imageView.setImageDrawable(C26511Dt.A03(context, C22200yh.A0O(A07), A07, false));
            r7.A04.setText(AnonymousClass3J9.A02(view.getContext(), documentPickerActivity.A0A, file.getName(), documentPickerActivity.A0H));
            r7.A03.setText(C44891zj.A03(documentPickerActivity.A0A, r5.A01));
            TextView textView = r7.A02;
            AnonymousClass018 r0 = documentPickerActivity.A0A;
            long j = r5.A00;
            textView.setText(C38131nZ.A0A(r0, j, false));
            textView.setContentDescription(C38131nZ.A0A(documentPickerActivity.A0A, j, true));
            View view2 = r7.A00;
            C12960it.A0r(documentPickerActivity, view2, R.string.checked_icon_label);
            if (documentPickerActivity.A0N.contains(r5)) {
                view.setBackgroundResource(R.drawable.contact_row_selection);
            } else {
                view.setBackgroundResource(0);
                i2 = 8;
            }
            view2.setVisibility(i2);
        }
        return view;
    }
}
