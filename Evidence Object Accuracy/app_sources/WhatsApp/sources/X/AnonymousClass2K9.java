package X;

/* renamed from: X.2K9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2K9 {
    public static String A00(String str) {
        String obj;
        if (str.isEmpty()) {
            obj = "";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".");
            obj = sb.toString();
        }
        StringBuilder sb2 = new StringBuilder(" AND  ( ");
        sb2.append(" ( ");
        sb2.append(obj);
        sb2.append("from_me");
        sb2.append(" = 1");
        sb2.append(" AND ");
        sb2.append("(  CASE ");
        sb2.append(" WHEN ");
        sb2.append(obj);
        sb2.append("status");
        sb2.append(" = ");
        sb2.append(0);
        sb2.append(" THEN 0 ");
        sb2.append(" WHEN ");
        sb2.append(obj);
        sb2.append("receipt_server_timestamp");
        sb2.append(" > 0 ");
        sb2.append(" THEN ");
        sb2.append(obj);
        sb2.append("receipt_server_timestamp");
        sb2.append(" >= ? ");
        sb2.append(" WHEN ");
        sb2.append(obj);
        sb2.append("receipt_device_timestamp");
        sb2.append(" > 0 ");
        sb2.append(" THEN ");
        sb2.append(obj);
        sb2.append("receipt_device_timestamp");
        sb2.append(" >= ? ");
        sb2.append(" WHEN ");
        sb2.append(obj);
        sb2.append("timestamp");
        sb2.append(" > 0 ");
        sb2.append(" THEN ");
        sb2.append(obj);
        sb2.append("timestamp");
        sb2.append(" >= ? ");
        sb2.append(" ELSE 0  END  )");
        sb2.append(" )");
        sb2.append(" OR ");
        sb2.append(" ( ");
        sb2.append(obj);
        sb2.append("from_me");
        sb2.append(" = 0");
        sb2.append(" AND ");
        sb2.append(obj);
        sb2.append("timestamp");
        sb2.append(" >= ? ");
        sb2.append(" ) ");
        sb2.append(" ) ");
        return sb2.toString();
    }
}
