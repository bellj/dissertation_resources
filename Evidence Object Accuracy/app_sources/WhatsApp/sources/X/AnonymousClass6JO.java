package X;

import android.text.TextUtils;
import java.util.ArrayList;

/* renamed from: X.6JO  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6JO implements Runnable {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ C130095yn A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ AnonymousClass6JO(AnonymousClass016 r1, C130095yn r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C130095yn r7 = this.A01;
        String str = this.A02;
        AnonymousClass016 r6 = this.A00;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass61S.A03("action", "novi-get-transactions", A0l);
        A0l.add(new AnonymousClass61S("wavi_only", true));
        if (!TextUtils.isEmpty(str)) {
            AnonymousClass61S.A03("before", str, A0l);
        }
        r7.A07.A0B(C117305Zk.A09(r6, r7, 12), C117315Zl.A0B("account", A0l), "get", 3);
    }
}
