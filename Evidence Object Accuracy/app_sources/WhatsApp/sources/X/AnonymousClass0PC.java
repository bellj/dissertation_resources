package X;

/* renamed from: X.0PC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PC {
    public final int A00;
    public final String A01;

    public AnonymousClass0PC(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof AnonymousClass0PC) {
            AnonymousClass0PC r4 = (AnonymousClass0PC) obj;
            if (this.A00 == r4.A00) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00;
    }
}
