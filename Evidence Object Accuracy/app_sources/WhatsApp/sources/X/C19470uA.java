package X;

import com.whatsapp.util.Log;

/* renamed from: X.0uA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19470uA implements AbstractC18270sB {
    public final C18280sC A00;
    public final C15410nB A01;

    public C19470uA(C18280sC r1, C15410nB r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        C32441cA r2 = this.A00.A00;
        StringBuilder sb = new StringBuilder("HourlyCronAction; battery ");
        sb.append(r2);
        Log.i(sb.toString());
        this.A01.A01();
    }
}
