package X;

import android.app.Application;
import com.whatsapp.contact.picker.ContactsAttachmentSelector;

/* renamed from: X.2fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53942fq extends AnonymousClass0Yo {
    public final /* synthetic */ ContactsAttachmentSelector A00;

    public C53942fq(ContactsAttachmentSelector contactsAttachmentSelector) {
        this.A00 = contactsAttachmentSelector;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C53812fG.class)) {
            ContactsAttachmentSelector contactsAttachmentSelector = this.A00;
            Application application = contactsAttachmentSelector.getApplication();
            C16590pI r5 = contactsAttachmentSelector.A03;
            C15550nR r4 = ((AbstractActivityC36611kC) contactsAttachmentSelector).A0J;
            AnonymousClass1AW r7 = contactsAttachmentSelector.A04;
            return new C53812fG(application, contactsAttachmentSelector.A00, contactsAttachmentSelector.A01, r4, r5, contactsAttachmentSelector.A0S, r7);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
