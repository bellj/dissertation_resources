package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.settings.SettingsRowIconText;
import com.whatsapp.settings.SettingsRowNoticeView;

/* renamed from: X.2cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53152cz extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53152cz(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v4, types: [com.whatsapp.settings.SettingsRowIconText] */
    public void A00() {
        AnonymousClass01J A00;
        SettingsRowNoticeView settingsRowNoticeView;
        if (this instanceof SettingsRowNoticeView) {
            SettingsRowNoticeView settingsRowNoticeView2 = (SettingsRowNoticeView) this;
            if (!settingsRowNoticeView2.A01) {
                settingsRowNoticeView2.A01 = true;
                A00 = AnonymousClass2P6.A00(settingsRowNoticeView2.generatedComponent());
                settingsRowNoticeView = settingsRowNoticeView2;
            } else {
                return;
            }
        } else if (!this.A01) {
            this.A01 = true;
            A00 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            settingsRowNoticeView = (SettingsRowIconText) this;
        } else {
            return;
        }
        settingsRowNoticeView.A04 = C12960it.A0R(A00);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
