package X;

import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape2S0300000_I0;

/* renamed from: X.1w4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC42891w4 extends AbstractActivityC42901w5 {
    public ViewGroup A00;
    public TextView A01;

    public View A2e() {
        View inflate = View.inflate(this, R.layout.share_link_action_item, null);
        ViewGroup viewGroup = this.A00;
        AnonymousClass009.A03(viewGroup);
        viewGroup.addView(inflate);
        return inflate;
    }

    public C83663xe A2f() {
        C83663xe r3 = new C83663xe();
        ViewOnClickCListenerShape0S0200000_I0 viewOnClickCListenerShape0S0200000_I0 = new ViewOnClickCListenerShape0S0200000_I0(this, 1, r3);
        ((AnonymousClass2V9) r3).A00 = A2e();
        r3.A00(viewOnClickCListenerShape0S0200000_I0, getString(R.string.copy_link), R.drawable.ic_action_copy);
        return r3;
    }

    public C83683xg A2g() {
        C83683xg r4 = new C83683xg();
        ViewOnClickCListenerShape0S0200000_I0 viewOnClickCListenerShape0S0200000_I0 = new ViewOnClickCListenerShape0S0200000_I0(this, 2, r4);
        findViewById(R.id.link_btn).setOnClickListener(new ViewOnClickCListenerShape2S0300000_I0(this, r4, viewOnClickCListenerShape0S0200000_I0, 0));
        ((AnonymousClass2V9) r4).A00 = A2e();
        r4.A00(viewOnClickCListenerShape0S0200000_I0, getString(R.string.share_link), R.drawable.ic_share);
        return r4;
    }

    public C83673xf A2h() {
        C83673xf r6 = new C83673xf();
        ViewOnClickCListenerShape0S0200000_I0 viewOnClickCListenerShape0S0200000_I0 = new ViewOnClickCListenerShape0S0200000_I0(this, 3, r6);
        String string = getString(R.string.localized_app_name);
        ((AnonymousClass2V9) r6).A00 = A2e();
        r6.A00(viewOnClickCListenerShape0S0200000_I0, getString(R.string.share_link_via_whatsapp, string), R.drawable.ic_action_forward);
        return r6;
    }

    public void A2i() {
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(this, (int) R.style.SectionDivider);
        View view = new View(contextThemeWrapper, null, R.style.SectionDivider);
        view.setLayoutParams(new LinearLayout.LayoutParams(contextThemeWrapper, (AttributeSet) null));
        ViewGroup viewGroup = this.A00;
        AnonymousClass009.A03(viewGroup);
        viewGroup.addView(view);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        setContentView(R.layout.share_link);
        this.A00 = (ViewGroup) findViewById(R.id.share_link_root);
        this.A01 = (TextView) findViewById(R.id.link);
    }
}
