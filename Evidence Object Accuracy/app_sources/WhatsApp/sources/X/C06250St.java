package X;

import android.view.View;

/* renamed from: X.0St  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06250St {
    public static final C018408o A01 = new AnonymousClass0RW().A00.A00().A00.A07().A00.A08().A00.A09();
    public final C018408o A00;

    public C06190Sn A06() {
        return null;
    }

    public void A0B(View view) {
    }

    public void A0C(AnonymousClass0U7 r1) {
    }

    public void A0D(C018408o r1) {
    }

    public boolean A0E() {
        return false;
    }

    public boolean A0F() {
        return false;
    }

    public C06250St(C018408o r1) {
        this.A00 = r1;
    }

    public AnonymousClass0U7 A00() {
        return A03();
    }

    public AnonymousClass0U7 A01() {
        return AnonymousClass0U7.A04;
    }

    public AnonymousClass0U7 A02() {
        return A03();
    }

    public AnonymousClass0U7 A03() {
        return AnonymousClass0U7.A04;
    }

    public AnonymousClass0U7 A04() {
        return A03();
    }

    public AnonymousClass0U7 A05(int i) {
        return AnonymousClass0U7.A04;
    }

    public C018408o A07() {
        return this.A00;
    }

    public C018408o A08() {
        return this.A00;
    }

    public C018408o A09() {
        return this.A00;
    }

    public C018408o A0A(int i, int i2, int i3, int i4) {
        return A01;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C06250St)) {
            return false;
        }
        C06250St r4 = (C06250St) obj;
        if (A0F() != r4.A0F() || A0E() != r4.A0E() || !C015407h.A01(A03(), r4.A03()) || !C015407h.A01(A01(), r4.A01()) || !C015407h.A01(A06(), r4.A06())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C015407h.A00(Boolean.valueOf(A0F()), Boolean.valueOf(A0E()), A03(), A01(), A06());
    }
}
