package X;

/* renamed from: X.31U  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31U extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    public Integer A0K;
    public Integer A0L;
    public Integer A0M;
    public Integer A0N;
    public Integer A0O;
    public Integer A0P;
    public Integer A0Q;
    public Integer A0R;
    public Integer A0S;
    public Integer A0T;
    public Integer A0U;
    public Integer A0V;
    public Integer A0W;
    public Integer A0X;
    public Long A0Y;

    public AnonymousClass31U() {
        super(2318, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(7, this.A01);
        r3.Abe(29, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(36, this.A04);
        r3.Abe(28, this.A05);
        r3.Abe(27, this.A06);
        r3.Abe(19, this.A07);
        r3.Abe(3, this.A08);
        r3.Abe(14, this.A09);
        r3.Abe(6, this.A0A);
        r3.Abe(5, this.A0B);
        r3.Abe(10, this.A0C);
        r3.Abe(32, this.A0D);
        r3.Abe(11, this.A0E);
        r3.Abe(20, this.A0F);
        r3.Abe(25, this.A0G);
        r3.Abe(17, this.A0H);
        r3.Abe(2, this.A0I);
        r3.Abe(30, this.A0J);
        r3.Abe(24, this.A0K);
        r3.Abe(22, this.A0L);
        r3.Abe(15, this.A0M);
        r3.Abe(31, this.A0N);
        r3.Abe(33, this.A0O);
        r3.Abe(8, this.A0P);
        r3.Abe(9, this.A0Q);
        r3.Abe(35, this.A0R);
        r3.Abe(18, this.A0S);
        r3.Abe(23, this.A0T);
        r3.Abe(16, this.A0U);
        r3.Abe(12, this.A0V);
        r3.Abe(21, this.A0W);
        r3.Abe(13, this.A0X);
        r3.Abe(26, this.A0Y);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidDatabaseMigrationDailyStatus {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationBlankMeJid", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationBroadcastMeJid", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationCallLog", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationChat", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationDropDeprecatedTables", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationEphemeral", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationEphemeralSetting", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationFrequent", C12960it.A0Y(this.A07));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationFts", C12960it.A0Y(this.A08));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationFuture", C12960it.A0Y(this.A09));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationGroupParticipant", C12960it.A0Y(this.A0A));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationJid", C12960it.A0Y(this.A0B));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationLabelJid", C12960it.A0Y(this.A0C));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationLegacyQuotedOrderMessage", C12960it.A0Y(this.A0D));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationLink", C12960it.A0Y(this.A0E));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationLocation", C12960it.A0Y(this.A0F));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationMainMessage", C12960it.A0Y(this.A0G));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationMention", C12960it.A0Y(this.A0H));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationMessageMedia", C12960it.A0Y(this.A0I));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationMessageMediaFixer", C12960it.A0Y(this.A0J));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationMissedCall", C12960it.A0Y(this.A0K));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationPayment", C12960it.A0Y(this.A0L));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationQuoted", C12960it.A0Y(this.A0M));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationQuotedOrderMessage", C12960it.A0Y(this.A0N));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationQuotedOrderMessageV2", C12960it.A0Y(this.A0O));
        String str = null;
        Integer num = this.A0P;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationReceiptDevice", str);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationReceiptUser", C12960it.A0Y(this.A0Q));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationRenameDeprecatedTables", C12960it.A0Y(this.A0R));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationRevoked", C12960it.A0Y(this.A0S));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationSendCount", C12960it.A0Y(this.A0T));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationSystem", C12960it.A0Y(this.A0U));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationText", C12960it.A0Y(this.A0V));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationThumbnail", C12960it.A0Y(this.A0W));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dbMigrationVcard", C12960it.A0Y(this.A0X));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastMigrationAtemptT", this.A0Y);
        return C12960it.A0d("}", A0k);
    }
}
