package X;

/* renamed from: X.5Xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116995Xs {
    public static final AnonymousClass1TK A00 = C72453ed.A12("1.3.6.1.4.1.188.7.1.1.2");
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B = C72453ed.A12("1.3.6.1.5.5.8.1.2");
    public static final AnonymousClass1TK A0C = C72453ed.A12("1.3.6.1.4.1.18227.2.1");
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L = C72453ed.A12("1.3.6.1.4.1.11591.4.11");
    public static final AnonymousClass1TK A0M;
    public static final AnonymousClass1TK A0N;
    public static final AnonymousClass1TK A0O;
    public static final AnonymousClass1TK A0P;
    public static final AnonymousClass1TK A0Q;
    public static final AnonymousClass1TK A0R;
    public static final AnonymousClass1TK A0S;
    public static final AnonymousClass1TK A0T;
    public static final AnonymousClass1TK A0U;
    public static final AnonymousClass1TK A0V;
    public static final AnonymousClass1TK A0W;
    public static final AnonymousClass1TK A0X;
    public static final AnonymousClass1TK A0Y;
    public static final AnonymousClass1TK A0Z;
    public static final AnonymousClass1TK A0a;
    public static final AnonymousClass1TK A0b;
    public static final AnonymousClass1TK A0c;
    public static final AnonymousClass1TK A0d;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("2.16.840.1.113730.1");
        A0M = A12;
        A0R = AnonymousClass1TL.A02("1", A12);
        A0N = AnonymousClass1TL.A02("2", A12);
        A0T = AnonymousClass1TL.A02("3", A12);
        A0O = AnonymousClass1TL.A02("4", A12);
        A0S = AnonymousClass1TL.A02("7", A12);
        A0P = AnonymousClass1TL.A02("8", A12);
        A0U = AnonymousClass1TL.A02("12", A12);
        A0Q = AnonymousClass1TL.A02("13", A12);
        AnonymousClass1TK A122 = C72453ed.A12("2.16.840.1.113733.1");
        A0X = A122;
        A0Z = AnonymousClass1TL.A02("6.3", A122);
        A0d = AnonymousClass1TL.A02("6.9", A122);
        A0c = AnonymousClass1TL.A02("6.11", A122);
        A0Y = AnonymousClass1TL.A02("6.13", A122);
        A0a = AnonymousClass1TL.A02("6.15", A122);
        A0b = AnonymousClass1TL.A02("8.1", A122);
        AnonymousClass1TK A123 = C72453ed.A12("2.16.840.1.113719");
        A0V = A123;
        A0W = AnonymousClass1TL.A02("1.9.4.1", A123);
        AnonymousClass1TK A124 = C72453ed.A12("1.2.840.113533.7");
        A09 = A124;
        A0A = AnonymousClass1TL.A02("65.0", A124);
        A02 = AnonymousClass1TL.A02("66.10", A124);
        AnonymousClass1TK A125 = C72453ed.A12("1.3.6.1.4.1.3029");
        A03 = A125;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("1", A125);
        A04 = A022;
        A07 = AnonymousClass1TL.A02("1.1", A022);
        A05 = AnonymousClass1TL.A02("1.2", A022);
        A06 = AnonymousClass1TL.A02("1.3", A022);
        A08 = AnonymousClass1TL.A02("1.4", A022);
        AnonymousClass1TK A126 = C72453ed.A12("1.3.6.1.4.1.1722.12.2");
        A01 = A126;
        A0D = AnonymousClass1TL.A02("1.5", A126);
        A0E = AnonymousClass1TL.A02("1.8", A126);
        A0F = AnonymousClass1TL.A02("1.12", A126);
        A0G = AnonymousClass1TL.A02("1.16", A126);
        A0H = AnonymousClass1TL.A02("2.4", A126);
        A0I = AnonymousClass1TL.A02("2.5", A126);
        A0J = AnonymousClass1TL.A02("2.7", A126);
        A0K = AnonymousClass1TL.A02("2.8", A126);
    }
}
