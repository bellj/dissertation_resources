package X;

/* renamed from: X.5si  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126335si {
    public final AnonymousClass1V8 A00;

    public C126335si(C126345sj r10, AnonymousClass3CT r11, String str, String str2, String str3, String str4) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-verify-send-otp");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (C117295Zj.A1W(str2, 1, false)) {
            C41141sy.A01(A0N, "nonce", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 2000, false)) {
            C41141sy.A01(A0N, "otp", str3);
        }
        if (str4 != null && C117295Zj.A1W(str4, 1, true)) {
            C41141sy.A01(A0N, "identifier", str4);
        }
        if (r10 != null) {
            A0N.A05(r10.A00);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r11);
    }
}
