package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.util.Log;

/* renamed from: X.1HS  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HS extends BroadcastReceiver {
    public final C18640sm A00;
    public final C247716u A01;
    public final AnonymousClass01d A02;
    public final C14830m7 A03;
    public final C14850m9 A04;
    public final C22050yP A05;
    public final C21940yE A06;
    public final Object A07 = new Object();
    public volatile boolean A08 = false;

    public AnonymousClass1HS(C18640sm r2, C247716u r3, AnonymousClass01d r4, C14830m7 r5, C14850m9 r6, C22050yP r7, C21940yE r8) {
        this.A03 = r5;
        this.A04 = r6;
        this.A02 = r4;
        this.A05 = r7;
        this.A01 = r3;
        this.A06 = r8;
        this.A00 = r2;
    }

    public static AnonymousClass1I0 A00(NetworkInfo networkInfo) {
        if (networkInfo == null) {
            return null;
        }
        boolean z = true;
        boolean z2 = false;
        if (networkInfo.getType() == 1) {
            z2 = true;
        }
        if (networkInfo.getType() != 0) {
            z = false;
        }
        return new AnonymousClass1I0(networkInfo.getTypeName(), networkInfo.getSubtypeName(), networkInfo.getSubtype(), z2, z, networkInfo.isConnected(), networkInfo.isRoaming());
    }

    public static /* synthetic */ void A01(Context context, AnonymousClass1HS r8) {
        boolean z;
        if (Build.VERSION.SDK_INT < 29 || !r8.A04.A07(614)) {
            z = false;
        } else {
            z = r8.A02();
        }
        AnonymousClass1I0 A00 = A00(r8.A01.A01());
        long A002 = r8.A03.A00();
        int i = Build.VERSION.SDK_INT;
        if (i < 29 || !z) {
            IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            if (i >= 24) {
                intentFilter.addAction("android.net.conn.RESTRICT_BACKGROUND_CHANGED");
            }
            if (context.registerReceiver(r8, intentFilter) == null) {
                Log.i("CONNECTIVITY_ACTION doesn't return a sticky intent, update voip network medium directly");
                C21940yE r5 = r8.A06;
                r5.A00();
                if (r5.A01.A03()) {
                    r5.A02.execute(new RunnableBRunnable0Shape7S0100000_I0_7(r5, 19));
                }
            }
        } else if (i >= 24) {
            IntentFilter intentFilter2 = new IntentFilter();
            if (i >= 24) {
                intentFilter2.addAction("android.net.conn.RESTRICT_BACKGROUND_CHANGED");
            }
            context.registerReceiver(r8, intentFilter2);
            r8.A00.A09(A00, r8.A05, r8.A06);
        } else {
            throw new IllegalArgumentException("For <24 versions we must register for CONNECTIVITY_ACTION");
        }
        r8.A00.A0A(AnonymousClass1I1.A00(A00, A002));
        r8.A05.A06(A00);
    }

    public final boolean A02() {
        AnonymousClass01d r0 = this.A02;
        return this.A00.A0E(r0.A0H(), r0.A0N());
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A08) {
            synchronized (this.A07) {
                if (!this.A08) {
                    AnonymousClass22D.A00(context);
                    this.A08 = true;
                }
            }
        }
        if (intent.getAction().equals("android.net.conn.RESTRICT_BACKGROUND_CHANGED")) {
            C21940yE r3 = this.A06;
            if (r3.A01.A03()) {
                r3.A02.execute(new RunnableBRunnable0Shape7S0100000_I0_7(r3, 19));
                return;
            }
            return;
        }
        C18640sm r32 = this.A00;
        AnonymousClass1I0 A07 = r32.A07();
        r32.A0A(AnonymousClass1I1.A00(A07, this.A03.A00()));
        this.A06.A00();
        this.A05.A06(A07);
    }
}
