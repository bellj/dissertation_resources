package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ln  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100364ln implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1W9(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1W9[i];
    }
}
