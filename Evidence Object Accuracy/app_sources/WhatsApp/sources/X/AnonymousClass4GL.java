package X;

import java.util.HashSet;

/* renamed from: X.4GL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4GL {
    public static final HashSet A00;

    static {
        Byte[] bArr = new Byte[12];
        bArr[0] = (byte) 1;
        bArr[1] = (byte) 2;
        bArr[2] = (byte) 2;
        bArr[3] = (byte) 4;
        bArr[4] = (byte) 5;
        bArr[5] = (byte) 8;
        bArr[6] = (byte) 11;
        bArr[7] = (byte) 13;
        bArr[8] = (byte) 15;
        bArr[9] = (byte) 20;
        bArr[10] = (byte) 24;
        A00 = C12970iu.A13((byte) -2, bArr, 11);
    }
}
