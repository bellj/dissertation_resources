package X;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: X.33n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620233n extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;
    public final /* synthetic */ String A02 = "tos_no_wallet";
    public final /* synthetic */ String A03 = "tos_merchant";
    public final /* synthetic */ boolean A04;
    public final /* synthetic */ boolean A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C620233n(Context context, C14900mE r4, AnonymousClass1FK r5, C18650sn r6, C18610sj r7, boolean z, boolean z2) {
        super(context, r4, r6);
        this.A01 = r7;
        this.A00 = r5;
        this.A04 = z;
        this.A05 = z2;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r3) {
        this.A01.A0I.A05(C12960it.A0b("TosV2 onRequestError: ", r3));
        this.A00.AV3(r3);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r3) {
        this.A01.A0I.A05(C12960it.A0b("TosV2 onResponseError: ", r3));
        this.A00.AVA(r3);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r9) {
        AnonymousClass1V8 A0E = r9.A0E("accept_pay");
        AnonymousClass46N r3 = new AnonymousClass46N();
        boolean z = false;
        if (A0E != null) {
            String A0I = A0E.A0I("consumer", null);
            String A0I2 = A0E.A0I("merchant", null);
            if ((!this.A04 || "1".equals(A0I)) && (!this.A05 || "1".equals(A0I2))) {
                z = true;
            }
            r3.A02 = z;
            r3.A00 = "1".equals(A0E.A0I("outage", null));
            r3.A01 = "1".equals(A0E.A0I("sandbox", null));
            if (!TextUtils.isEmpty(A0I)) {
                String str = this.A02;
                if (!TextUtils.isEmpty(str)) {
                    C21860y6 r2 = this.A01.A09;
                    C32641cU A01 = r2.A01(str);
                    if ("1".equals(A0I)) {
                        r2.A06(A01);
                    } else {
                        r2.A05(A01);
                    }
                }
            }
            if (!TextUtils.isEmpty(A0I2)) {
                String str2 = this.A03;
                if (!TextUtils.isEmpty(str2)) {
                    C18660so r22 = this.A01.A0C;
                    C32641cU A012 = r22.A01(str2);
                    if ("1".equals(A0I2)) {
                        r22.A06(A012);
                    } else {
                        r22.A05(A012);
                    }
                }
            }
            this.A01.A0D.A0L(r3.A01);
        } else {
            r3.A02 = false;
        }
        this.A00.AVB(r3);
    }
}
