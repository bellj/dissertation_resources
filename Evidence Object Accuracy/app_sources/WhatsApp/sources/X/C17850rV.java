package X;

import android.text.TextUtils;

/* renamed from: X.0rV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17850rV implements AbstractC17860rW {
    public final C15450nH A00;
    public final C14820m6 A01;
    public final AnonymousClass018 A02;
    public final C14850m9 A03;

    public C17850rV(C15450nH r1, C14820m6 r2, AnonymousClass018 r3, C14850m9 r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC17860rW
    public String ABp() {
        C14820m6 r0 = this.A01;
        String A0B = r0.A0B();
        String A0C = r0.A0C();
        if (TextUtils.isEmpty(A0B) || TextUtils.isEmpty(A0C)) {
            return this.A02.A05();
        }
        return C22650zQ.A01(A0B, A0C);
    }
}
