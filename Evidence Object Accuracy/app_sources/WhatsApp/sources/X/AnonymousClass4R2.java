package X;

/* renamed from: X.4R2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R2 {
    public int A00 = 0;
    public final int A01;
    public final int A02;
    public final byte[] A03;

    public AnonymousClass4R2(int i, int i2) {
        this.A02 = i;
        this.A01 = i2;
        this.A03 = new byte[(i2 << 1) - 1];
    }
}
