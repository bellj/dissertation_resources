package X;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;

/* renamed from: X.0CW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CW extends C02360Bs {
    public MenuItem A00;
    public AbstractC12310hi A01;
    public final int A02;
    public final int A03;

    public AnonymousClass0CW(Context context, boolean z) {
        super(context, z);
        Configuration configuration = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
            this.A02 = 22;
            this.A03 = 21;
            return;
        }
        this.A02 = 21;
        this.A03 = 22;
    }

    @Override // X.C02360Bs, android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int i;
        int pointToPosition;
        int i2;
        if (this.A01 != null) {
            ListAdapter adapter = getAdapter();
            if (adapter instanceof HeaderViewListAdapter) {
                HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                i = headerViewListAdapter.getHeadersCount();
                adapter = headerViewListAdapter.getWrappedAdapter();
            } else {
                i = 0;
            }
            AnonymousClass0BQ r4 = (AnonymousClass0BQ) adapter;
            C07340Xp r3 = null;
            if (motionEvent.getAction() != 10 && (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) != -1 && (i2 = pointToPosition - i) >= 0 && i2 < r4.getCount()) {
                r3 = r4.getItem(i2);
            }
            MenuItem menuItem = this.A00;
            if (menuItem != r3) {
                AnonymousClass07H r1 = r4.A01;
                if (menuItem != null) {
                    this.A01.ARc(menuItem, r1);
                }
                this.A00 = r3;
                if (r3 != null) {
                    this.A01.ARb(r3, r1);
                }
            }
        }
        return super.onHoverEvent(motionEvent);
    }

    @Override // android.widget.ListView, android.view.KeyEvent.Callback, android.widget.AbsListView, android.view.View
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
        if (listMenuItemView != null) {
            if (i == this.A02) {
                if (listMenuItemView.isEnabled() && listMenuItemView.A0D.hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (i == this.A03) {
                setSelection(-1);
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
                }
                ((AnonymousClass0BQ) adapter).A01.A0F(false);
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void setHoverListener(AbstractC12310hi r1) {
        this.A01 = r1;
    }
}
