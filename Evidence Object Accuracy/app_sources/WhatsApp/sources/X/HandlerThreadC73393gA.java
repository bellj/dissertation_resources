package X;

import android.os.HandlerThread;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;

/* renamed from: X.3gA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerThreadC73393gA extends HandlerThread {
    public final /* synthetic */ VoipPhysicalCamera A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerThreadC73393gA(VoipPhysicalCamera voipPhysicalCamera) {
        super("VoipCameraThread");
        this.A00 = voipPhysicalCamera;
    }

    @Override // android.os.HandlerThread, java.lang.Thread, java.lang.Runnable
    public void run() {
        Log.i("voip/video/VoipCamera/CameraThread Start");
        super.run();
        Log.i("voip/video/VoipCamera/CameraThread Exit");
    }
}
