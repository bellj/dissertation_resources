package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC121025h8 extends AbstractC1316063k {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(31);
    public String A00;
    public boolean A01;
    public final int A02;
    public final C1316463o A03;
    public final AnonymousClass603 A04;
    public final C1316763r A05;
    public final C1316363n A06;
    public final String A07;

    public AbstractC121025h8(AnonymousClass102 r3, AnonymousClass1V8 r4) {
        super(r4);
        int A00;
        String A0H = r4.A0H("type");
        if ("CASH".equalsIgnoreCase(A0H)) {
            A00 = 1;
        } else {
            A00 = C117305Zk.A00("BANK".equalsIgnoreCase(A0H) ? 1 : 0);
        }
        this.A02 = A00;
        this.A00 = r4.A0I("code", "");
        this.A07 = r4.A0H("status");
        this.A01 = "true".equals(r4.A0I("is_cancelable", "false"));
        this.A04 = AnonymousClass603.A00(r3, r4.A0F("quote"));
        this.A06 = C1316363n.A00(r3, r4.A0F("transaction-amount"));
        this.A03 = C1316463o.A00(r4.A0E("claim"));
        this.A05 = C1316763r.A01(r4.A0E("refund_transaction"));
    }

    public AbstractC121025h8(Parcel parcel) {
        super(parcel);
        this.A02 = parcel.readInt();
        this.A00 = parcel.readString();
        this.A01 = C12970iu.A1W(parcel.readByte());
        this.A07 = parcel.readString();
        this.A04 = new AnonymousClass603((AnonymousClass6F2) C12990iw.A0I(parcel, AnonymousClass603.class), (AnonymousClass6F2) C12990iw.A0I(parcel, AnonymousClass603.class), (AnonymousClass6F2) C12990iw.A0I(parcel, AnonymousClass603.class), C12990iw.A0p(parcel), parcel.readLong());
        this.A06 = (C1316363n) C12990iw.A0I(parcel, C1316363n.class);
        this.A03 = (C1316463o) C12990iw.A0I(parcel, C1316463o.class);
        this.A05 = (C1316763r) C12990iw.A0I(parcel, C1316763r.class);
    }

    public AbstractC121025h8(String str) {
        super(str);
        AnonymousClass603 r2;
        JSONObject A05 = C13000ix.A05(str);
        this.A02 = A05.getInt("type");
        this.A00 = A05.getString("code");
        this.A07 = A05.optString("status");
        this.A01 = C12970iu.A1W(A05.getInt("is_cancelable"));
        String optString = A05.optString("quote");
        if (!TextUtils.isEmpty(optString)) {
            try {
                JSONObject A052 = C13000ix.A05(optString);
                r2 = new AnonymousClass603(AnonymousClass6F2.A01(A052.getString("source")), AnonymousClass6F2.A01(A052.getString("target")), AnonymousClass6F2.A01(A052.getString("fee")), A052.getString("id"), A052.getLong("expiry-ts"));
            } catch (JSONException e) {
                Log.w("PAY: TradeQuote fromJsonString threw: ", e);
            }
            AnonymousClass009.A05(r2);
            this.A04 = r2;
            C1316363n A01 = C1316363n.A01(A05.getString("transaction_amount"));
            AnonymousClass009.A05(A01);
            this.A06 = A01;
            this.A03 = C1316463o.A01(A05.optString("claim"));
            this.A05 = AbstractC1316063k.A01(A05);
        }
        r2 = null;
        AnonymousClass009.A05(r2);
        this.A04 = r2;
        C1316363n A01 = C1316363n.A01(A05.getString("transaction_amount"));
        AnonymousClass009.A05(A01);
        this.A06 = A01;
        this.A03 = C1316463o.A01(A05.optString("claim"));
        this.A05 = AbstractC1316063k.A01(A05);
    }

    public static AbstractC121025h8 A00(AnonymousClass102 r2, AnonymousClass1V8 r3) {
        String A0H = r3.A0H("type");
        if ("CASH".equalsIgnoreCase(A0H)) {
            return new C121015h7(r2, r3);
        }
        if ("BANK".equalsIgnoreCase(A0H)) {
            return new C121005h6(r2, r3);
        }
        throw new AnonymousClass1V9("Unsupported WithdrawalType");
    }

    @Override // X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        try {
            jSONObject.put("type", this.A02);
            jSONObject.put("code", this.A00);
            jSONObject.put("status", this.A07);
            jSONObject.put("is_cancelable", C12960it.A1S(this.A01 ? 1 : 0) ? 1 : 0);
            AnonymousClass603 r5 = this.A04;
            JSONObject A0a = C117295Zj.A0a();
            try {
                A0a.put("id", r5.A04);
                A0a.put("expiry-ts", r5.A00);
                C117315Zl.A0W(r5.A02, "source", A0a);
                C117315Zl.A0W(r5.A03, "target", A0a);
                C117315Zl.A0W(r5.A01, "fee", A0a);
            } catch (JSONException e) {
                Log.w("PAY: TradeQuote toJson threw: ", e);
            }
            jSONObject.put("quote", A0a);
            jSONObject.put("transaction_amount", this.A06.A02());
            C1316463o r0 = this.A03;
            if (r0 != null) {
                jSONObject.put("claim", r0.A02());
            }
            C1316763r r4 = this.A05;
            if (r4 != null) {
                JSONObject A0a2 = C117295Zj.A0a();
                int i = r4.A01;
                A0a2.put("reason", i != 1 ? i != 2 ? null : "ASYNC_NOVI_INITIATED" : "CLAIM");
                A0a2.put("completed_timestamp_seconds", r4.A00);
                jSONObject.put("refund_transaction", A0a2);
            }
        } catch (JSONException unused) {
            Log.w("PAY:NoviTransactionWithdrawal failed to create the JSON");
        }
    }

    @Override // X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A02);
        parcel.writeString(this.A00);
        parcel.writeByte(this.A01 ? (byte) 1 : 0);
        parcel.writeString(this.A07);
        AnonymousClass603 r2 = this.A04;
        parcel.writeString(r2.A04);
        parcel.writeLong(r2.A00);
        parcel.writeParcelable(r2.A02, i);
        parcel.writeParcelable(r2.A03, i);
        parcel.writeParcelable(r2.A01, i);
        parcel.writeParcelable(this.A06, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A05, i);
    }
}
