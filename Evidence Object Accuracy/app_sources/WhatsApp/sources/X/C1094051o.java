package X;

import java.util.Iterator;

/* renamed from: X.51o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094051o implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r6, AbstractC94534c0 r7, AnonymousClass4RG r8) {
        C83023wZ A07 = r7.A07();
        if (!C72473ef.A00(r6 instanceof C83003wX ? 1 : 0)) {
            return false;
        }
        AbstractC94534c0 A00 = C83003wX.A00(r6);
        if (!(A00 instanceof C83023wZ)) {
            return true;
        }
        C83023wZ A072 = A00.A07();
        Iterator it = A07.iterator();
        while (it.hasNext()) {
            if (!A072.A00.contains(it.next())) {
                return false;
            }
        }
        return true;
    }
}
