package X;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentMethodSelectionActivity;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.io.Serializable;
import java.util.List;

/* renamed from: X.6CT  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass6CT implements AbstractC1311861p {
    @Override // X.AbstractC1311861p
    public View AFP(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return null;
    }

    @Override // X.AbstractC1311861p
    public List A9w(List list) {
        C1315463e A02;
        if (this instanceof AbstractC121845k9) {
            AnonymousClass61F r1 = ((AbstractC121845k9) this).A03;
            AbstractC28901Pl A00 = AnonymousClass61F.A00(list);
            if (A00 == null || (A02 = AnonymousClass61F.A02(A00)) == null) {
                return list;
            }
            return r1.A05(A02, list);
        } else if (this instanceof C121855kA) {
            return null;
        } else {
            return list;
        }
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ int AAc() {
        if (!(this instanceof C121905kS)) {
            return R.string.payments_settings_add_new_account;
        }
        return R.string.novi_deposit_add_card_row_text;
    }

    @Override // X.AbstractC1311861p
    public View AAd(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (!(this instanceof C121855kA)) {
            return null;
        }
        AbstractActivityC121655j9 r1 = ((C121855kA) this).A00;
        if (r1.A0D.size() == 0) {
            return null;
        }
        View inflate = layoutInflater.inflate(R.layout.india_upi_payment_bottomsheet_view_balance_row, (ViewGroup) null);
        C117295Zj.A0m(inflate, R.id.check_balance_icon, AnonymousClass00T.A00(r1, R.color.settings_icon));
        return inflate;
    }

    @Override // X.AbstractC1311861p
    public View AD5(LayoutInflater layoutInflater, FrameLayout frameLayout) {
        if (!(this instanceof C121855kA)) {
            return null;
        }
        return C12960it.A0F(layoutInflater, frameLayout, R.layout.powered_by_upi_logo);
    }

    @Override // X.AbstractC1311861p
    public int AEM(AbstractC28901Pl r3) {
        if ((this instanceof C121855kA) && r3.equals(((C121855kA) this).A00.A02)) {
            return R.drawable.countrypicker_checkmark;
        }
        return 0;
    }

    @Override // X.AbstractC1311861p
    public String AEP(AbstractC28901Pl r4) {
        if (this instanceof AbstractC121845k9) {
            return r4 instanceof C30881Ze ? C1311161i.A04(((AbstractC121845k9) this).A00, (C30881Ze) r4) : "";
        } else if (!(this instanceof C121835k8)) {
            return null;
        } else {
            AnonymousClass1ZY r0 = r4.A08;
            AnonymousClass009.A05(r0);
            boolean A0A = r0.A0A();
            AnonymousClass6CN r02 = ((C121835k8) this).A01;
            if (!A0A) {
                return r02.A03.getString(R.string.payment_method_unverified);
            }
            BrazilPaymentActivity brazilPaymentActivity = r02.A03;
            if (!((AbstractActivityC121685jC) brazilPaymentActivity).A0O.A08()) {
                return null;
            }
            if (r4.A01 == 2) {
                return brazilPaymentActivity.A01.getString(R.string.p2p_default_method_message_enabled);
            }
            if (r4.A03 == 2) {
                return brazilPaymentActivity.A01.getString(R.string.p2m_default_method_message_enabled);
            }
            return null;
        }
    }

    @Override // X.AbstractC1311861p
    public String AEQ(AbstractC28901Pl r5) {
        if (this instanceof AbstractC121845k9) {
            AbstractC121845k9 r0 = (AbstractC121845k9) this;
            return C1311161i.A02(r0.A00, r0.A01, r5, r0.A02, true);
        } else if (!(this instanceof C121855kA)) {
            return null;
        } else {
            AbstractActivityC121655j9 r3 = ((C121855kA) this).A00;
            return C1311161i.A02(r3, ((AbstractActivityC121545iU) r3).A01, r5, ((AbstractActivityC121685jC) r3).A0P, false);
        }
    }

    @Override // X.AbstractC1311861p
    public void AM1() {
        Intent intent;
        if (this instanceof C121855kA) {
            AbstractActivityC121655j9 r3 = ((C121855kA) this).A00;
            if (r3.A0D.size() == 1) {
                C119755f3 r0 = (C119755f3) C117315Zl.A08(r3.A0D, 0).A08;
                if (r0 == null || C12970iu.A1Y(r0.A05.A00)) {
                    AbstractC28901Pl A08 = C117315Zl.A08(r3.A0D, 0);
                    intent = C12990iw.A0D(r3, IndiaUpiCheckBalanceActivity.class);
                    C117315Zl.A0M(intent, A08);
                } else {
                    r3.A3O(r3.A08, "ConfirmPaymentFragment");
                    C004802e A0S = C12980iv.A0S(r3);
                    A0S.A07(R.string.upi_check_balance_no_pin_set_title);
                    A0S.A06(R.string.upi_check_balance_no_pin_set_message);
                    C117295Zj.A0q(A0S, r3, 19, R.string.learn_more);
                    A0S.setNegativeButton(R.string.ok, null);
                    A0S.A04(new IDxDListenerShape14S0100000_3_I1(r3, 8));
                    C12970iu.A1J(A0S);
                    return;
                }
            } else {
                List list = r3.A0D;
                intent = C12990iw.A0D(r3, IndiaUpiPaymentMethodSelectionActivity.class);
                intent.putExtra("bank_accounts", (Serializable) list);
            }
            r3.A2E(intent, 1015);
        }
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ void AMp() {
        if (this instanceof C121905kS) {
            C128365vz r2 = new AnonymousClass610("BACK_CLICK", "ADD_MONEY", "REVIEW_TRANSACTION", "ARROW").A00;
            r2.A0i = "PAYMENT_METHODS";
            r2.A0T = "DEBIT";
            C123475nD r1 = ((C121905kS) this).A00;
            C30881Ze r0 = r1.A00;
            if (r0 != null) {
                r2.A0S = r0.A0A;
            }
            r1.A0J.A05(r2);
        }
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ boolean AdN(AbstractC28901Pl r3) {
        if (!(this instanceof AbstractC121845k9)) {
            return false;
        }
        AnonymousClass1ZY r1 = r3.A08;
        if (r1 instanceof AbstractC30871Zd) {
            return !"ACTIVE".equals(((AbstractC30871Zd) r1).A0I);
        }
        return false;
    }

    @Override // X.AbstractC1311861p
    public boolean AdV() {
        return (this instanceof C121835k8) || (this instanceof C121825k7);
    }

    @Override // X.AbstractC1311861p
    public boolean AdZ() {
        return !(this instanceof C121855kA);
    }

    @Override // X.AbstractC1311861p
    public void Adj(AbstractC28901Pl r3, PaymentMethodRow paymentMethodRow) {
        C129945yY r0;
        if (this instanceof C121835k8) {
            C121835k8 r1 = (C121835k8) this;
            if (C1311161i.A0B(r3)) {
                r0 = r1.A01.A03.A0R;
            } else {
                return;
            }
        } else if (this instanceof C121825k7) {
            C121825k7 r12 = (C121825k7) this;
            if (C1311161i.A0B(r3)) {
                r0 = r12.A00.A0H;
            } else {
                return;
            }
        } else {
            return;
        }
        r0.A02(r3, paymentMethodRow);
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ void onCreate() {
        if (this instanceof C121905kS) {
            C128365vz r2 = AnonymousClass610.A02("NAVIGATION_START", "ADD_MONEY").A00;
            r2.A0i = "PAYMENT_METHODS";
            r2.A0T = "DEBIT";
            C123475nD r1 = ((C121905kS) this).A00;
            C30881Ze r0 = r1.A00;
            if (r0 != null) {
                r2.A0S = r0.A0A;
            }
            r1.A0J.A05(r2);
        }
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ void onDestroy() {
        if (this instanceof C121905kS) {
            C128365vz r2 = AnonymousClass610.A02("NAVIGATION_END", "ADD_MONEY").A00;
            r2.A0i = "PAYMENT_METHODS";
            r2.A0T = "DEBIT";
            C123475nD r1 = ((C121905kS) this).A00;
            C30881Ze r0 = r1.A00;
            if (r0 != null) {
                r2.A0S = r0.A0A;
            }
            r1.A0J.A05(r2);
        }
    }
}
