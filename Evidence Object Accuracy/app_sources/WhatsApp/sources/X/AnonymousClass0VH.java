package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VH implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass0E7(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new AnonymousClass0E7(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass0E7[i];
    }
}
