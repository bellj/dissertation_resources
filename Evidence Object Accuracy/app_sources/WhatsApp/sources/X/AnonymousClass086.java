package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.lang.ref.WeakReference;
import java.util.Arrays;

/* renamed from: X.086  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass086 {
    public int A00 = -1;
    public int A01 = 0;
    public Typeface A02;
    public C016107p A03;
    public C016107p A04;
    public C016107p A05;
    public C016107p A06;
    public C016107p A07;
    public C016107p A08;
    public C016107p A09;
    public boolean A0A;
    public final TextView A0B;
    public final AnonymousClass08C A0C;

    public AnonymousClass086(TextView textView) {
        this.A0B = textView;
        this.A0C = new AnonymousClass08C(textView);
    }

    public static C016107p A00(Context context, C011905s r3, int i) {
        ColorStateList A03;
        synchronized (r3) {
            A03 = r3.A00.A03(context, i);
        }
        if (A03 == null) {
            return null;
        }
        C016107p r1 = new C016107p();
        r1.A02 = true;
        r1.A00 = A03;
        return r1;
    }

    public static void A01(EditorInfo editorInfo, InputConnection inputConnection, TextView textView) {
        if (Build.VERSION.SDK_INT < 30 && inputConnection != null) {
            EditorInfoCompat.setInitialSurroundingText(editorInfo, textView.getText());
        }
    }

    public void A02() {
        if (!(this.A05 == null && this.A09 == null && this.A06 == null && this.A03 == null)) {
            Drawable[] compoundDrawables = this.A0B.getCompoundDrawables();
            A09(compoundDrawables[0], this.A05);
            A09(compoundDrawables[1], this.A09);
            A09(compoundDrawables[2], this.A06);
            A09(compoundDrawables[3], this.A03);
        }
        if (Build.VERSION.SDK_INT < 17) {
            return;
        }
        if (this.A07 != null || this.A04 != null) {
            Drawable[] compoundDrawablesRelative = this.A0B.getCompoundDrawablesRelative();
            A09(compoundDrawablesRelative[0], this.A07);
            A09(compoundDrawablesRelative[2], this.A04);
        }
    }

    public void A03(int i) {
        AnonymousClass08C r4 = this.A0C;
        if (!(!(r4.A09 instanceof AnonymousClass011))) {
            return;
        }
        if (i == 0) {
            r4.A03 = 0;
            r4.A01 = -1.0f;
            r4.A00 = -1.0f;
            r4.A02 = -1.0f;
            r4.A07 = new int[0];
            r4.A06 = false;
        } else if (i == 1) {
            DisplayMetrics displayMetrics = r4.A08.getResources().getDisplayMetrics();
            r4.A05(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
            if (r4.A07()) {
                r4.A04();
            }
        } else {
            StringBuilder sb = new StringBuilder("Unknown auto-size text type: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public void A04(int i, int i2, int i3, int i4) {
        AnonymousClass08C r4 = this.A0C;
        if (!(r4.A09 instanceof AnonymousClass011)) {
            DisplayMetrics displayMetrics = r4.A08.getResources().getDisplayMetrics();
            r4.A05(TypedValue.applyDimension(i4, (float) i, displayMetrics), TypedValue.applyDimension(i4, (float) i2, displayMetrics), TypedValue.applyDimension(i4, (float) i3, displayMetrics));
            if (r4.A07()) {
                r4.A04();
            }
        }
    }

    public void A05(Context context, int i) {
        String string;
        ColorStateList A01;
        ColorStateList A012;
        ColorStateList A013;
        C013406h r2 = new C013406h(context, context.obtainStyledAttributes(i, AnonymousClass07O.A0M));
        TypedArray typedArray = r2.A02;
        if (typedArray.hasValue(14)) {
            this.A0B.setAllCaps(typedArray.getBoolean(14, false));
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 23) {
            if (typedArray.hasValue(3) && (A013 = r2.A01(3)) != null) {
                this.A0B.setTextColor(A013);
            }
            if (typedArray.hasValue(5) && (A012 = r2.A01(5)) != null) {
                this.A0B.setLinkTextColor(A012);
            }
            if (typedArray.hasValue(4) && (A01 = r2.A01(4)) != null) {
                this.A0B.setHintTextColor(A01);
            }
        }
        if (typedArray.hasValue(0) && typedArray.getDimensionPixelSize(0, -1) == 0) {
            this.A0B.setTextSize(0, 0.0f);
        }
        A06(context, r2);
        if (i2 >= 26 && typedArray.hasValue(13) && (string = typedArray.getString(13)) != null) {
            this.A0B.setFontVariationSettings(string);
        }
        r2.A04();
        Typeface typeface = this.A02;
        if (typeface != null) {
            this.A0B.setTypeface(typeface, this.A01);
        }
    }

    public final void A06(Context context, C013406h r19) {
        String string;
        Typeface create;
        Typeface A03;
        int i = this.A01;
        TypedArray typedArray = r19.A02;
        this.A01 = typedArray.getInt(2, i);
        int i2 = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i2 >= 28) {
            int i3 = typedArray.getInt(11, -1);
            this.A00 = i3;
            if (i3 != -1) {
                this.A01 = (this.A01 & 2) | 0;
            }
        }
        int i4 = 10;
        if (typedArray.hasValue(10) || typedArray.hasValue(12)) {
            this.A02 = null;
            if (typedArray.hasValue(12)) {
                i4 = 12;
            }
            int i5 = this.A00;
            int i6 = this.A01;
            if (!context.isRestricted()) {
                AnonymousClass08J r13 = new AnonymousClass08J(this, new WeakReference(this.A0B), i5, i6);
                try {
                    int i7 = this.A01;
                    int resourceId = typedArray.getResourceId(i4, 0);
                    if (resourceId != 0) {
                        TypedValue typedValue = r19.A00;
                        if (typedValue == null) {
                            typedValue = new TypedValue();
                            r19.A00 = typedValue;
                        }
                        Context context2 = r19.A01;
                        if (!context2.isRestricted() && (A03 = AnonymousClass00X.A03(context2, typedValue, r13, resourceId, i7, true)) != null) {
                            if (i2 >= 28 && this.A00 != -1) {
                                Typeface create2 = Typeface.create(A03, 0);
                                int i8 = this.A00;
                                boolean z2 = false;
                                if ((this.A01 & 2) != 0) {
                                    z2 = true;
                                }
                                A03 = Typeface.create(create2, i8, z2);
                            }
                            this.A02 = A03;
                        }
                    }
                    boolean z3 = false;
                    if (this.A02 == null) {
                        z3 = true;
                    }
                    this.A0A = z3;
                } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
                }
            }
            if (this.A02 == null && (string = typedArray.getString(i4)) != null) {
                if (i2 < 28 || this.A00 == -1) {
                    create = Typeface.create(string, this.A01);
                } else {
                    Typeface create3 = Typeface.create(string, 0);
                    int i9 = this.A00;
                    if ((this.A01 & 2) != 0) {
                        z = true;
                    }
                    create = Typeface.create(create3, i9, z);
                }
            } else {
                return;
            }
        } else if (typedArray.hasValue(1)) {
            this.A0A = false;
            int i10 = typedArray.getInt(1, 1);
            if (i10 == 1) {
                create = Typeface.SANS_SERIF;
            } else if (i10 == 2) {
                create = Typeface.SERIF;
            } else if (i10 == 3) {
                create = Typeface.MONOSPACE;
            } else {
                return;
            }
        } else {
            return;
        }
        this.A02 = create;
    }

    public void A07(ColorStateList colorStateList) {
        C016107p r1 = this.A08;
        if (r1 == null) {
            r1 = new C016107p();
            this.A08 = r1;
        }
        r1.A00 = colorStateList;
        boolean z = false;
        if (colorStateList != null) {
            z = true;
        }
        r1.A02 = z;
        this.A05 = r1;
        this.A09 = r1;
        this.A06 = r1;
        this.A03 = r1;
        this.A07 = r1;
        this.A04 = r1;
    }

    public void A08(PorterDuff.Mode mode) {
        C016107p r1 = this.A08;
        if (r1 == null) {
            r1 = new C016107p();
            this.A08 = r1;
        }
        r1.A01 = mode;
        boolean z = false;
        if (mode != null) {
            z = true;
        }
        r1.A03 = z;
        this.A05 = r1;
        this.A09 = r1;
        this.A06 = r1;
        this.A03 = r1;
        this.A07 = r1;
        this.A04 = r1;
    }

    public final void A09(Drawable drawable, C016107p r3) {
        if (drawable != null && r3 != null) {
            C012005t.A02(drawable, r3, this.A0B.getDrawableState());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x020e  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0233  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0277  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02b6  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02f6  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0302  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0319  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0324  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0364  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x0389  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0393  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x03d2  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x03d6  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x03da  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x03de  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x03e5  */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x03e8  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x03eb  */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x03ee  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x03f1  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x03f4  */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x03fc  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x0400  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0404  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0408  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x042f  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0438  */
    /* JADX WARNING: Removed duplicated region for block: B:249:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01b4  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01d5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(android.util.AttributeSet r27, int r28) {
        /*
        // Method dump skipped, instructions count: 1104
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass086.A0A(android.util.AttributeSet, int):void");
    }

    public void A0B(int[] iArr, int i) {
        AnonymousClass08C r4 = this.A0C;
        if (!(r4.A09 instanceof AnonymousClass011)) {
            int length = iArr.length;
            int i2 = 0;
            if (length > 0) {
                int[] iArr2 = new int[length];
                if (i == 0) {
                    iArr2 = Arrays.copyOf(iArr, length);
                } else {
                    DisplayMetrics displayMetrics = r4.A08.getResources().getDisplayMetrics();
                    do {
                        iArr2[i2] = Math.round(TypedValue.applyDimension(i, (float) iArr[i2], displayMetrics));
                        i2++;
                    } while (i2 < length);
                }
                r4.A07 = AnonymousClass08C.A02(iArr2);
                if (!r4.A08()) {
                    StringBuilder sb = new StringBuilder("None of the preset sizes is valid: ");
                    sb.append(Arrays.toString(iArr));
                    throw new IllegalArgumentException(sb.toString());
                }
            } else {
                r4.A05 = false;
            }
            if (r4.A07()) {
                r4.A04();
            }
        }
    }
}
