package X;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.5yz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130215yz {
    public static boolean A00() {
        try {
            Boolean bool = (Boolean) Class.forName("com.facebook.endtoend.EndToEnd").getMethod("isRunningEndToEndTest", new Class[0]).invoke(null, new Object[0]);
            boolean exists = new File("/sdcard/e2e/media/fineYoungGentleman.jpg").exists();
            AnonymousClass0TU.A00(bool, "is e2e test: %s");
            AnonymousClass0TU.A00(Boolean.valueOf(exists), "static frame file exists: %s");
            if (bool == null) {
                return false;
            }
            if (!bool.booleanValue() || !exists) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException | IllegalAccessException | NoClassDefFoundError | NoSuchMethodError | NoSuchMethodException | InvocationTargetException e) {
            Log.i("OpticE2EConfig", "Failed to access test", e);
            return false;
        }
    }

    public static byte[] A01() {
        try {
            File file = new File("/sdcard/e2e/media/fineYoungGentleman.jpg");
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr = new byte[(int) file.length()];
            fileInputStream.read(bArr);
            return bArr;
        } catch (IOException e) {
            throw C117315Zl.A0J(e);
        }
    }
}
