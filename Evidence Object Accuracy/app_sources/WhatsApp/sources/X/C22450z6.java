package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.0z6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22450z6 {
    public final C15570nT A00;

    public C22450z6(C15570nT r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r3.A00 != 1) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.AbstractC15340mz r5) {
        /*
            boolean r0 = r5 instanceof X.C30461Xm     // Catch: NullPointerException -> 0x0037
            r4 = 0
            if (r0 == 0) goto L_0x0036
            r3 = r5
            X.1Xm r3 = (X.C30461Xm) r3     // Catch: NullPointerException -> 0x0037
            int r1 = r3.A00     // Catch: NullPointerException -> 0x0037
            r0 = 4
            r2 = 1
            if (r1 != r0) goto L_0x0023
            int r0 = r3.A00     // Catch: NullPointerException -> 0x0037
            if (r0 == r2) goto L_0x0035
        L_0x0012:
            boolean r0 = r3 instanceof X.AnonymousClass1Y3     // Catch: NullPointerException -> 0x0037
            if (r0 == 0) goto L_0x0036
            r0 = r3
            X.1Y3 r0 = (X.AnonymousClass1Y3) r0     // Catch: NullPointerException -> 0x0037
            int r1 = r0.A00     // Catch: NullPointerException -> 0x0037
            r0 = 2
            if (r1 != r0) goto L_0x0036
            int r0 = r3.A00     // Catch: NullPointerException -> 0x0037
            if (r0 == r2) goto L_0x0036
            goto L_0x0035
        L_0x0023:
            r0 = 12
            if (r1 != r0) goto L_0x002c
            int r0 = r3.A00     // Catch: NullPointerException -> 0x0037
            if (r0 == r2) goto L_0x0035
            goto L_0x0012
        L_0x002c:
            r0 = 90
            if (r1 != r0) goto L_0x0012
            int r0 = r3.A00     // Catch: NullPointerException -> 0x0037
            if (r0 == r2) goto L_0x0035
            goto L_0x0012
        L_0x0035:
            r4 = 1
        L_0x0036:
            return r4
        L_0x0037:
            r2 = move-exception
            java.lang.String r0 = "GroupMessageUtils/importantmsg/null "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = X.C30041Vv.A0C(r5)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0, r2)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22450z6.A00(X.0mz):boolean");
    }

    public final boolean A01(AbstractC15340mz r5, int i) {
        if (r5 instanceof C30461Xm) {
            C30461Xm r52 = (C30461Xm) r5;
            if (((AnonymousClass1XB) r52).A00 == i) {
                for (Jid jid : r52.A01) {
                    if (this.A00.A0F(jid)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
