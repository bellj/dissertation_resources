package X;

import android.os.IInterface;

/* renamed from: X.3rJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79853rJ extends AbstractBinderC73293fz implements IInterface {
    public final AbstractC116405Vh A00;

    public BinderC79853rJ(AbstractC116405Vh r2) {
        super("com.google.android.gms.maps.internal.ICancelableCallback");
        this.A00 = r2;
    }
}
