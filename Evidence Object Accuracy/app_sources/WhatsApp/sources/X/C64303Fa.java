package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.ConversationListRowHeaderView;
import java.util.List;

/* renamed from: X.3Fa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64303Fa {
    public ConversationListRowHeaderView A00;
    public C58442pT A01;
    public final C15610nY A02;

    public C64303Fa(Context context, ConversationListRowHeaderView conversationListRowHeaderView, C15610nY r5, AnonymousClass12F r6) {
        this.A00 = conversationListRowHeaderView;
        this.A02 = r5;
        this.A01 = new C58442pT(context, conversationListRowHeaderView.A00, r5, r6);
    }

    public void A00() {
        C27531Hw.A06(this.A01.A01);
    }

    public void A01() {
        this.A00.A01.setVisibility(8);
        C58442pT r1 = this.A01;
        r1.A08("");
        r1.A01.setPlaceholder(50);
    }

    public void A02(C15370n3 r5) {
        C58442pT r3 = this.A01;
        boolean A0M = r5.A0M();
        TextEmojiLabel textEmojiLabel = r3.A01;
        if (A0M) {
            textEmojiLabel.A0B(R.drawable.ic_verified);
        } else {
            textEmojiLabel.setCompoundDrawables(null, null, null, null);
        }
        C28801Pb.A00(textEmojiLabel.getContext(), r3, R.color.list_item_title);
    }

    public void A03(C15370n3 r8, AnonymousClass3J9 r9, List list) {
        TextEmojiLabel textEmojiLabel = this.A01.A01;
        textEmojiLabel.setPlaceholder(0);
        textEmojiLabel.A0D(r9, this.A02.A05(r8), list, 256, false);
    }
}
