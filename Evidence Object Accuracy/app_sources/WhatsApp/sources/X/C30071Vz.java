package X;

import javax.crypto.SecretKey;

/* renamed from: X.1Vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30071Vz {
    public final SecretKey A00;
    public final byte[] A01;
    public final byte[] A02;

    public C30071Vz(SecretKey secretKey, byte[] bArr, byte[] bArr2) {
        this.A00 = secretKey;
        this.A01 = bArr;
        this.A02 = bArr2;
    }
}
