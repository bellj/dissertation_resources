package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1210000_I0;
import java.util.Set;

/* renamed from: X.1xZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43771xZ {
    public final /* synthetic */ C43761xY A00;

    public C43771xZ(C43761xY r1) {
        this.A00 = r1;
    }

    public void A00(String str, Set set, boolean z) {
        boolean z2;
        C43761xY r2 = this.A00;
        AnonymousClass1P2 r0 = r2.A06;
        boolean z3 = r0.A07;
        boolean contains = set.contains(r0.A02);
        if (!z3 ? contains : !contains) {
            z2 = false;
        } else {
            z2 = true;
        }
        r2.A00 = z2;
        r2.A03.A0I(new RunnableBRunnable0Shape0S1210000_I0(r2, set, str, 0, z));
        AnonymousClass1P4 r02 = r2.A0B;
        if (r02 != null) {
            r2.A0C.A0H(r02.A01, 200);
        }
    }
}
