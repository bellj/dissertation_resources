package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2Uc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51362Uc extends ThumbnailButton {
    public boolean A00;

    public AbstractC51362Uc(Context context) {
        super(context);
        A00();
    }

    public AbstractC51362Uc(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC51362Uc(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }
}
