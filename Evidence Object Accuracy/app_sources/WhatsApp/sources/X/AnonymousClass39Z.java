package X;

import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.39Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39Z extends AbstractC120015fT {
    public final Map A00;

    @Override // X.AnonymousClass19V
    public String A01() {
        return "";
    }

    @Override // X.AbstractC120015fT
    public String A03() {
        return "bloks_version";
    }

    public AnonymousClass39Z(C18790t3 r16, C14820m6 r17, C14850m9 r18, AnonymousClass18L r19, AnonymousClass01H r20, String str, String str2, String str3, Map map, AnonymousClass01N r25, AnonymousClass01N r26) {
        super(r16, r17, r18, r19, r20, str, str2, str3, map, r25, r26, 5068499733235945L);
        this.A00 = C65053Hy.A01(str3);
    }

    @Override // X.AbstractC120015fT, X.AnonymousClass19V
    public void A02(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("flow_version_id", this.A00.get("flow_version_id"));
        jSONObject2.put("bloks_version", "8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666");
        jSONObject.put("variables", jSONObject2.toString());
    }
}
