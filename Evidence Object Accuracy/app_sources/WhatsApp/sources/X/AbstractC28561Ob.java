package X;

import android.content.Context;
import android.view.ViewGroup;

/* renamed from: X.1Ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC28561Ob extends ViewGroup implements AnonymousClass004 {
    public AnonymousClass2P7 A00;

    public abstract void A0Z();

    public AbstractC28561Ob(Context context) {
        super(context);
        A0Z();
    }

    public static String A0W(Object obj) {
        return AbstractC42671vd.A0Z(obj.toString());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
