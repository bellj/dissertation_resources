package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.0yE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21940yE {
    public final C18640sm A00;
    public final WhatsAppLibLoader A01;
    public final ExecutorC27271Gr A02;

    public C21940yE(C18640sm r3, WhatsAppLibLoader whatsAppLibLoader, AbstractC14440lR r5) {
        this.A02 = new ExecutorC27271Gr(r5, false);
        this.A01 = whatsAppLibLoader;
        this.A00 = r3;
    }

    public void A00() {
        if (this.A01.A03()) {
            this.A02.execute(new RunnableBRunnable0Shape7S0100000_I0_7(this, 18));
        }
    }
}
