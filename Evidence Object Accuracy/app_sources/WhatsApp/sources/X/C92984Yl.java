package X;

import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.4Yl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92984Yl {
    public static void A00(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | 16);
        textView.setTextColor(textView.getResources().getColor(R.color.payment_unsuccessful_transaction_amount_color));
    }

    public static void A01(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & -17);
    }
}
