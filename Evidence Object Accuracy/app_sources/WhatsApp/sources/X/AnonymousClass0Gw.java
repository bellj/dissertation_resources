package X;

import java.util.List;

/* renamed from: X.0Gw  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gw extends AnonymousClass0H3 {
    public final AnonymousClass0N8 A00;

    public AnonymousClass0Gw(List list) {
        super(list);
        int i = 0;
        AnonymousClass0N8 r1 = (AnonymousClass0N8) ((AnonymousClass0U8) list.get(0)).A0F;
        i = r1 != null ? r1.A01.length : i;
        this.A00 = new AnonymousClass0N8(new float[i], new int[i]);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r11, float f) {
        AnonymousClass0N8 r8 = this.A00;
        AnonymousClass0N8 r7 = (AnonymousClass0N8) r11.A0F;
        AnonymousClass0N8 r9 = (AnonymousClass0N8) r11.A09;
        int[] iArr = r7.A01;
        int length = iArr.length;
        int[] iArr2 = r9.A01;
        int length2 = iArr2.length;
        if (length == length2) {
            for (int i = 0; i < length; i++) {
                float[] fArr = r8.A00;
                float f2 = r7.A00[i];
                fArr[i] = f2 + ((r9.A00[i] - f2) * f);
                r8.A01[i] = AnonymousClass0TF.A02(f, iArr[i], iArr2[i]);
            }
            return r8;
        }
        StringBuilder sb = new StringBuilder("Cannot interpolate between gradients. Lengths vary (");
        sb.append(length);
        sb.append(" vs ");
        sb.append(length2);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
}
