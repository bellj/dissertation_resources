package X;

import java.util.concurrent.ThreadFactory;

/* renamed from: X.0ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ThreadFactoryC10650ey implements ThreadFactory {
    public int A00 = 10;
    public String A01 = "fonts-androidx";

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        return new C10790fD(runnable, this.A01, this.A00);
    }
}
