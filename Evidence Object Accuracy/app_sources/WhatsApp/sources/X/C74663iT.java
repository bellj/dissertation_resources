package X;

import java.util.List;

/* renamed from: X.3iT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74663iT extends AbstractC05290Oz {
    public final /* synthetic */ List A00;

    public C74663iT(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        Number number = (Number) AnonymousClass01Y.A02(this.A00, i);
        if (number == null) {
            return 1;
        }
        return number.intValue();
    }
}
