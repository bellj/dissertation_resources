package X;

import android.media.MediaPlayer;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4i8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98094i8 implements MediaPlayer.OnErrorListener {
    public final /* synthetic */ VideoSurfaceView A00;

    public C98094i8(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    @Override // android.media.MediaPlayer.OnErrorListener
    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder A0k = C12960it.A0k("videoview/ Error: ");
        A0k.append(i);
        Log.w(C12960it.A0e(",", A0k, i2));
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A02 = -1;
        videoSurfaceView.A06 = -1;
        MediaPlayer.OnErrorListener onErrorListener = videoSurfaceView.A0A;
        if (onErrorListener != null) {
            onErrorListener.onError(videoSurfaceView.A0C, i, i2);
        }
        return true;
    }
}
