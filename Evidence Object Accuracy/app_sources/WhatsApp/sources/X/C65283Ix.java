package X;

import android.net.Uri;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

/* renamed from: X.3Ix  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65283Ix {
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b7, code lost:
        if (r1.contains("_") != false) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00cc, code lost:
        if (r1.contains("_") == false) goto L_0x00ce;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.net.Uri A00(java.lang.String r12) {
        /*
            android.net.Uri r2 = android.net.Uri.parse(r12)
            java.lang.String r0 = r2.getScheme()
            if (r0 != 0) goto L_0x000e
            r0 = 1
        L_0x000b:
            if (r0 != 0) goto L_0x0058
            goto L_0x0019
        L_0x000e:
            java.lang.String r1 = r2.getScheme()
            java.lang.String r0 = "([a-zA-Z][a-zA-Z0-9+.-]*)?"
            boolean r0 = r1.matches(r0)
            goto L_0x000b
        L_0x0019:
            java.net.URI r2 = new java.net.URI     // Catch: URISyntaxException -> 0x0052
            r2.<init>(r12)     // Catch: URISyntaxException -> 0x0052
            boolean r0 = r2.isOpaque()
            if (r0 == 0) goto L_0x0049
            android.net.Uri$Builder r1 = new android.net.Uri$Builder
            r1.<init>()
            java.lang.String r0 = r2.getScheme()
            android.net.Uri$Builder r1 = r1.scheme(r0)
            java.lang.String r0 = r2.getRawSchemeSpecificPart()
            android.net.Uri$Builder r1 = r1.encodedOpaquePart(r0)
            java.lang.String r0 = r2.getRawFragment()
            android.net.Uri$Builder r0 = r1.encodedFragment(r0)
            android.net.Uri r1 = r0.build()
            A02(r12, r2, r1)
            return r1
        L_0x0049:
            android.net.Uri r1 = X.C12960it.A0C(r2)
            r0 = 0
            A03(r12, r2, r1, r0)
            return r1
        L_0x0052:
            r0 = move-exception
            java.lang.SecurityException r0 = A01(r12, r0)
            throw r0
        L_0x0058:
            boolean r0 = r2.isOpaque()
            if (r0 == 0) goto L_0x0079
            java.lang.String r4 = r2.getScheme()     // Catch: URISyntaxException -> 0x0073
            java.lang.String r3 = r2.getSchemeSpecificPart()     // Catch: URISyntaxException -> 0x0073
            java.lang.String r1 = r2.getFragment()     // Catch: URISyntaxException -> 0x0073
            java.net.URI r0 = new java.net.URI     // Catch: URISyntaxException -> 0x0073
            r0.<init>(r4, r3, r1)     // Catch: URISyntaxException -> 0x0073
            A02(r12, r0, r2)
            return r2
        L_0x0073:
            r0 = move-exception
            java.lang.SecurityException r0 = A01(r12, r0)
            throw r0
        L_0x0079:
            java.lang.String r5 = r2.getScheme()     // Catch: URISyntaxException -> 0x009b
            java.lang.String r6 = r2.getUserInfo()     // Catch: URISyntaxException -> 0x009b
            java.lang.String r7 = r2.getHost()     // Catch: URISyntaxException -> 0x009b
            int r8 = r2.getPort()     // Catch: URISyntaxException -> 0x009b
            java.lang.String r9 = r2.getPath()     // Catch: URISyntaxException -> 0x009b
            java.lang.String r10 = r2.getQuery()     // Catch: URISyntaxException -> 0x009b
            java.lang.String r11 = r2.getFragment()     // Catch: URISyntaxException -> 0x009b
            java.net.URI r4 = new java.net.URI     // Catch: URISyntaxException -> 0x009b
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)     // Catch: URISyntaxException -> 0x009b
            goto L_0x00b9
        L_0x009b:
            r3 = move-exception
            java.lang.String r0 = r2.toString()     // Catch: URISyntaxException -> 0x00d3
            java.net.URI r4 = new java.net.URI     // Catch: URISyntaxException -> 0x00d3
            r4.<init>(r0)     // Catch: URISyntaxException -> 0x00d3
            java.lang.String r1 = r2.getHost()     // Catch: URISyntaxException -> 0x00d3
            java.lang.String r0 = r4.getHost()     // Catch: URISyntaxException -> 0x00d3
            if (r0 != 0) goto L_0x00d3
            if (r1 == 0) goto L_0x00d3
            java.lang.String r0 = "_"
            boolean r0 = r1.contains(r0)     // Catch: URISyntaxException -> 0x00d3
            if (r0 == 0) goto L_0x00d3
        L_0x00b9:
            java.lang.String r1 = r2.getHost()
            java.lang.String r0 = r4.getHost()
            if (r0 != 0) goto L_0x00ce
            if (r1 == 0) goto L_0x00ce
            java.lang.String r0 = "_"
            boolean r1 = r1.contains(r0)
            r0 = 1
            if (r1 != 0) goto L_0x00cf
        L_0x00ce:
            r0 = 0
        L_0x00cf:
            A03(r12, r4, r2, r0)
            return r2
        L_0x00d3:
            java.lang.SecurityException r0 = A01(r12, r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65283Ix.A00(java.lang.String):android.net.Uri");
    }

    public static SecurityException A01(String str, URISyntaxException uRISyntaxException) {
        Locale locale = Locale.US;
        Object[] A1a = C12980iv.A1a();
        A1a[0] = str;
        A1a[1] = uRISyntaxException.getMessage();
        return new SecurityException(String.format(locale, "Parsing url %s caused exception: %s.", A1a));
    }

    public static void A02(String str, URI uri, Uri uri2) {
        boolean A04 = A04(uri.getScheme(), uri2.getScheme());
        boolean A042 = A04(uri.getSchemeSpecificPart(), uri2.getSchemeSpecificPart());
        if (!A04 || !A042) {
            String str2 = "";
            if (!A04) {
                str2 = C12960it.A0d(C12970iu.A0o(uri2, uri), C12960it.A0j(str2));
            }
            if (!A042) {
                str2 = C12960it.A0d(String.format(Locale.US, "javaUri opaque part: \"%s\". androidUri opaque part: \"%s\".", uri.getSchemeSpecificPart(), uri2.getSchemeSpecificPart()), C12960it.A0j(str2));
            }
            throw C12970iu.A0m(str2, str, Locale.US, C12980iv.A1b(uri, uri2));
        }
    }

    public static void A03(String str, URI uri, Uri uri2, boolean z) {
        boolean A04 = A04(uri.getScheme(), uri2.getScheme());
        boolean A042 = A04(uri.getAuthority(), uri2.getAuthority());
        boolean A043 = A04(uri.getPath(), uri2.getPath());
        String str2 = "";
        if (!A04) {
            str2 = C12960it.A0d(C12970iu.A0o(uri2, uri), C12960it.A0j(str2));
        }
        if (!z && !A042) {
            str2 = C12960it.A0d(String.format(Locale.US, "javaUri authority: \"%s\". androidUri authority: \"%s\".", uri.getAuthority(), uri2.getAuthority()), C12960it.A0j(str2));
        }
        if (!A043) {
            str2 = C12960it.A0d(String.format(Locale.US, "javaUri path: \"%s\". androidUri path: \"%s\".", uri.getPath(), uri2.getPath()), C12960it.A0j(str2));
        }
        if (!A04 || !A042 || !A043) {
            throw C12970iu.A0m(str2, str, Locale.US, C12980iv.A1b(uri, uri2));
        }
    }

    public static boolean A04(String str, String str2) {
        if (str == null || str.equals("")) {
            return str2 == null || str2.equals("");
        }
        return str.equals(str2);
    }
}
