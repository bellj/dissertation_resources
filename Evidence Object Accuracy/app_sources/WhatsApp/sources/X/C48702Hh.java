package X;

/* renamed from: X.2Hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48702Hh implements AnonymousClass005 {
    public final AnonymousClass02A A00;
    public final Object A01 = new Object();
    public volatile AbstractC48712Hi A02;

    public C48702Hh(ActivityC001000l r3) {
        this.A00 = new AnonymousClass02A(new C48742Hp(r3, this), r3);
    }

    @Override // X.AnonymousClass005
    public /* bridge */ /* synthetic */ Object generatedComponent() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    this.A02 = ((C48752Hq) this.A00.A00(C48752Hq.class)).A00;
                }
            }
        }
        return this.A02;
    }
}
