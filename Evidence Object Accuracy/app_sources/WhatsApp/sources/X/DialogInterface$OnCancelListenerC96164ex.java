package X;

import android.content.DialogInterface;
import com.whatsapp.backup.google.SettingsGoogleDrive;

/* renamed from: X.4ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class DialogInterface$OnCancelListenerC96164ex implements DialogInterface.OnCancelListener {
    public final /* synthetic */ SettingsGoogleDrive.AuthRequestDialogFragment A00;

    public /* synthetic */ DialogInterface$OnCancelListenerC96164ex(SettingsGoogleDrive.AuthRequestDialogFragment authRequestDialogFragment) {
        this.A00 = authRequestDialogFragment;
    }

    @Override // android.content.DialogInterface.OnCancelListener
    public final void onCancel(DialogInterface dialogInterface) {
        ((SettingsGoogleDrive) C13010iy.A01(this.A00)).A0t = true;
    }
}
