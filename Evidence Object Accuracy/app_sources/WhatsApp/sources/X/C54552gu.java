package X;

import com.whatsapp.R;
import com.whatsapp.mentions.MentionPickerView;

/* renamed from: X.2gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54552gu extends AnonymousClass0QE {
    public final /* synthetic */ MentionPickerView A00;

    public C54552gu(MentionPickerView mentionPickerView) {
        this.A00 = mentionPickerView;
    }

    @Override // X.AnonymousClass0QE
    public void A00() {
        MentionPickerView mentionPickerView = this.A00;
        mentionPickerView.A05(mentionPickerView.A0C.A07.size(), mentionPickerView.getResources().getDimensionPixelSize(R.dimen.mention_picker_row_height));
    }
}
