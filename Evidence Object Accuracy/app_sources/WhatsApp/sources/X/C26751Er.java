package X;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26751Er {
    public final C15570nT A00;
    public final C002701f A01;
    public final C14850m9 A02;
    public final AnonymousClass14Q A03;
    public final AnonymousClass160 A04;
    public final C22190yg A05;
    public final AnonymousClass19O A06;

    public C26751Er(C15570nT r1, C002701f r2, C14850m9 r3, AnonymousClass14Q r4, AnonymousClass160 r5, C22190yg r6, AnonymousClass19O r7) {
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = r6;
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A01 = r2;
    }

    public static final boolean A00(C38421o4 r4, AbstractC458623l r5, Object obj) {
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        for (AbstractC16130oV r1 : Collections.unmodifiableList(r4.A01)) {
            synchronized (r1) {
                C16150oX r0 = r1.A02;
                AnonymousClass009.A05(r0);
                if (r5.A67(r0, r1, obj)) {
                    atomicBoolean.set(true);
                }
            }
        }
        return atomicBoolean.get();
    }

    public static boolean A01(AbstractC16130oV r4, boolean z) {
        synchronized (r4) {
            C16150oX r3 = r4.A02;
            AnonymousClass009.A05(r3);
            if (((AbstractC15340mz) r4).A0C != 1) {
                return false;
            }
            r4.A0S();
            r3.A0a = false;
            r3.A0P = false;
            r3.A0L = z;
            r3.A0C = 0;
            return true;
        }
    }
}
