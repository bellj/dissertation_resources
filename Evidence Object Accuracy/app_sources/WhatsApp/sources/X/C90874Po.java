package X;

import com.whatsapp.biz.product.view.fragment.ProductReportReasonDialogFragment;

/* renamed from: X.4Po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90874Po {
    public final int A00;
    public final String A01;
    public final /* synthetic */ ProductReportReasonDialogFragment A02;

    public /* synthetic */ C90874Po(ProductReportReasonDialogFragment productReportReasonDialogFragment, String str, int i) {
        this.A02 = productReportReasonDialogFragment;
        this.A01 = str;
        this.A00 = i;
    }
}
