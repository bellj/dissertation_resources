package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2Lr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49662Lr implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100424lt();
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C49662Lr(int i, int i2, int i3, int i4) {
        this.A01 = i;
        this.A03 = i2;
        this.A02 = i3;
        this.A00 = i4;
    }

    public C49662Lr(Parcel parcel) {
        this.A01 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A02 = parcel.readInt();
        this.A00 = parcel.readInt();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C49662Lr r5 = (C49662Lr) obj;
            if (!(this.A01 == r5.A01 && this.A03 == r5.A03 && this.A02 == r5.A02 && this.A00 == r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((((527 + this.A01) * 31) + this.A03) * 31) + this.A02) * 31) + this.A00;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A00);
    }
}
