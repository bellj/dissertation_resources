package X;

import java.util.Collection;
import java.util.Map;

/* renamed from: X.5XH  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XH {
    Map asMap();

    void clear();

    boolean isEmpty();

    boolean put(Object obj, Object obj2);

    int size();

    Collection values();
}
