package X;

import android.media.CamcorderProfile;
import java.io.FileDescriptor;

/* renamed from: X.6Mm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC136426Mm {
    AnonymousClass60Q AeL(CamcorderProfile camcorderProfile, AnonymousClass6LR v, FileDescriptor fileDescriptor, int i, int i2, boolean z, boolean z2);

    AnonymousClass60Q AeM(CamcorderProfile camcorderProfile, AnonymousClass6LR v, String str, int i, int i2, boolean z, boolean z2);

    void AeU();
}
