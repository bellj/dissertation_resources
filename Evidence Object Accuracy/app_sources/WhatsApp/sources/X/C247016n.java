package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.16n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C247016n {
    public final AbstractC15710nm A00;
    public final C246916m A01;
    public final C14820m6 A02;
    public final C14850m9 A03;
    public final C17220qS A04;

    public C247016n(AbstractC15710nm r1, C246916m r2, C14820m6 r3, C14850m9 r4, C17220qS r5) {
        this.A04 = r5;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 10, insn: 0x00dc: INVOKE  (r10 I:X.1WA) type: VIRTUAL call: X.1WA.A00():void, block:B:13:0x00c7
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final void A00(
/*
[224] Method generation error in method: X.16n.A00(com.whatsapp.jid.UserJid, X.1WA, java.lang.String, java.lang.String, java.lang.String):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r16v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0039, code lost:
        if (r4 > r3.getLong(r0.toString(), 0)) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.whatsapp.jid.UserJid r18, X.AnonymousClass1WA r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, boolean r23) {
        /*
            r17 = this;
            r6 = r17
            r12 = r18
            boolean r0 = r6.A02(r12)
            r8 = r20
            r7 = r19
            r10 = r22
            r9 = r21
            if (r0 != 0) goto L_0x0054
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r4 = r0.getTime()
            X.0m6 r0 = r6.A02
            java.lang.String r2 = r12.getRawString()
            android.content.SharedPreferences r3 = r0.A00
            java.lang.String r1 = "galaxy_business_cert_expired_timestamp_"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r2 = r0.toString()
            r0 = 0
            long r1 = r3.getLong(r2, r0)
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0054
        L_0x003b:
            X.0m6 r1 = r6.A02
            java.lang.String r0 = r12.getRawString()
            r1.A0a(r0)
            X.0qS r1 = r6.A04
            X.1W4 r0 = new X.1W4
            r0.<init>(r12, r1)
            X.1WB r5 = new X.1WB
            r5.<init>(r6, r7, r8, r9, r10)
            r0.A00(r5)
        L_0x0053:
            return
        L_0x0054:
            if (r23 != 0) goto L_0x003b
            X.0m6 r0 = r6.A02
            java.lang.String r3 = r12.user
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "galaxy_biz_public_key_"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r1 = r0.toString()
            r0 = 0
            java.lang.String r0 = r2.getString(r1, r0)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x003b
            if (r20 == 0) goto L_0x0053
            if (r19 == 0) goto L_0x0053
            if (r21 == 0) goto L_0x0053
            if (r22 == 0) goto L_0x0053
            r11 = r6
            r13 = r7
            r14 = r8
            r15 = r9
            r16 = r10
            r11.A00(r12, r13, r14, r15, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C247016n.A01(com.whatsapp.jid.UserJid, X.1WA, java.lang.String, java.lang.String, java.lang.String, boolean):void");
    }

    public boolean A02(UserJid userJid) {
        try {
            JSONArray jSONArray = new JSONObject(this.A03.A03(1695)).getJSONArray("galaxy_allowed_list");
            for (int i = 0; i < jSONArray.length(); i++) {
                if (jSONArray.getString(i).equalsIgnoreCase(userJid.user)) {
                    return true;
                }
            }
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("GalaxyConnectionManager/allowedSelfSignedCert/");
            sb.append(e.getMessage());
            Log.w(sb.toString());
        }
        return false;
    }
}
