package X;

/* renamed from: X.4Mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90024Mh {
    public final C92344Vn A00;
    public final Object[] A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r4.length > 0) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C90024Mh(X.C92344Vn r3, java.lang.Object[] r4) {
        /*
            r2 = this;
            r2.<init>()
            r2.A00 = r3
            r2.A01 = r4
            if (r4 == 0) goto L_0x000d
            int r0 = r4.length
            r1 = 0
            if (r0 <= 0) goto L_0x000e
        L_0x000d:
            r1 = 1
        L_0x000e:
            java.lang.String r0 = "boundArgs must not be empty; use null"
            X.C87914Dn.A00(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C90024Mh.<init>(X.4Vn, java.lang.Object[]):void");
    }
}
