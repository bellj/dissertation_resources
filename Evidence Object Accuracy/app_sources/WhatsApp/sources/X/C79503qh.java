package X;

/* renamed from: X.3qh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C79503qh extends AbstractC94974cq {
    public AnonymousClass5BY A00;

    public static int A00(int i, int i2, int i3) {
        return i3 + i2 + C95484do.A01(i) + i;
    }

    @Override // X.AbstractC94974cq
    public int A03() {
        if (this.A00 != null) {
            int i = 0;
            while (true) {
                AnonymousClass5BY r1 = this.A00;
                if (i >= r1.A00) {
                    break;
                }
                r1.A02[i].A00();
                i++;
            }
        }
        return 0;
    }

    @Override // X.AbstractC94974cq
    public void A05(C95484do r4) {
        if (this.A00 != null) {
            int i = 0;
            while (true) {
                AnonymousClass5BY r1 = this.A00;
                if (i < r1.A00) {
                    r1.A02[i].A01();
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public C79503qh A06() {
        C79503qh r1 = (C79503qh) super.A04();
        AnonymousClass5BY r0 = this.A00;
        if (r0 != null) {
            r1.A00 = (AnonymousClass5BY) r0.clone();
        }
        return r1;
    }
}
