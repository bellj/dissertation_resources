package X;

import android.database.Cursor;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: X.0yZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22130yZ {
    public WeakHashMap A00 = new WeakHashMap();
    public final C15570nT A01;
    public final AnonymousClass13T A02;
    public final AnonymousClass10B A03;
    public final C18990tO A04;
    public final C20720wD A05;
    public final C14830m7 A06;
    public final C15990oG A07;
    public final C22830zi A08;
    public final C22100yW A09;
    public final C234911w A0A;
    public final C18770sz A0B;
    public final C14850m9 A0C;
    public final C14840m8 A0D;
    public final C20660w7 A0E;

    public C22130yZ(C15570nT r2, AnonymousClass13T r3, AnonymousClass10B r4, C18990tO r5, C20720wD r6, C14830m7 r7, C15990oG r8, C22830zi r9, C22100yW r10, C234911w r11, C18770sz r12, C14850m9 r13, C14840m8 r14, C20660w7 r15) {
        this.A06 = r7;
        this.A0C = r13;
        this.A01 = r2;
        this.A0E = r15;
        this.A05 = r6;
        this.A0D = r14;
        this.A07 = r8;
        this.A0B = r12;
        this.A02 = r3;
        this.A0A = r11;
        this.A08 = r9;
        this.A09 = r10;
        this.A04 = r5;
        this.A03 = r4;
    }

    public static AnonymousClass1PA A00(byte[] bArr, byte b) {
        try {
            return C15940oB.A01(C16050oM.A05(new byte[]{b}, bArr));
        } catch (AnonymousClass1RY e) {
            Log.e("DeviceADVInfoManager/generatePublicIdentityKey invalidKeyException", e);
            return null;
        }
    }

    public static boolean A01(AnonymousClass1PA r2, byte[] bArr, byte[] bArr2) {
        if (r2.A00 == 5) {
            return C32001bS.A00().A02(r2.A01, bArr, bArr2);
        }
        throw new AssertionError("PublicKey type is invalid");
    }

    public static byte[] A02(MessageDigest messageDigest, List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((AnonymousClass1JS) it.next()).A00.A01);
        }
        Collections.sort(arrayList, new C31301aK());
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            messageDigest.update((byte[]) it2.next());
        }
        return messageDigest.digest();
    }

    public final AbstractC27881Jp A03(C28601Of r10, UserJid userJid, Map map, Set set) {
        HashSet hashSet = new HashSet();
        boolean z = true;
        if (r10.A00.size() > 1) {
            boolean A0F = this.A01.A0F(userJid);
            ArrayList arrayList = new ArrayList();
            Iterator it = r10.A01().iterator();
            while (true) {
                if (it.hasNext()) {
                    DeviceJid deviceJid = (DeviceJid) ((Map.Entry) it.next()).getKey();
                    Object obj = map.get(C15940oB.A02(deviceJid));
                    if (obj == null) {
                        StringBuilder sb = new StringBuilder("DeviceADVInfoManager/getKeyHash cannot find identity key for device=");
                        sb.append(deviceJid);
                        sb.append("; isMe=");
                        sb.append(A0F);
                        Log.w(sb.toString());
                        if (A0F) {
                            break;
                        }
                        hashSet.add(deviceJid);
                    } else {
                        arrayList.add(obj);
                    }
                } else {
                    z = false;
                    if (!arrayList.isEmpty()) {
                        if (!hashSet.isEmpty() && !A0F) {
                            Iterator it2 = r10.A01().iterator();
                            while (it2.hasNext()) {
                                Map.Entry entry = (Map.Entry) it2.next();
                                if (!hashSet.contains(entry.getKey())) {
                                    set.add(Integer.valueOf(((Number) entry.getValue()).intValue()));
                                }
                            }
                        }
                        try {
                            byte[] A02 = A02(MessageDigest.getInstance("SHA-256"), arrayList);
                            int A022 = this.A0C.A02(310);
                            byte[] bArr = new byte[A022];
                            System.arraycopy(A02, 0, bArr, 0, A022);
                            return AbstractC27881Jp.A01(bArr, 0, A022);
                        } catch (NoSuchAlgorithmException e) {
                            Log.e("DeviceADVInfoManager/getKeyHash no such algorithm exception", e);
                            this.A0A.A04(false);
                            return null;
                        }
                    }
                }
            }
            this.A0A.A04(z);
        }
        return null;
    }

    public final AbstractC27881Jp A04(UserJid userJid, Set set) {
        Map A0A;
        Set<DeviceJid> keySet;
        AnonymousClass1JS A0E;
        C15570nT r3 = this.A01;
        boolean A0F = r3.A0F(userJid);
        if (A0F) {
            A0A = new HashMap();
            keySet = this.A0B.A0D(userJid);
        } else {
            A0A = this.A0B.A0A(userJid);
            keySet = A0A.keySet();
        }
        HashSet hashSet = new HashSet();
        if (keySet.size() > 1) {
            ArrayList arrayList = new ArrayList();
            for (DeviceJid deviceJid : keySet) {
                if (deviceJid.device != 0 || !r3.A0F(deviceJid.getUserJid())) {
                    A0E = this.A07.A0E(C15940oB.A02(deviceJid));
                } else {
                    A0E = this.A07.A00.A04().A01;
                }
                if (A0E == null) {
                    StringBuilder sb = new StringBuilder("DeviceADVInfoManager/getKeyHash cannot find identity key for device=");
                    sb.append(deviceJid);
                    sb.append("; isMe=");
                    sb.append(A0F);
                    Log.w(sb.toString());
                    if (A0F) {
                        this.A0A.A04(true);
                    } else {
                        hashSet.add(deviceJid);
                    }
                } else {
                    arrayList.add(A0E);
                }
            }
            if (arrayList.isEmpty()) {
                this.A0A.A04(false);
                return null;
            }
            if (!hashSet.isEmpty()) {
                for (Map.Entry entry : A0A.entrySet()) {
                    if (!hashSet.contains(entry.getKey())) {
                        set.add(Integer.valueOf(((Number) entry.getValue()).intValue()));
                    }
                }
            }
            try {
                byte[] A02 = A02(MessageDigest.getInstance("SHA-256"), arrayList);
                int A022 = this.A0C.A02(310);
                byte[] bArr = new byte[A022];
                System.arraycopy(A02, 0, bArr, 0, A022);
                return AbstractC27881Jp.A01(bArr, 0, A022);
            } catch (NoSuchAlgorithmException e) {
                Log.e("DeviceADVInfoManager/getKeyHash no such algorithm exception", e);
                this.A0A.A04(false);
                return null;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005e, code lost:
        if ((r16.A06.A00() - (1000 * r14)) >= 2592000000L) goto L_0x007a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C41961uR A05(com.whatsapp.jid.UserJid r17) {
        /*
            r16 = this;
            r3 = r16
            X.0nT r7 = r3.A01
            r7.A08()
            X.1Ih r1 = r7.A05
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            X.1Jp r9 = r3.A04(r1, r0)
            r7.A08()
            X.1Ih r0 = r7.A05
            X.0sz r4 = r3.A0B
            long r12 = r4.A04(r0)
            if (r9 != 0) goto L_0x0034
            X.0m7 r0 = r3.A06
            long r5 = r0.A00()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r12
            long r5 = r5 - r0
            r1 = 2592000000(0x9a7ec800, double:1.280618154E-314)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0034
            r12 = 0
        L_0x0034:
            r1 = r17
            boolean r0 = r7.A0F(r1)
            java.util.HashSet r11 = new java.util.HashSet
            r11.<init>()
            r7 = 0
            if (r0 == 0) goto L_0x007d
            r10 = r7
        L_0x0043:
            r5 = 0
            if (r0 != 0) goto L_0x007a
            long r14 = r4.A04(r1)
            if (r10 != 0) goto L_0x0060
            X.0m7 r0 = r3.A06
            long r3 = r0.A00()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r14
            long r3 = r3 - r0
            r1 = 2592000000(0x9a7ec800, double:1.280618154E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x007a
        L_0x0060:
            if (r9 != 0) goto L_0x006c
            int r0 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x006c
            if (r10 != 0) goto L_0x006c
            int r0 = (r14 > r5 ? 1 : (r14 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0079
        L_0x006c:
            X.1uR r8 = new X.1uR
            boolean r0 = r11.isEmpty()
            if (r0 == 0) goto L_0x0075
            r11 = r7
        L_0x0075:
            r7 = r8
            r8.<init>(r9, r10, r11, r12, r14)
        L_0x0079:
            return r7
        L_0x007a:
            r14 = 0
            goto L_0x0060
        L_0x007d:
            X.1Jp r10 = r3.A04(r1, r11)
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22130yZ.A05(com.whatsapp.jid.UserJid):X.1uR");
    }

    public C41911uM A06(UserJid userJid, byte[] bArr) {
        C234911w r1;
        int i;
        AnonymousClass1JS A0E = this.A07.A0E(C15940oB.A02(userJid.getPrimaryDevice()));
        if (A0E == null) {
            StringBuilder sb = new StringBuilder("DeviceADVInfoHandler/checkPrimaryIdentity/no primary identity. userJid=");
            sb.append(userJid);
            Log.w(sb.toString());
            this.A0E.A0K(Collections.singletonList(userJid.getPrimaryDevice()));
            C234911w r2 = this.A0A;
            C42001uV r12 = new C42001uV();
            r12.A00 = 2;
            r2.A00.A07(r12);
            return null;
        }
        try {
            C41921uN r4 = (C41921uN) AbstractC27091Fz.A0E(C41921uN.A03, bArr);
            byte[] A04 = r4.A02.A04();
            if (!A01(A0E.A00, C16050oM.A05(AnonymousClass01V.A0D, A04), r4.A01.A04())) {
                Log.w("DeviceADVInfoManager/verifyADVSignedKeyIndexList/fail to verify account signature");
                this.A0E.A0K(Collections.singletonList(userJid.getPrimaryDevice()));
                this.A0A.A00.A07(new C42011uW());
                return null;
            }
            try {
                return (C41911uM) AbstractC27091Fz.A0E(C41911uM.A06, A04);
            } catch (C28971Pt e) {
                StringBuilder sb2 = new StringBuilder("DeviceADVInfoManager/verifyADVSignedKeyIndexList/ADVKeyIndexList invalidProto:");
                sb2.append(e.getMessage());
                Log.e(sb2.toString());
                r1 = this.A0A;
                i = 4;
                r1.A00(i);
                return null;
            }
        } catch (C28971Pt e2) {
            StringBuilder sb3 = new StringBuilder("DeviceADVInfoManager/verifyADVSignedKeyIndexList/ADVSignedKeyIndexList invalidProto:");
            sb3.append(e2.getMessage());
            Log.e(sb3.toString());
            r1 = this.A0A;
            i = 3;
        }
    }

    public Map A07(List list, Map map, Map map2, int i) {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map2.entrySet()) {
            DeviceJid deviceJid = (DeviceJid) entry.getKey();
            Number number = (Number) entry.getValue();
            if (deviceJid.device == 0 || list.contains(Integer.valueOf(number.intValue()))) {
                hashMap.put(deviceJid, number);
            } else {
                C234911w r0 = this.A0A;
                r0.A00.A07(new C41971uS());
            }
        }
        for (Map.Entry entry2 : map.entrySet()) {
            Object key = entry2.getKey();
            Number number2 = (Number) entry2.getValue();
            long longValue = number2.longValue();
            if (longValue <= ((long) i)) {
                if (map2.containsKey(key) && ((Number) map2.get(key)).longValue() < longValue && list.contains(Integer.valueOf(number2.intValue()))) {
                    StringBuilder sb = new StringBuilder("DevicesUtil/filterDeviceWithKeyIndexList/larger index exists in current map, jid=");
                    sb.append(key);
                    sb.append("; index=");
                    sb.append(number2);
                    Log.i(sb.toString());
                }
            }
            hashMap.put(key, number2);
        }
        return hashMap;
    }

    public Map A08(Set set) {
        long j;
        AbstractC27881Jp A03;
        long j2;
        long j3;
        HashMap hashMap = new HashMap();
        if (!set.isEmpty()) {
            C15570nT r1 = this.A01;
            r1.A08();
            C27631Ih r8 = r1.A05;
            AnonymousClass009.A05(r8);
            HashSet hashSet = new HashSet(set);
            hashSet.add(r8);
            C18770sz r12 = this.A0B;
            HashMap hashMap2 = new HashMap(hashSet.size());
            HashSet hashSet2 = new HashSet();
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                Jid jid = (Jid) it.next();
                if (r12.A01.A0F(jid)) {
                    hashMap2.put(jid, r12.A07());
                } else if (jid != null) {
                    hashSet2.add(jid);
                }
            }
            C245916c r11 = r12.A05;
            Iterator it2 = hashSet2.iterator();
            while (it2.hasNext()) {
                AnonymousClass009.A0C("only query info for others", !r11.A01.A0F((Jid) it2.next()));
            }
            C246116e r4 = r11.A03;
            HashSet hashSet3 = new HashSet(hashSet2);
            HashMap hashMap3 = new HashMap();
            Object obj = r4.A03;
            synchronized (obj) {
                Iterator it3 = hashSet2.iterator();
                while (it3.hasNext()) {
                    UserJid userJid = (UserJid) it3.next();
                    Map map = r4.A04;
                    if (map.containsKey(userJid)) {
                        hashMap3.put(userJid, (AnonymousClass1YF) map.get(userJid));
                        hashSet3.remove(userJid);
                    }
                }
            }
            HashMap hashMap4 = new HashMap();
            if (!hashSet3.isEmpty()) {
                String[] strArr = new String[hashSet3.size()];
                int i = 0;
                Iterator it4 = hashSet3.iterator();
                while (it4.hasNext()) {
                    strArr[i] = String.valueOf(r4.A00.A01((Jid) it4.next()));
                    i++;
                }
                C29841Uw r6 = new C29841Uw(strArr, 975);
                C16310on A01 = r4.A01.get();
                try {
                    Iterator it5 = r6.iterator();
                    while (it5.hasNext()) {
                        String[] strArr2 = (String[]) it5.next();
                        C16330op r14 = A01.A03;
                        int length = strArr2.length;
                        StringBuilder sb = new StringBuilder("SELECT raw_id, timestamp, expected_timestamp, expected_ts_last_device_job_ts, expected_timestamp_update_ts, user_jid_row_id FROM user_device_info WHERE user_jid_row_id IN ");
                        sb.append(AnonymousClass1Ux.A00(length));
                        Cursor A09 = r14.A09(sb.toString(), strArr2);
                        int columnIndexOrThrow = A09.getColumnIndexOrThrow("raw_id");
                        int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("timestamp");
                        int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("expected_timestamp");
                        int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("expected_ts_last_device_job_ts");
                        int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("expected_timestamp_update_ts");
                        while (A09.moveToNext()) {
                            hashMap4.put((UserJid) r4.A00.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("user_jid_row_id"))), new AnonymousClass1YF(A09.getInt(columnIndexOrThrow), A09.getLong(columnIndexOrThrow2), A09.getLong(columnIndexOrThrow3), A09.getLong(columnIndexOrThrow4), A09.getLong(columnIndexOrThrow5)));
                        }
                        A09.close();
                    }
                    A01.close();
                    synchronized (obj) {
                        r4.A04.putAll(hashMap4);
                    }
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            hashMap3.putAll(hashMap4);
            Iterator it6 = hashSet2.iterator();
            while (it6.hasNext()) {
                Object next = it6.next();
                if (!hashMap3.containsKey(next)) {
                    hashMap3.put(next, null);
                }
            }
            hashMap2.putAll(hashMap3);
            HashMap hashMap5 = new HashMap();
            C15570nT r13 = r12.A01;
            r13.A08();
            C27631Ih r42 = r13.A05;
            if (hashSet.contains(r42)) {
                HashSet hashSet4 = new HashSet(hashSet);
                hashSet4.remove(r42);
                hashMap5.put(r42, r12.A05());
                hashSet = hashSet4;
            }
            hashMap5.putAll(r11.A05.A00(hashSet));
            for (Map.Entry entry : hashMap5.entrySet()) {
                UserJid userJid2 = (UserJid) entry.getKey();
                DeviceJid primaryDevice = userJid2.getPrimaryDevice();
                Map map2 = ((C28601Of) entry.getValue()).A00;
                if (!map2.containsKey(primaryDevice)) {
                    AnonymousClass1YB r5 = new AnonymousClass1YB();
                    HashMap hashMap6 = new HashMap(map2);
                    Map map3 = r5.A00;
                    AnonymousClass009.A05(map3);
                    map3.putAll(hashMap6);
                    r5.A01(userJid2.getPrimaryDevice(), 0L);
                    hashMap5.put(userJid2, r5.A00());
                }
            }
            HashMap hashMap7 = new HashMap(hashMap5.size());
            HashSet hashSet5 = new HashSet();
            for (Map.Entry entry2 : hashMap5.entrySet()) {
                int size = ((C28601Of) entry2.getValue()).A00.size();
                Iterator it7 = ((C28601Of) entry2.getValue()).A02().iterator();
                while (it7.hasNext()) {
                    DeviceJid deviceJid = (DeviceJid) it7.next();
                    boolean z = false;
                    if (deviceJid.device == 0) {
                        z = true;
                    }
                    if (z && r1.A0F(deviceJid.getUserJid())) {
                        hashMap7.put(C15940oB.A02(deviceJid), this.A07.A00.A04().A01);
                    } else if (size > 1) {
                        hashSet5.add(C15940oB.A02(deviceJid));
                    }
                }
            }
            if (!hashSet5.isEmpty()) {
                hashMap7.putAll(this.A07.A0P(hashSet5));
            }
            Object obj2 = hashMap5.get(r8);
            AnonymousClass009.A05(obj2);
            AbstractC27881Jp A032 = A03((C28601Of) obj2, r8, hashMap7, new HashSet());
            AnonymousClass1YF r15 = (AnonymousClass1YF) hashMap2.get(r8);
            long j4 = 0;
            if (r15 == null) {
                j = 0;
            } else {
                j = r15.A04;
            }
            if (A032 != null || this.A06.A00() - (1000 * j) < 2592000000L) {
                j4 = j;
            }
            Iterator it8 = set.iterator();
            while (it8.hasNext()) {
                UserJid userJid3 = (UserJid) it8.next();
                HashSet hashSet6 = new HashSet();
                boolean A0F = r1.A0F(userJid3);
                HashSet hashSet7 = null;
                C41961uR r62 = null;
                if (A0F) {
                    A03 = null;
                } else {
                    Object obj3 = hashMap5.get(userJid3);
                    AnonymousClass009.A05(obj3);
                    A03 = A03((C28601Of) obj3, userJid3, hashMap7, hashSet6);
                }
                if (A0F) {
                    j2 = 0;
                } else {
                    AnonymousClass1YF r16 = (AnonymousClass1YF) hashMap2.get(userJid3);
                    j2 = 0;
                    if (r16 == null) {
                        j3 = 0;
                    } else {
                        j3 = r16.A04;
                    }
                    if (A03 != null || this.A06.A00() - (1000 * j3) < 2592000000L) {
                        j2 = j3;
                    }
                }
                if (A032 != null || j4 != 0 || A03 != null || j2 != 0) {
                    if (!hashSet6.isEmpty()) {
                        hashSet7 = hashSet6;
                    }
                    r62 = new C41961uR(A032, A03, hashSet7, j4, j2);
                }
                hashMap.put(userJid3, r62);
            }
        }
        return hashMap;
    }

    public void A09(DeviceJid deviceJid, boolean z) {
        boolean z2 = false;
        if (deviceJid.device == 0) {
            z2 = true;
        }
        AnonymousClass009.A0F(!z2);
        this.A07.A0I.A00();
        if (z) {
            if (this.A01.A0E(deviceJid)) {
                this.A09.A0C(deviceJid, "unknown_companion", false, false);
            } else {
                C18770sz r3 = this.A0B;
                UserJid userJid = deviceJid.getUserJid();
                AnonymousClass1YJ r0 = new AnonymousClass1YJ();
                r0.A02(deviceJid);
                r3.A0G(r0.A00(), userJid, false);
            }
        }
        this.A08.A03(Collections.singleton(deviceJid));
        Iterator it = this.A00.keySet().iterator();
        if (it.hasNext()) {
            it.next();
            throw new NullPointerException("onCompanionDeviceVerificationFail");
        }
    }

    public void A0A(UserJid userJid, long j) {
        if (j - (this.A06.A00() / 1000) > 5184000) {
            StringBuilder sb = new StringBuilder("DeviceADVInfoHandler/removeDeviceInfoIfTimestampIsInvalid invalid local timestamp ts=");
            sb.append(j);
            Log.e(sb.toString());
            this.A0B.A0J(userJid, true);
        }
    }

    public final void A0B(UserJid userJid, long j) {
        C18770sz r1 = this.A0B;
        AnonymousClass1YF A09 = r1.A09(userJid);
        if (A09 != null) {
            r1.A0H(r1.A08(A09, j), userJid);
            return;
        }
        StringBuilder sb = new StringBuilder("DeviceADVInfoManager/updateDeviceInfoWithExpectedTs user has no device info, user=");
        sb.append(userJid);
        Log.w(sb.toString());
    }

    public boolean A0C(DeviceJid deviceJid, C15930o9 r12, byte[] bArr, int i) {
        byte[] bArr2;
        this.A07.A0I.A00();
        if (deviceJid == null || deviceJid.device == 0 || r12 == null || r12.A00 != 1) {
            return true;
        }
        byte[] bArr3 = r12.A02;
        try {
            bArr2 = C15940oB.A01(new C31641as(C31481ac.A00(((C42031uY) AbstractC27091Fz.A0A(AbstractC27881Jp.A01(bArr3, 1, bArr3.length - 1), C42031uY.A07)).A05.A04())).A00.A00()).A01;
        } catch (C28971Pt | AnonymousClass1RY | C31561ak e) {
            Log.e("DeviceADVInfoManager/extractIdentityFromCipherText/fail to get key", e);
            bArr2 = null;
        }
        return A0D(deviceJid, bArr, bArr2, (byte) 5, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0179, code lost:
        if (new X.AnonymousClass1JS(r4).equals(r12) == false) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01a6, code lost:
        if (((java.lang.Number) r4.get(r24)).longValue() != ((long) r2.A01)) goto L_0x01a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01f4, code lost:
        if (r3.A00 != null) goto L_0x01f6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0283  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0D(com.whatsapp.jid.DeviceJid r24, byte[] r25, byte[] r26, byte r27, int r28) {
        /*
        // Method dump skipped, instructions count: 856
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22130yZ.A0D(com.whatsapp.jid.DeviceJid, byte[], byte[], byte, int):boolean");
    }

    public boolean A0E(UserJid userJid, C41911uM r21, long j) {
        long j2;
        if (r21 == null) {
            StringBuilder sb = new StringBuilder("DeviceADVInfoManager/verifyKeyIndexListData/validate indexList fail, userJid=");
            sb.append(userJid);
            Log.w(sb.toString());
            return false;
        }
        long j3 = r21.A04;
        if (j != j3) {
            C234911w r6 = this.A0A;
            C42021uX r5 = new C42021uX();
            r5.A01 = Long.valueOf(j / 3600);
            r5.A00 = Long.valueOf(j3 / 3600);
            r6.A00.A07(r5);
            return false;
        }
        C18770sz r4 = this.A0B;
        AnonymousClass1YF A09 = r4.A09(userJid);
        long A00 = this.A06.A00();
        int A02 = this.A0C.A02(730);
        if (A02 < 1) {
            A02 = 1;
        }
        if (j >= (A00 - (((long) Math.min(35, A02)) * 86400000)) / 1000 || r21.A05.size() <= 1) {
            if (A09 == null || A09.A00 != r21.A02) {
                r4.A0J(userJid, true);
            }
            return true;
        }
        C234911w r13 = this.A0A;
        if (A09 != null) {
            j2 = A09.A04;
        } else {
            j2 = 0;
        }
        r13.A03(j2, j, true);
        return false;
    }
}
