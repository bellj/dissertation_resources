package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1FM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FM extends AbstractC17250qV {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C14650lo A02;
    public final C15550nR A03;
    public final C14830m7 A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;
    public final C17650rA A07;
    public final C22320yt A08;
    public final C15650ng A09;
    public final C21380xK A0A;
    public final AnonymousClass12H A0B;
    public final C242714w A0C;
    public final AnonymousClass102 A0D;
    public final C14850m9 A0E;
    public final C17070qD A0F;
    public final C22940zt A0G;
    public final C20320vZ A0H;

    public AnonymousClass1FM(AbstractC15710nm r9, C15570nT r10, C14650lo r11, C15550nR r12, C14830m7 r13, C16590pI r14, AnonymousClass018 r15, C17650rA r16, C22320yt r17, C15650ng r18, C21380xK r19, AnonymousClass12H r20, C242714w r21, AnonymousClass102 r22, C14850m9 r23, C17220qS r24, C17230qT r25, C17070qD r26, C22940zt r27, C20320vZ r28, AbstractC14440lR r29) {
        super(r9, r24, r25, r29, new int[]{104}, true);
        this.A04 = r13;
        this.A0E = r23;
        this.A05 = r14;
        this.A00 = r9;
        this.A01 = r10;
        this.A03 = r12;
        this.A0A = r19;
        this.A06 = r15;
        this.A0H = r28;
        this.A0F = r26;
        this.A09 = r18;
        this.A0B = r20;
        this.A0G = r27;
        this.A08 = r17;
        this.A02 = r11;
        this.A0D = r22;
        this.A0C = r21;
        this.A07 = r16;
    }

    @Override // X.AbstractC17250qV
    public void A00(AnonymousClass1V8 r40, int i) {
        String A0I;
        List<AnonymousClass1V8> A0J;
        long j;
        AnonymousClass1V8 A0D;
        AbstractC15340mz A01;
        List list = null;
        String A0I2 = r40.A0I("from", null);
        if (A0I2 == null || !A0I2.equals("0@s.whatsapp.net")) {
            AnonymousClass1V8 A0E = r40.A0E("campaign");
            if (A0E == null) {
                A0I = null;
            } else {
                A0I = A0E.A0I("id", null);
                list = A0E.A0J("revoke");
            }
            if (list == null || list.size() <= 0) {
                long j2 = 0;
                if (A0E != null) {
                    j2 = A0E.A08("duration", 0);
                }
                if (A0E != null) {
                    A0J = A0E.A0J("message");
                } else {
                    A0J = r40.A0J("message");
                }
                if (A0J != null) {
                    long A00 = this.A04.A00();
                    ArrayList arrayList = new ArrayList(A0J.size());
                    for (AnonymousClass1V8 r11 : A0J) {
                        long j3 = 0;
                        try {
                            j3 = r11.A08("order", 0);
                        } catch (AnonymousClass1V9 e) {
                            StringBuilder sb = new StringBuilder("connection/getMessagesPsaFromProtocolTree: ");
                            sb.append(e);
                            Log.e(sb.toString());
                        }
                        String A0I3 = r11.A0I("id", null);
                        AnonymousClass1V8 A0D2 = r11.A0D(0);
                        if (A0D2 != null) {
                            AbstractC15340mz A012 = this.A0H.A01(new AnonymousClass1IS(AnonymousClass1VX.A00, A0I3, false), (byte) 0, j3 * 1000);
                            A012.A0w(A0D2.A01);
                            long j4 = j2 * 1000;
                            if (j4 == 0) {
                                j = Long.MAX_VALUE;
                            } else {
                                j = j4 + A00;
                            }
                            arrayList.add(new AnonymousClass1V1(A012, A0I, j));
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        Object andSet = super.A05.getAndSet(null);
                        AnonymousClass009.A05(andSet);
                        super.A04.add(andSet);
                        this.A08.A01(new RunnableBRunnable0Shape1S0300000_I0_1(this, andSet, arrayList, 36), 27);
                        return;
                    }
                    return;
                }
                return;
            }
            Object andSet2 = super.A05.getAndSet(null);
            AnonymousClass009.A05(andSet2);
            super.A04.add(andSet2);
            this.A08.A01(new RunnableBRunnable0Shape0S1200000_I0(this, andSet2, A0I, 27), 27);
            return;
        }
        C14850m9 r9 = this.A0E;
        if (r9.A07(1844) && (A0D = r40.A0D(0)) != null) {
            List<AnonymousClass1V8> A0J2 = A0D.A0J("message");
            ArrayList arrayList2 = new ArrayList();
            long j5 = 0;
            try {
                j5 = r40.A08("t", 0) * 1000;
            } catch (AnonymousClass1V9 e2) {
                StringBuilder sb2 = new StringBuilder("connection/getMessageChatPSAFromProtocolTree: ");
                sb2.append(e2);
                Log.e(sb2.toString());
            }
            for (AnonymousClass1V8 r6 : A0J2) {
                if (r6 != null) {
                    String A0I4 = r6.A0I("id", null);
                    if (AnonymousClass1US.A0C(A0I4)) {
                        continue;
                    } else {
                        if ("text".equalsIgnoreCase(r6.A0I("type", "text"))) {
                            A01 = this.A0H.A01(new AnonymousClass1IS(AnonymousClass1VZ.A00, A0I4, false), (byte) 0, j5);
                            A01.A0w(r6.A01);
                            synchronized (A01.A10) {
                                A01.A02 = 0;
                            }
                        } else {
                            AnonymousClass1V8 A0D3 = r6.A0D(0);
                            if (A0D3 == null) {
                                Log.e("connection/generateChatPSAMultimediaFMessage null media");
                            } else {
                                try {
                                    C27081Fy A013 = C27081Fy.A01(A0D3.A01);
                                    C16590pI r2 = this.A05;
                                    AbstractC15710nm r22 = this.A00;
                                    C15550nR r23 = this.A03;
                                    AnonymousClass018 r24 = this.A06;
                                    C22940zt r25 = this.A0G;
                                    C14650lo r13 = this.A02;
                                    C15570nT r12 = this.A01;
                                    C20320vZ r112 = this.A0H;
                                    C17070qD r62 = this.A0F;
                                    AnonymousClass102 r5 = this.A0D;
                                    C17650rA r4 = this.A07;
                                    AnonymousClass1VZ r3 = AnonymousClass1VZ.A00;
                                    A01 = C32401c6.A05(r22, r12, r13, r23, r2, r24, r4, r5, r9, null, r62, r25, C32401c6.A01(r9, A013), A013, new AnonymousClass1IS(r3, A0I4, false), r112, j5, false, false, false);
                                    A01.A0e(r3);
                                    A01.A0k = "WhatsApp";
                                    A01.A0I = j5;
                                } catch (Exception e3) {
                                    StringBuilder sb3 = new StringBuilder("connection/generateChatPSAMultimediaFMessage ");
                                    sb3.append(e3);
                                    Log.e(sb3.toString());
                                }
                            }
                        }
                        arrayList2.add(A01);
                    }
                }
            }
            if (!arrayList2.isEmpty()) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    AbstractC15340mz r26 = (AbstractC15340mz) it.next();
                    if (this.A09.A0p(r26)) {
                        Log.i("PSANotificationHandler/message added");
                    } else {
                        StringBuilder sb4 = new StringBuilder("PSANotificationHandler/didn't add message: ");
                        sb4.append(r26.toString());
                        Log.e(sb4.toString());
                    }
                }
            }
        }
    }
}
