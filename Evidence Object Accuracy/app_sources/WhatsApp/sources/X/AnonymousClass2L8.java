package X;

import android.content.SharedPreferences;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2L8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L8 {
    public static final AnonymousClass1W9[] A0U = new AnonymousClass1W9[0];
    public int A00;
    public long A01;
    public long A02;
    public Map A03;
    public final AbstractC15710nm A04;
    public final C15570nT A05;
    public final C50232Or A06;
    public final C22160yd A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14820m6 A0A;
    public final C22350yw A0B;
    public final C14850m9 A0C;
    public final C21680xo A0D;
    public final C20710wC A0E;
    public final C16030oK A0F;
    public final C14840m8 A0G;
    public final C450720b A0H;
    public final AnonymousClass1EW A0I;
    public final C17230qT A0J;
    public final C18610sj A0K;
    public final C17070qD A0L;
    public final AnonymousClass1EX A0M;
    public final AnonymousClass208 A0N;
    public final AnonymousClass208 A0O;
    public final C18910tG A0P;
    public final AnonymousClass100 A0Q;
    public final C26601Ec A0R;
    public final JniBridge A0S;
    public final C33991fP A0T;

    public AnonymousClass2L8(AbstractC15710nm r2, C15570nT r3, C50232Or r4, C22160yd r5, C14830m7 r6, C16590pI r7, C14820m6 r8, C22350yw r9, C14850m9 r10, C21680xo r11, C20710wC r12, C16030oK r13, C14840m8 r14, C450720b r15, AnonymousClass1EW r16, C17230qT r17, C18610sj r18, C17070qD r19, AnonymousClass1EX r20, AnonymousClass208 r21, AnonymousClass208 r22, C18910tG r23, AnonymousClass100 r24, C26601Ec r25, JniBridge jniBridge, C33991fP r27, Map map) {
        this.A08 = r6;
        this.A0C = r10;
        this.A04 = r2;
        this.A05 = r3;
        this.A09 = r7;
        this.A0S = jniBridge;
        this.A07 = r5;
        this.A0I = r16;
        this.A0D = r11;
        this.A0L = r19;
        this.A0M = r20;
        this.A0P = r23;
        this.A0E = r12;
        this.A0G = r14;
        this.A0B = r9;
        this.A0J = r17;
        this.A0A = r8;
        this.A0Q = r24;
        this.A0K = r18;
        this.A0F = r13;
        this.A0R = r25;
        this.A06 = r4;
        this.A0H = r15;
        this.A0O = r21;
        this.A0T = r27;
        this.A03 = map;
        this.A0N = r22;
    }

    public static AnonymousClass1V8 A00(C15930o9 r7, Integer num, String str, String str2, int i, boolean z) {
        String str3;
        int i2 = r7.A01;
        int i3 = r7.A00;
        if (i3 == 0) {
            str3 = "msg";
        } else if (i3 == 1) {
            str3 = "pkmsg";
        } else if (i3 == 2) {
            str3 = "skmsg";
        } else if (i3 == 3) {
            str3 = "frskmsg";
        } else {
            StringBuilder sb = new StringBuilder("Unsupported ciphertext type ");
            sb.append(i3);
            throw new IllegalArgumentException(sb.toString());
        }
        return new AnonymousClass1V8("enc", r7.A02, (AnonymousClass1W9[]) A04(num, str3, str, str2, i2, i, z).toArray(A0U));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a0, code lost:
        if (r5 != 14) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass1V8 A01(X.C34081fY r11) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2L8.A01(X.1fY):X.1V8");
    }

    public static final AnonymousClass1V8 A02(String str, List list) {
        int size = list.size();
        AnonymousClass1V8[] r6 = new AnonymousClass1V8[size];
        for (int i = 0; i < size; i++) {
            r6[i] = new AnonymousClass1V8("participant", new AnonymousClass1W9[]{new AnonymousClass1W9((Jid) list.get(i), "jid")});
        }
        return new AnonymousClass1V8(str, (AnonymousClass1W9[]) null, r6);
    }

    public static AnonymousClass1V8 A03(byte[] bArr) {
        AnonymousClass1G3 A00 = C27081Fy.A00();
        AnonymousClass1G4 A0T = C456122i.A05.A0T();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C456122i r1 = (C456122i) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = A01;
        A00.A03();
        C27081Fy r12 = (C27081Fy) A00.A00;
        r12.A05 = (C456122i) A0T.A02();
        r12.A00 |= 512;
        return new AnonymousClass1V8("call", A00.A02().A02(), (AnonymousClass1W9[]) null);
    }

    public static List A04(Integer num, String str, String str2, String str3, int i, int i2, boolean z) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("v", Integer.toString(i)));
        arrayList.add(new AnonymousClass1W9("type", str));
        if (i2 != 0) {
            arrayList.add(new AnonymousClass1W9("count", String.valueOf(i2)));
        }
        if (str2 != null) {
            arrayList.add(new AnonymousClass1W9("mediatype", str2));
        }
        if (str3 != null) {
            arrayList.add(new AnonymousClass1W9("native_flow_name", str3));
        }
        if (num != null && num.intValue() > 0) {
            arrayList.add(new AnonymousClass1W9("duration", String.valueOf(num)));
        }
        if (z) {
            arrayList.add(new AnonymousClass1W9("decrypt-fail", "hide"));
        }
        return arrayList;
    }

    public static List A05(Integer num, String str, String str2, List list, Map map, Map map2, Map map3, int i, boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList();
        if (map2 != null) {
            for (Map.Entry entry : map2.entrySet()) {
                DeviceJid deviceJid = (DeviceJid) entry.getKey();
                String str3 = (String) map.get(DeviceJid.of(deviceJid.getUserJid()));
                if (str3 == null || str3.trim().length() == 0) {
                    StringBuilder sb = new StringBuilder("Connection/createParticipantsListWithEnc:empty ephemeral setting ephemeralSetting=");
                    boolean z3 = false;
                    if (str3 == null) {
                        z3 = true;
                    }
                    sb.append(z3);
                    Log.w(sb.toString());
                }
                C15930o9 r10 = (C15930o9) entry.getValue();
                String str4 = null;
                Integer num2 = null;
                String str5 = null;
                if (z) {
                    str4 = str;
                    num2 = num;
                    str5 = str2;
                }
                AnonymousClass1V8 A00 = A00(r10, num2, str4, str5, i, z2);
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(new AnonymousClass1W9(deviceJid, "jid"));
                if (str3 != null) {
                    arrayList2.add(new AnonymousClass1W9("eph_setting", str3));
                }
                arrayList.add(new AnonymousClass1V8(A00, "to", (AnonymousClass1W9[]) arrayList2.toArray(A0U)));
            }
        }
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                DeviceJid deviceJid2 = (DeviceJid) it.next();
                String str6 = (String) map.get(DeviceJid.of(deviceJid2.getUserJid()));
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new AnonymousClass1W9(deviceJid2, "jid"));
                if (str6 != null) {
                    arrayList3.add(new AnonymousClass1W9("eph_setting", str6));
                }
                arrayList.add(new AnonymousClass1V8("to", (AnonymousClass1W9[]) arrayList3.toArray(A0U)));
            }
        }
        if (map3 != null) {
            for (Map.Entry entry2 : map3.entrySet()) {
                AnonymousClass1V8 A002 = A00((C15930o9) entry2.getValue(), num, str, str2, i, z2);
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(new AnonymousClass1W9((Jid) entry2.getKey(), "jid"));
                arrayList.add(new AnonymousClass1V8(A002, "to", (AnonymousClass1W9[]) arrayList4.toArray(A0U)));
            }
        }
        return arrayList;
    }

    public static final AnonymousClass1W9[] A06(AbstractC14640lm r3, AbstractC14640lm r4, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9(r3, "to"));
        arrayList.add(new AnonymousClass1W9("id", str));
        arrayList.add(new AnonymousClass1W9("type", str3));
        if (r4 != null) {
            arrayList.add(new AnonymousClass1W9(r4, "participant"));
        }
        if (str2 != null) {
            arrayList.add(new AnonymousClass1W9("category", str2));
        }
        return (AnonymousClass1W9[]) arrayList.toArray(A0U);
    }

    public static final AnonymousClass1W9[] A07(Jid jid, Jid jid2, UserJid userJid, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9(jid, "to"));
        arrayList.add(new AnonymousClass1W9("id", str));
        if (str2 != null) {
            arrayList.add(new AnonymousClass1W9("type", str2));
        }
        if (jid2 != null) {
            arrayList.add(new AnonymousClass1W9(jid2, "participant"));
        }
        if (userJid != null) {
            arrayList.add(new AnonymousClass1W9(userJid, "recipient"));
        }
        if (str3 != null) {
            arrayList.add(new AnonymousClass1W9("category", str3));
        }
        return (AnonymousClass1W9[]) arrayList.toArray(A0U);
    }

    public static final AnonymousClass1V8[] A08(String[] strArr) {
        if (strArr == null) {
            return null;
        }
        int length = strArr.length;
        AnonymousClass1V8[] r6 = new AnonymousClass1V8[length];
        for (int i = 0; i < length; i++) {
            r6[i] = new AnonymousClass1V8("item", new AnonymousClass1W9[]{new AnonymousClass1W9("id", strArr[i])});
        }
        return new AnonymousClass1V8[]{new AnonymousClass1V8("list", (AnonymousClass1W9[]) null, r6)};
    }

    public void A09() {
        this.A0N.A05(new AnonymousClass1V8("presence", new AnonymousClass1W9[]{new AnonymousClass1W9("type", "available")}), 1);
    }

    public final void A0A(AbstractC14640lm r14, AbstractC14640lm r15, Integer num, String str, String str2, String str3, String str4) {
        AbstractC14640lm r8 = r14;
        AbstractC14640lm r7 = r15;
        if (!C15380n4.A0H(r15)) {
            r7 = r14;
            r8 = r15;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("type", str3));
        if (num != null) {
            arrayList.add(new AnonymousClass1W9("reason", String.valueOf(num)));
        }
        if (str2 != null) {
            arrayList.add(new AnonymousClass1W9("sub-type", str2));
        }
        ArrayList arrayList2 = new ArrayList();
        AnonymousClass1V8 r0 = new AnonymousClass1V8("error", (AnonymousClass1W9[]) arrayList.toArray(A0U));
        AnonymousClass1W9[] A07 = A07(r7, r8, null, str, "error", null);
        arrayList2.add(r0);
        if (str4 != null) {
            arrayList2.add(new AnonymousClass1V8("biz", new AnonymousClass1W9[]{new AnonymousClass1W9("reason", str4)}));
        }
        this.A0N.A05(new AnonymousClass1V8("receipt", A07, (AnonymousClass1V8[]) arrayList2.toArray(new AnonymousClass1V8[0])), 1);
    }

    public void A0B(AbstractC14640lm r17, AbstractC14640lm r18, String str, String str2, boolean z) {
        AnonymousClass1G7 r0 = (AnonymousClass1G7) AnonymousClass1G6.A0k.A0T();
        AnonymousClass1G9 r2 = (AnonymousClass1G9) AnonymousClass1G8.A05.A0T();
        r2.A07(r17.getRawString());
        r2.A08(z);
        r2.A05(str);
        r0.A07((AnonymousClass1G8) r2.A02());
        if (r18 != null) {
            r0.A0A(r18.getRawString());
        }
        C30331Wz r4 = new C30331Wz(new AnonymousClass1IS(r17, str, z), (byte) 15, 7, 0);
        r4.A01 = str2;
        AnonymousClass1G3 A00 = C27081Fy.A00();
        C32411c7.A0S(r4, new C39971qq(this.A05, null, this.A0C, A00, null, null, null, true, false, false));
        r0.A03();
        AnonymousClass1G6 r42 = (AnonymousClass1G6) r0.A00;
        r42.A0L = (C27081Fy) A00.A02();
        r42.A01 |= 2;
        A0K(new AnonymousClass1V8(new AnonymousClass1V8("message", r0.A02().A02(), (AnonymousClass1W9[]) null), "action", new AnonymousClass1W9[]{new AnonymousClass1W9("add", "relay")}), str, "v");
    }

    public final void A0C(AbstractC14640lm r11, DeviceJid deviceJid, UserJid userJid, AnonymousClass1IS r14, String str, String[] strArr, long j) {
        Pair A00 = C32401c6.A00(deviceJid, r14.A00, r11);
        A0I(new AnonymousClass1V8("receipt", A07((Jid) A00.first, (Jid) A00.second, userJid, r14.A01, str, null), A08(strArr)), j);
    }

    public final void A0D(DeviceJid deviceJid, DeviceJid deviceJid2, String str, String str2, String str3, long j) {
        A0I(new AnonymousClass1V8("receipt", new AnonymousClass1W9[]{new AnonymousClass1W9(deviceJid, "to"), new AnonymousClass1W9("id", str)}, new AnonymousClass1V8[]{new AnonymousClass1V8(str3, new AnonymousClass1W9[]{new AnonymousClass1W9("call-id", str2), new AnonymousClass1W9(deviceJid2, "call-creator")})}), j);
    }

    public final void A0E(Jid jid, AbstractC34031fT r15, AbstractC32491cF r16, Integer num, String str, String str2, List list, List list2) {
        AnonymousClass1V8[] r6;
        String str3;
        int intValue;
        String str4 = str2;
        if (str2 == null) {
            int i = this.A00 + 1;
            this.A00 = i;
            str4 = Integer.toHexString(i);
        }
        this.A03.put(str4, new C862546k(r15, r16, this));
        if (list != null) {
            int size = list.size();
            r6 = new AnonymousClass1V8[size];
            for (int i2 = 0; i2 < size; i2++) {
                AnonymousClass1IS r11 = (AnonymousClass1IS) list.get(i2);
                AnonymousClass1W9[] r2 = new AnonymousClass1W9[2];
                r2[0] = new AnonymousClass1W9("index", r11.A01);
                r2[1] = new AnonymousClass1W9("owner", r11.A02 ? "true" : "false");
                r6[i2] = new AnonymousClass1V8("item", r2);
            }
        } else {
            r6 = null;
        }
        if (list2 != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list2.iterator();
            while (it.hasNext()) {
                arrayList.add(AnonymousClass1EW.A00((AnonymousClass1JX) it.next()));
            }
            r6 = (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0]);
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new AnonymousClass1W9("type", str));
        arrayList2.add(new AnonymousClass1W9(jid, "jid"));
        if (num != null && (intValue = num.intValue()) > 0 && intValue < 1000000) {
            arrayList2.add(new AnonymousClass1W9("modify_tag", num.toString()));
        }
        AnonymousClass1V8 r1 = new AnonymousClass1V8(new AnonymousClass1V8("chat", (AnonymousClass1W9[]) arrayList2.toArray(A0U), r6), "action", (AnonymousClass1W9[]) null);
        if ("clear".equals(str)) {
            str3 = "f";
        } else {
            str3 = "m";
        }
        A0K(r1, str4, str3);
    }

    public void A0F(C15580nU r6, AbstractC32491cF r7, AnonymousClass1P4 r8, Runnable runnable, String str, AnonymousClass1W9[] r11) {
        String hexString;
        if (r8 != null) {
            hexString = r8.A01;
        } else {
            int i = this.A00 + 1;
            this.A00 = i;
            hexString = Integer.toHexString(i);
        }
        this.A03.put(hexString, new C862346i(r7, this, runnable, str));
        AnonymousClass1V8 r3 = new AnonymousClass1V8(str, r11);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("id", hexString));
        arrayList.add(new AnonymousClass1W9("xmlns", "w:g2"));
        arrayList.add(new AnonymousClass1W9("type", "set"));
        arrayList.add(new AnonymousClass1W9(r6, "to"));
        if (r8 != null) {
            arrayList.add(new AnonymousClass1W9("web", r8.A00));
        }
        this.A0N.A05(new AnonymousClass1V8(r3, "iq", (AnonymousClass1W9[]) arrayList.toArray(A0U)), 1);
    }

    public final void A0G(C15580nU r10, AnonymousClass1P4 r11, String str, String str2, List list, boolean z) {
        int size = list.size();
        AnonymousClass1V8[] r4 = new AnonymousClass1V8[size];
        for (int i = 0; i < size; i++) {
            r4[i] = new AnonymousClass1V8("participant", new AnonymousClass1W9[]{new AnonymousClass1W9((Jid) list.get(i), "jid")});
        }
        AnonymousClass1V8 r3 = new AnonymousClass1V8(str2, (AnonymousClass1W9[]) null, r4);
        int i2 = 5;
        if (r11 == null) {
            i2 = 4;
        }
        AnonymousClass1W9[] r42 = new AnonymousClass1W9[i2];
        r42[0] = new AnonymousClass1W9("id", str);
        r42[1] = new AnonymousClass1W9("xmlns", "w:g2");
        r42[2] = new AnonymousClass1W9("type", "set");
        r42[3] = new AnonymousClass1W9(r10, "to");
        if (r11 != null) {
            r42[4] = new AnonymousClass1W9("web", r11.A00);
        }
        if (z) {
            r3 = new AnonymousClass1V8(r3, "admin", (AnonymousClass1W9[]) null);
        }
        this.A0N.A05(new AnonymousClass1V8(r3, "iq", r42), 1);
    }

    public void A0H(UserJid userJid, AbstractC34031fT r6, AnonymousClass1IS r7, AbstractC32491cF r8, String str, String str2) {
        if (str == null) {
            int i = this.A00 + 1;
            this.A00 = i;
            str = Integer.toHexString(i);
        }
        this.A03.put(str, new C862446j(r6, r8, this));
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("kind", "status"));
        AbstractC14640lm r2 = r7.A00;
        AnonymousClass009.A05(r2);
        arrayList.add(new AnonymousClass1W9(r2, "jid"));
        arrayList.add(new AnonymousClass1W9("index", r7.A01));
        arrayList.add(new AnonymousClass1W9("owner", String.valueOf(r7.A02)));
        arrayList.add(new AnonymousClass1W9(userJid, "chat"));
        arrayList.add(new AnonymousClass1W9("checksum", str2));
        A0K(new AnonymousClass1V8("read", (AnonymousClass1W9[]) arrayList.toArray(A0U)), str, "d");
    }

    public final void A0I(AnonymousClass1V8 r7, long j) {
        AnonymousClass1V4 A01 = this.A0J.A01(j);
        if (A01 != null) {
            synchronized (A01) {
                int i = A01.A00;
                if (i == 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Processing is already done for ");
                    sb.append(A01.A0B);
                    String obj = sb.toString();
                    AbstractC15710nm r2 = A01.A05;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(A01.A01());
                    sb2.append("/failed processing done check");
                    r2.AaV(sb2.toString(), obj, true);
                } else {
                    A01.A03(i, SystemClock.uptimeMillis() - A01.A01);
                    A01.A00 = 0;
                    A01.A03(0, SystemClock.uptimeMillis() - A01.A04);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(A01.A01());
                    sb3.append("/onProcessingDone/stanzaId = ");
                    sb3.append(A01.A0B);
                    Log.i(sb3.toString());
                    C17230qT r3 = A01.A08;
                    synchronized (r3) {
                        r3.A02(A01.A02).remove(Long.valueOf(A01.A03));
                    }
                }
            }
        }
        this.A0N.A05(r7, 1);
    }

    public void A0J(AnonymousClass1V8 r11, AnonymousClass1OT r12) {
        String str;
        C17230qT r3 = this.A0J;
        long j = r12.A00;
        AnonymousClass1V4 A00 = r3.A00(2, j);
        if (!(A00 == null || A00.A0A == null)) {
            C22350yw r32 = this.A0B;
            synchronized (r32) {
                r32.A02.add(r12);
            }
        }
        Jid jid = r12.A01;
        String str2 = r12.A05;
        if (!"receipt".equals(str2) || !"delivery".equals(r12.A08)) {
            str = r12.A08;
        } else {
            str = null;
        }
        ArrayList arrayList = new ArrayList();
        String str3 = r12.A07;
        if (str3 != null) {
            arrayList.add(new AnonymousClass1W9("id", str3));
        } else {
            AnonymousClass009.A0A("received stanza with null id", false);
        }
        if (jid != null) {
            arrayList.add(new AnonymousClass1W9(jid, "to"));
        }
        if (str2 != null) {
            arrayList.add(new AnonymousClass1W9("class", str2));
        } else {
            AnonymousClass009.A0A("received stanza with null class", false);
        }
        if (str != null) {
            arrayList.add(new AnonymousClass1W9("type", str));
        }
        Jid jid2 = r12.A02;
        if (jid2 != null) {
            arrayList.add(new AnonymousClass1W9(jid2, "participant"));
        }
        UserJid userJid = r12.A03;
        if (userJid != null) {
            arrayList.add(new AnonymousClass1W9(userJid, "recipient"));
        }
        String str4 = r12.A06;
        if (!TextUtils.isEmpty(str4) && !"0".equals(str4)) {
            arrayList.add(new AnonymousClass1W9("edit", str4));
        }
        List list = r12.A09;
        if (list != null) {
            arrayList.addAll(list);
        }
        A0I(new AnonymousClass1V8("ack", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0]), r11 == null ? null : new AnonymousClass1V8[]{r11}), j);
    }

    public final void A0K(AnonymousClass1V8 r4, String str, String str2) {
        AnonymousClass208 r2 = this.A0O;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(DefaultCrypto.BUFFER_SIZE);
        r2.A06(r4, byteArrayOutputStream);
        A0L(str, str2, byteArrayOutputStream.toByteArray());
    }

    public final void A0L(String str, String str2, byte[] bArr) {
        C33991fP r4 = this.A0T;
        AnonymousClass1UP r0 = r4.A00;
        byte[] bArr2 = r0.A05;
        byte[] bArr3 = r0.A06;
        if (bArr2 != null && bArr3 != null && bArr != null) {
            try {
                byte[] bArr4 = new byte[16];
                r4.A01.nextBytes(bArr4);
                byte[] A02 = C33991fP.A02(bArr2, bArr4, bArr);
                if (A02 == null) {
                    Log.e("qr encrypt aes fail");
                    return;
                }
                byte[] A00 = C33991fP.A00(bArr4, A02);
                byte[] A01 = C33991fP.A01(bArr3, A00);
                if (A01 == null) {
                    Log.e("qr encrypt mac fail");
                    return;
                }
                AnonymousClass1V8 r2 = new AnonymousClass1V8(new AnonymousClass1V8("enc", C33991fP.A00(A01, A00), str2 != null ? new AnonymousClass1W9[]{new AnonymousClass1W9("type", str2)} : null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("xmlns", "w:web"), new AnonymousClass1W9("id", str)});
                StringBuilder sb = new StringBuilder("connection/sendWebEncrypted id=");
                sb.append(str);
                Log.i(sb.toString());
                this.A0N.A05(r2, 1);
            } catch (Exception e) {
                Log.e("qr encrypt fail ", e);
            }
        }
    }

    public void A0M(boolean z) {
        SharedPreferences sharedPreferences;
        String string;
        String str;
        int i = this.A00 + 1;
        this.A00 = i;
        String hexString = Integer.toHexString(i);
        this.A03.put(hexString, new AnonymousClass34I(this));
        C21680xo r6 = this.A0D;
        synchronized (r6) {
            sharedPreferences = r6.A01;
            string = sharedPreferences.getString("ab_props:sys:config_hash", null);
        }
        if (string == null || z) {
            str = "";
        } else {
            synchronized (r6) {
                str = sharedPreferences.getString("ab_props:sys:config_hash", null);
            }
        }
        this.A0N.A05(new AnonymousClass1V8(new AnonymousClass1V8("props", new AnonymousClass1W9[]{new AnonymousClass1W9("protocol", 1), new AnonymousClass1W9("hash", str)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("id", hexString), new AnonymousClass1W9("xmlns", "abt")}), 1);
    }

    public void A0N(boolean z) {
        int i = this.A00 + 1;
        this.A00 = i;
        String hexString = Integer.toHexString(i);
        this.A03.put(hexString, new AnonymousClass34H(this));
        String str = AbstractC15460nI.A0F;
        if (str == null || z) {
            str = "";
        }
        this.A0N.A05(new AnonymousClass1V8(new AnonymousClass1V8("props", new AnonymousClass1W9[]{new AnonymousClass1W9("protocol", "2"), new AnonymousClass1W9("hash", str)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", hexString), new AnonymousClass1W9("xmlns", "w"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), 1);
    }
}
