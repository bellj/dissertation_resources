package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.3BO  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BO {
    public AnonymousClass2Zb A00;
    public C38911ou A01;
    public final View A02;
    public final WaImageView A03;

    public AnonymousClass3BO(Context context) {
        View A0F = C12960it.A0F(LayoutInflater.from(context), null, R.layout.fresco_gif_search_item_view);
        this.A02 = A0F;
        this.A03 = C12980iv.A0X(A0F, R.id.gif);
    }
}
