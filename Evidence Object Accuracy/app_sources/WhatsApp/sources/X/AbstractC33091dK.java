package X;

import android.view.View;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.Jid;
import java.util.List;
import java.util.Set;

/* renamed from: X.1dK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC33091dK {
    void A7D();

    AbstractC14640lm ADL();

    List AFi();

    Set AGa();

    void AO5(ViewHolder viewHolder, AbstractC14640lm v, int i);

    void AO6(View view, ViewHolder viewHolder, AbstractC14640lm v, int i, int i2);

    void AO7(ViewHolder viewHolder, AbstractC15340mz v);

    void AO8(AnonymousClass1JV v);

    void ASJ(View view, ViewHolder viewHolder, AbstractC14640lm v, int i);

    boolean AaG(Jid jid);
}
