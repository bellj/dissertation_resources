package X;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;

/* renamed from: X.3Iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65273Iw {
    public static Bundle A00(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 26) {
            return bundle.deepCopy();
        }
        Parcel obtain = Parcel.obtain();
        try {
            int dataPosition = obtain.dataPosition();
            obtain.writeBundle(bundle);
            obtain.setDataPosition(dataPosition);
            return obtain.readBundle(C65273Iw.class.getClassLoader());
        } finally {
            obtain.recycle();
        }
    }

    public static void A01(Bundle bundle, Class cls, String str) {
        A02(cls, bundle.get(str), str);
    }

    public static void A02(Class cls, Object obj, String str) {
        Class<?> cls2;
        if (obj != null && (cls2 = obj.getClass()) != cls) {
            StringBuilder A0k = C12960it.A0k("Expecting: ");
            A0k.append(cls);
            A0k.append(" under key ");
            A0k.append(str);
            throw C12970iu.A0f(C12960it.A0Z(cls2, " but was: ", A0k));
        }
    }

    public static void A03(String str, Bundle bundle) {
        Object obj = bundle.get(str);
        if (obj != null) {
            A02(byte[].class, obj, str);
            return;
        }
        throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("Missing required key: ")));
    }
}
