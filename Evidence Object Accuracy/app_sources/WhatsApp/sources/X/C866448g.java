package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.48g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C866448g extends AnonymousClass1JW {
    public final AbstractC14640lm A00;
    public final UserJid A01;
    public final Long A02;
    public final Long A03;

    public C866448g(AbstractC15710nm r1, C15450nH r2, AbstractC14640lm r3, UserJid userJid, Long l, Long l2, String str, boolean z) {
        super(r1, r2);
        this.A00 = r3;
        this.A0G = str;
        this.A0O = z;
        this.A01 = userJid;
        this.A03 = l;
        this.A02 = l2;
    }
}
