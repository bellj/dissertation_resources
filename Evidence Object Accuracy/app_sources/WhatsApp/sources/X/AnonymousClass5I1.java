package X;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: X.5I1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5I1 extends AbstractList<String> implements AnonymousClass5Z4, RandomAccess {
    public final AnonymousClass5Z4 A00;

    @Override // X.AnonymousClass5Z4
    public final AnonymousClass5Z4 AhK() {
        return this;
    }

    public AnonymousClass5I1(AnonymousClass5Z4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5Z4
    public final void Agp(AbstractC111925Bi r2) {
        throw C12970iu.A0z();
    }

    @Override // X.AnonymousClass5Z4
    public final Object Ah7(int i) {
        return this.A00.Ah7(i);
    }

    @Override // X.AnonymousClass5Z4
    public final List AhI() {
        return this.A00.AhI();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return this.A00.get(i);
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection, java.lang.Iterable
    public final Iterator iterator() {
        return new AnonymousClass5D9(this);
    }

    @Override // java.util.AbstractList, java.util.List
    public final ListIterator listIterator(int i) {
        return new AnonymousClass5DQ(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00.size();
    }
}
