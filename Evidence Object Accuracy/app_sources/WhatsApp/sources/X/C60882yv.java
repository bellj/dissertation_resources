package X;

import android.text.TextUtils;
import android.widget.FrameLayout;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60882yv extends AnonymousClass4UX {
    @Override // X.AnonymousClass4UX
    public void A00(FrameLayout frameLayout, AnonymousClass1OY r5, AbstractC15340mz r6, C16470p4 r7) {
        TextEmojiLabel textEmojiLabel;
        int i;
        frameLayout.removeAllViews();
        C58532qo r1 = new C58532qo(frameLayout.getContext());
        frameLayout.addView(r1);
        AnonymousClass1Z6 r0 = r7.A02;
        if (r0 != null) {
            String str = r0.A01;
            if (!TextUtils.isEmpty(str)) {
                AnonymousClass009.A05(str);
                textEmojiLabel = r1.A00;
                r5.setMessageText(str, textEmojiLabel, r6);
                i = 0;
                textEmojiLabel.setVisibility(i);
            }
        }
        textEmojiLabel = r1.A00;
        i = 8;
        textEmojiLabel.setVisibility(i);
    }
}
