package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.0C4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C4 extends AbstractC005102i implements AbstractC11180fs {
    public static final Interpolator A0R = new AccelerateInterpolator();
    public static final Interpolator A0S = new DecelerateInterpolator();
    public int A00 = 0;
    public Activity A01;
    public Context A02;
    public Context A03;
    public View A04;
    public AnonymousClass0CF A05;
    public AnonymousClass02Q A06;
    public AbstractC009504t A07;
    public AnonymousClass0PN A08;
    public ActionBarContainer A09;
    public ActionBarContextView A0A;
    public ActionBarOverlayLayout A0B;
    public AbstractC017508e A0C;
    public ArrayList A0D = new ArrayList();
    public ArrayList A0E = new ArrayList();
    public boolean A0F = true;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L = true;
    public boolean A0M;
    public boolean A0N;
    public final AbstractC12530i4 A0O = new C02600Dd(this);
    public final AbstractC12530i4 A0P = new AnonymousClass0De(this);
    public final AbstractC11270g1 A0Q = new AnonymousClass0YB(this);

    public AnonymousClass0C4(Activity activity, boolean z) {
        this.A01 = activity;
        View decorView = activity.getWindow().getDecorView();
        A0Y(decorView);
        if (!z) {
            this.A04 = decorView.findViewById(16908290);
        }
    }

    public AnonymousClass0C4(Dialog dialog) {
        A0Y(dialog.getWindow().getDecorView());
    }

    @Override // X.AbstractC005102i
    public float A00() {
        return AnonymousClass028.A00(this.A09);
    }

    @Override // X.AbstractC005102i
    public int A01() {
        return ((C017408d) this.A0C).A01;
    }

    @Override // X.AbstractC005102i
    public Context A02() {
        Context context = this.A03;
        if (context == null) {
            TypedValue typedValue = new TypedValue();
            this.A02.getTheme().resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                context = new ContextThemeWrapper(this.A02, i);
            } else {
                context = this.A02;
            }
            this.A03 = context;
        }
        return context;
    }

    @Override // X.AbstractC005102i
    public View A03() {
        return ((C017408d) this.A0C).A06;
    }

    @Override // X.AbstractC005102i
    public AbstractC009504t A04(AnonymousClass02Q r4) {
        AnonymousClass0CF r0 = this.A05;
        if (r0 != null) {
            r0.A05();
        }
        this.A0B.setHideOnContentScrollEnabled(false);
        this.A0A.A03();
        AnonymousClass0CF r2 = new AnonymousClass0CF(this.A0A.getContext(), this, r4);
        AnonymousClass07H r1 = r2.A03;
        r1.A07();
        try {
            if (!r2.A00.AOg(r1, r2)) {
                return null;
            }
            this.A05 = r2;
            r2.A06();
            this.A0A.A05(r2);
            A0Z(true);
            this.A0A.sendAccessibilityEvent(32);
            return r2;
        } finally {
            r1.A06();
        }
    }

    @Override // X.AbstractC005102i
    public void A06() {
        if (!this.A0H) {
            this.A0H = true;
            A0a(false);
        }
    }

    @Override // X.AbstractC005102i
    public void A07(float f) {
        AnonymousClass028.A0V(this.A09, f);
    }

    @Override // X.AbstractC005102i
    public void A08(int i) {
        C017408d r3 = (C017408d) this.A0C;
        r3.AcM(C012005t.A01().A04(r3.A09.getContext(), R.drawable.ic_pip_close));
    }

    @Override // X.AbstractC005102i
    public void A09(int i) {
        A0H(this.A02.getString(i));
    }

    @Override // X.AbstractC005102i
    public void A0A(int i) {
        A0I(this.A02.getString(i));
    }

    @Override // X.AbstractC005102i
    public void A0B(Configuration configuration) {
        new C05280Oy(this.A02).A00.getResources().getBoolean(R.bool.abc_action_bar_embed_tabs);
        this.A09.setTabContainer(null);
        ((C017408d) this.A0C).A09.setCollapsible(false);
        this.A0B.A0G = false;
    }

    @Override // X.AbstractC005102i
    public void A0C(Drawable drawable) {
        this.A09.setPrimaryBackground(drawable);
    }

    @Override // X.AbstractC005102i
    public void A0D(Drawable drawable) {
        this.A0C.AcM(drawable);
    }

    @Override // X.AbstractC005102i
    public void A0E(Drawable drawable) {
        C017408d r0 = (C017408d) this.A0C;
        r0.A04 = drawable;
        r0.A00();
    }

    @Override // X.AbstractC005102i
    public void A0F(View view) {
        this.A0C.Ac3(view);
    }

    @Override // X.AbstractC005102i
    public void A0G(View view, C009604u r3) {
        view.setLayoutParams(r3);
        this.A0C.Ac3(view);
    }

    @Override // X.AbstractC005102i
    public void A0H(CharSequence charSequence) {
        this.A0C.Acw(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0I(CharSequence charSequence) {
        this.A0C.Ad1(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0J(CharSequence charSequence) {
        this.A0C.setWindowTitle(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0K(boolean z) {
        if (z != this.A0K) {
            this.A0K = z;
            ArrayList arrayList = this.A0D;
            if (0 < arrayList.size()) {
                arrayList.get(0);
                throw new NullPointerException("onMenuVisibilityChanged");
            }
        }
    }

    @Override // X.AbstractC005102i
    public void A0L(boolean z) {
        if (!this.A0G) {
            A0M(z);
        }
    }

    @Override // X.AbstractC005102i
    public void A0M(boolean z) {
        int i = 0;
        if (z) {
            i = 4;
        }
        A0X(i, 4);
    }

    @Override // X.AbstractC005102i
    public void A0N(boolean z) {
        A0X(16, 16);
    }

    @Override // X.AbstractC005102i
    public void A0O(boolean z) {
        A0X(0, 2);
    }

    @Override // X.AbstractC005102i
    public void A0P(boolean z) {
        int i = 0;
        if (z) {
            i = 8;
        }
        A0X(i, 8);
    }

    @Override // X.AbstractC005102i
    public void A0Q(boolean z) {
        AnonymousClass0PN r0;
        this.A0M = z;
        if (!z && (r0 = this.A08) != null) {
            r0.A00();
        }
    }

    @Override // X.AbstractC005102i
    public boolean A0U() {
        AnonymousClass0XN r0;
        C07340Xp r02;
        AbstractC017508e r03 = this.A0C;
        if (r03 == null || (r0 = ((C017408d) r03).A09.A0Q) == null || (r02 = r0.A01) == null) {
            return false;
        }
        r02.collapseActionView();
        return true;
    }

    @Override // X.AbstractC005102i
    public boolean A0V(int i, KeyEvent keyEvent) {
        AnonymousClass07H r2;
        AnonymousClass0CF r0 = this.A05;
        if (r0 == null || (r2 = r0.A03) == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent.getDeviceId()).getKeyboardType() == 1) {
            z = false;
        }
        r2.setQwertyMode(z);
        return r2.performShortcut(i, keyEvent, 0);
    }

    public void A0X(int i, int i2) {
        AbstractC017508e r2 = this.A0C;
        int i3 = ((C017408d) r2).A01;
        if ((i2 & 4) != 0) {
            this.A0G = true;
        }
        r2.Ac5((i & i2) | ((i2 ^ -1) & i3));
    }

    public final void A0Y(View view) {
        AbstractC017508e wrapper;
        ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) view.findViewById(R.id.decor_content_parent);
        this.A0B = actionBarOverlayLayout;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        View findViewById = view.findViewById(R.id.action_bar);
        if (findViewById instanceof AbstractC017508e) {
            wrapper = (AbstractC017508e) findViewById;
        } else if (findViewById instanceof Toolbar) {
            wrapper = ((Toolbar) findViewById).getWrapper();
        } else {
            StringBuilder sb = new StringBuilder("Can't make a decor toolbar out of ");
            sb.append(findViewById != null ? findViewById.getClass().getSimpleName() : "null");
            throw new IllegalStateException(sb.toString());
        }
        this.A0C = wrapper;
        this.A0A = (ActionBarContextView) view.findViewById(R.id.action_context_bar);
        ActionBarContainer actionBarContainer = (ActionBarContainer) view.findViewById(R.id.action_bar_container);
        this.A09 = actionBarContainer;
        AbstractC017508e r1 = this.A0C;
        if (r1 == null || this.A0A == null || actionBarContainer == null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getClass().getSimpleName());
            sb2.append(" can only be used with a compatible window decor layout");
            throw new IllegalStateException(sb2.toString());
        }
        Context context = ((C017408d) r1).A09.getContext();
        this.A02 = context;
        if ((((C017408d) this.A0C).A01 & 4) != 0) {
            this.A0G = true;
        }
        Context context2 = new C05280Oy(context).A00;
        context2.getApplicationInfo();
        context2.getResources().getBoolean(R.bool.abc_action_bar_embed_tabs);
        this.A09.setTabContainer(null);
        ((C017408d) this.A0C).A09.setCollapsible(false);
        this.A0B.A0G = false;
        TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(null, AnonymousClass07O.A00, R.attr.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(14, false)) {
            ActionBarOverlayLayout actionBarOverlayLayout2 = this.A0B;
            if (actionBarOverlayLayout2.A0J) {
                this.A0J = true;
                actionBarOverlayLayout2.setHideOnContentScrollEnabled(true);
            } else {
                throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
            }
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(12, 0);
        if (dimensionPixelSize != 0) {
            A07((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    public void A0Z(boolean z) {
        boolean z2;
        AnonymousClass0QQ A0F;
        AnonymousClass0QQ A0F2;
        long j;
        boolean z3 = this.A0N;
        if (z) {
            if (!z3) {
                this.A0N = true;
                z2 = false;
                A0a(z2);
            }
        } else if (z3) {
            z2 = false;
            this.A0N = false;
            A0a(z2);
        }
        if (AnonymousClass028.A0r(this.A09)) {
            AbstractC017508e r7 = this.A0C;
            if (z) {
                C017408d r72 = (C017408d) r7;
                A0F2 = AnonymousClass028.A0F(r72.A09);
                A0F2.A02(0.0f);
                A0F2.A07(100);
                A0F2.A09(new C02640Di(r72, 4));
                ActionBarContextView actionBarContextView = this.A0A;
                AnonymousClass0QQ r2 = actionBarContextView.A0C;
                if (r2 != null) {
                    r2.A00();
                }
                if (actionBarContextView.getVisibility() != 0) {
                    actionBarContextView.setAlpha(0.0f);
                }
                A0F = AnonymousClass028.A0F(actionBarContextView);
                A0F.A02(1.0f);
                A0F.A07(200);
                AnonymousClass0YA r1 = actionBarContextView.A0J;
                r1.A02.A0C = A0F;
                r1.A00 = 0;
                A0F.A09(r1);
            } else {
                C017408d r73 = (C017408d) r7;
                A0F = AnonymousClass028.A0F(r73.A09);
                A0F.A02(1.0f);
                A0F.A07(200);
                A0F.A09(new C02640Di(r73, 0));
                ActionBarContextView actionBarContextView2 = this.A0A;
                AnonymousClass0QQ r0 = actionBarContextView2.A0C;
                if (r0 != null) {
                    r0.A00();
                }
                A0F2 = AnonymousClass028.A0F(actionBarContextView2);
                A0F2.A02(0.0f);
                A0F2.A07(100);
                AnonymousClass0YA r12 = actionBarContextView2.A0J;
                r12.A02.A0C = A0F2;
                r12.A00 = 8;
                A0F2.A09(r12);
            }
            AnonymousClass0PN r4 = new AnonymousClass0PN();
            ArrayList arrayList = r4.A05;
            arrayList.add(A0F2);
            View view = (View) A0F2.A00.get();
            if (view != null) {
                j = view.animate().getDuration();
            } else {
                j = 0;
            }
            View view2 = (View) A0F.A00.get();
            if (view2 != null) {
                view2.animate().setStartDelay(j);
            }
            arrayList.add(A0F);
            r4.A01();
            return;
        }
        AbstractC017508e r02 = this.A0C;
        if (z) {
            ((C017408d) r02).A09.setVisibility(4);
            this.A0A.setVisibility(0);
            return;
        }
        ((C017408d) r02).A09.setVisibility(0);
        this.A0A.setVisibility(8);
    }

    public final void A0a(boolean z) {
        boolean z2;
        View view;
        View view2;
        View view3;
        boolean z3 = this.A0H;
        boolean z4 = this.A0I;
        if (this.A0N || (!z3 && !z4)) {
            z2 = true;
        } else {
            z2 = false;
        }
        boolean z5 = this.A0L;
        if (z2) {
            if (!z5) {
                this.A0L = true;
                AnonymousClass0PN r0 = this.A08;
                if (r0 != null) {
                    r0.A00();
                }
                this.A09.setVisibility(0);
                if (this.A00 != 0 || (!this.A0M && !z)) {
                    this.A09.setAlpha(1.0f);
                    this.A09.setTranslationY(0.0f);
                    if (this.A0F && (view2 = this.A04) != null) {
                        view2.setTranslationY(0.0f);
                    }
                    this.A0P.AMC(null);
                } else {
                    this.A09.setTranslationY(0.0f);
                    float f = (float) (-this.A09.getHeight());
                    if (z) {
                        int[] iArr = {0, 0};
                        this.A09.getLocationInWindow(iArr);
                        f -= (float) iArr[1];
                    }
                    this.A09.setTranslationY(f);
                    AnonymousClass0PN r3 = new AnonymousClass0PN();
                    AnonymousClass0QQ A0F = AnonymousClass028.A0F(this.A09);
                    A0F.A06(0.0f);
                    A0F.A0A(this.A0Q);
                    if (!r3.A03) {
                        r3.A05.add(A0F);
                    }
                    if (this.A0F && (view3 = this.A04) != null) {
                        view3.setTranslationY(f);
                        AnonymousClass0QQ A0F2 = AnonymousClass028.A0F(view3);
                        A0F2.A06(0.0f);
                        if (!r3.A03) {
                            r3.A05.add(A0F2);
                        }
                    }
                    Interpolator interpolator = A0S;
                    boolean z6 = r3.A03;
                    if (!z6) {
                        r3.A01 = interpolator;
                        r3.A00 = 250;
                    }
                    AbstractC12530i4 r02 = this.A0P;
                    if (!z6) {
                        r3.A02 = r02;
                    }
                    this.A08 = r3;
                    r3.A01();
                }
                ActionBarOverlayLayout actionBarOverlayLayout = this.A0B;
                if (actionBarOverlayLayout != null) {
                    AnonymousClass028.A0R(actionBarOverlayLayout);
                }
            }
        } else if (z5) {
            this.A0L = false;
            AnonymousClass0PN r03 = this.A08;
            if (r03 != null) {
                r03.A00();
            }
            if (this.A00 != 0 || (!this.A0M && !z)) {
                this.A0O.AMC(null);
                return;
            }
            this.A09.setAlpha(1.0f);
            this.A09.setTransitioning(true);
            AnonymousClass0PN r32 = new AnonymousClass0PN();
            float f2 = (float) (-this.A09.getHeight());
            if (z) {
                int[] iArr2 = {0, 0};
                this.A09.getLocationInWindow(iArr2);
                f2 -= (float) iArr2[1];
            }
            AnonymousClass0QQ A0F3 = AnonymousClass028.A0F(this.A09);
            A0F3.A06(f2);
            A0F3.A0A(this.A0Q);
            if (!r32.A03) {
                r32.A05.add(A0F3);
            }
            if (this.A0F && (view = this.A04) != null) {
                AnonymousClass0QQ A0F4 = AnonymousClass028.A0F(view);
                A0F4.A06(f2);
                if (!r32.A03) {
                    r32.A05.add(A0F4);
                }
            }
            Interpolator interpolator2 = A0R;
            boolean z7 = r32.A03;
            if (!z7) {
                r32.A01 = interpolator2;
                r32.A00 = 250;
            }
            AbstractC12530i4 r04 = this.A0O;
            if (!z7) {
                r32.A02 = r04;
            }
            this.A08 = r32;
            r32.A01();
        }
    }
}
