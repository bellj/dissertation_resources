package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.util.Log;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0wT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20880wT extends AbstractC18220s6 {
    public final C15570nT A00;
    public final C20870wS A01;
    public final C20670w8 A02;
    public final C15550nR A03;
    public final C18230s7 A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final C14820m6 A07;
    public final C15990oG A08;
    public final C18240s8 A09;
    public final C15600nX A0A;
    public final AbstractC14440lR A0B;
    public final Random A0C;

    public C20880wT(Context context, C15570nT r2, C20870wS r3, C20670w8 r4, C15550nR r5, C18230s7 r6, AnonymousClass01d r7, C14830m7 r8, C14820m6 r9, C15990oG r10, C18240s8 r11, C15600nX r12, AbstractC14440lR r13, Random random) {
        super(context);
        this.A06 = r8;
        this.A0C = random;
        this.A04 = r6;
        this.A00 = r2;
        this.A0B = r13;
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = r3;
        this.A09 = r11;
        this.A05 = r7;
        this.A08 = r10;
        this.A07 = r9;
        this.A0A = r12;
    }

    public final void A02() {
        long A00 = this.A06.A00();
        SharedPreferences sharedPreferences = this.A07.A00;
        if (!sharedPreferences.contains("dithered_last_signed_prekey_rotation")) {
            long nextInt = A00 - (((long) this.A0C.nextInt(2592000)) * 1000);
            StringBuilder sb = new StringBuilder("no signed prekey rotation schedule established; setting last rotation time to ");
            sb.append(C38121nY.A02(nextInt));
            Log.i(sb.toString());
            sharedPreferences.edit().putLong("dithered_last_signed_prekey_rotation", nextInt).apply();
        }
        long j = sharedPreferences.getLong("dithered_last_signed_prekey_rotation", Long.MIN_VALUE);
        if (j >= 0 && j <= A00) {
            long j2 = 2592000000L + j;
            if (j2 >= A00) {
                if (!sharedPreferences.getBoolean("bad_signed_pre_key_check_done", false)) {
                    Log.i("RotateKeysAction/checking bad signed pre key");
                    C18240s8 r4 = this.A09;
                    r4.A00.submit(new RunnableBRunnable0Shape1S0100000_I0_1(this, 47));
                }
                long j3 = j2 - A00;
                StringBuilder sb2 = new StringBuilder("scheduling alarm to trigger signed prekey rotation; now=");
                sb2.append(C38121nY.A02(A00));
                sb2.append("; lastSignedPrekeyRotation=");
                sb2.append(C38121nY.A02(j));
                sb2.append("; deltaToAlarm=");
                sb2.append(j3);
                Log.i(sb2.toString());
                PendingIntent A002 = A00("com.whatsapp.action.ROTATE_SIGNED_PREKEY", 134217728);
                if (!this.A04.A02(A002, 2, j3 + SystemClock.elapsedRealtime())) {
                    Log.w("RotateKeysAction/setupRotateKeysAlarm AlarmManager is null");
                    return;
                }
                return;
            }
        }
        StringBuilder sb3 = new StringBuilder("scheduling immediate signed prekey rotation; now=");
        sb3.append(C38121nY.A02(A00));
        sb3.append("; lastSignedPrekeyRotation=");
        sb3.append(C38121nY.A02(j));
        Log.i(sb3.toString());
        this.A0B.Ab2(new RunnableBRunnable0Shape1S0100000_I0_1(this, 46));
    }

    public final void A03(Intent intent) {
        PowerManager.WakeLock wakeLock;
        StringBuilder sb = new StringBuilder("RotateKeysAction/rotateSignedPrekeyAndSenderKeys; intent=");
        sb.append(intent);
        Log.i(sb.toString());
        PowerManager A0I = this.A05.A0I();
        if (A0I == null) {
            Log.w("RotateKeysAction/rotateSignedPrekeyAndSenderKeys pm=null");
            wakeLock = null;
        } else {
            wakeLock = C39151pN.A00(A0I, "RotateKeysAction#rotateSignedPrekeyAndSenderKeys", 1);
            wakeLock.setReferenceCounted(false);
            wakeLock.acquire(300000);
        }
        try {
            try {
                try {
                    C18240s8 r2 = this.A09;
                    r2.A00.submit(new RunnableBRunnable0Shape1S0100000_I0_1(this, 45)).get();
                    A02();
                } catch (ExecutionException e) {
                    AssertionError assertionError = new AssertionError("exception during rotate keys alarm");
                    assertionError.initCause(e);
                    throw assertionError;
                }
            } catch (InterruptedException e2) {
                AssertionError assertionError2 = new AssertionError("interrupted during rotate keys alarm");
                assertionError2.initCause(e2);
                throw assertionError2;
            }
        } finally {
            if (wakeLock != null) {
                wakeLock.release();
            }
        }
    }
}
