package X;

import java.security.cert.CertPathParameters;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.5By  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112075By implements CertPathParameters {
    public final int A00;
    public final Set A01;
    public final C112085Bz A02;

    @Override // java.security.cert.CertPathParameters, java.lang.Object
    public Object clone() {
        return this;
    }

    public /* synthetic */ C112075By(C93504aH r2) {
        this.A02 = r2.A02;
        this.A01 = Collections.unmodifiableSet(r2.A01);
        this.A00 = r2.A00;
    }
}
