package X;

import java.util.HashMap;

/* renamed from: X.146  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass146 extends AbstractC16220oe {
    public final AnonymousClass15I A00;

    public AnonymousClass146(AnonymousClass15I r1) {
        this.A00 = r1;
    }

    /* renamed from: A05 */
    public void A03(AnonymousClass1KM r7) {
        super.A03(r7);
        HashMap hashMap = new HashMap(this.A00.A01);
        for (String str : hashMap.keySet()) {
            int intValue = ((Number) hashMap.get(str)).intValue();
            for (AnonymousClass1KM r0 : A01()) {
                r0.toString();
                r0.A01(str, intValue);
            }
        }
    }
}
