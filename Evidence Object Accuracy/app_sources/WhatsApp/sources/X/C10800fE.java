package X;

import android.os.Process;

/* renamed from: X.0fE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10800fE extends Thread {
    public final /* synthetic */ C08550bO A00;

    public /* synthetic */ C10800fE(C08550bO r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                ((AnonymousClass0eL) this.A00.A00.take()).run();
            } catch (InterruptedException unused) {
            }
        }
    }
}
