package X;

import java.util.Comparator;

/* renamed from: X.1oX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38681oX implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        AnonymousClass1KZ r3 = (AnonymousClass1KZ) obj;
        AnonymousClass1KZ r4 = (AnonymousClass1KZ) obj2;
        boolean z = r3.A0N;
        if (r4.A0N ^ z) {
            return z ? -1 : 1;
        }
        return r4.A00 - r3.A00;
    }
}
