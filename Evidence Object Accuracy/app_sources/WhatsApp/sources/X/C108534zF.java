package X;

import java.util.Arrays;

/* renamed from: X.4zF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108534zF implements AbstractC115595Se {
    @Override // X.AbstractC115595Se
    public final byte[] AhH(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
