package X;

/* renamed from: X.2BR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2BR extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Double A02;
    public Double A03;
    public Double A04;
    public Double A05;
    public Long A06;

    public AnonymousClass2BR() {
        super(1336, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(13, this.A00);
        r3.Abe(12, this.A01);
        r3.Abe(11, this.A06);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(1, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMemoryStat {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidDalvikPrivateDirty", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidNativePrivateDirty", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidThreadCount", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "privateBytes", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sharedBytes", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uptime", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "workingSetSize", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
