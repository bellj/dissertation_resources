package X;

import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2YI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YI extends AnimatorListenerAdapter {
    public final /* synthetic */ C47342Ag A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass2YI(C47342Ag r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        if (r1 != 5) goto L_0x0048;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0052  */
    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAnimationEnd(android.animation.Animator r8) {
        /*
            r7 = this;
            super.onAnimationEnd(r8)
            X.2Ag r5 = r7.A00
            android.animation.AnimatorSet r0 = r5.A02
            if (r0 == 0) goto L_0x0077
            java.util.ArrayList r0 = r0.getChildAnimations()
            java.util.Iterator r1 = r0.iterator()
        L_0x0011:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r1.next()
            android.animation.Animator r0 = (android.animation.Animator) r0
            android.animation.ValueAnimator r0 = (android.animation.ValueAnimator) r0
            r0.removeAllUpdateListeners()
            goto L_0x0011
        L_0x0023:
            android.animation.AnimatorSet r0 = r5.A02
            r0.removeAllListeners()
            boolean r4 = r7.A01
            r3 = 0
            if (r4 == 0) goto L_0x0075
            android.animation.AnimatorSet r0 = r5.A01(r3)
        L_0x0031:
            r5.A02 = r0
            if (r4 == 0) goto L_0x0057
            com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView r6 = r5.A0H
            int r1 = r5.A00()
            r0 = 1
            r2 = 4
            if (r1 == r0) goto L_0x006a
            r0 = 2
            if (r1 == r0) goto L_0x0067
            r0 = 3
            if (r1 == r0) goto L_0x005c
            r0 = 5
            if (r1 == r0) goto L_0x0067
        L_0x0048:
            android.widget.ImageView r0 = r6.A03
            int r1 = r0.getVisibility()
            r0 = 8
            if (r1 == r0) goto L_0x0057
            android.widget.ImageView r0 = r6.A03
            r0.setVisibility(r2)
        L_0x0057:
            r5.A09 = r4
            r5.A07 = r3
            return
        L_0x005c:
            android.widget.ImageView r0 = r6.A05
            r0.setVisibility(r2)
            android.widget.ImageView r0 = r6.A06
            r0.setVisibility(r2)
            goto L_0x0048
        L_0x0067:
            android.widget.ImageView r0 = r6.A05
            goto L_0x006c
        L_0x006a:
            android.widget.ImageView r0 = r6.A06
        L_0x006c:
            r0.setVisibility(r2)
            com.whatsapp.WaTextView r0 = r6.A09
            r0.setVisibility(r2)
            goto L_0x0048
        L_0x0075:
            r0 = 0
            goto L_0x0031
        L_0x0077:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2YI.onAnimationEnd(android.animation.Animator):void");
    }
}
