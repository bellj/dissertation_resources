package X;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1Ml  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28281Ml {
    public static boolean equalsImpl(Set set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size()) {
                    return false;
                }
                if (set.containsAll(set2)) {
                    return true;
                }
                return false;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    public static int hashCodeImpl(Set set) {
        Iterator it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            Object next = it.next();
            i = ((i + (next != null ? next.hashCode() : 0)) ^ -1) ^ -1;
        }
        return i;
    }

    public static AnonymousClass5I8 intersection(Set set, Set set2) {
        C28291Mn.A04(set, "set1");
        C28291Mn.A04(set2, "set2");
        return new C81293tm(set, set2);
    }

    public static HashSet newHashSet() {
        return new HashSet();
    }

    public static HashSet newHashSetWithExpectedSize(int i) {
        return new HashSet(C28271Mk.capacity(i));
    }

    public static boolean removeAllImpl(Set set, Collection collection) {
        if (collection instanceof AnonymousClass5Z2) {
            collection = ((AnonymousClass5Z2) collection).elementSet();
        }
        if (!(collection instanceof Set) || collection.size() <= set.size()) {
            return removeAllImpl(set, collection.iterator());
        }
        return AnonymousClass1I4.removeAll(set.iterator(), collection);
    }

    public static boolean removeAllImpl(Set set, Iterator it) {
        boolean z = false;
        while (it.hasNext()) {
            z |= set.remove(it.next());
        }
        return z;
    }
}
