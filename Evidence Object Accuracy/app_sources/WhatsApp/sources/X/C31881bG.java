package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1bG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31881bG extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C31881bG A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;
    public AnonymousClass1K6 A03 = AnonymousClass277.A01;
    public C31861bE A04;

    static {
        C31881bG r0 = new C31881bG();
        A05 = r0;
        r0.A0W();
    }

    public C31881bG() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A02 = r0;
        this.A01 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A09(this.A02, 1) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i += CodedOutputStream.A09(this.A01, 2);
        }
        if ((i3 & 4) == 4) {
            C31861bE r0 = this.A04;
            if (r0 == null) {
                r0 = C31861bE.A03;
            }
            i += CodedOutputStream.A0A(r0, 3);
        }
        for (int i4 = 0; i4 < this.A03.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A03.get(i4), 4);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A01, 2);
        }
        if ((this.A00 & 4) == 4) {
            C31861bE r0 = this.A04;
            if (r0 == null) {
                r0 = C31861bE.A03;
            }
            codedOutputStream.A0L(r0, 3);
        }
        for (int i = 0; i < this.A03.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A03.get(i), 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
