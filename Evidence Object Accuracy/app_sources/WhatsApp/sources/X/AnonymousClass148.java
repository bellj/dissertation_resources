package X;

import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.util.Log;

/* renamed from: X.148  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass148 {
    public final C006202y A00 = new C006202y(30);
    public final C16270oj A01;

    public AnonymousClass148(C16270oj r3) {
        this.A01 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006c, code lost:
        if (r1 != null) goto L_0x006e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.C38461o8 A00(java.lang.String r17) {
        /*
            r16 = this;
            r2 = r16
            monitor-enter(r2)
            X.02y r0 = r2.A00     // Catch: all -> 0x007c
            r4 = r17
            java.lang.Object r5 = r0.A04(r4)     // Catch: all -> 0x007c
            X.1o8 r5 = (X.C38461o8) r5     // Catch: all -> 0x007c
            if (r5 != 0) goto L_0x007a
            X.0oj r1 = r2.A01     // Catch: all -> 0x007c
            X.0on r3 = r1.get()     // Catch: all -> 0x007c
            X.0op r8 = r3.A03     // Catch: all -> 0x0075
            java.lang.String r5 = "SELECT media_id, file_hash, media_key, mime_type, upload_url, direct_path, enc_file_hash, file_size, width, height FROM web_upload_media_data_store WHERE media_id=?"
            r7 = 1
            java.lang.String[] r1 = new java.lang.String[r7]     // Catch: all -> 0x0075
            r6 = 0
            r1[r6] = r17     // Catch: all -> 0x0075
            android.database.Cursor r1 = r8.A09(r5, r1)     // Catch: all -> 0x0075
            if (r1 == 0) goto L_0x006b
            boolean r5 = r1.moveToLast()     // Catch: all -> 0x0066
            if (r5 == 0) goto L_0x006b
            java.lang.String r6 = r1.getString(r6)     // Catch: all -> 0x0066
            java.lang.String r7 = r1.getString(r7)     // Catch: all -> 0x0066
            r5 = 2
            byte[] r12 = r1.getBlob(r5)     // Catch: all -> 0x0066
            r5 = 3
            java.lang.String r8 = r1.getString(r5)     // Catch: all -> 0x0066
            r5 = 4
            java.lang.String r9 = r1.getString(r5)     // Catch: all -> 0x0066
            r5 = 5
            java.lang.String r10 = r1.getString(r5)     // Catch: all -> 0x0066
            r5 = 6
            java.lang.String r11 = r1.getString(r5)     // Catch: all -> 0x0066
            r5 = 7
            int r13 = r1.getInt(r5)     // Catch: all -> 0x0066
            r5 = 8
            int r14 = r1.getInt(r5)     // Catch: all -> 0x0066
            r5 = 9
            int r15 = r1.getInt(r5)     // Catch: all -> 0x0066
            X.1o8 r5 = new X.1o8     // Catch: all -> 0x0066
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch: all -> 0x0066
            r0.A08(r4, r5)     // Catch: all -> 0x0066
            goto L_0x006e
        L_0x0066:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x006a
        L_0x006a:
            throw r0     // Catch: all -> 0x0075
        L_0x006b:
            r5 = 0
            if (r1 == 0) goto L_0x0071
        L_0x006e:
            r1.close()     // Catch: all -> 0x0075
        L_0x0071:
            r3.close()     // Catch: all -> 0x007c
            goto L_0x007a
        L_0x0075:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0079
        L_0x0079:
            throw r0     // Catch: all -> 0x007c
        L_0x007a:
            monitor-exit(r2)
            return r5
        L_0x007c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass148.A00(java.lang.String):X.1o8");
    }

    public synchronized void A01(String str) {
        C16310on A02;
        AnonymousClass009.A00();
        this.A00.A07(str);
        try {
            A02 = this.A01.A02();
        } catch (SQLiteDatabaseCorruptException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("WebUploadMediaKeyStore/delete/");
            sb.append(str);
            Log.e(sb.toString(), e);
        }
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("web_upload_media_data_store", "media_id =?", new String[]{str});
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
