package X;

import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;

/* renamed from: X.06n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C014006n {
    public static final Matrix A0G = new Matrix();
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public Paint A06;
    public Paint A07;
    public PathMeasure A08;
    public Boolean A09;
    public String A0A;
    public final Matrix A0B;
    public final Path A0C;
    public final Path A0D;
    public final AnonymousClass00N A0E;
    public final AnonymousClass06o A0F;

    public C014006n() {
        this.A0B = new Matrix();
        this.A01 = 0.0f;
        this.A00 = 0.0f;
        this.A03 = 0.0f;
        this.A02 = 0.0f;
        this.A05 = 255;
        this.A0A = null;
        this.A09 = null;
        this.A0E = new AnonymousClass00N();
        this.A0F = new AnonymousClass06o();
        this.A0C = new Path();
        this.A0D = new Path();
    }

    public C014006n(C014006n r4) {
        this.A0B = new Matrix();
        this.A01 = 0.0f;
        this.A00 = 0.0f;
        this.A03 = 0.0f;
        this.A02 = 0.0f;
        this.A05 = 255;
        this.A0A = null;
        this.A09 = null;
        AnonymousClass00N r2 = new AnonymousClass00N();
        this.A0E = r2;
        this.A0F = new AnonymousClass06o(r2, r4.A0F);
        this.A0C = new Path(r4.A0C);
        this.A0D = new Path(r4.A0D);
        this.A01 = r4.A01;
        this.A00 = r4.A00;
        this.A03 = r4.A03;
        this.A02 = r4.A02;
        this.A04 = r4.A04;
        this.A05 = r4.A05;
        this.A0A = r4.A0A;
        String str = r4.A0A;
        if (str != null) {
            r2.put(str, this);
        }
        this.A09 = r4.A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0114, code lost:
        if (r1.A00 != 0) goto L_0x0116;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(android.graphics.Canvas r23, android.graphics.Matrix r24, X.AnonymousClass06o r25, int r26, int r27) {
        /*
        // Method dump skipped, instructions count: 506
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C014006n.A00(android.graphics.Canvas, android.graphics.Matrix, X.06o, int, int):void");
    }

    public float getAlpha() {
        return ((float) this.A05) / 255.0f;
    }

    public int getRootAlpha() {
        return this.A05;
    }

    public void setAlpha(float f) {
        this.A05 = (int) (f * 255.0f);
    }

    public void setRootAlpha(int i) {
        this.A05 = i;
    }
}
