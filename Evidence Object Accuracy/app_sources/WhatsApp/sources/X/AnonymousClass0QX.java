package X;

import android.os.Build;

/* renamed from: X.0QX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QX {
    public static int A00(AnonymousClass0U4 r2, C05000Nw r3) {
        int i = r3.A00;
        if (i != 0) {
            return i;
        }
        int i2 = 255;
        if (r2 != null) {
            i2 = 15;
        }
        return r3.A05 ? i2 | 32768 : i2;
    }

    public static boolean A01(int i) {
        if (!(i == 15 || i == 255)) {
            if (i != 32768) {
                if (i == 32783) {
                    int i2 = Build.VERSION.SDK_INT;
                    if (i2 < 28 || i2 > 29) {
                        return true;
                    }
                    return false;
                } else if (i != 33023 && i != 0) {
                    return false;
                }
            } else if (Build.VERSION.SDK_INT >= 30) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
