package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

/* renamed from: X.0E4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E4 extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VJ();
    public SparseArray A00;

    public AnonymousClass0E4(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        SparseArray sparseArray = new SparseArray(readInt);
        this.A00 = sparseArray;
        for (int i = 0; i < readInt; i++) {
            sparseArray.append(iArr[i], readParcelableArray[i]);
        }
    }

    public AnonymousClass0E4(Parcelable parcelable) {
        super(parcelable);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: android.os.Parcelable[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        super.writeToParcel(parcel, i);
        SparseArray sparseArray = this.A00;
        if (sparseArray != null) {
            i2 = sparseArray.size();
        } else {
            i2 = 0;
        }
        parcel.writeInt(i2);
        int[] iArr = new int[i2];
        Parcelable[] parcelableArr = new Parcelable[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            iArr[i3] = this.A00.keyAt(i3);
            parcelableArr[i3] = this.A00.valueAt(i3);
        }
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }
}
