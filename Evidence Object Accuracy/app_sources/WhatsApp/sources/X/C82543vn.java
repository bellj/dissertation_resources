package X;

/* renamed from: X.3vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C82543vn extends AnonymousClass2KW {
    public final int A00;
    public final int A01;
    public final byte[] A02;

    public C82543vn(int i, int[] iArr, int i2) {
        super(i, i2);
        this.A01 = i;
        this.A00 = i2;
        int i3 = i * i2;
        byte[] bArr = new byte[i3];
        this.A02 = bArr;
        for (int i4 = 0; i4 < i3; i4++) {
            int i5 = iArr[i4];
            bArr[i4] = (byte) (((((i5 >> 16) & 255) + ((i5 >> 7) & 510)) + (i5 & 255)) >> 2);
        }
    }

    @Override // X.AnonymousClass2KW
    public byte[] A00() {
        int i = super.A01;
        int i2 = super.A00;
        int i3 = this.A01;
        if (i == i3 && i2 == this.A00) {
            return this.A02;
        }
        int i4 = i * i2;
        byte[] bArr = new byte[i4];
        int i5 = (0 * i3) + 0;
        if (i == i3) {
            System.arraycopy(this.A02, i5, bArr, 0, i4);
        } else {
            for (int i6 = 0; i6 < i2; i6++) {
                System.arraycopy(this.A02, i5, bArr, i6 * i, i);
                i5 += i3;
            }
        }
        return bArr;
    }

    @Override // X.AnonymousClass2KW
    public byte[] A01(byte[] bArr, int i) {
        if (i < 0 || i >= super.A00) {
            throw C12970iu.A0f("Requested row is outside the image: ".concat(String.valueOf(i)));
        }
        int i2 = super.A01;
        if (bArr == null || bArr.length < i2) {
            bArr = new byte[i2];
        }
        System.arraycopy(this.A02, ((i + 0) * this.A01) + 0, bArr, 0, i2);
        return bArr;
    }
}
