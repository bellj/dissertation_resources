package X;

import android.content.Context;

/* renamed from: X.1mA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37341mA extends AnonymousClass1OY {
    public boolean A00;

    public AbstractC37341mA(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 r1 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r2 = r1.A06;
            ((AbstractC28551Oa) this).A0L = (C14850m9) r2.A04.get();
            ((AbstractC28551Oa) this).A0P = (AnonymousClass1CY) r2.ABO.get();
            ((AbstractC28551Oa) this).A0F = (AbstractC15710nm) r2.A4o.get();
            ((AbstractC28551Oa) this).A0N = (C244415n) r2.AAg.get();
            ((AbstractC28551Oa) this).A0J = (AnonymousClass01d) r2.ALI.get();
            ((AbstractC28551Oa) this).A0K = (AnonymousClass018) r2.ANb.get();
            ((AbstractC28551Oa) this).A0M = (C22050yP) r2.A7v.get();
            ((AbstractC28551Oa) this).A0G = (AnonymousClass19I) r2.A4a.get();
            this.A0k = (C14830m7) r2.ALb.get();
            ((AnonymousClass1OY) this).A0J = (C14900mE) r2.A8X.get();
            this.A13 = (AnonymousClass13H) r2.ABY.get();
            this.A1P = (AbstractC14440lR) r2.ANe.get();
            ((AnonymousClass1OY) this).A0L = (C15570nT) r2.AAr.get();
            this.A0h = (AnonymousClass19P) r2.ACQ.get();
            ((AnonymousClass1OY) this).A0M = (C239613r) r2.AI9.get();
            ((AnonymousClass1OY) this).A0O = (C18790t3) r2.AJw.get();
            this.A0n = (C19990v2) r2.A3M.get();
            this.A10 = (AnonymousClass19M) r2.A6R.get();
            ((AnonymousClass1OY) this).A0N = (C15450nH) r2.AII.get();
            this.A0v = (C21250x7) r2.AJh.get();
            this.A0w = (C18470sV) r2.AK8.get();
            ((AnonymousClass1OY) this).A0R = (C16170oZ) r2.AM4.get();
            this.A1Q = (AnonymousClass19Z) r2.A2o.get();
            ((AnonymousClass1OY) this).A0K = (AnonymousClass18U) r2.AAU.get();
            this.A12 = (C14410lO) r2.AB3.get();
            ((AnonymousClass1OY) this).A0I = (AnonymousClass12P) r2.A0H.get();
            ((AnonymousClass1OY) this).A0a = (C21270x9) r2.A4A.get();
            this.A0s = (C20040v7) r2.AAK.get();
            this.A15 = (C17220qS) r2.ABt.get();
            ((AnonymousClass1OY) this).A0X = (C15550nR) r2.A45.get();
            ((AnonymousClass1OY) this).A0U = (C253619c) r2.AId.get();
            ((AnonymousClass1OY) this).A0Z = (C15610nY) r2.AMe.get();
            this.A1M = (C252018m) r2.A7g.get();
            this.A1A = (C17070qD) r2.AFC.get();
            this.A0t = (AnonymousClass1BK) r2.AFZ.get();
            ((AnonymousClass1OY) this).A0b = (C253318z) r2.A4B.get();
            this.A0p = (C15650ng) r2.A4m.get();
            ((AnonymousClass1OY) this).A0V = (C238013b) r2.A1Z.get();
            this.A11 = (C20710wC) r2.A8m.get();
            this.A14 = (C22910zq) r2.A9O.get();
            this.A1J = (AnonymousClass12F) r2.AJM.get();
            this.A1F = r2.A41();
            this.A1E = (AnonymousClass12V) r2.A0p.get();
            this.A1I = (C240514a) r2.AJL.get();
            this.A1O = (AnonymousClass19O) r2.ACO.get();
            this.A17 = (C26151Cf) r2.ADi.get();
            this.A1H = (C26701Em) r2.ABc.get();
            this.A0x = (AnonymousClass132) r2.ALx.get();
            ((AnonymousClass1OY) this).A0S = (C19850um) r2.A2v.get();
            this.A0y = (C21400xM) r2.ABd.get();
            this.A0z = (C15670ni) r2.AIb.get();
            this.A1N = (C23000zz) r2.ALg.get();
            ((AnonymousClass1OY) this).A0Y = (C22700zV) r2.AMN.get();
            this.A0m = (C14820m6) r2.AN3.get();
            ((AnonymousClass1OY) this).A0W = (C22640zP) r2.A3Z.get();
            this.A19 = (C22710zW) r2.AF7.get();
            ((AnonymousClass1OY) this).A0T = (AnonymousClass19Q) r2.A2u.get();
            this.A1K = (AnonymousClass1AB) r2.AKI.get();
            this.A18 = (AnonymousClass18T) r2.AE9.get();
            this.A0r = (C15600nX) r2.A8x.get();
            this.A0u = (C22440z5) r2.AG6.get();
            this.A1D = (C16630pM) r2.AIc.get();
            this.A0j = (C18640sm) r2.A3u.get();
            this.A1L = (C26671Ej) r2.AKR.get();
            this.A1G = r2.A42();
            this.A0o = (C20830wO) r2.A4W.get();
            this.A0q = (C242814x) r2.A71.get();
            this.A0d = (AnonymousClass19K) r2.AFh.get();
            this.A16 = (AnonymousClass19J) r2.ACA.get();
            this.A1R = (C237512w) r2.AAD.get();
            this.A0c = (AnonymousClass1AO) r2.AFg.get();
            this.A0l = (C17170qN) r2.AMt.get();
            this.A0i = (C17000q6) r2.ACv.get();
            this.A1B = (C21190x1) r2.A3G.get();
            this.A0f = r1.A02();
        }
    }
}
