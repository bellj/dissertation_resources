package X;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;

/* renamed from: X.4iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98594iw implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        String str6 = null;
        ArrayList arrayList = null;
        String str7 = null;
        String str8 = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 3:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case 4:
                    str3 = C95664e9.A08(parcel, readInt);
                    break;
                case 5:
                    str4 = C95664e9.A08(parcel, readInt);
                    break;
                case 6:
                    uri = (Uri) C95664e9.A07(parcel, Uri.CREATOR, readInt);
                    break;
                case 7:
                    str5 = C95664e9.A08(parcel, readInt);
                    break;
                case '\b':
                    j = C95664e9.A04(parcel, readInt);
                    break;
                case '\t':
                    str6 = C95664e9.A08(parcel, readInt);
                    break;
                case '\n':
                    arrayList = C95664e9.A0B(parcel, Scope.CREATOR, readInt);
                    break;
                case 11:
                    str7 = C95664e9.A08(parcel, readInt);
                    break;
                case '\f':
                    str8 = C95664e9.A08(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new GoogleSignInAccount(uri, str, str2, str3, str4, str5, str6, str7, str8, arrayList, i, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
