package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.3xM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83513xM extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC14670lq A01;

    public C83513xM(View view, AbstractC14670lq r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        View view = this.A00;
        view.setVisibility(8);
        view.setEnabled(true);
        view.clearAnimation();
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.setEnabled(false);
    }
}
