package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0yU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22080yU implements AbstractC22060yS {
    public final AbstractC15710nm A00;
    public final AnonymousClass00E A01 = new AnonymousClass00E(10, 1000);
    public final AbstractC14440lR A02;
    public final C002601e A03 = new C002601e(null, new AnonymousClass01N() { // from class: X.22E
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return new C454121m();
        }
    });
    public final ReferenceQueue A04 = new ReferenceQueue();
    public final ConcurrentHashMap A05 = new ConcurrentHashMap();
    public volatile Runnable A06;
    public volatile boolean A07 = true;

    public C22080yU(AbstractC15710nm r4, AbstractC14440lR r5) {
        this.A00 = r4;
        this.A02 = r5;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        this.A07 = false;
        if (this.A06 == null) {
            this.A06 = this.A02.AbK(new RunnableBRunnable0Shape8S0100000_I0_8(this, 21), "MemoryLeakReporter/onAppBackgrounded", 5000);
        }
    }

    @Override // X.AbstractC22060yS
    public void AMG() {
        this.A07 = true;
        Runnable runnable = this.A06;
        if (runnable != null) {
            this.A02.AaP(runnable);
            this.A06 = null;
        }
    }
}
