package X;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.OrientationEventListener;
import com.whatsapp.camera.OldCameraLayout;

/* renamed from: X.2ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52362ag extends OrientationEventListener {
    public int A00;
    public final /* synthetic */ OldCameraLayout A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52362ag(Context context, OldCameraLayout oldCameraLayout) {
        super(context);
        this.A01 = oldCameraLayout;
        this.A00 = oldCameraLayout.A07.getRotation();
    }

    @Override // android.view.OrientationEventListener
    public void onOrientationChanged(int i) {
        int i2;
        OldCameraLayout oldCameraLayout = this.A01;
        Display display = oldCameraLayout.A07;
        int rotation = display.getRotation();
        if (rotation != -1 && ((i2 = this.A00) != 1 ? i2 == 3 && rotation == 1 : rotation == 3)) {
            Point point = new Point();
            display.getSize(point);
            oldCameraLayout.A00(rotation, 0, 0, point.x, point.y);
        }
        this.A00 = rotation;
    }
}
