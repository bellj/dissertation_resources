package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2d1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2d1 extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final ImageView A02;
    public final ImageView A03;
    public final TextView A04;

    public AnonymousClass2d1(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        setOrientation(1);
        LayoutInflater.from(context).inflate(R.layout.wallpaper_category_view, this);
        this.A04 = C12960it.A0I(this, R.id.wallpaper_category_title);
        this.A03 = C12970iu.A0K(this, R.id.wallpaper_category_preview);
        this.A02 = C12970iu.A0K(this, R.id.wallpaper_category_preview_icon);
    }

    public void A00(Drawable drawable, Drawable drawable2, String str) {
        int i;
        this.A04.setText(str);
        this.A03.setImageDrawable(drawable);
        ImageView imageView = this.A02;
        if (drawable2 == null) {
            i = 8;
        } else {
            imageView.setImageDrawable(drawable2);
            i = 0;
        }
        imageView.setVisibility(i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    public void setPreviewScaleType(ImageView.ScaleType scaleType) {
        this.A03.setScaleType(scaleType);
    }
}
