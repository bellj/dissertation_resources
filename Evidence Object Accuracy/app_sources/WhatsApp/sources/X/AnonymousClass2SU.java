package X;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.widget.ImageView;
import com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.filter.FilterUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2SU  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2SU {
    public VideoCallParticipantView A00;
    public VideoPort A01;
    public boolean A02;
    public final AnonymousClass2A6 A03;
    public final UserJid A04;
    public final AnonymousClass5X3 A05 = new C70953c9(this);
    public final AnonymousClass4LP A06;
    public final String A07;

    public AnonymousClass2SU(AnonymousClass2A6 r4, UserJid userJid, AnonymousClass4LP r6, String str) {
        StringBuilder sb = new StringBuilder("voip/VoipActivityV2/video/");
        sb.append(str);
        sb.append("/VideoParticipantPresenter for ");
        sb.append(userJid);
        Log.i(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("voip/VoipActivityV2/video/");
        sb2.append(str);
        sb2.append("/");
        this.A07 = sb2.toString();
        this.A04 = userJid;
        this.A03 = r4;
        this.A06 = r6;
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0180  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap A00() {
        /*
        // Method dump skipped, instructions count: 410
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2SU.A00():android.graphics.Bitmap");
    }

    public Point A01(AnonymousClass1S6 r5) {
        int i;
        int i2;
        VoipActivityV2 voipActivityV2;
        OrientationViewModel orientationViewModel;
        if (!(this instanceof C60232wG)) {
            C60242wH r1 = (C60242wH) this;
            int i3 = 0;
            if (r5.A0G && (orientationViewModel = (voipActivityV2 = r1.A00).A0s) != null && orientationViewModel.A06.A01() != null && ((Number) voipActivityV2.A0s.A06.A01()).intValue() >= 0) {
                i3 = ((Number) voipActivityV2.A0s.A06.A01()).intValue() * 90;
            }
            if (((((r5.A03 * 90) - i3) + 360) % 360) % 180 != 0) {
                i = r5.A02;
                i2 = r5.A05;
            } else {
                i = r5.A05;
                i2 = r5.A02;
            }
            return new Point(i, i2);
        }
        VoipActivityV2 voipActivityV22 = ((C60232wG) this).A00;
        Point adjustedCameraPreviewSize = voipActivityV22.A1Q.getAdjustedCameraPreviewSize();
        if (adjustedCameraPreviewSize == null && r5.A04 == 6) {
            return voipActivityV22.A1Q.lastAdjustedCameraPreviewSize;
        }
        return adjustedCameraPreviewSize;
    }

    public final AnonymousClass1S6 A02() {
        CallInfo A2i = this.A06.A00.A2i();
        if (A2i != null) {
            return A2i.getInfoByJid(this.A04);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.A07);
        sb.append("VideoParticipantPresenter can not get callInfo from voip");
        Log.i(sb.toString());
        return null;
    }

    public void A03() {
        if (!(this instanceof C60232wG)) {
            C60242wH r0 = (C60242wH) this;
            UserJid userJid = r0.A04;
            if (!r0.A0A()) {
                Voip.stopVideoRenderStream(userJid.getRawString());
                Voip.setVideoDisplayPort(userJid.getRawString(), null);
                return;
            }
            return;
        }
        ((C60232wG) this).A00.A33();
    }

    public final void A04() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A07);
        sb.append("detachFromParticipantView ");
        sb.append(this.A00);
        sb.append(" for ");
        UserJid userJid = this.A04;
        sb.append(userJid);
        Log.i(sb.toString());
        VideoCallParticipantView videoCallParticipantView = this.A00;
        if (videoCallParticipantView != null && userJid.equals(videoCallParticipantView.A0O)) {
            VideoPort videoPort = this.A01;
            if (videoPort != null) {
                videoPort.setListener(null);
                this.A01 = null;
            }
            ImageView imageView = this.A00.A0I;
            if (imageView.getVisibility() == 0) {
                imageView.setImageBitmap(null);
                imageView.setVisibility(8);
            }
            this.A00.A0O = null;
            this.A00 = null;
        }
    }

    public final void A05() {
        StringBuilder sb;
        String str;
        if (this.A02) {
            CallInfo A2i = this.A06.A00.A2i();
            if (A2i == null) {
                sb = new StringBuilder();
                sb.append(this.A07);
                str = "startRenderingIfReady can not get callInfo";
            } else {
                AnonymousClass1S6 infoByJid = A2i.getInfoByJid(this.A04);
                if (infoByJid == null) {
                    sb = new StringBuilder();
                    sb.append(this.A07);
                    str = "startRenderingIfReady cancelled due to no participant info";
                } else {
                    VideoCallParticipantView videoCallParticipantView = this.A00;
                    AnonymousClass009.A03(videoCallParticipantView);
                    A07(videoCallParticipantView, A2i, infoByJid);
                    if (!(this instanceof C60232wG)) {
                        C60242wH r4 = (C60242wH) this;
                        UserJid userJid = r4.A04;
                        if (!r4.A0A()) {
                            if (Voip.setVideoDisplayPort(userJid.getRawString(), r4.A01) == 0) {
                                Voip.startVideoRenderStream(userJid.getRawString());
                            } else {
                                C29631Ua r2 = r4.A00.A0v;
                                if (r2 != null) {
                                    r2.A0m(null, null, 22);
                                }
                            }
                        }
                    } else {
                        C60232wG r42 = (C60232wG) this;
                        if (infoByJid.A04 != 6 && !A2i.isCallOnHold()) {
                            r42.A00.A3H(r42, infoByJid);
                        }
                    }
                    A09(infoByJid);
                    return;
                }
            }
            sb.append(str);
            Log.i(sb.toString());
        }
    }

    public final void A06(VideoCallParticipantView videoCallParticipantView) {
        if (videoCallParticipantView != this.A00 || !this.A04.equals(videoCallParticipantView.A0O)) {
            if (this.A00 != null) {
                A04();
            }
            StringBuilder sb = new StringBuilder();
            sb.append(this.A07);
            sb.append("attachToParticipantView ");
            sb.append(videoCallParticipantView);
            sb.append(" for ");
            UserJid userJid = this.A04;
            sb.append(userJid);
            Log.i(sb.toString());
            this.A00 = videoCallParticipantView;
            videoCallParticipantView.A0O = userJid;
            VideoPort AHY = this.A03.AHY(videoCallParticipantView);
            this.A01 = AHY;
            AHY.setListener(this.A05);
        }
    }

    public final void A07(VideoCallParticipantView videoCallParticipantView, CallInfo callInfo, AnonymousClass1S6 r7) {
        int i = r7.A04;
        if (i != 6 && !callInfo.self.A09 && !r7.A0I && i != 2) {
            ImageView imageView = videoCallParticipantView.A0I;
            if (imageView.getVisibility() == 0) {
                imageView.setImageBitmap(null);
                imageView.setVisibility(8);
            }
        } else if (!AnonymousClass4GV.A00) {
            ImageView imageView2 = videoCallParticipantView.A0I;
            if (imageView2.getVisibility() != 0) {
                Bitmap A00 = A00();
                if (A00 == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.A07);
                    sb.append("showLastFrameOverlay no bitmap");
                    Log.i(sb.toString());
                } else {
                    int width = A00.getWidth() / 40;
                    int i2 = 8;
                    if (width >= 8) {
                        i2 = 16;
                        if (width <= 16) {
                            i2 = width;
                        }
                    }
                    FilterUtils.blurNative(A00, i2, 2);
                }
                imageView2.setImageBitmap(A00);
                imageView2.setVisibility(0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0204, code lost:
        if (r12.A0C == false) goto L_0x0206;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08(com.whatsapp.voipcalling.CallInfo r11, X.AnonymousClass1S6 r12) {
        /*
        // Method dump skipped, instructions count: 577
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2SU.A08(com.whatsapp.voipcalling.CallInfo, X.1S6):void");
    }

    public final void A09(AnonymousClass1S6 r4) {
        StringBuilder sb;
        String str;
        VideoCallParticipantView videoCallParticipantView = this.A00;
        if (videoCallParticipantView == null) {
            sb = new StringBuilder();
            sb.append(this.A07);
            str = "updateLayoutParams cancelled due to no participant view";
        } else if (r4 == null && (r4 = A02()) == null) {
            sb = new StringBuilder();
            sb.append(this.A07);
            str = "updateLayoutParams cancelled due to no participant info";
        } else {
            Point A01 = A01(r4);
            if (A01 == null || A01.x == 0 || A01.y == 0) {
                sb = new StringBuilder();
                sb.append(this.A07);
                str = "updateLayoutParams cancelled due to bad video size";
            } else {
                this.A03.Afb(A01, videoCallParticipantView);
                return;
            }
        }
        sb.append(str);
        Log.i(sb.toString());
    }
}
