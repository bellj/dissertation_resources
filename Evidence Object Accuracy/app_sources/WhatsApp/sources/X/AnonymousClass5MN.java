package X;

/* renamed from: X.5MN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MN extends AnonymousClass1TM {
    public AnonymousClass5NG A00;
    public AbstractC114775Na A01;
    public AnonymousClass5N2 A02;
    public C114725Mv A03;
    public AnonymousClass5MX A04;
    public C114765Mz A05;
    public C114765Mz A06;

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r2 = new C94954co(7);
        AnonymousClass5NG r0 = this.A00;
        if (r0 != null) {
            r2.A06(r0);
        }
        r2.A06(this.A03);
        r2.A06(this.A02);
        r2.A06(this.A06);
        C114765Mz r02 = this.A05;
        if (r02 != null) {
            r2.A06(r02);
        }
        AbstractC114775Na r03 = this.A01;
        if (r03 != null) {
            r2.A06(r03);
        }
        AnonymousClass5MX r1 = this.A04;
        if (r1 != null) {
            r2.A06(new C114835Ng(r1));
        }
        return new AnonymousClass5NZ(r2);
    }

    public AnonymousClass5MN(AbstractC114775Na r5) {
        if (r5.A0B() < 3 || r5.A0B() > 7) {
            throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r5.A0B()));
        }
        int i = 0;
        if (r5.A0D(0) instanceof AnonymousClass5NG) {
            this.A00 = AnonymousClass5NG.A00(r5.A0D(0));
            i = 1;
        }
        int i2 = i + 1;
        this.A03 = C114725Mv.A00(r5.A0D(i));
        int i3 = i2 + 1;
        this.A02 = AnonymousClass5N2.A00(r5.A0D(i2));
        int i4 = i3 + 1;
        this.A06 = C114765Mz.A00(r5.A0D(i3));
        if (i4 < r5.A0B() && ((r5.A0D(i4) instanceof AnonymousClass5NA) || (r5.A0D(i4) instanceof AnonymousClass5NE) || (r5.A0D(i4) instanceof C114765Mz))) {
            i4++;
            this.A05 = C114765Mz.A00(r5.A0D(i4));
        }
        if (i4 < r5.A0B() && !(r5.A0D(i4) instanceof AnonymousClass5NU)) {
            i4++;
            this.A01 = AbstractC114775Na.A04(r5.A0D(i4));
        }
        if (i4 < r5.A0B() && (r5.A0D(i4) instanceof AnonymousClass5NU)) {
            this.A04 = AnonymousClass5MX.A01(AbstractC114775Na.A05((AnonymousClass5NU) r5.A0D(i4), true));
        }
    }
}
