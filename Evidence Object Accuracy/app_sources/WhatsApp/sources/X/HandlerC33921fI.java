package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.HashMap;

/* renamed from: X.1fI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC33921fI extends Handler {
    public final HashMap A00 = new HashMap();
    public final /* synthetic */ C22930zs A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC33921fI(Looper looper, C22930zs r3) {
        super(looper);
        this.A01 = r3;
    }

    public final void A00(AbstractC14640lm r5, boolean z) {
        if (!hasMessages(0, r5) && !hasMessages(3, r5)) {
            if (!hasMessages(2, r5)) {
                C22930zs r1 = this.A01;
                if (r1.A01.A06 && (z || hasMessages(5, r5))) {
                    r1.A04.A08(Message.obtain(null, 0, 5, 0, r5), false);
                }
            }
            removeMessages(4, r5);
            removeMessages(5, r5);
        }
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        Message message2;
        long j;
        C22930zs r8;
        int i = message.what;
        if (i == 0) {
            AbstractC14640lm r5 = (AbstractC14640lm) message.obj;
            int i2 = message.arg2;
            if (!hasMessages(1, r5) && !hasMessages(4, r5)) {
                removeMessages(0, r5);
                if (hasMessages(2, r5) || hasMessages(5, r5)) {
                    HashMap hashMap = this.A00;
                    Number number = (Number) hashMap.get(r5);
                    long currentTimeMillis = System.currentTimeMillis();
                    if (number != null && currentTimeMillis - number.longValue() > 10000) {
                        r8 = this.A01;
                        if (r8.A01.A06) {
                            if (r8.A01(r5)) {
                                r8.A04.A08(Message.obtain(null, 0, 4, i2, r5), false);
                            }
                            hashMap.put(r5, Long.valueOf(currentTimeMillis));
                            r8.A03.A00(r5);
                        }
                    }
                    removeMessages(2, r5);
                    message2 = obtainMessage(2, 0, 0, r5);
                    j = 2500;
                } else {
                    r8 = this.A01;
                    if (r8.A01.A06) {
                        if (r8.A01(r5)) {
                            r8.A04.A08(Message.obtain(null, 0, 4, i2, r5), false);
                        }
                        this.A00.put(r5, Long.valueOf(System.currentTimeMillis()));
                        r8.A03.A00(r5);
                    }
                    removeMessages(2, r5);
                    message2 = obtainMessage(2, 0, 0, r5);
                    j = 2500;
                }
            } else {
                return;
            }
        } else if (i == 1 || i == 2) {
            AbstractC14640lm r4 = (AbstractC14640lm) message.obj;
            if (!hasMessages(0, r4) && !hasMessages(3, r4)) {
                if (!hasMessages(5, r4)) {
                    C22930zs r1 = this.A01;
                    if (r1.A01.A06 && r1.A01(r4)) {
                        r1.A04.A08(Message.obtain(null, 0, 5, 0, r4), false);
                    }
                }
                removeMessages(1, r4);
                removeMessages(2, r4);
                return;
            }
            return;
        } else if (i == 3) {
            Object obj = message.obj;
            int i3 = message.arg2;
            if (!hasMessages(1, obj) && !hasMessages(4, obj)) {
                if (!hasMessages(2, obj)) {
                    C22930zs r12 = this.A01;
                    if (r12.A01.A06) {
                        r12.A04.A08(Message.obtain(null, 0, 4, i3, obj), false);
                    }
                }
                message2 = obtainMessage(5, 0, 0, obj);
                j = C26061Bw.A0L;
            } else {
                return;
            }
        } else if (i == 4) {
            A00((AbstractC14640lm) message.obj, false);
            return;
        } else if (i == 5) {
            A00((AbstractC14640lm) message.obj, true);
            return;
        } else {
            return;
        }
        sendMessageDelayed(message2, j);
    }
}
