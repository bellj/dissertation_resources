package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44H  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44H extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final C621134m A01;

    public AnonymousClass44H(SearchViewModel searchViewModel, C621134m r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
