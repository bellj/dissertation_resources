package X;

import android.view.View;

/* renamed from: X.5lh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122585lh extends AbstractC118835cS {
    public final C64853Hd A00;

    public C122585lh(View view, C64853Hd r2) {
        super(view);
        this.A00 = r2;
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        C30061Vy r2 = (C30061Vy) ((C123045mW) r4).A00;
        C64853Hd r1 = this.A00;
        r1.A03(r2, false);
        if (C30041Vv.A12(r2)) {
            r1.A01();
        } else if (C30041Vv.A13(r2)) {
            r1.A02();
        } else {
            r1.A00();
        }
    }
}
