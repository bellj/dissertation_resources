package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: X.01Y  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01Y extends C16720pe {
    public static final int A00(Iterable iterable, Object obj) {
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(obj);
        }
        int i = 0;
        for (Object obj2 : iterable) {
            if (i < 0) {
                throw new ArithmeticException("Index overflow has happened.");
            } else if (C16700pc.A0O(obj, obj2)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    public static final Object A01(List list) {
        int size = list.size();
        if (size == 0) {
            throw new NoSuchElementException("List is empty.");
        } else if (size == 1) {
            return list.get(0);
        } else {
            throw new IllegalArgumentException("List has more than one element.");
        }
    }

    public static final Object A02(List list, int i) {
        C16700pc.A0E(list, 0);
        if (i < 0 || i > list.size() - 1) {
            return null;
        }
        return list.get(i);
    }

    public static final String A03(Iterable iterable) {
        StringBuilder sb = new StringBuilder();
        A09(sb, ", ", "WindowLayoutInfo{ DisplayFeatures[", "] }", iterable);
        String obj = sb.toString();
        C16700pc.A0B(obj);
        return obj;
    }

    public static final List A05(Iterable iterable) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return A06(iterable);
            }
            Object[] array = collection.toArray(new Comparable[0]);
            if (array != null) {
                if (array.length > 1) {
                    Arrays.sort(array);
                }
                return C10980fW.A04(array);
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        }
        ArrayList arrayList = new ArrayList();
        A0A(iterable, arrayList);
        C002501a.A0C(arrayList);
        return arrayList;
    }

    public static final List A06(Iterable iterable) {
        C16700pc.A0E(iterable, 0);
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return C16770pj.A0G();
            }
            if (size != 1) {
                return A07(collection);
            }
            return C16780pk.A0K(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
        }
        ArrayList arrayList = new ArrayList();
        A0A(iterable, arrayList);
        return C16770pj.A0H(arrayList);
    }

    public static final List A07(Collection collection) {
        C16700pc.A0E(collection, 0);
        return new ArrayList(collection);
    }

    public static final Set A08(Iterable iterable) {
        C16700pc.A0E(iterable, 0);
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return C113585Id.A01();
            }
            if (size != 1) {
                LinkedHashSet linkedHashSet = new LinkedHashSet(C17540qy.A03(collection.size()));
                A0A(iterable, linkedHashSet);
                return linkedHashSet;
            }
            return C17630r8.A00(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        A0A(iterable, linkedHashSet2);
        return C113585Id.A02(linkedHashSet2);
    }

    public static final void A09(Appendable appendable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Iterable iterable) {
        CharSequence charSequence4;
        C16700pc.A0E(charSequence2, 3);
        C16700pc.A0E(charSequence3, 4);
        appendable.append(charSequence2);
        int i = 0;
        for (Object obj : iterable) {
            i++;
            if (i > 1) {
                appendable.append(charSequence);
            }
            if (obj == null || (obj instanceof CharSequence)) {
                charSequence4 = (CharSequence) obj;
            } else if (obj instanceof Character) {
                appendable.append(((Character) obj).charValue());
            } else {
                charSequence4 = String.valueOf(obj);
            }
            appendable.append(charSequence4);
        }
        appendable.append(charSequence3);
    }

    public static final void A0A(Iterable iterable, Collection collection) {
        for (Object obj : iterable) {
            collection.add(obj);
        }
    }
}
