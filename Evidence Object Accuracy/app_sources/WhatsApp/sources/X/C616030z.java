package X;

/* renamed from: X.30z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C616030z extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public String A04;
    public String A05;
    public String A06;

    public C616030z() {
        super(3548, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A04);
        r3.Abe(2, this.A05);
        r3.Abe(3, this.A06);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A02);
        r3.Abe(6, this.A01);
        r3.Abe(7, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamProductSearch {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogOwnerJid", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "searchEntryPoint", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "searchResultIndex", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "searchViewAction", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
