package X;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* renamed from: X.1jh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C36321jh {
    public static AnonymousClass5YB A00;

    public static C36331ji A00(Bitmap bitmap) {
        C13020j0.A02(bitmap, "image must not be null");
        try {
            AnonymousClass5YB r2 = A00;
            C13020j0.A02(r2, "IBitmapDescriptorFactory is not initialized");
            C65873Li r22 = (C65873Li) r2;
            Parcel A01 = r22.A01();
            C65183In.A01(A01, bitmap);
            Parcel A02 = r22.A02(6, A01);
            IObjectWrapper A012 = AbstractBinderC79573qo.A01(A02.readStrongBinder());
            A02.recycle();
            return new C36331ji(A012);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
