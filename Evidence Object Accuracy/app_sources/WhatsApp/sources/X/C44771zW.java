package X;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.chromium.net.UrlRequest;

/* renamed from: X.1zW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44771zW {
    public static final Executor A00;

    public static int A00(int i) {
        switch (i) {
            case 10:
                return 1;
            case 11:
                return 6;
            case 12:
                return 15;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return 3;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return 5;
            case 15:
                return 8;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return 9;
            case 17:
                return 10;
            case 18:
                return 11;
            case 19:
                return 12;
            case C43951xu.A01:
                return 13;
            case 21:
                return 14;
            case 22:
            case 26:
            default:
                return 2;
            case 23:
                return 16;
            case 24:
                return 18;
            case 25:
                return 17;
            case 27:
                return 25;
            case 28:
                return 26;
            case 29:
                return 27;
            case C25991Bp.A0S:
                return 28;
            case 31:
                return 29;
        }
    }

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 2, (long) 1, TimeUnit.SECONDS, new AnonymousClass2AF(), new AnonymousClass2AG("Google Drive Write Worker #"));
        threadPoolExecutor.setRejectedExecutionHandler(new AnonymousClass2AH());
        A00 = threadPoolExecutor;
    }

    public static int A01(C14820m6 r4, boolean z) {
        if (!z) {
            int A01 = r4.A01();
            if (A01 != 0) {
                int i = 1;
                if (A01 != 1) {
                    i = 2;
                    if (A01 != 2) {
                        i = 3;
                        if (A01 != 3) {
                            if (A01 != 4) {
                            }
                        }
                    }
                }
                return i;
            }
            return 0;
        }
        return 4;
    }

    public static Dialog A02(Activity activity, DialogInterface.OnCancelListener onCancelListener, int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        StringBuilder sb = new StringBuilder("gdrive-util/get-error-dialog creating dialog for ");
        sb.append(A03(i));
        Log.i(sb.toString());
        AnonymousClass2AK r4 = new DialogInterface.OnClickListener(activity, i, i2) { // from class: X.2AK
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ Activity A02;

            {
                this.A00 = r2;
                this.A02 = r1;
                this.A01 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i6) {
                PendingIntent activity2;
                int i7 = this.A00;
                Activity activity3 = this.A02;
                int i8 = this.A01;
                try {
                    Intent A01 = C471929k.A00.A01(activity3, null, i7);
                    if (A01 == null || (activity2 = PendingIntent.getActivity(activity3, i8, A01, C88384Fl.A00 | 134217728)) == null) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("gdrive-util/get-error-dialog pending intent is null for error code: ");
                        sb2.append(C44771zW.A03(i7));
                        Log.e(sb2.toString());
                        return;
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("gdrive-util/get-error-dialog starting resolution for ");
                    sb3.append(C44771zW.A03(i7));
                    Log.e(sb3.toString());
                    C56492ky r1 = new C56492ky(i7, activity2);
                    if (r1.A00()) {
                        PendingIntent pendingIntent = r1.A02;
                        C13020j0.A01(pendingIntent);
                        activity3.startIntentSenderForResult(pendingIntent.getIntentSender(), i8, null, 0, 0, 0);
                    }
                } catch (IntentSender.SendIntentException e) {
                    Log.e("gdrive-util/get-error-dialog", e);
                }
            }
        };
        C004802e r3 = new C004802e(activity);
        r3.A0B(false);
        r3.setNegativeButton(R.string.skip, new DialogInterface.OnClickListener(onCancelListener) { // from class: X.2AL
            public final /* synthetic */ DialogInterface.OnCancelListener A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i6) {
                this.A00.onCancel(dialogInterface);
            }
        });
        if (i == 0) {
            return null;
        }
        if (i == 1) {
            i3 = R.string.google_drive_error_google_play_services_are_missing_title;
            i4 = R.string.google_drive_error_google_play_services_are_missing_message_at_restore_time;
            if (z) {
                i4 = R.string.google_drive_error_google_play_services_are_missing_message_at_backup_time;
            }
            i5 = R.string.google_drive_error_google_play_services_are_missing_positive_button_label;
        } else if (i == 2) {
            i3 = R.string.google_drive_error_google_play_services_updation_required_title;
            i4 = R.string.google_drive_error_google_play_services_updation_required_message_at_restore_time;
            if (z) {
                i4 = R.string.google_drive_error_google_play_services_updation_required_message_at_backup_time;
            }
            i5 = R.string.google_drive_error_google_play_services_update_required_positive_button_label;
        } else if (i != 3) {
            if (i == 18 || (i == 1 && true == C472329r.A03(activity))) {
                i = 18;
            }
            return C471729i.A00(activity, onCancelListener, new AnonymousClass2AN(activity, C471729i.A00.A01(activity, "d", i), i2), i);
        } else {
            i3 = R.string.google_drive_error_google_play_services_disabled_title;
            i4 = R.string.google_drive_error_google_play_services_disabled_message_at_restore_time;
            if (z) {
                i4 = R.string.google_drive_error_google_play_services_disabled_message_at_backup_time;
            }
            i5 = R.string.google_drive_error_google_play_services_disabled_positive_button_label;
        }
        r3.A07(i3);
        r3.A06(i4);
        r3.setPositiveButton(i5, r4);
        return r3.create();
    }

    public static String A03(int i) {
        if (i == 0) {
            return "success";
        }
        if (i == 1) {
            return "service-missing";
        }
        if (i == 2) {
            return "service-version-update-required";
        }
        if (i == 3) {
            return "service-disabled";
        }
        if (i == 9) {
            return "service-invalid";
        }
        if (i == 18) {
            return "service-updating";
        }
        StringBuilder sb = new StringBuilder("unexpected-return-code: ");
        sb.append(i);
        return sb.toString();
    }

    public static String A04(int i) {
        switch (i) {
            case 10:
                return "none";
            case 11:
                return "auth-failed";
            case 12:
                return "account-missing";
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return "google-drive-full";
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return "google-drive-not-reachable";
            case 15:
                return "local-storage-full";
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return "local-chat-backup-missing";
            case 17:
                return "file-not-found";
            case 18:
                return "base-folder-does-not-exist";
            case 19:
                return "gdrive-servers-are-not-working-properly";
            case C43951xu.A01:
                return "failed-to-authenticate-with-whatsapp-servers";
            case 21:
                return "gps-unavailable";
            case 22:
                return "local-gdrive-file-map-is-missing";
            case 23:
                return "read-external-storage-permission-is-missing";
            case 24:
                return "backup-generated-using-newer-version-of-app";
            case 25:
                return "service-disabled";
            case 26:
                return "unknown";
            case 27:
                return "media-without-message-from-db";
            case 28:
                return "auth-failed-user-recoverable";
            case 29:
                return "auth-failed-user-recoverable-notified";
            case C25991Bp.A0S:
                return "auth-failed-user-security-exception";
            case 31:
                return "auth-failed-user-lib-exception";
            default:
                StringBuilder sb = new StringBuilder("unexpected-error-code:");
                sb.append(i);
                return sb.toString();
        }
    }

    public static String A05(int i) {
        if (i == 0) {
            return "off";
        }
        if (i == 1) {
            return "daily";
        }
        if (i == 2) {
            return "weekly";
        }
        if (i == 3) {
            return "monthly";
        }
        if (i == 4) {
            return "manual";
        }
        StringBuilder sb = new StringBuilder("Unexpected backup frequency: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static String A06(Context context, AbstractC15710nm r2, C15810nw r3, String str) {
        if (str.startsWith("_INTERNAL_FILES_") || str.startsWith("_INTERNAL_DATABASES_")) {
            return A07(context, r2, str);
        }
        return r3.A07(str).getAbsolutePath();
    }

    public static String A07(Context context, AbstractC15710nm r7, String str) {
        String replace;
        if ("_INTERNAL_DATABASES_/chatsettings.db".equals(str)) {
            StringBuilder sb = new StringBuilder("gdrive-util/convert-upload-title-to-local-path/ignored-file-skipped-from-backup ");
            sb.append(str);
            Log.i(sb.toString());
            return null;
        }
        try {
            if (str.startsWith("_INTERNAL_FILES_")) {
                replace = str.replace("_INTERNAL_FILES_", context.getFilesDir().getAbsolutePath());
            } else if (str.startsWith("_INTERNAL_DATABASES_")) {
                replace = str.replace("_INTERNAL_DATABASES_", context.getDatabasePath("dummy.db").getParentFile().getAbsolutePath());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("gdrive-util/upload-title-to-local-path/unexpected-title: ");
                sb2.append(str);
                throw new IllegalArgumentException(sb2.toString());
            }
            String canonicalPath = new File(replace).getCanonicalPath();
            Set<File> A0E = A0E(context);
            HashSet hashSet = new HashSet(A0E.size());
            for (File file : A0E) {
                hashSet.add(new File(file.getCanonicalPath()));
            }
            if (hashSet.contains(new File(canonicalPath))) {
                return canonicalPath;
            }
            StringBuilder sb3 = new StringBuilder("gdrive-util/convert-upload-title-to-local-path/local-path-unexpected/return-null ");
            sb3.append(canonicalPath);
            sb3.append(" not in [");
            sb3.append(TextUtils.join(" ", hashSet));
            sb3.append("]");
            Log.e(sb3.toString());
            r7.AaV("gdrive-util/convert-upload-title-to-local-path", "gdrive: upload title maps to invalid local path", true);
            return null;
        } catch (IOException e) {
            Log.e("gdrive-util/upload-title-to-local-path", e);
            return null;
        }
    }

    public static String A08(Context context, C15810nw r11, File file) {
        String str;
        String absolutePath = file.getAbsolutePath();
        if (r11.A0B(file)) {
            StringBuilder sb = new StringBuilder();
            sb.append(((File) r11.A03.get()).getAbsolutePath());
            sb.append("/");
            str = absolutePath.replace(sb.toString(), "");
            if (str.startsWith("_INTERNAL_FILES_") || str.startsWith("_INTERNAL_DATABASES_")) {
                StringBuilder sb2 = new StringBuilder("gdrive-util/local-path-to-upload-title/malicious-file-name: ");
                sb2.append(str);
                Log.e(sb2.toString());
                return null;
            }
        } else {
            String absolutePath2 = context.getFilesDir().getAbsolutePath();
            if (absolutePath.startsWith(absolutePath2)) {
                str = absolutePath.replace(absolutePath2, "_INTERNAL_FILES_");
            } else {
                String absolutePath3 = context.getDatabasePath("dummy.db").getParentFile().getAbsolutePath();
                if (absolutePath.startsWith(absolutePath3)) {
                    str = absolutePath.replace(absolutePath3, "_INTERNAL_DATABASES_");
                } else {
                    StringBuilder sb3 = new StringBuilder("Unexpected file: ");
                    sb3.append(absolutePath);
                    throw new IllegalArgumentException(sb3.toString());
                }
            }
            String.format(Locale.ENGLISH, "gdrive-util/local-path-to-upload-title %s -> %s", absolutePath, str);
        }
        return str;
    }

    public static String A09(C15810nw r2, C15890o4 r3, File file, long j) {
        try {
            try {
                return C14350lI.A06(file, MessageDigest.getInstance("MD5"), j);
            } catch (IOException e) {
                if (!r2.A0B(file) || r3.A07()) {
                    Log.e("gdrive-util/get-message-digest", e);
                    return null;
                }
                throw new AnonymousClass2AP(e);
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static String A0A(C44931zn r6) {
        int i;
        int i2;
        int i3;
        int i4;
        Locale locale = Locale.ENGLISH;
        Object[] objArr = new Object[9];
        Double d = r6.A08;
        if (d != null) {
            i = (int) d.doubleValue();
        } else {
            i = -1;
        }
        objArr[0] = Integer.valueOf(i);
        Double d2 = r6.A04;
        if (d2 != null) {
            i2 = (int) d2.doubleValue();
        } else {
            i2 = -1;
        }
        objArr[1] = Integer.valueOf(i2);
        Double d3 = r6.A06;
        if (d3 != null) {
            i3 = (int) d3.doubleValue();
        } else {
            i3 = -1;
        }
        objArr[2] = Integer.valueOf(i3);
        Double d4 = r6.A05;
        if (d4 != null) {
            i4 = (int) d4.doubleValue();
        } else {
            i4 = -1;
        }
        objArr[3] = Integer.valueOf(i4);
        objArr[4] = r6.A0N;
        objArr[5] = r6.A01;
        objArr[6] = r6.A0J;
        objArr[7] = r6.A0E;
        objArr[8] = r6.A0D;
        return String.format(locale, "total size:%d, chat size:%d, media size:%d, media files count:%d retryCount:%d includeVideos:%b wifi-on-finish:%b backup-stage:%d result:%d", objArr);
    }

    public static String A0B(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("@");
        if (indexOf <= 1) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str.charAt(0));
        sb.append("***");
        sb.append(str.substring(indexOf - 1));
        return sb.toString();
    }

    public static String A0C(String str, String... strArr) {
        int length = strArr.length;
        if (length == 0) {
            return str;
        }
        if (length % 2 != 0) {
            StringBuilder sb = new StringBuilder("gdrive-util/append-query-parameters/odd number of params - ");
            sb.append(length);
            Log.e(sb.toString());
        }
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        int i = 0;
        do {
            buildUpon.appendQueryParameter(strArr[i], strArr[i + 1]);
            i += 2;
        } while (i < length);
        return buildUpon.build().toString();
    }

    public static List A0D(C14330lG r4) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(r4.A04().A01);
        arrayList.add(r4.A04().A00);
        arrayList.add(r4.A04().A0N);
        arrayList.add(r4.A04().A05);
        arrayList.add(r4.A04().A0O);
        File file = r4.A04().A0P;
        C14330lG.A03(file, false);
        arrayList.add(file);
        File file2 = r4.A04().A02;
        C14330lG.A03(file2, false);
        arrayList.add(file2);
        File file3 = r4.A04().A0M;
        C14330lG.A03(file3, false);
        arrayList.add(file3);
        File file4 = r4.A04().A04;
        C14330lG.A03(file4, false);
        arrayList.add(file4);
        ArrayList arrayList2 = new ArrayList(arrayList);
        File file5 = r4.A04().A0M;
        C14330lG.A03(file5, false);
        arrayList2.remove(file5);
        return arrayList2;
    }

    public static Set A0E(Context context) {
        HashSet hashSet = new HashSet();
        hashSet.add(context.getDatabasePath("chatsettingsbackup.db"));
        hashSet.add(new File(context.getFilesDir(), "wallpaper.jpg"));
        StringBuilder sb = new StringBuilder("gdrive-util/get-internal-files-for-restore [");
        sb.append(TextUtils.join(", ", hashSet));
        sb.append("]");
        Log.i(sb.toString());
        return hashSet;
    }

    public static boolean A0F(C22730zY r9, File file, List list) {
        if (r9 == null || r9.A0D(86400000)) {
            LinkedList linkedList = new LinkedList();
            linkedList.add(file);
            while (linkedList.peek() != null) {
                Object poll = linkedList.poll();
                AnonymousClass009.A05(poll);
                File file2 = (File) poll;
                if (!file2.exists()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("gdrive-util/get-files-in-folder/does-not-exist ");
                    sb.append(file2.getAbsolutePath());
                    Log.w(sb.toString());
                } else if (file2.isDirectory()) {
                    File[] listFiles = file2.listFiles();
                    if (listFiles != null) {
                        for (File file3 : listFiles) {
                            if (!file3.exists()) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("gdrive-util/get-files-in-folder/does-not-exist ");
                                sb2.append(file3.getAbsolutePath());
                                Log.w(sb2.toString());
                            } else if (file3.isDirectory()) {
                                linkedList.add(file3);
                            } else if (file3.length() > 0) {
                                list.add(file3);
                            }
                        }
                    }
                } else if (file2.length() > 0) {
                    list.add(file2);
                }
                list.size();
            }
            return true;
        }
        StringBuilder sb3 = new StringBuilder("gdrive-util/get-files-in-folder/timeout ");
        sb3.append(file.getAbsolutePath());
        Log.e(sb3.toString());
        return false;
    }

    public static boolean A0G(C14820m6 r2) {
        return r2.A00.getInt("gdrive_state", 0) == 1;
    }

    public static boolean A0H(C14820m6 r2) {
        if (r2.A00.getInt("gdrive_state", 0) == 2) {
            return true;
        }
        return false;
    }

    public static boolean A0I(C14820m6 r2) {
        if (r2.A00.getInt("gdrive_state", 0) == 3) {
            return true;
        }
        return false;
    }

    public static boolean A0J(C14820m6 r2) {
        boolean z = r2.A00.getBoolean("new_jid", false);
        StringBuilder sb = new StringBuilder("gdrive-util/is-new-jid/");
        sb.append(z);
        Log.i(sb.toString());
        return z;
    }

    public static boolean A0K(C14820m6 r7, long j) {
        String str;
        int A01 = r7.A01();
        long A07 = r7.A07(r7.A09());
        if (A07 > j) {
            StringBuilder sb = new StringBuilder("gdrive-util/should-backup/last-backup-timestamp-is-in-future/");
            sb.append(A07);
            sb.append(" , ignoring it");
            Log.i(sb.toString());
            A07 = -1;
        }
        if (A01 == 0) {
            str = "gdrive-util/should-backup/frequency/none";
        } else if (A01 == 1) {
            return true;
        } else {
            if (A01 != 2) {
                if (A01 == 3) {
                    long j2 = j - A07;
                    if (j2 >= 2505600000L) {
                        return true;
                    }
                    str = String.format(Locale.ENGLISH, "gdrive-util/should-backup/frequency/monthly its only %d days since the last successful backup.", Long.valueOf(j2 / 86400000));
                } else if (A01 != 4) {
                    StringBuilder sb2 = new StringBuilder("gdrive-util/should-backup frequency has unexpected value: ");
                    sb2.append(A01);
                    sb2.append(", no auto backups will be performed.");
                    Log.e(sb2.toString());
                    return false;
                } else {
                    str = "gdrive-util/should-backup/frequency/manual";
                }
            } else if (j - A07 >= 518400000) {
                return true;
            } else {
                str = "gdrive-util/should-backup/frequency/weekly its not 7 days since the last successful backup.";
            }
        }
        Log.i(str);
        return false;
    }

    public static boolean A0L(C15880o3 r12, File file, String str, boolean z) {
        String str2;
        EnumC16570pG A002;
        C44641zJ r0;
        if (file == null || !file.exists() || !z) {
            str2 = "gdrive-util/validate local msgstore does not exist or is unusable";
        } else if (file.length() == 0) {
            str2 = "gdrive-util/validate local msgstore exists but is empty.";
        } else {
            if (!(str == null || (A002 = C32811cm.A00(file.getName())) == null)) {
                try {
                    AnonymousClass15D r11 = r12.A0X;
                    C15570nT r2 = r12.A06;
                    C19490uC r7 = r12.A0G;
                    C15820nx r5 = r12.A08;
                    C17050qB r6 = r12.A0A;
                    C16600pJ r9 = r12.A0W;
                    AbstractC33251dh A003 = C33231df.A00(r2, new C33211dd(file), r12.A07, r5, r6, r7, r12.A0R, r9, A002, r11);
                    if (!(A003 instanceof AnonymousClass2AU)) {
                        AnonymousClass2AW r4 = (AnonymousClass2AW) A003;
                        if (r4.A00 == null) {
                            try {
                                InputStream A09 = r4.A09();
                                try {
                                    AnonymousClass2AX A07 = r4.A07(A09, false);
                                    if (A07 != null) {
                                        if (!(A07 instanceof AnonymousClass2AZ)) {
                                            if (A07 instanceof C47332Af) {
                                                r0 = ((C47332Af) A07).A00;
                                            }
                                            A09.close();
                                        } else {
                                            r0 = ((AnonymousClass2AZ) A07).A00;
                                        }
                                        if (C32781cj.A0E(r0, str)) {
                                            A09.close();
                                        }
                                        A09.close();
                                    } else {
                                        throw new IOException("No prefix found");
                                    }
                                } catch (Throwable th) {
                                    try {
                                        A09.close();
                                    } catch (Throwable unused) {
                                    }
                                    throw th;
                                }
                            } catch (AnonymousClass03G e) {
                                throw new IOException("failed to read prefix", e);
                            }
                        }
                        C47352Ah A06 = r4.A06();
                        if (A06 != null && A06.A02(str)) {
                        }
                    }
                } catch (IOException e2) {
                    Log.e("msgstore/has-jid-mismatch/failed to read backup footer", e2);
                }
                if (r12.A07.A04(file)) {
                    return true;
                }
                str2 = "gdrive-util/the backup is not supported.";
            }
            str2 = "gdrive-util/validate local msgstore exists but for a different jid.";
        }
        Log.i(str2);
        return false;
    }

    public static boolean A0M(C14850m9 r2) {
        return Build.VERSION.SDK_INT >= 30 && r2.A07(603);
    }
}
