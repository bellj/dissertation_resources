package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;

/* renamed from: X.1BT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BT extends C22220yj {
    public final C002701f A00;
    public final C235512c A01;
    public final AbstractC14440lR A02;

    public AnonymousClass1BT(C002701f r2, AnonymousClass1BV r3, C235512c r4, AbstractC14440lR r5) {
        super(r3, 30);
        this.A02 = r5;
        this.A01 = r4;
        this.A00 = r2;
    }

    @Override // X.C22220yj
    public void A08(int i) {
        Object A01 = A01(i);
        if (A01 instanceof C470828w) {
            this.A02.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 48, A01));
        }
        super.A08(i);
    }

    @Override // X.C22220yj
    public /* bridge */ /* synthetic */ void A09(AbstractC38871oq r5) {
        AnonymousClass577 r52 = (AnonymousClass577) r5;
        super.A09(r52);
        AbstractC470728v r3 = r52.A01;
        if (r3 instanceof C470828w) {
            this.A02.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 47, r3));
        }
    }

    /* renamed from: A0A */
    public boolean A07(AbstractC470728v r2) {
        if (r2.A6v()) {
            return super.A07(r2);
        }
        return false;
    }
}
