package X;

import android.content.Context;

/* renamed from: X.5fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120295fw extends AnonymousClass24Y {
    public final C15570nT A00;

    public C120295fw(Context context, C15570nT r2, C17070qD r3) {
        super(context, r3);
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e  */
    @Override // X.AnonymousClass24Y
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.PendingIntent A00(android.content.Context r6, X.AbstractC28901Pl r7, java.lang.String r8) {
        /*
            r5 = this;
            if (r7 == 0) goto L_0x000f
            X.1ZY r0 = r7.A08
            if (r0 == 0) goto L_0x000f
            int r0 = r8.hashCode()
            r4 = 1
            r3 = 0
            switch(r0) {
                case -945151213: goto L_0x0032;
                case -863506419: goto L_0x0028;
                case -188177059: goto L_0x001e;
                case 1084491615: goto L_0x0014;
                default: goto L_0x000f;
            }
        L_0x000f:
            android.app.PendingIntent r0 = super.A00(r6, r7, r8)
            return r0
        L_0x0014:
            java.lang.String r0 = "MERCHANT_VERIFIED"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x000f
            r0 = 3
            goto L_0x003b
        L_0x001e:
            java.lang.String r0 = "MERCHANT_VERIFICATION_FAILURE"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x000f
            r0 = 2
            goto L_0x003b
        L_0x0028:
            java.lang.String r0 = "PAYMENT_METHOD_VERIFIED"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x000f
            r0 = 1
            goto L_0x003b
        L_0x0032:
            java.lang.String r0 = "MERCHANT_DISABLED"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x000f
            r0 = 0
        L_0x003b:
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            switch(r0) {
                case 0: goto L_0x004e;
                case 1: goto L_0x0041;
                case 2: goto L_0x004e;
                case 3: goto L_0x004e;
                default: goto L_0x0040;
            }
        L_0x0040:
            goto L_0x000f
        L_0x0041:
            java.lang.Class<com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity> r0 = com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r6, r0)
            X.C117315Zl.A0M(r1, r7)
            r1.addFlags(r2)
            goto L_0x005e
        L_0x004e:
            java.lang.Class<com.whatsapp.payments.ui.BusinessHubActivity> r0 = com.whatsapp.payments.ui.BusinessHubActivity.class
            android.content.Intent r1 = X.C12990iw.A0D(r6, r0)
            r0 = 335544320(0x14000000, float:6.4623485E-27)
            r1.addFlags(r0)
            java.lang.String r0 = "extra_force_get_methods"
            r1.putExtra(r0, r4)
        L_0x005e:
            android.app.PendingIntent r0 = X.AnonymousClass1UY.A00(r6, r3, r1, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120295fw.A00(android.content.Context, X.1Pl, java.lang.String):android.app.PendingIntent");
    }

    @Override // X.AnonymousClass24Y
    public String A01(AbstractC28901Pl r3, AnonymousClass1V8 r4) {
        int A04 = r3.A04();
        if (!(A04 == 1 || A04 == 4)) {
            if (A04 == 5) {
                C119785f6 r0 = (C119785f6) r3.A08;
                if (r0 != null) {
                    return r0.A03;
                }
                return null;
            } else if (!(A04 == 6 || A04 == 7)) {
                return null;
            }
        }
        C119775f5 r02 = (C119775f5) r3.A08;
        if (r02 != null) {
            return r02.A05;
        }
        return null;
    }

    @Override // X.AnonymousClass24Y
    public String A02(AbstractC28901Pl r2, String str) {
        if (str == null) {
            return super.A02(r2, str);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0082  */
    @Override // X.AnonymousClass24Y
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A03(X.AbstractC28901Pl r7, java.lang.String r8) {
        /*
            r6 = this;
            int r0 = r8.hashCode()
            r5 = 1
            r4 = 0
            switch(r0) {
                case -945151213: goto L_0x0032;
                case -863506419: goto L_0x0014;
                case -188177059: goto L_0x0011;
                case 1084491615: goto L_0x000e;
                default: goto L_0x0009;
            }
        L_0x0009:
            java.lang.String r0 = super.A03(r7, r8)
            return r0
        L_0x000e:
            java.lang.String r0 = "MERCHANT_VERIFIED"
            goto L_0x0034
        L_0x0011:
            java.lang.String r0 = "MERCHANT_VERIFICATION_FAILURE"
            goto L_0x0034
        L_0x0014:
            java.lang.String r0 = "PAYMENT_METHOD_VERIFIED"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0009
            boolean r0 = r7 instanceof X.C30881Ze
            if (r0 == 0) goto L_0x0009
            X.1Ze r7 = (X.C30881Ze) r7
            android.content.Context r3 = r6.A00
            r2 = 2131886705(0x7f120271, float:1.9407996E38)
            java.lang.Object[] r1 = new java.lang.Object[r5]
            java.lang.String r0 = X.C1311161i.A05(r3, r7)
            java.lang.String r0 = X.C12960it.A0X(r3, r0, r1, r4, r2)
            return r0
        L_0x0032:
            java.lang.String r0 = "MERCHANT_DISABLED"
        L_0x0034:
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0009
            boolean r0 = r7 instanceof X.AnonymousClass1ZW
            if (r0 == 0) goto L_0x0009
            X.1ZY r0 = r7.A08
            X.1ZX r0 = (X.AnonymousClass1ZX) r0
            if (r0 == 0) goto L_0x0082
            java.lang.String r3 = r0.A02
        L_0x0046:
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x0052
            X.0nT r0 = r6.A00
            java.lang.String r3 = r0.A05()
        L_0x0052:
            java.lang.String r0 = "MERCHANT_VERIFIED"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0066
            android.content.Context r2 = r6.A00
            r1 = 2131886704(0x7f120270, float:1.9407994E38)
        L_0x005f:
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.String r0 = X.C12960it.A0X(r2, r3, r0, r4, r1)
            return r0
        L_0x0066:
            java.lang.String r0 = "MERCHANT_VERIFICATION_FAILURE"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0074
            android.content.Context r2 = r6.A00
            r1 = 2131886703(0x7f12026f, float:1.9407992E38)
            goto L_0x005f
        L_0x0074:
            java.lang.String r0 = "MERCHANT_DISABLED"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0085
            android.content.Context r2 = r6.A00
            r1 = 2131886702(0x7f12026e, float:1.940799E38)
            goto L_0x005f
        L_0x0082:
            java.lang.String r3 = ""
            goto L_0x0046
        L_0x0085:
            java.lang.String r0 = ""
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120295fw.A03(X.1Pl, java.lang.String):java.lang.String");
    }
}
