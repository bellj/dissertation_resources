package X;

import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3eK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72293eK extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ UserJid $bizJid;
    public final /* synthetic */ List $catalogCategoryGroups;
    public final /* synthetic */ Map $categoryParentToChildItemMap;
    public final /* synthetic */ CatalogCategoryGroupsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72293eK(CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel, UserJid userJid, List list, Map map) {
        super(1);
        this.this$0 = catalogCategoryGroupsViewModel;
        this.$catalogCategoryGroups = list;
        this.$categoryParentToChildItemMap = map;
        this.$bizJid = userJid;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AnonymousClass4K6 r9 = (AnonymousClass4K6) obj;
        C16700pc.A0E(r9, 0);
        if (r9 instanceof C60262wN) {
            this.this$0.A03.A0A(Boolean.FALSE);
            Map map = ((C60262wN) r9).A01;
            Map map2 = this.$categoryParentToChildItemMap;
            UserJid userJid = this.$bizJid;
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                Iterable<AnonymousClass4SX> iterable = (Iterable) A15.getValue();
                Object key = A15.getKey();
                ArrayList A0E = C16760pi.A0E(iterable);
                for (AnonymousClass4SX r1 : iterable) {
                    A0E.add(new AnonymousClass2wR(r1, userJid));
                }
                map2.put(key, A0E);
            }
            C12990iw.A0P(this.this$0.A08).A0A(new AnonymousClass411(this.$catalogCategoryGroups, this.$categoryParentToChildItemMap));
        } else if (r9 instanceof C849440n) {
            this.this$0.A03.A0A(Boolean.TRUE);
        }
        return AnonymousClass1WZ.A00;
    }
}
