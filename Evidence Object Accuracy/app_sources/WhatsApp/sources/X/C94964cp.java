package X;

import java.lang.reflect.Method;
import java.util.HashMap;

/* renamed from: X.4cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94964cp {
    public static String A07 = AbstractC95364da.class.getName().replace('.', '/');
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final HashMap A04 = C12970iu.A11();
    public final AnonymousClass49K A05;
    public final C93734ae[] A06;

    public C94964cp(Class cls, AnonymousClass49K r6, C93734ae[] r7) {
        String concat;
        this.A06 = r7;
        this.A05 = r6;
        String name = cls.getName();
        this.A02 = name;
        if (name.startsWith("java.")) {
            StringBuilder A0k = C12960it.A0k("net.minidev.asm.");
            A0k.append(name);
            concat = C12960it.A0d("AccAccess", A0k);
        } else {
            concat = name.concat("AccAccess");
        }
        this.A00 = concat;
        this.A01 = concat.replace('.', '/');
        this.A03 = name.replace('.', '/');
    }

    public static final void A00(C94464br r5, AnonymousClass4YT r6, int i) {
        r6.A02(21, 2);
        if (i == 0) {
            r6.A09(r5, 154);
            return;
        }
        if (i == 1) {
            r6.A00(4);
        } else if (i == 2) {
            r6.A00(5);
        } else {
            int i2 = 6;
            if (i != 3) {
                if (i == 4) {
                    i2 = 7;
                } else if (i == 5) {
                    i2 = 8;
                } else if (i >= 6) {
                    r6.A01(16, i);
                } else {
                    throw C12990iw.A0m("non supported negative values");
                }
            }
            r6.A00(i2);
        }
        r6.A09(r5, 160);
    }

    public final void A01(C93734ae r19, AnonymousClass4YT r20) {
        C94464br r5;
        int i;
        boolean z;
        String str;
        String str2;
        String str3;
        r20.A02(25, 1);
        String str4 = this.A03;
        r20.A04(192, str4);
        r20.A02(25, 3);
        Class cls = r19.A00;
        C95574dz A02 = C95574dz.A02(cls);
        String replace = cls.getName().replace('.', '/');
        Method method = (Method) this.A04.get(cls);
        if (method != null) {
            r20.A07(method.getDeclaringClass().getName().replace('.', '/'), method.getName(), C95574dz.A01(method), 184, false);
        } else {
            if (cls.isEnum()) {
                r5 = new C94464br();
                r20.A09(r5, 198);
                r20.A02(25, 3);
                z = false;
                r20.A07("java/lang/Object", "toString", "()Ljava/lang/String;", 182, false);
                i = 184;
                StringBuilder A0k = C12960it.A0k("(Ljava/lang/String;)L");
                A0k.append(replace);
                str3 = C12960it.A0d(";", A0k);
                str2 = "valueOf";
                str = replace;
            } else if (cls.equals(String.class)) {
                r5 = new C94464br();
                r20.A09(r5, 198);
                r20.A02(25, 3);
                i = 182;
                z = false;
                str = "java/lang/Object";
                str2 = "toString";
                str3 = "()Ljava/lang/String;";
            } else {
                r20.A04(192, replace);
            }
            r20.A07(str, str2, str3, i, z);
            r20.A02(58, 3);
            r20.A08(r5);
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            r20.A0B(null, 3, null, 0, i2);
            r20.A02(25, 1);
            r20.A04(192, str4);
            r20.A02(25, 3);
            r20.A04(192, replace);
        }
        Method method2 = r19.A04;
        if (method2 == null) {
            r20.A06(str4, r19.A01, A02.A06(), 181);
        } else {
            r20.A07(str4, method2.getName(), C95574dz.A01(method2), 182, false);
        }
        r20.A00(177);
    }

    public final void A02(AnonymousClass4YT r14) {
        String replace = NoSuchFieldException.class.getName().replace('.', '/');
        r14.A04(187, replace);
        r14.A00(89);
        StringBuilder A0k = C12960it.A0k("mapping ");
        A0k.append(this.A02);
        r14.A05(C12960it.A0d(" failed to map field:", A0k));
        r14.A02(21, 2);
        r14.A07("java/lang/Integer", "toString", "(I)Ljava/lang/String;", 184, false);
        r14.A07("java/lang/String", "concat", "(Ljava/lang/String;)Ljava/lang/String;", 182, false);
        r14.A07(replace, "<init>", "(Ljava/lang/String;)V", 183, false);
        r14.A00(191);
    }

    public final void A03(AnonymousClass4YT r14) {
        String replace = NoSuchFieldException.class.getName().replace('.', '/');
        r14.A04(187, replace);
        r14.A00(89);
        StringBuilder A0k = C12960it.A0k("mapping ");
        A0k.append(this.A02);
        r14.A05(C12960it.A0d(" failed to map field:", A0k));
        r14.A02(25, 2);
        r14.A07("java/lang/String", "concat", "(Ljava/lang/String;)Ljava/lang/String;", 182, false);
        r14.A07(replace, "<init>", "(Ljava/lang/String;)V", 183, false);
        r14.A00(191);
    }
}
