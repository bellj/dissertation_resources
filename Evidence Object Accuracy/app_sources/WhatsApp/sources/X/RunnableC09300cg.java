package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09300cg implements Runnable {
    public final /* synthetic */ RecyclerView A00;

    public RunnableC09300cg(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // java.lang.Runnable
    public void run() {
        RecyclerView recyclerView = this.A00;
        if (recyclerView.A0g && !recyclerView.isLayoutRequested()) {
            if (!recyclerView.A0j) {
                recyclerView.requestLayout();
            } else if (recyclerView.A0m) {
                recyclerView.A0n = true;
            } else {
                recyclerView.A0G();
            }
        }
    }
}
