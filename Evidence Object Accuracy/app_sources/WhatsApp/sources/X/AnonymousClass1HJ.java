package X;

/* renamed from: X.1HJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HJ implements AbstractC22800zf {
    public final C16240og A00;
    public final C14820m6 A01;
    public final C14850m9 A02;
    public final C17220qS A03;
    public final C21280xA A04;

    public AnonymousClass1HJ(C16240og r1, C14820m6 r2, C14850m9 r3, C17220qS r4, C21280xA r5) {
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x011e, code lost:
        if ((r9 - r2.A05.A05[r2.A02].A04) > r6) goto L_0x0120;
     */
    @Override // X.AbstractC22800zf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AbY(X.AnonymousClass1N8 r15, boolean r16) {
        /*
        // Method dump skipped, instructions count: 346
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HJ.AbY(X.1N8, boolean):void");
    }

    @Override // X.AbstractC22800zf
    public void AbZ(AnonymousClass1N8 r3, int i, boolean z, boolean z2) {
        throw new UnsupportedOperationException("fieldstatssender/send should not call send method with dithered info");
    }
}
