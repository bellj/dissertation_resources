package X;

import android.view.ViewTreeObserver;
import com.whatsapp.greenalert.GreenAlertActivity;

/* renamed from: X.4nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101574nk implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GreenAlertActivity A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101574nk(GreenAlertActivity greenAlertActivity) {
        this.A00 = greenAlertActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        GreenAlertActivity greenAlertActivity = this.A00;
        C12980iv.A1E(greenAlertActivity.A06, this);
        greenAlertActivity.A2f();
    }
}
