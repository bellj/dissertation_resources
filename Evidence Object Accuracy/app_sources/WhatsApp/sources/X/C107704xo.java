package X;

import java.util.List;

/* renamed from: X.4xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107704xo implements AbstractC116805Wy {
    public final List A00;

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        return 0;
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return 1;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        return -1;
    }

    public C107704xo(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        return this.A00;
    }
}
