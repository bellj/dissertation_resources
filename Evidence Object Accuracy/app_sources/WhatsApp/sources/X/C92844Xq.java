package X;

/* renamed from: X.4Xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92844Xq {
    public final Object A00;
    public final Object A01;
    public final Throwable A02;
    public final AnonymousClass1J7 A03;
    public final AbstractC114125Kh A04;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92844Xq) {
                C92844Xq r5 = (C92844Xq) obj;
                if (!C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A04, r5.A04) || !C16700pc.A0O(this.A03, r5.A03) || !C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A02, r5.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    public C92844Xq(Object obj, Object obj2, Throwable th, AnonymousClass1J7 r4, AbstractC114125Kh r5) {
        this.A01 = obj;
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = obj2;
        this.A02 = th;
    }

    public int hashCode() {
        int i = 0;
        int A0D = ((((((C72453ed.A0D(this.A01) * 31) + C72453ed.A0D(this.A04)) * 31) + C72453ed.A0D(this.A03)) * 31) + C72453ed.A0D(this.A00)) * 31;
        Throwable th = this.A02;
        if (th != null) {
            i = th.hashCode();
        }
        return A0D + i;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CompletedContinuation(result=");
        A0k.append(this.A01);
        A0k.append(", cancelHandler=");
        A0k.append(this.A04);
        A0k.append(", onCancellation=");
        A0k.append(this.A03);
        A0k.append(", idempotentResume=");
        A0k.append(this.A00);
        A0k.append(", cancelCause=");
        A0k.append(this.A02);
        return C12970iu.A0u(A0k);
    }
}
