package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.3IR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IR {
    public static final Charset A05 = Charset.forName(DefaultCrypto.UTF_8);
    public final String A00;
    public final ByteBuffer A01;
    public final AtomicReferenceArray A02;
    public final AtomicReferenceArray A03;
    public final C90774Pe[] A04;

    public AnonymousClass3IR(String str, ByteBuffer byteBuffer, AtomicReferenceArray atomicReferenceArray, AtomicReferenceArray atomicReferenceArray2, C90774Pe[] r5) {
        this.A01 = byteBuffer;
        this.A04 = r5;
        this.A00 = str;
        this.A02 = atomicReferenceArray;
        this.A03 = atomicReferenceArray2;
    }

    public C92344Vn A00(int i) {
        AtomicReferenceArray atomicReferenceArray = this.A02;
        C92344Vn r4 = (C92344Vn) atomicReferenceArray.get(i);
        if (r4 == null) {
            int i2 = this.A04[0].A02 + (i << 4);
            ByteBuffer byteBuffer = this.A01;
            int i3 = byteBuffer.getInt(i2);
            int i4 = byteBuffer.getInt(i2 + 4);
            short s = byteBuffer.getShort(i2 + 8);
            byteBuffer.getShort(i2 + 10);
            r4 = new C92344Vn(this, i3, i4, s);
            if (!atomicReferenceArray.compareAndSet(i, null, r4)) {
                return (C92344Vn) atomicReferenceArray.get(i);
            }
        }
        return r4;
    }

    public Object A01(int i) {
        AtomicReferenceArray atomicReferenceArray = this.A03;
        Object obj = atomicReferenceArray.get(i);
        if (obj == null) {
            int i2 = this.A04[1].A02 + (i << 4);
            ByteBuffer byteBuffer = this.A01;
            int i3 = byteBuffer.getInt(i2);
            int i4 = byteBuffer.getInt(i2 + 4);
            byteBuffer.getInt(i2 + 8);
            byteBuffer.getInt();
            ByteBuffer duplicate = byteBuffer.duplicate();
            duplicate.position(i3);
            duplicate.limit(i3 + i4);
            byte[] bArr = new byte[duplicate.remaining()];
            duplicate.get(bArr);
            obj = new String(bArr, A05);
            if (!atomicReferenceArray.compareAndSet(i, null, obj)) {
                return atomicReferenceArray.get(i);
            }
        }
        return obj;
    }
}
