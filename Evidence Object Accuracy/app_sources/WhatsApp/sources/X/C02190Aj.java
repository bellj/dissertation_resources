package X;

import android.graphics.PointF;
import android.util.Property;
import android.view.View;

/* renamed from: X.0Aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02190Aj extends Property {
    public C02190Aj() {
        super(PointF.class, "bottomRight");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return null;
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        View view = (View) obj;
        PointF pointF = (PointF) obj2;
        AnonymousClass0U3.A04.A06(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
    }
}
