package X;

import X.AbstractC001200n;
import X.AnonymousClass052;
import X.AnonymousClass05J;
import X.AnonymousClass05K;
import X.AnonymousClass06Y;
import X.AnonymousClass074;
import X.C06830Vg;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.activity.result.ActivityResultRegistry$1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/* renamed from: X.052  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass052 {
    public ArrayList A00 = new ArrayList();
    public Random A01 = new Random();
    public final Bundle A02 = new Bundle();
    public final Map A03 = new HashMap();
    public final Map A04 = new HashMap();
    public final Map A05 = new HashMap();
    public final Map A06 = new HashMap();
    public final transient Map A07 = new HashMap();
    public final /* synthetic */ ActivityC001000l A08;

    public AnonymousClass052(ActivityC001000l r2) {
        this.A08 = r2;
    }

    public final int A00(String str) {
        Map map = this.A04;
        Number number = (Number) map.get(str);
        if (number != null) {
            return number.intValue();
        }
        int nextInt = this.A01.nextInt(2147418112);
        while (true) {
            int i = nextInt + 65536;
            Map map2 = this.A06;
            Integer valueOf = Integer.valueOf(i);
            if (map2.containsKey(valueOf)) {
                nextInt = this.A01.nextInt(2147418112);
            } else {
                map2.put(valueOf, str);
                map.put(str, valueOf);
                return i;
            }
        }
    }

    public final AnonymousClass05L A01(AnonymousClass05J r7, AnonymousClass05K r8, AbstractC001200n r9, String str) {
        AbstractC009904y ADr = r9.ADr();
        C009804x r2 = (C009804x) ADr;
        if (r2.A02.compareTo(AnonymousClass05I.STARTED) >= 0) {
            StringBuilder sb = new StringBuilder("LifecycleOwner ");
            sb.append(r9);
            sb.append(" is attempting to register while current state is ");
            sb.append(r2.A02);
            sb.append(". LifecycleOwners must call register before they are STARTED.");
            throw new IllegalStateException(sb.toString());
        }
        int A00 = A00(str);
        Map map = this.A03;
        C04680Mq r22 = (C04680Mq) map.get(str);
        if (r22 == null) {
            r22 = new C04680Mq(ADr);
        }
        ActivityResultRegistry$1 activityResultRegistry$1 = new AnonymousClass054(r7, r8, str) { // from class: androidx.activity.result.ActivityResultRegistry$1
            public final /* synthetic */ AnonymousClass05J A01;
            public final /* synthetic */ AnonymousClass05K A02;
            public final /* synthetic */ String A03;

            {
                this.A03 = r4;
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass054
            public void AWQ(AnonymousClass074 r72, AbstractC001200n r82) {
                if (AnonymousClass074.ON_START.equals(r72)) {
                    AnonymousClass052 r5 = AnonymousClass052.this;
                    Map map2 = r5.A07;
                    String str2 = this.A03;
                    AnonymousClass05J r3 = this.A01;
                    AnonymousClass05K r23 = this.A02;
                    map2.put(str2, new AnonymousClass06Y(r3, r23));
                    Map map3 = r5.A05;
                    if (map3.containsKey(str2)) {
                        Object obj = map3.get(str2);
                        map3.remove(str2);
                        r3.ALs(obj);
                    }
                    Bundle bundle = r5.A02;
                    C06830Vg r0 = (C06830Vg) bundle.getParcelable(str2);
                    if (r0 != null) {
                        bundle.remove(str2);
                        r3.ALs(r23.A02(r0.A01, r0.A00));
                    }
                } else if (AnonymousClass074.ON_STOP.equals(r72)) {
                    AnonymousClass052.this.A07.remove(this.A03);
                } else if (AnonymousClass074.ON_DESTROY.equals(r72)) {
                    AnonymousClass052.this.A05(this.A03);
                }
            }
        };
        r22.A00.A00(activityResultRegistry$1);
        r22.A01.add(activityResultRegistry$1);
        map.put(str, r22);
        return new AnonymousClass0C3(this, r8, str, A00);
    }

    public final AnonymousClass05L A02(AnonymousClass05J r4, AnonymousClass05K r5, String str) {
        int A00 = A00(str);
        this.A07.put(str, new AnonymousClass06Y(r4, r5));
        Map map = this.A05;
        if (map.containsKey(str)) {
            Object obj = map.get(str);
            map.remove(str);
            r4.ALs(obj);
        }
        Bundle bundle = this.A02;
        C06830Vg r0 = (C06830Vg) bundle.getParcelable(str);
        if (r0 != null) {
            bundle.remove(str);
            r4.ALs(r5.A02(r0.A01, r0.A00));
        }
        return new AnonymousClass06Z(this, r5, str, A00);
    }

    public final void A03(Bundle bundle) {
        ArrayList<Integer> integerArrayList = bundle.getIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS");
        ArrayList<String> stringArrayList = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS");
        if (!(stringArrayList == null || integerArrayList == null)) {
            this.A00 = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS");
            this.A01 = (Random) bundle.getSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT");
            Bundle bundle2 = this.A02;
            bundle2.putAll(bundle.getBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT"));
            for (int i = 0; i < stringArrayList.size(); i++) {
                String str = stringArrayList.get(i);
                Map map = this.A04;
                if (map.containsKey(str)) {
                    Object remove = map.remove(str);
                    if (!bundle2.containsKey(str)) {
                        this.A06.remove(remove);
                    }
                }
                int intValue = integerArrayList.get(i).intValue();
                String str2 = stringArrayList.get(i);
                Map map2 = this.A06;
                Integer valueOf = Integer.valueOf(intValue);
                map2.put(valueOf, str2);
                map.put(str2, valueOf);
            }
        }
    }

    public void A04(AnonymousClass05K r12, Object obj, int i) {
        ActivityC001000l r3 = this.A08;
        AnonymousClass0MP A01 = r12.A01(r3, obj);
        if (A01 != null) {
            new Handler(Looper.getMainLooper()).post(new RunnableC09750dS(this, A01, i));
            return;
        }
        Intent A00 = r12.A00(r3, obj);
        Bundle bundle = null;
        if (A00.getExtras() != null && A00.getExtras().getClassLoader() == null) {
            A00.setExtrasClassLoader(r3.getClassLoader());
        }
        if (A00.hasExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) {
            bundle = A00.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
            A00.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
        }
        if ("androidx.activity.result.contract.action.REQUEST_PERMISSIONS".equals(A00.getAction())) {
            String[] stringArrayExtra = A00.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
            if (stringArrayExtra == null) {
                stringArrayExtra = new String[0];
            }
            AnonymousClass00T.A0E(r3, stringArrayExtra, i);
        } else if ("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST".equals(A00.getAction())) {
            C06800Vd r0 = (C06800Vd) A00.getParcelableExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST");
            try {
                r3.startIntentSenderForResult(r0.A03, i, r0.A02, r0.A00, r0.A01, 0, bundle);
            } catch (IntentSender.SendIntentException e) {
                new Handler(Looper.getMainLooper()).post(new RunnableC09760dT(e, this, i));
            }
        } else {
            r3.startActivityForResult(A00, i, bundle);
        }
    }

    public final void A05(String str) {
        Object remove;
        if (!this.A00.contains(str) && (remove = this.A04.remove(str)) != null) {
            this.A06.remove(remove);
        }
        this.A07.remove(str);
        Map map = this.A05;
        if (map.containsKey(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Dropping pending result for request ");
            sb.append(str);
            sb.append(": ");
            sb.append(map.get(str));
            Log.w("ActivityResultRegistry", sb.toString());
            map.remove(str);
        }
        Bundle bundle = this.A02;
        if (bundle.containsKey(str)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Dropping pending result for request ");
            sb2.append(str);
            sb2.append(": ");
            sb2.append(bundle.getParcelable(str));
            Log.w("ActivityResultRegistry", sb2.toString());
            bundle.remove(str);
        }
        Map map2 = this.A03;
        C04680Mq r4 = (C04680Mq) map2.get(str);
        if (r4 != null) {
            ArrayList arrayList = r4.A01;
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                r4.A00.A01((AnonymousClass03H) it.next());
            }
            arrayList.clear();
            map2.remove(str);
        }
    }

    public final boolean A06(Intent intent, int i, int i2) {
        AnonymousClass05J r1;
        String str = (String) this.A06.get(Integer.valueOf(i));
        if (str == null) {
            return false;
        }
        this.A00.remove(str);
        AnonymousClass06Y r0 = (AnonymousClass06Y) this.A07.get(str);
        if (r0 == null || (r1 = r0.A00) == null) {
            this.A05.remove(str);
            this.A02.putParcelable(str, new C06830Vg(i2, intent));
            return true;
        }
        r1.ALs(r0.A01.A02(intent, i2));
        return true;
    }
}
