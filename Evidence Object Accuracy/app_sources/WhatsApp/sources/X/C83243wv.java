package X;

import android.view.animation.Animation;
import com.whatsapp.group.GroupParticipantsSearchFragment;

/* renamed from: X.3wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83243wv extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ GroupParticipantsSearchFragment A00;

    public C83243wv(GroupParticipantsSearchFragment groupParticipantsSearchFragment) {
        this.A00 = groupParticipantsSearchFragment;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0F().A0n();
    }
}
