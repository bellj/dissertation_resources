package X;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;

/* renamed from: X.0jQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13270jQ implements AbstractC13280jR {
    public Queue A00 = new ArrayDeque();
    public final Map A01 = new HashMap();
    public final Executor A02;

    public C13270jQ(Executor executor) {
        this.A02 = executor;
    }
}
