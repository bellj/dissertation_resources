package X;

import java.io.IOException;

/* renamed from: X.4bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94424bn {
    public static final C94744cT A02;
    public volatile AbstractC111915Bh A00;
    public volatile AbstractC117125Yn A01;

    public final AbstractC117125Yn A01(AbstractC117125Yn r2) {
        if (this.A01 == null) {
            synchronized (this) {
                if (this.A01 == null) {
                    try {
                        this.A01 = r2;
                        this.A00 = AbstractC111915Bh.A00;
                    } catch (C868048x unused) {
                        this.A01 = r2;
                        this.A00 = AbstractC111915Bh.A00;
                    }
                }
            }
        }
        return this.A01;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C94424bn)) {
            return false;
        }
        C94424bn r4 = (C94424bn) obj;
        AbstractC117125Yn r2 = this.A01;
        AbstractC117125Yn r1 = r4.A01;
        return r2 == null ? r1 == null ? A00().equals(r4.A00()) : A01(r1.AhB()).equals(r1) : r1 != null ? r2.equals(r1) : r2.equals(r4.A01(r2.AhB()));
    }

    public int hashCode() {
        return 1;
    }

    static {
        C94744cT r0;
        Class cls = C88354Fi.A00;
        if (cls != null) {
            try {
                r0 = (C94744cT) C72453ed.A0k(cls, "getEmptyRegistry");
            } catch (Exception unused) {
            }
            A02 = r0;
        }
        r0 = C94744cT.A01;
        A02 = r0;
    }

    public final AbstractC111915Bh A00() {
        AbstractC111915Bh r1;
        if (this.A00 != null) {
            return this.A00;
        }
        synchronized (this) {
            if (this.A00 != null) {
                return this.A00;
            }
            if (this.A01 == null) {
                r1 = AbstractC111915Bh.A00;
            } else {
                AbstractC117125Yn r4 = this.A01;
                try {
                    AnonymousClass4MH r3 = new AnonymousClass4MH(r4.Ah0());
                    AbstractC79223qF r2 = r3.A00;
                    AnonymousClass5XT A0d = C72453ed.A0d(r4);
                    C108684zU r0 = r2.A00;
                    if (r0 == null) {
                        r0 = new C108684zU(r2);
                    }
                    A0d.Agw(r0, r4);
                    C79283qL r22 = (C79283qL) r2;
                    if (r22.A01 - r22.A00 == 0) {
                        r1 = new C79243qH(r3.A01);
                    } else {
                        throw C12960it.A0U("Did not write as much data as expected.");
                    }
                } catch (IOException e) {
                    throw new RuntimeException(C12960it.A0d(" threw an IOException (should never happen).", C72453ed.A0v(r4, "ByteString")), e);
                }
            }
            this.A00 = r1;
            return this.A00;
        }
    }
}
