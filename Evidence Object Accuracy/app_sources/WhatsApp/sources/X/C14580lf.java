package X;

import com.whatsapp.util.Log;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0lf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14580lf implements AbstractC14590lg {
    public final C14620lj A00 = new C14620lj();
    public final C14620lj A01 = new C14620lj();
    public final AtomicBoolean A02 = new AtomicBoolean(false);

    public final void A00(AbstractC14590lg r3) {
        this.A01.A03(r3, null);
    }

    public final void A01(AbstractC14590lg r2, Executor executor) {
        this.A01.A03(r2, executor);
    }

    public final void A02(Object obj) {
        if (this.A02.compareAndSet(false, true)) {
            this.A01.A04(obj);
        }
    }

    public final void A03(Throwable th) {
        if (this.A02.compareAndSet(false, true)) {
            this.A00.A04(th);
        }
    }

    public void A04() {
        this.A01.A01();
        this.A00.A01();
        if (!this.A02.get()) {
            Log.w("asyncfuture/unsubscribe called before completion, possibly not intended", new Throwable());
        }
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        A02(obj);
    }
}
