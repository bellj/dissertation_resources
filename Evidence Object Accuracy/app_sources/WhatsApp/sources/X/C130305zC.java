package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PayToolbar;

/* renamed from: X.5zC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130305zC {
    public static void A00(ActivityC13810kN r6, AnonymousClass018 r7, PayToolbar payToolbar, int i) {
        A01(r6, r7, payToolbar, r6.getString(i), R.drawable.ic_back, false);
        r6.findViewById(R.id.toolbar_bottom_divider).setVisibility(0);
    }

    public static void A01(ActivityC13810kN r3, AnonymousClass018 r4, PayToolbar payToolbar, String str, int i, boolean z) {
        payToolbar.A07();
        r3.A1e(payToolbar);
        AbstractC005102i A1U = r3.A1U();
        if (A1U != null) {
            A1U.A0I(str);
            A1U.A0M(true);
        }
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(r3, r4, i);
        C117305Zk.A13(r3.getResources(), A00, R.color.lightActionBarItemDrawableTint);
        payToolbar.setNavigationIcon(A00);
        payToolbar.setNavigationOnClickListener(C117305Zk.A0A(r3, 187));
        if (z) {
            payToolbar.setLogo(AnonymousClass2GE.A04(r3.getResources().getDrawable(R.drawable.novi_wordmark), r3.getResources().getColor(R.color.novi_header)));
        }
    }
}
