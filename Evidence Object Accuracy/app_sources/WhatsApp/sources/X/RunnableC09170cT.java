package X;

import java.lang.ref.WeakReference;

/* renamed from: X.0cT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09170cT implements Runnable {
    public final WeakReference A00;

    public RunnableC09170cT(AnonymousClass0EP r2) {
        this.A00 = new WeakReference(r2);
    }

    @Override // java.lang.Runnable
    public void run() {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null) {
            ((AnonymousClass0EP) weakReference.get()).A0K = false;
        }
    }
}
