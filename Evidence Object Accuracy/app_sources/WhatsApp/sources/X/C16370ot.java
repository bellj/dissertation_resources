package X;

import android.database.Cursor;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.Map;

/* renamed from: X.0ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16370ot {
    public final AbstractC15710nm A00;
    public final C14830m7 A01;
    public final C242514u A02;
    public final C18740sw A03;
    public final C20530vu A04;
    public final C16510p9 A05;
    public final C19990v2 A06;
    public final C20290vW A07;
    public final C21410xN A08;
    public final C240213x A09;
    public final C242814x A0A;
    public final C242614v A0B;
    public final C19790ug A0C;
    public final C20560vx A0D;
    public final C20280vV A0E;
    public final C20060v9 A0F;
    public final C20090vC A0G;
    public final C20120vF A0H;
    public final C20130vG A0I;
    public final AnonymousClass1YX A0J;
    public final C241914o A0K;
    public final C16490p7 A0L;
    public final C20210vO A0M;
    public final AnonymousClass151 A0N;
    public final C20500vr A0O;
    public final C20570vy A0P;
    public final C20370ve A0Q;
    public final AnonymousClass119 A0R;
    public final C22440z5 A0S;
    public final C20540vv A0T;
    public final C20520vt A0U;
    public final C20860wR A0V;
    public final C242314s A0W;
    public final C20490vq A0X;
    public final C20510vs A0Y;
    public final C242414t A0Z;
    public final Map A0a;

    public C16370ot(AbstractC15710nm r3, C14830m7 r4, C242514u r5, C18740sw r6, C20530vu r7, C16510p9 r8, C19990v2 r9, C20290vW r10, C21410xN r11, C240213x r12, C242814x r13, C242614v r14, C19790ug r15, C20560vx r16, C20280vV r17, C20060v9 r18, C20090vC r19, C20120vF r20, C20130vG r21, C21620xi r22, C241914o r23, C16490p7 r24, C20210vO r25, AnonymousClass151 r26, C20500vr r27, C20570vy r28, C20370ve r29, AnonymousClass119 r30, C22440z5 r31, C20540vv r32, C20520vt r33, C20860wR r34, C242314s r35, C20490vq r36, C20510vs r37, C242414t r38) {
        this.A01 = r4;
        this.A05 = r8;
        this.A00 = r3;
        this.A06 = r9;
        this.A03 = r6;
        this.A0G = r19;
        this.A0R = r30;
        this.A0X = r36;
        this.A0O = r27;
        this.A0Y = r37;
        this.A0C = r15;
        this.A0K = r23;
        this.A0M = r25;
        this.A0U = r33;
        this.A04 = r7;
        this.A07 = r10;
        this.A0I = r21;
        this.A0L = r24;
        this.A0T = r32;
        this.A0V = r34;
        this.A0W = r35;
        this.A0F = r18;
        this.A0Z = r38;
        this.A02 = r5;
        this.A08 = r11;
        this.A0B = r14;
        this.A0H = r20;
        this.A0E = r17;
        this.A0D = r16;
        this.A0Q = r29;
        this.A0S = r31;
        this.A0P = r28;
        this.A0A = r13;
        this.A09 = r12;
        this.A0N = r26;
        this.A0J = r22.A01;
        this.A0a = r22.A02;
    }

    public AbstractC15340mz A00(long j) {
        AbstractC15340mz r4;
        AbstractC14640lm A05;
        long uptimeMillis = SystemClock.uptimeMillis();
        C16310on A01 = this.A0L.get();
        Cursor A09 = A01.A03.A09(C32301bw.A0C, new String[]{String.valueOf(j)});
        if (!A09.moveToLast() || (A05 = this.A05.A05(A09.getLong(A09.getColumnIndexOrThrow("chat_row_id")))) == null) {
            r4 = null;
        } else {
            r4 = A02(A09, A05, false, true);
        }
        A09.close();
        A01.close();
        this.A07.A00("CachedMessageStore/getMessage/rowId", SystemClock.uptimeMillis() - uptimeMillis);
        return r4;
    }

    public AbstractC15340mz A01(Cursor cursor) {
        AbstractC14640lm A06 = this.A05.A06(cursor);
        if (A06 == null) {
            return null;
        }
        return A02(cursor, A06, false, true);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x0778, code lost:
        if (r0 == null) goto L_0x077a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ff, code lost:
        if (r3.size() > 0) goto L_0x0201;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AbstractC15340mz A02(android.database.Cursor r32, X.AbstractC14640lm r33, boolean r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 3324
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16370ot.A02(android.database.Cursor, X.0lm, boolean, boolean):X.0mz");
    }

    public AbstractC15340mz A03(AnonymousClass1IS r12) {
        if (r12 == null) {
            return null;
        }
        C16310on A01 = this.A0L.get();
        try {
            AbstractC15340mz A04 = A04(r12);
            if (A04 == null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                C16330op r7 = A01.A03;
                String str = AnonymousClass1Y7.A03;
                String[] strArr = new String[3];
                C16510p9 r0 = this.A05;
                AbstractC14640lm r3 = r12.A00;
                AnonymousClass009.A05(r3);
                int i = 0;
                strArr[0] = Long.toString(r0.A02(r3));
                if (r12.A02) {
                    i = 1;
                }
                strArr[1] = String.valueOf(i);
                strArr[2] = r12.A01;
                Cursor A09 = r7.A09(str, strArr);
                if (A09 == null) {
                    Log.e("CachedMessageStore/getmessage no cursor!");
                } else {
                    if (A09.moveToLast()) {
                        A04 = A02(A09, r3, false, true);
                    }
                    A09.close();
                }
                this.A07.A00("CachedMessageStore/getMessage/key", SystemClock.uptimeMillis() - uptimeMillis);
            }
            A01.close();
            return A04;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final AbstractC15340mz A04(AnonymousClass1IS r6) {
        AbstractC15340mz r2;
        AbstractC15340mz r0;
        AbstractC15340mz r22;
        AnonymousClass1YX r4 = this.A0J;
        synchronized (r4) {
            C006202y r3 = r4.A01;
            r2 = (AbstractC15340mz) r3.A04(r6);
            if (r2 == null) {
                Map map = r4.A02;
                WeakReference weakReference = (WeakReference) map.get(r6);
                if (weakReference != null) {
                    r2 = (AbstractC15340mz) weakReference.get();
                    map.remove(r6);
                    if (r2 != null) {
                        r3.A08(r6, r2);
                    }
                }
            }
        }
        if (r2 != null) {
            return r2;
        }
        AnonymousClass1PE A06 = this.A06.A06(r6.A00);
        return (A06 == null || (r0 = A06.A0a) == null || !r6.equals(r0.A0z) || (r22 = A06.A0a) == null) ? (AbstractC15340mz) this.A0a.get(r6) : r22;
    }

    public void A05(AnonymousClass1IS r3) {
        if (r3 != null) {
            AnonymousClass1YX r1 = this.A0J;
            synchronized (r1) {
                r1.A02.remove(r3);
                r1.A01.A07(r3);
            }
            this.A0a.remove(r3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0047, code lost:
        if (r3 > r1) goto L_0x0049;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A06(X.AbstractC15340mz r9) {
        /*
            r8 = this;
            X.1IS r0 = r9.A0z
            X.0lm r6 = r0.A00
            X.AnonymousClass009.A05(r6)
            byte r5 = r9.A0y
            r0 = 8
            if (r5 == r0) goto L_0x0035
            boolean r0 = r9.A0v
            if (r0 != 0) goto L_0x0021
            long r2 = r9.A12
            X.0v2 r0 = r8.A06
            X.1PE r0 = r0.A06(r6)
            if (r0 != 0) goto L_0x0089
            r0 = -9223372036854775808
        L_0x001d:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 <= 0) goto L_0x007e
        L_0x0021:
            boolean r0 = r9.A0v
            if (r0 == 0) goto L_0x0035
            long r2 = r9.A12
            X.0v2 r0 = r8.A06
            X.1PE r0 = r0.A06(r6)
            if (r0 != 0) goto L_0x0086
            r0 = -9223372036854775808
        L_0x0031:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 <= 0) goto L_0x007e
        L_0x0035:
            boolean r0 = r9.A0v
            if (r0 != 0) goto L_0x0049
            long r3 = r9.A12
            X.0v2 r7 = r8.A06
            X.1PE r0 = r7.A06(r6)
            if (r0 != 0) goto L_0x0083
            r1 = -9223372036854775808
        L_0x0045:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x005d
        L_0x0049:
            boolean r0 = r9.A0v
            if (r0 == 0) goto L_0x008c
            long r3 = r9.A12
            X.0v2 r7 = r8.A06
            X.1PE r0 = r7.A06(r6)
            if (r0 != 0) goto L_0x0080
            r1 = -9223372036854775808
        L_0x0059:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x008c
        L_0x005d:
            X.1PE r0 = r7.A06(r6)
            if (r0 == 0) goto L_0x007e
            java.lang.String r2 = r0.A0d
            if (r2 == 0) goto L_0x007e
            java.lang.String r1 = "\""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r5)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            boolean r0 = r2.contains(r0)
            if (r0 == 0) goto L_0x008c
        L_0x007e:
            r0 = 1
            return r0
        L_0x0080:
            long r1 = r0.A0D
            goto L_0x0059
        L_0x0083:
            long r1 = r0.A0C
            goto L_0x0045
        L_0x0086:
            long r0 = r0.A0F
            goto L_0x0031
        L_0x0089:
            long r0 = r0.A0E
            goto L_0x001d
        L_0x008c:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16370ot.A06(X.0mz):boolean");
    }
}
