package X;

/* renamed from: X.66N  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66N implements AnonymousClass6MJ {
    public final C125445rG A00;
    public final AnonymousClass6Lj A01;
    public final C129875yR A02;
    public volatile int A03;
    public volatile C1310360y A04;
    public volatile Boolean A05;

    public AnonymousClass66N() {
        this(null);
    }

    public AnonymousClass66N(C125445rG r3) {
        this.A03 = 0;
        AnonymousClass66F r1 = new AnonymousClass66F(this);
        this.A01 = r1;
        this.A00 = r3;
        C129875yR r0 = new C129875yR();
        this.A02 = r0;
        r0.A01 = r1;
    }

    @Override // X.AnonymousClass6MJ
    public void A6e() {
        this.A02.A00();
    }

    @Override // X.AnonymousClass6MJ
    public /* bridge */ /* synthetic */ Object AGH() {
        if (this.A05 == null) {
            throw C12960it.A0U("Configure Preview operation hasn't completed yet.");
        } else if (this.A05.booleanValue()) {
            return this.A04;
        } else {
            throw new AnonymousClass6L0("Failed to configure preview.");
        }
    }
}
