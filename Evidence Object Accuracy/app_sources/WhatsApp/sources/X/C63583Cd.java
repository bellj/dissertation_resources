package X;

import java.util.List;

/* renamed from: X.3Cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63583Cd {
    public final C454921v A00;
    public final C454721t A01;

    public C63583Cd(C454921v r1, C454721t r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        C454721t r3 = this.A01;
        C63863Df r1 = r3.A03;
        if (!r1.A00.isEmpty()) {
            List list = r3.A04;
            AbstractC454821u A00 = r1.A00(list);
            AbstractC454821u r0 = r3.A01;
            if (r0 != null && !list.contains(r0)) {
                r3.A01 = null;
            }
            if (A00 instanceof AnonymousClass33J) {
                this.A00.A02();
            }
            this.A00.A01();
        }
    }
}
