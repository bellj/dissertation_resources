package X;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0eK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0eK implements Runnable {
    public static Comparator A04 = new C10330eR();
    public static final ThreadLocal A05 = new ThreadLocal();
    public long A00;
    public long A01;
    public ArrayList A02 = new ArrayList();
    public ArrayList A03 = new ArrayList();

    public static final AnonymousClass03U A00(RecyclerView recyclerView, int i, long j) {
        AnonymousClass0QT r4 = recyclerView.A0K;
        int A01 = r4.A01();
        for (int i2 = 0; i2 < A01; i2++) {
            AnonymousClass03U A012 = RecyclerView.A01(r4.A04(i2));
            if (A012.A05 == i && (A012.A00 & 4) == 0) {
                return null;
            }
        }
        AnonymousClass0QS r1 = recyclerView.A0w;
        try {
            recyclerView.A09++;
            AnonymousClass03U A013 = r1.A01(i, j);
            if (A013 != null) {
                if (!A013.A07() || (A013.A00 & 4) != 0) {
                    r1.A0A(A013, false);
                } else {
                    r1.A05(A013.A0H);
                }
            }
            return A013;
        } finally {
            int i3 = 1;
            int i4 = recyclerView.A09 - i3;
            recyclerView.A09 = i4;
            if (i4 < i3) {
                recyclerView.A09 = 0;
            }
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        WeakReference weakReference;
        RecyclerView recyclerView;
        C04950Nr r0;
        try {
            C003401m.A01("RV Prefetch");
            ArrayList arrayList = this.A02;
            if (!arrayList.isEmpty()) {
                int size = arrayList.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    RecyclerView recyclerView2 = (RecyclerView) arrayList.get(i);
                    if (recyclerView2.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView2.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    long nanos = TimeUnit.MILLISECONDS.toNanos(j) + this.A00;
                    int size2 = arrayList.size();
                    int i2 = 0;
                    for (int i3 = 0; i3 < size2; i3++) {
                        RecyclerView recyclerView3 = (RecyclerView) arrayList.get(i3);
                        if (recyclerView3.getWindowVisibility() == 0) {
                            AnonymousClass0Z8 r02 = recyclerView3.A0L;
                            r02.A00(recyclerView3, false);
                            i2 += r02.A00;
                        }
                    }
                    ArrayList arrayList2 = this.A03;
                    arrayList2.ensureCapacity(i2);
                    int i4 = 0;
                    for (int i5 = 0; i5 < size2; i5++) {
                        RecyclerView recyclerView4 = (RecyclerView) arrayList.get(i5);
                        if (recyclerView4.getWindowVisibility() == 0) {
                            AnonymousClass0Z8 r13 = recyclerView4.A0L;
                            int abs = Math.abs(r13.A01) + Math.abs(r13.A02);
                            for (int i6 = 0; i6 < (r13.A00 << 1); i6 += 2) {
                                if (i4 >= arrayList2.size()) {
                                    r0 = new C04950Nr();
                                    arrayList2.add(r0);
                                } else {
                                    r0 = (C04950Nr) arrayList2.get(i4);
                                }
                                int[] iArr = r13.A03;
                                int i7 = iArr[i6 + 1];
                                boolean z = false;
                                if (i7 <= abs) {
                                    z = true;
                                }
                                r0.A04 = z;
                                r0.A02 = abs;
                                r0.A00 = i7;
                                r0.A03 = recyclerView4;
                                r0.A01 = iArr[i6];
                                i4++;
                            }
                        }
                    }
                    Collections.sort(arrayList2, A04);
                    for (int i8 = 0; i8 < arrayList2.size(); i8++) {
                        C04950Nr r9 = (C04950Nr) arrayList2.get(i8);
                        if (r9.A03 == null) {
                            break;
                        }
                        AnonymousClass03U A00 = A00(r9.A03, r9.A01, r9.A04 ? Long.MAX_VALUE : nanos);
                        if (!(A00 == null || (weakReference = A00.A0D) == null || !A00.A07() || (A00.A00 & 4) != 0 || (recyclerView = (RecyclerView) weakReference.get()) == null)) {
                            if (recyclerView.A0e && recyclerView.A0K.A01() != 0) {
                                recyclerView.A0P();
                            }
                            AnonymousClass0Z8 r7 = recyclerView.A0L;
                            r7.A00(recyclerView, true);
                            if (r7.A00 != 0) {
                                C003401m.A01("RV Nested Prefetch");
                                C05480Ps r12 = recyclerView.A0y;
                                AnonymousClass02M r6 = recyclerView.A0N;
                                r12.A04 = 1;
                                r12.A03 = r6.A0D();
                                r12.A08 = false;
                                r12.A0D = false;
                                r12.A09 = false;
                                for (int i9 = 0; i9 < (r7.A00 << 1); i9 += 2) {
                                    A00(recyclerView, r7.A03[i9], nanos);
                                }
                                C003401m.A00();
                            }
                        }
                        r9.A04 = false;
                        r9.A02 = 0;
                        r9.A00 = 0;
                        r9.A03 = null;
                        r9.A01 = 0;
                    }
                }
            }
        } finally {
            this.A01 = 0;
            C003401m.A00();
        }
    }
}
