package X;

import com.whatsapp.R;

/* renamed from: X.2GZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2GZ {
    public static final int[] A00 = {R.attr.background, R.attr.backgroundSplit, R.attr.backgroundStacked, R.attr.contentInsetEnd, R.attr.contentInsetEndWithActions, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.contentInsetStart, R.attr.contentInsetStartWithNavigation, R.attr.customNavigationLayout, R.attr.displayOptions, R.attr.divider, R.attr.elevation, R.attr.height, R.attr.hideOnContentScroll, R.attr.homeAsUpIndicator, R.attr.homeLayout, R.attr.icon, R.attr.indeterminateProgressStyle, R.attr.itemPadding, R.attr.logo, R.attr.navigationMode, R.attr.popupTheme, R.attr.progressBarPadding, R.attr.progressBarStyle, R.attr.subtitle, R.attr.subtitleTextStyle, R.attr.title, R.attr.titleTextStyle};
    public static final int[] A01 = {R.attr.afgrvGridSize, R.attr.afgrvGridSpacing};
    public static final int[] A02 = {16843039, 16843040};
    public static final int[] A03 = {R.attr.colorButtonNormal, R.attr.colorButtonStroke, R.attr.colorSelfieButtonStroke};
    public static final int[] A04 = {R.attr.new_caption_enabled};
    public static final int[] A05 = {R.attr.cmcIsGridView};
    public static final int[] A06 = {R.attr.cpbBackground, R.attr.cpbColor, R.attr.cpbFill, R.attr.cpbOutlineColor, R.attr.cpbOutlineWidth, R.attr.cpbStrokeWidthFactor, R.attr.cpbTextColor};
    public static final int[] A07 = {R.attr.alwaysShowClearIcon, R.attr.clearFocusOnBack, R.attr.clearIcon, R.attr.consumeTouch};
    public static final int[] A08 = {R.attr.toastString};
    public static final int[] A09 = {16843379, R.attr.backgroundTint, R.attr.backgroundTintMode, R.attr.borderWidth, R.attr.elevation, R.attr.fabCustomSize, R.attr.fabSize, R.attr.hideMotionSpec, R.attr.hoveredFocusedTranslationZ, R.attr.maxImageSize, R.attr.pressedTranslationZ, R.attr.rippleColor, R.attr.showMotionSpec, R.attr.useCompatPadding};
    public static final int[] A0A = {R.attr.icBottomDrawable, R.attr.icContentColor, R.attr.icTopDrawable};
    public static final int[] A0B = {R.attr.listItemWithIconDescription, R.attr.listItemWithIconIcon, R.attr.listItemWithIconIconColor, R.attr.listItemWithIconMirrorIconForRtl, R.attr.listItemWithIconTitle, R.attr.listItemWithIconTitleColor};
    public static final int[] A0C = {R.attr.mcclChildId};
    public static final int[] A0D = {R.attr.custom_key};
    public static final int[] A0E = {R.attr.qrCodeColor, R.attr.showAnim, R.attr.stampDrawable};
    public static final int[] A0F = {R.attr.rmtvLines, R.attr.rmtvLinkBold, R.attr.rmtvLinkColor, R.attr.rmtvText};
    public static final int[] A0G = {R.attr.new_recipients_enabled};
    public static final int[] A0H = {R.attr.rcpbBackgroundColor, R.attr.rcpbProgressColor, R.attr.rcpbTrackWidth};
    public static final int[] A0I = {R.attr.spbDividerColor, R.attr.spbDividerWidth, R.attr.spbFrameColor, R.attr.spbInitialBackgroundColor, R.attr.spbTrackWidth};
    public static final int[] A0J = {R.attr.scvBorderColor, R.attr.scvBorderSize, R.attr.scvIcon, R.attr.scvIconContentDescription, R.attr.scvSelectionColor};
    public static final int[] A0K = {R.attr.shadowColor, R.attr.shadowDx, R.attr.shadowDy, R.attr.shadowRadius};
    public static final int[] A0L = {R.attr.sivDimension, R.attr.sivSelector};
    public static final int[] A0M = {R.attr.shrvGridSize, R.attr.shrvGridSpacing};
    public static final int[] A0N = {R.attr.adjustTextGravity, R.attr.maxTextLineCount};
    public static final int[] A0O = {R.attr.tbtnAspectRatio, R.attr.tbtnBorderColor, R.attr.tbtnBorderSize, R.attr.tbtnForegroundOnly, R.attr.tbtnRadius, R.attr.tbtnSelectionColor, R.attr.tbtnShowShadow};
    public static final int[] A0P = {16843087, 16843088, 16843365, 16843379};
    public static final int[] A0Q = {16843087, 16843088, 16843365, 16843379};
    public static final int[] A0R = {R.attr.waflBackgroundPressedTint, R.attr.waflBackgroundTint, R.attr.waflForegroundPressedTint, R.attr.waflForegroundTint};
    public static final int[] A0S = {16843379, R.attr.wibMirrorForRtl};
    public static final int[] A0T = {16843379, R.attr.wivMirrorForRtl};
    public static final int[] A0U = {R.attr.ignoreForBidiLayout, R.attr.wallBackgroundTint};
    public static final int[] A0V = {R.attr.radius};
    public static final int[] A0W = {R.attr.waSwitchDescriptionId, R.attr.waSwitchSubTitleStyle, R.attr.waSwitchTitleId, R.attr.waSwitchTitleStyle};
    public static final int[] A0X = {16843087, 16843088, 16843365, 16843379};
}
