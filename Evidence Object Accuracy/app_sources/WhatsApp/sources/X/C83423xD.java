package X;

import android.view.animation.Animation;
import android.widget.ListView;
import com.whatsapp.KeyboardPopupLayout;

/* renamed from: X.3xD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83423xD extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final int A00;
    public final ListView A01;
    public final KeyboardPopupLayout A02;

    public C83423xD(ListView listView, KeyboardPopupLayout keyboardPopupLayout, int i) {
        this.A02 = keyboardPopupLayout;
        this.A01 = listView;
        this.A00 = i;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A02.setClipChildren(true);
        this.A01.setTranscriptMode(this.A00);
    }
}
