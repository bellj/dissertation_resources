package X;

import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.2nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58012nz extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ SharedTextPreviewDialogFragment A00;

    public C58012nz(SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment) {
        this.A00 = sharedTextPreviewDialogFragment;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = this.A00;
        sharedTextPreviewDialogFragment.A1K();
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, C12990iw.A03(((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A02), 0.0f);
        translateAnimation.setDuration(150);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A02.setVisibility(0);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A01.setVisibility(0);
        int selectionStart = sharedTextPreviewDialogFragment.A0E.getSelectionStart();
        int selectionEnd = sharedTextPreviewDialogFragment.A0E.getSelectionEnd();
        MentionableEntry mentionableEntry = sharedTextPreviewDialogFragment.A0E;
        mentionableEntry.setText(mentionableEntry.getStringText());
        sharedTextPreviewDialogFragment.A0E.setSelection(selectionStart, selectionEnd);
        sharedTextPreviewDialogFragment.A0O = false;
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G.startAnimation(translateAnimation);
    }
}
