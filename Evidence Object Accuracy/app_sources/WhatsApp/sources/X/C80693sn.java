package X;

import android.graphics.Outline;
import android.graphics.Rect;

/* renamed from: X.3sn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80693sn extends C50702Qp {
    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        Rect rect = this.A0A;
        copyBounds(rect);
        outline.setOval(rect);
    }
}
