package X;

import android.app.Notification;
import android.os.Parcel;
import android.support.v4.app.INotificationSideChannel;

/* renamed from: X.02w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C006002w implements AbstractC005802u {
    public final int A00;
    public final Notification A01;
    public final String A02;
    public final String A03;

    public C006002w(Notification notification, String str, String str2, int i) {
        this.A02 = str;
        this.A00 = i;
        this.A03 = str2;
        this.A01 = notification;
    }

    @Override // X.AbstractC005802u
    public void AbX(INotificationSideChannel iNotificationSideChannel) {
        String str = this.A02;
        int i = this.A00;
        String str2 = this.A03;
        Notification notification = this.A01;
        C06900Vo r7 = (C06900Vo) iNotificationSideChannel;
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeString(str2);
            if (notification != null) {
                obtain.writeInt(1);
                notification.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            r7.A00.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("NotifyTask[");
        sb.append("packageName:");
        sb.append(this.A02);
        sb.append(", id:");
        sb.append(this.A00);
        sb.append(", tag:");
        sb.append(this.A03);
        sb.append("]");
        return sb.toString();
    }
}
