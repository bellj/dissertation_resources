package X;

import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;

/* renamed from: X.3Wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68623Wc implements AbstractC16010oI {
    public final C15650ng A00;
    public final C28941Pp A01;

    public C68623Wc(C15650ng r1, C28941Pp r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC16010oI
    public void AI3(byte[] bArr) {
        String str;
        AbstractC15340mz A02 = this.A01.A02((byte) 0);
        if (!A02.A0x()) {
            Log.i("decryptmessagerunnable/axolotl message decryption had no data; ciphertext only");
            if (bArr != null) {
                try {
                    str = new String(bArr, AnonymousClass01V.A08);
                } catch (UnsupportedEncodingException unused) {
                }
                A02.A0l(str);
            }
            str = null;
            A02.A0l(str);
        }
        this.A00.A0p(A02);
    }
}
