package X;

import java.util.Arrays;

/* renamed from: X.3pW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC78773pW extends AbstractBinderC78743pT {
    public final byte[] A00;

    public BinderC78773pW(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.A00 = bArr;
    }
}
