package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1CR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CR {
    public final AnonymousClass19Q A00;
    public final AnonymousClass1CQ A01;

    public AnonymousClass1CR(AnonymousClass19Q r1, AnonymousClass1CQ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static void A00(AnonymousClass1CR r7, UserJid userJid, Integer num, int i) {
        int i2 = 2;
        if (i == 0) {
            i2 = 1;
        } else if (i == 1) {
            i2 = 3;
        } else if (i != 2) {
            i2 = -1;
        }
        r7.A01.A00(new AnonymousClass3VK(r7, userJid, num, Integer.valueOf(i2), null, null), userJid);
    }
}
