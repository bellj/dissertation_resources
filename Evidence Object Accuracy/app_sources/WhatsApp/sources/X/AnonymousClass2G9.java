package X;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import com.facebook.redex.IDxUListenerShape12S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.R;

/* renamed from: X.2G9  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2G9 extends FrameLayout {
    public abstract void A0C();

    public abstract void setCloseButtonListener(AnonymousClass5V0 v);

    public abstract void setFullscreenButtonClickListener(AnonymousClass5V0 v);

    public abstract void setPlayer(AnonymousClass21T v);

    public abstract void setPlayerElevation(int i);

    public AnonymousClass2G9(Context context) {
        super(context);
    }

    public void A00() {
        AnonymousClass39B r2 = (AnonymousClass39B) this;
        AnonymousClass21T r0 = r2.A06;
        if (r0 != null) {
            if (r0.A0B()) {
                AnonymousClass4Xy r1 = r2.A0h.A06;
                if (r1.A02) {
                    r1.A00();
                }
                r2.A06.A05();
            }
            if (!r2.A05()) {
                r2.A01();
            }
            r2.removeCallbacks(r2.A0i);
            r2.A0I();
            r2.A03(500);
        }
    }

    public void A01() {
        AnonymousClass39B r3 = (AnonymousClass39B) this;
        r3.A0M.setVisibility(0);
        r3.A0I();
        r3.setSystemUiVisibility(0);
        r3.A0D();
        if (!r3.A05()) {
            if (r3.A0K() && !r3.A0l) {
                ImageButton imageButton = r3.A0W;
                imageButton.setVisibility(0);
                imageButton.startAnimation(r3.A0P);
            }
            if (r3.A0B) {
                r3.A0G();
                ViewGroup viewGroup = r3.A0N;
                viewGroup.setVisibility(0);
                viewGroup.startAnimation(r3.A0P);
            } else {
                ProgressBar progressBar = r3.A0c;
                progressBar.setVisibility(0);
                progressBar.startAnimation(r3.A0P);
            }
            if (r3.A0l) {
                r3.A0F();
            }
        }
    }

    public void A02() {
        AnonymousClass39B r2 = (AnonymousClass39B) this;
        AnonymousClass2G8 r1 = r2.A01;
        if (r1 != null) {
            r1.A00 = true;
            r2.A01 = null;
        }
        r2.A0F = false;
        r2.A0I.removeCallbacksAndMessages(0);
    }

    public void A03(int i) {
        AnonymousClass39B r3 = (AnonymousClass39B) this;
        r3.A02();
        AnonymousClass2G8 r1 = new AnonymousClass2G8(r3);
        r3.A01 = r1;
        r3.postDelayed(new RunnableBRunnable0Shape16S0100000_I1_2(r1, 17), (long) i);
    }

    public void A04(int i, int i2) {
        AnonymousClass39B r4 = (AnonymousClass39B) this;
        AnonymousClass21T r0 = r4.A06;
        if (r0 != null && r0.A04() != null) {
            ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(i), Integer.valueOf(i2));
            ofObject.setDuration(150L);
            ofObject.addUpdateListener(new IDxUListenerShape12S0100000_2_I1(r4, 10));
            ofObject.start();
        }
    }

    public boolean A05() {
        AnonymousClass39B r2 = (AnonymousClass39B) this;
        if (r2.A0B) {
            if (r2.A0N.getVisibility() == 0) {
                return true;
            }
            return false;
        } else if (r2.A0c.getVisibility() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int[] getViewIdsToIgnoreScaling() {
        return new int[]{R.id.logo_button, R.id.play_frame, R.id.loading};
    }
}
