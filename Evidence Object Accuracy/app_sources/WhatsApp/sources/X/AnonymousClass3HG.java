package X;

import android.app.ProgressDialog;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.3HG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HG {
    public AnonymousClass38Y A00;
    public final C18790t3 A01;
    public final AnonymousClass018 A02;
    public final C25771At A03;
    public final C18810t5 A04;
    public final C252018m A05;
    public final AbstractC14440lR A06;

    public AnonymousClass3HG(C18790t3 r1, AnonymousClass018 r2, C25771At r3, C18810t5 r4, C252018m r5, AbstractC14440lR r6) {
        this.A06 = r6;
        this.A01 = r1;
        this.A05 = r5;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(X.C22680zT r4, X.C16590pI r5, java.lang.String r6, java.lang.String r7) {
        /*
            r3 = 0
            java.lang.String r1 = "eu"
            java.lang.String r0 = r4.A03(r6)     // Catch: IOException | 29w | 29x -> 0x0014
            boolean r0 = r1.equals(r0)     // Catch: IOException | 29w | 29x -> 0x0014
            if (r0 != 0) goto L_0x0027
            android.content.Context r0 = r5.A00     // Catch: IOException | 29w | 29x -> 0x0014
            X.1Id r0 = X.C27581Ic.A00(r0)     // Catch: IOException | 29w | 29x -> 0x0014
            goto L_0x0028
        L_0x0014:
            r2 = move-exception
            java.lang.String r0 = "RegistrationHelper/getAdvertisingId at "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r7)
            java.lang.String r0 = " failed"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.e(r0, r2)
        L_0x0027:
            r0 = r3
        L_0x0028:
            if (r0 == 0) goto L_0x002c
            java.lang.String r3 = r0.A00
        L_0x002c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HG.A00(X.0zT, X.0pI, java.lang.String, java.lang.String):java.lang.String");
    }

    public void A01() {
        ProgressDialog progressDialog;
        AnonymousClass38Y r1 = this.A00;
        if (r1 != null && (progressDialog = r1.A00) != null && progressDialog.isShowing()) {
            r1.A00.cancel();
        }
    }

    public void A02(ActivityC13810kN r12, AnonymousClass10O r13, String str) {
        String str2;
        String obj;
        C12980iv.A1M(this.A00);
        Uri.Builder appendPath = this.A05.A01().appendPath("verification.php");
        AnonymousClass018 r0 = this.A02;
        String A05 = r0.A05();
        String A06 = r0.A06();
        AnonymousClass01d r3 = r13.A08;
        TelephonyManager A0N = r3.A0N();
        if (A0N != null) {
            str2 = AnonymousClass23L.A01(A0N.getNetworkOperator(), "000-000");
        } else {
            str2 = "none";
        }
        HashMap A11 = C12970iu.A11();
        if (r13.A00 != null) {
            A11.put("platform", "android");
            A11.put("network", str2);
            A11.put("lc", A05);
            A11.put("lg", A06);
            A11.put("context", r13.A00);
            StringBuilder A0h = C12960it.A0h();
            if (C003501n.A0B(r3)) {
                A0h.append("rted ");
            }
            try {
                Class.forName("org.acra.ACRA");
                A0h.append("nw-wap ");
            } catch (ClassNotFoundException unused) {
            }
            if (A0h.length() == 0) {
                obj = null;
            } else {
                obj = A0h.toString();
            }
            A11.put("diagnostic", obj);
            String str3 = "true";
            String str4 = "false";
            if (r13.A01) {
                str4 = str3;
            }
            A11.put("fail_too_many", str4);
            String str5 = "false";
            if (r13.A02) {
                str5 = str3;
            }
            A11.put("no_route_sms", str5);
            String str6 = "false";
            if (r13.A03) {
                str6 = str3;
            }
            A11.put("no_route_voice", str6);
            String str7 = "false";
            if (r13.A05) {
                str7 = str3;
            }
            A11.put("valid_number", str7);
            if (!r13.A04) {
                str3 = "false";
            }
            A11.put("no_number", str3);
            A11.put("debug-context", r13.A00(r13.A00));
        }
        Iterator it = A11.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            String str8 = (String) A11.get(A0x);
            if (TextUtils.isEmpty(str8)) {
                str8 = "";
            }
            appendPath.appendQueryParameter(A0x, str8);
        }
        AnonymousClass38Y r4 = new AnonymousClass38Y(r12, this.A01, this.A03, this.A04, appendPath.toString(), r13.A00(str));
        this.A00 = r4;
        this.A06.Aaz(r4, new String[0]);
    }
}
