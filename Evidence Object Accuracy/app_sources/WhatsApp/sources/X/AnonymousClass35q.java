package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import java.util.List;

/* renamed from: X.35q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35q extends AbstractC69213Yj {
    public View A00;
    public WaImageView A01;
    public WaTextView A02;
    public List A03;
    public final int A04;
    public final AnonymousClass1AB A05;
    public final C235512c A06;
    public final AbstractC116245Ur A07;
    public final boolean A08;

    public AnonymousClass35q(Context context, LayoutInflater layoutInflater, C14850m9 r4, AnonymousClass1AB r5, C235512c r6, AbstractC116245Ur r7, int i, boolean z) {
        super(context, layoutInflater, r4, i);
        this.A06 = r6;
        this.A05 = r5;
        this.A07 = r7;
        this.A04 = C12980iv.A03(z ? 1 : 0);
        this.A08 = z;
    }

    @Override // X.AbstractC69213Yj
    public void A03(View view) {
        View findViewById = view.findViewById(R.id.empty);
        this.A00 = findViewById;
        findViewById.setVisibility(4);
        this.A01 = C12980iv.A0X(view, R.id.empty_image);
        WaTextView A0N = C12960it.A0N(view, R.id.empty_text);
        this.A02 = A0N;
        A0N.setText(R.string.sticker_picker_no_favorited_stickers);
    }

    @Override // X.AbstractC69213Yj, X.AnonymousClass5WC
    public void AP2(View view, ViewGroup viewGroup, int i) {
        super.AP2(view, viewGroup, i);
        this.A00 = null;
    }
}
