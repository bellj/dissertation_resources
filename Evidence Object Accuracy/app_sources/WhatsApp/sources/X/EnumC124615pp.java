package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124615pp extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124615pp[] A00;
    public static final EnumC124615pp A01;
    public static final EnumC124615pp A02;
    public final String fieldName;

    public static EnumC124615pp valueOf(String str) {
        return (EnumC124615pp) Enum.valueOf(EnumC124615pp.class, str);
    }

    public static EnumC124615pp[] values() {
        return (EnumC124615pp[]) A00.clone();
    }

    static {
        EnumC124615pp r5 = new EnumC124615pp("DISPLAY_NAME", "display_name", 0);
        A01 = r5;
        EnumC124615pp r4 = new EnumC124615pp("PROFILE_ICON_BLOB", "profile_icon_blob", 1);
        A02 = r4;
        EnumC124615pp r2 = new EnumC124615pp("INDIA_UPI_CONTACT_DATA", "india_upi_contact_data", 2);
        EnumC124615pp[] r1 = new EnumC124615pp[3];
        C12990iw.A1P(r5, r4, r1);
        r1[2] = r2;
        A00 = r1;
    }

    public EnumC124615pp(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
