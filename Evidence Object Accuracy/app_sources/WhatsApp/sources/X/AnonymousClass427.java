package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.Collection;

/* renamed from: X.427  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass427 extends AnonymousClass1GY {
    public final /* synthetic */ ContactPickerFragment A00;

    public AnonymousClass427(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AnonymousClass1GY
    public void A00() {
        this.A00.A0m.A00.A1g(true);
    }

    @Override // X.AnonymousClass1GY
    public void A01(Collection collection, boolean z) {
        this.A00.A0m.A00.A1g(false);
    }
}
