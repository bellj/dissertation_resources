package X;

import android.view.animation.Animation;
import com.whatsapp.phonematching.CountryPicker;

/* renamed from: X.2o1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58032o1 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ CountryPicker A00;

    public C58032o1(CountryPicker countryPicker) {
        this.A00 = countryPicker;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        CountryPicker countryPicker = this.A00;
        countryPicker.A01.setIconified(true);
        countryPicker.A00.setVisibility(8);
        countryPicker.A02.setVisibility(0);
    }
}
