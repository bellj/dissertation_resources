package X;

/* renamed from: X.4YM  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YM {
    public int A00;
    public C89594Ko A01;
    public final AnonymousClass5XW A02;

    public AnonymousClass4YM(AnonymousClass5XW r1) {
        this.A02 = r1;
    }

    public static int A00(AnonymousClass4YM r3, AnonymousClass5XW r4) {
        return r3.A01.A00[((AnonymousClass4BL) AnonymousClass4BL.A00.get(Integer.valueOf(r4.readByte() & 255))).typeId];
    }

    public int A01() {
        int readInt;
        boolean z = this instanceof AnonymousClass45U;
        A02();
        if (!z) {
            AnonymousClass5XW r0 = this.A02;
            r0.readInt();
            readInt = r0.readInt() * A00(this, r0);
            A04((long) readInt);
        } else {
            AnonymousClass5XW r2 = this.A02;
            r2.readInt();
            readInt = r2.readInt() * A00(this, r2);
            C1110557x r22 = (C1110557x) r2;
            r22.A00.AZq(new byte[readInt]);
            r22.A01.write(new byte[readInt]);
        }
        return this.A00 + 4 + 4 + 1 + readInt;
    }

    public long A02() {
        int i;
        int i2 = this.A00;
        if (i2 == 1) {
            i = this.A02.readByte();
        } else if (i2 == 2) {
            i = this.A02.readShort();
        } else if (i2 == 4) {
            i = this.A02.readInt();
        } else if (i2 == 8) {
            return this.A02.readLong();
        } else {
            throw C12970iu.A0f("ID Length must be 1, 2, 4, or 8");
        }
        return (long) i;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r14v3, resolved type: X.4BL[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0306 A[Catch: EOFException -> 0x035e, TryCatch #0 {EOFException -> 0x035e, blocks: (B:3:0x0009, B:5:0x0011, B:6:0x001d, B:8:0x0024, B:9:0x0033, B:10:0x003c, B:12:0x0043, B:13:0x005a, B:14:0x0065, B:16:0x006b, B:20:0x0087, B:23:0x0091, B:31:0x00aa, B:32:0x00ad, B:33:0x00b0, B:35:0x00b5, B:37:0x00b9, B:39:0x00f0, B:40:0x0103, B:42:0x010d, B:43:0x013d, B:45:0x0149, B:46:0x0166, B:47:0x017a, B:49:0x01ab, B:50:0x01be, B:52:0x01c8, B:54:0x01dd, B:55:0x01e0, B:56:0x01f1, B:57:0x0206, B:59:0x0212, B:60:0x0220, B:62:0x0224, B:63:0x023f, B:64:0x025e, B:66:0x026f, B:67:0x0276, B:68:0x027a, B:70:0x0280, B:71:0x028c, B:73:0x0292, B:75:0x029a, B:76:0x029e, B:77:0x02b3, B:78:0x02bc, B:79:0x02c1, B:80:0x02de, B:81:0x02e5, B:82:0x02f5, B:83:0x0306, B:84:0x0316, B:85:0x031c, B:86:0x0321, B:87:0x0329, B:88:0x032d, B:89:0x034c, B:90:0x034d, B:91:0x035d), top: B:94:0x0009 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
        // Method dump skipped, instructions count: 912
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YM.A03():void");
    }

    public void A04(long j) {
        AnonymousClass5XW r2 = this.A02;
        r2.AcZ(r2.position() + j);
    }
}
