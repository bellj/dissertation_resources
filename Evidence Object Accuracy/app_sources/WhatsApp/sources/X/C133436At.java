package X;

/* renamed from: X.6At  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133436At implements AnonymousClass6MV {
    public final /* synthetic */ AnonymousClass02N A00;
    public final /* synthetic */ AnonymousClass607 A01;
    public final /* synthetic */ C1323666p A02;

    public C133436At(AnonymousClass02N r1, AnonymousClass607 r2, C1323666p r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        C1323666p r0 = this.A02;
        r0.AKk();
        r0.APo(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r5) {
        C1323666p r3 = this.A02;
        r3.AKl();
        this.A01.A01(this.A00, r3, new C128545wH(r5));
    }
}
