package X;

import com.whatsapp.invites.ViewGroupInviteActivity;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/* renamed from: X.38e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626838e extends AbstractC16350or {
    public int A00;
    public C63373Bi A01;
    public final C15570nT A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C19990v2 A05;
    public final C15600nX A06;
    public final C17220qS A07;
    public final C20660w7 A08;
    public final C32511cH A09;
    public final C28581Od A0A;
    public final WeakReference A0B;

    public C626838e(C15570nT r2, C15550nR r3, C15610nY r4, C19990v2 r5, C15600nX r6, ViewGroupInviteActivity viewGroupInviteActivity, C17220qS r8, C20660w7 r9, C32511cH r10, C28581Od r11) {
        this.A02 = r2;
        this.A05 = r5;
        this.A08 = r9;
        this.A07 = r8;
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r6;
        this.A0B = C12970iu.A10(viewGroupInviteActivity);
        this.A0A = r11;
        this.A09 = r10;
    }

    public final void A08(C15580nU r12, UserJid userJid, AnonymousClass1PD r14, String str, String str2, Collection collection, int i, int i2, long j) {
        ArrayList A0w = C12980iv.A0w(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C15370n3 A0A = this.A03.A0A(C12990iw.A0b(it));
            if (!(A0A == null || A0A.A0C == null)) {
                A0w.add(A0A);
            }
        }
        Collections.sort(A0w, new AnonymousClass44Q(this.A02, this.A04, this));
        this.A01 = new C63373Bi(r12, userJid, r14, str, str2, A0w, i, i2, j);
    }
}
