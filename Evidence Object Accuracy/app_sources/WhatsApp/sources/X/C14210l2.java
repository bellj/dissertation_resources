package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0l2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14210l2 {
    public final List A00 = new ArrayList();

    public static C14210l2 A00(Object obj) {
        C14210l2 r1 = new C14210l2();
        r1.A05(obj, 0);
        return r1;
    }

    public static C14220l3 A01(C14210l2 r0, Object obj, int i) {
        r0.A05(obj, i);
        return new C14220l3(r0.A00);
    }

    public static C14220l3 A02(Object obj) {
        C14210l2 r1 = new C14210l2();
        r1.A05(obj, 0);
        return new C14220l3(r1.A00);
    }

    public C14220l3 A03() {
        return new C14220l3(this.A00);
    }

    public void A04(Object obj, int i) {
        List list = this.A00;
        if (list.size() <= i) {
            list.add(i, obj);
            return;
        }
        throw new IllegalArgumentException("Arguments must be continuous");
    }

    @Deprecated
    public void A05(Object obj, int i) {
        List list = this.A00;
        if (list.size() <= i) {
            list.add(i, obj);
            return;
        }
        throw new IllegalArgumentException("Arguments must be continuous");
    }
}
