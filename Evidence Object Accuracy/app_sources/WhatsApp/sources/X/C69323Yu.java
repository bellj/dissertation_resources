package X;

import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.3Yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69323Yu implements AbstractC17410ql {
    public final /* synthetic */ C15260mp A00;

    public C69323Yu(C15260mp r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        C15260mp r3 = this.A00;
        AnonymousClass240 r2 = r3.A07;
        if (r2 != null) {
            r3.A04 = true;
            r2.A00.A02.setVisibility(8);
            if (r3.A00 == 3) {
                r3.A0I(3);
            }
        }
    }

    @Override // X.AbstractC17410ql
    public void AMk() {
        C15260mp r4 = this.A00;
        if (r4.A04) {
            ViewGroup viewGroup = ((C15270mq) r4).A05;
            C34271fr.A00(viewGroup, viewGroup.getResources().getText(R.string.avatar_sticker_pack_deleted), 0).A03();
            r4.A04 = false;
        }
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        C15260mp r2 = this.A00;
        AnonymousClass240 r0 = r2.A07;
        if (r0 != null) {
            r0.A00.A02.setVisibility(0);
            if (z) {
                r2.A0I(3);
            }
        }
    }
}
