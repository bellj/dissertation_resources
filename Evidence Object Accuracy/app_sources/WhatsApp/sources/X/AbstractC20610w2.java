package X;

import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0w2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC20610w2 {
    public static int A06 = 200;
    public final C006202y A00 = new C006202y(250);
    public final AbstractC15710nm A01;
    public final C18460sU A02;
    public final C20850wQ A03;
    public final C16490p7 A04;
    public final C22840zj A05;

    public AbstractC20610w2(AbstractC15710nm r3, C18460sU r4, C20850wQ r5, C16490p7 r6, C22840zj r7) {
        this.A02 = r4;
        this.A01 = r3;
        this.A04 = r6;
        this.A05 = r7;
        this.A03 = r5;
    }

    public C32611cR A00(AbstractC15340mz r7) {
        C32611cR r2;
        C32611cR r1;
        C32611cR r22;
        if (!(this instanceof C20600w1)) {
            C245415x r4 = (C245415x) this;
            AnonymousClass009.A0E(r7 instanceof AnonymousClass1Iv);
            C006202y r3 = ((AbstractC20610w2) r4).A00;
            r2 = (C32611cR) r3.A04(Long.valueOf(r7.A11));
            if (r2 == null) {
                r22 = r4.A05(r7.A0z);
                long j = r7.A11;
                synchronized (this) {
                    Long valueOf = Long.valueOf(j);
                    r1 = (C32611cR) r3.A04(valueOf);
                    if (r1 == null) {
                        r3.A08(valueOf, r22);
                        return r22;
                    }
                    return r1;
                }
            }
            return r2;
        }
        C20600w1 r42 = (C20600w1) this;
        C006202y r32 = ((AbstractC20610w2) r42).A00;
        r2 = (C32611cR) r32.A04(Long.valueOf(r7.A11));
        if (r2 == null) {
            r22 = r42.A05(r7.A0z, r7.A11);
            long j2 = r7.A11;
            synchronized (this) {
                Long valueOf2 = Long.valueOf(j2);
                r1 = (C32611cR) r32.A04(valueOf2);
                if (r1 == null) {
                    r32.A08(valueOf2, r22);
                    return r22;
                }
                return r1;
            }
        }
        return r2;
    }

    public String A01(int i) {
        if (!(this instanceof C20600w1)) {
            boolean z = false;
            if (i > 0) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            StringBuilder sb = new StringBuilder("INSERT INTO message_add_on_receipt_device (message_add_on_row_id, receipt_device_jid_row_id, primary_device_version) SELECT ?, ?, ?");
            for (int i2 = 1; i2 < i; i2++) {
                sb.append(" UNION ALL SELECT ?,?,?");
            }
            return sb.toString();
        }
        boolean z2 = false;
        if (i > 0) {
            z2 = true;
        }
        AnonymousClass009.A0E(z2);
        StringBuilder sb2 = new StringBuilder("INSERT INTO receipt_device (message_row_id, receipt_device_jid_row_id, primary_device_version) SELECT ?, ?, ?");
        for (int i3 = 1; i3 < i; i3++) {
            sb2.append(" UNION ALL SELECT ?,?,?");
        }
        return sb2.toString();
    }

    public final void A02(AbstractC15340mz r20, Set set, boolean z) {
        C32611cR r8;
        String str;
        String str2;
        String str3;
        if (!set.isEmpty() && r20.A11 != -1) {
            C006202y r2 = this.A00;
            if (r2.A04(Long.valueOf(r20.A11)) == null) {
                r8 = new C32611cR();
            } else {
                r8 = (C32611cR) r2.A04(Long.valueOf(r20.A11));
            }
            AnonymousClass009.A05(r8);
            Map A01 = this.A05.A01(C15380n4.A09(this.A01, set));
            try {
                C16310on A02 = this.A04.A02();
                AnonymousClass1Lx A00 = A02.A00();
                int i = 0;
                if (z) {
                    String[] strArr = {String.valueOf(r20.A11)};
                    C16330op r11 = A02.A03;
                    boolean z2 = this instanceof C20600w1;
                    if (!z2) {
                        str3 = "message_add_on_receipt_device";
                    } else {
                        str3 = "receipt_device";
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(!z2 ? "message_add_on_row_id" : "message_row_id");
                    sb.append(" = ?");
                    String obj = sb.toString();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(!z2 ? "MessageAddOnReceiptDeviceStore/" : "MessageReceiptDeviceStore/");
                    sb2.append("addBlankReceiptsForTargetDevicesImpl/DELETE_RECEIPT_DEVICE");
                    sb2.toString();
                    r11.A01(str3, obj, strArr);
                }
                DeviceJid[] deviceJidArr = (DeviceJid[]) set.toArray(new DeviceJid[0]);
                int length = deviceJidArr.length;
                int min = Math.min(length, A06);
                AnonymousClass1YE r3 = null;
                while (r3 == null) {
                    try {
                        String A012 = A01(min);
                        C16330op r22 = A02.A03;
                        StringBuilder sb3 = new StringBuilder();
                        if (!(this instanceof C20600w1)) {
                            str2 = "MessageAddOnReceiptDeviceStore/";
                        } else {
                            str2 = "MessageReceiptDeviceStore/";
                        }
                        sb3.append(str2);
                        sb3.append("INSERT_DEVICE_RECEIPT_SQL");
                        sb3.toString();
                        r3 = r22.A0A(A012);
                    } catch (SQLiteException unused) {
                        A06 = Math.max(10, A06 - 10);
                        min /= 2;
                    }
                }
                while (length > 0) {
                    if (min > length) {
                        String A013 = A01(length);
                        C16330op r23 = A02.A03;
                        StringBuilder sb4 = new StringBuilder();
                        if (!(this instanceof C20600w1)) {
                            str = "MessageAddOnReceiptDeviceStore/";
                        } else {
                            str = "MessageReceiptDeviceStore/";
                        }
                        sb4.append(str);
                        sb4.append("INSERT_DEVICE_RECEIPT_SQL");
                        sb4.toString();
                        r3 = r23.A0A(A013);
                        min = length;
                    }
                    SQLiteStatement sQLiteStatement = r3.A00;
                    sQLiteStatement.clearBindings();
                    int i2 = i;
                    for (int i3 = 1; i3 <= min * 3; i3 += 3) {
                        r8.A00.put(deviceJidArr[i2], new C32621cS(0));
                        r3.A01(i3, r20.A11);
                        r3.A01(i3 + 1, this.A02.A01(deviceJidArr[i2]));
                        Long l = (Long) A01.get(deviceJidArr[i2].getUserJid());
                        if (l == null) {
                            sQLiteStatement.bindNull(i3 + 2);
                        } else {
                            r3.A01(i3 + 2, l.longValue());
                        }
                        i2++;
                    }
                    sQLiteStatement.execute();
                    i += min;
                    length -= min;
                }
                A00.A00();
                A02.A03(new RunnableBRunnable0Shape0S0300000_I0(this, r20, r8, 39));
                A00.close();
                A02.close();
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
                this.A03.A02();
            }
        }
    }

    public void A03(Set set) {
        String str;
        HashSet hashSet = new HashSet(set.size());
        Iterator it = set.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (deviceJid.device != 0) {
                hashSet.add(Long.toString(this.A02.A01(deviceJid)));
            }
        }
        if (!hashSet.isEmpty()) {
            StringBuilder sb = new StringBuilder("receipt_device_jid_row_id");
            sb.append(" IN ");
            sb.append(AnonymousClass1Ux.A00(hashSet.size()));
            sb.append(" AND ");
            sb.append("receipt_device_timestamp");
            sb.append(" IS NULL");
            String obj = sb.toString();
            C16310on A02 = this.A04.A02();
            try {
                C16330op r5 = A02.A03;
                boolean z = this instanceof C20600w1;
                if (!z) {
                    str = "message_add_on_receipt_device";
                } else {
                    str = "receipt_device";
                }
                String[] strArr = (String[]) hashSet.toArray(new String[0]);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(!z ? "MessageAddOnReceiptDeviceStore/" : "MessageReceiptDeviceStore/");
                sb2.append("deleteCompanionReceiptsForUndeliveredMessages/DELETE_RECEIPT_DEVICE");
                sb2.toString();
                int A01 = r5.A01(str, obj, strArr);
                A02.close();
                if (A01 > 0) {
                    this.A00.A06(-1);
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public boolean A04(com.whatsapp.jid.DeviceJid r9, X.AbstractC15340mz r10) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C20600w1
            if (r0 != 0) goto L_0x0072
            r3 = r8
            X.15x r3 = (X.C245415x) r3
            X.02y r2 = r3.A00
            long r0 = r10.A11
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Object r0 = r2.A04(r0)
            X.1cR r0 = (X.C32611cR) r0
            if (r0 != 0) goto L_0x00d2
            X.1IS r5 = r10.A0z
            X.0lm r1 = r5.A00
            X.AnonymousClass009.A05(r1)
            X.0p9 r0 = r3.A00
            long r6 = r0.A02(r1)
            X.0sU r0 = r3.A02
            long r0 = r0.A01(r9)
            java.lang.String r2 = java.lang.String.valueOf(r0)
            r0 = 4
            java.lang.String[] r4 = new java.lang.String[r0]
            java.lang.String r1 = java.lang.String.valueOf(r6)
            r0 = 0
            r4[r0] = r1
            r1 = 1
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x004a
            java.lang.String r0 = "1"
        L_0x003f:
            r4[r1] = r0
            r1 = 2
            java.lang.String r0 = r5.A01
            r4[r1] = r0
            r0 = 3
            r4[r0] = r2
            goto L_0x004d
        L_0x004a:
            java.lang.String r0 = "0"
            goto L_0x003f
        L_0x004d:
            X.0p7 r0 = r3.A04     // Catch: SQLiteDatabaseCorruptException -> 0x00c7
            X.0on r2 = r0.get()     // Catch: SQLiteDatabaseCorruptException -> 0x00c7
            X.0op r1 = r2.A03     // Catch: all -> 0x006d
            java.lang.String r0 = "SELECT receipt_device_jid_row_id FROM message_add_on JOIN message_add_on_receipt_device ON message_add_on._id = message_add_on_receipt_device.message_add_on_row_id WHERE chat_row_id = ? AND from_me = ? AND key_id = ? AND receipt_device_jid_row_id = ?"
            android.database.Cursor r1 = r1.A09(r0, r4)     // Catch: all -> 0x006d
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0066
            r1.close()     // Catch: all -> 0x006d
            r2.close()     // Catch: SQLiteDatabaseCorruptException -> 0x00c7
            return r0
        L_0x0066:
            r0 = move-exception
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch: all -> 0x006c
        L_0x006c:
            throw r0     // Catch: all -> 0x006d
        L_0x006d:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0071
        L_0x0071:
            throw r0     // Catch: SQLiteDatabaseCorruptException -> 0x00c7
        L_0x0072:
            X.02y r2 = r8.A00
            long r0 = r10.A11
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Object r0 = r2.A04(r0)
            X.1cR r0 = (X.C32611cR) r0
            if (r0 != 0) goto L_0x00d2
            long r0 = r10.A11
            java.lang.String r2 = java.lang.String.valueOf(r0)
            X.0sU r0 = r8.A02
            long r0 = r0.A01(r9)
            java.lang.String r1 = java.lang.String.valueOf(r0)
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]
            r0 = 0
            r3[r0] = r2
            r0 = 1
            r3[r0] = r1
            X.0p7 r0 = r8.A04     // Catch: SQLiteDatabaseCorruptException -> 0x00c0
            X.0on r2 = r0.get()     // Catch: SQLiteDatabaseCorruptException -> 0x00c0
            X.0op r1 = r2.A03     // Catch: all -> 0x00bb
            java.lang.String r0 = "SELECT receipt_device_jid_row_id FROM receipt_device WHERE message_row_id = ? AND receipt_device_jid_row_id = ?"
            android.database.Cursor r1 = r1.A09(r0, r3)     // Catch: all -> 0x00bb
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x00b4
            r1.close()     // Catch: all -> 0x00bb
            r2.close()     // Catch: SQLiteDatabaseCorruptException -> 0x00c0
            return r0
        L_0x00b4:
            r0 = move-exception
            if (r1 == 0) goto L_0x00ba
            r1.close()     // Catch: all -> 0x00ba
        L_0x00ba:
            throw r0     // Catch: all -> 0x00bb
        L_0x00bb:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x00bf
        L_0x00bf:
            throw r0     // Catch: SQLiteDatabaseCorruptException -> 0x00c0
        L_0x00c0:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            X.0wQ r0 = r8.A03
            goto L_0x00cd
        L_0x00c7:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            X.0wQ r0 = r3.A03
        L_0x00cd:
            r0.A02()
            r0 = 0
            return r0
        L_0x00d2:
            java.util.concurrent.ConcurrentHashMap r0 = r0.A00
            java.util.Set r0 = r0.keySet()
            boolean r0 = r0.contains(r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC20610w2.A04(com.whatsapp.jid.DeviceJid, X.0mz):boolean");
    }
}
