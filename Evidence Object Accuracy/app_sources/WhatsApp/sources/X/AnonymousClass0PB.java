package X;

/* renamed from: X.0PB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PB {
    public Long A00;
    public String A01;

    public AnonymousClass0PB(String str, long j) {
        this.A01 = str;
        this.A00 = Long.valueOf(j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof AnonymousClass0PB) {
            AnonymousClass0PB r5 = (AnonymousClass0PB) obj;
            if (this.A01.equals(r5.A01)) {
                Long l = this.A00;
                Long l2 = r5.A00;
                if (l != null) {
                    return l.equals(l2);
                }
                if (l2 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.A01.hashCode() * 31;
        Long l = this.A00;
        return hashCode + (l != null ? l.hashCode() : 0);
    }
}
