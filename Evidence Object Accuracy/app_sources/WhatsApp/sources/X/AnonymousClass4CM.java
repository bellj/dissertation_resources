package X;

/* renamed from: X.4CM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CM extends IllegalArgumentException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4CM(int r3, int r4) {
        /*
            r2 = this;
            r0 = 54
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "Unpaired surrogate at index "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " of "
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r4)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4CM.<init>(int, int):void");
    }
}
