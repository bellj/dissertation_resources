package X;

import android.content.Intent;
import android.os.PowerManager;
import java.util.List;

/* renamed from: X.0ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09340ck implements Runnable {
    public final /* synthetic */ C07620Zm A00;

    public RunnableC09340ck(C07620Zm r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C07620Zm r4 = this.A00;
        List list = r4.A09;
        synchronized (list) {
            r4.A00 = (Intent) list.get(0);
        }
        Intent intent = r4.A00;
        if (intent != null) {
            String action = intent.getAction();
            int intExtra = r4.A00.getIntExtra("KEY_START_ID", 0);
            C06390Tk A00 = C06390Tk.A00();
            String str = C07620Zm.A0A;
            Integer valueOf = Integer.valueOf(intExtra);
            A00.A02(str, String.format("Processing command %s, %s", r4.A00, valueOf), new Throwable[0]);
            PowerManager.WakeLock A002 = AnonymousClass0RM.A00(r4.A02, String.format("%s (%s)", action, valueOf));
            try {
                C06390Tk.A00().A02(str, String.format("Acquiring operation wake lock (%s) %s", action, A002), new Throwable[0]);
                A002.acquire();
                r4.A06.A00(r4.A00, r4, intExtra);
            } finally {
                try {
                    C06390Tk.A00().A02(str, String.format("Releasing operation wake lock (%s) %s", action, A002), new Throwable[0]);
                    A002.release();
                    r4.A03.post(new RunnableC09350cl(r4));
                } catch (Throwable th) {
                }
            }
            C06390Tk.A00().A02(str, String.format("Releasing operation wake lock (%s) %s", action, A002), new Throwable[0]);
            A002.release();
            r4.A03.post(new RunnableC09350cl(r4));
        }
    }
}
