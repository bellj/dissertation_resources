package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/* renamed from: X.22g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455922g extends AbstractC456022h {
    public static final long A03 = TimeUnit.DAYS.toMillis(7);
    public final long A00;
    public final String A01;
    public final String A02;

    public C455922g(UserJid userJid, String str, String str2, long j) {
        super(userJid);
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C455922g r7 = (C455922g) obj;
            if (((AbstractC455722e) this).A00 != ((AbstractC455722e) r7).A00 || !((AbstractC456022h) this).A00.getRawString().equals(((AbstractC456022h) r7).A00.getRawString()) || !this.A01.equals(r7.A01) || !this.A02.equals(r7.A02) || this.A00 != r7.A00) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(((AbstractC455722e) this).A00), ((AbstractC456022h) this).A00.getRawString(), this.A01, this.A02, Long.valueOf(this.A00)});
    }
}
