package X;

/* renamed from: X.31I  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31I extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public Long A09;
    public String A0A;

    public AnonymousClass31I() {
        super(1342, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A0A);
        r3.Abe(4, this.A00);
        r3.Abe(7, this.A04);
        r3.Abe(10, this.A05);
        r3.Abe(5, this.A01);
        r3.Abe(14, this.A06);
        r3.Abe(6, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(8, this.A07);
        r3.Abe(1, this.A08);
        r3.Abe(2, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamRegistrationComplete {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deviceIdentifier", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationAttemptSkipWithNoVertical", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationContactsPermissionSource", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationGoogleDriveBackupStatus", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationProfilePictureSet", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationProfilePictureSource", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationProfilePictureTapped", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationRetryFetchingBizProfile", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationStoragePermissionSource", C12960it.A0Y(this.A07));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationT", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "registrationTForFillBusinessInfoScreen", this.A09);
        return C12960it.A0d("}", A0k);
    }
}
