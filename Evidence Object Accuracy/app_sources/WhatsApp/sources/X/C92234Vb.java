package X;

/* renamed from: X.4Vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92234Vb {
    public final boolean A00;
    public final boolean A01;
    public final /* synthetic */ AnonymousClass192 A02;

    public C92234Vb(AnonymousClass192 r1, boolean z, boolean z2) {
        this.A02 = r1;
        this.A00 = z;
        this.A01 = z2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("PrepareResult{");
        A0k.append("doNetworkFetch=");
        A0k.append(this.A00);
        A0k.append(", hasDictionary=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
