package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* renamed from: X.1ZD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZD implements Parcelable {
    public static final Map A0D;
    public static final Parcelable.Creator CREATOR = new C100184lV();
    public long A00;
    public String A01;
    public String A02;
    public final AbstractC30791Yv A03;
    public final AnonymousClass1ZK A04;
    public final AnonymousClass1ZI A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final List A0A;
    public final boolean A0B;
    public final byte[] A0C;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        HashMap hashMap = new HashMap();
        A0D = hashMap;
        hashMap.put(1, new HashSet(Arrays.asList(2, 5, 6, 3, 4)));
        hashMap.put(2, new HashSet(Arrays.asList(5, 6, 3, 4)));
        hashMap.put(5, new HashSet(Arrays.asList(2, 6, 3, 4)));
        hashMap.put(6, new HashSet(Arrays.asList(2, 5, 3, 4)));
    }

    public AnonymousClass1ZD(AbstractC30791Yv r1, AnonymousClass1ZK r2, AnonymousClass1ZI r3, String str, String str2, String str3, String str4, String str5, String str6, List list, byte[] bArr, long j, boolean z) {
        this.A0C = bArr;
        this.A08 = str;
        this.A05 = r3;
        this.A07 = str2;
        this.A09 = str3;
        this.A03 = r1;
        this.A06 = str4;
        this.A04 = r2;
        this.A02 = str5;
        this.A01 = str6;
        this.A00 = j;
        this.A0B = z;
        this.A0A = list;
    }

    public AnonymousClass1ZD(Parcel parcel) {
        this.A0C = parcel.createByteArray();
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A08 = readString;
        Parcelable readParcelable = parcel.readParcelable(AnonymousClass1ZI.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A05 = (AnonymousClass1ZI) readParcelable;
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A07 = readString2;
        this.A09 = parcel.readString();
        this.A03 = AnonymousClass102.A00(parcel);
        String readString3 = parcel.readString();
        AnonymousClass009.A05(readString3);
        this.A06 = readString3;
        Parcelable readParcelable2 = parcel.readParcelable(AnonymousClass1ZK.class.getClassLoader());
        AnonymousClass009.A05(readParcelable2);
        this.A04 = (AnonymousClass1ZK) readParcelable2;
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A00 = parcel.readLong();
        this.A0B = parcel.readInt() != 1 ? false : true;
        ArrayList arrayList = new ArrayList();
        this.A0A = arrayList;
        parcel.readList(arrayList, AnonymousClass1ZJ.class.getClassLoader());
    }

    public static int A00(String str) {
        if ("pending".equals(str)) {
            return 1;
        }
        if ("processing".equals(str)) {
            return 2;
        }
        if ("completed".equals(str)) {
            return 3;
        }
        if ("canceled".equals(str)) {
            return 4;
        }
        if ("partially_shipped".equals(str)) {
            return 5;
        }
        if ("shipped".equals(str)) {
            return 6;
        }
        StringBuilder sb = new StringBuilder("CheckoutInfoContent/getOrderStatus can not recognise order status: ");
        sb.append(str);
        Log.w(sb.toString());
        return 0;
    }

    public AnonymousClass20C A01(AnonymousClass1ZI r6) {
        long abs = Math.abs(r6.A01);
        int i = r6.A00;
        AbstractC30791Yv r1 = this.A03;
        if (i <= 0) {
            return new AnonymousClass20C(r1, abs);
        }
        return new AnonymousClass20C(r1, i, abs);
    }

    public String A02(AnonymousClass018 r4) {
        String A03 = A03(r4, this.A05);
        if (A03 != null) {
            return A03;
        }
        AbstractC30791Yv r2 = this.A03;
        AnonymousClass009.A05(r2);
        return r2.AAB(r4, BigDecimal.ZERO, 0);
    }

    public String A03(AnonymousClass018 r6, AnonymousClass1ZI r7) {
        if (r7 == null || r7.A01 == 0) {
            return null;
        }
        AnonymousClass20C A01 = A01(r7);
        AbstractC30791Yv r2 = this.A03;
        AnonymousClass009.A05(r2);
        return r2.AAB(r6, A01.A02.A00, 0);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.A0C);
        parcel.writeString(this.A08);
        parcel.writeParcelable(this.A05, i);
        parcel.writeString(this.A07);
        parcel.writeString(this.A09);
        this.A03.writeToParcel(parcel, i);
        parcel.writeString(this.A06);
        parcel.writeParcelable(this.A04, i);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeInt(this.A0B ? 1 : 0);
        parcel.writeList(this.A0A);
    }
}
