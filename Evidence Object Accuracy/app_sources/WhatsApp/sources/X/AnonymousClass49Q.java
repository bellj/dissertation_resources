package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49Q  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49Q extends Enum {
    public static final AnonymousClass49Q A00 = new AnonymousClass49Q("EXACT_MATCH", 4);
    public static final AnonymousClass49Q A01 = new AnonymousClass49Q("NOT_A_NUMBER", 0);
    public static final AnonymousClass49Q A02 = new AnonymousClass49Q("NO_MATCH", 1);
    public static final AnonymousClass49Q A03 = new AnonymousClass49Q("NSN_MATCH", 3);
    public static final AnonymousClass49Q A04 = new AnonymousClass49Q("SHORT_NSN_MATCH", 2);

    public AnonymousClass49Q(String str, int i) {
    }
}
