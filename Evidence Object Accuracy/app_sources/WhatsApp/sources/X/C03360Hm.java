package X;

/* renamed from: X.0Hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03360Hm extends AbstractC08890c1 {
    public static final C03360Hm A01 = new C03360Hm(-16777216);
    public static final C03360Hm A02 = new C03360Hm(0);
    public int A00;

    public C03360Hm(int i) {
        this.A00 = i;
    }

    @Override // java.lang.Object
    public String toString() {
        return String.format("#%08x", Integer.valueOf(this.A00));
    }
}
