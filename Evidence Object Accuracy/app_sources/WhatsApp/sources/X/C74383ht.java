package X;

import android.view.View;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.3ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74383ht extends AnonymousClass04v {
    public final /* synthetic */ MediaViewFragment A00;
    public final /* synthetic */ ExoPlaybackControlView A01;

    public C74383ht(MediaViewFragment mediaViewFragment, ExoPlaybackControlView exoPlaybackControlView) {
        this.A00 = mediaViewFragment;
        this.A01 = exoPlaybackControlView;
    }

    @Override // X.AnonymousClass04v
    public void A00(View view, int i) {
        if (i == 256) {
            ExoPlaybackControlView exoPlaybackControlView = this.A01;
            if (exoPlaybackControlView.A07()) {
                exoPlaybackControlView.A00();
                return;
            }
            exoPlaybackControlView.A01();
            exoPlaybackControlView.A06(3000);
            return;
        }
        super.A00(view, i);
    }
}
