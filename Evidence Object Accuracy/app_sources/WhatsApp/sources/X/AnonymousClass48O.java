package X;

import com.whatsapp.R;

/* renamed from: X.48O  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass48O extends AbstractC93424a9 {
    public final EnumC87274Aw A00;

    public AnonymousClass48O(EnumC87274Aw r7) {
        super(new C92794Xl(R.dimen.wds_badge_icon_dimen_12, R.dimen.wds_badge_icon_dimen_14, R.dimen.wds_badge_icon_dimen_16, R.dimen.wds_badge_icon_dimen_24), new AnonymousClass48H());
        this.A00 = r7;
    }
}
