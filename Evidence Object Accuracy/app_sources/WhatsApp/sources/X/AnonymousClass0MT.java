package X;

import android.content.ClipData;
import android.os.Build;

/* renamed from: X.0MT  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0MT {
    public final AbstractC12620iD A00;

    public AnonymousClass0MT(ClipData clipData, int i) {
        if (Build.VERSION.SDK_INT >= 31) {
            this.A00 = new C07440Xz(clipData, i);
        } else {
            this.A00 = new AnonymousClass0Y0(clipData, i);
        }
    }
}
