package X;

import android.content.SharedPreferences;

/* renamed from: X.17D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17D {
    public final C16630pM A00;
    public volatile SharedPreferences A01;

    public AnonymousClass17D(C16630pM r1) {
        this.A00 = r1;
    }

    public final SharedPreferences A00() {
        if (this.A01 == null) {
            synchronized (AnonymousClass17D.class) {
                if (this.A01 == null) {
                    this.A01 = this.A00.A01("fb_credentials_prefs");
                }
            }
        }
        return this.A01;
    }

    public void A01(String str) {
        synchronized (AnonymousClass17D.class) {
            A00().edit().putString("pref_fb_user_credentials_encrypted", str).apply();
        }
    }
}
