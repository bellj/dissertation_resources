package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KZ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KZ implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass662 A01;

    public AnonymousClass6KZ(AnonymousClass662 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0100, code lost:
        if (r2 >= r1) goto L_0x0102;
     */
    @Override // java.util.concurrent.Callable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object call() {
        /*
        // Method dump skipped, instructions count: 276
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6KZ.call():java.lang.Object");
    }
}
