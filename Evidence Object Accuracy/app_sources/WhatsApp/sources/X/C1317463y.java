package X;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.payments.ui.IndiaUpiDebitCardVerificationActivity;

/* renamed from: X.63y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1317463y implements TextWatcher {
    public final int A00;
    public final EditText A01;
    public final /* synthetic */ IndiaUpiDebitCardVerificationActivity A02;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C1317463y(EditText editText, IndiaUpiDebitCardVerificationActivity indiaUpiDebitCardVerificationActivity, int i) {
        this.A02 = indiaUpiDebitCardVerificationActivity;
        this.A00 = i;
        this.A01 = editText;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int parseInt;
        if (i3 != 0) {
            this.A02.A06.setVisibility(4);
        }
        IndiaUpiDebitCardVerificationActivity indiaUpiDebitCardVerificationActivity = this.A02;
        int A3I = indiaUpiDebitCardVerificationActivity.A3I();
        Button button = indiaUpiDebitCardVerificationActivity.A07;
        if (A3I == 0) {
            button.setEnabled(true);
        } else {
            button.setEnabled(false);
        }
        if (charSequence.length() >= this.A00) {
            EditText editText = this.A01;
            if (editText != null) {
                editText.requestFocus();
                if (editText == indiaUpiDebitCardVerificationActivity.A05) {
                    EditText editText2 = indiaUpiDebitCardVerificationActivity.A04;
                    if (!TextUtils.isEmpty(C117295Zj.A0T(editText2)) && (parseInt = Integer.parseInt(C117295Zj.A0T(editText2))) != -1) {
                        if (parseInt < 1 || parseInt > 12) {
                            indiaUpiDebitCardVerificationActivity.A06.setText(R.string.debit_card_expiry_invalid_month_error_text);
                            indiaUpiDebitCardVerificationActivity.A3J();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            indiaUpiDebitCardVerificationActivity.A3L(indiaUpiDebitCardVerificationActivity.A00, indiaUpiDebitCardVerificationActivity.A01, false);
        }
    }
}
