package X;

/* renamed from: X.4Ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88244Ev {
    public static final String A00(AnonymousClass5WO r3) {
        Object obj;
        if (r3 instanceof C114205Kp) {
            return r3.toString();
        }
        try {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(r3);
            A0h.append('@');
            obj = C12960it.A0d(C72453ed.A0o(r3), A0h);
        } catch (Throwable th) {
            obj = new AnonymousClass5BR(th);
        }
        if (AnonymousClass5BU.A00(obj) != null) {
            StringBuilder A0h2 = C12960it.A0h();
            A0h2.append((Object) C12980iv.A0s(r3));
            A0h2.append('@');
            obj = C12960it.A0d(C72453ed.A0o(r3), A0h2);
        }
        return (String) obj;
    }
}
