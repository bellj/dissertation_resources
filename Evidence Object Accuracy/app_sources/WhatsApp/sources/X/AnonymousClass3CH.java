package X;

import com.whatsapp.Conversation;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3CH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CH {
    public final /* synthetic */ Conversation A00;

    public AnonymousClass3CH(Conversation conversation) {
        this.A00 = conversation;
    }

    public void A00(AnonymousClass2KT r17, C92664Wv r18) {
        Conversation conversation = this.A00;
        String str = r18.A00;
        if (conversation.A1O.A0I(C15370n3.A05(conversation.A2c))) {
            C36021jC.A01(conversation, 106);
        } else {
            C32361c2 A00 = conversation.A1z.A00(r17, conversation.A2D);
            C16170oZ r4 = ((AbstractActivityC13750kH) conversation).A03;
            List singletonList = Collections.singletonList(conversation.A2c.A0B(AbstractC14640lm.class));
            r4.A07(r17, conversation.A21, null, A00, conversation.A20.A07, AbstractC32741cf.A04(str), singletonList, Collections.emptyList(), conversation.A4L, false, true);
        }
        conversation.A3K(true);
        C14310lE r2 = conversation.A21;
        r2.A09(r2.A05);
        conversation.A0S.setVisibility(8);
    }
}
