package X;

import com.facebook.redex.EmptyBaseRunnable0;
import java.util.concurrent.locks.Lock;

/* renamed from: X.3lQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractRunnableC76303lQ extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ C108324ys A00;

    public /* synthetic */ AbstractRunnableC76303lQ(C108324ys r1) {
        this.A00 = r1;
    }

    public abstract void A00();

    @Override // java.lang.Runnable
    public final void run() {
        Lock lock;
        try {
            C108324ys r1 = this.A00;
            lock = r1.A0K;
            lock.lock();
            try {
                if (!Thread.interrupted()) {
                    A00();
                }
            } catch (RuntimeException e) {
                HandlerC79083q1 r12 = r1.A0F.A06;
                r12.sendMessage(r12.obtainMessage(2, e));
            }
        } finally {
            lock.unlock();
        }
    }
}
