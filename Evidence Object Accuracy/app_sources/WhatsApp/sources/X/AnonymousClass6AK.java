package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.6AK  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AK implements AnonymousClass6MO {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;
    public final /* synthetic */ String A02;

    public AnonymousClass6AK(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, String str) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
        this.A02 = str;
    }

    @Override // X.AnonymousClass6MO
    public void ANl(C30881Ze r4) {
        Log.i("PAY: BrazilPayBloksActivity BrazilGetVerificationMethods - onCardVerified");
        ((AbstractActivityC121705jc) this.A01).A0G.A00().A03(new AbstractC451720l(this.A00, this) { // from class: X.67s
            public final /* synthetic */ AnonymousClass3FE A00;
            public final /* synthetic */ AnonymousClass6AK A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                AnonymousClass6AK r1 = this.A01;
                AnonymousClass3FE r3 = this.A00;
                if (list.size() > 0) {
                    r3.A01("on_success", r1.A01.A0L.A02((C30881Ze) C12980iv.A0o(list), null));
                }
            }
        }, r4);
    }

    @Override // X.AnonymousClass6MO
    public void AVP(C452120p r7, ArrayList arrayList) {
        int i;
        if (r7 != null) {
            i = r7.A00;
        } else if (arrayList != null && !arrayList.isEmpty()) {
            BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
            if (C1309660r.A01(arrayList)) {
                i = -233;
            } else {
                JSONArray A02 = brazilPayBloksActivity.A07.A02(arrayList);
                C30881Ze r3 = (C30881Ze) brazilPayBloksActivity.A05.A08(this.A02);
                if (A02 != null) {
                    this.A00.A01("on_success", brazilPayBloksActivity.A0L.A02(r3, A02.toString()));
                    return;
                }
                return;
            }
        } else {
            return;
        }
        AbstractActivityC121705jc.A0l(this.A00, null, i);
    }
}
