package X;

import java.util.Arrays;
import java.util.LinkedList;

/* renamed from: X.4bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94314bb {
    public final AnonymousClass4YU A00;
    public final LinkedList A01;

    public C94314bb(AnonymousClass4YU r1, LinkedList linkedList) {
        this.A01 = linkedList;
        this.A00 = r1;
    }

    public static C92574Wl A00(String str, AnonymousClass5T6... r5) {
        try {
            AnonymousClass4YU r3 = new AnonymousClass4YU(str);
            r3.A05();
            CharSequence charSequence = r3.A02;
            if (!(charSequence.charAt(0) == '$' || charSequence.charAt(0) == '@')) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("$.");
                r3 = new AnonymousClass4YU(C12960it.A0d(str, A0h));
                r3.A05();
            }
            if (r3.A02.charAt(r3.A00) != '.') {
                return new C94314bb(r3, new LinkedList(Arrays.asList(r5))).A01();
            }
            throw C82843wH.A00("Path must not end with a '.' or '..'");
        } catch (Exception e) {
            if (!(e instanceof C82843wH)) {
                throw new C82843wH(e);
            }
            throw e;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        if (r2 == '@') goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C92574Wl A01() {
        /*
            r5 = this;
        L_0x0000:
            X.4YU r3 = r5.A00
            int r1 = r3.A01
            boolean r0 = r3.A0A(r1)
            if (r0 == 0) goto L_0x0025
            java.lang.CharSequence r0 = r3.A02
            char r1 = r0.charAt(r1)
            r0 = 32
            if (r1 == r0) goto L_0x0020
            r0 = 9
            if (r1 == r0) goto L_0x0020
            r0 = 10
            if (r1 == r0) goto L_0x0020
            r0 = 13
            if (r1 != r0) goto L_0x0025
        L_0x0020:
            r0 = 1
            r3.A07(r0)
            goto L_0x0000
        L_0x0025:
            java.lang.CharSequence r4 = r3.A02
            int r0 = r3.A01
            char r2 = r4.charAt(r0)
            r0 = 36
            if (r2 == r0) goto L_0x0036
            r1 = 64
            r0 = 0
            if (r2 != r1) goto L_0x0037
        L_0x0036:
            r0 = 1
        L_0x0037:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0096
            int r0 = r3.A01
            char r0 = r4.charAt(r0)
            X.3wl r2 = new X.3wl
            r2.<init>(r0)
            int r1 = r3.A01
            int r0 = r3.A00
            if (r1 >= r0) goto L_0x0088
            r0 = 1
            r3.A07(r0)
            int r0 = r3.A01
            char r1 = r4.charAt(r0)
            r0 = 46
            if (r1 == r0) goto L_0x0080
            int r0 = r3.A01
            char r1 = r4.charAt(r0)
            r0 = 91
            if (r1 == r0) goto L_0x0080
            java.lang.String r0 = "Illegal character at position "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            int r0 = r3.A01
            r1.append(r0)
            java.lang.String r0 = " expected '.' or '['"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            X.3wH r0 = X.C82843wH.A00(r0)
            throw r0
        L_0x0080:
            X.4Ut r0 = new X.4Ut
            r0.<init>(r2)
            r5.A02(r0)
        L_0x0088:
            java.lang.String r1 = r2.A02
            java.lang.String r0 = "$"
            boolean r1 = r1.equals(r0)
            X.4Wl r0 = new X.4Wl
            r0.<init>(r2, r1)
            return r0
        L_0x0096:
            java.lang.String r0 = "Path must start with '$' or '@'"
            X.3wH r0 = X.C82843wH.A00(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94314bb.A01():X.4Wl");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0256, code lost:
        if (r0 == false) goto L_0x0258;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x05cb, code lost:
        if (r21 != 0) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x05cd, code lost:
        if (r2 != 0) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x05cf, code lost:
        if (r19 != 0) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:351:0x05e3, code lost:
        if (r11.A01 >= r11.A00) goto L_0x05e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x05f6, code lost:
        r1 = X.C12960it.A0k("Arguments to function: '");
        r1.append(r23);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x060b, code lost:
        throw X.C82843wH.A00(X.C12960it.A0d("' are not closed properly.", r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00d4, code lost:
        if (r1 == '@') goto L_0x00d6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0224  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0319 A[LOOP:4: B:194:0x0304->B:202:0x0319, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x051c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:389:0x03a8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0115  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A02(X.C92154Ut r26) {
        /*
        // Method dump skipped, instructions count: 1556
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94314bb.A02(X.4Ut):boolean");
    }

    public final boolean A03(C92154Ut r7) {
        AnonymousClass4YU r4 = this.A00;
        boolean A08 = r4.A08('[');
        if (A08 && !r4.A09('*', r4.A01)) {
            return false;
        }
        if (!r4.A08('*') && (!r4.A0A(r4.A01 + 1))) {
            return false;
        }
        if (A08) {
            int A02 = r4.A02('*', r4.A01);
            if (r4.A09(']', A02)) {
                r4.A01 = r4.A02(']', A02) + 1;
            } else {
                throw C82843wH.A00(C12960it.A0W(A02 + 1, "Expected wildcard token to end with ']' on position "));
            }
        } else {
            r4.A07(1);
        }
        r7.A00(new C83113wi());
        if (r4.A01 >= r4.A00) {
            return true;
        }
        A02(r7);
        return true;
    }
}
