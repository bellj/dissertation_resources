package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120865gs extends C120895gv {
    public final /* synthetic */ AnonymousClass60I A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120865gs(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, AnonymousClass60I r11) {
        super(context, r8, r9, r10, "upi-bind-device");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        Log.i(C12960it.A0b("PAY: IndiaUpiGetBankAccountsAction: sendGetBankAccounts: onRequestError: ", r2));
        A05(r2);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        Log.i(C12960it.A0b("PAY: IndiaUpiGetBankAccountsAction: sendGetBankAccounts: onResponseError: ", r2));
        A05(r2);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r7) {
        super.A04(r7);
        AnonymousClass60I r5 = this.A00;
        String A03 = r5.A0G.A03();
        C1329668y r0 = r5.A0B;
        String str = r5.A04;
        String str2 = r5.A05;
        r0.A0I(str, str2, A03);
        StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiGetBankAccountsAction processSuccess: device binding done. stored psp: ");
        A0k.append(str);
        A0k.append(" seqNumPrefix: ");
        A0k.append(str2);
        A0k.append(" bindID: ");
        Log.i(C12960it.A0d(C1309060l.A01(A03), A0k));
        AnonymousClass6MR r1 = r5.A02;
        if (r1 != null) {
            r1.AP4(null);
        }
    }

    public final void A05(C452120p r7) {
        AnonymousClass60I r5 = this.A00;
        if (r5.A02 != null) {
            if (r7.A00 == 11453) {
                String A03 = r5.A0G.A03();
                C1329668y r0 = r5.A0B;
                String str = r5.A04;
                String str2 = r5.A05;
                r0.A0I(str, str2, A03);
                StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiGetBankAccountsAction processError: device binding already done. stored psp: ");
                A0k.append(str);
                A0k.append(" seqNumPrefix: ");
                A0k.append(str2);
                A0k.append(" bindID: ");
                Log.i(C12960it.A0d(C1309060l.A01(A03), A0k));
            }
            r5.A02.AP4(r7);
        }
    }
}
