package X;

import com.whatsapp.util.Log;

/* renamed from: X.0yH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21970yH {
    public final C16590pI A00;
    public final C18810t5 A01;
    public final C18800t4 A02;

    public C21970yH(C16590pI r1, C18810t5 r2, C18800t4 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public final AbstractC37631mk A00(C37581mf r5, String str) {
        AbstractC37631mk A00 = r5.A00(this.A02, str, null);
        if (A00.A7O() >= 400) {
            StringBuilder sb = new StringBuilder("WallpaperDownloader/download/Error, code=");
            sb.append(A00.A7O());
            Log.e(sb.toString());
            return null;
        }
        A00.A7O();
        return A00;
    }
}
