package X;

import com.whatsapp.R;
import com.whatsapp.migration.android.integration.service.GoogleMigrateService;
import com.whatsapp.util.Log;

/* renamed from: X.3YV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YV implements AnonymousClass5XP {
    public final /* synthetic */ GoogleMigrateService A00;

    public AnonymousClass3YV(GoogleMigrateService googleMigrateService) {
        this.A00 = googleMigrateService;
    }

    @Override // X.AnonymousClass5XP
    public void ANg() {
        C26451Dk r2 = this.A00.A04;
        Log.i("GoogleMigrateNotificationManager/onCancellationComplete()");
        r2.A02(C16590pI.A00(r2.A00).getString(R.string.google_migrate_notification_import_cancelled), null, -1, true, true);
    }

    @Override // X.AnonymousClass5XP
    public void ANh() {
        C26451Dk r2 = this.A00.A04;
        Log.i("GoogleMigrateNotificationManager/onCancelling()");
        r2.A02(C16590pI.A00(r2.A00).getString(R.string.google_migrate_notification_cancelling_import), null, -1, false, false);
    }

    @Override // X.AnonymousClass5XP
    public void AOQ(boolean z) {
        StringBuilder A0k = C12960it.A0k("GoogleMigrateService/onComplete/success = ");
        A0k.append(z);
        C12960it.A1F(A0k);
        if (z) {
            GoogleMigrateService googleMigrateService = this.A00;
            C26451Dk r3 = googleMigrateService.A04;
            Log.i("GoogleMigrateNotificationManager/onComplete()");
            r3.A02(C16590pI.A00(r3.A00).getString(R.string.google_migrate_notification_import_completed), null, -1, true, false);
            Log.i("GoogleMigrateService/onComplete/sent import complete logging");
            googleMigrateService.A05.A01("google_migrate_import_complete", "google_migrate_import_complete_next");
        }
    }

    @Override // X.AnonymousClass5XP
    public void APl(int i) {
        Log.i(C12960it.A0W(i, "GoogleMigrateService/onError/errorCode = "));
        C26451Dk r3 = this.A00.A04;
        C16590pI r2 = r3.A00;
        r3.A02(C16590pI.A00(r2).getString(R.string.google_migrate_notification_import_failed), C16590pI.A00(r2).getString(R.string.google_migrate_notification_import_failed_detail), -1, true, false);
    }

    @Override // X.AnonymousClass5XP
    public void ARJ() {
        this.A00.A04.A01(0);
    }

    @Override // X.AnonymousClass5XP
    public void AU8(int i) {
        if (i != 301 && i != 104 && i != 101) {
            this.A00.A04.A01(0);
        }
    }

    @Override // X.AnonymousClass5XP
    public void AU9() {
        C26451Dk r2 = this.A00.A04;
        r2.A02(C16590pI.A00(r2.A00).getString(R.string.loading_spinner), null, -1, true, false);
    }

    @Override // X.AnonymousClass5XP
    public void AUL(int i) {
        Log.i(C12960it.A0W(i, "GoogleMigrateService/onProgress; progress="));
        this.A00.A04.A01(i);
    }
}
