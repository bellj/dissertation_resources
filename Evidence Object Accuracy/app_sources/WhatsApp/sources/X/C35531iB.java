package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1iB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35531iB {
    public final List A00;
    public final List A01;
    public final List A02;
    public final List A03;
    public final List A04;

    public C35531iB() {
        this.A02 = new ArrayList();
        this.A00 = new ArrayList();
        this.A03 = new ArrayList();
        this.A04 = new ArrayList();
        this.A01 = new ArrayList();
    }

    public C35531iB(List list, List list2, List list3, List list4, List list5) {
        this.A02 = list;
        this.A00 = list2;
        this.A03 = list3;
        this.A04 = list4;
        this.A01 = list5;
    }

    public static C35531iB A00() {
        return new C35531iB();
    }

    public C35531iB A01() {
        return new C35531iB(new ArrayList(this.A02), new ArrayList(this.A00), new ArrayList(this.A03), new ArrayList(this.A04), new ArrayList(this.A01));
    }
}
