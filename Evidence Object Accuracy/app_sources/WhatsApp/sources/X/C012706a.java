package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/* renamed from: X.06a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C012706a extends AnonymousClass05K {
    @Override // X.AnonymousClass05K
    public Intent A00(Context context, Object obj) {
        Bundle bundleExtra;
        C06800Vd r7 = (C06800Vd) obj;
        Intent intent = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST");
        Intent intent2 = r7.A02;
        if (!(intent2 == null || (bundleExtra = intent2.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) == null)) {
            intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundleExtra);
            intent2.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
            if (intent2.getBooleanExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", false)) {
                r7 = new C06800Vd(null, r7.A03, r7.A00, r7.A01);
            }
        }
        intent.putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", r7);
        if (AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("CreateIntent created the following intent: ");
            sb.append(intent);
            Log.v("FragmentManager", sb.toString());
        }
        return intent;
    }

    @Override // X.AnonymousClass05K
    public Object A02(Intent intent, int i) {
        return new C06830Vg(i, intent);
    }
}
