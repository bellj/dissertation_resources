package X;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.37j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624737j extends AbstractC16350or {
    public final C15610nY A00;
    public final AnonymousClass118 A01;
    public final WeakReference A02;
    public final ArrayList A03;
    public final List A04;

    public C624737j(C15610nY r2, AbstractActivityC36611kC r3, AnonymousClass118 r4, List list, List list2) {
        ArrayList arrayList;
        this.A00 = r2;
        this.A01 = r4;
        if (list != null) {
            arrayList = C12980iv.A0x(list);
        } else {
            arrayList = null;
        }
        this.A03 = arrayList;
        this.A04 = list2;
        this.A02 = C12970iu.A10(r3);
    }
}
