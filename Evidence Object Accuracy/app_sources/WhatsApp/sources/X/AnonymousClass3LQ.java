package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;

/* renamed from: X.3LQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LQ implements SensorEventListener {
    public final /* synthetic */ C618632v A00;

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public AnonymousClass3LQ(C618632v r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0138, code lost:
        if (r3 != 3) goto L_0x013a;
     */
    @Override // android.hardware.SensorEventListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSensorChanged(android.hardware.SensorEvent r19) {
        /*
        // Method dump skipped, instructions count: 373
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3LQ.onSensorChanged(android.hardware.SensorEvent):void");
    }
}
