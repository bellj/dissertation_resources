package X;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import java.util.List;

/* renamed from: X.0aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08000aQ implements AbstractC12850if, AbstractC12030hG, AbstractC12870ih {
    public AnonymousClass0OG A00 = new AnonymousClass0OG();
    public boolean A01;
    public final Path A02 = new Path();
    public final RectF A03 = new RectF();
    public final AnonymousClass0AA A04;
    public final AnonymousClass0QR A05;
    public final AnonymousClass0QR A06;
    public final AnonymousClass0QR A07;
    public final String A08;
    public final boolean A09;

    public C08000aQ(AnonymousClass0AA r5, C08240ao r6, AbstractC08070aX r7) {
        this.A08 = r6.A03;
        this.A09 = r6.A04;
        this.A04 = r5;
        AnonymousClass0QR A88 = r6.A01.A88();
        this.A06 = A88;
        AnonymousClass0QR A882 = r6.A02.A88();
        this.A07 = A882;
        AnonymousClass0H1 r1 = new AnonymousClass0H1(r6.A00.A00);
        this.A05 = r1;
        r7.A03(A88);
        r7.A03(A882);
        r7.A03(r1);
        A88.A07.add(this);
        A882.A07.add(this);
        r1.A07.add(this);
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A03) {
            r0 = this.A07;
        } else if (obj == AbstractC12810iX.A02) {
            r0 = this.A06;
        } else if (obj == AbstractC12810iX.A07) {
            r0 = this.A05;
        } else {
            return;
        }
        r0.A08(r2);
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        float A09;
        boolean z = this.A01;
        Path path = this.A02;
        if (!z) {
            path.reset();
            if (!this.A09) {
                PointF pointF = (PointF) this.A07.A03();
                float f = pointF.x / 2.0f;
                float f2 = pointF.y / 2.0f;
                AnonymousClass0QR r0 = this.A05;
                if (r0 == null) {
                    A09 = 0.0f;
                } else {
                    A09 = ((AnonymousClass0H1) r0).A09();
                }
                float min = Math.min(f, f2);
                if (A09 > min) {
                    A09 = min;
                }
                PointF pointF2 = (PointF) this.A06.A03();
                path.moveTo(pointF2.x + f, (pointF2.y - f2) + A09);
                path.lineTo(pointF2.x + f, (pointF2.y + f2) - A09);
                if (A09 > 0.0f) {
                    RectF rectF = this.A03;
                    float f3 = pointF2.x + f;
                    float f4 = A09 * 2.0f;
                    float f5 = pointF2.y + f2;
                    rectF.set(f3 - f4, f5 - f4, f3, f5);
                    path.arcTo(rectF, 0.0f, 90.0f, false);
                }
                path.lineTo((pointF2.x - f) + A09, pointF2.y + f2);
                if (A09 > 0.0f) {
                    RectF rectF2 = this.A03;
                    float f6 = pointF2.x - f;
                    float f7 = pointF2.y + f2;
                    float f8 = A09 * 2.0f;
                    rectF2.set(f6, f7 - f8, f8 + f6, f7);
                    path.arcTo(rectF2, 90.0f, 90.0f, false);
                }
                path.lineTo(pointF2.x - f, (pointF2.y - f2) + A09);
                if (A09 > 0.0f) {
                    RectF rectF3 = this.A03;
                    float f9 = pointF2.x - f;
                    float f10 = pointF2.y - f2;
                    float f11 = A09 * 2.0f;
                    rectF3.set(f9, f10, f9 + f11, f11 + f10);
                    path.arcTo(rectF3, 180.0f, 90.0f, false);
                }
                path.lineTo((pointF2.x + f) - A09, pointF2.y - f2);
                if (A09 > 0.0f) {
                    RectF rectF4 = this.A03;
                    float f12 = pointF2.x + f;
                    float f13 = A09 * 2.0f;
                    float f14 = pointF2.y - f2;
                    rectF4.set(f12 - f13, f14, f12, f14 + f13);
                    path.arcTo(rectF4, 270.0f, 90.0f, false);
                }
                path.close();
                this.A00.A00(path);
            }
            this.A01 = true;
        }
        return path;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A01 = false;
        this.A04.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list.size(); i++) {
            AbstractC12470hy r2 = (AbstractC12470hy) list.get(i);
            if (r2 instanceof C07950aL) {
                C07950aL r22 = (C07950aL) r2;
                if (r22.A03 == AnonymousClass0J5.SIMULTANEOUSLY) {
                    this.A00.A00.add(r22);
                    r22.A05.add(this);
                }
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A08;
    }
}
