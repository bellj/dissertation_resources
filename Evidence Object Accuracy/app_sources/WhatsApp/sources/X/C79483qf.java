package X;

import java.util.Arrays;

/* renamed from: X.3qf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79483qf extends C79503qh implements Cloneable {
    public int[] A00 = C88764Ha.A01;
    public long[] A01;
    public long[] A02;
    public String[] A03;
    public String[] A04;

    public C79483qf() {
        String[] strArr = C88764Ha.A03;
        this.A03 = strArr;
        this.A04 = strArr;
        long[] jArr = C88764Ha.A02;
        this.A01 = jArr;
        this.A02 = jArr;
        ((C79503qh) this).A00 = null;
        ((AbstractC94974cq) this).A00 = -1;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final int A03() {
        int length;
        int length2;
        int length3;
        int i;
        int A03 = super.A03();
        String[] strArr = this.A03;
        int i2 = 0;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (true) {
                String[] strArr2 = this.A03;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i3];
                if (str != null) {
                    i5++;
                    int A032 = C95484do.A03(str);
                    i4 += C72453ed.A07(A032) + A032;
                }
                i3++;
            }
            A03 = A03 + i4 + i5;
        }
        String[] strArr3 = this.A04;
        if (strArr3 != null && strArr3.length > 0) {
            int i6 = 0;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                String[] strArr4 = this.A04;
                if (i6 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i6];
                if (str2 != null) {
                    i8++;
                    int A033 = C95484do.A03(str2);
                    i7 += C72453ed.A07(A033) + A033;
                }
                i6++;
            }
            A03 = A03 + i7 + i8;
        }
        int[] iArr = this.A00;
        if (iArr != null && (length3 = iArr.length) > 0) {
            int i9 = 0;
            int i10 = 0;
            do {
                int i11 = iArr[i9];
                if (i11 >= 0) {
                    i = C72453ed.A07(i11);
                } else {
                    i = 10;
                }
                i10 += i;
                i9++;
            } while (i9 < length3);
            A03 = A03 + i10 + length3;
        }
        long[] jArr = this.A01;
        if (jArr != null && (length2 = jArr.length) > 0) {
            int i12 = 0;
            int i13 = 0;
            do {
                i13 += C95484do.A02(jArr[i12]);
                i12++;
            } while (i12 < length2);
            A03 = A03 + i13 + length2;
        }
        long[] jArr2 = this.A02;
        if (jArr2 == null || (length = jArr2.length) <= 0) {
            return A03;
        }
        int i14 = 0;
        do {
            i14 += C95484do.A02(jArr2[i2]);
            i2++;
        } while (i2 < length);
        return A03 + i14 + length;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final void A05(C95484do r6) {
        String[] strArr = this.A03;
        int i = 0;
        if (strArr != null && strArr.length > 0) {
            int i2 = 0;
            while (true) {
                String[] strArr2 = this.A03;
                if (i2 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i2];
                if (str != null) {
                    r6.A07(1, str);
                }
                i2++;
            }
        }
        String[] strArr3 = this.A04;
        if (strArr3 != null && strArr3.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr4 = this.A04;
                if (i3 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i3];
                if (str2 != null) {
                    r6.A07(2, str2);
                }
                i3++;
            }
        }
        int[] iArr = this.A00;
        if (iArr != null && iArr.length > 0) {
            int i4 = 0;
            while (true) {
                int[] iArr2 = this.A00;
                if (i4 >= iArr2.length) {
                    break;
                }
                int i5 = iArr2[i4];
                r6.A05(24);
                if (i5 >= 0) {
                    r6.A06(i5);
                } else {
                    r6.A09((long) i5);
                }
                i4++;
            }
        }
        long[] jArr = this.A01;
        if (jArr != null && jArr.length > 0) {
            int i6 = 0;
            while (true) {
                long[] jArr2 = this.A01;
                if (i6 >= jArr2.length) {
                    break;
                }
                long j = jArr2[i6];
                r6.A05(32);
                r6.A09(j);
                i6++;
            }
        }
        long[] jArr3 = this.A02;
        if (jArr3 != null && jArr3.length > 0) {
            while (true) {
                long[] jArr4 = this.A02;
                if (i >= jArr4.length) {
                    break;
                }
                long j2 = jArr4[i];
                r6.A05(40);
                r6.A09(j2);
                i++;
            }
        }
        super.A05(r6);
    }

    @Override // X.AbstractC94974cq, java.lang.Object
    public final /* synthetic */ Object clone() {
        try {
            C79483qf r2 = (C79483qf) super.A06();
            String[] strArr = this.A03;
            if (strArr != null && strArr.length > 0) {
                r2.A03 = (String[]) strArr.clone();
            }
            String[] strArr2 = this.A04;
            if (strArr2 != null && strArr2.length > 0) {
                r2.A04 = (String[]) strArr2.clone();
            }
            int[] iArr = this.A00;
            if (iArr != null && iArr.length > 0) {
                r2.A00 = (int[]) iArr.clone();
            }
            long[] jArr = this.A01;
            if (jArr != null && jArr.length > 0) {
                r2.A01 = (long[]) jArr.clone();
            }
            long[] jArr2 = this.A02;
            if (jArr2 != null && jArr2.length > 0) {
                r2.A02 = (long[]) jArr2.clone();
            }
            return r2;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0046, code lost:
        if (r1.length != 0) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005b, code lost:
        if (r1.length != 0) goto L_0x002d;
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r4 = 1
            if (r6 == r5) goto L_0x0074
            boolean r0 = r6 instanceof X.C79483qf
            r3 = 0
            if (r0 == 0) goto L_0x002d
            X.3qf r6 = (X.C79483qf) r6
            java.lang.String[] r1 = r5.A03
            java.lang.String[] r0 = r6.A03
            boolean r0 = X.C94684cL.A00(r1, r0)
            if (r0 == 0) goto L_0x002d
            java.lang.String[] r1 = r5.A04
            java.lang.String[] r0 = r6.A04
            boolean r0 = X.C94684cL.A00(r1, r0)
            if (r0 == 0) goto L_0x002d
            int[] r2 = r5.A00
            int[] r1 = r6.A00
            if (r2 == 0) goto L_0x002e
            int r0 = r2.length
            if (r0 == 0) goto L_0x002e
            boolean r0 = java.util.Arrays.equals(r2, r1)
            if (r0 != 0) goto L_0x0033
        L_0x002d:
            return r3
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            int r0 = r1.length
            if (r0 != 0) goto L_0x002d
        L_0x0033:
            long[] r2 = r5.A01
            long[] r1 = r6.A01
            if (r2 == 0) goto L_0x0043
            int r0 = r2.length
            if (r0 == 0) goto L_0x0043
            boolean r0 = java.util.Arrays.equals(r2, r1)
            if (r0 != 0) goto L_0x0048
            return r3
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            int r0 = r1.length
            if (r0 != 0) goto L_0x002d
        L_0x0048:
            long[] r2 = r5.A02
            long[] r1 = r6.A02
            if (r2 == 0) goto L_0x0058
            int r0 = r2.length
            if (r0 == 0) goto L_0x0058
            boolean r0 = java.util.Arrays.equals(r2, r1)
            if (r0 != 0) goto L_0x005d
            return r3
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            int r0 = r1.length
            if (r0 != 0) goto L_0x002d
        L_0x005d:
            X.5BY r1 = r5.A00
            if (r1 == 0) goto L_0x006c
            int r0 = r1.A00
            if (r0 == 0) goto L_0x006c
            X.5BY r0 = r6.A00
            boolean r0 = r1.equals(r0)
            return r0
        L_0x006c:
            X.5BY r0 = r6.A00
            if (r0 == 0) goto L_0x0074
            int r0 = r0.A00
            if (r0 != 0) goto L_0x002d
        L_0x0074:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C79483qf.equals(java.lang.Object):boolean");
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int length;
        int length2;
        int hashCode = (C79483qf.class.getName().hashCode() + 527) * 31;
        String[] strArr = this.A03;
        if (strArr == null) {
            length = 0;
        } else {
            length = strArr.length;
        }
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            String str = strArr[i2];
            if (str != null) {
                i = C12990iw.A08(str, i * 31);
            }
        }
        int i3 = (hashCode + i) * 31;
        String[] strArr2 = this.A04;
        if (strArr2 == null) {
            length2 = 0;
        } else {
            length2 = strArr2.length;
        }
        int i4 = 0;
        for (int i5 = 0; i5 < length2; i5++) {
            String str2 = strArr2[i5];
            if (str2 != null) {
                i4 = C12990iw.A08(str2, i4 * 31);
            }
        }
        int i6 = (i3 + i4) * 31;
        int[] iArr = this.A00;
        int hashCode2 = (i6 + ((iArr == null || iArr.length == 0) ? 0 : Arrays.hashCode(iArr))) * 31;
        long[] jArr = this.A01;
        int hashCode3 = (hashCode2 + ((jArr == null || jArr.length == 0) ? 0 : Arrays.hashCode(jArr))) * 31;
        long[] jArr2 = this.A02;
        int hashCode4 = (hashCode3 + ((jArr2 == null || jArr2.length == 0) ? 0 : Arrays.hashCode(jArr2))) * 31;
        AnonymousClass5BY r1 = ((C79503qh) this).A00;
        return hashCode4 + ((r1 == null || r1.A00 == 0) ? 0 : r1.hashCode());
    }
}
