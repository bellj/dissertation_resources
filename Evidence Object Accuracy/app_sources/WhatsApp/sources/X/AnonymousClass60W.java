package X;

import android.content.Context;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.lang.reflect.Method;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60W  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60W {
    public AnonymousClass61A A00;
    public final C22680zT A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final AnonymousClass01d A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final C14850m9 A07;
    public final C17220qS A08;
    public final C1308460e A09;
    public final C1329668y A0A;
    public final C18610sj A0B;
    public final AnonymousClass6BE A0C;
    public final C30931Zj A0D;
    public final C22120yY A0E;

    public AnonymousClass60W(C22680zT r9, C15570nT r10, C15450nH r11, AnonymousClass01d r12, C14830m7 r13, C16590pI r14, C14850m9 r15, C17220qS r16, C1308460e r17, C1329668y r18, C18610sj r19, AnonymousClass6BE r20, C22120yY r21) {
        C30931Zj A00 = C30931Zj.A00("IndiaUpiSimSwapDetectionUtils", "payment", "IN");
        this.A0D = A00;
        this.A06 = r14;
        this.A05 = r13;
        this.A07 = r15;
        this.A02 = r10;
        this.A03 = r11;
        this.A08 = r16;
        this.A04 = r12;
        this.A01 = r9;
        this.A0E = r21;
        this.A09 = r17;
        this.A0B = r19;
        this.A0C = r20;
        this.A0A = r18;
        if (Build.VERSION.SDK_INT >= 22) {
            this.A00 = new AnonymousClass61A(r14, r12, A00, r9, r20, r18);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        if (r4 > 10) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.C22680zT r8, X.AnonymousClass6BE r9, java.lang.String r10, java.lang.String r11) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            r7 = 0
            if (r0 == 0) goto L_0x0008
            return r7
        L_0x0008:
            if (r10 != 0) goto L_0x0048
            r2 = 0
        L_0x000b:
            if (r11 != 0) goto L_0x003f
            r6 = 0
        L_0x000e:
            java.lang.String r0 = X.AnonymousClass1ZT.A01(r6)
            java.lang.String r5 = X.AnonymousClass1ZT.A00(r8, r0, r2)
            int r4 = r5.length()
            r0 = 12
            java.lang.String r3 = "91"
            if (r4 == r0) goto L_0x0034
            boolean r0 = r5.startsWith(r3)
            if (r0 == 0) goto L_0x0030
            java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
            r1 = 0
            java.lang.String r0 = "incorrect_country_prefix_validation"
            r9.AKg(r2, r1, r0, r1)
        L_0x0030:
            r0 = 10
            if (r4 <= r0) goto L_0x0051
        L_0x0034:
            boolean r0 = r5.startsWith(r3)
            if (r0 == 0) goto L_0x0051
            boolean r0 = android.text.TextUtils.equals(r5, r6)
            return r0
        L_0x003f:
            java.lang.String r1 = "\\D"
            java.lang.String r0 = ""
            java.lang.String r6 = r11.replaceAll(r1, r0)
            goto L_0x000e
        L_0x0048:
            java.lang.String r1 = "\\D"
            java.lang.String r0 = ""
            java.lang.String r2 = r10.replaceAll(r1, r0)
            goto L_0x000b
        L_0x0051:
            java.lang.String r0 = ""
            java.lang.String r0 = r6.replaceFirst(r3, r0)
            boolean r0 = android.text.TextUtils.equals(r5, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60W.A00(X.0zT, X.6BE, java.lang.String, java.lang.String):boolean");
    }

    public int A01() {
        String str;
        String str2;
        String str3;
        boolean z;
        if (this.A03.A05(AbstractC15460nI.A0x)) {
            C15570nT r0 = this.A02;
            r0.A08();
            String A01 = C248917h.A01(r0.A01);
            C30931Zj r2 = this.A0D;
            StringBuilder A0k = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled : device binding status: ");
            C1329668y r02 = this.A0A;
            try {
                JSONObject A0a = C117295Zj.A0a();
                synchronized (r02) {
                    z = false;
                    try {
                        String A04 = r02.A03.A04();
                        if (!TextUtils.isEmpty(A04)) {
                            z = C13000ix.A05(A04).optBoolean("skipDevBinding", false);
                        }
                    } catch (JSONException e) {
                        Log.w("PAY: IndiaUpiPaymentSharedPrefs readDeviceBinding threw: ", e);
                    }
                }
                A0a.put("skipDevBinding", z);
                A0a.put("device_binding_sim_iccid", C1309060l.A01(r02.A0P("device_binding_sim_iccid")[0]));
                A0a.put("device_binding_sim_id", C1309060l.A01(r02.A0P("device_binding_sim_id")[0]));
                String A07 = r02.A07();
                if (!TextUtils.isEmpty(A07)) {
                    A0a.put("psp", A07);
                    A0a.put("devBinding", r02.A0O(A07));
                }
                str = A0a.toString();
            } catch (JSONException e2) {
                Log.w("PAY: IndiaUpiPaymentSharedPrefs logDeviceBindingStatus threw: ", e2);
                str = "";
            }
            r2.A06(C12960it.A0d(str, A0k));
            C120475gF r6 = new C120475gF(this.A05, this.A07, this.A08, this.A09, this.A0B);
            int i = Build.VERSION.SDK_INT;
            if (i >= 22) {
                return this.A00.A03(r6, A01);
            }
            if (i < 22) {
                r2.A06("Check sim on version < 22");
                TelephonyManager A0N = this.A04.A0N();
                String line1Number = A0N.getLine1Number();
                C22680zT r11 = this.A01;
                AnonymousClass6BE r10 = this.A0C;
                if (A00(r11, r10, line1Number, A01)) {
                    str3 = "Phone 1 matched";
                } else {
                    StringBuilder A0k2 = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled Phone 1 not matched | sim number : ");
                    A0k2.append(line1Number);
                    A0k2.append(" | waNumber : ");
                    r2.A06(C12960it.A0d(A01, A0k2));
                    String simSerialNumber = A0N.getSimSerialNumber();
                    String A09 = r02.A09();
                    if (TextUtils.equals(simSerialNumber, A09)) {
                        str3 = "ICCID 1 matched";
                    } else {
                        StringBuilder A0k3 = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled ICCID 1 not matched simId : ");
                        A0k3.append(C1309060l.A01(simSerialNumber));
                        A0k3.append(" | storedId : ");
                        r2.A06(C12960it.A0d(C1309060l.A01(A09), A0k3));
                        String A042 = A04("getLine1Number");
                        StringBuilder A0k4 = C12960it.A0k("Phone ");
                        A0k4.append(A042);
                        A0k4.append(" phone2 ");
                        r2.A06(C12960it.A0d(A042, A0k4));
                        if (A00(r11, r10, A042, A01)) {
                            str3 = "Phone 2 matched";
                        } else {
                            StringBuilder A0k5 = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled Phone 2 not matched | sim number : ");
                            A0k5.append(line1Number);
                            A0k5.append(" | waNumber : ");
                            r2.A06(C12960it.A0d(A01, A0k5));
                            String A043 = A04("getSimSerialNumber");
                            StringBuilder A0k6 = C12960it.A0k("ID");
                            A0k6.append(A09);
                            A0k6.append(" ID2 ");
                            r2.A04(C12960it.A0d(A043, A0k6));
                            if (TextUtils.equals(A09, A043)) {
                                str3 = "ICCID 2 matched";
                            } else {
                                StringBuilder A0k7 = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled ICCID 2 not matched simId : ");
                                A0k7.append(C1309060l.A01(A043));
                                A0k7.append(" | storedId : ");
                                r2.A06(C12960it.A0d(C1309060l.A01(A09), A0k7));
                                str2 = "IndiaUpiSimSwapDetectionUtils : No ICCID matched on API 22 or lower";
                            }
                        }
                    }
                }
                r2.A06(str3);
            } else {
                str2 = "IndiaUpiSimSwapDetectionUtils : API is too low to support payments";
            }
            r2.A06(str2);
            return 1;
        }
        return 0;
    }

    public SmsManager A02(int i) {
        return AnonymousClass61A.A00(i);
    }

    public String A03() {
        Context context;
        int i;
        try {
            context = this.A06.A00;
        } catch (Exception e) {
            this.A0D.A0A("Unable to get device bind ICCID", e);
        }
        if (AnonymousClass00T.A01(context, "android.permission.READ_PHONE_STATE") != 0 || ((i = Build.VERSION.SDK_INT) >= 30 && AnonymousClass00T.A01(context, "android.permission.READ_PHONE_NUMBERS") != 0)) {
            return null;
        }
        C15570nT r0 = this.A02;
        r0.A08();
        String A01 = C248917h.A01(r0.A01);
        if (i >= 22) {
            return this.A00.A04(A01);
        }
        if (i < 22) {
            TelephonyManager A0N = this.A04.A0N();
            String line1Number = A0N.getLine1Number();
            C22680zT r2 = this.A01;
            AnonymousClass6BE r1 = this.A0C;
            if (A00(r2, r1, line1Number, A01)) {
                this.A0D.A04("store first iccid");
                return A0N.getSimSerialNumber();
            } else if (A00(r2, r1, A04("getLine1Number"), A01)) {
                this.A0D.A04("store second iccid");
                return A04("getSimSerialNumber");
            } else if (A0N.getSimSerialNumber() != null) {
                return A0N.getSimSerialNumber();
            }
        }
        return null;
    }

    public final String A04(String str) {
        TelephonyManager A0N = this.A04.A0N();
        String str2 = null;
        try {
            Method method = Class.forName(A0N.getClass().getName()).getMethod(str, Integer.TYPE);
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 1, 0);
            Object invoke = method.invoke(A0N, objArr);
            if (invoke != null) {
                str2 = invoke.toString();
                return str2;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }

    public List A05(Context context) {
        return AnonymousClass61A.A02(context);
    }
}
