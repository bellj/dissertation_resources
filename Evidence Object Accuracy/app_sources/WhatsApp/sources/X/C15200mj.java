package X;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15200mj {
    public static final InputFilter[] A00 = new InputFilter[0];

    public static boolean A08(int i) {
        int i2 = i & 4095;
        return i2 == 129 || i2 == 225 || i2 == 18 || i2 == 145;
    }

    public static void A00(Typeface typeface, C15190mi r5, AnonymousClass3C4 r6, AnonymousClass28D r7, String str) {
        if (r7.A0O(59, false)) {
            r5.setPadding(0, 0, 0, 0);
        }
        if (str != null && !str.equals(r5.getText().toString())) {
            r5.setText(str);
        }
        r5.setHint(r7.A0I(36));
        String A0I = r7.A0I(51);
        if (A0I != null) {
            try {
                r5.setGravity(AnonymousClass3JW.A07(A0I));
            } catch (AnonymousClass491 e) {
                C28691Op.A01("TextInputBinderUtils", "Error parsing text align", e);
            }
        }
        String A0I2 = r7.A0I(56);
        if (A0I2 != null) {
            try {
                A06(r5, r6, Integer.valueOf(AnonymousClass3JW.A08(A0I2)).intValue());
            } catch (AnonymousClass491 e2) {
                C28691Op.A01("TextInputBinderUtils", "Error parsing text input type", e2);
            }
        }
        if (r7.A0O(49, false) && !A08(r5.getInputType())) {
            r5.setSingleLine(true);
        }
        String A0I3 = r7.A0I(53);
        if (A0I3 != null) {
            try {
                r5.setTextSize(2, AnonymousClass3JW.A03(A0I3));
            } catch (AnonymousClass491 e3) {
                C28691Op.A01("TextInputBinderUtils", "Error parsing scaled text size for text input", e3);
            }
        }
        if (typeface != null) {
            r5.setTypeface(typeface);
            return;
        }
        String A0I4 = r7.A0I(54);
        if (A0I4 != null) {
            try {
                r5.setTypeface(null, AnonymousClass3JW.A09(A0I4));
            } catch (AnonymousClass491 e4) {
                C28691Op.A01("TextInputBinderUtils", "Error parsing text style for text input", e4);
            }
        }
    }

    public static void A01(TextUtils.TruncateAt truncateAt, C15190mi r3, AnonymousClass3C4 r4) {
        KeyListener keyListener;
        if (r4 != null && r3.getEllipsize() != truncateAt) {
            if (truncateAt == null) {
                if (r4.A0J != r3.getKeyListener()) {
                    keyListener = r4.A0J;
                }
                r4.A0G = truncateAt;
                r3.setEllipsize(truncateAt);
            }
            r4.A0J = r3.getKeyListener();
            keyListener = null;
            r3.setKeyListener(keyListener);
            r4.A0G = truncateAt;
            r3.setEllipsize(truncateAt);
        }
    }

    public static void A02(C14260l7 r4, C15190mi r5, AnonymousClass3C4 r6, AnonymousClass28D r7) {
        Drawable.ConstantState constantState;
        if (r7 != null) {
            Drawable textCursorDrawable = r5.getTextCursorDrawable();
            r6.A0C = textCursorDrawable;
            if (textCursorDrawable != null && (constantState = textCursorDrawable.getConstantState()) != null) {
                Drawable newDrawable = constantState.newDrawable();
                newDrawable.setColorFilter(new PorterDuffColorFilter(AnonymousClass4Di.A00(r4, r7), PorterDuff.Mode.SRC_OVER));
                r5.setTextCursorDrawable(newDrawable);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e7, code lost:
        if (r4.equals(r2) == false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.C14260l7 r2, X.C15190mi r3, java.lang.String r4) {
        /*
        // Method dump skipped, instructions count: 334
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15200mj.A03(X.0l7, X.0mi, java.lang.String):void");
    }

    public static void A04(C15190mi r2) {
        for (ViewParent parent = r2.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof RecyclerView) {
                ((ViewGroup) parent).setImportantForAutofill(1);
            }
        }
    }

    public static void A05(C15190mi r1, AnonymousClass3C4 r2) {
        Drawable drawable = r2.A0C;
        if (drawable != null) {
            r1.setTextCursorDrawable(drawable);
        }
    }

    public static void A06(C15190mi r2, AnonymousClass3C4 r3, int i) {
        if (A08(i) || (r3 != null && A08(r3.A05))) {
            r2.setInputType(i);
            A01(null, r2, r3);
        } else if ((i & 131087) == 131073 || (r3 != null && (r3.A05 & 131087) == 131073)) {
            r2.setInputType(i);
        } else {
            r2.setRawInputType(i);
        }
        if (r3 != null) {
            r3.A05 = i;
        }
    }

    public static void A07(C15190mi r2, AnonymousClass3C4 r3, boolean z) {
        KeyListener keyListener;
        if (Build.VERSION.SDK_INT < 21) {
            if (z) {
                keyListener = null;
            } else {
                keyListener = r3.A0I;
            }
            r2.setKeyListener(keyListener);
            return;
        }
        r2.setShowSoftInputOnFocus(!z);
        r2.setOnKeyListener(z ? new View$OnKeyListenerC101014mq() : null);
    }

    public static boolean A09(C15190mi r3) {
        Editable text = r3.getText();
        if (text == null || r3.getWidth() == 0 || text.length() == 0 || r3.getLineCount() > 1 || (r3.getInputType() & 131087) == 131073 || A08(r3.getInputType())) {
            return false;
        }
        return true;
    }
}
