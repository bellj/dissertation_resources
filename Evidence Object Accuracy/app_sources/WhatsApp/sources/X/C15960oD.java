package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0oD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15960oD extends AbstractC15970oE {
    public static final C15960oD A00 = new C15960oD();
    public static final Parcelable.Creator CREATOR = new C100024lF();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "broadcast";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 6;
    }

    public C15960oD() {
        super("location");
    }

    public C15960oD(Parcel parcel) {
        super(parcel);
    }
}
