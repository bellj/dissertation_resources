package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageButton;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2hc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54992hc extends AnonymousClass03U {
    public final TextEmojiLabel A00;
    public final C28801Pb A01;
    public final WaImageButton A02;
    public final ThumbnailButton A03;
    public final C15600nX A04;
    public final AnonymousClass11F A05;

    public C54992hc(View view, C15610nY r4, C15600nX r5, AnonymousClass11F r6, AnonymousClass12F r7) {
        super(view);
        this.A05 = r6;
        this.A03 = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.group_icon);
        C28801Pb r0 = new C28801Pb(view, r4, r7, (int) R.id.group_name);
        this.A01 = r0;
        this.A04 = r5;
        TextEmojiLabel textEmojiLabel = r0.A01;
        AnonymousClass028.A0a(textEmojiLabel, 2);
        C27531Hw.A06(textEmojiLabel);
        this.A00 = C12970iu.A0T(view, R.id.group_status);
        this.A02 = (WaImageButton) AnonymousClass028.A0D(view, R.id.remove_button);
    }
}
