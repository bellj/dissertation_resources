package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5c3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118585c3 extends AnonymousClass02M {
    public String A00 = "WhatsappPay";
    public final List A01 = C12960it.A0l();

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC118845cT) r2).A08((AbstractC128945wv) this.A01.get(i));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C121985kj(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_payment_option_view));
        }
        if (i == 1) {
            return new C121975ki(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_payment_option_view));
        }
        throw C12960it.A0U("PaymentOptionsBottomSheetAdapter/onCreateViewHolder/unhandled view type");
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AbstractC128945wv) this.A01.get(i)).A01;
    }
}
