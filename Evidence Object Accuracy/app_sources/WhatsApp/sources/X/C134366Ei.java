package X;

import android.content.Intent;
import com.whatsapp.payments.ui.NoviAmountEntryActivity;
import com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity;

/* renamed from: X.6Ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C134366Ei implements AbstractC14590lg {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C30861Zc A01;
    public final /* synthetic */ NoviPayHubAddPaymentMethodActivity A02;

    public /* synthetic */ C134366Ei(C30861Zc r1, NoviPayHubAddPaymentMethodActivity noviPayHubAddPaymentMethodActivity, int i) {
        this.A02 = noviPayHubAddPaymentMethodActivity;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        C1315463e A02;
        NoviPayHubAddPaymentMethodActivity noviPayHubAddPaymentMethodActivity = this.A02;
        C30861Zc r5 = this.A01;
        int i = this.A00;
        AbstractC28901Pl r8 = (AbstractC28901Pl) obj;
        if (r8 != null && (A02 = AnonymousClass61F.A02(r8)) != null) {
            Intent A0D = C12990iw.A0D(noviPayHubAddPaymentMethodActivity, NoviAmountEntryActivity.class);
            A0D.putExtra("withdrawal_type", 2);
            A0D.putExtra("account_info", A02);
            A0D.putExtra("bank_for_withdrawal", r5);
            A0D.putExtra("amount_entry_type", "withdraw");
            noviPayHubAddPaymentMethodActivity.startActivityForResult(A0D, i);
        }
    }
}
