package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: X.085  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass085 {
    public int A00 = -1;
    public C016107p A01;
    public C016107p A02;
    public C016107p A03;
    public final View A04;
    public final C011905s A05;

    public AnonymousClass085(View view) {
        this.A04 = view;
        this.A05 = C011905s.A01();
    }

    public void A00() {
        View view = this.A04;
        Drawable background = view.getBackground();
        if (background != null) {
            int i = Build.VERSION.SDK_INT;
            if (i <= 21 ? i == 21 : this.A02 != null) {
                C016107p r2 = this.A03;
                if (r2 == null) {
                    r2 = new C016107p();
                    this.A03 = r2;
                }
                r2.A00 = null;
                r2.A02 = false;
                r2.A01 = null;
                r2.A03 = false;
                ColorStateList A08 = AnonymousClass028.A08(view);
                if (A08 != null) {
                    r2.A02 = true;
                    r2.A00 = A08;
                }
                PorterDuff.Mode A09 = AnonymousClass028.A09(view);
                if (A09 != null) {
                    r2.A03 = true;
                    r2.A01 = A09;
                }
                if (r2.A02 || r2.A03) {
                    C012005t.A02(background, r2, view.getDrawableState());
                    return;
                }
            }
            C016107p r1 = this.A01;
            if (r1 != null || (r1 = this.A02) != null) {
                C012005t.A02(background, r1, view.getDrawableState());
            }
        }
    }

    public void A01() {
        this.A00 = -1;
        this.A02 = null;
        A00();
        A00();
    }

    public void A02(int i) {
        ColorStateList A03;
        this.A00 = i;
        C011905s r2 = this.A05;
        if (r2 != null) {
            Context context = this.A04.getContext();
            synchronized (r2) {
                A03 = r2.A00.A03(context, i);
            }
            if (A03 != null) {
                C016107p r1 = this.A02;
                if (r1 == null) {
                    r1 = new C016107p();
                    this.A02 = r1;
                }
                r1.A00 = A03;
                r1.A02 = true;
                A00();
                A00();
            }
        }
        this.A02 = null;
        A00();
        A00();
    }

    public void A03(ColorStateList colorStateList) {
        C016107p r1 = this.A01;
        if (r1 == null) {
            r1 = new C016107p();
            this.A01 = r1;
        }
        r1.A00 = colorStateList;
        r1.A02 = true;
        A00();
    }

    public void A04(PorterDuff.Mode mode) {
        C016107p r1 = this.A01;
        if (r1 == null) {
            r1 = new C016107p();
            this.A01 = r1;
        }
        r1.A01 = mode;
        r1.A03 = true;
        A00();
    }

    public void A05(AttributeSet attributeSet, int i) {
        ColorStateList A03;
        View view = this.A04;
        Context context = view.getContext();
        int[] iArr = AnonymousClass07O.A0P;
        C013406h A00 = C013406h.A00(context, attributeSet, iArr, i, 0);
        Context context2 = view.getContext();
        TypedArray typedArray = A00.A02;
        AnonymousClass028.A0L(context2, typedArray, attributeSet, view, iArr, i);
        try {
            if (typedArray.hasValue(0)) {
                this.A00 = typedArray.getResourceId(0, -1);
                C011905s r4 = this.A05;
                Context context3 = view.getContext();
                int i2 = this.A00;
                synchronized (r4) {
                    A03 = r4.A00.A03(context3, i2);
                }
                if (A03 != null) {
                    C016107p r1 = this.A02;
                    if (r1 == null) {
                        r1 = new C016107p();
                        this.A02 = r1;
                    }
                    r1.A00 = A03;
                    r1.A02 = true;
                    A00();
                }
            }
            if (typedArray.hasValue(1)) {
                AnonymousClass028.A0M(A00.A01(1), view);
            }
            if (typedArray.hasValue(2)) {
                AnonymousClass028.A0N(C014706y.A00(null, typedArray.getInt(2, -1)), view);
            }
        } finally {
            A00.A04();
        }
    }
}
