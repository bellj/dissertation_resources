package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1PH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1PH extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1PH A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public int A01;

    static {
        AnonymousClass1PH r0 = new AnonymousClass1PH();
        A02 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A02;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass1PH r9 = (AnonymousClass1PH) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A01;
                int i3 = r9.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r8.Afp(i2, r9.A01, z, z2);
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r82.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 8) {
                            int A022 = r82.A02();
                            if (A022 == 0 || A022 == 1 || A022 == 2) {
                                this.A00 = 1 | this.A00;
                                this.A01 = A022;
                            } else {
                                super.A0X(1, A022);
                            }
                        } else if (!A0a(r82, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AnonymousClass1PH();
            case 5:
                return new AnonymousClass1PI();
            case 6:
                break;
            case 7:
                if (A03 == null) {
                    synchronized (AnonymousClass1PH.class) {
                        if (A03 == null) {
                            A03 = new AnonymousClass255(A02);
                        }
                    }
                }
                return A03;
            default:
                throw new UnsupportedOperationException();
        }
        return A02;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
