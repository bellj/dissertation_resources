package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.1wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC43301wn extends Handler implements AbstractC43311wo {
    public final /* synthetic */ C19890uq A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC43301wn(Looper looper, C19890uq r2) {
        super(looper);
        this.A00 = r2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:238|(8:240|(1:249)|248|373|250|(5:256|(3:258|(2:261|259)|390)|262|784|272)|278|279)|384|248|373|250|(6:252|254|256|(0)|262|784)|278|279) */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0449, code lost:
        if (r1.A00.A09.isEmpty() != false) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x07e9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x07ea, code lost:
        com.whatsapp.util.Log.w("languagepackmanager/on-get-biz-language-pack/invalidproto:", r1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x076e  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x0785 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r18) {
        /*
        // Method dump skipped, instructions count: 2870
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC43301wn.handleMessage(android.os.Message):void");
    }
}
