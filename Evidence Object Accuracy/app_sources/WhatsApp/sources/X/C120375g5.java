package X;

import android.content.Context;

/* renamed from: X.5g5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120375g5 extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C17220qS A02;
    public final C18650sn A03;

    public C120375g5(Context context, C14900mE r2, C17220qS r3, C18650sn r4, C64513Fv r5, C18610sj r6) {
        super(r5, r6);
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }
}
