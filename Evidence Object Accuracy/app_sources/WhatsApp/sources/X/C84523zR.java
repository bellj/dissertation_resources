package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.3zR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84523zR extends AbstractC75653kC {
    public final WaTextView A00;

    public C84523zR(View view) {
        super(view);
        this.A00 = (WaTextView) AnonymousClass028.A0D(view, R.id.order_detail_timestamp);
    }
}
