package X;

/* renamed from: X.3BZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BZ {
    public final C15370n3 A00;
    public final C15370n3 A01;
    public final C15370n3 A02;
    public final AbstractC15340mz A03;
    public final AnonymousClass1YY A04;
    public final C33181da A05;
    public final C92434Vw A06;

    public AnonymousClass3BZ(C15370n3 r1, C15370n3 r2, C15370n3 r3, AbstractC15340mz r4, AnonymousClass1YY r5, C33181da r6, C92434Vw r7) {
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A05 = r6;
        this.A02 = r3;
        this.A06 = r7;
    }
}
