package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.audiopicker.AudioPickerActivity;

/* renamed from: X.3X8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X8 implements AnonymousClass23E {
    public final /* synthetic */ C64393Fj A00;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public /* synthetic */ AnonymousClass3X8(C64393Fj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        C64393Fj r0 = this.A00;
        ImageView imageView = r0.A05;
        imageView.setImageBitmap(null);
        r0.A03.setBackground(null);
        imageView.setBackgroundResource(0);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        Resources resources;
        int i;
        C64393Fj r2 = this.A00;
        ImageView imageView = r2.A05;
        imageView.setImageBitmap(bitmap);
        if (bitmap == C64843Hc.A05) {
            r2.A03.setBackground(null);
            resources = r2.A0C.getResources();
            i = R.drawable.audio_picker_empty_thumb_background;
        } else {
            FrameLayout frameLayout = r2.A03;
            AudioPickerActivity audioPickerActivity = r2.A0C;
            frameLayout.setBackground(audioPickerActivity.getResources().getDrawable(R.drawable.audio_picker_thumb_frame_background));
            resources = audioPickerActivity.getResources();
            i = R.drawable.audio_picker_filled_thumb_background;
        }
        imageView.setBackground(resources.getDrawable(i));
    }
}
