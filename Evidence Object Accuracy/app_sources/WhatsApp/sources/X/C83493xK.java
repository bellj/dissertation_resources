package X;

import android.view.animation.Animation;

/* renamed from: X.3xK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83493xK extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C14680lr A00;

    public C83493xK(C14680lr r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A06.clearAnimation();
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.A06.setVisibility(0);
    }
}
