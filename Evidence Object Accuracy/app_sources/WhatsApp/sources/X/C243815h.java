package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.15h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243815h {
    public AbstractC35031h7 A00;
    public final AbstractC35031h7 A01;
    public final AbstractC35031h7 A02 = new C35021h6();
    public final AbstractC35031h7[] A03;

    public C243815h(C14850m9 r5) {
        C35041h8 r3 = new C35041h8();
        this.A01 = r3;
        this.A03 = new AbstractC35031h7[]{new C35051h9(), new C35061hA(), new C35071hB(), new C35081hC(), new C35091hD(), new C35101hE(r5), new C35111hF(), r3};
    }

    public synchronized AbstractC35031h7 A00(Context context) {
        AbstractC35031h7 r0;
        r0 = this.A00;
        if (r0 == null) {
            if (Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {
                this.A00 = new C35061hA();
            } else {
                try {
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.HOME");
                    List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 65536);
                    if (queryIntentActivities != null) {
                        for (ResolveInfo resolveInfo : queryIntentActivities) {
                            String str = resolveInfo.activityInfo.packageName;
                            StringBuilder sb = new StringBuilder();
                            sb.append("badger/homepackage/");
                            sb.append(str);
                            Log.i(sb.toString());
                            AbstractC35031h7[] r4 = this.A03;
                            int length = r4.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    break;
                                }
                                AbstractC35031h7 r1 = r4[i];
                                if (r1.A00(context.getApplicationContext()).contains(str)) {
                                    this.A00 = r1;
                                    break;
                                }
                                i++;
                            }
                            if (this.A00 != null) {
                                break;
                            }
                        }
                    } else {
                        Log.e("badger/nohome");
                    }
                } catch (Exception e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("badger/getbadger ");
                    sb2.append(e.getMessage());
                    Log.e(sb2.toString(), e);
                }
                if (this.A00 == null) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        Log.i("badger/getbadger/notfound/default");
                        this.A00 = this.A01;
                    } else {
                        Log.i("badger/getbadger/notfound/disable");
                        this.A00 = this.A02;
                    }
                }
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("badger/getbadger ");
            sb3.append(Arrays.asList(this.A03).indexOf(this.A00));
            Log.i(sb3.toString());
            r0 = this.A00;
        }
        return r0;
    }
}
