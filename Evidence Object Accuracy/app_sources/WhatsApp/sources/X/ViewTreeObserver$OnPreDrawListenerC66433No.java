package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3No  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66433No implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass3S9 A01;

    public ViewTreeObserver$OnPreDrawListenerC66433No(View view, AnonymousClass3S9 r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        C12980iv.A1G(view, this);
        AnonymousClass3S9 r0 = this.A01;
        view.post(new RunnableBRunnable0Shape3S0300000_I1(this, r0.A00, r0.A03, 5));
        return true;
    }
}
