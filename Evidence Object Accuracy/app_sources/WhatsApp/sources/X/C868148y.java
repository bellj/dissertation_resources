package X;

import java.io.IOException;

/* renamed from: X.48y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C868148y extends IOException {
    public AbstractC117135Yq zza = null;

    public C868148y(String str) {
        super(str);
    }

    public static C868148y A00() {
        return new C868148y("Protocol message had invalid UTF-8.");
    }

    public static C868148y A01() {
        return new C868148y("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }
}
