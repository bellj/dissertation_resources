package X;

/* renamed from: X.23I  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass23I extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Double A02;
    public Double A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;

    public AnonymousClass23I() {
        super(1910, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(8, this.A03);
        r3.Abe(24, this.A04);
        r3.Abe(3, this.A05);
        r3.Abe(2, this.A06);
        r3.Abe(1, this.A00);
        r3.Abe(4, this.A07);
        r3.Abe(23, this.A08);
        r3.Abe(22, this.A09);
        r3.Abe(21, this.A0A);
        r3.Abe(14, this.A0B);
        r3.Abe(13, this.A0C);
        r3.Abe(12, this.A0D);
        r3.Abe(11, this.A0E);
        r3.Abe(10, this.A0F);
        r3.Abe(9, this.A0G);
        r3.Abe(20, this.A0H);
        r3.Abe(19, this.A0I);
        r3.Abe(18, this.A0J);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamAndroidDatabaseOverallMigrationEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "afterMigrationMsgstoreSize", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "beforeMigrationMsgstoreSize", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "freeSpaceAvailable", this.A03);
        Integer num = this.A04;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationInitiator", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationProcessedCnt", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationRegisteredCnt", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationSucceeded", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationT", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseConsistencyFailedCnt", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseConsistencySkippedCnt", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseConsistencySuccessCnt", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseMigrationFailedCnt", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseMigrationSkippedCnt", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseMigrationSuccessCnt", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseRollbackFailedCnt", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseRollbackSkippedCnt", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseRollbackSuccessCnt", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseVerificationFailedCnt", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseVerificationSkippedCnt", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phaseVerificationSuccessCnt", this.A0J);
        sb.append("}");
        return sb.toString();
    }
}
