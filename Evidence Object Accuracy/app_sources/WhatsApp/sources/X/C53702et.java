package X;

import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import java.lang.ref.WeakReference;

/* renamed from: X.2et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53702et extends AnonymousClass04X {
    public final /* synthetic */ BottomSheetBehavior A00;

    public C53702et(BottomSheetBehavior bottomSheetBehavior) {
        this.A00 = bottomSheetBehavior;
    }

    @Override // X.AnonymousClass04X
    public void A00(int i) {
        if (i == 1) {
            this.A00.A0K(1);
        }
    }

    @Override // X.AnonymousClass04X
    public int A02(View view) {
        BottomSheetBehavior bottomSheetBehavior = this.A00;
        if (bottomSheetBehavior.A0J) {
            return bottomSheetBehavior.A08;
        }
        return bottomSheetBehavior.A02;
    }

    @Override // X.AnonymousClass04X
    public int A03(View view, int i, int i2) {
        return view.getLeft();
    }

    @Override // X.AnonymousClass04X
    public int A04(View view, int i, int i2) {
        int i3;
        int i4;
        BottomSheetBehavior bottomSheetBehavior = this.A00;
        if (bottomSheetBehavior.A0I) {
            i3 = bottomSheetBehavior.A03;
        } else {
            i3 = 0;
        }
        if (bottomSheetBehavior.A0J) {
            i4 = bottomSheetBehavior.A08;
        } else {
            i4 = bottomSheetBehavior.A02;
        }
        if (i >= i3) {
            i3 = i;
            if (i > i4) {
                return i4;
            }
        }
        return i3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0080, code lost:
        if (r5 < X.C12980iv.A05(r5, r3.A02)) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008d, code lost:
        if (r1 < X.C12980iv.A05(r5, r4)) goto L_0x008f;
     */
    @Override // X.AnonymousClass04X
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.view.View r8, float r9, float r10) {
        /*
            r7 = this;
            r4 = 0
            r2 = 0
            r6 = 4
            int r0 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            com.google.android.material.bottomsheet.BottomSheetBehavior r3 = r7.A00
            if (r0 >= 0) goto L_0x0029
            boolean r0 = r3.A0I
            if (r0 == 0) goto L_0x0091
            int r4 = r3.A03
        L_0x000f:
            r6 = 3
        L_0x0010:
            X.0Su r1 = r3.A0D
            int r0 = r8.getLeft()
            boolean r0 = r1.A0C(r0, r4)
            if (r0 == 0) goto L_0x009d
            r1 = 2
            r3.A0K(r1)
            com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0
            r0.<init>(r8, r3, r6, r1)
            r8.postOnAnimation(r0)
            return
        L_0x0029:
            boolean r0 = r3.A0J
            if (r0 == 0) goto L_0x004b
            boolean r0 = r3.A0O(r8, r10)
            if (r0 == 0) goto L_0x004b
            int r1 = r8.getTop()
            int r0 = r3.A02
            if (r1 > r0) goto L_0x0047
            float r1 = java.lang.Math.abs(r9)
            float r0 = java.lang.Math.abs(r10)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x004b
        L_0x0047:
            int r4 = r3.A08
            r6 = 5
            goto L_0x0010
        L_0x004b:
            int r0 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x005e
            float r1 = java.lang.Math.abs(r9)
            float r0 = java.lang.Math.abs(r10)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x005e
            int r4 = r3.A02
            goto L_0x0010
        L_0x005e:
            int r5 = r8.getTop()
            boolean r0 = r3.A0I
            if (r0 == 0) goto L_0x0076
            int r2 = r3.A03
            int r1 = X.C12980iv.A05(r5, r2)
            int r4 = r3.A02
            int r0 = X.C12980iv.A05(r5, r4)
            if (r1 >= r0) goto L_0x0010
            r4 = r2
            goto L_0x000f
        L_0x0076:
            int r2 = r3.A04
            if (r5 >= r2) goto L_0x0083
            int r0 = r3.A02
            int r0 = X.C12980iv.A05(r5, r0)
            if (r5 >= r0) goto L_0x008f
            goto L_0x000f
        L_0x0083:
            int r1 = X.C12980iv.A05(r5, r2)
            int r4 = r3.A02
            int r0 = X.C12980iv.A05(r5, r4)
            if (r1 >= r0) goto L_0x0010
        L_0x008f:
            r4 = r2
            goto L_0x009a
        L_0x0091:
            int r1 = r8.getTop()
            int r0 = r3.A04
            if (r1 <= r0) goto L_0x000f
            r4 = r0
        L_0x009a:
            r6 = 6
            goto L_0x0010
        L_0x009d:
            r3.A0K(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53702et.A05(android.view.View, float, float):void");
    }

    @Override // X.AnonymousClass04X
    public void A07(View view, int i, int i2, int i3, int i4) {
        this.A00.A0J(i2);
    }

    @Override // X.AnonymousClass04X
    public boolean A08(View view, int i) {
        WeakReference weakReference;
        View view2;
        BottomSheetBehavior bottomSheetBehavior = this.A00;
        int i2 = bottomSheetBehavior.A0B;
        if (i2 == 1 || bottomSheetBehavior.A0O || ((i2 == 3 && bottomSheetBehavior.A01 == i && (view2 = (View) bottomSheetBehavior.A0F.get()) != null && view2.canScrollVertically(-1)) || (weakReference = bottomSheetBehavior.A0G) == null || weakReference.get() != view)) {
            return false;
        }
        return true;
    }
}
