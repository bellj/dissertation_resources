package X;

import android.widget.AbsListView;
import com.whatsapp.BottomSheetListView;
import com.whatsapp.R;
import com.whatsapp.registration.EULA;

/* renamed from: X.4ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102294ou implements AbsListView.OnScrollListener {
    public final /* synthetic */ BottomSheetListView A00;
    public final /* synthetic */ EULA A01;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C102294ou(BottomSheetListView bottomSheetListView, EULA eula) {
        this.A01 = eula;
        this.A00 = bottomSheetListView;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.A01.findViewById(R.id.fade_view).setVisibility(C12960it.A02(this.A00.A00() ? 1 : 0));
    }
}
