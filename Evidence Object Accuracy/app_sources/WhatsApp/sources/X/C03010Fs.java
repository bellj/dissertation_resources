package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03010Fs extends AnonymousClass0ZL {
    @Override // X.AbstractC12410hs
    public float ADB(View view, ViewGroup viewGroup) {
        return view.getTranslationX() + ((float) viewGroup.getWidth());
    }
}
