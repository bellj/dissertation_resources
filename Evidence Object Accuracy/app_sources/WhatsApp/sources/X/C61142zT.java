package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C61142zT extends AbstractC52952c7 implements AnonymousClass5X0 {
    public View A00 = findViewById(R.id.add_btn);
    public View A01 = findViewById(R.id.block_btn);
    public View A02 = findViewById(R.id.content);
    public View A03 = findViewById(R.id.dismiss_btn);
    public View A04 = findViewById(R.id.exit_group_btn);
    public View A05 = findViewById(R.id.not_spam_btn);
    public View A06 = findViewById(R.id.spam_btn);
    public TextView A07 = C12960it.A0J(this, R.id.add_btn_text);
    public TextView A08 = C12960it.A0J(this, R.id.block_btn_text);
    public TextView A09 = C12960it.A0J(this, R.id.dismiss_btn_text);
    public TextView A0A = C12960it.A0J(this, R.id.exit_group_btn_text);
    public TextView A0B = C12960it.A0J(this, R.id.header);
    public TextView A0C = C12960it.A0J(this, R.id.not_spam_btn_text);
    public TextView A0D = C12960it.A0J(this, R.id.spam_btn_text);
    public TextEmojiLabel A0E = C12970iu.A0U(this, R.id.group_privacy_info);
    public C64433Fn A0F;
    public AnonymousClass01d A0G;

    @Override // X.AnonymousClass5X0
    public int getType() {
        return 0;
    }

    public C61142zT(Context context) {
        super(context);
        FrameLayout.inflate(context, R.layout.conversation_block_add_footer, this);
    }

    @Override // X.AnonymousClass5X0
    public void AIS() {
        this.A02.setVisibility(8);
    }

    @Override // X.AnonymousClass5X0
    public void AaU(AnonymousClass4UC r8) {
        int i;
        int i2;
        int i3;
        this.A02.setVisibility(0);
        this.A06.setVisibility(r8.A0B);
        View view = this.A05;
        int i4 = r8.A0A;
        view.setVisibility(i4);
        View view2 = this.A01;
        int i5 = r8.A02;
        view2.setVisibility(i5);
        this.A00.setVisibility(r8.A00);
        this.A03.setVisibility(r8.A03);
        this.A04.setVisibility(r8.A04);
        TextEmojiLabel textEmojiLabel = this.A0E;
        int i6 = r8.A06;
        textEmojiLabel.setVisibility(i6);
        if (i4 == 0 && (i3 = r8.A09) != -1) {
            TextView textView = this.A0C;
            textView.setText(i3);
            C12960it.A0r(getContext(), textView, r8.A08);
        }
        if (i5 == 0 && (i2 = r8.A01) != -1) {
            this.A08.setText(i2);
        }
        if (i6 == 0 && (i = r8.A05) != -1) {
            AbstractC28491Nn.A03(textEmojiLabel);
            AbstractC28491Nn.A04(textEmojiLabel, this.A0G);
            textEmojiLabel.setText(this.A0F.A00(C12990iw.A0q(this, i)));
        }
        int i7 = r8.A07;
        if (i7 != -1) {
            TextView textView2 = this.A0B;
            textView2.setText(i7);
            C12960it.A0r(getContext(), textView2, i7);
        }
    }

    @Override // X.AnonymousClass5X0
    public void setup(C64433Fn r3) {
        this.A0F = r3;
        C12960it.A13(this.A06, this, r3, 46);
        C12960it.A13(this.A01, this, r3, 44);
        C12960it.A0y(this.A00, r3, 42);
        C12960it.A0y(this.A05, r3, 41);
        C12960it.A0y(this.A03, this, 40);
        C12960it.A13(this.A04, this, r3, 45);
        C27531Hw.A06(this.A0D);
        C27531Hw.A06(this.A0C);
        C27531Hw.A06(this.A08);
        C27531Hw.A06(this.A07);
        C27531Hw.A06(this.A0A);
    }
}
