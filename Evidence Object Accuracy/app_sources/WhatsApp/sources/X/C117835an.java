package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import java.util.Map;

/* renamed from: X.5an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117835an extends AnonymousClass0PW {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayBloksActivity A01;
    public final /* synthetic */ Map A02;

    public C117835an(AnonymousClass3FE r1, NoviPayBloksActivity noviPayBloksActivity, Map map) {
        this.A01 = noviPayBloksActivity;
        this.A02 = map;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0PW
    public void A01(int i, CharSequence charSequence) {
        this.A01.A2v(i);
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r5) {
        AnonymousClass0U4 r2 = r5.A01;
        if (r2 != null) {
            Map map = this.A02;
            if (map == null || TextUtils.isEmpty((CharSequence) map.get("login_attempt_id"))) {
                this.A01.A2C(R.string.payments_loading);
            }
            NoviPayBloksActivity.A1J(null, this.A00, this.A01, r2.A01, null);
        }
    }
}
