package X;

import android.graphics.PointF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import java.util.Map;

/* renamed from: X.0Aw  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Aw extends GestureDetector.SimpleOnGestureListener implements ScaleGestureDetector.OnScaleGestureListener {
    public PointF A00;
    public boolean A01;
    public boolean A02;
    public final View A03;
    public final AnonymousClass0OT A04;
    public final C14260l7 A05;
    public final AnonymousClass28D A06;
    public final AnonymousClass28D A07;
    public final Map A08 = new AnonymousClass00N(2);

    public AnonymousClass0Aw(View view, AnonymousClass0OT r4, C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7) {
        this.A05 = r5;
        this.A07 = r6;
        this.A06 = r7;
        this.A03 = view;
        this.A04 = r4;
    }

    public AbstractC14200l1 A00(int i) {
        Map map = this.A08;
        Integer valueOf = Integer.valueOf(i);
        if (map.containsKey(valueOf)) {
            return (AbstractC14200l1) map.get(valueOf);
        }
        AbstractC14200l1 A0G = this.A07.A0G(i);
        map.put(valueOf, A0G);
        return A0G;
    }

    public void A01(MotionEvent motionEvent) {
        AbstractC14200l1 A00 = A00(50);
        if (A00 != null) {
            PointF A002 = this.A04.A00(new PointF(motionEvent.getX(), motionEvent.getY()));
            AnonymousClass28D r6 = this.A07;
            C14210l2 r8 = new C14210l2();
            r8.A04(this.A06, 0);
            C14260l7 r5 = this.A05;
            r8.A04(r5, 1);
            View view = this.A03;
            r8.A04(Float.valueOf((A002.x * 100.0f) / ((float) view.getWidth())), 2);
            r8.A04(Float.valueOf((A002.y * 100.0f) / ((float) view.getHeight())), 3);
            C28701Oq.A01(r5, r6, r8.A03(), A00);
        }
    }

    public final boolean A02() {
        return (A00(36) == null && A00(45) == null) ? false : true;
    }

    public final boolean A03() {
        AbstractC14200l1 A00 = A00(43);
        if (A00 == null) {
            return false;
        }
        AnonymousClass28D r3 = this.A07;
        C14210l2 r2 = new C14210l2();
        r2.A04(this.A06, 0);
        C14260l7 r1 = this.A05;
        r2.A04(r1, 1);
        C28701Oq.A01(r1, r3, r2.A03(), A00);
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        AbstractC14200l1 A00 = A00(45);
        if (A00 == null) {
            A00 = A00(36);
        }
        if (A00 == null) {
            return false;
        }
        PointF A002 = this.A04.A00(new PointF(motionEvent.getX(), motionEvent.getY()));
        AnonymousClass28D r7 = this.A07;
        C14210l2 r9 = new C14210l2();
        r9.A04(this.A06, 0);
        C14260l7 r6 = this.A05;
        r9.A04(r6, 1);
        View view = this.A03;
        r9.A04(Float.valueOf((A002.x * 100.0f) / ((float) view.getWidth())), 2);
        r9.A04(Float.valueOf((A002.y * 100.0f) / ((float) view.getHeight())), 3);
        C28701Oq.A01(r6, r7, r9.A03(), A00);
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        AbstractC14200l1 A00 = A00(49);
        if (A00 != null) {
            PointF A002 = this.A04.A00(new PointF(motionEvent.getX(), motionEvent.getY()));
            AnonymousClass28D r6 = this.A07;
            C14210l2 r9 = new C14210l2();
            r9.A04(this.A06, 0);
            C14260l7 r5 = this.A05;
            r9.A04(r5, 1);
            View view = this.A03;
            r9.A04(Float.valueOf((A002.x * 100.0f) / ((float) view.getWidth())), 2);
            r9.A04(Float.valueOf((A002.y * 100.0f) / ((float) view.getHeight())), 3);
            C28701Oq.A01(r5, r6, r9.A03(), A00);
        }
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int i;
        if (!(motionEvent == null || motionEvent2 == null)) {
            float x = motionEvent2.getX() - motionEvent.getX();
            float y = motionEvent2.getY() - motionEvent.getY();
            boolean z = false;
            if (Math.abs(y) > Math.abs(x)) {
                z = true;
            }
            if (z) {
                i = 42;
                if (y > 0.0f) {
                    i = 38;
                }
            } else {
                i = 40;
                if (x > 0.0f) {
                    i = 41;
                }
            }
            AbstractC14200l1 A00 = A00(i);
            if (A00 != null) {
                AnonymousClass28D r4 = this.A07;
                C14210l2 r3 = new C14210l2();
                r3.A04(this.A06, 0);
                C14260l7 r2 = this.A05;
                r3.A04(r2, 1);
                C28701Oq.A01(r2, r4, r3.A03(), A00);
                return true;
            }
        }
        return false;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
        AbstractC14200l1 A00 = A00(35);
        if (A00 != null) {
            AnonymousClass28D r3 = this.A07;
            C14210l2 r2 = new C14210l2();
            r2.A04(this.A06, 0);
            C14260l7 r1 = this.A05;
            r2.A04(r1, 1);
            C28701Oq.A01(r1, r3, r2.A03(), A00);
        }
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        AbstractC14200l1 A00;
        if (this.A00 == null || (A00 = A00(44)) == null) {
            return false;
        }
        AnonymousClass28D r6 = this.A07;
        C14210l2 r8 = new C14210l2();
        r8.A04(this.A06, 0);
        C14260l7 r5 = this.A05;
        r8.A04(r5, 1);
        r8.A04(Float.valueOf(scaleGestureDetector.getScaleFactor()), 2);
        View view = this.A03;
        r8.A04(Float.valueOf((this.A00.x * 100.0f) / ((float) view.getWidth())), 3);
        r8.A04(Float.valueOf((this.A00.y * 100.0f) / ((float) view.getHeight())), 4);
        C28701Oq.A01(r5, r6, r8.A03(), A00);
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        this.A00 = this.A04.A00(new PointF(scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY()));
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        this.A00 = null;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        AbstractC14200l1 A00 = A00(48);
        if (A00 != null) {
            AnonymousClass28D r5 = this.A07;
            C14210l2 r7 = new C14210l2();
            r7.A04(this.A06, 0);
            C14260l7 r4 = this.A05;
            r7.A04(r4, 1);
            View view = this.A03;
            r7.A04(Float.valueOf((f * 100.0f) / ((float) view.getWidth())), 2);
            r7.A04(Float.valueOf((f2 * 100.0f) / ((float) view.getHeight())), 3);
            Object A01 = C28701Oq.A01(r4, r5, r7.A03(), A00);
            if (!(A01 instanceof Boolean)) {
                C28691Op.A00("BloksFoaExtensionsGestureListener", "onScroll return value should return boolean (true if handled)");
            } else {
                boolean booleanValue = ((Boolean) A01).booleanValue();
                if (booleanValue) {
                    this.A02 = true;
                }
                this.A01 = true;
                return booleanValue;
            }
        }
        return false;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return A02() && A03();
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return !A02() && A03();
    }
}
