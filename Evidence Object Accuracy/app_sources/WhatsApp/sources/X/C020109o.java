package X;

import android.database.DataSetObserver;

/* renamed from: X.09o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020109o extends DataSetObserver {
    public final /* synthetic */ AnonymousClass0XR A00;

    public C020109o(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        AnonymousClass0XR r1 = this.A00;
        if (r1.A0D.isShowing()) {
            r1.Ade();
        }
    }

    @Override // android.database.DataSetObserver
    public void onInvalidated() {
        this.A00.dismiss();
    }
}
