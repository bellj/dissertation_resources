package X;

/* renamed from: X.0vW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20290vW {
    public long A00 = -1;
    public final C16490p7 A01;
    public final C16120oU A02;
    public final AnonymousClass00E A03;

    public C20290vW(C16490p7 r3, C16120oU r4) {
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = new C29821Uu().samplingRate;
    }

    public void A00(String str, long j) {
        boolean booleanValue;
        AnonymousClass00E r5 = this.A03;
        if (r5.A00()) {
            C29821Uu r4 = new C29821Uu();
            r4.A05 = str;
            r4.A03 = Long.valueOf(j);
            r4.A01 = Boolean.valueOf(AnonymousClass01I.A01());
            C16490p7 r8 = this.A01;
            r8.A04();
            C29561To r1 = r8.A05;
            synchronized (r1) {
                booleanValue = r1.A06(r1.A00).booleanValue();
            }
            r4.A00 = Boolean.valueOf(booleanValue);
            long j2 = this.A00;
            if (j2 == -1) {
                r8.A04();
                j2 = r8.A07.length() / 1048576;
                this.A00 = j2;
            }
            r4.A04 = Long.valueOf(j2);
            if (AnonymousClass009.A01 == Boolean.TRUE) {
                r4.A02 = 1;
            }
            this.A02.A08(r4, r5.A03 * 1);
        }
    }
}
