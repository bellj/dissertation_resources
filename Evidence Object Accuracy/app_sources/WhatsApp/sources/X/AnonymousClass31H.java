package X;

/* renamed from: X.31H  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31H extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;

    public AnonymousClass31H() {
        super(1722, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(13, this.A00);
        r3.Abe(1, this.A02);
        r3.Abe(7, this.A03);
        r3.Abe(3, this.A06);
        r3.Abe(15, this.A07);
        r3.Abe(8, this.A04);
        r3.Abe(10, this.A01);
        r3.Abe(9, this.A08);
        r3.Abe(2, this.A09);
        r3.Abe(16, this.A0A);
        r3.Abe(11, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCatalogBiz {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cartToggle", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogBizAction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogEntryPoint", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "collectionIndex", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkOpenFrom", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isOrderMsgAttached", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "orderId", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productId", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productIndex", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "quantity", this.A05);
        return C12960it.A0d("}", A0k);
    }
}
