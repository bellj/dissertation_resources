package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;
import com.whatsapp.HomePagerSlidingTabStrip;

/* renamed from: X.2M0  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2M0 extends HorizontalScrollView implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass2M0(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        if (this instanceof HomePagerSlidingTabStrip) {
            HomePagerSlidingTabStrip homePagerSlidingTabStrip = (HomePagerSlidingTabStrip) this;
            if (!homePagerSlidingTabStrip.A00) {
                homePagerSlidingTabStrip.A00 = true;
                homePagerSlidingTabStrip.generatedComponent();
            }
        } else if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
