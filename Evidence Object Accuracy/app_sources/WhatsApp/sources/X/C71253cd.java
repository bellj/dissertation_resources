package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.3cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71253cd implements Comparator {
    public final Collator A00;

    public C71253cd(AnonymousClass018 r3) {
        Collator instance = Collator.getInstance(C12970iu.A14(r3));
        this.A00 = instance;
        instance.setDecomposition(1);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        String str = ((C51492Vb) obj).A06;
        String str2 = ((C51492Vb) obj2).A06;
        if (str == null) {
            return str2 == null ? 0 : 1;
        }
        if (str2 == null) {
            return -1;
        }
        return this.A00.compare(str, str2);
    }
}
