package X;

import android.util.Pair;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;

/* renamed from: X.2Cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47822Cw {
    public final Pair A00;
    public final C28601Of A01;
    public final C28601Of A02;
    public final UserJid A03;

    public /* synthetic */ C47822Cw(Pair pair, UserJid userJid, HashMap hashMap, HashMap hashMap2) {
        this.A03 = userJid;
        this.A00 = pair;
        this.A01 = C28601Of.A00(hashMap);
        this.A02 = C28601Of.A00(hashMap2);
    }
}
