package X;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.Conversation;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.support.ReportSpamDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1kZ */
/* loaded from: classes2.dex */
public abstract class AbstractC36781kZ extends C28791Pa implements AbstractC36791ka {
    public C15370n3 A00;
    public final ActivityC000800j A01;
    public final AbstractC13860kS A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C15450nH A05;
    public final C16170oZ A06;
    public final C18330sH A07;
    public final AnonymousClass2TT A08;
    public final AnonymousClass2Dn A09 = new AnonymousClass41U(this);
    public final C22330yu A0A;
    public final C243915i A0B;
    public final C27131Gd A0C = new C36731kS(this);
    public final AnonymousClass10S A0D;
    public final C22700zV A0E;
    public final AbstractC13920kY A0F;
    public final C255319t A0G;
    public final AnonymousClass1A6 A0H;
    public final C17050qB A0I;
    public final C14820m6 A0J;
    public final AnonymousClass018 A0K;
    public final C14950mJ A0L;
    public final C19990v2 A0M;
    public final C20830wO A0N;
    public final C22100yW A0O;
    public final AbstractC33331dp A0P = new C857544a(this);
    public final C244215l A0Q;
    public final AbstractC14640lm A0R;
    public final C15860o1 A0S;
    public final AnonymousClass12F A0T;
    public final AnonymousClass12U A0U;
    public final C255719x A0V;
    public final AbstractC14440lR A0W;
    public final C21280xA A0X;

    public AbstractC36781kZ(ActivityC000800j r2, AbstractC13860kS r3, C14900mE r4, C15570nT r5, C15450nH r6, C16170oZ r7, C18330sH r8, AnonymousClass2TT r9, C22330yu r10, C243915i r11, AnonymousClass10S r12, C22700zV r13, AbstractC13920kY r14, C255319t r15, AnonymousClass1A6 r16, C17050qB r17, C14820m6 r18, AnonymousClass018 r19, C14950mJ r20, C19990v2 r21, C20830wO r22, C15370n3 r23, C22100yW r24, C244215l r25, AbstractC14640lm r26, C15860o1 r27, AnonymousClass12F r28, AnonymousClass12U r29, C255719x r30, AbstractC14440lR r31, C21280xA r32) {
        this.A01 = r2;
        this.A02 = r3;
        this.A0F = r14;
        this.A03 = r4;
        this.A04 = r5;
        this.A0W = r31;
        this.A0M = r21;
        this.A0U = r29;
        this.A05 = r6;
        this.A06 = r7;
        this.A0L = r20;
        this.A0X = r32;
        this.A0K = r19;
        this.A08 = r9;
        this.A0B = r11;
        this.A0D = r12;
        this.A0T = r28;
        this.A0S = r27;
        this.A0I = r17;
        this.A07 = r8;
        this.A0A = r10;
        this.A0E = r13;
        this.A0J = r18;
        this.A0G = r15;
        this.A0O = r24;
        this.A0V = r30;
        this.A0N = r22;
        this.A0Q = r25;
        this.A0H = r16;
        this.A0R = r26;
        this.A00 = r23;
    }

    public static void A00(Menu menu, int i, int i2) {
        menu.add(0, i, 0, i2);
    }

    public int A02() {
        C15860o1 r0 = this.A0S;
        AbstractC14640lm r2 = this.A0R;
        if (!r0.A0S(r2)) {
            if (!C16970q3.A03(this.A0J, this.A0M, r2)) {
                return R.string.menuitem_mute_notifications;
            }
        }
        return R.string.menuitem_unmute_notifications;
    }

    public void A03(Menu menu) {
        if (this.A05.A05(AbstractC15460nI.A0Q)) {
            A00(menu, 3, R.string.export_attachment);
        }
    }

    public void A04(MenuItem menuItem) {
        ActivityC000800j r4 = this.A01;
        SpannableString spannableString = new SpannableString(r4.getString(A02()));
        AbstractC14640lm r2 = this.A0R;
        if (C16970q3.A03(this.A0J, this.A0M, r2)) {
            spannableString.setSpan(new ForegroundColorSpan(AnonymousClass00T.A00(r4, R.color.list_item_disabled)), 0, spannableString.length(), 0);
        }
        if (Build.VERSION.SDK_INT == 19) {
            menuItem.setTitle(spannableString.toString());
        } else {
            menuItem.setTitle(spannableString);
        }
    }

    public void A05(MenuItem menuItem, int i, boolean z) {
        AnonymousClass2E0 r0;
        View actionView = menuItem.getActionView();
        float f = 0.4f;
        if (z) {
            f = 1.0f;
        }
        actionView.setAlpha(f);
        actionView.setEnabled(z);
        if (z) {
            if (!this.A0K.A04().A06) {
                r0 = new AnonymousClass2E0(0.0f, 0.0f, 0.2f, 0.0f);
            } else {
                r0 = new AnonymousClass2E0(0.2f, 0.0f, 0.0f, 0.0f);
            }
            actionView.setOnTouchListener(r0);
            actionView.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 17, menuItem));
            actionView.setOnLongClickListener(new View.OnLongClickListener(i) { // from class: X.3Mv
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    AbstractC36781kZ r8 = AbstractC36781kZ.this;
                    Toast A02 = r8.A03.A02(C12990iw.A0q(view, this.A00));
                    int[] A07 = C13000ix.A07();
                    Rect A0J = C12980iv.A0J();
                    view.getLocationOnScreen(A07);
                    view.getWindowVisibleDisplayFrame(A0J);
                    int height = (A07[1] + view.getHeight()) - A0J.top;
                    int i2 = A07[0];
                    if (C28141Kv.A01(r8.A0K)) {
                        Point point = new Point();
                        C12970iu.A17(r8.A01, point);
                        if (A02.getView() != null) {
                            A02.getView().measure(point.x, point.y);
                            i2 -= A02.getView().getMeasuredWidth();
                        }
                    } else {
                        i2 += view.getWidth();
                    }
                    A02.setGravity(51, i2, height);
                    A02.show();
                    return true;
                }
            });
        }
    }

    @Override // X.AbstractC36791ka
    public void AOk(Menu menu) {
        SubMenu addSubMenu;
        MenuItem add;
        int i;
        if (!(this instanceof AnonymousClass2xX)) {
            AnonymousClass2xY r3 = (AnonymousClass2xY) this;
            Log.i("contactconversationmenu/oncreateoptionsmenu");
            C15570nT r0 = r3.A02;
            AbstractC14640lm r2 = r3.A0R;
            r0.A0F(r2);
            C14850m9 r1 = r3.A0B;
            boolean A00 = C41861uH.A00(r1, r2);
            if (!A00) {
                if (!((AbstractC36781kZ) r3).A00.A0J()) {
                    boolean A07 = r3.A07();
                    MenuItem add2 = menu.add(0, 26, 0, R.string.video_call);
                    add2.setActionView(R.layout.video_call_menu_item);
                    r3.A05(add2, R.string.video_call, A07);
                    add = menu.add(0, 25, 0, R.string.audio_call);
                    add.setActionView(R.layout.audio_call_menu_item);
                    add.getActionView().setContentDescription(((AbstractC36781kZ) r3).A01.getString(R.string.audio_call));
                    r3.A05(add, R.string.audio_call, A07);
                    i = 2;
                    add2.setShowAsAction(2);
                } else {
                    boolean A072 = r1.A07(1464);
                    r3.A04.A03(new AbstractC30111Wd(menu, r3) { // from class: X.3Up
                        public final /* synthetic */ Menu A00;
                        public final /* synthetic */ AnonymousClass2xY A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC30111Wd
                        public final void ANP(C30141Wg r9) {
                            int i2;
                            MenuItem add3;
                            AnonymousClass2xY r5 = this.A01;
                            Menu menu2 = this.A00;
                            if (r9 == null) {
                                return;
                            }
                            if (!r9.A0K || !r5.A0B.A07(957)) {
                                if (r5.A03.A00(r9)) {
                                    i2 = R.string.view_shop;
                                    add3 = menu2.add(0, 30, 1, R.string.view_shop);
                                    add3.setActionView(R.layout.view_shop_menu_item);
                                    View actionView = add3.getActionView();
                                    actionView.setTag(R.id.tag_shop_url, r9.A0B);
                                    C12960it.A0r(((AbstractC36781kZ) r5).A01, actionView, R.string.view_shop);
                                } else if (r9.A0I) {
                                    i2 = R.string.view_catalog;
                                    add3 = menu2.add(0, 29, 0, R.string.view_catalog);
                                    ActivityC000800j r22 = ((AbstractC36781kZ) r5).A01;
                                    ImageView imageView = (ImageView) C12980iv.A0N(r22, R.layout.view_menu_item_image_button_placeholder);
                                    C12990iw.A0x(r22, imageView, R.drawable.ic_action_view_catalog);
                                    C12960it.A0r(r22, imageView, R.string.view_catalog);
                                    add3.setActionView(imageView);
                                } else {
                                    return;
                                }
                                r5.A05(add3, i2, true);
                                add3.setShowAsAction(2);
                            }
                        }
                    }, (UserJid) r2);
                    if (!r3.A07.A02((UserJid) ((AbstractC36781kZ) r3).A00.A0B(UserJid.class)) || A072) {
                        boolean A073 = r3.A07();
                        add = menu.add(0, 28, 5, R.string.call);
                        add.setActionView(R.layout.audio_video_call_menu_item);
                        add.getActionView().setContentDescription(((AbstractC36781kZ) r3).A01.getString(R.string.call));
                        r3.A05(add, R.string.call, A073);
                        i = 2;
                    }
                }
                add.setShowAsAction(i);
            }
            boolean A0J = ((AbstractC36781kZ) r3).A00.A0J();
            A00(menu, 21, R.string.view_contact);
            if (A0J) {
                A00(menu, 22, R.string.add_contact);
                if (!A00) {
                    A00(menu, 9, R.string.report_spam);
                }
                A00(menu, 23, R.string.block);
                A00(menu, 24, R.string.unblock);
                A00(menu, 7, R.string.search);
                A00(menu, 4, r3.A02());
                if (r3.A06()) {
                    A00(menu, 12, R.string.ephemeral_setting);
                }
                A00(menu, 5, R.string.wallpaper);
                addSubMenu = menu.addSubMenu(0, 1, 0, R.string.more);
                addSubMenu.clearHeader();
                addSubMenu.clearHeader();
                A00(addSubMenu, 6, R.string.view_conversation_media);
            } else {
                A00(menu, 22, R.string.add_contact);
                A00(menu, 6, R.string.view_conversation_media);
                A00(menu, 7, R.string.search);
                A00(menu, 4, r3.A02());
                if (r3.A06()) {
                    A00(menu, 12, R.string.ephemeral_setting);
                }
                A00(menu, 5, R.string.wallpaper);
                addSubMenu = menu.addSubMenu(0, 1, 0, R.string.more);
                addSubMenu.clearHeader();
                addSubMenu.clearHeader();
                A00(addSubMenu, 9, R.string.report_spam);
                A00(addSubMenu, 23, R.string.block);
                A00(addSubMenu, 24, R.string.unblock);
            }
            A00(addSubMenu, 8, R.string.clear_chat);
            r3.A03(addSubMenu);
            A00(addSubMenu, 2, R.string.add_shortcut_short);
            return;
        }
        Log.i("listconversationmenu/oncreateoptionsmenu");
        A00(menu, 21, R.string.list_info);
        A00(menu, 6, R.string.view_list_media);
        A00(menu, 7, R.string.search);
        A00(menu, 5, R.string.wallpaper);
        SubMenu addSubMenu2 = menu.addSubMenu(0, 1, 0, R.string.more);
        addSubMenu2.clearHeader();
        A00(addSubMenu2, 8, R.string.clear_chat);
        A03(addSubMenu2);
        A00(addSubMenu2, 2, R.string.add_shortcut_short);
    }

    @Override // X.AbstractC36791ka
    public boolean ATH(MenuItem menuItem) {
        Intent intent;
        String packageName;
        String str;
        this.A0B.A01 = 6;
        int itemId = menuItem.getItemId();
        if (itemId == 12) {
            AbstractC14640lm r3 = this.A0R;
            if (r3 instanceof UserJid) {
                UserJid userJid = (UserJid) r3;
                if (!this.A0E.A02(userJid)) {
                    ActivityC000800j r2 = this.A01;
                    r2.startActivity(C14960mK.A0E(r2, r3, this.A0M.A01(userJid), 3));
                    return true;
                }
            }
        } else if (itemId != 16908332) {
            switch (itemId) {
                case 2:
                    this.A07.A05(this.A00);
                    return true;
                case 3:
                    if (this.A0I.A02()) {
                        Log.w("conversation/email-attachment/need-sd-card");
                        AbstractC13860kS r22 = this.A02;
                        boolean A00 = C14950mJ.A00();
                        int i = R.string.need_sd_card_shared_storage;
                        if (A00) {
                            i = R.string.need_sd_card;
                        }
                        r22.Ado(i);
                        return true;
                    }
                    C255319t r8 = this.A0G;
                    ActivityC000800j r7 = this.A01;
                    AbstractC13860kS r6 = this.A02;
                    AbstractC14640lm r5 = this.A0R;
                    C15370n3 r32 = this.A00;
                    if (r8.A04.A0B(null, r5, 1, 2).size() > 0) {
                        C36021jC.A01(r7, 10);
                        return true;
                    }
                    r8.A01(r7, r6, r32, false);
                    return true;
                case 4:
                    AbstractC14640lm r33 = this.A0R;
                    if (C16970q3.A03(this.A0J, this.A0M, r33)) {
                        ActivityC000800j r23 = this.A01;
                        C16970q3.A00(r23, r23.findViewById(R.id.footer), this.A06, r33);
                        return true;
                    } else if (this.A0S.A0S(r33)) {
                        this.A0W.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this, 0));
                        return true;
                    } else {
                        MuteDialogFragment.A00(r33).A1F(this.A01.A0V(), "MuteDialogFragment");
                        return true;
                    }
                case 5:
                    Log.i("conversation/menu/wallpaper/");
                    ActivityC000800j r52 = this.A01;
                    AbstractC14640lm r34 = this.A0R;
                    if (r34 == null || C41691tw.A08(r52)) {
                        intent = new Intent();
                        packageName = r52.getPackageName();
                        str = "com.whatsapp.settings.chat.wallpaper.WallpaperCurrentPreviewActivity";
                    } else {
                        intent = new Intent();
                        packageName = r52.getPackageName();
                        str = "com.whatsapp.settings.chat.wallpaper.WallpaperCategoriesActivity";
                    }
                    Intent className = intent.setClassName(packageName, str);
                    className.putExtra("chat_jid", C15380n4.A03(r34));
                    r52.startActivity(className);
                    return true;
                case 6:
                    ActivityC000800j r1 = this.A01;
                    r1.startActivity(C14960mK.A0C(r1, this.A0R));
                    return true;
                case 7:
                    this.A01.onSearchRequested();
                    return true;
                case 8:
                    this.A0V.A06(this.A01).A00(new AbstractC14590lg() { // from class: X.5A6
                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            AbstractC36781kZ r0 = AbstractC36781kZ.this;
                            r0.A0V.A07(r0.A0R, new C1115059q(r0));
                        }
                    });
                    return true;
                case 9:
                    this.A0O.A05().A00(new AbstractC14590lg() { // from class: X.3bJ
                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            AbstractC36781kZ r24 = AbstractC36781kZ.this;
                            Boolean bool = (Boolean) obj;
                            AbstractC13860kS r12 = r24.A02;
                            if (!r12.AJN()) {
                                r12.Adm(ReportSpamDialogFragment.A00(r24.A0R, null, null, "overflow_menu_report", 1, bool.booleanValue(), true, true, true));
                            }
                        }
                    });
                    return true;
                case 10:
                    this.A03.A0F("Export chat for internal testing", 0);
                    AnonymousClass1A6 r0 = this.A0H;
                    r0.A01.Aaz(new AnonymousClass37F(this.A01, this.A0R, r0.A00), new Void[0]);
                    break;
                default:
                    return false;
            }
        } else {
            this.A01.finish();
            return true;
        }
        return true;
    }

    @Override // X.AbstractC36791ka
    public boolean AUA(Menu menu) {
        boolean z = false;
        if (((Conversation) this.A0F).A1g.getConversationCursorAdapter().getCount() > 0) {
            z = true;
        }
        menu.findItem(8).setVisible(z);
        menu.findItem(7).setVisible(z);
        MenuItem findItem = menu.findItem(3);
        if (findItem != null) {
            findItem.setVisible(z);
        }
        MenuItem findItem2 = menu.findItem(9);
        if (findItem2 != null) {
            findItem2.setVisible(true);
        }
        MenuItem findItem3 = menu.findItem(1);
        SubMenu subMenu = findItem3.getSubMenu();
        MenuItem findItem4 = subMenu.findItem(10);
        if (findItem4 != null) {
            findItem4.setVisible(z);
        }
        findItem3.setVisible(subMenu.hasVisibleItems());
        return true;
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        this.A0D.A03(this.A0C);
        this.A0A.A03(this.A09);
        this.A0Q.A03(this.A0P);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        this.A0D.A04(this.A0C);
        this.A0A.A04(this.A09);
        this.A0Q.A04(this.A0P);
    }
}
