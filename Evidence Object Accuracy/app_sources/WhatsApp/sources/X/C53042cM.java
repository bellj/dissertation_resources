package X;

import android.content.Context;
import android.widget.FrameLayout;
import com.facebook.redex.IDxProviderShape16S0100000_2_I1;

/* renamed from: X.2cM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53042cM extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass17S A00;
    public AnonymousClass12P A01;
    public AbstractC15710nm A02;
    public AnonymousClass2Q0 A03;
    public C50542Py A04;
    public C15570nT A05;
    public C15450nH A06;
    public C252818u A07;
    public AnonymousClass1D6 A08;
    public C90814Pi A09;
    public C14650lo A0A;
    public C18850tA A0B;
    public AnonymousClass130 A0C;
    public C17050qB A0D;
    public C14830m7 A0E;
    public C16590pI A0F;
    public C14820m6 A0G;
    public AnonymousClass018 A0H;
    public AnonymousClass4EK A0I;
    public AnonymousClass10Q A0J;
    public C15680nj A0K;
    public C14850m9 A0L;
    public C22050yP A0M;
    public C16120oU A0N;
    public C26121Cc A0O;
    public C17070qD A0P;
    public C26141Ce A0Q;
    public C25901Bg A0R;
    public C26111Cb A0S;
    public C25681Ai A0T;
    public C26851Fb A0U;
    public AnonymousClass1CT A0V;
    public AnonymousClass12S A0W;
    public AnonymousClass12O A0X;
    public AbstractC14440lR A0Y;
    public AnonymousClass2P7 A0Z;
    public boolean A0a;
    public int[] A0b;
    public final AnonymousClass01F A0c;
    public final AbstractC116635Wf A0d;
    public final AbstractC116635Wf A0e;
    public final AbstractC116635Wf A0f;
    public final AbstractC116635Wf A0g;
    public final AbstractC116635Wf A0h;
    public final AbstractC116635Wf A0i;
    public final C58932s0 A0j;
    public final C58942s1 A0k;
    public final C68003Ts A0l;
    public final AnonymousClass52Q A0m;
    public final C68063Ty A0n;
    public final C68023Tu A0o;
    public final AnonymousClass52P A0p;

    public C53042cM(Context context, AnonymousClass01F r25) {
        super(context, null, 0);
        if (!this.A0a) {
            this.A0a = true;
            AnonymousClass2P6 r1 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r3 = r1.A06;
            this.A0E = C12980iv.A0b(r3);
            this.A0L = C12960it.A0S(r3);
            this.A05 = C12970iu.A0S(r3);
            this.A0Y = C12960it.A0T(r3);
            this.A0F = C12970iu.A0X(r3);
            this.A02 = C12970iu.A0Q(r3);
            this.A0S = (C26111Cb) r3.AJ3.get();
            this.A0N = C12970iu.A0b(r3);
            this.A00 = (AnonymousClass17S) r3.A0F.get();
            this.A06 = (C15450nH) r3.AII.get();
            this.A0B = (C18850tA) r3.AKx.get();
            this.A07 = (C252818u) r3.AMy.get();
            this.A01 = C12980iv.A0W(r3);
            this.A0C = C12990iw.A0Y(r3);
            this.A0H = C12960it.A0R(r3);
            this.A0P = (C17070qD) r3.AFC.get();
            this.A0W = (AnonymousClass12S) r3.AMF.get();
            this.A0M = (C22050yP) r3.A7v.get();
            this.A0D = (C17050qB) r3.ABL.get();
            this.A0X = (AnonymousClass12O) r3.AMG.get();
            this.A08 = (AnonymousClass1D6) r3.A1G.get();
            this.A0J = (AnonymousClass10Q) r3.AGu.get();
            this.A0G = C12970iu.A0Z(r3);
            this.A0K = (C15680nj) r3.A4e.get();
            this.A0A = C12980iv.A0Y(r3);
            this.A0I = new AnonymousClass4EK();
            this.A0V = (AnonymousClass1CT) r3.AMB.get();
            this.A0T = (C25681Ai) r3.AJE.get();
            this.A0R = (C25901Bg) r3.AEg.get();
            this.A0U = (C26851Fb) r3.AHq.get();
            this.A0O = (C26121Cc) r3.AFA.get();
            this.A0Q = (C26141Ce) r3.AFG.get();
            this.A03 = (AnonymousClass2Q0) r1.A00.get();
            this.A04 = (C50542Py) r1.A01.get();
        }
        this.A0b = new int[]{33, 16, 12, 27, 1, 11, 22, 20, 13, 14, 15, 21};
        this.A0c = r25;
        IDxProviderShape16S0100000_2_I1 iDxProviderShape16S0100000_2_I1 = new IDxProviderShape16S0100000_2_I1(this, 0);
        AnonymousClass2SL r0 = new AnonymousClass2SL(this.A0G, this.A0N);
        AnonymousClass028.A0g(this, new C74323hn(this));
        C14850m9 r13 = this.A0L;
        this.A0m = new AnonymousClass52Q(this, this.A0I, this.A0J, r13);
        C14830m7 r10 = this.A0E;
        C16120oU r15 = this.A0N;
        this.A0h = new C68053Tx(this, r10, this.A0G, this.A0K, r13, this.A0M, r15);
        C14830m7 r102 = this.A0E;
        C14850m9 r132 = this.A0L;
        C16590pI r11 = this.A0F;
        C14960mK r14 = new C14960mK();
        C26111Cb r6 = this.A0S;
        C17070qD r152 = this.A0P;
        C14820m6 r12 = this.A0G;
        this.A0n = new C68063Ty(this, r102, r11, r12, r132, r14, r152, this.A0R, r6, this.A0T);
        C16120oU r62 = this.A0N;
        AnonymousClass12P r7 = this.A01;
        this.A0i = new C68013Tt(r7, this, r12, r62, iDxProviderShape16S0100000_2_I1);
        this.A0f = new C67993Tr(this, this.A06, r0);
        this.A0d = new C68043Tw(this, this.A0B, r132);
        this.A0o = new C68023Tu(this, r132, this.A0V, this.A0W, this.A0X, iDxProviderShape16S0100000_2_I1);
        this.A0j = new C58932s0(this, this.A0O);
        this.A0k = new C58942s1(this, this.A0Q);
        C252818u r5 = this.A07;
        this.A0e = new C68033Tv(context, r7, this, r5, this.A08, r12, this.A0H, r62);
        this.A0l = new C68003Ts(r25, this, r5, r102, this.A0U, new AnonymousClass4S4(r12, r132, r62, new AnonymousClass4LA(r12)));
        this.A0p = new AnonymousClass52P(this);
        this.A0g = new C67983Tq(this, new C89254Jg(r12), r132, iDxProviderShape16S0100000_2_I1);
    }

    public void A00(int i, int i2) {
        AnonymousClass30C r1 = new AnonymousClass30C();
        r1.A00 = Integer.valueOf(i2);
        r1.A01 = Integer.valueOf(i);
        this.A0N.A06(r1);
    }

    public void A01(C90814Pi r12) {
        this.A09 = r12;
        this.A0n.AIR();
        AbstractC116635Wf r10 = this.A0h;
        r10.AIR();
        AbstractC116635Wf r9 = this.A0i;
        r9.AIR();
        C68023Tu r8 = this.A0o;
        r8.AIR();
        AbstractC116635Wf r6 = this.A0f;
        r6.AIR();
        C58942s1 r5 = this.A0k;
        r5.AIR();
        C58932s0 r4 = this.A0j;
        r4.AIR();
        AbstractC116635Wf r7 = this.A0d;
        r7.AIR();
        AbstractC116635Wf r3 = this.A0e;
        r3.AIR();
        C68003Ts r2 = this.A0l;
        r2.AIR();
        int bannerType = getBannerType();
        if (bannerType == 16) {
            r7.AfF();
        } else if (bannerType == 12) {
            r8.AfF();
        } else if (bannerType == 27) {
            r2.AfF();
        } else {
            if (bannerType != 0) {
                if (bannerType == 1) {
                    r6 = r10;
                } else if (bannerType == 11) {
                    r6 = r9;
                } else if (bannerType == 20) {
                    r4.AfF();
                } else if (bannerType == 22) {
                    r6 = r3;
                } else if (bannerType == 33) {
                    r6 = this.A0g;
                } else if (bannerType != 13) {
                    if (bannerType == 14) {
                        r5.AfF();
                    } else {
                        throw C12960it.A0U(C12960it.A0W(bannerType, "Unhandled banner type: "));
                    }
                }
                r6.AfF();
            }
            r5.A02(bannerType);
            r4.A02(bannerType);
        }
    }

    public boolean A02(C90814Pi r8) {
        this.A09 = r8;
        if (this.A0X.A01() != null || this.A0d.AdK()) {
            return true;
        }
        C14820m6 r5 = this.A0G;
        if (r5.A00.getInt("education_banner_count", 0) >= 3) {
            if (!r5.A1J("education_banner_timestamp", ((long) 7) * 86400000)) {
                return false;
            }
            r5.A0P(0);
        }
        if (getBannerType() == 0) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0Z;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0Z = r0;
        }
        return r0.generatedComponent();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0023, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0076 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0023 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int getBannerType() {
        /*
            r7 = this;
            int[] r3 = r7.A0b
            int r2 = r3.length
            r6 = 0
            r1 = 0
        L_0x0005:
            if (r1 >= r2) goto L_0x0077
            r5 = r3[r1]
            r4 = 1
            if (r5 == r4) goto L_0x006b
            r4 = 16
            if (r5 == r4) goto L_0x0068
            r4 = 20
            if (r5 == r4) goto L_0x005f
            r4 = 22
            if (r5 == r4) goto L_0x006e
            r0 = 27
            if (r5 == r0) goto L_0x0057
            r4 = 33
            if (r5 == r4) goto L_0x0054
            switch(r5) {
                case 11: goto L_0x0049;
                case 12: goto L_0x003e;
                case 13: goto L_0x0033;
                case 14: goto L_0x0026;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r1 = r1 + 1
            goto L_0x0005
        L_0x0026:
            X.2s1 r0 = r7.A0k
            X.1Cd r0 = r0.A02
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x0023
            r0 = 14
            return r0
        L_0x0033:
            X.5Wf r0 = r7.A0f
            boolean r0 = r0.AdK()
            if (r0 == 0) goto L_0x0023
            r0 = 13
            return r0
        L_0x003e:
            X.3Tu r0 = r7.A0o
            boolean r0 = r0.AdK()
            if (r0 == 0) goto L_0x0023
            r0 = 12
            return r0
        L_0x0049:
            X.5Wf r0 = r7.A0i
            boolean r0 = r0.AdK()
            if (r0 == 0) goto L_0x0023
            r0 = 11
            return r0
        L_0x0054:
            X.5Wf r0 = r7.A0g
            goto L_0x0070
        L_0x0057:
            X.3Ts r0 = r7.A0l
            X.2cM r0 = r0.A02
            r0.getContext()
            goto L_0x0023
        L_0x005f:
            X.2s0 r0 = r7.A0j
            X.1Cd r0 = r0.A02
            boolean r0 = r0.A02()
            goto L_0x0074
        L_0x0068:
            X.5Wf r0 = r7.A0d
            goto L_0x0070
        L_0x006b:
            X.5Wf r0 = r7.A0h
            goto L_0x0070
        L_0x006e:
            X.5Wf r0 = r7.A0e
        L_0x0070:
            boolean r0 = r0.AdK()
        L_0x0074:
            if (r0 == 0) goto L_0x0023
            return r4
        L_0x0077:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53042cM.getBannerType():int");
    }
}
