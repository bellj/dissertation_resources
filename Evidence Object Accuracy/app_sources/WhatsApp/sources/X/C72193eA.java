package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3eA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72193eA extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $onSuccess;
    public final /* synthetic */ AnonymousClass12V this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72193eA(AnonymousClass12V r2, AnonymousClass1J7 r3) {
        super(1);
        this.this$0 = r2;
        this.$onSuccess = r3;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C16700pc.A0E(obj, 0);
        AnonymousClass12V r4 = this.this$0;
        r4.A00.A0H(new RunnableBRunnable0Shape3S0300000_I1(r4, this.$onSuccess, obj, 31));
        return AnonymousClass1WZ.A00;
    }
}
