package X;

import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.IndiaUpiInteropSendToUpiActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentTwoFactorNudgeFragment;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentIncentiveViewFragment;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: X.6Cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134006Cy implements AbstractC1310961f {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public C134006Cy(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AbstractC1310961f
    public void ALy() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        indiaUpiSendPaymentActivity.A3O(57, "available_payment_methods_prompt");
        indiaUpiSendPaymentActivity.A3P(indiaUpiSendPaymentActivity);
    }

    @Override // X.AbstractC136486Ms
    public void AMA(String str) {
        C123715nk r2 = this.A00.A03;
        boolean z = !str.isEmpty();
        r2.A00.setEnabled(z);
        r2.A00.setClickable(z);
    }

    @Override // X.AbstractC136486Ms
    public void APq(String str) {
        String str2;
        String str3;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D.AKg(C12980iv.A0i(), 51, "max_amount_shake", ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0d);
        AnonymousClass61I.A02(AnonymousClass61I.A00(((ActivityC13790kL) indiaUpiSendPaymentActivity).A05, null, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0U, null, true), ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D, "new_payment");
        if (((ActivityC13810kN) indiaUpiSendPaymentActivity).A0C.A07(1933)) {
            if (indiaUpiSendPaymentActivity.A04.A00.A00.compareTo(new BigDecimal(((ActivityC13810kN) indiaUpiSendPaymentActivity).A06.A02(AbstractC15460nI.A1z))) == 0) {
                str2 = "-10022";
                str3 = "MAX_AMOUNT_2K_INLINE";
            } else {
                str2 = "-10020";
                str3 = "MAX_AMOUNT_100K";
            }
            indiaUpiSendPaymentActivity.A3e(str2, str3);
        }
    }

    @Override // X.AbstractC136486Ms
    public void AQf(String str, boolean z) {
        if (!TextUtils.isEmpty(str) && !z) {
            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
            indiaUpiSendPaymentActivity.A2l(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0U);
        }
    }

    @Override // X.AbstractC1310961f
    public void AR5() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        AnonymousClass2S0 r3 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0U;
        if (r3 != null && r3.A01 != null) {
            AnonymousClass6BE r2 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D;
            Bundle A0D = C12970iu.A0D();
            PaymentIncentiveViewFragment paymentIncentiveViewFragment = new PaymentIncentiveViewFragment(r2, r3);
            paymentIncentiveViewFragment.A0U(A0D);
            paymentIncentiveViewFragment.A05 = new C125805rq(paymentIncentiveViewFragment);
            indiaUpiSendPaymentActivity.Adm(paymentIncentiveViewFragment);
        }
    }

    @Override // X.AbstractC1310961f
    public void ATT() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        if (C15380n4.A0J(((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0E) && ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A00 == 0) {
            indiaUpiSendPaymentActivity.A2i(null);
        } else if (!indiaUpiSendPaymentActivity.A3Z()) {
        } else {
            if (!indiaUpiSendPaymentActivity.A3Y()) {
                indiaUpiSendPaymentActivity.startActivity(C12990iw.A0D(indiaUpiSendPaymentActivity, IndiaUpiInteropSendToUpiActivity.class));
            } else {
                C36021jC.A01(indiaUpiSendPaymentActivity, 34);
            }
        }
    }

    @Override // X.AbstractC1310961f
    public void ATV() {
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        PaymentMethodsListPickerFragment A00 = PaymentMethodsListPickerFragment.A00(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0g);
        A00.A08 = new AnonymousClass6CU(indiaUpiSendPaymentActivity, A00);
        A00.A06 = new AnonymousClass6CQ(indiaUpiSendPaymentActivity);
        paymentBottomSheet.A01 = A00;
        indiaUpiSendPaymentActivity.Adl(paymentBottomSheet, "18");
    }

    @Override // X.AbstractC1310961f
    public void ATa() {
        this.A00.A3O(64, "enter_user_payment_id");
    }

    @Override // X.AbstractC1310961f
    public void AV5(C30821Yy r14, String str) {
        String str2;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B.AL9("request_payment", 123);
        if (((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B == null) {
            ALy();
            return;
        }
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0A = r14;
        if (indiaUpiSendPaymentActivity.A3Y()) {
            PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
            ConfirmPaymentFragment A00 = ConfirmPaymentFragment.A00(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B, null, !((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0i ? 1 : 0);
            A00.A0L = new AnonymousClass6CJ(((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A02.A02("INR"), indiaUpiSendPaymentActivity, paymentBottomSheet);
            A00.A0M = new AnonymousClass6CM(indiaUpiSendPaymentActivity);
            paymentBottomSheet.A01 = A00;
            indiaUpiSendPaymentActivity.Adm(paymentBottomSheet);
            return;
        }
        C30931Zj r7 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m;
        C31021Zs[] r5 = new C31021Zs[1];
        UserJid userJid = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0G;
        if (userJid != null) {
            str2 = userJid.toString();
        } else {
            str2 = "";
        }
        r5[0] = new C31021Zs("receiver_jid", str2);
        AbstractC15340mz r10 = null;
        r7.A09(null, "requesting payment ", r5);
        PaymentView A2e = indiaUpiSendPaymentActivity.A2e();
        if (A2e == null || A2e.getStickerIfSelected() == null) {
            ((ActivityC13830kP) indiaUpiSendPaymentActivity).A05.Ab2(new Runnable() { // from class: X.6GG
                @Override // java.lang.Runnable
                public final void run() {
                    String str3;
                    List list;
                    C30921Zi r0;
                    IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = C134006Cy.this.A00;
                    C18610sj r4 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0M;
                    PaymentView paymentView = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W;
                    if (paymentView != null) {
                        str3 = paymentView.getPaymentNote();
                    } else {
                        str3 = "";
                    }
                    PaymentView paymentView2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W;
                    if (paymentView2 != null) {
                        list = paymentView2.getMentionedJids();
                    } else {
                        list = null;
                    }
                    C28861Ph A2f = indiaUpiSendPaymentActivity2.A2f(str3, list);
                    UserJid userJid2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0C;
                    AnonymousClass009.A05(userJid2);
                    C30821Yy r1 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0A;
                    PaymentView paymentView3 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W;
                    if (paymentView3 != null) {
                        r0 = paymentView3.getPaymentBackground();
                    } else {
                        r0 = null;
                    }
                    r4.A07(r1, r0, userJid2, A2f);
                }
            });
            indiaUpiSendPaymentActivity.AaN();
            indiaUpiSendPaymentActivity.A2q();
            indiaUpiSendPaymentActivity.A2g(1);
            return;
        }
        indiaUpiSendPaymentActivity.A2C(R.string.register_wait_message);
        C20350vc r6 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0S;
        PaymentView paymentView = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0W;
        AnonymousClass009.A03(paymentView);
        AnonymousClass1KS stickerIfSelected = paymentView.getStickerIfSelected();
        AnonymousClass009.A05(stickerIfSelected);
        AbstractC14640lm r8 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0E;
        AnonymousClass009.A05(r8);
        UserJid userJid2 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0G;
        long j = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A02;
        if (j != 0) {
            r10 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A09.A0K.A00(j);
        }
        PaymentView paymentView2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0W;
        C117315Zl.A0R(((ActivityC13810kN) indiaUpiSendPaymentActivity).A05, r6.A01(paymentView2.getPaymentBackground(), r8, userJid2, r10, stickerIfSelected, paymentView2.getStickerSendOrigin()), new AbstractC14590lg(r14, this) { // from class: X.6EX
            public final /* synthetic */ C30821Yy A00;
            public final /* synthetic */ C134006Cy A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C134006Cy r1 = this.A01;
                IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = r1.A00;
                AnonymousClass61P r2 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0V;
                AbstractC14640lm r62 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0E;
                AnonymousClass009.A05(r62);
                UserJid userJid3 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0G;
                long j2 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A02;
                String paymentNote = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W.getPaymentNote();
                List mentionedJids = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W.getMentionedJids();
                r2.A03(indiaUpiSendPaymentActivity2, this.A00, ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0W.getPaymentBackground(), r62, userJid3, (AnonymousClass22U) obj, new C133826Cg(r1), paymentNote, mentionedJids, j2);
            }
        });
    }

    @Override // X.AbstractC1310961f
    public void AVp(C30821Yy r10) {
        BigDecimal bigDecimal;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B.AL9("send_payment", 123);
        indiaUpiSendPaymentActivity.A3O(5, "new_payment");
        if (((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B == null) {
            ALy();
            return;
        }
        boolean z = true;
        if (indiaUpiSendPaymentActivity.A09) {
            if (indiaUpiSendPaymentActivity.A00 != 5) {
                indiaUpiSendPaymentActivity.A08 = true;
                indiaUpiSendPaymentActivity.A3b();
                return;
            } else if (!((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0i && (bigDecimal = indiaUpiSendPaymentActivity.A06) != null && r10.A00.compareTo(bigDecimal) > 0) {
                C36021jC.A01(indiaUpiSendPaymentActivity, 39);
                return;
            }
        }
        AbstractC28901Pl r2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B;
        C119755f3 r0 = (C119755f3) r2.A08;
        if (r0 == null || C12970iu.A1Y(r0.A05.A00)) {
            if (((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C.A01().getBoolean("payments_upi_pin_primer_dialog_shown", false)) {
                C12960it.A0t(C117295Zj.A05(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C), "payments_upi_pin_primer_dialog_shown", false);
            } else {
                int A02 = ((ActivityC13810kN) indiaUpiSendPaymentActivity).A0C.A02(1124);
                String[] split = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C.A05().split(";");
                int length = split.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        if (split[i].equalsIgnoreCase(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0B.A0A)) {
                            break;
                        }
                        i++;
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!(!indiaUpiSendPaymentActivity.A05.A01().isEmpty()) && z && A02 > 0 && ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C.A01().getInt("payments_two_factor_nudge_count", 0) < A02) {
                    C18600si r1 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C;
                    if (r1.A01.A00() - C12980iv.A0F(r1.A01(), "payments_last_two_factor_nudge_time") > TimeUnit.HOURS.toMillis(24)) {
                        IndiaUpiPaymentTwoFactorNudgeFragment indiaUpiPaymentTwoFactorNudgeFragment = new IndiaUpiPaymentTwoFactorNudgeFragment();
                        indiaUpiPaymentTwoFactorNudgeFragment.A05 = indiaUpiSendPaymentActivity;
                        indiaUpiSendPaymentActivity.Adm(indiaUpiPaymentTwoFactorNudgeFragment);
                        return;
                    }
                }
            }
            PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
            paymentBottomSheet.A01 = indiaUpiSendPaymentActivity.A3J(r10, paymentBottomSheet);
            indiaUpiSendPaymentActivity.Adm(paymentBottomSheet);
            return;
        }
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("extra_bank_account", r2);
        IndiaUpiPinPrimerDialogFragment indiaUpiPinPrimerDialogFragment = new IndiaUpiPinPrimerDialogFragment();
        indiaUpiPinPrimerDialogFragment.A0U(A0D);
        indiaUpiPinPrimerDialogFragment.A04 = indiaUpiSendPaymentActivity;
        PaymentBottomSheet paymentBottomSheet2 = new PaymentBottomSheet();
        paymentBottomSheet2.A01 = indiaUpiPinPrimerDialogFragment;
        indiaUpiSendPaymentActivity.Adl(paymentBottomSheet2, "IndiaUpiPinPrimerDialogFragment");
        C12960it.A0t(C117295Zj.A05(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C), "payments_upi_pin_primer_dialog_shown", true);
    }

    @Override // X.AbstractC1310961f
    public void AVq() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        AbstractActivityC121685jC.A1o(indiaUpiSendPaymentActivity, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0U, 47);
    }

    @Override // X.AbstractC1310961f
    public void AVs() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        Object[] A1b = C12970iu.A1b();
        A1b[0] = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A03.A08(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A08);
        indiaUpiSendPaymentActivity.Adr(A1b, 0, R.string.payments_cancel);
    }

    @Override // X.AbstractC1310961f
    public void AXP(boolean z) {
        int i;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        AnonymousClass2S0 r2 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0U;
        AnonymousClass6BE r1 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D;
        if (z) {
            i = 49;
        } else {
            i = 48;
        }
        AbstractActivityC121685jC.A1o(indiaUpiSendPaymentActivity, r1, r2, i);
        indiaUpiSendPaymentActivity.A3N();
    }
}
