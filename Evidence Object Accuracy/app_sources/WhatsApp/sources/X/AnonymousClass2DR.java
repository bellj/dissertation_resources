package X;

import java.util.Map;

/* renamed from: X.2DR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2DR {
    public final /* synthetic */ C27661Io A00;
    public final /* synthetic */ AnonymousClass46T A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ Map A04;

    public AnonymousClass2DR(C27661Io r1, AnonymousClass46T r2, String str, String str2, Map map) {
        this.A00 = r1;
        this.A02 = str;
        this.A03 = str2;
        this.A01 = r2;
        this.A04 = map;
    }

    public void A00(Map map) {
        C27661Io r4 = this.A00;
        C63963Dq r3 = r4.A02;
        if (r3 == null) {
            C16700pc.A0K("fcsLoadingEventManager");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        r3.A00(AnonymousClass4AI.A02, this.A02, this.A03, map);
        C27661Io.A00(r4, "actionPerformed");
        r4.A04("action_performed", "submit");
        AnonymousClass46T r1 = this.A01;
        C27661Io.A01(r4, ((AnonymousClass4VP) r1).A00, r1.A02(this.A04, map), 3);
    }

    public void A01(Map map) {
        C27661Io r4 = this.A00;
        C63963Dq r3 = r4.A02;
        if (r3 == null) {
            C16700pc.A0K("fcsLoadingEventManager");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        r3.A00(AnonymousClass4AI.A01, this.A02, this.A03, map);
        C27661Io.A00(r4, "actionPerformed");
        r4.A04("action_performed", "submit");
        AnonymousClass46T r1 = this.A01;
        C27661Io.A01(r4, ((AnonymousClass4VP) r1).A00, r1.A02(this.A04, map), 2);
    }
}
