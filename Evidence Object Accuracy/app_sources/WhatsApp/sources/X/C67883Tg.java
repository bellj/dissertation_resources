package X;

import android.util.JsonReader;
import android.util.JsonToken;

/* renamed from: X.3Tg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67883Tg implements AnonymousClass5X9 {
    public C67903Ti A00;
    public AnonymousClass39n A01;
    public String A02;
    public final JsonReader A03;

    public C67883Tg(JsonReader jsonReader) {
        this.A03 = jsonReader;
    }

    @Override // X.AnonymousClass5X9
    public AnonymousClass39n ALh() {
        AnonymousClass39n r0;
        this.A02 = null;
        this.A00 = null;
        JsonReader jsonReader = this.A03;
        JsonToken peek = jsonReader.peek();
        int[] iArr = C88494Fw.A00;
        switch (iArr[peek.ordinal()]) {
            case 1:
                r0 = AnonymousClass39n.A06;
                break;
            case 2:
                r0 = AnonymousClass39n.A08;
                break;
            case 3:
                r0 = AnonymousClass39n.A07;
                break;
            case 4:
                r0 = AnonymousClass39n.A09;
                break;
            case 5:
                r0 = AnonymousClass39n.A02;
                break;
            case 6:
                r0 = AnonymousClass39n.A0A;
                break;
            case 7:
                r0 = AnonymousClass39n.A04;
                break;
            case 8:
                r0 = AnonymousClass39n.A03;
                break;
            case 9:
                r0 = AnonymousClass39n.A01;
                break;
            case 10:
                r0 = AnonymousClass39n.A0B;
                break;
            default:
                throw C12960it.A0U(C12960it.A0b("unknown JsonToken ", peek));
        }
        this.A01 = r0;
        switch (iArr[jsonReader.peek().ordinal()]) {
            case 1:
                this.A02 = jsonReader.nextName();
                break;
            case 2:
            case 3:
            case 9:
            case 10:
                this.A00 = new C67903Ti(jsonReader);
                break;
            case 4:
                jsonReader.beginArray();
                break;
            case 5:
                jsonReader.endArray();
                break;
            case 6:
                jsonReader.beginObject();
                break;
            case 7:
                jsonReader.endObject();
                break;
            case 8:
                break;
            default:
                throw C12960it.A0U("unknown JsonToken ");
        }
        return this.A01;
    }

    @Override // X.AnonymousClass5X9
    public String AZ6() {
        return this.A02;
    }

    @Override // X.AnonymousClass5X9
    public AnonymousClass39n AZ7() {
        return this.A01;
    }

    @Override // X.AnonymousClass5X9
    public AnonymousClass5XA AZ8() {
        return this.A00;
    }

    @Override // X.AnonymousClass5X9
    public void Ae4() {
        AnonymousClass39n r1 = this.A01;
        AnonymousClass39n r3 = AnonymousClass39n.A09;
        if (r1 == r3 || r1 == AnonymousClass39n.A0A) {
            int i = 1;
            while (true) {
                AnonymousClass39n ALh = ALh();
                if (ALh == r3 || ALh == AnonymousClass39n.A0A) {
                    i++;
                } else if (ALh == AnonymousClass39n.A02 || ALh == AnonymousClass39n.A04) {
                    i--;
                }
                if (i == 0) {
                    return;
                }
            }
        }
    }
}
