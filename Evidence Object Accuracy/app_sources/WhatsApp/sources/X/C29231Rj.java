package X;

/* renamed from: X.1Rj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29231Rj {
    public static int A00(C14370lK r2) {
        if (r2 == C14370lK.A0I) {
            return 1;
        }
        if (C14370lK.A0B == r2 || C14370lK.A0G == r2) {
            return 0;
        }
        if (C14370lK.A05 == r2) {
            return 2;
        }
        if (C14370lK.A08 == r2) {
            return 3;
        }
        if (C14370lK.A0X == r2) {
            return 4;
        }
        if (C14370lK.A04 == r2) {
            return 5;
        }
        if (C14370lK.A0S == r2) {
            return 6;
        }
        if (C14370lK.A0J == r2) {
            return 8;
        }
        return C14370lK.A0K == r2 ? 9 : -1;
    }
}
