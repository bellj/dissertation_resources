package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import java.util.ArrayList;

/* renamed from: X.5xL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129205xL {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final AbstractC136266Lw A05;
    public final C30931Zj A06 = C117305Zk.A0V("PaymentGetTokenIdAction", "network");

    public C129205xL(Context context, C14900mE r4, C18640sm r5, C18650sn r6, C18610sj r7, AbstractC136266Lw r8) {
        this.A00 = context;
        this.A01 = r4;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
        this.A05 = r8;
    }

    public void A00(String str) {
        if (TextUtils.isEmpty(str)) {
            this.A05.AVO(null, null);
            return;
        }
        this.A06.A06("starts to fetch token id");
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "get-token-id", A0l);
        C117295Zj.A1M("credential-id", str, A0l);
        this.A04.A0F(new IDxRCallbackShape2S0100000_3_I1(this.A00, this.A01, this.A03, this, 9), C117295Zj.A0K(A0l), "get", 0);
    }
}
