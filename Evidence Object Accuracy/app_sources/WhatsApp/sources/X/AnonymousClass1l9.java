package X;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.text.FinalBackspaceAwareEntry;
import com.whatsapp.util.Log;

/* renamed from: X.1l9  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1l9 extends AnonymousClass1lA {
    public float A00;
    public int A01;
    public TextPaint A02;
    public String A03;
    public String A04;

    public AnonymousClass1l9(Context context) {
        super(context);
    }

    public AnonymousClass1l9(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass1l9(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean A05() {
        Editable A08;
        if (!(this instanceof FinalBackspaceAwareEntry)) {
            A08 = getText();
        } else {
            FinalBackspaceAwareEntry finalBackspaceAwareEntry = (FinalBackspaceAwareEntry) this;
            A08 = finalBackspaceAwareEntry.A08(finalBackspaceAwareEntry.getText());
        }
        return TextUtils.isEmpty(A08);
    }

    @Override // com.whatsapp.WaEditText, X.AnonymousClass011, android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        int i = this.A01;
        if (i != 0) {
            int i2 = editorInfo.imeOptions;
            int i3 = i2 & 255;
            if ((i3 & i) != 0) {
                int i4 = i2 ^ i3;
                editorInfo.imeOptions = i4;
                int i5 = i | i4;
                editorInfo.imeOptions = i5;
                i2 = i5;
            }
            if ((1073741824 & i2) != 0) {
                editorInfo.imeOptions = i2 & -1073741825;
            }
        }
        return onCreateInputConnection;
    }

    @Override // com.whatsapp.WaEditText, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        String str;
        int paddingLeft;
        super.onDraw(canvas);
        if (!isInEditMode() && this.A02 != null && !TextUtils.isEmpty(this.A04) && A05()) {
            AnonymousClass018 r0 = ((WaEditText) this).A03;
            if (r0 == null || (!r0.A04().A06)) {
                str = this.A04;
                paddingLeft = getPaddingLeft();
            } else {
                this.A02.setTextAlign(Paint.Align.RIGHT);
                str = this.A04;
                paddingLeft = getWidth() - getPaddingRight();
            }
            canvas.drawText(str, (float) paddingLeft, ((float) getTotalPaddingTop()) - this.A00, this.A02);
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (!TextUtils.isEmpty(this.A03) && A05()) {
            accessibilityNodeInfo.setText(this.A03);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A03 != null) {
            int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            if (this.A02 == null) {
                TextPaint textPaint = new TextPaint(1);
                this.A02 = textPaint;
                textPaint.setColor(AnonymousClass00T.A00(getContext(), R.color.hint_text));
                this.A02.setTextSize(getTextSize());
                this.A02.setTextAlign(Paint.Align.LEFT);
            }
            this.A04 = TextUtils.ellipsize(this.A03, this.A02, (float) measuredWidth, TextUtils.TruncateAt.END).toString();
            this.A00 = this.A02.getFontMetrics().top;
        }
    }

    @Override // android.view.View
    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        if (!TextUtils.isEmpty(this.A03) && A05()) {
            accessibilityEvent.getText().add(this.A03);
        }
    }

    public void setHint(String str) {
        this.A03 = str;
        this.A04 = null;
        requestLayout();
    }

    public void setInputEnterAction(int i) {
        this.A01 = i;
        setRawInputType(180225);
        if (i == 0) {
            i = 1073741824;
        }
        AnonymousClass01d r0 = ((WaEditText) this).A02;
        if (r0 != null) {
            ContentResolver A0C = r0.A0C();
            if (A0C == null) {
                Log.w("conversation-text-entry/set-input-enter-action cr=null");
            } else if ("com.htc.android.htcime/.HTCIMEService".equals(Settings.Secure.getString(A0C, "default_input_method"))) {
                i |= 268435456;
            }
        }
        setImeOptions(i);
    }
}
