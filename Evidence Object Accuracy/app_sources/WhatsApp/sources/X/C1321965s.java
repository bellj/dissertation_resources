package X;

/* renamed from: X.65s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1321965s implements AbstractC009404s {
    public final /* synthetic */ AbstractC16870pt A00;
    public final /* synthetic */ C128345vx A01;

    public C1321965s(AbstractC16870pt r1, C128345vx r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C128345vx r0 = this.A01;
        return new C123495nF(r0.A0A, r0.A0C, r0.A0Q, r0.A0V, this.A00, r0.A0a);
    }
}
