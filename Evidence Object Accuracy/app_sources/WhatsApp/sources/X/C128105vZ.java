package X;

import android.graphics.Typeface;

/* renamed from: X.5vZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128105vZ {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final Typeface A04;
    public final CharSequence A05;
    public final String A06;

    public C128105vZ(Typeface typeface, CharSequence charSequence, String str, int i, int i2, int i3, int i4) {
        this.A02 = i;
        this.A01 = i2;
        this.A06 = str;
        this.A05 = charSequence;
        this.A00 = i3;
        this.A03 = i4;
        this.A04 = typeface;
    }
}
