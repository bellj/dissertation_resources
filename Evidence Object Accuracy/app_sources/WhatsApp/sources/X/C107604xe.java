package X;

/* renamed from: X.4xe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107604xe implements AbstractC116795Wx {
    public final int A00;
    public final /* synthetic */ C14060kn A01;

    public C107604xe(C14060kn r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC116795Wx
    public boolean AJx() {
        C14060kn r2 = this.A01;
        return !r2.A0E && !r2.A07() && r2.A0L[this.A00].A06(r2.A0D);
    }

    @Override // X.AbstractC116795Wx
    public void ALR() {
        C14060kn r2 = this.A01;
        AnonymousClass5Q2 r0 = r2.A0L[this.A00].A0C;
        if (r0 != null) {
            throw ((C106604vy) r0).A00;
        }
        r2.A02();
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:94:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC116795Wx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZs(X.C89864Lr r20, X.C76763mA r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 511
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107604xe.AZs(X.4Lr, X.3mA, boolean):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r9 == -1) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0050, code lost:
        if ((r7.A03 + r9) > r7.A02) goto L_0x0052;
     */
    @Override // X.AbstractC116795Wx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int Ae2(long r14) {
        /*
            r13 = this;
            X.0kn r4 = r13.A01
            int r3 = r13.A00
            boolean r0 = r4.A0E
            if (r0 != 0) goto L_0x0068
            boolean r0 = r4.A07()
            if (r0 != 0) goto L_0x0068
            r4.A05(r3)
            X.4wc[] r0 = r4.A0L
            r7 = r0[r3]
            boolean r6 = r4.A0D
            monitor-enter(r7)
            int r5 = r7.A03     // Catch: all -> 0x0065
            int r8 = r7.A04     // Catch: all -> 0x0065
            int r8 = r8 + r5
            int r0 = r7.A01     // Catch: all -> 0x0065
            if (r8 < r0) goto L_0x0022
            int r8 = r8 - r0
        L_0x0022:
            int r9 = r7.A02     // Catch: all -> 0x0065
            if (r5 == r9) goto L_0x0045
            long[] r0 = r7.A0N     // Catch: all -> 0x0065
            r1 = r0[r8]     // Catch: all -> 0x0065
            r10 = r14
            int r0 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0045
            long r0 = r7.A06     // Catch: all -> 0x0065
            int r2 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x003a
            if (r6 == 0) goto L_0x003a
            int r9 = r9 - r5
            monitor-exit(r7)
            goto L_0x0047
        L_0x003a:
            int r9 = r9 - r5
            r12 = 1
            int r9 = r7.A00(r8, r9, r10, r12)     // Catch: all -> 0x0065
            r0 = -1
            monitor-exit(r7)
            if (r9 != r0) goto L_0x0047
            goto L_0x0046
        L_0x0045:
            monitor-exit(r7)
        L_0x0046:
            r9 = 0
        L_0x0047:
            monitor-enter(r7)
            if (r9 < 0) goto L_0x0052
            int r2 = r7.A03     // Catch: all -> 0x0062
            int r2 = r2 + r9
            int r1 = r7.A02     // Catch: all -> 0x0062
            r0 = 1
            if (r2 <= r1) goto L_0x0053
        L_0x0052:
            r0 = 0
        L_0x0053:
            X.C95314dV.A03(r0)     // Catch: all -> 0x0062
            int r0 = r7.A03     // Catch: all -> 0x0062
            int r0 = r0 + r9
            r7.A03 = r0     // Catch: all -> 0x0062
            monitor-exit(r7)
            if (r9 != 0) goto L_0x0069
            r4.A06(r3)
            return r9
        L_0x0062:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0065:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0068:
            r9 = 0
        L_0x0069:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107604xe.Ae2(long):int");
    }
}
