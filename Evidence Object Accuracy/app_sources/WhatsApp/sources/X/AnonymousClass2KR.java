package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2KR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KR extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2KR A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;

    static {
        AnonymousClass2KR r0 = new AnonymousClass2KR();
        A03 = r0;
        r0.A0W();
    }

    public AnonymousClass2KR() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A01 = r0;
        this.A02 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r6, Object obj, Object obj2) {
        switch (r6.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                AnonymousClass2KR r8 = (AnonymousClass2KR) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                AbstractC27881Jp r2 = this.A01;
                boolean z2 = true;
                if ((r8.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r7.Afm(r2, r8.A01, z, z2);
                boolean z3 = false;
                if ((this.A00 & 2) == 2) {
                    z3 = true;
                }
                AbstractC27881Jp r3 = this.A02;
                boolean z4 = false;
                if ((r8.A00 & 2) == 2) {
                    z4 = true;
                }
                this.A02 = r7.Afm(r3, r8.A02, z3, z4);
                if (r7 == C463025i.A00) {
                    this.A00 |= r8.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r72.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            this.A00 |= 1;
                            this.A01 = r72.A08();
                        } else if (A032 == 18) {
                            this.A00 |= 2;
                            this.A02 = r72.A08();
                        } else if (!A0a(r72, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2KR();
            case 5:
                return new AnonymousClass2KS();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (AnonymousClass2KR.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A01, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A02, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A01, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A02, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
