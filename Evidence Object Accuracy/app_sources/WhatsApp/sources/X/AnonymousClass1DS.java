package X;

/* renamed from: X.1DS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DS {
    public final AbstractC15710nm A00;
    public final C16590pI A01;
    public final C14820m6 A02;
    public final AnonymousClass180 A03;
    public final AnonymousClass1DR A04;
    public final C16120oU A05;

    public AnonymousClass1DS(AbstractC15710nm r1, C16590pI r2, C14820m6 r3, AnonymousClass180 r4, AnonymousClass1DR r5, C16120oU r6) {
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = r6;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
    }
}
