package X;

/* renamed from: X.1ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31631ar implements AbstractC31371aR {
    public final int A00;
    public final int A01;
    public final int A02;
    public final C31641as A03;
    public final C31491ad A04;
    public final C31591an A05;
    public final AbstractC31681aw A06;
    public final byte[] A07;

    @Override // X.AbstractC31371aR
    public int getType() {
        return 3;
    }

    public C31631ar(C31641as r7, C31491ad r8, C31591an r9, AbstractC31681aw r10, int i, int i2, int i3) {
        this.A02 = i;
        this.A00 = i2;
        this.A06 = r10;
        this.A01 = i3;
        this.A04 = r8;
        this.A03 = r7;
        this.A05 = r9;
        AnonymousClass1G4 A0T = C42031uY.A07.A0T();
        A0T.A03();
        C42031uY r1 = (C42031uY) A0T.A00;
        r1.A00 |= 4;
        r1.A03 = i3;
        byte[] A00 = r8.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C42031uY r12 = (C42031uY) A0T.A00;
        r12.A00 |= 8;
        r12.A04 = A01;
        byte[] A002 = r7.A00.A00();
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(A002, 0, A002.length);
        A0T.A03();
        C42031uY r13 = (C42031uY) A0T.A00;
        r13.A00 |= 16;
        r13.A05 = A012;
        byte[] bArr = r9.A04;
        AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C42031uY r14 = (C42031uY) A0T.A00;
        r14.A00 |= 32;
        r14.A06 = A013;
        A0T.A03();
        C42031uY r15 = (C42031uY) A0T.A00;
        r15.A00 |= 1;
        r15.A02 = i2;
        if (r10 instanceof C31691ax) {
            int intValue = ((Number) r10.A00()).intValue();
            A0T.A03();
            C42031uY r16 = (C42031uY) A0T.A00;
            r16.A00 |= 2;
            r16.A01 = intValue;
        }
        this.A07 = C31241aE.A00(new byte[]{(byte) (((i << 4) | 3) & 255)}, A0T.A02().A02());
    }

    public C31631ar(byte[] bArr) {
        AbstractC31681aw r1;
        try {
            int i = (bArr[0] & 255) >> 4;
            this.A02 = i;
            if (i > 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unknown version: ");
                sb.append(i);
                throw new C31781b6(sb.toString());
            } else if (i >= 3) {
                C42031uY r2 = (C42031uY) AbstractC27091Fz.A0A(AbstractC27881Jp.A01(bArr, 1, bArr.length - 1), C42031uY.A07);
                int i2 = r2.A00;
                if ((i2 & 4) == 4 && (i2 & 8) == 8 && (i2 & 16) == 16 && (i2 & 32) == 32) {
                    this.A07 = bArr;
                    this.A00 = r2.A02;
                    if ((i2 & 2) == 2) {
                        r1 = new C31691ax(Integer.valueOf(r2.A01));
                    } else {
                        r1 = C31671av.A00;
                    }
                    this.A06 = r1;
                    this.A01 = r2.A03;
                    this.A04 = C31481ac.A00(r2.A04.A04());
                    this.A03 = new C31641as(C31481ac.A00(r2.A05.A04()));
                    this.A05 = new C31591an(r2.A06.A04());
                    return;
                }
                throw new C31521ag("Incomplete message.");
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Legacy version: ");
                sb2.append(i);
                throw new C31571al(sb2.toString());
            }
        } catch (C28971Pt | C31561ak | C31571al e) {
            throw new C31521ag(e);
        }
    }

    @Override // X.AbstractC31371aR
    public byte[] Abf() {
        return this.A07;
    }
}
