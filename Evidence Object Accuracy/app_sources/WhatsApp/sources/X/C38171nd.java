package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import org.json.JSONObject;

/* renamed from: X.1nd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38171nd implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99854ky();
    public final long A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C38171nd(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A01 = readString;
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A02 = readString2;
        this.A00 = parcel.readLong();
    }

    public C38171nd(String str, String str2, long j) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }

    public C38171nd(JSONObject jSONObject) {
        this.A01 = jSONObject.optString("id");
        this.A02 = jSONObject.optString("message_id");
        this.A00 = jSONObject.optLong("expiry_ts");
    }

    public AnonymousClass1V8 A00() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("id", this.A01));
        arrayList.add(new AnonymousClass1W9("message_id", this.A02));
        return new AnonymousClass1V8("order", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0]));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeLong(this.A00);
    }
}
