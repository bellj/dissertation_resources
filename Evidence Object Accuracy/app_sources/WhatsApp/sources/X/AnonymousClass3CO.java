package X;

import com.whatsapp.Conversation;
import com.whatsapp.util.Log;
import org.json.JSONException;

/* renamed from: X.3CO  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3CO {
    public final /* synthetic */ AnonymousClass1OY A00;

    public /* synthetic */ AnonymousClass3CO(AnonymousClass1OY r1) {
        this.A00 = r1;
    }

    public final void A00(AnonymousClass1ZN r14) {
        AnonymousClass1OY r5 = this.A00;
        Conversation A0o = r5.A0o();
        if (A0o == null) {
            Log.e("ConversationRow/dynamicReplyOnClickCallback/error: not click in Conversation");
        } else {
            AnonymousClass1Z8 r3 = r14.A02;
            if (r3 != null) {
                try {
                    r5.A0i.A01(A0o, ((AbstractC28551Oa) r5).A0K, r5.getFMessage(), r3);
                } catch (JSONException unused) {
                    Log.e("[PAY] : ConversationRow exception processing NFM message");
                }
            } else {
                AbstractC15340mz fMessage = r5.getFMessage();
                C16170oZ r4 = ((AbstractActivityC13750kH) A0o).A03;
                AbstractC14640lm A01 = C15370n3.A01(A0o.A2c);
                C20320vZ r2 = r4.A1G;
                AnonymousClass1XD r7 = new AnonymousClass1XD(r2.A07.A02(A01, true), r14.A03, r14.A04, r4.A0R.A00());
                r2.A04(r7, fMessage);
                r4.A0M(r7);
                r4.A0f.A0S(r7);
            }
        }
        r5.A0s();
    }
}
