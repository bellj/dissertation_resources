package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.Conversation;

/* renamed from: X.3xR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83563xR extends AbstractC52172aN {
    public final /* synthetic */ Conversation A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83563xR(Context context, Conversation conversation) {
        super(context);
        this.A00 = conversation;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        Conversation conversation = this.A00;
        conversation.A3g.A00(3);
        conversation.A3M(true);
    }
}
