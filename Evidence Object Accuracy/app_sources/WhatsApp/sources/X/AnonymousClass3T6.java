package X;

import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.3T6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3T6 implements AbstractC116405Vh {
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A00;

    public AnonymousClass3T6(GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        this.A00 = groupChatLiveLocationsActivity2;
    }

    @Override // X.AbstractC116405Vh
    public void ANe() {
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = this.A00;
        groupChatLiveLocationsActivity2.A0X = false;
        AnonymousClass009.A05(groupChatLiveLocationsActivity2.A06);
    }

    @Override // X.AbstractC116405Vh
    public void AQZ() {
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = this.A00;
        groupChatLiveLocationsActivity2.A0X = false;
        C35961j4 r0 = groupChatLiveLocationsActivity2.A06;
        AnonymousClass009.A05(r0);
        r0.A04();
        AbstractView$OnCreateContextMenuListenerC35851ir r2 = groupChatLiveLocationsActivity2.A0M;
        if (r2.A0j != null) {
            r2.A0V(Float.valueOf(groupChatLiveLocationsActivity2.A06.A02().A02));
            return;
        }
        C35991j8 r02 = r2.A0l;
        if (r02 != null) {
            LatLng A00 = r02.A00();
            if (!groupChatLiveLocationsActivity2.A06.A00().A02().A04.A00(A00) && !groupChatLiveLocationsActivity2.A0M.A0t) {
                groupChatLiveLocationsActivity2.A0X = true;
                groupChatLiveLocationsActivity2.A06.A0B(C65193Io.A02(A00, Math.min(groupChatLiveLocationsActivity2.A00 * 2.0f, 16.0f)), this);
            }
        } else if (!r2.A0u && groupChatLiveLocationsActivity2.A0Y) {
            groupChatLiveLocationsActivity2.A0Y = false;
            groupChatLiveLocationsActivity2.A2i(true);
        }
    }
}
