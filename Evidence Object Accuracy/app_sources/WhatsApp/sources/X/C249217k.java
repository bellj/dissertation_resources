package X;

/* renamed from: X.17k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C249217k {
    public final C15450nH A00;
    public final C16590pI A01;
    public final C18360sK A02;
    public final AnonymousClass018 A03;
    public final C245716a A04;
    public final C14840m8 A05;
    public final AnonymousClass17R A06;
    public final C249117j A07;

    public C249217k(C15450nH r1, C16590pI r2, C18360sK r3, AnonymousClass018 r4, C245716a r5, C14840m8 r6, AnonymousClass17R r7, C249117j r8) {
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = r7;
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
        this.A02 = r3;
        this.A07 = r8;
    }
}
