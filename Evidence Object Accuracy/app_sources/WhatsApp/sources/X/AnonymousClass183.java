package X;

/* renamed from: X.183  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass183 extends AbstractC003201k {
    public final AnonymousClass182 A00;
    public final AnonymousClass180 A01;

    public AnonymousClass183(AnonymousClass182 r1, AnonymousClass180 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC003201k
    public void A00(AnonymousClass01E r3, AnonymousClass01F r4) {
        this.A01.A02(r3, "paused");
        if (r3.A0j) {
            this.A00.A00(r3, 2);
        }
    }

    @Override // X.AbstractC003201k
    public void A01(AnonymousClass01E r4, AnonymousClass01F r5) {
        String str;
        boolean z = r4.A0j;
        StringBuilder sb = new StringBuilder("resumed ");
        if (z) {
            str = "visible";
        } else {
            str = "invisible";
        }
        sb.append(str);
        this.A01.A02(r4, sb.toString());
        if (z) {
            this.A00.A00(r4, 1);
        }
    }
}
