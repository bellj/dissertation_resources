package X;

import java.util.Date;

/* renamed from: X.5GJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GJ implements AnonymousClass5WR {
    public Date A00 = null;
    public AnonymousClass4TK A01;
    public final AnonymousClass5S2 A02;

    public AnonymousClass5GJ(AnonymousClass5S2 r2) {
        this.A02 = r2;
    }

    @Override // X.AnonymousClass5WR
    public void AIv(AnonymousClass4TK r2) {
        this.A01 = r2;
        this.A00 = new Date();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:130:0x0042 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v0, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r13v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r13v3, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0178 A[Catch: 4C6 -> 0x01ee, TryCatch #4 {4C6 -> 0x01ee, blocks: (B:3:0x0002, B:4:0x002d, B:5:0x0037, B:6:0x003c, B:8:0x0040, B:9:0x0042, B:10:0x0046, B:12:0x004c, B:13:0x0056, B:14:0x005a, B:15:0x005f, B:17:0x0062, B:19:0x0068, B:21:0x006c, B:22:0x0073, B:24:0x0076, B:26:0x007e, B:27:0x0081, B:28:0x0084, B:29:0x0087, B:31:0x008d, B:33:0x0095, B:35:0x00a4, B:37:0x00aa, B:39:0x00ae, B:40:0x00b5, B:42:0x00b8, B:44:0x00bf, B:47:0x00da, B:49:0x00ef, B:50:0x00f5, B:52:0x00f8, B:54:0x00fc, B:56:0x0104, B:60:0x011e, B:61:0x0124, B:63:0x0126, B:65:0x012a, B:67:0x0132, B:68:0x0136, B:70:0x0160, B:71:0x0166, B:75:0x016c, B:76:0x016d, B:77:0x0173, B:78:0x0174, B:80:0x0178, B:83:0x0181, B:84:0x0188, B:85:0x0189, B:86:0x01c1, B:88:0x01c3, B:89:0x01d5, B:91:0x01d7, B:92:0x01dd, B:94:0x01df, B:95:0x01e5, B:97:0x01e7, B:98:0x01ed), top: B:112:0x0002, inners: #2, #3, #6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0189 A[Catch: 4C6 -> 0x01ee, TryCatch #4 {4C6 -> 0x01ee, blocks: (B:3:0x0002, B:4:0x002d, B:5:0x0037, B:6:0x003c, B:8:0x0040, B:9:0x0042, B:10:0x0046, B:12:0x004c, B:13:0x0056, B:14:0x005a, B:15:0x005f, B:17:0x0062, B:19:0x0068, B:21:0x006c, B:22:0x0073, B:24:0x0076, B:26:0x007e, B:27:0x0081, B:28:0x0084, B:29:0x0087, B:31:0x008d, B:33:0x0095, B:35:0x00a4, B:37:0x00aa, B:39:0x00ae, B:40:0x00b5, B:42:0x00b8, B:44:0x00bf, B:47:0x00da, B:49:0x00ef, B:50:0x00f5, B:52:0x00f8, B:54:0x00fc, B:56:0x0104, B:60:0x011e, B:61:0x0124, B:63:0x0126, B:65:0x012a, B:67:0x0132, B:68:0x0136, B:70:0x0160, B:71:0x0166, B:75:0x016c, B:76:0x016d, B:77:0x0173, B:78:0x0174, B:80:0x0178, B:83:0x0181, B:84:0x0188, B:85:0x0189, B:86:0x01c1, B:88:0x01c3, B:89:0x01d5, B:91:0x01d7, B:92:0x01dd, B:94:0x01df, B:95:0x01e5, B:97:0x01e7, B:98:0x01ed), top: B:112:0x0002, inners: #2, #3, #6 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass5WR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void check(java.security.cert.Certificate r31) {
        /*
        // Method dump skipped, instructions count: 511
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5GJ.check(java.security.cert.Certificate):void");
    }
}
