package X;

import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;

/* renamed from: X.47z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865847z extends C236712o {
    public final /* synthetic */ CallsHistoryFragment A00;

    public C865847z(CallsHistoryFragment callsHistoryFragment) {
        this.A00 = callsHistoryFragment;
    }

    @Override // X.C236712o
    public void A01(AnonymousClass1YT r3) {
        Log.i("voip/CallsFragment/onCallEnded");
        if (r3.A0B.A03 || r3.A00 != 2) {
            this.A00.A1C();
        }
    }

    @Override // X.C236712o
    public void A03(AnonymousClass1YT r2, boolean z) {
        Log.i("voip/CallsFragment/onCallMissed");
        this.A00.A1C();
    }
}
