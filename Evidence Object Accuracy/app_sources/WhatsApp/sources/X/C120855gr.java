package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.5gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120855gr extends C120895gv {
    public final /* synthetic */ AnonymousClass1ZR A00;
    public final /* synthetic */ AnonymousClass1ZR A01;
    public final /* synthetic */ AnonymousClass6Ln A02;
    public final /* synthetic */ C120485gG A03;
    public final /* synthetic */ boolean A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120855gr(Context context, C14900mE r8, AnonymousClass1ZR r9, AnonymousClass1ZR r10, AnonymousClass6Ln r11, C18650sn r12, C64513Fv r13, C120485gG r14, boolean z) {
        super(context, r8, r12, r13, "upi-get-vpa-name");
        this.A03 = r14;
        this.A02 = r11;
        this.A04 = z;
        this.A01 = r9;
        this.A00 = r10;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r14) {
        this.A03.A06.A05(r14, "in_upi_get_vpa_name_tag");
        AnonymousClass6Ln r0 = this.A02;
        if (r0 != null) {
            r0.AVN(null, null, null, null, r14, null, null, false, false, false, this.A04, false);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r14) {
        this.A03.A06.A05(r14, "in_upi_get_vpa_name_tag");
        AnonymousClass6Ln r0 = this.A02;
        if (r0 != null) {
            r0.AVN(null, null, null, null, r14, null, null, false, false, false, this.A04, false);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r20) {
        AnonymousClass1V8 A0c = C117305Zk.A0c(r20);
        if (A0c != null) {
            C119715ez r4 = new C119715ez();
            C120485gG r6 = this.A03;
            r4.A01(r6.A02, A0c, 7);
            if (TextUtils.isEmpty(r4.A07())) {
                r6.A04.A00(this.A01, r4.A0M());
            }
            r6.A06.A06("in_upi_get_vpa_name_tag", 2);
            AnonymousClass6Ln r62 = this.A02;
            if (r62 != null) {
                boolean A0N = r4.A0N();
                boolean A0L = r4.A0L();
                AnonymousClass1ZR A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, r4.A0I(), "accountHolderName");
                String A0H = r4.A0H();
                r62.AVN(UserJid.getNullable(r4.A07()), A0I, C117305Zk.A0I(C117305Zk.A0J(), String.class, r4.A0G(), "upiHandle"), this.A00, null, A0H, r4.A0B(), A0N, A0L, r4.A0M(), this.A04, r4.A0J());
                return;
            }
            return;
        }
        Log.e("PAY: IndiaUpiPayNonWaVpaAction verifyPaymentVpa: missing account node");
        this.A03.A06.A05(null, "in_upi_get_vpa_name_tag");
        AnonymousClass6Ln r3 = this.A02;
        if (r3 != null) {
            r3.AVN(null, null, null, null, null, null, null, false, false, false, this.A04, false);
        }
    }
}
