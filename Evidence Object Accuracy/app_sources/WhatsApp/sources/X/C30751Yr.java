package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;

/* renamed from: X.1Yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30751Yr {
    public double A00;
    public double A01;
    public float A02 = -1.0f;
    public int A03 = -1;
    public int A04 = -1;
    public long A05;
    public final UserJid A06;

    public C30751Yr(UserJid userJid) {
        this.A06 = userJid;
    }

    public void A00(C30751Yr r3) {
        AnonymousClass009.A0F(r3.A06.equals(this.A06));
        this.A05 = r3.A05;
        this.A00 = r3.A00;
        this.A01 = r3.A01;
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A02 = r3.A02;
    }

    public boolean equals(Object obj) {
        if (obj instanceof C30751Yr) {
            C30751Yr r7 = (C30751Yr) obj;
            if (r7.A06.equals(this.A06) && r7.A05 == this.A05) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A06, Long.valueOf(this.A05)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[UserLocation jid=");
        sb.append(this.A06);
        sb.append(" latitude=");
        sb.append(this.A00);
        sb.append(" longitude=");
        sb.append(this.A01);
        sb.append(" accuracy=");
        sb.append(this.A03);
        sb.append(" speed=");
        sb.append(this.A02);
        sb.append(" bearing=");
        sb.append(this.A04);
        sb.append(" timestamp=");
        sb.append(this.A05);
        sb.append("]");
        return sb.toString();
    }
}
