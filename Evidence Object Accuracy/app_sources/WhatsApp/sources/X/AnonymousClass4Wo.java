package X;

/* renamed from: X.4Wo  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Wo {
    public boolean A00;
    public final C30211Wn A01;

    public AnonymousClass4Wo(C30211Wn r1, boolean z) {
        this.A01 = r1;
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof AnonymousClass4Wo)) {
            AnonymousClass4Wo r4 = (AnonymousClass4Wo) obj;
            if (this.A00 == r4.A00) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(Boolean.valueOf(this.A00), A1a);
    }
}
