package X;

import android.widget.EditText;

/* renamed from: X.0e2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10100e2 implements Runnable {
    public final /* synthetic */ EditText A00;
    public final /* synthetic */ C14260l7 A01;
    public final /* synthetic */ AnonymousClass3C4 A02;
    public final /* synthetic */ AnonymousClass28D A03;
    public final /* synthetic */ AbstractC14200l1 A04;

    public RunnableC10100e2(EditText editText, C14260l7 r2, AnonymousClass3C4 r3, AnonymousClass28D r4, AbstractC14200l1 r5) {
        this.A02 = r3;
        this.A00 = editText;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (this.A02.A0O) {
            EditText editText = this.A00;
            AnonymousClass28D r4 = this.A03;
            AbstractC14200l1 r3 = this.A04;
            C14210l2 r2 = new C14210l2();
            r2.A05(editText.getText().toString(), 0);
            editText.setText((CharSequence) C28701Oq.A01(this.A01, r4, r2.A03(), r3));
        }
    }
}
