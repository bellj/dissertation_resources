package X;

import android.content.ContentValues;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.0vG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20130vG {
    public final C15570nT A00;
    public final C16510p9 A01;
    public final C18460sU A02;
    public final C16490p7 A03;
    public final C21390xL A04;

    public C20130vG(C15570nT r1, C16510p9 r2, C18460sU r3, C16490p7 r4, C21390xL r5) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
    }

    public void A00(AbstractC15340mz r9) {
        List<UserJid> list = r9.A0o;
        if (list != null && !list.isEmpty() && A02()) {
            C16310on A02 = this.A03.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                for (UserJid userJid : list) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(r9.A11));
                    contentValues.put("jid_row_id", Long.valueOf(this.A02.A01(userJid)));
                    A02.A03.A06(contentValues, "message_mentions", 4);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public void A01(AbstractC15340mz r9, long j) {
        List<UserJid> list = r9.A0o;
        if (list != null && !list.isEmpty()) {
            C16310on A02 = this.A03.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                for (UserJid userJid : list) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(j));
                    contentValues.put("jid_row_id", Long.valueOf(this.A02.A01(userJid)));
                    A02.A03.A06(contentValues, "message_quoted_mentions", 4);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public boolean A02() {
        if (!this.A02.A0C() || this.A04.A00("mention_message_ready", 0) != 1) {
            return false;
        }
        return true;
    }
}
