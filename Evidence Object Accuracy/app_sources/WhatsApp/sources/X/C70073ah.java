package X;

/* renamed from: X.3ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70073ah implements AnonymousClass5WL {
    public final /* synthetic */ C58582r1 A00;

    public C70073ah(C58582r1 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        C58582r1 r0 = this.A00;
        C36021jC.A00(r0.A02, r0.A0I.A03);
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        C58582r1 r2 = this.A00;
        C36021jC.A00(r2.A02, r2.A0I.A03);
        long A0L = AbstractActivityC13740kG.A0L(r2.A06, r2);
        C625937v r3 = new C625937v(r2.A0B, r2.A0S, r2.A01, A0L, false, z);
        r2.A00 = r3;
        r2.A0W.Aaz(r3, new Object[0]);
    }
}
