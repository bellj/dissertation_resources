package X;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4bJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94134bJ {
    public static final ConcurrentHashMap A07 = new ConcurrentHashMap();
    public static final String[] A08 = {"key", "value"};
    public final ContentResolver A00;
    public final ContentObserver A01;
    public final Uri A02;
    public final Object A03 = C12970iu.A0l();
    public final Object A04 = C12970iu.A0l();
    public final List A05 = C12960it.A0l();
    public volatile Map A06;

    public C94134bJ(ContentResolver contentResolver, Uri uri) {
        this.A00 = contentResolver;
        this.A02 = uri;
        this.A01 = new C72963fS(this);
    }

    public final Map A00() {
        try {
            HashMap A11 = C12970iu.A11();
            Cursor query = this.A00.query(this.A02, A08, null, null, null);
            if (query == null) {
                return A11;
            }
            while (query.moveToNext()) {
                try {
                    A11.put(query.getString(0), query.getString(1));
                } finally {
                    query.close();
                }
            }
            return A11;
        } catch (SQLiteException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            return null;
        }
    }
}
