package X;

import android.os.Bundle;

/* renamed from: X.5bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118325bd extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128375w0 A01;

    public C118325bd(Bundle bundle, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123565nM.class)) {
            C128375w0 r1 = this.A01;
            C14830m7 r2 = r1.A0A;
            C14900mE r22 = r1.A02;
            C15570nT r23 = r1.A03;
            C16590pI r24 = r1.A0B;
            AbstractC14440lR r25 = r1.A0z;
            C241414j r26 = r1.A0I;
            AnonymousClass12P r27 = r1.A01;
            AnonymousClass14X r28 = r1.A0u;
            C15550nR r29 = r1.A07;
            C15610nY r210 = r1.A08;
            AnonymousClass01d r211 = r1.A09;
            AnonymousClass018 r212 = r1.A0C;
            AnonymousClass60Y r213 = r1.A0c;
            C17070qD r214 = r1.A0V;
            C238013b r215 = r1.A06;
            C15650ng r216 = r1.A0E;
            AnonymousClass1AA r217 = r1.A05;
            C130155yt r218 = r1.A0W;
            AnonymousClass61F r15 = r1.A0d;
            AnonymousClass604 r14 = r1.A0s;
            C20300vX r13 = r1.A0F;
            C21860y6 r12 = r1.A0N;
            C22710zW r11 = r1.A0U;
            AnonymousClass102 r10 = r1.A0H;
            C14650lo r9 = r1.A04;
            C129925yW r8 = r1.A0K;
            AnonymousClass17Z r7 = r1.A0e;
            AbstractC16870pt r6 = r1.A0b;
            C20370ve r5 = r1.A0G;
            AnonymousClass1A7 r4 = r1.A0X;
            C243515e r3 = r1.A0Q;
            C130095yn r219 = r1.A0m;
            return new C123565nM(this.A00, r27, r22, r23, r9, r217, r215, r29, r210, r211, r2, r24, r212, r216, r13, r5, r10, r26, r8, r1.A0L, r12, r3, r11, r214, r218, r4, r6, r213, r15, r7, r219, r14, r28, r25);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviPayDetailsViewModel");
    }
}
