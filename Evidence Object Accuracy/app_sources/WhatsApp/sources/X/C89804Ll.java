package X;

import android.content.Context;

/* renamed from: X.4Ll  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C89804Ll {
    public final Context A00;
    public final C19370u0 A01;

    public C89804Ll(Context context, C19370u0 r3) {
        AnonymousClass009.A05(context);
        this.A00 = context.getApplicationContext();
        AnonymousClass009.A05(r3);
        this.A01 = r3;
    }
}
