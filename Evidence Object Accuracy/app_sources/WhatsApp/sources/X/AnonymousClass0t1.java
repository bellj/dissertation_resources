package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallParticipantJid;

/* renamed from: X.0t1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass0t1 {
    public final C18780t0 A00;
    public final C18770sz A01;
    public final C14850m9 A02;

    public AnonymousClass0t1(C18780t0 r1, C18770sz r2, C14850m9 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public CallParticipantJid A00(UserJid userJid, String str) {
        byte[] bArr;
        DeviceJid[] deviceJidArr = (DeviceJid[]) this.A01.A0D(userJid).toArray(new DeviceJid[0]);
        C14850m9 r1 = this.A02;
        if (r1.A07(1970)) {
            bArr = this.A00.A09(userJid);
        } else {
            bArr = null;
        }
        int length = deviceJidArr.length;
        if (length > 5 && r1.A07(1525)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" calling to primary device only because callee has too many devices");
            Log.i(sb.toString());
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                DeviceJid deviceJid = deviceJidArr[i];
                if (deviceJid.device == 0) {
                    deviceJidArr = new DeviceJid[]{deviceJid};
                    break;
                }
                i++;
            }
        }
        return new CallParticipantJid(userJid, deviceJidArr, bArr);
    }
}
