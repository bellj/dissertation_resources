package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122435lS extends AbstractC118825cR {
    public final View A00;
    public final TextView A01;
    public final TextView A02;

    public C122435lS(View view) {
        super(view);
        this.A00 = view;
        this.A01 = C12960it.A0I(view, R.id.primaryText);
        this.A02 = C12960it.A0I(view, R.id.secondaryText);
    }
}
