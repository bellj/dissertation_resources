package X;

import java.util.ArrayDeque;
import java.util.Queue;

/* renamed from: X.0k6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13660k6 {
    public Queue A00;
    public boolean A01;
    public final Object A02 = new Object();

    public final void A00(AbstractC13670k8 r3) {
        synchronized (this.A02) {
            Queue queue = this.A00;
            if (queue == null) {
                queue = new ArrayDeque();
                this.A00 = queue;
            }
            queue.add(r3);
        }
    }

    public final void A01(C13600jz r3) {
        AbstractC13670k8 r0;
        Object obj = this.A02;
        synchronized (obj) {
            if (this.A00 != null && !this.A01) {
                this.A01 = true;
                while (true) {
                    synchronized (obj) {
                        r0 = (AbstractC13670k8) this.A00.poll();
                        if (r0 == null) {
                            this.A01 = false;
                            return;
                        }
                    }
                    r0.Agv(r3);
                }
            }
        }
    }
}
