package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.whatsapp.R;

/* renamed from: X.2ub  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59382ub extends AbstractC75723kJ {
    public C59382ub(View view) {
        super(view);
    }

    @Override // X.AbstractC75723kJ
    public void A08() {
        this.A0H.clearAnimation();
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r1) {
        A0A();
    }

    public void A0A() {
        View view = this.A0H;
        Animation loadAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.glimmer);
        loadAnimation.setStartOffset((loadAnimation.getDuration() / 4) * (((long) A00()) % 4));
        view.startAnimation(loadAnimation);
    }
}
