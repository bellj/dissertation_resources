package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;

/* renamed from: X.4nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101724nz implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ UserNoticeBottomSheetDialogFragment A01;

    public ViewTreeObserver$OnGlobalLayoutListenerC101724nz(View view, UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment) {
        this.A01 = userNoticeBottomSheetDialogFragment;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C12980iv.A1E(this.A00, this);
        UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = this.A01;
        userNoticeBottomSheetDialogFragment.A07.setVisibility(4);
        userNoticeBottomSheetDialogFragment.A06.setVisibility(8);
        userNoticeBottomSheetDialogFragment.A1M();
    }
}
