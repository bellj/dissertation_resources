package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.5lz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122765lz extends AbstractC118835cS {
    public final ImageView A00;
    public final TextView A01;
    public final TextView A02;
    public final C14900mE A03;
    public final C18790t3 A04;
    public final C18810t5 A05;
    public final C244415n A06;

    public C122765lz(View view, C14900mE r4, C18790t3 r5, C18810t5 r6, C244415n r7) {
        super(view);
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        TextView A0I = C12960it.A0I(view, R.id.title);
        this.A02 = A0I;
        this.A01 = C12960it.A0I(view, R.id.subtitle);
        this.A00 = C12970iu.A0K(view, R.id.icon);
        C27531Hw.A06(A0I);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r15, int i) {
        C122925mK r152 = (C122925mK) r15;
        this.A02.setText(r152.A02);
        this.A01.setText(r152.A01);
        String str = r152.A05;
        if (str == null) {
            this.A00.setImageDrawable(r152.A00);
        } else {
            Context context = this.A0H.getContext();
            File file = new File(context.getCacheDir(), "novi_withdraw_transactions_thumbnails");
            if (!file.exists() && !file.mkdirs()) {
                Log.w(C12960it.A0d(file.getAbsolutePath(), C12960it.A0k("BizSearchActivity/getThumbnailLoader/could not create diskcache directory:")));
            }
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.small_list_avatar_size);
            Drawable drawable = context.getResources().getDrawable(R.drawable.ic_transfer_rounded);
            C38771og r8 = new C38771og(this.A03, this.A04, this.A05, file, "novi-payment-transaction-details");
            r8.A00 = dimensionPixelSize;
            r8.A01 = Math.min(4194304L, file.getFreeSpace() / 16);
            r8.A03 = drawable;
            r8.A02 = drawable;
            r8.A05 = true;
            r8.A00().A01(this.A00, str);
        }
        if (r152.A03 != null && r152.A04 != null) {
            C117295Zj.A0o(this.A0H, this, r152, 34);
        }
    }
}
