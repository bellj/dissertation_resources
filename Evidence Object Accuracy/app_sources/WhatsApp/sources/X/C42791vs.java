package X;

import androidx.fragment.app.DialogFragment;

/* renamed from: X.1vs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42791vs {
    public static void A00(DialogFragment dialogFragment, AnonymousClass01F r3) {
        String name = dialogFragment.getClass().getName();
        if (r3.A0A(name) == null) {
            dialogFragment.A1F(r3, name);
        }
    }

    public static void A01(DialogFragment dialogFragment, AnonymousClass01F r3) {
        String name = dialogFragment.getClass().getName();
        if (r3.A0A(name) == null) {
            C004902f r0 = new C004902f(r3);
            r0.A09(dialogFragment, name);
            r0.A02();
        }
    }
}
