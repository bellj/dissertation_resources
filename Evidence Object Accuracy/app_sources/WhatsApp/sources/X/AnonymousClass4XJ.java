package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4XJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XJ {
    public final AnonymousClass1MU A00;
    public final UserJid A01;
    public final String A02;
    public final String A03;

    public /* synthetic */ AnonymousClass4XJ(AnonymousClass1MU r1, UserJid userJid, String str, String str2) {
        this.A01 = userJid;
        this.A03 = str;
        this.A00 = r1;
        this.A02 = str2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass4XJ r5 = (AnonymousClass4XJ) obj;
            if (!this.A01.equals(r5.A01) || !this.A03.equals(r5.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A03, A1a);
    }
}
