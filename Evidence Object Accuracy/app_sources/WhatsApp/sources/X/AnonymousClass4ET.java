package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.4ET  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ET {
    public static final AnonymousClass5UW A00(JSONObject jSONObject) {
        for (AnonymousClass5W9 r1 : AnonymousClass4GM.A00) {
            if (jSONObject.has(r1.ADP())) {
                try {
                    return r1.A8V(jSONObject);
                } catch (JSONException unused) {
                    return null;
                }
            }
        }
        return null;
    }
}
