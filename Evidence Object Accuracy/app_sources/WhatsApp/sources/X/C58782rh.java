package X;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.2rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58782rh extends AbstractC83933y8 {
    public final /* synthetic */ C64473Fr A00;
    public final /* synthetic */ AtomicReference A01;

    public C58782rh(C64473Fr r1, AtomicReference atomicReference) {
        this.A00 = r1;
        this.A01 = atomicReference;
    }

    @Override // X.AbstractC83933y8, X.AbstractC83953yA, X.AbstractC83983yD, X.AbstractC84003yF, X.AbstractC84013yG, X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        AtomicReference atomicReference = this.A01;
        atomicReference.set(this.A00.A0X.A0A());
        if (((File) atomicReference.get()).exists()) {
            return Boolean.TRUE;
        }
        return null;
    }
}
