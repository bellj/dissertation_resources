package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54592gy extends AbstractC018308n {
    public final AnonymousClass018 A00;

    public C54592gy(AnonymousClass018 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r6, RecyclerView recyclerView) {
        int dimensionPixelSize = C12960it.A09(view).getDimensionPixelSize(R.dimen.gallery_picker_preview_spacing);
        if (C28141Kv.A01(this.A00)) {
            rect.set(0, 0, dimensionPixelSize, 0);
        } else {
            rect.set(dimensionPixelSize, 0, 0, 0);
        }
    }
}
