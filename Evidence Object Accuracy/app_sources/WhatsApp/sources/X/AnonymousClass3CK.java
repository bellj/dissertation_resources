package X;

import com.whatsapp.calling.callgrid.view.CallGrid;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3CK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CK {
    public final /* synthetic */ CallGrid A00;

    public AnonymousClass3CK(CallGrid callGrid) {
        this.A00 = callGrid;
    }

    public void A00(C64363Fg r4) {
        CallGridViewModel callGridViewModel = this.A00.A05;
        AnonymousClass009.A05(callGridViewModel);
        if (r4.A0H && !r4.A0A) {
            callGridViewModel.A08(r4.A0S);
        }
        if (!r4.A0G) {
            StringBuilder A0k = C12960it.A0k("voip/CallGridViewModel//showVoiceCallParticipantMenu ");
            UserJid userJid = r4.A0S;
            A0k.append(userJid);
            C12960it.A1F(A0k);
            callGridViewModel.A09.A0B(userJid);
        }
    }
}
