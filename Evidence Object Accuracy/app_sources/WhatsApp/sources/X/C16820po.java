package X;

/* renamed from: X.0po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16820po {
    public final String A00;

    public C16820po(String str) {
        this.A00 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C16820po)) {
            return false;
        }
        return this.A00.equals(((C16820po) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return this.A00;
    }
}
