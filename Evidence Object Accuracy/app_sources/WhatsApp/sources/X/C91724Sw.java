package X;

import java.math.BigInteger;
import java.security.cert.CRLSelector;

/* renamed from: X.4Sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91724Sw {
    public BigInteger A00 = null;
    public boolean A01 = false;
    public boolean A02 = false;
    public byte[] A03 = null;
    public final CRLSelector A04;

    public C91724Sw(CRLSelector cRLSelector) {
        this.A04 = (CRLSelector) cRLSelector.clone();
    }
}
