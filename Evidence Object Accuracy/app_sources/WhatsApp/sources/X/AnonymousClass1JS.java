package X;

/* renamed from: X.1JS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JS {
    public final AnonymousClass1PA A00;

    public AnonymousClass1JS(AnonymousClass1PA r1) {
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AnonymousClass1JS)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass1JS) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
