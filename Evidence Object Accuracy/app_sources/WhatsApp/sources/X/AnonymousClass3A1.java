package X;

/* renamed from: X.3A1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3A1 extends Exception {
    public final Throwable cause;
    public final boolean isRecoverable;
    public final C28751Ow mediaPeriodId;
    public final C100614mC rendererFormat;
    public final int rendererFormatSupport;
    public final int rendererIndex;
    public final String rendererName;
    public final long timestampMs;
    public final int type;

    public AnonymousClass3A1(C100614mC r1, C28751Ow r2, String str, String str2, Throwable th, int i, int i2, int i3, long j, boolean z) {
        super(str, th);
        this.type = i;
        this.cause = th;
        this.rendererName = str2;
        this.rendererIndex = i2;
        this.rendererFormat = r1;
        this.rendererFormatSupport = i3;
        this.mediaPeriodId = r2;
        this.timestampMs = j;
        this.isRecoverable = z;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass3A1(X.C100614mC r15, java.lang.String r16, java.lang.Throwable r17, int r18, int r19, int r20, boolean r21) {
        /*
            r14 = this;
            r4 = 0
            r9 = r19
            r8 = r18
            r10 = r20
            r3 = r15
            r6 = r16
            if (r18 == 0) goto L_0x0070
            r2 = 1
            if (r8 == r2) goto L_0x0037
            r0 = 3
            if (r8 == r0) goto L_0x0034
            java.lang.String r5 = "Unexpected runtime error"
        L_0x0014:
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 != 0) goto L_0x0027
            java.lang.StringBuilder r1 = X.C12960it.A0j(r5)
            java.lang.String r0 = ": "
            r1.append(r0)
            java.lang.String r5 = X.C12960it.A0d(r4, r1)
        L_0x0027:
            long r11 = android.os.SystemClock.elapsedRealtime()
            r2 = r14
            r13 = r21
            r7 = r17
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r13)
            return
        L_0x0034:
            java.lang.String r5 = "Remote error"
            goto L_0x0014
        L_0x0037:
            java.lang.StringBuilder r1 = X.C12960it.A0j(r6)
            java.lang.String r0 = " error, index="
            r1.append(r0)
            r1.append(r9)
            java.lang.String r0 = ", format="
            r1.append(r0)
            r1.append(r15)
            java.lang.String r0 = ", format_supported="
            r1.append(r0)
            if (r20 == 0) goto L_0x006d
            if (r10 == r2) goto L_0x006a
            r0 = 2
            if (r10 == r0) goto L_0x0067
            r0 = 3
            if (r10 == r0) goto L_0x0064
            r0 = 4
            if (r10 != r0) goto L_0x0073
            java.lang.String r0 = "YES"
        L_0x005f:
            java.lang.String r5 = X.C12960it.A0d(r0, r1)
            goto L_0x0014
        L_0x0064:
            java.lang.String r0 = "NO_EXCEEDS_CAPABILITIES"
            goto L_0x005f
        L_0x0067:
            java.lang.String r0 = "NO_UNSUPPORTED_DRM"
            goto L_0x005f
        L_0x006a:
            java.lang.String r0 = "NO_UNSUPPORTED_TYPE"
            goto L_0x005f
        L_0x006d:
            java.lang.String r0 = "NO"
            goto L_0x005f
        L_0x0070:
            java.lang.String r5 = "Source error"
            goto L_0x0014
        L_0x0073:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3A1.<init>(X.4mC, java.lang.String, java.lang.Throwable, int, int, int, boolean):void");
    }

    public AnonymousClass3A1 A00(C28751Ow r13) {
        String message = getMessage();
        Throwable th = this.cause;
        int i = this.type;
        return new AnonymousClass3A1(this.rendererFormat, r13, message, this.rendererName, th, i, this.rendererIndex, this.rendererFormatSupport, this.timestampMs, this.isRecoverable);
    }
}
