package X;

/* renamed from: X.31N  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31N extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Double A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;

    public AnonymousClass31N() {
        super(1766, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(13, this.A06);
        r3.Abe(14, this.A07);
        r3.Abe(11, this.A08);
        r3.Abe(10, this.A09);
        r3.Abe(15, this.A0A);
        r3.Abe(12, this.A0B);
        r3.Abe(16, this.A0C);
        r3.Abe(7, this.A00);
        r3.Abe(6, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(3, this.A0D);
        r3.Abe(5, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMediaDailyDataUsage {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesReceived", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesSent", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countDownloaded", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countForward", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countMessageReceived", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countMessageSent", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countShared", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countUploaded", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "countViewed", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isAutoDownload", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaTransferOrigin", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transferDate", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transferRadio", C12960it.A0Y(this.A05));
        return C12960it.A0d("}", A0k);
    }
}
