package X;

import android.net.Uri;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.io.File;

/* renamed from: X.5y9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129695y9 {
    public final C14900mE A00;
    public final C14830m7 A01;
    public final C14300lD A02;
    public final C130155yt A03;
    public final C130125yq A04;
    public final AnonymousClass61F A05;
    public final AbstractC14440lR A06;

    public C129695y9(C14900mE r1, C14830m7 r2, C14300lD r3, C130155yt r4, C130125yq r5, AnonymousClass61F r6, AbstractC14440lR r7) {
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
    }

    public final void A00(AnonymousClass1KA r21, AnonymousClass6MX r22, C14370lK r23, File file, String str, String str2, String str3, String str4) {
        byte[] bArr;
        C14480lV r11 = new C14480lV(true, false, true);
        C130125yq r6 = this.A04;
        long A00 = this.A01.A00();
        C129655y5 r1 = r6.A00;
        C127675us A01 = r1.A01("MEDIA");
        AnonymousClass009.A05(A01);
        C29361Rw A002 = C29361Rw.A00();
        C120025fU r9 = new C120025fU(AnonymousClass4F7.A00(A002.A01, A01.A02), A002.A02.A01, A00);
        AnonymousClass1K9 A003 = AnonymousClass1K9.A00(Uri.fromFile(file), r9, r21, r11, r23, null, null, 6, false, false, true, false);
        C14300lD r7 = this.A02;
        if (str4 != null) {
            bArr = AnonymousClass61L.A03(str4.getBytes());
        } else {
            bArr = null;
        }
        C1309560q r2 = r6.A01;
        C127675us A012 = r1.A01("MEDIA");
        AnonymousClass009.A05(A012);
        AnonymousClass1KC A03 = r7.A03(new AnonymousClass67Y(r2, A012, r9, bArr), A003, true);
        A03.A0U = "mms";
        A03.A0G.A03(new AbstractC14590lg() { // from class: X.6EH
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass6MX.this.AY5(C12960it.A05(obj));
            }
        }, null);
        this.A06.Ab2(new Runnable(A03, this) { // from class: X.6Hw
            public final /* synthetic */ AnonymousClass1KC A00;
            public final /* synthetic */ C129695y9 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C129695y9 r0 = this.A01;
                r0.A02.A0D(this.A00, null);
            }
        });
        A03.A03(new AbstractC14590lg(A03, r22, this, str, str4, str2, str3) { // from class: X.6Em
            public final /* synthetic */ AnonymousClass1KC A00;
            public final /* synthetic */ AnonymousClass6MX A01;
            public final /* synthetic */ C129695y9 A02;
            public final /* synthetic */ String A03;
            public final /* synthetic */ String A04;
            public final /* synthetic */ String A05;
            public final /* synthetic */ String A06;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
                this.A03 = r4;
                this.A04 = r5;
                this.A05 = r6;
                this.A06 = r7;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                String str5;
                C129695y9 r72 = this.A02;
                AnonymousClass1KC r3 = this.A00;
                AnonymousClass6MX r62 = this.A01;
                String str6 = this.A03;
                String str7 = this.A04;
                String str8 = this.A05;
                String str9 = this.A06;
                C39871qg r12 = (C39871qg) r3.A08.A00();
                if (r12 != null && !r12.A02.get()) {
                    r12.A01.delete();
                }
                C39721qR r13 = (C39721qR) r3.A0H.A00();
                if (r13 == null || r13.A00 != 0) {
                    r72.A00.A06.execute(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0088: INVOKE  
                          (wrap: java.util.concurrent.Executor : 0x0081: IGET  (r1v4 java.util.concurrent.Executor A[REMOVE]) = (wrap: X.0mE : 0x007f: IGET  (r0v2 X.0mE A[REMOVE]) = (r7v0 'r72' X.5y9) X.5y9.A00 X.0mE) X.0mE.A06 java.util.concurrent.Executor)
                          (wrap: X.6Hv : 0x0085: CONSTRUCTOR  (r0v3 X.6Hv A[REMOVE]) = (r6v0 'r62' X.6MX), (r5v0 'str6' java.lang.String) call: X.6Hv.<init>(X.6MX, java.lang.String):void type: CONSTRUCTOR)
                         type: INTERFACE call: java.util.concurrent.Executor.execute(java.lang.Runnable):void in method: X.6Em.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0085: CONSTRUCTOR  (r0v3 X.6Hv A[REMOVE]) = (r6v0 'r62' X.6MX), (r5v0 'str6' java.lang.String) call: X.6Hv.<init>(X.6MX, java.lang.String):void type: CONSTRUCTOR in method: X.6Em.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Hv, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        X.5y9 r7 = r10.A02
                        X.1KC r3 = r10.A00
                        X.6MX r6 = r10.A01
                        java.lang.String r5 = r10.A03
                        java.lang.String r2 = r10.A04
                        java.lang.String r9 = r10.A05
                        java.lang.String r8 = r10.A06
                        X.0lj r0 = r3.A08
                        java.lang.Object r1 = r0.A00()
                        X.1qg r1 = (X.C39871qg) r1
                        if (r1 == 0) goto L_0x0025
                        java.util.concurrent.atomic.AtomicBoolean r0 = r1.A02
                        boolean r0 = r0.get()
                        if (r0 != 0) goto L_0x0025
                        java.io.File r0 = r1.A01
                        r0.delete()
                    L_0x0025:
                        X.0lj r0 = r3.A0H
                        java.lang.Object r1 = r0.A00()
                        X.1qR r1 = (X.C39721qR) r1
                        if (r1 == 0) goto L_0x007f
                        int r0 = r1.A00
                        if (r0 != 0) goto L_0x007f
                        X.1Rd r1 = r1.A02
                        monitor-enter(r1)
                        java.lang.String r4 = r1.A07     // Catch: all -> 0x007c
                        monitor-exit(r1)
                        r3 = 5
                        if (r2 != 0) goto L_0x003d
                        r3 = 0
                    L_0x003d:
                        java.util.ArrayList r2 = X.C12960it.A0l()
                        java.lang.String r1 = "action"
                        java.lang.String r0 = "novi-upload-media"
                        X.AnonymousClass61S.A03(r1, r0, r2)
                        java.lang.String r0 = "media_type"
                        X.AnonymousClass61S.A03(r0, r9, r2)
                        java.lang.String r0 = "mime_type"
                        X.AnonymousClass61S.A03(r0, r5, r2)
                        java.lang.String r0 = "everstore_handle"
                        X.AnonymousClass61S.A03(r0, r4, r2)
                        java.lang.String r1 = java.lang.Integer.toString(r3)
                        java.lang.String r0 = "encryption_aad"
                        X.AnonymousClass61S.A03(r0, r1, r2)
                        if (r8 == 0) goto L_0x0067
                        java.lang.String r0 = "step_up_metadata"
                        X.AnonymousClass61S.A03(r0, r8, r2)
                    L_0x0067:
                        java.lang.String r0 = "account"
                        X.60z r4 = X.C117315Zl.A0B(r0, r2)
                        X.5yt r3 = r7.A03
                        r2 = 4
                        r0 = 1
                        com.facebook.redex.IDxAListenerShape0S1200000_3_I1 r1 = new com.facebook.redex.IDxAListenerShape0S1200000_3_I1
                        r1.<init>(r6, r7, r5, r0)
                        java.lang.String r0 = "set"
                        r3.A0B(r1, r4, r0, r2)
                        return
                    L_0x007c:
                        r0 = move-exception
                        monitor-exit(r1)
                        throw r0
                    L_0x007f:
                        X.0mE r0 = r7.A00
                        java.util.concurrent.Executor r1 = r0.A06
                        X.6Hv r0 = new X.6Hv
                        r0.<init>(r6, r5)
                        r1.execute(r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C134406Em.accept(java.lang.Object):void");
                }
            }, null);
        }

        public void A01(AnonymousClass6MX r8, File file, String str, String str2) {
            this.A05.A0B(new AbstractC136286Ly(r8, this, file, str, str2) { // from class: X.6BW
                public final /* synthetic */ AnonymousClass6MX A00;
                public final /* synthetic */ C129695y9 A01;
                public final /* synthetic */ File A02;
                public final /* synthetic */ String A03 = "image/jpeg";
                public final /* synthetic */ String A04;
                public final /* synthetic */ String A05;

                {
                    this.A01 = r3;
                    this.A02 = r4;
                    this.A04 = r5;
                    this.A05 = r6;
                    this.A00 = r2;
                }

                @Override // X.AbstractC136286Ly
                public final void AT6(C1315463e r13) {
                    String str3;
                    C129695y9 r3 = this.A01;
                    File file2 = this.A02;
                    String str4 = this.A03;
                    String str5 = this.A04;
                    String str6 = this.A05;
                    AnonymousClass6MX r5 = this.A00;
                    C14370lK r6 = C14370lK.A0M;
                    AnonymousClass1KA r4 = new AnonymousClass1KA(SearchActionVerificationClientService.NOTIFICATION_ID, 100, 1600, 1600);
                    if (r13 != null) {
                        str3 = r13.A02;
                    } else {
                        str3 = null;
                    }
                    r3.A00(r4, r5, r6, file2, str4, str5, str6, str3);
                }
            });
        }
    }
