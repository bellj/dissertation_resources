package X;

import java.io.File;
import java.util.Random;

/* renamed from: X.1DC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DC implements AbstractC16990q5 {
    public final AnonymousClass1DB A00;
    public final AnonymousClass1DA A01;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOo() {
    }

    public AnonymousClass1DC(AnonymousClass1DB r1, AnonymousClass1DA r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        if (!this.A00.A05()) {
            AnonymousClass1DA r2 = this.A01;
            C16490p7 r0 = r2.A05;
            r0.A04();
            if (r0.A01 && r2.A06.A07(1456)) {
                Random random = r2.A09;
                byte byteValue = new Byte[]{(byte) 1, (byte) 3, (byte) 2, (byte) 20, (byte) 13}[random.nextInt(5)].byteValue();
                File A0B = r2.A01.A0B(byteValue, 0, new Integer[]{2, 1, 3}[random.nextInt(3)].intValue());
                AbstractC14440lR r8 = r2.A08;
                r8.Aaz(new C864747l(r2.A00, r2.A02, r2.A03, r2.A04, r2.A07, r8, A0B, byteValue), new Void[0]);
            }
        }
    }
}
