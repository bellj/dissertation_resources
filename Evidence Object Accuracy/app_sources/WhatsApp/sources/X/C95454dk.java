package X;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.4dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95454dk {
    public static final Map A00;
    public static final Map A01;
    public static final Pattern A02 = Pattern.compile("^(\\S+)\\s+-->\\s+(\\S+)(.*)?$");
    public static final Pattern A03 = Pattern.compile("(\\S+?):(\\S+)");

    static {
        HashMap A11 = C12970iu.A11();
        A11.put("white", Integer.valueOf(Color.rgb(255, 255, 255)));
        A11.put("lime", Integer.valueOf(Color.rgb(0, 255, 0)));
        A11.put("cyan", Integer.valueOf(Color.rgb(0, 255, 255)));
        A11.put("red", Integer.valueOf(Color.rgb(255, 0, 0)));
        A11.put("yellow", Integer.valueOf(Color.rgb(255, 255, 0)));
        A11.put("magenta", Integer.valueOf(Color.rgb(255, 0, 255)));
        A11.put("blue", Integer.valueOf(Color.rgb(0, 0, 255)));
        A11.put("black", Integer.valueOf(Color.rgb(0, 0, 0)));
        A01 = Collections.unmodifiableMap(A11);
        HashMap A112 = C12970iu.A11();
        A112.put("bg_white", Integer.valueOf(Color.rgb(255, 255, 255)));
        A112.put("bg_lime", Integer.valueOf(Color.rgb(0, 255, 0)));
        A112.put("bg_cyan", Integer.valueOf(Color.rgb(0, 255, 255)));
        A112.put("bg_red", Integer.valueOf(Color.rgb(255, 0, 0)));
        A112.put("bg_yellow", Integer.valueOf(Color.rgb(255, 255, 0)));
        A112.put("bg_magenta", Integer.valueOf(Color.rgb(255, 0, 255)));
        A112.put("bg_blue", Integer.valueOf(Color.rgb(0, 0, 255)));
        A112.put("bg_black", Integer.valueOf(Color.rgb(0, 0, 0)));
        A00 = Collections.unmodifiableMap(A112);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.text.SpannedString A00(java.lang.String r11, java.lang.String r12, java.util.List r13) {
        /*
        // Method dump skipped, instructions count: 498
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95454dk.A00(java.lang.String, java.lang.String, java.util.List):android.text.SpannedString");
    }

    public static AnonymousClass4PC A01(C95304dT r5, String str, List list, Matcher matcher) {
        AnonymousClass4WA r3 = new AnonymousClass4WA();
        try {
            r3.A09 = C94624cD.A01(matcher.group(1));
            r3.A08 = C94624cD.A01(matcher.group(2));
            A04(r3, matcher.group(3));
            StringBuilder A0h = C12960it.A0h();
            while (true) {
                String A0L = r5.A0L();
                if (!TextUtils.isEmpty(A0L)) {
                    if (A0h.length() > 0) {
                        A0h.append("\n");
                    }
                    A0h.append(A0L.trim());
                } else {
                    r3.A0A = A00(str, A0h.toString(), list);
                    return new AnonymousClass4PC(r3.A00().A00(), r3.A09, r3.A08);
                }
            }
        } catch (NumberFormatException unused) {
            Log.w("WebvttCueParser", C12960it.A0d(matcher.group(), C12960it.A0k("Skipping cue with bad header: ")));
            return null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v10, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public static List A02(AnonymousClass4R5 r11, String str, List list) {
        int i;
        ArrayList A0l = C12960it.A0l();
        for (int i2 = 0; i2 < list.size(); i2++) {
            AnonymousClass4UD r2 = (AnonymousClass4UD) list.get(i2);
            String str2 = r11.A01;
            Set set = r11.A03;
            String str3 = r11.A02;
            if (!r2.A07.isEmpty() || !r2.A08.isEmpty() || !r2.A0A.isEmpty() || !r2.A09.isEmpty()) {
                String str4 = r2.A07;
                int i3 = 0;
                if (!str4.isEmpty()) {
                    i3 = -1;
                    if (str4.equals(str)) {
                        i3 = 1073741824;
                    }
                }
                String str5 = r2.A08;
                if (!str5.isEmpty()) {
                    int i4 = -1;
                    if (i3 != -1) {
                        if (str5.equals(str2)) {
                            i4 = i3 + 2;
                        }
                        i3 = i4;
                    }
                }
                String str6 = r2.A09;
                if (!str6.isEmpty()) {
                    if (i3 != -1 && str6.equals(str3)) {
                        i3 += 4;
                    }
                }
                if (i3 != -1 && set.containsAll(r2.A0A)) {
                    i = i3 + (r2.A0A.size() << 2);
                }
            } else {
                i = TextUtils.isEmpty(str2);
            }
            if (i > 0) {
                A0l.add(new AnonymousClass5BZ(r2, i == 1 ? 1 : 0));
            }
        }
        Collections.sort(A0l);
        return A0l;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cd, code lost:
        if (r0 != -1) goto L_0x00cf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0156 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x007b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(android.text.SpannableStringBuilder r14, X.AnonymousClass4R5 r15, java.lang.String r16, java.util.List r17, java.util.List r18) {
        /*
        // Method dump skipped, instructions count: 524
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95454dk.A03(android.text.SpannableStringBuilder, X.4R5, java.lang.String, java.util.List, java.util.List):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0137  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.AnonymousClass4WA r7, java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 514
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95454dk.A04(X.4WA, java.lang.String):void");
    }
}
