package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.53s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1099653s implements AbstractC116495Vr {
    public final /* synthetic */ C90184Mx A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ C17480qs A02;
    public final /* synthetic */ String A03;

    public C1099653s(C90184Mx r1, UserJid userJid, C17480qs r3, String str) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = userJid;
        this.A03 = str;
    }

    @Override // X.AbstractC116495Vr
    public void AU2(String str) {
        C16700pc.A0E(str, 0);
        C17480qs.A00(null, this.A00, this.A01, this.A02, str, this.A03);
    }

    @Override // X.AbstractC116495Vr
    public void AU3(C90134Ms r7) {
        String str = r7.A01;
        C16700pc.A0B(str);
        C17480qs.A00(r7, this.A00, this.A01, this.A02, str, this.A03);
    }
}
