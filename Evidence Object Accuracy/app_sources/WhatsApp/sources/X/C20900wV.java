package X;

/* renamed from: X.0wV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20900wV {
    public final C16490p7 A00;
    public final C21390xL A01;

    public C20900wV(C16490p7 r1, C21390xL r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static final void A00(AbstractC15340mz r8) {
        boolean z = false;
        boolean z2 = false;
        if (r8.A11 > 0) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("SendCountMessageStore/validateMessage/message must have row_id set; key=");
        AnonymousClass1IS r2 = r8.A0z;
        sb.append(r2);
        AnonymousClass009.A0B(sb.toString(), z2);
        if (r8.A08() == 1) {
            z = true;
        }
        StringBuilder sb2 = new StringBuilder("SendCountMessageStore/validateMessage/message in main storage; key=");
        sb2.append(r2);
        AnonymousClass009.A0B(sb2.toString(), z);
    }
}
