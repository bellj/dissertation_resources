package X;

import android.content.Context;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

/* renamed from: X.23N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23N {
    public static void A00(Context context, AnonymousClass01d r4, CharSequence charSequence) {
        AccessibilityManager A0P = r4.A0P();
        if (A0P != null && A0P.isEnabled()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(16384);
            obtain.setClassName("android.widget.Button");
            obtain.setPackageName(context.getPackageName());
            obtain.getText().add(charSequence);
            A0P.sendAccessibilityEvent(obtain);
        }
    }

    public static void A01(View view) {
        AnonymousClass028.A0g(view, new AnonymousClass2VZ(view));
    }

    public static void A02(View view, int i) {
        AnonymousClass028.A0g(view, new AnonymousClass2VY(new AnonymousClass2VX[]{new AnonymousClass2VX(16, i)}));
    }

    public static void A03(View view, int i) {
        AnonymousClass028.A0g(view, new AnonymousClass2VY(new AnonymousClass2VX[]{new AnonymousClass2VX(1, i)}));
    }

    public static void A04(View view, boolean z) {
        AnonymousClass028.A0g(view, new AnonymousClass2VW(z));
    }

    public static boolean A05(AccessibilityManager accessibilityManager) {
        return accessibilityManager != null && accessibilityManager.isTouchExplorationEnabled();
    }
}
