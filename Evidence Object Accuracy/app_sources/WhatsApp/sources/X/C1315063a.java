package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315063a implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(25);
    public final int A00;
    public final C1316563p A01;
    public final AbstractC128515wE A02;
    public final C1316363n A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315063a(C1316563p r1, AbstractC128515wE r2, C1316363n r3, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        AbstractC128515wE r2 = this.A02;
        int i2 = r2.A00;
        parcel.writeInt(i2);
        if (i2 == 0) {
            C120935gz r22 = (C120935gz) r2;
            parcel.writeString(r22.A01);
            parcel.writeString(r22.A00);
        } else if (i2 == 1) {
            C120945h0 r23 = (C120945h0) r2;
            parcel.writeString(r23.A02.toString());
            parcel.writeString(r23.A01.toString());
            parcel.writeString(r23.A03);
            parcel.writeInt(r23.A00);
        }
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeInt(this.A00);
    }
}
