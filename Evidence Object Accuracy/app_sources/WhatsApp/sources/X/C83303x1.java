package X;

import android.view.animation.Animation;
import com.whatsapp.phonematching.CountryPicker;

/* renamed from: X.3x1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83303x1 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ CountryPicker A00;

    public C83303x1(CountryPicker countryPicker) {
        this.A00 = countryPicker;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A01.setIconified(false);
    }
}
