package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0VK implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        if (parcel.readParcelable(null) == null) {
            return AbstractC015707l.A01;
        }
        throw new IllegalStateException("superState must be null");
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        if (parcel.readParcelable(classLoader) == null) {
            return AbstractC015707l.A01;
        }
        throw new IllegalStateException("superState must be null");
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AbstractC015707l[i];
    }
}
