package X;

import java.util.Iterator;
import java.util.LinkedHashMap;
import org.json.JSONObject;

/* renamed from: X.3dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72043dv extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ C16660pY this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72043dv(C16660pY r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Object obj;
        C14850m9 r1 = this.this$0.A00;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        String A03 = r1.A03(1320);
        if (A03 != null) {
            try {
                JSONObject A05 = C13000ix.A05(A03);
                Iterator<String> keys = A05.keys();
                C16700pc.A0B(keys);
                AnonymousClass1WR r4 = new AnonymousClass1WR(new AnonymousClass1WQ(new AnonymousClass5KG(A05), AnonymousClass1WM.A06(keys), true));
                while (r4.hasNext()) {
                    String A0x = C12970iu.A0x(r4);
                    JSONObject optJSONObject = A05.optJSONObject(A0x);
                    C16700pc.A0C(optJSONObject);
                    String optString = optJSONObject.optString("app_id");
                    boolean optBoolean = optJSONObject.optBoolean("enabled", true);
                    long optLong = optJSONObject.optLong("expiration_secs", 0);
                    String optString2 = optJSONObject.optString("version", "");
                    C16700pc.A0B(A0x);
                    linkedHashMap.put(A0x, new C91284Rd(optString, optString2, optLong, optBoolean));
                }
                obj = AnonymousClass1WZ.A00;
            } catch (Throwable th) {
                obj = new AnonymousClass5BR(th);
            }
            Throwable A00 = AnonymousClass5BU.A00(obj);
            if (A00 != null) {
                A00.getMessage();
            }
        }
        return linkedHashMap;
    }
}
