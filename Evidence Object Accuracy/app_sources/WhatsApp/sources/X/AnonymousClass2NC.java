package X;

import java.util.Locale;

/* renamed from: X.2NC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NC extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String[] A02;
    public final /* synthetic */ Locale[] A03;

    public AnonymousClass2NC(AnonymousClass2L8 r1, String str, String[] strArr, Locale[] localeArr) {
        this.A00 = r1;
        this.A01 = str;
        this.A03 = localeArr;
        this.A02 = strArr;
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [java.util.Locale[], java.io.Serializable] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC35941j2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(int r8) {
        /*
            r7 = this;
            X.2L8 r0 = r7.A00
            X.20b r2 = r0.A0H
            java.util.Locale[] r6 = r7.A03
            java.lang.String[] r5 = r7.A02
            java.lang.String r4 = r7.A01
            java.lang.String r1 = "xmpp/reader/on-get-biz-language-pack-error code="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            X.20c r3 = r2.A00
            r2 = 0
            r1 = 0
            r0 = 108(0x6c, float:1.51E-43)
            android.os.Message r2 = android.os.Message.obtain(r2, r1, r0, r1)
            android.os.Bundle r1 = r2.getData()
            java.lang.String r0 = "requestLocales"
            r1.putSerializable(r0, r6)
            java.lang.String r0 = "haveHashes"
            if (r5 == 0) goto L_0x0044
            r1.putStringArray(r0, r5)
        L_0x0036:
            java.lang.String r0 = "ns"
            r1.putString(r0, r4)
            java.lang.String r0 = "errorCode"
            r1.putInt(r0, r8)
            r3.AYY(r2)
            return
        L_0x0044:
            r1.remove(r0)
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2NC.A00(int):void");
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [java.util.Locale[], java.io.Serializable] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC35941j2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.AnonymousClass1V8 r12) {
        /*
            r11 = this;
            java.lang.String r0 = "languagepack"
            X.1V8 r2 = r12.A0E(r0)
            X.AnonymousClass009.A05(r2)
            java.lang.String r0 = "lg"
            java.lang.String r1 = ""
            java.lang.String r3 = r2.A0I(r0, r1)
            java.lang.String r0 = "lc"
            java.lang.String r1 = r2.A0I(r0, r1)
            java.lang.String r9 = "hash"
            r0 = 0
            java.lang.String r8 = r2.A0I(r9, r0)
            java.lang.String r0 = r11.A01
            java.lang.String r7 = "ns"
            java.lang.String r6 = r2.A0I(r7, r0)
            byte[] r5 = r2.A01
            X.2L8 r0 = r11.A00
            X.20b r2 = r0.A0H
            java.util.Locale[] r10 = r11.A03
            java.util.Locale r4 = new java.util.Locale
            r4.<init>(r3, r1)
            java.lang.String r0 = "xmpp/reader/on-get-biz-language-pack requested="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = X.AbstractC27291Gt.A08(r10)
            r1.append(r0)
            java.lang.String r0 = " locale="
            r1.append(r0)
            java.lang.String r0 = X.AbstractC27291Gt.A05(r4)
            r1.append(r0)
            java.lang.String r0 = " hash="
            r1.append(r0)
            r1.append(r8)
            java.lang.String r0 = " ns="
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            X.20c r3 = r2.A00
            r2 = 0
            r1 = 0
            r0 = 107(0x6b, float:1.5E-43)
            android.os.Message r2 = android.os.Message.obtain(r2, r1, r0, r1)
            android.os.Bundle r1 = r2.getData()
            java.lang.String r0 = "requestLocales"
            r1.putSerializable(r0, r10)
            java.lang.String r0 = "locale"
            r1.putSerializable(r0, r4)
            r1.putString(r9, r8)
            r1.putString(r7, r6)
            java.lang.String r0 = "blob"
            r1.putByteArray(r0, r5)
            r3.AYY(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2NC.A02(X.1V8):void");
    }
}
