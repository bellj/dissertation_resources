package X;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5Ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113485Ht extends X509Certificate implements AnonymousClass5S0 {
    public C114585Mh basicConstraints;
    public AnonymousClass5S2 bcHelper;
    public C114565Mf c;
    public boolean[] keyUsage;
    public String sigAlgName;
    public byte[] sigAlgParams;

    public AbstractC113485Ht(String str, C114585Mh r2, C114565Mf r3, AnonymousClass5S2 r4, byte[] bArr, boolean[] zArr) {
        this.bcHelper = r4;
        this.c = r3;
        this.basicConstraints = r2;
        this.keyUsage = zArr;
        this.sigAlgName = str;
        this.sigAlgParams = bArr;
    }

    @Override // java.security.cert.X509Certificate
    public void checkValidity() {
        checkValidity(new Date());
    }

    @Override // java.security.cert.Certificate
    public byte[] getEncoded() {
        try {
            return this.c.A02("DER");
        } catch (IOException e) {
            throw new CertificateEncodingException(e.toString());
        }
    }

    @Override // java.security.cert.X509Certificate
    public String getSigAlgName() {
        return this.sigAlgName;
    }

    @Override // java.security.cert.X509Certificate
    public byte[] getSigAlgParams() {
        return AnonymousClass1TT.A02(this.sigAlgParams);
    }

    @Override // java.security.cert.Certificate
    public final void verify(PublicKey publicKey) {
        A02(publicKey, new AnonymousClass5GM(this));
    }

    @Override // java.security.cert.Certificate
    public final void verify(PublicKey publicKey, String str) {
        A02(publicKey, new AnonymousClass5GP(str, this));
    }

    public static Collection A00(String str, C114565Mf r6) {
        String obj;
        byte[] A03 = A03(str, r6);
        if (A03 != null) {
            try {
                ArrayList A0l = C12960it.A0l();
                Enumeration A0C = AbstractC114775Na.A04(A03).A0C();
                while (A0C.hasMoreElements()) {
                    AnonymousClass5N1 A01 = AnonymousClass5N1.A01(A0C.nextElement());
                    ArrayList A0l2 = C12960it.A0l();
                    int i = A01.A00;
                    C12980iv.A1R(A0l2, i);
                    switch (i) {
                        case 0:
                        case 3:
                        case 5:
                            A0l2.add(A01.A01());
                            break;
                        case 1:
                        case 2:
                        case 6:
                            obj = ((AnonymousClass5VP) A01.A01).AGy();
                            A0l2.add(obj);
                            break;
                        case 4:
                            obj = AnonymousClass5N2.A01(A01.A01, C114905Nn.A0l).toString();
                            A0l2.add(obj);
                            break;
                        case 7:
                            try {
                                obj = InetAddress.getByAddress(AnonymousClass5NH.A05(A01.A01)).getHostAddress();
                                A0l2.add(obj);
                                break;
                            } catch (UnknownHostException unused) {
                                break;
                            }
                        case 8:
                            obj = AnonymousClass1TK.A00(A01.A01).A01;
                            A0l2.add(obj);
                            break;
                        default:
                            throw C12990iw.A0i(C12960it.A0e("Bad tag number: ", C12960it.A0h(), i));
                    }
                    A0l.add(Collections.unmodifiableList(A0l2));
                }
                if (A0l.size() != 0) {
                    return Collections.unmodifiableCollection(A0l);
                }
            } catch (Exception e) {
                throw new CertificateParsingException(e.getMessage());
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r1.equals(X.AnonymousClass5ME.A00) == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r2.A00 == null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (r0 != false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01(java.security.PublicKey r5, java.security.Signature r6, X.AnonymousClass1TN r7, byte[] r8) {
        /*
            r4 = this;
            X.5Mf r0 = r4.c
            X.5Mv r3 = r0.A02
            X.5Mg r0 = r0.A03
            X.5Mv r2 = r0.A07
            X.1TK r1 = r3.A01
            X.1TK r0 = r2.A01
            boolean r0 = r1.A04(r0)
            if (r0 == 0) goto L_0x002a
            java.lang.String r0 = "org.spongycastle.x509.allow_absent_equiv_NULL"
            boolean r0 = X.C94664cJ.A01(r0)
            if (r0 == 0) goto L_0x0037
            X.1TN r1 = r3.A00
            if (r1 != 0) goto L_0x0032
            X.1TN r1 = r2.A00
            if (r1 == 0) goto L_0x0043
        L_0x0022:
            X.5ME r0 = X.AnonymousClass5ME.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0043
        L_0x002a:
            java.lang.String r1 = "signature algorithm in TBS cert not same as outer cert"
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            r0.<init>(r1)
            throw r0
        L_0x0032:
            X.1TN r0 = r2.A00
            if (r0 != 0) goto L_0x0037
            goto L_0x0022
        L_0x0037:
            X.1TN r1 = r3.A00
            X.1TN r0 = r2.A00
            if (r1 == 0) goto L_0x004a
            boolean r0 = r1.equals(r0)
        L_0x0041:
            if (r0 == 0) goto L_0x002a
        L_0x0043:
            X.C95444dj.A03(r6, r7)
            r6.initVerify(r5)
            goto L_0x0051
        L_0x004a:
            if (r0 == 0) goto L_0x0043
            boolean r0 = r0.equals(r1)
            goto L_0x0041
        L_0x0051:
            X.49G r1 = new X.49G     // Catch: IOException -> 0x0078
            r1.<init>(r6)     // Catch: IOException -> 0x0078
            r0 = 512(0x200, float:7.175E-43)
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch: IOException -> 0x0078
            r2.<init>(r1, r0)     // Catch: IOException -> 0x0078
            X.5Mf r0 = r4.c     // Catch: IOException -> 0x0078
            X.5Mg r1 = r0.A03     // Catch: IOException -> 0x0078
            java.lang.String r0 = "DER"
            r1.A00(r2, r0)     // Catch: IOException -> 0x0078
            r2.close()     // Catch: IOException -> 0x0078
            boolean r0 = r6.verify(r8)
            if (r0 == 0) goto L_0x0070
            return
        L_0x0070:
            java.lang.String r1 = "certificate does not verify with supplied key"
            java.security.SignatureException r0 = new java.security.SignatureException
            r0.<init>(r1)
            throw r0
        L_0x0078:
            r0 = move-exception
            java.lang.String r1 = r0.toString()
            java.security.cert.CertificateEncodingException r0 = new java.security.cert.CertificateEncodingException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC113485Ht.A01(java.security.PublicKey, java.security.Signature, X.1TN, byte[]):void");
    }

    private void A02(PublicKey publicKey, AnonymousClass5VR r10) {
        boolean A04 = AbstractC116995Xs.A0C.A04(this.c.A02.A01);
        C114725Mv r0 = this.c.A02;
        if (A04) {
            AbstractC114775Na A042 = AbstractC114775Na.A04(r0.A00);
            AbstractC114775Na A043 = AbstractC114775Na.A04(AnonymousClass5MA.A00(this.c.A01).A0B());
            boolean z = false;
            for (int i = 0; i != A043.A0B(); i++) {
                C114725Mv A00 = C114725Mv.A00(A042.A0D(i));
                try {
                    A01(publicKey, r10.A8Z(C95444dj.A01(A00)), A00.A00, AnonymousClass5MA.A00(A043.A0D(i)).A0B());
                    z = true;
                } catch (InvalidKeyException | NoSuchAlgorithmException unused) {
                } catch (SignatureException e) {
                    throw e;
                }
            }
            if (!z) {
                throw new InvalidKeyException("no matching key found");
            }
            return;
        }
        A01(publicKey, r10.A8Z(C95444dj.A01(r0)), this.c.A02.A00, getSignature());
    }

    public static byte[] A03(String str, C114565Mf r3) {
        C114715Mu A00;
        AnonymousClass5NH r0;
        AnonymousClass5MX r1 = r3.A03.A08;
        if (r1 == null || (A00 = AnonymousClass5MX.A00(C72453ed.A12(str), r1)) == null || (r0 = A00.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // java.security.cert.X509Certificate
    public void checkValidity(Date date) {
        if (date.getTime() > getNotAfter().getTime()) {
            throw new CertificateExpiredException(C12960it.A0d(this.c.A03.A0A.A03(), C12960it.A0k("certificate expired on ")));
        } else if (date.getTime() < getNotBefore().getTime()) {
            throw new CertificateNotYetValidException(C12960it.A0d(this.c.A03.A0B.A03(), C12960it.A0k("certificate not valid till ")));
        }
    }

    @Override // java.security.cert.X509Certificate
    public int getBasicConstraints() {
        AnonymousClass5NF r0;
        BigInteger bigInteger;
        C114585Mh r2 = this.basicConstraints;
        if (r2 == null || (r0 = r2.A00) == null || r0.A00 == 0) {
            return -1;
        }
        AnonymousClass5NG r02 = r2.A01;
        if (r02 == null) {
            return Integer.MAX_VALUE;
        }
        new BigInteger(r02.A01);
        AnonymousClass5NG r03 = this.basicConstraints.A01;
        if (r03 != null) {
            bigInteger = new BigInteger(r03.A01);
        } else {
            bigInteger = null;
        }
        return bigInteger.intValue();
    }

    @Override // java.security.cert.X509Extension
    public Set getCriticalExtensionOIDs() {
        if (getVersion() == 3) {
            HashSet A12 = C12970iu.A12();
            AnonymousClass5MX r3 = this.c.A03.A08;
            if (r3 != null) {
                Enumeration elements = r3.A01.elements();
                while (elements.hasMoreElements()) {
                    AnonymousClass1TK r1 = (AnonymousClass1TK) elements.nextElement();
                    if (AnonymousClass5MX.A00(r1, r3).A02) {
                        A12.add(r1.A01);
                    }
                }
                return A12;
            }
        }
        return null;
    }

    @Override // java.security.cert.X509Certificate
    public List getExtendedKeyUsage() {
        byte[] A03 = A03("2.5.29.37", this.c);
        if (A03 == null) {
            return null;
        }
        try {
            AbstractC114775Na A04 = AbstractC114775Na.A04((Object) AnonymousClass1TL.A03(A03));
            ArrayList A0l = C12960it.A0l();
            for (int i = 0; i != A04.A0B(); i++) {
                A0l.add(((AnonymousClass1TK) A04.A0D(i)).A01);
            }
            return Collections.unmodifiableList(A0l);
        } catch (Exception unused) {
            throw new CertificateParsingException("error processing extended key usage extension");
        }
    }

    @Override // java.security.cert.X509Extension
    public byte[] getExtensionValue(String str) {
        AnonymousClass5NH r0;
        C114715Mu A00;
        AnonymousClass5MX r1 = this.c.A03.A08;
        if (r1 == null || (A00 = AnonymousClass5MX.A00(C72453ed.A12(str), r1)) == null) {
            r0 = null;
        } else {
            r0 = A00.A01;
        }
        if (r0 == null) {
            return null;
        }
        try {
            return r0.A01();
        } catch (Exception e) {
            throw C12960it.A0U(C12960it.A0d(e.toString(), C12960it.A0k("error parsing ")));
        }
    }

    @Override // java.security.cert.X509Certificate
    public Collection getIssuerAlternativeNames() {
        return A00(C114715Mu.A0J.A01, this.c);
    }

    @Override // java.security.cert.X509Certificate
    public Principal getIssuerDN() {
        return new C114925Np(this.c.A03.A05);
    }

    @Override // java.security.cert.X509Certificate
    public boolean[] getIssuerUniqueID() {
        AnonymousClass5MA r1 = this.c.A03.A03;
        if (r1 == null) {
            return null;
        }
        byte[] A0B = r1.A0B();
        int length = (A0B.length << 3) - r1.A00;
        boolean[] zArr = new boolean[length];
        for (int i = 0; i != length; i++) {
            zArr[i] = C12960it.A1S(A0B[i >> 3] & (128 >>> (i % 8)));
        }
        return zArr;
    }

    @Override // java.security.cert.X509Certificate
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.A03.A05.A02("DER"));
        } catch (IOException unused) {
            throw C12960it.A0U("can't encode issuer DN");
        }
    }

    @Override // java.security.cert.X509Certificate
    public boolean[] getKeyUsage() {
        boolean[] zArr = this.keyUsage;
        if (zArr == null) {
            return null;
        }
        return (boolean[]) zArr.clone();
    }

    @Override // java.security.cert.X509Extension
    public Set getNonCriticalExtensionOIDs() {
        if (getVersion() == 3) {
            HashSet A12 = C12970iu.A12();
            AnonymousClass5MX r3 = this.c.A03.A08;
            if (r3 != null) {
                Enumeration elements = r3.A01.elements();
                while (elements.hasMoreElements()) {
                    AnonymousClass1TK r1 = (AnonymousClass1TK) elements.nextElement();
                    if (!AnonymousClass5MX.A00(r1, r3).A02) {
                        A12.add(r1.A01);
                    }
                }
                return A12;
            }
        }
        return null;
    }

    @Override // java.security.cert.X509Certificate
    public Date getNotAfter() {
        return this.c.A03.A0A.A04();
    }

    @Override // java.security.cert.X509Certificate
    public Date getNotBefore() {
        return this.c.A03.A0B.A04();
    }

    @Override // java.security.cert.Certificate
    public PublicKey getPublicKey() {
        try {
            AnonymousClass1TK r0 = this.c.A03.A09.A01.A01;
            Map map = C27511Hu.A00;
            synchronized (map) {
                map.get(r0);
            }
            return null;
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.cert.X509Certificate
    public BigInteger getSerialNumber() {
        return new BigInteger(this.c.A03.A00.A01);
    }

    @Override // java.security.cert.X509Certificate
    public String getSigAlgOID() {
        return this.c.A02.A01.A01;
    }

    @Override // java.security.cert.X509Certificate
    public byte[] getSignature() {
        AnonymousClass5MA r1 = this.c.A01;
        if (r1.A00 == 0) {
            return AnonymousClass1TT.A02(r1.A01);
        }
        throw C12960it.A0U("attempt to get non-octet aligned data from BIT STRING");
    }

    @Override // java.security.cert.X509Certificate
    public Collection getSubjectAlternativeNames() {
        return A00(C114715Mu.A0U.A01, this.c);
    }

    @Override // java.security.cert.X509Certificate
    public Principal getSubjectDN() {
        return new C114925Np(this.c.A03.A06);
    }

    @Override // java.security.cert.X509Certificate
    public boolean[] getSubjectUniqueID() {
        AnonymousClass5MA r1 = this.c.A03.A04;
        if (r1 == null) {
            return null;
        }
        byte[] A0B = r1.A0B();
        int length = (A0B.length << 3) - r1.A00;
        boolean[] zArr = new boolean[length];
        for (int i = 0; i != length; i++) {
            zArr[i] = C12960it.A1S(A0B[i >> 3] & (128 >>> (i % 8)));
        }
        return zArr;
    }

    @Override // java.security.cert.X509Certificate
    public X500Principal getSubjectX500Principal() {
        try {
            return new X500Principal(this.c.A03.A06.A02("DER"));
        } catch (IOException unused) {
            throw C12960it.A0U("can't encode subject DN");
        }
    }

    @Override // java.security.cert.X509Certificate
    public byte[] getTBSCertificate() {
        try {
            return this.c.A03.A02("DER");
        } catch (IOException e) {
            throw new CertificateEncodingException(e.toString());
        }
    }

    @Override // java.security.cert.X509Certificate
    public int getVersion() {
        return this.c.A03.A01.A0B() + 1;
    }

    @Override // java.security.cert.X509Extension
    public boolean hasUnsupportedCriticalExtension() {
        AnonymousClass5MX r1;
        if (getVersion() != 3 || (r1 = this.c.A03.A08) == null) {
            return false;
        }
        Enumeration elements = r1.A01.elements();
        while (elements.hasMoreElements()) {
            AnonymousClass1TL r2 = (AnonymousClass1TL) elements.nextElement();
            if (!r2.A04(C114715Mu.A0L) && !r2.A04(C114715Mu.A0B) && !r2.A04(C114715Mu.A0Q) && !r2.A04(C114715Mu.A0G) && !r2.A04(C114715Mu.A08) && !r2.A04(C114715Mu.A0K) && !r2.A04(C114715Mu.A0C) && !r2.A04(C114715Mu.A0P) && !r2.A04(C114715Mu.A06) && !r2.A04(C114715Mu.A0U) && !r2.A04(C114715Mu.A0N) && AnonymousClass5MX.A00(r2, r1).A02) {
                return true;
            }
        }
        return false;
    }

    @Override // java.security.cert.Certificate, java.lang.Object
    public String toString() {
        Object r6;
        StringBuffer stringBuffer = new StringBuffer();
        String str = AnonymousClass1T7.A00;
        stringBuffer.append("  [0]         Version: ");
        stringBuffer.append(getVersion());
        stringBuffer.append(str);
        stringBuffer.append("         SerialNumber: ");
        stringBuffer.append(getSerialNumber());
        stringBuffer.append(str);
        stringBuffer.append("             IssuerDN: ");
        stringBuffer.append(getIssuerDN());
        stringBuffer.append(str);
        stringBuffer.append("           Start Date: ");
        stringBuffer.append(getNotBefore());
        stringBuffer.append(str);
        stringBuffer.append("           Final Date: ");
        stringBuffer.append(getNotAfter());
        stringBuffer.append(str);
        stringBuffer.append("            SubjectDN: ");
        stringBuffer.append(getSubjectDN());
        stringBuffer.append(str);
        stringBuffer.append("           Public Key: ");
        stringBuffer.append(getPublicKey());
        stringBuffer.append(str);
        stringBuffer.append("  Signature Algorithm: ");
        stringBuffer.append(this.sigAlgName);
        stringBuffer.append(str);
        C95444dj.A02(str, stringBuffer, getSignature());
        AnonymousClass5MX r1 = this.c.A03.A08;
        if (r1 != null) {
            Enumeration elements = r1.A01.elements();
            if (elements.hasMoreElements()) {
                stringBuffer.append("       Extensions: \n");
            }
            while (elements.hasMoreElements()) {
                AnonymousClass1TK r5 = (AnonymousClass1TK) elements.nextElement();
                C114715Mu A00 = AnonymousClass5MX.A00(r5, r1);
                AnonymousClass5NH r0 = A00.A01;
                if (r0 != null) {
                    AnonymousClass1TO A002 = AnonymousClass5NH.A00(stringBuffer, r0, A00);
                    try {
                    } catch (Exception unused) {
                        stringBuffer.append(r5.A01);
                        stringBuffer.append(" value = ");
                        stringBuffer.append("*****");
                    }
                    if (r5.A04(C114715Mu.A06)) {
                        r6 = C114585Mh.A00(A002.A05());
                    } else if (r5.A04(C114715Mu.A0L)) {
                        AnonymousClass1TL A05 = A002.A05();
                        r6 = A05 != null ? new AnonymousClass5MQ(AnonymousClass5MA.A00(A05)) : null;
                    } else if (r5.A04(AbstractC116995Xs.A0R)) {
                        r6 = new C114855Ni(AnonymousClass5MA.A00(A002.A05()));
                    } else if (r5.A04(AbstractC116995Xs.A0T)) {
                        r6 = new C114865Nj(AnonymousClass5NT.A00(A002.A05()));
                    } else if (r5.A04(AbstractC116995Xs.A0Z)) {
                        r6 = new C114875Nk(AnonymousClass5NT.A00(A002.A05()));
                    } else {
                        stringBuffer.append(r5.A01);
                        stringBuffer.append(" value = ");
                        stringBuffer.append(C93034Ys.A00(A002.A05()));
                        stringBuffer.append(str);
                    }
                    stringBuffer.append(r6);
                    stringBuffer.append(str);
                }
                stringBuffer.append(str);
            }
        }
        return stringBuffer.toString();
    }

    @Override // java.security.cert.X509Certificate, java.security.cert.Certificate
    public final void verify(PublicKey publicKey, Provider provider) {
        try {
            A02(publicKey, new AnonymousClass5GQ(provider, this));
        } catch (NoSuchProviderException e) {
            throw new NoSuchAlgorithmException(C12960it.A0d(e.getMessage(), C12960it.A0k("provider issue: ")));
        }
    }
}
