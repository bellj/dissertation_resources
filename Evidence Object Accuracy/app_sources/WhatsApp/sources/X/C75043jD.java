package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.scroller.RecyclerFastScroller;

/* renamed from: X.3jD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75043jD extends AbstractC05270Ox {
    public final /* synthetic */ RecyclerFastScroller A00;

    public C75043jD(RecyclerFastScroller recyclerFastScroller) {
        this.A00 = recyclerFastScroller;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i2 != 0) {
            RecyclerFastScroller recyclerFastScroller = this.A00;
            recyclerFastScroller.A02();
            if (recyclerFastScroller.A08 != null && recyclerFastScroller.A02.getVisibility() == 0) {
                recyclerFastScroller.A08.AfN();
            }
        }
    }
}
