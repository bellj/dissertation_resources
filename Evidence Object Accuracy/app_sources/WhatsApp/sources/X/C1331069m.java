package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.69m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1331069m implements AnonymousClass6MM {
    public final /* synthetic */ C128545wH A00;
    public final /* synthetic */ C130775zx A01;
    public final /* synthetic */ PinBottomSheetDialogFragment A02;
    public final /* synthetic */ AbstractC1308360d A03;

    public C1331069m(C128545wH r1, C130775zx r2, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC1308360d r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = pinBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A02.A1M();
        this.A03.A01();
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        Object[] objArr = new Object[0];
        this.A03.A05(this.A02, this.A00.A02(C130775zx.A00(Boolean.TRUE, str, "AUTH", null, null, objArr, C117295Zj.A03(this.A01.A01))));
    }
}
