package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: X.06l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC013806l {
    @Override // X.AbstractC013806l
    void setTint(int i);

    @Override // X.AbstractC013806l
    void setTintList(ColorStateList colorStateList);

    @Override // X.AbstractC013806l
    void setTintMode(PorterDuff.Mode mode);
}
