package X;

import android.app.Activity;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.24B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24B implements AbstractC28771Oy {
    public C15370n3 A00;
    public WeakReference A01;
    public final C14900mE A02;
    public final C15610nY A03;
    public final C17050qB A04;
    public final C14950mJ A05;
    public final C16120oU A06;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public AnonymousClass24B(Activity activity, C14900mE r3, C15610nY r4, C17050qB r5, C14950mJ r6, C15370n3 r7, C16120oU r8) {
        this.A02 = r3;
        this.A06 = r8;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = new WeakReference(activity);
        this.A00 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0069, code lost:
        if (r5 != -1) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1RN r8) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.AnonymousClass24A
            if (r0 != 0) goto L_0x0072
            java.lang.ref.WeakReference r0 = r7.A01
            if (r0 == 0) goto L_0x00ce
            java.lang.Object r3 = r0.get()
            android.app.Activity r3 = (android.app.Activity) r3
            if (r3 == 0) goto L_0x00ce
            boolean r0 = X.C36021jC.A03(r3)
            if (r0 != 0) goto L_0x00ce
            X.0mE r0 = r7.A02
            X.0kS r0 = r0.A00
            if (r0 != r3) goto L_0x00ce
            int r5 = r8.A00
            r0 = 9
            r6 = 0
            if (r5 != r0) goto L_0x003e
            X.0qB r2 = r7.A04
            X.0mJ r1 = r7.A05
            X.249 r0 = new X.249
            r0.<init>(r3, r1)
            boolean r0 = r2.A04(r0)
            if (r0 == 0) goto L_0x003d
        L_0x0032:
            r2 = 2131887828(0x7f1206d4, float:1.9410274E38)
            r1 = 2131892411(0x7f1218bb, float:1.941957E38)
        L_0x0038:
            java.lang.Object[] r0 = new java.lang.Object[r6]
            r7.A01(r0, r5, r2, r1)
        L_0x003d:
            return
        L_0x003e:
            r0 = 4
            if (r5 != r0) goto L_0x0048
            r2 = 2131887828(0x7f1206d4, float:1.9410274E38)
            r1 = 2131888976(0x7f120b50, float:1.9412603E38)
            goto L_0x0038
        L_0x0048:
            r0 = 5
            if (r5 != r0) goto L_0x0064
            X.0n3 r4 = r7.A00
            if (r4 == 0) goto L_0x006b
            r3 = 2131887828(0x7f1206d4, float:1.9410274E38)
            r2 = 2131892235(0x7f12180b, float:1.9419213E38)
            r0 = 1
            java.lang.Object[] r1 = new java.lang.Object[r0]
            X.0nY r0 = r7.A03
            java.lang.String r0 = r0.A04(r4)
            r1[r6] = r0
            r7.A01(r1, r5, r3, r2)
            return
        L_0x0064:
            r0 = 8
            if (r5 == r0) goto L_0x006b
            r0 = -1
            if (r5 != r0) goto L_0x0032
        L_0x006b:
            r2 = 2131887828(0x7f1206d4, float:1.9410274E38)
            r1 = 2131888997(0x7f120b65, float:1.9412645E38)
            goto L_0x0038
        L_0x0072:
            r4 = 2131891077(0x7f121385, float:1.9416864E38)
            java.lang.ref.WeakReference r0 = r7.A01
            if (r0 == 0) goto L_0x00d1
            java.lang.Object r6 = r0.get()
            android.app.Activity r6 = (android.app.Activity) r6
            if (r6 == 0) goto L_0x00d1
            boolean r0 = X.C36021jC.A03(r6)
            if (r0 != 0) goto L_0x00d1
            X.0mE r0 = r7.A02
            X.0kS r0 = r0.A00
            if (r0 != r6) goto L_0x00d1
            int r5 = r8.A00
            r0 = 9
            r3 = 0
            if (r5 != r0) goto L_0x00ac
            X.0qB r2 = r7.A04
            X.0mJ r1 = r7.A05
            X.249 r0 = new X.249
            r0.<init>(r6, r1)
            boolean r0 = r2.A04(r0)
            if (r0 == 0) goto L_0x003d
        L_0x00a3:
            r1 = 2131892412(0x7f1218bc, float:1.9419572E38)
        L_0x00a6:
            java.lang.Object[] r0 = new java.lang.Object[r3]
            r7.A01(r0, r5, r4, r1)
            return
        L_0x00ac:
            r0 = 4
            if (r5 != r0) goto L_0x00bc
            boolean r0 = X.C14950mJ.A00()
            r1 = 2131888974(0x7f120b4e, float:1.9412598E38)
            if (r0 == 0) goto L_0x00a6
            r1 = 2131888973(0x7f120b4d, float:1.9412596E38)
            goto L_0x00a6
        L_0x00bc:
            r0 = 5
            if (r5 != r0) goto L_0x00c3
            r1 = 2131891078(0x7f121386, float:1.9416866E38)
            goto L_0x00a6
        L_0x00c3:
            r0 = 8
            if (r5 == r0) goto L_0x00ca
            r0 = -1
            if (r5 != r0) goto L_0x00a3
        L_0x00ca:
            r1 = 2131888998(0x7f120b66, float:1.9412647E38)
            goto L_0x00a6
        L_0x00ce:
            java.lang.String r0 = "basemediadownloadlistener/notifyuser/skip"
            goto L_0x00d3
        L_0x00d1:
            java.lang.String r0 = "productdownloadlistener/notifyuser/skip"
        L_0x00d3:
            com.whatsapp.util.Log.i(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass24B.A00(X.1RN):void");
    }

    public void A01(Object[] objArr, int i, int i2, int i3) {
        Activity activity;
        WeakReference weakReference = this.A01;
        if (weakReference == null || (activity = (Activity) weakReference.get()) == null || C36021jC.A03(activity) || this.A02.A00 != activity) {
            Log.i("basemediadownloadlistener/notifyuser/skip");
            return;
        }
        AnonymousClass009.A05(activity);
        AbstractC13860kS r3 = (AbstractC13860kS) activity;
        if (i == 4) {
            r3.Adq(new C1096252k(activity, AnonymousClass3GM.A00(this.A06, 4), 4), objArr, i2, i3, R.string.manage_storage_button_text);
        } else {
            r3.Adr(objArr, i2, i3);
        }
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        this.A01 = null;
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r7, C28781Oz r8) {
        if (this instanceof C618932z) {
            C618932z r3 = (C618932z) this;
            int i = r7.A00;
            if (i == 0) {
                return;
            }
            if (i == 9) {
                if (!r3.A02) {
                    r3.A00(r7);
                    r3.A02 = true;
                }
            } else if (i == 4) {
                if (!r3.A00) {
                    r3.A00(r7);
                    r3.A00 = true;
                }
            } else if (i != 2 && i != 3 && i != 11) {
                r3.A00(r7);
            } else if (!r3.A01) {
                r3.A00(r7);
                r3.A01 = true;
            }
        } else if (this instanceof C618832y) {
            C618832y r4 = (C618832y) this;
            int i2 = r7.A00;
            if (i2 == 0) {
                C26531Dv r2 = r4.A00;
                synchronized (r2) {
                    Log.i("gdpr/on-report-downloaded");
                    r2.A0C.A0T(3);
                }
            } else if (i2 == 5 || i2 == 8) {
                r4.A01(new Object[0], i2, R.string.download_failed, R.string.gdpr_download_expired);
                r4.A00.A04();
            } else if (i2 == 4) {
                r4.A01(new Object[0], i2, R.string.download_failed, R.string.insufficient_space_for_download_shared_storage);
            } else {
                r4.A00(r7);
            }
            r4.A01 = null;
        } else if (r7.A00 != 0) {
            A00(r7);
            this.A01 = null;
        }
    }
}
