package X;

import android.net.Uri;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1Fo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26981Fo {
    public final C14330lG A00;
    public final C15810nw A01;
    public final C17050qB A02;
    public final C16590pI A03;
    public final C15890o4 A04;
    public final C14820m6 A05;
    public final C14950mJ A06;
    public final C26991Fp A07;
    public final C27001Fq A08;

    public C26981Fo(C14330lG r1, C15810nw r2, C17050qB r3, C16590pI r4, C15890o4 r5, C14820m6 r6, C14950mJ r7, C26991Fp r8, C27001Fq r9) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A07 = r8;
        this.A08 = r9;
    }

    public static final Uri A00(String str, String str2, String str3) {
        Uri.Builder appendPath = new Uri.Builder().scheme("content").authority("com.whatsapp.provider.MigrationContentProvider").appendPath("media");
        if (str != null) {
            appendPath.appendQueryParameter("query_param_country_code", str);
        }
        if (str2 != null) {
            appendPath.appendQueryParameter("query_param_phone_number", str2);
        }
        if (str3 != null) {
            appendPath.appendQueryParameter("path", str3);
        }
        return appendPath.build();
    }

    public static boolean A01(File file) {
        StringBuilder sb;
        String str;
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            StringBuilder sb2 = new StringBuilder("MediaMigrationUtil/mediaFolderIsEmpty/no-files-in-folder: ");
            sb2.append(file);
            Log.i(sb2.toString());
        } else {
            for (File file2 : listFiles) {
                if (file2.isDirectory() && !A01(file2)) {
                    sb = new StringBuilder();
                    str = "MediaMigrationUtil/mediaFolderIsEmpty/is-directory-and-contain-media-file-name: ";
                } else if (!file2.isDirectory() && !".nomedia".equals(file2.getName())) {
                    sb = new StringBuilder();
                    str = "MediaMigrationUtil/mediaFolderIsEmpty/is-file-and-is-media-file-file-name: ";
                }
                sb.append(str);
                sb.append(file2);
                Log.i(sb.toString());
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002b, code lost:
        if (r4.A0C().equals(r5.getString("registration_sibling_app_phone_number", null)) == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
        // Method dump skipped, instructions count: 665
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26981Fo.A02():void");
    }
}
