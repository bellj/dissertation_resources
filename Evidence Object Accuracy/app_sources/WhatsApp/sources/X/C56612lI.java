package X;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/* renamed from: X.2lI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56612lI extends AbstractC15040mS {
    public static boolean A05;
    public C27591Id A00;
    public String A01;
    public boolean A02 = false;
    public final C93924ay A03;
    public final Object A04 = C12970iu.A0l();

    public C56612lI(C14160kx r3) {
        super(r3);
        this.A03 = new C93924ay(r3.A04);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035 A[Catch: all -> 0x015d, TryCatch #8 {, blocks: (B:3:0x0001, B:5:0x000b, B:6:0x0013, B:8:0x001d, B:10:0x0021, B:11:0x0029, B:13:0x002f, B:16:0x0035, B:17:0x0037, B:19:0x003d, B:20:0x004a, B:70:0x0148, B:71:0x014b, B:72:0x0159, B:21:0x004b, B:23:0x0050, B:25:0x0066, B:27:0x0074, B:28:0x007d, B:29:0x0082, B:33:0x008a, B:35:0x0097, B:36:0x009c, B:39:0x00a6, B:41:0x00aa, B:42:0x00b8, B:44:0x00ce, B:46:0x00d0, B:48:0x00d8, B:50:0x00da, B:52:0x00e0, B:53:0x00e8, B:62:0x0116, B:63:0x011f, B:64:0x012f, B:66:0x0131, B:67:0x0141, B:55:0x00ea, B:57:0x0100, B:58:0x0108, B:59:0x010a, B:61:0x010c), top: B:79:0x0001, inners: #8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003d A[Catch: all -> 0x015d, TryCatch #8 {, blocks: (B:3:0x0001, B:5:0x000b, B:6:0x0013, B:8:0x001d, B:10:0x0021, B:11:0x0029, B:13:0x002f, B:16:0x0035, B:17:0x0037, B:19:0x003d, B:20:0x004a, B:70:0x0148, B:71:0x014b, B:72:0x0159, B:21:0x004b, B:23:0x0050, B:25:0x0066, B:27:0x0074, B:28:0x007d, B:29:0x0082, B:33:0x008a, B:35:0x0097, B:36:0x009c, B:39:0x00a6, B:41:0x00aa, B:42:0x00b8, B:44:0x00ce, B:46:0x00d0, B:48:0x00d8, B:50:0x00da, B:52:0x00e0, B:53:0x00e8, B:62:0x0116, B:63:0x011f, B:64:0x012f, B:66:0x0131, B:67:0x0141, B:55:0x00ea, B:57:0x0100, B:58:0x0108, B:59:0x010a, B:61:0x010c), top: B:79:0x0001, inners: #8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0144  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final synchronized X.C27591Id A00(X.C56612lI r15) {
        /*
        // Method dump skipped, instructions count: 352
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56612lI.A00(X.2lI):X.1Id");
    }

    public static String A01(String str) {
        MessageDigest messageDigest;
        int i = 0;
        while (true) {
            if (i >= 2) {
                messageDigest = null;
                break;
            }
            try {
                messageDigest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException unused) {
            }
            if (messageDigest != null) {
                break;
            }
            i++;
        }
        if (messageDigest == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, messageDigest.digest(str.getBytes())));
    }

    private final boolean A02(String str) {
        try {
            String A01 = A01(str);
            A09("Storing hashed adid.");
            FileOutputStream openFileOutput = ((C15050mT) this).A00.A00.openFileOutput("gaClientIdData", 0);
            openFileOutput.write(A01.getBytes());
            openFileOutput.close();
            this.A01 = A01;
            return true;
        } catch (IOException e) {
            A0C("Error creating hash file", e);
            return false;
        }
    }
}
