package X;

import android.app.Activity;
import android.content.Context;

/* renamed from: X.697  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass697 implements AbstractC450620a {
    public final C14900mE A00;
    public final C16590pI A01;
    public final C20370ve A02;
    public final AnonymousClass102 A03;
    public final C14850m9 A04;
    public final C17220qS A05;
    public final AnonymousClass68Z A06;
    public final C1308460e A07;
    public final C18650sn A08;
    public final C18610sj A09;
    public final C17070qD A0A;
    public final C121265hX A0B;
    public final C18590sh A0C;
    public final AbstractC14440lR A0D;

    public AnonymousClass697(C14900mE r1, C16590pI r2, C20370ve r3, AnonymousClass102 r4, C14850m9 r5, C17220qS r6, AnonymousClass68Z r7, C1308460e r8, C18650sn r9, C18610sj r10, C17070qD r11, C121265hX r12, C18590sh r13, AbstractC14440lR r14) {
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A0D = r14;
        this.A05 = r6;
        this.A0C = r13;
        this.A0A = r11;
        this.A07 = r8;
        this.A09 = r10;
        this.A03 = r4;
        this.A02 = r3;
        this.A06 = r7;
        this.A08 = r9;
        this.A0B = r12;
    }

    @Override // X.AbstractC450620a
    public void Aa3(Activity activity, AnonymousClass1IR r4, AnonymousClass5UT r5) {
        C12960it.A1E(new C124155oh(activity, r4, this, r5), this.A0D);
    }

    @Override // X.AbstractC450620a
    public void Afg(AnonymousClass1ZR r13, AnonymousClass6Ln r14) {
        Context context = this.A01.A00;
        C14900mE r3 = this.A00;
        C17220qS r5 = this.A05;
        C18590sh r11 = this.A0C;
        new C120485gG(context, r3, this.A03, r5, this.A06, this.A07, this.A08, this.A09, this.A0B, r11).A00(r13, null, r14);
    }
}
