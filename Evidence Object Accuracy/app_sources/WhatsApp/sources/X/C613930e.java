package X;

/* renamed from: X.30e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C613930e extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Long A03;
    public Long A04;

    public C613930e() {
        super(3078, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(5, this.A01);
        r3.Abe(3, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCtwaBusinessPreview {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogPresent", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaBusinessPreviewAction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "eventTimestampMs", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "openingHoursPresent", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profileLoadTimeMs", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
