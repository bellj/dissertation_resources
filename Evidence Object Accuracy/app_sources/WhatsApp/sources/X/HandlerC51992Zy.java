package X;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import java.lang.ref.SoftReference;

/* renamed from: X.2Zy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC51992Zy extends Handler {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC51992Zy(Looper looper) {
        super(looper);
        AnonymousClass009.A05(looper);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        Object obj = message.obj;
        if (obj != null) {
            Pair pair = (Pair) obj;
            long A0G = C12980iv.A0G(pair.first);
            Object obj2 = pair.second;
            if (obj2 instanceof Drawable) {
                AnonymousClass3IZ.A0Z.put(Long.valueOf(A0G), new SoftReference(obj2));
                return;
            }
            C52502ax r4 = (C52502ax) obj2;
            Drawable drawable = (Drawable) r4.getTag();
            AnonymousClass3IZ.A0Z.put(Long.valueOf(A0G), new SoftReference(drawable));
            if (r4.A01 == A0G) {
                r4.A03 = drawable;
                r4.invalidate();
            }
        }
    }
}
