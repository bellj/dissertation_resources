package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119785f6 extends AnonymousClass1ZX {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(13);
    public int A00 = 0;
    public String A01;
    public String A02;
    public String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r8, AnonymousClass1V8 r9, int i) {
        AbstractC28901Pl A05;
        int i2;
        boolean equals = "1".equals(r9.A0I("can-sell", null));
        boolean A1T = C117295Zj.A1T(r9, "can-payout", null, "1");
        boolean A1T2 = C117295Zj.A1T(r9, "can-add-payout", null, "1");
        int A00 = (equals ? 1 : 0) + C117305Zk.A00(A1T ? 1 : 0);
        int i3 = 0;
        if (A1T2) {
            i3 = 4;
        }
        super.A00 = A00 + i3;
        String A0I = r9.A0I("display-state", null);
        if (TextUtils.isEmpty(A0I)) {
            A0I = "ACTIVE";
        }
        this.A06 = A0I;
        this.A08 = r9.A0I("merchant-id", null);
        this.A0D = C117295Zj.A1T(r9, "p2m-eligible", null, "1");
        this.A0E = C117295Zj.A1T(r9, "p2p-eligible", null, "1");
        this.A0B = r9.A0I("support-phone-number", null);
        super.A02 = r9.A0I("business-name", null);
        this.A02 = r9.A0I("gateway-name", null);
        super.A03 = r9.A0I("country", null);
        this.A04 = r9.A0I("credential-id", null);
        super.A01 = C28421Nd.A01(r9.A0I("created", null), 0);
        this.A05 = r9.A0I("dashboard-url", null);
        this.A0A = r9.A0I("provider_contact_website", null);
        this.A07 = r9.A0I("logo-uri", null);
        this.A0C = C12960it.A0l();
        Iterator it = r9.A0J("payout").iterator();
        while (it.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it);
            String A0I2 = A0d.A0I("type", null);
            if ("bank".equals(A0I2)) {
                C119745f2 r0 = new C119745f2();
                r0.A01(r8, A0d, 0);
                A05 = r0.A05();
                if (A05 != null) {
                    i2 = r0.A00;
                    A05.A04 = i2;
                    A05.A0C = this.A04;
                    this.A0C.add(A05);
                }
            } else if ("prepaid-card".equals(A0I2)) {
                C119775f5 r1 = new C119775f5();
                r1.A01(r8, A0d, 0);
                ((AbstractC30871Zd) r1).A00 = 8;
                A05 = r1.A05();
                if (A05 != null) {
                    i2 = r1.A01;
                    A05.A04 = i2;
                    A05.A0C = this.A04;
                    this.A0C.add(A05);
                }
            }
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: BrazilMerchantMethodData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        JSONObject A0B = A0B();
        try {
            A0B.put("v", 1);
            if (!TextUtils.isEmpty(this.A05)) {
                A0B.put("dashboardUrl", this.A05);
            }
            if (!TextUtils.isEmpty(this.A03)) {
                A0B.put("notificationType", this.A03);
            }
            if (!TextUtils.isEmpty(this.A02)) {
                A0B.put("gatewayName", this.A02);
            }
            if (!TextUtils.isEmpty(this.A0A)) {
                A0B.put("providerContactWebsite", this.A0A);
            }
            A0B.put("p2mEligible", this.A0D);
            A0B.put("p2pEligible", this.A0E);
            return C117305Zk.A0l(this.A07, "logoUri", A0B);
        } catch (JSONException e) {
            Log.w(C12960it.A0b("PAY: BrazilMerchantMethodData toDBString threw: ", e));
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                A0C(A05);
                super.A00 = A05.optInt("state", 0);
                this.A08 = A05.optString("merchantId", null);
                this.A0D = A05.optBoolean("p2mEligible", false);
                this.A0E = A05.optBoolean("p2pEligible", false);
                this.A0B = A05.optString("supportPhoneNumber", null);
                this.A05 = A05.optString("dashboardUrl", null);
                this.A03 = A05.optString("notificationType", null);
                this.A02 = A05.optString("gatewayName", null);
                this.A0A = A05.optString("providerContactWebsite", null);
                this.A07 = A05.optString("logoUri", null);
            } catch (JSONException e) {
                Log.w(C12960it.A0b("PAY: BrazilMerchantMethodData fromDBString threw: ", e));
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C17930rd A00 = C17930rd.A00("BR");
        if (A00 == null) {
            return null;
        }
        return new AnonymousClass1ZW(A00, this, this.A04, this.A07, this.A08, this.A02, this.A0D, this.A0E);
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return new LinkedHashSet(Collections.singletonList(C30771Yt.A04));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ merchantId: ");
        String str = this.A08;
        A0k.append(str);
        A0k.append(" p2mEligible: ");
        A0k.append(this.A0D);
        A0k.append(" p2pEligible: ");
        A0k.append(this.A0E);
        A0k.append(" state: ");
        A0k.append(super.A00);
        A0k.append(" supportPhoneNumber: ");
        A0k.append(this.A0B);
        A0k.append(" dashboardUrl: ");
        A0k.append(this.A05);
        A0k.append(" merchantId: ");
        A0k.append(str);
        A0k.append(" businessName: ");
        A0k.append(super.A02);
        A0k.append(" displayState: ");
        A0k.append(this.A06);
        A0k.append(" providerContactWebsite: ");
        A0k.append(this.A0A);
        A0k.append(" logoUri: ");
        A0k.append(this.A07);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(super.A00);
        parcel.writeString(this.A08);
        parcel.writeString(this.A0B);
        parcel.writeString(super.A02);
        parcel.writeString(this.A02);
        parcel.writeString(this.A06);
        parcel.writeString(super.A03);
        parcel.writeString(this.A04);
        parcel.writeLong(super.A01);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A01);
        parcel.writeString(this.A05);
        parcel.writeString(this.A03);
        parcel.writeByte(this.A0D ? (byte) 1 : 0);
        parcel.writeByte(this.A0E ? (byte) 1 : 0);
        parcel.writeString(this.A0A);
        parcel.writeString(this.A07);
    }
}
