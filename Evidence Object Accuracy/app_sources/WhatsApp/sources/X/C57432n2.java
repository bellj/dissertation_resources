package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57432n2 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57432n2 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02 = AbstractC27881Jp.A01;
    public String A03 = "";
    public String A04 = "";

    static {
        C57432n2 r0 = new C57432n2();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57432n2 r9 = (C57432n2) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A03;
                int i2 = r9.A00;
                this.A03 = r8.Afy(str, r9.A03, A1R, C12960it.A1R(i2));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A02 = r8.Afm(this.A02, r9.A02, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 8, 8);
                String str2 = this.A04;
                int i4 = r9.A00;
                this.A04 = r8.Afy(str2, r9.A04, A1V, C12960it.A1V(i4 & 8, 8));
                if (r8 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A03 = A0A;
                            } else if (A03 == 16) {
                                int A02 = r82.A02();
                                if ((A02 != 0 ? A02 != 1 ? A02 != 2 ? null : AnonymousClass4B1.A03 : AnonymousClass4B1.A01 : AnonymousClass4B1.A02) == null) {
                                    super.A0X(2, A02);
                                } else {
                                    this.A00 |= 2;
                                    this.A01 = A02;
                                }
                            } else if (A03 == 130) {
                                this.A00 |= 4;
                                this.A02 = r82.A08();
                            } else if (A03 == 138) {
                                String A0A2 = r82.A0A();
                                this.A00 |= 8;
                                this.A04 = A0A2;
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57432n2();
            case 5:
                return new C81593uG();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57432n2.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A03, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A03(2, this.A01, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A02, 16, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(17, this.A04, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A02, 16);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(17, this.A04);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
