package X;

/* renamed from: X.4TI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TI {
    public final int A00 = 200;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final boolean A05;

    public AnonymousClass4TI(String str, String str2, String str3, String str4, boolean z) {
        this.A03 = str;
        this.A04 = str2;
        this.A02 = str3;
        this.A01 = str4;
        this.A05 = z;
    }
}
