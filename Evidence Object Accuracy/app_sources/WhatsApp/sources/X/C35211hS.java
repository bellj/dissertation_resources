package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1hS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35211hS {
    public C30751Yr A00;
    public final long A01;
    public final AnonymousClass1IS A02;
    public final List A03;

    public C35211hS(AnonymousClass1IS r2, List list, long j) {
        ArrayList arrayList = new ArrayList();
        this.A03 = arrayList;
        this.A02 = r2;
        this.A01 = j;
        if (list != null) {
            arrayList.addAll(list);
        }
    }
}
