package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09B  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09B extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass0BZ A00;

    public AnonymousClass09B(AnonymousClass0BZ r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        AbstractC12110hO r0 = this.A00.A06;
        if (r0 != null) {
            r0.AW1();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AbstractC12110hO r0 = this.A00.A06;
        if (r0 != null) {
            r0.AW1();
        }
    }
}
