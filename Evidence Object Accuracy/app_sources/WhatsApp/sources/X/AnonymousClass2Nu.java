package X;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape0S0001000_I0;
import com.whatsapp.calling.views.VoipCallFooter;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.2Nu  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Nu implements AbstractC35201hQ {
    public static final Object A0J = new AudioManager.OnAudioFocusChangeListener() { // from class: X.4hx
        @Override // android.media.AudioManager.OnAudioFocusChangeListener
        public final void onAudioFocusChange(int i) {
            Log.i(C12960it.A0W(i, "voip audio focus changed: "));
        }
    };
    public static final boolean A0K;
    public int A00;
    public int A01;
    public Boolean A02;
    public boolean A03 = false;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public final Context A09;
    public final C236912q A0A;
    public final C29631Ua A0B;
    public final AnonymousClass01d A0C;
    public final C14820m6 A0D;
    public final C14850m9 A0E;
    public final AnonymousClass3FX A0F;
    public final AnonymousClass2ON A0G;
    public final C237612x A0H;
    public final AnonymousClass2Z1 A0I;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 28) {
            z = true;
        }
        A0K = z;
    }

    public AnonymousClass2Nu(Context context, C236912q r5, C29631Ua r6, AnonymousClass01d r7, C14820m6 r8, C14850m9 r9, C237612x r10) {
        AnonymousClass3FX r0;
        AnonymousClass39S r02;
        this.A0B = r6;
        this.A0E = r9;
        this.A09 = context;
        this.A0C = r7;
        this.A0D = r8;
        this.A0A = r5;
        this.A0H = r10;
        StringBuilder sb = new StringBuilder("voip/audio_route/create ");
        sb.append(this);
        Log.i(sb.toString());
        AnonymousClass4LQ r2 = new AnonymousClass4LQ(this);
        if (Build.VERSION.SDK_INT >= 23) {
            r0 = new AnonymousClass39M(r7);
        } else {
            r0 = new AnonymousClass483(context, r7);
        }
        r0.A00 = r2;
        this.A0F = r0;
        this.A0I = new AnonymousClass2Z1(this);
        if (!A0K) {
            r02 = null;
        } else {
            r02 = new AnonymousClass39S(this);
        }
        this.A0G = r02;
    }

    public final String A00() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        boolean z = false;
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (stackTraceElement.getClassName().equals(getClass().getName())) {
                z = true;
            } else if (z) {
                StringBuilder sb = new StringBuilder();
                sb.append(stackTraceElement.getClassName());
                sb.append("/");
                sb.append(stackTraceElement.getMethodName());
                return sb.toString();
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r3.A00 == 3) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r3 = this;
            boolean r0 = r3.A07
            if (r0 != 0) goto L_0x000a
            int r1 = r3.A00
            r0 = 3
            r2 = 0
            if (r1 != r0) goto L_0x000b
        L_0x000a:
            r2 = 1
        L_0x000b:
            r3.A07 = r2
            java.lang.String r1 = "voip/audio_route/rememberBluetoothState "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Nu.A01():void");
    }

    public void A02() {
        StringBuilder sb = new StringBuilder("voip/audio_route/resetAudioManager ");
        sb.append(this);
        sb.append(", telecom: ");
        sb.append(this.A06);
        Log.i(sb.toString());
        if (!this.A06) {
            AudioManager A0G = this.A0C.A0G();
            if (A0G != null) {
                A0G.setSpeakerphoneOn(false);
            }
            A09(null, false);
        }
        this.A00 = 2;
        AudioManager A0G2 = this.A0C.A0G();
        if (A0G2 != null) {
            try {
                if (!this.A06) {
                    A0G2.setMode(0);
                }
            } catch (Exception e) {
                Log.e(e);
            }
            A0G2.abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) A0J);
        }
    }

    public void A03() {
        AudioManager A0G = this.A0C.A0G();
        if (A0G != null && this.A02 != null) {
            StringBuilder sb = new StringBuilder("voip/audio_route/restoreOsMicrophone call from: ");
            sb.append(A00());
            sb.append(", isMicrophoneMute change from ");
            sb.append(A0G.isMicrophoneMute());
            sb.append(" to ");
            sb.append(this.A02);
            Log.i(sb.toString());
            A0G.setMicrophoneMute(this.A02.booleanValue());
        }
    }

    public void A04() {
        AudioManager A0G = this.A0C.A0G();
        if (A0G != null) {
            this.A02 = Boolean.valueOf(A0G.isMicrophoneMute());
            A0G.setMicrophoneMute(false);
            StringBuilder sb = new StringBuilder("voip/audio_route/unmuteOsMicrophone call from: ");
            sb.append(A00());
            sb.append(", isMicrophoneMute change from ");
            sb.append(this.A02);
            sb.append(" to ");
            sb.append(A0G.isMicrophoneMute());
            Log.i(sb.toString());
        }
    }

    public void A05(CallInfo callInfo) {
        if (callInfo != null && callInfo.callState != Voip.CallState.NONE) {
            if (this.A0A.A08()) {
                A06(callInfo);
                A09(callInfo, true);
                return;
            }
            C52142aJ A0F = this.A0B.A0F(callInfo.callId);
            if (!A0K || A0F == null || A0F.getCallAudioState() == null ? !this.A0F.A03() : A0F.getCallAudioState().getRoute() != 4) {
                Boolean A00 = Voip.A00("options.android_should_use_speaker_for_ringtone");
                if (callInfo.videoEnabled || ((A00 != null && A00.booleanValue() && callInfo.callState == Voip.CallState.RECEIVED_CALL) || (AnonymousClass1SF.A0M(this.A0D, this.A0E) && callInfo.groupJid != null))) {
                    A0A(callInfo, true);
                } else {
                    A0A(callInfo, false);
                }
            } else {
                A08(callInfo, null);
            }
        }
    }

    public void A06(CallInfo callInfo) {
        boolean z = true;
        if (this.A00 != 1 || callInfo.videoEnabled || callInfo.callState == Voip.CallState.RECEIVED_CALL) {
            z = false;
        }
        this.A08 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        if (r4.callState != com.whatsapp.voipcalling.Voip.CallState.RECEIVED_CALL) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(com.whatsapp.voipcalling.CallInfo r4, java.lang.Boolean r5) {
        /*
            r3 = this;
            r3.A08(r4, r5)
            if (r4 == 0) goto L_0x005a
            com.whatsapp.voipcalling.Voip$CallState r2 = r4.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.NONE
            if (r2 == r0) goto L_0x005a
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACTIVE_ELSEWHERE
            if (r2 == r0) goto L_0x005a
            java.lang.String r0 = "voip/audio_route/checkAndTurnOnSpeakerPhone usingSpeakerBefore: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            boolean r0 = r3.A08
            r1.append(r0)
            java.lang.String r0 = ", video call: "
            r1.append(r0)
            boolean r0 = r4.videoEnabled
            r1.append(r0)
            java.lang.String r0 = ", call state: "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            int r1 = r3.A00
            r0 = 2
            r2 = 1
            if (r1 != r0) goto L_0x005b
            boolean r0 = r3.A08
            if (r0 != 0) goto L_0x0057
            java.lang.String r0 = "options.android_should_use_speaker_for_ringtone"
            java.lang.Boolean r1 = com.whatsapp.voipcalling.Voip.A00(r0)
            boolean r0 = r4.videoEnabled
            if (r0 != 0) goto L_0x0057
            if (r1 == 0) goto L_0x005b
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x005b
            com.whatsapp.voipcalling.Voip$CallState r1 = r4.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.RECEIVED_CALL
            if (r1 != r0) goto L_0x005b
        L_0x0057:
            r3.A0A(r4, r2)
        L_0x005a:
            return
        L_0x005b:
            int r0 = r3.A00
            if (r0 != r2) goto L_0x005a
            com.whatsapp.voipcalling.Voip$CallState r1 = r4.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACCEPT_SENT
            if (r1 != r0) goto L_0x005a
            boolean r0 = r3.A08
            if (r0 != 0) goto L_0x005a
            r0 = 0
            r3.A0A(r4, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Nu.A07(com.whatsapp.voipcalling.CallInfo, java.lang.Boolean):void");
    }

    public void A08(CallInfo callInfo, Boolean bool) {
        Voip.CallState callState;
        VoipCallFooter voipCallFooter;
        AnonymousClass009.A01();
        if (bool != null) {
            this.A04 = bool.booleanValue();
        }
        if (callInfo != null && (callState = callInfo.callState) != Voip.CallState.NONE && callState != Voip.CallState.ACTIVE_ELSEWHERE) {
            int i = this.A00;
            AudioManager A0G = this.A0C.A0G();
            if (A0G != null) {
                C29631Ua r4 = this.A0B;
                C52142aJ A0F = r4.A0F(callInfo.callId);
                boolean z = true;
                if (this.A04 || !A0K || A0F == null || A0F.getCallAudioState() == null) {
                    if (A0G.isBluetoothScoOn()) {
                        this.A00 = 3;
                    } else if (A0G.isSpeakerphoneOn() && !this.A05) {
                        this.A00 = 1;
                        if (i != 1) {
                            this.A08 = false;
                        }
                    } else if (this.A0F.A03()) {
                        this.A00 = 4;
                    } else {
                        this.A00 = 2;
                    }
                    z = false;
                } else {
                    int route = A0F.getCallAudioState().getRoute();
                    if (route == 1) {
                        this.A00 = 2;
                    } else if (route == 2) {
                        this.A00 = 3;
                    } else if (route == 4) {
                        this.A00 = 4;
                    } else if (route == 8) {
                        this.A00 = 1;
                    }
                }
                StringBuilder sb = new StringBuilder("voip/audio_route/updateAudioRoute: [");
                sb.append(Voip.A05(i));
                sb.append(" -> ");
                sb.append(Voip.A05(this.A00));
                sb.append("], using telecom: ");
                sb.append(z);
                sb.append(", Bluetooth: [ScoAudioState: ");
                sb.append(AnonymousClass2Z1.A00(this.A01));
                sb.append(", ScoOn: ");
                sb.append(A0G.isBluetoothScoOn());
                sb.append("], Speaker: ");
                sb.append(A0G.isSpeakerphoneOn());
                sb.append(", fallBackToNonTelecomToSyncAudioRoute: ");
                sb.append(this.A04);
                sb.append(", ");
                sb.append(this);
                Log.i(sb.toString());
                this.A05 = false;
                int i2 = this.A00;
                r4.A0x.execute(new RunnableBRunnable0Shape0S0001000_I0(i2, 1));
                AnonymousClass1L4 r2 = r4.A0c;
                if (r2 != null) {
                    boolean A08 = r4.A1S.A08();
                    AnonymousClass009.A01();
                    VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = ((VoipActivityV2) r2).A1N;
                    if (!(voipCallControlBottomSheetV2 == null || (voipCallFooter = voipCallControlBottomSheetV2.A0M) == null)) {
                        voipCallFooter.A02(callInfo, i2, A08);
                    }
                }
                r4.A0j(callInfo);
                r4.A1K = false;
            }
        }
    }

    public void A09(CallInfo callInfo, boolean z) {
        String str;
        String str2;
        C29631Ua r1 = this.A0B;
        if (callInfo == null) {
            str = null;
        } else {
            str = callInfo.callId;
        }
        C52142aJ A0F = r1.A0F(str);
        StringBuilder sb = new StringBuilder("voip/audio_route/changeBluetoothState ");
        if (z) {
            str2 = "On";
        } else {
            str2 = "Off";
        }
        sb.append(str2);
        sb.append(" using telecom: ");
        boolean z2 = false;
        if (A0F != null) {
            z2 = true;
        }
        sb.append(z2);
        Log.i(sb.toString());
        if (!A0K || A0F == null) {
            AudioManager A0G = this.A0C.A0G();
            if (A0G != null) {
                try {
                    if (z) {
                        if (A0G.isBluetoothScoOn()) {
                            Log.i("voip/audio_route/changeBluetoothState startBluetoothSco when isBluetoothScoOn is true");
                        }
                        A0G.startBluetoothSco();
                        A0G.setBluetoothScoOn(true);
                    } else {
                        A0G.setBluetoothScoOn(false);
                        A0G.stopBluetoothSco();
                    }
                } catch (Exception e) {
                    Log.e(e);
                }
                A08(callInfo, null);
                return;
            }
            return;
        }
        int i = 5;
        if (z) {
            i = 2;
        }
        A0F.setAudioRoute(i);
    }

    public void A0A(CallInfo callInfo, boolean z) {
        String str;
        String str2;
        C29631Ua r1 = this.A0B;
        if (callInfo == null) {
            str = null;
        } else {
            str = callInfo.callId;
        }
        C52142aJ A0F = r1.A0F(str);
        StringBuilder sb = new StringBuilder("voip/audio_route/changeSpeakerphoneState ");
        if (z) {
            str2 = "On";
        } else {
            str2 = "Off";
        }
        sb.append(str2);
        sb.append(" using telecom: ");
        boolean z2 = false;
        if (A0F != null) {
            z2 = true;
        }
        sb.append(z2);
        Log.i(sb.toString());
        if (!A0K || A0F == null) {
            AudioManager A0G = this.A0C.A0G();
            if (A0G != null) {
                A0G.setSpeakerphoneOn(z);
                A08(callInfo, null);
                return;
            }
            return;
        }
        int i = 5;
        if (z) {
            i = 8;
        }
        A0F.setAudioRoute(i);
    }

    @Override // X.AbstractC35201hQ
    public void ANJ(int i) {
        StringBuilder sb = new StringBuilder("voip/audio_route/onBluetoothHeadsetConnectionStateChanged state: ");
        sb.append(C236912q.A00(i));
        Log.i(sb.toString());
        if (i != 0) {
            if (i == 2) {
                A05(Voip.getCallInfo());
                return;
            } else if (i != 3) {
                return;
            }
        }
        A09(Voip.getCallInfo(), false);
    }
}
