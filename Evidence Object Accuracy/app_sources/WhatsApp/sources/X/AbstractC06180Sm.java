package X;

import android.util.Log;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0Sm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC06180Sm {
    public boolean A00 = false;
    public boolean A01 = false;
    public final ViewGroup A02;
    public final ArrayList A03 = new ArrayList();
    public final ArrayList A04 = new ArrayList();

    public AbstractC06180Sm(ViewGroup viewGroup) {
        this.A02 = viewGroup;
    }

    public static AbstractC06180Sm A01(ViewGroup viewGroup) {
        Object tag = viewGroup.getTag(R.id.special_effects_controller_view_tag);
        if (tag instanceof AbstractC06180Sm) {
            return (AbstractC06180Sm) tag;
        }
        AnonymousClass0EJ r1 = new AnonymousClass0EJ(viewGroup);
        viewGroup.setTag(R.id.special_effects_controller_view_tag, r1);
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0259, code lost:
        if (r7 == -1) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x025b, code lost:
        r13.set(r7, r11.get(r9));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0262, code lost:
        r9 = r9 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0265, code lost:
        r0 = r0.A0C;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0269, code lost:
        if (r0 == null) goto L_0x026f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x026b, code lost:
        r10 = r0.A0E;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x026d, code lost:
        if (r10 != null) goto L_0x0274;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x026f, code lost:
        r10 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0274, code lost:
        if (r6 != false) goto L_0x027a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0276, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0277, code lost:
        if (r0 == null) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x027a, code lost:
        r0 = r14.A0C;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x027c, code lost:
        if (r0 != null) goto L_0x027f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x027f, code lost:
        r9 = r0.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0282, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0283, code lost:
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0285, code lost:
        r7 = r0.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0287, code lost:
        r15 = r13.size();
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x028c, code lost:
        if (r11 >= r15) goto L_0x029c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x028e, code lost:
        r1.put(r13.get(r11), r10.get(r11));
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x029c, code lost:
        r12 = new X.AnonymousClass00N();
        r4.A08(r14.A0A, r12);
        X.AbstractC008904n.A00(r13, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02a9, code lost:
        if (r9 == null) goto L_0x02e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02ab, code lost:
        r9.A03(r13, r12);
        r15 = r13.size() - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02b4, code lost:
        if (r15 < 0) goto L_0x02e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02b6, code lost:
        r11 = (java.lang.String) r13.get(r15);
        r9 = (android.view.View) r12.get(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02c2, code lost:
        if (r9 != null) goto L_0x02c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02c4, code lost:
        r1.remove(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02d0, code lost:
        if (r11.equals(X.AnonymousClass028.A0J(r9)) != false) goto L_0x02dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02d2, code lost:
        r1.put(X.AnonymousClass028.A0J(r9), r1.remove(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02dd, code lost:
        r15 = r15 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x02e0, code lost:
        X.AbstractC008904n.A00(r12.keySet(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02e7, code lost:
        r9 = new X.AnonymousClass00N();
        r4.A08(r0.A0A, r9);
        X.AbstractC008904n.A00(r10, r9);
        X.AbstractC008904n.A00(r1.values(), r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x02fd, code lost:
        if (r7 == null) goto L_0x0375;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x02ff, code lost:
        r7.A03(r10, r9);
        r15 = r10.size() - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0308, code lost:
        if (r15 < 0) goto L_0x038f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x030a, code lost:
        r11 = (java.lang.String) r10.get(r15);
        r0 = (android.view.View) r9.get(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x031a, code lost:
        if (r0 != null) goto L_0x0340;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x031c, code lost:
        r20 = r1.size();
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0323, code lost:
        if (r7 >= r20) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0325, code lost:
        r0 = r1.A02;
        r16 = r7 << 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0333, code lost:
        if (r11.equals(r0[r16 + 1]) == false) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0335, code lost:
        r0 = r0[r16];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0337, code lost:
        if (r0 == null) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0339, code lost:
        r1.remove(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x033d, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0348, code lost:
        if (r11.equals(X.AnonymousClass028.A0J(r0)) != false) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x034a, code lost:
        r20 = r1.size();
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0351, code lost:
        if (r7 >= r20) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0353, code lost:
        r0 = r1.A02;
        r16 = r7 << 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0361, code lost:
        if (r11.equals(r0[r16 + 1]) == false) goto L_0x036f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0363, code lost:
        r7 = r0[r16];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0365, code lost:
        if (r7 == null) goto L_0x0372;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0367, code lost:
        r1.put(r7, X.AnonymousClass028.A0J(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x036f, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0372, code lost:
        r15 = r15 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0375, code lost:
        r7 = r1.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0379, code lost:
        r7 = r7 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x037b, code lost:
        if (r7 < 0) goto L_0x038f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0389, code lost:
        if (r9.containsKey(r1.A02[(r7 << 1) + 1]) != false) goto L_0x0379;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x038b, code lost:
        r1.A06(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x038f, code lost:
        X.AnonymousClass0EJ.A00(r12, r1.keySet());
        X.AnonymousClass0EJ.A00(r9, r1.values());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03a1, code lost:
        if (r1.isEmpty() == false) goto L_0x03ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03a3, code lost:
        r27.clear();
        r26.clear();
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03ac, code lost:
        X.AnonymousClass0TQ.A00(r12, r0, r14, r6, true);
        X.AnonymousClass0WC.A00(r0, new X.AnonymousClass0e0(r9, r4, r3, r8, r6));
        r27.addAll(r12.values());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03d5, code lost:
        if (r13.isEmpty() != false) goto L_0x03e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03d7, code lost:
        r0 = (android.view.View) r12.get(r13.get(0));
        r17 = r0;
        r5.A08(r0, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03e9, code lost:
        r26.addAll(r9.values());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03f6, code lost:
        if (r10.isEmpty() != false) goto L_0x0413;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x03f8, code lost:
        r9 = (android.view.View) r9.get(r10.get(0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0403, code lost:
        if (r9 == null) goto L_0x0413;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0405, code lost:
        X.AnonymousClass0WC.A00(r0, new X.RunnableC10030du(r28, r9, r4, r5));
        r24 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x0413, code lost:
        r5.A0A(r29, r2, r27);
        r5.A0D(r2, null, null, r2, null, null, r26);
        r7 = java.lang.Boolean.TRUE;
        r3.put(r8, r7);
        r3.put(r3, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x023e, code lost:
        if (r0 != null) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0242, code lost:
        if (r10 == null) goto L_0x0244;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0244, code lost:
        r10 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0249, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x024e, code lost:
        if (r9 >= r10.size()) goto L_0x0265;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0250, code lost:
        r7 = r13.indexOf(r10.get(r9));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
        // Method dump skipped, instructions count: 1872
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC06180Sm.A02():void");
    }

    public void A03() {
        String str;
        String str2;
        ViewGroup viewGroup = this.A02;
        boolean A0q = AnonymousClass028.A0q(viewGroup);
        ArrayList arrayList = this.A03;
        synchronized (arrayList) {
            A05();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((AnonymousClass0Q9) it.next()).A01();
            }
            Iterator it2 = new ArrayList(this.A04).iterator();
            while (it2.hasNext()) {
                AnonymousClass0Q9 r4 = (AnonymousClass0Q9) it2.next();
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: ");
                    if (A0q) {
                        str2 = "";
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Container ");
                        sb2.append(viewGroup);
                        sb2.append(" is not attached to window. ");
                        str2 = sb2.toString();
                    }
                    sb.append(str2);
                    sb.append("Cancelling running operation ");
                    sb.append(r4);
                    Log.v("FragmentManager", sb.toString());
                }
                r4.A02();
            }
            Iterator it3 = new ArrayList(arrayList).iterator();
            while (it3.hasNext()) {
                AnonymousClass0Q9 r42 = (AnonymousClass0Q9) it3.next();
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: ");
                    if (A0q) {
                        str = "";
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Container ");
                        sb4.append(viewGroup);
                        sb4.append(" is not attached to window. ");
                        str = sb4.toString();
                    }
                    sb3.append(str);
                    sb3.append("Cancelling pending operation ");
                    sb3.append(r42);
                    Log.v("FragmentManager", sb3.toString());
                }
                r42.A02();
            }
        }
    }

    public void A04() {
        boolean z;
        ArrayList arrayList = this.A03;
        synchronized (arrayList) {
            A05();
            this.A00 = false;
            int size = arrayList.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                AnonymousClass0Q9 r3 = (AnonymousClass0Q9) arrayList.get(size);
                EnumC03930Jr A01 = EnumC03930Jr.A01(r3.A04.A0A);
                EnumC03930Jr r1 = r3.A01;
                EnumC03930Jr r0 = EnumC03930Jr.VISIBLE;
                if (r1 == r0 && A01 != r0) {
                    AnonymousClass0O9 r02 = r3.A04.A0C;
                    if (r02 == null) {
                        z = false;
                    } else {
                        z = r02.A0F;
                    }
                    this.A00 = z;
                }
            }
        }
    }

    public final void A05() {
        Iterator it = this.A03.iterator();
        while (it.hasNext()) {
            AnonymousClass0Q9 r2 = (AnonymousClass0Q9) it.next();
            if (r2.A00 == AnonymousClass0JF.ADDING) {
                r2.A03(AnonymousClass0JF.NONE, EnumC03930Jr.A00(r2.A04.A05().getVisibility()));
            }
        }
    }

    public final void A06(C06460Ts r7, AnonymousClass0JF r8, EnumC03930Jr r9) {
        ArrayList arrayList = this.A03;
        synchronized (arrayList) {
            AnonymousClass02N r5 = new AnonymousClass02N();
            AnonymousClass01E r4 = r7.A02;
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    AnonymousClass0Q9 r2 = new AnonymousClass0Q9(r5, r7, r8, r9);
                    arrayList.add(r2);
                    RunnableC09550d6 r0 = new RunnableC09550d6(r2, this);
                    List list = r2.A07;
                    list.add(r0);
                    list.add(new RunnableC09560d7(r2, this));
                    break;
                }
                AnonymousClass0Q9 r1 = (AnonymousClass0Q9) it.next();
                if (r1.A04.equals(r4) && !r1.A02) {
                    r1.A03(r8, r9);
                    break;
                }
            }
        }
    }
}
