package X;

import com.whatsapp.R;

/* renamed from: X.5fg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120135fg extends AbstractC38191ng {
    public final C130065yk A00;

    @Override // X.AbstractC38191ng
    public String A09() {
        return "https://faq.whatsapp.com/general/payments/cant-see-cashback-in-bank-account-br-p2p/";
    }

    public C120135fg(AnonymousClass12P r12, C21740xu r13, C15550nR r14, C15610nY r15, C14830m7 r16, AnonymousClass018 r17, C14850m9 r18, C130065yk r19, AnonymousClass14X r20) {
        super(r12, r13, r14, r15, r16, r17, r18, r20, 1);
        this.A00 = r19;
    }

    @Override // X.AbstractC38191ng
    public int A00() {
        return R.string.incentive_cashback_subtitle_br;
    }

    @Override // X.AbstractC38191ng
    public int A01() {
        return R.string.incentive_banner_default_cta_text_br;
    }

    @Override // X.AbstractC38191ng
    public int A02() {
        return R.string.incentive_banner_description_redeemed_br;
    }

    @Override // X.AbstractC38191ng
    public int A03() {
        return R.string.incentive_banner_title_redeemed_br;
    }

    @Override // X.AbstractC38191ng
    public int A04() {
        return R.string.incentives_banner_unreg_cta;
    }

    @Override // X.AbstractC38191ng
    public int A05() {
        return R.string.incentive_blurb_base_txn_text_br;
    }

    @Override // X.AbstractC38191ng
    public int A06() {
        return R.string.incentive_blurb_cashback_txn_text_br;
    }

    @Override // X.AbstractC38191ng
    public boolean A0A(C50942Ry r3, C50932Rx r4) {
        return super.A0A(r3, r4) && this.A00.A06.A03();
    }
}
