package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.16l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246816l {
    public long A00;
    public String A01;
    public String A02;
    public Map A03 = new HashMap();
    public final AbstractC15710nm A04;
    public final C18700ss A05;
    public final C246916m A06;
    public final C247216p A07;
    public final C14820m6 A08;
    public final C20140vH A09;
    public final C14850m9 A0A;
    public final C17220qS A0B;

    public C246816l(AbstractC15710nm r2, C18700ss r3, C246916m r4, C247216p r5, C14820m6 r6, C20140vH r7, C14850m9 r8, C17220qS r9) {
        this.A0B = r9;
        this.A08 = r6;
        this.A04 = r2;
        this.A06 = r4;
        this.A05 = r3;
        this.A09 = r7;
        this.A07 = r5;
        this.A0A = r8;
    }

    public String A00(UserJid userJid) {
        C14820m6 r0 = this.A08;
        String rawString = userJid.getRawString();
        SharedPreferences sharedPreferences = r0.A00;
        StringBuilder sb = new StringBuilder("smb_business_direct_connection_public_key_");
        sb.append(rawString);
        return sharedPreferences.getString(sb.toString(), null);
    }

    public synchronized String A01(UserJid userJid) {
        String str;
        if (this.A02 == null || (str = this.A01) == null) {
            C14820m6 r0 = this.A08;
            String rawString = userJid.getRawString();
            SharedPreferences sharedPreferences = r0.A00;
            StringBuilder sb = new StringBuilder("smb_business_direct_connection_enc_string_");
            sb.append(rawString);
            str = sharedPreferences.getString(sb.toString(), null);
        }
        return str;
    }

    public synchronized void A02(AnonymousClass1W3 r6, UserJid userJid, boolean z) {
        Map map = this.A03;
        List list = (List) map.get(userJid);
        if (list != null) {
            list.add(r6);
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r6);
            map.put(userJid, arrayList);
            if (z) {
                String rawString = userJid.getRawString();
                C14820m6 r0 = this.A08;
                r0.A0Z(rawString);
                SharedPreferences sharedPreferences = r0.A00;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                StringBuilder sb = new StringBuilder("smb_business_direct_connection_enc_string_");
                sb.append(rawString);
                edit.remove(sb.toString()).apply();
                SharedPreferences.Editor edit2 = sharedPreferences.edit();
                StringBuilder sb2 = new StringBuilder("smb_business_direct_connection_enc_string_expired_timestamp_");
                sb2.append(rawString);
                edit2.remove(sb2.toString()).apply();
                SharedPreferences.Editor edit3 = sharedPreferences.edit();
                StringBuilder sb3 = new StringBuilder("dc_business_domain_");
                sb3.append(rawString);
                edit3.remove(sb3.toString()).apply();
            } else if (!TextUtils.isEmpty(A00(userJid))) {
                if (A01(userJid) == null || A06(userJid)) {
                    A03(userJid);
                } else {
                    A05(userJid);
                }
            }
            new AnonymousClass1W4(userJid, this.A0B).A00(new AnonymousClass1W5(this));
        }
    }

    public final void A03(UserJid userJid) {
        AnonymousClass1W7 r8 = new AnonymousClass1W7(userJid, this.A0B);
        r8.A00 = new AnonymousClass1W8(this, userJid);
        C17220qS r7 = r8.A02;
        String A01 = r7.A01();
        r7.A09(r8, new AnonymousClass1V8(new AnonymousClass1V8("signed_user_info", new AnonymousClass1W9[]{new AnonymousClass1W9("biz_jid", r8.A01.getRawString())}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "w:biz:catalog"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("id", A01)}), A01, 287, 32000);
    }

    public synchronized void A04(UserJid userJid) {
        Map map = this.A03;
        List<AnonymousClass1W3> list = (List) map.get(userJid);
        if (list == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("DirectConnectionManager/onDirectConnectionInfoFailed/No listeners for jid - ");
            sb.append(userJid);
            Log.e(sb.toString());
        } else {
            for (AnonymousClass1W3 r0 : list) {
                r0.APB(userJid);
            }
            map.remove(userJid);
        }
    }

    public synchronized void A05(UserJid userJid) {
        Map map = this.A03;
        List<AnonymousClass1W3> list = (List) map.get(userJid);
        if (list == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("DirectConnectionManager/onDirectConnectionInfoSucceeded/No listeners for jid - ");
            sb.append(userJid);
            Log.e(sb.toString());
        } else {
            for (AnonymousClass1W3 r0 : list) {
                r0.APC(userJid);
            }
            map.remove(userJid);
        }
    }

    public boolean A06(UserJid userJid) {
        long time = new Date().getTime();
        C14820m6 r0 = this.A08;
        String rawString = userJid.getRawString();
        SharedPreferences sharedPreferences = r0.A00;
        StringBuilder sb = new StringBuilder("smb_business_direct_connection_enc_string_expired_timestamp_");
        sb.append(rawString);
        return time > sharedPreferences.getLong(sb.toString(), 0);
    }
}
