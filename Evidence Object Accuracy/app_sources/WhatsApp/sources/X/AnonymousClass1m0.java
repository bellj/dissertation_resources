package X;

import java.util.concurrent.CountDownLatch;

/* renamed from: X.1m0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1m0 extends AbstractC18860tB {
    public final AbstractC14640lm A00;
    public final CountDownLatch A01;

    public AnonymousClass1m0(AbstractC14640lm r1, CountDownLatch countDownLatch) {
        this.A00 = r1;
        this.A01 = countDownLatch;
    }
}
