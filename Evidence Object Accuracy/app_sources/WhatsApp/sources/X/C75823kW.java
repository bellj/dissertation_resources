package X;

/* renamed from: X.3kW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75823kW extends AbstractC03680Is {
    public int A00 = 0;
    public C08870bz A01;
    public final AbstractC75883ke A02;

    public C75823kW(AbstractC75883ke r3, int i) {
        AnonymousClass0RA.A00(C12960it.A1U(i));
        this.A02 = r3;
        this.A01 = C08870bz.A00(r3, r3.get(i));
    }

    @Override // X.AbstractC03680Is, java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        C08870bz r0 = this.A01;
        if (r0 != null) {
            r0.close();
        }
        this.A01 = null;
        this.A00 = -1;
        super.close();
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        write(new byte[]{(byte) i});
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            StringBuilder A0k = C12960it.A0k("length=");
            A0k.append(bArr.length);
            A0k.append("; regionStart=");
            A0k.append(i);
            throw new ArrayIndexOutOfBoundsException(C12960it.A0e("; regionLength=", A0k, i2));
        } else if (C08870bz.A01(this.A01)) {
            int i3 = this.A00 + i2;
            if (C08870bz.A01(this.A01)) {
                if (i3 > ((AnonymousClass5XR) this.A01.A04()).AGm()) {
                    AbstractC75883ke r4 = this.A02;
                    AnonymousClass5XR r3 = (AnonymousClass5XR) r4.get(i3);
                    ((AnonymousClass5XR) this.A01.A04()).A7m(r3, 0, 0, this.A00);
                    this.A01.close();
                    this.A01 = C08870bz.A00(r4, r3);
                }
                ((AnonymousClass5XR) this.A01.A04()).AgC(bArr, this.A00, i, i2);
                this.A00 += i2;
                return;
            }
            throw new C113205Go();
        } else {
            throw new C113205Go();
        }
    }
}
