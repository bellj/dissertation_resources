package X;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.io.File;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.661 */
/* loaded from: classes4.dex */
public class AnonymousClass661 implements AbstractC1311761o {
    public static final Camera.ShutterCallback A0g = new AnonymousClass63H();
    public static volatile AnonymousClass661 A0h;
    public int A00;
    public int A01;
    public int A02;
    public Matrix A03;
    public C129535xs A04;
    public C125395rB A05;
    public AbstractC1311561m A06;
    public AnonymousClass60Q A07;
    public AbstractC136426Mm A08;
    public UUID A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public final int A0E;
    public final Camera.ErrorCallback A0F = new AnonymousClass639(this);
    public final Camera.FaceDetectionListener A0G = new AnonymousClass63A(this);
    public final AbstractC136166Lg A0H = new AnonymousClass666(this);
    public final AbstractC136406Mk A0I = new AnonymousClass668(this);
    public final AnonymousClass60S A0J;
    public final C129305xV A0K;
    public final C128965wx A0L;
    public final C129935yX A0M;
    public final C128435w6 A0N = new C128435w6();
    public final AnonymousClass63B A0O;
    public final C129775yH A0P = new C129775yH();
    public final C129775yH A0Q = new C129775yH();
    public final C129885yS A0R;
    public final C130055yj A0S;
    public final C1308560f A0T;
    public final AtomicBoolean A0U = new AtomicBoolean(false);
    public final AtomicBoolean A0V = new AtomicBoolean(false);
    public final AtomicBoolean A0W = new AtomicBoolean(false);
    public volatile int A0X;
    public volatile Camera A0Y;
    public volatile AnonymousClass66P A0Z;
    public volatile C124805q9 A0a;
    public volatile FutureTask A0b;
    public volatile boolean A0c;
    public volatile boolean A0d;
    public volatile boolean A0e;
    public volatile boolean A0f;

    public AnonymousClass661(Context context) {
        C1308560f r2 = new C1308560f();
        this.A0T = r2;
        C130055yj r1 = new C130055yj(r2);
        this.A0S = r1;
        AnonymousClass60S r0 = new AnonymousClass60S(r1, r2);
        this.A0J = r0;
        C129885yS r12 = new C129885yS(r0);
        this.A0R = r12;
        this.A0L = new C128965wx();
        this.A0O = new AnonymousClass63B(r12, r2);
        this.A0K = new C129305xV(r12, r2);
        this.A0E = Math.round(TypedValue.applyDimension(1, 30.0f, context.getResources().getDisplayMetrics()));
        this.A0M = new C129935yX();
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0273  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ X.C127255uC A00(X.C129535xs r18, X.AnonymousClass661 r19, X.AbstractC1311561m r20, int r21) {
        /*
        // Method dump skipped, instructions count: 649
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass661.A00(X.5xs, X.661, X.61m, int):X.5uC");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r9.A00(X.AnonymousClass60J.A0Q) != null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
        if (r9.A00(X.AnonymousClass60J.A0d) != null) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A01(X.C129455xk r6, X.AnonymousClass661 r7, X.AnonymousClass60B r8, X.C129755yF r9, X.AnonymousClass60J r10) {
        /*
        // Method dump skipped, instructions count: 397
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass661.A01(X.5xk, X.661, X.60B, X.5yF, X.60J):void");
    }

    public static /* synthetic */ void A02(AnonymousClass661 r4) {
        try {
            try {
                if (r4.A0e) {
                    r4.A08();
                }
            } catch (RuntimeException e) {
                Log.e("Camera1Device", "Stop video recording failed, likely due to nothing being captured", e);
            }
            if (r4.A0Y != null) {
                r4.A06();
                r4.A0M.A00();
            }
            if (r4.A0Z != null) {
                r4.A0Z.A01();
            }
            r4.A0Z = null;
            r4.A04 = null;
        } finally {
            if (r4.A0Y != null) {
                r4.A06();
                r4.A0M.A00();
            }
            if (r4.A0Z != null) {
                r4.A0Z.A01();
            }
            r4.A0Z = null;
            r4.A04 = null;
        }
    }

    public static /* synthetic */ void A03(AnonymousClass661 r5, AbstractC1311561m r6, int i) {
        C119055co r52;
        SparseArray sparseArray;
        AnonymousClass61K.A01("Should not check for open camera on the UI thread.");
        if (r5.A0Y == null || r5.A00 != i) {
            r5.A06();
            AnonymousClass60C.A00().A00 = SystemClock.elapsedRealtime();
            r5.A0Y = (Camera) r5.A0T.A03("open_camera_on_camera_handler_thread", new AnonymousClass6KP(r5, r5.A0J.A02(i)));
            C04160Kp.A00(r5.A0Y, "Camera is null.");
            r5.A00 = i;
            r5.A0Y.setErrorCallback(r5.A0F);
            C129885yS r2 = r5.A0R;
            Camera camera = r5.A0Y;
            if (camera != null) {
                int A02 = r2.A03.A02(i);
                Camera.Parameters parameters = camera.getParameters();
                if (r6 == null || !C12970iu.A1Y(r6.AAQ(AbstractC1311561m.A00))) {
                    r52 = new C119055co(parameters);
                    sparseArray = r2.A00;
                } else {
                    sparseArray = r2.A00;
                    r52 = (C119055co) sparseArray.get(A02);
                    if (r52 == null) {
                        r52 = new C119055co(parameters);
                    }
                    C119075cq r62 = new C119075cq(parameters, r52);
                    r2.A01.put(A02, r62);
                    r2.A02.put(A02, new C119105ct(parameters, camera, r52, r62, i));
                    return;
                }
                sparseArray.put(A02, r52);
                C119075cq r62 = new C119075cq(parameters, r52);
                r2.A01.put(A02, r62);
                r2.A02.put(A02, new C119105ct(parameters, camera, r52, r62, i));
                return;
            }
            throw C12980iv.A0n("camera is null!");
        }
    }

    public final int A04(int i) {
        int i2;
        int i3 = this.A00;
        int A01 = this.A0J.A01(i3);
        int A00 = C117325Zm.A00(i);
        if (i3 == 1) {
            i2 = 360 - ((A01 + A00) % 360);
        } else {
            i2 = (A01 - A00) + 360;
        }
        return i2 % 360;
    }

    public AbstractC130685zo A05() {
        if (isConnected()) {
            return this.A0R.A02(this.A00);
        }
        throw new C136076Kx("Cannot get camera settings");
    }

    public final void A06() {
        if (this.A0Y != null) {
            A09();
            this.A0U.set(false);
            this.A0V.set(false);
            Camera camera = this.A0Y;
            this.A0Y = null;
            AnonymousClass63B r2 = this.A0O;
            if (r2.A0B) {
                Handler handler = r2.A04;
                handler.removeMessages(1);
                handler.removeMessages(2);
                r2.A0A = null;
                r2.A03.setZoomChangeListener(null);
                r2.A03 = null;
                r2.A0B = false;
            }
            C129305xV r22 = this.A0K;
            r22.A06.A06("The FocusController must be released on the Optic thread.");
            r22.A09 = false;
            r22.A01 = null;
            r22.A08 = false;
            r22.A07 = false;
            this.A0f = false;
            C129885yS r0 = this.A0R;
            r0.A02.remove(r0.A03.A02(this.A00));
            this.A0T.A03("close_camera_on_camera_handler_thread", new AnonymousClass6KQ(camera, this));
        }
    }

    public final void A07() {
        if (isConnected()) {
            A5k(this.A0H);
            C128965wx r0 = this.A0L;
            Camera camera = this.A0Y;
            C129465xl r3 = r0.A00;
            ReentrantLock reentrantLock = r3.A01;
            reentrantLock.lock();
            if (camera != null) {
                try {
                    if (!r3.A00()) {
                        reentrantLock.lock();
                        boolean z = true;
                        if ((r3.A00 & 1) != 1) {
                            z = false;
                        }
                        reentrantLock.unlock();
                        if (!z) {
                            reentrantLock.lock();
                            if (r3.A01()) {
                                r3.A00 = 1;
                                reentrantLock.unlock();
                                AnonymousClass616.A00();
                                camera.startPreview();
                            } else {
                                throw C12960it.A0U("Cannot progress to STARTING, not in STOPPED state");
                            }
                        }
                    }
                } finally {
                    AnonymousClass616.A00();
                    reentrantLock.unlock();
                }
            }
        }
    }

    public final void A08() {
        try {
            AbstractC136426Mm r0 = this.A08;
            if (r0 != null) {
                r0.AeU();
                this.A08 = null;
            }
        } finally {
            A0B(null);
            this.A0e = false;
        }
    }

    public final synchronized void A09() {
        FutureTask futureTask = this.A0b;
        if (futureTask != null) {
            this.A0T.A08(futureTask);
            this.A0b = null;
        }
    }

    public final void A0A(int i, int i2) {
        Matrix matrix;
        float f;
        float f2;
        float f3;
        Matrix matrix2 = new Matrix();
        this.A03 = matrix2;
        float f4 = 1.0f;
        if (this.A00 == 1) {
            f4 = -1.0f;
        }
        matrix2.setScale(f4, 1.0f);
        int A04 = A04(this.A01);
        this.A03.postRotate((float) A04);
        if (A04 == 90 || A04 == 270) {
            matrix = this.A03;
            f = (float) i2;
            f2 = f / 2000.0f;
            f3 = (float) i;
        } else {
            matrix = this.A03;
            f = (float) i;
            f2 = f / 2000.0f;
            f3 = (float) i2;
        }
        matrix.postScale(f2, f3 / 2000.0f);
        this.A03.postTranslate(f / 2.0f, f3 / 2.0f);
    }

    public void A0B(MediaRecorder mediaRecorder) {
        Camera camera = this.A0Y;
        if (camera != null) {
            C129885yS r2 = this.A0R;
            boolean z = this.A0A;
            int i = this.A02;
            if (mediaRecorder != null) {
                camera.unlock();
                mediaRecorder.setCamera(camera);
                mediaRecorder.setVideoSource(1);
                return;
            }
            camera.lock();
            C119105ct A00 = r2.A00(this.A00);
            AbstractC125485rK.A02(AbstractC130685zo.A0A, A00, i);
            ((AbstractC125485rK) A00).A00.A01(AbstractC130685zo.A0T, Boolean.valueOf(z));
            A00.A03();
            A00.A02();
        }
    }

    @Override // X.AbstractC1311761o
    public void A5k(AbstractC136166Lg r7) {
        if (r7 != null) {
            C129935yX r5 = this.A0M;
            synchronized (r5) {
                r5.A05.A01(r7);
            }
            C1308560f r2 = this.A0T;
            boolean A09 = r2.A09();
            boolean isConnected = isConnected();
            if (A09) {
                if (isConnected) {
                    Camera camera = this.A0Y;
                    C129885yS r3 = this.A0R;
                    r5.A02(camera, (C129845yO) r3.A02(this.A00).A03(AbstractC130685zo.A0m), C12960it.A05(r3.A02(this.A00).A03(AbstractC130685zo.A0i)));
                }
            } else if (isConnected) {
                r2.A07("enable_preview_frame_listeners", new IDxCallableShape15S0100000_3_I1(this, 1));
            }
        } else {
            throw C12970iu.A0f("listener is required");
        }
    }

    @Override // X.AbstractC1311761o
    public void A5l(C128425w5 r4) {
        AbstractC1311561m r1 = this.A06;
        if (r1 == null || !C12970iu.A1Y(r1.AAQ(AbstractC1311561m.A0A))) {
            C128965wx r12 = this.A0L;
            if (r12.A00.A00()) {
                r4.A00();
            }
            r12.A01.A01(r4);
            return;
        }
        this.A0T.A07("add_on_preview_started_listener", new AnonymousClass6KR(r4, this));
    }

    @Override // X.AbstractC1311761o
    public void A7Y(AbstractC129405xf r9, C129535xs r10, AbstractC1311561m r11, AnonymousClass6LQ r12, AnonymousClass6LR r13, String str, int i, int i2) {
        AnonymousClass616.A00();
        if (this.A0C) {
            this.A09 = this.A0S.A00(this.A0T.A00, str);
        }
        this.A0T.A00(r9, "connect", new CallableC135966Km(r10, this, r11, i, i2));
    }

    @Override // X.AbstractC1311761o
    public boolean A8z(AbstractC129405xf r4) {
        AnonymousClass616.A00();
        C128435w6.A00(this);
        if (this.A0C) {
            this.A0S.A02(this.A09);
            this.A09 = null;
        }
        this.A0T.A00(r4, "disconnect", new IDxCallableShape15S0100000_3_I1(this, 0));
        return true;
    }

    @Override // X.AbstractC1311761o
    public void AA3(int i, int i2) {
        Rect rect = new Rect(i, i2, i, i2);
        int i3 = -this.A0E;
        rect.inset(i3, i3);
        this.A0T.A00(new C118965cf(this), "focus", new AnonymousClass6KT(rect, this));
    }

    @Override // X.AbstractC1311761o
    public int ABB() {
        return this.A00;
    }

    @Override // X.AbstractC1311761o
    public AbstractC130695zp ABG() {
        if (isConnected()) {
            return this.A0R.A01(this.A00);
        }
        throw new C136076Kx("Cannot get camera capabilities");
    }

    @Override // X.AbstractC1311761o
    public int AGc(int i) {
        return this.A0J.A01(i);
    }

    @Override // X.AbstractC1311761o
    public int AHu() {
        AnonymousClass63B r1 = this.A0O;
        if (r1.A0B) {
            return r1.A09;
        }
        return 0;
    }

    @Override // X.AbstractC1311761o
    public boolean AI9(int i) {
        try {
            return this.A0J.A02(i) != -1;
        } catch (RuntimeException unused) {
            return false;
        }
    }

    @Override // X.AbstractC1311761o
    public void AIr(Matrix matrix, int i, int i2, int i3) {
        C125395rB r1 = new C125395rB(matrix, i3, A04(this.A01), i, i2);
        this.A05 = r1;
        this.A0K.A03 = r1;
    }

    @Override // X.AbstractC1311761o
    public boolean AJz() {
        return this.A0e;
    }

    @Override // X.AbstractC1311761o
    public boolean AK6() {
        return AI9(0) && AI9(1);
    }

    @Override // X.AbstractC1311761o
    public boolean AKp(float[] fArr) {
        C125395rB r0 = this.A05;
        if (r0 == null) {
            return false;
        }
        r0.A00.mapPoints(fArr);
        return true;
    }

    @Override // X.AbstractC1311761o
    public void ALV(AbstractC129405xf r4, C128385w1 r5) {
        this.A0T.A00(r4, "modify_settings", new AnonymousClass6KO(this, r5));
    }

    @Override // X.AbstractC1311761o
    public void ATI(int i) {
        this.A0X = i;
        AnonymousClass66P r1 = this.A0Z;
        if (r1 != null) {
            r1.A00 = this.A0X;
        }
    }

    @Override // X.AbstractC1311761o
    public void AaL(AbstractC136166Lg r4) {
        if (r4 != null) {
            C129935yX r1 = this.A0M;
            synchronized (r1) {
                r1.A07.remove(r4);
                r1.A05.A02(r4);
            }
            if (this.A0S.A04) {
                this.A0T.A07("disable_preview_frame_listeners", new IDxCallableShape15S0100000_3_I1(this, 2));
                return;
            }
            return;
        }
        throw C12970iu.A0f("listener is required");
    }

    @Override // X.AbstractC1311761o
    public void AaM(C128425w5 r4) {
        AbstractC1311561m r1 = this.A06;
        if (r1 == null || !C12970iu.A1Y(r1.AAQ(AbstractC1311561m.A0A))) {
            this.A0L.A01.A02(r4);
        } else {
            this.A0T.A07("remove_on_preview_started_listener", new AnonymousClass6KS(r4, this));
        }
    }

    @Override // X.AbstractC1311761o
    public void Abs(Handler handler) {
        this.A0T.A00 = handler;
    }

    @Override // X.AbstractC1311761o
    public void Ac9(AbstractC136156Lf r2) {
        this.A0K.A02 = r2;
    }

    @Override // X.AbstractC1311761o
    public void AcO(C125385rA r3) {
        C130055yj r0 = this.A0S;
        synchronized (r0.A02) {
            r0.A00 = r3;
        }
    }

    @Override // X.AbstractC1311761o
    public void Acd(AbstractC129405xf r4, int i) {
        this.A0T.A00(r4, "set_rotation", new AnonymousClass6KV(this, i));
    }

    @Override // X.AbstractC1311761o
    public void AdE(AbstractC129405xf r5, int i) {
        this.A0T.A00(null, "set_zoom_level", new AnonymousClass6KU(this, i));
    }

    @Override // X.AbstractC1311761o
    public boolean AdG(Matrix matrix, int i, int i2, int i3, int i4, boolean z) {
        float f;
        matrix.reset();
        float f2 = (float) i;
        float f3 = (float) i2;
        float f4 = f2 / f3;
        int A04 = A04(this.A01);
        if (A04 == 90 || A04 == 270) {
            i4 = i3;
            i3 = i4;
        }
        float f5 = (float) i3;
        float f6 = (float) i4;
        int i5 = ((f5 / f6) > f4 ? 1 : ((f5 / f6) == f4 ? 0 : -1));
        if (!z ? i5 <= 0 : i5 > 0) {
            f = f3 / f6;
        } else {
            f = f2 / f5;
        }
        matrix.setScale((f5 / f2) * f, (f6 / f3) * f, (float) (i >> 1), (float) (i2 >> 1));
        return true;
    }

    @Override // X.AbstractC1311761o
    public void AeN(AbstractC129405xf r6, File file) {
        String absolutePath = file.getAbsolutePath();
        if (absolutePath == null) {
            throw C12970iu.A0f("Both videoPath and videoFileDescriptor cannot be null, one must contain a valid value");
        } else if (!isConnected()) {
            r6.A00(C12990iw.A0m("Can't record video before it's initialised."));
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.A0e = true;
            this.A0T.A00(new C118995ci(r6, this), "start_video", new CallableC135876Kd(this, absolutePath, elapsedRealtime));
        }
    }

    @Override // X.AbstractC1311761o
    public void AeV(AbstractC129405xf r4, boolean z) {
        if (!this.A0e) {
            r4.A00(C12990iw.A0m("Not recording video"));
            return;
        }
        this.A0T.A00(r4, "stop_video_recording", new AnonymousClass6KW(this, SystemClock.elapsedRealtime()));
    }

    @Override // X.AbstractC1311761o
    public void Aef(AbstractC129405xf r6) {
        if (((CountDownLatch) this.A0N.A00.get()).getCount() <= 0) {
            AnonymousClass616.A00();
            this.A0T.A00(r6, "switch_camera", new IDxCallableShape15S0100000_3_I1(this, 4));
        }
    }

    @Override // X.AbstractC1311761o
    public void Aeh(C129455xk r7, AnonymousClass60B r8) {
        String str;
        if (!isConnected()) {
            r7.A00(new C136076Kx("Cannot take a photo"));
            return;
        }
        C128435w6 r3 = this.A0N;
        if (((CountDownLatch) r3.A00.get()).getCount() > 0) {
            str = "Busy taking photo";
        } else if (this.A0e && !this.A0B) {
            str = "Cannot take a photo while recording video";
        } else if (r8.A00(AnonymousClass60B.A04) != null) {
            r7.A00(C12980iv.A0u("Burst capture not supported on camera1"));
            return;
        } else {
            AnonymousClass60C.A00().A03 = SystemClock.elapsedRealtime();
            C117305Zk.A1C(A05());
            r3.A01(2);
            this.A0W.set(false);
            this.A0T.A00(new C119025cl(r7, this, r8), "take_photo", new CallableC135866Kc(r7, this, r8));
            return;
        }
        r7.A00(new C124675pv(str));
    }

    @Override // X.AbstractC1311761o
    public boolean isConnected() {
        if (this.A0Y != null) {
            return this.A0U.get() || this.A0V.get();
        }
        return false;
    }
}
