package X;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: X.3MA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MA implements InputFilter {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ C57762na A01;
    public final /* synthetic */ AnonymousClass28D A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public AnonymousClass3MA(C14260l7 r1, C57762na r2, AnonymousClass28D r3, AbstractC14200l1 r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        String obj = spanned.toString();
        StringBuffer stringBuffer = new StringBuffer(obj);
        stringBuffer.replace(i3, i4, charSequence.toString());
        C14220l3 A01 = C14210l2.A01(C14210l2.A00(obj), stringBuffer.toString(), 1);
        AnonymousClass28D r0 = this.A02;
        if (C64983Hr.A02(C14250l6.A00(C14230l4.A00(this.A00, r0.A06), A01, this.A03))) {
            return null;
        }
        return spanned.subSequence(i3, i4);
    }
}
