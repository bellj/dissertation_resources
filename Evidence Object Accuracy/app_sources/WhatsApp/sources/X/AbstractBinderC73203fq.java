package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73203fq extends Binder implements IInterface {
    public AbstractBinderC73203fq() {
        attachInterface(this, "com.google.android.gms.auth.api.accounttransfer.internal.IAccountTransferCallbacks");
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        AbstractC77813nt r0;
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        AbstractBinderC78993ps r3 = (AbstractBinderC78993ps) this;
        switch (i) {
            case 1:
                C12970iu.A0F(parcel, Status.CREATOR);
                throw C12970iu.A0z();
            case 2:
                C12970iu.A0F(parcel, Status.CREATOR);
                C12970iu.A0F(parcel, C78943pn.CREATOR);
                throw C12970iu.A0z();
            case 3:
                C12970iu.A0F(parcel, Status.CREATOR);
                C12970iu.A0F(parcel, C78913pk.CREATOR);
                throw C12970iu.A0z();
            case 4:
                if (!(r3 instanceof BinderC78963pp)) {
                    throw C12970iu.A0z();
                }
                ((AbstractC77813nt) ((BinderC78963pp) r3).A00).A00.A01(null);
                return true;
            case 5:
                Status status = (Status) C12970iu.A0F(parcel, Status.CREATOR);
                if (!(r3 instanceof BinderC78963pp)) {
                    r0 = ((BinderC78973pq) r3).A00;
                } else {
                    r0 = ((BinderC78963pp) r3).A00;
                }
                C13690kA r02 = r0.A00;
                r02.A00.A07(new C77693nh(status));
                return true;
            case 6:
                byte[] createByteArray = parcel.createByteArray();
                if (!(r3 instanceof BinderC78973pq)) {
                    throw C12970iu.A0z();
                }
                ((AbstractC77813nt) ((BinderC78973pq) r3).A01).A00.A01(createByteArray);
                return true;
            case 7:
                C12970iu.A0F(parcel, C78373oo.CREATOR);
                throw C12970iu.A0z();
            default:
                return false;
        }
    }
}
