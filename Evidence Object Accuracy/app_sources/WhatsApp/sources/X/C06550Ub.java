package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* renamed from: X.0Ub  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06550Ub {
    public static final Map A00 = new HashMap();
    public static final byte[] A01 = {80, 75, 3, 4};

    public static AnonymousClass0ST A00(Context context, String str, int i) {
        try {
            AbstractC02750Dv A09 = A09(A0A(context.getResources().openRawResource(i)));
            if (A07(A09).booleanValue()) {
                return A04(str, new ZipInputStream(A09.AIz()));
            }
            return A03(A09.AIz(), str);
        } catch (Resources.NotFoundException e) {
            return new AnonymousClass0ST((Throwable) e);
        }
    }

    public static AnonymousClass0ST A01(Context context, String str, String str2) {
        try {
            if (str.endsWith(".zip") || str.endsWith(".lottie")) {
                return A04(str2, new ZipInputStream(context.getAssets().open(str)));
            }
            return A03(context.getAssets().open(str), str2);
        } catch (IOException e) {
            return new AnonymousClass0ST((Throwable) e);
        }
    }

    public static AnonymousClass0ST A02(AbstractC08850bx r23, String str, boolean z) {
        AnonymousClass0ST r1;
        try {
            try {
                float A002 = AnonymousClass0UV.A00();
                AnonymousClass036 r22 = new AnonymousClass036();
                ArrayList arrayList = new ArrayList();
                HashMap hashMap = new HashMap();
                HashMap hashMap2 = new HashMap();
                HashMap hashMap3 = new HashMap();
                ArrayList arrayList2 = new ArrayList();
                C013506i r16 = new C013506i();
                C05540Py r4 = new C05540Py();
                r23.A0A();
                int i = 0;
                float f = 0.0f;
                float f2 = 0.0f;
                float f3 = 0.0f;
                int i2 = 0;
                while (r23.A0H()) {
                    switch (r23.A04(AnonymousClass0ML.A03)) {
                        case 0:
                            i = r23.A03();
                            continue;
                        case 1:
                            i2 = r23.A03();
                            continue;
                        case 2:
                            f = (float) r23.A02();
                            continue;
                        case 3:
                            f2 = ((float) r23.A02()) - 0.01f;
                            continue;
                        case 4:
                            f3 = (float) r23.A02();
                            continue;
                        case 5:
                            String[] split = r23.A08().split("\\.");
                            int parseInt = Integer.parseInt(split[0]);
                            int parseInt2 = Integer.parseInt(split[1]);
                            int parseInt3 = Integer.parseInt(split[2]);
                            if (parseInt >= 4) {
                                if (parseInt > 4) {
                                    continue;
                                } else if (parseInt2 >= 4) {
                                    if (parseInt2 <= 4 && parseInt3 < 0) {
                                    }
                                }
                            }
                            AnonymousClass0R5.A00("Lottie only supports bodymovin >= 4.4.0");
                            r4.A0E.add("Lottie only supports bodymovin >= 4.4.0");
                            break;
                        case 6:
                            r23.A09();
                            int i3 = 0;
                            while (r23.A0H()) {
                                AnonymousClass0PU A003 = AnonymousClass0RQ.A00(r4, r23);
                                if (A003.A0E == AnonymousClass0JI.IMAGE) {
                                    i3++;
                                }
                                arrayList.add(A003);
                                r22.A09(A003.A07, A003);
                                if (i3 > 4) {
                                    StringBuilder sb = new StringBuilder("You have ");
                                    sb.append(i3);
                                    sb.append(" images. Lottie should primarily be used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers to shape layers.");
                                    AnonymousClass0R5.A00(sb.toString());
                                }
                            }
                            break;
                        case 7:
                            r23.A09();
                            while (r23.A0H()) {
                                ArrayList arrayList3 = new ArrayList();
                                AnonymousClass036 r13 = new AnonymousClass036();
                                r23.A0A();
                                String str2 = null;
                                String str3 = null;
                                int i4 = 0;
                                int i5 = 0;
                                while (r23.A0H()) {
                                    int A04 = r23.A04(AnonymousClass0ML.A00);
                                    if (A04 == 0) {
                                        str2 = r23.A08();
                                    } else if (A04 == 1) {
                                        r23.A09();
                                        while (r23.A0H()) {
                                            AnonymousClass0PU A004 = AnonymousClass0RQ.A00(r4, r23);
                                            r13.A09(A004.A07, A004);
                                            arrayList3.add(A004);
                                        }
                                        r23.A0B();
                                    } else if (A04 == 2) {
                                        i4 = r23.A03();
                                    } else if (A04 == 3) {
                                        i5 = r23.A03();
                                    } else if (A04 == 4) {
                                        str3 = r23.A08();
                                    } else if (A04 != 5) {
                                        r23.A0D();
                                        r23.A0E();
                                    } else {
                                        r23.A08();
                                    }
                                }
                                r23.A0C();
                                if (str3 != null) {
                                    C04970Nt r8 = new C04970Nt(str2, str3, i4, i5);
                                    hashMap2.put(r8.A04, r8);
                                } else {
                                    hashMap.put(str2, arrayList3);
                                }
                            }
                            break;
                        case 8:
                            r23.A0A();
                            while (r23.A0H()) {
                                if (r23.A04(AnonymousClass0ML.A01) != 0) {
                                    r23.A0D();
                                    r23.A0E();
                                } else {
                                    r23.A09();
                                    while (r23.A0H()) {
                                        r23.A0A();
                                        String str4 = null;
                                        String str5 = null;
                                        String str6 = null;
                                        while (r23.A0H()) {
                                            int A042 = r23.A04(C04360Lk.A00);
                                            if (A042 == 0) {
                                                str4 = r23.A08();
                                            } else if (A042 == 1) {
                                                str5 = r23.A08();
                                            } else if (A042 == 2) {
                                                str6 = r23.A08();
                                            } else if (A042 != 3) {
                                                r23.A0D();
                                                r23.A0E();
                                            } else {
                                                r23.A02();
                                            }
                                        }
                                        r23.A0C();
                                        AnonymousClass0NP r82 = new AnonymousClass0NP(str4, str5, str6);
                                        hashMap3.put(r82.A01, r82);
                                    }
                                    r23.A0B();
                                }
                            }
                            r23.A0C();
                            continue;
                        case 9:
                            r23.A09();
                            while (r23.A0H()) {
                                ArrayList arrayList4 = new ArrayList();
                                r23.A0A();
                                String str7 = null;
                                String str8 = null;
                                double d = 0.0d;
                                char c = 0;
                                while (r23.A0H()) {
                                    int A043 = r23.A04(AnonymousClass0ME.A01);
                                    if (A043 == 0) {
                                        c = r23.A08().charAt(0);
                                    } else if (A043 == 1) {
                                        r23.A02();
                                    } else if (A043 == 2) {
                                        d = r23.A02();
                                    } else if (A043 == 3) {
                                        str7 = r23.A08();
                                    } else if (A043 == 4) {
                                        str8 = r23.A08();
                                    } else if (A043 != 5) {
                                        r23.A0D();
                                        r23.A0E();
                                    } else {
                                        r23.A0A();
                                        while (r23.A0H()) {
                                            if (r23.A04(AnonymousClass0ME.A00) != 0) {
                                                r23.A0D();
                                                r23.A0E();
                                            } else {
                                                r23.A09();
                                                while (r23.A0H()) {
                                                    arrayList4.add(AnonymousClass0TM.A01(r4, r23));
                                                }
                                                r23.A0B();
                                            }
                                        }
                                        r23.A0C();
                                    }
                                }
                                r23.A0C();
                                C05090Of r83 = new C05090Of(str7, str8, arrayList4, c, d);
                                r16.A03(r83.hashCode(), r83);
                            }
                            break;
                        case 10:
                            r23.A09();
                            while (r23.A0H()) {
                                String str9 = null;
                                r23.A0A();
                                float f4 = 0.0f;
                                float f5 = 0.0f;
                                while (r23.A0H()) {
                                    int A044 = r23.A04(AnonymousClass0ML.A02);
                                    if (A044 == 0) {
                                        str9 = r23.A08();
                                    } else if (A044 == 1) {
                                        f4 = (float) r23.A02();
                                    } else if (A044 != 2) {
                                        r23.A0D();
                                        r23.A0E();
                                    } else {
                                        f5 = (float) r23.A02();
                                    }
                                }
                                r23.A0C();
                                arrayList2.add(new AnonymousClass0NQ(str9, f4, f5));
                            }
                            break;
                        default:
                            r23.A0D();
                            r23.A0E();
                            continue;
                    }
                    r23.A0B();
                }
                r4.A04 = new Rect(0, 0, (int) (((float) i) * A002), (int) (((float) i2) * A002));
                r4.A02 = f;
                r4.A00 = f2;
                r4.A01 = f3;
                r4.A07 = arrayList;
                r4.A05 = r22;
                r4.A0B = hashMap;
                r4.A0A = hashMap2;
                r4.A06 = r16;
                r4.A09 = hashMap3;
                r4.A08 = arrayList2;
                if (str != null) {
                    C05800Ra.A01.A00.A08(str, r4);
                }
                r1 = new AnonymousClass0ST(r4);
            } catch (Exception e) {
                r1 = new AnonymousClass0ST((Throwable) e);
            }
            return r1;
        } finally {
            if (z) {
                AnonymousClass0UV.A05(r23);
            }
        }
    }

    public static AnonymousClass0ST A03(InputStream inputStream, String str) {
        try {
            return A02(AbstractC08850bx.A01(A09(A0A(inputStream))), str, true);
        } finally {
            AnonymousClass0UV.A05(inputStream);
        }
    }

    public static AnonymousClass0ST A04(String str, ZipInputStream zipInputStream) {
        try {
            return A05(str, zipInputStream);
        } finally {
            AnonymousClass0UV.A05(zipInputStream);
        }
    }

    public static AnonymousClass0ST A05(String str, ZipInputStream zipInputStream) {
        HashMap hashMap = new HashMap();
        try {
            ZipEntry nextEntry = zipInputStream.getNextEntry();
            C05540Py r3 = null;
            while (nextEntry != null) {
                String name = nextEntry.getName();
                if (!name.contains("__MACOSX") && !nextEntry.getName().equalsIgnoreCase("manifest.json")) {
                    if (nextEntry.getName().contains(".json")) {
                        r3 = (C05540Py) A02(AbstractC08850bx.A01(A09(A0A(zipInputStream))), null, false).A00;
                    } else if (name.contains(".png") || name.contains(".webp") || name.contains(".jpg") || name.contains(".jpeg")) {
                        String[] split = name.split("/");
                        hashMap.put(split[split.length - 1], BitmapFactory.decodeStream(zipInputStream));
                    }
                    nextEntry = zipInputStream.getNextEntry();
                }
                zipInputStream.closeEntry();
                nextEntry = zipInputStream.getNextEntry();
            }
            if (r3 == null) {
                return new AnonymousClass0ST((Throwable) new IllegalArgumentException("Unable to parse composition"));
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                Object key = entry.getKey();
                Iterator it = r3.A0A.values().iterator();
                while (true) {
                    if (it.hasNext()) {
                        C04970Nt r5 = (C04970Nt) it.next();
                        if (r5.A03.equals(key)) {
                            Bitmap bitmap = (Bitmap) entry.getValue();
                            int i = r5.A02;
                            int i2 = r5.A01;
                            if (!(bitmap.getWidth() == i && bitmap.getHeight() == i2)) {
                                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, i, i2, true);
                                bitmap.recycle();
                                bitmap = createScaledBitmap;
                            }
                            r5.A00 = bitmap;
                        }
                    }
                }
            }
            for (Map.Entry entry2 : r3.A0A.entrySet()) {
                if (((C04970Nt) entry2.getValue()).A00 == null) {
                    StringBuilder sb = new StringBuilder("There is no image for ");
                    sb.append(((C04970Nt) entry2.getValue()).A03);
                    return new AnonymousClass0ST((Throwable) new IllegalStateException(sb.toString()));
                }
            }
            if (str != null) {
                C05800Ra.A01.A00.A08(str, r3);
            }
            return new AnonymousClass0ST(r3);
        } catch (IOException e) {
            return new AnonymousClass0ST((Throwable) e);
        }
    }

    public static C06120Sg A06(String str, Callable callable) {
        if (str != null) {
            C05540Py r0 = (C05540Py) C05800Ra.A01.A00.A04(str);
            if (r0 != null) {
                return new C06120Sg(new CallableC10440ed(r0), false);
            }
            Map map = A00;
            if (map.containsKey(str)) {
                return (C06120Sg) map.get(str);
            }
        }
        C06120Sg r1 = new C06120Sg(callable, false);
        if (str != null) {
            r1.A01(new C07930aJ(str));
            r1.A00(new C07940aK(str));
            A00.put(str, r1);
        }
        return r1;
    }

    public static Boolean A07(AbstractC02750Dv r5) {
        try {
            AbstractC02750Dv AZ1 = r5.AZ1();
            for (byte b : A01) {
                if (AZ1.readByte() != b) {
                    return Boolean.FALSE;
                }
            }
            AZ1.close();
            return Boolean.TRUE;
        } catch (Exception unused) {
            return Boolean.FALSE;
        }
    }

    public static String A08(Context context, int i) {
        StringBuilder sb = new StringBuilder("rawRes");
        sb.append((context.getResources().getConfiguration().uiMode & 48) == 32 ? "_night_" : "_day_");
        sb.append(i);
        return sb.toString();
    }

    public static final AbstractC02750Dv A09(AbstractC117195Yx r1) {
        return new AnonymousClass5FW(r1);
    }

    public static final AbstractC117195Yx A0A(InputStream inputStream) {
        C16700pc.A0D(inputStream, 0);
        return new AnonymousClass5FY(inputStream, new C92014Uc());
    }
}
