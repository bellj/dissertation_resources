package X;

import android.widget.AbsListView;
import com.whatsapp.contact.picker.BidiContactListView;

/* renamed from: X.4op  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102244op implements AbsListView.OnScrollListener {
    public int A00 = 0;
    public final /* synthetic */ BidiContactListView A01;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public C102244op(BidiContactListView bidiContactListView) {
        this.A01 = bidiContactListView;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        int i2 = this.A00;
        if (i2 == 0 && i != i2) {
            this.A01.A01.A01(absListView);
        }
        this.A00 = i;
    }
}
