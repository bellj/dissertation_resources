package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.util.Log;

/* renamed from: X.3ZG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZG implements AbstractC21730xt {
    public final /* synthetic */ C452420s A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ byte[] A02;

    public AnonymousClass3ZG(C452420s r1, String str, byte[] bArr) {
        this.A00 = r1;
        this.A02 = bArr;
        this.A01 = str;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("BackupTokenProtocolHelper/sendBackupTokenRequest/delivery-failure");
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.e("BackupTokenProtocolHelper/sendBackupTokenRequest/error");
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        Log.i("BackupTokenProtocolHelper/sendBackupTokenRequest/success");
        this.A00.A04.Ab2(new RunnableBRunnable0Shape1S1200000_I1(this.A02, this, this.A01, 16));
    }
}
