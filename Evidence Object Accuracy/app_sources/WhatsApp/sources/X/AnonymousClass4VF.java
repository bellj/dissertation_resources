package X;

import java.util.List;

/* renamed from: X.4VF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4VF {
    public final List A00;
    public final AnonymousClass5X6[] A01;

    public AnonymousClass4VF(List list) {
        this.A00 = list;
        this.A01 = new AnonymousClass5X6[list.size()];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002b, code lost:
        if ("application/cea-708".equals(r6) != false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC14070ko r8, X.C92824Xo r9) {
        /*
            r7 = this;
            r4 = 0
        L_0x0001:
            X.5X6[] r3 = r7.A01
            int r0 = r3.length
            if (r4 >= r0) goto L_0x006b
            r9.A03()
            int r1 = r9.A01()
            r0 = 3
            X.5X6 r2 = r8.Af4(r1, r0)
            java.util.List r0 = r7.A00
            java.lang.Object r5 = r0.get(r4)
            X.4mC r5 = (X.C100614mC) r5
            java.lang.String r6 = r5.A0T
            java.lang.String r0 = "application/cea-608"
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x002d
            java.lang.String r0 = "application/cea-708"
            boolean r0 = r0.equals(r6)
            r1 = 0
            if (r0 == 0) goto L_0x002e
        L_0x002d:
            r1 = 1
        L_0x002e:
            java.lang.String r0 = "Invalid closed caption mime type provided: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r6, r0)
            if (r1 == 0) goto L_0x0062
            java.lang.String r0 = r5.A0Q
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = r9.A02()
        L_0x0042:
            X.4ap r1 = X.C93844ap.A00()
            r1.A0O = r0
            r1.A0R = r6
            int r0 = r5.A0G
            r1.A0E = r0
            java.lang.String r0 = r5.A0S
            r1.A0Q = r0
            int r0 = r5.A03
            r1.A02 = r0
            java.util.List r0 = r5.A0U
            r1.A0S = r0
            X.C72453ed.A17(r1, r2)
            r3[r4] = r2
            int r4 = r4 + 1
            goto L_0x0001
        L_0x0062:
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x006b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4VF.A00(X.0ko, X.4Xo):void");
    }
}
