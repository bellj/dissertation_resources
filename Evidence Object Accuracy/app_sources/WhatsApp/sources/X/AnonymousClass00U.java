package X;

import android.content.Context;

/* renamed from: X.00U  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00U {
    public static Context A00(Context context) {
        return context.createDeviceProtectedStorageContext();
    }

    public static boolean A01(Context context) {
        return context.isDeviceProtectedStorage();
    }
}
