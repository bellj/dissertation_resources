package X;

import android.content.Context;
import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.requirements.Requirement;

/* renamed from: X.1LH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1LH {
    public void A00(Context context, Job job) {
        if (job instanceof AnonymousClass1LJ) {
            ((AnonymousClass1LJ) job).Abz(context);
        }
        for (Requirement requirement : job.parameters.requirements) {
            if (requirement instanceof AnonymousClass1LJ) {
                ((AnonymousClass1LJ) requirement).Abz(context);
            }
        }
    }
}
