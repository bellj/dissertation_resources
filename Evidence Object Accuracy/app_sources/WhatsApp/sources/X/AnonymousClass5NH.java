package X;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Enumeration;

/* renamed from: X.5NH  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NH extends AnonymousClass1TL implements AbstractC117265Ze {
    public byte[] A00;

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return new AnonymousClass5N5(this.A00);
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return this;
    }

    @Override // X.AbstractC117265Ze
    public InputStream AEi() {
        return new ByteArrayInputStream(this.A00);
    }

    public AnonymousClass5NH(byte[] bArr) {
        if (bArr != null) {
            this.A00 = bArr;
            return;
        }
        throw C12980iv.A0n("'string' cannot be null");
    }

    public static AnonymousClass1TO A00(StringBuffer stringBuffer, AnonymousClass5NH r3, C114715Mu r4) {
        AnonymousClass1TO r1 = new AnonymousClass1TO(r3.A00);
        stringBuffer.append("                       critical(");
        stringBuffer.append(r4.A02);
        stringBuffer.append(") ");
        return r1;
    }

    public static AnonymousClass5NH A01(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NH)) {
            return (AnonymousClass5NH) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return A01(AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("failed to construct OCTET STRING from byte[]: ")));
            }
        } else {
            if (obj instanceof AnonymousClass1TN) {
                AnonymousClass1TL Aer = ((AnonymousClass1TN) obj).Aer();
                if (Aer instanceof AnonymousClass5NH) {
                    return (AnonymousClass5NH) Aer;
                }
            }
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    public static AnonymousClass5NH A04(AnonymousClass5NU r6, boolean z) {
        AnonymousClass1TL r0;
        AnonymousClass5N6 r02;
        if (!z) {
            AnonymousClass1TL A00 = AnonymousClass5NU.A00(r6);
            if (r6.A02) {
                boolean z2 = r6 instanceof C114815Ne;
                AnonymousClass5NH[] r1 = {A01(A00)};
                if (z2) {
                    return new AnonymousClass5N6(r1);
                }
                r02 = new AnonymousClass5N6(r1);
            } else if (A00 instanceof AnonymousClass5NH) {
                AnonymousClass5NH r5 = (AnonymousClass5NH) A00;
                if (r6 instanceof C114815Ne) {
                    return r5;
                }
                r0 = r5.A07();
                return (AnonymousClass5NH) r0;
            } else if (A00 instanceof AbstractC114775Na) {
                AbstractC114775Na r52 = (AbstractC114775Na) A00;
                boolean z3 = r6 instanceof C114815Ne;
                int A0B = r52.A0B();
                AnonymousClass5NH[] r2 = new AnonymousClass5NH[A0B];
                for (int i = 0; i < A0B; i++) {
                    r2[i] = A01(r52.A0D(i));
                }
                r02 = new AnonymousClass5N6(r2);
                if (z3) {
                    return r02;
                }
            } else {
                throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(r6), C12960it.A0k("unknown object in getInstance: ")));
            }
            r0 = r02.A07();
            return (AnonymousClass5NH) r0;
        } else if (r6.A02) {
            return A01(AnonymousClass5NU.A00(r6));
        } else {
            throw C12970iu.A0f("object implicit - explicit expected.");
        }
    }

    public static byte[] A05(Object obj) {
        return A01(obj).A00;
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A07() {
        return !(this instanceof AnonymousClass5N5) ? new AnonymousClass5N5(this.A00) : this;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r5, boolean z) {
        if (!(this instanceof AnonymousClass5N5)) {
            Enumeration A0B = ((AnonymousClass5N6) this).A0B();
            if (z) {
                r5.A00.write(36);
            }
            OutputStream outputStream = r5.A00;
            outputStream.write(128);
            while (A0B.hasMoreElements()) {
                r5.A04(((AnonymousClass1TN) A0B.nextElement()).Aer(), true);
            }
            outputStream.write(0);
            outputStream.write(0);
            return;
        }
        r5.A06(this.A00, 4, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NH)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NH) r3).A00);
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("#");
        byte[] bArr = this.A00;
        return C12960it.A0d(C72463ee.A0I(bArr, bArr.length), A0k);
    }
}
