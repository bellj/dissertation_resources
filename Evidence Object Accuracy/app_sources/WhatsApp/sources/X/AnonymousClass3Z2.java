package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.3Z2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z2 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1ON A00;
    public final /* synthetic */ C244315m A01;

    public AnonymousClass3Z2(AnonymousClass1ON r1, C244315m r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendInitRegIq/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r10, String str) {
        String str2;
        AnonymousClass1ON r5 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initRegOnSuccess id=")));
        byte[] A01 = C244315m.A01(r10, r5, "ok_pub");
        byte[] A012 = C244315m.A01(r10, r5, "ok_key_signature");
        byte[] A013 = C244315m.A01(r10, r5, "hk_pub");
        byte[] A014 = C244315m.A01(r10, r5, "hk_key_signature");
        byte[] A015 = C244315m.A01(r10, r5, "ed_pub");
        byte[] A016 = C244315m.A01(r10, r5, "ed_key_signature");
        if (A01 != null && A012 != null && A013 != null && A014 != null && A015 != null && A016 != null) {
            if (!C16550pE.A02(A013, A014)) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initRegOnSuccess/hk_pub cannot be verified with hk_key_signature id=")));
                str2 = "hk_pub cannot be verified with hk_key_signature";
            } else if (!C16550pE.A02(A01, A012)) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initRegOnSuccess/ok_pub cannot be verified with ok_key_signature id=")));
                str2 = "ok_pub cannot be verified with ok_key_signature";
            } else if (!C16550pE.A02(A015, A016)) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initRegOnSuccess/ed_pub cannot be verified with ed_key_signature id=")));
                str2 = "ed_pub cannot be verified with ed_key_signature";
            } else {
                ((AnonymousClass1OF) r5).A00.A01();
                AnonymousClass1OO r7 = new AnonymousClass1OO((NativeHolder) JniBridge.jvidispatchOOO(5, r5.A0D, A01));
                C89664Kv r0 = new C89664Kv((NativeHolder) JniBridge.jvidispatchOO(26, r7.A00));
                JniBridge.getInstance();
                NativeHolder nativeHolder = r0.A00;
                if (((int) JniBridge.jvidispatchIIO(1, (long) 80, nativeHolder)) != 0) {
                    EncBackupViewModel.A00(r5.A08.A00, 4);
                    return;
                }
                JniBridge.getInstance();
                byte[] bArr = (byte[]) JniBridge.jvidispatchOIO(0, (long) 81, nativeHolder);
                synchronized (r5.A0C) {
                    r5.A01 = r7;
                    r5.A05 = A013;
                    r5.A03 = A015;
                    r5.A06 = bArr;
                    r5.A00 = 1;
                }
                r5.A00();
                return;
            }
            r5.APr(str2, 2);
        }
    }
}
