package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1DN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DN {
    public final C17200qQ A00;
    public final C14330lG A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C22670zS A04;
    public final C15820nx A05;
    public final C22730zY A06;
    public final C15550nR A07;
    public final C18640sm A08;
    public final AnonymousClass01d A09;
    public final C16590pI A0A;
    public final C15890o4 A0B;
    public final C14820m6 A0C;
    public final AnonymousClass018 A0D;
    public final C14950mJ A0E;
    public final C241714m A0F;
    public final C20040v7 A0G;
    public final C20820wN A0H;
    public final C16490p7 A0I;
    public final AnonymousClass150 A0J;
    public final AnonymousClass197 A0K;
    public final C14850m9 A0L;
    public final AnonymousClass1CC A0M;
    public final C22050yP A0N;
    public final C16120oU A0O;
    public final C22710zW A0P;
    public final C20420vj A0Q;
    public final C15510nN A0R;
    public final C22720zX A0S;
    public final C240514a A0T;
    public final C235512c A0U;

    public AnonymousClass1DN(C17200qQ r2, C14330lG r3, C15570nT r4, C15450nH r5, C22670zS r6, C15820nx r7, C22730zY r8, C15550nR r9, C18640sm r10, AnonymousClass01d r11, C16590pI r12, C15890o4 r13, C14820m6 r14, AnonymousClass018 r15, C14950mJ r16, C241714m r17, C20040v7 r18, C20820wN r19, C16490p7 r20, AnonymousClass150 r21, AnonymousClass197 r22, C14850m9 r23, AnonymousClass1CC r24, C22050yP r25, C16120oU r26, C22710zW r27, C20420vj r28, C15510nN r29, C22720zX r30, C240514a r31, C235512c r32) {
        this.A0L = r23;
        this.A02 = r4;
        this.A0A = r12;
        this.A01 = r3;
        this.A0O = r26;
        this.A03 = r5;
        this.A00 = r2;
        this.A0E = r16;
        this.A0G = r18;
        this.A04 = r6;
        this.A07 = r9;
        this.A0F = r17;
        this.A09 = r11;
        this.A0D = r15;
        this.A0Q = r28;
        this.A05 = r7;
        this.A0N = r25;
        this.A0U = r32;
        this.A0T = r31;
        this.A0I = r20;
        this.A0B = r13;
        this.A0C = r14;
        this.A0P = r27;
        this.A0S = r30;
        this.A0R = r29;
        this.A0H = r19;
        this.A0M = r24;
        this.A08 = r10;
        this.A0K = r22;
        this.A06 = r8;
        this.A0J = r21;
    }

    public static long[] A00(File file) {
        long j;
        long j2;
        long[] jArr = {0, 0};
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    jArr[0] = jArr[0] + file2.length();
                    j = jArr[1];
                    j2 = 1;
                } else if (file2.isDirectory()) {
                    long[] A00 = A00(file2);
                    jArr[0] = jArr[0] + A00[0];
                    j = jArr[1];
                    j2 = A00[1];
                }
                jArr[1] = j + j2;
            }
        } else {
            StringBuilder sb = new StringBuilder("mediafoldersize listedFiles is null for folder ");
            sb.append(file);
            Log.w(sb.toString());
        }
        return jArr;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:369:0x047f */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:390:0x0412 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v15, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v17, resolved type: X.1oY */
    /* JADX DEBUG: Multi-variable search result rejected for r13v2, resolved type: X.0mJ */
    /* JADX DEBUG: Multi-variable search result rejected for r14v5, resolved type: X.12c */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v0, types: [X.0mJ] */
    /* JADX WARN: Type inference failed for: r1v16, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r13v1 */
    /* JADX WARN: Type inference failed for: r1v18, types: [X.1oY] */
    /* JADX WARN: Type inference failed for: r13v4, types: [X.0zX] */
    /* JADX WARN: Type inference failed for: r1v59 */
    /* JADX WARN: Type inference failed for: r1v74 */
    /* JADX WARN: Type inference failed for: r1v75 */
    /* JADX WARN: Type inference failed for: r1v76 */
    /* JADX WARN: Type inference failed for: r1v77 */
    /* JADX WARN: Type inference failed for: r1v78 */
    /* JADX WARN: Type inference failed for: r1v79 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A01() {
        /*
        // Method dump skipped, instructions count: 2660
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1DN.A01():void");
    }
}
