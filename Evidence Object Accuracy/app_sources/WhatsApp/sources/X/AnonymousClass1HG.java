package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.1HG  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1HG {
    public final int A00;
    public final AnonymousClass1HE A01;
    public final Set A02 = new HashSet();
    public final Executor A03;

    public AnonymousClass1HG(AnonymousClass1HE r2, Executor executor, int i) {
        this.A00 = i;
        this.A03 = executor;
        this.A01 = r2;
    }

    public final void A00(String str, HashMap hashMap) {
        String str2;
        LinkedList linkedList;
        HashMap hashMap2 = hashMap;
        if (hashMap == null) {
            hashMap2 = new HashMap(1);
        }
        int i = this.A00;
        if (i != 1) {
            str2 = i != 2 ? i != 3 ? i != 4 ? "UNKNOWN_ANOMALY" : "DELAYED_JOB" : "FREQUENT_RUNNING_JOB" : "LONG_RUNNING_JOB";
        } else {
            str2 = "BLOCKED_QUEUE";
        }
        hashMap2.put("anomalyName", str2);
        synchronized (this) {
            linkedList = new LinkedList(this.A02);
        }
        this.A03.execute(new RunnableBRunnable0Shape0S1300000_I0(this, linkedList, hashMap2, str, 8));
    }
}
