package X;

/* renamed from: X.45K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45K extends AbstractC92674Wx {
    public final /* synthetic */ AnonymousClass33M A00;

    public AnonymousClass45K(AnonymousClass33M r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC92674Wx
    public void A01() {
        AnonymousClass33M r1 = this.A00;
        r1.A0A = Boolean.valueOf(!r1.A0A.booleanValue());
        super.A01();
    }
}
