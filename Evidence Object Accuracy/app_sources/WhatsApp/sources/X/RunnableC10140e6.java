package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0e6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10140e6 implements Runnable {
    public final AnonymousClass040 A00 = AnonymousClass040.A00();
    public final /* synthetic */ AnonymousClass022 A01;
    public final /* synthetic */ String A02 = "com.whatsapp.backup.google.google-backup-worker";

    public RunnableC10140e6(AnonymousClass022 r2) {
        this.A01 = r2;
    }

    public AbstractFutureC44231yX A00() {
        return this.A00;
    }

    @Override // java.lang.Runnable
    public void run() {
        ArrayList arrayList;
        ArrayList arrayList2;
        try {
            AbstractC12700iM A0B = this.A01.A04.A0B();
            String str = this.A02;
            C07740a0 r2 = (C07740a0) A0B;
            AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
            if (str == null) {
                A00.A6T(1);
            } else {
                A00.A6U(1, str);
            }
            AnonymousClass0QN r11 = r2.A01;
            r11.A02();
            r11.A03();
            Cursor A002 = AnonymousClass0LC.A00(r11, A00, true);
            try {
                int A003 = AnonymousClass0LB.A00(A002, "id");
                int A004 = AnonymousClass0LB.A00(A002, "state");
                int A005 = AnonymousClass0LB.A00(A002, "output");
                int A006 = AnonymousClass0LB.A00(A002, "run_attempt_count");
                AnonymousClass00N r6 = new AnonymousClass00N();
                AnonymousClass00N r5 = new AnonymousClass00N();
                while (A002.moveToNext()) {
                    if (!A002.isNull(A003)) {
                        String string = A002.getString(A003);
                        if (((ArrayList) r6.get(string)) == null) {
                            r6.put(string, new ArrayList());
                        }
                    }
                    if (!A002.isNull(A003)) {
                        String string2 = A002.getString(A003);
                        if (((ArrayList) r5.get(string2)) == null) {
                            r5.put(string2, new ArrayList());
                        }
                    }
                }
                A002.moveToPosition(-1);
                r2.A01(r6);
                r2.A00(r5);
                ArrayList arrayList3 = new ArrayList(A002.getCount());
                while (A002.moveToNext()) {
                    if (A002.isNull(A003) || (arrayList = (ArrayList) r6.get(A002.getString(A003))) == null) {
                        arrayList = new ArrayList();
                    }
                    if (A002.isNull(A003) || (arrayList2 = (ArrayList) r5.get(A002.getString(A003))) == null) {
                        arrayList2 = new ArrayList();
                    }
                    AnonymousClass0PO r1 = new AnonymousClass0PO();
                    r1.A03 = A002.getString(A003);
                    r1.A02 = AnonymousClass0UK.A05(A002.getInt(A004));
                    r1.A01 = C006503b.A00(A002.getBlob(A005));
                    r1.A00 = A002.getInt(A006);
                    r1.A05 = arrayList;
                    r1.A04 = arrayList2;
                    arrayList3.add(r1);
                }
                r11.A05();
                r11.A04();
                this.A00.A09((List) C004401z.A0I.apply(arrayList3));
            } finally {
                A002.close();
                A00.A01();
            }
        } catch (Throwable th) {
            this.A00.A0A(th);
        }
    }
}
