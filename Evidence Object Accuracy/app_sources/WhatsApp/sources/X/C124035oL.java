package X;

import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiQuickBuyActivity;
import java.util.List;

/* renamed from: X.5oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124035oL extends AbstractC16350or {
    public final /* synthetic */ AnonymousClass68A A00;

    public C124035oL(AnonymousClass68A r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return C117295Zj.A0Z(((AbstractActivityC121685jC) this.A00.A00).A0P);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        List list = (List) obj;
        if (list != null && list.size() > 0) {
            ((AbstractActivityC121525iS) this.A00.A00).A0B = C241414j.A01(list);
        }
        AnonymousClass68A r0 = this.A00;
        C128335vw r3 = r0.A01;
        String str = r3.A0F;
        IndiaUpiQuickBuyActivity indiaUpiQuickBuyActivity = r0.A00;
        if (str == null) {
            indiaUpiQuickBuyActivity.A3b(r3.A04);
        } else {
            ((IndiaUpiCheckOrderDetailsActivity) indiaUpiQuickBuyActivity).A06.A00(new Runnable(r3) { // from class: X.6IF
                public final /* synthetic */ C128335vw A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C124035oL.this.A00.A00.A3b(this.A01.A04);
                }
            }, str);
        }
    }
}
