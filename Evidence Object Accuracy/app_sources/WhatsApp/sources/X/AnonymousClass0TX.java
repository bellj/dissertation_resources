package X;

import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import java.io.UnsupportedEncodingException;

/* renamed from: X.0TX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TX {
    public static final boolean A00;
    public static final byte[] A01 = A01("WEBP");
    public static final byte[] A02 = A01("RIFF");
    public static final byte[] A03 = A01("VP8L");
    public static final byte[] A04 = A01("VP8X");
    public static final byte[] A05 = A01("VP8 ");

    static {
        int i = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i <= 17) {
            z = true;
        }
        A00 = z;
        if (i >= 17 && i == 17) {
            byte[] decode = Base64.decode("UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==", 0);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
        }
    }

    public static boolean A00(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        int length;
        int length2;
        if (i2 >= 20 && (bArr2 = A02) != null && bArr != null && (length = bArr2.length) + i <= (length2 = bArr.length)) {
            int i3 = 0;
            while (true) {
                if (i3 < length) {
                    if (bArr[i3 + i] != bArr2[i3]) {
                        break;
                    }
                    i3++;
                } else {
                    int i4 = i + 8;
                    byte[] bArr3 = A01;
                    if (bArr3 != null) {
                        int length3 = bArr3.length;
                        if (length3 + i4 <= length2) {
                            for (int i5 = 0; i5 < length3; i5++) {
                                if (bArr[i5 + i4] == bArr3[i5]) {
                                }
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static byte[] A01(String str) {
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("ASCII not found!", e);
        }
    }
}
