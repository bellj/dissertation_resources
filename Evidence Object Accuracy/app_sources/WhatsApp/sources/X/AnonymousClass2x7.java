package X;

/* renamed from: X.2x7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x7 extends AnonymousClass2x8 {
    public final /* synthetic */ C90784Pf A00;
    public final /* synthetic */ C623336s A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2x7(C90784Pf r1, C623336s r2, C15610nY r3, AnonymousClass018 r4) {
        super(r3, r4);
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2x8, X.C71333cl
    public int A00(C15370n3 r4, C15370n3 r5) {
        C90784Pf r2 = this.A00;
        boolean A07 = C15370n3.A07(r4, r2.A02);
        if (A07 == C15370n3.A07(r5, r2.A02)) {
            return super.A00(r4, r5);
        }
        return A07 ? -1 : 1;
    }
}
