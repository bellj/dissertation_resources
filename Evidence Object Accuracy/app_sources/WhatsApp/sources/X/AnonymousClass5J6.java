package X;

import android.graphics.Paint;

/* renamed from: X.5J6  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5J6 extends AnonymousClass1WI implements AnonymousClass1WK {
    public AnonymousClass5J6() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        return new Paint(1);
    }
}
