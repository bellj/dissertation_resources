package X;

import java.util.Enumeration;

/* renamed from: X.5MT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MT extends AnonymousClass1TM {
    public AnonymousClass5NG A00;
    public AnonymousClass5NH A01;
    public C114705Mt A02;

    public AnonymousClass5MT(AbstractC114775Na r6) {
        Enumeration A0C = r6.A0C();
        while (A0C.hasMoreElements()) {
            AnonymousClass5NU A01 = AnonymousClass5NU.A01(A0C.nextElement());
            int i = A01.A00;
            if (i == 0) {
                this.A01 = AnonymousClass5NH.A04(A01, false);
            } else if (i == 1) {
                this.A02 = new C114705Mt(AbstractC114775Na.A05(A01, false));
            } else if (i == 2) {
                this.A00 = AnonymousClass5NG.A01(A01);
            } else {
                throw C12970iu.A0f("illegal tag");
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(3);
        AnonymousClass5NH r0 = this.A01;
        if (r0 != null) {
            C94954co.A03(r0, r3, false);
        }
        C114705Mt r1 = this.A02;
        if (r1 != null) {
            C94954co.A02(r1, r3, 1, false);
        }
        AnonymousClass5NG r12 = this.A00;
        if (r12 != null) {
            C94954co.A02(r12, r3, 2, false);
        }
        return new AnonymousClass5NZ(r3);
    }

    public String toString() {
        String str;
        AnonymousClass5NH r0 = this.A01;
        if (r0 != null) {
            byte[] bArr = r0.A00;
            str = C72463ee.A0I(bArr, bArr.length);
        } else {
            str = "null";
        }
        StringBuilder A0k = C12960it.A0k("AuthorityKeyIdentifier: KeyID(");
        A0k.append(str);
        return C12960it.A0d(")", A0k);
    }
}
