package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92604Wp {
    public final UserJid A00;
    public final boolean A01;

    public C92604Wp(UserJid userJid, boolean z) {
        this.A01 = z;
        this.A00 = userJid;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92604Wp r5 = (C92604Wp) obj;
            if (this.A01 != r5.A01 || !C29941Vi.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A00;
        return C12960it.A06(Boolean.valueOf(this.A01), A1a);
    }
}
