package X;

/* renamed from: X.06I  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass06I implements AnonymousClass06H {
    public static final AnonymousClass06I A01 = new AnonymousClass06I();
    public final boolean A00 = true;

    @Override // X.AnonymousClass06H
    public int A78(CharSequence charSequence, int i, int i2) {
        int i3 = i2 + 0;
        boolean z = false;
        for (int i4 = 0; i4 < i3; i4++) {
            byte directionality = Character.getDirectionality(charSequence.charAt(i4));
            if (directionality != 0) {
                if (directionality == 1 || directionality == 2) {
                    if (this.A00) {
                        return 0;
                    }
                }
            } else if (!this.A00) {
                return 1;
            }
            z = true;
        }
        if (z) {
            return this.A00 ? 1 : 0;
        }
        return 2;
    }
}
