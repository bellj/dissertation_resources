package X;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.0Sl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC06170Sl {
    public static final String A05 = C06390Tk.A01("ConstraintTracker");
    public Object A00;
    public final Context A01;
    public final AbstractC11500gO A02;
    public final Object A03 = new Object();
    public final Set A04 = new LinkedHashSet();

    public abstract Object A00();

    public abstract void A01();

    public abstract void A02();

    public AbstractC06170Sl(Context context, AbstractC11500gO r3) {
        this.A01 = context.getApplicationContext();
        this.A02 = r3;
    }

    public void A03(AbstractC11430gH r4) {
        synchronized (this.A03) {
            Set set = this.A04;
            if (set.remove(r4) && set.isEmpty()) {
                A02();
            }
        }
    }

    public void A04(Object obj) {
        synchronized (this.A03) {
            Object obj2 = this.A00;
            if (obj2 != obj && (obj2 == null || !obj2.equals(obj))) {
                this.A00 = obj;
                ((C07760a2) this.A02).A02.execute(new RunnableC09640dH(this, new ArrayList(this.A04)));
            }
        }
    }
}
