package X;

import java.util.Arrays;

/* renamed from: X.4WB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4WB {
    public final int A00;
    public final int A01;
    public final int A02 = 7;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final byte[] A08;
    public final byte[] A09;
    public final byte[] A0A;

    public AnonymousClass4WB(String str, String str2, String str3, String str4, String str5, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A07 = str;
        this.A06 = str2;
        this.A04 = str3;
        this.A09 = bArr;
        this.A08 = bArr2;
        this.A0A = bArr3;
        this.A03 = str4;
        this.A05 = str5;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamsysMMSDownloadRequest{mediaType=");
        A0k.append(this.A01);
        A0k.append(", downloadMode=");
        A0k.append(this.A00);
        A0k.append(", transferOption=");
        A0k.append(this.A02);
        A0k.append(", temporaryFilePath='");
        A0k.append(this.A07);
        A0k.append('\'');
        A0k.append(", outputFilePath='");
        A0k.append(this.A06);
        A0k.append('\'');
        A0k.append(", directPath='");
        A0k.append(this.A04);
        A0k.append('\'');
        A0k.append(", hashIdentifier=");
        A0k.append(Arrays.toString(this.A08));
        A0k.append(", plaintextHashIdentifier=");
        A0k.append(Arrays.toString(this.A0A));
        A0k.append(", connBlockJSONStr='");
        A0k.append(this.A03);
        A0k.append('\'');
        A0k.append(", loggingIdentifier='");
        A0k.append(this.A05);
        A0k.append('\'');
        return C12970iu.A0v(A0k);
    }
}
