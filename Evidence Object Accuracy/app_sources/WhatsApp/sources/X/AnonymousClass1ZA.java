package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ZA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZA implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100324lj();
    public final String A00;
    public final List A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZA(Parcel parcel) {
        this.A00 = parcel.readString();
        ArrayList arrayList = new ArrayList();
        this.A01 = arrayList;
        parcel.readList(arrayList, AnonymousClass1ZB.class.getClassLoader());
    }

    public AnonymousClass1ZA(String str, List list) {
        this.A00 = str;
        this.A01 = list;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeList(this.A01);
    }
}
