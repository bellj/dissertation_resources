package X;

/* renamed from: X.31S  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31S extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Integer A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public Integer A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;

    public AnonymousClass31S() {
        super(450, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(16, this.A0D);
        r3.Abe(10, this.A07);
        r3.Abe(14, this.A08);
        r3.Abe(13, this.A0E);
        r3.Abe(19, this.A00);
        r3.Abe(18, this.A01);
        r3.Abe(9, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(23, this.A04);
        r3.Abe(5, this.A05);
        r3.Abe(2, this.A09);
        r3.Abe(6, this.A0F);
        r3.Abe(7, this.A0G);
        r3.Abe(1, this.A0A);
        r3.Abe(8, this.A06);
        r3.Abe(22, this.A0H);
        r3.Abe(17, this.A0I);
        r3.Abe(12, this.A0J);
        r3.Abe(20, this.A0B);
        r3.Abe(11, this.A0K);
        r3.Abe(21, this.A0C);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMessageReceive {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deviceCount", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deviceSizeBucket", C12960it.A0Y(this.A07));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "disappearingChatInitiator", C12960it.A0Y(this.A08));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ephemeralityDuration", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isAReply", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isForwardedForward", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isViewOnce", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageIsInternational", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageIsInvisible", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageIsOffline", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageMediaType", C12960it.A0Y(this.A09));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageReceiveT0", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageReceiveT1", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageType", C12960it.A0Y(this.A0A));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mutedGroupMessage", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "paddingBytesSize", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "participantCount", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiverDefaultDisappearingDuration", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "revokeType", C12960it.A0Y(this.A0B));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "senderDefaultDisappearingDuration", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "typeOfGroup", C12960it.A0Y(this.A0C));
        return C12960it.A0d("}", A0k);
    }
}
