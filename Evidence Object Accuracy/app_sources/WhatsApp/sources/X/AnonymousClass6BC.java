package X;

import org.json.JSONObject;

/* renamed from: X.6BC  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BC implements AbstractC16870pt {
    public final C16120oU A00;
    public final AnonymousClass17V A01;
    public final String A02 = "BR";

    @Override // X.AbstractC16870pt
    public void AKh(Integer num, Integer num2, String str, String str2, boolean z) {
    }

    @Override // X.AbstractC16870pt
    public void AeG() {
    }

    public AnonymousClass6BC(C16120oU r2, AnonymousClass17V r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC16870pt
    public AnonymousClass2SP A8H() {
        AnonymousClass2SP r1 = new AnonymousClass2SP();
        r1.A0U = this.A01.A00();
        r1.A0R = this.A02;
        return r1;
    }

    @Override // X.AbstractC16870pt
    public void AKa(C452120p r4, int i) {
        int i2;
        C16120oU r2 = this.A00;
        AnonymousClass2SP A8H = A8H();
        A8H.A0B = Integer.valueOf(i);
        if (r4 != null) {
            A8H.A0S = String.valueOf(r4.A00);
            A8H.A0T = r4.A09;
            i2 = 2;
        } else {
            i2 = 1;
        }
        A8H.A0C = Integer.valueOf(i2);
        A8H.A09 = C12970iu.A0h();
        r2.A07(A8H);
    }

    @Override // X.AbstractC16870pt
    public void AKf(AnonymousClass2SP r2) {
        r2.A0U = this.A01.A00();
        r2.A0R = this.A02;
        this.A00.A07(r2);
    }

    @Override // X.AbstractC16870pt
    public void AKg(Integer num, Integer num2, String str, String str2) {
        AnonymousClass2SP A8H = A8H();
        A8H.A0Z = str;
        A8H.A09 = num;
        if (num2 != null) {
            A8H.A08 = num2;
        }
        if (str2 != null) {
            A8H.A0Y = str2;
        }
        this.A00.A07(A8H);
    }

    @Override // X.AbstractC16870pt
    public void AKi(AnonymousClass3FW r5, Integer num, Integer num2, String str, String str2) {
        AnonymousClass2SP A8H = A8H();
        A8H.A0Z = str;
        A8H.A09 = num;
        if (num2 != null) {
            A8H.A08 = num2;
        }
        if (str2 != null) {
            A8H.A0Y = str2;
        }
        if (r5 != null) {
            JSONObject jSONObject = r5.A01;
            if (jSONObject.has("is_payment_account_setup")) {
                A8H.A02 = Boolean.valueOf(jSONObject.optBoolean("is_payment_account_setup"));
                jSONObject.remove("is_payment_account_setup");
            }
            if (jSONObject.length() > 0) {
                A8H.A0X = r5.toString();
            }
        }
        this.A00.A07(A8H);
    }

    @Override // X.AbstractC16870pt
    public void AKj(AnonymousClass3FW r2, Integer num, Integer num2, String str, String str2, String str3, String str4, boolean z, boolean z2) {
        AKi(r2, num, num2, "payment_transaction_details", str2);
    }

    @Override // X.AbstractC16870pt
    public void reset() {
        C117305Zk.A1L(this.A01);
    }
}
