package X;

import android.os.SystemClock;

/* renamed from: X.397  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass397 extends AnonymousClass1MS {
    public final /* synthetic */ C64113Eh A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass397(C64113Eh r2) {
        super("CameraQRCodeProcessor");
        this.A00 = r2;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        C49262Kb r0;
        C49262Kb A00;
        while (true) {
            C64113Eh r2 = this.A00;
            if (r2.A07) {
                try {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    AnonymousClass4Q1 AAP = r2.A06.AAP();
                    if (AAP != null) {
                        byte[] bArr = AAP.A02;
                        int i = AAP.A01;
                        int i2 = AAP.A00;
                        int i3 = i;
                        if (i >= 320) {
                            i3 = (i * 3) >> 2;
                        }
                        int i4 = i2;
                        if (i2 >= 320) {
                            i4 = (i2 * 3) >> 2;
                        }
                        try {
                            A00 = r2.A04.A00(new AnonymousClass2KZ(new AnonymousClass2KX(new AnonymousClass2KV(bArr, i, i2, (i - i3) >> 1, (i2 - i4) >> 1, i3, i4))), r2.A02);
                        } catch (AbstractC49392Ko unused) {
                        }
                        if (r2.A07) {
                            synchronized (r2) {
                                if (A00 != null && ((r0 = r2.A00) == null || !r0.A02.equals(A00.A02))) {
                                    r2.A00 = A00;
                                    r2.A05.AT1(A00);
                                }
                            }
                        }
                    }
                    long uptimeMillis2 = ((long) (1000 / r2.A03)) - (SystemClock.uptimeMillis() - uptimeMillis);
                    if (r2.A07 && uptimeMillis2 > 0) {
                        Thread.sleep(uptimeMillis2);
                    }
                } catch (InterruptedException unused2) {
                }
            } else {
                return;
            }
        }
    }
}
