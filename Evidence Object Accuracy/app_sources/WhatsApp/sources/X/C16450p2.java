package X;

/* renamed from: X.0p2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C16450p2 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;

    public C16450p2() {
        super(3062, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A01);
        r3.Abe(10, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(2, this.A06);
        r3.Abe(8, this.A07);
        r3.Abe(4, this.A05);
        r3.Abe(7, this.A08);
        r3.Abe(1, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamDirectorySearchConsumerServerErrors {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchAcsErrorCode", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchAcsTokenNotReadyReason", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchCallType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorApiErrorCode", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorApiErrorSubCode", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorEntrypoint", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorFbtraceId", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorHttpResponseCode", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorMessage", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySessionId", this.A09);
        sb.append("}");
        return sb.toString();
    }
}
