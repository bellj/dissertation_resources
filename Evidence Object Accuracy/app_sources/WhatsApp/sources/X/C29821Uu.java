package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.1Uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29821Uu extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public String A05;

    public C29821Uu() {
        super(2450, new AnonymousClass00E(1000, 1000, SearchActionVerificationClientService.NOTIFICATION_ID), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A05);
        r3.Abe(7, this.A04);
        r3.Abe(5, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(8, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamDbPerf {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidPerfDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidPerfName", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dbSizeInMb", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isMainMessageStoreMigrationCompleted", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "onMainThread", this.A01);
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "startupStage", obj);
        sb.append("}");
        return sb.toString();
    }
}
