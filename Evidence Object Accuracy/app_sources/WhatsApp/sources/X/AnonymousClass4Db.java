package X;

import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.4Db  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Db {
    public static int[] A00(Collection collection) {
        if (collection instanceof AnonymousClass5I2) {
            AnonymousClass5I2 r4 = (AnonymousClass5I2) collection;
            return Arrays.copyOfRange(r4.array, r4.start, r4.end);
        }
        Object[] array = collection.toArray();
        int length = array.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = C12960it.A05(array[i]);
        }
        return iArr;
    }
}
