package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99404kF implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 2) {
                str = C95664e9.A09(parcel, str, c, 3, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78213oY(i, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78213oY[i];
    }
}
