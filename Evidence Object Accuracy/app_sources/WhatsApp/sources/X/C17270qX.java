package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.0qX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17270qX implements AbstractC16990q5 {
    public final AnonymousClass01H A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOo() {
    }

    public C17270qX(AnonymousClass01H r1, AnonymousClass01H r2, AnonymousClass01H r3) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        AbstractC15710nm r2;
        String A00;
        String str;
        ((C26061Bw) this.A02.get()).A08();
        AnonymousClass01H r5 = this.A00;
        ((SharedPreferences) ((AnonymousClass114) r5.get()).A03.A02.get()).edit().remove("/export/enc/owner").remove("/export/enc/version").remove("/export/enc/account_hash").remove("/export/enc/server_salt").remove("/export/enc/seed").apply();
        AnonymousClass114 r4 = (AnonymousClass114) r5.get();
        C15570nT r0 = r4.A00;
        r0.A08();
        C27631Ih r22 = r0.A05;
        synchronized (r4) {
            C15750nq r1 = r4.A03;
            AnonymousClass2F8 A01 = r1.A01();
            if (!(A01 == null || r22 == null || r4.A02(r22, A01))) {
                r1.A04();
                StringBuilder sb = new StringBuilder();
                sb.append("ExportEncryptionManager/onCheckPrefetchedKeyConsistency(); ");
                sb.append("cleared prefetched key, a different user is now logged in or key is old");
                Log.i(sb.toString());
            }
        }
        ((AnonymousClass114) r5.get()).A00();
        AnonymousClass01H r12 = this.A01;
        ((C15730no) r12.get()).A04();
        C15730no r7 = (C15730no) r12.get();
        C15790nu r6 = r7.A0A;
        boolean z = false;
        if (r6.A05()) {
            AnonymousClass01H r9 = r6.A05.A02;
            if (((SharedPreferences) r9.get()).getLong("/export/provider/timestamp", 0) != 0 && Math.abs(System.currentTimeMillis() - ((SharedPreferences) r9.get()).getLong("/export/provider/timestamp", 0)) > C15790nu.A07) {
                z = true;
            }
        }
        if (z) {
            r6.A01();
            r2 = r7.A02;
            A00 = r6.A01();
            str = "xpm-export-provider-expired";
        } else if (!r6.A05()) {
            AnonymousClass01H r92 = r6.A05.A02;
            if (((SharedPreferences) r92.get()).getLong("/export/provider_closed/timestamp", 0) != 0 && Math.abs(System.currentTimeMillis() - ((SharedPreferences) r92.get()).getLong("/export/provider_closed/timestamp", 0)) > C15790nu.A06) {
                r6.A00();
                r2 = r7.A02;
                A00 = r6.A00();
                str = "xpm-export-metadata-expired";
            } else {
                return;
            }
        } else {
            return;
        }
        r2.AaV(str, A00, false);
        r7.A03();
    }
}
