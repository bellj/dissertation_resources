package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.03v  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03v {
    public static final String A09 = C06390Tk.A01("WorkContinuationImpl");
    public AbstractC12800iW A00;
    public boolean A01;
    public final AnonymousClass023 A02;
    public final AnonymousClass022 A03;
    public final String A04;
    public final List A05 = new ArrayList();
    public final List A06;
    public final List A07;
    public final List A08;

    public AnonymousClass03v(AnonymousClass023 r4, AnonymousClass022 r5, String str, List list, List list2) {
        this.A03 = r5;
        this.A04 = str;
        this.A02 = r4;
        this.A08 = list;
        this.A07 = list2;
        this.A06 = new ArrayList(list.size());
        if (list2 != null) {
            Iterator it = list2.iterator();
            while (it.hasNext()) {
                this.A05.addAll(((AnonymousClass03v) it.next()).A05);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            String obj = ((AnonymousClass020) list.get(i)).A02.toString();
            this.A06.add(obj);
            this.A05.add(obj);
        }
    }

    public static Set A00(AnonymousClass03v r3) {
        HashSet hashSet = new HashSet();
        List<AnonymousClass03v> list = r3.A07;
        if (list != null && !list.isEmpty()) {
            for (AnonymousClass03v r0 : list) {
                hashSet.addAll(r0.A06);
            }
        }
        return hashSet;
    }

    public static boolean A01(AnonymousClass03v r5, Set set) {
        List list = r5.A06;
        set.addAll(list);
        Set A00 = A00(r5);
        for (Object obj : set) {
            if (A00.contains(obj)) {
                return true;
            }
        }
        List<AnonymousClass03v> list2 = r5.A07;
        if (list2 != null && !list2.isEmpty()) {
            for (AnonymousClass03v r0 : list2) {
                if (A01(r0, set)) {
                    return true;
                }
            }
        }
        set.removeAll(list);
        return false;
    }

    public final AnonymousClass03v A02(AnonymousClass021 r7) {
        List singletonList = Collections.singletonList(r7);
        if (singletonList.isEmpty()) {
            return this;
        }
        return new AnonymousClass03v(AnonymousClass023.KEEP, this.A03, this.A04, singletonList, Collections.singletonList(this));
    }

    public void A03() {
        if (!this.A01) {
            RunnableC10260eI r1 = new RunnableC10260eI(this);
            ((C07760a2) this.A03.A06).A01.execute(r1);
            this.A00 = r1.A00;
            return;
        }
        C06390Tk.A00().A05(A09, String.format("Already enqueued work ids (%s)", TextUtils.join(", ", this.A06)), new Throwable[0]);
    }
}
