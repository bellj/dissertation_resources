package X;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/* renamed from: X.0PL  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PL {
    public FrameLayout A00;
    public final Context A01;
    public final Handler A02 = new Handler(Looper.getMainLooper());
    public final C04540Mc A03;
    public final C14260l7 A04;

    public AnonymousClass0PL(Context context, C04540Mc r4, C14260l7 r5) {
        this.A01 = context;
        this.A04 = r5;
        this.A03 = r4;
    }

    public final Button A00() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        Context context = this.A01;
        gradientDrawable.setCornerRadius(AnonymousClass0LQ.A00(context, 4.0f));
        C14260l7 r2 = this.A04;
        gradientDrawable.setStroke(1, AnonymousClass0TG.A00(context, EnumC03700Iu.A09, r2));
        gradientDrawable.setColor(AnonymousClass0TG.A00(context, EnumC03700Iu.A08, r2));
        Button button = new Button(context);
        button.setBackgroundDrawable(gradientDrawable);
        button.setText(AnonymousClass0K4.dialog_cancel);
        button.setTextSize(17.0f);
        button.setTextColor(AnonymousClass0TG.A00(context, EnumC03700Iu.A0A, r2));
        button.setHeight((int) AnonymousClass0LQ.A00(context, 52.0f));
        button.setOnClickListener(new View.OnClickListener() { // from class: X.0WI
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AnonymousClass09V r0 = AnonymousClass0PL.this.A03.A00.A05;
                if (r0 != null) {
                    r0.dismiss();
                }
            }
        });
        button.setAlpha(0.0f);
        return button;
    }

    public final void A01(FrameLayout frameLayout) {
        Context context = this.A01;
        AnonymousClass0A8 r2 = new AnonymousClass0A8(context, AnonymousClass0TG.A00(context, EnumC03700Iu.A07, this.A04), (int) AnonymousClass0LQ.A00(context, 32.0f));
        C02330Bg r5 = new C02330Bg(context);
        AnonymousClass0A8 r0 = null;
        if (r2 instanceof Animatable) {
            r0 = r2;
        }
        r5.A00 = r0;
        r5.setImageDrawable(r2);
        if (this.A00 == null) {
            this.A00 = new FrameLayout(context);
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        Button A00 = A00();
        AnonymousClass0QQ A0F = AnonymousClass028.A0F(A00);
        View view = (View) A0F.A00.get();
        if (view != null) {
            view.animate().setStartDelay(3000);
        }
        A0F.A02(1.0f);
        A0F.A07(200);
        A0F.A08(new LinearInterpolator());
        A0F.A01();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(80);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins((int) AnonymousClass0LQ.A00(context, 20.0f), 0, (int) AnonymousClass0LQ.A00(context, 20.0f), (int) AnonymousClass0LQ.A00(context, 20.0f));
        layoutParams2.weight = 1.0f;
        linearLayout.addView(A00, layoutParams2);
        this.A00.addView(r5, layoutParams);
        this.A00.addView(linearLayout);
        frameLayout.addView(this.A00);
        Animatable animatable = r5.A00;
        if (animatable != null) {
            animatable.start();
        }
        r5.A01 = true;
    }
}
