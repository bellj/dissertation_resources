package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.0xU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21480xU extends AbstractC17700rF {
    public final C14830m7 A00;
    public final C17680rD A01;
    public final C21470xT A02;

    public C21480xU(C14830m7 r1, C17680rD r2, C21470xT r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public C455922g A07(UserJid userJid) {
        C17680rD r4 = this.A01;
        Object obj = null;
        String string = r4.A02.A01(r4.A03).getString(userJid.getRawString(), null);
        if (string != null) {
            try {
                obj = r4.A01.A00(string);
            } catch (C456222j e) {
                r4.A01(e, "getObject");
                r4.A00(userJid);
            }
        }
        return (C455922g) obj;
    }
}
