package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4j8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98714j8 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        long j = 0;
        long j2 = 0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = -1;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    i3 = C95664e9.A02(parcel, readInt);
                    break;
                case 4:
                    j = C95664e9.A04(parcel, readInt);
                    break;
                case 5:
                    j2 = C95664e9.A04(parcel, readInt);
                    break;
                case 6:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 7:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case '\b':
                    i4 = C95664e9.A02(parcel, readInt);
                    break;
                case '\t':
                    i5 = C95664e9.A02(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78423ot(str, str2, i, i2, i3, i4, i5, j, j2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78423ot[i];
    }
}
