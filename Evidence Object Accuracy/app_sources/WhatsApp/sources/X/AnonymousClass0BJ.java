package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0BJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BJ extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass0BD A02;

    public AnonymousClass0BJ(AnonymousClass0BD r1, int i, int i2) {
        this.A02 = r1;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass0A7 r3 = this.A02.A0L;
        int i = this.A01;
        r3.setAlpha((int) (((float) i) + (((float) (this.A00 - i)) * f)));
    }
}
