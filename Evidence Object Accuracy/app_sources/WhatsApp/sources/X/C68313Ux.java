package X;

import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

/* renamed from: X.3Ux  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68313Ux implements AnonymousClass5Vo {
    public final /* synthetic */ AnonymousClass2ET A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ Set A02;
    public final /* synthetic */ AnonymousClass1J7 A03;

    public C68313Ux(AnonymousClass2ET r1, UserJid userJid, Set set, AnonymousClass1J7 r4) {
        this.A00 = r1;
        this.A02 = set;
        this.A01 = userJid;
        this.A03 = r4;
    }

    @Override // X.AnonymousClass5Vo
    public void AQJ(C92404Vt r3, int i) {
        Object r0;
        AnonymousClass1J7 r1 = this.A03;
        if (i == -1) {
            r0 = new C849440n();
        } else {
            r0 = new C849540o();
        }
        r1.AJ4(r0);
    }

    @Override // X.AnonymousClass5Vo
    public void AQK(C92404Vt r7, AnonymousClass3CI r8) {
        AnonymousClass2ET r5 = this.A00;
        Set set = this.A02;
        UserJid userJid = this.A01;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            linkedHashMap.put(A0x, r5.A03.A01(userJid, A0x));
        }
        this.A03.AJ4(new C60262wN(linkedHashMap, false));
    }
}
