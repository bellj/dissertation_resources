package X;

import java.io.Serializable;

/* renamed from: X.1WL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WL implements AbstractC16710pd, Serializable {
    public volatile Object _value = AnonymousClass1WS.A00;
    public AnonymousClass1WK initializer;
    public final Object lock = this;

    public /* synthetic */ AnonymousClass1WL(AnonymousClass1WK r2) {
        this.initializer = r2;
    }

    @Override // X.AbstractC16710pd
    public boolean AJW() {
        return this._value != AnonymousClass1WS.A00;
    }

    @Override // X.AbstractC16710pd
    public Object getValue() {
        Object obj;
        Object obj2 = this._value;
        AnonymousClass1WS r0 = AnonymousClass1WS.A00;
        if (obj2 != r0) {
            return obj2;
        }
        synchronized (this.lock) {
            obj = this._value;
            if (obj == r0) {
                AnonymousClass1WK r02 = this.initializer;
                C16700pc.A0C(r02);
                obj = r02.AJ3();
                this._value = obj;
                this.initializer = null;
            }
        }
        return obj;
    }

    @Override // java.lang.Object
    public String toString() {
        return AJW() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    private final Object writeReplace() {
        return new C112625Ec(getValue());
    }
}
