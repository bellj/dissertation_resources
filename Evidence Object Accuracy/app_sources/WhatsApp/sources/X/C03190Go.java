package X;

/* renamed from: X.0Go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03190Go extends AnonymousClass0Q1 {
    @Override // X.AnonymousClass0Q1
    public void A00(C06340Tf r1, C06340Tf r2) {
        r1.next = r2;
    }

    @Override // X.AnonymousClass0Q1
    public void A01(C06340Tf r1, Thread thread) {
        r1.thread = thread;
    }

    @Override // X.AnonymousClass0Q1
    public boolean A02(C05940Ro r2, C05940Ro r3, AnonymousClass041 r4) {
        boolean z;
        synchronized (r4) {
            if (r4.listeners == r2) {
                r4.listeners = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    @Override // X.AnonymousClass0Q1
    public boolean A03(C06340Tf r2, C06340Tf r3, AnonymousClass041 r4) {
        boolean z;
        synchronized (r4) {
            if (r4.waiters == r2) {
                r4.waiters = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    @Override // X.AnonymousClass0Q1
    public boolean A04(AnonymousClass041 r2, Object obj, Object obj2) {
        boolean z;
        synchronized (r2) {
            if (r2.value == obj) {
                r2.value = obj2;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }
}
