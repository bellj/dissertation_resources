package X;

import android.app.Activity;
import android.content.Intent;
import com.whatsapp.Conversation;
import java.util.Map;

/* renamed from: X.2zK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61052zK extends AbstractC60952zA {
    @Override // X.AbstractC60952zA
    public void A06(Activity activity, AbstractC17160qM r16, AnonymousClass018 r17, AnonymousClass1Z8 r18, C16660pY r19, String str, long j) {
        String str2;
        long j2;
        C65963Lt r11;
        super.A06(activity, r16, r17, r18, r19, str, j);
        Conversation conversation = (Conversation) AbstractC35731ia.A01(activity, Conversation.class);
        C91284Rd r2 = (C91284Rd) ((Map) r19.A01.getValue()).get("address_message");
        if (r2 == null) {
            str2 = "com.bloks.www.whatsapp.commerce.address_message";
        } else if (r2.A03) {
            str2 = r2.A01;
        } else {
            return;
        }
        if (conversation != null) {
            String str3 = str2;
            if (r2 != null) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(r2.A01);
                str3 = C12960it.A0d(r2.A02, A0h);
                j2 = r2.A00 * 1000;
                if (j2 == 0) {
                    r11 = null;
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(conversation.getPackageName(), "com.whatsapp.wabloks.ui.WaBloksActivity");
                    A0A.putExtra("screen_name", str2);
                    A0A.putExtra("screen_params", (String) null);
                    A0A.putExtra("screen_cache_config", r11);
                    A0A.putExtra("chat_id", C15380n4.A03(conversation.A2c.A0B(AbstractC14640lm.class)));
                    A0A.putExtra("message_id", str);
                    A0A.putExtra("action_name", "address_message");
                    A0A.putExtra("message_row_id", j);
                    activity.startActivity(A0A);
                }
            } else {
                j2 = 3600000;
            }
            StringBuilder A0j = C12960it.A0j(str3);
            A0j.append(":");
            r11 = new C65963Lt(C12960it.A0d(r17.A07(), A0j), j2, true);
            Intent A0A = C12970iu.A0A();
            A0A.setClassName(conversation.getPackageName(), "com.whatsapp.wabloks.ui.WaBloksActivity");
            A0A.putExtra("screen_name", str2);
            A0A.putExtra("screen_params", (String) null);
            A0A.putExtra("screen_cache_config", r11);
            A0A.putExtra("chat_id", C15380n4.A03(conversation.A2c.A0B(AbstractC14640lm.class)));
            A0A.putExtra("message_id", str);
            A0A.putExtra("action_name", "address_message");
            A0A.putExtra("message_row_id", j);
            activity.startActivity(A0A);
        }
    }
}
