package X;

import com.whatsapp.R;

/* renamed from: X.3VP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VP implements AbstractC116515Vt {
    public final /* synthetic */ AnonymousClass283 A00;

    public AnonymousClass3VP(AnonymousClass283 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116515Vt
    public void AU0() {
        this.A00.A0Y = null;
    }

    @Override // X.AbstractC116515Vt
    public void AU1(String str) {
        AnonymousClass283 r1 = this.A00;
        r1.A2C(R.string.pincode_verification_progress_spinner);
        C53912fi r0 = r1.A0a;
        AnonymousClass19T r4 = r0.A0C;
        r4.A06.A00(new C1099553r(new AnonymousClass3V4(r0, str), r4), r0.A0H, str).A06();
    }
}
