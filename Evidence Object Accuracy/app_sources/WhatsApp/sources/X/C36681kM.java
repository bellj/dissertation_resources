package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.status.SetStatus;
import java.util.ArrayList;

/* renamed from: X.1kM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36681kM extends ArrayAdapter {
    public final ArrayList A00;
    public final /* synthetic */ SetStatus A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36681kM(Context context, SetStatus setStatus, ArrayList arrayList) {
        super(context, (int) R.id.status_row, arrayList);
        this.A01 = setStatus;
        this.A00 = arrayList;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextEmojiLabel textEmojiLabel;
        if (view == null) {
            view = AnonymousClass01d.A01(viewGroup.getContext()).inflate(R.layout.setstatus_row, (ViewGroup) null);
        }
        String str = (String) this.A00.get(i);
        if (!(str == null || (textEmojiLabel = (TextEmojiLabel) view.findViewById(R.id.status_row)) == null)) {
            View findViewById = view.findViewById(R.id.status_selected_check);
            SetStatus setStatus = this.A01;
            int i2 = 4;
            if (str.equals(setStatus.A01.A00())) {
                i2 = 0;
            }
            findViewById.setVisibility(i2);
            findViewById.setContentDescription(setStatus.getString(R.string.checked_icon_label));
            textEmojiLabel.A0G(null, str);
        }
        return view;
    }
}
