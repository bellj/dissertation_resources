package X;

import android.content.Context;
import android.text.TextUtils;
import androidx.window.extensions.layout.WindowLayoutComponent;
import androidx.window.sidecar.SidecarProvider;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0UG  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0UG {
    public static final /* synthetic */ AnonymousClass0UG A00 = new AnonymousClass0UG();

    static {
        C88204Er.A00(AbstractC12760iS.class).A01();
    }

    public static final C08950c7 A00(String str) {
        String group;
        Integer valueOf;
        Integer valueOf2;
        Integer valueOf3;
        String str2;
        if (str != null && !AnonymousClass03C.A0J(str)) {
            Matcher matcher = Pattern.compile("(\\d+)(?:\\.(\\d+))(?:\\.(\\d+))(?:-(.+))?").matcher(str);
            if (!(!matcher.matches() || (group = matcher.group(1)) == null || (valueOf = Integer.valueOf(Integer.parseInt(group))) == null)) {
                int intValue = valueOf.intValue();
                String group2 = matcher.group(2);
                if (!(group2 == null || (valueOf2 = Integer.valueOf(Integer.parseInt(group2))) == null)) {
                    int intValue2 = valueOf2.intValue();
                    String group3 = matcher.group(3);
                    if (!(group3 == null || (valueOf3 = Integer.valueOf(Integer.parseInt(group3))) == null)) {
                        int intValue3 = valueOf3.intValue();
                        if (matcher.group(4) != null) {
                            str2 = matcher.group(4);
                        } else {
                            str2 = "";
                        }
                        C16700pc.A0B(str2);
                        return new C08950c7(str2, intValue, intValue2, intValue3);
                    }
                }
            }
        }
        return null;
    }

    public static final C07530Zb A01(Context context) {
        String apiVersion;
        if (C07530Zb.A03 == null) {
            ReentrantLock reentrantLock = C07530Zb.A02;
            reentrantLock.lock();
            try {
                if (C07530Zb.A03 == null) {
                    AnonymousClass0ZY r3 = null;
                    try {
                        try {
                            apiVersion = SidecarProvider.getApiVersion();
                        } catch (Throwable unused) {
                        }
                    } catch (NoClassDefFoundError | UnsupportedOperationException unused2) {
                    }
                    if (!TextUtils.isEmpty(apiVersion)) {
                        C08950c7 A002 = A00(apiVersion);
                        if (A002 != null && A002.compareTo(C08950c7.A07) >= 0) {
                            AnonymousClass0ZY r1 = new AnonymousClass0ZY(context);
                            if (r1.A03()) {
                                r3 = r1;
                            }
                        }
                    }
                    C07530Zb.A03 = new C07530Zb(r3);
                }
            } finally {
                reentrantLock.unlock();
            }
        }
        C07530Zb r0 = C07530Zb.A03;
        C16700pc.A0C(r0);
        return r0;
    }

    public final AbstractC12760iS A02(Context context) {
        AbstractC12440hv r1;
        WindowLayoutComponent A002;
        C16700pc.A0E(context, 0);
        C07560Ze r2 = C07560Ze.A00;
        try {
            A002 = AnonymousClass0UQ.A00();
        } catch (Throwable unused) {
        }
        if (A002 != null) {
            r1 = new C07520Za(A002);
            return new C07540Zc(r1, r2);
        }
        r1 = A01(context);
        return new C07540Zc(r1, r2);
    }
}
