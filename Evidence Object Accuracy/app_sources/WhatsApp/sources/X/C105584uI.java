package X;

/* renamed from: X.4uI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105584uI implements AbstractC009404s {
    public final AnonymousClass2IV A00;
    public final AnonymousClass3EX A01;

    public C105584uI(AnonymousClass2IV r1, AnonymousClass3EX r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass2IV r0 = this.A00;
        return new C74443hz(AbstractC250617y.A00(r0.A00.A03.AO3), this.A01);
    }
}
