package X;

import com.whatsapp.backup.encryptedbackup.EncBackupMainActivity;

/* renamed from: X.3hY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74203hY extends AbstractC010305c {
    public final /* synthetic */ EncBackupMainActivity A00;
    public final /* synthetic */ boolean A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74203hY(EncBackupMainActivity encBackupMainActivity, boolean z) {
        super(true);
        this.A00 = encBackupMainActivity;
        this.A01 = z;
    }

    @Override // X.AbstractC010305c
    public void A00() {
        if (this.A01) {
            EncBackupMainActivity.A02(this.A00);
        }
    }
}
