package X;

/* renamed from: X.4wM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106834wM implements AnonymousClass5Yf {
    public final long A00;
    public final AnonymousClass5Yf A01;

    public C106834wM(AnonymousClass5Yf r4, long j) {
        this.A01 = r4;
        C95314dV.A03(C12990iw.A1W((r4.AFo() > j ? 1 : (r4.AFo() == j ? 0 : -1))));
        this.A00 = j;
    }

    @Override // X.AnonymousClass5Yf
    public void A5r(int i) {
        this.A01.A5r(i);
    }

    @Override // X.AnonymousClass5Yf
    public long AFf() {
        return this.A01.AFf() - this.A00;
    }

    @Override // X.AnonymousClass5Yf
    public long AFo() {
        return this.A01.AFo() - this.A00;
    }

    @Override // X.AnonymousClass5Yf
    public int AZ0(byte[] bArr, int i, int i2) {
        return this.A01.AZ0(bArr, i, i2);
    }

    @Override // X.AnonymousClass5Yf
    public void AZ4(byte[] bArr, int i, int i2) {
        this.A01.AZ4(bArr, i, i2);
    }

    @Override // X.AnonymousClass5Yf
    public boolean AZ5(byte[] bArr, int i, int i2, boolean z) {
        return this.A01.AZ5(bArr, 0, i2, z);
    }

    @Override // X.AnonymousClass5Yf
    public boolean AZu(byte[] bArr, int i, int i2, boolean z) {
        return this.A01.AZu(bArr, 0, i2, z);
    }

    @Override // X.AnonymousClass5Yf
    public void Aaj() {
        this.A01.Aaj();
    }

    @Override // X.AnonymousClass5Yf
    public int Ae1(int i) {
        return this.A01.Ae1(1);
    }

    @Override // X.AnonymousClass5Yf
    public void Ae3(int i) {
        this.A01.Ae3(i);
    }

    @Override // X.AnonymousClass5Yf
    public long getLength() {
        return this.A01.getLength() - this.A00;
    }

    @Override // X.AnonymousClass5Yf, X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        return this.A01.read(bArr, i, i2);
    }

    @Override // X.AnonymousClass5Yf
    public void readFully(byte[] bArr, int i, int i2) {
        this.A01.readFully(bArr, i, i2);
    }
}
