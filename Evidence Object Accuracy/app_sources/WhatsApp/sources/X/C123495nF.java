package X;

/* renamed from: X.5nF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123495nF extends AbstractC118085bF {
    public final C130065yk A00;

    public C123495nF(C14830m7 r1, AnonymousClass018 r2, C18600si r3, C17070qD r4, AbstractC16870pt r5, C130065yk r6) {
        super(r1, r2, r3, r4, r5);
        this.A00 = r6;
    }

    public int A0C(int i) {
        if (i != 0 || !C12980iv.A1W(this.A06.A01(), "payment_brazil_nux_dismissed")) {
            C130015yf r1 = this.A00.A06;
            if (!r1.A03() || C117295Zj.A0Z(this.A07).isEmpty()) {
                return r1.A03() ? 2 : 1;
            }
        }
        return 0;
    }
}
