package X;

import android.content.Context;

/* renamed from: X.2xh  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2xh extends AnonymousClass1OY {
    public boolean A00;

    public AnonymousClass2xh(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            C60792ye r2 = (C60792ye) this;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J A08 = AnonymousClass1OY.A08(r3, r2);
            AnonymousClass1OY.A0L(A08, r2);
            AnonymousClass1OY.A0M(A08, r2);
            AnonymousClass1OY.A0K(A08, r2);
            AnonymousClass1OY.A0I(r3, A08, r2, AnonymousClass1OY.A09(A08, r2, AnonymousClass1OY.A0B(A08, r2)));
            r2.A02 = A08.A2e();
            r2.A01 = (C26171Ch) A08.A2a.get();
        }
    }
}
