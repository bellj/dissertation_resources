package X;

import android.view.View;

/* renamed from: X.0W7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W7 implements View.OnAttachStateChangeListener {
    public final /* synthetic */ AnonymousClass0OS A00;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public AnonymousClass0W7(AnonymousClass0OS r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        this.A00.A00.A05();
        view.removeOnAttachStateChangeListener(this);
    }
}
