package X;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.SparseBooleanArray;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.VideoTimelineView;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.gallery.GalleryFragmentBase;
import com.whatsapp.gallerypicker.GalleryPickerFragment;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

/* renamed from: X.0os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class AsyncTaskC16360os extends AsyncTask {
    public final AbstractC16350or A00;

    public /* synthetic */ AsyncTaskC16360os(AbstractC16350or r1) {
        this.A00 = r1;
    }

    public final void A00() {
        AbstractC16350or r2 = this.A00;
        AbstractC001200n r0 = r2.A01;
        if (r0 != null) {
            AnonymousClass054 r1 = r2.A00;
            if (r1 != null) {
                r0.ADr().A01(r1);
            }
            r2.A01 = null;
        }
    }

    public void A01(Object... objArr) {
        publishProgress(objArr);
    }

    @Override // android.os.AsyncTask
    public final Object doInBackground(Object... objArr) {
        return this.A00.A05(objArr);
    }

    @Override // android.os.AsyncTask
    public void onCancelled() {
        try {
            this.A00.A02();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    @Override // android.os.AsyncTask
    public void onCancelled(Object obj) {
        try {
            AbstractC16350or r1 = this.A00;
            if (r1 instanceof C38661oT) {
                ((C38661oT) r1).A08((AnonymousClass4S5) obj);
            } else if (r1 instanceof C628238s) {
                ((C628238s) r1).A04.A0C.AL6(453128091, 2, 4);
            } else if (r1 instanceof C42811vu) {
                C42811vu r12 = (C42811vu) r1;
                r12.A00 = null;
                r12.A01 = null;
            }
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    @Override // android.os.AsyncTask
    public void onPostExecute(Object obj) {
        try {
            this.A00.A07(obj);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    @Override // android.os.AsyncTask
    public void onPreExecute() {
        this.A00.A06();
    }

    @Override // android.os.AsyncTask
    public final void onProgressUpdate(Object... objArr) {
        AbstractC33511eG r2;
        int intValue;
        AbstractC16350or r4 = this.A00;
        if (r4 instanceof C38711oa) {
            C38711oa r42 = (C38711oa) r4;
            if (!((AbstractC16350or) r42).A02.isCancelled() && (r2 = r42.A00) != null) {
                Object obj = objArr[0];
                if (obj instanceof C71733dQ) {
                    r2.ATK((C71733dQ) obj);
                } else if (obj instanceof AnonymousClass1KZ) {
                    r2.ATJ((AnonymousClass1KZ) obj);
                } else if (obj instanceof String) {
                    r2.ATM((String) obj);
                }
            }
        } else if (r4 instanceof C38801oj) {
            ((C38801oj) r4).A01.ARs(((Bitmap[]) objArr)[0]);
        } else if (r4 instanceof C38661oT) {
            C38661oT r43 = (C38661oT) r4;
            int intValue2 = ((Number) objArr[1]).intValue();
            AnonymousClass15I r0 = r43.A05;
            String str = r43.A07;
            r0.A01.put(str, Integer.valueOf(intValue2));
            for (AnonymousClass1KM r02 : r43.A03.A01()) {
                r02.toString();
                r02.A01(str, intValue2);
            }
        } else if (r4 instanceof AnonymousClass38J) {
            AnonymousClass38J r44 = (AnonymousClass38J) r4;
            AnonymousClass1KS[] r12 = (AnonymousClass1KS[]) objArr;
            boolean z = true;
            if (r12.length != 1) {
                z = false;
            }
            AnonymousClass009.A0E(z);
            AnonymousClass1KS r22 = r12[0];
            AnonymousClass009.A05(r22);
            AnonymousClass1KR r3 = r44.A01;
            if (r3 != null) {
                StickerStorePackPreviewActivity stickerStorePackPreviewActivity = (StickerStorePackPreviewActivity) r3;
                stickerStorePackPreviewActivity.A0M.A0E();
                Object obj2 = stickerStorePackPreviewActivity.A0T.get(r22.A0C);
                AnonymousClass009.A05(obj2);
                int intValue3 = ((Number) obj2).intValue();
                SparseBooleanArray sparseBooleanArray = stickerStorePackPreviewActivity.A0L.A01;
                if (sparseBooleanArray != null) {
                    sparseBooleanArray.put(intValue3, true);
                }
                stickerStorePackPreviewActivity.A0M.A03(intValue3);
            }
        } else if (r4 instanceof AnonymousClass38N) {
            ((AnonymousClass38N) r4).A03.A00(((AnonymousClass4OI[]) objArr)[0]);
        } else if (r4 instanceof C627838o) {
            Integer[] numArr = (Integer[]) objArr;
            ProgressDialog progressDialog = ((C627838o) r4).A00;
            if (progressDialog != null) {
                progressDialog.setProgress(numArr[0].intValue());
            }
        } else if (r4 instanceof C627738n) {
            C627738n r45 = (C627738n) r4;
            boolean booleanValue = ((Boolean[]) objArr)[0].booleanValue();
            r45.A00.A09.AcF(booleanValue);
            r45.A01.A00(booleanValue);
            StringBuilder sb = new StringBuilder("dictionaryloader/prepare/onProgressUpdate/hasDictionary=");
            sb.append(booleanValue);
            Log.i(sb.toString());
        } else if (r4 instanceof AnonymousClass383) {
            List[] listArr = (List[]) objArr;
            GalleryPickerFragment galleryPickerFragment = (GalleryPickerFragment) ((AnonymousClass383) r4).A06.get();
            if (galleryPickerFragment != null) {
                for (List list : listArr) {
                    if (galleryPickerFragment.A0B() != null) {
                        C54332gY r1 = galleryPickerFragment.A0F;
                        r1.A00.addAll(list);
                        r1.A02();
                        if (galleryPickerFragment.A0F.A00.size() == 0) {
                            galleryPickerFragment.A1A();
                        } else {
                            View view = galleryPickerFragment.A06;
                            if (view != null) {
                                view.setVisibility(8);
                            }
                        }
                    }
                }
            }
        } else if (r4 instanceof C626538b) {
            C626538b r46 = (C626538b) r4;
            List[] listArr2 = (List[]) objArr;
            GalleryFragmentBase galleryFragmentBase = (GalleryFragmentBase) r46.A08.get();
            if (galleryFragmentBase != null) {
                for (List list2 : listArr2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(galleryFragmentBase.A0H);
                    sb2.append("/report bucket ");
                    sb2.append(r46.A00);
                    sb2.append(" ");
                    sb2.append(list2.size());
                    Log.i(sb2.toString());
                    if (r46.A00 == 0) {
                        galleryFragmentBase.A0I.clear();
                        galleryFragmentBase.A0A.A02();
                    }
                    r46.A00 += list2.size();
                    galleryFragmentBase.A0I.addAll(list2);
                    galleryFragmentBase.A0A.A02();
                }
            }
        } else if (r4 instanceof C45011zv) {
            C44951zp r13 = ((C45011zv) r4).A05;
            int intValue4 = ((Integer[]) objArr)[0].intValue();
            for (AbstractC45001zu r03 : r13.A01()) {
                r03.AUO(intValue4);
            }
        } else if (r4 instanceof C627538l) {
            C91544Sd[] r122 = (C91544Sd[]) objArr;
            ContactPickerFragment contactPickerFragment = (ContactPickerFragment) ((C627538l) r4).A08.get();
            if (contactPickerFragment != null && contactPickerFragment.A0c()) {
                contactPickerFragment.A1Y(r122[0]);
            }
        } else if (r4 instanceof C628738x) {
            AnonymousClass4NP[] r123 = (AnonymousClass4NP[]) objArr;
            ContactPickerFragment contactPickerFragment2 = (ContactPickerFragment) ((C628738x) r4).A0C.get();
            if (contactPickerFragment2 != null && contactPickerFragment2.A0c()) {
                contactPickerFragment2.A1X(r123[0]);
            }
        } else if (r4 instanceof AnonymousClass38F) {
            AnonymousClass38F r47 = (AnonymousClass38F) r4;
            LinkedHashMap linkedHashMap = ((LinkedHashMap[]) objArr)[0];
            if (linkedHashMap != null) {
                CallsHistoryFragment callsHistoryFragment = r47.A00;
                callsHistoryFragment.A0g = linkedHashMap;
                callsHistoryFragment.A07.getFilter().filter(callsHistoryFragment.A0d);
            }
        } else if (r4 instanceof AnonymousClass38R) {
            AnonymousClass38R r48 = (AnonymousClass38R) r4;
            VideoTimelineView videoTimelineView = (VideoTimelineView) r48.A05.get();
            if (videoTimelineView != null) {
                ArrayList arrayList = videoTimelineView.A0N;
                if (arrayList != null) {
                    Collections.addAll(arrayList, objArr);
                }
                if (System.currentTimeMillis() > r48.A00 + 500) {
                    r48.A00 = System.currentTimeMillis();
                    videoTimelineView.invalidate();
                }
            }
        } else if (r4 instanceof AnonymousClass2KI) {
            Integer[] numArr2 = (Integer[]) objArr;
            AnonymousClass2F9 r14 = ((AnonymousClass2KI) r4).A01;
            int intValue5 = numArr2[0].intValue();
            AbstractActivityC28171Kz r9 = r14.A02.A00;
            if (r9 instanceof RestoreFromBackupActivity) {
                RestoreFromBackupActivity restoreFromBackupActivity = (RestoreFromBackupActivity) r9;
                if (intValue5 - restoreFromBackupActivity.A00 > 0) {
                    restoreFromBackupActivity.A00 = intValue5;
                    if (intValue5 % 10 == 0) {
                        StringBuilder sb3 = new StringBuilder("gdrive-activity/msg-restore-progress/");
                        sb3.append(intValue5);
                        sb3.append("%");
                        Log.i(sb3.toString());
                    }
                    if (intValue5 <= 100) {
                        restoreFromBackupActivity.A08.setText(restoreFromBackupActivity.getString(R.string.settings_gdrive_backup_msgstore_restore_message_with_percentage_placeholder, ((ActivityC13830kP) restoreFromBackupActivity).A01.A0K().format(((double) intValue5) / 100.0d)));
                        restoreFromBackupActivity.A05.setIndeterminate(true);
                    }
                }
            }
            ProgressDialogC48342Fq r04 = AnonymousClass2F9.A0B;
            if (!(r04 == null || r04.getProgress() == (intValue = numArr2[0].intValue()))) {
                AnonymousClass2F9.A0B.setProgress(intValue);
            }
        }
    }
}
