package X;

import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3Sa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67573Sa implements AnonymousClass5WW {
    public final C89034Ik A00;
    public final C63423Bn A01;

    public C67573Sa(C89034Ik r1, C63423Bn r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView = (RecyclerView) obj;
        C63423Bn r5 = this.A01;
        r5.A06 = recyclerView;
        r5.A03 = recyclerView.getScrollX();
        recyclerView.A0n(r5.A05);
        int i = r5.A00;
        if (i != -1) {
            if (!r5.A08) {
                recyclerView.A0Y(i);
            } else if (!r5.A07.equals("")) {
                C74773il r1 = new C74773il(context);
                r1.A09(r5.A07);
                ((AbstractC05520Pw) r1).A00 = r5.A00;
                AnonymousClass02H layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.A0R(r1);
                }
            } else {
                recyclerView.A0Z(i);
            }
            r5.A00 = -1;
            r5.A08 = false;
            r5.A07 = "";
        }
        int i2 = r5.A01;
        if (i2 != -1 || r5.A02 != -1) {
            boolean z = r5.A09;
            int i3 = r5.A02;
            if (z) {
                recyclerView.A0d(i2, i3);
            } else {
                recyclerView.scrollBy(i2, i3);
            }
            r5.A01 = -1;
            r5.A02 = -1;
            r5.A09 = false;
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2 = (RecyclerView) obj;
        if (Build.VERSION.SDK_INT >= 26 && (recyclerView = this.A01.A06) != null) {
            recyclerView.setImportantForAutofill(0);
        }
        C63423Bn r1 = this.A01;
        r1.A06 = null;
        recyclerView2.A0o(r1.A05);
    }
}
