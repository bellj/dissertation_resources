package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2YF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YF extends AnimatorListenerAdapter {
    public final /* synthetic */ C47342Ag A00;

    public AnonymousClass2YF(C47342Ag r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2Ab r3 = this.A00.A04;
        C47342Ag r2 = r3.A0Q;
        if (r2.A00() == 1 && r3.A05 != null && !r3.A01.isFinishing()) {
            r3.A05.show();
        } else if (r2.A00() == 3 && r3.A06 != null && !r3.A01.isFinishing()) {
            r3.A06.show();
        }
    }
}
