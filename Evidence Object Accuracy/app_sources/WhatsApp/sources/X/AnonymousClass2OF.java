package X;

/* renamed from: X.2OF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OF extends AnonymousClass1JW {
    public final String A00;
    public final byte[] A01;

    public AnonymousClass2OF(AbstractC15710nm r1, C15450nH r2, String str, byte[] bArr) {
        super(r1, r2);
        this.A01 = bArr;
        this.A00 = str;
    }
}
