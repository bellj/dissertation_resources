package X;

/* renamed from: X.06t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC014206t extends AbstractC014106p {
    public int A00;
    public int A01 = 0;
    public String A02;
    public C014506w[] A03 = null;

    public boolean A02() {
        return false;
    }

    public AbstractC014206t() {
    }

    public AbstractC014206t(AbstractC014206t r7) {
        C014506w[] r4;
        this.A02 = r7.A02;
        this.A00 = r7.A00;
        C014506w[] r5 = r7.A03;
        if (r5 == null) {
            r4 = null;
        } else {
            int length = r5.length;
            r4 = new C014506w[length];
            for (int i = 0; i < length; i++) {
                r4[i] = new C014506w(r5[i]);
            }
        }
        this.A03 = r4;
    }

    public C014506w[] getPathData() {
        return this.A03;
    }

    public String getPathName() {
        return this.A02;
    }

    public void setPathData(C014506w[] r6) {
        C014506w[] r4;
        C014506w[] r42 = this.A03;
        if (!C014306u.A01(r42, r6)) {
            if (r6 == null) {
                r4 = null;
            } else {
                int length = r6.length;
                r4 = new C014506w[length];
                for (int i = 0; i < length; i++) {
                    r4[i] = new C014506w(r6[i]);
                }
            }
            this.A03 = r4;
            return;
        }
        for (int i2 = 0; i2 < r6.length; i2++) {
            r42[i2].A00 = r6[i2].A00;
            for (int i3 = 0; i3 < r6[i2].A01.length; i3++) {
                r42[i2].A01[i3] = r6[i2].A01[i3];
            }
        }
    }
}
