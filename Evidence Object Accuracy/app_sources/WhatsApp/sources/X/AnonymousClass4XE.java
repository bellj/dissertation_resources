package X;

import android.net.Uri;
import java.util.Arrays;

/* renamed from: X.4XE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XE {
    public final int A00 = -1;
    public final int[] A01 = new int[0];
    public final long[] A02 = new long[0];
    public final Uri[] A03 = new Uri[0];

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass4XE.class != obj.getClass()) {
                return false;
            }
            AnonymousClass4XE r5 = (AnonymousClass4XE) obj;
            if (this.A00 != r5.A00 || !Arrays.equals(this.A03, r5.A03) || !Arrays.equals(this.A01, r5.A01) || !Arrays.equals(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A00 * 31) + Arrays.hashCode(this.A03)) * 31) + Arrays.hashCode(this.A01)) * 31) + Arrays.hashCode(this.A02);
    }
}
