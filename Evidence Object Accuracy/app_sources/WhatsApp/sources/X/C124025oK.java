package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.util.List;

/* renamed from: X.5oK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124025oK extends AbstractC16350or {
    public final /* synthetic */ AbstractActivityC121525iS A00;

    public C124025oK(AbstractActivityC121525iS r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        String str;
        List list;
        C28861Ph A2f;
        C30921Zi r8;
        AnonymousClass1KS r17;
        AbstractC15340mz r25;
        Integer num;
        C30921Zi r14;
        AbstractActivityC121525iS r0 = this.A00;
        PaymentView A2e = r0.A2e();
        if (A2e == null || A2e.getStickerIfSelected() == null) {
            C18610sj r3 = ((AbstractActivityC121685jC) r0).A0M;
            if (!(r0 instanceof IndiaUpiSendPaymentActivity)) {
                IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = (IndiaUpiCheckOrderDetailsActivity) r0;
                C20320vZ r4 = ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A0b;
                AbstractC14640lm r6 = ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A0E;
                C15650ng r1 = ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A09;
                A2f = r4.A03(null, r6, r1.A0K.A03(indiaUpiCheckOrderDetailsActivity.A0D), "", null, 0, false);
            } else {
                PaymentView paymentView = r0.A0W;
                if (paymentView != null) {
                    str = paymentView.getPaymentNote();
                } else {
                    str = "";
                }
                PaymentView paymentView2 = r0.A0W;
                if (paymentView2 != null) {
                    list = paymentView2.getMentionedJids();
                } else {
                    list = null;
                }
                A2f = r0.A2f(str, list);
            }
            C30821Yy r5 = r0.A0A;
            AbstractC30791Yv r42 = r0.A09;
            AbstractC28901Pl r62 = r0.A0B;
            C119835fB r7 = r0.A0G;
            String str2 = ((AbstractActivityC121685jC) r0).A0l;
            String str3 = ((AbstractActivityC121685jC) r0).A0n;
            PaymentView paymentView3 = r0.A0W;
            if (paymentView3 != null) {
                r8 = paymentView3.getPaymentBackground();
            } else {
                r8 = null;
            }
            return r3.A02(r42, r5, r62, r7, r8, A2f, str2, str3, AbstractActivityC121685jC.A1p(r0));
        }
        C129175xI r9 = ((AbstractActivityC121685jC) r0).A0W;
        PaymentView paymentView4 = r0.A0W;
        if (paymentView4 != null) {
            r17 = paymentView4.getStickerIfSelected();
        } else {
            r17 = null;
        }
        AnonymousClass009.A05(r17);
        AnonymousClass1KC r15 = r0.A0D;
        String str4 = r0.A0c;
        AbstractC14640lm r43 = ((AbstractActivityC121685jC) r0).A0E;
        AnonymousClass009.A05(r43);
        UserJid userJid = ((AbstractActivityC121685jC) r0).A0G;
        long j = ((AbstractActivityC121685jC) r0).A02;
        if (j != 0) {
            r25 = ((AbstractActivityC121685jC) r0).A09.A0K.A00(j);
        } else {
            r25 = null;
        }
        PaymentView paymentView5 = r0.A0W;
        if (paymentView5 != null) {
            num = paymentView5.getStickerSendOrigin();
        } else {
            num = null;
        }
        C30821Yy r11 = r0.A0A;
        AbstractC30791Yv r10 = r0.A09;
        AbstractC28901Pl r12 = r0.A0B;
        C119835fB r13 = r0.A0G;
        String str5 = ((AbstractActivityC121685jC) r0).A0l;
        String str6 = ((AbstractActivityC121685jC) r0).A0n;
        if (paymentView5 != null) {
            r14 = paymentView5.getPaymentBackground();
        } else {
            r14 = null;
        }
        return r9.A00(r10, r11, r12, r13, r14, r15, r9.A04.A02(r43, userJid, r25, r17, num), r17, str4, str5, str6, AbstractActivityC121685jC.A1p(r0));
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1IR r8 = (AnonymousClass1IR) obj;
        AbstractActivityC121525iS r5 = this.A00;
        if (!r5.A0u) {
            ((AbstractActivityC121545iU) r5).A0B.A04(123, 2);
            r5.AaN();
            r5.A2q();
            r5.A2g(0);
        } else if (r8 == null) {
            int A00 = AnonymousClass69E.A00(((AbstractActivityC121545iU) r5).A06, 0);
            C121265hX r4 = ((AbstractActivityC121545iU) r5).A0B;
            r4.A03(123, "error_code", (long) new C452120p(A00).A00);
            r4.A04(123, 3);
            r5.A39();
        } else {
            ((AbstractActivityC121545iU) r5).A0B.A04(123, 2);
            r5.A3S(r8, true);
        }
    }
}
