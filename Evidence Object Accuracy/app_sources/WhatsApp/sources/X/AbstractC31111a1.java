package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.1a1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC31111a1 {
    public final AbstractC15710nm A00;
    public final C15450nH A01;
    public final C16630pM A02;
    public final Set A03 = new HashSet();

    public void A01() {
    }

    public AbstractC31111a1(AbstractC15710nm r2, C15450nH r3, C16630pM r4) {
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
    }

    public C31091Zz A00(byte[] bArr) {
        C31091Zz A00 = ((C31101a0) this).A00.A00(AnonymousClass029.A0M, bArr);
        if (A00 == null) {
            StringBuilder sb = new StringBuilder("EncryptedKeyHelperAESPassword/");
            sb.append("crypto issue on encryption");
            Log.e(sb.toString());
        }
        return A00;
    }

    public void A02(String str, Throwable th) {
        Log.e("EncryptedKeyHelper/reportKeystoreCriticalException/", th);
        SharedPreferences A01 = this.A02.A01("keystore");
        long j = A01.getLong("client_static_keypair_enc_success", 0);
        long j2 = A01.getLong("client_static_keypair_enc_failed", 0);
        AbstractC15710nm r5 = this.A00;
        StringBuilder sb = new StringBuilder("keystore-error-");
        sb.append(str);
        sb.append("-");
        sb.append(th.getClass().getSimpleName());
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(th.getMessage());
        sb2.append(String.format(Locale.US, " KS Stats OK/KO: %d/%d", Long.valueOf(j), Long.valueOf(j2)));
        r5.A02(obj, sb2.toString(), th);
    }

    public byte[] A03(EnumC31131a3 r4, C31091Zz r5) {
        byte[] A01 = ((C31101a0) this).A00.A01(r5, AnonymousClass029.A0M);
        if (A01 == null) {
            StringBuilder sb = new StringBuilder("EncryptedKeyHelperAESPassword/");
            sb.append("crypto issue on decryption while ");
            sb.append(r4.toString());
            Log.e(sb.toString());
        }
        return A01;
    }
}
