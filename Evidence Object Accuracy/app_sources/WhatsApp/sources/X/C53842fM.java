package X;

import android.app.Application;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.2fM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53842fM extends AnonymousClass014 {
    public Integer A00;
    public boolean A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final AnonymousClass016 A05;
    public final AnonymousClass016 A06;
    public final AnonymousClass016 A07 = C12980iv.A0T();
    public final AnonymousClass016 A08;
    public final AnonymousClass016 A09;
    public final AnonymousClass016 A0A;
    public final C15570nT A0B;
    public final C19850um A0C;
    public final C90094Mo A0D;
    public final C25811Ax A0E;
    public final AnonymousClass19Q A0F;
    public final AnonymousClass19T A0G;
    public final AnonymousClass3EX A0H;
    public final C25801Aw A0I;
    public final C14830m7 A0J;
    public final C14820m6 A0K;
    public final C14850m9 A0L;
    public final UserJid A0M;
    public final C19840ul A0N;
    public final C25681Ai A0O;
    public final C27691It A0P;

    public C53842fM(Application application, C15570nT r5, C19850um r6, C90094Mo r7, C25811Ax r8, AnonymousClass19Q r9, AnonymousClass19T r10, AnonymousClass3EX r11, C25801Aw r12, C14830m7 r13, C14820m6 r14, C14850m9 r15, UserJid userJid, C19840ul r17, C25681Ai r18) {
        super(application);
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A05 = A0T;
        this.A0A = C12980iv.A0T();
        this.A09 = C12980iv.A0T();
        this.A0P = C13000ix.A03();
        this.A08 = C12980iv.A0T();
        this.A06 = C12980iv.A0T();
        this.A0J = r13;
        this.A0L = r15;
        this.A0M = userJid;
        this.A0B = r5;
        this.A0H = r11;
        this.A0N = r17;
        this.A0G = r10;
        this.A0C = r6;
        this.A0I = r12;
        this.A0F = r9;
        this.A0E = r8;
        this.A0O = r18;
        this.A0K = r14;
        this.A0D = r7;
        this.A04 = application.getResources().getDimensionPixelSize(R.dimen.product_catalog_list_thumb_size);
        this.A03 = application.getResources().getDimensionPixelSize(R.dimen.category_entry_point_thumbnail_size);
        r11.A00 = A0T;
    }

    public void A04(UserJid userJid) {
        boolean A0F = this.A0B.A0F(userJid);
        C14850m9 r1 = this.A0L;
        int i = 582;
        if (A0F) {
            i = 451;
        }
        if (r1.A07(i)) {
            AnonymousClass19T r7 = this.A0G;
            int i2 = this.A04;
            int A03 = C12980iv.A03(r7.A08.A0F(userJid) ? 1 : 0) << 2;
            C19850um r8 = r7.A0C;
            synchronized (r8) {
                C44661zL r3 = (C44661zL) r8.A01.get(userJid);
                if (r3 != null) {
                    r3.A00 = new C44711zQ(null, true);
                    List list = r3.A03;
                    int size = list.size();
                    if (size > A03) {
                        for (int i3 = A03; i3 < size; i3++) {
                            list.remove(C12980iv.A0C(list));
                        }
                    }
                }
            }
            List A07 = r8.A07(userJid);
            if (!A07.isEmpty()) {
                r7.A03.A0A(new C84593zZ(new C90854Pm(A07, true, true), userJid));
                A03 <<= 1;
            }
            r7.A04(userJid, i2, A03, true);
        }
    }

    public void A05(UserJid userJid) {
        boolean A0F = this.A0B.A0F(userJid);
        C14850m9 r1 = this.A0L;
        int i = 582;
        if (A0F) {
            i = 451;
        }
        if (r1.A07(i)) {
            A04(userJid);
        }
        this.A0G.A03(userJid, this.A04);
    }

    public void A06(UserJid userJid) {
        C90094Mo r4 = this.A0D;
        C14850m9 r1 = r4.A01;
        if (r1.A07(1514)) {
            C25801Aw r3 = this.A0I;
            if (r3.A04(userJid, "catalog_category_dummy_root_id")) {
                this.A08.A0B(r3.A01(userJid, "catalog_category_dummy_root_id"));
                return;
            }
            C89194Ja r2 = new C89194Ja(this);
            if (r1.A07(1514)) {
                C14650lo r12 = r4.A00;
                if (r12.A08()) {
                    r12.A03(new AbstractC30111Wd(r2, userJid) { // from class: X.3Uq
                        public final /* synthetic */ C89194Ja A01;
                        public final /* synthetic */ UserJid A02;

                        {
                            this.A01 = r2;
                            this.A02 = r3;
                        }

                        @Override // X.AbstractC30111Wd
                        public final void ANP(C30141Wg r10) {
                            C89194Ja r13 = this.A01;
                            UserJid userJid2 = this.A02;
                            if (r10 != null && r10.A0H) {
                                C53842fM r22 = r13.A00;
                                HashSet A12 = C12970iu.A12();
                                String str = r22.A0F.A00;
                                AnonymousClass19T r14 = r22.A0G;
                                int i = r22.A03;
                                r14.A00(new C68293Uv(r22, userJid2), new C92404Vt(userJid2, str, A12, i, i));
                            }
                        }
                    }, userJid);
                }
            }
        }
    }
}
