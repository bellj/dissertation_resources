package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2xu  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xu extends AnonymousClass1OY {
    public boolean A00;
    public final TextView A01;
    public final AnonymousClass01F A02;

    public AnonymousClass2xu(Context context, AnonymousClass01F r4, AbstractC13890kV r5, AnonymousClass1XB r6) {
        super(context, r5, r6);
        A0Z();
        this.A02 = r4;
        setClickable(false);
        setLongClickable(false);
        TextView A0I = C12960it.A0I(getRootView(), R.id.info);
        this.A01 = A0I;
        AnonymousClass1OY.A0D(context, this, A0I);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public final void A1M() {
        String string = getContext().getString(R.string.kic_system_message);
        Drawable A06 = AnonymousClass1OY.A06(this);
        TextView textView = this.A01;
        textView.setText(C52252aV.A01(textView.getPaint(), A06, string));
        C12960it.A0y(textView, this, 34);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public C32271bt getFMessage() {
        return (C32271bt) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C32271bt);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
