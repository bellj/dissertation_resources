package X;

/* renamed from: X.3JQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JQ {
    public static final C47762Cm A00 = new C47762Cm();

    public static float A00(AnonymousClass28D r1, float f, int i, boolean z) {
        String A0I = r1.A0I(i);
        if (A0I == null || (z && A0I.equals("auto"))) {
            return Float.NaN;
        }
        try {
            if (A0I.endsWith("%")) {
                return AnonymousClass3JW.A00(A0I) * 0.01f * f;
            }
            return AnonymousClass3JW.A01(A0I);
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing flexbox style value", e);
            return Float.NaN;
        }
    }

    public static float A01(AnonymousClass28D r2, int i) {
        String A0I = r2.A0I(i);
        if (A0I == null) {
            return Float.NaN;
        }
        try {
            return AnonymousClass3JW.A01(A0I);
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing padding value", e);
            return Float.NaN;
        }
    }

    public static AnonymousClass28D A02(AnonymousClass28D r2) {
        if (r2 == null || AnonymousClass28D.A05(r2) == null || AnonymousClass28D.A05(r2).A01 != 13368) {
            return null;
        }
        return AnonymousClass28D.A05(r2);
    }

    public static void A03(AnonymousClass39w r5, C94874cg r6, String str) {
        float A01;
        float[] fArr;
        int i;
        try {
            if (str.endsWith("%")) {
                A01 = AnonymousClass3JW.A00(str);
                if (Float.compare(A01, Float.NaN) != 0) {
                    C94874cg.A00(r6, 3);
                    fArr = r6.A01;
                    int i2 = r6.A00;
                    int i3 = i2 + 1;
                    r6.A00 = i3;
                    fArr[i2] = (float) 8;
                    i = i3 + 1;
                    r6.A00 = i;
                    fArr[i3] = (float) r5.mIntValue;
                } else {
                    return;
                }
            } else {
                A01 = AnonymousClass3JW.A01(str);
                if (Float.compare(A01, Float.NaN) != 0) {
                    C94874cg.A00(r6, 3);
                    fArr = r6.A01;
                    int i4 = r6.A00;
                    int i5 = i4 + 1;
                    r6.A00 = i5;
                    fArr[i4] = (float) 7;
                    i = i5 + 1;
                    r6.A00 = i;
                    fArr[i5] = (float) r5.mIntValue;
                } else {
                    return;
                }
            }
            r6.A00 = i + 1;
            fArr[i] = A01;
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing padding value", e);
        }
    }

    public static void A04(AnonymousClass39w r5, C47762Cm r6, String str) {
        float A01;
        float[] fArr;
        int i;
        try {
            if (str.endsWith("%")) {
                A01 = AnonymousClass3JW.A00(str);
                if (!C47762Cm.A01(A01)) {
                    C47762Cm.A00(r6, 3);
                    fArr = r6.A01;
                    int i2 = r6.A00;
                    int i3 = i2 + 1;
                    r6.A00 = i3;
                    fArr[i2] = (float) 25;
                    i = i3 + 1;
                    r6.A00 = i;
                    fArr[i3] = (float) r5.mIntValue;
                } else {
                    return;
                }
            } else if (str.equals("auto")) {
                float[] A1Y = C12980iv.A1Y(r6);
                int i4 = r6.A00;
                int i5 = i4 + 1;
                r6.A00 = i5;
                A1Y[i4] = (float) 26;
                r6.A00 = i5 + 1;
                A1Y[i5] = (float) r5.mIntValue;
                return;
            } else {
                A01 = AnonymousClass3JW.A01(str);
                if (!C47762Cm.A01(A01)) {
                    C47762Cm.A00(r6, 3);
                    fArr = r6.A01;
                    int i6 = r6.A00;
                    int i7 = i6 + 1;
                    r6.A00 = i7;
                    fArr[i6] = (float) 24;
                    i = i7 + 1;
                    r6.A00 = i;
                    fArr[i7] = (float) r5.mIntValue;
                } else {
                    return;
                }
            }
            r6.A00 = i + 1;
            fArr[i] = A01;
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing margin value", e);
        }
    }

    public static void A05(AnonymousClass39w r5, C47762Cm r6, String str) {
        float A01;
        float[] fArr;
        int i;
        try {
            if (str.endsWith("%")) {
                A01 = AnonymousClass3JW.A00(str);
                if (!C47762Cm.A01(A01)) {
                    C47762Cm.A00(r6, 3);
                    fArr = r6.A01;
                    int i2 = r6.A00;
                    int i3 = i2 + 1;
                    r6.A00 = i3;
                    fArr[i2] = (float) 28;
                    i = i3 + 1;
                    r6.A00 = i;
                    fArr[i3] = (float) r5.mIntValue;
                } else {
                    return;
                }
            } else {
                A01 = AnonymousClass3JW.A01(str);
                if (!C47762Cm.A01(A01)) {
                    C47762Cm.A00(r6, 3);
                    fArr = r6.A01;
                    int i4 = r6.A00;
                    int i5 = i4 + 1;
                    r6.A00 = i5;
                    fArr[i4] = (float) 27;
                    i = i5 + 1;
                    r6.A00 = i;
                    fArr[i5] = (float) r5.mIntValue;
                } else {
                    return;
                }
            }
            r6.A00 = i + 1;
            fArr[i] = A01;
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing position value", e);
        }
    }
}
