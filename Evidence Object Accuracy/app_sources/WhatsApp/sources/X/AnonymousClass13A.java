package X;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import com.whatsapp.util.Log;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.13A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13A {
    public final C16590pI A00;
    public final AnonymousClass139 A01;
    public final C15490nL A02;
    public final AnonymousClass138 A03;
    public final AnonymousClass4VR A04;
    public final Executor A05;

    public AnonymousClass13A(C16590pI r4, AnonymousClass139 r5, C15490nL r6, AnonymousClass138 r7, AbstractC14440lR r8) {
        AnonymousClass1MO r2 = new AnonymousClass1MO(r8, 5, false);
        AnonymousClass4VR r0 = new AnonymousClass4VR(r4, new C15500nM(r4, new AnonymousClass200()));
        this.A00 = r4;
        this.A03 = r7;
        this.A02 = r6;
        this.A01 = r5;
        this.A05 = r2;
        this.A04 = r0;
    }

    public final void A00(String str, String str2, boolean z) {
        try {
            if (this.A02.A01(str2).A03) {
                Intent intent = new Intent("com.whatsapp.action.INSTRUMENTATION_CALLBACK_SERVICE").setPackage(str2);
                try {
                    AnonymousClass4VR r0 = this.A04;
                    String str3 = AnonymousClass01V.A09;
                    List<ResolveInfo> queryIntentServices = r0.A00.queryIntentServices(intent, 0);
                    if (!queryIntentServices.isEmpty()) {
                        if (queryIntentServices.size() <= 1) {
                            ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
                            if (serviceInfo == null || !str3.equals(serviceInfo.permission)) {
                                StringBuilder sb = new StringBuilder("Service not protected by permission ");
                                sb.append(str3);
                                throw new SecurityException(sb.toString());
                            }
                        } else {
                            StringBuilder sb2 = new StringBuilder("Multiple services can handle this intent ");
                            sb2.append(intent.getAction());
                            throw new SecurityException(sb2.toString());
                        }
                    }
                    if (!this.A00.A00.bindService(intent, new ServiceConnectionC470428s(this, str, str2, z), 1)) {
                        Log.w("CallbackServiceProxy/Failed to bind to stella service");
                        return;
                    }
                    return;
                } catch (Throwable th) {
                    Log.e("CallbackServiceProxy/Failed to bind to stella service", th);
                    return;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        Log.w("CallbackServiceProxy/verification failed, dropping event");
    }
}
