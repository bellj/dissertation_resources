package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75523jz extends AnonymousClass03U {
    public ImageView A00;
    public TextView A01;
    public final /* synthetic */ AnonymousClass1KX A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75523jz(View view, AnonymousClass1KX r3) {
        super(view);
        this.A02 = r3;
        this.A01 = C12960it.A0J(view, R.id.no_results);
        this.A00 = C12970iu.A0L(view, R.id.shrug_emoji);
    }
}
