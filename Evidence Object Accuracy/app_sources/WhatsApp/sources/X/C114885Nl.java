package X;

import java.io.EOFException;
import java.io.InputStream;

/* renamed from: X.5Nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114885Nl extends AnonymousClass49A {
    public int A00;
    public int A01;
    public boolean A02 = true;
    public boolean A03 = false;

    public C114885Nl(InputStream inputStream, int i) {
        super(inputStream, i);
        this.A00 = inputStream.read();
        int read = inputStream.read();
        this.A01 = read;
        if (read >= 0) {
            A01();
            return;
        }
        throw new EOFException();
    }

    public final boolean A01() {
        if (!this.A03 && this.A02 && this.A00 == 0 && this.A01 == 0) {
            this.A03 = true;
            A00();
        }
        return this.A03;
    }

    @Override // java.io.InputStream
    public int read() {
        if (A01()) {
            return -1;
        }
        int read = super.A01.read();
        if (read >= 0) {
            int i = this.A00;
            this.A00 = this.A01;
            this.A01 = read;
            return i;
        }
        throw new EOFException();
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        if (this.A02 || i2 < 3) {
            return super.read(bArr, i, i2);
        }
        if (this.A03) {
            return -1;
        }
        InputStream inputStream = super.A01;
        int read = inputStream.read(bArr, i + 2, i2 - 2);
        if (read >= 0) {
            bArr[i] = (byte) this.A00;
            bArr[i + 1] = (byte) this.A01;
            this.A00 = inputStream.read();
            int read2 = inputStream.read();
            this.A01 = read2;
            if (read2 >= 0) {
                return read + 2;
            }
            throw new EOFException();
        }
        throw new EOFException();
    }
}
