package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.04Y  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass04Y {
    public long A00 = 120;
    public long A01 = 250;
    public long A02 = 250;
    public long A03 = 120;
    public AbstractC11350g9 A04 = null;
    public ArrayList A05 = new ArrayList();

    public abstract void A08();

    public abstract void A09();

    public abstract void A0A(AnonymousClass03U v);

    public abstract boolean A0B();

    public static void A00(AnonymousClass03U r3) {
        int i = r3.A00;
        int i2 = i & 14;
        if ((i & 4) == 0 && (i2 & 4) == 0) {
            int i3 = r3.A03;
            r3.A00();
        }
    }

    public AnonymousClass0N0 A01(AnonymousClass03U r4) {
        AnonymousClass0N0 r2 = new AnonymousClass0N0();
        View view = r4.A0H;
        r2.A00 = view.getLeft();
        r2.A01 = view.getTop();
        view.getRight();
        view.getBottom();
        return r2;
    }

    public final void A02() {
        ArrayList arrayList = this.A05;
        if (0 < arrayList.size()) {
            arrayList.get(0);
            throw new NullPointerException("onAnimationsFinished");
        } else {
            arrayList.clear();
        }
    }

    public final void A03(AnonymousClass03U r8) {
        boolean z;
        AbstractC11350g9 r2 = this.A04;
        if (r2 != null) {
            AnonymousClass0Z7 r22 = (AnonymousClass0Z7) r2;
            r8.A05(true);
            if (r8.A0A != null && r8.A0B == null) {
                r8.A0A = null;
            }
            r8.A0B = null;
            if ((r8.A00 & 16) == 0) {
                RecyclerView recyclerView = r22.A00;
                View view = r8.A0H;
                recyclerView.A0Q();
                AnonymousClass0QT r6 = recyclerView.A0K;
                RecyclerView recyclerView2 = ((AnonymousClass0Z0) r6.A01).A00;
                int indexOfChild = recyclerView2.indexOfChild(view);
                if (indexOfChild == -1) {
                    r6.A08(view);
                } else {
                    AnonymousClass0QO r1 = r6.A00;
                    if (r1.A06(indexOfChild)) {
                        r1.A07(indexOfChild);
                        r6.A08(view);
                        View childAt = recyclerView2.getChildAt(indexOfChild);
                        if (childAt != null) {
                            recyclerView2.A0h(childAt);
                            childAt.clearAnimation();
                        }
                        recyclerView2.removeViewAt(indexOfChild);
                    } else {
                        z = false;
                        recyclerView.A0s(!z);
                        if (!z && (r8.A00 & 256) != 0) {
                            recyclerView.removeDetachedView(view, false);
                            return;
                        }
                    }
                }
                z = true;
                AnonymousClass03U A01 = RecyclerView.A01(view);
                AnonymousClass0QS r0 = recyclerView.A0w;
                r0.A09(A01);
                r0.A08(A01);
                recyclerView.A0s(!z);
                if (!z) {
                }
            }
        }
    }

    public long A04() {
        return this.A00;
    }

    public long A05() {
        return this.A01;
    }

    public long A06() {
        return this.A02;
    }

    public long A07() {
        return this.A03;
    }

    public boolean A0C(AnonymousClass03U r3, List list) {
        if (!((AbstractC008704k) this).A00 || (r3.A00 & 4) != 0) {
            return true;
        }
        return false;
    }
}
