package X;

import android.text.Spannable;
import android.text.style.URLSpan;
import android.util.Pair;
import java.util.Iterator;

/* renamed from: X.3Ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63263Ax {
    public static void A00(Spannable spannable, String str) {
        C20920wX A00 = C20920wX.A00();
        Iterator it = new C111895Bf(AnonymousClass39u.A02, A00, spannable, A00.A0F(Integer.parseInt(str))).iterator();
        while (it.hasNext()) {
            AnonymousClass3F5 r0 = (AnonymousClass3F5) it.next();
            int i = r0.A00;
            Pair A0D = C12960it.A0D(Integer.valueOf(i), i + r0.A02.length());
            if (!AnonymousClass3IG.A01(A0D, spannable)) {
                A0D = AnonymousClass3IG.A00(A0D, AnonymousClass3IG.A02, spannable);
                if (AnonymousClass3IG.A01(A0D, spannable)) {
                }
            }
            spannable.setSpan(new URLSpan(C12970iu.A0s(spannable.subSequence(C12960it.A05(A0D.first), C12960it.A05(A0D.second)), C12960it.A0k("tel:"))), C12960it.A05(A0D.first), C12960it.A05(A0D.second), 33);
        }
    }
}
