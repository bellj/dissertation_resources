package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.util.Map;

/* renamed from: X.2Nm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49972Nm implements AbstractC28461Nh {
    public final /* synthetic */ C17150qL A00;
    public final /* synthetic */ File A01;

    @Override // X.AbstractC28461Nh
    public /* synthetic */ void AOt(long j) {
    }

    public C49972Nm(C17150qL r1, File file) {
        this.A00 = r1;
        this.A01 = file;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        String substring;
        if (AnonymousClass1US.A0C(str)) {
            substring = "";
        } else {
            substring = str.substring(0, Math.min(str.length(), 500));
        }
        AbstractC15710nm r5 = this.A00.A00;
        StringBuilder sb = new StringBuilder();
        File file = this.A01;
        sb.append(String.valueOf(file.length()));
        sb.append(":uploadServiceError:");
        sb.append(substring);
        r5.AaV("voip-time-series-upload-fail", sb.toString(), false);
        StringBuilder sb2 = new StringBuilder("app/VoipTimeSeriesLogger: failed upload of ");
        sb2.append(file.getName());
        sb2.append(" with size ");
        sb2.append(file.length());
        sb2.append("reason: ");
        sb2.append(substring);
        Log.w(sb2.toString());
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        AbstractC15710nm r4 = this.A00.A00;
        File file = this.A01;
        r4.AaV("voip-time-series-upload-success", String.valueOf(file.length()), false);
        StringBuilder sb = new StringBuilder("app/VoipTimeSeriesLogger: successful upload of ");
        sb.append(file.getName());
        sb.append(" with size ");
        sb.append(file.length());
        Log.i(sb.toString());
    }
}
