package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3zC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84373zC extends AnonymousClass2Cu {
    public final /* synthetic */ AnonymousClass1PZ A00;

    public C84373zC(AnonymousClass1PZ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        AnonymousClass1PZ r1 = this.A00;
        if (AnonymousClass1PZ.A00(r1, userJid)) {
            r1.A02();
        }
    }
}
