package X;

import com.whatsapp.components.PhoneNumberEntry;

/* renamed from: X.35y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35y extends C469928m {
    public final /* synthetic */ PhoneNumberEntry A00;

    public AnonymousClass35y(PhoneNumberEntry phoneNumberEntry) {
        this.A00 = phoneNumberEntry;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r3 == null) goto L_0x004c;
     */
    @Override // X.C469928m, android.text.TextWatcher
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void afterTextChanged(android.text.Editable r7) {
        /*
            r6 = this;
            java.lang.String r4 = r7.toString()
            com.whatsapp.components.PhoneNumberEntry r5 = r6.A00
            java.lang.String r1 = r5.A07
            if (r1 == 0) goto L_0x005b
            X.0zT r0 = r5.A01
            X.1ND r0 = r0.A01(r1)
            if (r0 != 0) goto L_0x0054
            r0 = 0
        L_0x0013:
            boolean r0 = r4.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x005b
            java.lang.String r3 = r5.A07
        L_0x001b:
            if (r3 == 0) goto L_0x004c
        L_0x001d:
            com.whatsapp.WaEditText r0 = r5.A02
            android.text.Editable r0 = r0.getText()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x004c
            r5.A02(r3)
            com.whatsapp.WaEditText r0 = r5.A03
            java.lang.String r2 = X.C12970iu.A0p(r0)
            java.lang.String r1 = "\\D"
            java.lang.String r0 = ""
            java.lang.String r1 = r2.replaceAll(r1, r0)
            com.whatsapp.WaEditText r0 = r5.A03
            r0.setText(r1)
            com.whatsapp.WaEditText r0 = r5.A02
            boolean r0 = r0.hasFocus()
            if (r0 == 0) goto L_0x004c
            com.whatsapp.WaEditText r0 = r5.A03
            r0.requestFocus()
        L_0x004c:
            X.4WI r0 = r5.A04
            if (r0 == 0) goto L_0x0053
            r0.A01(r4, r3)
        L_0x0053:
            return
        L_0x0054:
            int r0 = r0.A00
            java.lang.String r0 = java.lang.Integer.toString(r0)
            goto L_0x0013
        L_0x005b:
            java.lang.String r3 = r5.A07
            if (r3 == 0) goto L_0x0065
            int r0 = r4.length()
            if (r0 <= 0) goto L_0x001d
        L_0x0065:
            java.lang.String r3 = X.C22650zQ.A00(r4)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass35y.afterTextChanged(android.text.Editable):void");
    }
}
