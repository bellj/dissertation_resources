package X;

import android.graphics.Bitmap;

/* renamed from: X.4v7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106094v7 implements AnonymousClass5YZ {
    @Override // X.AbstractC12900ik, X.AbstractC12240hb
    public void Aa4(Object obj) {
        ((Bitmap) obj).recycle();
    }

    @Override // X.AbstractC12900ik
    public Object get(int i) {
        return Bitmap.createBitmap(1, (int) Math.ceil(((double) i) / 2.0d), Bitmap.Config.RGB_565);
    }
}
