package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VJ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0VJ implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass0E4(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new AnonymousClass0E4(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass0E4[i];
    }
}
