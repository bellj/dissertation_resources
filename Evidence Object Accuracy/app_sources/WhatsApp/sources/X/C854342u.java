package X;

/* renamed from: X.42u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C854342u extends AbstractC16110oT {
    public Boolean A00;

    public C854342u() {
        super(3048, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAdvMetadataCreationFailure {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "advMetadataIsMe", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
