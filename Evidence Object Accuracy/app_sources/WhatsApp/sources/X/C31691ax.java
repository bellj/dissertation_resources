package X;

/* renamed from: X.1ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31691ax extends AbstractC31681aw {
    public static final long serialVersionUID = 0;
    public final Object reference;

    public C31691ax(Object obj) {
        this.reference = obj;
    }

    @Override // X.AbstractC31681aw, java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof C31691ax) {
            return this.reference.equals(((C31691ax) obj).reference);
        }
        return false;
    }

    @Override // X.AbstractC31681aw, java.lang.Object
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @Override // X.AbstractC31681aw, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("Optional.of(");
        sb.append(this.reference);
        sb.append(")");
        return sb.toString();
    }
}
