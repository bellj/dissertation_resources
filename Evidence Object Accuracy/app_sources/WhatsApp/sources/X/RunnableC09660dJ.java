package X;

/* renamed from: X.0dJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09660dJ implements Runnable {
    public final ExecutorC10610eu A00;
    public final Runnable A01;

    public RunnableC09660dJ(ExecutorC10610eu r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A01.run();
        } finally {
            this.A00.A00();
        }
    }
}
