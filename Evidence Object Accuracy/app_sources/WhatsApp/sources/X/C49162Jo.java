package X;

/* renamed from: X.2Jo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C49162Jo {
    public int A00 = -1;
    public EnumC49142Jm A01;
    public AnonymousClass39v A02;
    public C65243It A03;
    public C63623Ch A04;

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.A02);
        sb.append("\n ecLevel: ");
        sb.append(this.A01);
        sb.append("\n version: ");
        sb.append(this.A03);
        sb.append("\n maskPattern: ");
        sb.append(this.A00);
        C63623Ch r1 = this.A04;
        if (r1 == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(r1);
        }
        sb.append(">>\n");
        return sb.toString();
    }
}
