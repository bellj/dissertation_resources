package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1CO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CO {
    public AnonymousClass4T7 A00;
    public final AbstractC15710nm A01;
    public final C16590pI A02;
    public final AnonymousClass018 A03;

    public AnonymousClass1CO(AbstractC15710nm r1, C16590pI r2, AnonymousClass018 r3) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = r3;
    }

    public synchronized AnonymousClass4T7 A00() {
        AnonymousClass4T7 r2;
        r2 = this.A00;
        if (r2 == null) {
            ArrayList arrayList = new ArrayList();
            try {
                InputStream open = this.A02.A00.getAssets().open("directory_tier_4_city_neighbourhood.json");
                byte[] bArr = new byte[open.available()];
                open.read(bArr);
                open.close();
                AnonymousClass4T7 A01 = A01(new JSONObject(new String(bArr, DefaultCrypto.UTF_8)));
                if (A01 != null) {
                    arrayList.addAll(A01.A05);
                }
            } catch (IOException | JSONException e) {
                StringBuilder sb = new StringBuilder("SaoPauloNeighbourhoodManager/loadDirectoryRegionsFromFile Failed to parse neighbourhood json");
                sb.append(e);
                Log.e(sb.toString());
                this.A01.AaV("SaoPauloNeighbourhoodManager/loadDirectoryRegionsFromFile Failed to parse neighbourhood json", "directory_tier_4_city_neighbourhood.json", true);
            }
            Collections.sort(arrayList, new Comparator(Collator.getInstance(AnonymousClass018.A00(this.A03.A00))) { // from class: X.5CZ
                public final /* synthetic */ Collator A00;

                {
                    this.A00 = r1;
                }

                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return this.A00.compare(((AnonymousClass4T7) obj).A04, ((AnonymousClass4T7) obj2).A04);
                }
            });
            r2 = new AnonymousClass4T7("São Paulo", arrayList, -23.550651d, -46.633382d, 2800.0d, 130000.0d);
            this.A00 = r2;
        }
        return r2;
    }

    public final AnonymousClass4T7 A01(JSONObject jSONObject) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("regions");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(A01(optJSONArray.getJSONObject(i)));
                }
            }
            Collections.sort(arrayList, new Comparator(Collator.getInstance(AnonymousClass018.A00(this.A03.A00))) { // from class: X.5Ca
                public final /* synthetic */ Collator A00;

                {
                    this.A00 = r1;
                }

                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return this.A00.compare(((AnonymousClass4T7) obj).A04, ((AnonymousClass4T7) obj2).A04);
                }
            });
            String string = jSONObject.getString("name");
            double d = jSONObject.getDouble("search_radius");
            double optDouble = jSONObject.optDouble("coverage_radius", d);
            JSONObject jSONObject2 = jSONObject.getJSONObject("center");
            return new AnonymousClass4T7(string, arrayList, jSONObject2.getDouble("lat"), jSONObject2.getDouble("lng"), d, optDouble);
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("SaoPauloNeighbourhoodManager/getDirectorySearchRegion Failed to parse neighbourhood json");
            sb.append(e);
            Log.e(sb.toString());
            this.A01.AaV("SaoPauloNeighbourhoodManager/getDirectorySearchRegion Failed to parse neighbourhood json", null, true);
            return null;
        }
    }
}
