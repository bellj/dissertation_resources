package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.search.views.itemviews.AudioPlayerMetadataView;
import com.whatsapp.search.views.itemviews.AudioPlayerView;
import com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView;

/* renamed from: X.1y8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44061y8 extends AbstractC44071y9 {
    public C50532Px A00;
    public C15570nT A01;
    public C15550nR A02;
    public C21270x9 A03;
    public AnonymousClass19D A04;
    public AnonymousClass018 A05;
    public C239913u A06;
    public C63663Cl A07;
    public AudioPlayerMetadataView A08 = ((AudioPlayerMetadataView) AnonymousClass028.A0D(this, R.id.search_row_voice_note_metadata));
    public AudioPlayerView A09 = ((AudioPlayerView) AnonymousClass028.A0D(this, R.id.search_row_voice_note_controls));
    public VoiceNoteProfileAvatarView A0A = ((VoiceNoteProfileAvatarView) AnonymousClass028.A0D(this, R.id.search_row_voice_note_preview));
    public AnonymousClass01H A0B;
    public boolean A0C;
    public boolean A0D;
    public final AnonymousClass1J1 A0E;

    public C44061y8(Context context) {
        super(context);
        A00();
        this.A0E = this.A03.A03(context, "attachment-voice-note-audio-view");
        setOrientation(0);
        setGravity(16);
        LinearLayout.inflate(context, R.layout.search_attachment_voice_note, this);
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.search_attachment_background);
        AnonymousClass009.A05(A04);
        setBackground(AnonymousClass2GE.A04(A04, AnonymousClass00T.A00(getContext(), R.color.search_attachment_background)));
        C863346u r4 = new C863346u(this);
        AnonymousClass59F r3 = new AbstractC116165Uj() { // from class: X.59F
            @Override // X.AbstractC116165Uj
            public final C30421Xi ACs() {
                return ((AbstractC44071y9) C44061y8.this).A09;
            }
        };
        AudioPlayerView audioPlayerView = this.A09;
        audioPlayerView.setPlaybackListener(new AnonymousClass3OU(super.A03, audioPlayerView, r3, r4, this.A0B));
        boolean A07 = super.A05.A07(1316);
        this.A0D = A07;
        if (A07) {
            C50532Px r0 = this.A00;
            VoiceNoteProfileAvatarView voiceNoteProfileAvatarView = this.A0A;
            AnonymousClass01J r1 = r0.A00.A04;
            this.A07 = new C63663Cl((AnonymousClass19D) r1.ABo.get(), (C14820m6) r1.AN3.get(), voiceNoteProfileAvatarView);
            this.A0A.setOnFastPlaybackButtonClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 49));
        }
    }

    public final void A02() {
        AnonymousClass55J r8 = new AnonymousClass5U0() { // from class: X.55J
            @Override // X.AnonymousClass5U0
            public final void APZ(int i) {
                C44061y8 r0 = C44061y8.this;
                r0.A08.setDescription(C38131nZ.A04(r0.A05, (long) i));
            }
        };
        AnonymousClass55M r9 = new AnonymousClass5U1() { // from class: X.55M
            @Override // X.AnonymousClass5U1
            public final void AW0(boolean z) {
                View findViewById = AnonymousClass12P.A00(C44061y8.this.getContext()).findViewById(R.id.proximity_overlay);
                if (findViewById != null) {
                    int i = 4;
                    if (z) {
                        i = 0;
                    }
                    findViewById.setVisibility(i);
                }
            }
        };
        AudioPlayerView audioPlayerView = this.A09;
        C60932z2 r0 = new C60932z2(r8, r9, r9, this, audioPlayerView);
        C14850m9 r3 = super.A05;
        C30421Xi r4 = super.A09;
        AnonymousClass59A r5 = new AbstractC116135Ug() { // from class: X.59A
            @Override // X.AbstractC116135Ug
            public final void ATp(int i, String str) {
                C44061y8.this.A08.setDescription(str);
            }
        };
        AnonymousClass3JG.A01(r0, super.A03, this.A05, r3, r4, r5, audioPlayerView);
    }
}
