package X;

/* renamed from: X.0SB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SB {
    public static final float[] A01 = {1.0f, 0.1f, 0.01f, 0.001f, 1.0E-4f, 1.0E-5f, 1.0E-6f, 1.0E-7f, 1.0E-8f, 1.0E-9f, 1.0E-10f, 1.0E-11f, 1.0E-12f, 1.0E-13f, 1.0E-14f, 1.0E-15f, 1.0E-16f, 1.0E-17f, 1.0E-18f, 1.0E-19f, 1.0E-20f, 1.0E-21f, 1.0E-22f, 1.0E-23f, 1.0E-24f, 1.0E-25f, 1.0E-26f, 1.0E-27f, 1.0E-28f, 1.0E-29f, 1.0E-30f, 1.0E-31f, 1.0E-32f, 1.0E-33f, 1.0E-34f, 1.0E-35f, 1.0E-36f, 1.0E-37f, 1.0E-38f};
    public static final float[] A02 = {1.0f, 10.0f, 100.0f, 1000.0f, 10000.0f, 100000.0f, 1000000.0f, 1.0E7f, 1.0E8f, 1.0E9f, 1.0E10f, 9.9999998E10f, 1.0E12f, 9.9999998E12f, 1.0E14f, 9.9999999E14f, 1.00000003E16f, 9.9999998E16f, 9.9999998E17f, 1.0E19f, 1.0E20f, 1.0E21f, 1.0E22f, 1.0E23f, 1.0E24f, 1.0E25f, 1.0E26f, 1.0E27f, 1.0E28f, 1.0E29f, 1.0E30f, 1.0E31f, 1.0E32f, 1.0E33f, 1.0E34f, 1.0E35f, 1.0E36f, 1.0E37f, 1.0E38f};
    public int A00;

    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        if (r14 == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0087, code lost:
        if (r23.A00 != (r0 + 1)) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0089, code lost:
        return Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008a, code lost:
        if (r8 != 0) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008c, code lost:
        if (r17 == 0) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008e, code lost:
        r8 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x008f, code lost:
        if (r14 == false) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0091, code lost:
        r5 = (r0 - r17) - r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0095, code lost:
        r0 = r23.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0097, code lost:
        if (r0 >= r26) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0099, code lost:
        r3 = r24.charAt(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x009f, code lost:
        if (r3 == 'E') goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a3, code lost:
        if (r3 != 'e') goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a5, code lost:
        r0 = r23.A00 + 1;
        r23.A00 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ab, code lost:
        if (r0 == r26) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ad, code lost:
        r3 = r24.charAt(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b3, code lost:
        if (r3 == '+') goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00b7, code lost:
        if (r3 == '-') goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b9, code lost:
        switch(r3) {
            case 48: goto L_0x00f1;
            case 49: goto L_0x00f1;
            case 50: goto L_0x00f1;
            case 51: goto L_0x00f1;
            case 52: goto L_0x00f1;
            case 53: goto L_0x00f1;
            case 54: goto L_0x00f1;
            case 55: goto L_0x00f1;
            case 56: goto L_0x00f1;
            case 57: goto L_0x00f1;
            default: goto L_0x00bc;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00bc, code lost:
        r23.A00--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00c2, code lost:
        r8 = r8 + r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c5, code lost:
        if (r8 > 39) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c9, code lost:
        if (r8 < -44) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00cb, code lost:
        r4 = (float) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ce, code lost:
        if (r1 == 0) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00d0, code lost:
        if (r5 <= 0) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00d2, code lost:
        r0 = X.AnonymousClass0SB.A02[r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d6, code lost:
        r4 = r4 * r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00d7, code lost:
        if (r20 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00da, code lost:
        return -r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00db, code lost:
        if (r5 >= 0) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00df, code lost:
        if (r5 >= -38) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00e1, code lost:
        r4 = (float) (((double) r4) * 1.0E-20d);
        r5 = r5 + 20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00eb, code lost:
        r0 = X.AnonymousClass0SB.A01[-r5];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00f1, code lost:
        r14 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00f3, code lost:
        r14 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00f5, code lost:
        r14 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f6, code lost:
        r23.A00++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00fc, code lost:
        r4 = r23.A00;
        r0 = r4;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0100, code lost:
        if (r0 >= r26) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0102, code lost:
        r13 = r24.charAt(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0108, code lost:
        if (r13 < '0') goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x010a, code lost:
        if (r13 > '9') goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x010f, code lost:
        if (((long) r3) > 922337203685477580L) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0111, code lost:
        r3 = (r3 * 10) + (r13 - '0');
        r0 = r23.A00 + 1;
        r23.A00 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x011f, code lost:
        if (r23.A00 == r4) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0121, code lost:
        if (r14 == false) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0123, code lost:
        r5 = r5 - r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0125, code lost:
        r5 = r5 + r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float A00(java.lang.String r24, int r25, int r26) {
        /*
        // Method dump skipped, instructions count: 320
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SB.A00(java.lang.String, int, int):float");
    }
}
