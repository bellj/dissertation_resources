package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.192  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass192 {
    public C626037w A00;
    public boolean A01;
    public final C18640sm A02;
    public final C14830m7 A03;
    public final AnonymousClass018 A04;
    public final AnonymousClass196 A05;
    public final AnonymousClass195 A06;
    public final AnonymousClass197 A07;
    public final C16120oU A08;
    public final AbstractC231810r A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass192(C18640sm r1, C14830m7 r2, AnonymousClass018 r3, AnonymousClass196 r4, AnonymousClass195 r5, AnonymousClass197 r6, C16120oU r7, AbstractC231810r r8, AbstractC14440lR r9) {
        this.A03 = r2;
        this.A0A = r9;
        this.A08 = r7;
        this.A04 = r3;
        this.A09 = r8;
        this.A02 = r1;
        this.A06 = r5;
        this.A05 = r4;
        this.A07 = r6;
    }

    public C93724ad A00() {
        String string = this.A06.A00.A00.getString("emoji_dictionary_info", null);
        if (string == null) {
            return new C93724ad();
        }
        try {
            C93724ad r6 = new C93724ad();
            JSONObject jSONObject = new JSONObject(string);
            r6.A04 = jSONObject.optString("request_etag", null);
            r6.A00 = jSONObject.optLong("cache_fetch_time", 0);
            r6.A03 = jSONObject.optString("language", null);
            r6.A01 = jSONObject.optLong("last_fetch_attempt_time", 0);
            r6.A05 = jSONObject.optString("language_attempted_to_fetch", null);
            return r6;
        } catch (JSONException unused) {
            return new C93724ad();
        }
    }

    public boolean A01(C93724ad r5) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("request_etag", r5.A04);
            jSONObject.put("language", r5.A03);
            jSONObject.put("cache_fetch_time", r5.A00);
            jSONObject.put("last_fetch_attempt_time", r5.A01);
            jSONObject.put("language_attempted_to_fetch", r5.A05);
            this.A06.A00.A00.edit().putString("emoji_dictionary_info", jSONObject.toString()).apply();
            return true;
        } catch (JSONException unused) {
            return false;
        }
    }
}
