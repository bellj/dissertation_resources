package X;

import android.graphics.drawable.Drawable;
import android.widget.SeekBar;
import com.whatsapp.settings.chat.wallpaper.WallpaperCurrentPreviewActivity;

/* renamed from: X.3OS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OS implements SeekBar.OnSeekBarChangeListener {
    public final /* synthetic */ WallpaperCurrentPreviewActivity A00;

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public AnonymousClass3OS(WallpaperCurrentPreviewActivity wallpaperCurrentPreviewActivity) {
        this.A00 = wallpaperCurrentPreviewActivity;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        WallpaperCurrentPreviewActivity wallpaperCurrentPreviewActivity;
        AnonymousClass2JR r0;
        Drawable drawable;
        if (seekBar != null && z && (r0 = (wallpaperCurrentPreviewActivity = this.A00).A09) != null && (drawable = r0.A00) != null) {
            AnonymousClass2JP.A03(wallpaperCurrentPreviewActivity, drawable, seekBar.getProgress());
            wallpaperCurrentPreviewActivity.A0A.setImageDrawable(drawable);
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        C33191db A0D;
        if (seekBar != null) {
            int progress = seekBar.getProgress();
            WallpaperCurrentPreviewActivity wallpaperCurrentPreviewActivity = this.A00;
            AbstractC14640lm A01 = AbstractC14640lm.A01(wallpaperCurrentPreviewActivity.getIntent().getStringExtra("chat_jid"));
            AbstractC15850o0 r4 = wallpaperCurrentPreviewActivity.A08;
            if (r4 instanceof C252218o) {
                C252218o r42 = (C252218o) r4;
                boolean A08 = C41691tw.A08(wallpaperCurrentPreviewActivity);
                boolean z = true;
                if (A01 == null || (A0D = r42.A08.AHh(A01, A08)) == null) {
                    A0D = r42.A0D(wallpaperCurrentPreviewActivity, A08);
                } else {
                    z = false;
                }
                Object obj = new AnonymousClass01T(A0D, Boolean.valueOf(z)).A00;
                AnonymousClass009.A05(obj);
                C33191db r0 = (C33191db) obj;
                r42.A0F(wallpaperCurrentPreviewActivity, A01, new C33191db(Integer.valueOf(progress), r0.A01, r0.A02));
            }
        }
    }
}
