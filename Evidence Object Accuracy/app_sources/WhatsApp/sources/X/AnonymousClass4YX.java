package X;

import java.lang.reflect.Field;
import sun.misc.Unsafe;

/* renamed from: X.4YX  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YX {
    public Unsafe A00;

    public AnonymousClass4YX(Unsafe unsafe) {
        this.A00 = unsafe;
    }

    public final int A04(Object obj, long j) {
        return this.A00.getInt(obj, j);
    }

    public final long A05(Object obj, long j) {
        return this.A00.getLong(obj, j);
    }

    public final long A06(Field field) {
        return this.A00.objectFieldOffset(field);
    }

    public abstract void A07(long j, byte b);

    public final void A0B(Object obj, long j, int i) {
        this.A00.putInt(obj, j, i);
    }

    public final void A0C(Object obj, long j, long j2) {
        this.A00.putLong(obj, j, j2);
    }

    public abstract void A0E(byte[] bArr, long j, long j2, long j3);

    public static byte A00(Object obj, long j) {
        int i;
        boolean z = C95624e5.A08;
        long j2 = -4 & j;
        AnonymousClass4YX r0 = C95624e5.A02;
        if (z) {
            i = r0.A04(obj, j2);
            j ^= -1;
        } else {
            i = r0.A00.getInt(obj, j2);
        }
        return (byte) (i >>> ((int) ((j & 3) << 3)));
    }

    public byte A01(Object obj, long j) {
        return A00(obj, j);
    }

    public double A02(Object obj, long j) {
        return Double.longBitsToDouble(this.A00.getLong(obj, j));
    }

    public float A03(Object obj, long j) {
        return Float.intBitsToFloat(this.A00.getInt(obj, j));
    }

    public void A08(Object obj, long j, byte b) {
        if (C95624e5.A08) {
            C95624e5.A05(obj, j, b);
        } else {
            C95624e5.A06(obj, j, b);
        }
    }

    public void A09(Object obj, long j, double d) {
        A0C(obj, j, Double.doubleToLongBits(d));
    }

    public void A0A(Object obj, long j, float f) {
        A0B(obj, j, Float.floatToIntBits(f));
    }

    public void A0D(Object obj, long j, boolean z) {
        boolean z2 = C95624e5.A08;
        byte b = z ? (byte) 1 : 0;
        if (z2) {
            C95624e5.A05(obj, j, b);
        } else {
            C95624e5.A06(obj, j, b);
        }
    }

    public boolean A0F(Object obj, long j) {
        return C12960it.A1S(A00(obj, j));
    }
}
