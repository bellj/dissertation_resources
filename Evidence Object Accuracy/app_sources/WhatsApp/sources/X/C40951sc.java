package X;

import com.whatsapp.jid.DeviceJid;
import java.util.Arrays;

/* renamed from: X.1sc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40951sc {
    public final int A00;
    public final int A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final long A06;
    public final long A07;
    public final long A08;
    public final long A09;
    public final long A0A;
    public final AnonymousClass1JT A0B;
    public final DeviceJid A0C;

    public C40951sc(AnonymousClass1JT r3, DeviceJid deviceJid, int i, int i2, int i3, long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8) {
        this.A06 = j;
        this.A0C = deviceJid;
        this.A02 = i;
        this.A04 = j2;
        this.A09 = j3;
        this.A0A = j4;
        this.A08 = j5;
        this.A00 = i2;
        this.A07 = j6;
        this.A03 = j7;
        this.A01 = i3;
        this.A05 = j8;
        this.A0B = r3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C40951sc r7 = (C40951sc) obj;
            if (!(this.A02 == r7.A02 && this.A04 == r7.A04 && this.A09 == r7.A09 && this.A0A == r7.A0A && this.A08 == r7.A08 && this.A00 == r7.A00 && this.A07 == r7.A07 && this.A03 == r7.A03 && this.A0C.equals(r7.A0C) && this.A01 == r7.A01 && this.A05 == r7.A05)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A0C, Integer.valueOf(this.A02), Long.valueOf(this.A04), Long.valueOf(this.A09), Long.valueOf(this.A0A), Long.valueOf(this.A08), Integer.valueOf(this.A00), Long.valueOf(this.A07), Long.valueOf(this.A03), Integer.valueOf(this.A01), Long.valueOf(this.A05)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncDatum{deviceJid=");
        sb.append(this.A0C);
        sb.append(", syncType=");
        sb.append(this.A02);
        sb.append(", latestMsgId=");
        sb.append(this.A04);
        sb.append(", stageOldestMsgId=");
        sb.append(this.A09);
        sb.append(", syncOldestMsgId=");
        sb.append(this.A0A);
        sb.append(", sendMsgsCount=");
        sb.append(this.A08);
        sb.append(", chunkOrder=");
        sb.append(this.A00);
        sb.append(", sentBytes=");
        sb.append(this.A07);
        sb.append(", lastChunkTimestamp=");
        sb.append(this.A03);
        sb.append(", status=");
        sb.append(this.A01);
        sb.append(", peerMsgRowId=");
        sb.append(this.A05);
        sb.append(", bootstrapId=");
        sb.append(this.A0B);
        sb.append('}');
        return sb.toString();
    }
}
