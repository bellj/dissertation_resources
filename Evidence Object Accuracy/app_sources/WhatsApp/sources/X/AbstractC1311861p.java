package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.List;

/* renamed from: X.61p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC1311861p {
    List A9w(List list);

    int AAc();

    View AAd(LayoutInflater layoutInflater, ViewGroup viewGroup);

    View AD5(LayoutInflater layoutInflater, FrameLayout frameLayout);

    int AEM(AbstractC28901Pl v);

    String AEP(AbstractC28901Pl v);

    String AEQ(AbstractC28901Pl v);

    View AFP(LayoutInflater layoutInflater, ViewGroup viewGroup);

    void ALx();

    void AM1();

    void AMp();

    boolean AdN(AbstractC28901Pl v);

    boolean AdV();

    boolean AdZ();

    void Adj(AbstractC28901Pl v, PaymentMethodRow paymentMethodRow);

    void onCreate();

    void onDestroy();
}
