package X;

/* renamed from: X.5G6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G6 implements AnonymousClass5X5 {
    public AnonymousClass5XE A00;
    public AnonymousClass20K A01;
    public boolean A02;
    public boolean A03;
    public byte[] A04 = {-90, -90, -90, -90, -90, -90, -90, -90};

    @Override // X.AnonymousClass5X5
    public String AAf() {
        return this.A00.AAf();
    }

    public AnonymousClass5G6(AnonymousClass5XE r2) {
        this.A00 = r2;
        this.A03 = true;
    }

    @Override // X.AnonymousClass5X5
    public void AIf(AnonymousClass20L r3, boolean z) {
        this.A02 = z;
        if (r3 instanceof C113025Fs) {
            r3 = ((C113025Fs) r3).A01;
        }
        if (r3 instanceof AnonymousClass20K) {
            this.A01 = (AnonymousClass20K) r3;
        } else if (r3 instanceof C113075Fx) {
            C113075Fx r32 = (C113075Fx) r3;
            byte[] bArr = r32.A01;
            this.A04 = bArr;
            this.A01 = (AnonymousClass20K) r32.A00;
            if (bArr.length != 8) {
                throw C12970iu.A0f("IV not equal to 8");
            }
        }
    }

    @Override // X.AnonymousClass5X5
    public byte[] AfE(byte[] bArr, int i, int i2) {
        if (!this.A02) {
            int i3 = i2 >> 3;
            if ((i3 << 3) == i2) {
                int length = this.A04.length;
                byte[] bArr2 = new byte[i2 - length];
                byte[] bArr3 = new byte[length];
                byte[] bArr4 = new byte[length + 8];
                System.arraycopy(bArr, 0, bArr3, 0, length);
                int length2 = this.A04.length;
                System.arraycopy(bArr, 0 + length2, bArr2, 0, i2 - length2);
                AnonymousClass5XE r8 = this.A00;
                r8.AIf(this.A01, !this.A03);
                int i4 = i3 - 1;
                int i5 = 5;
                do {
                    for (int i6 = i4; i6 >= 1; i6--) {
                        System.arraycopy(bArr3, 0, bArr4, 0, this.A04.length);
                        int i7 = (i6 - 1) << 3;
                        System.arraycopy(bArr2, i7, bArr4, this.A04.length, 8);
                        int i8 = (i4 * i5) + i6;
                        int i9 = 1;
                        while (i8 != 0) {
                            int length3 = this.A04.length - i9;
                            C72463ee.A0P((byte) i8, bArr4, bArr4[length3], length3);
                            i8 >>>= 8;
                            i9++;
                        }
                        r8.AZY(bArr4, bArr4, 0, 0);
                        System.arraycopy(bArr4, 0, bArr3, 0, 8);
                        System.arraycopy(bArr4, 8, bArr2, i7, 8);
                    }
                    i5--;
                } while (i5 >= 0);
                if (AnonymousClass1TT.A01(bArr3, this.A04)) {
                    return bArr2;
                }
                throw new C114965Nt("checksum failed");
            }
            throw new C114965Nt("unwrap data must be a multiple of 8 bytes");
        }
        throw C12960it.A0U("not set for unwrapping");
    }

    @Override // X.AnonymousClass5X5
    public byte[] Ag9(byte[] bArr, int i, int i2) {
        if (this.A02) {
            int i3 = i2 >> 3;
            if ((i3 << 3) == i2) {
                byte[] bArr2 = this.A04;
                int length = bArr2.length;
                byte[] bArr3 = new byte[length + i2];
                byte[] bArr4 = new byte[length + 8];
                System.arraycopy(bArr2, 0, bArr3, 0, length);
                System.arraycopy(bArr, 0, bArr3, this.A04.length, i2);
                AnonymousClass5XE r7 = this.A00;
                r7.AIf(this.A01, this.A03);
                int i4 = 0;
                do {
                    for (int i5 = 1; i5 <= i3; i5++) {
                        System.arraycopy(bArr3, 0, bArr4, 0, this.A04.length);
                        int i6 = i5 << 3;
                        System.arraycopy(bArr3, i6, bArr4, this.A04.length, 8);
                        r7.AZY(bArr4, bArr4, 0, 0);
                        int i7 = (i3 * i4) + i5;
                        int i8 = 1;
                        while (i7 != 0) {
                            int length2 = this.A04.length - i8;
                            C72463ee.A0P((byte) i7, bArr4, bArr4[length2], length2);
                            i7 >>>= 8;
                            i8++;
                        }
                        System.arraycopy(bArr4, 0, bArr3, 0, 8);
                        System.arraycopy(bArr4, 8, bArr3, i6, 8);
                    }
                    i4++;
                } while (i4 != 6);
                return bArr3;
            }
            throw new AnonymousClass5O2("wrap data must be a multiple of 8 bytes");
        }
        throw C12960it.A0U("not set for wrapping");
    }
}
