package X;

/* renamed from: X.0cX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09210cX implements Runnable {
    public final /* synthetic */ animation.Animation$AnimationListenerC07020Wj A00;

    public RunnableC09210cX(animation.Animation$AnimationListenerC07020Wj r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        animation.Animation$AnimationListenerC07020Wj r2 = this.A00;
        r2.A01.endViewTransition(r2.A00);
        r2.A02.A00();
    }
}
