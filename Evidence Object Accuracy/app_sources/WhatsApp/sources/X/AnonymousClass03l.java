package X;

import android.app.PendingIntent;
import android.os.Bundle;
import androidx.core.graphics.drawable.IconCompat;

/* renamed from: X.03l  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03l {
    @Deprecated
    public int A00;
    public PendingIntent A01;
    public IconCompat A02;
    public CharSequence A03;
    public boolean A04;
    public boolean A05;
    public final int A06;
    public final Bundle A07;
    public final C007103o[] A08;
    public final C007103o[] A09;

    public AnonymousClass03l(int i, CharSequence charSequence, PendingIntent pendingIntent) {
        this(pendingIntent, new Bundle(), i != 0 ? IconCompat.A02(null, "", i) : null, charSequence, null, null, 0, true, true);
    }

    public AnonymousClass03l(PendingIntent pendingIntent, Bundle bundle, IconCompat iconCompat, CharSequence charSequence, C007103o[] r7, C007103o[] r8, int i, boolean z, boolean z2) {
        this.A05 = true;
        this.A02 = iconCompat;
        if (iconCompat != null && iconCompat.A04() == 2) {
            this.A00 = iconCompat.A03();
        }
        this.A03 = C005602s.A00(charSequence);
        this.A01 = pendingIntent;
        this.A07 = bundle;
        this.A09 = r7;
        this.A08 = r8;
        this.A04 = z;
        this.A06 = i;
        this.A05 = z2;
    }

    public IconCompat A00() {
        int i;
        IconCompat iconCompat = this.A02;
        if (iconCompat != null || (i = this.A00) == 0) {
            return iconCompat;
        }
        IconCompat A02 = IconCompat.A02(null, "", i);
        this.A02 = A02;
        return A02;
    }
}
