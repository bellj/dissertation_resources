package X;

import java.nio.charset.Charset;
import java.util.Arrays;

/* renamed from: X.4wJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106804wJ implements AbstractC116785Ww {
    public static final int A0D;
    public static final byte[] A0E;
    public static final byte[] A0F;
    public static final int[] A0G = {13, 14, 16, 18, 20, 21, 27, 32, 6, 7, 6, 6, 1, 1, 1, 1};
    public static final int[] A0H;
    public int A00;
    public int A01;
    public int A02 = -1;
    public int A03;
    public long A04;
    public long A05;
    public AbstractC14070ko A06;
    public AnonymousClass5WY A07;
    public AnonymousClass5X6 A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final byte[] A0C = new byte[1];

    static {
        int[] iArr = {18, 24, 33, 37, 41, 47, 51, 59, 61, 6, 1, 1, 1, 1, 1, 1};
        A0H = iArr;
        Charset charset = C88814Hf.A05;
        A0E = "#!AMR\n".getBytes(charset);
        A0F = "#!AMR-WB\n".getBytes(charset);
        A0D = iArr[8];
    }

    public final boolean A00(AnonymousClass5Yf r6) {
        byte[] bArr = A0E;
        r6.Aaj();
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        r6.AZ4(bArr2, 0, length);
        if (Arrays.equals(bArr2, bArr)) {
            this.A0B = false;
        } else {
            bArr = A0F;
            r6.Aaj();
            int length2 = bArr.length;
            byte[] bArr3 = new byte[length2];
            r6.AZ4(bArr3, 0, length2);
            if (!Arrays.equals(bArr3, bArr)) {
                return false;
            }
            this.A0B = true;
        }
        r6.Ae3(bArr.length);
        return true;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r3) {
        this.A06 = r3;
        this.A08 = r3.Af4(0, 1);
        r3.A9V();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0076, code lost:
        if (r6 != false) goto L_0x0078;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b5  */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r12, X.AnonymousClass4IG r13) {
        /*
        // Method dump skipped, instructions count: 253
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106804wJ.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A04 = 0;
        this.A01 = 0;
        this.A00 = 0;
        if (j != 0) {
            AnonymousClass5WY r1 = this.A07;
            if (r1 instanceof C106964wZ) {
                this.A05 = ((C106964wZ) r1).A00(j);
                return;
            }
        }
        this.A05 = 0;
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        return A00(r2);
    }
}
