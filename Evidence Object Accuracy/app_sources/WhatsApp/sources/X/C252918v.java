package X;

import java.io.File;
import java.util.List;

/* renamed from: X.18v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C252918v {
    public int A00;
    public int A01 = Integer.MAX_VALUE;
    public C58962tj A02;
    public final C14900mE A03;
    public final C15450nH A04;
    public final C18790t3 A05;
    public final C16590pI A06;
    public final C14850m9 A07;
    public final C16120oU A08;
    public final C20110vE A09;
    public final C22600zL A0A;

    public C252918v(C14900mE r2, C15450nH r3, C18790t3 r4, C16590pI r5, C14850m9 r6, C16120oU r7, C20110vE r8, C22600zL r9) {
        this.A06 = r5;
        this.A07 = r6;
        this.A03 = r2;
        this.A05 = r4;
        this.A04 = r3;
        this.A08 = r7;
        this.A0A = r9;
        this.A09 = r8;
    }

    public void A00() {
        if (this.A00 == 0) {
            File file = new File(this.A06.A00.getCacheDir(), "product_catalog_images");
            C68183Uk r4 = new C68183Uk(this.A01);
            C14850m9 r5 = this.A07;
            C58962tj r0 = new C58962tj(this.A03, this.A04, this.A05, r4, r5, this.A08, this.A09, this.A0A, file, "catalog-imager", 4);
            this.A02 = r0;
            r4.A00 = r0;
        }
        this.A00++;
    }

    public void A01(C68203Um r3) {
        r3.A03 = true;
        C58962tj r0 = this.A02;
        if (r0 != null) {
            r0.A00(r3);
        }
        List list = r3.A02;
        if (list != null && list.size() > 0) {
            for (C68203Um r02 : r3.A02) {
                A01(r02);
            }
        }
    }
}
