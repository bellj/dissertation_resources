package X;

import com.whatsapp.util.Log;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

/* renamed from: X.3dW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71793dW extends SSLSocket {
    public final int A00;
    public final int A01;
    public final C18790t3 A02;
    public final SSLSocket A03;

    public C71793dW(C18790t3 r1, SSLSocket sSLSocket, int i, int i2) {
        this.A00 = i;
        this.A03 = sSLSocket;
        this.A02 = r1;
        this.A01 = i2;
    }

    @Override // javax.net.ssl.SSLSocket
    public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.A03.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    @Override // java.net.Socket
    public void bind(SocketAddress socketAddress) {
        this.A03.bind(socketAddress);
    }

    @Override // java.net.Socket, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        this.A03.close();
    }

    @Override // java.net.Socket
    public void connect(SocketAddress socketAddress) {
        this.A03.connect(socketAddress);
    }

    @Override // java.net.Socket
    public void connect(SocketAddress socketAddress, int i) {
        this.A03.connect(socketAddress, i);
    }

    @Override // java.net.Socket
    public SocketChannel getChannel() {
        return this.A03.getChannel();
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getEnableSessionCreation() {
        return this.A03.getEnableSessionCreation();
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getEnabledCipherSuites() {
        return this.A03.getEnabledCipherSuites();
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getEnabledProtocols() {
        return this.A03.getEnabledProtocols();
    }

    @Override // java.net.Socket
    public InetAddress getInetAddress() {
        return this.A03.getInetAddress();
    }

    @Override // java.net.Socket
    public InputStream getInputStream() {
        return new AnonymousClass1oJ(this.A02, this.A03.getInputStream(), Integer.valueOf(this.A00), Integer.valueOf(this.A01));
    }

    @Override // java.net.Socket
    public boolean getKeepAlive() {
        return this.A03.getKeepAlive();
    }

    @Override // java.net.Socket
    public InetAddress getLocalAddress() {
        return this.A03.getLocalAddress();
    }

    @Override // java.net.Socket
    public int getLocalPort() {
        return this.A03.getLocalPort();
    }

    @Override // java.net.Socket
    public SocketAddress getLocalSocketAddress() {
        return this.A03.getLocalSocketAddress();
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getNeedClientAuth() {
        return this.A03.getNeedClientAuth();
    }

    @Override // java.net.Socket
    public boolean getOOBInline() {
        return this.A03.getOOBInline();
    }

    @Override // java.net.Socket
    public OutputStream getOutputStream() {
        return new AnonymousClass23V(this.A02, this.A03.getOutputStream(), Integer.valueOf(this.A00), Integer.valueOf(this.A01));
    }

    @Override // java.net.Socket
    public int getPort() {
        return this.A03.getPort();
    }

    @Override // java.net.Socket
    public synchronized int getReceiveBufferSize() {
        return this.A03.getReceiveBufferSize();
    }

    @Override // java.net.Socket
    public SocketAddress getRemoteSocketAddress() {
        return this.A03.getRemoteSocketAddress();
    }

    @Override // java.net.Socket
    public boolean getReuseAddress() {
        return this.A03.getReuseAddress();
    }

    @Override // javax.net.ssl.SSLSocket
    public SSLParameters getSSLParameters() {
        return this.A03.getSSLParameters();
    }

    @Override // java.net.Socket
    public synchronized int getSendBufferSize() {
        return this.A03.getSendBufferSize();
    }

    @Override // javax.net.ssl.SSLSocket
    public SSLSession getSession() {
        return this.A03.getSession();
    }

    @Override // java.net.Socket
    public int getSoLinger() {
        return this.A03.getSoLinger();
    }

    @Override // java.net.Socket
    public synchronized int getSoTimeout() {
        return this.A03.getSoTimeout();
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getSupportedCipherSuites() {
        return this.A03.getSupportedCipherSuites();
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getSupportedProtocols() {
        return this.A03.getSupportedProtocols();
    }

    @Override // java.net.Socket
    public boolean getTcpNoDelay() {
        return this.A03.getTcpNoDelay();
    }

    @Override // java.net.Socket
    public int getTrafficClass() {
        return this.A03.getTrafficClass();
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getUseClientMode() {
        return this.A03.getUseClientMode();
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getWantClientAuth() {
        return this.A03.getWantClientAuth();
    }

    @Override // java.net.Socket
    public boolean isBound() {
        return this.A03.isBound();
    }

    @Override // java.net.Socket
    public boolean isClosed() {
        return this.A03.isClosed();
    }

    @Override // java.net.Socket
    public boolean isConnected() {
        return this.A03.isConnected();
    }

    @Override // java.net.Socket
    public boolean isInputShutdown() {
        return this.A03.isInputShutdown();
    }

    @Override // java.net.Socket
    public boolean isOutputShutdown() {
        return this.A03.isOutputShutdown();
    }

    @Override // javax.net.ssl.SSLSocket
    public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.A03.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    @Override // java.net.Socket
    public void sendUrgentData(int i) {
        this.A03.sendUrgentData(i);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setEnableSessionCreation(boolean z) {
        this.A03.setEnableSessionCreation(z);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setEnabledCipherSuites(String[] strArr) {
        this.A03.setEnabledCipherSuites(strArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0015, code lost:
        if ("SSLv3".equals(r14[0]) == false) goto L_0x0017;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005e  */
    @Override // javax.net.ssl.SSLSocket
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setEnabledProtocols(java.lang.String[] r14) {
        /*
            r13 = this;
            javax.net.ssl.SSLSocket r4 = r13.A03
            int r10 = r14.length
            java.lang.String r12 = "accounting-socket/set-enabled-protocols/current-list: "
            java.lang.String r7 = ", "
            r9 = 1
            if (r10 < r9) goto L_0x0017
            java.lang.String r11 = "SSLv3"
            r5 = 0
            if (r10 != r9) goto L_0x002a
            r0 = r14[r5]
            boolean r0 = r11.equals(r0)
            if (r0 != 0) goto L_0x0043
        L_0x0017:
            java.lang.StringBuilder r1 = X.C12960it.A0j(r12)
            java.lang.String r0 = android.text.TextUtils.join(r7, r14)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.i(r0)
        L_0x0026:
            r4.setEnabledProtocols(r14)
            return
        L_0x002a:
            if (r10 == r9) goto L_0x0043
            r8 = r14
        L_0x002d:
            java.util.ArrayList r3 = X.C12960it.A0l()
            int r6 = r8.length
            r2 = 0
        L_0x0033:
            if (r2 >= r6) goto L_0x0048
            r1 = r8[r2]
            boolean r0 = r11.equals(r1)
            if (r0 != 0) goto L_0x0040
            r3.add(r1)
        L_0x0040:
            int r2 = r2 + 1
            goto L_0x0033
        L_0x0043:
            java.lang.String[] r8 = r4.getSupportedProtocols()
            goto L_0x002d
        L_0x0048:
            int r0 = r3.size()
            java.lang.String r2 = "accounting-socket/set-enabled-protocols/modified-list: "
            if (r10 != r0) goto L_0x0067
            if (r10 == r9) goto L_0x0067
            android.text.TextUtils.join(r7, r14)
            android.text.TextUtils.join(r7, r3)
        L_0x0058:
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0026
            java.lang.String[] r0 = new java.lang.String[r5]
            java.lang.Object[] r14 = r3.toArray(r0)
            java.lang.String[] r14 = (java.lang.String[]) r14
            goto L_0x0026
        L_0x0067:
            java.lang.StringBuilder r1 = X.C12960it.A0j(r12)
            java.lang.String r0 = android.text.TextUtils.join(r7, r14)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.i(r0)
            java.lang.StringBuilder r1 = X.C12960it.A0j(r2)
            java.lang.String r0 = android.text.TextUtils.join(r7, r3)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            com.whatsapp.util.Log.i(r0)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71793dW.setEnabledProtocols(java.lang.String[]):void");
    }

    @Override // java.net.Socket
    public void setKeepAlive(boolean z) {
        this.A03.setKeepAlive(z);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setNeedClientAuth(boolean z) {
        this.A03.setNeedClientAuth(z);
    }

    @Override // java.net.Socket
    public void setOOBInline(boolean z) {
        this.A03.setOOBInline(z);
    }

    @Override // java.net.Socket
    public void setPerformancePreferences(int i, int i2, int i3) {
        this.A03.setPerformancePreferences(i, i2, i3);
    }

    @Override // java.net.Socket
    public synchronized void setReceiveBufferSize(int i) {
        this.A03.setReceiveBufferSize(i);
    }

    @Override // java.net.Socket
    public void setReuseAddress(boolean z) {
        this.A03.setReuseAddress(z);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setSSLParameters(SSLParameters sSLParameters) {
        this.A03.setSSLParameters(sSLParameters);
    }

    @Override // java.net.Socket
    public synchronized void setSendBufferSize(int i) {
        this.A03.setSendBufferSize(i);
    }

    @Override // java.net.Socket
    public void setSoLinger(boolean z, int i) {
        this.A03.setSoLinger(z, i);
    }

    @Override // java.net.Socket
    public synchronized void setSoTimeout(int i) {
        this.A03.setSoTimeout(i);
    }

    @Override // java.net.Socket
    public void setTcpNoDelay(boolean z) {
        this.A03.setTcpNoDelay(z);
    }

    @Override // java.net.Socket
    public void setTrafficClass(int i) {
        this.A03.setTrafficClass(i);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setUseClientMode(boolean z) {
        this.A03.setUseClientMode(z);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setWantClientAuth(boolean z) {
        this.A03.setWantClientAuth(z);
    }

    @Override // java.net.Socket
    public void shutdownInput() {
        this.A03.shutdownInput();
    }

    @Override // java.net.Socket
    public void shutdownOutput() {
        this.A03.shutdownOutput();
    }

    @Override // javax.net.ssl.SSLSocket
    public void startHandshake() {
        try {
            this.A03.startHandshake();
        } catch (SSLHandshakeException | SSLProtocolException e) {
            StringBuilder A0k = C12960it.A0k("accounting-socket-factory/enabled suites ");
            SSLSocket sSLSocket = this.A03;
            A0k.append(Arrays.toString(sSLSocket.getEnabledCipherSuites()));
            A0k.append(" supported suites ");
            Log.e(C12960it.A0d(Arrays.toString(sSLSocket.getSupportedCipherSuites()), A0k));
            throw e;
        }
    }

    @Override // javax.net.ssl.SSLSocket, java.net.Socket, java.lang.Object
    public String toString() {
        return this.A03.toString();
    }
}
