package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.voipcalling.Voip;
import java.util.List;

/* renamed from: X.1S7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1S7 {
    public final int A00;
    public final int A01;
    public final GroupJid A02;
    public final Voip.CallLogInfo A03;
    public final String A04;
    public final String A05;
    public final List A06;
    public final boolean A07;
    public final boolean A08;

    public AnonymousClass1S7(Voip.CallLogInfo callLogInfo, String str, String str2, String str3, List list, int i, int i2, boolean z, boolean z2) {
        this.A01 = i;
        this.A04 = str;
        this.A00 = i2;
        this.A06 = list;
        this.A02 = GroupJid.getNullable(str2);
        this.A08 = z;
        this.A03 = callLogInfo;
        this.A07 = z2;
        this.A05 = str3;
    }
}
