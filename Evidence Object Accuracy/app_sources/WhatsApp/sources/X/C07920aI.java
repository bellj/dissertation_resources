package X;

import com.airbnb.lottie.LottieAnimationView;

/* renamed from: X.0aI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07920aI implements AbstractC12010hE {
    public final /* synthetic */ LottieAnimationView A00;

    public C07920aI(LottieAnimationView lottieAnimationView) {
        this.A00 = lottieAnimationView;
    }

    @Override // X.AbstractC12010hE
    public /* bridge */ /* synthetic */ void AVI(Object obj) {
        LottieAnimationView lottieAnimationView = this.A00;
        int i = lottieAnimationView.A02;
        if (i != 0) {
            lottieAnimationView.setImageResource(i);
        }
        AbstractC12010hE r0 = lottieAnimationView.A04;
        if (r0 == null) {
            r0 = LottieAnimationView.A0J;
        }
        r0.AVI(obj);
    }
}
