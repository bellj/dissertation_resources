package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.2c4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ScaleGestureDetector$OnScaleGestureListenerC52942c4 extends FrameLayout implements ScaleGestureDetector.OnScaleGestureListener, AnonymousClass004 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public Rect A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public C252718t A0F;
    public AbstractC115435Rn A0G;
    public AbstractC115445Ro A0H;
    public AnonymousClass2G9 A0I;
    public AnonymousClass2P7 A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public int[] A0R;
    public final ScaleGestureDetector A0S;
    public final View A0T;
    public final C06260Su A0U;
    public final ArrayList A0V;

    public ScaleGestureDetector$OnScaleGestureListenerC52942c4(Context context, View view) {
        super(context);
        if (!this.A0O) {
            this.A0O = true;
            this.A0F = (C252718t) AnonymousClass2P6.A00(generatedComponent()).A9K.get();
        }
        this.A00 = 1.0f;
        this.A0L = true;
        this.A06 = 0;
        this.A0V = C12980iv.A0w(4);
        if (view == null) {
            this.A0T = this;
            view = this;
        } else {
            this.A0T = view;
        }
        int[] A07 = C13000ix.A07();
        view.getLocationInWindow(A07);
        int i = A07[0];
        this.A0B = new Rect(i, A07[1], i + this.A0T.getWidth(), A07[1] + this.A0T.getHeight());
        C06260Su r1 = new C06260Su(getContext(), this, new AnonymousClass2CM(this));
        r1.A06 = (int) (((float) r1.A06) * (1.0f / 1.0f));
        this.A0U = r1;
        r1.A01 = 2000.0f;
        ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(context, this);
        this.A0S = scaleGestureDetector;
        C04200Kt.A00(scaleGestureDetector);
        View A0G = C12970iu.A0G(AnonymousClass12P.A00(context));
        AnonymousClass028.A0h(A0G, new AnonymousClass07F() { // from class: X.4rp
            @Override // X.AnonymousClass07F
            public final C018408o AMH(View view2, C018408o r3) {
                return ScaleGestureDetector$OnScaleGestureListenerC52942c4.A03(view2, r3, ScaleGestureDetector$OnScaleGestureListenerC52942c4.this);
            }
        });
        AnonymousClass028.A0R(A0G);
    }

    public static int A00(ScaleGestureDetector$OnScaleGestureListenerC52942c4 r1, int i) {
        return (int) (((1.0f - r1.A00) * ((float) i)) / 2.0f);
    }

    public static /* synthetic */ int A01(ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2, int i) {
        return (r2.getRightOfDraggableArea() - i) + A00(r2, i);
    }

    public static /* synthetic */ int A02(ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2, int i) {
        return r2.getLeftOfDraggableArea() - A00(r2, i);
    }

    public static /* synthetic */ C018408o A03(View view, C018408o r7, ScaleGestureDetector$OnScaleGestureListenerC52942c4 r8) {
        boolean z = r8.A0N;
        View view2 = r8.A0D;
        if (z) {
            if (view2 != null) {
                view2.setPadding(r7.A04(), r8.A0D.getPaddingTop(), r7.A05(), r8.A0D.getPaddingBottom());
            }
            C018408o A0I = AnonymousClass028.A0I(view, r7);
            return A0I.A08(0, A0I.A06(), 0, A0I.A03());
        }
        if (view2 != null) {
            view2.setPadding(0, 0, 0, 0);
        }
        return AnonymousClass028.A0I(view, r7);
    }

    public final int A04(int i) {
        int i2 = this.A04;
        int leftOfDraggableArea = getLeftOfDraggableArea();
        int A00 = A00(this, i);
        int i3 = leftOfDraggableArea - A00;
        int rightOfDraggableArea = (getRightOfDraggableArea() - i) + A00;
        return i2 <= (rightOfDraggableArea >> 1) + i3 ? i3 : rightOfDraggableArea;
    }

    public final int A05(int i) {
        int A07 = A07(i);
        int A06 = A06(i);
        int i2 = this.A05;
        return C12980iv.A05(i2, A07) >= C12980iv.A05(i2, A06) ? A06 : A07;
    }

    public final int A06(int i) {
        int A00 = A00(this, i);
        return Math.max((getBottomOfDraggableArea() - i) + A00, getTopOfDraggableArea() - A00);
    }

    public final int A07(int i) {
        int A00 = A00(this, i);
        return Math.min((getBottomOfDraggableArea() - i) + A00, getTopOfDraggableArea() - A00);
    }

    public void A08() {
        int[] A07 = C13000ix.A07();
        View view = this.A0T;
        view.getLocationInWindow(A07);
        int i = A07[0];
        this.A0B = new Rect(i, A07[1], i + view.getWidth(), A07[1] + view.getHeight());
    }

    public final void A09(float f) {
        if (this.A0C != null) {
            Iterator it = this.A0V.iterator();
            while (it.hasNext()) {
                View view = (View) it.next();
                try {
                    int i = ((FrameLayout.LayoutParams) view.getLayoutParams()).gravity;
                    if ((i & 48) == 48) {
                        view.setPivotY(0.0f);
                        C12990iw.A1A(view, view.getPaddingLeft(), (int) (((float) this.A06) * f));
                    }
                    if ((i & 80) == 80) {
                        view.setPivotY((float) view.getMeasuredHeight());
                        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), (int) (((float) this.A06) * f));
                    }
                    if ((i & 8388611) == 8388611) {
                        view.setPivotX(0.0f);
                        C12990iw.A1A(view, (int) (((float) this.A06) * f), view.getPaddingTop());
                    }
                    if ((i & 8388613) == 8388613) {
                        view.setPivotX((float) view.getMeasuredWidth());
                        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), (int) (((float) this.A06) * f), view.getPaddingBottom());
                    }
                } catch (Exception unused) {
                }
                float f2 = 1.0f / f;
                view.setScaleX(f2);
                view.setScaleY(f2);
            }
        }
    }

    public void A0A(boolean z) {
        if (this.A0C != null && !this.A0K) {
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC66293Na(viewTreeObserver, this, z));
            requestLayout();
        }
    }

    @Override // android.view.View
    public void computeScroll() {
        super.computeScroll();
        if (this.A0U.A0A()) {
            postInvalidateOnAnimation();
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        if (this.A0L) {
            canvas.clipRect(this.A0B);
        }
        super.dispatchDraw(canvas);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0J;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0J = r0;
        }
        return r0.generatedComponent();
    }

    private int getBottomOfDraggableArea() {
        return this.A0B.bottom - this.A01;
    }

    public float getCurrentChildScale() {
        return this.A00;
    }

    private int getLeftOfDraggableArea() {
        return this.A01 + this.A0B.left;
    }

    private int getRightOfDraggableArea() {
        return this.A0B.right - this.A01;
    }

    private int getTopOfDraggableArea() {
        return this.A01 + this.A0B.top;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.A0U.A0E(motionEvent) || super.onInterceptTouchEvent(motionEvent);
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        View view = this.A0C;
        if (view != null && !this.A0N) {
            if (!this.A0K) {
                view.setScaleX(this.A00);
                this.A0C.setScaleY(this.A00);
            }
            View view2 = this.A0C;
            int i5 = this.A04;
            view2.layout(i5, this.A05, view2.getWidth() + i5, this.A05 + this.A0C.getHeight());
        }
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        if (this.A0C == null) {
            return false;
        }
        float scaleFactor = this.A00 * scaleGestureDetector.getScaleFactor();
        this.A00 = scaleFactor;
        if (scaleFactor > 1.0f) {
            this.A00 = 1.0f;
            scaleFactor = 1.0f;
        } else if (scaleFactor < 0.67f) {
            this.A00 = 0.67f;
            scaleFactor = 0.67f;
        }
        this.A0C.setScaleX(scaleFactor);
        this.A0C.setScaleY(this.A00);
        A09(this.A00);
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return C12960it.A1W(this.A0C);
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        View view = this.A0C;
        if (view != null) {
            boolean z = this.A0N;
            float f = this.A00;
            if (z) {
                if (f <= 0.85f) {
                    this.A0M = true;
                    AbstractC115435Rn r0 = this.A0G;
                    if (r0 != null) {
                        ((AnonymousClass5AR) r0).A00.A9m(true);
                    }
                    AnonymousClass2G9 r5 = this.A0I;
                    if (r5 != null) {
                        r5.A04(0, getResources().getColor(R.color.black));
                        this.A0I.setPlayerElevation(6);
                        this.A0I.setVisibility(0);
                    }
                }
                this.A0C.animate().scaleX(this.A00).scaleY(this.A00).setDuration(125);
            } else {
                int i = (f > 0.85f ? 1 : (f == 0.85f ? 0 : -1));
                ViewPropertyAnimator animate = view.animate();
                float f2 = 1.0f;
                if (i <= 0) {
                    f2 = 0.67f;
                }
                animate.scaleX(f2).scaleY(f2).setDuration(125);
                this.A00 = f2;
            }
            this.A0C.setScaleX(this.A00);
            this.A0C.setScaleY(this.A00);
            A09(this.A00);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        C06260Su r4 = this.A0U;
        if ((r4.A03 != 1 && !this.A0S.isInProgress() && (this.A0C == null || motionEvent.getX() < ((float) (this.A04 + A00(this, this.A0C.getWidth()))) || motionEvent.getX() > ((float) ((this.A04 + this.A0C.getWidth()) - A00(this, this.A0C.getWidth()))) || motionEvent.getY() < ((float) (this.A05 + A00(this, this.A0C.getHeight()))) || motionEvent.getY() > ((float) ((this.A05 + this.A0C.getHeight()) - A00(this, this.A0C.getHeight()))))) || this.A0Q || this.A0K) {
            return false;
        }
        this.A0S.onTouchEvent(motionEvent);
        r4.A07(motionEvent);
        return true;
    }

    @Override // android.view.ViewGroup
    public void onViewAdded(View view) {
        super.onViewAdded(view);
        View view2 = this.A0C;
        if (view2 != null) {
            removeView(view2);
        }
        this.A0E = null;
        this.A0C = view;
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ObjectAnimator.ofFloat(this.A0C, "scaleX", this.A00), ObjectAnimator.ofFloat(this.A0C, "scaleY", this.A00), ObjectAnimator.ofFloat(this.A0C, "translationX", 0.0f), ObjectAnimator.ofFloat(this.A0C, "translationY", 0.0f), ObjectAnimator.ofFloat(this.A0C, "alpha", 0.0f, 1.0f));
        animatorSet.setInterpolator(new AccelerateInterpolator());
        animatorSet.setDuration(130L);
        animatorSet.addListener(new IDxLAdapterShape0S0100000_1_I1(this, 9));
        animatorSet.start();
        int[] iArr = this.A0R;
        if (iArr != null) {
            for (int i : iArr) {
                View findViewById = this.A0C.findViewById(i);
                if (findViewById != null) {
                    this.A0V.add(findViewById);
                }
            }
        }
    }

    @Override // android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        this.A0V.clear();
        this.A0C = null;
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        return true;
    }

    public void setChildPadding(int i) {
        this.A01 = i;
    }

    public void setClipToDependentView(boolean z) {
        this.A0L = z;
    }

    public void setControlView(AnonymousClass2G9 r2) {
        this.A0I = r2;
        this.A0D = r2.findViewById(R.id.header);
    }

    public void setDismissListener(AbstractC115435Rn r1) {
        this.A0G = r1;
    }

    public void setExitingFullScreen(boolean z) {
        this.A0M = z;
    }

    public void setFullscreen(boolean z) {
        this.A0N = z;
    }

    public void setLockChild(boolean z) {
        this.A0Q = z;
    }

    public void setViewAddedListener(AbstractC115445Ro r1) {
        this.A0H = r1;
    }
}
