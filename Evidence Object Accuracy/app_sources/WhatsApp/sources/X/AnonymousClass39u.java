package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39u  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass39u extends Enum {
    public static final /* synthetic */ AnonymousClass39u[] A00;
    public static final AnonymousClass39u A01;
    public static final AnonymousClass39u A02;
    /* Fake field, exist only in values array */
    AnonymousClass39u EF9;

    static {
        C81403tx r9 = new C81403tx();
        C56802ly r7 = new C56802ly();
        A01 = r7;
        C56812lz r5 = new C56812lz();
        A02 = r5;
        A00 = new AnonymousClass39u[]{r9, r7, r5, new C81413ty(), new C81423tz()};
    }

    public /* synthetic */ AnonymousClass39u(String str, int i) {
    }

    public boolean A00(C20920wX r7, C71133cR r8, String str) {
        C71123cQ A0D;
        C71113cP A0B;
        if (this instanceof C56812lz) {
            AnonymousClass39u r4 = A01;
            if (r4.A00(r7, r8, str)) {
                return true;
            }
            if (r8.hasNationalNumber) {
                String valueOf = String.valueOf(r8.nationalNumber_);
                if (valueOf.startsWith("8") && r8.hasCountryCode && r8.countryCode_ == 7 && r8.hasCountryCodeSource && r8.countryCodeSource_ == AnonymousClass4AP.FROM_DEFAULT_COUNTRY) {
                    long parseLong = Long.parseLong(valueOf.substring(1));
                    r8.hasNationalNumber = true;
                    r8.nationalNumber_ = parseLong;
                    AnonymousClass4AP r0 = AnonymousClass4AP.FROM_NUMBER_WITHOUT_PLUS_SIGN;
                    r8.hasCountryCodeSource = true;
                    r8.countryCodeSource_ = r0;
                    return r4.A00(r7, r8, str);
                }
            }
            if (!r8.hasItalianLeadingZero || !r8.italianLeadingZero_) {
                return false;
            }
            r8.hasItalianLeadingZero = false;
            r8.italianLeadingZero_ = false;
            return r4.A00(r7, r8, str);
        } else if (!r7.A0L(r8)) {
            return false;
        } else {
            int i = 0;
            while (i < str.length() - 1) {
                char charAt = str.charAt(i);
                if (charAt == 'x' || charAt == 'X') {
                    int i2 = i + 1;
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == 'x' || charAt2 == 'X') {
                        if (r7.A09(r8, str.substring(i2)) != AnonymousClass49Q.A03) {
                            return false;
                        }
                        i = i2;
                    } else if (!C20920wX.A03(str.substring(i)).equals(r8.extension_)) {
                        return false;
                    }
                }
                i++;
            }
            if (r8.countryCodeSource_ != AnonymousClass4AP.FROM_DEFAULT_COUNTRY || (A0D = r7.A0D(r7.A0F(r8.countryCode_))) == null || (A0B = r7.A0B(C20920wX.A01(r8), A0D.numberFormat_)) == null) {
                return true;
            }
            String str2 = A0B.nationalPrefixFormattingRule_;
            if (str2.length() <= 0 || A0B.nationalPrefixOptionalWhenFormatting_ || C20920wX.A03(str2.substring(0, str2.indexOf("$1"))).length() == 0) {
                return true;
            }
            return r7.A0J(A0D, C12960it.A0k(C20920wX.A03(r8.rawInput_)), null);
        }
    }

    public static AnonymousClass39u valueOf(String str) {
        return (AnonymousClass39u) Enum.valueOf(AnonymousClass39u.class, str);
    }

    public static AnonymousClass39u[] values() {
        return (AnonymousClass39u[]) A00.clone();
    }
}
