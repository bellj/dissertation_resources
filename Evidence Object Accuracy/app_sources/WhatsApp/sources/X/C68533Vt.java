package X;

import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.3Vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68533Vt implements AbstractC48942In {
    public final /* synthetic */ C44341yl A00;

    public C68533Vt(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC48942In
    public C59422uh A82(LatLng latLng, AnonymousClass2K1 r14, AnonymousClass1B4 r15, int i) {
        AnonymousClass01J r1 = this.A00.A03;
        C14900mE A0R = C12970iu.A0R(r1);
        C14850m9 A0S = C12960it.A0S(r1);
        AbstractC15710nm A0Q = C12970iu.A0Q(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        AnonymousClass018 A0R2 = C12960it.A0R(r1);
        C17170qN A0a = C12990iw.A0a(r1);
        return new C59422uh(latLng, A0Q, A0R, C12990iw.A0W(r1), r14, r15, (C16340oq) r1.A5z.get(), A0a, A0R2, A0S, A0T);
    }
}
