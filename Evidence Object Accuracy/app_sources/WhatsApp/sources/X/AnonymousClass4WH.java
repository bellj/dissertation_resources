package X;

/* renamed from: X.4WH  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4WH {
    public boolean A01(Object obj) {
        return true;
    }

    public void A00(Object obj) {
        if (this instanceof C82793wC) {
            ((AnonymousClass28D) obj).A02.put(35, ((C82793wC) this).A01);
        } else if (this instanceof C82763w9) {
            ((AnonymousClass28D) obj).A02.put(35, 0L);
        } else if (this instanceof C82783wB) {
            ((AnonymousClass28D) obj).A02.put(31, ((C82783wB) this).A01.A0E.toString());
        } else if (this instanceof C82753w8) {
            ((AnonymousClass28D) obj).A0K().remove(((C82753w8) this).A00);
        } else if (this instanceof C82743w7) {
            ((AnonymousClass28D) obj).A0K().addAll(C64943Hn.A02(((C82743w7) this).A00));
        } else if (this instanceof C82733w6) {
            ((AnonymousClass28D) obj).A0K().addAll(0, C64943Hn.A02(((C82733w6) this).A00));
        } else if (!(this instanceof C82773wA)) {
            AnonymousClass28D r5 = (AnonymousClass28D) obj;
            r5.A0K().clear();
            r5.A0K().addAll(C64943Hn.A02(((C82723w5) this).A00));
        } else {
            C82773wA r3 = (C82773wA) this;
            AnonymousClass28D r52 = (AnonymousClass28D) obj;
            int i = r3.A00;
            if (i == -1) {
                i = r52.A0K().size();
            }
            r52.A0K().add(i, r3.A01);
        }
    }
}
