package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.4by  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94524by {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A00 = AtomicReferenceFieldUpdater.newUpdater(C94524by.class, Object.class, "_next");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A01 = AtomicReferenceFieldUpdater.newUpdater(C94524by.class, Object.class, "_prev");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A02 = AtomicReferenceFieldUpdater.newUpdater(C94524by.class, Object.class, "_removedRef");
    public volatile /* synthetic */ Object _next = this;
    public volatile /* synthetic */ Object _prev = this;
    public volatile /* synthetic */ Object _removedRef = null;

    public final int A00(AnonymousClass5LC r3, C94524by r4, C94524by r5) {
        A01.lazySet(r4, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = A00;
        atomicReferenceFieldUpdater.lazySet(r4, r5);
        r3.A00 = r5;
        if (!AnonymousClass0KN.A00(this, r5, r3, atomicReferenceFieldUpdater)) {
            return 0;
        }
        return r3.A00(this) == null ? 1 : 2;
    }

    public final Object A01() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof AnonymousClass4WM)) {
                return obj;
            }
            ((AnonymousClass4WM) obj).A00(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0039, code lost:
        if (X.AnonymousClass0KN.A00(r4, r0, ((X.AnonymousClass4V9) r3).A00, X.C94524by.A00) == false) goto L_0x0001;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C94524by A02() {
        /*
            r7 = this;
            r6 = 0
        L_0x0001:
            java.lang.Object r5 = r7._prev
            X.4by r5 = (X.C94524by) r5
            r0 = r5
        L_0x0006:
            r4 = r6
        L_0x0007:
            java.lang.Object r3 = r0._next
            if (r3 != r7) goto L_0x0016
            if (r5 == r0) goto L_0x0047
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.C94524by.A01
            boolean r1 = X.AnonymousClass0KN.A00(r7, r5, r0, r1)
            if (r1 != 0) goto L_0x0047
            goto L_0x0001
        L_0x0016:
            boolean r1 = r7.A09()
            if (r1 == 0) goto L_0x001d
            return r6
        L_0x001d:
            if (r3 == r6) goto L_0x0047
            boolean r1 = r3 instanceof X.AnonymousClass4WM
            if (r1 == 0) goto L_0x0029
            X.4WM r3 = (X.AnonymousClass4WM) r3
            r3.A00(r0)
            goto L_0x0001
        L_0x0029:
            boolean r1 = r3 instanceof X.AnonymousClass4V9
            if (r1 == 0) goto L_0x0042
            if (r4 == 0) goto L_0x003d
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r2 = X.C94524by.A00
            X.4V9 r3 = (X.AnonymousClass4V9) r3
            X.4by r1 = r3.A00
            boolean r0 = X.AnonymousClass0KN.A00(r4, r0, r1, r2)
            if (r0 == 0) goto L_0x0001
            r0 = r4
            goto L_0x0006
        L_0x003d:
            java.lang.Object r0 = r0._prev
            X.4by r0 = (X.C94524by) r0
            goto L_0x0007
        L_0x0042:
            X.4by r3 = (X.C94524by) r3
            r4 = r0
            r0 = r3
            goto L_0x0007
        L_0x0047:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94524by.A02():X.4by");
    }

    public final C94524by A03() {
        AnonymousClass4V9 r0;
        C94524by r02;
        Object A012 = A01();
        if (!(A012 instanceof AnonymousClass4V9) || (r0 = (AnonymousClass4V9) A012) == null || (r02 = r0.A00) == null) {
            return (C94524by) A012;
        }
        return r02;
    }

    public final C94524by A04() {
        C94524by A022 = A02();
        if (A022 == null) {
            Object obj = this._prev;
            while (true) {
                A022 = (C94524by) obj;
                if (!A022.A09()) {
                    break;
                }
                obj = A022._prev;
            }
        }
        return A022;
    }

    public final C94524by A05() {
        Object A012;
        C94524by r2;
        Object obj;
        do {
            A012 = A01();
            if (A012 instanceof AnonymousClass4V9) {
                return ((AnonymousClass4V9) A012).A00;
            }
            if (A012 == this) {
                return (C94524by) A012;
            }
            r2 = (C94524by) A012;
            obj = r2._removedRef;
            if (obj == null) {
                obj = new AnonymousClass4V9(r2);
                A02.lazySet(r2, obj);
            }
        } while (!AnonymousClass0KN.A00(this, A012, obj, A00));
        r2.A02();
        return null;
    }

    public void A06() {
        if (!(this instanceof AnonymousClass5LE)) {
            A05();
            return;
        }
        throw C12960it.A0U("head cannot be removed");
    }

    public final void A07(C94524by r3) {
        A01.lazySet(r3, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = A00;
        atomicReferenceFieldUpdater.lazySet(r3, this);
        while (A01() == this) {
            if (AnonymousClass0KN.A00(this, this, r3, atomicReferenceFieldUpdater)) {
                r3.A08(this);
                return;
            }
        }
    }

    public final void A08(C94524by r3) {
        Object obj;
        do {
            obj = r3._prev;
            if (A01() != r3) {
                return;
            }
        } while (!AnonymousClass0KN.A00(r3, obj, this, A01));
        if (A09()) {
            r3.A02();
        }
    }

    public boolean A09() {
        if (!(this instanceof AnonymousClass5LE)) {
            return A01() instanceof AnonymousClass4V9;
        }
        return false;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(new AnonymousClass5KX(this) { // from class: X.5KW
        });
        A0h.append('@');
        return C12960it.A0d(C72453ed.A0o(this), A0h);
    }
}
