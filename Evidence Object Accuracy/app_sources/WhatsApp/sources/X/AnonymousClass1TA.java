package X;

import java.security.AccessController;

/* renamed from: X.1TA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TA {
    public static Class A00(Class cls, String str) {
        try {
            ClassLoader classLoader = cls.getClassLoader();
            return classLoader != null ? classLoader.loadClass(str) : (Class) AccessController.doPrivileged(new C111975Bo(str));
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }
}
