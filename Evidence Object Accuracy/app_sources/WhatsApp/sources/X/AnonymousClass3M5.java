package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.math.BigDecimal;

/* renamed from: X.3M5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M5 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(43);
    public int A00;
    public AnonymousClass3M0 A01;
    public C30711Yn A02;
    public BigDecimal A03;
    public final int A04;
    public final String A05;
    public final String A06;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M5(AnonymousClass3M0 r1, C30711Yn r2, String str, String str2, BigDecimal bigDecimal, int i, int i2) {
        this.A06 = str;
        this.A05 = str2;
        this.A03 = bigDecimal;
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
        this.A04 = i2;
    }

    public /* synthetic */ AnonymousClass3M5(Parcel parcel) {
        C30711Yn r0;
        this.A06 = C12990iw.A0p(parcel);
        this.A05 = C12990iw.A0p(parcel);
        this.A03 = (BigDecimal) parcel.readSerializable();
        String readString = parcel.readString();
        if (readString != null) {
            r0 = new C30711Yn(readString);
        } else {
            r0 = null;
        }
        this.A02 = r0;
        this.A01 = (AnonymousClass3M0) C12990iw.A0I(parcel, AnonymousClass3M0.class);
        this.A00 = parcel.readInt();
        this.A04 = parcel.readInt();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3M5 r5 = (AnonymousClass3M5) obj;
            if (this.A00 != r5.A00 || this.A04 != r5.A04 || !this.A06.equals(r5.A06) || !this.A05.equals(r5.A05) || !C29941Vi.A00(this.A03, r5.A03) || !C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[7];
        objArr[0] = this.A06;
        objArr[1] = this.A05;
        objArr[2] = this.A03;
        objArr[3] = this.A02;
        objArr[4] = this.A01;
        objArr[5] = Integer.valueOf(this.A00);
        return C12980iv.A0B(Integer.valueOf(this.A04), objArr, 6);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        String str;
        parcel.writeString(this.A06);
        parcel.writeString(this.A05);
        parcel.writeSerializable(this.A03);
        C30711Yn r0 = this.A02;
        if (r0 != null) {
            str = r0.A00;
        } else {
            str = null;
        }
        parcel.writeString(str);
        parcel.writeParcelable(this.A01, i);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A04);
    }
}
