package X;

/* renamed from: X.1nv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38341nv {
    public final boolean A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;
    public final boolean A04;

    public C38341nv(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.A04 = z;
        this.A00 = z2;
        this.A01 = z3;
        this.A02 = z4;
        this.A03 = z5;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("WriteResult{wasSuccess=");
        sb.append(this.A04);
        sb.append(", chatAdded=");
        sb.append(this.A00);
        sb.append(", chatUnarchived=");
        sb.append(this.A01);
        sb.append(", isDuplicate=");
        sb.append(this.A02);
        sb.append(", isExpired=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
