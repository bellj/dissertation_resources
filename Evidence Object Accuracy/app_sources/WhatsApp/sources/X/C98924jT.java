package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;

/* renamed from: X.4jT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98924jT implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Status status = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 1) {
                C95664e9.A0D(parcel, readInt);
            } else {
                status = (Status) C95664e9.A07(parcel, Status.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78663pH(status);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78663pH[i];
    }
}
