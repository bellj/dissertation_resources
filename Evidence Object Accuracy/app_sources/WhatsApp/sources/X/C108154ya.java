package X;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import com.facebook.msys.mci.DefaultCrypto;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108154ya implements AnonymousClass5QT {
    public static Boolean A01;
    public static Long A02;
    public static final AbstractC95414df A03;
    public static final AnonymousClass4PM A04;
    public static final AnonymousClass4PM A05 = new AnonymousClass4PM(new AnonymousClass4PM(new AnonymousClass4PM(Uri.parse(C72453ed.A0r("content://com.google.android.gms.phenotype/", Uri.encode("com.google.android.gms.clearcut.public"))), "", "").A00, "gms:playlog:service:sampling_", "").A00, "gms:playlog:service:sampling_", "LogSampling__");
    public static final Charset A06 = Charset.forName(DefaultCrypto.UTF_8);
    public static final HashMap A07 = C12970iu.A11();
    public static final ConcurrentHashMap A08 = new ConcurrentHashMap();
    public final Context A00;

    public C108154ya(Context context) {
        this.A00 = context;
        if (context != null) {
            AbstractC95414df.A01(context);
        }
    }

    public static long A00(long j, long j2, long j3) {
        long j4 = (j ^ j2) * j3;
        long j5 = ((j4 ^ (j4 >>> 47)) ^ j2) * j3;
        return (j5 ^ (j5 >>> 47)) * j3;
    }

    public static long A03(byte[] bArr, int i) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr, i, 8);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        return wrap.getLong();
    }

    public static void A04(byte[] bArr, long[] jArr, int i, long j, long j2) {
        long A032 = A03(bArr, i);
        long A033 = A03(bArr, i + 8);
        long A034 = A03(bArr, i + 16);
        long A035 = A03(bArr, i + 24);
        long j3 = j + A032;
        long j4 = A033 + j3 + A034;
        jArr[0] = j4 + A035;
        jArr[1] = Long.rotateRight(j2 + j3 + A035, 21) + Long.rotateRight(j4, 44) + j3;
    }

    static {
        String valueOf = String.valueOf(Uri.encode("com.google.android.gms.clearcut.public"));
        AnonymousClass4PM r4 = new AnonymousClass4PM(new AnonymousClass4PM(new AnonymousClass4PM(Uri.parse(C72453ed.A0s("content://com.google.android.gms.phenotype/", valueOf, valueOf.length())), "", "").A00, "gms:playlog:service:samplingrules_", "").A00, "gms:playlog:service:samplingrules_", "LogSamplingRules__");
        A04 = r4;
        A03 = new C79093q2(r4, false);
    }

    public static long A01(Context context) {
        Object obj;
        Long l = A02;
        if (l == null) {
            long j = 0;
            if (context == null) {
                return 0;
            }
            Boolean bool = A01;
            if (bool == null) {
                bool = Boolean.valueOf(C12960it.A1T(C15080mX.A00(context).A00.checkCallingOrSelfPermission("com.google.android.providers.gsf.permission.READ_GSERVICES")));
                A01 = bool;
            }
            if (bool.booleanValue()) {
                ContentResolver contentResolver = context.getContentResolver();
                synchronized (C95244dN.class) {
                    C95244dN.A01(contentResolver);
                    obj = C95244dN.A00;
                }
                HashMap hashMap = C95244dN.A08;
                long j2 = 0;
                long j3 = 0L;
                synchronized (C95244dN.class) {
                    if (hashMap.containsKey("android_id")) {
                        Object obj2 = hashMap.get("android_id");
                        if (obj2 != null) {
                            j3 = obj2;
                        }
                    } else {
                        j3 = null;
                    }
                }
                Long l2 = (Number) j3;
                if (l2 != null) {
                    j = l2.longValue();
                } else {
                    String A00 = C95244dN.A00(contentResolver, "android_id");
                    if (A00 != null) {
                        try {
                            long parseLong = Long.parseLong(A00);
                            l2 = Long.valueOf(parseLong);
                            j2 = parseLong;
                        } catch (NumberFormatException unused) {
                        }
                    }
                    synchronized (C95244dN.class) {
                        if (obj == C95244dN.A00) {
                            hashMap.put("android_id", l2);
                            C95244dN.A01.remove("android_id");
                        }
                    }
                    j = j2;
                }
            }
            l = Long.valueOf(j);
            A02 = l;
        }
        return l.longValue();
    }

    public static long A02(String str, long j) {
        ByteBuffer allocate;
        if (str == null || str.isEmpty()) {
            allocate = ByteBuffer.allocate(8);
        } else {
            byte[] bytes = str.getBytes(A06);
            allocate = ByteBuffer.allocate(bytes.length + 8);
            allocate.put(bytes);
        }
        allocate.putLong(j);
        byte[] array = allocate.array();
        int length = array.length;
        if (length > length) {
            throw new IndexOutOfBoundsException(C12960it.A0e("Out of bound index with offput: 0 and length: ", C12980iv.A0t(67), length));
        } else if (length <= 32) {
            if (length > 16) {
                long j2 = ((long) (length << 1)) - 7286425919675154353L;
                long A032 = A03(array, 0) * -5435081209227447693L;
                long A033 = A03(array, 8);
                int i = length + 0;
                long A034 = A03(array, i - 8) * j2;
                return A00(Long.rotateRight(A032 + A033, 43) + Long.rotateRight(A034, 30) + (A03(array, i - 16) * -7286425919675154353L), A032 + Long.rotateRight(A033 - 7286425919675154353L, 18) + A034, j2);
            } else if (length >= 8) {
                long j3 = ((long) (length << 1)) - 7286425919675154353L;
                long A035 = A03(array, 0) - 7286425919675154353L;
                long A036 = A03(array, (length + 0) - 8);
                return A00((Long.rotateRight(A036, 37) * j3) + A035, (Long.rotateRight(A035, 25) + A036) * j3, j3);
            } else if (length >= 4) {
                return A00(((long) length) + ((((long) (((array[3] & 255) << 24) | (((array[0] & 255) | ((array[1] & 255) << 8)) | ((array[2] & 255) << 16)))) & 4294967295L) << 3), ((long) C72453ed.A0K(array, (length + 0) - 4)) & 4294967295L, ((long) (length << 1)) - 7286425919675154353L);
            } else if (length <= 0) {
                return -7286425919675154353L;
            } else {
                long j4 = (((long) ((array[0] & 255) + ((array[(length >> 1) + 0] & 255) << 8))) * -7286425919675154353L) ^ (((long) (length + ((array[(length - 1) + 0] & 255) << 2))) * -4348849565147123417L);
                return (j4 ^ (j4 >>> 47)) * -7286425919675154353L;
            }
        } else if (length <= 64) {
            long j5 = ((long) (length << 1)) - 7286425919675154353L;
            long A037 = A03(array, 0) * -7286425919675154353L;
            long A038 = A03(array, 8);
            int i2 = length + 0;
            long A039 = A03(array, i2 - 8) * j5;
            long rotateRight = Long.rotateRight(A037 + A038, 43) + Long.rotateRight(A039, 30) + (A03(array, i2 - 16) * -7286425919675154353L);
            long A00 = A00(rotateRight, Long.rotateRight(-7286425919675154353L + A038, 18) + A037 + A039, j5);
            long A0310 = A03(array, 16) * j5;
            long A0311 = A03(array, 24);
            long A0312 = (rotateRight + A03(array, i2 - 32)) * j5;
            return A00(((A00 + A03(array, i2 - 24)) * j5) + Long.rotateRight(A0310 + A0311, 43) + Long.rotateRight(A0312, 30), A0310 + Long.rotateRight(A0311 + A037, 18) + A0312, j5);
        } else {
            long j6 = 2480279821605975764L;
            long j7 = 1390051526045402406L;
            long[] jArr = new long[2];
            long[] jArr2 = new long[2];
            long A0313 = A03(array, 0) + 95310865018149119L;
            int i3 = length - 1;
            int i4 = (i3 >> 6) << 6;
            int i5 = i3 & 63;
            int i6 = (i4 + i5) - 63;
            int i7 = 0;
            while (true) {
                long rotateRight2 = (Long.rotateRight(((A0313 + j6) + jArr[0]) + A03(array, i7 + 8), 37) * -5435081209227447693L) ^ jArr2[1];
                j6 = (Long.rotateRight(j6 + jArr[1] + A03(array, i7 + 48), 42) * -5435081209227447693L) + jArr[0] + A03(array, i7 + 40);
                A0313 = Long.rotateRight(j7 + jArr2[0], 33) * -5435081209227447693L;
                A04(array, jArr, i7, jArr[1] * -5435081209227447693L, rotateRight2 + jArr2[0]);
                A04(array, jArr2, i7 + 32, A0313 + jArr2[1], j6 + A03(array, i7 + 16));
                i7 += 64;
                if (i7 == i4) {
                    long j8 = -5435081209227447693L + ((255 & rotateRight2) << 1);
                    long j9 = jArr2[0] + ((long) i5);
                    jArr2[0] = j9;
                    long j10 = jArr[0] + j9;
                    jArr[0] = j10;
                    jArr2[0] = jArr2[0] + j10;
                    long rotateRight3 = (Long.rotateRight(((A0313 + j6) + jArr[0]) + A03(array, i6 + 8), 37) * j8) ^ (jArr2[1] * 9);
                    long rotateRight4 = (Long.rotateRight(j6 + jArr[1] + A03(array, i6 + 48), 42) * j8) + (jArr[0] * 9) + A03(array, i6 + 40);
                    long rotateRight5 = Long.rotateRight(rotateRight2 + jArr2[0], 33) * j8;
                    A04(array, jArr, i6, jArr[1] * j8, rotateRight3 + jArr2[0]);
                    A04(array, jArr2, i6 + 32, jArr2[1] + rotateRight5, rotateRight4 + A03(array, i6 + 16));
                    return A00(A00(jArr[0], jArr2[0], j8) + ((rotateRight4 ^ (rotateRight4 >>> 47)) * -4348849565147123417L) + rotateRight3, A00(jArr[1], jArr2[1], j8) + rotateRight5, j8);
                }
                j7 = rotateRight2;
            }
        }
    }
}
