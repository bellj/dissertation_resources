package X;

/* renamed from: X.43n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856243n extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Long A06;
    public Long A07;

    public C856243n() {
        super(2110, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A03);
        r3.Abe(4, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(8, this.A02);
        r3.Abe(6, this.A04);
        r3.Abe(1, this.A06);
        r3.Abe(5, this.A05);
        r3.Abe(2, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPushLatency {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deliveredPriority", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "fbnsAvailable", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gcmAvailable", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasSessionId", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "originalPriority", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushDelayT", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushTransport", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "serverTimestampT", this.A07);
        return C12960it.A0d("}", A0k);
    }
}
