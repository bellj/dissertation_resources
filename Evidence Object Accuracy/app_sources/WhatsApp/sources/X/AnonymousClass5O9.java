package X;

/* renamed from: X.5O9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O9 extends AnonymousClass5OF {
    public static final int[] A04;
    public int A00;
    public int[] A01;
    public int[] A02;
    public int[] A03;

    static {
        int i;
        int[] iArr = new int[64];
        A04 = iArr;
        int i2 = 0;
        do {
            iArr[i2] = (2043430169 >>> (32 - i2)) | (2043430169 << i2);
            i2++;
            i = 16;
        } while (i2 < 16);
        do {
            int i3 = i % 32;
            iArr[i] = (2055708042 >>> (32 - i3)) | (2055708042 << i3);
            i++;
        } while (i < 64);
    }

    public AnonymousClass5O9() {
        this.A01 = new int[8];
        this.A03 = new int[16];
        this.A02 = new int[68];
        reset();
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5O9(this);
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "SM3";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 32;
    }

    @Override // X.AnonymousClass5OF, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        int[] iArr = this.A01;
        iArr[0] = 1937774191;
        iArr[1] = 1226093241;
        iArr[2] = 388252375;
        iArr[3] = -628488704;
        iArr[4] = -1452330820;
        iArr[5] = 372324522;
        iArr[6] = -477237683;
        iArr[7] = -1325724082;
        this.A00 = 0;
    }

    public AnonymousClass5O9(AnonymousClass5O9 r5) {
        super(r5);
        int[] iArr = new int[8];
        this.A01 = iArr;
        this.A03 = new int[16];
        this.A02 = new int[68];
        System.arraycopy(r5.A01, 0, iArr, 0, 8);
        int[] iArr2 = r5.A03;
        int[] iArr3 = this.A03;
        System.arraycopy(iArr2, 0, iArr3, 0, iArr3.length);
        this.A00 = r5.A00;
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        A0K();
        for (int i2 : this.A01) {
            AbstractC95434di.A01(bArr, i2, i);
            i += 4;
        }
        reset();
        return 32;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r5) {
        AnonymousClass5O9 r52 = (AnonymousClass5O9) r5;
        super.A0M(r52);
        int[] iArr = r52.A01;
        int[] iArr2 = this.A01;
        System.arraycopy(iArr, 0, iArr2, 0, iArr2.length);
        int[] iArr3 = r52.A03;
        int[] iArr4 = this.A03;
        System.arraycopy(iArr3, 0, iArr4, 0, iArr4.length);
        this.A00 = r52.A00;
    }
}
