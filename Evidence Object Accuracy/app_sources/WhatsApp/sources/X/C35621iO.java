package X;

import java.util.Comparator;

/* renamed from: X.1iO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35621iO {
    public static Comparator A00 = new Comparator() { // from class: X.5CO
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return (int) (((AbstractC15340mz) obj2).A12 - ((AbstractC15340mz) obj).A12);
        }
    };
}
