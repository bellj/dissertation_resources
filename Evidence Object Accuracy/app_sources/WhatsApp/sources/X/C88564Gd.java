package X;

/* renamed from: X.4Gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88564Gd {
    public static final boolean A00;

    static {
        Object obj;
        try {
            obj = Class.forName("android.os.Build");
        } catch (Throwable th) {
            obj = new AnonymousClass5BR(th);
        }
        A00 = !(obj instanceof AnonymousClass5BR);
    }
}
