package X;

import android.view.View;

/* renamed from: X.0TD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TD {
    public static int A00(View view, View view2, AbstractC06220Sq r4, AnonymousClass02H r5, C05480Ps r6, boolean z) {
        if (r5.A06() == 0 || r6.A00() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(AnonymousClass02H.A02(view) - AnonymousClass02H.A02(view2)) + 1;
        }
        return Math.min(r4.A07(), r4.A08(view2) - r4.A0B(view));
    }

    public static int A01(View view, View view2, AbstractC06220Sq r5, AnonymousClass02H r6, C05480Ps r7, boolean z) {
        int A00;
        if (r6.A06() == 0 || (A00 = r7.A00()) == 0 || view == null || view2 == null) {
            return 0;
        }
        return z ? (int) ((((float) (r5.A08(view2) - r5.A0B(view))) / ((float) (Math.abs(AnonymousClass02H.A02(view) - AnonymousClass02H.A02(view2)) + 1))) * ((float) r7.A00())) : A00;
    }

    public static int A02(View view, View view2, AbstractC06220Sq r6, AnonymousClass02H r7, C05480Ps r8, boolean z, boolean z2) {
        int max;
        if (r7.A06() == 0 || r8.A00() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(AnonymousClass02H.A02(view), AnonymousClass02H.A02(view2));
        int max2 = Math.max(AnonymousClass02H.A02(view), AnonymousClass02H.A02(view2));
        if (z2) {
            max = Math.max(0, (r8.A00() - max2) - 1);
        } else {
            max = Math.max(0, min);
        }
        if (!z) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(r6.A08(view2) - r6.A0B(view))) / ((float) (Math.abs(AnonymousClass02H.A02(view) - AnonymousClass02H.A02(view2)) + 1)))) + ((float) (r6.A06() - r6.A0B(view))));
    }
}
