package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.2kA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56002kA extends AbstractC65073Ia implements AnonymousClass5SC {
    public static AnonymousClass5WW A0F = new AnonymousClass3SU();
    public static AnonymousClass5WW A0G = new C106274vR();
    public static AnonymousClass5WW A0H = new C106244vO();
    public static AnonymousClass5WW A0I = new C106264vQ();
    public static AnonymousClass5WW A0J = new AnonymousClass3SX();
    public static AnonymousClass5WW A0K = new AnonymousClass3SY();
    public static AnonymousClass5WW A0L = new C106254vP();
    public static AnonymousClass5WW A0M = new C106284vS();
    public static AnonymousClass5WW A0N = new AnonymousClass3SV();
    public static AnonymousClass5WW A0O = new AnonymousClass3SW();
    public int A00 = 0;
    public int A01;
    public int A02;
    public int A03 = 0;
    public int A04;
    public AnonymousClass04Y A05;
    public AnonymousClass0FB A06;
    public AnonymousClass4IB A07;
    public C54502gp A08;
    public List A09;
    public List A0A;
    public List A0B;
    public boolean A0C;
    public final long A0D;
    public final AnonymousClass5SF A0E;

    public C56002kA(AnonymousClass5SF r6, long j) {
        super(EnumC869849t.VIEW);
        this.A0D = j;
        this.A0E = r6;
        C93304Zx[] r2 = {new C93304Zx(A0L, this), new C93304Zx(A0F, this), new C93304Zx(A0N, this), new C93304Zx(A0O, this), new C93304Zx(A0J, this), new C93304Zx(A0I, this), new C93304Zx(A0K, this), new C93304Zx(A0G, this), new C93304Zx(A0M, this), new C93304Zx(A0H, this)};
        int i = 0;
        do {
            A04(r2[i]);
            i++;
        } while (i < 10);
    }

    public void A05(AbstractC05270Ox r2) {
        List list = this.A0B;
        if (list == null) {
            list = C12980iv.A0w(4);
            this.A0B = list;
        }
        list.add(r2);
    }

    @Override // X.AnonymousClass5SC
    public Object A8B(Context context) {
        return new RecyclerView(context);
    }
}
