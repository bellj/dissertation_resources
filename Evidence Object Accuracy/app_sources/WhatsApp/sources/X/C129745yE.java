package X;

import android.app.Activity;
import android.widget.PopupWindow;
import com.whatsapp.KeyboardPopupLayout;
import java.util.HashMap;

/* renamed from: X.5yE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129745yE {
    public final Activity A00;
    public final AbstractC15710nm A01;
    public final KeyboardPopupLayout A02;
    public final AnonymousClass01d A03;
    public final C14820m6 A04;
    public final AnonymousClass018 A05;
    public final AnonymousClass1AD A06;
    public final AnonymousClass19M A07;
    public final C231510o A08;
    public final AnonymousClass193 A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final AnonymousClass1BP A0C;
    public final C253719d A0D;
    public final AbstractC253919f A0E;
    public final C16630pM A0F;
    public final AnonymousClass1BQ A0G;
    public final C252718t A0H;
    public final HashMap A0I = C12970iu.A11();

    public C129745yE(Activity activity, AbstractC15710nm r3, KeyboardPopupLayout keyboardPopupLayout, AnonymousClass01d r5, C14820m6 r6, AnonymousClass018 r7, AnonymousClass1AD r8, AnonymousClass19M r9, C231510o r10, AnonymousClass193 r11, C14850m9 r12, C16120oU r13, AnonymousClass1BP r14, C253719d r15, AbstractC253919f r16, C16630pM r17, AnonymousClass1BQ r18, C252718t r19) {
        this.A0A = r12;
        this.A0D = r15;
        this.A0H = r19;
        this.A01 = r3;
        this.A0B = r13;
        this.A07 = r9;
        this.A08 = r10;
        this.A03 = r5;
        this.A05 = r7;
        this.A0E = r16;
        this.A09 = r11;
        this.A04 = r6;
        this.A0F = r17;
        this.A0C = r14;
        this.A0G = r18;
        this.A00 = activity;
        this.A02 = keyboardPopupLayout;
        this.A06 = r8;
    }

    public void A00() {
        PopupWindow popupWindow;
        HashMap hashMap = this.A0I;
        if (hashMap.containsKey(1) && (popupWindow = (PopupWindow) hashMap.get(1)) != null) {
            popupWindow.dismiss();
        }
    }

    public void A01(int i) {
        AbstractC15280mr r0;
        PopupWindow popupWindow;
        HashMap hashMap = this.A0I;
        for (Number number : hashMap.keySet()) {
            if (!(number.intValue() == i || (popupWindow = (PopupWindow) hashMap.get(number)) == null)) {
                popupWindow.dismiss();
            }
        }
        Integer valueOf = Integer.valueOf(i);
        if (hashMap.containsKey(valueOf) && (r0 = (AbstractC15280mr) hashMap.get(valueOf)) != null) {
            r0.A06();
        }
    }
}
