package X;

import android.widget.ImageView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63493Bu {
    public final int A00;
    public final int A01;
    public final ImageView.ScaleType A02;
    public final C90244Nd A03;
    public final AnonymousClass2OC A04;
    public final AnonymousClass2OC A05;
    public final AnonymousClass2OC A06;
    public final List A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;

    public C63493Bu(ImageView.ScaleType scaleType, C90244Nd r4, AnonymousClass2OC r5, AnonymousClass2OC r6, AnonymousClass2OC r7, List list, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A01 = i;
        this.A06 = r5;
        this.A05 = r6;
        this.A00 = i2;
        this.A04 = r7;
        this.A09 = z3;
        this.A08 = z4;
        this.A02 = scaleType;
        ArrayList A0l = C12960it.A0l();
        this.A07 = A0l;
        if (z && !list.isEmpty()) {
            A0l.addAll(list);
            this.A0A = z;
        }
        this.A03 = r4;
        this.A0B = z2;
    }
}
