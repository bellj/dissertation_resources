package X;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.whatsapp.Conversation;
import com.whatsapp.conversation.selectlist.SelectListBottomSheet;

/* renamed from: X.2Pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50402Pk implements AbstractC50412Pl {
    public final C14900mE A00;

    public C50402Pk(C14900mE r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC50412Pl
    public void AZA(Context context, AbstractC15340mz r5, C16470p4 r6, int i) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("arg_select_list_content", r6);
        SelectListBottomSheet selectListBottomSheet = new SelectListBottomSheet();
        selectListBottomSheet.A0U(bundle);
        selectListBottomSheet.A00 = new AnonymousClass4Q8(context, this, r5);
        Activity A00 = AbstractC35731ia.A00(context);
        if (A00 instanceof Conversation) {
            C42791vs.A01(selectListBottomSheet, ((ActivityC000900k) A00).A0V());
        }
    }
}
