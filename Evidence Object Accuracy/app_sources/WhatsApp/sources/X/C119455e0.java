package X;

import android.app.AlertDialog;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;

/* renamed from: X.5e0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119455e0 extends AbstractC58672rA {
    public byte[] A00;
    public final ActivityC13810kN A01;
    public final FingerprintBottomSheet A02;
    public final C14830m7 A03;
    public final C129155xG A04;
    public final AnonymousClass6MY A05;
    public final C130015yf A06;

    public C119455e0(ActivityC13810kN r1, FingerprintBottomSheet fingerprintBottomSheet, C14830m7 r3, C129155xG r4, AnonymousClass6MY r5, C130015yf r6) {
        this.A03 = r3;
        this.A06 = r6;
        this.A01 = r1;
        this.A02 = fingerprintBottomSheet;
        this.A04 = r4;
        this.A05 = r5;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        FingerprintBottomSheet fingerprintBottomSheet = this.A02;
        fingerprintBottomSheet.A1G(true);
        this.A05.AX7(this.A00);
        fingerprintBottomSheet.A1C();
    }

    @Override // X.AbstractC58672rA
    public void A02() {
        this.A05.AW2();
    }

    @Override // X.AbstractC58672rA
    public void A04(AnonymousClass02N r9, AnonymousClass21K r10) {
        Object[] A1a;
        long A00 = this.A06.A00() * 1000;
        if (A00 > this.A03.A00()) {
            this.A02.A1L(A00);
            return;
        }
        C129155xG r6 = this.A04;
        C1323566o r5 = new C1323566o(r10, this);
        long A03 = C117295Zj.A03(r6.A01);
        if (r6 instanceof C121195hP) {
            A1a = C12980iv.A1a();
            A1a[0] = ((C121195hP) r6).A01;
            C117315Zl.A0Z(A1a, A03);
        } else if (!(r6 instanceof C121185hO)) {
            A1a = C12970iu.A1b();
            A1a[0] = Long.valueOf(A03);
        } else {
            C121185hO r4 = (C121185hO) r6;
            A1a = new Object[3];
            A1a[0] = r4.A00;
            C117315Zl.A0Z(A1a, A03);
            A1a[2] = r4.A01;
        }
        if (!r6.A04.A08(r9, new C1323366m(r6, r5, A03), AnonymousClass605.A00(A1a))) {
            C119455e0 r1 = r5.A01;
            r1.A02.A1B();
            new AlertDialog.Builder(r1.A01).setTitle(R.string.payments_biometric_invalidated_key_title).setMessage(R.string.payments_biometric_invalidated_key_error).setPositiveButton(R.string.ok, new IDxCListenerShape10S0100000_3_I1(r5, 3)).setCancelable(false).show();
        }
    }

    @Override // X.AbstractC58672rA
    public void A06(byte[] bArr) {
        this.A00 = bArr;
    }
}
