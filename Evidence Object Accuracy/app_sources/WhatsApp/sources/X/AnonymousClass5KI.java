package X;

/* renamed from: X.5KI  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KI extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ CharSequence $this_splitToSequence;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KI(CharSequence charSequence) {
        super(1);
        this.$this_splitToSequence = charSequence;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C114075Kc r2 = (C114075Kc) obj;
        C16700pc.A0E(r2, 0);
        return AnonymousClass03B.A05(this.$this_splitToSequence, r2);
    }
}
