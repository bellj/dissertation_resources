package X;

/* renamed from: X.0vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20360vd {
    public final C15570nT A00;
    public final C26281Cs A01;

    public C20360vd(C15570nT r1, C26281Cs r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(C30921Zi r10, AbstractC15340mz r11) {
        AnonymousClass1IR r0 = r11.A0L;
        if (r0 != null) {
            String A04 = C31001Zq.A04(r0.A03);
            if ("p2p".equals(A04) || "p2m".equals(A04)) {
                C26281Cs r5 = this.A01;
                boolean z = false;
                if (r10 != null) {
                    z = true;
                }
                boolean z2 = r11 instanceof C30061Vy;
                synchronized (r5) {
                    C26291Ct r6 = r5.A01;
                    C456522m A00 = r6.A00();
                    A00.A05++;
                    if (z) {
                        A00.A0A++;
                    }
                    if (z2) {
                        A00.A0B++;
                        if (z) {
                            A00.A09++;
                        }
                    }
                    r6.A01(A00);
                }
            }
        }
    }
}
