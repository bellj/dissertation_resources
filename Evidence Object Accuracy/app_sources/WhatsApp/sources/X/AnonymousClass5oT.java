package X;

import java.util.List;

/* renamed from: X.5oT  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oT extends AbstractC16350or {
    public final C17070qD A00;
    public final C125845ru A01;

    public AnonymousClass5oT(ActivityC13790kL r1, C17070qD r2, C125845ru r3) {
        super(r1);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A0Z = C117295Zj.A0Z(this.A00);
        if (A0Z.isEmpty()) {
            return null;
        }
        AnonymousClass1ZY r1 = C117315Zl.A08(A0Z, C1311161i.A01(A0Z)).A08;
        if (r1 instanceof C119755f3) {
            return r1;
        }
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C119755f3 r4 = (C119755f3) obj;
        AbstractActivityC121505iP r0 = this.A01.A00;
        if (r4 != null) {
            r0.AaN();
            r0.A32(r4);
            return;
        }
        ((AbstractActivityC121685jC) r0).A0M.A08(new C1327368a(r0), 2);
    }
}
