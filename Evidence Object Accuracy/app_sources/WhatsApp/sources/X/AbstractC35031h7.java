package X;

import android.app.Notification;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1h7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35031h7 {
    public void A01(Notification notification, Context context, int i) {
    }

    public List A00(Context context) {
        String[] strArr;
        char c;
        String str;
        String str2;
        if (this instanceof C35061hA) {
            strArr = new String[3];
            strArr[0] = "com.miui.miuilite";
            strArr[1] = "com.miui.miuihome";
            c = 2;
            str = "com.miui.miuihome2";
        } else if (this instanceof C35071hB) {
            strArr = new String[3];
            strArr[0] = "com.sonyericsson.home";
            strArr[1] = "com.sonymobile.home";
            c = 2;
            str = "com.sonymobile.launcher";
        } else if (this instanceof C35021h6) {
            return null;
        } else {
            if (!(this instanceof C35051h9)) {
                if (this instanceof C35101hE) {
                    C35101hE r6 = (C35101hE) this;
                    String str3 = Build.MANUFACTURER;
                    if (str3.equalsIgnoreCase("OPPO") || str3.equalsIgnoreCase("realme")) {
                        String str4 = Build.BRAND;
                        if (str4.equalsIgnoreCase("oppo") || str4.equalsIgnoreCase("realme")) {
                            boolean z = false;
                            try {
                                z = !TextUtils.isEmpty((String) Class.forName("android.os.SystemProperties").getDeclaredMethod("get", String.class).invoke(null, "ro.build.version.oplusrom"));
                            } catch (Exception e) {
                                Log.e("Error while checking oppo launcher information", e);
                            }
                            if (z && r6.A00.A07(503)) {
                                str2 = "com.android.launcher";
                            }
                        }
                    }
                    str2 = "com.oppo.launcher";
                } else if (this instanceof C35091hD) {
                    str2 = "com.huawei.android.launcher";
                } else if (!(this instanceof C35081hC)) {
                    strArr = new String[4];
                    strArr[0] = "com.asus.launcher";
                    strArr[1] = "com.lge.launcher";
                    strArr[2] = "com.lge.launcher2";
                    c = 3;
                    str = "com.lge.launcher3";
                } else if (((C35081hC) this).A03(context) < 4.0f) {
                    return new ArrayList();
                } else {
                    str2 = "com.htc.launcher";
                }
                return Collections.singletonList(str2);
            }
            strArr = new String[4];
            strArr[0] = "com.sec.android.app.launcher";
            strArr[1] = "com.sec.android.app.twlauncher";
            strArr[2] = "com.sec.android.app.easylauncher";
            c = 3;
            str = "com.sec.android.emergencylauncher";
        }
        strArr[c] = str;
        return Arrays.asList(strArr);
    }

    public void A02(Context context, int i) {
        Intent intent;
        Object valueOf;
        String valueOf2;
        String str;
        if (this instanceof C35061hA) {
            try {
                Object newInstance = Class.forName("android.app.MiuiNotification").newInstance();
                Field declaredField = newInstance.getClass().getDeclaredField("messageCount");
                declaredField.setAccessible(true);
                declaredField.set(newInstance, Integer.valueOf(i));
                return;
            } catch (Exception unused) {
                intent = new Intent("android.intent.action.APPLICATION_MESSAGE_UPDATE");
                StringBuilder sb = new StringBuilder();
                sb.append(context.getPackageName());
                sb.append("/");
                sb.append("com.whatsapp.Main");
                intent.putExtra("android.intent.extra.update_application_component_name", sb.toString());
                if (i == 0) {
                    valueOf = "";
                } else {
                    valueOf = Integer.valueOf(i);
                }
                valueOf2 = String.valueOf(valueOf);
                str = "android.intent.extra.update_application_message_text";
            }
        } else if (this instanceof C35071hB) {
            boolean z = false;
            if (context.getPackageManager().resolveContentProvider("com.sonymobile.home.resourceprovider", 0) == null) {
                intent = new Intent("com.sonyericsson.home.action.UPDATE_BADGE");
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME", context.getPackageName());
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME", "com.whatsapp.Main");
                if (i > 0) {
                    z = true;
                }
                intent.putExtra("com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE", z);
                valueOf2 = String.valueOf(i);
                str = "com.sonyericsson.home.intent.extra.badge.MESSAGE";
            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("badge_count", Integer.valueOf(i));
                contentValues.put("package_name", context.getPackageName());
                contentValues.put("activity_name", "com.whatsapp.Main");
                try {
                    context.getContentResolver().insert(Uri.parse("content://com.sonymobile.home.resourceprovider/badge"), contentValues);
                    return;
                } catch (Exception e) {
                    Log.e("badger/sony/updatebadge", e);
                    return;
                }
            }
        } else if (this instanceof C35021h6) {
            return;
        } else {
            if (!(this instanceof C35081hC)) {
                intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
                intent.putExtra("badge_count", i);
                intent.putExtra("badge_count_package_name", context.getPackageName());
                valueOf2 = "com.whatsapp.Main";
                str = "badge_count_class_name";
            } else {
                float A03 = ((C35081hC) this).A03(context);
                if (A03 >= 5.0f) {
                    Intent intent2 = new Intent("com.htc.launcher.action.SET_NOTIFICATION");
                    intent2.setFlags(16);
                    intent2.putExtra("com.htc.launcher.extra.COMPONENT", new ComponentName(context.getPackageName(), "com.whatsapp.Main").flattenToShortString());
                    intent2.putExtra("com.htc.launcher.extra.COUNT", i);
                    context.sendBroadcast(intent2);
                    return;
                } else if (A03 >= 4.0f) {
                    Intent intent3 = new Intent("com.htc.launcher.action.UPDATE_SHORTCUT");
                    intent3.setFlags(16);
                    intent3.putExtra("packagename", context.getPackageName());
                    intent3.putExtra("count", i);
                    context.sendBroadcast(intent3);
                    ContentResolver contentResolver = context.getContentResolver();
                    HashSet hashSet = new HashSet();
                    StringBuilder sb2 = new StringBuilder("%");
                    sb2.append(context.getPackageName());
                    sb2.append("%");
                    Cursor query = contentResolver.query(Uri.parse("content://com.htc.launcher.settings/favorites"), new String[]{"_id", "intent"}, "intent LIKE ?", new String[]{sb2.toString()}, null);
                    if (query != null) {
                        try {
                            int columnIndex = query.getColumnIndex("_id");
                            int columnIndex2 = query.getColumnIndex("intent");
                            query.moveToFirst();
                            while (!query.isAfterLast()) {
                                try {
                                    ComponentName component = Intent.parseUri(query.getString(columnIndex2), 0).getComponent();
                                    if (component != null && context.getPackageName().equals(component.getPackageName()) && "com.whatsapp.Main".equals(component.getClassName())) {
                                        hashSet.add(Integer.valueOf(query.getInt(columnIndex)));
                                    }
                                } catch (URISyntaxException unused2) {
                                }
                                query.moveToNext();
                            }
                            query.close();
                        } catch (Throwable th) {
                            try {
                                query.close();
                            } catch (Throwable unused3) {
                            }
                            throw th;
                        }
                    }
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        Intent intent4 = new Intent("com.htc.launcher.action.UPDATE_SHORTCUT");
                        intent3.setFlags(16);
                        intent4.putExtra("packagename", context.getPackageName());
                        intent4.putExtra("favorite_item_id", ((Number) it.next()).longValue());
                        ComponentName componentName = new ComponentName(context.getPackageName(), "com.whatsapp.Main");
                        StringBuilder sb3 = new StringBuilder("%");
                        sb3.append(componentName.flattenToShortString());
                        sb3.append("%");
                        intent4.putExtra("selectArgs", new String[]{sb3.toString()});
                        intent4.putExtra("count", i);
                        context.sendBroadcast(intent4);
                    }
                    return;
                } else {
                    return;
                }
            }
        }
        intent.putExtra(str, valueOf2);
        context.sendBroadcast(intent);
    }
}
