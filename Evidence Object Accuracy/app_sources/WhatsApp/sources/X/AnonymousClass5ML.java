package X;

/* renamed from: X.5ML  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5ML extends AnonymousClass1TM {
    public AnonymousClass5NE A00;
    public AnonymousClass5NE A01;
    public C114555Me A02;
    public C114755My A03;
    public AnonymousClass5MX A04;

    public AnonymousClass5ML(AbstractC114775Na r5) {
        C114555Me r1;
        C114755My r2;
        AnonymousClass5NU r12;
        AnonymousClass1TN A00 = AbstractC114775Na.A00(r5);
        if (A00 instanceof C114555Me) {
            r1 = (C114555Me) A00;
        } else {
            r1 = A00 != null ? new C114555Me(AbstractC114775Na.A04(A00)) : null;
        }
        this.A02 = r1;
        AnonymousClass1TN A0D = r5.A0D(1);
        if (A0D == null || (A0D instanceof C114755My)) {
            r2 = (C114755My) A0D;
        } else if (A0D instanceof AnonymousClass5NU) {
            r2 = new C114755My((AnonymousClass5NU) A0D);
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(A0D), C12960it.A0k("unknown object in factory: ")));
        }
        this.A03 = r2;
        this.A01 = AnonymousClass5NE.A01(r5.A0D(2));
        if (r5.A0B() > 4) {
            this.A00 = AnonymousClass5NE.A01(AnonymousClass5NU.A00((AnonymousClass5NU) r5.A0D(3)));
            r12 = (AnonymousClass5NU) r5.A0D(4);
        } else if (r5.A0B() > 3) {
            r12 = (AnonymousClass5NU) r5.A0D(3);
            if (r12.A00 == 0) {
                this.A00 = AnonymousClass5NE.A01(AnonymousClass5NU.A00(r12));
                return;
            }
        } else {
            return;
        }
        this.A04 = AnonymousClass5MX.A01(AbstractC114775Na.A05(r12, true));
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(5);
        r3.A06(this.A02);
        r3.A06(this.A03);
        r3.A06(this.A01);
        AnonymousClass5NE r2 = this.A00;
        if (r2 != null) {
            C94954co.A02(r2, r3, 0, true);
        }
        AnonymousClass5MX r0 = this.A04;
        if (r0 != null) {
            C94954co.A03(r0, r3, true);
        }
        return new AnonymousClass5NZ(r3);
    }
}
