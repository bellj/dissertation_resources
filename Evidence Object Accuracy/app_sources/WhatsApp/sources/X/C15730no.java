package X;

import android.content.ComponentName;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.CancellationSignal;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.0no  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15730no {
    public CancellationSignal A00;
    public CountDownLatch A01;
    public final AbstractC15710nm A02;
    public final AnonymousClass01d A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C16120oU A06;
    public final C20740wF A07;
    public final C20180vL A08;
    public final C235211z A09;
    public final C15790nu A0A;
    public final C15750nq A0B;
    public final AnonymousClass114 A0C;
    public final AnonymousClass111 A0D;
    public final AnonymousClass2F3 A0E;
    public final C20170vK A0F;
    public final C19520uF A0G;
    public final C15760nr A0H;
    public final C18350sJ A0I;
    public final C28181Ma A0J;
    public final AnonymousClass01H A0K;
    public final AnonymousClass01H A0L;
    public final AnonymousClass01H A0M;
    public final List A0N = new ArrayList();

    public C15730no(AbstractC15710nm r3, AnonymousClass01d r4, C14830m7 r5, C14850m9 r6, C16120oU r7, C20740wF r8, C20180vL r9, C235211z r10, C15790nu r11, C15750nq r12, AnonymousClass114 r13, AnonymousClass111 r14, C20170vK r15, C19520uF r16, C15760nr r17, C18350sJ r18, AnonymousClass01H r19, AnonymousClass01H r20, AnonymousClass01H r21) {
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
        this.A06 = r7;
        this.A0B = r12;
        this.A03 = r4;
        this.A08 = r9;
        this.A0M = r19;
        this.A0D = r14;
        this.A07 = r8;
        this.A0I = r18;
        this.A0G = r16;
        this.A0H = r17;
        this.A0C = r13;
        this.A09 = r10;
        this.A0F = r15;
        this.A0L = r20;
        this.A0K = r21;
        this.A0A = r11;
        C28181Ma r0 = new C28181Ma("ExportFlowManager/duration");
        this.A0J = r0;
        r0.A01();
        AnonymousClass2F3 r02 = new AnonymousClass2F3(this);
        this.A0E = r02;
        r15.A03(r02);
    }

    public final synchronized long A00() {
        return this.A0J.A00();
    }

    public final C41761u5 A01(int i) {
        long j;
        long j2;
        long j3;
        C15750nq r2 = this.A0B;
        String A02 = r2.A02();
        C41761u5 r10 = new C41761u5();
        r10.A05 = Integer.valueOf(i);
        r10.A09 = A02;
        r10.A03 = 1;
        if (10 == i) {
            r10.A07 = Long.valueOf(A00() / 1000);
            C21660xm r11 = (C21660xm) this.A0L.get();
            Long A00 = ((C20820wN) this.A0K.get()).A00();
            C19540uH r5 = this.A0H.A01.A00;
            C16310on A002 = r5.A00();
            try {
                Cursor A09 = A002.A03.A09("SELECT  f.file_size AS exported_file_size FROM exported_files_metadata AS f WHERE f.exported_path = ?", new String[]{"migration/messages_export.zip"});
                if (!A09.moveToFirst()) {
                    j2 = 0;
                } else {
                    j2 = A09.getLong(A09.getColumnIndexOrThrow("exported_file_size"));
                }
                A09.close();
                A002.close();
                A002 = r5.A00();
                try {
                    Cursor A092 = A002.A03.A09("SELECT  SUM(f.file_size) AS media_size FROM exported_files_metadata AS f WHERE f.required = 0", null);
                    if (!A092.moveToFirst()) {
                        j3 = 0;
                    } else {
                        j3 = A092.getLong(A092.getColumnIndexOrThrow("media_size"));
                    }
                    A092.close();
                    A002.close();
                    long A022 = ((C14950mJ) this.A0M.get()).A02();
                    if (A00 != null) {
                        r10.A00 = Double.valueOf((double) r11.A01(A00.longValue()));
                    }
                    r10.A02 = Double.valueOf((double) r11.A01(j2));
                    r10.A01 = Double.valueOf((double) r11.A01(j3));
                    j = r11.A01(A022);
                } finally {
                }
            } finally {
            }
        } else if (8 == i) {
            r10.A06 = Long.valueOf(((C21660xm) this.A0L.get()).A01(((C14950mJ) this.A0M.get()).A02()));
            r10.A07 = Long.valueOf(A00() / 1000);
            r10.A08 = Long.valueOf((long) this.A0E.A00);
            r2.A03();
            return r10;
        } else if (3 == i) {
            C21660xm r6 = (C21660xm) this.A0L.get();
            Long A003 = ((C20820wN) this.A0K.get()).A00();
            long A023 = ((C14950mJ) this.A0M.get()).A02();
            if (A003 != null) {
                r10.A00 = Double.valueOf((double) r6.A01(A003.longValue()));
            }
            j = r6.A01(A023);
        } else {
            r10.A07 = Long.valueOf(A00() / 1000);
            r10.A08 = Long.valueOf((long) this.A0E.A00);
            return r10;
        }
        r10.A06 = Long.valueOf(j);
        return r10;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 2, insn: 0x00e4: IF  (r2 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:55:0x010d, block:B:48:0x00e4
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public void A02() {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15730no.A02():void");
    }

    public void A03() {
        this.A0A.A02();
        A02();
        this.A07.A03(true);
        Log.i("ExportFlowManager/cancelExportFlowAndClearData/complete");
        ((SharedPreferences) this.A0B.A02.get()).edit().remove("/export/start_time").apply();
        this.A0I.A09();
        Log.i("ExportFlowManager/disableExportProviderAndClearMigrationFlags/complete/success");
    }

    public void A04() {
        boolean A07 = this.A05.A07(843);
        C235211z r0 = this.A09;
        PackageManager packageManager = r0.A01;
        ComponentName componentName = r0.A00;
        boolean z = true;
        if (packageManager.getComponentEnabledSetting(componentName) != 1) {
            z = false;
        }
        if (A07 != z) {
            int i = 0;
            if (A07) {
                i = 1;
            }
            packageManager.setComponentEnabledSetting(componentName, i, 1);
        }
    }

    public void A05() {
        Log.i("ExportFlowManager/reset()");
        ((SharedPreferences) this.A0B.A02.get()).edit().remove("/export/enc/active/owner").remove("/export/enc/active/version").remove("/export/enc/active/account_hash").remove("/export/enc/active/server_salt").remove("/export/enc/active/last_fetch_time").remove("/export/enc/active/seed").apply();
        this.A0H.A01();
        this.A0D.A01();
    }

    public final synchronized void A06() {
        this.A0J.A01();
    }

    public final void A07(int i) {
        if (8 != i || ((SharedPreferences) this.A0B.A02.get()).getString("/export/logging/funnelId", null) != null) {
            this.A06.A05(A01(i));
        }
    }

    public boolean A08() {
        return this.A0A.A05() || ((SharedPreferences) this.A0B.A02.get()).getLong("/export/start_time", 0) > 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000a, code lost:
        if (r1 == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A09() {
        /*
            r2 = this;
            monitor-enter(r2)
            android.os.CancellationSignal r0 = r2.A00     // Catch: all -> 0x000f
            if (r0 == 0) goto L_0x000c
            boolean r1 = r0.isCanceled()     // Catch: all -> 0x000f
            r0 = 1
            if (r1 != 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            monitor-exit(r2)
            return r0
        L_0x000f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15730no.A09():boolean");
    }
}
