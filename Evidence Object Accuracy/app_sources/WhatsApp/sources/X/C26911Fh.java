package X;

import com.whatsapp.jid.UserJid;
import java.util.HashMap;

/* renamed from: X.1Fh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26911Fh extends AbstractC17250qV {
    public final C18050rp A00;
    public final C22710zW A01;
    public final C17070qD A02;
    public final AnonymousClass103 A03;
    public final C30931Zj A04 = C30931Zj.A00("PaymentsMessageHandler", "infra", "COMMON");

    public C26911Fh(AbstractC15710nm r11, C18050rp r12, C17220qS r13, C17230qT r14, C22710zW r15, C17070qD r16, AnonymousClass103 r17, AbstractC14440lR r18) {
        super(r11, r13, r14, r18, new int[]{247}, true);
        this.A00 = r12;
        this.A02 = r16;
        this.A01 = r15;
        this.A03 = r17;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0269, code lost:
        if (r3 == false) goto L_0x026b;
     */
    @Override // X.AbstractC17250qV
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1V8 r15, int r16) {
        /*
        // Method dump skipped, instructions count: 685
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26911Fh.A00(X.1V8, int):void");
    }

    public final void A03(UserJid userJid, AnonymousClass1V8 r11, int i) {
        AnonymousClass1V8 A0E = r11.A0E("consumer_status");
        if (A0E != null) {
            String A0I = A0E.A0I("value", null);
            String A0I2 = A0E.A0I("dhash", null);
            if (userJid != null && this.A01.A06()) {
                this.A04.A06("onPaymentConsumerStatusUpdate");
                HashMap hashMap = new HashMap();
                hashMap.put(Integer.valueOf(i), new AnonymousClass01T(A0I, A0I2));
                C17070qD r0 = this.A02;
                r0.A03();
                C241414j r3 = r0.A09;
                synchronized (r3) {
                    r3.A0K(userJid, null, null, hashMap, null);
                }
            }
        }
    }
}
