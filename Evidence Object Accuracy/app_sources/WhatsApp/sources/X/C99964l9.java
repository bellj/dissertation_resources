package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99964l9 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29901Ve(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29901Ve[i];
    }
}
