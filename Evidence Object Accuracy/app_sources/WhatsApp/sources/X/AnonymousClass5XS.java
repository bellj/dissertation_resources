package X;

import android.view.Surface;

/* renamed from: X.5XS  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XS {
    void APX(int i, long j);

    void AUw(Surface surface);

    void AYE(String str, long j, long j2);

    void AYF(String str);

    void AYG(AnonymousClass4U3 v);

    void AYH(AnonymousClass4U3 v);

    void AYI(long j, int i);

    void AYJ(C100614mC v, AnonymousClass4XN v2);

    void AYK(float f, int i, int i2, int i3);
}
