package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.5lb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122525lb extends AbstractC118835cS {
    public ImageView A00;

    public C122525lb(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.payment_support_icon);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        View view = this.A0H;
        view.setOnClickListener(((C123055mX) r3).A00);
        AnonymousClass2GE.A07(this.A00, AnonymousClass00T.A00(view.getContext(), R.color.novi_payment_support_icon_color));
    }
}
