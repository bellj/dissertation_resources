package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.0tU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19050tU extends AbstractC18500sY implements AbstractC19010tQ {
    public AnonymousClass1YE A00;
    public final C15570nT A01;
    public final C16370ot A02;
    public final C18460sU A03;
    public final C20620w3 A04;
    public final C20580vz A05;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19050tU(C15570nT r3, C16370ot r4, C18460sU r5, C20620w3 r6, C20580vz r7, C18480sW r8) {
        super(r8, "receipt_user", Integer.MIN_VALUE);
        this.A03 = r5;
        this.A01 = r3;
        this.A04 = r6;
        this.A02 = r4;
        this.A05 = r7;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        AbstractC14640lm A01;
        C16310on A012 = super.A05.get();
        try {
            AnonymousClass1YE A0A = A012.A03.A0A("INSERT OR IGNORE INTO receipt_user(message_row_id,receipt_user_jid_row_id,receipt_timestamp,read_timestamp,played_timestamp) VALUES (?, ?, ?, ?, ?)");
            A012.close();
            this.A00 = A0A;
            int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("key_remote_jid");
            int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("key_from_me");
            int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("key_id");
            int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("remote_resource");
            int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("receipt_device_timestamp");
            int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("read_device_timestamp");
            int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("played_device_timestamp");
            long j = -1;
            int i = 0;
            while (cursor.moveToNext()) {
                j = cursor.getLong(columnIndexOrThrow);
                i++;
                if (cursor.getLong(columnIndexOrThrow3) == 1 && (A01 = AbstractC14640lm.A01(cursor.getString(columnIndexOrThrow2))) != null) {
                    if (C15380n4.A0J(A01) || C15380n4.A0N(A01)) {
                        for (Map.Entry entry : this.A05.A01(new AnonymousClass1IS(A01, cursor.getString(columnIndexOrThrow4), true)).A00.entrySet()) {
                            A0W((C39931qm) entry.getValue(), (UserJid) entry.getKey(), j);
                        }
                    } else if (A01 instanceof UserJid) {
                        UserJid of = UserJid.of(A01);
                        C39931qm r4 = new C39931qm(cursor.getLong(columnIndexOrThrow6), cursor.getLong(columnIndexOrThrow7), cursor.getLong(columnIndexOrThrow8));
                        A0W(r4, of, j);
                        AbstractC14640lm A013 = AbstractC14640lm.A01(cursor.getString(columnIndexOrThrow5));
                        if (C15380n4.A0F(A013)) {
                            try {
                                AbstractC15340mz A03 = this.A02.A03(new AnonymousClass1IS(A013, cursor.getString(columnIndexOrThrow4), true));
                                if (A03 != null) {
                                    A0W(r4, of, A03.A11);
                                }
                            } catch (SQLiteException e) {
                                StringBuilder sb = new StringBuilder("receipt-user-db-migration/process-batch fail to read from message store, e=");
                                sb.append(e.getMessage());
                                Log.e(sb.toString());
                                if (A07() >= 20) {
                                    return new AnonymousClass2Ez(-1, 0);
                                }
                                throw e;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
            }
            return new AnonymousClass2Ez(j, i);
        } catch (Throwable th) {
            try {
                A012.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("receipt_user_ready", 2);
    }

    @Override // X.AbstractC18500sY
    public boolean A0V(C29001Pw r5) {
        C16310on A01 = super.A05.get();
        try {
            if (TextUtils.isEmpty(AnonymousClass1Uj.A00(A01.A03, "table", "messages"))) {
                A0H();
                A01.close();
                return true;
            }
            A01.close();
            return super.A0V(r5);
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0W(C39931qm r6, UserJid userJid, long j) {
        long A01 = this.A03.A01(userJid);
        if (A01 == -1) {
            super.A01.AaV("ReceiptUserStoreDatabaseMigration: invalid jid", userJid.toString(), false);
            return;
        }
        this.A00.A01(1, j);
        this.A00.A01(2, A01);
        this.A00.A01(3, r6.A00);
        this.A00.A01(4, r6.A02);
        this.A00.A01(5, r6.A01);
        this.A00.A00.executeInsert();
        this.A00.A00.clearBindings();
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = super.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("receipt_user", null, null);
            C21390xL r1 = this.A06;
            r1.A03("receipt_user_ready");
            r1.A03("migration_receipt_index");
            r1.A03("migration_receipt_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("ReceiptUserStore/ReceiptUserStoreDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
