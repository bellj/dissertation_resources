package X;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.0DV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DV extends AnonymousClass04v {
    public final AnonymousClass04v A00 = new AnonymousClass0DS(this);
    public final RecyclerView A01;

    public AnonymousClass0DV(RecyclerView recyclerView) {
        this.A01 = recyclerView;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        AnonymousClass02H layoutManager;
        super.A01(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !A08() && (layoutManager = ((RecyclerView) view).getLayoutManager()) != null) {
            layoutManager.A0s(accessibilityEvent);
        }
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        AnonymousClass02H layoutManager;
        RecyclerView recyclerView;
        int i2;
        int i3;
        if (super.A03(view, i, bundle)) {
            return true;
        }
        if (A08() || (layoutManager = this.A01.getLayoutManager()) == null || (recyclerView = layoutManager.A07) == null) {
            return false;
        }
        if (i == 4096) {
            if (recyclerView.canScrollVertically(1)) {
                i2 = (layoutManager.A00 - layoutManager.A0B()) - layoutManager.A08();
            } else {
                i2 = 0;
            }
            if (layoutManager.A07.canScrollHorizontally(1)) {
                i3 = (layoutManager.A03 - layoutManager.A09()) - layoutManager.A0A();
            }
            i3 = 0;
        } else if (i != 8192) {
            return false;
        } else {
            if (recyclerView.canScrollVertically(-1)) {
                i2 = -((layoutManager.A00 - layoutManager.A0B()) - layoutManager.A08());
            } else {
                i2 = 0;
            }
            if (layoutManager.A07.canScrollHorizontally(-1)) {
                i3 = -((layoutManager.A03 - layoutManager.A09()) - layoutManager.A0A());
            }
            i3 = 0;
        }
        if (i2 == 0 && i3 == 0) {
            return false;
        }
        layoutManager.A07.A0d(i3, i2);
        return true;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r9) {
        AnonymousClass02H layoutManager;
        super.A06(view, r9);
        String name = RecyclerView.class.getName();
        AccessibilityNodeInfo accessibilityNodeInfo = r9.A02;
        accessibilityNodeInfo.setClassName(name);
        if (!A08() && (layoutManager = this.A01.getLayoutManager()) != null) {
            RecyclerView recyclerView = layoutManager.A07;
            AnonymousClass0QS r4 = recyclerView.A0w;
            C05480Ps r3 = recyclerView.A0y;
            if (recyclerView.canScrollVertically(-1) || layoutManager.A07.canScrollHorizontally(-1)) {
                accessibilityNodeInfo.addAction(DefaultCrypto.BUFFER_SIZE);
                accessibilityNodeInfo.setScrollable(true);
            }
            if (layoutManager.A07.canScrollVertically(1) || layoutManager.A07.canScrollHorizontally(1)) {
                accessibilityNodeInfo.addAction(4096);
                accessibilityNodeInfo.setScrollable(true);
            }
            r9.A0I(AnonymousClass0TY.A01(layoutManager.A0X(r4, r3), layoutManager.A0W(r4, r3), 0, false));
        }
    }

    public AnonymousClass04v A07() {
        return this.A00;
    }

    public boolean A08() {
        RecyclerView recyclerView = this.A01;
        if (!recyclerView.A0g || recyclerView.A0e || recyclerView.A0J.A04.size() > 0) {
            return true;
        }
        return false;
    }
}
