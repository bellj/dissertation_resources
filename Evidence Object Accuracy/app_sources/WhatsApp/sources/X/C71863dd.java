package X;

import com.whatsapp.biz.catalog.view.activity.CatalogCategoryTabsActivity;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel;

/* renamed from: X.3dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71863dd extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogCategoryTabsActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71863dd(CatalogCategoryTabsActivity catalogCategoryTabsActivity) {
        super(0);
        this.this$0 = catalogCategoryTabsActivity;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return C13000ix.A02(this.this$0).A00(CatalogCategoryTabsViewModel.class);
    }
}
