package X;

/* renamed from: X.1nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38261nn extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public String A07;
    public String A08;

    public C38261nn() {
        super(3468, new AnonymousClass00E(1, 1, 1), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A05);
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(11, this.A06);
        r3.Abe(5, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(7, this.A04);
        r3.Abe(8, this.A07);
        r3.Abe(9, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        StringBuilder sb = new StringBuilder("WamOtpRetriever {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "businessPhoneNumber", this.A05);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ctaFallbackReason", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ctaType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageReceivedElapsedTimeSeconds", this.A06);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpEventSource", obj3);
        Integer num4 = this.A03;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpEventType", obj4);
        Integer num5 = this.A04;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpProductType", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpSessionId", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "thirdPartyPackageNameFromIntent", this.A08);
        sb.append("}");
        return sb.toString();
    }
}
