package X;

/* renamed from: X.4Ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92264Ve {
    public final int A00;
    public final int A01;
    public final byte[] A02;

    public C92264Ve(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        if (bArr != null) {
            bArr2 = (byte[]) bArr.clone();
        } else {
            bArr2 = null;
        }
        this.A02 = bArr2;
        this.A01 = i;
        this.A00 = i2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WtWriteParams{offset=");
        A0k.append(this.A01);
        A0k.append(", len=");
        A0k.append(this.A00);
        return C12970iu.A0v(A0k);
    }
}
