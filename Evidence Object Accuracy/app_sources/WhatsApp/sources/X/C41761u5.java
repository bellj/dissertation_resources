package X;

/* renamed from: X.1u5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41761u5 extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Double A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public String A09;

    public C41761u5() {
        super(2872, new AnonymousClass00E(1, 1, 1), 1, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A06);
        r3.Abe(7, this.A00);
        r3.Abe(8, this.A01);
        r3.Abe(10, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(1, this.A05);
        r3.Abe(11, this.A07);
        r3.Abe(12, this.A08);
        r3.Abe(6, this.A02);
        r3.Abe(2, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamXplatformMigrationExport {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "storageAvailSize", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "waDbSize", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "waMediaSize", this.A01);
        Integer num = this.A03;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmDonorPlatform", obj);
        Integer num2 = this.A04;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmError", obj2);
        Integer num3 = this.A05;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmEvent", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmExportDuration", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmExportProgress", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmExportedDbSize", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "xpmFunnelId", this.A09);
        sb.append("}");
        return sb.toString();
    }
}
