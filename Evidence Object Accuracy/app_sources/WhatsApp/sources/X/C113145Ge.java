package X;

import java.io.OutputStream;

/* renamed from: X.5Ge  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113145Ge implements AnonymousClass5VT {
    public byte A00 = 61;
    public final byte[] A01;
    public final byte[] A02;

    public C113145Ge() {
        byte[] bArr = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        this.A02 = bArr;
        byte[] bArr2 = new byte[128];
        this.A01 = bArr2;
        int i = 0;
        int i2 = 0;
        do {
            bArr2[i2] = -1;
            i2++;
        } while (i2 < 128);
        do {
            bArr2[bArr[i]] = (byte) i;
            i++;
        } while (i < 64);
    }

    public final int A00(String str, int i, int i2) {
        while (i < i2) {
            char charAt = str.charAt(i);
            if (charAt != '\n' && charAt != '\r' && charAt != '\t' && charAt != ' ') {
                break;
            }
            i++;
        }
        return i;
    }

    @Override // X.AnonymousClass5VT
    public int A9L(OutputStream outputStream, byte[] bArr, int i, int i2) {
        int i3;
        byte b;
        int i4 = i2;
        int i5 = 0;
        byte[] bArr2 = new byte[72];
        while (i4 > 0) {
            int min = Math.min(54, i4);
            int i6 = (i5 + min) - 2;
            int i7 = i5;
            int i8 = 0;
            while (i7 < i6) {
                int i9 = i7 + 1;
                byte b2 = bArr[i7];
                int i10 = i9 + 1;
                int i11 = bArr[i9] & 255;
                i7 = i10 + 1;
                int i12 = bArr[i10] & 255;
                int i13 = i8 + 1;
                byte[] bArr3 = this.A02;
                C72463ee.A0X(bArr3, bArr2, (b2 >>> 2) & 63, i8);
                int i14 = i13 + 1;
                C72463ee.A0X(bArr3, bArr2, ((b2 << 4) | (i11 >>> 4)) & 63, i13);
                int i15 = i14 + 1;
                C72463ee.A0X(bArr3, bArr2, ((i11 << 2) | (i12 >>> 6)) & 63, i14);
                i8 = i15 + 1;
                C72463ee.A0X(bArr3, bArr2, i12 & 63, i15);
            }
            int i16 = min - (i7 - i5);
            if (i16 == 1) {
                int i17 = bArr[i7] & 255;
                int i18 = i8 + 1;
                byte[] bArr4 = this.A02;
                C72463ee.A0X(bArr4, bArr2, (i17 >>> 2) & 63, i8);
                int i19 = i18 + 1;
                C72463ee.A0X(bArr4, bArr2, (i17 << 4) & 63, i18);
                i3 = i19 + 1;
                b = this.A00;
                bArr2[i19] = b;
                i8 = i3 + 1;
            } else if (i16 == 2) {
                int i20 = bArr[i7] & 255;
                int i21 = bArr[i7 + 1] & 255;
                int i22 = i8 + 1;
                byte[] bArr5 = this.A02;
                C72463ee.A0X(bArr5, bArr2, (i20 >>> 2) & 63, i8);
                int i23 = i22 + 1;
                C72463ee.A0X(bArr5, bArr2, ((i20 << 4) | (i21 >>> 4)) & 63, i22);
                i3 = i23 + 1;
                C72463ee.A0X(bArr5, bArr2, (i21 << 2) & 63, i23);
                i8 = i3 + 1;
                b = this.A00;
            } else {
                outputStream.write(bArr2, 0, i8 - 0);
                i5 += min;
                i4 -= min;
            }
            bArr2[i3] = b;
            outputStream.write(bArr2, 0, i8 - 0);
            i5 += min;
            i4 -= min;
        }
        return ((i4 + 2) / 3) << 2;
    }
}
