package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3qk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79533qk extends C98394ic implements IInterface {
    public C79533qk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }
}
