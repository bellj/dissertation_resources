package X;

import java.util.AbstractSet;
import java.util.Collection;

/* renamed from: X.5I5  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5I5<E> extends AbstractSet<E> {
    @Override // java.util.AbstractSet, java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean removeAll(Collection collection) {
        return C28281Ml.removeAllImpl(this, collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }
}
