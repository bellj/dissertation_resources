package X;

/* renamed from: X.3BE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BE {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3BE(String str, String str2) {
        C41141sy r1 = new C41141sy("fds");
        if (str != null && AnonymousClass3JT.A0E(str, 0, 9007199254740991L, true)) {
            C41141sy.A01(r1, "state", str);
        }
        if (str2 != null && AnonymousClass3JT.A0E(str2, 0, 9007199254740991L, true)) {
            C41141sy.A01(r1, "params", str2);
        }
        this.A00 = r1.A03();
    }
}
