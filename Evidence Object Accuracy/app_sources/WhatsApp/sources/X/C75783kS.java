package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.authentication.FingerprintView;

/* renamed from: X.3kS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75783kS extends AnonymousClass03J {
    public final /* synthetic */ FingerprintView A00;

    public C75783kS(FingerprintView fingerprintView) {
        this.A00 = fingerprintView;
    }

    @Override // X.AnonymousClass03J
    public void A01(Drawable drawable) {
        AnonymousClass4UT r0 = this.A00.A00;
        if (r0 != null) {
            r0.A00();
        }
    }
}
