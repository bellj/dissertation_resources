package X;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

/* renamed from: X.4WV  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4WV {
    public final ThreadLocal A00 = new AnonymousClass5HM(this);

    public Object A00() {
        Locale locale;
        String str;
        if (this instanceof AnonymousClass47q) {
            locale = Locale.US;
            str = "yyyy-MM-dd";
        } else if (this instanceof AnonymousClass47p) {
            locale = Locale.US;
            str = "MMM dd, yyyy";
        } else if ((this instanceof C865047o) || (this instanceof C864947n)) {
            return new GregorianCalendar();
        } else {
            locale = Locale.US;
            str = "yyyy-MM-dd HH:mm:ss.SSSZ";
        }
        return new SimpleDateFormat(str, locale);
    }

    public synchronized Object A01() {
        Object obj;
        ThreadLocal threadLocal = this.A00;
        obj = ((WeakReference) threadLocal.get()).get();
        if (obj == null) {
            obj = A00();
            threadLocal.set(C12970iu.A10(obj));
        }
        return obj;
    }
}
