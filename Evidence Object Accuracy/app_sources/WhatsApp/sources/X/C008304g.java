package X;

/* renamed from: X.04g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008304g extends AnonymousClass032 {
    public long mobileBytesRx;
    public long mobileBytesTx;
    public long wifiBytesRx;
    public long wifiBytesTx;

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A01(AnonymousClass032 r3) {
        C008304g r32 = (C008304g) r3;
        this.mobileBytesRx = r32.mobileBytesRx;
        this.mobileBytesTx = r32.mobileBytesTx;
        this.wifiBytesRx = r32.wifiBytesRx;
        this.wifiBytesTx = r32.wifiBytesTx;
        return this;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A02(AnonymousClass032 r5, AnonymousClass032 r6) {
        C008304g r52 = (C008304g) r5;
        C008304g r62 = (C008304g) r6;
        if (r62 == null) {
            r62 = new C008304g();
        }
        if (r52 == null) {
            r62.mobileBytesRx = this.mobileBytesRx;
            r62.mobileBytesTx = this.mobileBytesTx;
            r62.wifiBytesRx = this.wifiBytesRx;
            r62.wifiBytesTx = this.wifiBytesTx;
            return r62;
        }
        r62.mobileBytesTx = this.mobileBytesTx - r52.mobileBytesTx;
        r62.mobileBytesRx = this.mobileBytesRx - r52.mobileBytesRx;
        r62.wifiBytesTx = this.wifiBytesTx - r52.wifiBytesTx;
        r62.wifiBytesRx = this.wifiBytesRx - r52.wifiBytesRx;
        return r62;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C008304g r7 = (C008304g) obj;
            if (!(this.mobileBytesTx == r7.mobileBytesTx && this.mobileBytesRx == r7.mobileBytesRx && this.wifiBytesTx == r7.wifiBytesTx && this.wifiBytesRx == r7.wifiBytesRx)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.mobileBytesTx;
        long j2 = this.mobileBytesRx;
        long j3 = this.wifiBytesTx;
        long j4 = this.wifiBytesRx;
        return (((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("NetworkMetrics{mobileBytesTx=");
        sb.append(this.mobileBytesTx);
        sb.append(", mobileBytesRx=");
        sb.append(this.mobileBytesRx);
        sb.append(", wifiBytesTx=");
        sb.append(this.wifiBytesTx);
        sb.append(", wifiBytesRx=");
        sb.append(this.wifiBytesRx);
        sb.append('}');
        return sb.toString();
    }
}
