package X;

import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.4Ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC91994Ua {
    public Path A00(RectF rectF) {
        if (this instanceof AnonymousClass48B) {
            return C37501mV.A03(rectF);
        }
        Path path = new Path();
        float width = rectF.width() / ((float) 2);
        path.addCircle(rectF.left + width, rectF.top + width, width, Path.Direction.CW);
        return path;
    }
}
