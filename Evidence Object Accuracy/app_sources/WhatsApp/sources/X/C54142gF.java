package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.2gF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54142gF extends AnonymousClass02L {
    public final C48952Io A00;
    public final C89164Ix A01;
    public final C89174Iy A02;
    public final C89184Iz A03;
    public final AnonymousClass4J1 A04;
    public final AnonymousClass4J2 A05;
    public final AnonymousClass4J3 A06;
    public final AnonymousClass4J5 A07;
    public final AnonymousClass4J8 A08;
    public final AnonymousClass4J9 A09;
    public final AnonymousClass4JA A0A;

    public C54142gF(C48952Io r3, C89164Ix r4, C89174Iy r5, C89184Iz r6, AnonymousClass4J1 r7, AnonymousClass4J2 r8, AnonymousClass4J3 r9, AnonymousClass4J5 r10, AnonymousClass4J8 r11, AnonymousClass4J9 r12, AnonymousClass4JA r13) {
        super(new AnonymousClass0SC(new C74593iM()).A00());
        this.A02 = r5;
        this.A01 = r4;
        this.A04 = r7;
        this.A00 = r3;
        this.A03 = r6;
        this.A09 = r12;
        this.A05 = r8;
        this.A06 = r9;
        this.A07 = r10;
        this.A08 = r11;
        this.A0A = r13;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r1) {
        ((AbstractC37191le) r1).A08();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC37191le r22 = (AbstractC37191le) r2;
        r22.A08();
        r22.A09(A0E(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (AnonymousClass39o.values()[i].ordinal()) {
            case 0:
                return new C59702vF(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.loading_row));
            case 1:
                return new C59742vJ(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.list_shimmer_layout), viewGroup);
            case 2:
                return new C59732vI(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.list_shimmer_layout), viewGroup);
            case 3:
                return new C59822vR(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_location_view_holder));
            case 4:
                return new AnonymousClass40O(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_location_divider));
            case 5:
                AnonymousClass4J2 r2 = this.A05;
                View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.root_category_row);
                C71573d9 r22 = r2.A00;
                return new C59902vZ(r22.A01.A1B, A0F, C12970iu.A0V(r22.A04));
            case 6:
                return new AnonymousClass40Z(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.sub_category_row));
            case 7:
                C48952Io r23 = this.A00;
                View A0F2 = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.security_footer_row);
                AnonymousClass01J r24 = r23.A00.A03;
                return new C59882vX(A0F2, (AnonymousClass1B3) r24.AI0.get(), (AnonymousClass1B8) r24.A2C.get());
            case 8:
                C53162d0 r3 = new C53162d0(viewGroup.getContext());
                r3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                r3.setFAQLink("how-to-search-for-businesses-inside-whatsapp");
                return new C59962vf(r3);
            case 9:
                return new C59912va(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_filter_bar), new C54492go((AnonymousClass4J0) this.A03.A00.A03.A05.get()));
            case 10:
                C89174Iy r25 = this.A02;
                View A0F3 = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.business_profile_row);
                C71573d9 r26 = r25.A00;
                AnonymousClass01J r1 = r26.A04;
                AbstractC14440lR A0T = C12960it.A0T(r1);
                C15570nT A0S = C12970iu.A0S(r1);
                AnonymousClass018 A0R = C12960it.A0R(r1);
                C251118d A0V = C12970iu.A0V(r1);
                AnonymousClass2FL r0 = r26.A01;
                C48822Hx A03 = r0.A03();
                return new C59952ve(A0F3, A0S, A0V, AnonymousClass2FL.A00(r0), A03, (AnonymousClass1CN) r1.A5v.get(), (AnonymousClass131) r1.A49.get(), A0R, A0T);
            case 11:
                return new AnonymousClass40M(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.business_profile_divider));
            case 12:
                return new AnonymousClass40R(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.load_business_profile_row));
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new C59722vH(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.load_all_business_row));
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new AnonymousClass40U(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.no_more_results_row));
            case 15:
                return new AnonymousClass40V(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.refine_results_row));
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass40S(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.load_sub_category_row));
            case 17:
                return new C59892vY(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.directory_error_row), C12970iu.A0V(this.A01.A00.A04));
            case 18:
                return new AnonymousClass40W(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.retry_row));
            case 19:
                return new AnonymousClass40N(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.location_fallback_row));
            case C43951xu.A01:
                return new AnonymousClass40Q(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.finding_location_row));
            case 21:
                return new AnonymousClass40P(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_location_error_row));
            case 22:
            case 23:
            case 24:
            case 25:
            case 28:
            case 29:
            case 32:
            case 33:
            case 35:
            case 36:
            case 41:
            case 42:
            default:
                Log.e(C12960it.A0W(i, "DirectorySearchFragmentListAdapter/onCreateViewHolder type not handled: "));
                throw C12960it.A0U(C12960it.A0f(C12960it.A0j("DirectorySearchFragmentListAdapter/onCreateViewHolder type not handled: "), i));
            case 26:
                return new C59752vK(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.location_fallback_row));
            case 27:
                return new AnonymousClass40X(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.set_different_location_row));
            case C25991Bp.A0S:
                return new C59712vG(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.category_search_row));
            case 31:
                return new C59832vS(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.directory_search_query_error_row));
            case 34:
                return new C59872vW(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.item_second_level_category), (AnonymousClass1CN) this.A08.A00.A04.A5v.get());
            case 37:
                return new C59922vb(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_history_bar), new C54322gX((AnonymousClass4J4) this.A06.A00.A03.A09.get()));
            case 38:
                AnonymousClass4J5 r27 = this.A07;
                View A0F4 = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.popular_categories_view);
                C71573d9 r02 = r27.A00;
                AnonymousClass01J r32 = r02.A04;
                AnonymousClass018 A0R2 = C12960it.A0R(r32);
                return new C59942vd(A0F4, C12970iu.A0V(r32), new C54312gW((AnonymousClass4J7) r02.A03.A0B.get()), A0R2);
            case 39:
                return new C59932vc(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.popular_categories_shimmer_view), C12970iu.A0V(this.A09.A00.A04));
            case 40:
                return new C59972vg(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.item_category_breadcrumbs));
            case 43:
                C16700pc.A0E(viewGroup, 0);
                return new C59802vP(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.item_nearby_business_location_header));
            case 44:
                C16700pc.A0E(viewGroup, 0);
                return new C59762vL(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.list_shimmer_layout));
            case 45:
                C16700pc.A0E(viewGroup, 0);
                return new C59792vO(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.item_nearby_business_empty));
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                AnonymousClass4J1 r28 = this.A04;
                C16700pc.A0E(viewGroup, 0);
                return new C59852vU(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.item_neary_by_business_list), r28.A00.A03.A04());
            case 47:
                C16700pc.A0E(viewGroup, 0);
                return new C59862vV(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.item_nearby_business_location_request));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((C37171lc) A0E(i)).A00.ordinal();
    }
}
