package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import com.whatsapp.R;
import org.json.JSONObject;

/* renamed from: X.21u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC454821u {
    public static float A03 = 12.0f;
    public static float A04 = 24.0f;
    public static float A05 = 24.0f;
    public static float A06 = 32.0f;
    public static float A07 = 96.0f;
    public static float A08 = 96.0f;
    public float A00;
    public final Paint A01;
    public final RectF A02 = new RectF();

    public void A05() {
    }

    public boolean A0B() {
        return false;
    }

    public boolean A0C() {
        return false;
    }

    public boolean A0D() {
        return false;
    }

    public boolean A0E() {
        return false;
    }

    public Drawable A0F() {
        return null;
    }

    public boolean A0L() {
        return false;
    }

    public AbstractC454821u() {
        Paint paint = new Paint(1);
        this.A01 = paint;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }

    public static void A00(AbstractC454821u r6, float f) {
        RectF rectF = r6.A02;
        float centerX = rectF.centerX();
        float centerY = rectF.centerY();
        rectF.set(centerX - ((centerX - rectF.left) * f), centerY - ((centerY - rectF.top) * f), centerX - ((centerX - rectF.right) * f), centerY - (f * (centerY - rectF.bottom)));
        r6.A04();
    }

    public static void A01(AbstractC454821u r5, float f, float f2, float f3, float f4) {
        float f5 = f - f2;
        float f6 = f3 - f4;
        float min = Math.min(f5, f6);
        RectF rectF = r5.A02;
        float f7 = (f5 - min) / 2.0f;
        float f8 = (f6 - min) / 2.0f;
        rectF.set(f2 + f7, f4 + f8, f - f7, f3 - f8);
        rectF.sort();
    }

    public float A02() {
        float f;
        float f2;
        if (this instanceof AnonymousClass33K) {
            f = this.A01.getStrokeWidth() * 5.0f;
            f2 = 3.0f;
        } else if (!(this instanceof AnonymousClass33I) && !(this instanceof AnonymousClass33H)) {
            return this.A01.getStrokeWidth();
        } else {
            f = this.A01.getStrokeWidth() * 3.0f;
            f2 = 2.0f;
        }
        return f / f2;
    }

    public C91484Rx A03() {
        if (!(this instanceof AnonymousClass33L)) {
            return new C91484Rx(this.A02, this.A00, A02(), this.A01.getColor());
        }
        AnonymousClass33L r1 = (AnonymousClass33L) this;
        RectF rectF = ((AbstractC454821u) r1).A02;
        float f = ((AbstractC454821u) r1).A00;
        int color = ((AbstractC454821u) r1).A01.getColor();
        return new AnonymousClass45I(rectF, r1.A05, f, r1.A02(), color, r1.A03);
    }

    public void A04() {
        RectF rectF = this.A02;
        if (rectF.width() < A03) {
            rectF.set(rectF.centerX() - (A03 / 2.0f), rectF.top, rectF.centerX() + (A03 / 2.0f), rectF.bottom);
        }
        if (rectF.height() < A03) {
            rectF.set(rectF.left, rectF.centerY() - (A03 / 2.0f), rectF.right, rectF.centerY() + (A03 / 2.0f));
        }
    }

    public void A06(float f) {
        RectF rectF = this.A02;
        float centerX = rectF.centerX();
        float centerY = rectF.centerY();
        rectF.set(centerX - ((centerX - rectF.left) * f), centerY - ((centerY - rectF.top) * f), centerX - ((centerX - rectF.right) * f), centerY - (f * (centerY - rectF.bottom)));
        A04();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        if (r9 == 2) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(float r8, int r9) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.AnonymousClass33I
            if (r0 != 0) goto L_0x0010
            boolean r0 = r7 instanceof X.AnonymousClass33G
            if (r0 != 0) goto L_0x0010
            boolean r0 = r7 instanceof X.AnonymousClass33F
            if (r0 != 0) goto L_0x0010
            r7.A06(r8)
            return
        L_0x0010:
            android.graphics.RectF r5 = r7.A02
            float r4 = r5.centerX()
            float r3 = r5.centerY()
            r1 = 2
            if (r9 == 0) goto L_0x0021
            r6 = 1065353216(0x3f800000, float:1.0)
            if (r9 != r1) goto L_0x0022
        L_0x0021:
            r6 = r8
        L_0x0022:
            r0 = 1
            if (r9 == r0) goto L_0x0029
            if (r9 == r1) goto L_0x0029
            r8 = 1065353216(0x3f800000, float:1.0)
        L_0x0029:
            float r0 = r5.left
            float r0 = r4 - r0
            float r0 = r0 * r6
            float r2 = r4 - r0
            float r0 = r5.top
            float r0 = r3 - r0
            float r0 = r0 * r8
            float r1 = r3 - r0
            float r0 = r5.right
            float r0 = r4 - r0
            float r6 = r6 * r0
            float r4 = r4 - r6
            float r0 = r5.bottom
            float r0 = r3 - r0
            float r8 = r8 * r0
            float r3 = r3 - r8
            r5.set(r2, r1, r4, r3)
            r7.A04()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC454821u.A07(float, int):void");
    }

    public void A08(float f, int i) {
        A07(f, 2);
    }

    public void A09(int i) {
        this.A01.setColor(i);
    }

    public void A0A(JSONObject jSONObject) {
        RectF rectF = this.A02;
        rectF.left = ((float) jSONObject.getInt("l")) / 100.0f;
        rectF.top = ((float) jSONObject.getInt("t")) / 100.0f;
        rectF.right = ((float) jSONObject.getInt("r")) / 100.0f;
        rectF.bottom = ((float) jSONObject.getInt("b")) / 100.0f;
        this.A00 = ((float) jSONObject.optInt("rotate", 0)) / 100.0f;
        A09(jSONObject.getInt("color"));
        A0O(((float) jSONObject.getInt("stroke")) / 100.0f);
    }

    public String A0G() {
        if (this instanceof AnonymousClass33K) {
            return "thinking-bubble";
        }
        if (this instanceof AnonymousClass33L) {
            return "text";
        }
        if (this instanceof AnonymousClass33I) {
            return "speech-bubble-rect";
        }
        if (this instanceof AnonymousClass33H) {
            return "speech-bubble-oval";
        }
        if (this instanceof AnonymousClass33G) {
            return "rect";
        }
        if (!(this instanceof AnonymousClass33J)) {
            return !(this instanceof AnonymousClass33F) ? "arrow" : "oval";
        }
        return "pen";
    }

    public String A0H(Context context) {
        int i;
        if (this instanceof AnonymousClass33K) {
            i = R.string.doodle_item_thought_bubble;
        } else if (this instanceof AnonymousClass33L) {
            return ((AnonymousClass33L) this).A05;
        } else {
            if (this instanceof AnonymousClass33I) {
                i = R.string.doodle_item_rectangular_bubble;
            } else if (this instanceof AnonymousClass33H) {
                i = R.string.doodle_item_oval_bubble;
            } else if (this instanceof AnonymousClass33G) {
                i = R.string.doodle_item_rectangle;
            } else if (this instanceof AnonymousClass33J) {
                return "";
            } else {
                if (!(this instanceof AnonymousClass33F)) {
                    i = R.string.doodle_item_arrow;
                } else {
                    i = R.string.doodle_item_oval;
                }
            }
        }
        return context.getString(i);
    }

    public void A0I(Canvas canvas) {
        if (!(this instanceof AnonymousClass33K) && !(this instanceof AnonymousClass33L) && !(this instanceof AnonymousClass33I) && !(this instanceof AnonymousClass33H)) {
            boolean z = this instanceof AnonymousClass33G;
        }
        A0P(canvas);
    }

    public boolean A0J() {
        return !(this instanceof AnonymousClass33L) && !(this instanceof AnonymousClass33J);
    }

    public boolean A0K() {
        return !(this instanceof AnonymousClass33L) && !(this instanceof AnonymousClass33J);
    }

    public void A0M(C91484Rx r3) {
        this.A02.set(r3.A03);
        this.A00 = r3.A00;
        A09(r3.A02);
        A0O(r3.A01);
    }

    public void A0N(JSONObject jSONObject) {
        jSONObject.put("type", A0G());
        RectF rectF = this.A02;
        jSONObject.put("l", (int) (rectF.left * 100.0f));
        jSONObject.put("t", (int) (rectF.top * 100.0f));
        jSONObject.put("r", (int) (rectF.right * 100.0f));
        jSONObject.put("b", (int) (rectF.bottom * 100.0f));
        float f = this.A00;
        if (f != 0.0f) {
            jSONObject.put("rotate", (int) (f * 100.0f));
        }
        jSONObject.put("color", this.A01.getColor());
        jSONObject.put("stroke", (int) (A02() * 100.0f));
    }

    public void A0O(float f) {
        this.A01.setStrokeWidth(f);
    }

    public void A0P(Canvas canvas) {
        int color;
        float degrees;
        if (!(this instanceof AnonymousClass33K)) {
            if (this instanceof AnonymousClass33L) {
                AnonymousClass33L r2 = (AnonymousClass33L) this;
                if (!TextUtils.isEmpty(r2.A05)) {
                    RectF rectF = ((AbstractC454821u) r2).A02;
                    rectF.sort();
                    canvas.save();
                    float f = ((AbstractC454821u) r2).A00;
                    if (Math.abs(f) < 3.0f) {
                        f = 0.0f;
                    }
                    canvas.rotate(f, rectF.centerX(), rectF.centerY());
                    float width = rectF.width() / r2.A00;
                    canvas.translate((rectF.left + (rectF.width() / 2.0f)) - ((((float) r2.A04.getWidth()) * width) / 2.0f), rectF.top);
                    canvas.scale(width, width, 0.0f, 0.0f);
                    int i = r2.A03;
                    TextPaint textPaint = r2.A08;
                    if (i == 3) {
                        textPaint.setStrokeWidth(textPaint.getTextSize() / 12.0f);
                        textPaint.setStyle(Paint.Style.STROKE);
                        textPaint.setColor(-16777216);
                        r2.A04.draw(canvas);
                        textPaint.setStrokeWidth(0.0f);
                        textPaint.setStyle(Paint.Style.FILL);
                        color = -1;
                    } else {
                        color = ((AbstractC454821u) r2).A01.getColor();
                    }
                    textPaint.setColor(color);
                    r2.A04.draw(canvas);
                } else {
                    return;
                }
            } else if (this instanceof AnonymousClass33H) {
                AnonymousClass33H r22 = (AnonymousClass33H) this;
                RectF rectF2 = ((AbstractC454821u) r22).A02;
                rectF2.sort();
                Paint paint = ((AbstractC454821u) r22).A01;
                paint.setStyle(Paint.Style.STROKE);
                Matrix matrix = r22.A00;
                matrix.reset();
                matrix.setRotate(((AbstractC454821u) r22).A00, 0.0f, 0.0f);
                matrix.postScale(rectF2.width() / 2000.0f, rectF2.height() / 2000.0f);
                matrix.postTranslate(rectF2.centerX(), rectF2.centerY());
                Path path = r22.A03;
                path.reset();
                path.setFillType(Path.FillType.WINDING);
                Path path2 = r22.A02;
                path2.transform(matrix, path);
                Paint paint2 = r22.A01;
                paint2.setStyle(Paint.Style.FILL);
                paint2.setColor(-31);
                path.close();
                canvas.drawPath(path, paint2);
                path.reset();
                path.setFillType(Path.FillType.WINDING);
                path2.transform(matrix, path);
                canvas.drawPath(path, paint);
                return;
            } else if (this instanceof AnonymousClass33G) {
                RectF rectF3 = this.A02;
                rectF3.sort();
                canvas.save();
                canvas.rotate(this.A00, rectF3.centerX(), rectF3.centerY());
                canvas.drawRect(rectF3, this.A01);
            } else if (this instanceof AnonymousClass33J) {
                AnonymousClass33J r23 = (AnonymousClass33J) this;
                if (r23.A05) {
                    r23.A03.A02(canvas);
                    return;
                }
                return;
            } else if (!(this instanceof AnonymousClass33F)) {
                canvas.save();
                float f2 = this.A00;
                RectF rectF4 = this.A02;
                canvas.rotate(f2, rectF4.centerX(), rectF4.centerY());
                float f3 = rectF4.left;
                float f4 = rectF4.bottom;
                float f5 = rectF4.right;
                float f6 = rectF4.top;
                Paint paint3 = this.A01;
                canvas.drawLine(f3, f4, f5, f6, paint3);
                if (f5 == f3) {
                    degrees = 90.0f;
                } else {
                    degrees = (float) Math.toDegrees(Math.atan((double) ((f6 - f4) / (f5 - f3))));
                }
                canvas.save();
                canvas.translate(f5, f6);
                int i2 = 150;
                if (f3 > f5) {
                    i2 = 30;
                }
                canvas.rotate(degrees + ((float) i2));
                canvas.drawLine(0.0f, 0.0f, paint3.getStrokeWidth() * 5.0f, 0.0f, paint3);
                int i3 = 60;
                if (f3 > f5) {
                    i3 = -60;
                }
                canvas.rotate((float) i3);
                canvas.drawLine(0.0f, 0.0f, paint3.getStrokeWidth() * 5.0f, 0.0f, paint3);
                canvas.restore();
            } else {
                RectF rectF5 = this.A02;
                rectF5.sort();
                canvas.save();
                canvas.rotate(this.A00, rectF5.centerX(), rectF5.centerY());
                canvas.drawOval(rectF5, this.A01);
            }
            canvas.restore();
            return;
        }
        AnonymousClass33K r24 = (AnonymousClass33K) this;
        RectF rectF6 = ((AbstractC454821u) r24).A02;
        rectF6.sort();
        canvas.save();
        Matrix matrix2 = r24.A00;
        RectF rectF7 = r24.A05;
        matrix2.setRectToRect(new RectF(rectF7.left, rectF7.top, rectF7.right, rectF7.bottom), rectF6, Matrix.ScaleToFit.CENTER);
        Path path3 = r24.A04;
        path3.reset();
        path3.setFillType(Path.FillType.WINDING);
        r24.A02.transform(matrix2, path3);
        Paint paint4 = r24.A01;
        paint4.setStyle(Paint.Style.FILL);
        paint4.setColor(-31);
        path3.close();
        canvas.drawPath(path3, paint4);
        path3.reset();
        path3.setFillType(Path.FillType.WINDING);
        r24.A03.transform(matrix2, path3);
        Paint paint5 = ((AbstractC454821u) r24).A01;
        paint5.setStyle(Paint.Style.STROKE);
        canvas.drawPath(path3, paint5);
        canvas.restore();
        r24.A0R(canvas, 1.3f, 1.0f);
        r24.A0R(canvas, 1.7f, 0.5f);
    }

    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        if (f == f3) {
            f3 += 1.0f;
        }
        if (f2 == f4) {
            f4 += 1.0f;
        }
        RectF rectF2 = this.A02;
        rectF2.set(f, f2, f3, f4);
        rectF2.sort();
        A04();
    }
}
