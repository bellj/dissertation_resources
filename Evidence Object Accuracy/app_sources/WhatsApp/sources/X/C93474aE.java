package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4aE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93474aE {
    public final int A00;
    public final String A01;
    public final List A02;

    public C93474aE(List list, int i, String str) {
        this.A02 = list;
        this.A00 = i;
        this.A01 = str;
    }

    public static C93474aE A00(C95304dT r15) {
        List list;
        try {
            int A01 = C95304dT.A01(r15, 21) & 3;
            int A0C = r15.A0C();
            int i = r15.A01;
            int i2 = 0;
            for (int i3 = 0; i3 < A0C; i3++) {
                r15.A0T(1);
                int A0F = r15.A0F();
                for (int i4 = 0; i4 < A0F; i4++) {
                    int A0F2 = r15.A0F();
                    i2 += A0F2 + 4;
                    r15.A0T(A0F2);
                }
            }
            r15.A0S(i);
            byte[] bArr = new byte[i2];
            String str = null;
            int i5 = 0;
            for (int i6 = 0; i6 < A0C; i6++) {
                int A0C2 = r15.A0C() & 127;
                int A0F3 = r15.A0F();
                for (int i7 = 0; i7 < A0F3; i7++) {
                    int A0F4 = r15.A0F();
                    byte[] bArr2 = C95464dl.A02;
                    int length = bArr2.length;
                    System.arraycopy(bArr2, 0, bArr, i5, length);
                    int i8 = i5 + length;
                    System.arraycopy(r15.A02, r15.A01, bArr, i8, A0F4);
                    if (A0C2 == 33 && i7 == 0) {
                        str = AnonymousClass4ZS.A00(new AnonymousClass4YP(bArr, i8, i8 + A0F4));
                    }
                    i5 = i8 + A0F4;
                    r15.A0T(A0F4);
                }
            }
            if (i2 == 0) {
                list = null;
            } else {
                list = Collections.singletonList(bArr);
            }
            return new C93474aE(list, A01 + 1, str);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new AnonymousClass496("Error parsing HEVC config", e);
        }
    }
}
