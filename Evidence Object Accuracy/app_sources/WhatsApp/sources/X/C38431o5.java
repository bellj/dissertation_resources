package X;

import java.io.InputStream;

/* renamed from: X.1o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38431o5 implements AbstractC14500lX {
    public final C14370lK A00;

    public C38431o5(C14370lK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC14500lX
    public AbstractC37941nG ACi(byte[] bArr) {
        return new AbstractC37941nG() { // from class: X.564
            @Override // X.AbstractC37941nG
            public final InputStream A9M(InputStream inputStream) {
                return inputStream;
            }
        };
    }

    @Override // X.AbstractC14500lX
    public AnonymousClass2QI ADk() {
        return new AnonymousClass2QI() { // from class: X.563
            @Override // X.AnonymousClass2QI
            public final C37891nB A8r(byte[] bArr) {
                return AbstractC65063Hz.A00(C32891cu.A00(bArr, C38431o5.this.A00.A03, 80));
            }
        };
    }
}
