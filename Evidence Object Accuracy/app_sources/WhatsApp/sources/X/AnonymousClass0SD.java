package X;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;

/* renamed from: X.0SD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SD {
    public Context A00;
    public Uri A01;
    public final AnonymousClass0SD A02;

    public AnonymousClass0SD(Context context, Uri uri, AnonymousClass0SD r3) {
        this.A02 = r3;
        this.A00 = context;
        this.A01 = uri;
    }

    public static AnonymousClass0SD A00(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new AnonymousClass0SD(context, DocumentsContract.buildDocumentUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri)), null);
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004d, code lost:
        if (r10 == null) goto L_0x0055;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0SD[] A01() {
        /*
            r13 = this;
            android.content.Context r6 = r13.A00
            android.content.ContentResolver r7 = r6.getContentResolver()
            android.net.Uri r1 = r13.A01
            java.lang.String r0 = android.provider.DocumentsContract.getDocumentId(r1)
            android.net.Uri r8 = android.provider.DocumentsContract.buildChildDocumentsUriUsingTree(r1, r0)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r0 = 1
            r5 = 0
            r10 = 0
            java.lang.String[] r9 = new java.lang.String[r0]     // Catch: Exception -> 0x0036, all -> 0x0073
            java.lang.String r0 = "document_id"
            r9[r5] = r0     // Catch: Exception -> 0x0036, all -> 0x0073
            r12 = r10
            r11 = r10
            android.database.Cursor r10 = r7.query(r8, r9, r10, r11, r12)     // Catch: Exception -> 0x0036, all -> 0x0073
        L_0x0024:
            boolean r0 = r10.moveToNext()     // Catch: Exception -> 0x0036, all -> 0x0073
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = r10.getString(r5)     // Catch: Exception -> 0x0036, all -> 0x0073
            android.net.Uri r0 = android.provider.DocumentsContract.buildDocumentUriUsingTree(r1, r0)     // Catch: Exception -> 0x0036, all -> 0x0073
            r4.add(r0)     // Catch: Exception -> 0x0036, all -> 0x0073
            goto L_0x0024
        L_0x0036:
            r3 = move-exception
            java.lang.String r2 = "DocumentFile"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0073
            r1.<init>()     // Catch: all -> 0x0073
            java.lang.String r0 = "Failed query: "
            r1.append(r0)     // Catch: all -> 0x0073
            r1.append(r3)     // Catch: all -> 0x0073
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0073
            android.util.Log.w(r2, r0)     // Catch: all -> 0x0073
            if (r10 == 0) goto L_0x0055
        L_0x004f:
            r10.close()     // Catch: RuntimeException -> 0x0053, Exception -> 0x0055
            goto L_0x0055
        L_0x0053:
            r0 = move-exception
            throw r0
        L_0x0055:
            int r0 = r4.size()
            android.net.Uri[] r0 = new android.net.Uri[r0]
            java.lang.Object[] r4 = r4.toArray(r0)
            android.net.Uri[] r4 = (android.net.Uri[]) r4
            int r3 = r4.length
            X.0SD[] r2 = new X.AnonymousClass0SD[r3]
        L_0x0064:
            if (r5 >= r3) goto L_0x0072
            r1 = r4[r5]
            X.0SD r0 = new X.0SD
            r0.<init>(r6, r1, r13)
            r2[r5] = r0
            int r5 = r5 + 1
            goto L_0x0064
        L_0x0072:
            return r2
        L_0x0073:
            r0 = move-exception
            if (r10 == 0) goto L_0x007c
            r10.close()     // Catch: RuntimeException -> 0x007a, Exception -> 0x007c
            throw r0
        L_0x007a:
            r0 = move-exception
            throw r0
        L_0x007c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SD.A01():X.0SD[]");
    }
}
