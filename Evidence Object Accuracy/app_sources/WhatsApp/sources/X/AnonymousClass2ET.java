package X;

import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

/* renamed from: X.2ET  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2ET {
    public final AnonymousClass19Q A00;
    public final AnonymousClass19T A01;
    public final AnonymousClass4JX A02;
    public final C25801Aw A03;

    public AnonymousClass2ET(AnonymousClass19Q r2, AnonymousClass19T r3, AnonymousClass4JX r4, C25801Aw r5) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r2, 4);
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A00 = r2;
    }

    public void A00(UserJid userJid, Set set, AnonymousClass1J7 r13) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            if (!this.A03.A04(userJid, (String) it.next())) {
                Set set2 = set;
                if (set.contains("catalog_category_dummy_root_id")) {
                    set2 = C16900pw.A00;
                }
                C68313Ux r2 = new C68313Ux(this, userJid, set, r13);
                int i = this.A02.A00;
                C92404Vt r4 = new C92404Vt(userJid, this.A00.A00, set2, i, i);
                this.A01.A00(new C68303Uw(r2, this), r4);
                return;
            }
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it2 = set.iterator();
        while (it2.hasNext()) {
            String str = (String) it2.next();
            linkedHashMap.put(str, this.A03.A01(userJid, str));
        }
        r13.AJ4(new C60262wN(linkedHashMap, true));
    }
}
