package X;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DR  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DR implements ListIterator, AbstractC16910px {
    public static final AnonymousClass5DR A00 = new AnonymousClass5DR();

    @Override // java.util.ListIterator, java.util.Iterator
    public boolean hasNext() {
        return false;
    }

    @Override // java.util.ListIterator
    public boolean hasPrevious() {
        return false;
    }

    @Override // java.util.ListIterator
    public int nextIndex() {
        return 0;
    }

    @Override // java.util.ListIterator
    public int previousIndex() {
        return -1;
    }

    @Override // java.util.ListIterator
    public /* bridge */ /* synthetic */ void add(Object obj) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public Object next() {
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator
    public Object previous() {
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.ListIterator
    public /* bridge */ /* synthetic */ void set(Object obj) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }
}
