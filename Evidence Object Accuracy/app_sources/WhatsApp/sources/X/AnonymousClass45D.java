package X;

import java.io.File;

/* renamed from: X.45D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45D extends AbstractC91424Rr {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public boolean A04;
    public int[] A05;

    public AnonymousClass45H A00() {
        File file = super.A00;
        byte[] bArr = super.A03;
        boolean z = super.A02;
        int i = this.A02;
        int i2 = this.A03;
        int i3 = this.A00;
        int i4 = this.A01;
        boolean z2 = this.A04;
        return new AnonymousClass45H(file, super.A01, bArr, this.A05, i, i2, i3, i4, z, z2);
    }
}
