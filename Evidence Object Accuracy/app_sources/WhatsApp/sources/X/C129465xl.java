package X;

import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.5xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129465xl {
    public int A00 = 0;
    public final ReentrantLock A01 = new ReentrantLock();

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r1 != false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00() {
        /*
            r3 = this;
            java.util.concurrent.locks.ReentrantLock r2 = r3.A01
            r2.lock()
            r2.lock()     // Catch: all -> 0x0036
            int r1 = r3.A00     // Catch: all -> 0x0031
            r0 = 2
            r1 = r1 & r0
            boolean r0 = X.C12960it.A1V(r1, r0)
            r2.unlock()     // Catch: all -> 0x0036
            if (r0 != 0) goto L_0x002c
            r2.lock()     // Catch: all -> 0x0036
            int r1 = r3.A00     // Catch: all -> 0x0024
            r0 = 4
            r1 = r1 & r0
            boolean r1 = X.C12960it.A1V(r1, r0)
            r2.unlock()     // Catch: all -> 0x0036
            goto L_0x0029
        L_0x0024:
            r0 = move-exception
            r2.unlock()     // Catch: all -> 0x0036
            throw r0     // Catch: all -> 0x0036
        L_0x0029:
            r0 = 0
            if (r1 == 0) goto L_0x002d
        L_0x002c:
            r0 = 1
        L_0x002d:
            r2.unlock()
            return r0
        L_0x0031:
            r0 = move-exception
            r2.unlock()     // Catch: all -> 0x0036
            throw r0     // Catch: all -> 0x0036
        L_0x0036:
            r0 = move-exception
            r2.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129465xl.A00():boolean");
    }

    public boolean A01() {
        ReentrantLock reentrantLock = this.A01;
        reentrantLock.lock();
        try {
            return C12960it.A1T(this.A00);
        } finally {
            reentrantLock.unlock();
        }
    }
}
