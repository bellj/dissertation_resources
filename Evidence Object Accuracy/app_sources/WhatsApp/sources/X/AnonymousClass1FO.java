package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import com.whatsapp.R;
import java.text.DateFormat;
import java.util.Date;

/* renamed from: X.1FO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FO {
    public static final int[] A03 = {1, 2, 3, 5, 14, 30};
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final AnonymousClass018 A02;

    public AnonymousClass1FO(C14830m7 r1, C14820m6 r2, AnonymousClass018 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public static Uri A00(C21740xu r1) {
        if (C38241nl.A03()) {
            return Uri.parse("https://faq.whatsapp.com/android/download-and-installation/about-supported-android-devices");
        }
        return r1.A01();
    }

    public static String A01(Context context, Uri uri, int i) {
        boolean A032 = C38241nl.A03();
        int i2 = R.string.futureproof_message_action_update;
        if (A032) {
            i2 = R.string.futureproof_message_action_learn_more;
        }
        return context.getString(i, context.getString(i2, uri.toString()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0052, code lost:
        if (r6 < (r7 + 7)) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02(X.C20640w5 r15) {
        /*
            r14 = this;
            X.0m6 r0 = r14.A01
            android.content.SharedPreferences r9 = r0.A00
            java.lang.String r8 = "software_expiration_last_warned"
            r0 = 0
            long r12 = r9.getLong(r8, r0)
            X.0m7 r0 = r14.A00
            long r3 = r0.A00()
            int r0 = (r12 > r3 ? 1 : (r12 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0019
            r12 = 0
        L_0x0019:
            r1 = 86400000(0x5265c00, double:4.2687272E-316)
            long r1 = r1 + r12
            r11 = -1
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0029
            java.lang.String r0 = "software/expiration/suppress/24h"
            com.whatsapp.util.Log.i(r0)
        L_0x0028:
            return r11
        L_0x0029:
            java.util.Date r10 = r15.A01()
            long r1 = r10.getTime()
            long r1 = r1 - r3
            r5 = 86400000(0x5265c00, double:4.2687272E-316)
            long r1 = r1 / r5
            int r0 = (int) r1
            int r7 = r0 + 1
            long r0 = r10.getTime()
            long r0 = r0 - r12
            long r0 = r0 / r5
            int r2 = (int) r0
            int r6 = r2 + 1
            boolean r0 = X.C38241nl.A03()
            if (r0 == 0) goto L_0x0060
            r0 = 30
            if (r7 <= r0) goto L_0x0054
            r0 = 90
            if (r7 > r0) goto L_0x0028
            int r0 = r7 + 7
            if (r6 < r0) goto L_0x0028
        L_0x0054:
            android.content.SharedPreferences$Editor r0 = r9.edit()
            android.content.SharedPreferences$Editor r0 = r0.putLong(r8, r3)
            r0.apply()
            return r7
        L_0x0060:
            int[] r5 = X.AnonymousClass1FO.A03
            int r2 = r5.length
            r1 = 0
        L_0x0064:
            if (r1 >= r2) goto L_0x0028
            r0 = r5[r1]
            if (r7 > r0) goto L_0x006d
            if (r6 <= r0) goto L_0x006d
            goto L_0x0054
        L_0x006d:
            int r1 = r1 + 1
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1FO.A02(X.0w5):int");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v7, resolved type: android.text.SpannableString */
    /* JADX WARN: Multi-variable type inference failed */
    public Dialog A03(Activity activity, C20640w5 r11, C21740xu r12) {
        boolean z;
        int time = ((int) ((r11.A01().getTime() - this.A00.A00()) / 86400000)) + 1;
        if (C38241nl.A03()) {
            boolean z2 = false;
            if (time <= 30) {
                z2 = true;
            }
            int A01 = this.A01.A01();
            if (!z2 || !(A01 == 0 || A01 == 4)) {
                z = false;
            } else {
                z = true;
            }
            String string = activity.getString(R.string.software_about_to_deprecate_title);
            if (z2) {
                int A00 = AnonymousClass00T.A00(activity, R.color.settings_dangerous_text);
                SpannableString spannableString = new SpannableString(string);
                spannableString.setSpan(new ForegroundColorSpan(A00), 0, string.length(), 0);
                string = spannableString;
            }
            Spanned A002 = AnonymousClass1US.A00(activity, new Object[]{DateFormat.getDateInstance(2, AnonymousClass018.A00(this.A02.A00)).format(new Date(r11.A01().getTime() - 86400000)), activity.getString(R.string.localized_app_name)}, R.string.software_about_to_deprecate_with_date);
            C004802e r2 = new C004802e(activity);
            r2.setTitle(string);
            r2.A0A(A002);
            r2.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener(activity) { // from class: X.2Ou
                public final /* synthetic */ Activity A00;

                {
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    C36021jC.A00(this.A00, 115);
                }
            });
            r2.A00(R.string.learn_more, new DialogInterface.OnClickListener(activity) { // from class: X.2Ov
                public final /* synthetic */ Activity A00;

                {
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    Activity activity2 = this.A00;
                    C36021jC.A00(activity2, 115);
                    activity2.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse("https://faq.whatsapp.com/android/download-and-installation/about-supported-android-devices")));
                }
            });
            if (z) {
                r2.setPositiveButton(R.string.settings_msg_store_backup_now, new DialogInterface.OnClickListener(activity) { // from class: X.2Ow
                    public final /* synthetic */ Activity A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        Activity activity2 = this.A00;
                        C36021jC.A00(activity2, 115);
                        Intent intent = new Intent();
                        intent.setClassName(activity2.getPackageName(), "com.whatsapp.settings.SettingsChat");
                        activity2.startActivity(intent);
                    }
                });
            }
            return r2.create();
        }
        C004802e r6 = new C004802e(activity);
        r6.A07(R.string.software_about_to_expire_title);
        r6.A0A(this.A02.A0I(new Object[]{Integer.valueOf(time)}, R.plurals.software_about_to_expire, (long) time));
        r6.setPositiveButton(R.string.upgrade, new DialogInterface.OnClickListener(activity, r12) { // from class: X.2Ox
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ C21740xu A01;

            {
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                Activity activity2 = this.A00;
                C21740xu r22 = this.A01;
                C36021jC.A00(activity2, 115);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(r22.A01());
                activity2.startActivity(intent);
            }
        });
        r6.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(activity) { // from class: X.2Oy
            public final /* synthetic */ Activity A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C36021jC.A00(this.A00, 115);
            }
        });
        return r6.create();
    }
}
