package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.3o1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77883o1 extends AbstractC77963o9 {
    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12451000;
    }

    public C77883o1(Context context, Looper looper, AbstractC14980mM r10, AbstractC15000mO r11, AnonymousClass3BW r12) {
        super(context, looper, r10, r11, r12, 126);
    }
}
