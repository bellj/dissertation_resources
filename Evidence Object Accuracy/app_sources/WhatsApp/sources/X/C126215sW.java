package X;

/* renamed from: X.5sW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126215sW {
    public final AnonymousClass1V8 A00;

    public C126215sW(AnonymousClass3CT r17, String str, String str2, String str3, String str4, String str5, String str6) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-bind-network-token");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 10000, false)) {
            C41141sy.A01(A0N, "device-csr", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 10000, false)) {
            C41141sy.A01(A0N, "jws-token", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 1, 10000, false)) {
            C41141sy.A01(A0N, "client-reference-id", str4);
        }
        if (C117295Zj.A1V(str5, 1, false)) {
            C41141sy.A01(A0N, "device-id", str5);
        }
        if (C117295Zj.A1W(str6, 1, false)) {
            C41141sy.A01(A0N, "nonce", str6);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r17);
    }
}
