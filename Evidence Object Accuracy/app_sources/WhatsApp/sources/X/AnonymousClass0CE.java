package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* renamed from: X.0CE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CE extends AnonymousClass0CA implements AbstractC013806l {
    public int A00 = -1;
    public int A01 = -1;
    public AnonymousClass0CC A02;
    public AbstractC05490Pt A03;
    public boolean A04;

    public AnonymousClass0CE(Resources resources, AnonymousClass0CC r3) {
        super(null);
        A04(new AnonymousClass0CC(resources, r3, this));
        onStateChange(getState());
        jumpToCurrentState();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:47:0x0123 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:77:0x01ed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v3, types: [android.graphics.drawable.Drawable] */
    /* JADX WARN: Type inference failed for: r4v5, types: [android.graphics.drawable.Drawable] */
    /* JADX WARN: Type inference failed for: r0v68, types: [android.graphics.drawable.Drawable] */
    /* JADX WARN: Type inference failed for: r0v72, types: [android.graphics.drawable.Drawable] */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0229, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append(r27.getPositionDescription());
        r1.append(": <transition> tag requires 'fromId' & 'toId' attributes");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0243, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r1.toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0CE A00(android.content.Context r23, android.content.res.Resources.Theme r24, android.content.res.Resources r25, android.util.AttributeSet r26, org.xmlpull.v1.XmlPullParser r27) {
        /*
        // Method dump skipped, instructions count: 643
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CE.A00(android.content.Context, android.content.res.Resources$Theme, android.content.res.Resources, android.util.AttributeSet, org.xmlpull.v1.XmlPullParser):X.0CE");
    }

    @Override // X.AnonymousClass0CA, X.AnonymousClass0AB
    public /* bridge */ /* synthetic */ AnonymousClass09z A03() {
        return new AnonymousClass0CC(null, this.A02, this);
    }

    @Override // X.AnonymousClass0CA, X.AnonymousClass0AB
    public void A04(AnonymousClass09z r2) {
        super.A04(r2);
        if (r2 instanceof AnonymousClass0CC) {
            this.A02 = (AnonymousClass0CC) r2;
        }
    }

    @Override // X.AnonymousClass0CA
    public /* bridge */ /* synthetic */ AnonymousClass0CD A05() {
        return new AnonymousClass0CC(null, this.A02, this);
    }

    @Override // X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        AbstractC05490Pt r0 = this.A03;
        if (r0 != null) {
            r0.A02();
            this.A03 = null;
            A02(this.A01);
            this.A01 = -1;
            this.A00 = -1;
        }
    }

    @Override // X.AnonymousClass0CA, X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public Drawable mutate() {
        if (!this.A04) {
            super.mutate();
            if (this == this) {
                this.A02.A04();
                this.A04 = true;
            }
        }
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cd, code lost:
        if (A02(r5) != false) goto L_0x0025;
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass0CA, X.AnonymousClass0AB, android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onStateChange(int[] r15) {
        /*
            r14 = this;
            X.0CC r0 = r14.A02
            int r5 = r0.A0A(r15)
            int r4 = r14.A01
            if (r5 == r4) goto L_0x00d1
            X.0Pt r1 = r14.A03
            if (r1 == 0) goto L_0x0033
            int r4 = r14.A01
            if (r5 == r4) goto L_0x0025
            int r0 = r14.A00
            if (r5 != r0) goto L_0x0030
            boolean r0 = r1.A03()
            if (r0 == 0) goto L_0x0030
            r1.A00()
            int r0 = r14.A00
            r14.A01 = r0
            r14.A00 = r5
        L_0x0025:
            r1 = 1
        L_0x0026:
            android.graphics.drawable.Drawable r0 = r14.A05
            if (r0 == 0) goto L_0x002f
            boolean r0 = r0.setState(r15)
            r1 = r1 | r0
        L_0x002f:
            return r1
        L_0x0030:
            r1.A02()
        L_0x0033:
            r0 = 0
            r14.A03 = r0
            r0 = -1
            r14.A00 = r0
            r14.A01 = r0
            X.0CC r8 = r14.A02
            int r0 = r8.A09(r4)
            int r1 = r8.A09(r5)
            if (r1 == 0) goto L_0x00c9
            if (r0 == 0) goto L_0x00c9
            long r2 = (long) r0
            r0 = 32
            long r2 = r2 << r0
            long r0 = (long) r1
            long r0 = r0 | r2
            X.036 r6 = r8.A00
            r2 = -1
            java.lang.Long r7 = java.lang.Long.valueOf(r2)
            java.lang.Object r2 = r6.A04(r0, r7)
            java.lang.Number r2 = (java.lang.Number) r2
            long r2 = r2.longValue()
            int r11 = (int) r2
            if (r11 < 0) goto L_0x00c9
            X.036 r2 = r8.A00
            java.lang.Object r2 = r2.A04(r0, r7)
            java.lang.Number r2 = (java.lang.Number) r2
            long r12 = r2.longValue()
            r2 = 8589934592(0x200000000, double:4.243991582E-314)
            long r12 = r12 & r2
            r9 = 0
            int r2 = (r12 > r9 ? 1 : (r12 == r9 ? 0 : -1))
            r6 = 0
            if (r2 == 0) goto L_0x007e
            r6 = 1
        L_0x007e:
            r14.A02(r11)
            android.graphics.drawable.Drawable r3 = r14.A05
            boolean r2 = r3 instanceof android.graphics.drawable.AnimationDrawable
            if (r2 == 0) goto L_0x00b1
            X.036 r2 = r8.A00
            java.lang.Object r0 = r2.A04(r0, r7)
            java.lang.Number r0 = (java.lang.Number) r0
            long r7 = r0.longValue()
            r0 = 4294967296(0x100000000, double:2.121995791E-314)
            long r7 = r7 & r0
            int r0 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            r1 = 0
            if (r0 == 0) goto L_0x009f
            r1 = 1
        L_0x009f:
            android.graphics.drawable.AnimationDrawable r3 = (android.graphics.drawable.AnimationDrawable) r3
            X.0C9 r0 = new X.0C9
            r0.<init>(r3, r1, r6)
        L_0x00a6:
            r0.A01()
            r14.A03 = r0
            r14.A00 = r4
            r14.A01 = r5
            goto L_0x0025
        L_0x00b1:
            boolean r0 = r3 instanceof X.AnonymousClass07M
            if (r0 == 0) goto L_0x00bd
            X.07M r3 = (X.AnonymousClass07M) r3
            X.0C8 r0 = new X.0C8
            r0.<init>(r3)
            goto L_0x00a6
        L_0x00bd:
            boolean r0 = r3 instanceof android.graphics.drawable.Animatable
            if (r0 == 0) goto L_0x00c9
            android.graphics.drawable.Animatable r3 = (android.graphics.drawable.Animatable) r3
            X.0C7 r0 = new X.0C7
            r0.<init>(r3)
            goto L_0x00a6
        L_0x00c9:
            boolean r0 = r14.A02(r5)
            if (r0 == 0) goto L_0x00d1
            goto L_0x0025
        L_0x00d1:
            r1 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CE.onStateChange(int[]):boolean");
    }

    @Override // X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        AbstractC05490Pt r0 = this.A03;
        if (r0 != null && (visible || z2)) {
            if (z) {
                r0.A01();
            } else {
                jumpToCurrentState();
                return visible;
            }
        }
        return visible;
    }
}
