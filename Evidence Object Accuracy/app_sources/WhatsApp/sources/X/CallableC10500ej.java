package X;

import java.io.InputStream;
import java.util.concurrent.Callable;

/* renamed from: X.0ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10500ej implements Callable {
    public final /* synthetic */ InputStream A00;
    public final /* synthetic */ String A01;

    public CallableC10500ej(InputStream inputStream, String str) {
        this.A00 = inputStream;
        this.A01 = str;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return C06550Ub.A03(this.A00, this.A01);
    }
}
