package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.WaEditText;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.47K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47K extends C469928m {
    public final /* synthetic */ GifSearchContainer A00;

    public AnonymousClass47K(GifSearchContainer gifSearchContainer) {
        this.A00 = gifSearchContainer;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        GifSearchContainer gifSearchContainer = this.A00;
        gifSearchContainer.A0L = charSequence;
        WaEditText waEditText = gifSearchContainer.A07;
        Runnable runnable = gifSearchContainer.A0V;
        waEditText.removeCallbacks(runnable);
        gifSearchContainer.A07.postDelayed(runnable, 500);
        View view = gifSearchContainer.A01;
        int i4 = 0;
        if (TextUtils.isEmpty(charSequence)) {
            i4 = 4;
        }
        view.setVisibility(i4);
    }
}
