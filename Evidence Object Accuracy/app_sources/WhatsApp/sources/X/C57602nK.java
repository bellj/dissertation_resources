package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57602nK extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57602nK A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public int A01;
    public long A02;
    public AbstractC27881Jp A03 = AbstractC27881Jp.A01;
    public C43261wh A04;
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";

    static {
        C57602nK r0 = new C57602nK();
        A09 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C81603uH r1;
        switch (r15.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57602nK r2 = (C57602nK) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A06;
                int i2 = r2.A00;
                this.A06 = r7.Afy(str, r2.A06, A1R, C12960it.A1R(i2));
                this.A08 = r7.Afy(this.A08, r2.A08, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A02 = r7.Afs(this.A02, r2.A02, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A07 = r7.Afy(this.A07, r2.A07, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A03 = r7.Afm(this.A03, r2.A03, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A05 = r7.Afy(this.A05, r2.A05, C12960it.A1V(this.A00 & 32, 32), C12960it.A1V(r2.A00 & 32, 32));
                this.A04 = (C43261wh) r7.Aft(this.A04, r2.A04);
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 128, 128);
                int i4 = this.A01;
                int i5 = r2.A00;
                this.A01 = r7.Afp(i4, r2.A01, A1V, C12960it.A1V(i5 & 128, 128));
                if (r7 == C463025i.A00) {
                    this.A00 = i3 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A2 = r72.A0A();
                            this.A00 = 1 | this.A00;
                            this.A06 = A0A2;
                        } else if (A03 == 18) {
                            String A0A3 = r72.A0A();
                            this.A00 |= 2;
                            this.A08 = A0A3;
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A02 = r72.A06();
                        } else if (A03 == 34) {
                            String A0A4 = r72.A0A();
                            this.A00 |= 8;
                            this.A07 = A0A4;
                        } else if (A03 == 42) {
                            this.A00 |= 16;
                            this.A03 = r72.A08();
                        } else if (A03 == 50) {
                            String A0A5 = r72.A0A();
                            this.A00 |= 32;
                            this.A05 = A0A5;
                        } else if (A03 == 58) {
                            if ((this.A00 & 64) == 64) {
                                r1 = (C81603uH) this.A04.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r72, r22, C43261wh.A0O);
                            this.A04 = r0;
                            if (r1 != null) {
                                this.A04 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 64;
                        } else if (A03 == 64) {
                            int A02 = r72.A02();
                            if (A02 == 0 || A02 == 1) {
                                this.A00 |= 128;
                                this.A01 = A02;
                            } else {
                                super.A0X(8, A02);
                            }
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57602nK();
            case 5:
                return new C81893uk();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (C57602nK.class) {
                        if (A0A == null) {
                            A0A = AbstractC27091Fz.A09(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw C12970iu.A0z();
        }
        return A09;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A06, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A08, i2);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A05(3, this.A02);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A07, i2);
        }
        int i4 = this.A00;
        if ((i4 & 16) == 16) {
            i2 = AbstractC27091Fz.A05(this.A03, 5, i2);
        }
        if ((i4 & 32) == 32) {
            i2 = AbstractC27091Fz.A04(6, this.A05, i2);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 7, i2);
        }
        if ((this.A00 & 128) == 128) {
            i2 = AbstractC27091Fz.A03(8, this.A01, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A06);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A08);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A02);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A07);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A03, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(6, this.A05);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 7);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0E(8, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
