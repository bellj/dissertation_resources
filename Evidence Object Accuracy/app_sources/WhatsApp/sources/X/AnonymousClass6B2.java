package X;

import java.io.File;

/* renamed from: X.6B2  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B2 implements AbstractC117185Yw {
    public final /* synthetic */ C129025x3 A00;
    public final /* synthetic */ C118855cU A01;

    public AnonymousClass6B2(C129025x3 r1, C118855cU r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass22Y
    public void AMI(C30921Zi r3, File file) {
        if (file != null) {
            C129025x3 r1 = this.A00;
            r1.A01 = false;
            r1.A00 = true;
        }
        this.A01.A08(this.A00);
    }

    @Override // X.AnonymousClass22X
    public void APk() {
        this.A01.A08(this.A00);
    }

    @Override // X.AnonymousClass22X
    public void AXX() {
        this.A01.A08(this.A00);
    }
}
