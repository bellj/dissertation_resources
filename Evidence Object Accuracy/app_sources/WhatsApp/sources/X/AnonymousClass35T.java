package X;

import android.os.Handler;
import android.os.Looper;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/* renamed from: X.35T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35T extends AbstractC33641ei {
    public AbstractC28651Ol A00 = null;
    public final Handler A01 = new AnonymousClass2a7(Looper.getMainLooper(), this);
    public final AbstractC15710nm A02;
    public final C16590pI A03;
    public final C14850m9 A04;
    public final C30421Xi A05;
    public final C53022cE A06;

    public AnonymousClass35T(AnonymousClass12Q r11, AbstractC15710nm r12, C14900mE r13, AnonymousClass1J1 r14, AnonymousClass01d r15, C16590pI r16, AnonymousClass018 r17, C14850m9 r18, AnonymousClass1CH r19, AbstractC15340mz r20, C48242Fd r21) {
        super(r11, r13, r15, r17, r19, r21);
        AnonymousClass009.A05(r20);
        C30421Xi r2 = (C30421Xi) r20;
        this.A05 = r2;
        C53022cE r0 = new C53022cE(A02());
        r0.setMessage(r2, r14);
        this.A06 = r0;
        this.A03 = r16;
        this.A04 = r18;
        this.A02 = r12;
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        return TimeUnit.SECONDS.toMillis((long) ((AbstractC16130oV) this.A05).A00);
    }

    @Override // X.AbstractC33641ei
    public void A07() {
        AbstractC28651Ol A0C = A0C();
        if (A0C != null) {
            try {
                A0C.A08();
                super.A05.A01();
                this.A01.sendEmptyMessage(0);
                return;
            } catch (IOException e) {
                Log.e(e);
            }
        }
        super.A01.A07(R.string.gallery_audio_cannot_load, 0);
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        AbstractC28651Ol r0 = this.A00;
        if (r0 != null) {
            r0.A09();
            this.A00.A06();
            this.A00 = null;
        }
        this.A01.removeMessages(0);
    }

    public final AbstractC28651Ol A0C() {
        if (this.A00 == null) {
            C16150oX A00 = AbstractC15340mz.A00(this.A05);
            try {
                AbstractC28651Ol A002 = AbstractC28651Ol.A00(this.A03, this.A04, A00.A0F, 3);
                this.A00 = A002;
                try {
                    A002.A05();
                } catch (IOException | IllegalStateException e) {
                    this.A02.AaV("StatusPlaybackVoice/failed to prepare audio player", e.toString(), true);
                    throw e;
                }
            } catch (IOException e2) {
                Log.e(e2);
            }
        }
        return this.A00;
    }
}
