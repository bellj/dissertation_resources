package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1G5  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1G5 implements AnonymousClass1G2, Cloneable {
    public static void A01(Iterable iterable, Collection collection) {
        boolean z = iterable instanceof Collection;
        Iterator it = iterable.iterator();
        if (z) {
            while (it.hasNext()) {
                it.next();
            }
            collection.addAll((Collection) iterable);
            return;
        }
        while (it.hasNext()) {
            collection.add(it.next());
        }
    }

    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() {
        throw null;
    }
}
