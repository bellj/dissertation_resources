package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.storage.StorageUsageMediaPreviewOverflowOverlayView;

/* renamed from: X.2cA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52982cA extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2T3 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public final WaTextView A03;
    public final StorageUsageMediaPreviewOverflowOverlayView A04;

    public C52982cA(Context context) {
        super(context, null, 0);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
        C12960it.A0E(this).inflate(R.layout.storage_usage_media_preview_overflow_item_view, (ViewGroup) this, true);
        this.A04 = (StorageUsageMediaPreviewOverflowOverlayView) AnonymousClass028.A0D(this, R.id.overflow_overlay_view);
        this.A03 = C12960it.A0N(this, R.id.overflow_text_view);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    public void setFrameDrawable(Drawable drawable) {
        this.A04.setFrameDrawable(drawable);
        AnonymousClass2T3 r0 = this.A00;
        if (r0 != null) {
            r0.setFrameDrawable(drawable);
        }
    }
}
