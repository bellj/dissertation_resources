package X;

import android.content.Context;
import com.whatsapp.status.playback.StatusReplyActivity;

/* renamed from: X.4qx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103564qx implements AbstractC009204q {
    public final /* synthetic */ StatusReplyActivity A00;

    public C103564qx(StatusReplyActivity statusReplyActivity) {
        this.A00 = statusReplyActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
