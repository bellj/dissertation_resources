package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.1kE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36631kE extends ArrayAdapter {
    public final /* synthetic */ AbstractActivityC36611kC A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36631kE(Context context, AbstractActivityC36611kC r3, List list) {
        super(context, (int) R.layout.multiple_contact_picker_row, list);
        this.A00 = r3;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C64043Ea r0;
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        C15370n3 r4 = (C15370n3) item;
        if (view == null) {
            AbstractActivityC36611kC r3 = this.A00;
            view = r3.getLayoutInflater().inflate(R.layout.multiple_contact_picker_row, viewGroup, false);
            r0 = new C64043Ea(view, r3.A0L, r3, r3.A0U);
            view.setTag(r0);
        } else {
            r0 = (C64043Ea) view.getTag();
        }
        this.A00.A30(r0, r4);
        return view;
    }
}
