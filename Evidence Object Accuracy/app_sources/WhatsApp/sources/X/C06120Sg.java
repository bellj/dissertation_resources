package X;

import android.os.Handler;
import android.os.Looper;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: X.0Sg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06120Sg {
    public static Executor A04 = Executors.newCachedThreadPool();
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final Set A01 = new LinkedHashSet(1);
    public final Set A02 = new LinkedHashSet(1);
    public volatile AnonymousClass0ST A03 = null;

    public C06120Sg(Callable callable, boolean z) {
        if (z) {
            try {
                A02((AnonymousClass0ST) callable.call());
            } catch (Throwable th) {
                A02(new AnonymousClass0ST(th));
            }
        } else {
            A04.execute(new C10960fU(this, callable));
        }
    }

    public synchronized void A00(AbstractC12010hE r2) {
        if (!(this.A03 == null || this.A03.A01 == null)) {
            r2.AVI(this.A03.A01);
        }
        this.A01.add(r2);
    }

    public synchronized void A01(AbstractC12010hE r2) {
        if (!(this.A03 == null || this.A03.A00 == null)) {
            r2.AVI(this.A03.A00);
        }
        this.A02.add(r2);
    }

    public final void A02(AnonymousClass0ST r3) {
        if (this.A03 == null) {
            this.A03 = r3;
            this.A00.post(new RunnableC09370cn(this));
            return;
        }
        throw new IllegalStateException("A task may only be set once.");
    }
}
