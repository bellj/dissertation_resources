package X;

import android.os.Build;
import androidx.work.OverwritingInputMerger;

/* renamed from: X.01x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C004201x extends AbstractC004301y {
    public C004201x(Class cls) {
        super(cls);
        this.A00.A0F = OverwritingInputMerger.class.getName();
    }

    @Override // X.AbstractC004301y
    public /* bridge */ /* synthetic */ AnonymousClass020 A01() {
        if (!this.A03 || Build.VERSION.SDK_INT < 23 || !this.A00.A09.A04()) {
            return new AnonymousClass021(this);
        }
        throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
    }
}
