package X;

import android.content.Context;
import java.util.ArrayList;

/* renamed from: X.5ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129965ya {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C15650ng A03;
    public final C18650sn A04;
    public final C18600si A05;
    public final C18610sj A06;
    public final C18620sk A07;
    public final C17070qD A08;
    public final C18590sh A09;
    public final AbstractC14440lR A0A;

    public C129965ya(Context context, C14900mE r2, C18640sm r3, C15650ng r4, C18650sn r5, C18600si r6, C18610sj r7, C18620sk r8, C17070qD r9, C18590sh r10, AbstractC14440lR r11) {
        this.A00 = context;
        this.A01 = r2;
        this.A0A = r11;
        this.A09 = r10;
        this.A08 = r9;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A02 = r3;
        this.A04 = r5;
    }

    public void A00(AnonymousClass1FK r14) {
        C17220qS r6 = this.A06.A08;
        String A01 = r6.A01();
        r6.A09(new C120185fl(this.A00, this.A01, r14, this.A04, this), new C126355sk(new C126385sn(A01), this.A09.A01()).A00, A01, 204, C26061Bw.A0L);
    }

    public void A01(C129115xC r9, AnonymousClass1V8 r10, String str, String str2, byte[] bArr) {
        ArrayList A0l = C12960it.A0l();
        if (r10 != null) {
            A0l.add(r10);
        }
        if (bArr != null) {
            A0l.add(new AnonymousClass1V8("password", bArr, new AnonymousClass1W9[0]));
        }
        C18610sj r3 = this.A06;
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[4];
        C12960it.A1M("action", "generate-payments-dyi-report", r4, 0);
        C12960it.A1M("version", "1", r4, 1);
        C12960it.A1M("nonce", str, r4, 2);
        C12960it.A1M("type", str2, r4, 3);
        r3.A0E(new C120195fm(this.A00, this.A01, this.A04, this, r9), new AnonymousClass1V8("account", r4, (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0])), "get");
    }
}
