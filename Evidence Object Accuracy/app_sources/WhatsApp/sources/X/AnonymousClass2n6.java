package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2n6 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2n6 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public C57592nJ A04;

    static {
        AnonymousClass2n6 r0 = new AnonymousClass2n6();
        A05 = r0;
        r0.A0W();
    }

    public AnonymousClass2n6() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A02 = r0;
        this.A03 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81623uJ r1;
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass2n6 r9 = (AnonymousClass2n6) obj2;
                this.A04 = (C57592nJ) r8.Aft(this.A04, r9.A04);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                int i2 = this.A01;
                int i3 = r9.A00;
                this.A01 = r8.Afp(i2, r9.A01, A1V, C12960it.A1V(i3 & 2, 2));
                this.A02 = r8.Afm(this.A02, r9.A02, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A03 = r8.Afm(this.A03, r9.A03, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r9.A00 & 8, 8));
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            if ((this.A00 & 1) == 1) {
                                r1 = (C81623uJ) this.A04.A0T();
                            } else {
                                r1 = null;
                            }
                            C57592nJ r0 = (C57592nJ) AbstractC27091Fz.A0H(r82, r92, C57592nJ.A09);
                            this.A04 = r0;
                            if (r1 != null) {
                                this.A04 = (C57592nJ) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 1;
                        } else if (A03 == 16) {
                            this.A00 |= 2;
                            this.A01 = r82.A02();
                        } else if (A03 == 26) {
                            this.A00 |= 4;
                            this.A02 = r82.A08();
                        } else if (A03 == 34) {
                            this.A00 |= 8;
                            this.A03 = r82.A08();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2n6();
            case 5:
                return new C82383vX();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (AnonymousClass2n6.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C57592nJ r0 = this.A04;
            if (r0 == null) {
                r0 = C57592nJ.A09;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A03(2, this.A01);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A02, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A03, 4, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C57592nJ r0 = this.A04;
            if (r0 == null) {
                r0 = C57592nJ.A09;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A02, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A03, 4);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
