package X;

import com.whatsapp.authentication.AppAuthenticationActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3xy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83833xy extends AnonymousClass4UT {
    public final /* synthetic */ AppAuthenticationActivity A00;

    public C83833xy(AppAuthenticationActivity appAuthenticationActivity) {
        this.A00 = appAuthenticationActivity;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        Log.i("AppAuthenticationActivity/fingerprint-success-animation-end");
        AppAuthenticationActivity appAuthenticationActivity = this.A00;
        appAuthenticationActivity.A2T();
        appAuthenticationActivity.finish();
    }
}
