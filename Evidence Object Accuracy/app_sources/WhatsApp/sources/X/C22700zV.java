package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.0zV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22700zV {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C15570nT A01;
    public final C15450nH A02;
    public final C32121be A03 = new C32121be();
    public final C15550nR A04;
    public final C21580xe A05;
    public final AnonymousClass10S A06;
    public final C14820m6 A07;
    public final AnonymousClass018 A08;
    public final C15990oG A09;
    public final C18240s8 A0A;
    public final C16120oU A0B;
    public final Object A0C = new Object();
    public final Map A0D = Collections.synchronizedMap(new HashMap());

    public C22700zV(C15570nT r3, C15450nH r4, C15550nR r5, C21580xe r6, AnonymousClass10S r7, C14820m6 r8, AnonymousClass018 r9, C15990oG r10, C18240s8 r11, C16120oU r12) {
        this.A01 = r3;
        this.A0B = r12;
        this.A02 = r4;
        this.A04 = r5;
        this.A08 = r9;
        this.A06 = r7;
        this.A09 = r10;
        this.A07 = r8;
        this.A05 = r6;
        this.A0A = r11;
    }

    public AnonymousClass1M2 A00(UserJid userJid) {
        C21580xe r0 = this.A05;
        AnonymousClass1M2 r5 = null;
        if (userJid == null) {
            Log.w("contact-mgr-db/cannot get verified name details by null jid");
            return null;
        }
        C16310on A01 = ((AbstractC21570xd) r0).A00.get();
        Cursor A03 = AbstractC21570xd.A03(A01, "wa_vnames", "jid = ?", null, "CONTACT_VNAMES", C21580xe.A0B, new String[]{userJid.getRawString()});
        if (A03.moveToNext()) {
            r5 = C32131bf.A00(A03);
        }
        A03.close();
        A01.close();
        return r5;
    }

    public void A01(UserJid userJid) {
        synchronized (this.A0C) {
            C21580xe r3 = this.A05;
            try {
                C16310on A02 = ((AbstractC21570xd) r3).A00.A02();
                try {
                    AnonymousClass1Lx A00 = A02.A00();
                    r3.A0H(A00, userJid);
                    A00.A00();
                    A00.close();
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IllegalArgumentException e) {
                StringBuilder sb = new StringBuilder("contact-mgr-db/unable to delete vname details ");
                sb.append(userJid);
                AnonymousClass009.A08(sb.toString(), e);
            }
        }
        this.A0D.remove(userJid);
        this.A04.A0I();
        this.A00.post(new RunnableBRunnable0Shape11S0200000_I1_1(this, 12, userJid));
    }

    public boolean A02(UserJid userJid) {
        AnonymousClass1M2 A00 = A00(userJid);
        return A00 != null && A00.A01();
    }

    public boolean A03(UserJid userJid, C32141bg r14, int i, boolean z) {
        boolean z2;
        synchronized (this.A0C) {
            AnonymousClass1M2 A00 = A00(userJid);
            z2 = false;
            int i2 = A00 != null ? A00.A03 : 0;
            long j = A00 != null ? A00.A04 : 0;
            ContentValues contentValues = new ContentValues();
            if (i2 != i) {
                contentValues.put("verified_level", Integer.valueOf(i));
            }
            if (r14 != null) {
                long j2 = r14.privacyModeTs;
                if (j < j2 || (j > 0 && j2 == 0)) {
                    contentValues.put("host_storage", Integer.valueOf(r14.hostStorage));
                    contentValues.put("actual_actors", Integer.valueOf(r14.actualActors));
                    contentValues.put("privacy_mode_ts", Long.valueOf(r14.privacyModeTs));
                }
            }
            if (contentValues.size() > 0) {
                z2 = true;
                C21580xe r3 = this.A05;
                try {
                    C16310on A02 = ((AbstractC21570xd) r3).A00.A02();
                    try {
                        AbstractC21570xd.A01(contentValues, A02, "wa_vnames", "jid = ?", new String[]{C15380n4.A03(userJid)});
                        A02.close();
                    } catch (Throwable th) {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IllegalArgumentException e) {
                    StringBuilder sb = new StringBuilder("wadbhelper/update-multi-fields/unable to update fields");
                    sb.append(userJid);
                    sb.append(", ");
                    sb.append(contentValues.toString());
                    AnonymousClass009.A08(sb.toString(), e);
                }
                r3.A02.A05(r3.A0C(userJid));
                if (z && i2 != i) {
                    this.A04.A0I();
                }
                this.A00.post(new RunnableBRunnable0Shape11S0200000_I1_1(this, 13, userJid));
            }
        }
        return z2;
    }

    public boolean A04(UserJid userJid, C32141bg r6, byte[] bArr, int i) {
        boolean z;
        synchronized (this.A0C) {
            A05(userJid, r6, bArr, i);
            AnonymousClass1M2 A00 = A00(userJid);
            AnonymousClass009.A05(A00);
            if (A00.A02 == 0) {
                this.A00.post(new RunnableBRunnable0Shape11S0200000_I1_1(this, 11, userJid));
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public final boolean A05(UserJid userJid, C32141bg r23, byte[] bArr, int i) {
        C32151bh r2;
        C16310on A02;
        String str;
        try {
            boolean z = false;
            try {
                r2 = (C32151bh) AbstractC27091Fz.A0E(C32151bh.A04, bArr);
            } catch (C28971Pt e) {
                Log.w("vname invalidproto:", e);
            } catch (IllegalArgumentException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("vname failed to get identity entry for jid = ");
                sb.append(userJid);
                Log.w(sb.toString(), e2);
            }
            if ((r2.A00 & 1) == 1) {
                C32161bi r22 = (C32161bi) AbstractC27091Fz.A0E(C32161bi.A06, r2.A01.A04());
                if (r22 != null) {
                    synchronized (this.A0C) {
                        AnonymousClass1M2 A00 = A00(userJid);
                        if (A00 == null || A00.A05 != r22.A02 || A00.A02 > 0) {
                            ArrayList arrayList = new ArrayList();
                            for (C32181bk r7 : r22.A03) {
                                if (!TextUtils.isEmpty(r7.A02)) {
                                    String str2 = r7.A02;
                                    if (!TextUtils.isEmpty(r7.A01)) {
                                        str = r7.A01;
                                    } else {
                                        str = "";
                                    }
                                    arrayList.add(new AnonymousClass01T(new Locale(str2, str), r7.A03));
                                }
                            }
                            C21580xe r72 = this.A05;
                            long j = r22.A02;
                            String str3 = r22.A04;
                            String str4 = r22.A05;
                            ArrayList arrayList2 = null;
                            try {
                                A02 = ((AbstractC21570xd) r72).A00.A02();
                            } catch (IllegalArgumentException e3) {
                                StringBuilder sb2 = new StringBuilder("contact-mgr-db/unable to store vname details ");
                                sb2.append(userJid);
                                AnonymousClass009.A08(sb2.toString(), e3);
                            }
                            try {
                                AnonymousClass1Lx A01 = A02.A01();
                                r72.A0H(A01, userJid);
                                String A03 = C15380n4.A03(userJid);
                                boolean z2 = false;
                                int i2 = 7;
                                if (r23 != null) {
                                    z2 = true;
                                    i2 = 10;
                                }
                                ContentValues contentValues = new ContentValues(i2);
                                contentValues.put("jid", A03);
                                contentValues.put("serial", Long.valueOf(j));
                                contentValues.put("issuer", str3);
                                contentValues.put("verified_name", str4);
                                contentValues.put("verified_level", Integer.valueOf(i));
                                contentValues.put("cert_blob", (byte[]) null);
                                contentValues.put("identity_unconfirmed_since", (Long) null);
                                if (z2) {
                                    contentValues.put("host_storage", Integer.valueOf(r23.hostStorage));
                                    contentValues.put("actual_actors", Integer.valueOf(r23.actualActors));
                                    contentValues.put("privacy_mode_ts", Long.valueOf(r23.privacyModeTs));
                                }
                                AbstractC21570xd.A00(contentValues, A02, "wa_vnames");
                                contentValues.clear();
                                Iterator it = arrayList.iterator();
                                while (it.hasNext()) {
                                    AnonymousClass01T r14 = (AnonymousClass01T) it.next();
                                    contentValues.put("jid", A03);
                                    Locale locale = (Locale) r14.A00;
                                    AnonymousClass009.A05(locale);
                                    contentValues.put("lg", locale.getLanguage());
                                    contentValues.put("lc", locale.getCountry());
                                    contentValues.put("verified_name", (String) r14.A01);
                                    AbstractC21570xd.A00(contentValues, A02, "wa_vnames_localized");
                                }
                                if (userJid != null) {
                                    arrayList2 = r72.A0C(userJid);
                                }
                                A01.A00();
                                A01.close();
                                A02.close();
                                if (arrayList2 != null && !arrayList2.isEmpty()) {
                                    r72.A02.A05(arrayList2);
                                }
                                z = true;
                            } catch (Throwable th) {
                                try {
                                    A02.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        } else {
                            z = A03(userJid, r23, i, false) | false;
                        }
                    }
                    this.A0D.put(userJid, Long.valueOf(System.currentTimeMillis()));
                    C32121be r1 = this.A03;
                    new C32171bj(userJid);
                    r1.A05();
                    return z;
                }
            }
            StringBuilder sb3 = new StringBuilder("vname certificate details could no be found or validated for jid ");
            sb3.append(userJid);
            Log.w(sb3.toString());
            this.A0D.put(userJid, Long.valueOf(System.currentTimeMillis()));
            C32121be r1 = this.A03;
            new C32171bj(userJid);
            r1.A05();
            return z;
        } catch (Throwable th2) {
            StringBuilder sb4 = new StringBuilder("vname certificate details could no be found or validated for jid ");
            sb4.append(userJid);
            Log.w(sb4.toString());
            this.A0D.put(userJid, Long.valueOf(System.currentTimeMillis()));
            C32121be r12 = this.A03;
            new C32171bj(userJid);
            r12.A05();
            throw th2;
        }
    }
}
