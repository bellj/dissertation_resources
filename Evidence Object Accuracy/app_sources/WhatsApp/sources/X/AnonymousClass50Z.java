package X;

/* renamed from: X.50Z  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50Z implements AnonymousClass5XU {
    public final AnonymousClass4DV A00;
    public final AbstractC117135Yq A01;
    public final AnonymousClass4DX A02;

    public AnonymousClass50Z(AnonymousClass4DV r1, AbstractC117135Yq r2, AnonymousClass4DX r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5XU
    public final int Agk(Object obj) {
        return ((AbstractC80173rp) obj).zzb.hashCode();
    }

    @Override // X.AnonymousClass5XU
    public final Object Agm() {
        return ((AnonymousClass50X) ((AbstractC80173rp) this.A01).A06(5)).AhL();
    }

    @Override // X.AnonymousClass5XU
    public final boolean Agt(Object obj, Object obj2) {
        return ((AbstractC80173rp) obj).zzb.equals(((AbstractC80173rp) obj2).zzb);
    }

    @Override // X.AnonymousClass5XU
    public final void Agy(AbstractC115295Qy r2, Object obj) {
        throw C12980iv.A0n("zzc");
    }

    @Override // X.AnonymousClass5XU
    public final void Agz(C93624aT r3, Object obj, byte[] bArr, int i, int i2) {
        AbstractC80173rp r4 = (AbstractC80173rp) obj;
        if (r4.zzb == C94994cs.A05) {
            r4.zzb = C94994cs.A00();
        }
        throw C12980iv.A0n("zza");
    }

    @Override // X.AnonymousClass5XU
    public final int Ah2(Object obj) {
        C94994cs r7 = ((AbstractC80173rp) obj).zzb;
        int i = r7.A01;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < r7.A00; i2++) {
                int A07 = 2 + 1 + C72453ed.A07(r7.A03[i2] >>> 3);
                int A04 = AnonymousClass4DU.A04(3);
                int A02 = ((AbstractC111925Bi) r7.A04[i2]).A02();
                i += A07 + A04 + C72453ed.A07(A02) + A02;
            }
            r7.A01 = i;
        }
        return i;
    }

    @Override // X.AnonymousClass5XU
    public final void AhA(Object obj, Object obj2) {
        C95684eB.A0S(obj, obj2);
    }

    @Override // X.AnonymousClass5XU
    public final void AhF(Object obj) {
        ((AbstractC80173rp) obj).zzb.A02 = false;
        throw C12980iv.A0n("zzc");
    }

    @Override // X.AnonymousClass5XU
    public final boolean AhJ(Object obj) {
        throw C12980iv.A0n("zzc");
    }
}
