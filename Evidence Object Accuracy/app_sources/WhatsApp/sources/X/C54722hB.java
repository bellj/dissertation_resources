package X;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2hB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54722hB extends AbstractC05270Ox {
    public final /* synthetic */ AbstractActivityC59392ue A00;

    public C54722hB(AbstractActivityC59392ue r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (linearLayoutManager != null) {
            if (C12980iv.A0A(linearLayoutManager) <= 4) {
                AbstractActivityC59392ue r0 = this.A00;
                C53802fF r2 = r0.A0E;
                UserJid userJid = r0.A0K;
                String str = r0.A0N;
                boolean A1V = C12980iv.A1V(r0.A00, -1);
                AnonymousClass19T r3 = r2.A02;
                int i3 = r2.A00;
                int A03 = C12980iv.A03(r3.A08.A0F(userJid) ? 1 : 0) * 9;
                if (str.equals("catalog_products_all_items_collection_id")) {
                    r3.A05(userJid, i3, A03, true);
                } else {
                    r3.A06(userJid, str, i3, A03, A1V);
                }
            }
            AbstractActivityC59392ue.A02(this.A00);
        }
    }
}
