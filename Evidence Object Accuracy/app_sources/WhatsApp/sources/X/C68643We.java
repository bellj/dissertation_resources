package X;

/* renamed from: X.3We  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68643We implements AbstractC16010oI {
    public final int A00;
    public final AnonymousClass31D A01;
    public final C238613h A02;
    public final C22170ye A03;
    public final C20660w7 A04;
    public final AnonymousClass2LM A05;
    public final C28941Pp A06;
    public final boolean A07;

    public C68643We(AnonymousClass31D r1, C238613h r2, C22170ye r3, C20660w7 r4, AnonymousClass2LM r5, C28941Pp r6, int i, boolean z) {
        this.A00 = i;
        this.A04 = r4;
        this.A07 = z;
        this.A03 = r3;
        this.A01 = r1;
        this.A06 = r6;
        this.A02 = r2;
        this.A05 = r5;
    }

    @Override // X.AbstractC16010oI
    public void AI3(byte[] bArr) {
        AnonymousClass2LM r1 = this.A05;
        if (r1 != null) {
            r1.A02 = true;
        }
        if (bArr != null && bArr.length != 0) {
            AnonymousClass31D r12 = this.A01;
            r12.A00 = Boolean.TRUE;
            r12.A06 = 12;
            C28941Pp r13 = this.A06;
            int i = this.A00;
            AnonymousClass1IS r3 = r13.A0C;
            if (r3 == null) {
                r3 = r13.A0i;
            }
            C30361Xc r2 = new C30361Xc(r3, bArr, i, r13.A0f);
            r13.A04(r2);
            this.A02.A00(r2, r13);
        } else if (this.A07) {
            this.A03.A08(this.A06);
        }
    }
}
