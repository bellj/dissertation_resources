package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2Bg  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Bg implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100194lW();
    public final long A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass2Bg(Parcel parcel) {
        this.A00 = parcel.readLong();
        this.A01 = parcel.readString();
    }

    public AnonymousClass2Bg(String str, long j) {
        this.A00 = j;
        this.A01 = str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
        parcel.writeString(this.A01);
    }
}
