package X;

import android.net.Uri;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: X.3Hx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65043Hx {
    public static String A00(String str) {
        try {
            String host = new URL(str).getHost();
            Uri parse = Uri.parse(C33771f3.A03(str, C33771f3.A03));
            if (!(AnonymousClass3JH.A03(parse, AnonymousClass3JH.A04) || (AnonymousClass3JH.A03(parse, AnonymousClass3JH.A03) && "1".equalsIgnoreCase(parse.getQueryParameter("fw"))))) {
                return host;
            }
            StringBuilder A0j = C12960it.A0j(host);
            A0j.append("/watch");
            return A0j.toString();
        } catch (MalformedURLException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0056, code lost:
        if (r1 != null) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01(X.AbstractC15340mz r5, X.AnonymousClass19O r6, boolean r7) {
        /*
            r2 = 0
            if (r7 == 0) goto L_0x0059
            X.1c2 r0 = r5.A0N
            r3 = 1
            if (r0 == 0) goto L_0x000d
            int r0 = r0.A01
            if (r0 != r3) goto L_0x000d
        L_0x000c:
            return r3
        L_0x000d:
            X.0lJ r4 = r5.A0T
            if (r4 == 0) goto L_0x0059
            java.lang.String r1 = r4.A04
            if (r1 != 0) goto L_0x0019
            java.lang.String r0 = r4.A06
            if (r0 == 0) goto L_0x0059
        L_0x0019:
            int r0 = r4.A01
            if (r0 <= 0) goto L_0x0059
            int r0 = r4.A00
            if (r0 <= 0) goto L_0x0059
            boolean r0 = r4.A08
            if (r0 != 0) goto L_0x002f
            X.1IS r0 = r5.A0z
            boolean r0 = r0.A02
            if (r0 == 0) goto L_0x000c
            boolean r0 = r5.A1B
            if (r0 != 0) goto L_0x000c
        L_0x002f:
            boolean r0 = r5 instanceof X.C28861Ph
            if (r0 == 0) goto L_0x0059
            X.1tc r0 = r6.A04
            X.0lG r0 = r0.A02
            if (r1 == 0) goto L_0x0054
            java.lang.String r1 = X.C003501n.A01(r1)
        L_0x003d:
            X.0lH r0 = r0.A04()
            java.io.File r0 = r0.A0Q
            X.C14330lG.A03(r0, r2)
            X.AnonymousClass009.A05(r1)
            java.io.File r0 = X.C14330lG.A00(r0, r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x000c
            return r2
        L_0x0054:
            java.lang.String r1 = r4.A06
            if (r1 == 0) goto L_0x0059
            goto L_0x003d
        L_0x0059:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65043Hx.A01(X.0mz, X.19O, boolean):boolean");
    }
}
