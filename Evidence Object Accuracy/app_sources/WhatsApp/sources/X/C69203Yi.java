package X;

import java.util.List;

/* renamed from: X.3Yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69203Yi implements AbstractC48542Gr {
    public final /* synthetic */ AnonymousClass3DS A00;
    public final /* synthetic */ C36051jF A01;

    @Override // X.AbstractC48542Gr
    public int AFu() {
        return 2;
    }

    public C69203Yi(AnonymousClass3DS r1, C36051jF r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC48542Gr
    public void AM4(long j) {
        C36051jF r1 = this.A01;
        AnonymousClass3DS r5 = this.A00;
        List list = r5.A09;
        AnonymousClass1Q5 r0 = r1.A01;
        r0.A07.AL3("inflated_rows", (String[]) list.toArray(new String[0]), r0.A06.A05);
        r5.A00 = true;
        list.clear();
    }
}
