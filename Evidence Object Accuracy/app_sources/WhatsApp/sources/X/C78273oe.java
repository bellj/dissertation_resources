package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78273oe extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99514kQ();
    public final int A00;
    public final int A01;

    public C78273oe(int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A07(parcel, 3, this.A01);
        C95654e8.A06(parcel, A00);
    }
}
