package X;

/* renamed from: X.2AR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AR extends Exception {
    public AnonymousClass2AR() {
    }

    public AnonymousClass2AR(String str) {
        super(str);
    }

    public AnonymousClass2AR(String str, Throwable th) {
        super("Having problems with local storage", th);
    }

    public AnonymousClass2AR(Throwable th) {
        super(th);
    }

    public static C84173yX A00(Throwable th) {
        return new C84173yX(th.getMessage(), -1);
    }
}
