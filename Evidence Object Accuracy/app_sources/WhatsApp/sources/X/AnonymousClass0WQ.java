package X;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.appcompat.widget.SearchView;
import com.whatsapp.R;

/* renamed from: X.0WQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WQ implements View.OnLayoutChangeListener {
    public final /* synthetic */ SearchView A00;

    public AnonymousClass0WQ(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9;
        int i10;
        SearchView searchView = this.A00;
        View view2 = searchView.A0Y;
        if (view2.getWidth() > 1) {
            Resources resources = searchView.getContext().getResources();
            int paddingLeft = searchView.A0a.getPaddingLeft();
            Rect rect = new Rect();
            boolean z = true;
            if (AnonymousClass028.A05(searchView) != 1) {
                z = false;
            }
            if (searchView.A0M) {
                i9 = resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_icon_width) + resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_text_padding_left);
            } else {
                i9 = 0;
            }
            SearchView.SearchAutoComplete searchAutoComplete = searchView.A0k;
            searchAutoComplete.getDropDownBackground().getPadding(rect);
            int i11 = rect.left;
            if (z) {
                i10 = -i11;
            } else {
                i10 = paddingLeft - (i11 + i9);
            }
            searchAutoComplete.setDropDownHorizontalOffset(i10);
            searchAutoComplete.setDropDownWidth((((view2.getWidth() + rect.left) + rect.right) + i9) - paddingLeft);
        }
    }
}
