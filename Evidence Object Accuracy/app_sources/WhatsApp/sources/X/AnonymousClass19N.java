package X;

import android.text.SpannableString;
import android.text.TextUtils;
import java.math.BigDecimal;

/* renamed from: X.19N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19N {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C16170oZ A03;
    public final AnonymousClass19Q A04;
    public final C14830m7 A05;
    public final C15890o4 A06;
    public final AnonymousClass018 A07;
    public final C15650ng A08;
    public final C19840ul A09;
    public final AnonymousClass17R A0A;
    public final AbstractC14440lR A0B;

    public AnonymousClass19N(C14330lG r1, C14900mE r2, C15570nT r3, C16170oZ r4, AnonymousClass19Q r5, C14830m7 r6, C15890o4 r7, AnonymousClass018 r8, C15650ng r9, C19840ul r10, AnonymousClass17R r11, AbstractC14440lR r12) {
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A0B = r12;
        this.A00 = r1;
        this.A03 = r4;
        this.A09 = r10;
        this.A0A = r11;
        this.A07 = r8;
        this.A08 = r9;
        this.A06 = r7;
        this.A04 = r5;
    }

    public static String A00(int i, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_");
        sb.append(i);
        return sb.toString();
    }

    public SpannableString A01(AnonymousClass1XV r8) {
        String str;
        if (r8.A0A == null || TextUtils.isEmpty(r8.A03)) {
            str = r8.A04;
            if (str == null) {
                return null;
            }
        } else {
            C30711Yn r5 = new C30711Yn(r8.A03);
            BigDecimal bigDecimal = r8.A0B;
            BigDecimal bigDecimal2 = r8.A0A;
            AnonymousClass018 r2 = this.A07;
            str = r5.A03(r2, bigDecimal2, true);
            if (!(bigDecimal == null || BigDecimal.ZERO.compareTo(bigDecimal) == 0 || bigDecimal.compareTo(bigDecimal2) >= 0)) {
                return C65003Ht.A02(str, r5.A03(r2, bigDecimal, true));
            }
        }
        return new SpannableString(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.ActivityC13810kN r25, X.C37071lG r26, X.AbstractC14640lm r27, com.whatsapp.jid.UserJid r28, java.util.List r29, int r30, int r31, long r32) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19N.A02(X.0kN, X.1lG, X.0lm, com.whatsapp.jid.UserJid, java.util.List, int, int, long):void");
    }
}
