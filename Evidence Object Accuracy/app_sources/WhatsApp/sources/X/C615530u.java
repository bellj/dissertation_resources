package X;

/* renamed from: X.30u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615530u extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public String A05;
    public String A06;

    public C615530u() {
        super(2952, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A05);
        r3.Abe(5, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(10, this.A04);
        r3.Abe(9, this.A00);
        r3.Abe(8, this.A01);
        r3.Abe(3, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamBusinessMessageLevelCsat {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessJid", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "csatActionType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "csatEntryPoint", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "csatUserRating", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasCsatRating", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isCsatRatingChanged", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageTemplateId", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
