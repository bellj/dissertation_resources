package X;

/* renamed from: X.30h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614230h extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public String A04;

    public C614230h() {
        super(2136, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(6, this.A04);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDeepLinkOpen {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkOpenFrom", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkSessionId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isContact", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "linkOwnerType", C12960it.A0Y(this.A03));
        return C12960it.A0d("}", A0k);
    }
}
