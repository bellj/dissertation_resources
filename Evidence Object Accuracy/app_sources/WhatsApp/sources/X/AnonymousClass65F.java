package X;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.payments.ui.stepup.NoviTextInputQuestionRow;

/* renamed from: X.65F  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65F implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ EditText A00;
    public final /* synthetic */ NoviTextInputQuestionRow A01;
    public final /* synthetic */ C127455uW A02;
    public final /* synthetic */ C129445xj A03;

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }

    public AnonymousClass65F(EditText editText, NoviTextInputQuestionRow noviTextInputQuestionRow, C127455uW r3, C129445xj r4) {
        this.A01 = noviTextInputQuestionRow;
        this.A02 = r3;
        this.A00 = editText;
        this.A03 = r4;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        String charSequence = ((TextView) view).getText().toString();
        this.A02.A02 = charSequence;
        this.A00.setText(charSequence);
        this.A03.A00();
    }
}
