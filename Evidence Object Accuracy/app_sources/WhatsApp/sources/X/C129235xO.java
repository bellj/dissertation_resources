package X;

import java.util.ArrayList;

/* renamed from: X.5xO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129235xO {
    public final C14830m7 A00;
    public final C14850m9 A01;
    public final C17070qD A02;
    public final C130155yt A03;
    public final C130125yq A04;
    public final AnonymousClass61F A05;
    public final AnonymousClass61C A06;
    public final C22980zx A07;

    public C129235xO(C14830m7 r1, C14850m9 r2, C17070qD r3, C130155yt r4, C130125yq r5, AnonymousClass61F r6, AnonymousClass61C r7, C22980zx r8) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = r7;
    }

    public AnonymousClass017 A00() {
        AnonymousClass016 A0T = C12980iv.A0T();
        ArrayList A0l = C12960it.A0l();
        AnonymousClass61S.A03("action", "novi-get-methods", A0l);
        this.A03.A0B(new AbstractC136196Lo(A0T, this) { // from class: X.69u
            public final /* synthetic */ AnonymousClass016 A00;
            public final /* synthetic */ C129235xO A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r6) {
                Object obj;
                C129235xO r3 = this.A01;
                AnonymousClass016 r4 = this.A00;
                if (!r6.A06() || (obj = r6.A02) == null) {
                    C452120p r2 = r6.A00;
                    C1316663q r1 = r6.A01;
                    C130785zy r0 = new C130785zy(r2, null);
                    r0.A01 = r1;
                    r4.A0A(r0);
                    return;
                }
                ArrayList A07 = r3.A07.A07((AnonymousClass1V8) obj);
                C38051nR A00 = r3.A02.A00();
                if (A07 == null) {
                    A07 = C12960it.A0l();
                }
                A00.A04(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0027: INVOKE  
                      (r1v3 'A00' X.1nR)
                      (wrap: X.67c : 0x0024: CONSTRUCTOR  (r0v4 X.67c A[REMOVE]) = (r4v0 'r4' X.016) call: X.67c.<init>(X.016):void type: CONSTRUCTOR)
                      (r2v2 'A07' java.util.ArrayList)
                     type: VIRTUAL call: X.1nR.A04(X.20l, java.util.List):void in method: X.69u.AV8(X.5zy):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0024: CONSTRUCTOR  (r0v4 X.67c A[REMOVE]) = (r4v0 'r4' X.016) call: X.67c.<init>(X.016):void type: CONSTRUCTOR in method: X.69u.AV8(X.5zy):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 19 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.67c, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 25 more
                    */
                /*
                    this = this;
                    X.5xO r3 = r5.A01
                    X.016 r4 = r5.A00
                    boolean r0 = r6.A06()
                    if (r0 == 0) goto L_0x002b
                    java.lang.Object r1 = r6.A02
                    if (r1 == 0) goto L_0x002b
                    X.0zx r0 = r3.A07
                    X.1V8 r1 = (X.AnonymousClass1V8) r1
                    java.util.ArrayList r2 = r0.A07(r1)
                    X.0qD r0 = r3.A02
                    X.1nR r1 = r0.A00()
                    if (r2 != 0) goto L_0x0022
                    java.util.ArrayList r2 = X.C12960it.A0l()
                L_0x0022:
                    X.67c r0 = new X.67c
                    r0.<init>(r4)
                    r1.A04(r0, r2)
                    return
                L_0x002b:
                    r3 = 0
                    X.20p r2 = r6.A00
                    X.63q r1 = r6.A01
                    X.5zy r0 = new X.5zy
                    r0.<init>(r2, r3)
                    r0.A01 = r1
                    r4.A0A(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C1331869u.AV8(X.5zy):void");
            }
        }, C117305Zk.A0Q(A0l), "get", 3);
        return A0T;
    }
}
