package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0301000_I0;

/* renamed from: X.182  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass182 {
    public final AnonymousClass181 A00;
    public final C16120oU A01;
    public final AbstractC14440lR A02;

    public AnonymousClass182(AnonymousClass181 r1, C16120oU r2, AbstractC14440lR r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(AnonymousClass01E r8, int i) {
        AnonymousClass00E r4;
        if (r8 instanceof AbstractC33081dJ) {
            r4 = ((AbstractC33081dJ) r8).AGM();
        } else {
            r4 = AnonymousClass01V.A02;
        }
        if (r4.A04.nextInt(r4.A03 * 2) == 0) {
            this.A02.Ab6(new RunnableBRunnable0Shape0S0301000_I0(this, r8, r4, i, 1));
        }
    }
}
