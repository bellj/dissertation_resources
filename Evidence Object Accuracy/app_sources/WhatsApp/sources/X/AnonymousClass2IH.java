package X;

/* renamed from: X.2IH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2IH {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2IH(C48302Fl r1) {
        this.A00 = r1;
    }

    public AnonymousClass3EZ A00(ActivityC13790kL r9, C15580nU r10, int i) {
        C48302Fl r1 = this.A00;
        AnonymousClass2FL r12 = r1.A01;
        return new AnonymousClass3EZ((AnonymousClass2II) r12.A0S.get(), (AnonymousClass2IJ) r12.A0d.get(), (C15570nT) r1.A03.AAr.get(), r9, r10, i);
    }
}
