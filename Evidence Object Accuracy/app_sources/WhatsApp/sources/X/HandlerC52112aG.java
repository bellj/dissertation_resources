package X;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.io.IOException;

/* renamed from: X.2aG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52112aG extends Handler implements Runnable {
    public int A00;
    public AbstractC14100kr A01;
    public IOException A02;
    public Thread A03;
    public boolean A04;
    public final int A05;
    public final long A06;
    public final AbstractC116335Va A07;
    public volatile boolean A08;
    public final /* synthetic */ C93584aP A09;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC52112aG(Looper looper, AbstractC14100kr r2, AbstractC116335Va r3, C93584aP r4, int i, long j) {
        super(looper);
        this.A09 = r4;
        this.A07 = r3;
        this.A01 = r2;
        this.A05 = i;
        this.A06 = j;
    }

    public void A00(boolean z) {
        this.A08 = z;
        this.A02 = null;
        if (hasMessages(0)) {
            this.A04 = true;
            removeMessages(0);
            if (!z) {
                sendEmptyMessage(1);
                return;
            }
        } else {
            synchronized (this) {
                this.A04 = true;
                ((C67803Sy) this.A07).A0D = true;
                Thread thread = this.A03;
                if (thread != null) {
                    thread.interrupt();
                }
            }
            if (!z) {
                return;
            }
        }
        this.A09.A00 = null;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.A01.ARt(this.A07, elapsedRealtime, elapsedRealtime - this.A06, true);
        this.A01 = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00fc A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0158  */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r36) {
        /*
        // Method dump skipped, instructions count: 546
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC52112aG.handleMessage(android.os.Message):void");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ba A[Catch: all -> 0x0284, TryCatch #6 {all -> 0x02a5, blocks: (B:10:0x0029, B:11:0x002d, B:115:0x0223, B:117:0x0227, B:119:0x0231, B:120:0x0235, B:14:0x0033, B:16:0x0063, B:17:0x0066, B:19:0x007e, B:20:0x0082, B:23:0x008c, B:25:0x009e, B:27:0x00af, B:29:0x00ba, B:30:0x00bf, B:32:0x00c9, B:33:0x00ce, B:35:0x00d8, B:36:0x00dd, B:38:0x00e7, B:39:0x00f2, B:41:0x00fc, B:46:0x010d, B:49:0x0116, B:51:0x0124, B:53:0x0131, B:54:0x013a, B:56:0x0140, B:58:0x0145, B:59:0x015c, B:61:0x017a, B:63:0x0187, B:64:0x018b, B:65:0x018e, B:67:0x0192, B:69:0x0198, B:70:0x019c, B:72:0x01a0, B:75:0x01ad, B:79:0x01b8, B:82:0x01bd, B:84:0x01c1, B:87:0x01c9, B:88:0x01d1, B:89:0x01d3, B:91:0x01d7, B:94:0x01e6, B:96:0x01ea, B:97:0x01ec, B:101:0x01f5, B:102:0x01f6, B:104:0x0204, B:105:0x0208, B:107:0x020f, B:111:0x0216, B:112:0x0217, B:123:0x0240, B:124:0x0241, B:125:0x0242, B:126:0x0247, B:128:0x025e, B:130:0x026b, B:131:0x0270, B:132:0x0273, B:133:0x0283, B:76:0x01af, B:78:0x01b5, B:110:0x0214), top: B:175:0x0029 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c9 A[Catch: all -> 0x0284, TryCatch #6 {all -> 0x02a5, blocks: (B:10:0x0029, B:11:0x002d, B:115:0x0223, B:117:0x0227, B:119:0x0231, B:120:0x0235, B:14:0x0033, B:16:0x0063, B:17:0x0066, B:19:0x007e, B:20:0x0082, B:23:0x008c, B:25:0x009e, B:27:0x00af, B:29:0x00ba, B:30:0x00bf, B:32:0x00c9, B:33:0x00ce, B:35:0x00d8, B:36:0x00dd, B:38:0x00e7, B:39:0x00f2, B:41:0x00fc, B:46:0x010d, B:49:0x0116, B:51:0x0124, B:53:0x0131, B:54:0x013a, B:56:0x0140, B:58:0x0145, B:59:0x015c, B:61:0x017a, B:63:0x0187, B:64:0x018b, B:65:0x018e, B:67:0x0192, B:69:0x0198, B:70:0x019c, B:72:0x01a0, B:75:0x01ad, B:79:0x01b8, B:82:0x01bd, B:84:0x01c1, B:87:0x01c9, B:88:0x01d1, B:89:0x01d3, B:91:0x01d7, B:94:0x01e6, B:96:0x01ea, B:97:0x01ec, B:101:0x01f5, B:102:0x01f6, B:104:0x0204, B:105:0x0208, B:107:0x020f, B:111:0x0216, B:112:0x0217, B:123:0x0240, B:124:0x0241, B:125:0x0242, B:126:0x0247, B:128:0x025e, B:130:0x026b, B:131:0x0270, B:132:0x0273, B:133:0x0283, B:76:0x01af, B:78:0x01b5, B:110:0x0214), top: B:175:0x0029 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d8 A[Catch: all -> 0x0284, TryCatch #6 {all -> 0x02a5, blocks: (B:10:0x0029, B:11:0x002d, B:115:0x0223, B:117:0x0227, B:119:0x0231, B:120:0x0235, B:14:0x0033, B:16:0x0063, B:17:0x0066, B:19:0x007e, B:20:0x0082, B:23:0x008c, B:25:0x009e, B:27:0x00af, B:29:0x00ba, B:30:0x00bf, B:32:0x00c9, B:33:0x00ce, B:35:0x00d8, B:36:0x00dd, B:38:0x00e7, B:39:0x00f2, B:41:0x00fc, B:46:0x010d, B:49:0x0116, B:51:0x0124, B:53:0x0131, B:54:0x013a, B:56:0x0140, B:58:0x0145, B:59:0x015c, B:61:0x017a, B:63:0x0187, B:64:0x018b, B:65:0x018e, B:67:0x0192, B:69:0x0198, B:70:0x019c, B:72:0x01a0, B:75:0x01ad, B:79:0x01b8, B:82:0x01bd, B:84:0x01c1, B:87:0x01c9, B:88:0x01d1, B:89:0x01d3, B:91:0x01d7, B:94:0x01e6, B:96:0x01ea, B:97:0x01ec, B:101:0x01f5, B:102:0x01f6, B:104:0x0204, B:105:0x0208, B:107:0x020f, B:111:0x0216, B:112:0x0217, B:123:0x0240, B:124:0x0241, B:125:0x0242, B:126:0x0247, B:128:0x025e, B:130:0x026b, B:131:0x0270, B:132:0x0273, B:133:0x0283, B:76:0x01af, B:78:0x01b5, B:110:0x0214), top: B:175:0x0029 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e7 A[Catch: all -> 0x0284, TryCatch #6 {all -> 0x02a5, blocks: (B:10:0x0029, B:11:0x002d, B:115:0x0223, B:117:0x0227, B:119:0x0231, B:120:0x0235, B:14:0x0033, B:16:0x0063, B:17:0x0066, B:19:0x007e, B:20:0x0082, B:23:0x008c, B:25:0x009e, B:27:0x00af, B:29:0x00ba, B:30:0x00bf, B:32:0x00c9, B:33:0x00ce, B:35:0x00d8, B:36:0x00dd, B:38:0x00e7, B:39:0x00f2, B:41:0x00fc, B:46:0x010d, B:49:0x0116, B:51:0x0124, B:53:0x0131, B:54:0x013a, B:56:0x0140, B:58:0x0145, B:59:0x015c, B:61:0x017a, B:63:0x0187, B:64:0x018b, B:65:0x018e, B:67:0x0192, B:69:0x0198, B:70:0x019c, B:72:0x01a0, B:75:0x01ad, B:79:0x01b8, B:82:0x01bd, B:84:0x01c1, B:87:0x01c9, B:88:0x01d1, B:89:0x01d3, B:91:0x01d7, B:94:0x01e6, B:96:0x01ea, B:97:0x01ec, B:101:0x01f5, B:102:0x01f6, B:104:0x0204, B:105:0x0208, B:107:0x020f, B:111:0x0216, B:112:0x0217, B:123:0x0240, B:124:0x0241, B:125:0x0242, B:126:0x0247, B:128:0x025e, B:130:0x026b, B:131:0x0270, B:132:0x0273, B:133:0x0283, B:76:0x01af, B:78:0x01b5, B:110:0x0214), top: B:175:0x0029 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00fc A[Catch: all -> 0x0284, TryCatch #6 {all -> 0x02a5, blocks: (B:10:0x0029, B:11:0x002d, B:115:0x0223, B:117:0x0227, B:119:0x0231, B:120:0x0235, B:14:0x0033, B:16:0x0063, B:17:0x0066, B:19:0x007e, B:20:0x0082, B:23:0x008c, B:25:0x009e, B:27:0x00af, B:29:0x00ba, B:30:0x00bf, B:32:0x00c9, B:33:0x00ce, B:35:0x00d8, B:36:0x00dd, B:38:0x00e7, B:39:0x00f2, B:41:0x00fc, B:46:0x010d, B:49:0x0116, B:51:0x0124, B:53:0x0131, B:54:0x013a, B:56:0x0140, B:58:0x0145, B:59:0x015c, B:61:0x017a, B:63:0x0187, B:64:0x018b, B:65:0x018e, B:67:0x0192, B:69:0x0198, B:70:0x019c, B:72:0x01a0, B:75:0x01ad, B:79:0x01b8, B:82:0x01bd, B:84:0x01c1, B:87:0x01c9, B:88:0x01d1, B:89:0x01d3, B:91:0x01d7, B:94:0x01e6, B:96:0x01ea, B:97:0x01ec, B:101:0x01f5, B:102:0x01f6, B:104:0x0204, B:105:0x0208, B:107:0x020f, B:111:0x0216, B:112:0x0217, B:123:0x0240, B:124:0x0241, B:125:0x0242, B:126:0x0247, B:128:0x025e, B:130:0x026b, B:131:0x0270, B:132:0x0273, B:133:0x0283, B:76:0x01af, B:78:0x01b5, B:110:0x0214), top: B:175:0x0029 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x010a  */
    @Override // java.lang.Runnable
    public void run() {
        /*
        // Method dump skipped, instructions count: 778
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC52112aG.run():void");
    }
}
