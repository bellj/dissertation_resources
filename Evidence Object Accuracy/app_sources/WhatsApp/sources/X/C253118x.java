package X;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.18x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C253118x {
    public final C14900mE A00;
    public final AnonymousClass18U A01;
    public final AnonymousClass01d A02;
    public final C14820m6 A03;
    public final C22710zW A04;
    public final C17070qD A05;

    public C253118x(C14900mE r1, AnonymousClass18U r2, AnonymousClass01d r3, C14820m6 r4, C22710zW r5, C17070qD r6) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
    }

    public static SpannableString A00(Context context, String str) {
        SpannableString spannableString = new SpannableString(Html.fromHtml(str));
        URLSpan[] uRLSpanArr = (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                int spanStart = spannableString.getSpanStart(uRLSpan);
                int spanEnd = spannableString.getSpanEnd(uRLSpan);
                int spanFlags = spannableString.getSpanFlags(uRLSpan);
                spannableString.removeSpan(uRLSpan);
                spannableString.setSpan(new C52302aa(context, uRLSpan.getURL()), spanStart, spanEnd, spanFlags);
            }
        }
        return spannableString;
    }

    public SpannableString A01(Context context, String str, Runnable[] runnableArr, String[] strArr, String[] strArr2) {
        SpannableString spannableString = new SpannableString(Html.fromHtml(str));
        URLSpan[] uRLSpanArr = (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            int i = 0;
            for (URLSpan uRLSpan : uRLSpanArr) {
                if (strArr[i].equals(uRLSpan.getURL())) {
                    int spanStart = spannableString.getSpanStart(uRLSpan);
                    int spanEnd = spannableString.getSpanEnd(uRLSpan);
                    int spanFlags = spannableString.getSpanFlags(uRLSpan);
                    spannableString.removeSpan(uRLSpan);
                    C58272oQ r10 = new C58272oQ(context, this.A01, this.A00, this.A02, strArr2[i]);
                    r10.A02 = new AnonymousClass5TE(runnableArr, i) { // from class: X.52y
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ Runnable[] A01;

                        {
                            this.A01 = r1;
                            this.A00 = r2;
                        }

                        @Override // X.AnonymousClass5TE
                        public final void A7H() {
                            this.A01[this.A00].run();
                        }
                    };
                    spannableString.setSpan(r10, spanStart, spanEnd, spanFlags);
                }
                i++;
            }
        }
        return spannableString;
    }

    public SpannableStringBuilder A02(Context context, Runnable runnable, String str, String str2) {
        return A03(context, runnable, str, str2, R.color.link_color);
    }

    public SpannableStringBuilder A03(Context context, Runnable runnable, String str, String str2, int i) {
        Spanned fromHtml = Html.fromHtml(str);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if (str2.equals(uRLSpan.getURL())) {
                    int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                    int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                    int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                    spannableStringBuilder.removeSpan(uRLSpan);
                    spannableStringBuilder.setSpan(new C83623xX(context, this, runnable, i), spanStart, spanEnd, spanFlags);
                }
            }
        }
        return spannableStringBuilder;
    }

    public void A04(Context context, Spannable spannable) {
        try {
            Linkify.addLinks(spannable, 10);
            C33771f3.A06(spannable);
            C63263Ax.A00(spannable, this.A03.A0B());
            C17070qD r1 = this.A05;
            C22710zW r0 = this.A04;
            C63243Av.A00(spannable, r0, r1);
            C63253Aw.A00(spannable, r0, r1);
        } catch (Exception unused) {
        }
        ArrayList A05 = C42971wC.A05(spannable);
        if (A05 != null && !A05.isEmpty()) {
            Iterator it = A05.iterator();
            while (it.hasNext()) {
                URLSpan uRLSpan = (URLSpan) it.next();
                String url = uRLSpan.getURL();
                spannable.setSpan(new C58272oQ(context, this.A01, this.A00, this.A02, url), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), spannable.getSpanFlags(uRLSpan));
            }
            Iterator it2 = A05.iterator();
            while (it2.hasNext()) {
                spannable.removeSpan(it2.next());
            }
        }
    }
}
