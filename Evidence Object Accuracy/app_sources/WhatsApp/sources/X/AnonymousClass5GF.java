package X;

import java.security.SecureRandom;

/* renamed from: X.5GF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GF implements AnonymousClass5Wv {
    @Override // X.AnonymousClass5Wv
    public int A5m(byte[] bArr, int i) {
        int length = bArr.length;
        int i2 = length - i;
        while (i < length) {
            bArr[i] = 0;
            i++;
        }
        return i2;
    }

    @Override // X.AnonymousClass5Wv
    public void AIb(SecureRandom secureRandom) {
    }

    @Override // X.AnonymousClass5Wv
    public int AYq(byte[] bArr) {
        int length = bArr.length;
        while (length > 0 && bArr[length - 1] == 0) {
            length--;
        }
        return length - length;
    }
}
