package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128815wi {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C118065bD A01;

    public /* synthetic */ C128815wi(C118065bD r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public final void A00(C452120p r9, String str) {
        C1310661b A00;
        C118065bD r4 = this.A01;
        int i = this.A00;
        if (r9 != null) {
            C1310661b A04 = r4.A04();
            A04.A01 = "01";
            r4.A01.A0B(A04);
        } else if (!(TextUtils.isEmpty(str) || (A00 = C1310661b.A00(Uri.parse(str), "SCANNED_QR_CODE")) == null || A00.A09 == null)) {
            r4.A01.A0B(A00);
            if (A00.A05 == null) {
                C1329668y r7 = r4.A0B;
                synchronized (r7) {
                    try {
                        C18600si r6 = r7.A03;
                        JSONObject A0b = C117295Zj.A0b(r6);
                        A0b.put("signedQrCode", str);
                        A0b.put("signedQrCodeTs", r7.A00.A00());
                        C117295Zj.A1E(r6, A0b);
                    } catch (JSONException e) {
                        Log.w("PAY: IndiaUpiPaymentSharedPrefs storeQrSignature threw: ", e);
                    }
                }
            }
        }
        r4.A02.A0B(new C127175u4(0, i));
    }
}
