package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AI  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AI extends Enum {
    public static final /* synthetic */ AnonymousClass4AI[] A00;
    public static final AnonymousClass4AI A01;
    public static final AnonymousClass4AI A02;
    public static final AnonymousClass4AI A03;

    public static AnonymousClass4AI valueOf(String str) {
        return (AnonymousClass4AI) Enum.valueOf(AnonymousClass4AI.class, str);
    }

    public static AnonymousClass4AI[] values() {
        return (AnonymousClass4AI[]) A00.clone();
    }

    static {
        AnonymousClass4AI r4 = new AnonymousClass4AI("ON_START", 0);
        A03 = r4;
        AnonymousClass4AI r3 = new AnonymousClass4AI("ON_COMPLETION", 1);
        A01 = r3;
        AnonymousClass4AI r1 = new AnonymousClass4AI("ON_FAILURE", 2);
        A02 = r1;
        AnonymousClass4AI[] r0 = new AnonymousClass4AI[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4AI(String str, int i) {
    }
}
