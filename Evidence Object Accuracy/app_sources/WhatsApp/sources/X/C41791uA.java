package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.1uA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41791uA extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public C41791uA() {
        super(472, new AnonymousClass00E(1, 1000, SearchActionVerificationClientService.NOTIFICATION_ID), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(4, this.A00);
        r3.Abe(7, this.A01);
        r3.Abe(3, this.A05);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamUiAction {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "participantCount", this.A04);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sizeBucket", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uiActionChatType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uiActionT", this.A05);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uiActionType", obj3);
        sb.append("}");
        return sb.toString();
    }
}
