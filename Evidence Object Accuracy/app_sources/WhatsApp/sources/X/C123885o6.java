package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.5o6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123885o6 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractActivityC121475iK A00;

    public C123885o6(AbstractActivityC121475iK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AbstractActivityC121475iK r2 = this.A00;
        String A0U = C117295Zj.A0U(((ActivityC13790kL) r2).A01, ((ActivityC13790kL) r2).A05);
        String str = r2.A0V;
        if (str.equals("business")) {
            C123595nP r3 = r2.A0R;
            r3.A09(new C129115xC(null, null, r3, null, -1), null, A0U);
        } else if (!str.equals("personal")) {
            Log.e(C12960it.A0d(str, C12960it.A0k("PAY: DyiReportBaseActivity/dyiReportButtonContainer::onClick - This payment account type is not supported. PaymentAccount = ")));
        } else {
            PinBottomSheetDialogFragment A00 = C125035qZ.A00();
            FingerprintBottomSheet fingerprintBottomSheet = null;
            if (Build.VERSION.SDK_INT >= 23) {
                fingerprintBottomSheet = C117305Zk.A0D();
            }
            r2.A0R.A05(r2, fingerprintBottomSheet, new C121195hP(((ActivityC13790kL) r2).A01, ((ActivityC13790kL) r2).A05, r2.A0H, r2.A0N, this, A0U), A00, A0U, "DYIREPORT", r2.A0W);
        }
    }
}
