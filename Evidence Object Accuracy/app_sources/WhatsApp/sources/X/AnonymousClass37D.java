package X;

import com.whatsapp.TextEmojiLabel;
import java.lang.ref.WeakReference;

/* renamed from: X.37D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37D extends AbstractC16350or {
    public final C15610nY A00;
    public final C15370n3 A01;
    public final WeakReference A02;

    public AnonymousClass37D(TextEmojiLabel textEmojiLabel, C15610nY r3, C15370n3 r4) {
        this.A02 = C12970iu.A10(textEmojiLabel);
        this.A01 = r4;
        this.A00 = r3;
    }
}
