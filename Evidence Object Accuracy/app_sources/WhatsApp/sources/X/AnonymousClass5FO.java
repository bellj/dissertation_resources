package X;

/* renamed from: X.5FO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FO implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FO(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r4) {
        String obj2;
        Float f = (Float) obj;
        if (f.isInfinite()) {
            obj2 = "null";
        } else {
            obj2 = f.toString();
        }
        appendable.append(obj2);
    }
}
