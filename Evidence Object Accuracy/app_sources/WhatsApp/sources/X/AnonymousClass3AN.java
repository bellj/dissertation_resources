package X;

import android.text.TextUtils;
import android.util.Log;

/* renamed from: X.3AN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AN {
    public static long A00(AnonymousClass28D r5) {
        Object obj = r5.A02.get(35);
        long j = 0;
        if (obj != null) {
            if (obj instanceof String) {
                String str = (String) obj;
                if (!TextUtils.isEmpty(str)) {
                    try {
                        j = Long.parseLong(str);
                        return j;
                    } catch (NumberFormatException e) {
                        Log.e("WaRcCountDownTimer", C12960it.A0b("Invalid long value:", obj), e);
                        return j;
                    }
                }
            } else if (obj instanceof Number) {
                return C12980iv.A0G(obj);
            } else {
                throw C12970iu.A0f("Attempting to extract unrecognized type from countdown timer component");
            }
        }
        return 0;
    }
}
