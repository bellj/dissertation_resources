package X;

/* renamed from: X.4Pd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C90764Pd {
    public final AnonymousClass4D4 A00;
    public final C87824Dd A01;
    public final String A02;

    public C90764Pd(AnonymousClass4D4 r4, C87824Dd r5, String str) {
        String format;
        if (r4 == null || str == null) {
            throw C72453ed.A0h();
        }
        this.A00 = r4;
        this.A01 = r5;
        if (str.length() >= 10) {
            format = str.substring(0, 10);
        } else {
            format = String.format("%-10s", str);
        }
        this.A02 = format;
    }
}
