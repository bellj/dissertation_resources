package X;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService;

/* renamed from: X.3Sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67603Sd implements IAppDataReaderService {
    public IBinder A00;

    public C67603Sd(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService
    public ParcelFileDescriptor AD1() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService");
            C12990iw.A18(this.A00, obtain, obtain2, 1);
            return obtain2.readInt() != 0 ? (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(obtain2) : null;
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
