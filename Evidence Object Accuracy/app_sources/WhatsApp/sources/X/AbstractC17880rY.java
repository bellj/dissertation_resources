package X;

import android.view.View;

/* renamed from: X.0rY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17880rY {
    public final int A00;

    public abstract Object A00(C14260l7 v, AnonymousClass28D v2);

    public abstract void A01(View view, C14260l7 v, AnonymousClass28D v2, AnonymousClass28D v3);

    public abstract void A02(View view, C14260l7 v, AnonymousClass28D v2, AnonymousClass28D v3);

    public AbstractC17880rY(int i) {
        this.A00 = i;
    }
}
