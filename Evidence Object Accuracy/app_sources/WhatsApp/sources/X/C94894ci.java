package X;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/* renamed from: X.4ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94894ci {
    public static final /* synthetic */ AtomicIntegerFieldUpdater A01 = AtomicIntegerFieldUpdater.newUpdater(C94894ci.class, "_handled");
    public final Throwable A00;
    public volatile /* synthetic */ int _handled;

    public /* synthetic */ C94894ci(Throwable th) {
        this(th, false);
    }

    public C94894ci(Throwable th, boolean z) {
        this.A00 = th;
        this._handled = z ? 1 : 0;
    }

    public final boolean A00() {
        return A01.compareAndSet(this, 0, 1);
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C12980iv.A0r(this));
        A0h.append('[');
        A0h.append(this.A00);
        return C72453ed.A0t(A0h);
    }
}
