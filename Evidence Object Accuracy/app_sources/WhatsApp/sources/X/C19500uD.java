package X;

import com.whatsapp.util.Log;

/* renamed from: X.0uD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19500uD {
    public final AbstractC15710nm A00;
    public final C16240og A01;
    public final C16590pI A02;
    public final C19480uB A03;
    public final C17220qS A04;

    public C19500uD(AbstractC15710nm r1, C16240og r2, C16590pI r3, C19480uB r4, C17220qS r5) {
        this.A02 = r3;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0030, code lost:
        if (java.lang.Byte.parseByte(r22) < 0) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(java.lang.Runnable r21, java.lang.String r22, byte[] r23, byte[] r24) {
        /*
            r20 = this;
            r15 = r20
            X.0nm r3 = r15.A00
            r4 = r24
            X.C32781cj.A0B(r3, r4)
            r5 = r23
            int r2 = r5.length
            r0 = 32
            if (r2 == r0) goto L_0x0024
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r2 = r0.toString()
            r1 = 1
            java.lang.String r0 = "crypto-iq-incorrect-server-salt-size"
            r3.AaV(r0, r2, r1)
        L_0x0024:
            r6 = r22
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x0032
            byte r0 = java.lang.Byte.parseByte(r6)     // Catch: NumberFormatException -> 0x0032
            if (r0 >= 0) goto L_0x0038
        L_0x0032:
            r1 = 1
            java.lang.String r0 = "crypto-iq-incorrect-key-version"
            r3.AaV(r0, r6, r1)
        L_0x0038:
            java.lang.String r1 = "BackupSendMethods/sendGetCipherKey/v="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            X.0qS r3 = r15.A04
            java.lang.String r2 = r3.A01()
            r11 = 2
            X.1W9[] r12 = new X.AnonymousClass1W9[r11]
            java.lang.String r1 = "action"
            java.lang.String r10 = "get"
            X.1W9 r0 = new X.1W9
            r0.<init>(r1, r10)
            r14 = 0
            r12[r14] = r0
            java.lang.String r1 = "version"
            X.1W9 r0 = new X.1W9
            r0.<init>(r1, r6)
            r13 = 1
            r12[r13] = r0
            X.1V8[] r9 = new X.AnonymousClass1V8[r11]
            java.lang.String r1 = "google"
            r7 = 0
            X.1V8 r0 = new X.1V8
            r0.<init>(r1, r4, r7)
            r9[r14] = r0
            java.lang.String r1 = "code"
            X.1V8 r0 = new X.1V8
            r0.<init>(r1, r5, r7)
            r9[r13] = r0
            java.lang.String r0 = "crypto"
            X.1V8 r8 = new X.1V8
            r8.<init>(r0, r12, r9)
            r0 = 4
            X.1W9[] r7 = new X.AnonymousClass1W9[r0]
            X.1VY r9 = X.AnonymousClass1VY.A00
            java.lang.String r1 = "to"
            X.1W9 r0 = new X.1W9
            r0.<init>(r9, r1)
            r7[r14] = r0
            java.lang.String r9 = "xmlns"
            java.lang.String r1 = "urn:xmpp:whatsapp:account"
            X.1W9 r0 = new X.1W9
            r0.<init>(r9, r1)
            r7[r13] = r0
            java.lang.String r1 = "type"
            X.1W9 r0 = new X.1W9
            r0.<init>(r1, r10)
            r7[r11] = r0
            java.lang.String r0 = "id"
            X.1W9 r1 = new X.1W9
            r1.<init>(r0, r2)
            r0 = 3
            r7[r0] = r1
            java.lang.String r1 = "iq"
            X.1V8 r0 = new X.1V8
            r0.<init>(r8, r1, r7)
            r16 = r21
            r19 = r4
            r18 = r5
            r17 = r6
            X.1cl r14 = new X.1cl
            r14.<init>(r15, r16, r17, r18, r19)
            r7 = 75
            r8 = 32000(0x7d00, double:1.581E-319)
            r4 = r14
            r5 = r0
            r6 = r2
            r3.A0A(r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19500uD.A00(java.lang.Runnable, java.lang.String, byte[], byte[]):void");
    }

    public boolean A01(Runnable runnable, byte[] bArr, byte[] bArr2, int i) {
        if (!this.A01.A06) {
            return false;
        }
        Log.i("sendmethods/sendcreatecipherkey");
        AbstractC15710nm r3 = this.A00;
        C32781cj.A0B(r3, bArr);
        int length = bArr2.length;
        if (length != 16) {
            StringBuilder sb = new StringBuilder("");
            sb.append(length);
            r3.AaV("crypto-iq-incorrect-account-salt-size", sb.toString(), true);
        }
        C17220qS r14 = this.A04;
        String A01 = r14.A01();
        r14.A0A(new C32791ck(this, runnable, bArr, bArr2, i), new AnonymousClass1V8(new AnonymousClass1V8("crypto", new AnonymousClass1W9[]{new AnonymousClass1W9("action", "create")}, new AnonymousClass1V8[]{new AnonymousClass1V8("google", bArr, (AnonymousClass1W9[]) null)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "urn:xmpp:whatsapp:account"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("id", A01)}), A01, 74, 32000);
        return true;
    }
}
