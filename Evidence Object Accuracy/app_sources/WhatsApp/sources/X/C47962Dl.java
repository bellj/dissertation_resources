package X;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2Dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47962Dl {
    public static int A00(AbstractC15340mz r1) {
        if (r1 instanceof C30411Xh) {
            return 1;
        }
        if (r1 instanceof C30351Xb) {
            return ((C30351Xb) r1).A14().size();
        }
        if (!C30041Vv.A0r(r1)) {
            return 0;
        }
        C16440p1 r12 = (C16440p1) r1;
        C16150oX r0 = ((AbstractC16130oV) r12).A02;
        if (r0 != null) {
            return r0.A01;
        }
        return r12.A00;
    }

    public static String A01(Context context, AbstractC15340mz r3) {
        if (!C30041Vv.A0r(r3)) {
            return null;
        }
        String A16 = ((AbstractC16130oV) r3).A16();
        if (TextUtils.isEmpty(A16)) {
            return context.getString(R.string.conversations_most_recent_contact);
        }
        return C14350lI.A08(A16);
    }

    public static List A02(AbstractC15340mz r3, C26501Ds r4) {
        if (r3 instanceof C30411Xh) {
            return Collections.singletonList(((C30411Xh) r3).A14());
        }
        if (r3 instanceof C30351Xb) {
            return ((C30351Xb) r3).A14();
        }
        List list = null;
        if (C30041Vv.A0r(r3)) {
            C16150oX r0 = ((AbstractC16130oV) r3).A02;
            AnonymousClass009.A05(r0);
            File file = r0.A0F;
            if (file != null) {
                try {
                    list = C41391tS.A00(r4.A01(Uri.fromFile(file)));
                    return list;
                } catch (IOException e) {
                    Log.e("vcardloader/splitvcards/exception", e);
                }
            }
        }
        return list;
    }
}
