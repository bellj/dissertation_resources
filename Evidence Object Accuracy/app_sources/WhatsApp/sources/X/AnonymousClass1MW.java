package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.1MW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MW extends Exception {
    public AnonymousClass1MW(Jid jid) {
        super(jid.toString());
    }

    public AnonymousClass1MW(String str) {
        super(str);
    }
}
