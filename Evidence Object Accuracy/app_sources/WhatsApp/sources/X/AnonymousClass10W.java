package X;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: X.10W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10W {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C15570nT A01;
    public final C18850tA A02;
    public final C15550nR A03;
    public final AnonymousClass10S A04;
    public final C15680nj A05;
    public final C22410z2 A06;

    public AnonymousClass10W(C15570nT r3, C18850tA r4, C15550nR r5, AnonymousClass10S r6, C15680nj r7, C22410z2 r8) {
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A06 = r8;
    }

    @Deprecated
    public void A00(Collection collection) {
        C18850tA r1 = this.A02;
        if (r1.A0Z.A03()) {
            r1.A04.A08();
            return;
        }
        this.A01.A08();
        this.A03.A0X(collection);
        this.A06.A05(new ArrayList(collection));
    }
}
