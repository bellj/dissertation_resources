package X;

import android.graphics.PointF;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08130ad implements AbstractC12590iA {
    public final List A00;

    public C08130ad() {
        this.A00 = Collections.singletonList(new AnonymousClass0U8(new PointF(0.0f, 0.0f)));
    }

    public C08130ad(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC12590iA
    public AnonymousClass0QR A88() {
        List list = this.A00;
        if (((AnonymousClass0U8) list.get(0)).A02()) {
            return new AnonymousClass0H2(list);
        }
        return new AnonymousClass0Gy(list);
    }

    @Override // X.AbstractC12590iA
    public List ADl() {
        return this.A00;
    }

    @Override // X.AbstractC12590iA
    public boolean AK5() {
        List list = this.A00;
        return list.size() == 1 && ((AnonymousClass0U8) list.get(0)).A02();
    }
}
