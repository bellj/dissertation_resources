package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4iY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98354iY implements IInterface {
    public final IBinder A00;

    public C98354iY(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }

    public final void A00(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            C12990iw.A18(this.A00, parcel, obtain, i);
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
