package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87154Ak extends Enum {
    public static final /* synthetic */ EnumC87154Ak[] A00;
    public static final EnumC87154Ak A01;
    public final int value = 1;

    static {
        EnumC87154Ak r1 = new EnumC87154Ak();
        A01 = r1;
        A00 = new EnumC87154Ak[]{r1};
    }

    public static EnumC87154Ak valueOf(String str) {
        return (EnumC87154Ak) Enum.valueOf(EnumC87154Ak.class, str);
    }

    public static EnumC87154Ak[] values() {
        return (EnumC87154Ak[]) A00.clone();
    }
}
