package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57502nA extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57502nA A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01 = 0;
    public int A02;
    public C43261wh A03;
    public Object A04;
    public String A05 = "";

    static {
        C57502nA r0 = new C57502nA();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C81603uH r1;
        boolean z = false;
        switch (r8.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C57502nA r10 = (C57502nA) obj2;
                boolean z2 = true;
                if ((this.A00 & 1) != 1) {
                    z2 = false;
                }
                String str = this.A05;
                boolean z3 = true;
                if ((r10.A00 & 1) != 1) {
                    z3 = false;
                }
                this.A05 = r9.Afy(str, r10.A05, z2, z3);
                this.A03 = (C43261wh) r9.Aft(this.A03, r10.A03);
                this.A02 = r9.Afp(this.A02, r10.A02, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r10.A00 & 8, 8));
                int i = r10.A01;
                switch ((i != 0 ? i != 2 ? null : EnumC87204Ap.A02 : EnumC87204Ap.A01).ordinal()) {
                    case 0:
                        if (this.A01 == 2) {
                            z = true;
                        }
                        this.A04 = r9.Afx(this.A04, r10.A04, z);
                        break;
                    case 1:
                        if (this.A01 != 0) {
                            z = true;
                        }
                        r9.Afw(z);
                        break;
                }
                if (r9 == C463025i.A00) {
                    int i2 = r10.A01;
                    if (i2 != 0) {
                        this.A01 = i2;
                    }
                    this.A00 |= r10.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r92.A0A();
                            this.A00 |= 1;
                            this.A05 = A0A;
                        } else if (A03 == 18) {
                            String A0A2 = r92.A0A();
                            this.A01 = 2;
                            this.A04 = A0A2;
                        } else if (A03 == 26) {
                            if ((this.A00 & 4) == 4) {
                                r1 = (C81603uH) this.A03.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r92, r102, C43261wh.A0O);
                            this.A03 = r0;
                            if (r1 != null) {
                                this.A03 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 4;
                        } else if (A03 == 32) {
                            int A02 = r92.A02();
                            if (A02 == 0 || A02 == 1) {
                                this.A00 |= 8;
                                this.A02 = A02;
                            } else {
                                super.A0X(4, A02);
                            }
                        } else if (!A0a(r92, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57502nA();
            case 5:
                return new C81803ub();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57502nA.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A05, 0);
        }
        if (this.A01 == 2) {
            i2 = AbstractC27091Fz.A04(2, (String) this.A04, i2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A03;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 3, i2);
        }
        if ((this.A00 & 8) == 8) {
            i2 = AbstractC27091Fz.A03(4, this.A02, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A05);
        }
        if (this.A01 == 2) {
            codedOutputStream.A0I(2, (String) this.A04);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A03;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
