package X;

import com.whatsapp.util.Log;

/* renamed from: X.1H6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H6 extends AbstractC250017s {
    public final C15570nT A00;
    public final C233311g A01;
    public final C14830m7 A02;

    public AnonymousClass1H6(C15570nT r1, C233311g r2, C14830m7 r3, C233511i r4) {
        super(r4);
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A08(C34461gB r8, C34461gB r9) {
        if (r9 == null || r9.A00 <= r8.A00) {
            C233311g r3 = this.A01;
            int i = r8.A00;
            StringBuilder sb = new StringBuilder("SyncdKeyManager/expireKeysWithEpochIfActive expiredKeyEpoch = ");
            sb.append(i);
            Log.i(sb.toString());
            AnonymousClass16W r1 = r3.A08;
            AnonymousClass1JZ A01 = r1.A01();
            if (A01 != null) {
                AnonymousClass1JR r6 = A01.A01;
                if (r6.A01() <= i) {
                    C16310on A02 = r1.A00.A02();
                    try {
                        A02.A03.A0C("UPDATE crypto_info SET timestamp = 0  WHERE device_id = ?  AND epoch = ? ", new String[]{String.valueOf(r6.A00()), String.valueOf(r6.A01())});
                        A02.close();
                    } catch (Throwable th) {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            A07(r8, r9);
            return;
        }
        A05(r8);
    }
}
