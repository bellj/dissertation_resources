package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3gh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73723gh extends View implements AnonymousClass5ST {
    public float A00 = 0.08f;
    public float A01 = 0.0533f;
    public C95274dQ A02 = C95274dQ.A06;
    public List A03 = Collections.emptyList();
    public final List A04 = C12960it.A0l();

    public C73723gh(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // X.AnonymousClass5ST
    public void AfK(C95274dQ r4, List list, float f, float f2, int i) {
        this.A03 = list;
        this.A02 = r4;
        this.A01 = f;
        this.A00 = f2;
        while (true) {
            List list2 = this.A04;
            if (list2.size() < list.size()) {
                list2.add(new AnonymousClass4UL(getContext()));
            } else {
                invalidate();
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009d, code lost:
        if (r4 != 2) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x017c, code lost:
        if (r16 == false) goto L_0x0499;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01ff  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0285  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x03e9  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0409  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0447  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x048a  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x04a7  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x04b7  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x01fb A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0184 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01ca  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatchDraw(android.graphics.Canvas r41) {
        /*
        // Method dump skipped, instructions count: 1224
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C73723gh.dispatchDraw(android.graphics.Canvas):void");
    }
}
