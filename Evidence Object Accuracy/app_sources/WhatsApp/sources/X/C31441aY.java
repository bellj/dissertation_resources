package X;

import java.text.ParseException;

/* renamed from: X.1aY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31441aY implements AbstractC31371aR {
    public final int A00;
    public final int A01;
    public final byte[] A02;
    public final byte[] A03;

    @Override // X.AbstractC31371aR
    public int getType() {
        return 4;
    }

    public C31441aY(C31721b0 r12, byte[] bArr, int i, int i2) {
        byte[] bArr2 = {(byte) 51};
        AnonymousClass1G4 A0T = AnonymousClass25G.A04.A0T();
        A0T.A03();
        AnonymousClass25G r1 = (AnonymousClass25G) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        A0T.A03();
        AnonymousClass25G r13 = (AnonymousClass25G) A0T.A00;
        r13.A00 |= 2;
        r13.A02 = i2;
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        AnonymousClass25G r14 = (AnonymousClass25G) A0T.A00;
        r14.A00 |= 4;
        r14.A03 = A01;
        byte[] A02 = A0T.A02().A02();
        byte[] A00 = C31241aE.A00(bArr2, A02);
        try {
            C32001bS A002 = C32001bS.A00();
            byte[] bArr3 = r12.A00;
            if (bArr3 == null || bArr3.length != 32) {
                throw new IllegalArgumentException("Invalid private key length!");
            }
            AnonymousClass5XJ r15 = A002.A00;
            this.A03 = C31241aE.A00(bArr2, A02, r15.calculateSignature(r15.AG4(64), bArr3, A00));
            this.A01 = i;
            this.A00 = i2;
            this.A02 = bArr;
        } catch (C31561ak e) {
            throw new AssertionError(e);
        }
    }

    public C31441aY(byte[] bArr) {
        try {
            byte[][] A02 = C31241aE.A02(bArr, 1, (bArr.length - 1) - 64, 64);
            byte b = A02[0][0];
            byte[] bArr2 = A02[1];
            int i = (b & 255) >> 4;
            if (i < 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("Legacy message: ");
                sb.append(i);
                throw new C31571al(sb.toString());
            } else if (i <= 3) {
                AnonymousClass25G r3 = (AnonymousClass25G) AbstractC27091Fz.A0E(AnonymousClass25G.A04, bArr2);
                int i2 = r3.A00;
                if ((i2 & 1) == 1 && (i2 & 2) == 2 && (i2 & 4) == 4) {
                    this.A03 = bArr;
                    this.A01 = r3.A01;
                    this.A00 = r3.A02;
                    this.A02 = r3.A03.A04();
                    return;
                }
                throw new C31521ag("Incomplete message.");
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown version: ");
                sb2.append(i);
                throw new C31521ag(sb2.toString());
            }
        } catch (C28971Pt | ParseException e) {
            throw new C31521ag(e);
        }
    }

    public void A00(C31491ad r5) {
        try {
            byte[] bArr = this.A03;
            byte[][] A01 = C31241aE.A01(bArr, bArr.length - 64, 64);
            if (!C32001bS.A00().A02(r5.A00, A01[0], A01[1])) {
                throw new C31521ag("Invalid signature!");
            }
        } catch (C31561ak e) {
            throw new C31521ag(e);
        }
    }

    @Override // X.AbstractC31371aR
    public byte[] Abf() {
        return this.A03;
    }
}
