package X;

/* renamed from: X.4ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105934ur implements AbstractC12930in {
    public C08870bz A00;
    public final int A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r4 > ((X.AnonymousClass5XR) r3.A04()).AGm()) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C105934ur(X.C08870bz r3, int r4) {
        /*
            r2 = this;
            r2.<init>()
            if (r4 < 0) goto L_0x0012
            java.lang.Object r0 = r3.A04()
            X.5XR r0 = (X.AnonymousClass5XR) r0
            int r1 = r0.AGm()
            r0 = 1
            if (r4 <= r1) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            X.AnonymousClass0RA.A00(r0)
            X.0bz r0 = r3.clone()
            r2.A00 = r0
            r2.A01 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C105934ur.<init>(X.0bz, int):void");
    }

    public synchronized void A00() {
        if (isClosed()) {
            throw new C10770fB();
        }
    }

    @Override // X.AbstractC12930in
    public synchronized byte AZm(int i) {
        A00();
        boolean z = true;
        AnonymousClass0RA.A00(C12990iw.A1W(i));
        if (i >= this.A01) {
            z = false;
        }
        AnonymousClass0RA.A00(z);
        return ((AnonymousClass5XR) this.A00.A04()).AZm(i);
    }

    @Override // X.AbstractC12930in
    public synchronized int AZr(byte[] bArr, int i, int i2, int i3) {
        A00();
        boolean z = false;
        if (i + i3 <= this.A01) {
            z = true;
        }
        AnonymousClass0RA.A00(z);
        return ((AnonymousClass5XR) this.A00.A04()).AZr(bArr, i, i2, i3);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        C08870bz r0 = this.A00;
        if (r0 != null) {
            r0.close();
        }
        this.A00 = null;
    }

    @Override // X.AbstractC12930in
    public synchronized boolean isClosed() {
        return !C08870bz.A01(this.A00);
    }

    @Override // X.AbstractC12930in
    public synchronized int size() {
        A00();
        return this.A01;
    }
}
