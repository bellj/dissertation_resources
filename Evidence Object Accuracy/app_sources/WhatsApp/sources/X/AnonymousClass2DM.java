package X;

import java.util.List;

/* renamed from: X.2DM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2DM {
    public final List A00;

    public AnonymousClass2DM(List list) {
        this.A00 = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AxolotlSessionEvent{jidList='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
