package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3mu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77203mu extends AbstractC76783mE {
    public final C95394dd A00 = new C95394dd();
    public final C95304dT A01 = new C95304dT();

    public C77203mu() {
        super("WebvttDecoder");
    }

    @Override // X.AbstractC76783mE
    public AbstractC116805Wy A07(byte[] bArr, int i, boolean z) {
        char c;
        int i2;
        String A0L;
        AnonymousClass4PC A01;
        C95304dT r0 = this.A01;
        r0.A0U(bArr, i);
        ArrayList A0l = C12960it.A0l();
        try {
            int i3 = r0.A01;
            String A0L2 = r0.A0L();
            if (A0L2 == null || !A0L2.startsWith("WEBVTT")) {
                r0.A0S(i3);
                throw AnonymousClass496.A00(C12960it.A0d(r0.A0L(), C12960it.A0k("Expected WEBVTT. Got ")));
            }
            do {
            } while (!TextUtils.isEmpty(r0.A0L()));
            ArrayList A0l2 = C12960it.A0l();
            while (true) {
                int i4 = r0.A01;
                String A0L3 = r0.A0L();
                if (A0L3 == null) {
                    c = 0;
                } else if ("STYLE".equals(A0L3)) {
                    c = 2;
                } else {
                    boolean startsWith = A0L3.startsWith("NOTE");
                    c = 3;
                    if (startsWith) {
                        c = 1;
                    }
                }
                r0.A0S(i4);
                if (c == 0) {
                    return new C107754xt(A0l2);
                }
                if (c == 1) {
                    do {
                    } while (!TextUtils.isEmpty(r0.A0L()));
                } else if (c == 2) {
                    if (A0l2.isEmpty()) {
                        r0.A0L();
                        C95394dd r1 = this.A00;
                        StringBuilder sb = r1.A01;
                        sb.setLength(0);
                        int i5 = r0.A01;
                        do {
                        } while (!TextUtils.isEmpty(r0.A0L()));
                        C95304dT r7 = r1.A00;
                        r7.A0U(r0.A02, r0.A01);
                        r7.A0S(i5);
                        ArrayList A0l3 = C12960it.A0l();
                        while (true) {
                            C95394dd.A02(r7);
                            String str = null;
                            if (C95304dT.A00(r7) < 5 || !"::cue".equals(r7.A0O(5))) {
                                break;
                            }
                            int i6 = r7.A01;
                            String A012 = C95394dd.A01(r7, sb);
                            if (A012 != null) {
                                if (!"{".equals(A012)) {
                                    if ("(".equals(A012)) {
                                        int i7 = r7.A01;
                                        int i8 = r7.A00;
                                        boolean z2 = false;
                                        while (i7 < i8 && !z2) {
                                            z2 = C12960it.A1V((char) r7.A02[i7], 41);
                                            i7++;
                                        }
                                        str = r7.A0O((i7 - 1) - i7).trim();
                                    }
                                    if (")".equals(C95394dd.A01(r7, sb))) {
                                        if (str == null) {
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                } else {
                                    r7.A0S(i6);
                                    str = "";
                                }
                                if (!"{".equals(C95394dd.A01(r7, sb))) {
                                    break;
                                }
                                AnonymousClass4UD r8 = new AnonymousClass4UD();
                                if (!"".equals(str)) {
                                    int indexOf = str.indexOf(91);
                                    if (indexOf != -1) {
                                        Matcher matcher = C95394dd.A02.matcher(str.substring(indexOf));
                                        if (matcher.matches()) {
                                            r8.A09 = matcher.group(1);
                                        }
                                        str = str.substring(0, indexOf);
                                    }
                                    String[] split = str.split("\\.", -1);
                                    String str2 = split[0];
                                    int indexOf2 = str2.indexOf(35);
                                    if (indexOf2 != -1) {
                                        r8.A08 = str2.substring(0, indexOf2);
                                        r8.A07 = str2.substring(indexOf2 + 1);
                                    } else {
                                        r8.A08 = str2;
                                    }
                                    int length = split.length;
                                    if (length > 1) {
                                        boolean z3 = true;
                                        if (length > length) {
                                            z3 = false;
                                        }
                                        C95314dV.A03(z3);
                                        r8.A0A = new HashSet(Arrays.asList(Arrays.copyOfRange(split, 1, length)));
                                    }
                                }
                                String str3 = null;
                                boolean z4 = false;
                                while (!z4) {
                                    int i9 = r7.A01;
                                    str3 = C95394dd.A01(r7, sb);
                                    if (str3 == null || "}".equals(str3)) {
                                        z4 = true;
                                    } else {
                                        z4 = false;
                                        r7.A0S(i9);
                                        C95394dd.A02(r7);
                                        String A00 = C95394dd.A00(r7, sb);
                                        if (!"".equals(A00) && ":".equals(C95394dd.A01(r7, sb))) {
                                            C95394dd.A02(r7);
                                            StringBuilder A0h = C12960it.A0h();
                                            while (true) {
                                                i2 = r7.A01;
                                                String A013 = C95394dd.A01(r7, sb);
                                                if (A013 == null) {
                                                    break;
                                                } else if ("}".equals(A013) || ";".equals(A013)) {
                                                    break;
                                                } else {
                                                    A0h.append(A013);
                                                }
                                            }
                                            r7.A0S(i2);
                                            String obj = A0h.toString();
                                            if (obj != null && !"".equals(obj)) {
                                                int i10 = r7.A01;
                                                String A014 = C95394dd.A01(r7, sb);
                                                if (!";".equals(A014)) {
                                                    if ("}".equals(A014)) {
                                                        r7.A0S(i10);
                                                    }
                                                }
                                                if ("color".equals(A00)) {
                                                    r8.A02 = C93084Za.A00(obj, true);
                                                    r8.A0D = true;
                                                } else if ("background-color".equals(A00)) {
                                                    r8.A00 = C93084Za.A00(obj, true);
                                                    r8.A0C = true;
                                                } else {
                                                    boolean z5 = true;
                                                    if ("ruby-position".equals(A00)) {
                                                        if ("over".equals(obj)) {
                                                            r8.A04 = 1;
                                                        } else if ("under".equals(obj)) {
                                                            r8.A04 = 2;
                                                        }
                                                    } else if ("text-combine-upright".equals(A00)) {
                                                        if (!"all".equals(obj) && !obj.startsWith("digits")) {
                                                            z5 = false;
                                                        }
                                                        r8.A0B = z5;
                                                    } else if ("text-decoration".equals(A00)) {
                                                        if ("underline".equals(obj)) {
                                                            r8.A05 = 1;
                                                        }
                                                    } else if ("font-family".equals(A00)) {
                                                        r8.A06 = obj.toLowerCase(Locale.US);
                                                    } else if ("font-weight".equals(A00)) {
                                                        if ("bold".equals(obj)) {
                                                            r8.A01 = 1;
                                                        }
                                                    } else if ("font-style".equals(A00) && "italic".equals(obj)) {
                                                        r8.A03 = 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if ("}".equals(str3)) {
                                    A0l3.add(r8);
                                }
                            } else {
                                break;
                            }
                        }
                        A0l.addAll(A0l3);
                    } else {
                        throw new C76723m6("A style block was found after the first cue.");
                    }
                } else if (c == 3 && (A0L = r0.A0L()) != null) {
                    Pattern pattern = C95454dk.A02;
                    Matcher matcher2 = pattern.matcher(A0L);
                    if (matcher2.matches()) {
                        A01 = C95454dk.A01(r0, null, A0l, matcher2);
                    } else {
                        String A0L4 = r0.A0L();
                        if (A0L4 != null) {
                            Matcher matcher3 = pattern.matcher(A0L4);
                            if (matcher3.matches()) {
                                A01 = C95454dk.A01(r0, A0L.trim(), A0l, matcher3);
                            }
                        }
                    }
                    if (A01 != null) {
                        A0l2.add(A01);
                    }
                }
            }
        } catch (AnonymousClass496 e) {
            throw new C76723m6(e);
        }
    }
}
