package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.1OF  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1OF implements AnonymousClass1OG {
    public final C32881ct A00;
    public final AbstractC14440lR A01;

    public AnonymousClass1OF(C32881ct r1, AbstractC14440lR r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        int i;
        byte[] bArr;
        byte[] bArr2;
        int i2;
        byte[] bArr3;
        byte[] bArr4;
        byte[] bArr5;
        if (this instanceof AnonymousClass1ON) {
            AnonymousClass1ON r0 = (AnonymousClass1ON) this;
            Object obj = r0.A0C;
            synchronized (obj) {
                i = r0.A00;
            }
            if (i != 0) {
                boolean z = false;
                if (i == 1) {
                    synchronized (obj) {
                        bArr = r0.A06;
                        bArr2 = r0.A03;
                    }
                    boolean z2 = false;
                    if (bArr != null) {
                        z2 = true;
                    }
                    AnonymousClass009.A0F(z2);
                    if (bArr2 != null) {
                        z = true;
                    }
                    AnonymousClass009.A0F(z);
                    C244315m r6 = r0.A0A;
                    C17220qS r11 = r6.A00;
                    String A01 = r11.A01();
                    StringBuilder sb = new StringBuilder("EncryptedBackupProtocolHelper/sendBeginRegIq id=");
                    sb.append(A01);
                    Log.i(sb.toString());
                    r11.A09(new AnonymousClass3ZH(r0, r6, bArr, bArr2), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("r1", bArr, (AnonymousClass1W9[]) null), C16550pE.A01()}), A01, 255, 32000);
                } else if (i == 2) {
                    byte[] bArr6 = r0.A04;
                    if (bArr6 != null) {
                        z = true;
                    }
                    AnonymousClass009.A0F(z);
                    C244315m r5 = r0.A0A;
                    C17220qS r10 = r5.A00;
                    String A012 = r10.A01();
                    StringBuilder sb2 = new StringBuilder("EncryptedBackupProtocolHelper/sendFinishRegIq id=");
                    sb2.append(A012);
                    Log.i(sb2.toString());
                    r10.A09(new AnonymousClass3Z3(r0, r5), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A012), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("reg_payload", bArr6, (AnonymousClass1W9[]) null), C16550pE.A01()}), A012, 255, 32000);
                } else {
                    StringBuilder sb3 = new StringBuilder("RegistrationUserHandler/unexpected currentOperation ");
                    sb3.append(i);
                    throw new IllegalStateException(sb3.toString());
                }
            } else {
                C244315m r52 = r0.A0A;
                C17220qS r9 = r52.A00;
                String A013 = r9.A01();
                StringBuilder sb4 = new StringBuilder("EncryptedBackupProtocolHelper/sendInitRegIq id=");
                sb4.append(A013);
                Log.i(sb4.toString());
                r9.A09(new AnonymousClass3Z2(r0, r52), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A013), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("init_reg", null), C16550pE.A01()}), A013, 255, 32000);
            }
        } else if (!(this instanceof AnonymousClass1OE)) {
            AnonymousClass2DA r02 = (AnonymousClass2DA) this;
            C244315m r53 = r02.A01;
            C17220qS r92 = r53.A00;
            String A014 = r92.A01();
            StringBuilder sb5 = new StringBuilder("EncryptedBackupProtocolHelper/sendDeleteAccountIq id=");
            sb5.append(A014);
            Log.i(sb5.toString());
            r92.A09(new AnonymousClass3Z6(r02, r53), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A014), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("delete", null), C16550pE.A01()}), A014, 255, 32000);
        } else {
            AnonymousClass1OE r03 = (AnonymousClass1OE) this;
            Object obj2 = r03.A0C;
            synchronized (obj2) {
                i2 = r03.A00;
            }
            if (i2 != 0) {
                boolean z3 = false;
                if (i2 == 1) {
                    synchronized (obj2) {
                        bArr3 = r03.A06;
                        bArr4 = r03.A05;
                    }
                    boolean z4 = false;
                    if (bArr3 != null) {
                        z4 = true;
                    }
                    AnonymousClass009.A0F(z4);
                    if (bArr4 != null) {
                        z3 = true;
                    }
                    AnonymousClass009.A0F(z3);
                    C244315m r54 = r03.A0A;
                    C17220qS r112 = r54.A00;
                    String A015 = r112.A01();
                    StringBuilder sb6 = new StringBuilder("EncryptedBackupProtocolHelper/sendBeginLoginIq id=");
                    sb6.append(A015);
                    Log.i(sb6.toString());
                    r112.A09(new AnonymousClass3ZE(r03, r54, bArr4), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A015), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("l1", bArr3, (AnonymousClass1W9[]) null), C16550pE.A01()}), A015, 255, 32000);
                } else if (i2 == 2) {
                    synchronized (obj2) {
                        bArr5 = r03.A07;
                    }
                    if (bArr5 != null) {
                        z3 = true;
                    }
                    AnonymousClass009.A0F(z3);
                    C244315m r62 = r03.A0A;
                    C17220qS r102 = r62.A00;
                    String A016 = r102.A01();
                    StringBuilder sb7 = new StringBuilder("EncryptedBackupProtocolHelper/finishLoginOnSuccess id=");
                    sb7.append(A016);
                    Log.i(sb7.toString());
                    r102.A09(new AnonymousClass3Z5(r03, r62), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A016), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("l3", bArr5, (AnonymousClass1W9[]) null), C16550pE.A01()}), A016, 255, 32000);
                } else {
                    StringBuilder sb8 = new StringBuilder("Unhandled operation ");
                    sb8.append(i2);
                    throw new IllegalStateException(sb8.toString());
                }
            } else {
                C244315m r55 = r03.A0A;
                C17220qS r93 = r55.A00;
                String A017 = r93.A01();
                StringBuilder sb9 = new StringBuilder("EncryptedBackupProtocolHelper/sendInitLoginIq id=");
                sb9.append(A017);
                Log.i(sb9.toString());
                r93.A09(new AnonymousClass3Z4(r03, r55), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A017), new AnonymousClass1W9("xmlns", "vesta"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("to", "s.whatsapp.net")}, new AnonymousClass1V8[]{new AnonymousClass1V8("init_login", null), C16550pE.A01()}), A017, 255, 32000);
            }
        }
    }

    @Override // X.AnonymousClass1OG
    public void APr(String str, int i) {
        AnonymousClass1OG r0;
        AnonymousClass2DA r2;
        AbstractC14440lR r4;
        RunnableBRunnable0Shape2S0100000_I0_2 runnableBRunnable0Shape2S0100000_I0_2;
        long longValue;
        String str2;
        if (!(this instanceof AnonymousClass2DA)) {
            Long A00 = this.A00.A00();
            if ((i == 500 || i == 3) && A00 != null) {
                r4 = this.A01;
                runnableBRunnable0Shape2S0100000_I0_2 = new RunnableBRunnable0Shape2S0100000_I0_2(this, 14);
                longValue = A00.longValue();
                str2 = "HsmCommandHandler/onError";
                r4.AbK(runnableBRunnable0Shape2S0100000_I0_2, str2, longValue);
                return;
            } else if (this instanceof AnonymousClass1ON) {
                AnonymousClass1ON r22 = (AnonymousClass1ON) this;
                synchronized (r22.A0C) {
                    r0 = r22.A02;
                    if (r0 == null) {
                        r0 = new AnonymousClass1OG() { // from class: X.590
                            @Override // X.AnonymousClass1OG
                            public final void APr(String str3, int i2) {
                                AnonymousClass1ON r1 = AnonymousClass1ON.this;
                                AnonymousClass1OP r02 = r1.A08;
                                synchronized (r1.A0C) {
                                }
                                EncBackupViewModel.A00(r02.A00, i2);
                            }
                        };
                        r22.A02 = r0;
                    }
                }
            } else if (!(this instanceof AnonymousClass1OE)) {
                r2 = (AnonymousClass2DA) this;
                r0 = r2.A00;
            } else {
                AnonymousClass1OE r23 = (AnonymousClass1OE) this;
                synchronized (r23.A0C) {
                    r0 = r23.A04;
                    if (r0 == null) {
                        r0 = new AnonymousClass1OG() { // from class: X.58z
                            @Override // X.AnonymousClass1OG
                            public final void APr(String str3, int i2) {
                                int i3;
                                AnonymousClass1OE r02 = AnonymousClass1OE.this;
                                AnonymousClass1OI r24 = r02.A08;
                                synchronized (r02.A0C) {
                                    i3 = r02.A00;
                                }
                                r24.APs(str3, i2, i3, -1, 0);
                            }
                        };
                        r23.A04 = r0;
                    }
                }
            }
        } else {
            r2 = (AnonymousClass2DA) this;
            Long A002 = ((AnonymousClass1OF) r2).A00.A00();
            if ((i == 500 || i == 3) && A002 != null) {
                r4 = ((AnonymousClass1OF) r2).A01;
                runnableBRunnable0Shape2S0100000_I0_2 = new RunnableBRunnable0Shape2S0100000_I0_2(r2, 13);
                longValue = A002.longValue();
                str2 = "DeleteAccountHandler/onError";
                r4.AbK(runnableBRunnable0Shape2S0100000_I0_2, str2, longValue);
                return;
            }
            r0 = r2.A00;
        }
        r0.APr(str, i);
    }
}
