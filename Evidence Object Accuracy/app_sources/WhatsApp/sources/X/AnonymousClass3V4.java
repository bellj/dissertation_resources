package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3V4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3V4 implements AbstractC116485Vq {
    public final /* synthetic */ C53912fi A00;
    public final /* synthetic */ String A01;

    public AnonymousClass3V4(C53912fi r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AbstractC116485Vq
    public void AU2(String str) {
        this.A00.A0I.A0A(str);
    }

    @Override // X.AbstractC116485Vq
    public void AU3(C90134Ms r6) {
        String str = r6.A01;
        if (str.equals("success")) {
            C53912fi r1 = this.A00;
            AnonymousClass016 r0 = r1.A0A;
            String str2 = this.A01;
            r0.A0A(str2);
            C14820m6 r2 = r1.A0G;
            UserJid userJid = r1.A0H;
            r2.A0s(userJid.getRawString(), str2);
            r2.A0r(userJid.getRawString(), r6.A00);
        }
        this.A00.A0I.A0A(str);
    }
}
