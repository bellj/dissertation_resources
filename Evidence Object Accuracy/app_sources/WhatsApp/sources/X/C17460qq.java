package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;

/* renamed from: X.0qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17460qq implements AbstractC17410ql {
    public final C235612d A00;
    public final C22210yi A01;
    public final AbstractC38651oS A02 = new AbstractC38651oS() { // from class: X.59W
        @Override // X.AbstractC38651oS
        public final void AWb(AnonymousClass4S5 r1) {
        }
    };
    public final AnonymousClass29Q A03 = new AnonymousClass29Q() { // from class: X.59Y
        @Override // X.AnonymousClass29Q
        public final void AWj(boolean z) {
        }
    };
    public final C235512c A04;
    public final C235412b A05;
    public final AbstractC14440lR A06;

    @Override // X.AbstractC17410ql
    public void AMk() {
    }

    public C17460qq(C235612d r2, C22210yi r3, C235512c r4, C235412b r5, AbstractC14440lR r6) {
        C16700pc.A0E(r6, 1);
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(r2, 4);
        C16700pc.A0E(r3, 5);
        this.A06 = r6;
        this.A05 = r5;
        this.A04 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        this.A06.Ab6(new RunnableBRunnable0Shape2S0100000_I0_2(this, 7));
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        this.A04.A0H(this.A00.A00(AnonymousClass1WF.A00), this.A02, 6, !z);
    }
}
