package X;

import java.security.AccessController;
import java.security.PrivilegedActionException;
import sun.misc.Unsafe;

/* renamed from: X.3tr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81343tr extends AnonymousClass4Y6 {
    public static final long A00;
    public static final long A01;
    public static final long A02;
    public static final long A03;
    public static final long A04;
    public static final Unsafe A05;

    static {
        Unsafe unsafe;
        try {
            try {
                unsafe = Unsafe.getUnsafe();
            } catch (SecurityException unused) {
                unsafe = (Unsafe) AccessController.doPrivileged(new C112025Bt());
            }
            try {
                A02 = unsafe.objectFieldOffset(AbstractC81363tt.class.getDeclaredField("waiters"));
                A00 = unsafe.objectFieldOffset(AbstractC81363tt.class.getDeclaredField("listeners"));
                A01 = unsafe.objectFieldOffset(AbstractC81363tt.class.getDeclaredField("value"));
                A04 = unsafe.objectFieldOffset(C94804cZ.class.getDeclaredField("thread"));
                A03 = unsafe.objectFieldOffset(C94804cZ.class.getDeclaredField("next"));
                A05 = unsafe;
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw e;
                } else if (!(e instanceof Error)) {
                    throw new RuntimeException(e);
                } else {
                    throw e;
                }
            }
        } catch (PrivilegedActionException e2) {
            throw new RuntimeException("Could not initialize intrinsics", e2.getCause());
        }
    }

    @Override // X.AnonymousClass4Y6
    public void A00(C94804cZ r4, C94804cZ r5) {
        A05.putObject(r4, A03, r5);
    }

    @Override // X.AnonymousClass4Y6
    public void A01(C94804cZ r4, Thread thread) {
        A05.putObject(r4, A04, thread);
    }

    @Override // X.AnonymousClass4Y6
    public boolean A02(C94814ca r8, C94814ca r9, AbstractC81363tt r10) {
        Unsafe unsafe = A05;
        long j = A00;
        while (!unsafe.compareAndSwapObject(r10, j, r8, r9)) {
            if (unsafe.getObject(r10, j) != r8) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AnonymousClass4Y6
    public boolean A03(C94804cZ r8, C94804cZ r9, AbstractC81363tt r10) {
        Unsafe unsafe = A05;
        long j = A02;
        while (!unsafe.compareAndSwapObject(r10, j, r8, r9)) {
            if (unsafe.getObject(r10, j) != r8) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AnonymousClass4Y6
    public boolean A04(AbstractC81363tt r8, Object obj, Object obj2) {
        Unsafe unsafe = A05;
        long j = A01;
        while (!unsafe.compareAndSwapObject(r8, j, (Object) null, obj2)) {
            if (unsafe.getObject(r8, j) != null) {
                return false;
            }
        }
        return true;
    }
}
