package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2ge  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54392ge extends AnonymousClass02M {
    public int A00 = 0;
    public long A01 = 0;
    public AbstractC116235Uq A02;
    public List A03;
    public boolean A04 = false;
    public final LayoutInflater A05;
    public final AnonymousClass1AB A06;
    public final AbstractC116245Ur A07;
    public final Integer A08;
    public final HashMap A09 = C12970iu.A11();

    public C54392ge(Context context, AnonymousClass1AB r4, AbstractC116245Ur r5, Integer num, List list) {
        this.A05 = LayoutInflater.from(context);
        this.A06 = r4;
        this.A07 = r5;
        this.A08 = num;
        A0E(list);
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        List list;
        Number number;
        if (!super.A00 || (list = this.A03) == null || (number = (Number) this.A09.get(((AnonymousClass1KS) list.get(i)).A0C)) == null) {
            return -1;
        }
        return number.longValue();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return C12970iu.A09(this.A03);
    }

    public void A0E(List list) {
        this.A03 = list;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KS r5 = (AnonymousClass1KS) it.next();
                HashMap hashMap = this.A09;
                if (hashMap.get(r5.A0C) == null) {
                    long j = this.A01;
                    this.A01 = 1 + j;
                    hashMap.put(r5.A0C, Long.valueOf(j));
                }
            }
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r11, int i) {
        AnonymousClass1KS r0;
        C55082hl r112 = (C55082hl) r11;
        List list = this.A03;
        if (list != null) {
            AnonymousClass1KS r3 = (AnonymousClass1KS) list.get(i);
            boolean z = this.A04;
            if (z != r112.A03) {
                r112.A03 = z;
                if (!z) {
                    StickerView stickerView = r112.A07;
                    stickerView.A04 = false;
                    stickerView.A03();
                } else if (r112.A02) {
                    StickerView stickerView2 = r112.A07;
                    stickerView2.A04 = true;
                    stickerView2.A02();
                }
            }
            int i2 = this.A00;
            if (r3 == null || (r0 = r112.A01) == null || !r3.A0C.equals(r0.A0C)) {
                r112.A01 = r3;
                View view = r112.A0H;
                if (r3 == null) {
                    view.setOnClickListener(null);
                    r112.A07.setImageResource(0);
                    view.setBackgroundResource(0);
                    view.setClickable(false);
                } else {
                    view.setOnClickListener(new ViewOnClickCListenerShape2S0201000_I1(r112, r3, i, 4));
                    view.setOnLongClickListener(r112.A04);
                    view.setBackgroundResource(R.drawable.selector_orange_gradient);
                    view.setContentDescription(C28111Kr.A01(view.getContext(), r3));
                    StickerView stickerView3 = r112.A07;
                    int dimensionPixelSize = C12960it.A09(stickerView3).getDimensionPixelSize(R.dimen.sticker_picker_item);
                    r112.A05.A04(stickerView3, r3, new AbstractC39661qJ() { // from class: X.59T
                        @Override // X.AbstractC39661qJ
                        public final void AWe(boolean z2) {
                            C55082hl r1 = C55082hl.this;
                            if (r1.A03 && r1.A02) {
                                StickerView stickerView4 = r1.A07;
                                stickerView4.A04 = true;
                                stickerView4.A02();
                            }
                        }
                    }, i2, dimensionPixelSize, dimensionPixelSize, true, true);
                }
            }
            r112.A00 = new View.OnLongClickListener(r3, this) { // from class: X.4nD
                public final /* synthetic */ AnonymousClass1KS A00;
                public final /* synthetic */ C54392ge A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view2) {
                    C54392ge r02 = this.A01;
                    AnonymousClass1KS r1 = this.A00;
                    AbstractC116235Uq r03 = r02.A02;
                    if (r03 == null) {
                        return false;
                    }
                    r03.AWf(r1);
                    return true;
                }
            };
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C55082hl(this.A05, viewGroup, this.A06, this.A07, this.A08);
    }
}
