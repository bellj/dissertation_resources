package X;

import java.io.IOException;
import java.util.Iterator;

/* renamed from: X.5NV  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NV extends AnonymousClass1TL implements AbstractC117215Yz {
    public final boolean A00;
    public final AnonymousClass1TN[] A01;

    public AnonymousClass5NV() {
        this.A01 = C94954co.A03;
        this.A00 = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r4.length < 2) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5NV(X.AnonymousClass1TN[] r4, boolean r5) {
        /*
            r3 = this;
            r3.<init>()
            r3.A01 = r4
            if (r5 != 0) goto L_0x000c
            int r2 = r4.length
            r1 = 2
            r0 = 0
            if (r2 >= r1) goto L_0x000d
        L_0x000c:
            r0 = 1
        L_0x000d:
            r3.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NV.<init>(X.1TN[], boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021 A[ORIG_RETURN, RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A05(byte[] r5, byte[] r6) {
        /*
            r4 = 0
            byte r0 = r5[r4]
            r1 = r0 & -33
            byte r0 = r6[r4]
            r0 = r0 & -33
            r3 = 1
            if (r1 != r0) goto L_0x001f
            int r1 = r5.length
            int r0 = r6.length
            int r2 = java.lang.Math.min(r1, r0)
            int r2 = r2 - r3
        L_0x0013:
            if (r3 >= r2) goto L_0x0026
            byte r1 = r5[r3]
            byte r0 = r6[r3]
            if (r1 == r0) goto L_0x0023
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0 & 255(0xff, float:3.57E-43)
        L_0x001f:
            if (r1 >= r0) goto L_0x0022
        L_0x0021:
            r4 = 1
        L_0x0022:
            return r4
        L_0x0023:
            int r3 = r3 + 1
            goto L_0x0013
        L_0x0026:
            byte r0 = r5[r2]
            r1 = r0 & 255(0xff, float:3.57E-43)
            byte r0 = r6[r2]
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r1 > r0) goto L_0x0022
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NV.A05(byte[], byte[]):boolean");
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        boolean z = this.A00;
        AnonymousClass1TN[] r0 = this.A01;
        if (!z) {
            r0 = (AnonymousClass1TN[]) r0.clone();
            A04(r0);
        }
        return new C114785Nb(r0);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return true;
    }

    public AnonymousClass5NV(AnonymousClass1TN r4) {
        if (r4 != null) {
            this.A01 = new AnonymousClass1TN[]{r4};
            this.A00 = true;
            return;
        }
        throw C12980iv.A0n("'element' cannot be null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        if (r2.length < 2) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5NV(X.C94954co r6, boolean r7) {
        /*
            r5 = this;
            r5.<init>()
            r4 = 2
            if (r7 == 0) goto L_0x0026
            int r3 = r6.A00
            if (r3 < r4) goto L_0x0026
            if (r3 != 0) goto L_0x001d
            X.1TN[] r2 = X.C94954co.A03
        L_0x000e:
            A04(r2)
        L_0x0011:
            r5.A01 = r2
            if (r7 != 0) goto L_0x0019
            int r1 = r2.length
            r0 = 0
            if (r1 >= r4) goto L_0x001a
        L_0x0019:
            r0 = 1
        L_0x001a:
            r5.A00 = r0
            return
        L_0x001d:
            X.1TN[] r2 = new X.AnonymousClass1TN[r3]
            X.1TN[] r1 = r6.A02
            r0 = 0
            java.lang.System.arraycopy(r1, r0, r2, r0, r3)
            goto L_0x000e
        L_0x0026:
            X.1TN[] r2 = r6.A07()
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NV.<init>(X.4co, boolean):void");
    }

    public static AnonymousClass5NV A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NV)) {
            return (AnonymousClass5NV) obj;
        }
        if (obj instanceof AbstractC117245Zc) {
            return A00(((AnonymousClass1TN) obj).Aer());
        }
        if (obj instanceof byte[]) {
            try {
                return A00(AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("failed to construct set from byte[]: ")));
            }
        } else {
            if (obj instanceof AnonymousClass1TN) {
                AnonymousClass1TL Aer = ((AnonymousClass1TN) obj).Aer();
                if (Aer instanceof AnonymousClass5NV) {
                    return (AnonymousClass5NV) Aer;
                }
            }
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    public static AnonymousClass5NV A01(AnonymousClass5NU r3) {
        AnonymousClass1TL A00 = AnonymousClass5NU.A00(r3);
        if (r3.A02) {
            return r3 instanceof C114815Ne ? new C114795Nc(A00) : new C114805Nd(A00);
        }
        if (A00 instanceof AnonymousClass5NV) {
            AnonymousClass5NV r2 = (AnonymousClass5NV) A00;
            return r3 instanceof C114815Ne ? r2 : (AnonymousClass5NV) r2.A07();
        } else if (A00 instanceof AbstractC114775Na) {
            AbstractC114775Na r22 = (AbstractC114775Na) A00;
            boolean z = r22 instanceof AnonymousClass5NW;
            AnonymousClass5NW r23 = r22;
            if (z) {
                AnonymousClass5NW r24 = (AnonymousClass5NW) r22;
                r24.A0E();
                r23 = r24;
            }
            AnonymousClass1TN[] r25 = r23.A00;
            return r3 instanceof C114815Ne ? new C114795Nc(r25) : new C114805Nd(r25, false);
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(r3), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    public static void A04(AnonymousClass1TN[] r12) {
        int length = r12.length;
        if (length >= 2) {
            AnonymousClass1TN r11 = r12[0];
            AnonymousClass1TN r9 = r12[1];
            try {
                byte[] A0c = C72463ee.A0c(r11);
                try {
                    byte[] A0c2 = C72463ee.A0c(r9);
                    byte[] bArr = A0c2;
                    if (A05(A0c2, A0c)) {
                        r9 = r11;
                        r11 = r9;
                        bArr = A0c;
                        A0c = A0c2;
                    }
                    for (int i = 2; i < length; i++) {
                        AnonymousClass1TN r4 = r12[i];
                        try {
                            byte[] A0c3 = C72463ee.A0c(r4);
                            if (A05(bArr, A0c3)) {
                                r12[i - 2] = r11;
                                r11 = r9;
                                A0c = bArr;
                                r9 = r4;
                                bArr = A0c3;
                            } else if (A05(A0c, A0c3)) {
                                r12[i - 2] = r11;
                                r11 = r4;
                                A0c = A0c3;
                            } else {
                                int i2 = i - 1;
                                while (true) {
                                    i2--;
                                    if (i2 <= 0) {
                                        break;
                                    }
                                    AnonymousClass1TN r1 = r12[i2 - 1];
                                    try {
                                        if (A05(C72463ee.A0c(r1), A0c3)) {
                                            break;
                                        }
                                        r12[i2] = r1;
                                    } catch (IOException unused) {
                                        throw C12970iu.A0f("cannot encode object added to SET");
                                    }
                                }
                                r12[i2] = r4;
                            }
                        } catch (IOException unused2) {
                            throw C12970iu.A0f("cannot encode object added to SET");
                        }
                    }
                    r12[length - 2] = r11;
                    r12[length - 1] = r9;
                } catch (IOException unused3) {
                    throw C12970iu.A0f("cannot encode object added to SET");
                }
            } catch (IOException unused4) {
                throw C12970iu.A0f("cannot encode object added to SET");
            }
        }
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A07() {
        if ((this instanceof C114805Nd) || (this instanceof C114785Nb)) {
            return this;
        }
        return new C114805Nd(this.A01, this.A00);
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r11, boolean z) {
        if (this instanceof C114805Nd) {
            C114805Nd r9 = (C114805Nd) this;
            if (z) {
                r11.A00.write(49);
            }
            AnonymousClass1TP A00 = r11.A00();
            AnonymousClass1TN[] r7 = r9.A01;
            int length = r7.length;
            int i = r9.A00;
            int i2 = 0;
            if (i < 0) {
                if (length > 16) {
                    i = 0;
                    for (int i3 = 0; i3 < length; i3++) {
                        i = C72453ed.A0S(r7, i3, i);
                    }
                    r9.A00 = i;
                } else {
                    AnonymousClass1TL[] r3 = new AnonymousClass1TL[length];
                    int i4 = 0;
                    for (int i5 = 0; i5 < length; i5++) {
                        AnonymousClass1TL A07 = C72463ee.A0N(r7, i5).A07();
                        r3[i5] = A07;
                        i4 += A07.A05();
                    }
                    r9.A00 = i4;
                    r11.A02(i4);
                    while (i2 < length) {
                        A00.A04(r3[i2], true);
                        i2++;
                    }
                    return;
                }
            }
            r11.A02(i);
            while (i2 < length) {
                A00.A04(C72463ee.A0N(r7, i2), true);
                i2++;
            }
        } else if (!(this instanceof C114785Nb)) {
            r11.A07(this.A01, 49, z);
        } else {
            C114785Nb r92 = (C114785Nb) this;
            if (z) {
                r11.A00.write(49);
            }
            AnonymousClass5N8 A01 = r11.A01();
            AnonymousClass1TN[] r8 = r92.A01;
            int length2 = r8.length;
            int i6 = r92.A00;
            int i7 = 0;
            if (i6 < 0) {
                if (length2 > 16) {
                    i6 = 0;
                    for (int i8 = 0; i8 < length2; i8++) {
                        i6 = C72453ed.A0R(r8, i8, i6);
                    }
                    r92.A00 = i6;
                } else {
                    AnonymousClass1TL[] r32 = new AnonymousClass1TL[length2];
                    int i9 = 0;
                    for (int i10 = 0; i10 < length2; i10++) {
                        AnonymousClass1TL A06 = C72463ee.A0N(r8, i10).A06();
                        r32[i10] = A06;
                        i9 += A06.A05();
                    }
                    r92.A00 = i9;
                    r11.A02(i9);
                    while (i7 < length2) {
                        r32[i7].A08(A01, true);
                        i7++;
                    }
                    return;
                }
            }
            r11.A02(i6);
            while (i7 < length2) {
                C72463ee.A0N(r8, i7).A06().A08(A01, true);
                i7++;
            }
        }
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r8) {
        if (r8 instanceof AnonymousClass5NV) {
            AnonymousClass5NV r82 = (AnonymousClass5NV) r8;
            int length = this.A01.length;
            if (r82.A01.length == length) {
                AnonymousClass5NV r4 = (AnonymousClass5NV) A06();
                AnonymousClass5NV r3 = (AnonymousClass5NV) r82.A06();
                for (int i = 0; i < length; i++) {
                    AnonymousClass1TL A0N = C72463ee.A0N(r4.A01, i);
                    AnonymousClass1TL A0N2 = C72463ee.A0N(r3.A01, i);
                    if (A0N == A0N2 || A0N.A0A(A0N2)) {
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        AnonymousClass1TN[] r3 = this.A01;
        int length = r3.length;
        int i = length + 1;
        while (true) {
            length--;
            if (length < 0) {
                return i;
            }
            i = C12990iw.A08(C72463ee.A0N(r3, length), i);
        }
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass5DB(C94954co.A04(this.A01));
    }

    @Override // java.lang.Object
    public String toString() {
        AnonymousClass1TN[] r4 = this.A01;
        int length = r4.length;
        if (length == 0) {
            return "[]";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('[');
        int i = 0;
        while (true) {
            stringBuffer.append(r4[i]);
            i++;
            if (i >= length) {
                stringBuffer.append(']');
                return stringBuffer.toString();
            }
            stringBuffer.append(", ");
        }
    }
}
