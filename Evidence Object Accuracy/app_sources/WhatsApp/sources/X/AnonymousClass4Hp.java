package X;

/* renamed from: X.4Hp  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Hp {
    public static final C91504Rz A00;
    public static final C91504Rz A01;
    public static final C91504Rz A02;
    public static final C91504Rz A03;
    public static final C91504Rz A04;
    public static final C91504Rz A05;
    public static final C91504Rz A06;
    public static final C91504Rz A07;
    public static final C91504Rz A08;
    public static final C91504Rz A09;
    public static final C91504Rz A0A;
    public static final C91504Rz A0B;
    public static final C91504Rz[] A0C;

    static {
        AnonymousClass4AH r1 = AnonymousClass4AH.START;
        AnonymousClass45r r3 = C88884Hs.A0I;
        C91504Rz r13 = new C91504Rz(null, r3, r1, "START");
        A02 = r13;
        AnonymousClass4AH r2 = AnonymousClass4AH.ACTIVE;
        C91504Rz r12 = new C91504Rz(null, r3, r2, "WAIT_SH_HRR");
        A0B = r12;
        C91504Rz r11 = new C91504Rz(C88884Hs.A05, null, r2, "WAIT_SEND_EARLY_DATA");
        A09 = r11;
        C91504Rz r10 = new C91504Rz(null, r3, r2, "WAIT_SH");
        A0A = r10;
        C91504Rz r9 = new C91504Rz(C88884Hs.A06, r3, r2, "WAIT_EE");
        A06 = r9;
        C91504Rz r8 = new C91504Rz(null, r3, r2, "WAIT_CERT_CR");
        A04 = r8;
        C91504Rz r7 = new C91504Rz(null, r3, r2, "WAIT_CERT");
        A03 = r7;
        C91504Rz r6 = new C91504Rz(null, r3, r2, "WAIT_CV");
        A05 = r6;
        C91504Rz r5 = new C91504Rz(null, r3, r2, "WAIT_FINISHED");
        A07 = r5;
        C91504Rz r4 = new C91504Rz(C88884Hs.A04, r3, r2, "WAIT_SEND_CERTS_FIN");
        A08 = r4;
        C91504Rz r32 = new C91504Rz(C88884Hs.A07, null, r2, "CONNECTED");
        A00 = r32;
        C91504Rz r22 = new C91504Rz(null, null, AnonymousClass4AH.END, "END");
        A01 = r22;
        C91504Rz[] r14 = new C91504Rz[12];
        C72453ed.A1F(r13, r11, r12, r10, r14);
        r14[4] = r9;
        C12970iu.A1R(r8, r7, r6, r5, r14);
        r14[9] = r4;
        r14[10] = r32;
        r14[11] = r22;
        A0C = r14;
    }
}
