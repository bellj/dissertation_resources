package X;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.widget.AppCompatSpinner;

/* renamed from: X.0XS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XS implements AbstractC12740iQ, DialogInterface.OnClickListener {
    public ListAdapter A00;
    public AnonymousClass04S A01;
    public CharSequence A02;
    public final /* synthetic */ AppCompatSpinner A03;

    @Override // X.AbstractC12740iQ
    public Drawable AAm() {
        return null;
    }

    @Override // X.AbstractC12740iQ
    public int ADN() {
        return 0;
    }

    @Override // X.AbstractC12740iQ
    public int AHX() {
        return 0;
    }

    public AnonymousClass0XS(AppCompatSpinner appCompatSpinner) {
        this.A03 = appCompatSpinner;
    }

    @Override // X.AbstractC12740iQ
    public CharSequence ADM() {
        return this.A02;
    }

    @Override // X.AbstractC12740iQ
    public boolean AK4() {
        AnonymousClass04S r0 = this.A01;
        if (r0 != null) {
            return r0.isShowing();
        }
        return false;
    }

    @Override // X.AbstractC12740iQ
    public void Abi(ListAdapter listAdapter) {
        this.A00 = listAdapter;
    }

    @Override // X.AbstractC12740iQ
    public void Abn(Drawable drawable) {
        Log.e("AppCompatSpinner", "Cannot set popup background for MODE_DIALOG, ignoring");
    }

    @Override // X.AbstractC12740iQ
    public void AcC(int i) {
        Log.e("AppCompatSpinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
    }

    @Override // X.AbstractC12740iQ
    public void AcD(int i) {
        Log.e("AppCompatSpinner", "Cannot set horizontal (original) offset for MODE_DIALOG, ignoring");
    }

    @Override // X.AbstractC12740iQ
    public void Ace(CharSequence charSequence) {
        this.A02 = charSequence;
    }

    @Override // X.AbstractC12740iQ
    public void Ad7(int i) {
        Log.e("AppCompatSpinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
    }

    @Override // X.AbstractC12740iQ
    public void Adf(int i, int i2) {
        if (this.A00 != null) {
            AppCompatSpinner appCompatSpinner = this.A03;
            C004802e r3 = new C004802e(appCompatSpinner.A04);
            CharSequence charSequence = this.A02;
            if (charSequence != null) {
                r3.setTitle(charSequence);
            }
            ListAdapter listAdapter = this.A00;
            int selectedItemPosition = appCompatSpinner.getSelectedItemPosition();
            AnonymousClass0OC r1 = r3.A01;
            r1.A0D = listAdapter;
            r1.A05 = this;
            r1.A00 = selectedItemPosition;
            r1.A0L = true;
            AnonymousClass04S create = r3.create();
            this.A01 = create;
            ListView listView = create.A00.A0J;
            if (Build.VERSION.SDK_INT >= 17) {
                listView.setTextDirection(i);
                listView.setTextAlignment(i2);
            }
            this.A01.show();
        }
    }

    @Override // X.AbstractC12740iQ
    public void dismiss() {
        AnonymousClass04S r0 = this.A01;
        if (r0 != null) {
            r0.dismiss();
            this.A01 = null;
        }
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        AppCompatSpinner appCompatSpinner = this.A03;
        appCompatSpinner.setSelection(i);
        if (appCompatSpinner.getOnItemClickListener() != null) {
            appCompatSpinner.performItemClick(null, i, this.A00.getItemId(i));
        }
        dismiss();
    }
}
