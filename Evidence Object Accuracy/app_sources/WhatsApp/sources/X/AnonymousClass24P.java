package X;

/* renamed from: X.24P  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass24P extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;

    public AnonymousClass24P() {
        super(2700, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamStructuredMessage {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageClass", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj2);
        sb.append("}");
        return sb.toString();
    }
}
