package X;

import android.os.Message;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.15o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244515o {
    public final C20670w8 A00;
    public final C14890mD A01;
    public final C14860mA A02;

    public C244515o(C20670w8 r1, C14890mD r2, C14860mA r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public void A00(AnonymousClass1JX r3, String str, int i) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(r3);
        A03(arrayList, i, str);
    }

    public void A01(String str, String str2, List list, int i, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("web", str2);
        A02(str, list, hashMap, null, i, z);
    }

    public void A02(String str, List list, Map map, Map map2, int i, boolean z) {
        String str2;
        String str3 = str;
        C14890mD r2 = this.A01;
        if (r2.A02() || z) {
            AnonymousClass2CW r4 = new AnonymousClass2CW(this, str3, (String) map.get("web"), list, i, z);
            ((AbstractC34011fR) r4).A00 = r2.A00().A03;
            C14860mA r0 = this.A02;
            C34021fS r11 = new C34021fS(r4, r0);
            if (str == null) {
                str3 = r0.A02();
            }
            if (7 == i || 8 == i) {
                str2 = "preempt-";
            } else {
                str2 = "";
            }
            C20670w8 r5 = this.A00;
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(str3);
            r5.A00(new SendWebForwardJob(Message.obtain(null, 0, 45, 0, new AnonymousClass2DD(r11, str3, list, map, map2, i)), sb.toString(), r2.A00().A03));
        }
    }

    public void A03(List list, int i, String str) {
        A01(str, null, list, i, false);
    }
}
