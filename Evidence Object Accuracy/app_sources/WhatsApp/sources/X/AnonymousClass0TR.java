package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0TR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TR {
    public static Map A00 = new HashMap();
    public static Map A01 = new HashMap();

    /* JADX DEBUG: Multi-variable search result rejected for r2v8, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009c, code lost:
        if (r0.booleanValue() != false) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.Class r9) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0TR.A00(java.lang.Class):int");
    }

    public static AbstractC11300g4 A01(Object obj, Constructor constructor) {
        try {
            constructor.newInstance(obj);
            return null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }
}
