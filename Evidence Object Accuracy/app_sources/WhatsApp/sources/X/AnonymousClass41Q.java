package X;

import com.whatsapp.calling.callhistory.CallLogActivity;

/* renamed from: X.41Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41Q extends AnonymousClass2Dn {
    public final /* synthetic */ CallLogActivity A00;

    public AnonymousClass41Q(CallLogActivity callLogActivity) {
        this.A00 = callLogActivity;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        this.A00.A2e();
    }
}
