package X;

/* renamed from: X.2V8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V8 extends AnonymousClass08s {
    public final /* synthetic */ AbstractC14940mI A00;
    public final /* synthetic */ C14930mH A01;

    public AnonymousClass2V8(AbstractC14940mI r1, C14930mH r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXq(AnonymousClass072 r2) {
        this.A00.ASK();
        r2.A09(this);
    }
}
