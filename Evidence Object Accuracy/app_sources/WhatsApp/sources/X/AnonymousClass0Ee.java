package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Ee  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ee extends C02280As {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(24);
    public boolean A00;

    public AnonymousClass0Ee(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt() != 1 ? false : true;
    }

    public AnonymousClass0Ee(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00 ? 1 : 0);
    }
}
