package X;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/* renamed from: X.4j9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98724j9 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Account account = null;
        GoogleSignInAccount googleSignInAccount = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                account = (Account) C95664e9.A07(parcel, Account.CREATOR, readInt);
            } else if (c == 3) {
                i2 = C95664e9.A02(parcel, readInt);
            } else if (c != 4) {
                C95664e9.A0D(parcel, readInt);
            } else {
                googleSignInAccount = (GoogleSignInAccount) C95664e9.A07(parcel, GoogleSignInAccount.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78383op(account, googleSignInAccount, i, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78383op[i];
    }
}
