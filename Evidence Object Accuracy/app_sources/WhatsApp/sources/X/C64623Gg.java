package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/* renamed from: X.3Gg */
/* loaded from: classes2.dex */
public class C64623Gg {
    public int A00;
    public int A01;
    public Canvas A02;
    public final /* synthetic */ AnonymousClass2k6 A03;

    public /* synthetic */ C64623Gg(AnonymousClass2k6 r1) {
        this.A03 = r1;
    }

    public static /* synthetic */ void A00(C64623Gg r6) {
        int i;
        int i2;
        if (r6.A02 != null) {
            int i3 = r6.A00;
            AnonymousClass2k6 r4 = r6.A03;
            C91194Qu[] r0 = r4.A07;
            if (r0 == null) {
                i = 0;
            } else {
                i = r0.length;
            }
            while (true) {
                if (i3 >= i) {
                    i2 = r6.A01;
                    break;
                }
                C91194Qu r2 = r4.A07[i3];
                if (r2 != null) {
                    if (r2.A01.A07.A04 == EnumC869849t.VIEW) {
                        i2 = i3 + 1;
                        break;
                    } else if (r2.A03) {
                        ((Drawable) r2.A02).draw(r6.A02);
                    }
                }
                i3++;
            }
            r6.A00 = i2;
        }
    }
}
