package X;

import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.chromium.net.UrlRequest;

/* renamed from: X.4dA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95114dA {
    public static AnonymousClass20L A00(String str, AlgorithmParameterSpec algorithmParameterSpec, AnonymousClass5ED r6) {
        AnonymousClass20L A03;
        if (algorithmParameterSpec == null || !(algorithmParameterSpec instanceof PBEParameterSpec)) {
            throw C12970iu.A0f("Need a PBEParameter spec with a PBE key.");
        }
        PBEParameterSpec pBEParameterSpec = (PBEParameterSpec) algorithmParameterSpec;
        AnonymousClass5ED.A00(r6);
        int i = r6.type;
        AnonymousClass5ED.A00(r6);
        AbstractC94944cn A01 = A01(i, r6.digest);
        byte[] encoded = r6.getEncoded();
        if (r6.tryWrong) {
            encoded = new byte[2];
        }
        byte[] salt = pBEParameterSpec.getSalt();
        int iterationCount = pBEParameterSpec.getIterationCount();
        A01.A01 = encoded;
        A01.A02 = salt;
        A01.A00 = iterationCount;
        AnonymousClass5ED.A00(r6);
        int i2 = r6.ivSize;
        AnonymousClass5ED.A00(r6);
        if (i2 != 0) {
            int i3 = r6.keySize;
            AnonymousClass5ED.A00(r6);
            A03 = A01.A04(i3, r6.ivSize);
        } else {
            A03 = A01.A03(r6.keySize);
        }
        if (str.startsWith("DES")) {
            AnonymousClass20L r0 = A03;
            if (A03 instanceof C113075Fx) {
                r0 = ((C113075Fx) r0).A00;
            }
            AnonymousClass5OK.A00(((AnonymousClass20K) r0).A00);
        }
        return A03;
    }

    public static AbstractC94944cn A01(int i, int i2) {
        if (i == 0 || i == 4) {
            if (i2 == 0) {
                return new C114995Nw(new AnonymousClass5OC());
            }
            if (i2 == 1) {
                return new C114995Nw(new AnonymousClass5OB());
            }
            if (i2 == 5) {
                return new C114995Nw(new AnonymousClass5GV());
            }
            throw C12960it.A0U("PKCS5 scheme 1 only supports MD2, MD5 and SHA1.");
        } else if (i == 1 || i == 5) {
            switch (i2) {
                case 0:
                    return new C115015Ny(new AnonymousClass5OC());
                case 1:
                    return new C115015Ny(new AnonymousClass5OB());
                case 2:
                    return new C115015Ny(new AnonymousClass5OA());
                case 3:
                    return new C115015Ny(new AnonymousClass5GX());
                case 4:
                    return new C115015Ny(new AnonymousClass5OE());
                case 5:
                    return new C115015Ny(new AnonymousClass5GV());
                case 6:
                    return new C115015Ny(new AnonymousClass5GW());
                case 7:
                    return new C115015Ny(new AnonymousClass5OD());
                case 8:
                    return new C115015Ny(new AnonymousClass5OG());
                case 9:
                    return new C115015Ny(new AnonymousClass5OH());
                case 10:
                    return new C115015Ny(new C113085Fy(224));
                case 11:
                    return new C115015Ny(new C113085Fy(256));
                case 12:
                    return new C115015Ny(new C113085Fy(384));
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    return new C115015Ny(new C113085Fy(512));
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    return new C115015Ny(new AnonymousClass5O9());
                default:
                    throw C12960it.A0U("unknown digest scheme for PBE PKCS5S2 encryption.");
            }
        } else if (i != 2) {
            return new C114985Nv();
        } else {
            switch (i2) {
                case 0:
                    return new C115005Nx(new AnonymousClass5OC());
                case 1:
                    return new C115005Nx(new AnonymousClass5OB());
                case 2:
                    return new C115005Nx(new AnonymousClass5OA());
                case 3:
                    return new C115005Nx(new AnonymousClass5GX());
                case 4:
                    return new C115005Nx(new AnonymousClass5OE());
                case 5:
                    return new C115005Nx(new AnonymousClass5GV());
                case 6:
                    return new C115005Nx(new AnonymousClass5GW());
                case 7:
                    return new C115005Nx(new AnonymousClass5OD());
                case 8:
                    return new C115005Nx(new AnonymousClass5OG());
                case 9:
                    return new C115005Nx(new AnonymousClass5OH());
                default:
                    throw C12960it.A0U("unknown digest scheme for PBE encryption.");
            }
        }
    }

    public static byte[] A02(PBEKeySpec pBEKeySpec, int i) {
        if (i == 2) {
            return AbstractC94944cn.A00(pBEKeySpec.getPassword());
        }
        if (i == 5 || i == 4) {
            return AbstractC94944cn.A01(pBEKeySpec.getPassword());
        }
        char[] password = pBEKeySpec.getPassword();
        if (password == null) {
            return new byte[0];
        }
        int length = password.length;
        byte[] bArr = new byte[length];
        for (int i2 = 0; i2 != length; i2++) {
            bArr[i2] = (byte) password[i2];
        }
        return bArr;
    }
}
