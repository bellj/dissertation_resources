package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2Te  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51182Te extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C51182Te A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;

    static {
        C51182Te r0 = new C51182Te();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        switch (r8.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C51182Te r10 = (C51182Te) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A01;
                int i3 = r10.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r9.Afp(i2, r10.A01, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                int i4 = this.A04;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r9.Afp(i4, r10.A04, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                int i5 = this.A05;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A05 = r9.Afp(i5, r10.A05, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                int i6 = this.A02;
                boolean z8 = false;
                if ((i3 & 8) == 8) {
                    z8 = true;
                }
                this.A02 = r9.Afp(i6, r10.A02, z7, z8);
                boolean z9 = false;
                if ((i & 16) == 16) {
                    z9 = true;
                }
                int i7 = this.A03;
                boolean z10 = false;
                if ((i3 & 16) == 16) {
                    z10 = true;
                }
                this.A03 = r9.Afp(i7, r10.A03, z9, z10);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                this.A00 |= 1;
                                this.A01 = r92.A02();
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A04 = r92.A02();
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A05 = r92.A02();
                            } else if (A03 == 32) {
                                this.A00 |= 8;
                                this.A02 = r92.A02();
                            } else if (A03 == 40) {
                                this.A00 |= 16;
                                this.A03 = r92.A02();
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C51182Te();
            case 5:
                return new C51192Tf();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C51182Te.class) {
                        if (A07 == null) {
                            A07 = new AnonymousClass255(A06);
                        }
                    }
                }
                return A07;
            default:
                throw new UnsupportedOperationException();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A04(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A04(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(3, this.A05);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A04(4, this.A02);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A04(5, this.A03);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A05);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A03);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
