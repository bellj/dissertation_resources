package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5g0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5g0 extends AbstractC124175oj {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final C128575wK A07;
    public final C18590sh A08;
    public final AbstractC14440lR A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;

    public AnonymousClass5g0(Context context, C14900mE r9, C15570nT r10, C18640sm r11, C14830m7 r12, C18650sn r13, C18600si r14, C18610sj r15, C128575wK r16, C18590sh r17, AnonymousClass1BY r18, C22120yY r19, AbstractC14440lR r20, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        super(r11, r14, r15, r18, r19);
        this.A00 = context;
        this.A04 = r12;
        this.A01 = r9;
        this.A02 = r10;
        this.A09 = r20;
        this.A08 = r17;
        this.A06 = r15;
        this.A03 = r11;
        this.A05 = r13;
        this.A0G = str;
        this.A0F = str2;
        this.A0D = str3;
        this.A0C = str4;
        this.A0B = str5;
        this.A0A = str6;
        this.A0E = str7;
        this.A07 = r16;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1W9 r4;
        AnonymousClass01T r7 = (AnonymousClass01T) obj;
        String str = (String) r7.A00;
        C452120p r2 = (C452120p) r7.A01;
        if (str != null) {
            ArrayList A0l = C12960it.A0l();
            C117295Zj.A1M("action", "br-prelink-merchant", A0l);
            String str2 = this.A0G;
            if ("PREPAID".equals(str2)) {
                AnonymousClass009.A04(str);
                r4 = new AnonymousClass1W9("card-token", str);
            } else if ("BANK".equals(str2)) {
                AnonymousClass009.A04(str);
                C117295Zj.A1M("bank-token", str, A0l);
                String str3 = this.A0D;
                AnonymousClass009.A04(str3);
                C117295Zj.A1M("bank-code", str3, A0l);
                String str4 = this.A0C;
                AnonymousClass009.A04(str4);
                C117295Zj.A1M("bank-branch", str4, A0l);
                String str5 = this.A0B;
                AnonymousClass009.A04(str5);
                r4 = new AnonymousClass1W9("bank-account-type", str5);
            } else {
                throw C12960it.A0U("Expecting card token or bank account!");
            }
            A0l.add(r4);
            C117295Zj.A1M("device-id", this.A08.A01(), A0l);
            C117295Zj.A1M("nonce", C117305Zk.A0j(this.A02, this.A04), A0l);
            C117295Zj.A1M("verify-type", str2, A0l);
            C117295Zj.A1M("verify-id", this.A0F, A0l);
            C117305Zk.A1J(this.A06, new IDxRCallbackShape2S0100000_3_I1(this.A00, this.A01, this.A05, this, 4), C117315Zl.A0G(C117305Zk.A1b(A0l)));
            return;
        }
        Log.e(C12960it.A0b("PAY: BrazilMerchantPreLinkAction token error: ", r2));
        this.A07.A00(r2, null);
    }
}
