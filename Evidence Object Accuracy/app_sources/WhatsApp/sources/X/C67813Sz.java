package X;

import android.net.Uri;
import android.text.TextUtils;
import android.util.LogPrinter;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.3Sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67813Sz implements AbstractC116345Vb {
    public static final Uri A01;
    public final LogPrinter A00 = new LogPrinter(4, "GA/LogCatTransport");

    @Override // X.AbstractC116345Vb
    public final Uri Ah3() {
        return A01;
    }

    static {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("uri");
        builder.authority("local");
        A01 = builder.build();
    }

    @Override // X.AbstractC116345Vb
    public final void AhM(AnonymousClass3IT r7) {
        ArrayList A0x = C12980iv.A0x(r7.A0A.values());
        Collections.sort(A0x, new AnonymousClass5C5());
        StringBuilder A0h = C12960it.A0h();
        int size = A0x.size();
        for (int i = 0; i < size; i++) {
            String obj = A0x.get(i).toString();
            if (!TextUtils.isEmpty(obj)) {
                if (A0h.length() != 0) {
                    A0h.append(", ");
                }
                A0h.append(obj);
            }
        }
        this.A00.println(A0h.toString());
    }
}
