package X;

/* renamed from: X.4dn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95474dn {
    public int A00;
    public C95474dn A01;
    public final int A02;
    public final C95474dn A03;
    public final C95044cz A04;
    public final AnonymousClass4YW A05;
    public final boolean A06;

    public C95474dn(C95474dn r3, C95044cz r4, AnonymousClass4YW r5, boolean z) {
        this.A05 = r5;
        this.A06 = z;
        this.A04 = r4;
        int i = r4.A00;
        this.A02 = i == 0 ? -1 : i - 2;
        this.A03 = r3;
        if (r3 != null) {
            r3.A01 = this;
        }
    }

    public static int A00(C95474dn r2, C95474dn r3, C95474dn r4, C95474dn r5) {
        int i = 0;
        if (r2 != null) {
            i = 0 + r2.A06("RuntimeVisibleAnnotations");
        }
        if (r3 != null) {
            i += r3.A06("RuntimeInvisibleAnnotations");
        }
        if (r4 != null) {
            i += r4.A06("RuntimeVisibleTypeAnnotations");
        }
        return r5 != null ? i + r5.A06("RuntimeInvisibleTypeAnnotations") : i;
    }

    public static C95474dn A01(String str, C95474dn r4, AnonymousClass4YW r5) {
        C95044cz r2 = new C95044cz();
        AnonymousClass4YW.A01(str, r2, r5);
        r2.A04(0);
        return new C95474dn(r4, r2, r5, true);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C95474dn A02(java.lang.String r4, X.C95474dn r5, X.AnonymousClass4YW r6, X.AnonymousClass4VT r7, int r8) {
        /*
            X.4cz r3 = new X.4cz
            r3.<init>()
            int r1 = r8 >>> 24
            if (r1 == 0) goto L_0x0025
            r0 = 1
            if (r1 == r0) goto L_0x0025
            switch(r1) {
                case 16: goto L_0x001b;
                case 17: goto L_0x001b;
                case 18: goto L_0x001b;
                case 19: goto L_0x0017;
                case 20: goto L_0x0017;
                case 21: goto L_0x0017;
                case 22: goto L_0x0025;
                case 23: goto L_0x001b;
                default: goto L_0x000f;
            }
        L_0x000f:
            switch(r1) {
                case 66: goto L_0x001b;
                case 67: goto L_0x001b;
                case 68: goto L_0x001b;
                case 69: goto L_0x001b;
                case 70: goto L_0x001b;
                case 71: goto L_0x002b;
                case 72: goto L_0x002b;
                case 73: goto L_0x002b;
                case 74: goto L_0x002b;
                case 75: goto L_0x002b;
                default: goto L_0x0012;
            }
        L_0x0012:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        L_0x0017:
            r3.A02(r1)
            goto L_0x002e
        L_0x001b:
            r0 = 16776960(0xffff00, float:2.3509528E-38)
            r8 = r8 & r0
            int r0 = r8 >> 8
            r3.A07(r1, r0)
            goto L_0x002e
        L_0x0025:
            int r0 = r8 >>> 16
            r3.A04(r0)
            goto L_0x002e
        L_0x002b:
            r3.A03(r8)
        L_0x002e:
            if (r7 != 0) goto L_0x0042
            r0 = 0
            r3.A02(r0)
        L_0x0034:
            X.AnonymousClass4YW.A01(r4, r3, r6)
            r0 = 0
            r3.A04(r0)
            r1 = 1
            X.4dn r0 = new X.4dn
            r0.<init>(r5, r3, r6, r1)
            return r0
        L_0x0042:
            byte[] r2 = r7.A01
            int r1 = r7.A00
            byte r0 = r2[r1]
            int r0 = r0 << 1
            int r0 = r0 + 1
            r3.A0A(r2, r1, r0)
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95474dn.A02(java.lang.String, X.4dn, X.4YW, X.4VT, int):X.4dn");
    }

    public static void A03(C95474dn r1, C95474dn r2, C95474dn r3, C95474dn r4, C95044cz r5, AnonymousClass4YW r6) {
        if (r1 != null) {
            r1.A0A(r5, r6.A02("RuntimeVisibleAnnotations"));
        }
        if (r2 != null) {
            r2.A0A(r5, r6.A02("RuntimeInvisibleAnnotations"));
        }
        if (r3 != null) {
            r3.A0A(r5, r6.A02("RuntimeVisibleTypeAnnotations"));
        }
        if (r4 != null) {
            r4.A0A(r5, r6.A02("RuntimeInvisibleTypeAnnotations"));
        }
    }

    public static void A04(C95044cz r1, AnonymousClass4YW r2, int i, int i2) {
        r1.A07(i2, r2.A07(3, i).A03);
    }

    public static void A05(C95044cz r6, C95474dn[] r7, int i, int i2) {
        int i3 = (i2 << 1) + 1;
        for (int i4 = 0; i4 < i2; i4++) {
            C95474dn r0 = r7[i4];
            i3 += r0 == null ? 0 : r0.A06(null) - 8;
        }
        r6.A04(i);
        r6.A03(i3);
        r6.A02(i2);
        for (int i5 = 0; i5 < i2; i5++) {
            C95474dn r3 = r7[i5];
            C95474dn r2 = null;
            int i6 = 0;
            while (r3 != null) {
                r3.A08();
                i6++;
                r3 = r3.A03;
                r2 = r3;
            }
            r6.A04(i6);
            while (r2 != null) {
                C95044cz.A00(r2.A04, r6);
                r2 = r2.A01;
            }
        }
    }

    public int A06(String str) {
        if (str != null) {
            this.A05.A02(str);
        }
        int i = 8;
        for (C95474dn r1 = this; r1 != null; r1 = r1.A03) {
            i += r1.A04.A00;
        }
        return i;
    }

    public C95474dn A07(String str) {
        this.A00++;
        if (this.A06) {
            AnonymousClass4YW.A01(str, this.A04, this.A05);
        }
        C95044cz r4 = this.A04;
        r4.A07(91, 0);
        return new C95474dn(null, r4, this.A05, false);
    }

    public void A08() {
        int i = this.A02;
        if (i != -1) {
            byte[] bArr = this.A04.A01;
            int i2 = this.A00;
            C72463ee.A0W(bArr, i2, i);
            bArr[i + 1] = (byte) i2;
        }
    }

    public void A09(String str, Object obj) {
        int i;
        C95044cz r3;
        AnonymousClass4YW r0;
        String A06;
        int i2;
        this.A00++;
        if (this.A06) {
            AnonymousClass4YW.A01(str, this.A04, this.A05);
        }
        if (obj instanceof String) {
            r3 = this.A04;
            i = 115;
            r0 = this.A05;
            A06 = (String) obj;
        } else {
            i = 66;
            if (obj instanceof Byte) {
                r3 = this.A04;
                i2 = this.A05.A07(3, ((Number) obj).byteValue()).A03;
                r3.A07(i, i2);
            } else if (obj instanceof Boolean) {
                A04(this.A04, this.A05, C12970iu.A1Y(obj) ? 1 : 0, 90);
                return;
            } else if (obj instanceof Character) {
                A04(this.A04, this.A05, ((Character) obj).charValue(), 67);
                return;
            } else if (obj instanceof Short) {
                A04(this.A04, this.A05, ((Number) obj).shortValue(), 83);
                return;
            } else if (obj instanceof C95574dz) {
                r3 = this.A04;
                i = 99;
                r0 = this.A05;
                A06 = ((C95574dz) obj).A06();
            } else {
                int i3 = 0;
                if (obj instanceof byte[]) {
                    byte[] bArr = (byte[]) obj;
                    C95044cz r32 = this.A04;
                    int length = bArr.length;
                    r32.A07(91, length);
                    while (i3 < length) {
                        A04(r32, this.A05, bArr[i3], 66);
                        i3++;
                    }
                    return;
                } else if (obj instanceof boolean[]) {
                    boolean[] zArr = (boolean[]) obj;
                    C95044cz r33 = this.A04;
                    int length2 = zArr.length;
                    r33.A07(91, length2);
                    while (i3 < length2) {
                        A04(r33, this.A05, zArr[i3] ? 1 : 0, 90);
                        i3++;
                    }
                    return;
                } else if (obj instanceof short[]) {
                    short[] sArr = (short[]) obj;
                    C95044cz r34 = this.A04;
                    int length3 = sArr.length;
                    r34.A07(91, length3);
                    while (i3 < length3) {
                        A04(r34, this.A05, sArr[i3], 83);
                        i3++;
                    }
                    return;
                } else if (obj instanceof char[]) {
                    char[] cArr = (char[]) obj;
                    C95044cz r35 = this.A04;
                    int length4 = cArr.length;
                    r35.A07(91, length4);
                    while (i3 < length4) {
                        A04(r35, this.A05, cArr[i3], 67);
                        i3++;
                    }
                    return;
                } else if (obj instanceof int[]) {
                    int[] iArr = (int[]) obj;
                    C95044cz r4 = this.A04;
                    int length5 = iArr.length;
                    r4.A07(91, length5);
                    while (i3 < length5) {
                        A04(r4, this.A05, iArr[i3], 73);
                        i3++;
                    }
                    return;
                } else if (obj instanceof long[]) {
                    long[] jArr = (long[]) obj;
                    C95044cz r6 = this.A04;
                    int length6 = jArr.length;
                    r6.A07(91, length6);
                    while (i3 < length6) {
                        r6.A07(74, this.A05.A08(5, jArr[i3]).A03);
                        i3++;
                    }
                    return;
                } else if (obj instanceof float[]) {
                    float[] fArr = (float[]) obj;
                    C95044cz r5 = this.A04;
                    int length7 = fArr.length;
                    r5.A07(91, length7);
                    while (i3 < length7) {
                        r5.A07(70, this.A05.A07(4, Float.floatToRawIntBits(fArr[i3])).A03);
                        i3++;
                    }
                    return;
                } else if (obj instanceof double[]) {
                    double[] dArr = (double[]) obj;
                    C95044cz r62 = this.A04;
                    int length8 = dArr.length;
                    r62.A07(91, length8);
                    while (i3 < length8) {
                        r62.A07(68, this.A05.A08(6, Double.doubleToRawLongBits(dArr[i3])).A03);
                        i3++;
                    }
                    return;
                } else {
                    C95404de A09 = this.A05.A09(obj);
                    this.A04.A07(".s.IFJDCS".charAt(A09.A04), A09.A03);
                    return;
                }
            }
        }
        i2 = r0.A02(A06);
        r3.A07(i, i2);
    }

    public void A0A(C95044cz r6, int i) {
        int i2 = 2;
        C95474dn r3 = null;
        int i3 = 0;
        C95474dn r1 = this;
        while (r1 != null) {
            r1.A08();
            i2 += r1.A04.A00;
            i3++;
            r1 = r1.A03;
            r3 = r1;
        }
        r6.A04(i);
        r6.A03(i2);
        r6.A04(i3);
        while (r3 != null) {
            C95044cz.A00(r3.A04, r6);
            r3 = r3.A01;
        }
    }
}
