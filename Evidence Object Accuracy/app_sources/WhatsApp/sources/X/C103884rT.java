package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;

/* renamed from: X.4rT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C103884rT implements AnonymousClass02O {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ StatusesViewModel A01;

    public /* synthetic */ C103884rT(UserJid userJid, StatusesViewModel statusesViewModel) {
        this.A01 = statusesViewModel;
        this.A00 = userJid;
    }

    @Override // X.AnonymousClass02O
    public final Object apply(Object obj) {
        return this.A01.A04(this.A00);
    }
}
