package X;

import android.text.TextUtils;
import com.facebook.msys.mci.DefaultCrypto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3H2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3H2 {
    public final int A00;
    public final long A01;
    public final long A02;
    public final List A03;
    public final Map A04;
    public final boolean A05;
    public final String A06;

    public AnonymousClass3H2(C15050mT r8, List list, Map map, int i, long j, long j2, boolean z) {
        List emptyList;
        String str;
        String obj;
        String A00;
        String obj2;
        C13020j0.A01(r8);
        C13020j0.A01(map);
        this.A02 = j;
        this.A05 = z;
        this.A01 = j2;
        this.A00 = i;
        if (list != null) {
            emptyList = list;
        } else {
            emptyList = Collections.emptyList();
        }
        this.A03 = emptyList;
        String str2 = null;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C100524m3 r2 = (C100524m3) it.next();
                if ("appendVersion".equals(r2.A00)) {
                    str = r2.A01;
                    break;
                }
            }
        }
        str = null;
        this.A06 = true != TextUtils.isEmpty(str) ? str : str2;
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object key = A15.getKey();
            if (!(key == null || !key.toString().startsWith("&") || (A00 = A00(r8, A15.getKey())) == null)) {
                Object value = A15.getValue();
                if (value == null) {
                    obj2 = "";
                } else {
                    obj2 = value.toString();
                }
                int length = obj2.length();
                if (length > 8192) {
                    obj2 = obj2.substring(0, DefaultCrypto.BUFFER_SIZE);
                    r8.A07(Integer.valueOf(length), obj2, "Hit param value is too long and will be trimmed");
                }
                A11.put(A00, obj2);
            }
        }
        Iterator A0n2 = C12960it.A0n(map);
        while (A0n2.hasNext()) {
            Map.Entry A152 = C12970iu.A15(A0n2);
            Object key2 = A152.getKey();
            if (key2 == null || !key2.toString().startsWith("&")) {
                String A002 = A00(r8, A152.getKey());
                if (A002 != null) {
                    Object value2 = A152.getValue();
                    if (value2 == null) {
                        obj = "";
                    } else {
                        obj = value2.toString();
                    }
                    int length2 = obj.length();
                    if (length2 > 8192) {
                        obj = obj.substring(0, DefaultCrypto.BUFFER_SIZE);
                        r8.A07(Integer.valueOf(length2), obj, "Hit param value is too long and will be trimmed");
                    }
                    A11.put(A002, obj);
                }
            }
        }
        if (!TextUtils.isEmpty(this.A06)) {
            C64933Hm.A02("_v", this.A06, A11);
            String str3 = this.A06;
            if (str3.equals("ma4.0.0") || str3.equals("ma4.0.1")) {
                A11.remove("adid");
            }
        }
        this.A04 = Collections.unmodifiableMap(A11);
    }

    public static String A00(C15050mT r5, Object obj) {
        if (obj != null) {
            String obj2 = obj.toString();
            if (obj2.startsWith("&")) {
                obj2 = obj2.substring(1);
            }
            int length = obj2.length();
            if (length > 256) {
                obj2 = obj2.substring(0, 256);
                r5.A07(Integer.valueOf(length), obj2, "Hit param name is too long and will be trimmed");
            }
            if (!TextUtils.isEmpty(obj2)) {
                return obj2;
            }
        }
        return null;
    }

    public final String toString() {
        StringBuilder A0k = C12960it.A0k("ht=");
        A0k.append(this.A02);
        long j = this.A01;
        if (j != 0) {
            A0k.append(", dbId=");
            A0k.append(j);
        }
        int i = this.A00;
        if (i != 0) {
            A0k.append(", appUID=");
            A0k.append(i);
        }
        Map map = this.A04;
        ArrayList A0x = C12980iv.A0x(map.keySet());
        Collections.sort(A0x);
        int size = A0x.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) A0x.get(i2);
            A0k.append(", ");
            A0k.append(str);
            A0k.append("=");
            A0k.append(C12970iu.A0t(str, map));
        }
        return A0k.toString();
    }
}
