package X;

import java.util.Map;

/* renamed from: X.67Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass67Q implements AbstractC136556Mz {
    public Map A00;

    @Override // X.AbstractC136556Mz
    public void A5v(Object obj, Map map, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.A5v(obj, map, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void A9R(Object obj, String str, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.A9R(obj, str, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void A9S(Object obj, String str, String str2, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.A9S(obj, str, str2, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void A9U(Object obj, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.A9U(obj, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void AKq(Object obj, String str, String str2, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.AKq(obj, str, str2, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void AKr(Object obj, String str, Map map, int i, int i2) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.AKr(obj, str, map, i, i2);
        }
    }

    @Override // X.AbstractC136556Mz
    public void AeI(Object obj, String str, int i, int i2, boolean z) {
        AbstractC136556Mz r0 = (AbstractC136556Mz) C12990iw.A0l(this.A00, i);
        if (r0 != null) {
            r0.AeI(obj, str, i, i2, z);
        }
    }
}
