package X;

/* renamed from: X.31P  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31P extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Double A02;
    public Double A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;

    public AnonymousClass31P() {
        super(1584, new AnonymousClass00E(1, 1, 100), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(15, this.A00);
        r3.Abe(7, this.A07);
        r3.Abe(2, this.A03);
        r3.Abe(3, this.A04);
        r3.Abe(10, this.A08);
        r3.Abe(1, this.A09);
        r3.Abe(14, this.A0A);
        r3.Abe(16, this.A05);
        r3.Abe(11, this.A06);
        r3.Abe(13, this.A0B);
        r3.Abe(9, this.A0C);
        r3.Abe(8, this.A0D);
        r3.Abe(6, this.A0E);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMediaStreamPlayback {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesDownloadedStart", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesTransferred", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "didPlay", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "initialBufferingT", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaSize", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallPlayT", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallT", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "playbackCount", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "playbackOrigin", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "playbackState", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "seekCount", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalRebufferingCount", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalRebufferingT", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoDuration", this.A0E);
        return C12960it.A0d("}", A0k);
    }
}
