package X;

import android.opengl.GLES20;
import com.whatsapp.util.Log;

/* renamed from: X.4Yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93054Yu {
    public static void A00(String str) {
        while (true) {
            int glGetError = GLES20.glGetError();
            if (glGetError != 0) {
                Log.e(C12960it.A0e(": glError ", C12960it.A0j(str), glGetError));
            } else {
                return;
            }
        }
    }

    public static void A01(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            throw C12990iw.A0m(C12960it.A0e(": GLES20 error: ", C12960it.A0j(str), glGetError));
        }
    }
}
