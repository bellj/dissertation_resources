package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.180  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass180 {
    public static final int A07;
    public static final String A08;
    public static final byte[] A09 = {0, 10};
    public C47892Dd A00;
    public MappedByteBuffer A01;
    public Map A02;
    public final ExecutorC27271Gr A03;
    public volatile File A04;
    public volatile String A05 = "unknown";
    public volatile boolean A06;

    static {
        String format = String.format(Locale.US, "%07d", Long.valueOf((System.currentTimeMillis() / 1000) % 10000000));
        A08 = format;
        A07 = format.getBytes().length + 1;
    }

    public AnonymousClass180(AbstractC14440lR r3) {
        this.A03 = new ExecutorC27271Gr(r3, true);
    }

    public static String A00(File file) {
        LinkedList linkedList = new LinkedList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            bufferedReader.readLine();
            String readLine = bufferedReader.readLine();
            while (readLine != null && !readLine.equals("\u0000")) {
                linkedList.addFirst(readLine);
                readLine = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException unused) {
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            sb.append((String) it.next());
            sb.append(":");
        }
        return sb.toString();
    }

    public Map A01() {
        if (!this.A06) {
            return new AnonymousClass00N();
        }
        Map map = this.A02;
        if (map != null) {
            return map;
        }
        AnonymousClass00N r5 = new AnonymousClass00N(5);
        for (int i = 0; i < 5; i++) {
            String valueOf = String.valueOf(i);
            if (!valueOf.equals(this.A05)) {
                File file = new File(this.A04, valueOf);
                if (file.exists()) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                        r5.put(bufferedReader.readLine(), file);
                        bufferedReader.close();
                    } catch (IOException unused) {
                    }
                }
            }
        }
        this.A02 = r5;
        return r5;
    }

    public void A02(Object obj, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(obj.getClass().getSimpleName());
        sb.append(" ");
        sb.append(str);
        this.A03.execute(new RunnableBRunnable0Shape0S1100000_I0(24, sb.toString(), this));
    }
}
