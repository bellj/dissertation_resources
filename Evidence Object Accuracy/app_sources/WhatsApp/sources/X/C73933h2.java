package X;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3h2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73933h2 extends Animation {
    public final int A00;
    public final ViewGroup A01;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return false;
    }

    public C73933h2(ViewGroup viewGroup, int i) {
        this.A01 = viewGroup;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = (int) (((float) this.A00) * f);
        Drawable background = this.A01.getBackground();
        if (background instanceof AnonymousClass2Zg) {
            AnonymousClass2Zg r1 = (AnonymousClass2Zg) background;
            r1.A00 = i;
            r1.invalidateSelf();
        }
    }
}
