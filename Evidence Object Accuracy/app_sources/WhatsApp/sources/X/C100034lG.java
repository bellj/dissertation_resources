package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100034lG implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1JV(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1JV[i];
    }
}
