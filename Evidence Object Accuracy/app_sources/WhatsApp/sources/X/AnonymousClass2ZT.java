package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;

/* renamed from: X.2ZT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZT extends Drawable {
    public final int A00;
    public final int A01;
    public final Typeface A02;
    public final TextPaint A03;
    public final CharSequence A04;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public AnonymousClass2ZT(Typeface typeface, CharSequence charSequence, int i, int i2) {
        this.A04 = charSequence;
        this.A02 = typeface;
        TextPaint textPaint = new TextPaint();
        this.A03 = textPaint;
        textPaint.setTextSize((float) i2);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(i);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setAntiAlias(true);
        if (typeface != null) {
            textPaint.setTypeface(typeface);
        }
        this.A01 = (int) Math.ceil((double) textPaint.measureText(charSequence, 0, charSequence.length()));
        this.A00 = textPaint.getFontMetricsInt(null);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        TextPaint textPaint = this.A03;
        int height = (int) (((float) (getBounds().height() >> 1)) - ((textPaint.descent() + textPaint.ascent()) / 2.0f));
        CharSequence charSequence = this.A04;
        canvas.drawText(charSequence, 0, charSequence.length(), (float) (getBounds().width() >> 1), (float) height, textPaint);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.A00;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.A01;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A03.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A03.setColorFilter(colorFilter);
    }
}
