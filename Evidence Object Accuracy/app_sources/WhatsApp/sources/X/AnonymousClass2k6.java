package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.facebook.rendercore.RootHostView;

/* renamed from: X.2k6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2k6 extends AbstractC52532bB {
    public Drawable A00;
    public SparseArray A01;
    public AnonymousClass5Po A02;
    public Object A03;
    public boolean A04;
    public boolean A05;
    public int[] A06 = new int[0];
    public C91194Qu[] A07;
    public C91194Qu[] A08;
    public final C64623Gg A09 = new C64623Gg(this);

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return true;
    }

    public AnonymousClass2k6(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setWillNotDraw(false);
        setChildrenDrawingOrderEnabled(true);
        this.A07 = new C91194Qu[8];
    }

    public static final String A00(C91194Qu[] r5) {
        if (r5 == null) {
            return "<null>";
        }
        StringBuilder A0h = C12960it.A0h();
        for (int i = 0; i < r5.length; i++) {
            C91194Qu r1 = r5[i];
            A0h.append("Item at index: ");
            if (r1 != null) {
                A0h.append(i);
                A0h.append(" Type: ");
                C91194Qu r12 = r5[i];
                A0h.append(r12.A01.A07.A03());
                A0h.append(" Position in parent: ");
                A0h.append(r12.A01.A03);
            } else {
                A0h.append(i);
                A0h.append(" item is null");
            }
            A0h.append("\n");
        }
        return A0h.toString();
    }

    public static void A01(AnonymousClass2k6 r7) {
        int childCount = r7.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = r7.getChildAt(i);
            if (childAt.isLayoutRequested()) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(childAt.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(childAt.getHeight(), 1073741824));
                childAt.layout(childAt.getLeft(), childAt.getTop(), childAt.getRight(), childAt.getBottom());
            }
            if (childAt instanceof AnonymousClass2k6) {
                A01((AnonymousClass2k6) childAt);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001d, code lost:
        if (r4 > -1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0041, code lost:
        if (r2[r4] == null) goto L_0x0043;
     */
    @Override // X.AbstractC52532bB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.C91194Qu r6) {
        /*
            r5 = this;
            X.4Qu[] r1 = r5.A07
            r4 = 0
        L_0x0003:
            int r0 = r1.length
            if (r4 >= r0) goto L_0x000d
            r0 = r1[r4]
            if (r0 == r6) goto L_0x000e
            int r4 = r4 + 1
            goto L_0x0003
        L_0x000d:
            r4 = -1
        L_0x000e:
            r2 = -1
            if (r4 > r2) goto L_0x001f
            X.4Qu[] r1 = r5.A08
            if (r1 == 0) goto L_0x007e
            r4 = 0
        L_0x0016:
            int r0 = r1.length
            if (r4 >= r0) goto L_0x007e
            r0 = r1[r4]
            if (r0 != r6) goto L_0x0077
            if (r4 <= r2) goto L_0x007e
        L_0x001f:
            com.facebook.rendercore.RenderTreeNode r0 = r6.A01
            X.3Ia r0 = r0.A07
            X.49t r2 = r0.A04
            X.49t r0 = X.EnumC869849t.DRAWABLE
            java.lang.Object r1 = r6.A02
            if (r2 != r0) goto L_0x0059
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            r0 = 0
            r1.setCallback(r0)
            android.graphics.Rect r0 = r1.getBounds()
            r5.invalidate(r0)
        L_0x0038:
            X.4Qu[] r3 = r5.A07
            X.4Qu[] r2 = r5.A08
            if (r2 == 0) goto L_0x0043
            r0 = r2[r4]
            r1 = 1
            if (r0 != 0) goto L_0x0044
        L_0x0043:
            r1 = 0
        L_0x0044:
            r0 = 0
            if (r1 == 0) goto L_0x0054
            r2[r4] = r0
        L_0x0049:
            r1 = 0
        L_0x004a:
            int r0 = r2.length
            if (r1 >= r0) goto L_0x007a
            r0 = r2[r1]
            if (r0 != 0) goto L_0x007d
            int r1 = r1 + 1
            goto L_0x004a
        L_0x0054:
            r3[r4] = r0
            if (r2 == 0) goto L_0x007d
            goto L_0x0049
        L_0x0059:
            android.view.View r1 = (android.view.View) r1
            r0 = 1
            r5.A05 = r0
            boolean r0 = r1.isPressed()
            if (r0 == 0) goto L_0x0068
            r0 = 0
            r1.setPressed(r0)
        L_0x0068:
            boolean r0 = r5.A04
            if (r0 == 0) goto L_0x0073
            super.removeViewInLayout(r1)
        L_0x006f:
            r0 = 1
            r5.A05 = r0
            goto L_0x0038
        L_0x0073:
            super.removeView(r1)
            goto L_0x006f
        L_0x0077:
            int r4 = r4 + 1
            goto L_0x0016
        L_0x007a:
            r0 = 0
            r5.A08 = r0
        L_0x007d:
            return
        L_0x007e:
            java.lang.String r4 = "\nScraped items: "
            java.lang.String r3 = "\nMounted items: "
            r2 = 0
            java.lang.String r0 = "Mount item was not found in the list of mounted items.\nItem to remove: "
            java.lang.StringBuilder r1 = X.C12960it.A0j(r0)
            com.facebook.rendercore.RenderTreeNode r0 = r6.A01
            java.lang.String r0 = r0.A00(r2)
            r1.append(r0)
            r1.append(r3)
            X.4Qu[] r0 = r5.A07
            java.lang.String r0 = A00(r0)
            r1.append(r0)
            r1.append(r4)
            X.4Qu[] r0 = r5.A08
            java.lang.String r0 = A00(r0)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2k6.A02(X.4Qu):void");
    }

    @Override // X.AbstractC52532bB
    public void A03(C91194Qu r6, int i) {
        if (r6.A01.A07.A04 == EnumC869849t.DRAWABLE) {
            Drawable drawable = (Drawable) r6.A02;
            drawable.setVisible(C12960it.A1T(getVisibility()), false);
            drawable.setCallback(this);
            C12980iv.A16(drawable, this);
            invalidate(r6.A01.A04);
        } else {
            View view = (View) r6.A02;
            this.A05 = true;
            if (!(view instanceof AnonymousClass2k6) || view.getParent() != this) {
                if (view.getLayoutParams() == null) {
                    view.setLayoutParams(generateDefaultLayoutParams());
                }
                boolean z = this.A04;
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (z) {
                    super.addViewInLayout(view, -1, layoutParams, true);
                } else {
                    super.addView(view, -1, layoutParams);
                }
            } else {
                AnonymousClass028.A0P(view);
                view.setVisibility(0);
            }
        }
        C91194Qu[] r3 = this.A07;
        int length = r3.length;
        if (i >= length) {
            int i2 = length;
            do {
                i2 <<= 1;
            } while (i >= i2);
            C91194Qu[] r1 = new C91194Qu[i2];
            System.arraycopy(r3, 0, r1, 0, length);
            this.A07 = r1;
            r3 = r1;
        }
        r3[i] = r6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
        if (r3[r7] == null) goto L_0x003a;
     */
    @Override // X.AbstractC52532bB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.C91194Qu r6, int r7, int r8) {
        /*
            r5 = this;
            java.lang.Object r2 = r6.A02
            com.facebook.rendercore.RenderTreeNode r0 = r6.A01
            X.3Ia r0 = r0.A07
            X.49t r1 = r0.A04
            X.49t r0 = X.EnumC869849t.DRAWABLE
            if (r1 != r0) goto L_0x0056
            r5.invalidate()
        L_0x000f:
            X.4Qu[] r4 = r5.A07
            int r3 = r4.length
            if (r8 < r3) goto L_0x0022
            r0 = r3
        L_0x0015:
            int r0 = r0 << 1
            if (r8 >= r0) goto L_0x0015
            X.4Qu[] r1 = new X.C91194Qu[r0]
            r0 = 0
            java.lang.System.arraycopy(r4, r0, r1, r0, r3)
            r5.A07 = r1
            r4 = r1
        L_0x0022:
            r1 = r4[r8]
            if (r1 == 0) goto L_0x0031
            X.4Qu[] r0 = r5.A08
            if (r0 != 0) goto L_0x002f
            int r0 = r4.length
            X.4Qu[] r0 = new X.C91194Qu[r0]
            r5.A08 = r0
        L_0x002f:
            r0[r8] = r1
        L_0x0031:
            X.4Qu[] r3 = r5.A08
            if (r3 == 0) goto L_0x003a
            r1 = r3[r7]
            r0 = 1
            if (r1 != 0) goto L_0x003b
        L_0x003a:
            r0 = 0
        L_0x003b:
            r1 = 0
            if (r0 == 0) goto L_0x0051
            r0 = r3[r7]
            r3[r7] = r1
        L_0x0042:
            r4[r8] = r0
            if (r3 == 0) goto L_0x006c
            r1 = 0
        L_0x0047:
            int r0 = r3.length
            if (r1 >= r0) goto L_0x0069
            r0 = r3[r1]
            if (r0 != 0) goto L_0x006c
            int r1 = r1 + 1
            goto L_0x0047
        L_0x0051:
            r0 = r4[r7]
            r4[r7] = r1
            goto L_0x0042
        L_0x0056:
            r0 = 1
            r5.A05 = r0
            r3 = r2
            android.view.View r3 = (android.view.View) r3
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 19
            if (r1 < r0) goto L_0x0065
            r3.cancelPendingInputEvents()
        L_0x0065:
            X.AnonymousClass028.A0Q(r3)
            goto L_0x000f
        L_0x0069:
            r0 = 0
            r5.A08 = r0
        L_0x006c:
            com.facebook.rendercore.RenderTreeNode r0 = r6.A01
            X.3Ia r0 = r0.A07
            X.49t r1 = r0.A04
            X.49t r0 = X.EnumC869849t.VIEW
            if (r1 != r0) goto L_0x007b
            android.view.View r2 = (android.view.View) r2
            X.AnonymousClass028.A0P(r2)
        L_0x007b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2k6.A04(X.4Qu, int, int):void");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        C64623Gg r2 = this.A09;
        r2.A02 = canvas;
        int i = 0;
        r2.A00 = 0;
        C91194Qu[] r0 = r2.A03.A07;
        if (r0 != null) {
            i = r0.length;
        }
        r2.A01 = i;
        super.dispatchDraw(canvas);
        if (r2.A02 != null && r2.A00 < r2.A01) {
            C64623Gg.A00(r2);
        }
        r2.A02 = null;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.draw(canvas);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        C91194Qu[] r0 = this.A07;
        if (r0 != null) {
            int length = r0.length;
            for (int i = 0; i < length; i++) {
                C91194Qu r2 = this.A07[i];
                if (r2 != null && r2.A01.A07.A04 == EnumC869849t.DRAWABLE) {
                    C12980iv.A16((Drawable) r2.A02, this);
                }
            }
        }
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setState(getDrawableState());
        }
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i, int i2) {
        if (this.A05) {
            int childCount = getChildCount();
            if (this.A06.length < childCount) {
                this.A06 = new int[childCount + 5];
            }
            C91194Qu[] r0 = this.A07;
            if (r0 != null) {
                int length = r0.length;
                int i3 = 0;
                for (int i4 = 0; i4 < length; i4++) {
                    C91194Qu r2 = this.A07[i4];
                    if (r2 != null && r2.A01.A07.A04 == EnumC869849t.VIEW) {
                        i3++;
                        this.A06[i3] = indexOfChild((View) r2.A02);
                    }
                }
            }
            this.A05 = false;
        }
        C64623Gg r22 = this.A09;
        if (r22.A02 != null && r22.A00 < r22.A01) {
            C64623Gg.A00(r22);
        }
        return this.A06[i2];
    }

    @Override // X.AbstractC52532bB
    public String getDescriptionOfMountedItems() {
        StringBuilder A0k = C12960it.A0k("\nMounted Items");
        A0k.append(A00(this.A07));
        A0k.append("\nScraped Items: ");
        return C12960it.A0d(A00(this.A08), A0k);
    }

    @Override // X.AbstractC52532bB
    public int getMountItemCount() {
        int i = 0;
        int i2 = 0;
        while (true) {
            C91194Qu[] r1 = this.A07;
            if (i >= r1.length) {
                return i2;
            }
            if (r1[i] != null) {
                i2++;
            }
            i++;
        }
    }

    @Override // android.view.View
    public Object getTag() {
        Object obj = this.A03;
        if (obj == null) {
            return super.getTag();
        }
        return obj;
    }

    @Override // android.view.View
    public Object getTag(int i) {
        Object obj;
        SparseArray sparseArray = this.A01;
        if (sparseArray == null || (obj = sparseArray.get(i)) == null) {
            return super.getTag(i);
        }
        return obj;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        C91194Qu[] r0 = this.A07;
        if (r0 != null) {
            int length = r0.length;
            for (int i = 0; i < length; i++) {
                C91194Qu r2 = this.A07[i];
                if (r2 != null && r2.A01.A07.A04 == EnumC869849t.DRAWABLE) {
                    ((Drawable) r2.A02).jumpToCurrentState();
                }
            }
        }
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return super.onInterceptTouchEvent(motionEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        AnonymousClass3HL r6;
        this.A04 = true;
        if (this instanceof RootHostView) {
            RootHostView rootHostView = (RootHostView) this;
            AnonymousClass4SN r5 = rootHostView.A00;
            int i5 = 0;
            if (r5.A02 && (r6 = r5.A00) != null) {
                r6.A01(View.MeasureSpec.makeMeasureSpec(i3 - i, 1073741824), null, View.MeasureSpec.makeMeasureSpec(i4 - i2, 1073741824));
                r5.A02 = false;
            }
            AnonymousClass3H1 r0 = r5.A01;
            if (r0 != null) {
                AnonymousClass3JO r2 = r5.A04;
                r2.A0B(r0);
                while (true) {
                    AnonymousClass3H1 r1 = r5.A01;
                    if (r0 == r1) {
                        break;
                    } else if (i5 > 4) {
                        C94604cB.A01("RootHostDelegate");
                        break;
                    } else {
                        r2.A0B(r1);
                        i5++;
                        r0 = r1;
                    }
                }
            }
            A01(rootHostView);
        } else if (this instanceof C55982k5) {
            C55982k5 r3 = (C55982k5) this;
            AnonymousClass3H1 r02 = r3.A00;
            if (r02 != null) {
                AnonymousClass3JO r4 = r3.A01;
                r4.A0B(r02);
                int i6 = 0;
                while (true) {
                    AnonymousClass3H1 r12 = r3.A00;
                    if (r02 == r12) {
                        break;
                    } else if (i6 > 4) {
                        C94604cB.A01("RenderTreeHostView");
                        break;
                    } else {
                        r4.A0B(r12);
                        i6++;
                        r02 = r12;
                    }
                }
            }
            A01(r3);
        }
        this.A04 = false;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setBounds(0, 0, getRight(), getBottom());
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        C91194Qu[] r0;
        if (isEnabled() && (r0 = this.A07) != null) {
            for (int length = r0.length - 1; length >= 0; length--) {
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override // android.view.ViewParent, android.view.View
    public void requestLayout() {
        for (ViewParent viewParent = this; viewParent instanceof AnonymousClass2k6; viewParent = viewParent.getParent()) {
            if (!(!((AnonymousClass2k6) viewParent).A04)) {
                return;
            }
        }
        super.requestLayout();
    }

    public void setForegroundCompat(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 23) {
            AnonymousClass4D7.A00(drawable, this);
        } else {
            setForegroundLollipop(drawable);
        }
    }

    private void setForegroundLollipop(Drawable drawable) {
        Drawable drawable2 = this.A00;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
                unscheduleDrawable(this.A00);
            }
            this.A00 = drawable;
            if (drawable != null) {
                drawable.setCallback(this);
                C12980iv.A16(drawable, this);
            }
            invalidate();
        }
    }

    public void setInterceptTouchEventHandler(AnonymousClass5Po r1) {
        this.A02 = r1;
    }

    public void setViewTag(Object obj) {
        this.A03 = obj;
    }

    public void setViewTags(SparseArray sparseArray) {
        this.A01 = sparseArray;
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        C91194Qu[] r0 = this.A07;
        if (r0 != null) {
            int length = r0.length;
            for (int i2 = 0; i2 < length; i2++) {
                C91194Qu r2 = this.A07[i2];
                if (r2 != null && r2.A01.A07.A04 == EnumC869849t.DRAWABLE) {
                    ((Drawable) r2.A02).setVisible(C12960it.A1T(i), false);
                }
            }
        }
    }
}
