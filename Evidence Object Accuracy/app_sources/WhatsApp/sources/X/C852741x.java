package X;

import com.whatsapp.biz.BusinessProfileExtraFieldsActivity;
import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.41x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852741x extends C27131Gd {
    public final /* synthetic */ BusinessProfileExtraFieldsActivity A00;

    public C852741x(BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity) {
        this.A00 = businessProfileExtraFieldsActivity;
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        if (userJid != null) {
            BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity = this.A00;
            if (userJid.equals(businessProfileExtraFieldsActivity.A0C)) {
                businessProfileExtraFieldsActivity.A2e();
            }
        }
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A2e();
    }
}
