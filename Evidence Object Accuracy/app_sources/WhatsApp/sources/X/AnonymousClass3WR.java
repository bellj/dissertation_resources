package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3WR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WR implements AbstractC116675Wj {
    public C91514Sa A00;
    public final C628038q A01;
    public final C18640sm A02;
    public final AbstractC14440lR A03;

    @Override // X.AbstractC116675Wj
    public void AXH() {
    }

    public AnonymousClass3WR(C91514Sa r2, C253318z r3, C18640sm r4, UserJid userJid, C17220qS r6, AbstractC14440lR r7) {
        this.A00 = r2;
        this.A03 = r7;
        this.A02 = r4;
        this.A01 = new C628038q(r3, this, userJid, r6);
    }

    public final void A00(C42351v4 r6) {
        C91514Sa r4 = this.A00;
        if (r4 != null) {
            r4.A01.A02(r4.A00, r4.A02, r6, r4.A04);
            AbstractC115945Tn r0 = r4.A03;
            if (r0 != null) {
                r0.ANQ(r6);
            }
        }
    }

    @Override // X.AbstractC116675Wj
    public void AUW() {
        A00(null);
    }

    @Override // X.AbstractC116675Wj
    public void AXG(C42351v4 r6) {
        if (r6 == null) {
            r6 = null;
        } else {
            int i = r6.A00;
            if (i == 1 || i == 2 || i == 3) {
                C91514Sa r4 = this.A00;
                if (r4 != null) {
                    C48842Hz r3 = r4.A01;
                    AnonymousClass04S r0 = r3.A01;
                    if (r0 != null) {
                        r0.cancel();
                    }
                    r3.A03(r4.A00, r4.A02, r4.A04);
                    return;
                }
                return;
            }
        }
        A00(r6);
    }
}
