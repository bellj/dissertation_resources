package X;

import android.view.View;
import java.util.List;

/* renamed from: X.0PR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PR {
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public List A08 = null;
    public boolean A09;
    public boolean A0A = true;

    public View A00(AnonymousClass0QS r7) {
        List list = this.A08;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                View view = ((AnonymousClass03U) this.A08.get(i)).A0H;
                AnonymousClass0B6 r2 = (AnonymousClass0B6) view.getLayoutParams();
                if ((r2.A00.A00 & 8) == 0 && this.A01 == r2.A00()) {
                    A01(view);
                    return view;
                }
            }
            return null;
        }
        View view2 = r7.A01(this.A01, Long.MAX_VALUE).A0H;
        this.A01 += this.A03;
        return view2;
    }

    public void A01(View view) {
        int i;
        int A00;
        int size = this.A08.size();
        View view2 = null;
        int i2 = Integer.MAX_VALUE;
        int i3 = 0;
        while (true) {
            if (i3 < size) {
                View view3 = ((AnonymousClass03U) this.A08.get(i3)).A0H;
                AnonymousClass0B6 r1 = (AnonymousClass0B6) view3.getLayoutParams();
                if (view3 != view && (r1.A00.A00 & 8) == 0 && (A00 = (r1.A00() - this.A01) * this.A03) >= 0 && A00 < i2) {
                    view2 = view3;
                    if (A00 == 0) {
                        break;
                    }
                    i2 = A00;
                }
                i3++;
            } else if (view2 == null) {
                i = -1;
            }
        }
        i = ((AnonymousClass0B6) view2.getLayoutParams()).A00();
        this.A01 = i;
    }
}
