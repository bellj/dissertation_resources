package X;

/* renamed from: X.0f2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10690f2 implements AbstractC16710pd {
    public AnonymousClass015 A00;
    public final AnonymousClass1WK A01;
    public final AnonymousClass1WK A02;
    public final C71583dA A03;

    public C10690f2(AnonymousClass1WK r1, AnonymousClass1WK r2, C71583dA r3) {
        this.A03 = r3;
        this.A02 = r1;
        this.A01 = r2;
    }

    /* renamed from: A00 */
    public AnonymousClass015 getValue() {
        AnonymousClass015 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass015 A00 = new AnonymousClass02A((AbstractC009404s) this.A01.AJ3(), (AnonymousClass05C) this.A02.AJ3()).A00(AnonymousClass0LY.A00(this.A03));
        this.A00 = A00;
        C16700pc.A0B(A00);
        return A00;
    }
}
