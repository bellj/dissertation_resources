package X;

/* renamed from: X.4at  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93874at {
    public final C91144Qp[] A00;

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        if (r0 > 0) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String A00(java.io.InputStream r5) {
        /*
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
        L_0x0005:
            int r3 = r5.read()
            r2 = 10
            r1 = 13
            if (r3 == r1) goto L_0x0020
            if (r3 == r2) goto L_0x0020
            if (r3 < 0) goto L_0x0018
            char r0 = (char) r3
            r4.append(r0)
            goto L_0x0005
        L_0x0018:
            int r0 = r4.length()
            if (r0 != 0) goto L_0x0038
            r0 = 0
            return r0
        L_0x0020:
            int r0 = r4.length()
            if (r0 == 0) goto L_0x0005
            if (r3 != r1) goto L_0x0038
            r1 = 1
            r5.mark(r1)
            int r0 = r5.read()
            if (r0 != r2) goto L_0x003d
            r5.mark(r1)
        L_0x0035:
            r5.reset()
        L_0x0038:
            java.lang.String r0 = r4.toString()
            return r0
        L_0x003d:
            if (r0 <= 0) goto L_0x0038
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C93874at.A00(java.io.InputStream):java.lang.String");
    }

    public C93874at(String str) {
        this.A00 = new C91144Qp[]{new C91144Qp(str, this), new C91144Qp(C12960it.A0d(str, C12960it.A0k("X509 ")), this), new C91144Qp("PKCS7", this)};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f2, code lost:
        throw X.C12990iw.A0i("malformed PEM data: header/footer mismatch");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0060, code lost:
        if (r3.startsWith(r5.A00) == false) goto L_0x01ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0066, code lost:
        if (r4.length() == 0) goto L_0x01eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0068, code lost:
        r6 = r4.toString();
        r7 = r6.length();
        r5 = new java.io.ByteArrayOutputStream((r7 >> 2) * 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0079, code lost:
        r4 = (X.C113145Ge) X.C88624Gk.A00;
        r11 = new byte[54];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0081, code lost:
        if (r7 <= 0) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0083, code lost:
        r1 = r6.charAt(r7 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008b, code lost:
        if (r1 == '\n') goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008f, code lost:
        if (r1 == '\r') goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0093, code lost:
        if (r1 == '\t') goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0097, code lost:
        if (r1 != ' ') goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0099, code lost:
        r7 = r7 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009d, code lost:
        if (r7 == 0) goto L_0x01c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x009f, code lost:
        r9 = r7;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a1, code lost:
        if (r9 <= 0) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a4, code lost:
        if (r2 == 4) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a6, code lost:
        r1 = r6.charAt(r9 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ae, code lost:
        if (r1 == '\n') goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00b2, code lost:
        if (r1 == '\r') goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b6, code lost:
        if (r1 == '\t') goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ba, code lost:
        if (r1 == ' ') goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00bc, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00be, code lost:
        r9 = r9 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00c1, code lost:
        r0 = r4.A00(r6, 0, r9);
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c6, code lost:
        if (r0 >= r9) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c8, code lost:
        r2 = r4.A01;
        r17 = r2[r6.charAt(r0)];
        r0 = r4.A00(r6, r0 + 1, r9);
        r16 = r2[r6.charAt(r0)];
        r0 = r4.A00(r6, r0 + 1, r9);
        r15 = r2[r6.charAt(r0)];
        r0 = r4.A00(r6, r0 + 1, r9);
        r8 = r0 + 1;
        r3 = r2[r6.charAt(r0)];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00fa, code lost:
        if ((((r17 | r16) | r15) | r3) < 0) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00fc, code lost:
        r13 = r14 + 1;
        X.C72463ee.A0O(r17 << 2, r11, r16 >> 4, r14);
        r2 = r13 + 1;
        X.C72463ee.A0O(r16 << 4, r11, r15 >> 2, r13);
        r14 = r2 + 1;
        X.C72463ee.A0O(r15 << 6, r11, r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0115, code lost:
        if (r14 != 54) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0117, code lost:
        r5.write(r11);
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x011b, code lost:
        r0 = r4.A00(r6, r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0126, code lost:
        throw X.C12990iw.A0i("invalid characters encountered in base64 data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0127, code lost:
        if (r14 <= 0) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0129, code lost:
        r5.write(r11, 0, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x012c, code lost:
        r3 = r4.A00(r6, r0, r7);
        r1 = r4.A00(r6, r3 + 1, r7);
        r2 = r4.A00(r6, r1 + 1, r7);
        r0 = r4.A00(r6, r2 + 1, r7);
        r3 = r6.charAt(r3);
        r1 = r6.charAt(r1);
        r8 = r6.charAt(r2);
        r2 = r6.charAt(r0);
        r0 = r4.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0157, code lost:
        if (r8 != r0) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0159, code lost:
        if (r2 != r0) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015b, code lost:
        r0 = r4.A01;
        r2 = r0[r3];
        r1 = r0[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0163, code lost:
        if ((r2 | r1) < 0) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0165, code lost:
        r5.write((r2 << 2) | (r1 >> 4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0171, code lost:
        throw X.C12990iw.A0i("invalid characters encountered at end of base64 data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0176, code lost:
        throw X.C12990iw.A0i("invalid characters encountered at end of base64 data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0177, code lost:
        if (r2 != r0) goto L_0x019b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0179, code lost:
        r0 = r4.A01;
        r3 = r0[r3];
        r1 = r0[r1];
        r2 = r0[r8];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0184, code lost:
        if (((r3 | r1) | r2) < 0) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0186, code lost:
        r5.write((r3 << 2) | (r1 >> 4));
        r5.write((r1 << 4) | (r2 >> 2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x019a, code lost:
        throw X.C12990iw.A0i("invalid characters encountered at end of base64 data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x019b, code lost:
        r0 = r4.A01;
        r4 = r0[r3];
        r1 = r0[r1];
        r3 = r0[r8];
        r2 = r0[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01a9, code lost:
        if ((((r4 | r1) | r3) | r2) < 0) goto L_0x01c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01ab, code lost:
        r5.write((r4 << 2) | (r1 >> 4));
        r5.write((r1 << 4) | (r3 >> 2));
        r5.write((r3 << 6) | r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01c5, code lost:
        throw X.C12990iw.A0i("invalid characters encountered at end of base64 data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01ce, code lost:
        return X.AbstractC114775Na.A04(r5.toByteArray());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01cf, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01e3, code lost:
        throw new X.AnonymousClass4CS(X.C12960it.A0d(r2.getMessage(), X.C12960it.A0k("unable to decode base64 string: ")), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01ea, code lost:
        throw X.C12990iw.A0i("malformed PEM data encountered");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01eb, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC114775Na A01(java.io.InputStream r19) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C93874at.A01(java.io.InputStream):X.5Na");
    }
}
