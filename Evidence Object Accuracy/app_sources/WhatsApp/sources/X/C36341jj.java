package X;

import android.content.Intent;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.group.NewGroup;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1jj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36341jj extends C36351jk {
    public final /* synthetic */ NewGroup A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36341jj(C14830m7 r9, C21320xE r10, C15650ng r11, C20710wC r12, NewGroup newGroup, C63323Bd r14, C22140ya r15, C14860mA r16) {
        super(r9, r10, r11, r12, r14, r15, r16);
        this.A00 = newGroup;
    }

    @Override // X.C36351jk, X.AbstractC36361jl
    public void AWy(C15580nU r11, C47572Bl r12) {
        NewGroup newGroup = this.A00;
        newGroup.A0c.set(r11);
        File A00 = newGroup.A0A.A00(newGroup.A0b);
        if (A00 != null && A00.exists()) {
            ((ActivityC13810kN) newGroup).A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 21, r11));
        }
        super.AWy(r11, r12);
        Map map = r12.A02;
        if (map.size() > 0) {
            Long l = null;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (Jid jid : map.keySet()) {
                C47582Bm r2 = (C47582Bm) map.get(jid);
                if (r2 != null) {
                    if (l == null) {
                        l = Long.valueOf(r2.A00);
                    }
                    arrayList.add(jid.getRawString());
                    arrayList2.add(r2.A01);
                }
            }
            Set keySet = map.keySet();
            C15580nU r5 = r12.A00;
            Intent intent = new Intent();
            intent.setClassName(newGroup.getPackageName(), "com.whatsapp.invites.InviteGroupParticipantsActivity");
            intent.putExtra("jids", arrayList);
            intent.putExtra("invite_hashes", arrayList2);
            intent.putExtra("invite_expiration", l);
            intent.putExtra("group_jid", r5.getRawString());
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("jids", C15380n4.A06(keySet));
            bundle.putParcelable("invite_intent", intent);
            newGroup.A02 = bundle;
        }
        ((ActivityC13810kN) newGroup).A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(newGroup, 19, r11));
    }

    @Override // X.C36351jk, X.AbstractC36361jl
    public void AXX() {
        Log.i("newgroup/CreateGroupResponseHandler/onTimeout");
        super.AXX();
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape6S0100000_I0_6(this, 49));
    }
}
