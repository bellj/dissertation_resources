package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.storage.StorageUsageGalleryActivity;

/* renamed from: X.1Kj  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Kj implements AbstractC28051Kk {
    public final /* synthetic */ StorageUsageGalleryActivity A00;

    @Override // X.AbstractC28051Kk
    public void AOw(C28011Kc r1, AbstractC14640lm r2) {
    }

    public AnonymousClass1Kj(StorageUsageGalleryActivity storageUsageGalleryActivity) {
        this.A00 = storageUsageGalleryActivity;
    }

    @Override // X.AbstractC28051Kk
    public void ANs(AnonymousClass4KZ r5) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 21, r5.A00));
    }

    @Override // X.AbstractC28051Kk
    public void ANt(C89454Ka r5) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 21, r5.A00));
    }
}
