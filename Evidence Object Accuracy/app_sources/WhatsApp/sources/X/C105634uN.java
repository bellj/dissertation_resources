package X;

import android.app.Application;
import com.whatsapp.jid.UserJid;

/* renamed from: X.4uN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105634uN implements AbstractC009404s {
    public final Application A00;
    public final AnonymousClass19T A01;
    public final AnonymousClass3EX A02;
    public final UserJid A03;

    public C105634uN(Application application, AnonymousClass19T r2, AnonymousClass3EX r3, UserJid userJid) {
        this.A03 = userJid;
        this.A02 = r3;
        this.A00 = application;
        this.A01 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new C53802fF(this.A00, this.A01, this.A02, this.A03);
    }
}
