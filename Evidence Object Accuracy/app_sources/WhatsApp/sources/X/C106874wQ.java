package X;

import java.lang.reflect.Constructor;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.4wQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106874wQ implements AnonymousClass5SK {
    public static final Constructor A00;
    public static final int[] A01 = {5, 4, 12, 8, 3, 10, 9, 11, 6, 2, 0, 1, 7, 14};

    static {
        Constructor constructor = null;
        try {
            if (Boolean.TRUE.equals(Class.forName("com.google.android.exoplayer2.ext.flac.FlacLibrary").getMethod("isAvailable", new Class[0]).invoke(null, new Object[0]))) {
                constructor = Class.forName("com.google.android.exoplayer2.ext.flac.FlacExtractor").asSubclass(AbstractC116785Ww.class).getConstructor(Integer.TYPE);
            }
        } catch (ClassNotFoundException unused) {
        } catch (Exception e) {
            throw new RuntimeException("Error instantiating FLAC extension", e);
        }
        A00 = constructor;
    }

    public final void A00(List list, int i) {
        switch (i) {
            case 0:
                list.add(new C106694w8());
                return;
            case 1:
                list.add(new C106704w9());
                return;
            case 2:
                list.add(new C106714wA());
                return;
            case 3:
                list.add(new C106804wJ());
                return;
            case 4:
                Constructor constructor = A00;
                if (constructor != null) {
                    try {
                        list.add((AbstractC116785Ww) constructor.newInstance(0));
                        return;
                    } catch (Exception e) {
                        throw new IllegalStateException("Unexpected error creating FLAC extractor", e);
                    }
                } else {
                    list.add(new C106764wF());
                    return;
                }
            case 5:
                list.add(new C106774wG());
                return;
            case 6:
                list.add(new C106814wK());
                return;
            case 7:
                list.add(new C106784wH());
                return;
            case 8:
                list.add(new C106824wL());
                list.add(new C106974wa());
                return;
            case 9:
                list.add(new C106744wD());
                return;
            case 10:
                list.add(new C106724wB());
                return;
            case 11:
                list.add(new C106734wC());
                return;
            case 12:
                list.add(new C106794wI());
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            default:
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                list.add(new C106754wE());
                return;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:137:0x0218 A[Catch: all -> 0x0232, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0011, B:7:0x0017, B:9:0x001e, B:10:0x0022, B:11:0x0025, B:12:0x0029, B:14:0x002e, B:17:0x0039, B:20:0x0044, B:24:0x0052, B:25:0x005c, B:28:0x006c, B:30:0x0078, B:33:0x0087, B:35:0x0092, B:38:0x00a0, B:39:0x00a9, B:41:0x00b4, B:43:0x00bd, B:45:0x00c5, B:46:0x00cb, B:47:0x00d3, B:50:0x00e0, B:54:0x00ea, B:55:0x00ed, B:57:0x00f3, B:59:0x00fb, B:61:0x0103, B:64:0x010e, B:66:0x0116, B:68:0x011e, B:71:0x0128, B:74:0x0134, B:77:0x0140, B:79:0x014e, B:81:0x0156, B:84:0x0161, B:86:0x0169, B:88:0x0171, B:90:0x0178, B:92:0x0180, B:94:0x0188, B:96:0x0190, B:98:0x0198, B:100:0x01a0, B:102:0x01a8, B:104:0x01b0, B:106:0x01b8, B:108:0x01be, B:110:0x01c6, B:112:0x01ce, B:114:0x01d6, B:116:0x01de, B:118:0x01e6, B:134:0x020f, B:135:0x0212, B:137:0x0218, B:140:0x021e, B:141:0x0221, B:142:0x0224), top: B:147:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00f3 A[Catch: all -> 0x0232, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0011, B:7:0x0017, B:9:0x001e, B:10:0x0022, B:11:0x0025, B:12:0x0029, B:14:0x002e, B:17:0x0039, B:20:0x0044, B:24:0x0052, B:25:0x005c, B:28:0x006c, B:30:0x0078, B:33:0x0087, B:35:0x0092, B:38:0x00a0, B:39:0x00a9, B:41:0x00b4, B:43:0x00bd, B:45:0x00c5, B:46:0x00cb, B:47:0x00d3, B:50:0x00e0, B:54:0x00ea, B:55:0x00ed, B:57:0x00f3, B:59:0x00fb, B:61:0x0103, B:64:0x010e, B:66:0x0116, B:68:0x011e, B:71:0x0128, B:74:0x0134, B:77:0x0140, B:79:0x014e, B:81:0x0156, B:84:0x0161, B:86:0x0169, B:88:0x0171, B:90:0x0178, B:92:0x0180, B:94:0x0188, B:96:0x0190, B:98:0x0198, B:100:0x01a0, B:102:0x01a8, B:104:0x01b0, B:106:0x01b8, B:108:0x01be, B:110:0x01c6, B:112:0x01ce, B:114:0x01d6, B:116:0x01de, B:118:0x01e6, B:134:0x020f, B:135:0x0212, B:137:0x0218, B:140:0x021e, B:141:0x0221, B:142:0x0224), top: B:147:0x0001 }] */
    @Override // X.AnonymousClass5SK
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AbstractC116785Ww[] A8I(android.net.Uri r9, java.util.Map r10) {
        /*
        // Method dump skipped, instructions count: 678
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106874wQ.A8I(android.net.Uri, java.util.Map):X.5Ww[]");
    }
}
