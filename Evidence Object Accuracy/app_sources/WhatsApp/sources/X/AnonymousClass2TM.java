package X;

import android.net.Uri;

/* renamed from: X.2TM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TM implements AnonymousClass2TK {
    public final int A00;
    public final Uri A01;
    public final C16590pI A02;
    public final AnonymousClass2FO A03;
    public final C19390u2 A04;

    public AnonymousClass2TM(Uri uri, C16590pI r2, AnonymousClass2FO r3, C19390u2 r4, int i) {
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
        this.A01 = uri;
        this.A00 = i;
    }

    @Override // X.AnonymousClass2TK
    public AbstractC35581iK A8O(boolean z) {
        String str;
        AnonymousClass2JH r2;
        Uri uri = this.A01;
        if (uri != null) {
            str = uri.toString();
        } else {
            str = "";
        }
        String str2 = null;
        if (str.startsWith(C617031s.A00.toString())) {
            C16590pI r3 = this.A02;
            C19390u2 r22 = this.A04;
            if (uri != null) {
                str2 = uri.getQueryParameter("bucketId");
            }
            return new C617031s(r3, r22, str2, this.A00);
        }
        if (!z) {
            r2 = new AnonymousClass2JH();
            r2.A04 = true;
        } else {
            int i = this.A00;
            if (uri != null) {
                str2 = uri.getQueryParameter("bucketId");
            }
            r2 = new AnonymousClass2JH();
            r2.A01 = 2;
            r2.A00 = i;
            r2.A02 = 2;
            r2.A03 = str2;
        }
        return this.A03.A00(r2);
    }
}
