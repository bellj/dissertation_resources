package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3DH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3DH {
    public final AbstractC15710nm A00;
    public final C14650lo A01;
    public final C246816l A02;
    public final AnonymousClass19Q A03;
    public final AnonymousClass4JX A04;
    public final AnonymousClass4K9 A05;
    public final AbstractC14440lR A06;

    public AnonymousClass3DH(AbstractC15710nm r2, C14650lo r3, C246816l r4, AnonymousClass19Q r5, AnonymousClass4JX r6, AnonymousClass4K9 r7, AbstractC14440lR r8) {
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r3, 4);
        C16700pc.A0E(r8, 5);
        C16700pc.A0E(r4, 6);
        C16700pc.A0E(r2, 7);
        this.A04 = r6;
        this.A05 = r7;
        this.A03 = r5;
        this.A01 = r3;
        this.A06 = r8;
        this.A02 = r4;
        this.A00 = r2;
    }

    public final AnonymousClass28H A00(AnonymousClass28K r9, String str, String str2, String str3) {
        UserJid userJid = r9.A00;
        int i = this.A04.A00;
        AnonymousClass4K9 r1 = this.A05;
        C16700pc.A0E(userJid, 0);
        int A03 = C12980iv.A03(r1.A00.A08.A0F(userJid) ? 1 : 0) * 9;
        return new AnonymousClass28H(new AnonymousClass28J(r9.A01, str, str2), userJid, str3, this.A03.A00, A03, i, i);
    }
}
