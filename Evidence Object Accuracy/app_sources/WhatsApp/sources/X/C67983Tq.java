package X;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Tq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67983Tq implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final C89254Jg A02;
    public final C14850m9 A03;
    public final AnonymousClass01N A04;

    public C67983Tq(C53042cM r1, C89254Jg r2, C14850m9 r3, AnonymousClass01N r4) {
        this.A03 = r3;
        this.A01 = r1;
        this.A04 = r4;
        this.A02 = r2;
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        AnonymousClass01N r1 = this.A04;
        if (r1.get() == null) {
            return false;
        }
        r1.get();
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (AdK() && this.A00 == null) {
            C53042cM r2 = this.A01;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.coversations_biz_shop_sunset_chat_banner);
            this.A00 = A0F;
            r2.addView(A0F);
        }
        View view = this.A00;
        if (view == null) {
            C53042cM r22 = this.A01;
            view = C12960it.A0F(C12960it.A0E(r22), r22, R.layout.coversations_biz_shop_sunset_chat_banner);
            this.A00 = view;
        }
        C53042cM r6 = this.A01;
        Context context = r6.getContext();
        SpannableStringBuilder A0J = C12990iw.A0J(context.getString(R.string.smb_shop_sunset_banner_description));
        SpannableString spannableString = new SpannableString(context.getString(R.string.learn_more));
        spannableString.setSpan(C12980iv.A0M(context, R.color.link_color), 0, spannableString.length(), 33);
        A0J.append((CharSequence) " ").append((CharSequence) spannableString);
        C12970iu.A0T(view, R.id.banner_description).A0G(null, A0J);
        r6.setBackgroundResource(R.color.chat_banner_background);
        C12960it.A13(r6, this, context, 5);
        C12960it.A10(AnonymousClass028.A0D(view, R.id.close), this, 33);
        view.setVisibility(0);
    }
}
