package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4Gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88634Gl {
    public static final Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A11.put("WhatsAppLongTerm1", new AnonymousClass2ST(new byte[]{20, 35, 117, 87, 77, 10, 88, 113, 102, -86, -25, 30, -66, 81, 100, 55, -60, -94, -117, 115, -29, 105, 92, 108, -31, -9, -7, 84, 93, -88, -18, 107}));
        A00 = Collections.unmodifiableMap(A11);
    }
}
