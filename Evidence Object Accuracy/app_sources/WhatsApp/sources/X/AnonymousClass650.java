package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.650  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass650 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C119215d9 A00;

    public AnonymousClass650(C119215d9 r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C119215d9 r4 = this.A00;
        View view = (View) ((AbstractC15280mr) r4).A05;
        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        if (!r4.isShowing()) {
            r4.showAtLocation(view, 48, 0, 1000000);
            C134146Dm r2 = r4.A0I;
            r2.A02.setVisibility(8);
            View view2 = r2.A01;
            if (view2 != null) {
                view2.setVisibility(8);
            }
        }
    }
}
