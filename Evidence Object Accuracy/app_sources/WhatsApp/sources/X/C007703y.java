package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.core.graphics.drawable.IconCompat;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.03y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C007703y {
    public static volatile AnonymousClass0PX A00;
    public static volatile List A01;

    public static int A00(Context context) {
        if (Build.VERSION.SDK_INT >= 25) {
            return ((ShortcutManager) context.getSystemService(ShortcutManager.class)).getMaxShortcutCountPerActivity();
        }
        return 5;
    }

    public static Intent A01(Context context, C007603x r3) {
        Intent intent;
        if (Build.VERSION.SDK_INT < 26 || (intent = ((ShortcutManager) context.getSystemService(ShortcutManager.class)).createShortcutResultIntent(r3.A02())) == null) {
            intent = new Intent();
        }
        r3.A03(intent);
        return intent;
    }

    public static AnonymousClass0PX A02(Context context) {
        if (A00 == null) {
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    A00 = (AnonymousClass0PX) Class.forName("androidx.sharetarget.ShortcutInfoCompatSaverImpl", false, C007703y.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context);
                } catch (Exception unused) {
                }
            }
            if (A00 == null) {
                A00 = new AnonymousClass0DD();
            }
        }
        return A00;
    }

    public static List A03(Context context) {
        if (Build.VERSION.SDK_INT >= 25) {
            List<ShortcutInfo> dynamicShortcuts = ((ShortcutManager) context.getSystemService(ShortcutManager.class)).getDynamicShortcuts();
            ArrayList arrayList = new ArrayList(dynamicShortcuts.size());
            for (ShortcutInfo shortcutInfo : dynamicShortcuts) {
                arrayList.add(new AnonymousClass03w(context, shortcutInfo).A00());
            }
            return arrayList;
        }
        try {
            return A02(context).A02();
        } catch (Exception unused) {
            return new ArrayList();
        }
    }

    public static List A04(Context context) {
        Bundle bundle;
        String string;
        if (A01 == null) {
            ArrayList arrayList = new ArrayList();
            if (Build.VERSION.SDK_INT >= 21) {
                PackageManager packageManager = context.getPackageManager();
                Intent intent = new Intent("androidx.core.content.pm.SHORTCUT_LISTENER");
                intent.setPackage(context.getPackageName());
                for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 128)) {
                    ActivityInfo activityInfo = resolveInfo.activityInfo;
                    if (!(activityInfo == null || (bundle = activityInfo.metaData) == null || (string = bundle.getString("androidx.core.content.pm.shortcut_listener_impl")) == null)) {
                        try {
                            Class.forName(string, false, C007703y.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context);
                            arrayList.add(null);
                        } catch (Exception unused) {
                        }
                    }
                }
            }
            if (A01 == null) {
                A01 = arrayList;
            }
        }
        return A01;
    }

    public static void A05(Context context) {
        if (Build.VERSION.SDK_INT >= 25) {
            ((ShortcutManager) context.getSystemService(ShortcutManager.class)).removeAllDynamicShortcuts();
        }
        A02(context).A00();
        Iterator it = A04(context).iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public static void A06(Context context, C007603x r5) {
        int i = Build.VERSION.SDK_INT;
        if (i <= 31 && (1 & r5.A01) != 0) {
            return;
        }
        if (i >= 26) {
            ((ShortcutManager) context.getSystemService(ShortcutManager.class)).requestPinShortcut(r5.A02(), null);
        } else if (A08(context)) {
            Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            r5.A03(intent);
            context.sendBroadcast(intent);
        }
    }

    public static void A07(Context context, List list) {
        Bitmap decodeStream;
        IconCompat iconCompat;
        if (Build.VERSION.SDK_INT <= 31) {
            ArrayList arrayList = new ArrayList(list);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C007603x r1 = (C007603x) it.next();
                if ((r1.A01 & 1) != 0) {
                    arrayList.remove(r1);
                }
            }
            list = arrayList;
        }
        int i = Build.VERSION.SDK_INT;
        if (i <= 29) {
            Iterator it2 = new ArrayList(list).iterator();
            while (it2.hasNext()) {
                C007603x r5 = (C007603x) it2.next();
                IconCompat iconCompat2 = r5.A09;
                if (iconCompat2 != null) {
                    int i2 = iconCompat2.A02;
                    if (i2 == 6 || i2 == 4) {
                        InputStream A0B = iconCompat2.A0B(context);
                        if (!(A0B == null || (decodeStream = BitmapFactory.decodeStream(A0B)) == null)) {
                            if (i2 == 6) {
                                iconCompat = new IconCompat(5);
                            } else {
                                iconCompat = new IconCompat(1);
                            }
                            iconCompat.A06 = decodeStream;
                            r5.A09 = iconCompat;
                        }
                    }
                }
                list.remove(r5);
            }
        }
        if (i >= 25) {
            ArrayList arrayList2 = new ArrayList();
            for (C007603x r0 : list) {
                arrayList2.add(r0.A02());
            }
            if (!((ShortcutManager) context.getSystemService(ShortcutManager.class)).addDynamicShortcuts(arrayList2)) {
                return;
            }
        }
        A02(context).A01(list);
        Iterator it3 = A04(context).iterator();
        while (it3.hasNext()) {
            it3.next();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A08(android.content.Context r5) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 < r0) goto L_0x0013
            java.lang.Class<android.content.pm.ShortcutManager> r0 = android.content.pm.ShortcutManager.class
            java.lang.Object r0 = r5.getSystemService(r0)
            android.content.pm.ShortcutManager r0 = (android.content.pm.ShortcutManager) r0
            boolean r0 = r0.isRequestPinShortcutSupported()
            return r0
        L_0x0013:
            java.lang.String r4 = "com.android.launcher.permission.INSTALL_SHORTCUT"
            int r0 = X.AnonymousClass00T.A01(r5, r4)
            r3 = 0
            if (r0 != 0) goto L_0x004d
            android.content.pm.PackageManager r2 = r5.getPackageManager()
            java.lang.String r1 = "com.android.launcher.action.INSTALL_SHORTCUT"
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
            java.util.List r0 = r2.queryBroadcastReceivers(r0, r3)
            java.util.Iterator r2 = r0.iterator()
        L_0x002f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004d
            java.lang.Object r0 = r2.next()
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            android.content.pm.ActivityInfo r0 = r0.activityInfo
            java.lang.String r1 = r0.permission
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x004b
            boolean r0 = r4.equals(r1)
            if (r0 == 0) goto L_0x002f
        L_0x004b:
            r0 = 1
            return r0
        L_0x004d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C007703y.A08(android.content.Context):boolean");
    }
}
