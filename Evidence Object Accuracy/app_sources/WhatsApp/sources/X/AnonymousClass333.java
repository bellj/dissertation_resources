package X;

import android.view.animation.AlphaAnimation;
import com.whatsapp.mediacomposer.VideoComposerFragment;

/* renamed from: X.333  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass333 extends C64453Fp {
    public final /* synthetic */ VideoComposerFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass333(VideoComposerFragment videoComposerFragment) {
        super(videoComposerFragment);
        this.A00 = videoComposerFragment;
    }

    @Override // X.C64453Fp
    public void A01() {
        super.A01();
        AlphaAnimation A0R = C12980iv.A0R();
        A0R.setDuration(300);
        VideoComposerFragment videoComposerFragment = this.A00;
        videoComposerFragment.A09.setVisibility(4);
        videoComposerFragment.A09.startAnimation(A0R);
    }

    @Override // X.C64453Fp
    public void A02() {
        super.A02();
        VideoComposerFragment videoComposerFragment = this.A00;
        if (videoComposerFragment.A09.getVisibility() != 0) {
            AlphaAnimation A0J = C12970iu.A0J();
            A0J.setDuration(300);
            videoComposerFragment.A09.setVisibility(0);
            videoComposerFragment.A09.startAnimation(A0J);
        }
    }
}
