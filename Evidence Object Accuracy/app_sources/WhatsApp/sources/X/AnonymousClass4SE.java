package X;

import android.media.MediaCodecInfo;

/* renamed from: X.4SE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SE {
    public final int A00;
    public final MediaCodecInfo.VideoCapabilities A01;
    public final String A02;
    public final MediaCodecInfo.CodecProfileLevel[] A03;

    public AnonymousClass4SE(MediaCodecInfo.VideoCapabilities videoCapabilities, String str, MediaCodecInfo.CodecProfileLevel[] codecProfileLevelArr, int i) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = videoCapabilities;
        this.A03 = codecProfileLevelArr;
    }
}
