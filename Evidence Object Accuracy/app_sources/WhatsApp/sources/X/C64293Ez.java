package X;

import android.os.Build;
import android.os.Trace;

/* renamed from: X.3Ez  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64293Ez {
    public void A00() {
        if (AnonymousClass4FY.A00 && Build.VERSION.SDK_INT >= 18) {
            Trace.endSection();
        }
    }

    public void A01(Class cls, String str) {
        if (AnonymousClass4FY.A00 && Build.VERSION.SDK_INT >= 18) {
            if (cls != null) {
                str = C12960it.A0d(cls.getSimpleName(), C12960it.A0j(str));
            }
            Trace.beginSection(str);
        }
    }

    public boolean A02() {
        return AnonymousClass4FY.A00 && Build.VERSION.SDK_INT >= 29 && Trace.isEnabled();
    }
}
