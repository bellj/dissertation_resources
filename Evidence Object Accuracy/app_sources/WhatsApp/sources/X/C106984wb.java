package X;

import java.io.EOFException;

/* renamed from: X.4wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106984wb implements AnonymousClass5X6 {
    public final byte[] A00 = new byte[4096];

    @Override // X.AnonymousClass5X6
    public void AA6(C100614mC r1) {
    }

    @Override // X.AnonymousClass5X6
    public void AbG(AnonymousClass4XD r1, int i, int i2, int i3, long j) {
    }

    @Override // X.AnonymousClass5X6
    public /* synthetic */ void AbC(C95304dT r1, int i) {
        r1.A0T(i);
    }

    @Override // X.AnonymousClass5X6
    public void AbD(C95304dT r1, int i, int i2) {
        r1.A0T(i);
    }

    @Override // X.AnonymousClass5X6
    public int AbF(AnonymousClass2BY r4, int i, int i2, boolean z) {
        byte[] bArr = this.A00;
        int read = r4.read(bArr, 0, Math.min(bArr.length, i));
        if (read != -1) {
            return read;
        }
        if (z) {
            return -1;
        }
        throw new EOFException();
    }
}
