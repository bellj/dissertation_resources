package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/* renamed from: X.2QO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QO extends LinearLayout implements ViewGroup.OnHierarchyChangeListener, AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public int[] A02;

    @Override // android.view.ViewGroup.OnHierarchyChangeListener
    public void onChildViewRemoved(View view, View view2) {
    }

    public AnonymousClass2QO(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A02 = new int[4];
        setOnHierarchyChangeListener(this);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.ViewGroup.OnHierarchyChangeListener
    public void onChildViewAdded(View view, View view2) {
        int childCount = getChildCount();
        if (this.A02.length < childCount) {
            this.A02 = new int[childCount];
        }
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        int childCount = getChildCount();
        if (childCount > 1 && size > 0) {
            View childAt = getChildAt(0);
            childAt.measure(View.MeasureSpec.makeMeasureSpec(0, 0), i2);
            int size2 = View.MeasureSpec.getSize(i) - childAt.getMeasuredWidth();
            boolean z = false;
            int i3 = 0;
            for (int i4 = 1; i4 < childCount; i4++) {
                View childAt2 = getChildAt(i4);
                childAt2.measure(View.MeasureSpec.makeMeasureSpec(size2, 0), i2);
                this.A02[i4] = childAt2.getMeasuredWidth();
                i3 += this.A02[i4];
                if (((float) childAt2.getMeasuredWidth()) > ((float) size2) / ((float) (childCount - 1))) {
                    z = true;
                }
            }
            if (z) {
                for (int i5 = 1; i5 < childCount; i5++) {
                    getChildAt(i5).measure(View.MeasureSpec.makeMeasureSpec((this.A02[i5] * size2) / i3, 1073741824), i2);
                }
                setMeasuredDimension(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2));
                return;
            }
        }
        super.onMeasure(i, i2);
    }
}
