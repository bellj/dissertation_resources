package X;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;

/* renamed from: X.12v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237412v {
    public final C15570nT A00;
    public final C16590pI A01;
    public final C14850m9 A02;

    public C237412v(C15570nT r1, C16590pI r2, C14850m9 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean A00() {
        C14850m9 r3 = this.A02;
        if (r3.A07(1500)) {
            Context context = this.A01.A00;
            if (Build.VERSION.SDK_INT >= 17) {
                context = context.createDisplayContext(((DisplayManager) context.getSystemService("display")).getDisplay(0));
            }
            if (context.getResources().getConfiguration().smallestScreenWidthDp >= r3.A02(1742) && !r3.A07(1761)) {
                return true;
            }
        }
        return false;
    }
}
