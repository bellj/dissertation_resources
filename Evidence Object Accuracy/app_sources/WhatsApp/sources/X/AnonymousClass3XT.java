package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.3XT  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3XT implements AbstractC44401yr {
    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r9) {
        int i;
        AbstractC15710nm r2;
        String str;
        AnonymousClass1Q5 A00;
        try {
            JSONObject jSONObject = r9.A04;
            int i2 = r9.A00;
            if (i2 == 0) {
                Object obj = r9.A02.A00;
                if (obj != null) {
                    AnonymousClass32K r4 = (AnonymousClass32K) this;
                    C17570r1 r22 = r4.A04;
                    int i3 = r4.A00;
                    AnonymousClass30Q r1 = new AnonymousClass30Q();
                    r1.A00 = Integer.valueOf(i3);
                    r1.A01 = 1;
                    r1.A02 = -1L;
                    r22.A00.A07(r1);
                    if (!(r4 instanceof C59252uF)) {
                        if (r4 instanceof C59272uH) {
                            C59272uH r42 = (C59272uH) r4;
                            C90834Pk r5 = (C90834Pk) obj;
                            r42.A09.A02("plm_details_view_tag");
                            r42.A01.A07(r42.A04.A00, r5.A02);
                            List list = r5.A01;
                            if (list == null || list.isEmpty()) {
                                AnonymousClass2EK.A00(r42.A02, 4);
                                r2 = r42.A00;
                                str = "GetProductListGraphQLService/onSuccessResponse error";
                            } else {
                                r42.A02.A02(r5);
                                return;
                            }
                        } else if (r4 instanceof C59262uG) {
                            C59262uG r43 = (C59262uG) r4;
                            C90124Mr r52 = (C90124Mr) obj;
                            r43.A0A.A02("view_product_tag");
                            C14650lo r12 = r43.A00;
                            AnonymousClass4TA r6 = r43.A07;
                            UserJid userJid = r6.A00;
                            r12.A07(userJid, r52.A01);
                            C44691zO r13 = r52.A00;
                            if (r13 != null) {
                                r43.A01.A0C(r13, userJid);
                                C92164Uu r44 = r43.A02;
                                r44.A00.A07.A0H(new RunnableBRunnable0Shape1S1200000_I1(r6, r44, r13.A0D, 8));
                                return;
                            }
                            return;
                        } else if (r4 instanceof C59282uI) {
                            C59282uI r45 = (C59282uI) r4;
                            C90114Mq r53 = (C90114Mq) obj;
                            AnonymousClass28H r7 = r45.A05;
                            if (r7.A06 == null && (A00 = C19840ul.A00(r45.A09)) != null) {
                                A00.A07("datasource_catalog");
                            }
                            UserJid userJid2 = r7.A05;
                            C44721zR r23 = r53.A00;
                            r45.A01.A07(userJid2, r53.A01);
                            AbstractC116505Vs r14 = r45.A03;
                            if (r23 != null) {
                                r14.AXA(r23, r7);
                                return;
                            }
                            r14.AQ9(r7, 0);
                            r2 = r45.A00;
                            str = "GetProductCatalogGraphQLService/get product catalog error";
                        } else if (!(r4 instanceof C59242uE)) {
                            C59232uD r46 = (C59232uD) r4;
                            AnonymousClass3CI r54 = (AnonymousClass3CI) obj;
                            if (r54.A00.isEmpty()) {
                                r46.A01.A00.AQJ(r46.A02, 0);
                                return;
                            }
                            r46.A01.A00.AQK(r46.A02, r54);
                            return;
                        } else {
                            C59242uE r47 = (C59242uE) r4;
                            r47.A04();
                            r47.A00.A01((C44651zK) obj, r47.A02);
                            return;
                        }
                        r2.AaV(str, "error_code=0", true);
                        return;
                    }
                    C59252uF r48 = (C59252uF) r4;
                    StringBuilder A0k = C12960it.A0k("GetSingleCollectionGraphQLService/sendRequest/success jid=");
                    C91864Tn r15 = r48.A02;
                    A0k.append(r15.A03);
                    C12960it.A1F(A0k);
                    r48.A04();
                    r48.A00.A01((C90084Mn) obj, r15);
                    return;
                }
                throw C12960it.A0U("No GraphQL Response available");
            } else if (i2 == 1) {
                Map map = r9.A03.A00;
                if (map != null) {
                    AnonymousClass44L r24 = (AnonymousClass44L) this;
                    try {
                        if (!map.isEmpty()) {
                            AnonymousClass3H5 r16 = (AnonymousClass3H5) C12960it.A0o(map).next();
                            switch (r16.A00) {
                                case 2498048:
                                    i = 451;
                                    break;
                                case 2498049:
                                case 2498050:
                                case 2498051:
                                case 2498052:
                                    i = 404;
                                    break;
                                case 2498053:
                                    i = 406;
                                    break;
                                case 2498054:
                                case 2498056:
                                    i = 0;
                                    break;
                                case 2498055:
                                    i = 500;
                                    break;
                                default:
                                    i = -1;
                                    break;
                            }
                            r24.A00(r16, jSONObject, i);
                            return;
                        }
                        throw C12970iu.A0f("GraphQL errors map is empty");
                    } catch (Exception e) {
                        r24.APp(e);
                    }
                } else {
                    throw C12960it.A0U("Error response received but no errors found");
                }
            } else {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(i2);
                throw C12960it.A0U(C12960it.A0d(" is not a valid status code", A0h));
            }
        } catch (Exception e2) {
            APp(e2);
        }
    }
}
