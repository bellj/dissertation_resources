package X;

import android.graphics.Rect;

/* renamed from: X.60B  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60B {
    public static final C125505rM A04 = new C125505rM(1);
    public static final C125505rM A05 = new C125505rM(0);
    public static final C125505rM A06 = new C125505rM(3);
    public static final C125505rM A07 = new C125505rM(5);
    public static final C125505rM A08 = new C125505rM(4);
    public static final C125505rM A09 = new C125505rM(2);
    public Rect A00;
    public boolean A01 = false;
    public boolean A02;
    public boolean A03 = true;

    public Object A00(C125505rM r3) {
        boolean z;
        int i = r3.A00;
        if (i == 0 || i == 1) {
            return null;
        }
        if (i == 2) {
            return this.A00;
        }
        if (i == 3) {
            z = this.A01;
        } else if (i == 4) {
            z = this.A03;
        } else if (i == 5) {
            z = this.A02;
        } else {
            throw C12990iw.A0m(C12960it.A0W(i, "Invalid photo capture request key "));
        }
        return Boolean.valueOf(z);
    }

    public void A01(C125505rM r3, Object obj) {
        int i = r3.A00;
        if (i == 1) {
            return;
        }
        if (i == 2) {
            this.A00 = (Rect) obj;
        } else if (i == 3) {
            this.A01 = C12970iu.A1Y(obj);
        } else if (i == 4) {
            this.A03 = C12970iu.A1Y(obj);
        } else if (i == 5) {
            this.A02 = C12970iu.A1Y(obj);
        } else {
            throw C12990iw.A0m(C12960it.A0W(i, "Failed to set photo capture request value: "));
        }
    }
}
