package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/* renamed from: X.2EH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EH extends AnonymousClass2EI {
    public C90864Pn A00;
    public AnonymousClass2EJ A01;
    public final C14900mE A02;
    public final C48202Ev A03;
    public final C89224Jd A04;
    public final C14820m6 A05;
    public final C19870uo A06;
    public final C17220qS A07;
    public final C19840ul A08;
    public final AbstractC14440lR A09;

    public AnonymousClass2EH(C14900mE r1, C14650lo r2, C48202Ev r3, C89224Jd r4, C14820m6 r5, C19870uo r6, C17220qS r7, C19840ul r8, AbstractC14440lR r9) {
        super(r2);
        this.A04 = r4;
        this.A02 = r1;
        this.A08 = r8;
        this.A07 = r7;
        this.A06 = r6;
        this.A05 = r5;
        this.A03 = r3;
        this.A09 = r9;
    }

    public final void A02(C90864Pn r14) {
        String A01 = this.A07.A01();
        C19870uo r5 = this.A06;
        ArrayList arrayList = new ArrayList();
        for (C92584Wm r11 : r14.A01) {
            ArrayList arrayList2 = new ArrayList();
            C44691zO r8 = r11.A01;
            arrayList2.add(new AnonymousClass1V8("id", r8.A0D, (AnonymousClass1W9[]) null));
            arrayList2.add(new AnonymousClass1V8("name", r8.A04, (AnonymousClass1W9[]) null));
            arrayList2.add(new AnonymousClass1V8("quantity", Long.toString(r11.A00), (AnonymousClass1W9[]) null));
            BigDecimal bigDecimal = r8.A05;
            AnonymousClass3M7 r1 = r8.A02;
            Date date = r14.A02;
            if (bigDecimal != null && (r1 == null || !r1.A00(date) || (bigDecimal = r1.A01) != null)) {
                arrayList2.add(new AnonymousClass1V8("price", Long.toString(bigDecimal.multiply(C30701Ym.A00).longValue()), (AnonymousClass1W9[]) null));
            }
            C30711Yn r0 = r8.A03;
            if (r0 != null) {
                arrayList2.add(new AnonymousClass1V8("currency", r0.A00, (AnonymousClass1W9[]) null));
            }
            arrayList.add(new AnonymousClass1V8("product", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) arrayList2.toArray(new AnonymousClass1V8[0])));
        }
        C14650lo r02 = super.A01;
        UserJid userJid = r14.A00;
        String A012 = r02.A07.A01(userJid);
        if (A012 != null) {
            this.A00 = r14;
            arrayList.add(new AnonymousClass1V8("direct_connection_encrypted_info", A012, (AnonymousClass1W9[]) null));
        }
        r5.A01(this, new AnonymousClass1V8(new AnonymousClass1V8("order", new AnonymousClass1W9[]{new AnonymousClass1W9("op", "create"), new AnonymousClass1W9("biz_jid", userJid.getRawString())}, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("smax_id", "10"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "fb:thrift_iq"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), A01, 252);
        StringBuilder sb = new StringBuilder("CreateOrderProtocol/doSendCreateOrderRequest/biz_jid=");
        sb.append(userJid);
        Log.i(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A08.A02("order_creates_tag");
        this.A02.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 10));
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        this.A02.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 12));
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        this.A02.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 11));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        int intValue;
        this.A08.A02("order_creates_tag");
        Pair A01 = C41151sz.A01(r5);
        C90864Pn r2 = this.A00;
        if (r2 == null || A01 == null || (intValue = ((Number) A01.first).intValue()) != 421) {
            this.A00 = null;
            this.A02.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 14, A01));
            return;
        }
        A01(r2.A00, intValue);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        AnonymousClass2EA r3;
        this.A08.A02("order_creates_tag");
        C89224Jd r5 = this.A04;
        AnonymousClass1V8 A0E = r7.A0E("order");
        if (A0E != null) {
            String A0I = A0E.A0I("id", null);
            String A0I2 = A0E.A0I("token", null);
            AnonymousClass2EC A00 = r5.A00.A00(A0E.A0E("price"));
            if (A0I != null) {
                r3 = new AnonymousClass2EA(A00, A0I, A0I2);
                this.A02.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 15, r3));
            }
        }
        r3 = null;
        this.A02.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 15, r3));
    }
}
