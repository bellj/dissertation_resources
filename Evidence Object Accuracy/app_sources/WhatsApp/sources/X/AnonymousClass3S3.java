package X;

import com.whatsapp.biz.catalog.CatalogMediaViewFragment;

/* renamed from: X.3S3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S3 implements AnonymousClass070 {
    public final /* synthetic */ CatalogMediaViewFragment A00;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public AnonymousClass3S3(CatalogMediaViewFragment catalogMediaViewFragment) {
        this.A00 = catalogMediaViewFragment;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        CatalogMediaViewFragment catalogMediaViewFragment = this.A00;
        catalogMediaViewFragment.A03.A03(catalogMediaViewFragment.A07, 30, catalogMediaViewFragment.A02.A0D, 11);
    }
}
