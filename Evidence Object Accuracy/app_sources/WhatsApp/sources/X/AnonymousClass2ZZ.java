package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZZ extends Drawable {
    public Bitmap A00;
    public Canvas A01;
    public Path A02 = null;
    public final Matrix A03 = C13000ix.A01();
    public final Paint A04;
    public final Drawable A05;
    public final AbstractC469028d A06;

    public AnonymousClass2ZZ(Resources.Theme theme, Resources resources, AbstractC469028d r5, int i) {
        Drawable A04 = AnonymousClass00X.A04(theme, resources, i);
        AnonymousClass009.A05(A04);
        this.A05 = A04;
        this.A06 = r5;
        Paint A0F = C12990iw.A0F();
        A0F.setDither(true);
        A0F.setFilterBitmap(true);
        A0F.setAntiAlias(true);
        this.A04 = A0F;
    }

    public AnonymousClass2ZZ(Resources resources, Bitmap bitmap, AbstractC469028d r5) {
        this.A05 = new BitmapDrawable(resources, bitmap);
        this.A06 = r5;
        Paint A0F = C12990iw.A0F();
        A0F.setDither(true);
        A0F.setFilterBitmap(true);
        A0F.setAntiAlias(true);
        this.A04 = A0F;
    }

    public AnonymousClass2ZZ(Drawable drawable, AbstractC469028d r4) {
        this.A05 = drawable;
        this.A06 = r4;
        Paint A0F = C12990iw.A0F();
        A0F.setDither(true);
        A0F.setFilterBitmap(true);
        A0F.setAntiAlias(true);
        this.A04 = A0F;
    }

    @Override // android.graphics.drawable.Drawable
    public void applyTheme(Resources.Theme theme) {
        this.A05.applyTheme(theme);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean canApplyTheme() {
        return this.A05.canApplyTheme();
    }

    @Override // android.graphics.drawable.Drawable
    public void clearColorFilter() {
        this.A05.clearColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (this.A02 == null) {
            this.A02 = (Path) this.A06.apply(new RectF(getBounds()));
        }
        if (this.A01 != null) {
            Paint paint = this.A04;
            if (paint.getShader() == null) {
                this.A05.draw(this.A01);
                Bitmap bitmap = this.A00;
                AnonymousClass009.A05(bitmap);
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
            }
        }
        Path path = this.A02;
        if (path == null || this.A00 == null) {
            this.A05.draw(canvas);
        } else {
            canvas.drawPath(path, this.A04);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A05.getAlpha();
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        return this.A05.getChangingConfigurations();
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        return this.A05.getColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.A05.getConstantState();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable getCurrent() {
        return this.A05.getCurrent();
    }

    @Override // android.graphics.drawable.Drawable
    public Rect getDirtyBounds() {
        return this.A05.getDirtyBounds();
    }

    @Override // android.graphics.drawable.Drawable
    public void getHotspotBounds(Rect rect) {
        this.A05.getHotspotBounds(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.A05.getIntrinsicHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.A05.getIntrinsicWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getLayoutDirection() {
        return this.A05.getLayoutDirection();
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumHeight() {
        return this.A05.getMinimumHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumWidth() {
        return this.A05.getMinimumWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.A05.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public Insets getOpticalInsets() {
        return this.A05.getOpticalInsets();
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        this.A05.getOutline(outline);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        return this.A05.getPadding(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public int[] getState() {
        return this.A05.getState();
    }

    @Override // android.graphics.drawable.Drawable
    public Region getTransparentRegion() {
        return this.A05.getTransparentRegion();
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        super.invalidateSelf();
        this.A05.invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        return this.A05.isAutoMirrored();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isFilterBitmap() {
        return this.A05.isFilterBitmap();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isProjected() {
        return this.A05.isProjected();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return this.A05.isStateful();
    }

    @Override // android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        this.A05.jumpToCurrentState();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        return this.A05.mutate();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009c, code lost:
        if (r1 != null) goto L_0x009e;
     */
    @Override // android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onBoundsChange(android.graphics.Rect r7) {
        /*
            r6 = this;
            super.onBoundsChange(r7)
            android.graphics.RectF r1 = new android.graphics.RectF
            r1.<init>(r7)
            X.28d r0 = r6.A06
            java.lang.Object r0 = r0.apply(r1)
            android.graphics.Path r0 = (android.graphics.Path) r0
            r6.A02 = r0
            r4 = 0
            r6.A00 = r4
            r6.A01 = r4
            android.graphics.Paint r3 = r6.A04
            r3.setShader(r4)
            android.graphics.drawable.Drawable r1 = r6.A05
            r1.setBounds(r7)
            boolean r0 = r1 instanceof android.graphics.drawable.BitmapDrawable
            if (r0 == 0) goto L_0x0086
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r0 = r1.getBitmap()
            r6.A00 = r0
        L_0x002d:
            android.graphics.Path r0 = r6.A02
            if (r0 == 0) goto L_0x00a6
            android.graphics.Bitmap r1 = r6.A00
            if (r1 == 0) goto L_0x00a6
            android.graphics.Canvas r0 = r6.A01
            if (r0 != 0) goto L_0x00a6
            android.graphics.Shader$TileMode r0 = android.graphics.Shader.TileMode.CLAMP
            android.graphics.BitmapShader r5 = new android.graphics.BitmapShader
            r5.<init>(r1, r0, r0)
            android.graphics.Bitmap r0 = r6.A00
            int r2 = r0.getWidth()
            int r0 = r7.width()
            r1 = 1065353216(0x3f800000, float:1.0)
            if (r2 == r0) goto L_0x0083
            int r0 = r7.width()
            float r4 = (float) r0
            android.graphics.Bitmap r0 = r6.A00
            int r0 = r0.getWidth()
            float r0 = (float) r0
            float r4 = r4 / r0
        L_0x005b:
            android.graphics.Bitmap r0 = r6.A00
            int r2 = r0.getHeight()
            int r0 = r7.height()
            if (r2 == r0) goto L_0x0074
            int r0 = r7.height()
            float r1 = (float) r0
            android.graphics.Bitmap r0 = r6.A00
            int r0 = r0.getHeight()
            float r0 = (float) r0
            float r1 = r1 / r0
        L_0x0074:
            android.graphics.Matrix r0 = r6.A03
            r0.reset()
            r0.preScale(r4, r1)
            r5.setLocalMatrix(r0)
            r3.setShader(r5)
            return
        L_0x0083:
            r4 = 1065353216(0x3f800000, float:1.0)
            goto L_0x005b
        L_0x0086:
            android.graphics.Bitmap r1 = r6.A00
            if (r1 != 0) goto L_0x009e
            int r2 = r7.width()
            int r1 = r7.height()
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch: OutOfMemoryError -> 0x0099
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r2, r1, r0)     // Catch: OutOfMemoryError -> 0x0099
            goto L_0x009a
        L_0x0099:
            r1 = 0
        L_0x009a:
            r6.A00 = r1
            if (r1 == 0) goto L_0x002d
        L_0x009e:
            android.graphics.Canvas r0 = new android.graphics.Canvas
            r0.<init>(r1)
            r6.A01 = r0
            goto L_0x002d
        L_0x00a6:
            r3.setShader(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2ZZ.onBoundsChange(android.graphics.Rect):void");
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        return this.A05.onLayoutDirectionChanged(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void scheduleSelf(Runnable runnable, long j) {
        this.A05.scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A05.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        this.A05.setAutoMirrored(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setChangingConfigurations(int i) {
        this.A05.setChangingConfigurations(i);
    }

    @Override // android.graphics.drawable.Drawable
    @Deprecated
    public void setColorFilter(int i, PorterDuff.Mode mode) {
        this.A05.setColorFilter(i, mode);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A05.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    @Deprecated
    public void setDither(boolean z) {
        this.A05.setDither(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setFilterBitmap(boolean z) {
        this.A05.setFilterBitmap(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspot(float f, float f2) {
        this.A05.setHotspot(f, f2);
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.A05.setHotspotBounds(i, i2, i3, i4);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        return this.A05.setState(iArr);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTint(int i) {
        this.A05.setTint(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintBlendMode(BlendMode blendMode) {
        this.A05.setTintBlendMode(blendMode);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintList(ColorStateList colorStateList) {
        this.A05.setTintList(colorStateList);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintMode(PorterDuff.Mode mode) {
        this.A05.setTintMode(mode);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        return this.A05.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Drawable
    public void unscheduleSelf(Runnable runnable) {
        this.A05.unscheduleSelf(runnable);
    }
}
