package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;

/* renamed from: X.2h2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54632h2 extends AbstractC018308n {
    public final int A00;
    public final int A01;
    public final boolean A02;
    public final /* synthetic */ AnonymousClass1KW A03;

    public C54632h2(AnonymousClass1KW r1, int i, int i2, boolean z) {
        this.A03 = r1;
        this.A02 = z;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r8, RecyclerView recyclerView) {
        int i;
        super.A01(rect, view, r8, recyclerView);
        AnonymousClass009.A0E(recyclerView instanceof ShapePickerRecyclerView);
        ShapePickerRecyclerView shapePickerRecyclerView = (ShapePickerRecyclerView) recyclerView;
        int A00 = RecyclerView.A00(view);
        if (A00 != -1) {
            C94834cc A0E = this.A03.A0I.A0E(A00);
            shapePickerRecyclerView.A0y();
            int i2 = shapePickerRecyclerView.A01;
            int actualShapeSpacing = shapePickerRecyclerView.getActualShapeSpacing();
            int i3 = A0E.A02;
            if (i3 == 0) {
                int i4 = A0E.A01 % i2;
                int i5 = (i4 * actualShapeSpacing) / i2;
                int i6 = actualShapeSpacing - (((i4 + 1) * actualShapeSpacing) / i2);
                boolean z = this.A02;
                int i7 = i5;
                if (z) {
                    i7 = i6;
                }
                rect.left = i7;
                if (!z) {
                    i5 = i6;
                }
                rect.right = i5;
                i = this.A01;
            } else if (i3 == 1) {
                i = this.A00;
            } else {
                return;
            }
            rect.top = i;
        }
    }
}
