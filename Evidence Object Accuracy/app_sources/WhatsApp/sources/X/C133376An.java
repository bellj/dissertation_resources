package X;

/* renamed from: X.6An  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133376An implements AnonymousClass6MV {
    public final /* synthetic */ C129365xb A00;
    public final /* synthetic */ C128925wt A01;

    public C133376An(C129365xb r1, C128925wt r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        this.A01.A00(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r5) {
        C128545wH r3 = new C128545wH(r5);
        C129365xb r2 = this.A00;
        r2.A00(r3, this.A01, r2.A0C);
    }
}
