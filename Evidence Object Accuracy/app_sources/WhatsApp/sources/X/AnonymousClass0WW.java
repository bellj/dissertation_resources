package X;

import android.view.View;
import android.view.ViewConfiguration;

/* renamed from: X.0WW  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0WW implements View.OnTouchListener, View.OnAttachStateChangeListener {
    public int A00;
    public Runnable A01;
    public Runnable A02;
    public boolean A03;
    public final float A04;
    public final int A05;
    public final int A06;
    public final View A07;
    public final int[] A08 = new int[2];

    public abstract AbstractC12600iB A00();

    public abstract boolean A03();

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public AnonymousClass0WW(View view) {
        this.A07 = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.A04 = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        int tapTimeout = ViewConfiguration.getTapTimeout();
        this.A06 = tapTimeout;
        this.A05 = (tapTimeout + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    public final void A01() {
        Runnable runnable = this.A02;
        if (runnable != null) {
            this.A07.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.A01;
        if (runnable2 != null) {
            this.A07.removeCallbacks(runnable2);
        }
    }

    public boolean A02() {
        AbstractC12600iB A00 = A00();
        if (A00 == null || !A00.AK4()) {
            return true;
        }
        A00.dismiss();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0055, code lost:
        if (r2 == 3) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005a, code lost:
        if (r0 != false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005c, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0069, code lost:
        if (A02() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0081, code lost:
        if (r1 != 3) goto L_0x0083;
     */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r15, android.view.MotionEvent r16) {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WW.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        this.A03 = false;
        this.A00 = -1;
        Runnable runnable = this.A01;
        if (runnable != null) {
            this.A07.removeCallbacks(runnable);
        }
    }
}
