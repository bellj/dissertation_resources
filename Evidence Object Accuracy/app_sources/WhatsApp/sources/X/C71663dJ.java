package X;

import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3dJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71663dJ extends Thread {
    public final /* synthetic */ Map A00;

    public C71663dJ(Map map) {
        this.A00 = map;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public final void run() {
        Throwable e;
        String str;
        StringBuilder sb;
        String str2;
        Map map = this.A00;
        Uri.Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204?id=gmob-apps").buildUpon();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            buildUpon.appendQueryParameter(A0x, C12970iu.A0t(A0x, map));
        }
        String obj = buildUpon.build().toString();
        try {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(obj).openConnection();
                try {
                    int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode < 200 || responseCode >= 300) {
                        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(obj) + 65);
                        A0t.append("Received non-success response code ");
                        A0t.append(responseCode);
                        A0t.append(" from pinging URL: ");
                        Log.w("HttpUrlPinger", C12960it.A0d(obj, A0t));
                    }
                } finally {
                    httpURLConnection.disconnect();
                }
            } catch (IOException | RuntimeException e2) {
                e = e2;
                str = e.getMessage();
                sb = C12960it.A0i(str, C12970iu.A07(obj) + 27);
                str2 = "Error while pinging URL: ";
                C12960it.A1L(str2, obj, ". ", sb);
                Log.w("HttpUrlPinger", C12960it.A0d(str, sb), e);
            }
        } catch (IndexOutOfBoundsException e3) {
            e = e3;
            str = e.getMessage();
            sb = C12960it.A0i(str, C12970iu.A07(obj) + 32);
            str2 = "Error while parsing ping URL: ";
            C12960it.A1L(str2, obj, ". ", sb);
            Log.w("HttpUrlPinger", C12960it.A0d(str, sb), e);
        }
    }
}
