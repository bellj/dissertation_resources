package X;

import android.content.Intent;
import android.os.ConditionVariable;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0510000_I0;
import com.whatsapp.Mp4Ops;
import com.whatsapp.jobqueue.job.messagejob.ProcessVCardMessageJob;
import com.whatsapp.media.download.ExpressPathGarbageCollectWorker;
import com.whatsapp.media.download.service.MediaDownloadService;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0yy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22370yy {
    public static final AnonymousClass00E A0w = new AnonymousClass00E(1, 60, 200);
    public long A00;
    public final ConditionVariable A01 = new ConditionVariable(true);
    public final C21040wk A02;
    public final AbstractC15710nm A03;
    public final C14330lG A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C20870wS A07;
    public final Mp4Ops A08;
    public final C002701f A09;
    public final C15450nH A0A;
    public final C18790t3 A0B;
    public final C20670w8 A0C;
    public final C14620lj A0D = new C14620lj();
    public final C14830m7 A0E;
    public final C16590pI A0F;
    public final C14950mJ A0G;
    public final C15650ng A0H;
    public final C15660nh A0I;
    public final AnonymousClass12H A0J;
    public final C22340yv A0K;
    public final C22180yf A0L;
    public final C14850m9 A0M;
    public final C20110vE A0N;
    public final C19940uv A0O;
    public final C14410lO A0P;
    public final AnonymousClass14P A0Q;
    public final AnonymousClass155 A0R;
    public final C17040qA A0S;
    public final C14420lP A0T;
    public final AnonymousClass12J A0U;
    public final AnonymousClass1CH A0V;
    public final C43131wS A0W = new C43131wS(this);
    public final AnonymousClass11X A0X;
    public final AnonymousClass104 A0Y;
    public final C239713s A0Z;
    public final C20660w7 A0a;
    public final C16630pM A0b;
    public final AnonymousClass1EK A0c;
    public final AnonymousClass160 A0d;
    public final C22600zL A0e;
    public final C22900zp A0f;
    public final C15860o1 A0g;
    public final C22590zK A0h;
    public final C26511Dt A0i;
    public final AnonymousClass19O A0j;
    public final C26481Dq A0k;
    public final C26521Du A0l;
    public final AbstractC14440lR A0m;
    public final AnonymousClass1MO A0n;
    public final C26501Ds A0o;
    public final C21710xr A0p;
    public final HashMap A0q = new HashMap();
    public final HashMap A0r = new HashMap();
    public final HashMap A0s = new HashMap();
    public final Set A0t = new HashSet();
    public final Executor A0u;
    public final Executor A0v;

    public C22370yy(C21040wk r6, AbstractC15710nm r7, C14330lG r8, C14900mE r9, C15570nT r10, C20870wS r11, Mp4Ops mp4Ops, C002701f r13, C15450nH r14, C18790t3 r15, C20670w8 r16, C14830m7 r17, C16590pI r18, C14950mJ r19, C15650ng r20, C15660nh r21, AnonymousClass12H r22, C22340yv r23, C22180yf r24, C14850m9 r25, C20110vE r26, C19940uv r27, C14410lO r28, AnonymousClass14P r29, AnonymousClass155 r30, C17040qA r31, C14420lP r32, AnonymousClass12J r33, AnonymousClass1CH r34, AnonymousClass11X r35, AnonymousClass104 r36, C239713s r37, C20660w7 r38, C16630pM r39, AnonymousClass1EK r40, AnonymousClass160 r41, C22600zL r42, C22900zp r43, C15860o1 r44, C22590zK r45, C26511Dt r46, AnonymousClass19O r47, C26481Dq r48, C26521Du r49, AbstractC14440lR r50, C26501Ds r51, C21710xr r52) {
        AnonymousClass1MO r2 = null;
        this.A0E = r17;
        this.A08 = mp4Ops;
        this.A0M = r25;
        this.A05 = r9;
        this.A06 = r10;
        this.A03 = r7;
        this.A0F = r18;
        this.A0m = r50;
        this.A04 = r8;
        this.A0Q = r29;
        this.A0B = r15;
        this.A02 = r6;
        this.A0a = r38;
        this.A0A = r14;
        this.A0o = r51;
        this.A0P = r28;
        this.A0G = r19;
        this.A0C = r16;
        this.A0e = r42;
        this.A0i = r46;
        this.A0L = r24;
        this.A07 = r11;
        this.A0Z = r37;
        this.A0l = r49;
        this.A0V = r34;
        this.A0h = r45;
        this.A0p = r52;
        this.A0R = r30;
        this.A0H = r20;
        this.A0f = r43;
        this.A0J = r22;
        this.A0g = r44;
        this.A0T = r32;
        this.A0I = r21;
        this.A0j = r47;
        this.A0K = r23;
        this.A0U = r33;
        this.A0S = r31;
        this.A0k = r48;
        this.A0Y = r36;
        this.A0O = r27;
        this.A0N = r26;
        this.A0b = r39;
        this.A0X = r35;
        this.A0d = r41;
        this.A09 = r13;
        this.A0c = r40;
        this.A0u = new ExecutorC39691qM(r9);
        this.A0v = new ExecutorC41461tZ(r50);
        this.A0n = r25.A02(776) > 0 ? new AnonymousClass1MO(r50, r25.A02(776), false) : r2;
    }

    public static final void A00(C28781Oz r4, AbstractC16130oV r5, boolean z) {
        Boolean bool;
        boolean z2;
        String str;
        byte[] bArr;
        Integer num;
        Integer num2;
        Long l;
        String str2;
        Integer num3;
        Integer num4;
        Integer num5;
        Long l2;
        Long l3;
        Integer num6;
        Integer num7;
        Integer num8;
        String str3;
        Long l4;
        Integer num9;
        Integer num10;
        byte[] bArr2;
        String str4;
        Boolean bool2;
        synchronized (r5) {
            C16150oX r2 = r5.A02;
            AnonymousClass009.A05(r2);
            AnonymousClass1RN A01 = r4.A01();
            AnonymousClass009.A05(A01);
            r2.A0a = false;
            r2.A0Z = false;
            r2.A0Y = false;
            r2.A0X = false;
            r2.A0W = z;
            synchronized (r4) {
                bool = r4.A04;
            }
            if (bool != null) {
                synchronized (r4) {
                    bool2 = r4.A04;
                }
                r2.A0L = bool2.booleanValue();
            }
            if (r4.A04() != null) {
                r2.A0P = r4.A04().booleanValue();
            }
            synchronized (r4) {
                z2 = r4.A0I;
            }
            if (z2) {
                if (r5.A14() != null) {
                    r5.A14().A01();
                }
                r2.A0K = null;
                r2.A0J = null;
            }
            r2.A07 = r4.A00();
            synchronized (r4) {
                str = r4.A0F;
            }
            if (str != null) {
                synchronized (r4) {
                    str4 = r4.A0F;
                }
                r2.A0I = str4;
            }
            synchronized (r4) {
                bArr = r4.A0K;
            }
            if (bArr != null) {
                C30061Vy r1 = (C30061Vy) r5;
                synchronized (r4) {
                    bArr2 = r4.A0K;
                }
                r1.A01 = AnonymousClass1KB.A00(bArr2);
            }
            synchronized (r4) {
                num = r4.A09;
            }
            if (num != null) {
                synchronized (r4) {
                    num10 = r4.A09;
                }
                r2.A02 = num10.intValue();
            }
            synchronized (r4) {
                num2 = r4.A0A;
            }
            if (num2 != null) {
                synchronized (r4) {
                    num9 = r4.A0A;
                }
                r2.A03 = num9.intValue();
            }
            synchronized (r4) {
                l = r4.A0D;
            }
            if (l != null) {
                synchronized (r4) {
                    l4 = r4.A0D;
                }
                r2.A09 = l4.longValue();
            }
            synchronized (r4) {
                str2 = r4.A0G;
            }
            if (str2 != null) {
                synchronized (r4) {
                    str3 = r4.A0G;
                }
                r2.A0G = str3;
            }
            synchronized (r4) {
                num3 = r4.A07;
            }
            if (num3 != null) {
                synchronized (r4) {
                    num8 = r4.A07;
                }
                r2.A01 = num8.intValue();
            }
            if (!z || A01.A00 == 0) {
                synchronized (r4) {
                    num4 = r4.A0C;
                }
                if (num4 != null) {
                    synchronized (r4) {
                        num7 = r4.A0C;
                    }
                    r2.A08 = num7.intValue();
                }
                synchronized (r4) {
                    num5 = r4.A0B;
                }
                if (num5 != null) {
                    synchronized (r4) {
                        num6 = r4.A0B;
                    }
                    r2.A06 = num6.intValue();
                }
                synchronized (r4) {
                    l2 = r4.A0E;
                }
                if (l2 != null) {
                    synchronized (r4) {
                        l3 = r4.A0E;
                    }
                    r2.A0A = l3.longValue();
                }
                if (!(r5.A0G() == null || r4.A0G() == null)) {
                    r5.A0G().A02(r4.A0G());
                }
            }
        }
    }

    public static /* synthetic */ void A01(C22370yy r6, C41471ta r7) {
        String str;
        String str2 = r7.A0E;
        File file = r7.A0B;
        if (file == null) {
            str = "mediadownloadmanager/scheduleExpressPathFileCleanUp encrypted file is null";
        } else if (str2 == null) {
            str = "mediadownloadmanager/scheduleExpressPathFileCleanUp encrypted file hash is null";
        } else {
            C004201x r5 = new C004201x(ExpressPathGarbageCollectWorker.class);
            C006403a r3 = new C006403a();
            r3.A00.put("file_path", file.getAbsolutePath());
            r5.A00.A0A = r3.A00();
            r5.A02(5, TimeUnit.MINUTES);
            ((AnonymousClass022) r6.A0p.get()).A05(AnonymousClass023.KEEP, (AnonymousClass021) r5.A00(), str2);
            return;
        }
        Log.e(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0368, code lost:
        if (r98.A11 != -1) goto L_0x036a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0190, code lost:
        if (r0.getType() != 9) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x024a, code lost:
        if (r0 != 12) goto L_0x024c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x025e, code lost:
        if (r98.A0G().A04() == false) goto L_0x0260;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C28921Pn A02(X.AbstractC28771Oy r97, X.AbstractC16130oV r98, int r99, long r100, boolean r102, boolean r103) {
        /*
        // Method dump skipped, instructions count: 1037
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22370yy.A02(X.1Oy, X.0oV, int, long, boolean, boolean):X.1Pn");
    }

    public C28921Pn A03(AbstractC16130oV r4) {
        AbstractC28931Po r1;
        HashMap hashMap = this.A0r;
        synchronized (hashMap) {
            r1 = (AbstractC28931Po) hashMap.get(r4.A05);
        }
        if (r1 instanceof C28921Pn) {
            return (C28921Pn) r1;
        }
        return null;
    }

    public Collection A04() {
        ArrayList arrayList;
        C43131wS r2 = this.A0W;
        synchronized (r2) {
            arrayList = new ArrayList(r2.values());
        }
        return arrayList;
    }

    public void A05() {
        synchronized (this.A0W) {
            for (AbstractC16130oV r1 : A04()) {
                A0C(r1, true, true);
            }
        }
    }

    public final void A06() {
        Collection<AbstractC16130oV> A04 = A04();
        ArrayList arrayList = new ArrayList();
        for (AbstractC16130oV r1 : A04) {
            if (A0F(r1)) {
                arrayList.add(r1);
            }
        }
        this.A0D.A04(arrayList);
    }

    public void A07(AbstractC28771Oy r15, AbstractC16130oV r16, int i) {
        String str;
        AbstractC28931Po r2;
        C28921Pn r22;
        C28921Pn A02 = A02(r15, r16, i, -1, true, false);
        if (A02 != null) {
            if (this.A0M.A07(1539) && (r16 instanceof C16440p1) && (str = r16.A04) != null) {
                HashMap hashMap = this.A0s;
                synchronized (hashMap) {
                    r2 = (AbstractC28931Po) hashMap.get(str);
                }
                if ((r2 instanceof C28921Pn) && (r22 = (C28921Pn) r2) != null) {
                    StringBuilder sb = new StringBuilder("mediaDownloadManager/startDownloadImmediately cancel existing express path download ");
                    sb.append(r22);
                    Log.i(sb.toString());
                    r22.A76(true);
                }
            }
            StringBuilder sb2 = new StringBuilder("MediaDownloadManager/start manual download ");
            sb2.append(r16.A0z);
            sb2.append(", message.mediaHash=");
            sb2.append(r16.A05);
            Log.i(sb2.toString());
            if (A0F(r16)) {
                this.A0f.A03(this.A0F.A00, new Intent("com.whatsapp.media.download.service.MediaDownloadService.DOWNLOAD_STARTED"), MediaDownloadService.class);
                A06();
            }
            AnonymousClass1MO r0 = this.A0n;
            if (r0 != null) {
                r0.execute(A02);
            } else {
                this.A0m.Ab2(A02);
            }
        }
    }

    public void A08(AbstractC28771Oy r13, AbstractC16130oV r14, int i, long j, boolean z, boolean z2) {
        String str;
        AbstractC28931Po r9;
        C28921Pn r92;
        C28921Pn A02 = A02(r13, r14, i, j, z, z2);
        if (A02 != null) {
            StringBuilder sb = new StringBuilder("MediaDownloadManager/queueDownload auto download ");
            sb.append(r14.A0z);
            sb.append(", message.mediaHash=");
            sb.append(r14.A05);
            Log.i(sb.toString());
            if (this.A0M.A07(1539) && (r14 instanceof C16440p1) && (str = r14.A04) != null) {
                HashMap hashMap = this.A0s;
                synchronized (hashMap) {
                    r9 = (AbstractC28931Po) hashMap.get(str);
                }
                if ((r9 instanceof C28921Pn) && (r92 = (C28921Pn) r9) != null) {
                    r92.A0K.A03(new AbstractC14590lg() { // from class: X.1we
                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            C28921Pn.this.A09(((Number) obj).longValue());
                        }
                    }, null);
                    ((AbstractRunnableC14570le) A02).A00.A03(new AbstractC14590lg() { // from class: X.1wf
                        @Override // X.AbstractC14590lg
                        public final void accept(Object obj) {
                            C28921Pn.this.A76(true);
                        }
                    }, this.A0v);
                    this.A0m.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, r14, r92, A02, 25));
                    return;
                }
            }
            this.A0X.A01(A02, r14);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r20.A01 != r19.A00) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
        if (r19.A01().A01 == null) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09(X.C28781Oz r19, X.C41471ta r20, X.AbstractC16130oV r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22370yy.A09(X.1Oz, X.1ta, X.0oV, boolean):void");
    }

    public final void A0A(AbstractC16130oV r3) {
        if (r3 != null) {
            C43131wS r1 = this.A0W;
            synchronized (r1) {
                r1.remove(r3.A0z);
            }
            A06();
        }
    }

    public final void A0B(AbstractC16130oV r4, int i, boolean z) {
        if (z) {
            if (C30041Vv.A0r(r4)) {
                this.A0C.A00(new ProcessVCardMessageJob(r4));
                this.A0J.A08(r4, 20);
            }
            C15650ng r2 = this.A0H;
            int i2 = 3;
            if (i == 13) {
                i2 = -1;
            }
            r2.A0a(r4, i2);
        }
    }

    public final void A0C(AbstractC16130oV r4, boolean z, boolean z2) {
        AbstractC28931Po r2;
        HashMap hashMap = this.A0r;
        synchronized (hashMap) {
            r2 = (AbstractC28931Po) hashMap.get(r4.A05);
        }
        if (r2 != null) {
            StringBuilder sb = new StringBuilder("MediaDownloadManager/cancelDownload/message.key=");
            sb.append(r4.A0z);
            sb.append(", message.mediaHash=");
            sb.append(r4.A05);
            Log.i(sb.toString());
            r2.A76(z2);
        }
        this.A0X.A05(r4);
        if (z) {
            Set set = this.A0t;
            synchronized (set) {
                set.add(r4.A0z);
            }
        }
    }

    public boolean A0D(AbstractC28771Oy r10, AbstractC28931Po r11, C41471ta r12, AbstractC16130oV r13, String str, boolean z) {
        C28921Pn A03;
        HashMap hashMap = this.A0r;
        synchronized (hashMap) {
            if (((AbstractC28931Po) hashMap.get(str)) == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediaDownloadManager/addAndDedupeDownload/No existing regular MMS download in progress, creating new download instance for mediaHash=");
                sb.append(str);
                Log.i(sb.toString());
                hashMap.put(str, r11);
                r11.A5g(new C43141wU(this, str));
                return false;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("MediaDownloadManager/addAndDedupeDownload/MMS download already in progress (according to current downloads) mediaHash=");
            sb2.append(str);
            Log.i(sb2.toString());
            if (!(r13 == null || r12 == null || (A03 = A03(r13)) == null)) {
                A03.A0H.A03(new AbstractC14590lg(r10, this, r12, r13, z) { // from class: X.1wV
                    public final /* synthetic */ AbstractC28771Oy A00;
                    public final /* synthetic */ C22370yy A01;
                    public final /* synthetic */ C41471ta A02;
                    public final /* synthetic */ AbstractC16130oV A03;
                    public final /* synthetic */ boolean A04;

                    {
                        this.A01 = r2;
                        this.A03 = r4;
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A04 = r5;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        C22370yy r2 = this.A01;
                        AbstractC16130oV r4 = this.A03;
                        C41471ta r5 = this.A02;
                        AbstractC28771Oy r6 = this.A00;
                        boolean z2 = this.A04;
                        C28781Oz r3 = (C28781Oz) obj;
                        r2.A0j.A0D(r4);
                        r2.A09(r3, r5, r4, true);
                        r2.A0A(r4);
                        r2.A0u.execute(new RunnableBRunnable0Shape0S0510000_I0(r2, r3, r4, r5, r6, 4, z2));
                    }
                }, this.A0v);
            }
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        if (r3.A04(r0, r5) == false) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r1 == false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0E(X.AbstractC16130oV r5) {
        /*
            r4 = this;
            X.11X r3 = r4.A0X
            monitor-enter(r3)
            java.util.Map r2 = r3.A01     // Catch: all -> 0x002e
            java.lang.Object r0 = r2.get(r5)     // Catch: all -> 0x002e
            X.1pG r0 = (X.RunnableC39081pG) r0     // Catch: all -> 0x002e
            if (r0 == 0) goto L_0x0012
            boolean r1 = r0.A03     // Catch: all -> 0x002e
            r0 = 1
            if (r1 != 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            monitor-exit(r3)
            if (r0 == 0) goto L_0x0018
            r1 = 0
            return r1
        L_0x0018:
            monitor-enter(r3)
            java.lang.Object r0 = r2.get(r5)     // Catch: all -> 0x002b
            X.1pG r0 = (X.RunnableC39081pG) r0     // Catch: all -> 0x002b
            if (r0 == 0) goto L_0x0028
            boolean r0 = r3.A04(r0, r5)     // Catch: all -> 0x002b
            r1 = 1
            if (r0 != 0) goto L_0x0029
        L_0x0028:
            r1 = 0
        L_0x0029:
            monitor-exit(r3)
            return r1
        L_0x002b:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x002e:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22370yy.A0E(X.0oV):boolean");
    }

    public final boolean A0F(AbstractC16130oV r7) {
        C14850m9 r1 = this.A0M;
        return r1.A07(1147) && ((r7 instanceof C16440p1) || (r7 instanceof AnonymousClass1X2)) && !C15380n4.A0N(r7.A0z.A00) && r7.A01 > ((long) r1.A02(1148)) * 1048576;
    }
}
