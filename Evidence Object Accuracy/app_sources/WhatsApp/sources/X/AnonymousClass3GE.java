package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3GE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GE {
    public static byte A00(FileInputStream fileInputStream, long j, long j2) {
        fileInputStream.skip(j - j2);
        int read = fileInputStream.read();
        if (read == -1) {
            return 0;
        }
        byte b = (byte) read;
        if (read >= 100) {
            return 99;
        }
        return b;
    }

    public static List A01(File file, int i) {
        byte b;
        ArrayList A0l = C12960it.A0l();
        if (!(file == null || file.length() == 0)) {
            long length = file.length();
            float f = ((float) length) / ((float) i);
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                A0l.add(Byte.valueOf(A00(fileInputStream, 0, 0)));
                long j = 1;
                for (int i2 = 1; i2 < i - 1; i2++) {
                    float f2 = ((float) i2) * f;
                    double d = (double) f2;
                    long floor = (long) Math.floor(d);
                    long ceil = (long) Math.ceil(d);
                    float f3 = f2 - ((float) floor);
                    byte A00 = A00(fileInputStream, floor, j);
                    j = floor + 1;
                    if (ceil != floor) {
                        b = A00(fileInputStream, ceil, j);
                        j = ceil + 1;
                    } else {
                        b = A00;
                    }
                    A0l.add(Byte.valueOf((byte) ((int) (((float) A00) + (((float) (b - A00)) * f3)))));
                }
                A0l.add(Byte.valueOf(A00(fileInputStream, length - 1, j)));
                fileInputStream.close();
                return A0l;
            } catch (IOException e) {
                Log.e("waveformutil/generateDisplayDataPoints/ error reading visualization file data ", e);
            }
        }
        return A0l;
    }
}
