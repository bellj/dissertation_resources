package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3D4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D4 {
    public int A00;
    public long A01 = SystemClock.uptimeMillis();
    public final int A02;
    public final AnonymousClass383 A03;
    public final List A04 = C12960it.A0l();

    public AnonymousClass3D4(AnonymousClass383 r3, int i) {
        this.A03 = r3;
        this.A02 = i;
    }

    public void A00(AnonymousClass4TC r7) {
        if (this.A00 < this.A02) {
            ((AbstractC16350or) this.A03).A02.A01(Collections.singletonList(r7));
        } else {
            List list = this.A04;
            list.add(r7);
            if (this.A01 + 1000 < SystemClock.uptimeMillis()) {
                ArrayList A0x = C12980iv.A0x(list);
                list.clear();
                ((AbstractC16350or) this.A03).A02.A01(A0x);
                this.A01 = SystemClock.uptimeMillis();
            }
        }
        this.A00++;
    }
}
