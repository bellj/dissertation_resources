package X;

import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;

/* renamed from: X.2H3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2H3 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ HorizontalScrollView A00;

    public AnonymousClass2H3(HorizontalScrollView horizontalScrollView) {
        this.A00 = horizontalScrollView;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        HorizontalScrollView horizontalScrollView = this.A00;
        horizontalScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        horizontalScrollView.fullScroll(66);
    }
}
