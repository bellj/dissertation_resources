package X;

import android.widget.ImageView;

/* renamed from: X.2iS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55432iS extends C106164vG {
    public int A00;
    public final /* synthetic */ AbstractC14670lq A01;

    public C55432iS(AbstractC14670lq r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.C106164vG, X.AnonymousClass5VV
    public void AWE(AnonymousClass4YC r12) {
        C89814Lm r10 = r12.A09;
        double d = 1.0d - 0.0d;
        float f = (float) (0.5d + (((r10.A00 - 0.0d) / d) * (1.0d - 0.5d)));
        AbstractC14670lq r7 = this.A01;
        ImageView imageView = r7.A0g;
        imageView.setScaleX(f);
        imageView.setScaleY(f);
        int i = this.A00;
        if (i != 0) {
            float f2 = (float) (0.0d + (((r10.A00 - 0.0d) / d) * (((double) i) - 0.0d)));
            int width = imageView.getWidth() >> 2;
            if (C28141Kv.A00(r7.A11)) {
                width = -width;
            }
            imageView.setTranslationX(f2 + ((float) width));
        }
    }
}
