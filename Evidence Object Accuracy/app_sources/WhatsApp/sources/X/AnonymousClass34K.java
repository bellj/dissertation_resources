package X;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.34K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34K extends C33711ex {
    public AnonymousClass34K(C16470p4 r1) {
        super(r1);
    }

    @Override // X.C33711ex
    public CharSequence A03(Context context, Paint paint, AnonymousClass018 r8) {
        AnonymousClass1ZC r0;
        C16470p4 r02 = this.A00;
        if (r02 == null || (r0 = r02.A04) == null) {
            return super.A03(context, paint, r8);
        }
        long A00 = (long) r0.A00();
        Object[] A1b = C12970iu.A1b();
        C12980iv.A1U(A1b, 0, A00);
        return r8.A0I(A1b, R.plurals.products_total_quantity, A00);
    }

    @Override // X.C33711ex
    public String A09(AnonymousClass018 r10) {
        String str;
        String str2;
        StringBuilder A0h = C12960it.A0h();
        String A04 = A04();
        if (TextUtils.isEmpty(A04)) {
            A0h.append(A04);
        }
        C16470p4 r8 = this.A00;
        if (r8 != null) {
            AnonymousClass1ZC r0 = r8.A04;
            if (r0 != null) {
                int A00 = r0.A00();
                A0h.append("\n");
                Object[] A1b = C12970iu.A1b();
                C12960it.A1P(A1b, A00, 0);
                A0h.append(r10.A0I(A1b, R.plurals.products_total_quantity, (long) A00));
            }
            str = r8.A07;
        } else {
            str = null;
        }
        if (!TextUtils.isEmpty(str)) {
            A0h.append("\n");
            A0h.append(str);
        }
        if (r8 != null) {
            str2 = r8.A08;
        } else {
            str2 = null;
        }
        if (!TextUtils.isEmpty(str2)) {
            A0h.append("\n");
            A0h.append(str2);
        }
        return A0h.toString();
    }

    @Override // X.C33711ex
    public void A0A(AbstractC15340mz r1, C39971qq r2) {
        AnonymousClass3GO.A00(r1, r2);
    }
}
