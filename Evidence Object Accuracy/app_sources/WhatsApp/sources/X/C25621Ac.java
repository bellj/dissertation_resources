package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1301100_I0;
import com.whatsapp.util.Log;

/* renamed from: X.1Ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25621Ac {
    public final C22320yt A00;
    public final C16510p9 A01;
    public final C19990v2 A02;
    public final C18460sU A03;
    public final C20850wQ A04;
    public final C16490p7 A05;

    public C25621Ac(C22320yt r1, C16510p9 r2, C19990v2 r3, C18460sU r4, C20850wQ r5, C16490p7 r6) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
    }

    public void A00(AbstractC14640lm r4, String str) {
        StringBuilder sb = new StringBuilder("msgstore/updategroupchatsubject/");
        sb.append(r4);
        Log.i(sb.toString());
        this.A00.A01(new RunnableBRunnable0Shape0S1200000_I0(this, r4, str, 16), 37);
    }

    public void A01(C15580nU r12, AnonymousClass1V0 r13, String str, int i, long j) {
        StringBuilder sb = new StringBuilder("msgstore/updategroupchat/");
        sb.append(r12);
        sb.append(" creation=");
        sb.append(j);
        sb.append(" groupType='");
        sb.append(i);
        sb.append("'");
        Log.i(sb.toString());
        this.A00.A01(new RunnableBRunnable0Shape0S1301100_I0(this, r12, r13, str, i, 0, j), 38);
    }

    public final boolean A02(AnonymousClass1PE r8, AbstractC14640lm r9, Long l) {
        ContentValues contentValues;
        try {
            try {
                C16310on A02 = this.A05.A02();
                try {
                    AnonymousClass1Lx A00 = A02.A00();
                    C16510p9 r4 = this.A01;
                    synchronized (r8) {
                        contentValues = new ContentValues(3);
                        if (l != null) {
                            contentValues.put("created_timestamp", l);
                        }
                        contentValues.put("subject", r8.A0e);
                        contentValues.put("group_type", Integer.valueOf(r8.A01));
                        AnonymousClass1V0 r0 = r8.A0b;
                        if (r0 != null) {
                            contentValues.put("growth_lock_level", Integer.valueOf(r0.A00));
                            contentValues.put("growth_lock_expiration_ts", Long.valueOf(r8.A0b.A01));
                        }
                    }
                    if (!r4.A0H(contentValues, r8)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("msgstore/addmsg/chatlist/insert/failed jid=");
                        sb.append(r9);
                        Log.e(sb.toString());
                    }
                    A00.A00();
                    A00.close();
                    A02.close();
                    return true;
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Error | RuntimeException e) {
                Log.e(e);
                throw e;
            }
        } catch (SQLiteDatabaseCorruptException e2) {
            Log.e(e2);
            this.A04.A02();
            return false;
        }
    }

    public boolean A03(AbstractC14640lm r4, int i) {
        AnonymousClass1PE A06 = this.A02.A06(r4);
        if (A06 == null) {
            StringBuilder sb = new StringBuilder("groupchatstore/updateGroupChatInfoGroupTypeIfExist/chat does not exist: ");
            sb.append(r4);
            Log.i(sb.toString());
            return false;
        }
        Log.i("groupchatstore/updateGroupChatInfoGroupTypeInBackgroundIfExist/update group type");
        A06.A01 = i;
        return A02(A06, r4, null);
    }
}
