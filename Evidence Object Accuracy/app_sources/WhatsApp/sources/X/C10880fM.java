package X;

import java.security.Provider;

/* renamed from: X.0fM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10880fM extends Provider {
    public C10880fM() {
        super("LinuxPRNG", 1.0d, "A Linux-specific random number provider that uses /dev/urandom");
        put("SecureRandom.SHA1PRNG", C10890fN.class.getName());
        put("SecureRandom.SHA1PRNG ImplementedIn", "Software");
    }
}
