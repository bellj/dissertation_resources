package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Tn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91864Tn {
    public final int A00;
    public final int A01;
    public final int A02;
    public final UserJid A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final boolean A07;

    public C91864Tn(UserJid userJid, String str, String str2, String str3, int i, int i2, int i3, boolean z) {
        this.A05 = str;
        this.A03 = userJid;
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A06 = str2;
        this.A04 = str3;
        this.A07 = z;
    }
}
