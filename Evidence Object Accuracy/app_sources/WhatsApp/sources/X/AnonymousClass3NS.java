package X;

import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;

/* renamed from: X.3NS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NS implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ TextStatusComposerActivity A00;

    public AnonymousClass3NS(TextStatusComposerActivity textStatusComposerActivity) {
        this.A00 = textStatusComposerActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        TextStatusComposerActivity textStatusComposerActivity = this.A00;
        C12980iv.A1E(textStatusComposerActivity.A04, this);
        ViewGroup viewGroup = textStatusComposerActivity.A04;
        viewGroup.setTranslationY(C12990iw.A03(viewGroup));
        textStatusComposerActivity.A04.animate().translationY(0.0f).alpha(1.0f).setDuration((long) 200).setListener(new AnonymousClass2G4(textStatusComposerActivity, 0));
    }
}
