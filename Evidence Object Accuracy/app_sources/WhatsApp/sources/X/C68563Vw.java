package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* renamed from: X.3Vw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68563Vw implements AbstractC116565Vy {
    public List A00 = null;
    public List A01;
    public boolean A02;
    public final int A03;
    public final AnonymousClass016 A04 = C12980iv.A0T();
    public final C63363Bh A05;
    public final AbstractC115865Tf A06;
    public final AbstractC115875Tg A07;
    public final AbstractC115885Th A08;
    public final AnonymousClass4N5 A09;
    public final C89334Jo A0A;
    public final AnonymousClass2K2 A0B;
    public final AnonymousClass2Jw A0C;
    public final String A0D;

    public C68563Vw(AbstractC115865Tf r3, AbstractC115875Tg r4, AbstractC115885Th r5, AnonymousClass4N5 r6, C89334Jo r7, AnonymousClass2K2 r8, AnonymousClass2Jw r9, int i) {
        this.A03 = i;
        this.A0B = r8;
        this.A07 = r4;
        this.A08 = r5;
        this.A06 = r3;
        this.A09 = r6;
        this.A0C = r9;
        this.A0A = r7;
        this.A05 = new C63363Bh();
        this.A0D = Long.toHexString(new Random().nextLong());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0028, code lost:
        if (r5.A01() == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C59612v1 A00(X.C48122Ek r12, java.util.List r13, int r14) {
        /*
            r11 = this;
            java.lang.Object r5 = r13.get(r14)
            X.3M8 r5 = (X.AnonymousClass3M8) r5
            java.lang.Double r0 = r12.A03
            double r2 = r0.doubleValue()
            java.lang.Double r0 = r12.A04
            double r0 = r0.doubleValue()
            com.google.android.gms.maps.model.LatLng r4 = new com.google.android.gms.maps.model.LatLng
            r4.<init>(r2, r0)
            r0 = 1
            int r14 = r14 + r0
            int r9 = r12.A01()
            boolean r0 = r12.A04()
            if (r0 == 0) goto L_0x002a
            boolean r0 = r5.A01()
            r10 = 1
            if (r0 != 0) goto L_0x002b
        L_0x002a:
            r10 = 0
        L_0x002b:
            X.3W8 r7 = new X.3W8
            r7.<init>(r5, r11, r14)
            X.54Y r8 = new X.54Y
            r8.<init>()
            X.4Pv r6 = new X.4Pv
            r6.<init>(r5, r11, r14)
            X.2v1 r3 = new X.2v1
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            X.5Th r0 = r11.A08
            java.lang.String r0 = r0.AGZ()
            r3.A00 = r0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C68563Vw.A00(X.2Ek, java.util.List, int):X.2v1");
    }

    public List A01() {
        if (this.A00 == null) {
            return null;
        }
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < this.A00.size(); i++) {
            AnonymousClass3M8 r2 = (AnonymousClass3M8) this.A00.get(i);
            String str = r2.A09;
            if (str != null) {
                C50292Oz r4 = new C50292Oz();
                r4.A02 = C12970iu.A0h();
                r4.A05 = str;
                r4.A01 = Double.valueOf(r2.A00);
                r4.A00 = Double.valueOf(r2.A01);
                r4.A04 = Long.valueOf(((long) i) + 1);
                r4.A06 = this.A0D;
                int i2 = 1;
                if (this.A03 == 1) {
                    i2 = 0;
                }
                r4.A03 = Integer.valueOf(i2);
                A0l.add(r4);
            }
        }
        this.A00 = null;
        return A0l;
    }

    public void A02() {
        boolean AJq = this.A06.AJq();
        C63363Bh r1 = this.A05;
        if (!AJq) {
            r1.A02 = 7;
            r1.A00++;
        } else {
            r1.A02 = 9;
        }
        A03();
    }

    public void A03() {
        this.A04.A0B(this.A05);
    }

    public void A04() {
        C63363Bh r3 = this.A05;
        List list = r3.A08;
        if (!list.isEmpty()) {
            int A0C = C12980iv.A0C(list);
            if (list.get(A0C) instanceof AnonymousClass405) {
                list.remove(A0C);
                r3.A02 = 2;
                A03();
            }
        }
    }

    public final void A05() {
        if (!this.A02) {
            C63363Bh r2 = this.A05;
            AnonymousClass4T8 r1 = r2.A04;
            if (r1 != null) {
                this.A00 = C12960it.A0l();
                this.A00.addAll(r2.A04.A03.subList(0, Math.min(r1.A03.size(), 10)));
                return;
            }
            return;
        }
        this.A00 = null;
    }

    public void A06(String str) {
        C63363Bh r4 = this.A05;
        r4.A04 = null;
        r4.A07 = false;
        r4.A00 = 0;
        r4.A06 = str;
        r4.A03 = new AnonymousClass2K4(150, null);
        r4.A02 = 0;
        r4.A08.clear();
        AnonymousClass4N5 r1 = this.A09;
        r1.A01.clear();
        r1.A00.clear();
        this.A01 = null;
        A03();
    }

    @Override // X.AbstractC116565Vy
    public void ANR(int i) {
        A04();
        C63363Bh r1 = this.A05;
        r1.A01 = i;
        r1.A02 = 8;
        A03();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        if (r16.A03.isEmpty() != false) goto L_0x0026;
     */
    @Override // X.AbstractC116565Vy
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANS(X.AnonymousClass4T8 r16) {
        /*
        // Method dump skipped, instructions count: 500
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C68563Vw.ANS(X.4T8):void");
    }
}
