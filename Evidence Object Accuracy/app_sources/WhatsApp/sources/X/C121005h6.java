package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121005h6 extends AbstractC121025h8 {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(32);
    public final C126775tQ A00;

    public C121005h6(AnonymousClass102 r5, AnonymousClass1V8 r6) {
        super(r5, r6);
        AnonymousClass1V8 A0F = r6.A0F("bank");
        C126775tQ r3 = null;
        String A0I = A0F.A0I("bank-name", null);
        String A0I2 = A0F.A0I("account-number", null);
        if (!AnonymousClass1US.A0C(A0I) && !AnonymousClass1US.A0C(A0I2)) {
            r3 = new C126775tQ(A0I, A0I2);
        }
        this.A00 = r3;
    }

    public C121005h6(Parcel parcel) {
        super(parcel);
        this.A00 = new C126775tQ(parcel.readString(), parcel.readString());
    }

    public C121005h6(String str) {
        super(str);
        C126775tQ r0;
        String string = C13000ix.A05(str).getString("bank");
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject A05 = C13000ix.A05(string);
                r0 = new C126775tQ(A05.getString("bank-name"), A05.getString("account-number"));
            } catch (JSONException unused) {
                Log.e("PAY: DisplayBank fromJsonString threw exception");
            }
            this.A00 = r0;
        }
        r0 = null;
        this.A00 = r0;
    }

    @Override // X.AbstractC121025h8, X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        super.A05(jSONObject);
        try {
            C126775tQ r3 = this.A00;
            JSONObject A0a = C117295Zj.A0a();
            try {
                A0a.put("bank-name", r3.A01);
                A0a.put("account-number", r3.A00);
            } catch (JSONException unused) {
                Log.w("PAY:DisplayBank/toJson: JSONException thrown");
            }
            jSONObject.put("bank", A0a);
        } catch (JSONException unused2) {
            Log.w("PAY:NoviTransactionWithdrawalBank failed to create the JSON");
        }
    }

    @Override // X.AbstractC121025h8, X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        C126775tQ r1 = this.A00;
        parcel.writeString(r1.A01);
        parcel.writeString(r1.A00);
    }
}
