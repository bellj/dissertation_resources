package X;

/* renamed from: X.4XK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XK {
    public final float A00;
    public final float A01;
    public final long A02;
    public final long A03;
    public final long A04;

    public AnonymousClass4XK(float f, float f2, long j, long j2, long j3) {
        this.A04 = j;
        this.A03 = j2;
        this.A02 = j3;
        this.A01 = f;
        this.A00 = f2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass4XK)) {
            return false;
        }
        AnonymousClass4XK r7 = (AnonymousClass4XK) obj;
        if (this.A04 == r7.A04 && this.A03 == r7.A03 && this.A02 == r7.A02 && this.A01 == r7.A01 && this.A00 == r7.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        long j = this.A04;
        int A0A = C72453ed.A0A(C72453ed.A0A(((int) (j ^ (j >>> 32))) * 31, this.A03), this.A02);
        float f = this.A01;
        int i2 = 0;
        if (f != 0.0f) {
            i = Float.floatToIntBits(f);
        } else {
            i = 0;
        }
        int i3 = (A0A + i) * 31;
        float f2 = this.A00;
        if (f2 != 0.0f) {
            i2 = Float.floatToIntBits(f2);
        }
        return i3 + i2;
    }
}
