package X;

import android.view.View;
import com.whatsapp.wabloks.ui.BkActionBottomSheet;

/* renamed from: X.5oF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123975oF extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractC115815Ta A00;
    public final /* synthetic */ BkActionBottomSheet A01;

    public C123975oF(AbstractC115815Ta r1, BkActionBottomSheet bkActionBottomSheet) {
        this.A01 = bkActionBottomSheet;
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AnonymousClass67H r5 = new AbstractC28681Oo() { // from class: X.67H
            @Override // X.AbstractC28681Oo
            public final AbstractC14200l1 AAN() {
                return AbstractC115815Ta.this.AAL().A0G(35);
            }
        };
        if (r5.AAN() != null) {
            BkActionBottomSheet bkActionBottomSheet = this.A01;
            AnonymousClass1AI.A08(bkActionBottomSheet.A00.A00((ActivityC000800j) bkActionBottomSheet.A0B(), bkActionBottomSheet.A0F(), new C89264Jh(bkActionBottomSheet.A04)), r5);
        }
    }
}
