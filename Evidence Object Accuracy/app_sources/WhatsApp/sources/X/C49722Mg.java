package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49722Mg extends AbstractC28131Kt {
    public final AbstractC14640lm A00;
    public final UserJid A01;

    public C49722Mg(AbstractC14640lm r2, UserJid userJid) {
        super(null, null);
        this.A00 = r2;
        this.A01 = userJid;
    }
}
