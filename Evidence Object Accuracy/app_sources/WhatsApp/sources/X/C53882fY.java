package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2fY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53882fY extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass016 A02;
    public final C15570nT A03;
    public final AnonymousClass2EE A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final UserJid A08;
    public final String A09;
    public final String A0A;

    public C53882fY(C15570nT r3, AnonymousClass2EE r4, C14830m7 r5, C16590pI r6, AnonymousClass018 r7, UserJid userJid, String str, String str2) {
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A02 = A0T;
        AnonymousClass016 A0T2 = C12980iv.A0T();
        this.A01 = A0T2;
        this.A05 = r5;
        this.A06 = r6;
        this.A03 = r3;
        this.A07 = r7;
        this.A04 = r4;
        this.A0A = str;
        this.A09 = str2;
        this.A08 = userJid;
        r4.A00 = A0T;
        r4.A01 = A0T2;
        this.A00 = AnonymousClass0R2.A00(new AnonymousClass02O() { // from class: X.4rO
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                return Boolean.valueOf(C12960it.A1W(obj));
            }
        }, A0T);
    }
}
