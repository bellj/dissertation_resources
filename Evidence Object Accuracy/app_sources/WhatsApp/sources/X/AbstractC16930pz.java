package X;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.0pz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16930pz implements AbstractC16940q0 {
    public static List A00(AbstractC16940q0 r3, JSONArray jSONArray, long j) {
        Object A7i;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (!(optJSONObject == null || (A7i = r3.A7i(optJSONObject, j)) == null)) {
                arrayList.add(A7i);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:225:0x02e7 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r14v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r14v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r14v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x02e9, code lost:
        r13 = (X.C44731zS) r0.A02.A7i(r10, r42);
     */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x02a7 A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x02e3 A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0308 A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0331 A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x037f A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x039f A[Catch: Exception -> 0x04b5, TryCatch #4 {Exception -> 0x04b5, blocks: (B:4:0x0005, B:6:0x000b, B:8:0x000f, B:10:0x0013, B:12:0x0019, B:14:0x001d, B:16:0x0021, B:18:0x0025, B:20:0x0029, B:22:0x002d, B:24:0x0031, B:26:0x0048, B:28:0x0050, B:33:0x0060, B:35:0x0071, B:37:0x007c, B:39:0x0086, B:41:0x008f, B:42:0x0093, B:43:0x0099, B:46:0x00a0, B:48:0x00be, B:50:0x00cf, B:53:0x00da, B:54:0x00e0, B:56:0x00e8, B:57:0x00ee, B:58:0x00fc, B:61:0x0103, B:63:0x0110, B:65:0x0118, B:69:0x0124, B:71:0x013c, B:73:0x0147, B:75:0x014f, B:76:0x0155, B:77:0x015b, B:79:0x0161, B:82:0x016e, B:84:0x0184, B:85:0x018a, B:86:0x0190, B:88:0x01a1, B:90:0x01ad, B:92:0x01b5, B:94:0x01bf, B:96:0x01c9, B:97:0x01cf, B:98:0x01d5, B:101:0x01e7, B:103:0x020b, B:105:0x0221, B:107:0x0227, B:109:0x0232, B:110:0x023b, B:112:0x0243, B:114:0x0249, B:120:0x0255, B:124:0x0265, B:125:0x026d, B:130:0x027a, B:131:0x0285, B:133:0x02a7, B:135:0x02c2, B:138:0x02c9, B:140:0x02cf, B:142:0x02d5, B:144:0x02dd, B:145:0x02e0, B:146:0x02e3, B:148:0x02e9, B:149:0x02f1, B:152:0x0308, B:155:0x0314, B:158:0x0331, B:160:0x0345, B:161:0x0372, B:162:0x0377, B:164:0x037f, B:165:0x0386, B:167:0x039f, B:171:0x03ab, B:173:0x03c2, B:175:0x03ca, B:177:0x03e4, B:179:0x0402, B:182:0x0420, B:184:0x0442, B:186:0x045b, B:190:0x0466, B:192:0x0472, B:194:0x0483, B:196:0x0489, B:198:0x0491, B:200:0x0495, B:202:0x049d, B:204:0x04a3, B:206:0x04ab, B:207:0x04ae), top: B:224:0x0005, inners: #0, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x03a9 A[RETURN] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC16940q0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A7i(org.json.JSONObject r41, long r42) {
        /*
        // Method dump skipped, instructions count: 1213
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC16930pz.A7i(org.json.JSONObject, long):java.lang.Object");
    }
}
