package X;

import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.2bK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52572bK extends ViewOutlineProvider {
    public final /* synthetic */ float A00;
    public final /* synthetic */ AnonymousClass28D A01;

    public C52572bK(AnonymousClass28D r1, float f) {
        this.A00 = f;
        this.A01 = r1;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        outline.setRoundRect(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight(), this.A00);
        Drawable background = view.getBackground();
        if (background != null) {
            background.getOutline(outline);
            outline.setAlpha(this.A01.A09(65, 1.0f));
        }
    }
}
