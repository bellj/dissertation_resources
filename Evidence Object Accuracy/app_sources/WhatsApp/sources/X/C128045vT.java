package X;

import android.content.Context;
import java.io.IOException;
import java.util.Properties;

/* renamed from: X.5vT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128045vT {
    public C130495zV A00;
    public C128835wk A01;
    public final Context A02;
    public final Properties A03;
    public final C128825wj A04;
    public final C129045x5 A05;

    public C128045vT(Context context, C129045x5 r5) {
        String str;
        this.A05 = r5;
        this.A02 = context;
        this.A00 = r5.A03;
        Properties properties = new Properties();
        try {
            properties.load(this.A02.getAssets().open("version.properties"));
            this.A03 = properties;
            this.A04 = new C128825wj(this);
            if (r5.A03 != null && (str = r5.A00) != null) {
                this.A01 = new C128835wk(str, this.A00);
            }
        } catch (IOException e) {
            throw C117315Zl.A0J(e);
        }
    }
}
