package X;

import com.whatsapp.voipcalling.CallGroupInfo;

/* renamed from: X.2O6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2O6 {
    public final int A00;
    public final AnonymousClass2O7 A01;
    public final CallGroupInfo A02;
    public final boolean A03;
    public final byte[] A04;

    public AnonymousClass2O6(AnonymousClass2O7 r1, CallGroupInfo callGroupInfo, byte[] bArr, int i, boolean z) {
        this.A01 = r1;
        this.A04 = bArr;
        this.A00 = i;
        this.A02 = callGroupInfo;
        this.A03 = z;
    }
}
