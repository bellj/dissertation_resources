package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.12y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237712y {
    public final C237812z A00;
    public final C18460sU A01;
    public final C16490p7 A02;

    public C237712y(C237812z r1, C18460sU r2, C16490p7 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public final void A00(C16330op r6, int i) {
        try {
            r6.A01("conversion_tuples", "jid_row_id=?", new String[]{String.valueOf(i)});
            UserJid of = UserJid.of(this.A01.A03((long) i));
            if (of != null) {
                for (C27171Gh r0 : this.A00.A01()) {
                    r0.A00.A02.A01("click_to_whatsapp_ads_log_trackers").edit().remove(of.getRawString()).apply();
                }
            }
        } catch (Exception e) {
            Log.e("conversionTupleMessageStore/deleteConversionTuple error accessing db", e);
        }
    }
}
