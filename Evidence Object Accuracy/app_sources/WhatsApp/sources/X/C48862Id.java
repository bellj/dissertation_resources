package X;

/* renamed from: X.2Id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48862Id extends AbstractC48872Ie {
    public static final long serialVersionUID = 0;
    public final Object reference;

    public C48862Id(Object obj) {
        this.reference = obj;
    }

    @Override // X.AbstractC48872Ie, java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof C48862Id) {
            return this.reference.equals(((C48862Id) obj).reference);
        }
        return false;
    }

    @Override // X.AbstractC48872Ie, java.lang.Object
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @Override // X.AbstractC48872Ie, java.lang.Object
    public String toString() {
        String valueOf = String.valueOf(this.reference);
        StringBuilder sb = new StringBuilder(valueOf.length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
