package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2wG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60232wG extends AnonymousClass2SU {
    public final /* synthetic */ VoipActivityV2 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60232wG(AnonymousClass2A6 r2, UserJid userJid, VoipActivityV2 voipActivityV2, String str) {
        super(r2, userJid, voipActivityV2.A1t, str);
        this.A00 = voipActivityV2;
    }
}
