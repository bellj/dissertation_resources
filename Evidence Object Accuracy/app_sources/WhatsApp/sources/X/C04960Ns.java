package X;

import android.view.View;

/* renamed from: X.0Ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04960Ns {
    public View A00;
    public AnonymousClass072 A01;
    public C05350Pf A02;
    public AbstractC11380gC A03;
    public String A04;

    public C04960Ns(View view, AnonymousClass072 r2, C05350Pf r3, AbstractC11380gC r4, String str) {
        this.A00 = view;
        this.A04 = str;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }
}
