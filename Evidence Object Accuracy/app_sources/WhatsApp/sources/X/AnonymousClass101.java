package X;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jobqueue.job.BulkGetPreKeyJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.101  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass101 {
    public static int A0A;
    public boolean A00;
    public boolean A01;
    public final Handler A02;
    public final C20670w8 A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C16120oU A06;
    public final AnonymousClass1VN A07 = new AnonymousClass1VN(10, 610);
    public final Map A08 = new HashMap();
    public final Map A09 = new HashMap();

    public AnonymousClass101(C20670w8 r7, C14830m7 r8, C14850m9 r9, C16120oU r10) {
        Handler handler = new Handler(Looper.getMainLooper());
        this.A04 = r8;
        this.A05 = r9;
        this.A02 = handler;
        this.A06 = r10;
        this.A03 = r7;
    }

    public synchronized void A00() {
        if (this.A01) {
            this.A01 = false;
            this.A00 = false;
            this.A07.A02();
            A01();
        }
    }

    public synchronized void A01() {
        Map map = this.A09;
        if (!map.isEmpty()) {
            long uptimeMillis = SystemClock.uptimeMillis();
            ArrayList arrayList = new ArrayList(map.size());
            ArrayList arrayList2 = new ArrayList();
            for (Map.Entry entry : map.entrySet()) {
                DeviceJid deviceJid = (DeviceJid) entry.getKey();
                C32601cQ r2 = (C32601cQ) entry.getValue();
                Map map2 = this.A08;
                if (!map2.containsKey(deviceJid)) {
                    arrayList.add(deviceJid);
                    boolean z = r2.A04;
                    if (z) {
                        arrayList2.add(deviceJid);
                    }
                    map2.put(deviceJid, new C32601cQ(r2.A00, r2.A02, r2.A01, uptimeMillis, z));
                }
            }
            A02(arrayList, arrayList2, 9);
            map.clear();
            this.A00 = false;
        }
    }

    public void A02(List list, List list2, int i) {
        DeviceJid[] deviceJidArr;
        int i2;
        int A02;
        if (list.isEmpty()) {
            Log.i("prekeysmanager/startPrekeyFetchJobs jids list is empty");
            return;
        }
        Log.i("prekeysmanager/startPrekeyFetchJobs creating BulkGetPreKeyJob");
        if (list2.isEmpty()) {
            deviceJidArr = null;
        } else {
            deviceJidArr = (DeviceJid[]) list2.toArray(new DeviceJid[0]);
        }
        synchronized (this) {
            i2 = A0A;
            if (i2 == 0) {
                i2 = this.A05.A02(767);
                A0A = i2;
            }
        }
        if (i2 <= 0 || list.size() < i2 || (A02 = this.A05.A02(921)) <= 0) {
            this.A03.A00(new BulkGetPreKeyJob((DeviceJid[]) list.toArray(new DeviceJid[0]), deviceJidArr, i));
            return;
        }
        Iterator it = new C29841Uw(list.toArray(new DeviceJid[0]), A02).iterator();
        while (it.hasNext()) {
            this.A03.A00(new BulkGetPreKeyJob((DeviceJid[]) it.next(), deviceJidArr, i));
        }
    }

    public synchronized void A03(DeviceJid[] deviceJidArr, int i, int i2, int i3, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("prekeysmanager/getprekeys request for jids:");
        sb.append(Arrays.toString(deviceJidArr));
        Log.i(sb.toString());
        long uptimeMillis = SystemClock.uptimeMillis();
        Map map = this.A08;
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (((C32601cQ) ((Map.Entry) it.next()).getValue()).A03 + 60000 < uptimeMillis) {
                it.remove();
            }
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        long uptimeMillis2 = SystemClock.uptimeMillis();
        for (DeviceJid deviceJid : deviceJidArr) {
            if (!map.containsKey(deviceJid)) {
                arrayList.add(deviceJid);
                map.put(deviceJid, new C32601cQ(i, i2, i3, uptimeMillis2, z));
                if (z) {
                    arrayList2.add(deviceJid);
                }
            }
        }
        A02(arrayList, arrayList2, i);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("prekeysmanager/sending getprekeys for jids:");
        sb2.append(Arrays.toString(deviceJidArr));
        Log.i(sb2.toString());
        A00();
    }

    public synchronized void A04(DeviceJid[] deviceJidArr, int i, boolean z) {
        A03(deviceJidArr, i, 0, 0, z);
    }
}
