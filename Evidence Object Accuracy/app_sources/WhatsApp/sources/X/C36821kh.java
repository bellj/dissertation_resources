package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1kh */
/* loaded from: classes2.dex */
public class C36821kh extends RelativeLayout implements AnonymousClass004 {
    public AnonymousClass2M5 A00;
    public C15550nR A01;
    public AnonymousClass10S A02;
    public C15610nY A03;
    public AnonymousClass01d A04;
    public AnonymousClass018 A05;
    public C21250x7 A06;
    public C14850m9 A07;
    public C20710wC A08;
    public AbstractC14640lm A09;
    public AbstractC14640lm A0A;
    public C21260x8 A0B;
    public C21280xA A0C;
    public AnonymousClass2P7 A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final TextView A0I;
    public final TextView A0J;
    public final WaImageView A0K;
    public final C27131Gd A0L;
    public final C236712o A0M;

    public C36821kh(Context context) {
        super(context, null);
        if (!this.A0E) {
            this.A0E = true;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            this.A07 = (C14850m9) r1.A04.get();
            this.A06 = (C21250x7) r1.AJh.get();
            this.A0B = (C21260x8) r1.A2k.get();
            this.A0C = (C21280xA) r1.AMU.get();
            this.A01 = (C15550nR) r1.A45.get();
            this.A04 = (AnonymousClass01d) r1.ALI.get();
            this.A03 = (C15610nY) r1.AMe.get();
            this.A05 = (AnonymousClass018) r1.ANb.get();
            this.A02 = (AnonymousClass10S) r1.A46.get();
            this.A08 = (C20710wC) r1.A8m.get();
        }
        this.A0M = new AnonymousClass1s0(this);
        this.A0L = new C36751kU(this);
        View inflate = LayoutInflater.from(context).inflate(R.layout.voip_return_to_call_banner, (ViewGroup) this, true);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.call_notification_timer);
        this.A0I = textView;
        this.A0J = (TextView) AnonymousClass028.A0D(inflate, R.id.call_notification_title);
        this.A0K = (WaImageView) AnonymousClass028.A0D(inflate, R.id.call_notification_icon);
        textView.setFocusable(true);
        AnonymousClass028.A0g(textView, new C51462Uv(this));
        setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(context, 14, this));
        AnonymousClass23N.A01(this);
        setVisibility(C21280xA.A01() ? 0 : 8);
        textView.setText("");
        textView.setTag(null);
    }

    public static /* synthetic */ void A00(C36821kh r3, Collection collection) {
        AbstractC14640lm r0;
        if (AnonymousClass1SF.A0P(r3.A07) && (r0 = r3.A09) != null && (r0 instanceof GroupJid)) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                Jid jid = ((C15370n3) it.next()).A0D;
                if (jid != null && jid.equals(r3.A09)) {
                    r3.A02();
                }
            }
        }
    }

    public final void A01() {
        if (getContext() == null) {
            Log.w("voip/VoipReturnToCallBanner no context when call start");
            return;
        }
        WaImageView waImageView = this.A0K;
        waImageView.setVisibility(0);
        boolean z = this.A0G;
        int i = R.drawable.ic_groupcall_voice;
        if (z) {
            i = R.drawable.ic_groupcall_video;
        }
        waImageView.setImageResource(i);
        Context context = getContext();
        boolean z2 = this.A0G;
        int i2 = R.string.audio_call;
        if (z2) {
            i2 = R.string.video_call;
        }
        waImageView.setContentDescription(context.getString(i2));
    }

    public final void A02() {
        GroupJid groupJid;
        String A09;
        String string;
        if (getContext() == null) {
            Log.w("voip/VoipReturnToCallBanner no context when call start");
            return;
        }
        if (this.A0F) {
            A09 = getContext().getString(R.string.voip_joinable_waiting_for_others);
        } else {
            AbstractC14640lm r1 = this.A09;
            if (r1 == null || !r1.equals(this.A0A)) {
                AbstractC14640lm r5 = this.A09;
                if (r5 == null) {
                    groupJid = null;
                } else if (r5 instanceof GroupJid) {
                    groupJid = (GroupJid) r5;
                } else {
                    A09 = this.A03.A04(this.A01.A0B(r5));
                    AnonymousClass028.A0a(this.A0K, 1);
                }
                A09 = AnonymousClass1SF.A09(this.A01, this.A03, this.A06, this.A08, groupJid);
                WaImageView waImageView = this.A0K;
                AnonymousClass028.A0a(waImageView, 1);
                if (A09 == null) {
                    Context context = getContext();
                    boolean z = this.A0G;
                    int i = R.string.call_banner_group_voice;
                    if (z) {
                        i = R.string.call_banner_group_video;
                    }
                    A09 = context.getString(i);
                    AnonymousClass028.A0a(waImageView, 2);
                }
            } else {
                A09 = getContext().getString(R.string.tap_to_return_to_call);
                Context context2 = getContext();
                boolean z2 = this.A0G;
                int i2 = R.string.ax_label_return_to_voice_call;
                if (z2) {
                    i2 = R.string.ax_label_return_to_video_call;
                }
                string = context2.getString(i2);
                AnonymousClass028.A0a(this.A0K, 2);
                TextView textView = this.A0J;
                textView.setText(A09);
                textView.setContentDescription(string);
            }
        }
        string = A09;
        TextView textView = this.A0J;
        textView.setText(A09);
        textView.setContentDescription(string);
    }

    public final void A03() {
        CallInfo callInfo;
        AbstractC14640lm peerJid;
        if (AnonymousClass1SF.A0P(this.A07) && (callInfo = Voip.getCallInfo()) != null) {
            if (callInfo.isGroupCall()) {
                peerJid = callInfo.groupJid;
            } else {
                peerJid = callInfo.getPeerJid();
                AnonymousClass009.A05(peerJid);
            }
            this.A09 = peerJid;
            this.A0G = callInfo.videoEnabled;
            A02();
            A01();
        }
        this.A0I.setVisibility(8);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0D;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0D = r0;
        }
        return r0.generatedComponent();
    }

    public CallInfo getCallInfo() {
        return Voip.getCallInfo();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A0B.A03(this.A0M);
        this.A02.A03(this.A0L);
        AnonymousClass2M5 r1 = this.A00;
        if (r1 != null) {
            r1.AYQ(getVisibility());
        }
        A03();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A0B.A04(this.A0M);
        this.A02.A04(this.A0L);
    }

    private void setContainerChatJid(AbstractC14640lm r1) {
        this.A0A = r1;
    }

    public void setShouldHideBanner(boolean z) {
        this.A0H = z;
        int i = 8;
        if (C21280xA.A01()) {
            i = 0;
        }
        setVisibility(i);
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        AnonymousClass2M5 r1;
        int visibility = getVisibility();
        if (this.A0H) {
            i = 8;
        }
        super.setVisibility(i);
        if (visibility != getVisibility() && (r1 = this.A00) != null) {
            r1.AYQ(getVisibility());
        }
    }

    public void setVisibilityChangeListener(AnonymousClass2M5 r1) {
        this.A00 = r1;
    }
}
