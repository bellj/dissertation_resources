package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/* renamed from: X.14T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14T {
    public final C14830m7 A00;
    public final C16120oU A01;
    public final AbstractC14440lR A02;

    public AnonymousClass14T(C14830m7 r1, C16120oU r2, AbstractC14440lR r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public static final String A00(AbstractC15340mz r3) {
        StringBuilder sb = new StringBuilder();
        sb.append(r3.A0z.A01);
        sb.append(r3.A0I);
        String obj = sb.toString();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(obj.getBytes());
            return Base64.encodeToString(instance.digest(), 11);
        } catch (NoSuchAlgorithmException e) {
            Log.e("OTP: Error computing sessionId for logging", e);
            return null;
        }
    }

    public void A01(AbstractC15340mz r7, Integer num, Integer num2) {
        C38261nn r5 = new C38261nn();
        r5.A03 = 0;
        r5.A02 = num2;
        r5.A01 = num;
        r5.A05 = Long.valueOf(Long.parseLong(r7.A0C().user));
        r5.A04 = 0;
        r5.A06 = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - r7.A0I));
        r5.A07 = A00(r7);
        this.A01.A07(r5);
    }
}
