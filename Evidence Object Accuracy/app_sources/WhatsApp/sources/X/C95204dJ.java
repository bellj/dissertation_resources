package X;

import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.4dJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95204dJ {
    public static long A00;
    public static C94484bt A01;
    public static final C95204dJ A02 = new C95204dJ();

    public static final C94484bt A00() {
        synchronized (A02) {
            C94484bt r4 = A01;
            if (r4 == null) {
                return new C94484bt();
            }
            A01 = r4.A02;
            r4.A02 = null;
            A00 -= (long) DefaultCrypto.BUFFER_SIZE;
            return r4;
        }
    }

    public static final void A01(C94484bt r7) {
        if (r7.A02 != null || r7.A03 != null) {
            throw C12970iu.A0f("Failed requirement.");
        } else if (!r7.A05) {
            synchronized (A02) {
                long j = A00 + ((long) DefaultCrypto.BUFFER_SIZE);
                if (j <= 65536) {
                    A00 = j;
                    r7.A02 = A01;
                    r7.A00 = 0;
                    r7.A01 = 0;
                    A01 = r7;
                }
            }
        }
    }
}
