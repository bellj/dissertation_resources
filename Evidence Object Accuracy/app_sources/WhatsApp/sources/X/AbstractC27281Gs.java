package X;

import java.util.Locale;

/* renamed from: X.1Gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC27281Gs {
    /* JADX WARNING: Removed duplicated region for block: B:110:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:111:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:114:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:115:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:117:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0055 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0083 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00be A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00f5 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(java.lang.String r5) {
        /*
        // Method dump skipped, instructions count: 552
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC27281Gs.A00(java.lang.String):java.lang.String");
    }

    public static String A01(Locale locale) {
        String A01;
        Object obj;
        String str;
        String language = locale.getLanguage();
        if (language.equals("pt")) {
            AnonymousClass00O r2 = AbstractC41281tH.A01;
            if (AnonymousClass1Th.A00.contains(locale.getCountry())) {
                str = "pt-PT";
            } else {
                str = "pt-BR";
            }
            obj = r2.get(str);
        } else {
            if (!language.equals("zh")) {
                A01 = AbstractC27291Gt.A01(locale);
            } else if ("HK".equals(locale.getCountry())) {
                A01 = "zh-HK";
            } else {
                A01 = "Hans".equals(AbstractC27291Gt.A02(locale)) ? "zh-Hans" : "zh-TW";
            }
            obj = AbstractC41281tH.A01.get(A01);
        }
        String str2 = (String) obj;
        if (str2 == null) {
            return locale.getDisplayLanguage(locale);
        }
        return str2;
    }
}
