package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.0WZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WZ implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass0CN A00;

    public AnonymousClass0WZ(AnonymousClass0CN r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass0CN r2 = this.A00;
        if (r2.AK4()) {
            C02410Ce r1 = r2.A0I;
            if (!r1.A0H) {
                View view = r2.A03;
                if (view == null || !view.isShown()) {
                    r2.dismiss();
                } else {
                    r1.Ade();
                }
            }
        }
    }
}
