package X;

import android.os.IBinder;

/* renamed from: X.3sP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80533sP extends C98344iX implements AnonymousClass5YN {
    public C80533sP(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.wearable.internal.IWearableListener");
    }
}
