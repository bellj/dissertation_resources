package X;

/* renamed from: X.4EJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EJ {
    public static int A00(int i) {
        int i2 = 1;
        int i3 = 0;
        if (i != 1) {
            i3 = 2;
            if (i != 2) {
                i2 = 3;
                if (i != 3) {
                    if (i != 4) {
                        throw C12970iu.A0f(C12960it.A0W(i, "unrecognized SignalMessageType; value="));
                    }
                }
            }
            return i2;
        }
        return i3;
    }
}
