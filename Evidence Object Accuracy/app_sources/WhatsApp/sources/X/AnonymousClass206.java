package X;

import android.text.TextUtils;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.InflaterInputStream;

/* renamed from: X.206  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass206 {
    public byte[] A00;
    public final AbstractC15710nm A01;
    public final AnonymousClass205 A02;
    public final AnonymousClass207 A03;

    public AnonymousClass206(AbstractC15710nm r1, AnonymousClass205 r2, AnonymousClass207 r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    public static void A00(InputStream inputStream, byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            int read = inputStream.read(bArr, i, length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new IOException("ran out of bytes while reading into buffer");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] A01(java.io.InputStream r6, int r7) {
        /*
            int r2 = r6.read()
            r0 = r2 & 128(0x80, float:1.794E-43)
            r5 = 0
            r1 = 0
            if (r0 == 0) goto L_0x000b
            r1 = 1
        L_0x000b:
            r0 = r2 & 127(0x7f, float:1.78E-43)
            byte[] r4 = new byte[r0]
            A00(r6, r4)
            int r6 = r0 << 1
            int r6 = r6 - r1
            byte[] r3 = new byte[r6]
        L_0x0017:
            if (r5 >= r6) goto L_0x0084
            int r0 = r5 % 2
            int r0 = 1 - r0
            int r1 = r0 << 2
            int r0 = r5 >> 1
            byte r2 = r4[r0]
            r0 = 15
            int r0 = r0 << r1
            r2 = r2 & r0
            int r2 = r2 >> r1
            r0 = 251(0xfb, float:3.52E-43)
            if (r7 == r0) goto L_0x004c
            r0 = 255(0xff, float:3.57E-43)
            if (r7 != r0) goto L_0x0070
            switch(r2) {
                case 0: goto L_0x0068;
                case 1: goto L_0x0068;
                case 2: goto L_0x0068;
                case 3: goto L_0x0068;
                case 4: goto L_0x0068;
                case 5: goto L_0x0068;
                case 6: goto L_0x0068;
                case 7: goto L_0x0068;
                case 8: goto L_0x0068;
                case 9: goto L_0x0068;
                case 10: goto L_0x0047;
                case 11: goto L_0x0047;
                default: goto L_0x0033;
            }
        L_0x0033:
            java.lang.String r1 = "bad nibble "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            X.1V9 r0 = new X.1V9
            r0.<init>(r1)
            throw r0
        L_0x0047:
            int r0 = r2 + -10
            int r0 = r0 + 45
            goto L_0x006a
        L_0x004c:
            switch(r2) {
                case 0: goto L_0x0068;
                case 1: goto L_0x0068;
                case 2: goto L_0x0068;
                case 3: goto L_0x0068;
                case 4: goto L_0x0068;
                case 5: goto L_0x0068;
                case 6: goto L_0x0068;
                case 7: goto L_0x0068;
                case 8: goto L_0x0068;
                case 9: goto L_0x0068;
                case 10: goto L_0x0063;
                case 11: goto L_0x0063;
                case 12: goto L_0x0063;
                case 13: goto L_0x0063;
                case 14: goto L_0x0063;
                case 15: goto L_0x0063;
                default: goto L_0x004f;
            }
        L_0x004f:
            java.lang.String r1 = "bad hex "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            X.1V9 r0 = new X.1V9
            r0.<init>(r1)
            throw r0
        L_0x0063:
            int r0 = r2 + -10
            int r0 = r0 + 65
            goto L_0x006a
        L_0x0068:
            int r0 = r2 + 48
        L_0x006a:
            byte r0 = (byte) r0
            r3[r5] = r0
            int r5 = r5 + 1
            goto L_0x0017
        L_0x0070:
            java.lang.String r1 = "bad packed type "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r7)
            java.lang.String r1 = r0.toString()
            X.1V9 r0 = new X.1V9
            r0.<init>(r1)
            throw r0
        L_0x0084:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass206.A01(java.io.InputStream, int):byte[]");
    }

    public final int A02(InputStream inputStream, int i) {
        if (i == 0) {
            return 0;
        }
        if (i == 248) {
            return inputStream.read();
        }
        if (i == 249) {
            return (inputStream.read() << 8) + inputStream.read();
        }
        StringBuilder sb = new StringBuilder("invalid list size in readListSize: token ");
        sb.append(i);
        throw new AnonymousClass1V9(sb.toString(), A05());
    }

    public AnonymousClass1V8 A03() {
        byte[] A01;
        int length;
        AnonymousClass207 r7 = this.A03;
        AnonymousClass009.A06(r7, "frameInputStream is null");
        do {
            try {
                AnonymousClass20O r1 = r7.A00;
                byte[] A00 = r1.A00(C16050oM.A01(r1.A00(3)));
                AnonymousClass20P r0 = r7.A01;
                A01 = r0.A03.A01(new byte[0], A00, r0.A00.getAndIncrement());
                length = A01.length;
            } catch (AnonymousClass20R e) {
                throw new AnonymousClass20S(e);
            }
        } while (length == 0);
        if (length != 1) {
            byte b = A01[0];
            if ((b & 2) != 0) {
                byte[] bArr = {b};
                InflaterInputStream inflaterInputStream = new InflaterInputStream(new ByteArrayInputStream(A01, 1, length - 1));
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(DefaultCrypto.BUFFER_SIZE);
                    byteArrayOutputStream.write(bArr);
                    byte[] bArr2 = new byte[DefaultCrypto.BUFFER_SIZE];
                    for (int read = inflaterInputStream.read(bArr2); read >= 0; read = inflaterInputStream.read(bArr2)) {
                        byteArrayOutputStream.write(bArr2, 0, read);
                    }
                    inflaterInputStream.close();
                    byteArrayOutputStream.close();
                    A01 = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    inflaterInputStream.close();
                } catch (Throwable th) {
                    try {
                        inflaterInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            this.A00 = A01;
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(A01);
            if ((((byte) byteArrayInputStream.read()) & 1) == 0) {
                return A04(byteArrayInputStream);
            }
            throw new AnonymousClass1V9("server to client stanza fragmentation not supported");
        }
        throw new AnonymousClass1V9("header only frame received");
    }

    public final AnonymousClass1V8 A04(InputStream inputStream) {
        AnonymousClass1W9[] r4;
        AnonymousClass1W9 r2;
        byte[] A01;
        int read;
        int A02 = A02(inputStream, inputStream.read());
        int read2 = inputStream.read();
        if (read2 == 2) {
            return null;
        }
        String A08 = A08(inputStream, read2);
        if (A02 == 0 || A08 == null) {
            throw new AnonymousClass1V9("nextTree sees 0 list or null tag", A05());
        }
        int i = A02 - 2;
        int i2 = A02 % 2;
        int i3 = (i + i2) / 2;
        if (i3 == 0) {
            r4 = null;
        } else {
            r4 = new AnonymousClass1W9[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                String A082 = A08(inputStream, inputStream.read());
                int read3 = inputStream.read();
                if (read3 == 247) {
                    r2 = new AnonymousClass1W9(Jid.get(A06(inputStream)), A082);
                } else if (read3 != 250) {
                    r2 = new AnonymousClass1W9(A082, A08(inputStream, read3));
                } else {
                    String A07 = A07(inputStream);
                    try {
                        r2 = new AnonymousClass1W9(Jid.get(A07), A082);
                    } catch (AnonymousClass1MW unused) {
                        r2 = new AnonymousClass1W9(A082, A07);
                    }
                }
                r4[i4] = r2;
            }
        }
        if (i2 == 1) {
            return new AnonymousClass1V8(A08, r4);
        }
        int read4 = inputStream.read();
        if (read4 == 0 || read4 == 248 || read4 == 249) {
            int A022 = A02(inputStream, read4);
            AnonymousClass1V8[] r22 = new AnonymousClass1V8[A022];
            for (int i5 = 0; i5 < A022; i5++) {
                r22[i5] = A04(inputStream);
            }
            return new AnonymousClass1V8(A08, r4, r22);
        }
        if (read4 == 252) {
            read = inputStream.read();
        } else if (read4 == 253) {
            read = ((inputStream.read() & 15) << 16) + (inputStream.read() << 8) + inputStream.read();
        } else if (read4 == 254) {
            read = inputStream.read() | ((inputStream.read() & 127) << 24) | (inputStream.read() << 16) | (inputStream.read() << 8);
        } else if (read4 != 255 && read4 != 251) {
            return new AnonymousClass1V8(A08, A08(inputStream, read4), r4);
        } else {
            A01 = A01(inputStream, read4);
            return new AnonymousClass1V8(A08, A01, r4);
        }
        A01 = new byte[read];
        A00(inputStream, A01);
        return new AnonymousClass1V8(A08, A01, r4);
    }

    public String A05() {
        byte[] bArr = this.A00;
        if (bArr == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder("size = ");
        int length = bArr.length;
        sb.append(Integer.toString(length));
        sb.append('<');
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(Integer.toString(bArr[i] & 255));
            if (((long) sb.length()) >= 262144) {
                break;
            }
        }
        if (((long) sb.length()) < 262144) {
            sb.append('>');
        } else {
            sb.append("...");
        }
        return sb.toString();
    }

    public final String A06(InputStream inputStream) {
        String str;
        String obj;
        int read = inputStream.read() & 255;
        int read2 = inputStream.read() & 255;
        String A08 = A08(inputStream, inputStream.read());
        if (read == 1) {
            str = "lid";
        } else {
            str = "s.whatsapp.net";
        }
        if (TextUtils.isEmpty(A08)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A08);
        if (read2 == 0) {
            obj = "";
        } else {
            StringBuilder sb2 = new StringBuilder(":");
            sb2.append(read2);
            obj = sb2.toString();
        }
        sb.append(obj);
        sb.append('@');
        sb.append(str);
        return sb.toString();
    }

    public final String A07(InputStream inputStream) {
        String A08 = A08(inputStream, inputStream.read());
        String A082 = A08(inputStream, inputStream.read());
        if (TextUtils.isEmpty(A08)) {
            return A082;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A08);
        sb.append('@');
        sb.append(A082);
        return sb.toString();
    }

    public final String A08(InputStream inputStream, int i) {
        int i2;
        if (i == -1) {
            throw new AnonymousClass1V9("-1 token in readString", A05());
        } else if (i <= 2 || i >= 236) {
            if (i == 0) {
                return null;
            }
            if (i != 247) {
                switch (i) {
                    case 236:
                    case 237:
                    case 238:
                    case 239:
                        int read = inputStream.read();
                        if (!(this instanceof AnonymousClass20T)) {
                            try {
                                try {
                                    String str = AnonymousClass20U.A01[i - 236][read];
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Token was null for doublebyte dictNum=");
                                    sb.append(i);
                                    sb.append(", dictIdx=");
                                    sb.append(read);
                                    AnonymousClass009.A06(str, sb.toString());
                                    return str;
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    StringBuilder sb2 = new StringBuilder("Fail while trying to access WapDict, dictNum=");
                                    sb2.append(i);
                                    sb2.append(", dictIdx=");
                                    sb2.append(read);
                                    Log.e(sb2.toString());
                                    throw e;
                                }
                            } catch (ArrayIndexOutOfBoundsException | NullPointerException unused) {
                                throw new AnonymousClass1V9("invalid token index in getToken()", A05());
                            }
                        } else {
                            throw new AnonymousClass1V9("Unexpected doublebyte token during use of web dictionary", A05());
                        }
                    default:
                        try {
                            switch (i) {
                                case 250:
                                    Log.w("Unexpected ReadString for token JID_PAIR");
                                    return A07(inputStream);
                                case 251:
                                case 255:
                                    return new String(A01(inputStream, i), AnonymousClass01V.A08);
                                case 252:
                                    i2 = inputStream.read();
                                    break;
                                case 253:
                                    i2 = ((inputStream.read() & 15) << 16) + (inputStream.read() << 8) + inputStream.read();
                                    break;
                                case 254:
                                    i2 = inputStream.read() | ((inputStream.read() & 127) << 24) | (inputStream.read() << 16) | (inputStream.read() << 8);
                                    break;
                                default:
                                    throw new AnonymousClass1V9("readString couldn't match token", A05());
                            }
                            byte[] bArr = new byte[i2];
                            A00(inputStream, bArr);
                            return new String(bArr, AnonymousClass01V.A08);
                        } catch (UnsupportedEncodingException unused2) {
                            return null;
                        }
                }
            } else {
                Log.w("Unexpected ReadString for token JID_4");
                return A06(inputStream);
            }
        } else if (!(this instanceof AnonymousClass20T)) {
            try {
                try {
                    String str2 = AnonymousClass20U.A00[i];
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Token was null for singlebyte idx: ");
                    sb3.append(i);
                    AnonymousClass009.A06(str2, sb3.toString());
                    return str2;
                } catch (ArrayIndexOutOfBoundsException e2) {
                    StringBuilder sb4 = new StringBuilder("Fail while trying to access WapDict, dictNum=");
                    sb4.append(0);
                    sb4.append(", dictIdx=");
                    sb4.append(i);
                    Log.e(sb4.toString());
                    throw e2;
                }
            } catch (ArrayIndexOutOfBoundsException | NullPointerException unused3) {
                throw new AnonymousClass1V9("invalid token index in getToken()", A05());
            }
        } else {
            try {
                String str3 = AnonymousClass20V.A02[i];
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Token was null for web dictionary idx: ");
                sb5.append(i);
                AnonymousClass009.A06(str3, sb5.toString());
                return str3;
            } catch (Exception e3) {
                Log.e(e3);
                throw new AnonymousClass1V9("invalid token/length in getToken", A05());
            }
        }
    }
}
