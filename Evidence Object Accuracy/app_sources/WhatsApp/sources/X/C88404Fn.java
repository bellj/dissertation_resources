package X;

import java.io.PrintStream;

/* renamed from: X.4Fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88404Fn {
    public static final AnonymousClass4UQ A00;

    static {
        AnonymousClass4UQ r0;
        Integer num;
        try {
            try {
                num = (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
            } catch (Exception e) {
                PrintStream printStream = System.err;
                printStream.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
                e.printStackTrace(printStream);
            }
        } catch (Throwable th) {
            PrintStream printStream2 = System.err;
            String name = C79983rW.class.getName();
            StringBuilder A0t = C12980iv.A0t(name.length() + 133);
            A0t.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            A0t.append(name);
            printStream2.println(C12960it.A0d("will be used. The error is: ", A0t));
            th.printStackTrace(printStream2);
            r0 = new C79983rW();
        }
        if (num != null && num.intValue() >= 19) {
            r0 = new C79993rX();
            A00 = r0;
        }
        if (!Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
            r0 = new C80003rY();
        } else {
            r0 = new C79983rW();
        }
        A00 = r0;
    }
}
