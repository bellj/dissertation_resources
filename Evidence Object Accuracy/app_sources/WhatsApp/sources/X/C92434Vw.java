package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Vw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92434Vw {
    public final int A00;
    public final int A01;
    public final UserJid A02;
    public final boolean A03;
    public final boolean A04;

    public C92434Vw(AnonymousClass1V2 r2, boolean z) {
        this.A02 = r2.A0A;
        this.A00 = r2.A01();
        this.A01 = r2.A02();
        this.A03 = r2.A0D();
        this.A04 = z;
    }

    public boolean A00() {
        return this.A00 > 0 && !this.A04 && !this.A03;
    }
}
