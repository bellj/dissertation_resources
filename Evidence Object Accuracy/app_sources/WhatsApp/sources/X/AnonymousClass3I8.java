package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.3I8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I8 {
    public static final List A00 = Arrays.asList(C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_one), R.id.emoji_one), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_two), R.id.emoji_two), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_three), R.id.emoji_three), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_four), R.id.emoji_four), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_five), R.id.emoji_five), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_six), R.id.emoji_six), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_seven), R.id.emoji_seven), C12960it.A0D(Integer.valueOf((int) R.id.emoji_container_eight), R.id.emoji_eight));

    public static List A00(String str) {
        ArrayList A0l = C12960it.A0l();
        try {
            JSONArray jSONArray = new JSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                C12980iv.A1R(A0l, jSONArray.getInt(i));
            }
        } catch (JSONException e) {
            Log.e(C12960it.A0d(str, C12960it.A0k("statusreply/statusreactions/invalid emoji list JSONArray:")), e);
        }
        return A0l;
    }

    public static boolean A01(AnonymousClass1CH r6, AbstractC15340mz r7) {
        if ((r7 instanceof AbstractC16130oV) && (!r7.A0z.A02 || ((r7 instanceof AnonymousClass1X2) && C30041Vv.A15((AnonymousClass1X4) r7)))) {
            AbstractC16130oV r4 = (AbstractC16130oV) r7;
            C16150oX A002 = AbstractC15340mz.A00(r4);
            C28921Pn A003 = r6.A00(A002);
            if (r7.A0y != 3 || !A002.A0a || A003 == null || A003.A0h == null || C38241nl.A02()) {
                if ((r7 instanceof AnonymousClass1X7) && C30041Vv.A0z(r4)) {
                    return true;
                }
                if (!A002.A0P || A002.A07 == 1 || A002.A0F == null) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }
}
