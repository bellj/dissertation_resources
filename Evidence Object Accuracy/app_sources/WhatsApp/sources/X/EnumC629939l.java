package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class EnumC629939l extends Enum {
    public static final /* synthetic */ EnumC629939l[] A00;
    public static final EnumC629939l A01;
    public static final EnumC629939l A02;
    public static final EnumC629939l A03;
    public static final EnumC629939l A04;

    public static EnumC629939l valueOf(String str) {
        return (EnumC629939l) Enum.valueOf(EnumC629939l.class, str);
    }

    public static EnumC629939l[] values() {
        return (EnumC629939l[]) A00.clone();
    }

    static {
        EnumC629939l r6 = new EnumC629939l("FILLED", 0);
        A02 = r6;
        EnumC629939l r5 = new EnumC629939l("TONAL", 1);
        A04 = r5;
        EnumC629939l r4 = new EnumC629939l("OUTLINE", 2);
        A03 = r4;
        EnumC629939l r2 = new EnumC629939l("BORDERLESS", 3);
        A01 = r2;
        EnumC629939l[] r1 = new EnumC629939l[4];
        C12970iu.A1U(r6, r5, r1);
        r1[2] = r4;
        r1[3] = r2;
        A00 = r1;
    }

    public EnumC629939l(String str, int i) {
    }
}
