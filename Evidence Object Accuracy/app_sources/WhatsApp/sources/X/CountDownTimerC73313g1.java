package X;

import android.os.CountDownTimer;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;

/* renamed from: X.3g1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class CountDownTimerC73313g1 extends CountDownTimer {
    public final /* synthetic */ VerifyPhoneNumber A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC73313g1(VerifyPhoneNumber verifyPhoneNumber, long j) {
        super(j, 1000);
        this.A00 = verifyPhoneNumber;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        Log.i("verifyphonenumber/primary-flash-call-timer-finish");
        VerifyPhoneNumber verifyPhoneNumber = this.A00;
        verifyPhoneNumber.A0D = null;
        verifyPhoneNumber.A2y();
        verifyPhoneNumber.A3G(verifyPhoneNumber.A2f(), verifyPhoneNumber.A2g());
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        VerifyPhoneNumber verifyPhoneNumber = this.A00;
        if (verifyPhoneNumber.A09 == C26061Bw.A0L) {
            verifyPhoneNumber.A2y();
            verifyPhoneNumber.A38(R.string.verify_flash_call_still_working);
        }
        verifyPhoneNumber.A09 -= 1000;
    }
}
