package X;

/* renamed from: X.4Wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92644Wt {
    public final AnonymousClass2KT A00;
    public final AbstractC15340mz A01;

    public C92644Wt(AnonymousClass2KT r1, AbstractC15340mz r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C92644Wt.class != obj.getClass()) {
            return false;
        }
        return this.A01.A0z.A01.equals(((C92644Wt) obj).A01.A0z.A01);
    }

    public int hashCode() {
        return this.A01.A0z.A01.hashCode();
    }
}
