package X;

import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.3XY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XY implements AbstractC44401yr {
    public final /* synthetic */ AnonymousClass23a A00;
    public final /* synthetic */ C18340sI A01;

    public AnonymousClass3XY(AnonymousClass23a r1, C18340sI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r5) {
        C18340sI r3 = this.A01;
        AnonymousClass23a r2 = this.A00;
        AnonymousClass18A r1 = (AnonymousClass18A) r5.A02;
        if (r5.A00 == 0) {
            try {
                C457723b A01 = r1.A01((JSONObject) r1.A00);
                r3.A01(A01);
                r2.AX3(A01);
            } catch (Exception unused) {
                r3.A00(r5, r2);
            }
        } else {
            r3.A00(r5, r2);
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        AnonymousClass23a r1 = this.A00;
        Integer A0g = C12970iu.A0g();
        if (r1 != null) {
            r1.AQB(A0g);
        }
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        AnonymousClass23a r1 = this.A00;
        Integer A0g = C12970iu.A0g();
        if (r1 != null) {
            r1.AQB(A0g);
        }
    }
}
