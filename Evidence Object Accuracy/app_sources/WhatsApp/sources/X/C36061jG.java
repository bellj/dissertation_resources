package X;

import com.whatsapp.jid.UserJid;
import java.util.HashSet;

/* renamed from: X.1jG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36061jG extends C36071jH {
    public final /* synthetic */ C36041jE A00;
    public final /* synthetic */ HashSet A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36061jG(C15570nT r2, C15610nY r3, C36041jE r4, HashSet hashSet) {
        super(r2, r3, false);
        this.A00 = r4;
        this.A01 = hashSet;
    }

    @Override // X.C36071jH
    public int A00(C15370n3 r5, C15370n3 r6) {
        C15570nT r1 = this.A00.A00;
        if (!r1.A0F(r5.A0D) && !r1.A0F(r6.A0D)) {
            HashSet hashSet = this.A01;
            boolean contains = hashSet.contains(r5.A0B(UserJid.class));
            boolean contains2 = hashSet.contains(r6.A0B(UserJid.class));
            if (contains) {
                if (!contains2) {
                    return -1;
                }
            } else if (contains2) {
                return 1;
            }
        }
        return super.compare(r5, r6);
    }
}
