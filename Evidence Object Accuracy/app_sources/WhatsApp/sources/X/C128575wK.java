package X;

import java.util.HashMap;

/* renamed from: X.5wK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128575wK {
    public final /* synthetic */ AnonymousClass3FE A00;

    public /* synthetic */ C128575wK(AnonymousClass3FE r1) {
        this.A00 = r1;
    }

    public final void A00(C452120p r5, C127975vM r6) {
        String str;
        AnonymousClass3FE r3 = this.A00;
        HashMap A11 = C12970iu.A11();
        if (r5 == null) {
            A11.put("business_name", r6.A02);
            A11.put("owner_full_name", r6.A04);
            A11.put("verify_type", r6.A05);
            A11.put("bank_name", r6.A01);
            A11.put("credential_id", r6.A03);
            str = "on_success";
        } else {
            C117295Zj.A1D(r5, A11);
            str = "on_failure";
        }
        r3.A01(str, A11);
    }
}
