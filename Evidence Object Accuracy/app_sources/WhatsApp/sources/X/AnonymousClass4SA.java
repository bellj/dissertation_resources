package X;

import android.net.Uri;
import java.util.List;

/* renamed from: X.4SA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SA {
    public final Uri A00;
    public final AnonymousClass1IS A01;
    public final List A02;
    public final List A03;

    public AnonymousClass4SA(Uri uri, AnonymousClass1IS r2, List list, List list2) {
        this.A03 = list;
        this.A01 = r2;
        this.A00 = uri;
        this.A02 = list2;
    }
}
