package X;

/* renamed from: X.0II  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0II extends AnonymousClass0eL {
    public final /* synthetic */ AnonymousClass0OD A00;

    public AnonymousClass0II(AnonymousClass0OD r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        AnonymousClass0OD r0 = this.A00;
        AnonymousClass04O r5 = r0.A0M;
        float f = r0.A0A;
        float f2 = r0.A0B;
        AnonymousClass04L r52 = (AnonymousClass04L) r5;
        r52.A06();
        AnonymousClass03S r02 = r52.A0O;
        if (r02 == null || !r02.A05(f, f2)) {
            AnonymousClass04Q r2 = r52.A0M;
            AnonymousClass03S r03 = r2.A0D;
            if (r03 != null) {
                r03.A02();
            }
            r2.A0D = null;
            AnonymousClass04Q r04 = r52.A0M;
            AbstractC12180hV r1 = r04.A0B;
            if (r1 != null) {
                r1.ASL(r04.A0R.A05(f, f2));
                return;
            }
            return;
        }
        AnonymousClass04Q r22 = r52.A0M;
        AnonymousClass03S r12 = r52.A0O;
        AnonymousClass03S r05 = r22.A0D;
        if (!(r05 == null || r05 == r12)) {
            r05.A02();
        }
        r22.A0D = r12;
    }
}
