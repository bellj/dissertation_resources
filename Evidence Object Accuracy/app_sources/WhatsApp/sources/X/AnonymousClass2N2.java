package X;

/* renamed from: X.2N2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2N2 {
    public final C15580nU A00;
    public final AnonymousClass2N4 A01;
    public final AnonymousClass1P4 A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if (android.text.TextUtils.isEmpty(r9) != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2N2(X.C15580nU r3, X.AnonymousClass2N4 r4, X.AnonymousClass1P4 r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            r2 = this;
            r2.<init>()
            r2.A04 = r6
            r2.A00 = r3
            r2.A06 = r7
            r2.A05 = r8
            r2.A03 = r9
            r2.A01 = r4
            r2.A02 = r5
            if (r8 != 0) goto L_0x001a
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            r0 = 0
            if (r1 == 0) goto L_0x001b
        L_0x001a:
            r0 = 1
        L_0x001b:
            X.AnonymousClass009.A0F(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2N2.<init>(X.0nU, X.2N4, X.1P4, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }
}
