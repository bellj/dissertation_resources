package X;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08590bS implements AbstractC12790iV {
    public static final C06090Sd A02 = new C06090Sd(32);
    public int A00;
    public final AtomicLong A01 = new AtomicLong(0);

    public AbstractC08590bS(int i) {
        this.A00 = i;
    }

    public static AnonymousClass0NT A00(InputStream inputStream, boolean z) {
        C06090Sd r6 = A02;
        AnonymousClass0NT r7 = (AnonymousClass0NT) r6.A00();
        if (r7 == null) {
            r7 = new AnonymousClass0NT(new byte[C25981Bo.A0F], 0);
        }
        byte[] bArr = r7.A02;
        int i = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, i, bArr.length - i);
                if (read == -1) {
                    break;
                }
                i += read;
                int length = bArr.length;
                if (i >= length) {
                    byte[] bArr2 = new byte[length << 1];
                    System.arraycopy(bArr, 0, bArr2, 0, length);
                    bArr = bArr2;
                }
            } catch (IOException e) {
                if (!z || i == 0) {
                    r6.A02(r7);
                    if (z) {
                        C06160Sk.A0J.A00();
                        return null;
                    }
                    throw e;
                }
            }
        }
        if (bArr != bArr) {
            r6.A02(r7);
            return new AnonymousClass0NT(bArr, i);
        }
        r7.A00 = i;
        return r7;
    }

    public static void A01(AnonymousClass0NT r2) {
        if (r2.A02.length == 131072) {
            r2.A01 = null;
            A02.A02(r2);
        }
    }
}
