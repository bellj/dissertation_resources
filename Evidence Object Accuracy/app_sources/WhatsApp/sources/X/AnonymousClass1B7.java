package X;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* renamed from: X.1B7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1B7 {
    public Map A00 = new AnonymousClass5IF(this);
    public Map A01 = new AnonymousClass5IE(this);
    public final C16240og A02;
    public final C16190ob A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C17220qS A06;
    public final C16630pM A07;
    public final AbstractC14440lR A08;
    public final List A09 = Arrays.asList("WA_BizDirectorySearch", "WA_PrivateStats");

    public AnonymousClass1B7(C16240og r4, C16190ob r5, C14830m7 r6, C14850m9 r7, C17220qS r8, C16630pM r9, AbstractC14440lR r10) {
        this.A04 = r6;
        this.A05 = r7;
        this.A08 = r10;
        this.A03 = r5;
        this.A06 = r8;
        this.A02 = r4;
        this.A07 = r9;
    }
}
