package X;

import android.view.animation.Animation;

/* renamed from: X.0Wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class animation.Animation$AnimationListenerC07000Wh implements Animation.AnimationListener {
    public final /* synthetic */ AnonymousClass0BD A00;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public animation.Animation$AnimationListenerC07000Wh(AnonymousClass0BD r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AbstractC11900h3 r0;
        AnonymousClass0BD r2 = this.A00;
        if (r2.A0R) {
            AnonymousClass0A7 r1 = r2.A0L;
            r1.setAlpha(255);
            r1.start();
            if (r2.A0Q && (r0 = r2.A0N) != null) {
                r0.AUm();
            }
            r2.A07 = r2.A0K.getTop();
            return;
        }
        r2.A01();
    }
}
