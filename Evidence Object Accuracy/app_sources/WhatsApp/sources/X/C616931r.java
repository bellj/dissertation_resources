package X;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import com.whatsapp.util.Log;

/* renamed from: X.31r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616931r extends AbstractC68833Wx {
    public static final String[] A01 = {"_id", "width", "height"};
    public final int A00;

    @Override // X.AbstractC35611iN
    public int getType() {
        return 0;
    }

    public C616931r(ContentResolver contentResolver, Uri uri, String str, String str2, int i, long j, long j2, long j3) {
        super(contentResolver, uri, str, str2, j, j2, j3);
        this.A00 = i;
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        boolean z;
        Bitmap bitmap;
        boolean z2 = false;
        try {
            if (i < 144) {
                z = false;
                bitmap = AnonymousClass3HO.A00().A01(this.A03, null, 3, this.A02);
            } else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                boolean z3 = false;
                if (Build.VERSION.SDK_INT <= 18) {
                    z3 = true;
                }
                if (z3) {
                    options.inInputShareable = true;
                    options.inPurgeable = true;
                    z = true;
                } else {
                    z = false;
                }
                ContentResolver contentResolver = this.A03;
                long j = this.A02;
                Cursor queryMiniThumbnail = MediaStore.Images.Thumbnails.queryMiniThumbnail(contentResolver, j, 1, A01);
                if (queryMiniThumbnail != null) {
                    try {
                        if (queryMiniThumbnail.moveToFirst()) {
                            long j2 = (long) i;
                            options.inSampleSize = AnonymousClass3Il.A00(queryMiniThumbnail.getInt(1), queryMiniThumbnail.getInt(2), i, j2 * j2 * 2);
                        }
                    } catch (Throwable th) {
                        try {
                            queryMiniThumbnail.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                if (queryMiniThumbnail != null) {
                    queryMiniThumbnail.close();
                }
                bitmap = AnonymousClass3HO.A00().A01(contentResolver, options, 1, j);
            }
            if (bitmap == null) {
                long j3 = (long) i;
                bitmap = A00(i, j3 * j3 * 2);
            }
            int i2 = this.A00;
            if (Build.VERSION.SDK_INT >= 29 || bitmap == null || i2 == 0) {
                z2 = z;
            } else {
                bitmap = AnonymousClass3Il.A01(bitmap, i2);
            }
            return (bitmap == null || z2 || !AnonymousClass1O1.A00()) ? bitmap : C22200yh.A09(bitmap);
        } catch (Throwable th2) {
            Log.e("miniThumbBitmap got exception", th2);
            return null;
        }
    }
}
