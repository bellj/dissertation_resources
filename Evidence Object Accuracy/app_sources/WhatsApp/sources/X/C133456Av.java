package X;

import java.io.File;

/* renamed from: X.6Av  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133456Av implements AnonymousClass6MV {
    public final /* synthetic */ C129265xR A00;
    public final /* synthetic */ C128015vQ A01;
    public final /* synthetic */ C14370lK A02;
    public final /* synthetic */ File A03;

    public C133456Av(C129265xR r1, C128015vQ r2, C14370lK r3, File file) {
        this.A00 = r1;
        this.A03 = file;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r4) {
        this.A00.A02.A06.execute(new Runnable() { // from class: X.6FZ
            @Override // java.lang.Runnable
            public final void run() {
                C128015vQ r0 = C128015vQ.this;
                r0.A02.A2n(r0.A01);
            }
        });
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r5) {
        C129265xR r3 = this.A00;
        File file = this.A03;
        r3.A00(r5, this.A01, this.A02, file);
    }
}
