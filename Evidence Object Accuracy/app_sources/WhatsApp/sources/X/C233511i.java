package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.11i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C233511i {
    public C234111o A00;
    public final AbstractC15710nm A01;
    public final C21910yB A02;

    public C233511i(AbstractC15710nm r1, C21910yB r2, C234111o r3) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = r3;
    }

    public static final AnonymousClass1JR A00(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("device_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("epoch");
        if (cursor.isNull(columnIndexOrThrow) || cursor.isNull(columnIndexOrThrow2)) {
            return null;
        }
        return new AnonymousClass1JR(cursor.getInt(columnIndexOrThrow), cursor.getInt(columnIndexOrThrow2));
    }

    public static final void A01(C16330op r5, String[] strArr) {
        AnonymousClass009.A0E(r5.A00.inTransaction());
        Iterator it = new C29841Uw(strArr, 975).iterator();
        while (it.hasNext()) {
            String[] strArr2 = (String[]) it.next();
            int length = strArr2.length;
            StringBuilder sb = new StringBuilder("DELETE FROM pending_mutations WHERE _id IN ( ");
            sb.append(TextUtils.join(",", Collections.nCopies(length, "?")));
            sb.append(" )");
            r5.A0C(sb.toString(), strArr2);
        }
    }

    public static final void A02(C16330op r5, String[] strArr) {
        AnonymousClass009.A0E(r5.A00.inTransaction());
        Iterator it = new C29841Uw(strArr, 975).iterator();
        while (it.hasNext()) {
            String[] strArr2 = (String[]) it.next();
            int length = strArr2.length;
            StringBuilder sb = new StringBuilder("DELETE FROM syncd_mutations WHERE mutation_index IN ");
            sb.append(AnonymousClass1Ux.A00(length));
            r5.A0C(sb.toString(), strArr2);
        }
    }

    public final AnonymousClass1JQ A03(Cursor cursor) {
        C234111o r5 = this.A00;
        boolean z = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("are_dependencies_missing")) == 1) {
            z = true;
        }
        String string = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        return r5.A01(A00(cursor), string, cursor.getString(cursor.getColumnIndexOrThrow("mutation_index")), cursor.getBlob(cursor.getColumnIndexOrThrow("mutation_value")), cursor.getBlob(cursor.getColumnIndexOrThrow("operation")), null, cursor.getInt(cursor.getColumnIndexOrThrow("mutation_version")), z);
    }

    public final AnonymousClass1JQ A04(Cursor cursor) {
        C234111o r5 = this.A00;
        boolean z = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("are_dependencies_missing")) == 1) {
            z = true;
        }
        AnonymousClass1JR A00 = A00(cursor);
        AnonymousClass009.A05(A00);
        return r5.A01(A00, null, cursor.getString(cursor.getColumnIndexOrThrow("mutation_index")), cursor.getBlob(cursor.getColumnIndexOrThrow("mutation_value")), C27791Jf.A03.A01, cursor.getBlob(cursor.getColumnIndexOrThrow("mutation_mac")), cursor.getInt(cursor.getColumnIndexOrThrow("mutation_version")), z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r1 != null) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1JQ A05(java.lang.String r6) {
        /*
            r5 = this;
            X.0yB r0 = r5.A02
            X.0on r4 = r0.get()
            X.0op r3 = r4.A03     // Catch: all -> 0x0030
            java.lang.String r2 = "SELECT _id, mutation_index, mutation_value, mutation_version, operation, device_id, epoch, are_dependencies_missing FROM pending_mutations WHERE mutation_index = ?"
            r0 = 1
            java.lang.String[] r1 = new java.lang.String[r0]     // Catch: all -> 0x0030
            r0 = 0
            r1[r0] = r6     // Catch: all -> 0x0030
            android.database.Cursor r1 = r3.A09(r2, r1)     // Catch: all -> 0x0030
            if (r1 == 0) goto L_0x0026
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0021
            if (r0 == 0) goto L_0x0026
            X.1JQ r0 = r5.A03(r1)     // Catch: all -> 0x0021
            goto L_0x0029
        L_0x0021:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0025
        L_0x0025:
            throw r0     // Catch: all -> 0x0030
        L_0x0026:
            r0 = 0
            if (r1 == 0) goto L_0x002c
        L_0x0029:
            r1.close()     // Catch: all -> 0x0030
        L_0x002c:
            r4.close()
            return r0
        L_0x0030:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0034
        L_0x0034:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C233511i.A05(java.lang.String):X.1JQ");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r1 != null) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1JQ A06(java.lang.String r6) {
        /*
            r5 = this;
            X.0yB r0 = r5.A02
            X.0on r4 = r0.get()
            X.0op r3 = r4.A03     // Catch: all -> 0x0030
            java.lang.String r2 = "SELECT mutation_index, mutation_value, mutation_version, are_dependencies_missing, device_id, epoch, mutation_mac FROM syncd_mutations WHERE mutation_index = ? "
            r0 = 1
            java.lang.String[] r1 = new java.lang.String[r0]     // Catch: all -> 0x0030
            r0 = 0
            r1[r0] = r6     // Catch: all -> 0x0030
            android.database.Cursor r1 = r3.A09(r2, r1)     // Catch: all -> 0x0030
            if (r1 == 0) goto L_0x0026
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0021
            if (r0 == 0) goto L_0x0026
            X.1JQ r0 = r5.A04(r1)     // Catch: all -> 0x0021
            goto L_0x0029
        L_0x0021:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0025
        L_0x0025:
            throw r0     // Catch: all -> 0x0030
        L_0x0026:
            r0 = 0
            if (r1 == 0) goto L_0x002c
        L_0x0029:
            r1.close()     // Catch: all -> 0x0030
        L_0x002c:
            r4.close()
            return r0
        L_0x0030:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0034
        L_0x0034:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C233511i.A06(java.lang.String):X.1JQ");
    }

    public final List A07(AbstractC34711ga r5, String str, String[] strArr) {
        AnonymousClass1JQ A04;
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            while (A09.moveToNext()) {
                if (r5.A96(A09.getString(A09.getColumnIndexOrThrow("mutation_index"))) && (A04 = A04(A09)) != null) {
                    arrayList.add(A04);
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A08(AbstractC14640lm r8, Set set, boolean z) {
        String obj;
        ArrayList arrayList = new ArrayList();
        if (set.isEmpty()) {
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r8.getRawString());
        arrayList2.addAll(set);
        C16310on A01 = this.A02.get();
        try {
            C16330op r5 = A01.A03;
            if (z) {
                int size = set.size();
                StringBuilder sb = new StringBuilder("SELECT _id, mutation_index, mutation_value, mutation_version, operation, device_id, epoch, are_dependencies_missing FROM pending_mutations WHERE chat_jid = ?  AND mutation_name IN ");
                sb.append(AnonymousClass1Ux.A00(size));
                obj = sb.toString();
            } else {
                int size2 = set.size();
                StringBuilder sb2 = new StringBuilder("SELECT mutation_index, mutation_value, mutation_version, are_dependencies_missing, device_id, epoch, mutation_mac FROM syncd_mutations WHERE chat_jid = ?  AND mutation_name IN ");
                sb2.append(AnonymousClass1Ux.A00(size2));
                obj = sb2.toString();
            }
            Cursor A09 = r5.A09(obj, (String[]) arrayList2.toArray(AnonymousClass01V.A0H));
            while (A09.moveToNext()) {
                AnonymousClass1JQ A03 = z ? A03(A09) : A04(A09);
                if (A03 != null) {
                    arrayList.add(A03);
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final List A09(String str, boolean z) {
        String str2;
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A02.get();
        try {
            C16330op r3 = A01.A03;
            if (z) {
                str2 = "SELECT _id, mutation_index, mutation_value, mutation_version, operation, device_id, epoch, are_dependencies_missing FROM pending_mutations WHERE mutation_name = ?";
            } else {
                str2 = "SELECT mutation_index, mutation_value, mutation_version, are_dependencies_missing, device_id, epoch, mutation_mac FROM syncd_mutations WHERE mutation_name = ?";
            }
            Cursor A09 = r3.A09(str2, new String[]{str});
            while (A09.moveToNext()) {
                AnonymousClass1JQ A03 = z ? A03(A09) : A04(A09);
                if (A03 != null) {
                    arrayList.add(A03);
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A0A(Set set, int i) {
        ArrayList arrayList = new ArrayList(set);
        arrayList.add(String.valueOf(i));
        ArrayList arrayList2 = new ArrayList();
        C16310on A01 = this.A02.get();
        try {
            C16330op r4 = A01.A03;
            int size = set.size();
            StringBuilder sb = new StringBuilder("SELECT _id, mutation_index, mutation_value, mutation_version, operation, device_id, epoch, are_dependencies_missing FROM pending_mutations WHERE collection_name IN ");
            sb.append(AnonymousClass1Ux.A00(size));
            sb.append(" OR ");
            sb.append("collection_name");
            sb.append(" IS NULL  ORDER BY ");
            sb.append("_id");
            sb.append(" ASC  LIMIT ?");
            Cursor A09 = r4.A09(sb.toString(), (String[]) arrayList.toArray(AnonymousClass01V.A0H));
            while (A09.moveToNext()) {
                arrayList2.add(A03(A09));
            }
            A09.close();
            A01.close();
            return arrayList2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Set A0B(Collection collection) {
        byte[] bArr;
        byte[] bArr2;
        HashSet hashSet = new HashSet();
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT OR REPLACE INTO pending_mutations (mutation_index, mutation_value, mutation_version, operation, device_id, epoch, is_ready_to_sync, collection_name, are_dependencies_missing, mutation_name, chat_jid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                AnonymousClass1JQ r6 = (AnonymousClass1JQ) it.next();
                if (this.A00.A02(r6.A03())) {
                    String str = r6.A06;
                    SQLiteStatement sQLiteStatement = A0A.A00;
                    sQLiteStatement.clearBindings();
                    A0A.A02(1, AnonymousClass1JQ.A00(r6.A06()));
                    C27831Jk A022 = r6.A02();
                    if (A022 == null) {
                        bArr = null;
                    } else {
                        bArr = A022.A02();
                    }
                    if (bArr != null) {
                        C27831Jk A023 = r6.A02();
                        if (A023 == null) {
                            bArr2 = null;
                        } else {
                            bArr2 = A023.A02();
                        }
                        sQLiteStatement.bindBlob(2, bArr2);
                    } else {
                        sQLiteStatement.bindNull(2);
                    }
                    A0A.A01(3, (long) r6.A03);
                    sQLiteStatement.bindBlob(4, r6.A05.A01);
                    AnonymousClass1JR r0 = r6.A00;
                    if (r0 == null) {
                        sQLiteStatement.bindNull(5);
                        sQLiteStatement.bindNull(6);
                    } else {
                        A0A.A01(5, (long) r0.A00());
                        A0A.A01(6, (long) r6.A00.A01());
                    }
                    long j = 0;
                    A0A.A01(7, 0);
                    A0A.A02(8, str);
                    if (r6.A05()) {
                        j = 1;
                    }
                    A0A.A01(9, j);
                    A0A.A02(10, r6.A03());
                    if (r6 instanceof AbstractC34381g3) {
                        A0A.A02(11, ((AbstractC34381g3) r6).ABH().getRawString());
                    } else {
                        sQLiteStatement.bindNull(11);
                    }
                    hashSet.add(String.valueOf(sQLiteStatement.executeInsert()));
                }
            }
            A00.A00();
            A00.close();
            A02.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0C(C16330op r6, AnonymousClass1JR r7, AbstractC14640lm r8, String str, String str2, String str3, byte[] bArr, byte[] bArr2, int i, boolean z) {
        long j;
        if (r7.A01() == 0) {
            AbstractC15710nm r3 = this.A01;
            StringBuilder sb = new StringBuilder("keyId=");
            sb.append(r7);
            r3.AaV("syncdMutationStore/insertOrReplaceMutation unexpected key", sb.toString(), true);
        }
        AnonymousClass1YE A0A = r6.A0A("INSERT OR REPLACE INTO syncd_mutations (mutation_index, mutation_value, mutation_version, collection_name, are_dependencies_missing, device_id, epoch, mutation_mac, chat_jid, mutation_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        SQLiteStatement sQLiteStatement = A0A.A00;
        sQLiteStatement.clearBindings();
        A0A.A02(1, str);
        if (bArr == null) {
            sQLiteStatement.bindNull(2);
        } else {
            sQLiteStatement.bindBlob(2, bArr);
        }
        A0A.A01(3, (long) i);
        A0A.A02(4, str2);
        if (z) {
            j = 1;
        } else {
            j = 0;
        }
        A0A.A01(5, j);
        A0A.A01(6, (long) r7.A00());
        A0A.A01(7, (long) r7.A01());
        sQLiteStatement.bindBlob(8, bArr2);
        if (r8 == null) {
            sQLiteStatement.bindNull(9);
        } else {
            A0A.A02(9, r8.getRawString());
        }
        A0A.A02(10, str3);
        if (sQLiteStatement.executeInsert() == -1) {
            Log.e("SyncdMutationsStore/insertOrReplaceMutation was unsuccessful");
        }
    }

    public final void A0D(C16330op r17, Collection collection) {
        byte[] A02;
        AbstractC14640lm r8;
        AnonymousClass009.A0E(r17.A00.inTransaction());
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1JQ r1 = (AnonymousClass1JQ) it.next();
            C27791Jf r2 = r1.A05;
            if (r2 == C27791Jf.A03) {
                arrayList.add(r1);
            } else if (r2 == C27791Jf.A02) {
                arrayList2.add(r1);
            } else {
                StringBuilder sb = new StringBuilder("Incorrect operation: ");
                sb.append(r2);
                throw new IllegalStateException(sb.toString());
            }
        }
        A02(r17, AnonymousClass1K2.A02(arrayList2));
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            AnonymousClass1JQ r12 = (AnonymousClass1JQ) it2.next();
            String str = r12.A06;
            String A00 = AnonymousClass1JQ.A00(r12.A06());
            C27831Jk A022 = r12.A02();
            if (A022 == null) {
                A02 = null;
            } else {
                A02 = A022.A02();
            }
            int i = r12.A03;
            boolean A05 = r12.A05();
            AnonymousClass1JR r7 = r12.A00;
            AnonymousClass009.A05(r7);
            byte[] bArr = r12.A02;
            AnonymousClass009.A05(bArr);
            if (r12 instanceof AbstractC34381g3) {
                r8 = ((AbstractC34381g3) r12).ABH();
            } else {
                r8 = null;
            }
            A0C(r17, r7, r8, A00, str, r12.A03(), A02, bArr, i, A05);
        }
    }

    public void A0E(AnonymousClass1JQ r7) {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A01(A02.A03, new String[]{r7.A07});
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0F(Collection collection) {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A0H(A0B(collection));
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0G(Collection collection) {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A0D(A02.A03, collection);
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0H(Set set) {
        if (!set.isEmpty()) {
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                Iterator it = new C29841Uw((String[]) set.toArray(AnonymousClass01V.A0H), 975).iterator();
                while (it.hasNext()) {
                    String[] strArr = (String[]) it.next();
                    C16330op r3 = A02.A03;
                    int length = strArr.length;
                    StringBuilder sb = new StringBuilder("UPDATE pending_mutations SET is_ready_to_sync = 1 WHERE _id IN ( ");
                    sb.append(TextUtils.join(",", Collections.nCopies(length, "?")));
                    sb.append(" )");
                    r3.A0C(sb.toString(), strArr);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public boolean A0I() {
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id FROM pending_mutations WHERE is_ready_to_sync = 1  LIMIT 1", null);
            boolean z = false;
            if (A09 != null) {
                if (A09.moveToNext() && A09.getString(0) != null) {
                    z = true;
                }
                A09.close();
            }
            A01.close();
            return z;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0J() {
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id FROM syncd_mutations LIMIT 1", null);
            boolean z = false;
            if (A09 != null) {
                if (A09.moveToNext() && A09.getString(0) != null) {
                    z = true;
                }
                A09.close();
            }
            A01.close();
            return z;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0K(Set set) {
        C16330op AHr = this.A02.AHr();
        int size = set.size();
        StringBuilder sb = new StringBuilder("SELECT _id FROM syncd_mutations WHERE collection_name IN ");
        sb.append(AnonymousClass1Ux.A00(size));
        sb.append(" LIMIT 1");
        Cursor A09 = AHr.A09(sb.toString(), (String[]) set.toArray(AnonymousClass01V.A0H));
        boolean z = false;
        if (A09 != null) {
            try {
                if (A09.moveToNext()) {
                    if (A09.getString(0) != null) {
                        z = true;
                    }
                }
                A09.close();
            } catch (Throwable th) {
                try {
                    A09.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return z;
    }
}
