package X;

import com.whatsapp.gallery.MediaGalleryActivity;
import java.util.Iterator;

/* renamed from: X.2z4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2z4 extends AnonymousClass1MD {
    public final /* synthetic */ MediaGalleryActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2z4(AnonymousClass12P r44, AbstractC15710nm r45, C14900mE r46, C15570nT r47, C15450nH r48, C16170oZ r49, ActivityC13790kL r50, AnonymousClass12T r51, C18850tA r52, C15550nR r53, C22700zV r54, C15610nY r55, AnonymousClass1A6 r56, C255419u r57, AnonymousClass01d r58, C14830m7 r59, C14820m6 r60, AnonymousClass018 r61, C15600nX r62, C253218y r63, C242114q r64, C22100yW r65, C22180yf r66, AnonymousClass19M r67, C231510o r68, AnonymousClass193 r69, C14850m9 r70, C16120oU r71, MediaGalleryActivity mediaGalleryActivity, C20710wC r73, AnonymousClass109 r74, C22370yy r75, AnonymousClass13H r76, C16630pM r77, C88054Ec r78, AnonymousClass12F r79, C253018w r80, AnonymousClass12U r81, C23000zz r82, C252718t r83, AbstractC14440lR r84, AnonymousClass01H r85) {
        super(r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r73, r74, r75, r76, r77, r78, r79, r80, r81, r82, r83, r84, r85);
        this.A00 = mediaGalleryActivity;
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        MediaGalleryActivity mediaGalleryActivity = this.A00;
        C35451ht r1 = mediaGalleryActivity.A0F;
        if (r1 != null) {
            r1.A00();
            mediaGalleryActivity.A0F = null;
        }
        mediaGalleryActivity.A06 = null;
        Iterator A0o = ActivityC13810kN.A0o(mediaGalleryActivity);
        while (A0o.hasNext()) {
            AnonymousClass01E r12 = (AnonymousClass01E) A0o.next();
            if (r12 instanceof AbstractC35481hz) {
                ((AbstractC35481hz) r12).AVm();
            }
        }
    }
}
