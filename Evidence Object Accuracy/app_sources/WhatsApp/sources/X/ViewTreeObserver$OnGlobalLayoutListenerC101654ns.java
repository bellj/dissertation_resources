package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.status.playback.MessageReplyActivity;
import com.whatsapp.status.playback.StatusReplyActivity;

/* renamed from: X.4ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101654ns implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ StatusReplyActivity A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101654ns(StatusReplyActivity statusReplyActivity) {
        this.A00 = statusReplyActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        StatusReplyActivity statusReplyActivity = this.A00;
        statusReplyActivity.A2n();
        View view = ((MessageReplyActivity) statusReplyActivity).A04;
        Runnable runnable = statusReplyActivity.A07;
        view.removeCallbacks(runnable);
        ((MessageReplyActivity) statusReplyActivity).A04.postDelayed(runnable, (long) statusReplyActivity.getResources().getInteger(17694722));
    }
}
