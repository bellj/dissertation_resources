package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* renamed from: X.34r  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass34r extends AnonymousClass2V0 {
    public AbstractC84543zT A00;

    public abstract String getDefaultMessageText();

    public abstract int getDrawableRes();

    public AnonymousClass34r(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
    }

    @Override // X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ CharSequence A02(C15370n3 r17, AbstractC15340mz r18) {
        Drawable A01 = AnonymousClass2GE.A01(getContext(), getDrawableRes(), R.color.msgStatusTint);
        TextPaint paint = ((AnonymousClass2V0) this).A01.getPaint();
        int textSize = ((int) paint.getTextSize()) + getIconSizeIncrease();
        SpannableStringBuilder A0J = C12990iw.A0J(C12960it.A0b("  ", ""));
        C52252aV.A02(paint, A01, A0J, textSize, 0, 1);
        CharSequence A012 = AnonymousClass3J0.A01(getContext(), this.A08, this.A0A, this.A0F, r17, r18.A0z.A02);
        if (TextUtils.isEmpty(A012)) {
            return A0J;
        }
        boolean A0G = C42941w9.A0G(A0J);
        CharSequence[] charSequenceArr = new CharSequence[4];
        charSequenceArr[0] = A012;
        char c = 8207;
        if (A0G) {
            c = 8206;
        }
        String valueOf = String.valueOf(c);
        charSequenceArr[1] = valueOf;
        charSequenceArr[2] = A0J;
        charSequenceArr[3] = valueOf;
        return TextUtils.concat(charSequenceArr);
    }

    public int getIconSizeIncrease() {
        return C12960it.A09(this).getDimensionPixelSize(R.dimen.search_icon_label_size_increase);
    }

    public void setThumbnailOnClickListener(View.OnClickListener onClickListener) {
        this.A00.setId(R.id.thumb_view);
        this.A00.setOnClickListener(onClickListener);
    }

    public void setUpThumbView(AbstractC84543zT r4) {
        r4.setRadius(getResources().getDimensionPixelSize(R.dimen.search_media_thumbnail_corner_radius));
        r4.setLayoutParams(new FrameLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.search_media_thumbnail_size), C12990iw.A07(this, R.dimen.search_media_thumbnail_size)));
        C42941w9.A07(r4, this.A0F, C12980iv.A06(this), 0);
    }
}
