package X;

import android.os.Bundle;

/* renamed from: X.4Ui  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92054Ui {
    public final Bundle A00;

    public C92054Ui(Bundle bundle) {
        if (bundle != null) {
            C65273Iw.A03("capabilities", bundle);
            this.A00 = bundle;
            return;
        }
        throw C12970iu.A0f("Bundle is null");
    }

    public byte[] A00() {
        Bundle bundle = this.A00;
        if (bundle.containsKey("capabilities")) {
            return bundle.getByteArray("capabilities");
        }
        throw C72463ee.A0D();
    }
}
