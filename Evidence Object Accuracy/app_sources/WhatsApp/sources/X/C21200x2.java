package X;

import android.os.SystemClock;

/* renamed from: X.0x2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21200x2 {
    public int A00 = 1;
    public int A01 = -1;
    public long A02;
    public final C16120oU A03;

    public C21200x2(C16120oU r2) {
        this.A03 = r2;
    }

    public void A00() {
        if (this.A01 == 1) {
            this.A01 = 0;
            this.A00 = 1;
        }
    }

    public void A01(String str) {
        if (this.A01 != 1) {
            this.A01 = -1;
            return;
        }
        C461024m r2 = new C461024m();
        r2.A01 = Long.valueOf(SystemClock.elapsedRealtime() - this.A02);
        r2.A02 = str;
        r2.A00 = Integer.valueOf(this.A00);
        this.A03.A08(r2, 650);
        this.A01 = -1;
        this.A00 = 1;
    }
}
