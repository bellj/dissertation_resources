package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.6B0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B0 implements AnonymousClass6MW {
    public final /* synthetic */ PinBottomSheetDialogFragment A00;
    public final /* synthetic */ AbstractC1308360d A01;

    public AnonymousClass6B0(PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC1308360d r2) {
        this.A01 = r2;
        this.A00 = pinBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass6MW
    public void APo(C452120p r6) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        if (pinBottomSheetDialogFragment != null) {
            pinBottomSheetDialogFragment.A1M();
        }
        Log.e(C12960it.A0b("PAY: FbPayHubActivity/PaymentStepUpWebviewAction onError: ", r6));
        int i = r6.A00;
        if (i == 1441) {
            AbstractC1308360d r1 = this.A01;
            C130015yf r0 = r1.A0I;
            long j = r6.A02;
            r0.A02(j);
            if (pinBottomSheetDialogFragment != null) {
                pinBottomSheetDialogFragment.A1P(j * 1000, true);
            } else {
                r1.A01();
            }
        } else {
            if (i == 1440) {
                if (pinBottomSheetDialogFragment != null) {
                    pinBottomSheetDialogFragment.A1O(r6.A01);
                    return;
                }
            } else if (i == 455) {
                if (pinBottomSheetDialogFragment != null) {
                    pinBottomSheetDialogFragment.A1C();
                }
                this.A01.A00();
                return;
            } else {
                if (i == 1448) {
                    this.A01.A0G.A02(r6, "FB", "PIN");
                }
                if (pinBottomSheetDialogFragment != null) {
                    pinBottomSheetDialogFragment.A1C();
                }
            }
            this.A01.A01();
        }
    }

    @Override // X.AnonymousClass6MW
    public void AX6(String str, String str2) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        if (pinBottomSheetDialogFragment != null) {
            pinBottomSheetDialogFragment.A1C();
        }
        AbstractC1308360d r3 = this.A01;
        ActivityC13790kL r2 = r3.A05;
        AnonymousClass009.A04(str);
        r2.startActivityForResult(C14960mK.A0a(r2, str, str2, true, true), r3.A00);
    }
}
