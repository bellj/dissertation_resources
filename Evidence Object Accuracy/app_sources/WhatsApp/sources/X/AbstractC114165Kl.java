package X;

/* renamed from: X.5Kl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC114165Kl extends AbstractC10990fX {
    public long A00;
    public C91124Qn A01;
    public boolean A02;

    public abstract void A07();

    @Override // X.AbstractC10990fX
    public final AbstractC10990fX A02(int i) {
        C88264Ex.A00(i);
        return this;
    }

    public long A05() {
        C91124Qn r0 = this.A01;
        if (r0 == null || r0.A00 == r0.A01) {
            return Long.MAX_VALUE;
        }
        return 0;
    }

    public final void A06() {
        long j = this.A00 - 4294967296L;
        this.A00 = j;
        if (j <= 0 && this.A02) {
            A07();
        }
    }

    public final void A08(AnonymousClass5LK r8) {
        C91124Qn r6 = this.A01;
        if (r6 == null) {
            r6 = new C91124Qn();
            this.A01 = r6;
        }
        Object[] objArr = r6.A02;
        int i = r6.A01;
        objArr[i] = r8;
        int length = objArr.length;
        int i2 = (length - 1) & (i + 1);
        r6.A01 = i2;
        int i3 = r6.A00;
        if (i2 == i3) {
            Object[] objArr2 = new Object[length << 1];
            System.arraycopy(objArr, i3, objArr2, 0, length - i3);
            Object[] objArr3 = r6.A02;
            int length2 = objArr3.length;
            int i4 = r6.A00;
            System.arraycopy(objArr3, 0, objArr2, length2 - i4, i4 - 0);
            r6.A02 = objArr2;
            r6.A00 = 0;
            r6.A01 = length;
        }
    }

    public final boolean A09() {
        C91124Qn r0 = this.A01;
        if (r0 == null) {
            return true;
        }
        return C12960it.A1V(r0.A00, r0.A01);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0A() {
        /*
            r6 = this;
            X.4Qn r5 = r6.A01
            r4 = 0
            if (r5 == 0) goto L_0x002d
            int r3 = r5.A00
            int r0 = r5.A01
            r1 = 0
            if (r3 == r0) goto L_0x001d
            java.lang.Object[] r0 = r5.A02
            r2 = r0[r3]
            r0[r3] = r1
            int r1 = r3 + 1
            int r0 = r0.length
            int r0 = r0 + -1
            r1 = r1 & r0
            r5.A00 = r1
            if (r2 == 0) goto L_0x0026
            r1 = r2
        L_0x001d:
            X.5LK r1 = (X.AnonymousClass5LK) r1
            if (r1 == 0) goto L_0x002d
            r1.run()
            r0 = 1
            return r0
        L_0x0026:
            java.lang.String r0 = "null cannot be cast to non-null type T of kotlinx.coroutines.internal.ArrayQueue"
            java.lang.NullPointerException r0 = X.C12980iv.A0n(r0)
            throw r0
        L_0x002d:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC114165Kl.A0A():boolean");
    }
}
