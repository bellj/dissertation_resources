package X;

import android.util.Pair;
import java.util.Map;

/* renamed from: X.2kC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56022kC extends AbstractC56052kF {
    public final int A00;
    public final C56032kD A01;
    public final Map A02 = C12970iu.A11();
    public final Map A03 = C12970iu.A11();

    public C56022kC(AnonymousClass2CD r3, int i) {
        C95314dV.A03(C12960it.A1U(i));
        this.A01 = new C56032kD(r3, false);
        this.A00 = i;
    }

    @Override // X.AbstractC56052kF, X.AbstractC67703Sn
    public void A02(AnonymousClass5QP r2) {
        super.A02(r2);
        A04(this.A01);
    }

    @Override // X.AnonymousClass2CD
    public AbstractC14080kp A8S(C28741Ov r4, AnonymousClass5VZ r5, long j) {
        if (this.A00 == Integer.MAX_VALUE) {
            return this.A01.A8S(r4, r5, j);
        }
        C28741Ov A01 = r4.A01(((Pair) r4.A04).second);
        this.A02.put(A01, r4);
        C67683Sl A05 = this.A01.A8S(A01, r5, j);
        this.A03.put(A05, A01);
        return A05;
    }

    @Override // X.AnonymousClass2CD
    public AnonymousClass4XL AED() {
        return this.A01.A07.AED();
    }

    @Override // X.AnonymousClass2CD
    public void Aa9(AbstractC14080kp r3) {
        this.A01.Aa9(r3);
        Object remove = this.A03.remove(r3);
        if (remove != null) {
            this.A02.remove(remove);
        }
    }
}
