package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5JQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5JQ extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ View $itemView;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5JQ(View view) {
        super(0);
        this.$itemView = view;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return C16700pc.A02(this.$itemView, R.id.image_category_list_item);
    }
}
