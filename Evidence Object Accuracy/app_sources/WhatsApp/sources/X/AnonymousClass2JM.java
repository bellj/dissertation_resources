package X;

import android.database.ContentObserver;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

/* renamed from: X.2JM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JM implements AbstractC35581iK {
    public int A00;
    public int A01;
    public int[] A02;
    public long[] A03;
    public final PriorityQueue A04;
    public final AbstractC35581iK[] A05;

    public AnonymousClass2JM(AbstractC35581iK[] r6, int i) {
        Comparator r2;
        AbstractC35581iK[] r4 = (AbstractC35581iK[]) r6.clone();
        this.A05 = r4;
        if (i == 1) {
            r2 = new AnonymousClass5CH();
        } else {
            r2 = new AnonymousClass5CI();
        }
        PriorityQueue priorityQueue = new PriorityQueue(4, r2);
        this.A04 = priorityQueue;
        this.A03 = new long[16];
        this.A01 = 0;
        int length = r4.length;
        this.A02 = new int[length];
        this.A00 = -1;
        priorityQueue.clear();
        for (int i2 = 0; i2 < length; i2++) {
            C92414Vu r1 = new C92414Vu(this.A05[i2], i2);
            if (r1.A00()) {
                this.A04.add(r1);
            }
        }
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        HashMap hashMap = new HashMap();
        for (AbstractC35581iK r0 : this.A05) {
            hashMap.putAll(r0.AAz());
        }
        return hashMap;
    }

    @Override // X.AbstractC35581iK
    public AbstractC35611iN AEC(int i) {
        if (i < 0 || i > getCount()) {
            StringBuilder sb = new StringBuilder("index ");
            sb.append(i);
            sb.append(" out of range max is ");
            sb.append(getCount());
            throw new IndexOutOfBoundsException(sb.toString());
        }
        int[] iArr = this.A02;
        int i2 = 0;
        Arrays.fill(iArr, 0);
        int i3 = this.A01;
        int i4 = 0;
        while (i2 < i3) {
            long j = this.A03[i2];
            int i5 = (int) (-1 & j);
            int i6 = (int) (j >> 32);
            int i7 = i4 + i5;
            if (i7 > i) {
                return this.A05[i6].AEC(iArr[i6] + (i - i4));
            }
            iArr[i6] = iArr[i6] + i5;
            i2++;
            i4 = i7;
        }
        while (true) {
            PriorityQueue priorityQueue = this.A04;
            C92414Vu r5 = (C92414Vu) priorityQueue.poll();
            if (r5 == null) {
                return null;
            }
            int i8 = r5.A03;
            if (i8 == this.A00) {
                int i9 = this.A01 - 1;
                long[] jArr = this.A03;
                jArr[i9] = jArr[i9] + 1;
            } else {
                this.A00 = i8;
                long[] jArr2 = this.A03;
                int length = jArr2.length;
                int i10 = this.A01;
                if (length == i10) {
                    long[] jArr3 = new long[i10 << 1];
                    System.arraycopy(jArr2, 0, jArr3, 0, i10);
                    this.A03 = jArr3;
                    jArr2 = jArr3;
                }
                int i11 = this.A01;
                this.A01 = i11 + 1;
                jArr2[i11] = 1 | (((long) this.A00) << 32);
            }
            if (i4 == i) {
                AbstractC35611iN r1 = r5.A02;
                if (!r5.A00()) {
                    return r1;
                }
                priorityQueue.add(r5);
                return r1;
            }
            if (r5.A00()) {
                priorityQueue.add(r5);
            }
            i4++;
        }
    }

    @Override // X.AbstractC35581iK
    public void AaX() {
        for (AbstractC35581iK r0 : this.A05) {
            r0.AaX();
        }
    }

    @Override // X.AbstractC35581iK
    public void close() {
        for (AbstractC35581iK r0 : this.A05) {
            r0.close();
        }
    }

    @Override // X.AbstractC35581iK
    public int getCount() {
        int i = 0;
        for (AbstractC35581iK r0 : this.A05) {
            i += r0.getCount();
        }
        return i;
    }

    @Override // X.AbstractC35581iK
    public boolean isEmpty() {
        for (AbstractC35581iK r0 : this.A05) {
            if (!r0.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AbstractC35581iK
    public void registerContentObserver(ContentObserver contentObserver) {
        for (AbstractC35581iK r0 : this.A05) {
            r0.registerContentObserver(contentObserver);
        }
    }

    @Override // X.AbstractC35581iK
    public void unregisterContentObserver(ContentObserver contentObserver) {
        for (AbstractC35581iK r0 : this.A05) {
            r0.unregisterContentObserver(contentObserver);
        }
    }
}
