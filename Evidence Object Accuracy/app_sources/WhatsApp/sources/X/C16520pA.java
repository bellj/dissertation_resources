package X;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.net.Uri;
import java.io.File;

/* renamed from: X.0pA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16520pA extends AbstractCursor {
    public static final String[] A05 = {"_id", "_data", "date", "title", "mime_type", "media_type", "duration"};
    public int A00 = -1;
    public Cursor A01;
    public boolean A02;
    public final C15650ng A03;
    public final AbstractC14640lm A04;

    @Override // android.database.AbstractCursor, android.database.Cursor
    public double getDouble(int i) {
        return 0.0d;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public float getFloat(int i) {
        return 0.0f;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getInt(int i) {
        return 0;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getType(int i) {
        return (i == 0 || i == 2 || i == 5 || i == 6) ? 1 : 3;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public boolean isNull(int i) {
        return false;
    }

    public C16520pA(Cursor cursor, C15650ng r3, AbstractC14640lm r4, boolean z) {
        this.A03 = r3;
        this.A01 = cursor;
        this.A04 = r4;
        this.A02 = z;
        moveToPosition(0);
    }

    public AbstractC16130oV A00() {
        AbstractC15340mz A02;
        AbstractC14640lm r4 = this.A04;
        C15650ng r0 = this.A03;
        Cursor cursor = this.A01;
        if (r4 == null) {
            A02 = r0.A0K.A01(cursor);
        } else {
            A02 = r0.A0K.A02(cursor, r4, false, true);
        }
        return (AbstractC16130oV) A02;
    }

    public final boolean A01() {
        C16150oX r1;
        File file;
        AbstractC16130oV A00 = A00();
        if (A00 == null || (r1 = A00.A02) == null) {
            return false;
        }
        if ((!A00.A0z.A02 && !r1.A0P) || (file = r1.A0F) == null) {
            return this.A02 && (A00 instanceof AnonymousClass1X2) && C30041Vv.A16((AnonymousClass1X4) A00);
        }
        Uri fromFile = Uri.fromFile(file);
        if (fromFile == null || fromFile.getPath() == null) {
            return false;
        }
        return new File(fromFile.getPath()).exists();
    }

    @Override // android.database.AbstractCursor, java.io.Closeable, java.lang.AutoCloseable, android.database.Cursor
    public void close() {
        super.close();
        this.A01.close();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String[] getColumnNames() {
        return A05;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getCount() {
        int i = this.A00;
        return i < 0 ? this.A01.getCount() : i;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public long getLong(int i) {
        if (i == 0) {
            Cursor cursor = this.A01;
            return cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        } else if (i == 2) {
            AbstractC16130oV A00 = A00();
            AnonymousClass009.A05(A00);
            return A00.A0I;
        } else if (i != 6) {
            return 0;
        } else {
            AbstractC16130oV A002 = A00();
            AnonymousClass009.A05(A002);
            return (long) A002.A00;
        }
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public short getShort(int i) {
        if (i != 5) {
            return 0;
        }
        AbstractC16130oV A00 = A00();
        AnonymousClass009.A05(A00);
        return (short) A00.A0y;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String getString(int i) {
        C16150oX r0;
        File file;
        if (i == 0) {
            Cursor cursor = this.A01;
            return Long.toString(cursor.getLong(cursor.getColumnIndexOrThrow("_id")));
        } else if (i == 1) {
            AbstractC16130oV A00 = A00();
            if (A00 == null || (r0 = A00.A02) == null || (file = r0.A0F) == null) {
                return "";
            }
            return file.getAbsolutePath();
        } else if (i == 2) {
            AbstractC16130oV A002 = A00();
            if (A002 != null) {
                return Long.toString(A002.A0I);
            }
            return "";
        } else if (i == 3) {
            AbstractC16130oV A003 = A00();
            AnonymousClass009.A05(A003);
            return A003.A15();
        } else if (i != 4) {
            return "";
        } else {
            AbstractC16130oV A004 = A00();
            AnonymousClass009.A05(A004);
            byte b = A004.A0y;
            if (b == 1) {
                return "image/*";
            }
            if (b == 2) {
                return "audio/*";
            }
            if (b == 3) {
                return "video/*";
            }
            if (b != 9) {
                if (b == 13) {
                    return "image/gif";
                }
                if (b == 23 || b == 37 || b == 25) {
                    return "image/*";
                }
                if (b != 26) {
                    if (b == 28) {
                        return "video/*";
                    }
                    if (b != 29) {
                        return "";
                    }
                    return "image/gif";
                }
            }
            return A004.A06;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
        if (r9 >= r8) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005c, code lost:
        r3 = false;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0064, code lost:
        if (r7.A01.moveToPrevious() == false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006a, code lost:
        if (A01() == false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006c, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006d, code lost:
        if (r2 <= 0) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006f, code lost:
        r0 = new java.lang.StringBuilder("mediacursor/prev/skip ");
        r0.append(r2);
        com.whatsapp.util.Log.i(r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0080, code lost:
        if (r3 != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0082, code lost:
        r7.A01.moveToPosition(-1);
        r0 = "mediacursor/prev/notfound";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008a, code lost:
        r8 = r8 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008d, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0090, code lost:
        return true;
     */
    @Override // android.database.AbstractCursor, android.database.CrossProcessCursor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onMove(int r8, int r9) {
        /*
            r7 = this;
            int r0 = r9 << 1
            r5 = -1
            if (r8 <= r0) goto L_0x000b
            android.database.Cursor r0 = r7.A01
            r0.moveToPosition(r5)
            r8 = -1
        L_0x000b:
            r6 = 0
            r4 = 1
            if (r9 <= r8) goto L_0x005a
            r3 = 0
            r2 = 0
        L_0x0011:
            android.database.Cursor r0 = r7.A01
            boolean r0 = r0.moveToNext()
            if (r0 == 0) goto L_0x0020
            boolean r0 = r7.A01()
            if (r0 == 0) goto L_0x0057
            r3 = 1
        L_0x0020:
            if (r2 <= 0) goto L_0x0033
            java.lang.String r1 = "mediacursor/next/skip "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
        L_0x0033:
            if (r3 != 0) goto L_0x0054
            int r8 = r8 + r4
            r7.A00 = r8
            android.database.Cursor r0 = r7.A01
            r0.moveToPosition(r5)
            java.lang.String r0 = "mediacursor/next/realcount "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            int r0 = r7.A00
            r1.append(r0)
            java.lang.String r0 = r1.toString()
        L_0x004d:
            com.whatsapp.util.Log.i(r0)
            r7.onChange(r4)
            return r6
        L_0x0054:
            int r8 = r8 + 1
            goto L_0x000b
        L_0x0057:
            int r2 = r2 + 1
            goto L_0x0011
        L_0x005a:
            if (r9 >= r8) goto L_0x0090
            r3 = 0
            r2 = 0
        L_0x005e:
            android.database.Cursor r0 = r7.A01
            boolean r0 = r0.moveToPrevious()
            if (r0 == 0) goto L_0x006d
            boolean r0 = r7.A01()
            if (r0 == 0) goto L_0x008d
            r3 = 1
        L_0x006d:
            if (r2 <= 0) goto L_0x0080
            java.lang.String r1 = "mediacursor/prev/skip "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
        L_0x0080:
            if (r3 != 0) goto L_0x008a
            android.database.Cursor r0 = r7.A01
            r0.moveToPosition(r5)
            java.lang.String r0 = "mediacursor/prev/notfound"
            goto L_0x004d
        L_0x008a:
            int r8 = r8 + -1
            goto L_0x005a
        L_0x008d:
            int r2 = r2 + 1
            goto L_0x005e
        L_0x0090:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16520pA.onMove(int, int):boolean");
    }
}
