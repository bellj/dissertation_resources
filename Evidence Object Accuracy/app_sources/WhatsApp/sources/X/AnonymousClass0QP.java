package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0QP  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0QP {
    public final AnonymousClass00N A00;
    public final AnonymousClass00N A01;
    public final AnonymousClass00N A02;

    public abstract AnonymousClass0QP A02();

    public abstract void A05(int i);

    public abstract boolean A09(int i);

    public AnonymousClass0QP(AnonymousClass00N r1, AnonymousClass00N r2, AnonymousClass00N r3) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = r3;
    }

    public int A00(int i, int i2) {
        if (A09(i2)) {
            return ((AnonymousClass0GE) this).A05.readInt();
        }
        return i;
    }

    public Parcelable A01(Parcelable parcelable, int i) {
        if (!A09(i)) {
            return parcelable;
        }
        AnonymousClass0GE r0 = (AnonymousClass0GE) this;
        return r0.A05.readParcelable(r0.getClass().getClassLoader());
    }

    public AbstractC007203r A03() {
        String readString = ((AnonymousClass0GE) this).A05.readString();
        if (readString == null) {
            return null;
        }
        AnonymousClass0QP A02 = A02();
        try {
            AnonymousClass00N r4 = this.A01;
            Method method = (Method) r4.get(readString);
            if (method == null) {
                System.currentTimeMillis();
                method = Class.forName(readString, true, AnonymousClass0QP.class.getClassLoader()).getDeclaredMethod("read", AnonymousClass0QP.class);
                r4.put(readString, method);
            }
            return (AbstractC007203r) method.invoke(null, A02);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (InvocationTargetException e4) {
            if (e4.getCause() instanceof RuntimeException) {
                throw e4.getCause();
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e4);
        }
    }

    public final Class A04(Class cls) {
        AnonymousClass00N r5 = this.A00;
        String name = cls.getName();
        Class cls2 = (Class) r5.get(name);
        if (cls2 != null) {
            return cls2;
        }
        Class<?> cls3 = Class.forName(String.format("%s.%sParcelizer", cls.getPackage().getName(), cls.getSimpleName()), false, cls.getClassLoader());
        r5.put(name, cls3);
        return cls3;
    }

    public void A06(int i, int i2) {
        A05(i2);
        ((AnonymousClass0GE) this).A05.writeInt(i);
    }

    public void A07(Parcelable parcelable, int i) {
        A05(i);
        ((AnonymousClass0GE) this).A05.writeParcelable(parcelable, 0);
    }

    public void A08(AbstractC007203r r9) {
        if (r9 == null) {
            ((AnonymousClass0GE) this).A05.writeString(null);
            return;
        }
        try {
            Class<?> cls = r9.getClass();
            ((AnonymousClass0GE) this).A05.writeString(A04(cls).getName());
            AnonymousClass0QP A02 = A02();
            try {
                AnonymousClass00N r6 = this.A02;
                String name = cls.getName();
                Method method = (Method) r6.get(name);
                if (method == null) {
                    Class A04 = A04(cls);
                    System.currentTimeMillis();
                    method = A04.getDeclaredMethod("write", cls, AnonymousClass0QP.class);
                    r6.put(name, method);
                }
                method.invoke(null, r9, A02);
                AnonymousClass0GE r4 = (AnonymousClass0GE) A02;
                int i = r4.A00;
                if (i >= 0) {
                    int i2 = r4.A06.get(i);
                    Parcel parcel = r4.A05;
                    int dataPosition = parcel.dataPosition();
                    parcel.setDataPosition(i2);
                    parcel.writeInt(dataPosition - i2);
                    parcel.setDataPosition(dataPosition);
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e2);
            } catch (NoSuchMethodException e3) {
                throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
            } catch (InvocationTargetException e4) {
                if (e4.getCause() instanceof RuntimeException) {
                    throw e4.getCause();
                }
                throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e4);
            }
        } catch (ClassNotFoundException e5) {
            StringBuilder sb = new StringBuilder();
            sb.append(r9.getClass().getSimpleName());
            sb.append(" does not have a Parcelizer");
            throw new RuntimeException(sb.toString(), e5);
        }
    }
}
