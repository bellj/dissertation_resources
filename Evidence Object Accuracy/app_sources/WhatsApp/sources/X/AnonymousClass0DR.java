package X;

import android.os.Bundle;
import android.view.View;
import androidx.preference.Preference;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0DR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DR extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass0FG A00;

    public AnonymousClass0DR(AnonymousClass0FG r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        return this.A00.A00.A03(view, i, bundle);
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        Preference A0E;
        AnonymousClass0FG r1 = this.A00;
        r1.A00.A06(view, r5);
        RecyclerView recyclerView = r1.A02;
        int A00 = RecyclerView.A00(view);
        AnonymousClass02M r12 = recyclerView.A0N;
        if ((r12 instanceof AnonymousClass0F1) && (A0E = ((AnonymousClass0F1) r12).A0E(A00)) != null) {
            A0E.A0G(r5);
        }
    }
}
