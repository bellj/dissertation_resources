package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.text.TextUtils;
import java.io.IOException;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.2lD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56562lD extends AbstractC15040mS {
    public int A00;
    public String A01;
    public String A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;

    public C56562lD(C14160kx r1) {
        super(r1);
    }

    @Override // X.AbstractC15040mS
    public final void A0F() {
        int i;
        int i2;
        C14160kx r3 = ((C15050mT) this).A00;
        Context context = r3.A00;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                Bundle bundle = applicationInfo.metaData;
                if (bundle != null && (i = bundle.getInt("com.google.android.gms.analytics.globalConfigResource")) > 0) {
                    C79673qz r2 = new C79673qz(r3, new C108694zV(r3));
                    try {
                        XmlResourceParser xml = ((C15050mT) r2).A00.A01.getResources().getXml(i);
                        try {
                            xml.next();
                            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                                if (xml.getEventType() == 2) {
                                    String lowerCase = xml.getName().toLowerCase(Locale.US);
                                    if (lowerCase.equals("screenname")) {
                                        String attributeValue = xml.getAttributeValue(null, "name");
                                        String trim = xml.nextText().trim();
                                        if (!TextUtils.isEmpty(attributeValue)) {
                                            TextUtils.isEmpty(trim);
                                        }
                                    } else if (lowerCase.equals("string")) {
                                        String attributeValue2 = xml.getAttributeValue(null, "name");
                                        String trim2 = xml.nextText().trim();
                                        if (!TextUtils.isEmpty(attributeValue2) && trim2 != null) {
                                            C108694zV r1 = (C108694zV) r2.A00;
                                            if ("ga_appName".equals(attributeValue2)) {
                                                r1.A01.A02 = trim2;
                                            } else if ("ga_appVersion".equals(attributeValue2)) {
                                                r1.A01.A03 = trim2;
                                            } else if ("ga_logLevel".equals(attributeValue2)) {
                                                r1.A01.A04 = trim2;
                                            } else {
                                                C56582lF r12 = r1.A00.A0C;
                                                C14160kx.A01(r12);
                                                r12.A0E("String xml configuration name not recognized", attributeValue2);
                                            }
                                        }
                                    } else if (lowerCase.equals("bool")) {
                                        String attributeValue3 = xml.getAttributeValue(null, "name");
                                        String trim3 = xml.nextText().trim();
                                        if (!TextUtils.isEmpty(attributeValue3) && !TextUtils.isEmpty(trim3)) {
                                            try {
                                                boolean parseBoolean = Boolean.parseBoolean(trim3);
                                                C108694zV r13 = (C108694zV) r2.A00;
                                                if ("ga_dryRun".equals(attributeValue3)) {
                                                    r13.A01.A01 = parseBoolean ? 1 : 0;
                                                } else {
                                                    C56582lF r14 = r13.A00.A0C;
                                                    C14160kx.A01(r14);
                                                    r14.A0E("Bool xml configuration name not recognized", attributeValue3);
                                                }
                                            } catch (NumberFormatException e) {
                                                r2.A07(trim3, e, "Error parsing bool configuration value");
                                            }
                                        }
                                    } else if (lowerCase.equals("integer")) {
                                        String attributeValue4 = xml.getAttributeValue(null, "name");
                                        String trim4 = xml.nextText().trim();
                                        if (!TextUtils.isEmpty(attributeValue4) && !TextUtils.isEmpty(trim4)) {
                                            try {
                                                int parseInt = Integer.parseInt(trim4);
                                                C108694zV r15 = (C108694zV) r2.A00;
                                                if ("ga_dispatchPeriod".equals(attributeValue4)) {
                                                    r15.A01.A00 = parseInt;
                                                } else {
                                                    C56582lF r16 = r15.A00.A0C;
                                                    C14160kx.A01(r16);
                                                    r16.A0E("Int xml configuration name not recognized", attributeValue4);
                                                }
                                            } catch (NumberFormatException e2) {
                                                r2.A07(trim4, e2, "Error parsing int configuration value");
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (IOException | XmlPullParserException e3) {
                            r2.A0C("Error parsing tracker configuration file", e3);
                        }
                        AnonymousClass4SS r5 = ((C108694zV) r2.A00).A01;
                        if (r5 != null) {
                            A09("Loading global XML config values");
                            String str = r5.A02;
                            if (str != null) {
                                this.A02 = str;
                                A0B("XML config - app name", str);
                            }
                            String str2 = r5.A03;
                            if (str2 != null) {
                                this.A01 = str2;
                                A0B("XML config - app version", str2);
                            }
                            String str3 = r5.A04;
                            boolean z = false;
                            if (str3 != null) {
                                String lowerCase2 = str3.toLowerCase(Locale.US);
                                if ("verbose".equals(lowerCase2)) {
                                    i2 = 0;
                                } else if ("info".equals(lowerCase2)) {
                                    i2 = 1;
                                } else if ("warning".equals(lowerCase2)) {
                                    i2 = 2;
                                } else if ("error".equals(lowerCase2)) {
                                    i2 = 3;
                                }
                                A0D("XML config - log level", Integer.valueOf(i2));
                            }
                            int i3 = r5.A00;
                            if (i3 >= 0) {
                                this.A00 = i3;
                                this.A03 = true;
                                A0B("XML config - dispatch period (sec)", Integer.valueOf(i3));
                            }
                            int i4 = r5.A01;
                            if (i4 != -1) {
                                if (1 == i4) {
                                    z = true;
                                }
                                this.A05 = z;
                                this.A04 = true;
                                A0B("XML config - dry run", Boolean.valueOf(z));
                                return;
                            }
                            return;
                        }
                        return;
                    } catch (Resources.NotFoundException e4) {
                        r2.A0E("inflate() called with unknown resourceId", e4);
                        return;
                    }
                } else {
                    return;
                }
            }
        } catch (PackageManager.NameNotFoundException e5) {
            A0E("PackageManager doesn't know about the app package", e5);
        }
        A0A("Couldn't get ApplicationInfo to load global config");
    }
}
