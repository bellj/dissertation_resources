package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2U4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2U4 extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;

    public AnonymousClass2U4(AnonymousClass2L8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r10) {
        String str = null;
        String str2 = null;
        String str3 = null;
        for (AnonymousClass1V8 r7 : r10.A0J("config")) {
            String A0I = r7.A0I("platform", null);
            String A0I2 = r7.A0I("id", null);
            if ("gcm".equals(A0I)) {
                str = A0I2;
            } else if ("fbns".equals(A0I)) {
                str2 = A0I2;
            }
            AnonymousClass1V8 A0E = r7.A0E("item");
            if (A0E != null) {
                str3 = A0E.A0I("hash", null);
            }
        }
        C450720b r1 = this.A00.A0H;
        Log.i("xmpp/reader/read/client_config");
        AbstractC450820c r4 = r1.A00;
        Bundle bundle = new Bundle();
        bundle.putString("gcmToken", str);
        bundle.putString("fbnsToken", str2);
        bundle.putString("mutedChatsHash", str3);
        r4.AYY(Message.obtain(null, 0, 6, 0, bundle));
    }
}
