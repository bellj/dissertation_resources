package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3q9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79163q9 extends AbstractC113535Hy<Double> implements AnonymousClass5Z5<Double>, RandomAccess {
    public static final C79163q9 A02;
    public int A00;
    public double[] A01;

    public C79163q9(double[] dArr, int i) {
        this.A01 = dArr;
        this.A00 = i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C79163q9)) {
            return super.addAll(collection);
        }
        C79163q9 r7 = (C79163q9) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.A01;
            if (i3 > dArr.length) {
                dArr = Arrays.copyOf(dArr, i3);
                this.A01 = dArr;
            }
            System.arraycopy(r7.A01, 0, dArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C79163q9)) {
                return super.equals(obj);
            }
            C79163q9 r11 = (C79163q9) obj;
            int i = this.A00;
            if (i == r11.A00) {
                double[] dArr = r11.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == dArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean remove(Object obj) {
        A02();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Double.valueOf(this.A01[i]))) {
                double[] dArr = this.A01;
                System.arraycopy(dArr, i + 1, dArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            double[] dArr = this.A01;
            System.arraycopy(dArr, i2, dArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }

    static {
        C79163q9 r0 = new C79163q9(new double[10], 0);
        A02 = r0;
        ((AbstractC113535Hy) r0).A00 = false;
    }

    public final void A03(int i, double d) {
        int i2;
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        if (i2 < dArr.length) {
            C72463ee.A0T(dArr, i, i2);
        } else {
            double[] dArr2 = new double[((i2 * 3) >> 1) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.A01, i, dArr2, i + 1, this.A00 - i);
            this.A01 = dArr2;
        }
        this.A01[i] = d;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= this.A00) {
            return new C79163q9(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A03(i, C72453ed.A00(obj));
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return Double.valueOf(this.A01[i]);
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + C72453ed.A0B(Double.doubleToLongBits(this.A01[i2]));
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        double d = dArr[i];
        AbstractC113535Hy.A01(dArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        double A00 = C72453ed.A00(obj);
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        double d = dArr[i];
        dArr[i] = A00;
        return Double.valueOf(d);
    }
}
