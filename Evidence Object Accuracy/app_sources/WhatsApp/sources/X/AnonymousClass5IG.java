package X;

import android.net.Uri;
import java.util.HashMap;

/* renamed from: X.5IG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IG extends HashMap<String, Uri> {
    public final /* synthetic */ String val$anchor;
    public final /* synthetic */ Uri val$uri;

    public AnonymousClass5IG(String str, Uri uri) {
        this.val$anchor = str;
        this.val$uri = uri;
        put(str, uri);
    }
}
