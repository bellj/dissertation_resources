package X;

import android.content.Context;
import android.util.Log;
import android.view.View;
import com.facebook.rendercore.RenderTreeNode;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3JO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JO {
    public AnonymousClass3H1 A00;
    public boolean A01;
    public boolean A02;
    public final Context A03;
    public final AnonymousClass036 A04 = new AnonymousClass036();
    public final AbstractC52532bB A05;

    public AnonymousClass3JO(AbstractC52532bB r2) {
        this.A03 = r2.getContext();
        this.A05 = r2;
    }

    public static void A00(Context context, C91194Qu r5) {
        RenderTreeNode renderTreeNode = r5.A01;
        AbstractC65073Ia r0 = renderTreeNode.A07;
        Object obj = r5.A02;
        Object obj2 = renderTreeNode.A08;
        List list = r0.A00;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                A05(context, obj, obj2, it);
            }
        }
        r5.A03 = true;
    }

    public static void A01(Context context, C91194Qu r7) {
        RenderTreeNode renderTreeNode = r7.A01;
        AbstractC65073Ia r5 = renderTreeNode.A07;
        Object obj = r7.A02;
        Object obj2 = renderTreeNode.A08;
        List list = r5.A00;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                C93304Zx r0 = (C93304Zx) r5.A00.get(size);
                r0.A00.Af8(context, obj, r0.A01, obj2);
            }
        }
        r7.A03 = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f2, code lost:
        if (((android.view.View) r2).isLayoutRequested() == false) goto L_0x00f4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.content.Context r23, X.C91194Qu r24, com.facebook.rendercore.RenderTreeNode r25) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JO.A02(android.content.Context, X.4Qu, com.facebook.rendercore.RenderTreeNode):void");
    }

    public static void A03(Context context, RenderTreeNode renderTreeNode, AbstractC65073Ia r5, Object obj) {
        Object obj2 = renderTreeNode.A08;
        List list = r5.A01;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                A05(context, obj, obj2, it);
            }
        }
    }

    public static void A04(Context context, RenderTreeNode renderTreeNode, AbstractC65073Ia r6, Object obj) {
        Object obj2 = renderTreeNode.A08;
        List list = r6.A01;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    C93304Zx r0 = (C93304Zx) r6.A01.get(size);
                    r0.A00.Af8(context, obj, r0.A01, obj2);
                } else {
                    return;
                }
            }
        }
    }

    public static void A05(Context context, Object obj, Object obj2, Iterator it) {
        C93304Zx r0 = (C93304Zx) it.next();
        r0.A00.A6O(context, obj, r0.A01, obj2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
        if (r13 == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005d, code lost:
        if (r12.getBottom() == r1) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A06(com.facebook.rendercore.RenderTreeNode r11, java.lang.Object r12, boolean r13) {
        /*
            android.graphics.Rect r0 = r11.A04
            android.graphics.Rect r9 = r11.A05
            int r4 = r0.left
            int r3 = r0.top
            int r2 = r0.right
            int r1 = r0.bottom
            X.3Ez r0 = X.C94614cC.A00
            boolean r11 = r0.A02()
            if (r11 == 0) goto L_0x0019
            java.lang.String r0 = "applyBoundsToMountContent"
            X.C94614cC.A01(r0)
        L_0x0019:
            boolean r0 = r12 instanceof android.view.View     // Catch: all -> 0x008f
            if (r0 == 0) goto L_0x0063
            android.view.View r12 = (android.view.View) r12     // Catch: all -> 0x008f
            int r10 = r2 - r4
            int r8 = r1 - r3
            if (r9 == 0) goto L_0x0034
            boolean r0 = r12 instanceof X.AbstractC52532bB     // Catch: all -> 0x008f
            if (r0 != 0) goto L_0x0034
            int r7 = r9.left     // Catch: all -> 0x008f
            int r6 = r9.top     // Catch: all -> 0x008f
            int r5 = r9.right     // Catch: all -> 0x008f
            int r0 = r9.bottom     // Catch: all -> 0x008f
            r12.setPadding(r7, r6, r5, r0)     // Catch: all -> 0x008f
        L_0x0034:
            if (r13 != 0) goto L_0x0042
            int r0 = r12.getMeasuredHeight()     // Catch: all -> 0x008f
            if (r0 != r8) goto L_0x0042
            int r0 = r12.getMeasuredWidth()     // Catch: all -> 0x008f
            if (r0 == r10) goto L_0x0047
        L_0x0042:
            X.C12980iv.A1A(r12, r10, r8)     // Catch: all -> 0x008f
            if (r13 != 0) goto L_0x005f
        L_0x0047:
            int r0 = r12.getLeft()     // Catch: all -> 0x008f
            if (r0 != r4) goto L_0x005f
            int r0 = r12.getTop()     // Catch: all -> 0x008f
            if (r0 != r3) goto L_0x005f
            int r0 = r12.getRight()     // Catch: all -> 0x008f
            if (r0 != r2) goto L_0x005f
            int r0 = r12.getBottom()     // Catch: all -> 0x008f
            if (r0 == r1) goto L_0x007a
        L_0x005f:
            r12.layout(r4, r3, r2, r1)     // Catch: all -> 0x008f
            goto L_0x007a
        L_0x0063:
            boolean r0 = r12 instanceof android.graphics.drawable.Drawable     // Catch: all -> 0x008f
            if (r0 == 0) goto L_0x0080
            if (r9 == 0) goto L_0x0075
            int r0 = r9.left     // Catch: all -> 0x008f
            int r4 = r4 + r0
            int r0 = r9.top     // Catch: all -> 0x008f
            int r3 = r3 + r0
            int r0 = r9.right     // Catch: all -> 0x008f
            int r2 = r2 - r0
            int r0 = r9.bottom     // Catch: all -> 0x008f
            int r1 = r1 - r0
        L_0x0075:
            android.graphics.drawable.Drawable r12 = (android.graphics.drawable.Drawable) r12     // Catch: all -> 0x008f
            r12.setBounds(r4, r3, r2, r1)     // Catch: all -> 0x008f
        L_0x007a:
            if (r11 == 0) goto L_0x007f
            X.C94614cC.A00()
        L_0x007f:
            return
        L_0x0080:
            java.lang.StringBuilder r1 = X.C12960it.A0h()     // Catch: all -> 0x008f
            java.lang.String r0 = "Unsupported mounted content "
            java.lang.String r0 = X.C12960it.A0Z(r12, r0, r1)     // Catch: all -> 0x008f
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)     // Catch: all -> 0x008f
            throw r0     // Catch: all -> 0x008f
        L_0x008f:
            r0 = move-exception
            if (r11 == 0) goto L_0x0095
            X.C94614cC.A00()
        L_0x0095:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JO.A06(com.facebook.rendercore.RenderTreeNode, java.lang.Object, boolean):void");
    }

    public void A07() {
        if (this.A00 != null) {
            boolean A02 = C94614cC.A00.A02();
            if (A02) {
                C94614cC.A01("MountState.bind");
            }
            int length = this.A00.A04.length;
            for (int i = 0; i < length; i++) {
                C91194Qu r1 = (C91194Qu) this.A04.A04(this.A00.A04[i].A07.A02(), null);
                if (r1 != null && !r1.A03) {
                    Object obj = r1.A02;
                    A00(this.A03, r1);
                    if ((obj instanceof View) && !(obj instanceof AbstractC52532bB)) {
                        View view = (View) obj;
                        if (view.isLayoutRequested()) {
                            A06(r1.A01, view, true);
                        }
                    }
                }
            }
            if (A02) {
                C94614cC.A00();
            }
        }
    }

    public void A08() {
        if (this.A00 != null) {
            boolean A02 = C94614cC.A00.A02();
            if (A02) {
                C94614cC.A01("MountState.unbind");
                C94614cC.A01("MountState.unbindAllContent");
            }
            int length = this.A00.A04.length;
            for (int i = 0; i < length; i++) {
                C91194Qu r1 = (C91194Qu) this.A04.A04(this.A00.A04[i].A07.A02(), null);
                if (r1 != null && r1.A03) {
                    A01(this.A03, r1);
                }
            }
            if (A02) {
                C94614cC.A00();
                C94614cC.A01("MountState.unbindExtensions");
                C94614cC.A00();
                C94614cC.A00();
            }
        }
    }

    public void A09() {
        if (this.A00 != null) {
            boolean A02 = C94614cC.A00.A02();
            if (A02) {
                C94614cC.A01("MountState.unmountAllItems");
            }
            A0A(0);
            if (A02) {
                C94614cC.A00();
            }
            this.A02 = true;
        }
    }

    public final void A0A(long j) {
        String obj;
        AnonymousClass036 r9 = this.A04;
        C91194Qu r7 = (C91194Qu) r9.A04(j, null);
        if (r7 != null) {
            boolean A02 = C94614cC.A00.A02();
            RenderTreeNode renderTreeNode = r7.A01;
            AbstractC65073Ia r5 = renderTreeNode.A07;
            Object obj2 = r7.A02;
            if (A02) {
                C94614cC.A01(C12960it.A0d(r5.A03(), C12960it.A0k("UnmountItem: ")));
            }
            List list = renderTreeNode.A00;
            if (list != null && list.size() > 0) {
                List list2 = renderTreeNode.A00;
                if (list2 != null) {
                    for (int A09 = C12990iw.A09(list2, 1); A09 >= 0; A09--) {
                        A0A(((RenderTreeNode) renderTreeNode.A00.get(A09)).A07.A02());
                    }
                }
                if (((AbstractC52532bB) obj2).getMountItemCount() > 0) {
                    throw C12960it.A0U("Recursively unmounting items from a ComponentHost, left some items behind maybe because not tracked by its MountState");
                }
            }
            long A022 = r5.A02();
            if (A022 == 0) {
                C91194Qu r4 = (C91194Qu) r9.A04(0, null);
                if (r4 != null) {
                    if (r4.A03) {
                        A01(this.A03, r4);
                    }
                    r9.A07(0);
                    RenderTreeNode renderTreeNode2 = this.A00.A03;
                    A04(this.A03, renderTreeNode2, renderTreeNode2.A07, r4.A02);
                }
            } else {
                r9.A07(A022);
                AbstractC52532bB r2 = r7.A00;
                if (A02) {
                    C94614cC.A01(C12960it.A0d(r5.A03(), C12960it.A0k("UnmountItem:remove: ")));
                }
                r2.A02(r7);
                if (A02) {
                    C94614cC.A00();
                }
                if (r7.A03) {
                    if (A02) {
                        C94614cC.A01(C12960it.A0d(r5.A03(), C12960it.A0k("UnmountItem:unbind: ")));
                    }
                    A01(this.A03, r7);
                    if (A02) {
                        C94614cC.A00();
                    }
                }
                if (obj2 instanceof View) {
                    ((View) obj2).setPadding(0, 0, 0, 0);
                }
                if (A02) {
                    C94614cC.A01(C12960it.A0d(r5.A03(), C12960it.A0k("UnmountItem:unmount: ")));
                }
                Context context = this.A03;
                A04(context, renderTreeNode, r5, obj2);
                if (A02) {
                    C94614cC.A00();
                }
                C89834Lo A00 = AnonymousClass3IH.A00(context, (AnonymousClass5SC) r7.A01.A07);
                if (A00 != null) {
                    try {
                        A00.A00.Aa6(obj2);
                    } catch (IllegalStateException e) {
                        StringBuilder A0k = C12960it.A0k("Lifecycle: ");
                        Object obj3 = A00.A01;
                        if (obj3 instanceof Class) {
                            StringBuilder A0k2 = C12960it.A0k(" <cls>");
                            A0k2.append(((Class) obj3).getName());
                            obj = C12960it.A0d("</cls>", A0k2);
                        } else {
                            obj = obj3.toString();
                        }
                        throw new IllegalStateException(C12960it.A0d(obj, A0k), e);
                    }
                }
            }
            if (A02) {
                C94614cC.A00();
            }
        }
    }

    public void A0B(AnonymousClass3H1 r22) {
        int i;
        boolean z;
        RenderTreeNode renderTreeNode;
        if (r22 == null) {
            throw C12960it.A0U("Trying to mount a null RenderTreeNode");
        } else if (!this.A01) {
            AnonymousClass3H1 r5 = this.A00;
            if (r22 != r5 || this.A02) {
                this.A00 = r22;
                boolean A02 = C94614cC.A00.A02();
                if (A02) {
                    C94614cC.A01("MountState.mount");
                    C94614cC.A01("RenderCoreExtension.beforeMount");
                }
                this.A01 = true;
                if (A02) {
                    C94614cC.A00();
                    C94614cC.A01("MountState.prepareMount");
                }
                if (!(this.A00 == null || r5 == null)) {
                    boolean A022 = C94614cC.A00.A02();
                    if (A022) {
                        C94614cC.A01("unmountOrMoveOldItems");
                    }
                    int i2 = 1;
                    while (true) {
                        RenderTreeNode[] renderTreeNodeArr = r5.A04;
                        if (i2 >= renderTreeNodeArr.length) {
                            break;
                        }
                        AbstractC65073Ia r6 = renderTreeNodeArr[i2].A07;
                        int A05 = C12960it.A05(this.A00.A02.get(r6.A02(), -1));
                        Object obj = null;
                        if (A05 > -1) {
                            renderTreeNode = this.A00.A04[A05];
                        } else {
                            renderTreeNode = null;
                        }
                        AnonymousClass036 r2 = this.A04;
                        C91194Qu r62 = (C91194Qu) r2.A04(r6.A02(), null);
                        if (r62 != null) {
                            if (A05 != -1) {
                                C91194Qu r0 = (C91194Qu) r2.A04(renderTreeNode.A06.A07.A02(), null);
                                if (r0 != null) {
                                    obj = r0.A02;
                                }
                                AbstractC52532bB r23 = r62.A00;
                                if (r23 == obj) {
                                    int i3 = r62.A01.A03;
                                    int i4 = renderTreeNode.A03;
                                    if (i3 != i4) {
                                        r23.A04(r62, i3, i4);
                                    }
                                }
                            }
                            A0A(r62.A01.A07.A02());
                        }
                        i2++;
                    }
                    if (A022) {
                        C94614cC.A00();
                    }
                }
                AnonymousClass036 r63 = this.A04;
                C91194Qu r24 = (C91194Qu) r63.A04(0, null);
                RenderTreeNode renderTreeNode2 = this.A00.A04[0];
                Context context = this.A03;
                if (r24 == null) {
                    AbstractC65073Ia r1 = renderTreeNode2.A07;
                    AbstractC52532bB r02 = this.A05;
                    A03(context, renderTreeNode2, r1, r02);
                    C91194Qu r25 = new C91194Qu(r02, renderTreeNode2, r02);
                    r63.A09(0, r25);
                    A00(context, r25);
                } else {
                    A02(context, r24, renderTreeNode2);
                }
                if (A02) {
                    C94614cC.A00();
                }
                RenderTreeNode[] renderTreeNodeArr2 = r22.A04;
                int length = renderTreeNodeArr2.length;
                int i5 = 1;
                while (i5 < length) {
                    RenderTreeNode renderTreeNode3 = renderTreeNodeArr2[i5];
                    AbstractC65073Ia r12 = renderTreeNode3.A07;
                    long A023 = r12.A02();
                    C91194Qu r11 = (C91194Qu) r63.A04(A023, null);
                    if (r11 != null) {
                        AbstractC65073Ia r03 = r11.A01.A07;
                        if (r03.A02() != A023) {
                            C94604cB.A00();
                            StringBuilder A0k = C12960it.A0k("The current render unit id does not match the new one.  index: ");
                            A0k.append(i5);
                            A0k.append(" mountableOutputCounts: ");
                            A0k.append(length);
                            A0k.append(" currentRenderUnitId: ");
                            A0k.append(r03.A02());
                            A0k.append(" newRenderUnitId: ");
                            String A0w = C12970iu.A0w(A0k, A023);
                            if (AnonymousClass4FY.A00) {
                                Log.e(C12960it.A0d("MountState", C12960it.A0j("RenderCore:")), A0w, null);
                            }
                            z = true;
                        } else {
                            z = false;
                        }
                        Class<?> cls = r03.getClass();
                        Class<?> cls2 = r12.getClass();
                        if (!cls.equals(cls2)) {
                            C94604cB.A00();
                            StringBuilder A0k2 = C12960it.A0k("Trying to update a MountItem with different ContentType. index: ");
                            A0k2.append(i5);
                            A0k2.append(" currentRenderUnitId: ");
                            A0k2.append(r03.A02());
                            A0k2.append(" newRenderUnitId: ");
                            A0k2.append(A023);
                            A0k2.append(" currentRenderUnitContentType: ");
                            A0k2.append(cls);
                            String A0Z = C12960it.A0Z(cls2, " newRenderUnitContentType: ", A0k2);
                            if (AnonymousClass4FY.A00) {
                                Log.e(C12960it.A0d("MountState", C12960it.A0j("RenderCore:")), A0Z, null);
                            }
                        } else if (!z) {
                            A02(context, r11, renderTreeNode3);
                        }
                        int A00 = r63.A00();
                        long[] jArr = new long[A00];
                        int A002 = r63.A00();
                        for (int i6 = 0; i6 < A002; i6++) {
                            jArr[i6] = r63.A01(i6);
                        }
                        C91194Qu r9 = null;
                        for (int i7 = 0; i7 < A00; i7++) {
                            long j = jArr[i7];
                            C91194Qu r13 = (C91194Qu) r63.A04(j, null);
                            if (r13 != null) {
                                if (r13.A01.A07.A02() == 0) {
                                    r63.A07(j);
                                    r9 = r13;
                                } else {
                                    if (r13.A01.A07.A02() == j) {
                                        j = r13.A01.A07.A02();
                                    }
                                    A0A(j);
                                }
                            }
                        }
                        r63.A09(0, r9);
                        i = 1;
                        i5 = 1;
                        i5 += i;
                    } else {
                        A0C(renderTreeNode3);
                    }
                    i = 1;
                    i5 += i;
                }
                this.A02 = false;
                this.A01 = false;
                if (A02) {
                    C94614cC.A00();
                    C94614cC.A01("RenderCoreExtension.afterMount");
                    C94614cC.A00();
                }
            }
        } else {
            throw C12960it.A0U("Trying to mount while already mounting!");
        }
    }

    public final void A0C(RenderTreeNode renderTreeNode) {
        Object A8B;
        AbstractC65073Ia r6 = renderTreeNode.A07;
        long A02 = r6.A02();
        if (A02 == 0) {
            Context context = this.A03;
            AbstractC52532bB r0 = this.A05;
            A03(context, renderTreeNode, r6, r0);
            C91194Qu r1 = new C91194Qu(r0, renderTreeNode, r0);
            this.A04.A09(0, r1);
            A00(context, r1);
            return;
        }
        boolean A022 = C94614cC.A00.A02();
        if (A022) {
            C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem: ")));
            C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem:before ")));
        }
        RenderTreeNode renderTreeNode2 = renderTreeNode.A06;
        AbstractC65073Ia r8 = renderTreeNode2.A07;
        long A023 = r8.A02();
        AnonymousClass036 r5 = this.A04;
        if (r5.A04(A023, null) == null) {
            A0C(renderTreeNode2);
        }
        long A024 = r8.A02();
        Object obj = ((C91194Qu) r5.A04(A024, null)).A02;
        if (obj instanceof AbstractC52532bB) {
            AbstractC52532bB r7 = (AbstractC52532bB) obj;
            Context context2 = this.A03;
            AnonymousClass5SC r3 = (AnonymousClass5SC) r6;
            C89834Lo A00 = AnonymousClass3IH.A00(context2, r3);
            if (A00 == null || (A8B = A00.A00.A5a()) == null) {
                A8B = r3.A8B(context2);
            }
            if (A022) {
                C94614cC.A00();
                C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem:mount ")));
            }
            A03(context2, renderTreeNode, r6, A8B);
            C91194Qu r32 = new C91194Qu(r7, renderTreeNode, A8B);
            r5.A09(A02, r32);
            r7.A03(r32, renderTreeNode.A03);
            if (A022) {
                C94614cC.A00();
                C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem:bind ")));
            }
            A00(context2, r32);
            if (A022) {
                C94614cC.A00();
                C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem:applyBounds ")));
            }
            A06(renderTreeNode, r32.A02, true);
            if (A022) {
                C94614cC.A00();
                C94614cC.A01(C12960it.A0d(r6.A03(), C12960it.A0k("MountItem:after ")));
                C94614cC.A00();
                C94614cC.A00();
                return;
            }
            return;
        }
        StringBuilder A0k = C12960it.A0k("Trying to mount a RenderTreeNode, its parent should be a Host, but was '");
        A0k.append(C12980iv.A0r(obj));
        A0k.append("'.\nParent RenderUnit: id=");
        A0k.append(A024);
        A0k.append("; contentType='");
        A0k.append(r8.getClass());
        A0k.append("'.\nChild RenderUnit: id=");
        A0k.append(A02);
        A0k.append("; contentType='");
        A0k.append(r6.getClass());
        throw C12990iw.A0m(C12960it.A0d("'.", A0k));
    }
}
