package X;

import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.calling.callgrid.view.CallGrid;
import com.whatsapp.util.Log;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2Ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48042Ec extends AbstractC008704k {
    public static AccelerateDecelerateInterpolator A0E;
    public C89394Ju A00;
    public ArrayList A01 = new ArrayList();
    public ArrayList A02 = new ArrayList();
    public ArrayList A03 = new ArrayList();
    public ArrayList A04 = new ArrayList();
    public ArrayList A05 = new ArrayList();
    public ArrayList A06 = new ArrayList();
    public ArrayList A07 = new ArrayList();
    public ArrayList A08 = new ArrayList();
    public ArrayList A09 = new ArrayList();
    public ArrayList A0A = new ArrayList();
    public ArrayList A0B = new ArrayList();
    public boolean A0C = false;
    public boolean A0D = false;

    @Override // X.AnonymousClass04Y
    public long A04() {
        return 300;
    }

    @Override // X.AnonymousClass04Y
    public long A05() {
        return 200;
    }

    @Override // X.AnonymousClass04Y
    public long A06() {
        return 200;
    }

    @Override // X.AnonymousClass04Y
    public long A07() {
        return 300;
    }

    public static void A01(List list) {
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AnonymousClass03U) list.get(size)).A0H.animate().cancel();
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A08() {
        ArrayList arrayList = this.A09;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            AnonymousClass03U r1 = ((C48082Eg) arrayList.get(size)).A04;
            View view = r1.A0H;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            A03(r1);
            arrayList.remove(size);
        }
        ArrayList arrayList2 = this.A0A;
        int size2 = arrayList2.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            A03((AnonymousClass03U) arrayList2.get(size2));
            arrayList2.remove(size2);
        }
        ArrayList arrayList3 = this.A07;
        int size3 = arrayList3.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            A0I((AnonymousClass03U) arrayList3.get(size3));
            arrayList3.remove(size3);
        }
        ArrayList arrayList4 = this.A08;
        int size4 = arrayList4.size();
        while (true) {
            size4--;
            if (size4 < 0) {
                break;
            }
            C48052Ed r12 = (C48052Ed) arrayList4.get(size4);
            AnonymousClass03U r0 = r12.A05;
            if (r0 != null) {
                A0L(r0, r12);
            }
            AnonymousClass03U r02 = r12.A04;
            if (r02 != null) {
                A0L(r02, r12);
            }
        }
        arrayList4.clear();
        if (A0B()) {
            ArrayList arrayList5 = this.A06;
            int size5 = arrayList5.size();
            while (true) {
                size5--;
                if (size5 < 0) {
                    break;
                }
                AbstractList abstractList = (AbstractList) arrayList5.get(size5);
                int size6 = abstractList.size();
                while (true) {
                    size6--;
                    if (size6 >= 0) {
                        AnonymousClass03U r13 = ((C48082Eg) abstractList.get(size6)).A04;
                        View view2 = r13.A0H;
                        view2.setTranslationY(0.0f);
                        view2.setTranslationX(0.0f);
                        A03(r13);
                        abstractList.remove(size6);
                        if (abstractList.isEmpty()) {
                            arrayList5.remove(abstractList);
                        }
                    }
                }
            }
            ArrayList arrayList6 = this.A02;
            int size7 = arrayList6.size();
            while (true) {
                size7--;
                if (size7 < 0) {
                    break;
                }
                AbstractList abstractList2 = (AbstractList) arrayList6.get(size7);
                int size8 = abstractList2.size();
                while (true) {
                    size8--;
                    if (size8 >= 0) {
                        A0I((AnonymousClass03U) abstractList2.get(size8));
                        abstractList2.remove(size8);
                        if (abstractList2.isEmpty()) {
                            arrayList6.remove(abstractList2);
                        }
                    }
                }
            }
            ArrayList arrayList7 = this.A04;
            int size9 = arrayList7.size();
            while (true) {
                size9--;
                if (size9 >= 0) {
                    AbstractList abstractList3 = (AbstractList) arrayList7.get(size9);
                    int size10 = abstractList3.size();
                    while (true) {
                        size10--;
                        if (size10 >= 0) {
                            C48052Ed r14 = (C48052Ed) abstractList3.get(size10);
                            AnonymousClass03U r03 = r14.A05;
                            if (r03 != null) {
                                A0L(r03, r14);
                            }
                            AnonymousClass03U r04 = r14.A04;
                            if (r04 != null) {
                                A0L(r04, r14);
                            }
                            if (abstractList3.isEmpty()) {
                                arrayList7.remove(abstractList3);
                            }
                        }
                    }
                } else {
                    A01(this.A0B);
                    A01(this.A05);
                    A01(this.A01);
                    A01(this.A03);
                    A02();
                    return;
                }
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A09() {
        long j;
        long j2;
        ArrayList arrayList = this.A0A;
        boolean z = !arrayList.isEmpty();
        ArrayList arrayList2 = this.A09;
        boolean z2 = !arrayList2.isEmpty();
        ArrayList arrayList3 = this.A08;
        boolean z3 = !arrayList3.isEmpty();
        ArrayList arrayList4 = this.A07;
        boolean z4 = !arrayList4.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass03U r8 = (AnonymousClass03U) it.next();
                if (this.A0D) {
                    A03(r8);
                    A0H();
                } else {
                    View view = r8.A0H;
                    ViewPropertyAnimator animate = view.animate();
                    this.A0B.add(r8);
                    animate.setDuration(300).scaleX(0.0f).scaleY(0.0f).setListener(new AnonymousClass2YX(view, animate, r8, this)).start();
                }
            }
            arrayList.clear();
            if (z2) {
                ArrayList arrayList5 = new ArrayList();
                arrayList5.addAll(arrayList2);
                this.A06.add(arrayList5);
                arrayList2.clear();
                RunnableBRunnable0Shape1S0200000_I0_1 runnableBRunnable0Shape1S0200000_I0_1 = new RunnableBRunnable0Shape1S0200000_I0_1(this, 30, arrayList5);
                if (z) {
                    ((C48082Eg) arrayList5.get(0)).A04.A0H.postOnAnimationDelayed(runnableBRunnable0Shape1S0200000_I0_1, 300);
                } else {
                    runnableBRunnable0Shape1S0200000_I0_1.run();
                }
            }
            if (z3) {
                ArrayList arrayList6 = new ArrayList();
                arrayList6.addAll(arrayList3);
                this.A04.add(arrayList6);
                arrayList3.clear();
                RunnableBRunnable0Shape1S0200000_I0_1 runnableBRunnable0Shape1S0200000_I0_12 = new RunnableBRunnable0Shape1S0200000_I0_1(this, 31, arrayList6);
                if (z) {
                    ((C48052Ed) arrayList6.get(0)).A05.A0H.postOnAnimationDelayed(runnableBRunnable0Shape1S0200000_I0_12, 300);
                } else {
                    runnableBRunnable0Shape1S0200000_I0_12.run();
                }
            }
            if (z4) {
                ArrayList arrayList7 = new ArrayList();
                arrayList7.addAll(arrayList4);
                this.A02.add(arrayList7);
                arrayList4.clear();
                RunnableBRunnable0Shape1S0200000_I0_1 runnableBRunnable0Shape1S0200000_I0_13 = new RunnableBRunnable0Shape1S0200000_I0_1(this, 29, arrayList7);
                if (z || z2 || z3) {
                    long j3 = 0;
                    if (z) {
                        j = 300;
                    } else {
                        j = 0;
                    }
                    if (z2) {
                        j2 = 200;
                    } else {
                        j2 = 0;
                    }
                    if (z3) {
                        j3 = 200;
                    }
                    ((AnonymousClass03U) arrayList7.get(0)).A0H.postOnAnimationDelayed(runnableBRunnable0Shape1S0200000_I0_13, j + Math.max(j2, j3));
                    return;
                }
                runnableBRunnable0Shape1S0200000_I0_13.run();
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A0A(AnonymousClass03U r8) {
        View view = r8.A0H;
        view.animate().cancel();
        ArrayList arrayList = this.A09;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (((C48082Eg) arrayList.get(size)).A04 == r8) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                A03(r8);
                arrayList.remove(size);
            }
        }
        A0K(r8, this.A08);
        if (this.A0A.remove(r8)) {
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            A03(r8);
        }
        if (this.A07.remove(r8)) {
            A0I(r8);
        }
        ArrayList arrayList2 = this.A04;
        int size2 = arrayList2.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            ArrayList arrayList3 = (ArrayList) arrayList2.get(size2);
            A0K(r8, arrayList3);
            if (arrayList3.isEmpty()) {
                arrayList2.remove(size2);
            }
        }
        ArrayList arrayList4 = this.A06;
        int size3 = arrayList4.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            AbstractList abstractList = (AbstractList) arrayList4.get(size3);
            int size4 = abstractList.size();
            while (true) {
                size4--;
                if (size4 < 0) {
                    break;
                } else if (((C48082Eg) abstractList.get(size4)).A04 == r8) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    A03(r8);
                    abstractList.remove(size4);
                    if (abstractList.isEmpty()) {
                        arrayList4.remove(size3);
                    }
                }
            }
        }
        ArrayList arrayList5 = this.A02;
        int size5 = arrayList5.size();
        while (true) {
            size5--;
            if (size5 >= 0) {
                AbstractCollection abstractCollection = (AbstractCollection) arrayList5.get(size5);
                if (abstractCollection.remove(r8)) {
                    A0I(r8);
                    if (abstractCollection.isEmpty()) {
                        arrayList5.remove(size5);
                    }
                }
            } else {
                this.A0B.remove(r8);
                this.A01.remove(r8);
                this.A03.remove(r8);
                this.A05.remove(r8);
                A0H();
                return;
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public boolean A0B() {
        return !this.A07.isEmpty() || !this.A08.isEmpty() || !this.A09.isEmpty() || !this.A0A.isEmpty() || !this.A05.isEmpty() || !this.A0B.isEmpty() || !this.A01.isEmpty() || !this.A03.isEmpty() || !this.A06.isEmpty() || !this.A02.isEmpty() || !this.A04.isEmpty();
    }

    @Override // X.AnonymousClass04Y
    public boolean A0C(AnonymousClass03U r3, List list) {
        return !list.isEmpty() || super.A0C(r3, list);
    }

    @Override // X.AbstractC008704k
    public boolean A0D(AnonymousClass03U r3) {
        A0J(r3);
        if (!this.A0D) {
            View view = r3.A0H;
            view.setScaleX(0.0f);
            view.setScaleY(0.0f);
        } else if (r3 instanceof AbstractC55202hx) {
            ((AbstractC55202hx) r3).A0B(4);
        }
        this.A07.add(r3);
        return true;
    }

    @Override // X.AbstractC008704k
    public boolean A0E(AnonymousClass03U r2) {
        A0J(r2);
        this.A0A.add(r2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0028, code lost:
        if (r1 != 0) goto L_0x002a;
     */
    @Override // X.AbstractC008704k
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0F(X.AnonymousClass03U r9, int r10, int r11, int r12, int r13) {
        /*
            r8 = this;
            r3 = r9
            android.view.View r2 = r9.A0H
            float r0 = r2.getTranslationX()
            int r0 = (int) r0
            int r4 = r10 + r0
            float r0 = r2.getTranslationY()
            int r0 = (int) r0
            int r5 = r11 + r0
            r8.A0J(r9)
            r6 = r12
            int r0 = r12 - r4
            r7 = r13
            int r1 = r13 - r5
            if (r0 != 0) goto L_0x0023
            if (r1 != 0) goto L_0x002a
            r8.A03(r9)
            r0 = 0
            return r0
        L_0x0023:
            int r0 = -r0
            float r0 = (float) r0
            r2.setTranslationX(r0)
            if (r1 == 0) goto L_0x002f
        L_0x002a:
            int r0 = -r1
            float r0 = (float) r0
            r2.setTranslationY(r0)
        L_0x002f:
            java.util.ArrayList r0 = r8.A09
            X.2Eg r2 = new X.2Eg
            r2.<init>(r3, r4, r5, r6, r7)
            r0.add(r2)
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48042Ec.A0F(X.03U, int, int, int, int):boolean");
    }

    @Override // X.AbstractC008704k
    public boolean A0G(AnonymousClass03U r14, AnonymousClass03U r15, int i, int i2, int i3, int i4) {
        if (r14 == r15) {
            return A0F(r14, i, i2, i3, i4);
        }
        View view = r14.A0H;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        float alpha = view.getAlpha();
        A0J(r14);
        view.setTranslationX(translationX);
        view.setTranslationY(translationY);
        view.setAlpha(alpha);
        A0J(r15);
        View view2 = r15.A0H;
        view2.setTranslationX((float) (-((int) (((float) (i3 - i)) - translationX))));
        view2.setTranslationY((float) (-((int) (((float) (i4 - i2)) - translationY))));
        view2.setAlpha(0.0f);
        this.A08.add(new C48052Ed(r14, r15, i, i2, i3, i4));
        return true;
    }

    public void A0H() {
        C89394Ju r0;
        if (!A0B()) {
            A02();
            if (this.A0C && (r0 = this.A00) != null) {
                CallGrid callGrid = r0.A00;
                if (callGrid.A0N.A04) {
                    Log.i("CallGrid/resizeGridView, callGridAdapter.notifyDataSetChanged()");
                    RecyclerView recyclerView = callGrid.A0J;
                    if (recyclerView.A09 > 0 || recyclerView.A0B != 0) {
                        recyclerView.post(new RunnableBRunnable0Shape14S0100000_I1(callGrid, 42));
                    } else {
                        callGrid.A03.A02();
                    }
                }
            }
            this.A0C = false;
        }
    }

    public final void A0I(AnonymousClass03U r3) {
        if (r3 instanceof AbstractC55202hx) {
            ((AbstractC55202hx) r3).A0B(0);
        }
        View view = r3.A0H;
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        A03(r3);
    }

    public final void A0J(AnonymousClass03U r3) {
        if (A0E == null) {
            A0E = new AccelerateDecelerateInterpolator();
        }
        r3.A0H.animate().setInterpolator(A0E);
        A0A(r3);
    }

    public final void A0K(AnonymousClass03U r4, List list) {
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                C48052Ed r1 = (C48052Ed) list.get(size);
                if (A0L(r4, r1) && r1.A05 == null && r1.A04 == null) {
                    list.remove(r1);
                }
            } else {
                return;
            }
        }
    }

    public final boolean A0L(AnonymousClass03U r5, C48052Ed r6) {
        if (r6.A04 == r5) {
            r6.A04 = null;
        } else if (r6.A05 != r5) {
            return false;
        } else {
            r6.A05 = null;
        }
        View view = r5.A0H;
        view.setAlpha(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        A03(r5);
        return true;
    }
}
