package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.5BJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5BJ implements FilenameFilter {
    @Override // java.io.FilenameFilter
    public boolean accept(File file, String str) {
        if (!str.startsWith("override-")) {
            return str.endsWith(".log") || str.endsWith(".zip") || str.endsWith(".tmp");
        }
        return false;
    }
}
