package X;

import android.view.View;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;

/* renamed from: X.3lT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnClickListenerC76333lT extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public final /* synthetic */ int A00;

    public View$OnClickListenerC76333lT(int i) {
        this.A00 = i;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        view.performAccessibilityAction(this.A00, null);
    }
}
