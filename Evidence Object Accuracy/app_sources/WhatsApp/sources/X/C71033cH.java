package X;

import com.whatsapp.voipcalling.CallLinkInfo;
import java.util.List;

/* renamed from: X.3cH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71033cH implements AbstractC115475Rr {
    public final AnonymousClass1V8 A00;
    public final C64083Ee A01;
    public final String A02;
    public final List A03;

    public C71033cH(AbstractC15710nm r21, AnonymousClass1V8 r22) {
        AnonymousClass1V8.A01(r22, "state");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r22, String.class, A0j, A0k, "choice", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A04(null, r22, String.class, A0j, A0k, null, new String[]{CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID}, false);
        this.A01 = (C64083Ee) AnonymousClass3JT.A02(r21, r22, 1);
        this.A03 = AnonymousClass3JT.A0A(r21, r22, new String[]{"choice"}, 2);
        this.A00 = r22;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C71033cH.class != obj.getClass()) {
                return false;
            }
            C71033cH r5 = (C71033cH) obj;
            if (!this.A02.equals(r5.A02) || !this.A03.equals(r5.A03) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = this.A02;
        objArr[1] = this.A03;
        return C12980iv.A0B(this.A01, objArr, 2);
    }
}
