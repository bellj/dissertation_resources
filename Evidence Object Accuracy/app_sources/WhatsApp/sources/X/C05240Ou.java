package X;

/* renamed from: X.0Ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05240Ou {
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final EnumC03730Ix A07;
    public final String A08;
    public final String A09;
    public final boolean A0A;

    public C05240Ou(EnumC03730Ix r1, String str, String str2, float f, float f2, float f3, float f4, int i, int i2, int i3, boolean z) {
        this.A09 = str;
        this.A08 = str2;
        this.A02 = f;
        this.A07 = r1;
        this.A06 = i;
        this.A01 = f2;
        this.A00 = f3;
        this.A04 = i2;
        this.A05 = i3;
        this.A03 = f4;
        this.A0A = z;
    }

    public int hashCode() {
        int hashCode = (((((int) (((float) (((this.A09.hashCode() * 31) + this.A08.hashCode()) * 31)) + this.A02)) * 31) + this.A07.ordinal()) * 31) + this.A06;
        long floatToRawIntBits = (long) Float.floatToRawIntBits(this.A01);
        return (((hashCode * 31) + ((int) (floatToRawIntBits ^ (floatToRawIntBits >>> 32)))) * 31) + this.A04;
    }
}
