package X;

import android.app.job.JobParameters;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.os.IBinder;

/* renamed from: X.09b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class job.JobServiceEngineC019209b extends JobServiceEngine implements AbstractC12320hj {
    public JobParameters A00;
    public final AbstractServiceC003801r A01;
    public final Object A02 = new Object();

    public job.JobServiceEngineC019209b(AbstractServiceC003801r r2) {
        super(r2);
        this.A01 = r2;
    }

    @Override // X.AbstractC12320hj
    public IBinder A7S() {
        return getBinder();
    }

    @Override // X.AbstractC12320hj
    public AbstractC12330hk A8p() {
        synchronized (this.A02) {
            JobParameters jobParameters = this.A00;
            if (jobParameters == null) {
                return null;
            }
            JobWorkItem dequeueWork = jobParameters.dequeueWork();
            if (dequeueWork == null) {
                return null;
            }
            dequeueWork.getIntent().setExtrasClassLoader(this.A01.getClassLoader());
            return new C07290Xk(dequeueWork, this);
        }
    }

    @Override // android.app.job.JobServiceEngine
    public boolean onStartJob(JobParameters jobParameters) {
        this.A00 = jobParameters;
        this.A01.A03(false);
        return true;
    }

    @Override // android.app.job.JobServiceEngine
    public boolean onStopJob(JobParameters jobParameters) {
        AbstractServiceC003801r r2 = this.A01;
        AnonymousClass0AK r1 = r2.A00;
        if (r1 != null) {
            r1.cancel(false);
        }
        boolean A04 = r2.A04();
        synchronized (this.A02) {
            this.A00 = null;
        }
        return A04;
    }
}
