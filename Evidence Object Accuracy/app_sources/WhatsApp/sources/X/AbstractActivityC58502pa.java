package X;

import com.whatsapp.qrcode.contactqr.QrSheetDeepLinkActivity;

/* renamed from: X.2pa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58502pa extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58502pa() {
        ActivityC13830kP.A1P(this, 100);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            QrSheetDeepLinkActivity qrSheetDeepLinkActivity = (QrSheetDeepLinkActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, qrSheetDeepLinkActivity);
            ActivityC13810kN.A10(A1M, qrSheetDeepLinkActivity);
            ((ActivityC13790kL) qrSheetDeepLinkActivity).A08 = ActivityC13790kL.A0S(r3, A1M, qrSheetDeepLinkActivity, ActivityC13790kL.A0Y(A1M, qrSheetDeepLinkActivity));
            qrSheetDeepLinkActivity.A0E = C12970iu.A0b(A1M);
            qrSheetDeepLinkActivity.A00 = (AnonymousClass17S) A1M.A0F.get();
            qrSheetDeepLinkActivity.A02 = C12990iw.A0T(A1M);
            qrSheetDeepLinkActivity.A0F = C12990iw.A0c(A1M);
            qrSheetDeepLinkActivity.A05 = C12960it.A0O(A1M);
            qrSheetDeepLinkActivity.A08 = C12960it.A0P(A1M);
            qrSheetDeepLinkActivity.A01 = (C22260yn) A1M.A5I.get();
            qrSheetDeepLinkActivity.A0I = (C17070qD) A1M.AFC.get();
            qrSheetDeepLinkActivity.A09 = (C253318z) A1M.A4B.get();
            qrSheetDeepLinkActivity.A07 = C12980iv.A0a(A1M);
            qrSheetDeepLinkActivity.A0C = (C15680nj) A1M.A4e.get();
            qrSheetDeepLinkActivity.A0H = (C22710zW) A1M.AF7.get();
            qrSheetDeepLinkActivity.A0G = (C22410z2) A1M.ANH.get();
            qrSheetDeepLinkActivity.A04 = C12970iu.A0V(A1M);
            qrSheetDeepLinkActivity.A06 = (C26311Cv) A1M.AAQ.get();
            qrSheetDeepLinkActivity.A0B = (C22610zM) A1M.A6b.get();
            qrSheetDeepLinkActivity.A0A = C12990iw.A0a(A1M);
            qrSheetDeepLinkActivity.A03 = (C250918b) A1M.A2B.get();
            qrSheetDeepLinkActivity.A0D = (C25951Bl) A1M.A2O.get();
        }
    }
}
