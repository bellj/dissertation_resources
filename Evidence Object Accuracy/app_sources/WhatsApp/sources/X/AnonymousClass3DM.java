package X;

import android.net.Uri;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.3DM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DM {
    public final C19950uw A00;
    public final C19940uv A01;
    public final AnonymousClass14Q A02;
    public final C37741mv A03;
    public final C91454Ru A04 = new C91454Ru();
    public final C39861qf A05 = new C39861qf();
    public final C455221z A06;
    public final String A07;

    public AnonymousClass3DM(C19950uw r2, C20110vE r3, C19940uv r4, AnonymousClass14Q r5, C37741mv r6, C455221z r7, String str) {
        this.A02 = r5;
        this.A01 = r4;
        this.A00 = r2;
        this.A03 = r6;
        this.A07 = str;
        this.A06 = r7;
        r3.A01.A01();
    }

    public AnonymousClass3FM A00() {
        int i;
        C39861qf r2 = this.A05;
        SystemClock.elapsedRealtime();
        AnonymousClass3FM r1 = (AnonymousClass3FM) this.A03.A00(new AnonymousClass221() { // from class: X.3Y2
            @Override // X.AnonymousClass221
            public final C95514dr Aav(C28481Nj r10) {
                AnonymousClass4AV r0;
                AnonymousClass3DM r3 = AnonymousClass3DM.this;
                Uri.Builder A02 = r3.A06.A02(r10);
                A02.appendQueryParameter("resume", "1");
                String obj = A02.build().toString();
                C19940uv r4 = r3.A01;
                C68953Xj r5 = new C68953Xj(r3.A00, r4, r3.A04, obj);
                r5.A00 = new AnonymousClass3FM();
                C19950uw r12 = r5.A01;
                String str = r5.A04;
                C28471Ni A00 = r12.A00(r5, str, 10);
                try {
                    int A022 = A00.A02(r10);
                    C91454Ru r8 = r5.A03;
                    r8.A00 = A00.A00;
                    r8.A02 = A00.A01;
                    r8.A01 = (long) A022;
                    r8.A03 = A00.A02;
                    if (A022 < 0 || A022 >= 400) {
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("mediaupload/MMS upload resume form post failed/error=");
                        A0h.append(A022);
                        A0h.append("; url=");
                        Log.w(C12960it.A0d(str, A0h));
                        AnonymousClass3FM r13 = r5.A00;
                        r13.A00 = A022;
                        r13.A02 = AnonymousClass4AV.FAILURE;
                    }
                } catch (IOException e) {
                    Log.w(C12960it.A0d(str, C12960it.A0k("mediaupload/MMS upload resume form post failed; url=")), e);
                    boolean A023 = r5.A02.A02(e);
                    AnonymousClass3FM r14 = r5.A00;
                    if (A023) {
                        r0 = AnonymousClass4AV.WATLS_ERROR;
                    } else {
                        r0 = AnonymousClass4AV.FAILURE;
                    }
                    r14.A02 = r0;
                    C91454Ru r22 = r5.A03;
                    r22.A00 = A00.A00;
                    r22.A02 = A00.A01;
                    r22.A03 = A00.A02;
                }
                AnonymousClass3FM r23 = r5.A00;
                r23.A03 = r3.A02.A00(r23.A03);
                AnonymousClass4AV r15 = r23.A02;
                if (r15 == null) {
                    r15 = AnonymousClass4AV.FAILURE;
                    r23.A02 = r15;
                }
                if (r15 == AnonymousClass4AV.WATLS_ERROR) {
                    Log.i("resumecheck/attempting fallback MMS upload form post - watls error");
                    r4.A00();
                } else if (r15 == AnonymousClass4AV.FAILURE) {
                    Log.i("resumecheck/attempting fallback MMS upload form post");
                } else {
                    if (r15 == AnonymousClass4AV.RESUME) {
                        r3.A05.A02 = C12980iv.A0l(r23.A01);
                    }
                    return C95514dr.A02(r23);
                }
                return C95514dr.A03(r23, r23.A00);
            }
        });
        if (r1 == null || r1.A02 == null) {
            String A0d = C12960it.A0d(this.A07, C12960it.A0k("resumecheck/failed; no routes; hash="));
            r1 = new AnonymousClass3FM();
            r1.A02 = AnonymousClass4AV.FAILURE;
            r1.A04 = A0d;
        }
        AnonymousClass4AV r0 = r1.A02;
        AnonymousClass009.A05(r0);
        SystemClock.elapsedRealtime();
        switch (r0.ordinal()) {
            case 0:
                i = 1;
                break;
            case 1:
                i = 3;
                break;
            default:
                i = 2;
                break;
        }
        r2.A01 = Integer.valueOf(i);
        C91454Ru r02 = this.A04;
        r2.A00 = new C39851qe(r02.A03, r02.A00, r02.A02, r02.A01);
        return r1;
    }
}
