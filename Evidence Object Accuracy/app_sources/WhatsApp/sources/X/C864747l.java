package X;

import android.os.SystemClock;
import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0100100_I1;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.47l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864747l extends AbstractC16350or {
    public final byte A00;
    public final AnonymousClass02N A01;
    public final AbstractC15710nm A02;
    public final C002701f A03;
    public final C15650ng A04;
    public final C15660nh A05;
    public final C16120oU A06;
    public final File A07;
    public final WeakReference A08 = C12970iu.A10(null);

    public C864747l(AbstractC15710nm r8, C002701f r9, C15650ng r10, C15660nh r11, C16120oU r12, AbstractC14440lR r13, File file, byte b) {
        this.A02 = r8;
        this.A06 = r12;
        this.A00 = b;
        this.A04 = r10;
        this.A05 = r11;
        this.A03 = r9;
        this.A07 = file;
        ExecutorC41461tZ r5 = new ExecutorC41461tZ(r13);
        AnonymousClass02N r2 = new AnonymousClass02N();
        r5.execute(new RunnableBRunnable0Shape1S0100100_I1(r2, 20000, 3));
        this.A01 = r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AnonymousClass4XU r7;
        File[] listFiles;
        AnonymousClass4XU r9 = new AnonymousClass4XU();
        byte b = this.A00;
        r9.A00 = b;
        C91944Tv r8 = new C91944Tv(this.A01, this.A02, this.A03, this.A04, this.A05, r9, this.A07, b);
        try {
            try {
                r7 = r8.A06;
                r7.A0C = Long.valueOf(SystemClock.uptimeMillis());
                listFiles = r8.A07.listFiles();
            } catch (AnonymousClass04U e) {
                Log.e("mediafilemerger/cancelled", e);
            }
            if (listFiles == null) {
                r7.A09 = Long.valueOf(SystemClock.uptimeMillis());
                return new C90344Nn(r9, r8.A08);
            }
            int length = listFiles.length;
            ArrayList A0w = C12980iv.A0w(length);
            for (File file : listFiles) {
                if (file.isFile()) {
                    A0w.add(new C90314Nk(file));
                }
            }
            Collections.sort(A0w, new IDxComparatorShape3S0000000_2_I1(18));
            r7.A07 += (long) A0w.size();
            r7.A0B = Long.valueOf(SystemClock.uptimeMillis());
            r7.A0A = Long.valueOf(SystemClock.uptimeMillis());
            AnonymousClass036 r4 = new AnonymousClass036();
            Iterator it = A0w.iterator();
            while (it.hasNext()) {
                C90314Nk r3 = (C90314Nk) it.next();
                long j = r3.A00;
                C90324Nl r2 = (C90324Nl) r4.A04(j, null);
                if (r2 == null) {
                    r2 = new C90324Nl(j);
                    r4.A09(j, r2);
                }
                File file2 = r3.A01;
                List list = r2.A01;
                boolean z = true;
                if (!list.isEmpty() && file2.getName().compareTo(((File) list.get(list.size() - 1)).getName()) < 0) {
                    z = false;
                }
                AnonymousClass009.A0F(z);
                list.add(file2);
            }
            AnonymousClass02N r0 = r8.A01;
            r0.A02();
            ArrayList A0l = C12960it.A0l();
            for (int i = 0; i < r4.A00(); i++) {
                C90324Nl r22 = (C90324Nl) r4.A03(i);
                if (r22.A01.size() >= 2) {
                    A0l.add(r22);
                    r7.A08 += (long) r22.A01.size();
                }
            }
            r0.A02();
            Collections.sort(A0l, new IDxComparatorShape3S0000000_2_I1(19));
            r0.A02();
            Iterator it2 = A0l.iterator();
            while (it2.hasNext()) {
                C90324Nl r6 = (C90324Nl) it2.next();
                HashMap A11 = C12970iu.A11();
                List list2 = r6.A01;
                int size = list2.size();
                while (true) {
                    size--;
                    if (size >= 0) {
                        File file3 = (File) list2.get(size);
                        try {
                            r0.A02();
                            String A00 = C38531oF.A00(file3);
                            long j2 = r6.A00;
                            r7.A04++;
                            r7.A01 += j2;
                            File file4 = (File) A11.get(A00);
                            if (file4 == null) {
                                A11.put(A00, file3);
                            } else {
                                file3.getParentFile();
                                file3.getName();
                                file4.getName();
                                Collection<AbstractC16130oV> A0C = r8.A05.A0C(r0, file3, A00);
                                r0.A02();
                                if (!A0C.isEmpty()) {
                                    List list3 = r8.A08;
                                    C15650ng r1 = r8.A04;
                                    C002701f r13 = r8.A03;
                                    byte b2 = r8.A00;
                                    ArrayList A0w2 = C12980iv.A0w(A0C.size());
                                    r13.A07(file4, A0C.size(), true);
                                    for (AbstractC16130oV r15 : A0C) {
                                        C16150oX r02 = r15.A02;
                                        AnonymousClass009.A05(r02);
                                        r02.A0F = file4;
                                        r1.A0W(r15);
                                        A0w2.add(new C90334Nm(file3, file4));
                                    }
                                    r13.A00(file3, b2, A0C.size(), false);
                                    A0C.size();
                                    list3.addAll(A0w2);
                                    long length2 = file4.length();
                                    r7.A06++;
                                    r7.A03 += length2;
                                } else {
                                    r8.A03.A05(file3, r8.A00);
                                    file3.delete();
                                    long length3 = file4.length();
                                    r7.A05++;
                                    r7.A02 += length3;
                                }
                            }
                        } catch (IOException e2) {
                            Log.e("mediafilemerger/processfileswithsamelength", e2);
                        }
                    }
                }
            }
            return new C90344Nn(r9, r8.A08);
        } finally {
            r8.A06.A09 = Long.valueOf(SystemClock.uptimeMillis());
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C90344Nn r3 = (C90344Nn) obj;
        AbstractC14590lg r0 = (AbstractC14590lg) this.A08.get();
        if (r0 != null) {
            r0.accept(r3);
        }
        this.A06.A07(r3.A00.A01());
    }
}
