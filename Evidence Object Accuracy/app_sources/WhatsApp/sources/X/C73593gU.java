package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.whatsapp.qrcode.QrScannerViewV2;

/* renamed from: X.3gU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73593gU extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ QrScannerViewV2 A00;

    public C73593gU(QrScannerViewV2 qrScannerViewV2) {
        this.A00 = qrScannerViewV2;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        this.A00.A01.AA4(motionEvent.getX(), motionEvent.getY());
        return true;
    }
}
