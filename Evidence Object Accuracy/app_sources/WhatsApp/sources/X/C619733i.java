package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;

/* renamed from: X.33i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619733i extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass3DE A00;
    public final /* synthetic */ C18610sj A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C619733i(Context context, C14900mE r2, C18650sn r3, AnonymousClass3DE r4, C18610sj r5) {
        super(context, r2, r3);
        this.A01 = r5;
        this.A00 = r4;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A00.A00();
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        this.A00.A00();
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r15) {
        AnonymousClass1V8 A0E = r15.A0E("account");
        if (A0E == null || !"1".equals(A0E.A0I("cancel-status", null))) {
            this.A00.A00();
            return;
        }
        AnonymousClass3DE r1 = this.A00;
        AnonymousClass18P r0 = r1.A04;
        AbstractC14440lR r2 = r0.A0D;
        C14830m7 r4 = r0.A01;
        C21380xK r6 = r0.A05;
        C20320vZ r11 = r0.A0C;
        C17070qD r10 = r0.A0B;
        C12990iw.A1N(new AnonymousClass38C(r4, r0.A03, r6, r1.A01, r1.A02, r0.A0A, r10, r11, new RunnableBRunnable0Shape16S0100000_I1_2(r1.A03, 0), 18), r2);
    }
}
