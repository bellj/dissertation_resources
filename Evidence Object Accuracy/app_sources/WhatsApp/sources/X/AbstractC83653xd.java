package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.InfoCard;

/* renamed from: X.3xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC83653xd extends InfoCard {
    public AbstractC83653xd(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }
}
