package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78043oH extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98624iz();
    public final boolean A00;
    public final byte[] A01;

    public C78043oH(byte[] bArr, boolean z) {
        this.A01 = bArr;
        this.A00 = z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0G(parcel, this.A01, 1, false);
        C95654e8.A09(parcel, 2, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
