package X;

import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;

/* renamed from: X.3Fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64453Fp {
    public final /* synthetic */ MediaComposerFragment A00;

    public C64453Fp(MediaComposerFragment mediaComposerFragment) {
        this.A00 = mediaComposerFragment;
    }

    public void A00() {
        if (this instanceof AnonymousClass334) {
            ImageComposerFragment imageComposerFragment = ((AnonymousClass334) this).A00;
            AnonymousClass3MP r0 = imageComposerFragment.A06.A05;
            if (r0.A00 <= r0.A03) {
                imageComposerFragment.A1K(true, false);
            }
        }
    }

    public void A01() {
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) ((AnonymousClass21Y) this.A00.A0B());
        AnonymousClass3YE r2 = mediaComposerActivity.A0e;
        boolean A08 = mediaComposerActivity.A0b.A08();
        mediaComposerActivity.A0b.A07();
        r2.A02(A08);
    }

    public void A02() {
        ((AnonymousClass21Y) this.A00.A0B()).APM();
    }

    public void A03() {
        AnonymousClass21Y r1;
        boolean z;
        if (!(this instanceof AnonymousClass334)) {
            MediaComposerFragment mediaComposerFragment = this.A00;
            ActivityC000900k A0B = mediaComposerFragment.A0B();
            if (A0B != null && !A0B.isFinishing()) {
                r1 = (AnonymousClass21Y) mediaComposerFragment.A0B();
                z = false;
            } else {
                return;
            }
        } else {
            ImageComposerFragment imageComposerFragment = ((AnonymousClass334) this).A00;
            AnonymousClass21U r0 = imageComposerFragment.A07;
            boolean A08 = r0.A08();
            boolean A09 = r0.A09();
            if (!A08 || A09) {
                imageComposerFragment.A1K(false, !A09);
            }
            r1 = (AnonymousClass21Y) imageComposerFragment.A0B();
            z = imageComposerFragment.A07.A0H;
        }
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) r1;
        if (!mediaComposerActivity.isFinishing()) {
            mediaComposerActivity.A0y = z;
        }
    }
}
