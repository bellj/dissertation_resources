package X;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/* renamed from: X.0Tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06500Tw {
    public static File A00(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null) {
            StringBuilder sb = new StringBuilder(".font");
            sb.append(Process.myPid());
            sb.append("-");
            sb.append(Process.myTid());
            sb.append("-");
            String obj = sb.toString();
            for (int i = 0; i < 100; i++) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(obj);
                sb2.append(i);
                File file = new File(cacheDir, sb2.toString());
                if (file.createNewFile()) {
                    return file;
                }
            }
        }
        return null;
    }

    public static ByteBuffer A01(Context context, Uri uri) {
        try {
            ParcelFileDescriptor A00 = C04050Ke.A00(context.getContentResolver(), uri, null, "r");
            if (A00 == null) {
                return null;
            }
            FileInputStream fileInputStream = new FileInputStream(A00.getFileDescriptor());
            try {
                FileChannel channel = fileInputStream.getChannel();
                MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                fileInputStream.close();
                A00.close();
                return map;
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException unused2) {
            return null;
        }
    }

    public static boolean A02(Resources resources, File file, int i) {
        try {
            InputStream openRawResource = resources.openRawResource(i);
            try {
                boolean A03 = A03(file, openRawResource);
                if (openRawResource != null) {
                    try {
                        openRawResource.close();
                    } catch (IOException unused) {
                    }
                }
                return A03;
            } finally {
                if (openRawResource == null) {
                    throw th;
                }
                try {
                } catch (IOException unused2) {
                    throw th;
                }
            }
        } catch (Throwable th) {
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        if (r5 == null) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A03(java.io.File r8, java.io.InputStream r9) {
        /*
            android.os.StrictMode$ThreadPolicy r7 = android.os.StrictMode.allowThreadDiskWrites()
            r6 = 0
            r5 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch: IOException -> 0x0028, all -> 0x004c
            r4.<init>(r8, r6)     // Catch: IOException -> 0x0028, all -> 0x004c
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r0]     // Catch: IOException -> 0x0025, all -> 0x0022
        L_0x000f:
            int r1 = r9.read(r2)     // Catch: IOException -> 0x0025, all -> 0x0022
            r0 = -1
            if (r1 == r0) goto L_0x001a
            r4.write(r2, r6, r1)     // Catch: IOException -> 0x0025, all -> 0x0022
            goto L_0x000f
        L_0x001a:
            r0 = 1
            r4.close()     // Catch: IOException -> 0x001e
        L_0x001e:
            android.os.StrictMode.setThreadPolicy(r7)
            return r0
        L_0x0022:
            r0 = move-exception
            r5 = r4
            goto L_0x004f
        L_0x0025:
            r3 = move-exception
            r5 = r4
            goto L_0x0029
        L_0x0028:
            r3 = move-exception
        L_0x0029:
            java.lang.String r2 = "TypefaceCompatUtil"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x004c
            r1.<init>()     // Catch: all -> 0x004c
            java.lang.String r0 = "Error copying resource contents to temp file: "
            r1.append(r0)     // Catch: all -> 0x004c
            java.lang.String r0 = r3.getMessage()     // Catch: all -> 0x004c
            r1.append(r0)     // Catch: all -> 0x004c
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x004c
            android.util.Log.e(r2, r0)     // Catch: all -> 0x004c
            if (r5 == 0) goto L_0x0048
            r5.close()     // Catch: IOException -> 0x0048
        L_0x0048:
            android.os.StrictMode.setThreadPolicy(r7)
            return r6
        L_0x004c:
            r0 = move-exception
            if (r5 == 0) goto L_0x0052
        L_0x004f:
            r5.close()     // Catch: IOException -> 0x0052
        L_0x0052:
            android.os.StrictMode.setThreadPolicy(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06500Tw.A03(java.io.File, java.io.InputStream):boolean");
    }
}
