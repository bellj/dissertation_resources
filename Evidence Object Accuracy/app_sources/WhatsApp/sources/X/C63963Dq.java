package X;

import java.util.Map;

/* renamed from: X.3Dq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63963Dq {
    public final C17120qI A00;
    public final String A01;

    public C63963Dq(C17120qI r1, String str) {
        C16700pc.A0G(r1, str);
        this.A00 = r1;
        this.A01 = str;
    }

    public final void A00(AnonymousClass4AI r3, String str, String str2, Map map) {
        C16700pc.A0E(str2, 2);
        this.A00.A02(this.A01).A01(new AnonymousClass5A3(r3, str2, str, map));
    }
}
