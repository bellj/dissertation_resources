package X;

/* renamed from: X.1Pj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28881Pj extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public String A03;

    public C28881Pj() {
        super(3080, new AnonymousClass00E(1, 1, 1), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A01);
        r3.Abe(3, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamOtpCopyCodeClick {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "businessPhoneNumber", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpEntryPoint", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otpEventType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "templateId", this.A03);
        sb.append("}");
        return sb.toString();
    }
}
