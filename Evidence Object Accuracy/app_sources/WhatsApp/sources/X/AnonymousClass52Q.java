package X;

/* renamed from: X.52Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52Q implements AbstractC116635Wf {
    public final C53042cM A00;
    public final AnonymousClass4EK A01;
    public final AnonymousClass10Q A02;
    public final C14850m9 A03;

    public AnonymousClass52Q(C53042cM r1, AnonymousClass4EK r2, AnonymousClass10Q r3, C14850m9 r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
    }
}
