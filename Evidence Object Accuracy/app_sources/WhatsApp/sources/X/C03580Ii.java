package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.0Ii  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03580Ii extends AbstractC92144Us {
    public final AnonymousClass3JI A00;
    public final C94004b6 A01;
    public final List A02;
    public final Map A03;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C03580Ii) {
                C03580Ii r5 = (C03580Ii) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A03, r5.A03) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A00.hashCode() * 31) + this.A02.hashCode()) * 31) + this.A03.hashCode()) * 31) + this.A01.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Success(data=");
        sb.append(this.A00);
        sb.append(", actions=");
        sb.append(this.A02);
        sb.append(", externalVariables=");
        sb.append(this.A03);
        sb.append(", fetchSummaryData=");
        sb.append(this.A01);
        sb.append(')');
        return sb.toString();
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public C03580Ii(AnonymousClass3JI r3, C94004b6 r4) {
        this(r3, r4, C16770pj.A0G(), C17530qx.A01());
        C16700pc.A0E(r3, 1);
    }

    public C03580Ii(AnonymousClass3JI r1, C94004b6 r2, List list, Map map) {
        super(r2);
        this.A00 = r1;
        this.A02 = list;
        this.A03 = map;
        this.A01 = r2;
    }

    public final AnonymousClass3JI A01() {
        return this.A00;
    }

    public final List A02() {
        return this.A02;
    }

    public final Map A03() {
        return this.A03;
    }
}
