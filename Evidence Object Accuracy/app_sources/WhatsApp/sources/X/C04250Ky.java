package X;

import android.os.Build;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: X.0Ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C04250Ky {
    public static void A00(AccessibilityEvent accessibilityEvent, int i) {
        if (Build.VERSION.SDK_INT >= 19) {
            C05760Qv.A01(accessibilityEvent, i);
        }
    }
}
