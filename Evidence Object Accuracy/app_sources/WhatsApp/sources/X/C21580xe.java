package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0xe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21580xe extends AbstractC21570xd {
    public static final String[] A08 = {"wa_contacts._id", "wa_contacts.jid", "is_whatsapp_user", "status", "number", "raw_contact_id", "display_name", "phone_type", "phone_label", "unseen_msg_count", "photo_ts", "thumb_ts", "photo_id_timestamp", "given_name", "family_name", "wa_name", "sort_name", "status_timestamp", "nickname", "company", "title", "status_autodownload_disabled", "keep_timestamp", "is_spam_reported", "is_sidelist_synced", "is_business_synced", "disappearing_mode_duration", "disappearing_mode_timestamp", "history_sync_initial_phash", "verified_name", "expires", "verified_level", "description", "identity_unconfirmed_since", "description_id_string", "description_time", "description_setter_jid", "restrict_mode", "announcement_group", "no_frequently_forwarded", "ephemeral_duration", "creator_jid", "in_app_support", "is_suspended", "require_membership_approval", "member_add_mode", "incognito", "group_state"};
    public static final String[] A09 = {"count(wa_contacts._id) AS _count"};
    public static final String[] A0A = {"wa_contacts.jid", "number"};
    public static final String[] A0B = {"_id", "jid", "serial", "issuer", "expires", "verified_name", "industry", "city", "country", "verified_level", "cert_blob", "identity_unconfirmed_since", "host_storage", "actual_actors", "privacy_mode_ts"};
    public Integer A00;
    public final C15570nT A01;
    public final AnonymousClass1GT A02;
    public final AnonymousClass01d A03;
    public final C14830m7 A04;
    public final AnonymousClass018 A05;
    public final AbstractC14440lR A06;
    public final Object A07 = new Object();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C21580xe(C15570nT r3, AnonymousClass01d r4, C14830m7 r5, AnonymousClass018 r6, AbstractC14440lR r7, C232010t r8) {
        super(r8);
        AnonymousClass1GT r1 = new AnonymousClass1GT();
        this.A04 = r5;
        this.A01 = r3;
        this.A06 = r7;
        this.A03 = r4;
        this.A05 = r6;
        this.A02 = r1;
        r1.A03(new AnonymousClass1VV(r3, this));
    }

    public static final C15370n3 A06(C15370n3 r5, C15370n3 r6) {
        C28811Pc r2;
        C28811Pc r0;
        if (r6 == null || ((r2 = r6.A0C) == null && r5.A0C != null)) {
            return r5;
        }
        if (Build.MANUFACTURER.equalsIgnoreCase("lge") && r2 != null && r2.A00 == -2 && (r0 = r5.A0C) != null && r0.A00 != -2) {
            return r5;
        }
        boolean z = r6.A0f;
        boolean z2 = r5.A0f;
        if (!z) {
            if (z2) {
                return r5;
            }
        } else if (z2 && r5.A08() < r6.A08()) {
            return r5;
        }
        return r6;
    }

    public static final Set A07(Cursor cursor, String str) {
        HashSet hashSet = new HashSet();
        if (cursor == null) {
            AnonymousClass009.A07(str);
        } else {
            while (cursor.moveToNext()) {
                UserJid nullable = UserJid.getNullable(cursor.getString(0));
                if (nullable != null) {
                    hashSet.add(nullable);
                }
            }
        }
        return hashSet;
    }

    public static boolean A08(C15370n3 r1) {
        C28811Pc r0;
        return (r1.A0D == null || (r0 = r1.A0C) == null || TextUtils.isEmpty(r0.A01)) ? false : true;
    }

    public Cursor A09() {
        C16310on A01 = super.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_contact_storage_usage", "jid != ?", "conversation_size DESC, conversation_message_count DESC", "CONTACT_STORAGE_USAGES", new String[]{"jid", "conversation_size", "conversation_message_count"}, new String[]{AnonymousClass1VX.A00.getRawString()});
            A01.close();
            return A03;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0078, code lost:
        if (r8 == null) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C15370n3 A0A(X.AbstractC14640lm r13) {
        /*
            r12 = this;
            r8 = 0
            if (r13 != 0) goto L_0x0009
            java.lang.String r0 = "contact-mgr-db/cannot get contact by null jid"
            com.whatsapp.util.Log.w(r0)
            return r8
        L_0x0009:
            r0 = 1
            X.1Ma r3 = new X.1Ma
            r3.<init>(r0)
            r3.A03()
            X.10t r0 = r12.A00     // Catch: all -> 0x0096
            X.0on r5 = r0.get()     // Catch: all -> 0x0096
            java.lang.String r6 = "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)"
            java.lang.String[] r10 = X.C21580xe.A08     // Catch: all -> 0x0091
            java.lang.String r7 = "wa_contacts.jid = ?"
            r0 = 1
            java.lang.String[] r11 = new java.lang.String[r0]     // Catch: all -> 0x0091
            r1 = 0
            java.lang.String r0 = r13.getRawString()     // Catch: all -> 0x0091
            r11[r1] = r0     // Catch: all -> 0x0091
            java.lang.String r9 = "CONTACTS"
            android.database.Cursor r2 = X.AbstractC21570xd.A03(r5, r6, r7, r8, r9, r10, r11)     // Catch: all -> 0x0091
            if (r2 != 0) goto L_0x004a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x008a
            r1.<init>()     // Catch: all -> 0x008a
            java.lang.String r0 = "contact-mgr-db/unable to get contact by jid "
            r1.append(r0)     // Catch: all -> 0x008a
            r1.append(r13)     // Catch: all -> 0x008a
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x008a
            X.AnonymousClass009.A07(r0)     // Catch: all -> 0x008a
            r5.close()     // Catch: all -> 0x0096
            return r8
        L_0x004a:
            r4 = r8
        L_0x004b:
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x008a
            if (r0 == 0) goto L_0x005f
            X.0n3 r1 = X.AnonymousClass1VW.A00(r2)     // Catch: all -> 0x008a
            X.0n3 r8 = A06(r1, r8)     // Catch: all -> 0x008a
            X.1Pc r0 = r1.A0C     // Catch: all -> 0x008a
            if (r0 != 0) goto L_0x004b
            r4 = r1
            goto L_0x004b
        L_0x005f:
            r2.getCount()     // Catch: all -> 0x008a
            r2.close()     // Catch: all -> 0x0091
            r5.close()     // Catch: all -> 0x0096
            if (r4 == 0) goto L_0x0078
            if (r4 == r8) goto L_0x007e
            X.0lR r2 = r12.A06
            r1 = 27
            com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2 r0 = new com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2
            r0.<init>(r12, r1, r4)
            r2.Ab2(r0)
        L_0x0078:
            if (r8 != 0) goto L_0x007e
        L_0x007a:
            r3.A00()
            return r8
        L_0x007e:
            X.018 r0 = r12.A05
            android.content.Context r0 = r0.A00
            java.util.Locale r0 = X.AnonymousClass018.A00(r0)
            r12.A0N(r8, r0)
            goto L_0x007a
        L_0x008a:
            r0 = move-exception
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch: all -> 0x0090
        L_0x0090:
            throw r0     // Catch: all -> 0x0091
        L_0x0091:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0095
        L_0x0095:
            throw r0     // Catch: all -> 0x0096
        L_0x0096:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21580xe.A0A(X.0lm):X.0n3");
    }

    public ArrayList A0B() {
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        ArrayList arrayList = new ArrayList();
        C16310on A01 = super.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "wa_contacts.jid LIKE '%@g.us' OR wa_contacts.jid LIKE '%@temp'", null, "CONTACTS", A08, null);
            if (A03 == null) {
                Log.e("contact-mgr-db/unable to get all group chats");
                A01.close();
                return arrayList;
            }
            while (A03.moveToNext()) {
                C15370n3 A00 = AnonymousClass1VW.A00(A03);
                if (A00.A0D != null) {
                    arrayList.add(A00);
                }
            }
            A03.close();
            A01.close();
            arrayList.size();
            r4.A00();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public ArrayList A0C(AbstractC14640lm r13) {
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        ArrayList arrayList = new ArrayList();
        C16310on A01 = super.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "wa_contacts.jid = ?", null, "CONTACTS", A08, new String[]{r13.getRawString()});
            if (A03 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("contact-mgr-db/unable to get contacts by jid ");
                sb.append(r13);
                AnonymousClass009.A07(sb.toString());
                A01.close();
                return arrayList;
            }
            while (A03.moveToNext()) {
                C15370n3 A00 = AnonymousClass1VW.A00(A03);
                if (A00.A0D != null) {
                    arrayList.add(A00);
                }
            }
            A03.close();
            A01.close();
            A0Q(arrayList);
            arrayList.size();
            r4.A00();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final Collection A0D(boolean z) {
        String str;
        Collection collection;
        if (z) {
            str = "is_sidelist_synced= 1)";
        } else {
            str = "is_sidelist_synced= 0 OR is_sidelist_synced IS NULL)";
        }
        StringBuilder sb = new StringBuilder("is_whatsapp_user = 1 AND wa_contacts.jid != ? AND wa_contacts.jid IS NOT NULL AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid != ? AND wa_contacts.jid != ? AND (raw_contact_id IS NULL OR raw_contact_id<0) AND (");
        sb.append(str);
        String obj = sb.toString();
        ArrayList arrayList = new ArrayList();
        C15570nT r0 = this.A01;
        r0.A08();
        String A03 = C15380n4.A03(r0.A05);
        String[] strArr = new String[6];
        int i = 0;
        strArr[0] = "broadcast";
        strArr[1] = "%@broadcast";
        strArr[2] = "%@g.us";
        strArr[3] = "%@temp";
        if (A03 == null) {
            A03 = AnonymousClass1VY.A00.getRawString();
        }
        strArr[4] = A03;
        strArr[5] = AnonymousClass1VZ.A00.getRawString();
        C16310on A01 = super.A00.get();
        try {
            Cursor A032 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", obj, null, "CONTACTS", A08, strArr);
            if (A032 == null) {
                AnonymousClass009.A07("contact-mgr-db/unable to get sidelist sync pending list");
                collection = Collections.emptyList();
            } else {
                try {
                    i = A032.getCount();
                    while (A032.moveToNext()) {
                        C15370n3 A00 = AnonymousClass1VW.A00(A032);
                        if (A00.A0D != null) {
                            arrayList.add(A00);
                        }
                    }
                } catch (IllegalStateException e) {
                    if (e.getMessage() == null || !e.getMessage().contains("Make sure the Cursor is initialized correctly before accessing data from it")) {
                        throw e;
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("contactmanagerdb/getSideListContacts/illegal-state-exception/cursor count=");
                    sb2.append(i);
                    sb2.append("; partial list size=");
                    sb2.append(arrayList.size());
                    AnonymousClass009.A08(sb2.toString(), e);
                }
                A032.close();
                collection = arrayList;
            }
            A01.close();
            return collection;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0E(ContentValues contentValues, Jid jid) {
        try {
            C16310on A02 = super.A00.A02();
            String A03 = C15380n4.A03(jid);
            AnonymousClass009.A05(A03);
            AbstractC21570xd.A01(contentValues, A02, "wa_contacts", "jid = ?", new String[]{A03});
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("contact-mgr-db/unable to update contact by jid ");
            sb.append(jid);
            AnonymousClass009.A08(sb.toString(), e);
        }
    }

    public final void A0F(C16310on r8, AnonymousClass1Lx r9, C15370n3 r10) {
        AnonymousClass009.A0F(r9.A01());
        Jid jid = r10.A0D;
        String A03 = C15380n4.A03(jid);
        AbstractC21570xd.A02(r8, "wa_contacts", "_id = ?", new String[]{String.valueOf(r10.A08())});
        AbstractC21570xd.A02(r8, "wa_contact_storage_usage", "jid = ? AND NOT EXISTS (SELECT 1 FROM wa_contacts WHERE jid = ?)", new String[]{A03, A03});
        if (jid instanceof C15580nU) {
            A0G(r8, r9, (C15580nU) jid, null);
        }
        AbstractC21570xd.A02(r8, "wa_group_admin_settings", "jid = ?", new String[]{A03});
    }

    public final void A0G(C16310on r7, AnonymousClass1Lx r8, C15580nU r9, AnonymousClass1PD r10) {
        AnonymousClass009.A0F(r8.A01());
        if (r9 != null) {
            String rawString = r9.getRawString();
            AnonymousClass009.A05(rawString);
            if (r10 == null) {
                AbstractC21570xd.A02(r7, "wa_group_descriptions", "jid = ?", new String[]{rawString});
                return;
            }
            String str = r10.A02;
            if (str != null) {
                AbstractC21570xd.A02(r7, "wa_group_descriptions", "jid = ?", new String[]{rawString});
                ContentValues contentValues = new ContentValues(5);
                contentValues.put("jid", rawString);
                contentValues.put("description", str);
                String str2 = r10.A03;
                String str3 = "";
                if (str2 == null) {
                    str2 = str3;
                }
                contentValues.put("description_id_string", str2);
                contentValues.put("description_time", Long.valueOf(r10.A00));
                UserJid userJid = r10.A01;
                if (userJid != null) {
                    str3 = userJid.getRawString();
                }
                contentValues.put("description_setter_jid", str3);
                AbstractC21570xd.A00(contentValues, r7, "wa_group_descriptions");
            }
        }
    }

    public final void A0H(AnonymousClass1Lx r8, UserJid userJid) {
        AnonymousClass009.A0F(r8.A01());
        String A03 = C15380n4.A03(userJid);
        AnonymousClass009.A05(A03);
        C16310on A02 = super.A00.A02();
        try {
            AbstractC21570xd.A02(A02, "wa_vnames", "jid = ?", new String[]{A03});
            AbstractC21570xd.A02(A02, "wa_vnames_localized", "jid = ?", new String[]{A03});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0I(C28021Kd r11) {
        try {
            C16310on A02 = super.A00.A02();
            AnonymousClass1Lx A00 = A02.A00();
            try {
                String rawString = r11.A01().getRawString();
                ContentValues contentValues = new ContentValues();
                contentValues.put("conversation_size", Long.valueOf(r11.A00.A0G));
                contentValues.put("conversation_message_count", Integer.valueOf(r11.A00.A06));
                if (AbstractC21570xd.A01(contentValues, A02, "wa_contact_storage_usage", "jid = ?", new String[]{rawString}) == 0) {
                    contentValues.put("jid", rawString);
                    AbstractC21570xd.A04(contentValues, A02, "wa_contact_storage_usage");
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IllegalArgumentException e) {
            AnonymousClass009.A08("contact-mgr-db/unable to update batch on storage usage table", e);
        }
    }

    public void A0J(C15370n3 r6) {
        String str;
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        Jid jid = r6.A0D;
        if (jid == null) {
            str = "contact-mgr-db/unable to add unknown contact with null jid";
        } else {
            C15570nT r1 = this.A01;
            r1.A08();
            if (r1.A05 == null) {
                str = "contact-mgr-db/unable to add unknown contact due to null me record";
            } else if (r6.A0K() || !r1.A0F(jid)) {
                ContentValues contentValues = new ContentValues(4);
                contentValues.put("jid", jid.getRawString());
                contentValues.put("is_whatsapp_user", Boolean.TRUE);
                contentValues.put("status", r6.A0R);
                contentValues.put("status_timestamp", Long.valueOf(r6.A0B));
                try {
                    C16310on A02 = super.A00.A02();
                    try {
                        r6.A0F(AbstractC21570xd.A00(contentValues, A02, "wa_contacts"));
                        A02.close();
                        this.A02.A05(Collections.singletonList(r6));
                        r3.A00();
                        return;
                    } catch (Throwable th) {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IllegalArgumentException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("contact-mgr-db/unable to add unknown contact ");
                    sb.append(r6);
                    AnonymousClass009.A08(sb.toString(), e);
                    return;
                }
            } else {
                Log.i("contact-mgr-db/unable to add unknown contact due to matching jid prefix");
                return;
            }
        }
        Log.w(str);
    }

    public void A0K(C15370n3 r6) {
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        ContentValues contentValues = new ContentValues(5);
        contentValues.put("display_name", r6.A0K);
        contentValues.put("phone_label", r6.A0P);
        contentValues.put("is_whatsapp_user", Boolean.valueOf(r6.A0f));
        contentValues.put("history_sync_initial_phash", r6.A0N);
        A0E(contentValues, r6.A0D);
        A0M(r6, (C15580nU) r6.A0B(C15580nU.class));
        StringBuilder sb = new StringBuilder("updated group info for jid=");
        sb.append(r6.A0D);
        sb.append(' ');
        sb.append(contentValues);
        sb.append(" | time: ");
        sb.append(r3.A00());
        Log.i(sb.toString());
        this.A02.A05(Collections.singleton(r6));
    }

    public final void A0L(C15370n3 r5) {
        C16310on A02 = super.A00.A02();
        AnonymousClass1Lx A00 = A02.A00();
        try {
            AnonymousClass009.A0F(A00.A01());
            C28181Ma r0 = new C28181Ma(true);
            r0.A03();
            A0F(A02, A00, r5);
            r0.A00();
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0M(C15370n3 r17, C15580nU r18) {
        if (r18 != null) {
            try {
                C16310on A02 = super.A00.A02();
                AnonymousClass1Lx A00 = A02.A00();
                A0G(A02, A00, r18, r17.A0G);
                boolean z = r17.A0i;
                boolean z2 = r17.A0W;
                boolean z3 = r17.A0g;
                int i = r17.A01;
                UserJid userJid = r17.A0E;
                boolean z4 = r17.A0Y;
                boolean z5 = r17.A0a;
                boolean z6 = r17.A0h;
                int i2 = r17.A03;
                boolean z7 = r17.A0Z;
                int i3 = r17.A02;
                ContentValues contentValues = new ContentValues();
                contentValues.put("jid", r18.getRawString());
                contentValues.put("restrict_mode", Boolean.valueOf(z));
                contentValues.put("announcement_group", Boolean.valueOf(z2));
                contentValues.put("no_frequently_forwarded", Boolean.valueOf(z3));
                contentValues.put("ephemeral_duration", Integer.valueOf(i));
                contentValues.put("creator_jid", C15380n4.A03(userJid));
                contentValues.put("in_app_support", Boolean.valueOf(z4));
                contentValues.put("is_suspended", Boolean.valueOf(z5));
                contentValues.put("require_membership_approval", Boolean.valueOf(z6));
                contentValues.put("member_add_mode", Integer.valueOf(i2));
                contentValues.put("incognito", Boolean.valueOf(z7));
                contentValues.put("group_state", Integer.valueOf(i3));
                AbstractC21570xd.A04(contentValues, A02, "wa_group_admin_settings");
                A00.A00();
                A00.close();
                A02.close();
            } catch (IllegalArgumentException e) {
                AnonymousClass009.A08("contact-mgr-db/unable to update group settings ", e);
            }
        }
    }

    public final void A0N(C15370n3 r18, Locale locale) {
        if (r18.A0L()) {
            String language = locale.getLanguage();
            String country = locale.getCountry();
            Jid jid = r18.A0D;
            C16310on A01 = super.A00.get();
            String A03 = C15380n4.A03(jid);
            AnonymousClass009.A05(A03);
            Cursor A032 = AbstractC21570xd.A03(A01, "wa_vnames_localized", "jid = ? AND lg = ?", null, "CONTACT_VNAMES_LOCALIZED", new String[]{"lc", "verified_name"}, new String[]{A03, language});
            if (A032 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("contact-mgr-db/unable to get localized vname by jid ");
                sb.append(jid);
                AnonymousClass009.A07(sb.toString());
            } else {
                String str = null;
                while (true) {
                    if (!A032.moveToNext()) {
                        break;
                    }
                    String string = A032.getString(0);
                    String string2 = A032.getString(1);
                    if (TextUtils.isEmpty(string)) {
                        str = string2;
                    } else if (string.equals(country)) {
                        str = string2;
                        break;
                    }
                }
                if (str != null) {
                    if (!(r18 instanceof C29861Va)) {
                        r18.A0T = str;
                    } else {
                        AnonymousClass009.A07("Setting verified name for ServerContact not allowed");
                    }
                }
                r18.A0V = locale;
                A032.close();
            }
            A01.close();
        }
    }

    public void A0O(UserJid userJid, String str, long j) {
        C28181Ma r5 = new C28181Ma(true);
        r5.A03();
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("status", str);
        contentValues.put("status_timestamp", Long.valueOf(j));
        boolean z = true;
        try {
            C16310on A02 = super.A00.A02();
            AbstractC21570xd.A01(contentValues, A02, "wa_contacts", "jid = ?", new String[]{userJid.getRawString()});
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("contact-mgr-db/unable to update contact status ");
            sb.append(userJid);
            sb.append(", statusNull=");
            if (str != null) {
                z = false;
            }
            sb.append(z);
            AnonymousClass009.A08(sb.toString(), e);
        }
        r5.A00();
    }

    public final void A0P(String str, Set set) {
        C16310on A02 = super.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AbstractC21570xd.A02(A02, str, null, null);
            Iterator it = set.iterator();
            while (it.hasNext()) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("jid", ((UserJid) it.next()).getRawString());
                AbstractC21570xd.A00(contentValues, A02, str);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0Q(Collection collection) {
        Locale A00 = AnonymousClass018.A00(this.A05.A00);
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            A0N((C15370n3) it.next(), A00);
        }
    }

    public void A0R(List list) {
        ContentValues contentValues = new ContentValues();
        try {
            C16310on A02 = super.A00.A02();
            AnonymousClass1Lx A00 = A02.A00();
            try {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    AbstractC14640lm r0 = (AbstractC14640lm) it.next();
                    if (r0 != null) {
                        contentValues.put("jid", r0.getRawString());
                        contentValues.put("conversation_size", (Integer) 0);
                        contentValues.put("conversation_message_count", (Integer) 0);
                        AbstractC21570xd.A04(contentValues, A02, "wa_contact_storage_usage");
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IllegalArgumentException e) {
            AnonymousClass009.A08("contact-mgr-db/unable to insert batch to storage usage table", e);
        }
    }

    public final void A0S(List list, int i, boolean z, boolean z2) {
        String str;
        C232010t r1;
        C16310on A01;
        Cursor A03;
        AnonymousClass1Lx r2;
        boolean z3;
        C28181Ma r15 = new C28181Ma(true);
        r15.A03();
        StringBuilder sb = new StringBuilder("is_whatsapp_user");
        if (z) {
            str = " = 0";
        } else {
            str = " = 1";
        }
        sb.append(str);
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r7 = r0.A05;
        if (r7 != null) {
            sb.append(" AND ");
            sb.append("wa_contacts.jid");
            sb.append(" != ?");
        }
        if (i == 1 || i == 2) {
            sb.append(" AND (");
            sb.append("raw_contact_id");
            sb.append(" > 0 OR ");
            sb.append("raw_contact_id");
            sb.append(" = ");
            sb.append(-2L);
            sb.append(" OR ");
            sb.append("raw_contact_id");
            sb.append(" = ");
            sb.append(-3L);
            sb.append(')');
        }
        HashMap hashMap = new HashMap();
        String[] strArr = r7 == null ? new String[0] : new String[]{r7.getRawString()};
        ArrayList arrayList = new ArrayList();
        try {
            r1 = super.A00;
            A01 = r1.get();
            A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", sb.toString(), "display_name, wa_contacts.jid, phone_type ASC", "CONTACTS", A08, strArr);
        } catch (IllegalArgumentException e) {
            AnonymousClass009.A08("contact-mgr-db/unable to apply contact picker list de-dupe batch ", e);
        }
        if (A03 == null) {
            try {
                AnonymousClass009.A07("contact-mgr-db/get-picker-list/unable to get contact picker list");
                A01.close();
            } catch (Throwable th) {
            }
        } else {
            while (A03.moveToNext()) {
                try {
                    C15370n3 A00 = AnonymousClass1VW.A00(A03);
                    Jid jid = A00.A0D;
                    if (jid != null && !C15380n4.A0N(jid) && !C15380n4.A0O(jid) && jid.getType() != 11 && (i != 3 || !C15380n4.A0G(jid))) {
                        if (hashMap.containsKey(jid)) {
                            List<C15370n3> list2 = (List) hashMap.get(jid);
                            AnonymousClass009.A05(list2);
                            ArrayList arrayList2 = new ArrayList();
                            boolean z4 = true;
                            if (A00.A0C != null) {
                                z3 = false;
                            } else if (!list2.isEmpty()) {
                                C15370n3 r13 = (C15370n3) list2.get(0);
                                if (r13.A0C != null) {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("contact-mgr-db/process-contact/removing duplicate contact with null key ");
                                    sb2.append(A00);
                                    Log.i(sb2.toString());
                                } else if (r13.A09 < A00.A09) {
                                    list2.remove(r13);
                                    arrayList2.add(r13);
                                    list2.add(A00);
                                    z3 = true;
                                }
                                arrayList2.add(A00);
                                z3 = true;
                            } else {
                                throw new IllegalStateException("same jid contacts must not be empty");
                            }
                            ArrayList arrayList3 = new ArrayList();
                            if (!z3) {
                                Iterator it = list2.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        C15370n3 r12 = (C15370n3) it.next();
                                        if (r12.A0C == null) {
                                            StringBuilder sb3 = new StringBuilder();
                                            sb3.append("contact-mgr-db/process-contact/removing duplicate contact with null key ");
                                            sb3.append(r12);
                                            Log.i(sb3.toString());
                                            arrayList3.add(r12);
                                            arrayList2.add(r12);
                                            list2.add(A00);
                                            break;
                                        }
                                    } else {
                                        Iterator it2 = list2.iterator();
                                        while (true) {
                                            if (it2.hasNext()) {
                                                if (A00.A0C.equals(((C15370n3) it2.next()).A0C)) {
                                                    StringBuilder sb4 = new StringBuilder("contact-mgr-db/process-contact/removing duplicate contact with matching key ");
                                                    sb4.append(A00);
                                                    Log.i(sb4.toString());
                                                    arrayList2.add(A00);
                                                    break;
                                                }
                                            } else {
                                                for (C15370n3 r22 : list2) {
                                                    if (A00.A0C.A00 != -2) {
                                                        C28811Pc r02 = r22.A0C;
                                                        AnonymousClass009.A05(r02);
                                                        if (r02.A00 == -2) {
                                                            StringBuilder sb5 = new StringBuilder("contact-mgr-db/process-contact/removing sim card duplicate contact ");
                                                            sb5.append(r22);
                                                            Log.i(sb5.toString());
                                                            arrayList3.add(r22);
                                                            arrayList2.add(r22);
                                                            z3 = true;
                                                        }
                                                    }
                                                }
                                                if (!z3) {
                                                    for (C15370n3 r14 : list2) {
                                                        String str2 = r14.A0K;
                                                        if ((str2 == null && A00.A0K != null) || (TextUtils.isEmpty(str2) && !TextUtils.isEmpty(A00.A0K))) {
                                                            StringBuilder sb6 = new StringBuilder("contact-mgr-db/process-contact/deduping null/empty display name contact ");
                                                            sb6.append(r14);
                                                            Log.i(sb6.toString());
                                                            arrayList3.add(r14);
                                                            z3 = true;
                                                        }
                                                    }
                                                    if (!z3) {
                                                        if (TextUtils.isEmpty(A00.A0K)) {
                                                            for (C15370n3 r03 : list2) {
                                                                if (!TextUtils.isEmpty(r03.A0K)) {
                                                                    Log.i("contact-mgr-db/process-contact/ignoring empty name since we have non-empty one");
                                                                }
                                                            }
                                                        }
                                                        for (C15370n3 r16 : list2) {
                                                            String str3 = r16.A0K;
                                                            if (str3 != null && str3.equals(A00.A0K) && A00.A08() < r16.A08()) {
                                                                arrayList3.add(r16);
                                                                z3 = true;
                                                            }
                                                        }
                                                        if (!z3) {
                                                            if (A00.A0K != null) {
                                                                for (C15370n3 r04 : list2) {
                                                                    if (A00.A0K.equals(r04.A0K)) {
                                                                    }
                                                                }
                                                                list2.add(A00);
                                                            }
                                                        }
                                                    }
                                                }
                                                list2.add(A00);
                                            }
                                        }
                                    }
                                }
                                z3 = true;
                            }
                            z4 = z3;
                            list2.removeAll(arrayList3);
                            if (!z4) {
                                StringBuilder sb7 = new StringBuilder("existing_contacts: ");
                                for (Object obj : list2) {
                                    sb7.append(obj);
                                    sb7.append(", ");
                                }
                                StringBuilder sb8 = new StringBuilder("contact-mgr-db/process-contact/contacts are identical, yet not (");
                                sb8.append((Object) sb7);
                                sb8.append(" and ");
                                sb8.append(A00);
                                sb8.append(')');
                                Log.w(sb8.toString());
                            }
                            arrayList.addAll(arrayList2);
                        } else {
                            ArrayList arrayList4 = new ArrayList();
                            arrayList4.add(A00);
                            hashMap.put(jid, arrayList4);
                        }
                    }
                } catch (Throwable th2) {
                    try {
                        A03.close();
                        throw th2;
                    } catch (Throwable unused) {
                        throw th2;
                    }
                }
            }
            A03.close();
            for (Map.Entry entry : hashMap.entrySet()) {
                list.addAll((Collection) entry.getValue());
            }
            A0Q(list);
            if (!arrayList.isEmpty()) {
                C16310on A02 = r1.A02();
                if (z2) {
                    r2 = A02.A01();
                } else {
                    r2 = A02.A00();
                }
                try {
                    Iterator it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        A0F(A02, r2, (C15370n3) it3.next());
                    }
                    r2.A00();
                    r2.close();
                    A02.close();
                } catch (Throwable th3) {
                    try {
                        r2.close();
                    } catch (Throwable unused2) {
                    }
                    throw th3;
                }
            }
            A01.close();
            list.size();
            r15.A00();
        }
    }
}
