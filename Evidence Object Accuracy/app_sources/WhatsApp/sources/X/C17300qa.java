package X;

import java.util.Map;

/* renamed from: X.0qa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17300qa implements AnonymousClass6MF {
    public final /* synthetic */ C19960ux A00;

    public C17300qa(C19960ux r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MF
    public /* bridge */ /* synthetic */ AbstractC120015fT A8U(String str, String str2, String str3, Map map, long j) {
        AnonymousClass01J r1 = this.A00.A01;
        return new C123785nw((C18790t3) r1.AJw.get(), (C14820m6) r1.AN3.get(), (C14850m9) r1.A04.get(), (AnonymousClass18L) r1.A89.get(), C18000rk.A00(r1.AMu), str, str2, map, r1.AIl, r1.AIm, j);
    }
}
