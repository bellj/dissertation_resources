package X;

/* renamed from: X.4Ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C88274Ey {
    public static final long A00(String str, long j, long j2, long j3) {
        try {
            String property = System.getProperty(str);
            if (property != null) {
                Long A0N = AnonymousClass1WU.A0N(property);
                if (A0N != null) {
                    long longValue = A0N.longValue();
                    if (j2 <= longValue && longValue <= j3) {
                        return longValue;
                    }
                    StringBuilder A0j = C12960it.A0j("System property '");
                    A0j.append(str);
                    A0j.append("' should be in range ");
                    A0j.append(j2);
                    A0j.append("..");
                    A0j.append(j3);
                    A0j.append(", but is '");
                    A0j.append(longValue);
                    throw C12960it.A0U(C72463ee.A0H(A0j, '\''));
                }
                StringBuilder A0j2 = C12960it.A0j("System property '");
                A0j2.append(str);
                A0j2.append("' has unrecognized value '");
                A0j2.append(property);
                throw C12960it.A0U(C72463ee.A0H(A0j2, '\''));
            }
        } catch (SecurityException unused) {
        }
        return j;
    }
}
