package X;

/* renamed from: X.30b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C613630b extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Long A03;

    public C613630b() {
        super(3266, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPsPhoneNumberHyperlink {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isPhoneNumHyperlinkOwner", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "phoneNumHyperlinkAction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "phoneNumberStatusOnWa", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
