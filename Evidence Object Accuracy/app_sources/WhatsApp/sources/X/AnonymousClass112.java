package X;

/* renamed from: X.112  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass112 {
    public final C14330lG A00;
    public final C16510p9 A01;
    public final C16490p7 A02;
    public final C20180vL A03;
    public final AnonymousClass111 A04;
    public final C15760nr A05;
    public final C15740np A06;

    public AnonymousClass112(C14330lG r1, C16510p9 r2, C16490p7 r3, C20180vL r4, AnonymousClass111 r5, C15760nr r6, C15740np r7) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r7;
        this.A02 = r3;
        this.A05 = r6;
    }
}
