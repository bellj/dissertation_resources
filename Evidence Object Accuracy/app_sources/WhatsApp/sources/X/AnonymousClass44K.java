package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44K extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AbstractC84543zT A01;

    public AnonymousClass44K(SearchViewModel searchViewModel, AbstractC84543zT r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }

    @Override // X.AbstractC51682Vy
    public void A0A() {
        this.A01.A03();
    }

    @Override // X.AbstractC51682Vy
    public void A0B(boolean z) {
        this.A01.setScrolling(z);
    }

    @Override // X.AbstractC51682Vy
    public void A0C(boolean z) {
        this.A01.setShouldPlay(z);
    }

    @Override // X.AbstractC51682Vy
    public boolean A0D() {
        return this.A01.A04();
    }
}
