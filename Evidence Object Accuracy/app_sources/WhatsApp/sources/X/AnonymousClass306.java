package X;

/* renamed from: X.306  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass306 extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass306() {
        super(1124, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamGifFromProviderSent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifSearchProvider", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
