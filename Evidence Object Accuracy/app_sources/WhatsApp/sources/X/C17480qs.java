package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.whatsapp.jid.UserJid;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.0qs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17480qs implements AbstractC17490qt {
    public C239213n A00;
    public final C14900mE A01;
    public final C14650lo A02;
    public final C18010rl A03;
    public final C14820m6 A04;
    public final AbstractC14440lR A05;

    public C17480qs(C239213n r2, C14900mE r3, C14650lo r4, C18010rl r5, C14820m6 r6, AbstractC14440lR r7) {
        C16700pc.A0E(r5, 1);
        C16700pc.A0E(r2, 2);
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(r7, 4);
        C16700pc.A0E(r3, 5);
        C16700pc.A0E(r6, 6);
        this.A03 = r5;
        this.A00 = r2;
        this.A02 = r4;
        this.A05 = r7;
        this.A01 = r3;
        this.A04 = r6;
    }

    public static final /* synthetic */ void A00(C90134Ms r8, C90184Mx r9, UserJid userJid, C17480qs r11, String str, String str2) {
        String str3;
        Object obj;
        String str4;
        String str5;
        int hashCode = str.hashCode();
        if (hashCode != -1930003499) {
            if (hashCode != -1867169789) {
                if (hashCode == 688255102 && str.equals("unserviceable_location")) {
                    str4 = "in_pin_code_not_servicable";
                    C17520qw r0 = new C17520qw("in_pin_code", str4);
                    Map singletonMap = Collections.singletonMap(r0.first, r0.second);
                    C16700pc.A0B(singletonMap);
                    C17520qw r02 = new C17520qw("validation_errors", singletonMap);
                    obj = Collections.singletonMap(r02.first, r02.second);
                    C16700pc.A0B(obj);
                    str3 = "success";
                }
            } else if (str.equals("success")) {
                r11.A01.A0H(new RunnableBRunnable0Shape0S1300000_I0(r11, r9, C71373cp.A00, "success", 9));
                if (r8 != null && (str5 = r8.A00) != null) {
                    C14820m6 r1 = r11.A04;
                    r1.A0s(userJid.getRawString(), str2);
                    r1.A0r(userJid.getRawString(), str5);
                    return;
                }
                return;
            }
            str3 = "error";
            obj = C71373cp.A00;
        } else {
            if (str.equals("invalid_postcode")) {
                str4 = "in_pin_code_invalid";
                C17520qw r0 = new C17520qw("in_pin_code", str4);
                Map singletonMap = Collections.singletonMap(r0.first, r0.second);
                C16700pc.A0B(singletonMap);
                C17520qw r02 = new C17520qw("validation_errors", singletonMap);
                obj = Collections.singletonMap(r02.first, r02.second);
                C16700pc.A0B(obj);
                str3 = "success";
            }
            str3 = "error";
            obj = C71373cp.A00;
        }
        r11.A01.A0H(new RunnableBRunnable0Shape0S1300000_I0(r11, r9, obj, str3, 9));
    }

    @Override // X.AbstractC17490qt
    public void AZB(Activity activity, C90184Mx r13, Map map) {
        Object obj;
        String str;
        Intent intent;
        Bundle extras;
        Map map2;
        Map map3 = map;
        if (map == null) {
            map3 = C71373cp.A00;
        }
        Object obj2 = map3.get("values");
        String str2 = null;
        if (!(obj2 instanceof Map) || (map2 = (Map) obj2) == null) {
            obj = null;
        } else {
            obj = map2.get("in_pin_code");
        }
        if (obj instanceof String) {
            str = (String) obj;
        } else {
            str = null;
        }
        if (!(activity == null || (intent = activity.getIntent()) == null || (extras = intent.getExtras()) == null)) {
            str2 = extras.getString("chat_id");
        }
        if (str == null || AnonymousClass03C.A0J(str) || str2 == null) {
            this.A01.A0H(new RunnableBRunnable0Shape0S1300000_I0(this, r13, C71373cp.A00, "error", 9));
            return;
        }
        UserJid userJid = UserJid.get(str2);
        C16700pc.A0B(userJid);
        this.A05.Ab2(new RunnableBRunnable0Shape0S1300000_I0(this, userJid, r13, str, 10));
    }
}
