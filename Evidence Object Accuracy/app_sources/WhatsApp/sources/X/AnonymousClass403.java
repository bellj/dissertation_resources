package X;

import com.whatsapp.businessdirectory.viewmodel.LocationOptionPickerViewModel;

/* renamed from: X.403  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass403 extends C37171lc {
    public final int A00;
    public final LocationOptionPickerViewModel A01;

    public AnonymousClass403(AnonymousClass39o r1, LocationOptionPickerViewModel locationOptionPickerViewModel, int i) {
        super(r1);
        this.A00 = i;
        this.A01 = locationOptionPickerViewModel;
    }
}
