package X;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* renamed from: X.04r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC009304r {
    @Deprecated
    public void A00(View view, View view2, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4) {
    }

    public boolean A02(Rect rect, View view, CoordinatorLayout coordinatorLayout) {
        return false;
    }

    public boolean A03(Rect rect, View view, CoordinatorLayout coordinatorLayout, boolean z) {
        return false;
    }

    @Deprecated
    public boolean A04(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i) {
        return false;
    }

    public boolean A05(View view, View view2, CoordinatorLayout coordinatorLayout) {
        return false;
    }

    public boolean A06(View view, View view2, CoordinatorLayout coordinatorLayout) {
        return false;
    }

    public boolean A07(View view, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4) {
        return false;
    }

    public void A09(Parcelable parcelable, View view, CoordinatorLayout coordinatorLayout) {
    }

    public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        return false;
    }

    public boolean A0D(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
        return false;
    }

    public boolean A0F(View view, View view2, CoordinatorLayout coordinatorLayout, float f, float f2) {
        return false;
    }

    public boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
        return false;
    }

    public void A0H(AnonymousClass0B5 r1) {
    }

    public AbstractC009304r() {
    }

    public AbstractC009304r(Context context, AttributeSet attributeSet) {
    }

    public void A01(View view, View view2, CoordinatorLayout coordinatorLayout, int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            A00(view, view2, coordinatorLayout, i, i2, i3, i4);
        }
    }

    public Parcelable A08(View view, CoordinatorLayout coordinatorLayout) {
        return View.BaseSavedState.EMPTY_STATE;
    }

    public void A0A(View view, View view2, CoordinatorLayout coordinatorLayout, int i) {
    }

    public void A0B(View view, View view2, CoordinatorLayout coordinatorLayout, int[] iArr, int i, int i2, int i3) {
    }

    public boolean A0E(View view, View view2, View view3, CoordinatorLayout coordinatorLayout, int i, int i2) {
        if (i2 == 0) {
            return A04(view, view2, view3, coordinatorLayout, i);
        }
        return false;
    }
}
