package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;

/* renamed from: X.0lS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14450lS {
    public int A00 = 1;
    public int A01 = 0;
    public long A02 = -1;
    public long A03 = -1;
    public C39741qT A04;
    public C14520lZ A05;
    public Boolean A06;
    public Integer A07;
    public Integer A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public final long A0E;
    public final C14430lQ A0F;
    public final C14420lP A0G;
    public final AbstractC14440lR A0H;
    public final boolean A0I;

    public C14450lS(C14430lQ r3, C14420lP r4, AbstractC14440lR r5, boolean z) {
        this.A0H = r5;
        this.A0G = r4;
        this.A0F = r3;
        this.A0I = z;
        this.A0E = SystemClock.uptimeMillis();
    }

    public synchronized int A00() {
        return this.A01;
    }

    public long A01() {
        Long l;
        Long l2;
        C14520lZ r1 = this.A05;
        if (r1 == null || (l = r1.A08) == null) {
            return 0;
        }
        long longValue = l.longValue();
        C39861qf r0 = r1.A01;
        return (r0 == null || (l2 = r0.A02) == null) ? longValue : longValue - l2.longValue();
    }

    public final void A02() {
        C14430lQ r7 = this.A0F;
        long j = this.A02;
        r7.A07 += j - this.A0E;
        long j2 = this.A03;
        if (j2 != -1) {
            r7.A08 += j - j2;
        }
        this.A0H.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(this, 1));
    }

    public synchronized void A03() {
        this.A03 = SystemClock.uptimeMillis();
    }

    public synchronized void A04(byte b, int i, boolean z) {
        this.A07 = Integer.valueOf(C33761f2.A00(b, i, z));
        this.A0B = z;
    }

    public synchronized void A05(int i) {
        this.A01 = i;
    }

    public synchronized void A06(C39741qT r2) {
        this.A04 = r2;
    }

    public synchronized void A07(C14520lZ r2) {
        this.A05 = r2;
    }
}
