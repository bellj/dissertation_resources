package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.3bD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70393bD implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass19P A01;
    public final /* synthetic */ AnonymousClass1KS A02;
    public final /* synthetic */ AnonymousClass1AB A03;
    public final /* synthetic */ StickerView A04;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70393bD(AnonymousClass19P r1, AnonymousClass1KS r2, AnonymousClass1AB r3, StickerView stickerView, int i) {
        this.A01 = r1;
        this.A04 = stickerView;
        this.A03 = r3;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r12) {
        if (bitmap == null || bitmap.getWidth() <= 0 || bitmap.getHeight() <= 0) {
            AnonymousClass1AB r0 = this.A03;
            AnonymousClass1KS r2 = this.A02;
            StickerView stickerView = this.A04;
            int i = this.A00;
            r0.A04(stickerView, r2, null, 1, i, i, false, false);
            return;
        }
        this.A04.setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A04.setImageResource(R.drawable.sticker_error_in_conversation);
    }
}
