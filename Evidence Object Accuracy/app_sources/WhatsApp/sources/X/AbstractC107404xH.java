package X;

/* renamed from: X.4xH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC107404xH implements AnonymousClass5YX {
    public final String A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AbstractC107404xH(String str) {
        this.A00 = str;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A00;
    }
}
