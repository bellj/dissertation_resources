package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133826Cg implements AbstractC1311061h {
    public final /* synthetic */ C134006Cy A00;

    public C133826Cg(C134006Cy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC1311061h
    public void A9z() {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00.A00;
        indiaUpiSendPaymentActivity.A2q();
        indiaUpiSendPaymentActivity.A2g(1);
    }

    @Override // X.AbstractC136476Mr
    public void AaH() {
        PaymentView paymentView = ((AbstractActivityC121525iS) this.A00.A00).A0W;
        if (paymentView != null) {
            paymentView.A04();
        }
    }

    @Override // X.AbstractC136476Mr
    public void AaN() {
        this.A00.A00.AaN();
    }

    @Override // X.AbstractC136476Mr
    public void AaQ() {
        PaymentView paymentView = ((AbstractActivityC121525iS) this.A00.A00).A0W;
        if (paymentView != null) {
            paymentView.A05();
        }
    }
}
