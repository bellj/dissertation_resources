package X;

import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;
import java.util.List;
import java.util.Map;

/* renamed from: X.27M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27M extends SurfaceView implements AnonymousClass27N, AnonymousClass004 {
    public int A00;
    public int A01;
    public Camera.Size A02;
    public Camera A03;
    public Handler A04;
    public Handler A05;
    public HandlerThread A06;
    public HandlerThread A07;
    public AnonymousClass27Z A08;
    public AnonymousClass2P7 A09;
    public List A0A;
    public Map A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public final Camera.AutoFocusCallback A0F;
    public final Camera.PreviewCallback A0G;
    public final Handler A0H;
    public final SurfaceHolder.Callback A0I;
    public final SurfaceHolder A0J;
    public final AnonymousClass2Ka A0K;
    public final Runnable A0L;

    public AnonymousClass27M(Context context) {
        super(context, null, 0);
        if (!this.A0C) {
            this.A0C = true;
            generatedComponent();
        }
        this.A0H = new Handler(Looper.getMainLooper());
        this.A0K = new AnonymousClass2Ka();
        AnonymousClass3MU r2 = new AnonymousClass3MU(this);
        this.A0I = r2;
        this.A0G = new Camera.PreviewCallback() { // from class: X.4hp
            @Override // android.hardware.Camera.PreviewCallback
            public final void onPreviewFrame(byte[] bArr, Camera camera) {
                AnonymousClass27M r3 = AnonymousClass27M.this;
                r3.A05.post(new RunnableBRunnable0Shape7S0200000_I0_7(r3, 27, bArr));
            }
        };
        this.A0F = new C97874hm(this);
        this.A0L = new RunnableBRunnable0Shape10S0100000_I0_10(this, 10);
        SurfaceHolder holder = getHolder();
        this.A0J = holder;
        holder.addCallback(r2);
    }

    public final void A00() {
        if (this.A08 != null) {
            this.A0H.post(new RunnableBRunnable0Shape0S0101000_I0(this, 1, 18));
        }
    }

    @Override // X.AnonymousClass27N
    public boolean AK9() {
        Camera camera = this.A03;
        if (camera == null || !this.A0D) {
            return false;
        }
        boolean equals = "torch".equals(camera.getParameters().getFlashMode());
        this.A0E = equals;
        return equals;
    }

    @Override // X.AnonymousClass27N
    public void Aab() {
        Handler handler = this.A04;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape10S0100000_I0_10(this, 11));
        }
    }

    @Override // X.AnonymousClass27N
    public void Aao() {
        Handler handler = this.A04;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape10S0100000_I0_10(this, 14));
        }
    }

    @Override // X.AnonymousClass27N
    public boolean Aee() {
        return this.A0D;
    }

    @Override // X.AnonymousClass27N
    public void Af2() {
        String str;
        Camera camera = this.A03;
        if (camera != null && this.A0D) {
            this.A0E = !this.A0E;
            Camera.Parameters parameters = camera.getParameters();
            if (this.A0E) {
                str = "torch";
            } else {
                str = "off";
            }
            parameters.setFlashMode(str);
            camera.setParameters(parameters);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A09;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A09 = r0;
        }
        return r0.generatedComponent();
    }

    public Camera.Size getPreviewSize() {
        return this.A02;
    }

    @Override // android.view.SurfaceView, android.view.View
    public void onAttachedToWindow() {
        Log.i("qrview/onAttachedToWindow");
        super.onAttachedToWindow();
        HandlerThread handlerThread = new HandlerThread("QrScannerCamera");
        this.A06 = handlerThread;
        handlerThread.start();
        this.A04 = new Handler(this.A06.getLooper());
        HandlerThread handlerThread2 = new HandlerThread("QrScannerViewDecode");
        this.A07 = handlerThread2;
        handlerThread2.start();
        this.A05 = new Handler(this.A07.getLooper());
    }

    @Override // android.view.SurfaceView, android.view.View
    public void onDetachedFromWindow() {
        Log.i("qrview/onDetachedFromWindow");
        super.onDetachedFromWindow();
        HandlerThread handlerThread = this.A06;
        if (handlerThread != null) {
            handlerThread.quit();
        }
        HandlerThread handlerThread2 = this.A07;
        if (handlerThread2 != null) {
            handlerThread2.quit();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0035, code lost:
        if (r1 == 2) goto L_0x0037;
     */
    @Override // android.view.SurfaceView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass27M.onMeasure(int, int):void");
    }

    @Override // X.AnonymousClass27N
    public void setQrDecodeHints(Map map) {
        this.A0B = map;
    }

    @Override // X.AnonymousClass27N
    public void setQrScannerCallback(AnonymousClass27Z r1) {
        this.A08 = r1;
    }
}
