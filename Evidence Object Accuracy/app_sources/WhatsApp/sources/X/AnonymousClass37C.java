package X;

import android.widget.TextView;
import java.lang.ref.WeakReference;

/* renamed from: X.37C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37C extends AbstractC16350or {
    public final C15610nY A00;
    public final C15580nU A01;
    public final WeakReference A02;

    public AnonymousClass37C(TextView textView, C15610nY r3, C15580nU r4) {
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = C12970iu.A10(textView);
    }
}
