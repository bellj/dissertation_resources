package X;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import org.chromium.net.UrlRequest;

/* renamed from: X.4jc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99014jc implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        LatLng latLng = null;
        String str = null;
        String str2 = null;
        IBinder iBinder = null;
        float f = 0.0f;
        float f2 = 0.0f;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f3 = 0.0f;
        float f4 = 0.5f;
        float f5 = 0.0f;
        float f6 = 1.0f;
        float f7 = 0.0f;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    latLng = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
                    break;
                case 3:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 4:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case 5:
                    iBinder = C95664e9.A06(parcel, readInt);
                    break;
                case 6:
                    f = C95664e9.A00(parcel, readInt);
                    break;
                case 7:
                    f2 = C95664e9.A00(parcel, readInt);
                    break;
                case '\b':
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\t':
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\n':
                    z3 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 11:
                    f3 = C95664e9.A00(parcel, readInt);
                    break;
                case '\f':
                    f4 = C95664e9.A00(parcel, readInt);
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    f5 = C95664e9.A00(parcel, readInt);
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    f6 = C95664e9.A00(parcel, readInt);
                    break;
                case 15:
                    f7 = C95664e9.A00(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56482kx(iBinder, latLng, str, str2, f, f2, f3, f4, f5, f6, f7, z, z2, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C56482kx[i];
    }
}
