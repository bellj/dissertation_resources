package X;

/* renamed from: X.0Q8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Q8 {
    public int A00;
    public int A01 = -1;
    public int A02 = 0;
    public int A03 = -1;
    public C06440Tp A04;
    public C06440Tp A05;
    public C06440Tp A06;

    public AnonymousClass0Q8() {
        C06440Tp r0 = new C06440Tp(-1, -1);
        this.A06 = r0;
        r0.A02 = 0;
        r0.A03 = 0;
        r0.A04 = 0;
        this.A04 = r0;
        this.A05 = r0;
        this.A00 = 0;
    }

    public final String A00(C06440Tp r8, int i) {
        if (i < 0) {
            return "<snip>";
        }
        if (r8 == null) {
            return "\n{x}";
        }
        StringBuilder sb = new StringBuilder("\n");
        sb.append(r8.toString());
        String obj = sb.toString();
        C06440Tp[] r5 = r8.A0B;
        for (C06440Tp r1 : r5) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(A00(r1, i - 1).replace("\n", "\n-"));
            obj = sb2.toString();
        }
        return obj;
    }

    public void A01() {
        boolean z;
        C06440Tp[] r2;
        long nanoTime = System.nanoTime();
        try {
            int i = this.A01;
            if (i != -1) {
                int i2 = i - this.A02;
                int i3 = this.A03;
                int i4 = 1 << (i3 << 1);
                if (i3 == -1) {
                    i4 = 0;
                }
                int i5 = i2 + i4;
                if (this.A00 > i5) {
                    C06440Tp r22 = this.A04;
                    while (this.A00 > i5 && r22 != null) {
                        if (!(r22.A01() == null || r22.A04 == this.A03 || r22.A0C != 0)) {
                            synchronized (r22) {
                                if (r22.A06 != null) {
                                    r22.A03();
                                }
                                r22.A06 = null;
                            }
                            this.A00--;
                        }
                        r22 = r22.A09;
                    }
                    do {
                        z = false;
                        for (C06440Tp r7 = this.A04; r7 != null; r7 = r7.A09) {
                            for (int i6 = 0; i6 < 4; i6++) {
                                C06440Tp r5 = r7.A0B[i6];
                                if (r5 != null && r5.A0C == 0 && r5.A01() == null) {
                                    int i7 = 0;
                                    int i8 = 0;
                                    int i9 = -1;
                                    do {
                                        r2 = r5.A0B;
                                        if (r2[i7] != null) {
                                            i8++;
                                            i9 = i7;
                                        }
                                        i7++;
                                    } while (i7 < 4);
                                    if (i8 == 1) {
                                        r7.A0B[i6] = r2[i9];
                                    } else if (i8 == 0) {
                                        r7.A0B[i6] = null;
                                    }
                                    A03(r5);
                                    r5.A02();
                                    z = true;
                                }
                            }
                        }
                    } while (z);
                }
            }
        } finally {
            C06160Sk.A0O.A02(System.nanoTime() - nanoTime);
        }
    }

    public void A02(C06440Tp r11) {
        int i;
        C06440Tp r6;
        int i2;
        if (r11.A01() != null) {
            this.A00++;
        }
        C06440Tp r7 = this.A06;
        int i3 = r11.A02;
        int i4 = r11.A03;
        int i5 = r11.A04;
        while (true) {
            int i6 = (i5 - r7.A04) - 1;
            i = (((i3 >> i6) & 1) << 1) + ((i4 >> i6) & 1);
            C06440Tp[] r1 = r7.A0B;
            if (r1[i] == null || i5 <= (i2 = (r6 = r1[i]).A04)) {
                break;
            }
            int i7 = i5 - i2;
            if ((i3 >> i7) != r6.A02 || (i4 >> i7) != r6.A03) {
                break;
            }
            r7 = r7.A0B[i];
        }
        C06440Tp[] r8 = r7.A0B;
        C06440Tp r72 = r8[i];
        if (r72 == null) {
            r8[i] = r11;
        } else {
            int i8 = r72.A04;
            if (i5 < i8) {
                r8[i] = r11;
                int i9 = (i8 - i5) - 1;
                r11.A0B[(((r72.A02 >> i9) & 1) << 1) + ((r72.A03 >> i9) & 1)] = r72;
            } else if (i8 == i5 && r72.A02 == i3 && r72.A03 == i4) {
                A03(r72);
                System.arraycopy(r72.A0B, 0, r11.A0B, 0, 4);
                r8[i] = r11;
                if (r72.A01() != null) {
                    this.A00--;
                }
                r72.A02();
            } else {
                int i10 = i5 - i8;
                int i11 = i3 >> i10;
                int i12 = i4 >> i10;
                int i13 = r72.A02;
                int i14 = r72.A03;
                while (true) {
                    if (i11 == i13 && i12 == i14) {
                        break;
                    }
                    i11 >>= 1;
                    i12 >>= 1;
                    i13 >>= 1;
                    i14 >>= 1;
                    i8--;
                }
                C06440Tp r4 = new C06440Tp(-1, -1);
                r4.A02 = i11;
                r4.A03 = i12;
                r4.A04 = i8;
                r8[i] = r4;
                C06440Tp[] r5 = r4.A0B;
                int i15 = r72.A02;
                int i16 = r72.A03;
                int i17 = (r72.A04 - i8) - 1;
                r5[(((i15 >> i17) & 1) << 1) + ((i16 >> i17) & 1)] = r72;
                int i18 = r11.A02;
                int i19 = r11.A03;
                int i20 = (r11.A04 - i8) - 1;
                r5[(((i18 >> i20) & 1) << 1) + ((i19 >> i20) & 1)] = r11;
                A03(r4);
                C06440Tp r0 = this.A05;
                r0.A09 = r4;
                r4.A08 = r0;
                this.A05 = r4;
            }
        }
        A03(r11);
        C06440Tp r02 = this.A05;
        r02.A09 = r11;
        r11.A08 = r02;
        this.A05 = r11;
        A01();
    }

    public final void A03(C06440Tp r3) {
        if (r3 == this.A04) {
            this.A04 = r3.A09;
        }
        if (r3 == this.A05) {
            this.A05 = r3.A08;
        }
        C06440Tp r1 = r3.A09;
        if (r1 != null) {
            r1.A08 = r3.A08;
        }
        C06440Tp r0 = r3.A08;
        if (r0 != null) {
            r0.A09 = r1;
        }
        r3.A09 = null;
        r3.A08 = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(A00(this.A06, 10));
        sb.append("\n");
        return sb.toString();
    }
}
