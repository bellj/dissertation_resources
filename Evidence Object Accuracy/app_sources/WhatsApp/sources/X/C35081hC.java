package X;

import android.content.Context;
import android.content.pm.FeatureInfo;
import com.whatsapp.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.1hC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35081hC extends AbstractC35031h7 {
    public static Pattern A01;
    public float A00 = -1.0f;

    public final float A03(Context context) {
        float f = this.A00;
        if (f >= 0.0f) {
            return f;
        }
        this.A00 = 0.0f;
        FeatureInfo[] systemAvailableFeatures = context.getPackageManager().getSystemAvailableFeatures();
        int length = systemAvailableFeatures.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            try {
                String str = systemAvailableFeatures[i].name;
                if (str != null && str.startsWith("com.htc.software.Sense")) {
                    Pattern pattern = A01;
                    if (pattern == null) {
                        pattern = Pattern.compile("com\\.htc\\.software\\.Sense(\\d+(?:\\.\\d+)?).*");
                        A01 = pattern;
                    }
                    Matcher matcher = pattern.matcher(str);
                    if (!matcher.matches()) {
                        throw new NumberFormatException("could not find version");
                        break;
                    }
                    Float valueOf = Float.valueOf(Float.parseFloat(matcher.group(1)));
                    if (valueOf != null) {
                        this.A00 = valueOf.floatValue();
                        break;
                    }
                }
            } catch (NumberFormatException unused) {
            }
            i++;
        }
        StringBuilder sb = new StringBuilder("badger/htc/sense ");
        sb.append(this.A00);
        Log.i(sb.toString());
        return this.A00;
    }
}
