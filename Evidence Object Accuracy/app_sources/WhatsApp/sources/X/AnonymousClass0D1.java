package X;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0D1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0D1 extends AbstractC07280Xj {
    public int A00;
    public ArrayList A01;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0D1(X.AnonymousClass0QV r7, int r8) {
        /*
            r6 = this;
            r6.<init>(r7)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r6.A01 = r4
            r6.A01 = r8
            X.0QV r5 = r6.A03
            r3 = r5
            if (r8 != 0) goto L_0x002d
            X.0Q7 r2 = r5.A0W
        L_0x0013:
            X.0Q7 r1 = r2.A03
            if (r1 == 0) goto L_0x0033
            X.0Q7 r0 = r1.A03
            if (r0 != r2) goto L_0x0033
            X.0QV r2 = r1.A06
        L_0x001d:
            r1 = r5
            r5 = r2
            if (r2 == 0) goto L_0x0035
            r3 = r2
            if (r8 != 0) goto L_0x0027
            X.0Q7 r2 = r2.A0W
            goto L_0x0013
        L_0x0027:
            r0 = 1
            if (r8 != r0) goto L_0x0033
            X.0Q7 r2 = r2.A0Y
            goto L_0x0013
        L_0x002d:
            r0 = 1
            if (r8 != r0) goto L_0x0033
            X.0Q7 r2 = r5.A0Y
            goto L_0x0013
        L_0x0033:
            r2 = 0
            goto L_0x001d
        L_0x0035:
            r6.A03 = r3
        L_0x0037:
            if (r8 != 0) goto L_0x0055
            X.0D3 r0 = r1.A0c
        L_0x003b:
            r4.add(r0)
            if (r8 != 0) goto L_0x004f
            X.0Q7 r2 = r1.A0X
        L_0x0042:
            X.0Q7 r1 = r2.A03
            if (r1 == 0) goto L_0x005d
            X.0Q7 r0 = r1.A03
            if (r0 != r2) goto L_0x005d
            X.0QV r1 = r1.A06
            if (r1 == 0) goto L_0x005d
            goto L_0x0037
        L_0x004f:
            r0 = 1
            if (r8 != r0) goto L_0x005d
            X.0Q7 r2 = r1.A0S
            goto L_0x0042
        L_0x0055:
            r0 = 1
            if (r8 != r0) goto L_0x005b
            X.0D2 r0 = r1.A0d
            goto L_0x003b
        L_0x005b:
            r0 = 0
            goto L_0x003b
        L_0x005d:
            java.util.Iterator r3 = r4.iterator()
        L_0x0061:
            boolean r0 = r3.hasNext()
            r2 = 1
            if (r0 == 0) goto L_0x007e
            java.lang.Object r1 = r3.next()
            X.0Xj r1 = (X.AbstractC07280Xj) r1
            int r0 = r6.A01
            if (r0 != 0) goto L_0x0077
            X.0QV r0 = r1.A03
            r0.A0a = r6
            goto L_0x0061
        L_0x0077:
            if (r0 != r2) goto L_0x0061
            X.0QV r0 = r1.A03
            r0.A0b = r6
            goto L_0x0061
        L_0x007e:
            int r1 = r6.A01
            if (r1 != 0) goto L_0x00a1
            X.0QV r0 = r6.A03
            X.0QV r0 = r0.A0Z
            X.0Cw r0 = (X.C02560Cw) r0
            boolean r0 = r0.A0A
            if (r0 == 0) goto L_0x00a1
            int r0 = r4.size()
            if (r0 <= r2) goto L_0x00a1
            int r0 = r4.size()
            int r0 = r0 - r2
            java.lang.Object r0 = r4.get(r0)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r0 = r0.A03
            r6.A03 = r0
        L_0x00a1:
            X.0QV r0 = r6.A03
            if (r1 != 0) goto L_0x00aa
            int r0 = r0.A0A
        L_0x00a7:
            r6.A00 = r0
            return
        L_0x00aa:
            int r0 = r0.A0L
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0D1.<init>(X.0QV, int):void");
    }

    @Override // X.AbstractC07280Xj
    public long A05() {
        ArrayList arrayList = this.A01;
        int size = arrayList.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            AbstractC07280Xj r2 = (AbstractC07280Xj) arrayList.get(i);
            j = j + ((long) r2.A05.A00) + r2.A05() + ((long) r2.A04.A00);
        }
        return j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0087  */
    @Override // X.AbstractC07280Xj
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
            r10 = this;
            java.util.ArrayList r5 = r10.A01
            java.util.Iterator r1 = r5.iterator()
        L_0x0006:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0016
            java.lang.Object r0 = r1.next()
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            r0.A06()
            goto L_0x0006
        L_0x0016:
            int r1 = r5.size()
            r9 = 1
            if (r1 < r9) goto L_0x0095
            r8 = 0
            java.lang.Object r0 = r5.get(r8)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r2 = r0.A03
            int r1 = r1 - r9
            java.lang.Object r0 = r5.get(r1)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r1 = r0.A03
            int r0 = r10.A01
            if (r0 != 0) goto L_0x0099
            X.0Q7 r0 = r2.A0W
            X.0Q7 r7 = r1.A0X
            X.0Xi r6 = X.AbstractC07280Xj.A02(r0, r8)
            int r4 = r0.A00()
            r3 = 0
        L_0x0040:
            int r0 = r5.size()
            if (r3 >= r0) goto L_0x005a
            java.lang.Object r0 = r5.get(r3)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r2 = r0.A03
            int r1 = r2.A0N
            r0 = 8
            if (r1 == r0) goto L_0x0096
            X.0Q7 r0 = r2.A0W
            int r4 = r0.A00()
        L_0x005a:
            if (r6 == 0) goto L_0x0061
            X.0Xi r0 = r10.A05
            X.AbstractC07280Xj.A03(r0, r6, r4)
        L_0x0061:
            X.0Xi r4 = X.AbstractC07280Xj.A02(r7, r8)
            int r6 = r7.A00()
            int r3 = r5.size()
        L_0x006d:
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x0085
            java.lang.Object r0 = r5.get(r3)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r2 = r0.A03
            int r1 = r2.A0N
            r0 = 8
            if (r1 == r0) goto L_0x006d
            X.0Q7 r0 = r2.A0X
        L_0x0081:
            int r6 = r0.A00()
        L_0x0085:
            if (r4 == 0) goto L_0x008d
            X.0Xi r1 = r10.A04
            int r0 = -r6
            X.AbstractC07280Xj.A03(r1, r4, r0)
        L_0x008d:
            X.0Xi r0 = r10.A05
            r0.A03 = r10
            X.0Xi r0 = r10.A04
            r0.A03 = r10
        L_0x0095:
            return
        L_0x0096:
            int r3 = r3 + 1
            goto L_0x0040
        L_0x0099:
            X.0Q7 r0 = r2.A0Y
            X.0Q7 r7 = r1.A0S
            X.0Xi r6 = X.AbstractC07280Xj.A02(r0, r9)
            int r4 = r0.A00()
            r3 = 0
        L_0x00a6:
            int r0 = r5.size()
            if (r3 >= r0) goto L_0x00c0
            java.lang.Object r0 = r5.get(r3)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r2 = r0.A03
            int r1 = r2.A0N
            r0 = 8
            if (r1 == r0) goto L_0x00e8
            X.0Q7 r0 = r2.A0Y
            int r4 = r0.A00()
        L_0x00c0:
            if (r6 == 0) goto L_0x00c7
            X.0Xi r0 = r10.A05
            X.AbstractC07280Xj.A03(r0, r6, r4)
        L_0x00c7:
            X.0Xi r4 = X.AbstractC07280Xj.A02(r7, r9)
            int r6 = r7.A00()
            int r3 = r5.size()
        L_0x00d3:
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x0085
            java.lang.Object r0 = r5.get(r3)
            X.0Xj r0 = (X.AbstractC07280Xj) r0
            X.0QV r2 = r0.A03
            int r1 = r2.A0N
            r0 = 8
            if (r1 == r0) goto L_0x00d3
            X.0Q7 r0 = r2.A0S
            goto L_0x0081
        L_0x00e8:
            int r3 = r3 + 1
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0D1.A06():void");
    }

    @Override // X.AbstractC07280Xj
    public void A07() {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A01;
            if (i < arrayList.size()) {
                ((AbstractC07280Xj) arrayList.get(i)).A07();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC07280Xj
    public void A08() {
        this.A07 = null;
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            ((AbstractC07280Xj) it.next()).A08();
        }
    }

    @Override // X.AbstractC07280Xj
    public boolean A0B() {
        ArrayList arrayList = this.A01;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (!((AbstractC07280Xj) arrayList.get(i)).A0B()) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:162:0x024a, code lost:
        if (r19 != false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x02cb, code lost:
        if (r19 != false) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x0357, code lost:
        if (r19 != false) goto L_0x0328;
     */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0244  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0251  */
    @Override // X.AbstractC07280Xj, X.AbstractC11720gk
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AfH(X.AbstractC11720gk r25) {
        /*
        // Method dump skipped, instructions count: 871
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0D1.AfH(X.0gk):void");
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("ChainRun ");
        if (super.A01 == 0) {
            str = "horizontal : ";
        } else {
            str = "vertical : ";
        }
        sb.append(str);
        String obj = sb.toString();
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append("<");
            String obj2 = sb2.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj2);
            sb3.append(next);
            String obj3 = sb3.toString();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(obj3);
            sb4.append("> ");
            obj = sb4.toString();
        }
        return obj;
    }
}
