package X;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.4av  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93894av {
    public static final Pattern A01 = Pattern.compile("\\s*,\\s*");
    public final List A00;

    public C93894av(List list) {
        this.A00 = Collections.unmodifiableList(list);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[");
        A0k.append(C95094d8.A00(this.A00, ",", ""));
        return C12960it.A0d("]", A0k);
    }
}
