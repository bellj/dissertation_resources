package X;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0D6  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0D6 extends AnonymousClass0Q4 {
    public final JobInfo A00;
    public final JobScheduler A01;

    public AnonymousClass0D6(ComponentName componentName, Context context, int i) {
        super(componentName);
        A03(i);
        this.A00 = new JobInfo.Builder(i, this.A02).setOverrideDeadline(0).build();
        this.A01 = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
    }

    @Override // X.AnonymousClass0Q4
    public void A04(Intent intent) {
        this.A01.enqueue(this.A00, new JobWorkItem(intent));
    }
}
