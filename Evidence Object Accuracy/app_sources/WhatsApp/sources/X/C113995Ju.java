package X;

import com.whatsapp.util.Log;

/* renamed from: X.5Ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113995Ju extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C113995Ju() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = (Throwable) obj;
        C16700pc.A0E(th, 0);
        Log.e("AvatarProfilePhotoViewModel/init fetching poses", th);
        return AnonymousClass1WZ.A00;
    }
}
