package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.1YP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YP {
    public boolean A00;
    public final DeviceJid A01;

    public AnonymousClass1YP(DeviceJid deviceJid, boolean z) {
        this.A01 = deviceJid;
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass1YP r4 = (AnonymousClass1YP) obj;
            if (this.A00 == r4.A00) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + (this.A00 ? 1 : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ParticipantDevice{deviceJid=");
        sb.append(this.A01);
        sb.append(", sentSenderKey=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
