package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.5z4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130265z4 {
    public static boolean A00(C14850m9 r6, C16120oU r7, AbstractC21180x0 r8, String str) {
        String str2;
        boolean z;
        String A03 = r6.A03(1854);
        String str3 = "";
        if (A03 != null) {
            str2 = A03.trim();
        } else {
            str2 = str3;
        }
        String A032 = r6.A03(1855);
        if (A032 != null) {
            str3 = A032.trim();
        }
        HashSet A12 = C12970iu.A12();
        if (!TextUtils.isEmpty(str2)) {
            A12.addAll(Arrays.asList(str2.split(",")));
        }
        HashSet A122 = C12970iu.A12();
        if (!TextUtils.isEmpty(str3)) {
            A122.addAll(Arrays.asList(str3.split(",")));
        }
        if (A12.isEmpty() && A122.isEmpty()) {
            return false;
        }
        HashSet hashSet = new HashSet(A12);
        hashSet.addAll(A122);
        r8.ALE(185477621);
        r8.AKw(185477621, "categories", hashSet.toString());
        r8.AKw(185477621, "checkLocation", str);
        HashMap A11 = C12970iu.A11();
        if (!A12.isEmpty()) {
            Iterator it = A12.iterator();
            z = true;
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                try {
                    A11.put(A0x, Boolean.valueOf(A01(A0x)));
                } catch (Exception e) {
                    Log.e(e);
                    z = false;
                }
            }
            C119955fN r1 = new C119955fN();
            r1.A02 = str;
            r1.A01 = (Boolean) A11.get("su_exists");
            r1.A00 = (Boolean) A11.get("rw_paths");
            r7.A07(r1);
        } else {
            z = true;
        }
        Iterator it2 = A122.iterator();
        boolean z2 = false;
        while (it2.hasNext()) {
            String A0x2 = C12970iu.A0x(it2);
            if (!A11.containsKey(A0x2) || !Boolean.TRUE.equals(A11.get(A0x2))) {
                try {
                } catch (Exception e2) {
                    Log.e(e2);
                    z = false;
                }
                if (A01(A0x2)) {
                }
            }
            z2 = true;
        }
        short s = 576;
        if (z) {
            s = 575;
        }
        r8.AL7(185477621, s);
        return z2;
    }

    public static boolean A01(String str) {
        if (str.equals("rw_paths")) {
            return C130385zK.A00();
        }
        if (str.equals("su_exists")) {
            Process process = null;
            try {
                boolean z = true;
                Process exec = Runtime.getRuntime().exec(new String[]{"which", "su"});
                if (exec == null) {
                    return false;
                }
                try {
                    InputStreamReader inputStreamReader = new InputStreamReader(exec.getInputStream());
                    try {
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        if (bufferedReader.readLine() == null) {
                            z = false;
                        }
                        bufferedReader.close();
                        inputStreamReader.close();
                        exec.destroy();
                        return z;
                    } catch (Throwable th) {
                        try {
                            inputStreamReader.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (Exception unused2) {
                    exec.destroy();
                    return false;
                }
            } catch (Throwable unused3) {
                if (0 == 0) {
                    return false;
                }
                process.destroy();
                return false;
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("Unknown check category: ")));
        }
    }
}
