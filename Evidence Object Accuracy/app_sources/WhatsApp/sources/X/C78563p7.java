package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78563p7 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98954jW();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;

    public C78563p7() {
    }

    public C78563p7(int i, int i2, int i3, long j, int i4) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A04 = j;
        this.A03 = i4;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A07(parcel, 3, this.A01);
        C95654e8.A07(parcel, 4, this.A02);
        C95654e8.A08(parcel, 5, this.A04);
        C95654e8.A07(parcel, 6, this.A03);
        C95654e8.A06(parcel, A00);
    }
}
