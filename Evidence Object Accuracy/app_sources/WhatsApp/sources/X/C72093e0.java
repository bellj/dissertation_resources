package X;

import com.whatsapp.avatar.home.AvatarHomeViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3e0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72093e0 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarHomeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72093e0(AvatarHomeViewModel avatarHomeViewModel) {
        super(1);
        this.this$0 = avatarHomeViewModel;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Log.i("onConfirmDeleteAvatarClicked/success");
        this.this$0.A00.A0B(new C83903y5(true));
        return AnonymousClass1WZ.A00;
    }
}
