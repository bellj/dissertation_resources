package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0cQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09140cQ implements Runnable {
    public final /* synthetic */ BiometricFragment A00;

    public RunnableC09140cQ(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0EP r1 = this.A00.A01;
        AnonymousClass0PW r0 = r1.A04;
        if (r0 == null) {
            r0 = new C02450Ci(r1);
            r1.A04 = r0;
        }
        r0.A00();
    }
}
