package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0uS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC19650uS {
    public abstract AbstractC16960q2 A00();

    public abstract Object A01(AnonymousClass3II v, Object obj, Map map);

    public final Map A02(AnonymousClass3II r13, AbstractC16960q2 r14, Object obj, Map map) {
        AnonymousClass3II[] r5;
        AnonymousClass3II r0;
        AnonymousClass3II r02;
        C16700pc.A0E(r14, 1);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (!(r13 == null || (r5 = r13.A02) == null)) {
            int length = r5.length;
            int i = 0;
            while (i < length) {
                AnonymousClass3II r11 = r5[i];
                i++;
                Enum[] enumArr = (Enum[]) r14.A9u().getEnumConstants();
                if (enumArr != null) {
                    int length2 = enumArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        Enum r1 = enumArr[i2];
                        i2++;
                        String ACu = ((AnonymousClass5UX) r1).ACu();
                        String str = r11.A00;
                        if (C16700pc.A0O(ACu, str)) {
                            if (r1 != null) {
                                Object Aam = r14.Aam(r1, obj, map);
                                if ((Aam instanceof List) && (Aam = ((Collection) Aam).toArray(new Object[0])) == null) {
                                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                                } else if (Aam instanceof Object[]) {
                                    ArrayList arrayList = new ArrayList();
                                    Object[] objArr = (Object[]) Aam;
                                    int length3 = objArr.length;
                                    int i3 = 0;
                                    while (i3 < length3) {
                                        Object obj2 = objArr[i3];
                                        i3++;
                                        C16700pc.A0E(str, 0);
                                        Map map2 = r13.A01;
                                        if (map2 == null) {
                                            r0 = null;
                                        } else {
                                            r0 = (AnonymousClass3II) map2.get(str);
                                        }
                                        arrayList.add(A01(r0, obj2, map));
                                    }
                                    linkedHashMap.put(str, arrayList);
                                } else {
                                    C16700pc.A0E(str, 0);
                                    Map map3 = r13.A01;
                                    if (map3 == null) {
                                        r02 = null;
                                    } else {
                                        r02 = (AnonymousClass3II) map3.get(str);
                                    }
                                    linkedHashMap.put(str, A01(r02, Aam, map));
                                }
                            }
                        }
                    }
                }
                String str2 = r11.A00;
                Log.e(C16700pc.A08("Field not supported: ", str2));
                linkedHashMap.put(str2, null);
            }
        }
        return linkedHashMap;
    }
}
