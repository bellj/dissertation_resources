package X;

/* renamed from: X.4wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107064wj implements AnonymousClass5WZ {
    public int A00;
    public int A01;
    public final int A02;
    public final int A03;
    public final C95304dT A04;

    @Override // X.AnonymousClass5WZ
    public int AD3() {
        return -1;
    }

    public C107064wj(C76873mN r3) {
        C95304dT r1 = r3.A00;
        this.A04 = r1;
        this.A02 = C95304dT.A02(r1, 12) & 255;
        this.A03 = r1.A0E();
    }

    @Override // X.AnonymousClass5WZ
    public int AGN() {
        return this.A03;
    }

    @Override // X.AnonymousClass5WZ
    public int AZv() {
        int i = this.A02;
        if (i == 8) {
            return this.A04.A0C();
        }
        if (i == 16) {
            return this.A04.A0F();
        }
        int i2 = this.A01;
        this.A01 = i2 + 1;
        if (i2 % 2 != 0) {
            return this.A00 & 15;
        }
        int A0C = this.A04.A0C();
        this.A00 = A0C;
        return (A0C & 240) >> 4;
    }
}
