package X;

import com.whatsapp.R;

/* renamed from: X.6Af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133296Af implements AnonymousClass6MU {
    public final /* synthetic */ C118155bM A00;

    public C133296Af(C118155bM r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MU
    public void APo(C452120p r13) {
        C16700pc.A0E(r13, 0);
        C117305Zk.A1N("PaymentCheckoutOrderViewModel", C16700pc.A08("init/getMerchantStatus : failed. Error code = ", Integer.valueOf(r13.A00)));
        C118155bM r0 = this.A00;
        AnonymousClass016 r5 = r0.A03;
        C130725zs r6 = r0.A09;
        EnumC124535ph r4 = EnumC124535ph.A01;
        int A01 = C117315Zl.A01(r4, C125305r2.A00);
        int i = R.string.order_details_order_details_not_available_title;
        int i2 = R.string.order_details_order_details_not_available_content;
        if (A01 != 1) {
            i = R.string.error_notification_title;
            i2 = R.string.something_went_wrong;
        }
        r5.A0A(r6.A00(null, new C1315963j(r4, i, i2), null, null, null));
    }

    @Override // X.AnonymousClass6MU
    public void AX1(EnumC124545pi r9) {
        C16700pc.A0E(r9, 0);
        C118155bM r0 = this.A00;
        r0.A03.A0A(r0.A09.A00(null, null, r9, null, null));
    }
}
