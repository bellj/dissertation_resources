package X;

import android.view.View;

/* renamed from: X.3Tb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67833Tb implements AnonymousClass5T2 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public View A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;

    @Override // X.AnonymousClass5T2
    public boolean Abl(C14260l7 r6, Object obj, int i) {
        float f;
        float f2;
        if (i == 35) {
            float floatValue = ((Number) obj).floatValue();
            this.A04 = floatValue;
            View view = this.A08;
            if (view != null) {
                view.setScaleX(floatValue);
                return true;
            }
        } else if (i == 36) {
            float floatValue2 = ((Number) obj).floatValue();
            this.A05 = floatValue2;
            View view2 = this.A08;
            if (view2 != null) {
                view2.setScaleY(floatValue2);
                return true;
            }
        } else if (i == 38) {
            try {
                String str = (String) obj;
                if (str == null) {
                    f = 0.0f;
                } else {
                    f = AnonymousClass3JW.A01(str);
                }
                this.A06 = f;
                this.A0C = str.endsWith("%");
            } catch (AnonymousClass491 e) {
                C28691Op.A01("ViewTransformsExtensionBinderUtils", "Could not parse translation_x value. ", e);
            }
            View view3 = this.A08;
            if (view3 != null) {
                view3.setTranslationX(C12990iw.A01(this.A06, C12990iw.A02(view3), this.A0C ? 1 : 0));
                return true;
            }
        } else if (i == 138) {
            float floatValue3 = ((Number) obj).floatValue();
            this.A03 = floatValue3;
            View view4 = this.A08;
            if (view4 != null) {
                view4.setRotation(floatValue3);
                return true;
            }
        } else if (i != 141) {
            switch (i) {
                case 40:
                    try {
                        String str2 = (String) obj;
                        if (str2 == null) {
                            f2 = 0.0f;
                        } else {
                            f2 = AnonymousClass3JW.A01(str2);
                        }
                        this.A07 = f2;
                        this.A0D = str2.endsWith("%");
                    } catch (AnonymousClass491 e2) {
                        C28691Op.A01("ViewTransformsExtensionBinderUtils", "Could not parse translation_y value. ", e2);
                    }
                    View view5 = this.A08;
                    if (view5 != null) {
                        view5.setTranslationY(C12990iw.A01(this.A07, C12990iw.A03(view5), this.A0D ? 1 : 0));
                        break;
                    }
                    break;
                case 41:
                    try {
                        String str3 = (String) obj;
                        this.A01 = AnonymousClass3JW.A01(str3);
                        this.A0A = str3.endsWith("%");
                        this.A09 = true;
                    } catch (AnonymousClass491 e3) {
                        C28691Op.A01("ViewTransformsExtensionBinderUtils", "Could not parse pivot_x value. ", e3);
                    }
                    View view6 = this.A08;
                    if (view6 != null) {
                        view6.setPivotX(C12990iw.A01(this.A01, C12990iw.A02(view6), this.A0A ? 1 : 0));
                        return true;
                    }
                    break;
                case 42:
                    try {
                        String str4 = (String) obj;
                        this.A02 = AnonymousClass3JW.A01(str4);
                        this.A0B = str4.endsWith("%");
                        this.A09 = true;
                    } catch (AnonymousClass491 e4) {
                        C28691Op.A01("ViewTransformsExtensionBinderUtils", "Could not parse pivot_y value. ", e4);
                    }
                    View view7 = this.A08;
                    if (view7 != null) {
                        view7.setPivotY(C12990iw.A01(this.A02, C12990iw.A03(view7), this.A0B ? 1 : 0));
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        } else {
            float floatValue4 = ((Number) obj).floatValue();
            this.A00 = floatValue4;
            View view8 = this.A08;
            if (view8 != null) {
                view8.setAlpha(floatValue4);
                return true;
            }
        }
        return true;
    }
}
