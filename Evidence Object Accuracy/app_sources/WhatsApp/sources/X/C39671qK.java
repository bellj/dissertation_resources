package X;

import java.util.Comparator;

/* renamed from: X.1qK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39671qK implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        int i;
        AnonymousClass1qE r3 = (AnonymousClass1qE) obj;
        AnonymousClass1qE r4 = (AnonymousClass1qE) obj2;
        int i2 = 0;
        if (r3 == null) {
            i = 0;
        } else {
            i = r3.A01;
        }
        if (r4 != null) {
            i2 = r4.A01;
        }
        return i2 - i;
    }
}
