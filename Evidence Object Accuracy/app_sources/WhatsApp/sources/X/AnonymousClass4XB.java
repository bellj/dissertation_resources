package X;

import android.net.Uri;
import java.util.List;

/* renamed from: X.4XB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XB {
    public final Uri A00;
    public final Object A01;
    public final List A02;
    public final List A03;

    public /* synthetic */ AnonymousClass4XB(Uri uri, Object obj, List list, List list2) {
        this.A00 = uri;
        this.A02 = list;
        this.A03 = list2;
        this.A01 = obj;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass4XB)) {
            return false;
        }
        AnonymousClass4XB r4 = (AnonymousClass4XB) obj;
        if (!this.A00.equals(r4.A00) || !AnonymousClass3JZ.A0H(null, null) || !AnonymousClass3JZ.A0H(null, null) || !AnonymousClass3JZ.A0H(null, null) || !this.A02.equals(r4.A02) || !AnonymousClass3JZ.A0H(null, null) || !this.A03.equals(r4.A03) || !AnonymousClass3JZ.A0H(this.A01, r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((this.A00.hashCode() * 31) + 0) * 31) + 0) * 31) + 0) * 31) + this.A02.hashCode()) * 31) + 0) * 31) + this.A03.hashCode()) * 31;
        Object obj = this.A01;
        if (obj != null) {
            i = obj.hashCode();
        }
        return hashCode + i;
    }
}
