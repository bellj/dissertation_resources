package X;

/* renamed from: X.1e9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33481e9 {
    public final C15650ng A00;
    public final C20120vF A01;
    public final C15660nh A02;
    public final C16490p7 A03;
    public final C33461e7 A04;
    public final AnonymousClass19O A05;

    public C33481e9(C15650ng r1, C20120vF r2, C15660nh r3, C16490p7 r4, C33461e7 r5, AnonymousClass19O r6) {
        this.A00 = r1;
        this.A02 = r3;
        this.A05 = r6;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0072 A[Catch: all -> 0x00ce, TryCatch #3 {RuntimeException -> 0x00da, blocks: (B:3:0x0001, B:24:0x00ca, B:4:0x0007, B:23:0x00c7, B:5:0x0010, B:8:0x002e, B:10:0x0039, B:12:0x0042, B:13:0x005d, B:15:0x0065, B:18:0x0072, B:21:0x009c, B:22:0x00c2), top: B:29:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1M9 A00(int r16, int r17) {
        /*
            r15 = this;
            r4 = 0
            X.0p7 r0 = r15.A03     // Catch: RuntimeException -> 0x00da
            X.0on r14 = r0.get()     // Catch: RuntimeException -> 0x00da
            X.0nh r0 = r15.A02     // Catch: all -> 0x00d5
            r12 = 2
            r11 = r17
            android.database.Cursor r7 = X.C33471e8.A01(r0, r4, r11, r12)     // Catch: all -> 0x00d5
            X.0ng r1 = r15.A00     // Catch: all -> 0x00ce
            r0 = 0
            X.0pA r10 = new X.0pA     // Catch: all -> 0x00ce
            r10.<init>(r7, r1, r4, r0)     // Catch: all -> 0x00ce
            r1 = 0
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch: all -> 0x00ce
            r8.<init>()     // Catch: all -> 0x00ce
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch: all -> 0x00ce
            r9.<init>()     // Catch: all -> 0x00ce
            X.0vF r0 = r15.A01     // Catch: all -> 0x00ce
            boolean r0 = r0.A0A()     // Catch: all -> 0x00ce
            if (r0 == 0) goto L_0x006d
            java.lang.String r0 = "file_size"
        L_0x002e:
            int r13 = r7.getColumnIndexOrThrow(r0)     // Catch: all -> 0x00ce
            r3 = 0
            boolean r0 = r10.moveToFirst()     // Catch: all -> 0x00ce
            if (r0 == 0) goto L_0x0065
        L_0x0039:
            long r5 = r7.getLong(r13)     // Catch: all -> 0x00ce
            long r1 = r1 + r5
            r0 = r16
            if (r3 >= r0) goto L_0x005d
            X.19O r5 = r15.A05     // Catch: all -> 0x00ce
            X.0oV r0 = r10.A00()     // Catch: all -> 0x00ce
            X.AnonymousClass009.A05(r0)     // Catch: all -> 0x00ce
            X.1iM r0 = X.AnonymousClass3GG.A00(r0, r5)     // Catch: all -> 0x00ce
            r8.add(r0)     // Catch: all -> 0x00ce
            X.0oV r0 = r0.A03     // Catch: all -> 0x00ce
            long r5 = r0.A11     // Catch: all -> 0x00ce
            java.lang.Long r0 = java.lang.Long.valueOf(r5)     // Catch: all -> 0x00ce
            r9.add(r0)     // Catch: all -> 0x00ce
        L_0x005d:
            int r3 = r3 + 1
            boolean r0 = r10.moveToNext()     // Catch: all -> 0x00ce
            if (r0 != 0) goto L_0x0039
        L_0x0065:
            int r6 = r10.getCount()     // Catch: all -> 0x00ce
            r10.close()     // Catch: all -> 0x00ce
            goto L_0x0070
        L_0x006d:
            java.lang.String r0 = "media_size"
            goto L_0x002e
        L_0x0070:
            if (r11 != r12) goto L_0x0099
            X.1e7 r5 = r15.A04     // Catch: all -> 0x00ce
            X.0xc r10 = r5.A01     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_LARGE_FILES_MEDIA_SIZE"
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_LARGE_FILES_COUNT"
            java.lang.String r0 = java.lang.String.valueOf(r6)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_LARGE_FILES_ROW_IDS"
            java.lang.String r0 = ","
            java.lang.String r0 = android.text.TextUtils.join(r0, r9)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r0 = "STORAGE_USAGE_LARGE_FILES_CACHE_TIME"
            r5.A01(r0)     // Catch: all -> 0x00ce
            goto L_0x00c2
        L_0x0099:
            r0 = 1
            if (r11 != r0) goto L_0x00c2
            X.1e7 r5 = r15.A04     // Catch: all -> 0x00ce
            X.0xc r10 = r5.A01     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_FORWARDED_FILES_MEDIA_SIZE"
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_FORWARDED_FILES_COUNT"
            java.lang.String r0 = java.lang.String.valueOf(r6)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r3 = "STORAGE_USAGE_FORWARDED_FILES_ROW_IDS"
            java.lang.String r0 = ","
            java.lang.String r0 = android.text.TextUtils.join(r0, r9)     // Catch: all -> 0x00ce
            r10.A05(r3, r0)     // Catch: all -> 0x00ce
            java.lang.String r0 = "STORAGE_USAGE_FORWARDED_FILES_CACHE_TIME"
            r5.A01(r0)     // Catch: all -> 0x00ce
        L_0x00c2:
            X.1M9 r0 = new X.1M9     // Catch: all -> 0x00ce
            r0.<init>(r8, r6, r1)     // Catch: all -> 0x00ce
            r7.close()     // Catch: all -> 0x00d5
            r14.close()     // Catch: RuntimeException -> 0x00da
            return r0
        L_0x00ce:
            r0 = move-exception
            if (r7 == 0) goto L_0x00d4
            r7.close()     // Catch: all -> 0x00d4
        L_0x00d4:
            throw r0     // Catch: all -> 0x00d5
        L_0x00d5:
            r0 = move-exception
            r14.close()     // Catch: all -> 0x00d9
        L_0x00d9:
            throw r0     // Catch: RuntimeException -> 0x00da
        L_0x00da:
            r1 = move-exception
            java.lang.String r0 = "StorageUsageDbFetcher/fetchMediaFilesSummary"
            com.whatsapp.util.Log.e(r0, r1)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33481e9.A00(int, int):X.1M9");
    }
}
