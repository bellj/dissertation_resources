package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.ui.voip.MultiContactThumbnail;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2w7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60162w7 extends AbstractC92184Uw {
    public View A00;
    public ImageView A01;
    public TextView A02;
    public C15570nT A03;
    public C28801Pb A04;
    public final ImageView A05;
    public final C15450nH A06;
    public final CallsHistoryFragment A07;
    public final C15550nR A08;
    public final C15610nY A09;
    public final AnonymousClass28F A0A = new C1102654w(this);
    public final AnonymousClass1J1 A0B;
    public final AnonymousClass1J1 A0C;
    public final AnonymousClass018 A0D;
    public final C15600nX A0E;
    public final C21250x7 A0F;
    public final C14850m9 A0G;
    public final C20710wC A0H;
    public final MultiContactThumbnail A0I;
    public final AnonymousClass19Z A0J;

    public C60162w7(View view, C15570nT r5, C15450nH r6, CallsHistoryFragment callsHistoryFragment, C15550nR r8, C15610nY r9, AnonymousClass1J1 r10, AnonymousClass1J1 r11, AnonymousClass018 r12, C15600nX r13, C21250x7 r14, C14850m9 r15, C20710wC r16, AnonymousClass12F r17, AnonymousClass19Z r18) {
        this.A0G = r15;
        this.A06 = r6;
        this.A0F = r14;
        this.A07 = callsHistoryFragment;
        this.A0J = r18;
        this.A0D = r12;
        this.A08 = r8;
        this.A09 = r9;
        this.A0H = r16;
        this.A0E = r13;
        this.A00 = AnonymousClass028.A0D(view, R.id.joinable_call_log_root_view);
        this.A04 = new C28801Pb(view, r9, r17, (int) R.id.participant_names);
        this.A01 = C12970iu.A0K(view, R.id.call_type_icon);
        this.A0I = (MultiContactThumbnail) AnonymousClass028.A0D(view, R.id.multi_contact_photo);
        this.A05 = C12970iu.A0K(view, R.id.contact_photo);
        this.A02 = C12960it.A0I(view, R.id.ongoing_label);
        C27531Hw.A06(this.A04.A01);
        this.A0B = r10;
        this.A0C = r11;
        this.A03 = r5;
    }

    @Override // X.AbstractC92184Uw
    public void A00(int i) {
        String A00;
        int i2;
        C64503Fu r9 = ((C1101054g) super.A00).A00;
        C14850m9 r3 = this.A0G;
        AnonymousClass1YT A0g = C12990iw.A0g(r9.A03, 0);
        C15550nR r7 = this.A08;
        C15370n3 A01 = AnonymousClass1SF.A01(r7, this.A0F, r3, this.A0H, A0g);
        boolean A1W = C12960it.A1W(A01);
        MultiContactThumbnail multiContactThumbnail = this.A0I;
        int i3 = 8;
        multiContactThumbnail.setVisibility(C13010iy.A00(A1W ? 1 : 0));
        ImageView imageView = this.A05;
        if (A1W) {
            i3 = 0;
        }
        imageView.setVisibility(i3);
        List A0l = C12960it.A0l();
        if (A1W) {
            this.A0C.A06(imageView, A01);
        } else {
            A0l = CallsHistoryFragment.A01(this.A06, r7, this.A09, C12990iw.A0g(r9.A03, 0), this.A07.A0e);
            ArrayList A0l2 = C12960it.A0l();
            for (int i4 = 0; i4 < A0l.size(); i4++) {
                C15370n3 A0A = r7.A0A((AbstractC14640lm) A0l.get(i4));
                if (A0A != null && i4 < 3) {
                    A0l2.add(A0A);
                }
            }
            if (A0l2.size() == 0) {
                C15570nT r0 = this.A03;
                r0.A08();
                C27631Ih r02 = r0.A05;
                if (r02 != null) {
                    A0l2.add(r7.A0A(r02));
                }
            }
            if (A0l2.size() != 0) {
                multiContactThumbnail.A00(this.A0A, this.A0B, A0l2);
            }
        }
        View view = this.A00;
        Context context = view.getContext();
        C15610nY r03 = this.A09;
        if (A01 == null || (A00 = C15610nY.A01(r03, A01)) == null) {
            AnonymousClass2OC A02 = C65293Iy.A02(r7, r03, A0l, false);
            if (A02 == null) {
                A00 = null;
            } else {
                A00 = A02.A00(context);
            }
        }
        this.A04.A09(this.A07.A0e, A00);
        ImageView imageView2 = this.A01;
        boolean A05 = r9.A05();
        int i5 = R.drawable.ic_voip_calls_tab_voice_indicator;
        if (A05) {
            i5 = R.drawable.ic_voip_calls_tab_video_indicator;
        }
        imageView2.setImageResource(i5);
        C12960it.A13(view, this, r9, 23);
        boolean equals = AnonymousClass1SF.A0A(C12990iw.A0g(r9.A03, 0).A03().A02).equals(Voip.getCurrentCallId());
        Context context2 = view.getContext();
        boolean A052 = r9.A05();
        int i6 = R.string.voip_joinable_voice_call_log_description;
        if (A052) {
            i6 = R.string.voip_joinable_video_call_log_description;
        }
        String A0X = C12960it.A0X(context2, A00, new Object[1], 0, i6);
        if (equals) {
            i2 = R.string.voip_joinable_active_call_log_click_action_description;
        } else {
            boolean A053 = r9.A05();
            i2 = R.string.voip_joinable_voice_call_log_click_action_description;
            if (A053) {
                i2 = R.string.voip_joinable_video_call_log_click_action_description;
            }
        }
        C65293Iy.A03(view, A0X, context2.getString(i2));
        TextView textView = this.A02;
        int i7 = R.string.voip_joinable_ongoing;
        if (equals) {
            i7 = R.string.voip_joinable_ongoing_tap_to_return;
        }
        textView.setText(i7);
    }
}
