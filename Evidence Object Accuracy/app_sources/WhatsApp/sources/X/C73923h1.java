package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3h1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73923h1 extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass2AT A01;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public C73923h1(AnonymousClass2AT r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i;
        if (f == 1.0f) {
            i = this.A00;
        } else {
            i = (int) (f * ((float) this.A00));
        }
        AnonymousClass2AS r1 = this.A01.A00;
        r1.getLayoutParams().height = i;
        r1.requestLayout();
    }
}
