package X;

import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5dW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119315dW extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119315dW() {
        A0R(new AnonymousClass65L(this));
    }

    public static void A02(AnonymousClass2FL r1, AnonymousClass01J r2, C249317l r3, WaBloksActivity waBloksActivity) {
        ((ActivityC13790kL) waBloksActivity).A08 = r3;
        waBloksActivity.A08 = C18000rk.A00(r2.A1W);
        waBloksActivity.A05 = (C18840t8) r2.A1V.get();
        waBloksActivity.A0E = r2.A4e();
        waBloksActivity.A01 = (C48912Ik) r1.A19.get();
        waBloksActivity.A04 = (C17120qI) r2.ALs.get();
        waBloksActivity.A0D = r2.A4d();
        waBloksActivity.A03 = r2.A2U();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            WaBloksActivity waBloksActivity = (WaBloksActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, waBloksActivity);
            ActivityC13810kN.A10(A1M, waBloksActivity);
            A02(r3, A1M, ActivityC13790kL.A0S(r3, A1M, waBloksActivity, ActivityC13790kL.A0Y(A1M, waBloksActivity)), waBloksActivity);
        }
    }
}
