package X;

import android.location.Location;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.model.LatLng;
import java.util.AbstractList;

/* renamed from: X.1U5  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1U5 implements SafeParcelable {
    @Override // android.os.Parcelable
    public final int describeContents() {
        return 0;
    }

    public static LatLng A01(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static LatLng A02(AbstractList abstractList) {
        return new LatLng(((Number) abstractList.get(0)).doubleValue(), ((Number) abstractList.get(1)).doubleValue());
    }
}
