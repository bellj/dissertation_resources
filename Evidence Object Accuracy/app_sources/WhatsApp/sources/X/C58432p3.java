package X;

import android.content.Context;
import android.net.Uri;
import android.util.Pair;
import android.view.View;

/* renamed from: X.2p3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58432p3 extends C58272oQ {
    public final /* synthetic */ AnonymousClass1OY A00;
    public final /* synthetic */ AbstractC15340mz A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58432p3(Context context, AnonymousClass12Q r8, C14900mE r9, AnonymousClass1OY r10, AnonymousClass01d r11, AbstractC15340mz r12, String str) {
        super(context, r8, r9, r11, str);
        this.A00 = r10;
        this.A01 = r12;
    }

    @Override // X.C58272oQ, X.AbstractC116465Vn
    public void onClick(View view) {
        Pair pair;
        String str = this.A09;
        String schemeSpecificPart = Uri.parse(str).getSchemeSpecificPart();
        AnonymousClass1OY r2 = this.A00;
        if (r2.A0d.A00.get(schemeSpecificPart) != null) {
            pair = (Pair) r2.A0d.A00.get(schemeSpecificPart);
        } else if (r2.A0g != null) {
            return;
        } else {
            if (!r2.A0j.A0B()) {
                pair = null;
            } else {
                AnonymousClass38S r3 = new AnonymousClass38S(r2.A0J, r2.A0b, new AnonymousClass4KP(this), r2.A15, str, schemeSpecificPart);
                r2.A0g = r3;
                C12960it.A1E(r3, r2.A1P);
                return;
            }
        }
        AnonymousClass1OY.A0F(pair, r2, schemeSpecificPart, str, this.A01.A0z.A02);
    }
}
