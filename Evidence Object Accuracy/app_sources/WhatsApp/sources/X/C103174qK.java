package X;

import android.content.Context;
import com.whatsapp.location.LiveLocationPrivacyActivity;

/* renamed from: X.4qK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103174qK implements AbstractC009204q {
    public final /* synthetic */ LiveLocationPrivacyActivity A00;

    public C103174qK(LiveLocationPrivacyActivity liveLocationPrivacyActivity) {
        this.A00 = liveLocationPrivacyActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
