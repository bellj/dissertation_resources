package X;

import android.util.JsonWriter;
import java.io.Closeable;

/* renamed from: X.2FI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FI implements Closeable {
    public final /* synthetic */ AnonymousClass2FE A00;

    public AnonymousClass2FI(JsonWriter jsonWriter, AnonymousClass2FE r3) {
        this.A00 = r3;
        jsonWriter.name("files");
        jsonWriter.beginArray();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.A01.endArray();
    }
}
