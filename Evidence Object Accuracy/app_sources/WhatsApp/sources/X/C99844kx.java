package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99844kx implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass20C(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass20C[i];
    }
}
