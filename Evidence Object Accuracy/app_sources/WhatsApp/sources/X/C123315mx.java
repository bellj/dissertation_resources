package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123315mx extends AbstractC125975s7 {
    public int A00 = R.drawable.avatar_contact;
    public int A01 = 0;
    public int A02 = 8;
    public View.OnClickListener A03;
    public View.OnClickListener A04;
    public C15370n3 A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;

    public C123315mx() {
        super(203);
    }
}
