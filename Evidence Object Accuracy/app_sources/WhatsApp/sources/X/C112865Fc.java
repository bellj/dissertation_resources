package X;

import java.io.IOException;

/* renamed from: X.5Fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112865Fc implements AnonymousClass1TN, AnonymousClass5VQ {
    public C92754Xh A00;

    public C112865Fc(C92754Xh r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        try {
            return new AnonymousClass5MB(this.A00.A01());
        } catch (IllegalArgumentException e) {
            throw new AnonymousClass492(e.getMessage(), e);
        }
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU("unable to get DER object", e);
        } catch (IllegalArgumentException e2) {
            throw new AnonymousClass4CU("unable to get DER object", e2);
        }
    }
}
