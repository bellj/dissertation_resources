package X;

import android.os.PowerManager;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.0lc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC14550lc {
    public final AnonymousClass01H A00;
    public final Map A01 = new HashMap();

    public AbstractC14550lc(AnonymousClass01H r2) {
        this.A00 = r2;
    }

    public synchronized Runnable A00(Object obj) {
        RunnableC39081pG r0;
        r0 = (RunnableC39081pG) this.A01.get(obj);
        return r0 != null ? r0.A02 : null;
    }

    public synchronized Runnable A01(Object obj, Object obj2) {
        RunnableC39081pG r1;
        Runnable r14;
        C14410lO r12;
        PowerManager.WakeLock wakeLock;
        Map map = this.A01;
        r1 = (RunnableC39081pG) map.get(obj);
        if (r1 == null) {
            if (this instanceof C39091pH) {
                r14 = new C39271pZ((AbstractC15340mz) obj, (C39091pH) this);
            } else if (this instanceof C14540lb) {
                C14540lb r13 = (C14540lb) this;
                C14510lY r2 = (C14510lY) obj2;
                AnonymousClass009.A05(r2);
                int A00 = r2.A00();
                if (A00 != 1) {
                    r12 = r13.A00;
                    if (A00 == 2) {
                        C16590pI r0 = r12.A09;
                        C14830m7 r02 = r12.A08;
                        C14850m9 r03 = r12.A0C;
                        C14900mE r04 = r12.A03;
                        AbstractC15710nm r05 = r12.A01;
                        AbstractC14440lR r06 = r12.A0R;
                        C14330lG r07 = r12.A02;
                        C18790t3 r08 = r12.A07;
                        C15450nH r09 = r12.A06;
                        C22600zL r010 = r12.A0Q;
                        AnonymousClass14Q r011 = r12.A0G;
                        AnonymousClass155 r012 = r12.A0J;
                        AnonymousClass153 r15 = r12.A0M;
                        C15660nh r132 = r12.A0A;
                        AnonymousClass14R r122 = r12.A0N;
                        C241114g r11 = r12.A0B;
                        AnonymousClass14D r10 = r12.A0I;
                        C19940uv r9 = r12.A0F;
                        C19950uw r8 = r12.A0D;
                        C20110vE r7 = r12.A0E;
                        C16630pM r6 = r12.A0O;
                        AnonymousClass14C r013 = r12.A0L;
                        r14 = new C39261pY(r05, r07, r04, r12.A05, r09, r08, r02, r0, r132, r11, r03, r8, r7, r9, r011, r10, r012, (C37991nL) r2, r013, r15, r122, r6, r010, r06);
                    }
                } else {
                    r12 = r13.A00;
                }
                if (r12.A04().booleanValue()) {
                    C16590pI r014 = r12.A09;
                    C14830m7 r015 = r12.A08;
                    C14850m9 r016 = r12.A0C;
                    C14900mE r017 = r12.A03;
                    AbstractC15710nm r018 = r12.A01;
                    AbstractC14440lR r019 = r12.A0R;
                    C14330lG r020 = r12.A02;
                    C18790t3 r021 = r12.A07;
                    C21040wk r022 = r12.A00;
                    C15450nH r023 = r12.A06;
                    C22600zL r024 = r12.A0Q;
                    AnonymousClass14Q r025 = r12.A0G;
                    AnonymousClass155 r026 = r12.A0J;
                    AnonymousClass153 r152 = r12.A0M;
                    C15660nh r133 = r12.A0A;
                    AnonymousClass14R r123 = r12.A0N;
                    C241114g r112 = r12.A0B;
                    AnonymousClass14D r102 = r12.A0I;
                    C19940uv r92 = r12.A0F;
                    C19950uw r82 = r12.A0D;
                    C20110vE r72 = r12.A0E;
                    C16630pM r62 = r12.A0O;
                    r14 = new AnonymousClass1RL(r022, r018, r020, r017, r12.A05, r023, r021, r015, r014, r133, r112, r016, r82, r72, r92, r025, r102, r026, r2, r12.A0L, r152, r123, r62, r024, r019);
                } else {
                    C16590pI r027 = r12.A09;
                    C14830m7 r028 = r12.A08;
                    C14850m9 r029 = r12.A0C;
                    C14900mE r030 = r12.A03;
                    AbstractC15710nm r031 = r12.A01;
                    AbstractC14440lR r032 = r12.A0R;
                    C14330lG r033 = r12.A02;
                    C18790t3 r034 = r12.A07;
                    C15450nH r035 = r12.A06;
                    C22600zL r036 = r12.A0Q;
                    AnonymousClass14Q r037 = r12.A0G;
                    AnonymousClass155 r038 = r12.A0J;
                    AnonymousClass153 r153 = r12.A0M;
                    C15660nh r134 = r12.A0A;
                    AnonymousClass14R r124 = r12.A0N;
                    C241114g r113 = r12.A0B;
                    AnonymousClass14D r103 = r12.A0I;
                    C19940uv r93 = r12.A0F;
                    C19950uw r83 = r12.A0D;
                    C20110vE r73 = r12.A0E;
                    C16630pM r63 = r12.A0O;
                    r14 = new C14560ld(r031, r033, r030, r12.A05, r035, r034, r028, r027, r134, r113, r029, r83, r73, r93, r037, r103, r038, r2, r12.A0L, r153, r124, r63, r036, r032);
                }
            } else if (this instanceof C39101pI) {
                AbstractC39111pJ r16 = (AbstractC39111pJ) obj2;
                AnonymousClass009.A05(r16);
                C26821Ey r64 = ((C39101pI) this).A00;
                synchronized (r64) {
                    if (r64.A00 == null) {
                        PowerManager A0I = r64.A07.A0I();
                        if (A0I == null) {
                            Log.w("media-transcode-queue/get-transcode-wakelock pm=null");
                        } else {
                            r64.A00 = C39151pN.A00(A0I, "mediatranscode", 1);
                        }
                    }
                    wakeLock = r64.A00;
                }
                if (r16 instanceof C39121pK) {
                    C16590pI r039 = r64.A08;
                    Mp4Ops mp4Ops = r64.A03;
                    C14850m9 r135 = r64.A0D;
                    AbstractC15710nm r125 = r64.A01;
                    C14330lG r114 = r64.A02;
                    C15450nH r104 = r64.A05;
                    C239713s r94 = r64.A0F;
                    C19350ty r84 = r64.A0B;
                    C39121pK r17 = (C39121pK) r16;
                    r14 = new C39131pL(wakeLock, r125, r114, mp4Ops, r104, r64.A06, r039, r64.A09, r84, r135, r17, r94, r64.A0G, r64.A0N);
                } else if (r16 instanceof C39161pO) {
                    C16590pI r040 = r64.A08;
                    Mp4Ops mp4Ops2 = r64.A03;
                    C14850m9 r041 = r64.A0D;
                    AbstractC15710nm r042 = r64.A01;
                    C14330lG r043 = r64.A02;
                    C16120oU r044 = r64.A0E;
                    C15450nH r154 = r64.A05;
                    C22190yg r136 = r64.A0M;
                    C239713s r126 = r64.A0F;
                    C19350ty r115 = r64.A0B;
                    C15660nh r105 = r64.A0C;
                    C17050qB r95 = r64.A06;
                    AnonymousClass152 r85 = r64.A0N;
                    C39161pO r18 = (C39161pO) r16;
                    r14 = new C39171pP(wakeLock, r042, r043, mp4Ops2, r154, r95, r040, r64.A09, r115, r105, r041, r044, r18, r126, r64.A0G, r64.A0I, r64.A0L, r136, r85);
                } else if (r16 instanceof C39181pQ) {
                    r14 = new C39191pR(wakeLock, r64.A01, r64.A02, r64.A08, (C39181pQ) r16, r64.A0N);
                } else if (r16 instanceof C39201pS) {
                    AbstractC15710nm r127 = r64.A01;
                    C15450nH r116 = r64.A05;
                    C14950mJ r106 = r64.A0A;
                    C22190yg r96 = r64.A0M;
                    C39201pS r19 = (C39201pS) r16;
                    r14 = new C39211pT(wakeLock, r127, r116, r64.A07, r106, r64.A0C, r19, r64.A0H, r64.A0I, r96);
                } else if (r16 instanceof C39221pU) {
                    AbstractC15710nm r117 = r64.A01;
                    C16590pI r107 = r64.A08;
                    C14330lG r97 = r64.A02;
                    C26511Dt r86 = r64.A0K;
                    C26521Du r74 = r64.A0O;
                    r14 = new C39231pV(r117, r97, r64.A04, r107, (C39221pU) r16, r64.A0J, r86, r74);
                } else if (r16 instanceof C39241pW) {
                    r14 = new C39251pX(r64.A08, (C39241pW) r16);
                } else {
                    throw new AssertionError("Unreachable code");
                }
            } else if ((this instanceof C252418q) || (this instanceof AnonymousClass11X) || (this instanceof C20330va)) {
                r14 = (AbstractRunnableC14570le) obj2;
            } else {
                r14 = (Runnable) obj2;
                AnonymousClass009.A05(r14);
            }
            r1 = new RunnableC39081pG(this, obj, obj2, r14);
            map.put(obj, r1);
            ((ThreadPoolExecutor) this.A00.get()).execute(r1);
        }
        return r1.A02;
    }

    public void A02(AbstractC14590lg r4) {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet(this.A01.keySet());
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            r4.accept(it.next());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        A05(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A03(java.lang.Runnable r5) {
        /*
            r4 = this;
            r3 = r4
            monitor-enter(r3)
            java.util.Map r0 = r4.A01     // Catch: all -> 0x002a
            java.util.Set r1 = r0.keySet()     // Catch: all -> 0x002a
            java.util.HashSet r0 = new java.util.HashSet     // Catch: all -> 0x002a
            r0.<init>(r1)     // Catch: all -> 0x002a
            java.util.Iterator r2 = r0.iterator()     // Catch: all -> 0x002c
        L_0x0011:
            boolean r0 = r2.hasNext()     // Catch: all -> 0x002c
            if (r0 == 0) goto L_0x0028
            java.lang.Object r1 = r2.next()     // Catch: all -> 0x002c
            java.lang.Runnable r0 = r4.A00(r1)     // Catch: all -> 0x002c
            boolean r0 = r5.equals(r0)     // Catch: all -> 0x002c
            if (r0 == 0) goto L_0x0011
            r4.A05(r1)     // Catch: all -> 0x002c
        L_0x0028:
            monitor-exit(r3)
            return
        L_0x002a:
            r0 = move-exception
            throw r0     // Catch: all -> 0x002c
        L_0x002c:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14550lc.A03(java.lang.Runnable):void");
    }

    public final synchronized boolean A04(RunnableC39081pG r3, Object obj) {
        boolean z;
        Map map = this.A01;
        if (r3.equals(map.get(obj))) {
            map.remove(obj);
            ((ThreadPoolExecutor) this.A00.get()).remove(r3);
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized boolean A05(Object obj) {
        boolean z;
        Map map = this.A01;
        RunnableC39081pG r1 = (RunnableC39081pG) map.get(obj);
        if (r1 != null) {
            r1.cancel();
            ((ThreadPoolExecutor) this.A00.get()).remove(r1);
            map.remove(obj);
            z = true;
        } else {
            z = false;
        }
        return z;
    }
}
