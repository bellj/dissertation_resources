package X;

import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;

/* renamed from: X.455  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass455 extends AbstractC75713kI {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass455(View view, AbstractView$OnCreateContextMenuListenerC35851ir r4) {
        super(view, r4);
        this.A00 = r4;
        view.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 40));
    }
}
