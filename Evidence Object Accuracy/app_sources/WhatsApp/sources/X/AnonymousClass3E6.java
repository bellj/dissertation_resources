package X;

/* renamed from: X.3E6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E6 {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EI A01;

    public AnonymousClass3E6(AbstractC15710nm r3, AnonymousClass1V8 r4, AnonymousClass3BH r5) {
        AnonymousClass1V8.A01(r4, "iq");
        this.A01 = (AnonymousClass3EI) AnonymousClass3JT.A05(r4, new AbstractC116095Uc(r3, r5.A00) { // from class: X.58w
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r42) {
                return new AnonymousClass3EI(this.A00, r42, this.A01);
            }
        }, new String[0]);
        this.A00 = r4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E6.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3E6) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
