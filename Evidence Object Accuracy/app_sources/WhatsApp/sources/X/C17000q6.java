package X;

import android.app.Activity;
import android.os.Bundle;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0q6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17000q6 {
    public static final Map A04;
    public static final Set A05;
    public AbstractC17160qM A00;
    public C16120oU A01;
    public C17070qD A02;
    public C16660pY A03;

    static {
        HashMap hashMap = new HashMap();
        A04 = hashMap;
        HashSet hashSet = new HashSet();
        A05 = hashSet;
        hashMap.put("novi_hub", new C61012zG());
        hashMap.put("novi_login", new C61022zH());
        hashMap.put("novi_tpp_complete_transaction", new C61042zJ());
        hashMap.put("novi_report_transaction", new C61032zI());
        hashMap.put("novi_view_bank_detail", new C60962zB());
        hashMap.put("novi_view_card_detail", new AnonymousClass42H());
        hashMap.put("novi_view_transaction", new AnonymousClass42I());
        hashMap.put("novi_view_code", new C60972zC());
        hashMap.put("review_and_pay", new AnonymousClass42E());
        hashMap.put("review_order", new AnonymousClass42G());
        hashMap.put("address_message", new C61052zK());
        hashMap.put("galaxy_message", new C61062zL());
        hashMap.put("payment_method", new AnonymousClass42F());
        hashMap.put("wa_payment_transaction_details", new C61002zF());
        hashMap.put("wa_payment_learn_more", new C60982zD());
        hashMap.put("wa_payment_fbpin_reset", new C60992zE());
        hashSet.add("address_message");
        hashSet.add("galaxy_message");
    }

    public static final void A00(C16120oU r3, String str, int i) {
        C615130q r2 = new C615130q();
        r2.A01 = 4;
        r2.A03 = Integer.valueOf(i);
        r2.A02 = 0;
        StringBuilder sb = new StringBuilder("{  \"cta\":\"");
        sb.append(str);
        sb.append("\"}");
        r2.A05 = sb.toString();
        r3.A05(r2);
    }

    public void A01(Activity activity, AnonymousClass018 r18, AbstractC15340mz r19, AnonymousClass1Z8 r20) {
        String str;
        String str2;
        AnonymousClass009.A05(r20);
        String str3 = r20.A00;
        AbstractC64483Fs r7 = (AbstractC64483Fs) A04.get(str3);
        if (r7 == null) {
            str = "NativeFlowActionUtils/handleRequest -- can not recognize NFM action: ";
        } else if (!A05.contains(str3)) {
            C17070qD r2 = this.A02;
            C16120oU r4 = this.A01;
            Bundle bundle = new Bundle();
            bundle.putString("nfm_action", str3);
            Class AEb = r2.A02().AEb(bundle);
            if (AEb == null) {
                str = "NativeFlowActionUtils/handleRequest/processPaymentNativeFlow -- NFM action support class not found: ";
            } else {
                A00(r4, str3, C33761f2.A00(r19.A0y, r19.A08, C30041Vv.A0q(r19)));
                r7.A03(activity, r19.A0z, r20, AEb);
                return;
            }
        } else if (!(r7 instanceof AbstractC60952zA)) {
            str2 = "NativeFlowActionUtils/processCommerceNativeFlow. Base class for commerce action should be CommerceNativeFlowAction.";
            Log.e(str2);
        } else {
            A00(this.A01, str3, C33761f2.A00(r19.A0y, r19.A08, C30041Vv.A0q(r19)));
            ((AbstractC60952zA) r7).A06(activity, this.A00, r18, r20, this.A03, r19.A0z.A01, r19.A11);
            return;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.append(str3);
        str2 = sb.toString();
        Log.e(str2);
    }
}
