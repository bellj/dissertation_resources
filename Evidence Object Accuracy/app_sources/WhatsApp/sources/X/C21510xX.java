package X;

import android.database.Cursor;
import android.util.LruCache;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0xX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21510xX {
    public final LruCache A00 = new LruCache(3);
    public final AbstractC15710nm A01;
    public final C237712y A02;

    public C21510xX(AbstractC15710nm r3, C237712y r4) {
        this.A01 = r3;
        this.A02 = r4;
    }

    public AnonymousClass22F A00(UserJid userJid) {
        AnonymousClass22F r7;
        C237712y r3 = this.A02;
        AnonymousClass22F r2 = null;
        try {
            C16310on A01 = r3.A02.get();
            Cursor A09 = A01.A03.A09("SELECT jid_row_id,data,source,biz_count,has_user_sent_last_message,last_interaction FROM conversion_tuples WHERE jid_row_id = ? ", new String[]{String.valueOf(r3.A01.A01(userJid))});
            if (A09.moveToNext()) {
                String string = A09.getString(A09.getColumnIndexOrThrow("data"));
                String string2 = A09.getString(A09.getColumnIndexOrThrow("source"));
                int i = A09.getInt(A09.getColumnIndexOrThrow("biz_count"));
                boolean z = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("has_user_sent_last_message")) > 0) {
                    z = true;
                }
                r7 = new AnonymousClass22F(userJid, string, string2, i, A09.getLong(A09.getColumnIndexOrThrow("last_interaction")), z);
            } else {
                r7 = null;
            }
            A09.close();
            A01.close();
            r2 = r7;
        } catch (Exception e) {
            Log.e("conversionTupleMessageStore/getConversionTuple error accessing db", e);
        }
        LruCache lruCache = this.A00;
        if (r2 != null) {
            lruCache.put(userJid, r2);
            return r2;
        }
        lruCache.remove(userJid);
        return r2;
    }

    public void A01(UserJid userJid) {
        C237712y r5 = this.A02;
        C16310on A02 = r5.A02.A02();
        try {
            r5.A00(A02.A03, (int) r5.A01.A01(userJid));
            A02.close();
            this.A00.remove(userJid);
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
