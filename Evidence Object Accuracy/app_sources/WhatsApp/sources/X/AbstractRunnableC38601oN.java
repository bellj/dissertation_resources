package X;

import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.Executor;

/* renamed from: X.1oN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractRunnableC38601oN implements Runnable, AbstractC28931Po, AbstractC38611oO {
    public AbstractC116715Wn A00;
    public final C455322a A01;

    public AbstractRunnableC38601oN(Executor executor) {
        this.A01 = new C455322a(executor);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C28781Oz A00(X.AnonymousClass1RN r4) {
        /*
            r3 = this;
            boolean r0 = r3 instanceof X.C38581oL
            if (r0 != 0) goto L_0x0026
            boolean r0 = r3 instanceof X.AnonymousClass22Z
            if (r0 != 0) goto L_0x0046
            boolean r0 = r3 instanceof X.C41841uF
            if (r0 != 0) goto L_0x0046
            boolean r0 = r3 instanceof X.AnonymousClass330
            if (r0 != 0) goto L_0x0046
            boolean r0 = r3 instanceof X.C38631oQ
            if (r0 != 0) goto L_0x0031
            X.1Oz r2 = new X.1Oz
            r2.<init>()
        L_0x0019:
            int r1 = r4.A00
            r0 = 0
            if (r1 != 0) goto L_0x001f
            r0 = 1
        L_0x001f:
            r2.A0E(r0)
            r2.A07()
            return r2
        L_0x0026:
            r0 = r3
            X.1oL r0 = (X.C38581oL) r0
            X.1Oz r2 = new X.1Oz
            r2.<init>()
            X.1KS r1 = r0.A03
            goto L_0x003b
        L_0x0031:
            r0 = r3
            X.1oQ r0 = (X.C38631oQ) r0
            X.1Oz r2 = new X.1Oz
            r2.<init>()
            X.1KS r1 = r0.A02
        L_0x003b:
            int r0 = r1.A03
            r2.A0A(r0)
            int r0 = r1.A02
            r2.A08(r0)
            goto L_0x0019
        L_0x0046:
            X.1Oz r0 = new X.1Oz
            r0.<init>()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractRunnableC38601oN.A00(X.1RN):X.1Oz");
    }

    public AnonymousClass1t3 A01() {
        AnonymousClass1t3 r4;
        AbstractC116715Wn r2;
        try {
            Object A02 = A02();
            if (!(this instanceof AbstractC38591oM)) {
                AbstractC38641oR r0 = (AbstractC38641oR) this;
                AnonymousClass4TY r1 = (AnonymousClass4TY) A02;
                C14850m9 r5 = r0.A03;
                C91624Sl r7 = new C91624Sl(new AnonymousClass3YC(r0.A02, r5, r1.A03), r1.A01, r1.A02, r1.A05, r1.A06);
                C18790t3 r42 = r0.A01;
                r2 = new AnonymousClass3YB(r0.A00, r42, r5, r0.A04, r7, r1.A00, this, r0.A05);
            } else {
                AbstractC38591oM r02 = (AbstractC38591oM) this;
                C93644aV r12 = (C93644aV) A02;
                C14850m9 r52 = r02.A03;
                C90374Nq r8 = new C90374Nq(new AnonymousClass3YC(r02.A02, r52, r12.A03), r12.A02);
                C43161wW r72 = r12.A00;
                if (r72 == null) {
                    r72 = new C43161wW(r12.A01);
                }
                C18790t3 r43 = r02.A01;
                r2 = new CallableC71513d3(r02.A00, r43, r52, r02.A04, r72, r8, this, r02.A05);
            }
            synchronized (this) {
                if (this.A00 != null) {
                    Log.e("Attempt to run same download multiple times");
                    r4 = new AnonymousClass1t3(new AnonymousClass1RN(13));
                } else {
                    this.A00 = r2;
                    r4 = r2.A9A();
                }
            }
        } catch (AnonymousClass4C5 e) {
            r4 = new AnonymousClass1t3(new AnonymousClass1RN(e.downloadStatus));
        }
        AnonymousClass1RN r3 = r4.A00;
        int i = r3.A00;
        C455322a r13 = this.A01;
        if (i == 13) {
            r13.APP(r3.A03);
            return r4;
        }
        r13.APQ(r3, A00(r3));
        return r4;
    }

    public Object A02() {
        C37711ms r3;
        File file;
        C14370lK r1;
        if (this instanceof C38581oL) {
            C38581oL r0 = (C38581oL) this;
            r3 = new C37711ms(r0.A03.A0F);
            file = r0.A05;
            r1 = C14370lK.A0S;
        } else if (this instanceof AnonymousClass22Z) {
            AnonymousClass22Z r02 = (AnonymousClass22Z) this;
            String str = r02.A00.A05;
            AnonymousClass009.A05(str);
            r3 = new C37711ms(str);
            file = r02.A03;
            r1 = C14370lK.A0B;
        } else if (this instanceof C41841uF) {
            C41841uF r03 = (C41841uF) this;
            r03.A08 = true;
            C41831uE r32 = r03.A04;
            String str2 = r32.A04;
            C37781mz r4 = new C37781mz(null, "ppic", null, str2, null);
            File A00 = C17080qE.A00(r03.A02.A00, str2, r32.A05);
            r03.A07 = A00;
            return new C93644aV(C14370lK.A0B, r4, A00);
        } else if (this instanceof AnonymousClass330) {
            AnonymousClass330 r04 = (AnonymousClass330) this;
            C37751mw r42 = new C37751mw(r04.A04, r04.A03);
            File file2 = r04.A02;
            return new C93644aV(r04.A00, r04.A01, r42, file2);
        } else if (!(this instanceof C38631oQ)) {
            AnonymousClass1t2 r05 = (AnonymousClass1t2) this;
            C14370lK r12 = C14370lK.A0J;
            String str3 = r05.A04;
            String str4 = r05.A03;
            AnonymousClass009.A05(str3);
            AnonymousClass009.A05("md-app-state");
            return new AnonymousClass4TY(r12, new C37781mz(str3, "md-app-state", null, str4, null), r05.A01, r05.A00, r05.A02, r05.A05);
        } else {
            C38631oQ r06 = (C38631oQ) this;
            C002701f r13 = r06.A00;
            AnonymousClass1KS r33 = r06.A02;
            String str5 = r33.A0C;
            File A02 = r13.A02();
            StringBuilder sb = new StringBuilder();
            sb.append(str5.replace('/', '-'));
            sb.append(".tmp");
            File file3 = new File(A02, sb.toString());
            C14370lK r43 = C14370lK.A0S;
            String str6 = r33.A07;
            String str7 = r33.A05;
            AnonymousClass009.A05(str6);
            AnonymousClass009.A05("sticker");
            return new AnonymousClass4TY(r43, new C37781mz(str6, "sticker", null, str7, null), file3, r06.A04, r33.A0A, r33.A0C);
        }
        return new C93644aV(r1, r3, file);
    }

    public void A03() {
        AbstractC116715Wn r0;
        synchronized (this) {
            r0 = this.A00;
        }
        if (r0 != null) {
            r0.cancel();
        }
    }

    @Override // X.AbstractC28931Po
    public void A5g(AbstractC28771Oy r5) {
        C455322a r3 = this.A01;
        C14620lj r1 = r3.A01;
        AnonymousClass5AC r0 = new AbstractC14590lg() { // from class: X.5AC
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AbstractC28771Oy.this.APP(C12970iu.A1Y(obj));
            }
        };
        Executor executor = r3.A03;
        r1.A03(r0, executor);
        r3.A02.A03(new AbstractC14590lg() { // from class: X.5AB
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                Pair pair = (Pair) obj;
                AbstractC28771Oy.this.APQ((AnonymousClass1RN) pair.first, (C28781Oz) pair.second);
            }
        }, executor);
        r3.A00.A03(new AbstractC14590lg() { // from class: X.5AD
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AbstractC28771Oy.this.APN(C12980iv.A0G(obj));
            }
        }, executor);
    }

    @Override // X.AbstractC28931Po
    public void A76(boolean z) {
        String str;
        if (!(this instanceof C38631oQ)) {
            if (this instanceof C38581oL) {
                str = "AvocadoStickerBitmapNetworkFetcher/StickerDownload/cancelMediaDownload attempted to cancel download";
            }
            A03();
        }
        str = "StickerBitmapNetworkFetcher/StickerDownload/cancelMediaDownload attempted to cancel download";
        Log.i(str);
        A03();
    }

    @Override // X.AbstractC38611oO
    public void APO(long j) {
        this.A01.APN(j);
    }

    @Override // java.lang.Runnable
    public void run() {
        A01();
    }
}
