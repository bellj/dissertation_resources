package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.rendercore.text.RCTextView;

/* renamed from: X.3SZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SZ implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        RCTextView rCTextView = (RCTextView) obj;
        if (obj3 != null) {
            C63303Bb r14 = (C63303Bb) obj3;
            CharSequence charSequence = r14.A04;
            Layout layout = r14.A02;
            float f = r14.A00;
            float f2 = r14.A01;
            C71163cU r0 = r14.A03;
            ColorStateList colorStateList = r0.A0P;
            int i = r0.A0M;
            int i2 = r0.A08;
            ImageSpan[] imageSpanArr = r14.A07;
            ClickableSpan[] clickableSpanArr = r14.A06;
            int i3 = r0.A09;
            rCTextView.A0B = charSequence;
            rCTextView.A0A = layout;
            rCTextView.A00 = f;
            rCTextView.A01 = f2;
            rCTextView.A02 = i2;
            rCTextView.A03 = i3;
            if (i != 0) {
                rCTextView.A07 = null;
                rCTextView.A04 = i;
            } else {
                rCTextView.A07 = colorStateList;
                rCTextView.A04 = colorStateList.getDefaultColor();
                Layout layout2 = rCTextView.A0A;
                if (layout2 != null) {
                    layout2.getPaint().setColor(rCTextView.A07.getColorForState(rCTextView.getDrawableState(), rCTextView.A04));
                }
            }
            rCTextView.A02(0, 0);
            if (imageSpanArr != null) {
                for (ImageSpan imageSpan : imageSpanArr) {
                    Drawable drawable = imageSpan.getDrawable();
                    drawable.setCallback(rCTextView);
                    drawable.setVisible(true, false);
                }
            }
            rCTextView.A0E = imageSpanArr;
            rCTextView.A0D = clickableSpanArr;
            rCTextView.invalidate();
            return;
        }
        throw C12990iw.A0m("Missing text layout context!");
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        View view;
        ViewParent parent;
        RCTextView rCTextView = (RCTextView) obj;
        rCTextView.A0B = null;
        rCTextView.A0A = null;
        rCTextView.A00 = 0.0f;
        rCTextView.A01 = 0.0f;
        rCTextView.A02 = 0;
        rCTextView.A03 = 0;
        rCTextView.A07 = null;
        rCTextView.A04 = 0;
        ImageSpan[] imageSpanArr = rCTextView.A0E;
        if (imageSpanArr != null) {
            int length = imageSpanArr.length;
            for (int i = 0; i < length; i++) {
                Drawable drawable = rCTextView.A0E[i].getDrawable();
                drawable.setCallback(null);
                drawable.setVisible(false, false);
            }
            rCTextView.A0E = null;
        }
        rCTextView.A0D = null;
        C53682er r1 = rCTextView.A0F;
        if (!(r1 == null || !r1.A07.isEnabled() || (parent = (view = r1.A06).getParent()) == null)) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain((int) EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            view.onInitializeAccessibilityEvent(obtain);
            C04250Ky.A00(obtain, 1);
            parent.requestSendAccessibilityEvent(view, obtain);
        }
        if (obj3 == null) {
            throw C12990iw.A0m("Missing text layout context!");
        }
    }
}
