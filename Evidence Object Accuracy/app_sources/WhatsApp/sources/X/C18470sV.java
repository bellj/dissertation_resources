package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0sV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18470sV {
    public final C14830m7 A00;
    public final C18460sU A01;
    public final C16490p7 A02;
    public final C21390xL A03;
    public final C242714w A04;
    public final Object A05 = new Object();
    public final Object A06 = new Object();
    public volatile ConcurrentHashMap A07;

    public C18470sV(C14830m7 r2, C18460sU r3, C16490p7 r4, C21390xL r5, C242714w r6) {
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r6;
    }

    public static String A00(List list) {
        Collections.sort(list, new Comparator() { // from class: X.1V3
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                AnonymousClass1V2 r6 = (AnonymousClass1V2) obj;
                AnonymousClass1V2 r7 = (AnonymousClass1V2) obj2;
                if (r6.A0C()) {
                    return -1;
                }
                if (r7.A0C()) {
                    return 1;
                }
                if (C15380n4.A0M(r6.A0A)) {
                    return -1;
                }
                if (!C15380n4.A0M(r7.A0A)) {
                    return -(r6.A04() > r7.A04() ? 1 : (r6.A04() == r7.A04() ? 0 : -1));
                }
                return 1;
            }
        });
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1V2 r7 = (AnonymousClass1V2) it.next();
                instance.update(r7.A0A.getRawString().getBytes());
                int A02 = r7.A02();
                instance.update(new byte[]{(byte) (A02 >> 24), (byte) (A02 >> 16), (byte) (A02 >> 8), (byte) A02});
                int A01 = r7.A01();
                instance.update(new byte[]{(byte) (A01 >> 24), (byte) (A01 >> 16), (byte) (A01 >> 8), (byte) A01});
                long A04 = r7.A04();
                instance.update(new byte[]{(byte) ((int) (A04 >> 56)), (byte) ((int) (A04 >> 48)), (byte) ((int) (A04 >> 40)), (byte) ((int) (A04 >> 32)), (byte) ((int) (A04 >> 24)), (byte) ((int) (A04 >> 16)), (byte) ((int) (A04 >> 8)), (byte) ((int) A04)});
                if (r7.A08() != null) {
                    instance.update(r7.A08().A0z.A01.getBytes());
                }
            }
            return Base64.encodeToString(instance.digest(), 2);
        } catch (NoSuchAlgorithmException unused) {
            return null;
        }
    }

    public static final void A01(ContentValues contentValues, AnonymousClass1V2 r3) {
        long j;
        long j2;
        long j3;
        long j4;
        contentValues.put("message_table_id", Long.valueOf(r3.A03()));
        synchronized (r3) {
            j = r3.A06;
        }
        contentValues.put("last_read_message_table_id", Long.valueOf(j));
        synchronized (r3) {
            j2 = r3.A07;
        }
        contentValues.put("last_read_receipt_sent_message_table_id", Long.valueOf(j2));
        synchronized (r3) {
            j3 = r3.A03;
        }
        contentValues.put("first_unread_message_table_id", Long.valueOf(j3));
        synchronized (r3) {
            j4 = r3.A02;
        }
        contentValues.put("autodownload_limit_message_table_id", Long.valueOf(j4));
        contentValues.put("timestamp", Long.valueOf(r3.A04()));
        contentValues.put("unseen_count", Integer.valueOf(r3.A02()));
        contentValues.put("total_count", Integer.valueOf(r3.A01()));
    }

    public static final void A02(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_");
        sb.append((z ? "status_list" : "status").toUpperCase(Locale.ROOT));
        sb.toString();
    }

    public final long A03(String str) {
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT timestamp FROM status WHERE jid_row_id=?", new String[]{str});
            if (A09.moveToNext()) {
                long j = A09.getLong(0);
                A09.close();
                A01.close();
                return j;
            }
            A09.close();
            A01.close();
            return 0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AnonymousClass1V2 A04(UserJid userJid) {
        A09();
        if (userJid == null) {
            return null;
        }
        return (AnonymousClass1V2) this.A07.get(userJid);
    }

    public List A05() {
        A09();
        ConcurrentHashMap concurrentHashMap = this.A07;
        ArrayList arrayList = new ArrayList(concurrentHashMap.size());
        for (AnonymousClass1V2 r1 : concurrentHashMap.values()) {
            if (!r1.A0D()) {
                arrayList.add(r1.A05());
            }
        }
        return arrayList;
    }

    public List A06() {
        String A02 = this.A03.A02("status_white_list");
        if (TextUtils.isEmpty(A02)) {
            return new ArrayList();
        }
        return C15380n4.A07(UserJid.class, Arrays.asList(A02.split(",")));
    }

    public List A07() {
        String A02 = this.A03.A02("status_black_list");
        if (TextUtils.isEmpty(A02)) {
            return new ArrayList();
        }
        return C15380n4.A07(UserJid.class, Arrays.asList(A02.split(",")));
    }

    public final ConcurrentHashMap A08(C16310on r26) {
        String str;
        UserJid userJid;
        boolean A0G = A0G();
        if (A0G) {
            str = "SELECT key_remote_jid, message_table_id, last_read_message_table_id, last_read_receipt_sent_message_table_id, first_unread_message_table_id, autodownload_limit_message_table_id, timestamp, unseen_count, total_count FROM status_list";
        } else {
            str = "SELECT jid_row_id, message_table_id, last_read_message_table_id, last_read_receipt_sent_message_table_id, first_unread_message_table_id, autodownload_limit_message_table_id, timestamp, unseen_count, total_count FROM status";
        }
        Cursor A09 = r26.A03.A09(str, null);
        try {
            ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
            if (A09 != null) {
                while (A09.moveToNext()) {
                    if (A0G) {
                        userJid = C15380n4.A02(A09.getString(0));
                    } else {
                        userJid = (UserJid) this.A01.A07(UserJid.class, A09.getLong(0));
                    }
                    if (userJid != null) {
                        AnonymousClass1V2 r8 = new AnonymousClass1V2(this.A00, userJid, A09.getInt(A09.getColumnIndexOrThrow("unseen_count")), A09.getInt(A09.getColumnIndexOrThrow("total_count")), A09.getLong(A09.getColumnIndexOrThrow("message_table_id")), A09.getLong(A09.getColumnIndexOrThrow("last_read_message_table_id")), A09.getLong(A09.getColumnIndexOrThrow("last_read_receipt_sent_message_table_id")), A09.getLong(A09.getColumnIndexOrThrow("first_unread_message_table_id")), A09.getLong(A09.getColumnIndexOrThrow("autodownload_limit_message_table_id")), A09.getLong(A09.getColumnIndexOrThrow("timestamp")));
                        concurrentHashMap.put(r8.A0A, r8);
                    }
                }
            }
            if (A09 != null) {
                A09.close();
            }
            if (A0G) {
                C21390xL r82 = this.A03;
                if (r82.A00("status_list_ready", 0) != 1) {
                    C18460sU r10 = this.A01;
                    if (r10.A0C()) {
                        C16310on A02 = this.A02.A02();
                        try {
                            C28181Ma r4 = new C28181Ma(false);
                            AnonymousClass1Lx A01 = A02.A01();
                            r4.A04("StatusStore/convertStatusListToV2");
                            for (AnonymousClass1V2 r11 : concurrentHashMap.values()) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("jid_row_id", Long.valueOf(r10.A01(r11.A0A)));
                                A01(contentValues, r11);
                                A02.A03.A02(contentValues, "status");
                            }
                            A02.A03.A01("status_list", null, null);
                            r82.A04("status_list_ready", 1);
                            A01.A00();
                            A01.close();
                            r4.A01();
                            A02.close();
                            return concurrentHashMap;
                        } catch (Throwable th) {
                            try {
                                A02.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                }
            }
            return concurrentHashMap;
        } catch (Throwable th2) {
            if (A09 != null) {
                try {
                    A09.close();
                } catch (Throwable unused2) {
                }
            }
            throw th2;
        }
    }

    public final void A09() {
        if (this.A07 == null) {
            C16310on A01 = this.A02.get();
            try {
                if (A0G()) {
                    AnonymousClass1Lx A012 = A01.A01();
                    synchronized (this.A06) {
                        if (this.A07 == null) {
                            this.A07 = A08(A01);
                        }
                    }
                    A012.A00();
                    A012.close();
                } else {
                    synchronized (this.A06) {
                        if (this.A07 == null) {
                            this.A07 = A08(A01);
                        }
                    }
                }
                A01.close();
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public final void A0A(ContentValues contentValues, UserJid userJid, AbstractC15340mz r11, boolean z, boolean z2) {
        String str;
        String str2;
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r4 = A02.A03;
            if (z) {
                str = "status_list";
                str2 = "key_remote_jid=?";
            } else {
                str = "status";
                str2 = "jid_row_id=?";
            }
            String[] A0J = A0J(userJid, z);
            A02("updateStatusesListForNewMessage/UPDATE", z);
            if (r4.A00(str, contentValues, str2, A0J) == 0) {
                if (z) {
                    contentValues.put("key_remote_jid", userJid.getRawString());
                } else {
                    contentValues.put("jid_row_id", Long.valueOf(this.A01.A01(userJid)));
                }
                A02("updateStatusesListForNewMessage/INSERT", z);
                long A022 = r4.A02(contentValues, str);
                if (z2 && this.A07.size() == 1) {
                    this.A03.A05("earliest_status_time", r11.A0I);
                }
                if (A022 == -1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("statusmsgstore/addmsg/statuslist/insert/failed gid=");
                    sb.append(userJid);
                    sb.append("; shouldUseDeprecatedTable=");
                    sb.append(z);
                    Log.e(sb.toString());
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0B(AnonymousClass1V2 r9, UserJid userJid) {
        String str;
        String str2;
        C16310on A02 = this.A02.A02();
        try {
            boolean A0G = A0G();
            ContentValues contentValues = new ContentValues(8);
            A01(contentValues, r9);
            C16330op r4 = A02.A03;
            if (A0G) {
                str = "status_list";
                str2 = "key_remote_jid=?";
            } else {
                str = "status";
                str2 = "jid_row_id=?";
            }
            String[] A0J = A0J(userJid, A0G);
            A02("updateStatus/UPDATE", A0G);
            if (r4.A00(str, contentValues, str2, A0J) == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("StatusStore/updateStatus/failed jid=");
                sb.append(userJid);
                sb.append("; shouldUseDeprecatedTable=");
                sb.append(A0G);
                Log.e(sb.toString());
            }
            A09();
            this.A07.put(userJid, r9);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0C(UserJid userJid) {
        String str;
        String str2;
        C16310on A02 = this.A02.A02();
        try {
            boolean A0G = A0G();
            C16330op r4 = A02.A03;
            if (A0G) {
                str = "status_list";
                str2 = "key_remote_jid=?";
            } else {
                str = "status";
                str2 = "jid_row_id=?";
            }
            String[] A0J = A0J(userJid, A0G);
            A02("deleteStatus/DELETE", A0G);
            r4.A01(str, str2, A0J);
            A09();
            this.A07.remove(userJid);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0D(UserJid userJid, int i, int i2) {
        String str;
        String str2;
        boolean A0G = A0G();
        C16310on A02 = this.A02.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("unseen_count", Integer.valueOf(i));
            contentValues.put("total_count", Integer.valueOf(i2));
            C16330op r4 = A02.A03;
            if (A0G) {
                str = "status_list";
                str2 = "key_remote_jid=?";
            } else {
                str = "status";
                str2 = "jid_row_id=?";
            }
            String[] A0J = A0J(userJid, A0G);
            A02("updateStatusCount/UPDATE", A0G);
            if (r4.A00(str, contentValues, str2, A0J) == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("StatusStore/updateStatusCount/update count failed jid=");
                sb.append(userJid);
                sb.append("; shouldUseDeprecatedTable=");
                sb.append(A0G);
                Log.e(sb.toString());
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0E(Collection collection, int i) {
        ArrayList A06;
        String join;
        String str;
        if (collection == null) {
            A06 = null;
        } else {
            A06 = C15380n4.A06(collection);
        }
        C21390xL r2 = this.A03;
        r2.A04("status_distribution", i);
        if (A06 != null) {
            if (i == 2) {
                join = TextUtils.join(",", A06);
                str = "status_black_list";
            } else if (i == 1) {
                join = TextUtils.join(",", A06);
                str = "status_white_list";
            } else {
                return;
            }
            r2.A06(str, join);
        }
    }

    public boolean A0F() {
        return this.A03.A02("status_distribution") != null;
    }

    public boolean A0G() {
        return this.A03.A00("status_list_ready", 0) == 0;
    }

    public boolean A0H(AbstractC15340mz r11) {
        int i;
        AnonymousClass009.A0A("isStatusExpired should be called for statuses only", C15380n4.A0N(r11.A0z.A00));
        if (C15380n4.A0M(r11.A0B())) {
            AnonymousClass1V1 A00 = this.A04.A00(r11);
            if (!A00.A00()) {
                StringBuilder sb = new StringBuilder("StatusStore/isStatusExpired/is new status psa/campaign id: ");
                sb.append(A00.A04);
                sb.append(", campaign first seen time: ");
                sb.append(A00.A01);
                sb.append(", campaign expiration time:");
                sb.append(A00.A00);
                Log.i(sb.toString());
                if (!A00.A01(this.A00.A00()) || A00.A04 == null) {
                    return false;
                }
                return true;
            }
            C21390xL r3 = this.A03;
            long A01 = r3.A01("status_psa_viewed_time", 0);
            long A012 = r3.A01("status_psa_exipration_time", 0);
            StringBuilder sb2 = new StringBuilder("StatusStore/isStatusExpired/is legacy status psa/psa seen ts: ");
            sb2.append(A01);
            sb2.append(", psa expire ts: ");
            sb2.append(A012);
            Log.i(sb2.toString());
            if (r11.A0I >= A01 || A012 == 0) {
                return false;
            }
            i = (A012 > this.A00.A00() ? 1 : (A012 == this.A00.A00() ? 0 : -1));
        } else {
            i = (r11.A0I > (this.A00.A00() - 86400000) ? 1 : (r11.A0I == (this.A00.A00() - 86400000) ? 0 : -1));
        }
        if (i < 0) {
            return true;
        }
        return false;
    }

    public boolean A0I(AbstractC15340mz r4) {
        if (!r4.A0z.A02) {
            AnonymousClass1V2 A04 = A04((UserJid) r4.A0B());
            if (A04 != null) {
                return A04.A0E(r4);
            }
            StringBuilder sb = new StringBuilder("statusmsgstore/isstatusunseen/no status for ");
            sb.append(r4.A0B());
            Log.w(sb.toString());
        }
        return false;
    }

    public final String[] A0J(UserJid userJid, boolean z) {
        String valueOf;
        String[] strArr = new String[1];
        if (z) {
            valueOf = C15380n4.A03(userJid);
        } else {
            valueOf = String.valueOf(userJid != null ? this.A01.A01(userJid) : -1);
        }
        strArr[0] = valueOf;
        return strArr;
    }
}
