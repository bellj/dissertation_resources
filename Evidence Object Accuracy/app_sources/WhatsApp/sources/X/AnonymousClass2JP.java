package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.2JP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JP {
    public static Drawable A00(Context context, Resources resources) {
        Bitmap bitmap;
        Point A00 = AbstractC15850o0.A00(context);
        try {
            InputStream openRawResource = resources.openRawResource(R.drawable.default_wallpaper);
            bitmap = C37501mV.A05(AbstractC15850o0.A01(A00, true), openRawResource).A02;
            if (openRawResource != null) {
                openRawResource.close();
            }
        } catch (IOException e) {
            Log.e("wallpaper/exception", e);
            bitmap = null;
        }
        if (bitmap != null) {
            bitmap.setDensity(0);
            return new BitmapDrawable(resources, bitmap);
        }
        Log.w("wallpaper/cannot decode default wallpaper");
        return null;
    }

    public static Drawable A01(Context context, Resources resources, File file) {
        if (file.exists()) {
            try {
                Bitmap bitmap = C37501mV.A04(AbstractC15850o0.A01(AbstractC15850o0.A00(context), true), file).A02;
                if (bitmap != null) {
                    return new BitmapDrawable(resources, bitmap);
                }
            } catch (OutOfMemoryError e) {
                Log.e(e);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x006e A[Catch: NameNotFoundException | OutOfMemoryError | RuntimeException -> 0x0084, TryCatch #0 {NameNotFoundException | OutOfMemoryError | RuntimeException -> 0x0084, blocks: (B:4:0x0010, B:7:0x001a, B:9:0x002d, B:13:0x003a, B:14:0x004a, B:18:0x005d, B:20:0x006e, B:23:0x0075, B:25:0x007b), top: B:32:0x0010 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable A02(android.content.Context r8, X.C14900mE r9, int r10, int r11, int r12) {
        /*
            r2 = 0
            r3 = 0
            android.content.pm.PackageManager r1 = r8.getPackageManager()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0082
            java.lang.String r0 = "com.whatsapp.wallpaper"
            android.content.res.Resources r0 = r1.getResourcesForApplication(r0)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0082
            android.graphics.drawable.Drawable r8 = r0.getDrawable(r10)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0082
            r0 = r8
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            android.graphics.Bitmap r4 = r0.getBitmap()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            if (r4 != 0) goto L_0x001a
            goto L_0x0072
        L_0x001a:
            int r0 = r4.getWidth()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r7 = (float) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r6 = (float) r11     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r7 = r7 / r6
            int r0 = r4.getHeight()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r1 = (float) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r0 = (float) r12     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r1 = r1 / r0
            r5 = 1
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x004a
            int r0 = r4.getWidth()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r0 = (float) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r0 = r0 / r1
            int r0 = (int) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            if (r0 <= 0) goto L_0x0073
            if (r12 <= 0) goto L_0x0073
            if (r11 <= 0) goto L_0x0073
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r4, r0, r12, r5)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            int r0 = r1.getWidth()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            int r0 = r0 - r11
            int r0 = r0 >> 1
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r1, r0, r2, r11, r12)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            goto L_0x006c
        L_0x004a:
            int r0 = r4.getHeight()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r1 = (float) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r1 = r1 * r6
            int r0 = r4.getWidth()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r0 = (float) r0     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            float r1 = r1 / r0
            int r0 = (int) r1     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            if (r0 <= 0) goto L_0x0073
            if (r12 <= 0) goto L_0x0073
            if (r11 <= 0) goto L_0x0073
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r4, r11, r0, r5)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            int r0 = r1.getHeight()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            int r0 = r0 - r12
            int r0 = r0 >> 1
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r1, r2, r0, r11, r12)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
        L_0x006c:
            if (r4 == r1) goto L_0x0073
            r1.recycle()     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            goto L_0x0073
        L_0x0072:
            r4 = 0
        L_0x0073:
            if (r4 == 0) goto L_0x007b
            android.graphics.drawable.BitmapDrawable r0 = new android.graphics.drawable.BitmapDrawable     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            r0.<init>(r3, r4)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            return r0
        L_0x007b:
            r0 = 2131888079(0x7f1207cf, float:1.9410783E38)
            r9.A05(r0, r2)     // Catch: NameNotFoundException | RuntimeException | OutOfMemoryError -> 0x0084
            return r8
        L_0x0082:
            r1 = move-exception
            goto L_0x0086
        L_0x0084:
            r1 = move-exception
            r3 = r8
        L_0x0086:
            java.lang.String r0 = "wallpaper/set-global-wallpaper"
            com.whatsapp.util.Log.e(r0, r1)
            r0 = 2131888079(0x7f1207cf, float:1.9410783E38)
            r9.A05(r0, r2)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2JP.A02(android.content.Context, X.0mE, int, int, int):android.graphics.drawable.Drawable");
    }

    public static void A03(Context context, Drawable drawable, int i) {
        drawable.setColorFilter(new PorterDuffColorFilter(C016907y.A06(AnonymousClass00T.A00(context, R.color.black), (int) ((((float) i) / 100.0f) * 255.0f)), PorterDuff.Mode.DARKEN));
    }
}
