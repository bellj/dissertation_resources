package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import com.google.android.material.chip.ChipGroup;
import com.whatsapp.R;

/* renamed from: X.34l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC621034l extends AbstractC74153hP {
    public AnonymousClass018 A00;
    public final ChipGroup A01;

    public AbstractC621034l(Context context) {
        super(context);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ChipGroup chipGroup = new ChipGroup(context, null);
        this.A01 = chipGroup;
        chipGroup.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        addView(chipGroup);
        boolean z = this instanceof C620634h;
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_token_list_vertical_margin);
        Resources resources = getResources();
        if (!z) {
            int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.search_item_horizontal_margin);
            setPadding(dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize);
        } else {
            int dimensionPixelSize3 = resources.getDimensionPixelSize(R.dimen.search_item_horizontal_margin);
            setPadding(dimensionPixelSize3, 0, dimensionPixelSize3, dimensionPixelSize);
        }
        chipGroup.setChipSpacing(getChipSpacingPx());
    }

    private int getChipSpacingPx() {
        return getResources().getDimensionPixelSize(R.dimen.search_token_list_yg_margin_all) << 1;
    }
}
