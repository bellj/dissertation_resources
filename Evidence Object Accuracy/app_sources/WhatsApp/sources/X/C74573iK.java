package X;

import java.util.List;

/* renamed from: X.3iK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74573iK extends AnonymousClass0Q0 {
    public List A00;
    public List A01;

    public C74573iK(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        return this.A01.get(i).equals(this.A00.get(i2));
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        AnonymousClass4OU r4 = (AnonymousClass4OU) this.A01.get(i);
        AnonymousClass4OU r3 = (AnonymousClass4OU) this.A00.get(i2);
        if (!r4.A01.equals(r3.A01)) {
            return false;
        }
        if ((r4 instanceof AnonymousClass47H) && (r3 instanceof AnonymousClass47H)) {
            return true;
        }
        if (!(r4 instanceof AnonymousClass47I) || !(r3 instanceof AnonymousClass47I)) {
            return false;
        }
        return true;
    }
}
