package X;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.businesscollection.view.activity.CollectionProductListActivity;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.2ue  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC59392ue extends AbstractActivityC83703xl {
    public int A00;
    public int A01;
    public RecyclerView A02;
    public C48882Ih A03;
    public AbstractC59092tx A04;
    public C25791Av A05;
    public C21770xx A06;
    public C53852fO A07;
    public AnonymousClass1FZ A08;
    public C19850um A09;
    public C25811Ax A0A;
    public AnonymousClass19Q A0B;
    public AnonymousClass19T A0C;
    public C37071lG A0D;
    public C53802fF A0E;
    public Button A0F;
    public C15550nR A0G;
    public C22700zV A0H;
    public C15610nY A0I;
    public AnonymousClass11G A0J;
    public UserJid A0K;
    public C19840ul A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public boolean A0Q;
    public final AnonymousClass4UV A0R = new C84453zK(this);
    public final AnonymousClass2SH A0S = new C59142u3(this);

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r3.A02.canScrollVertically(1) == false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.AbstractActivityC59392ue r3) {
        /*
            r0 = 2131365962(0x7f0a104a, float:1.8351804E38)
            android.view.View r2 = r3.findViewById(r0)
            X.2tx r0 = r3.A04
            java.util.List r0 = r0.A05
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x001b
            androidx.recyclerview.widget.RecyclerView r1 = r3.A02
            r0 = 1
            boolean r1 = r1.canScrollVertically(r0)
            r0 = 0
            if (r1 != 0) goto L_0x001d
        L_0x001b:
            r0 = 8
        L_0x001d:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC59392ue.A02(X.2ue):void");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        C44681zN r2;
        super.onCreate(bundle);
        setContentView(R.layout.collection_product_list);
        Intent intent = getIntent();
        this.A0K = ActivityC13790kL.A0V(intent, "cache_jid");
        String stringExtra = intent.getStringExtra("collection_id");
        AnonymousClass009.A05(stringExtra);
        this.A0N = stringExtra;
        String stringExtra2 = intent.getStringExtra("collection_name");
        AnonymousClass009.A05(stringExtra2);
        this.A0P = stringExtra2;
        this.A0O = intent.getStringExtra("collection_index");
        this.A00 = intent.getIntExtra("category_browsing_entry_point", -1);
        this.A01 = intent.getIntExtra("category_level", -1);
        if (!this.A0N.equals("catalog_products_all_items_collection_id")) {
            this.A0L.A01(774780089, "view_collection_details_tag", "CollectionProductListBaseActivity");
            boolean z = true;
            this.A0L.A05("view_collection_details_tag", "IsConsumer", !((ActivityC13790kL) this).A01.A0F(this.A0K));
            C19840ul r3 = this.A0L;
            if (this.A09.A04(this.A0K, this.A0N) == null) {
                z = false;
            }
            r3.A05("view_collection_details_tag", "Cached", z);
        }
        Button button = (Button) findViewById(R.id.view_cart);
        this.A0F = button;
        C12960it.A0z(button, this, 6);
        String str = this.A0P;
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            if (str != null) {
                A1U.A0I(str);
            }
        }
        this.A02 = (RecyclerView) findViewById(R.id.product_list);
        CollectionProductListActivity collectionProductListActivity = (CollectionProductListActivity) this;
        UserJid userJid = ((AbstractActivityC59392ue) collectionProductListActivity).A0K;
        String str2 = ((AbstractActivityC59392ue) collectionProductListActivity).A0O;
        C14900mE r15 = ((ActivityC13810kN) collectionProductListActivity).A05;
        C15570nT r14 = ((ActivityC13790kL) collectionProductListActivity).A01;
        AnonymousClass12P r12 = ((ActivityC13790kL) collectionProductListActivity).A00;
        AnonymousClass19T r11 = ((AbstractActivityC59392ue) collectionProductListActivity).A0C;
        C15550nR r10 = ((AbstractActivityC59392ue) collectionProductListActivity).A0G;
        C15610nY r9 = ((AbstractActivityC59392ue) collectionProductListActivity).A0I;
        AnonymousClass018 r8 = ((ActivityC13830kP) collectionProductListActivity).A01;
        ((AbstractActivityC59392ue) collectionProductListActivity).A04 = new C58992tm(r12, r15, r14, ((AbstractActivityC59392ue) collectionProductListActivity).A0B, r11, ((AbstractActivityC59392ue) collectionProductListActivity).A0D, new AnonymousClass5TV() { // from class: X.3VU
            @Override // X.AnonymousClass5TV
            public final void AUH(C44691zO r13, int i) {
                int i2;
                CollectionProductListActivity collectionProductListActivity2 = CollectionProductListActivity.this;
                if (((ActivityC13810kN) collectionProductListActivity2).A0C.A07(1514) && (i2 = ((AbstractActivityC59392ue) collectionProductListActivity2).A00) != -1) {
                    ((AbstractActivityC59392ue) collectionProductListActivity2).A0A.A00(((AbstractActivityC59392ue) collectionProductListActivity2).A0K, true, Integer.valueOf(i), Integer.valueOf(((AbstractActivityC59392ue) collectionProductListActivity2).A01), ((AbstractActivityC59392ue) collectionProductListActivity2).A0N, r13.A0D, i2, 3);
                }
            }
        }, new AnonymousClass3VY(collectionProductListActivity), r10, ((AbstractActivityC59392ue) collectionProductListActivity).A0H, r9, r8, userJid, str2);
        this.A02.setAdapter(this.A04);
        RecyclerView recyclerView = this.A02;
        recyclerView.A0W = new AbstractC11880h1() { // from class: X.4uU
            @Override // X.AbstractC11880h1
            public final void AYO(AnonymousClass03U r22) {
                if (r22 instanceof C59302uQ) {
                    ((C59302uQ) r22).A0A();
                }
            }
        };
        C12990iw.A1K(recyclerView);
        AnonymousClass04Y r1 = this.A02.A0R;
        if (r1 instanceof AbstractC008704k) {
            ((AbstractC008704k) r1).A00 = false;
        }
        this.A08.A03(this.A0S);
        this.A07 = (C53852fO) AnonymousClass3RX.A00(this, this.A03, this.A0K);
        this.A0E = (C53802fF) new AnonymousClass02A(new C105634uN(getApplication(), this.A0C, new AnonymousClass3EX(this.A06, this.A0B, this.A0K, ((ActivityC13830kP) this).A05), this.A0K), this).A00(C53802fF.class);
        this.A05.A03(this.A0R);
        C12960it.A18(this, this.A0E.A01, 25);
        C12960it.A18(this, this.A0E.A02.A02, 24);
        C12970iu.A1P(this, this.A0E.A02.A04, this.A04, 6);
        C53802fF r22 = this.A0E;
        UserJid userJid2 = this.A0K;
        String str3 = this.A0N;
        boolean A1V = C12980iv.A1V(this.A00, -1);
        AnonymousClass19T r7 = r22.A02;
        int i = r22.A00;
        boolean equals = str3.equals("catalog_products_all_items_collection_id");
        int A03 = C12980iv.A03(r7.A08.A0F(userJid2) ? 1 : 0) * 9;
        if (equals) {
            C19850um r0 = r7.A0C;
            r0.A0E(userJid2, A03);
            if (r0.A0J(userJid2)) {
                r7.A02.A0B(new C84613zb(userJid2, str3, true, true));
                A03 <<= 1;
            }
            r7.A05(userJid2, i, A03, true);
        } else {
            C19850um r5 = r7.A0C;
            synchronized (r5) {
                C44661zL r02 = (C44661zL) r5.A01.get(userJid2);
                if (!(r02 == null || (r2 = (C44681zN) r02.A04.get(str3)) == null)) {
                    r2.A00 = new C44711zQ(null, true);
                    List list = r2.A01.A04;
                    int size = list.size();
                    if (size > A03) {
                        for (int i2 = A03; i2 < size; i2++) {
                            list.remove(C12980iv.A0C(list));
                        }
                    }
                }
            }
            C44671zM A04 = r5.A04(userJid2, str3);
            if (A04 != null && !A04.A04.isEmpty()) {
                r7.A02.A0B(new C84613zb(userJid2, A04.A03, true, true));
                A03 <<= 1;
            }
            r7.A06(userJid2, str3, i, A03, A1V);
        }
        this.A02.A0n(new C54722hB(this));
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem A0P = ActivityC13790kL.A0P(menu);
        AbstractView$OnClickListenerC34281fs.A00(A0P.getActionView(), this, 25);
        TextView A0J = C12960it.A0J(A0P.getActionView(), R.id.cart_total_quantity);
        String str = this.A0M;
        if (str != null) {
            A0J.setText(str);
        }
        this.A07.A00.A05(this, new AnonymousClass02B(A0P, this) { // from class: X.3RD
            public final /* synthetic */ MenuItem A00;
            public final /* synthetic */ AbstractActivityC59392ue A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
                if (r3.A0M == null) goto L_0x000f;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r14) {
                /*
                    r13 = this;
                    X.2ue r3 = r13.A01
                    android.view.MenuItem r2 = r13.A00
                    boolean r0 = X.C12970iu.A1Y(r14)
                    if (r0 == 0) goto L_0x000f
                    java.lang.String r1 = r3.A0M
                    r0 = 1
                    if (r1 != 0) goto L_0x0010
                L_0x000f:
                    r0 = 0
                L_0x0010:
                    r2.setVisible(r0)
                    boolean r0 = r3.A0Q
                    if (r0 != 0) goto L_0x003b
                    r0 = 1
                    r3.A0Q = r0
                    X.19Q r1 = r3.A0B
                    r0 = 79
                    java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
                    com.whatsapp.jid.UserJid r2 = r3.A0K
                    java.lang.String r10 = r3.A0O
                    X.2fO r0 = r3.A07
                    X.016 r0 = r0.A00
                    java.lang.Object r4 = r0.A01()
                    java.lang.Boolean r4 = (java.lang.Boolean) r4
                    r12 = 41
                    r3 = 0
                    r7 = r3
                    r8 = r3
                    r9 = r3
                    r11 = r3
                    r6 = r3
                    r1.A01(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                L_0x003b:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3RD.ANq(java.lang.Object):void");
            }
        });
        this.A07.A05();
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A05.A04(this.A0R);
        this.A08.A04(this.A0S);
        this.A0D.A00();
        this.A0C.A04.A0B(Boolean.FALSE);
        this.A0L.A06("view_collection_details_tag", false);
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        this.A0E.A03.A00();
        super.onResume();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0Q = false;
    }
}
