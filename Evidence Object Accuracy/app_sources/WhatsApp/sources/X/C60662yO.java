package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.InteractiveMessageButton;
import com.whatsapp.conversation.conversationrow.InteractiveMessageView;

/* renamed from: X.2yO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60662yO extends C60852yk {
    public boolean A00;
    public final InteractiveMessageButton A01 = ((InteractiveMessageButton) AnonymousClass028.A0D(this, R.id.button));
    public final InteractiveMessageView A02;

    public C60662yO(Context context, AbstractC13890kV r5, AnonymousClass1X1 r6) {
        super(context, r5, r6);
        A0Z();
        InteractiveMessageView interactiveMessageView = (InteractiveMessageView) AnonymousClass028.A0D(this, R.id.interactive_view);
        this.A02 = interactiveMessageView;
        AnonymousClass1OY.A0R(this, interactiveMessageView, r6);
        AbstractC16130oV r2 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
        this.A02.A00(this, r2);
        this.A01.A00(this, ((AbstractC28551Oa) this).A0a, r2);
    }

    @Override // X.AbstractC60612yJ, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
            AnonymousClass1OY.A0J(A07, A08, this);
        }
    }

    @Override // X.C60852yk, X.AnonymousClass1OY
    public void A0s() {
        super.A0s();
        AbstractC16130oV r2 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
        this.A02.A00(this, r2);
        this.A01.A00(this, ((AbstractC28551Oa) this).A0a, r2);
    }

    @Override // X.C60852yk, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_video_interactive_left;
    }

    @Override // X.C60852yk, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_video_interactive_left;
    }

    @Override // X.C60852yk, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_video_interactive_right;
    }
}
