package X;

import android.content.Context;
import com.whatsapp.conversation.ConversationAttachmentContentView;

/* renamed from: X.429  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass429 extends AbstractC73733gi {
    public int A00;
    public final /* synthetic */ ConversationAttachmentContentView A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass429(Context context, ConversationAttachmentContentView conversationAttachmentContentView) {
        super(context);
        this.A01 = conversationAttachmentContentView;
    }

    public int getBaseHeightPx() {
        return this.A00;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(1, this.A00 + this.A01.A05);
    }

    public void setBaseHeightPx(int i) {
        this.A00 = i;
    }
}
