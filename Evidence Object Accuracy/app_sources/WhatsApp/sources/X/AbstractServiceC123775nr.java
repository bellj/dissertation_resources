package X;

import com.whatsapp.payments.service.NoviVideoSelfieFgService;

/* renamed from: X.5nr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractServiceC123775nr extends AnonymousClass1JK {
    public boolean A00 = false;

    public AbstractServiceC123775nr() {
        super("novivideoselfiefgservice", true);
    }

    @Override // X.AnonymousClass1JL
    public void A00() {
        if (!this.A00) {
            this.A00 = true;
            NoviVideoSelfieFgService noviVideoSelfieFgService = (NoviVideoSelfieFgService) this;
            AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) generatedComponent())).A01;
            ((AnonymousClass1JK) noviVideoSelfieFgService).A01 = (C22900zp) r1.A7t.get();
            noviVideoSelfieFgService.A07 = C12960it.A0Q(r1);
            noviVideoSelfieFgService.A0A = C117305Zk.A0W(r1);
            noviVideoSelfieFgService.A08 = C117305Zk.A0P(r1);
            noviVideoSelfieFgService.A09 = (C130155yt) r1.ADA.get();
            noviVideoSelfieFgService.A0D = (C128615wO) r1.ADY.get();
            noviVideoSelfieFgService.A0B = (C129695y9) r1.ADQ.get();
        }
    }

    @Override // X.AnonymousClass1JK, X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
    }
}
