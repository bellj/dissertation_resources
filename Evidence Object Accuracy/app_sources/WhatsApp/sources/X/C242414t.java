package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.14t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242414t {
    public final C17170qN A00;
    public final C16490p7 A01;

    public C242414t(C17170qN r1, C16490p7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final void A00(long j, int i) {
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("state", Integer.valueOf(i));
            A02.A03.A06(contentValues, "message_view_once_media", 5);
            A02.close();
        } catch (Exception e) {
            Log.e("ViewOnceMessageStore/updateInsert failed", e);
        }
    }

    public void A01(AbstractC15340mz r10) {
        int i;
        AnonymousClass1XH r7 = (AnonymousClass1XH) r10;
        long j = r10.A11;
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT state FROM message_view_once_media WHERE message_row_id = ?", new String[]{Long.toString(j)});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("state");
            if (A09.moveToNext()) {
                i = A09.getInt(columnIndexOrThrow);
                A09.close();
                A01.close();
            } else {
                A09.close();
                A01.close();
                i = 2;
            }
            r7.Ad8(i);
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(AbstractC15340mz r3) {
        if (r3 instanceof AnonymousClass1XH) {
            int i = r3.A0C;
            if (i == 9 || i == 10 || i == 8) {
                A03((AbstractC15340mz) ((AnonymousClass1XH) r3), 1);
            }
        }
    }

    public void A03(AbstractC15340mz r10, int i) {
        AnonymousClass1XH r2 = (AnonymousClass1XH) r10;
        r2.Ad8(i);
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("state", Integer.valueOf(r2.AHc()));
            A02.A03.A00("message_view_once_media", contentValues, "message_row_id = ?", new String[]{String.valueOf(r10.A11)});
            A02.close();
        } catch (Exception e) {
            Log.e("ViewOnceMessageStore/updateInsert failed", e);
        }
    }
}
