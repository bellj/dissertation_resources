package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DD implements Iterator {
    public int A00 = 0;
    public final int A01;
    public final /* synthetic */ AbstractC111925Bi A02;

    public AnonymousClass5DD(AbstractC111925Bi r2) {
        this.A02 = r2;
        this.A01 = r2.A02();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A01);
    }

    @Override // java.util.Iterator
    public /* synthetic */ Object next() {
        byte b;
        int i = this.A00;
        if (i < this.A01) {
            this.A00 = i + 1;
            C80273rz r2 = (C80273rz) this.A02;
            if (!(r2 instanceof C80263ry)) {
                b = r2.zzb[i];
            } else {
                C80263ry r22 = (C80263ry) r2;
                b = r22.zzb[r22.zzc + i];
            }
            return Byte.valueOf(b);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }
}
