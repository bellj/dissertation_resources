package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import kotlinx.coroutines.android.AndroidDispatcherFactory;

/* renamed from: X.3Gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64713Gp {
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r0 <= 0) goto L_0x0005;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r4.add(r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.List A00(java.io.BufferedReader r5) {
        /*
            java.util.LinkedHashSet r4 = new java.util.LinkedHashSet
            r4.<init>()
        L_0x0005:
            java.lang.String r2 = r5.readLine()
            if (r2 != 0) goto L_0x0010
            java.util.List r0 = X.AnonymousClass01Y.A06(r4)
            return r0
        L_0x0010:
            java.lang.String r0 = "#"
            r3 = 0
            int r1 = X.AnonymousClass03B.A03(r2, r0, r3)
            r0 = -1
            if (r1 == r0) goto L_0x0021
            java.lang.String r2 = r2.substring(r3, r1)
            X.C16700pc.A0B(r2)
        L_0x0021:
            java.lang.CharSequence r0 = X.AnonymousClass03B.A04(r2)
            java.lang.String r2 = r0.toString()
        L_0x0029:
            int r0 = r2.length()
            if (r3 >= r0) goto L_0x004a
            char r1 = r2.charAt(r3)
            int r3 = r3 + 1
            r0 = 46
            if (r1 == r0) goto L_0x0029
            boolean r0 = java.lang.Character.isJavaIdentifierPart(r1)
            if (r0 != 0) goto L_0x0029
            java.lang.String r0 = "Illegal service provider class name: "
            java.lang.String r0 = X.C16700pc.A08(r0, r2)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x004a:
            if (r0 <= 0) goto L_0x0005
            r4.add(r2)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64713Gp.A00(java.io.BufferedReader):java.util.List");
    }

    public final List A01(ClassLoader classLoader) {
        List list;
        try {
            ArrayList list2 = Collections.list(classLoader.getResources(C16700pc.A08("META-INF/services/", AndroidDispatcherFactory.class.getName())));
            C16700pc.A0B(list2);
            ArrayList A0l = C12960it.A0l();
            Iterator it = list2.iterator();
            while (it.hasNext()) {
                URL url = (URL) it.next();
                String obj = url.toString();
                if (AnonymousClass03C.A0L(obj, "jar")) {
                    String A07 = AnonymousClass03B.A07(obj, "jar:file:", obj);
                    int A01 = AnonymousClass03B.A01(A07, '!', 0, 6);
                    if (A01 != -1) {
                        A07 = A07.substring(0, A01);
                        C16700pc.A0B(A07);
                    }
                    String A072 = AnonymousClass03B.A07(obj, "!/", obj);
                    JarFile jarFile = new JarFile(A07, false);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(new ZipEntry(A072)), DefaultCrypto.UTF_8));
                    try {
                        list = A00(bufferedReader);
                        bufferedReader.close();
                        jarFile.close();
                    } catch (Throwable th) {
                        try {
                            throw th;
                        } catch (Throwable th2) {
                            C88184Ep.A00(bufferedReader, th);
                            throw th2;
                        }
                    }
                } else {
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(url.openStream()));
                    list = A00(bufferedReader2);
                    bufferedReader2.close();
                }
                AnonymousClass01Z.A0B(list, A0l);
            }
            Set A08 = AnonymousClass01Y.A08(A0l);
            if (!A08.isEmpty()) {
                ArrayList A0E = C16760pi.A0E(A08);
                Iterator it2 = A08.iterator();
                while (it2.hasNext()) {
                    Class<?> cls = Class.forName(C12970iu.A0x(it2), false, classLoader);
                    if (AndroidDispatcherFactory.class.isAssignableFrom(cls)) {
                        A0E.add(AndroidDispatcherFactory.class.cast(cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0])));
                    } else {
                        StringBuilder A0k = C12960it.A0k("Expected service of class ");
                        A0k.append(AndroidDispatcherFactory.class);
                        throw C12970iu.A0f(C12960it.A0Z(cls, ", but found ", A0k));
                    }
                }
                return A0E;
            }
            throw C12970iu.A0f("No providers were loaded with FastServiceLoader");
        } catch (Throwable unused) {
            return AnonymousClass01Y.A06(ServiceLoader.load(AndroidDispatcherFactory.class, classLoader));
        }
    }
}
