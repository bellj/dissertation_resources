package X;

/* renamed from: X.30N  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30N extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;

    public AnonymousClass30N() {
        super(1616, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamChatFilterEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "actionType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filterType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sessionId", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
