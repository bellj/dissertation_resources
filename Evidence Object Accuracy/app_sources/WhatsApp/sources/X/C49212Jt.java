package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.2Jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49212Jt implements AbstractC18880tD {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C41121sw A01;
    public final /* synthetic */ String A02;

    public C49212Jt(C41121sw r1, String str, long j) {
        this.A01 = r1;
        this.A00 = j;
        this.A02 = str;
    }

    @Override // X.AbstractC18880tD
    public void AUt() {
        C41121sw r0 = this.A01;
        r0.A03.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(r0.A0F, 7));
    }

    @Override // X.AbstractC18880tD
    public void AXI() {
        Log.i("CompanionDeviceQrHandler/handleSyncdDirty onSyncdDataDeleted");
        C41121sw r4 = this.A01;
        r4.A08.A08(this.A00, true);
        r4.A07.A04(this);
        r4.A03.A0H(new RunnableBRunnable0Shape0S1100000_I0(19, this.A02, this));
    }

    @Override // X.AbstractC18880tD
    public void AXJ() {
        Log.e("CompanionDeviceQrHandler/handleSyncdDirty onSyncdDataDeletionFailed");
        C41121sw r4 = this.A01;
        r4.A08.A08(this.A00, false);
        r4.A07.A04(this);
        r4.A03.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(this, 6));
    }
}
