package X;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import com.facebook.redex.RunnableBRunnable0Shape0S0800000_I0;
import com.whatsapp.appwidget.WidgetProvider;
import com.whatsapp.util.Log;

/* renamed from: X.0xB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21290xB {
    public Handler A00;
    public C20230vQ A01;
    public Runnable A02;
    public final C15570nT A03;
    public final C16590pI A04;
    public final C19990v2 A05;
    public final C15680nj A06;
    public final AnonymousClass10Y A07;
    public final C243815h A08;
    public final C15860o1 A09;

    public C21290xB(C15570nT r1, C16590pI r2, C19990v2 r3, C15680nj r4, AnonymousClass10Y r5, C243815h r6, C15860o1 r7) {
        this.A04 = r2;
        this.A03 = r1;
        this.A05 = r3;
        this.A08 = r6;
        this.A07 = r5;
        this.A09 = r7;
        this.A06 = r4;
    }

    public synchronized Handler A00() {
        Handler handler;
        handler = this.A00;
        if (handler == null) {
            HandlerThread handlerThread = new HandlerThread("update_widget", 10);
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper());
            this.A00 = handler;
        }
        return handler;
    }

    public synchronized void A01() {
        C16590pI r8 = this.A04;
        Context context = r8.A00;
        try {
            int[] appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, WidgetProvider.class));
            if (appWidgetIds != null && appWidgetIds.length > 0) {
                Intent intent = new Intent(context, WidgetProvider.class);
                intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
                intent.putExtra("appWidgetIds", appWidgetIds);
                context.sendBroadcast(intent);
            }
        } catch (RuntimeException e) {
            Log.e("widgetprovider/getAppWidgetIds failed", e);
        }
        C243815h r0 = this.A08;
        AbstractC35031h7 A00 = r0.A00(context);
        if (!(A00 == null || A00 == r0.A02)) {
            if (this.A02 == null) {
                C15570nT r7 = this.A03;
                C19990v2 r3 = this.A05;
                AnonymousClass10Y r9 = this.A07;
                C15860o1 r6 = this.A09;
                C15680nj r10 = this.A06;
                C20230vQ r4 = this.A01;
                if (r4 == null) {
                    r4 = (C20230vQ) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).ACe.get();
                    this.A01 = r4;
                }
                this.A02 = new RunnableBRunnable0Shape0S0800000_I0(r3, r4, A00, r6, r7, r8, r9, r10, 0);
            }
            A00().removeCallbacks(this.A02);
            A00().post(this.A02);
        }
    }
}
