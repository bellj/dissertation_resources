package X;

/* renamed from: X.4bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94364bg {
    public int A00;
    public int A01;
    public int A02;
    public int A03;

    public C94364bg() {
        this(null, 0, 0, 0, 0, 15);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C94364bg) {
                C94364bg r5 = (C94364bg) obj;
                if (!(this.A01 == r5.A01 && this.A03 == r5.A03 && this.A02 == r5.A02 && this.A00 == r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A01 * 31) + this.A03) * 31) + this.A02) * 31) + this.A00;
    }

    public /* synthetic */ C94364bg(C51012Sk r2, int i, int i2, int i3, int i4, int i5) {
        this.A01 = 0;
        this.A03 = 0;
        this.A02 = 0;
        this.A00 = 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Margins(left=");
        A0k.append(this.A01);
        A0k.append(", top=");
        A0k.append(this.A03);
        A0k.append(", right=");
        A0k.append(this.A02);
        A0k.append(", bottom=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
