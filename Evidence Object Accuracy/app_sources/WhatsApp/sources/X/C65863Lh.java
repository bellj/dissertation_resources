package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Status;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3Lh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65863Lh implements Handler.Callback {
    public static C65863Lh A0F;
    public static final Status A0G = new Status(4, "Sign-out occurred while this API call was in progress.");
    public static final Status A0H = new Status(4, "The user must be signed in to make this API call.");
    public static final Object A0I = C12970iu.A0l();
    public long A00 = 10000;
    public DialogInterface$OnCancelListenerC56312kg A01 = null;
    public C56402kp A02;
    public AnonymousClass5Yj A03;
    public boolean A04 = false;
    public final Context A05;
    public final Handler A06;
    public final C471729i A07;
    public final C64793Gx A08;
    public final Map A09 = new ConcurrentHashMap(5, 0.75f, 1);
    public final Set A0A = new AnonymousClass01b(0);
    public final Set A0B = new AnonymousClass01b(0);
    public final AtomicInteger A0C = new AtomicInteger(1);
    public final AtomicInteger A0D = new AtomicInteger(0);
    public volatile boolean A0E = true;

    public C65863Lh(Context context, Looper looper, C471729i r8) {
        this.A05 = context;
        HandlerC472529t r3 = new HandlerC472529t(looper, this);
        this.A06 = r3;
        this.A07 = r8;
        this.A08 = new C64793Gx(r8);
        PackageManager packageManager = context.getPackageManager();
        Boolean bool = C472629u.A03;
        if (bool == null) {
            boolean z = false;
            if (C472729v.A03() && packageManager.hasSystemFeature("android.hardware.type.automotive")) {
                z = true;
            }
            bool = Boolean.valueOf(z);
            C472629u.A03 = bool;
        }
        if (bool.booleanValue()) {
            this.A0E = false;
        }
        r3.sendMessage(r3.obtainMessage(6));
    }

    public static Status A00(C56492ky r6, AnonymousClass4XF r7) {
        String str = r7.A02.A02;
        String valueOf = String.valueOf(r6);
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 63 + valueOf.length());
        A0t.append("API: ");
        A0t.append(str);
        A0t.append(" is not available on this device. Connection failed with: ");
        return new Status(r6.A02, r6, C12960it.A0d(valueOf, A0t), 1, 17);
    }

    public static C65863Lh A01(Context context) {
        C65863Lh r3;
        HandlerThread handlerThread;
        synchronized (A0I) {
            r3 = A0F;
            if (r3 == null) {
                synchronized (C94924cl.A07) {
                    handlerThread = C94924cl.A05;
                    if (handlerThread == null) {
                        HandlerThread handlerThread2 = new HandlerThread("GoogleApiHandler", 9);
                        C94924cl.A05 = handlerThread2;
                        handlerThread2.start();
                        handlerThread = C94924cl.A05;
                    }
                }
                r3 = new C65863Lh(context.getApplicationContext(), handlerThread.getLooper(), C471729i.A00);
                A0F = r3;
            }
        }
        return r3;
    }

    public final C14970mL A02(AnonymousClass2RP r4) {
        AnonymousClass4XF r2 = r4.A06;
        Map map = this.A09;
        C14970mL r1 = (C14970mL) map.get(r2);
        if (r1 == null) {
            r1 = new C14970mL(r4, this);
            map.put(r2, r1);
        }
        if (r1.A04.Aae()) {
            this.A0B.add(r2);
        }
        r1.A03();
        return r1;
    }

    public final void A03() {
        C56402kp r3 = this.A02;
        if (r3 != null) {
            if (r3.A01 > 0 || A06()) {
                AnonymousClass5Yj r2 = this.A03;
                if (r2 == null) {
                    r2 = new C56302kf(this.A05, C108174yc.A00);
                    this.A03 = r2;
                }
                r2.AKY(r3);
            }
            this.A02 = null;
        }
    }

    public final void A04(C56492ky r4, int i) {
        if (!A07(r4, i)) {
            Handler handler = this.A06;
            handler.sendMessage(handler.obtainMessage(5, i, 0, r4));
        }
    }

    public final void A05(DialogInterface$OnCancelListenerC56312kg r4) {
        synchronized (A0I) {
            if (this.A01 != r4) {
                this.A01 = r4;
                this.A0A.clear();
            }
            this.A0A.addAll(r4.A01);
        }
    }

    public final boolean A06() {
        C56412kq r0;
        int i;
        if (this.A04 || (((r0 = C94784cX.A00().A00) != null && !r0.A03) || ((i = this.A08.A01.get(203400000, -1)) != -1 && i != 0))) {
            return false;
        }
        return true;
    }

    public final boolean A07(C56492ky r9, int i) {
        PendingIntent activity;
        C471729i r4 = this.A07;
        Context context = this.A05;
        if (AnonymousClass3GS.A00(context)) {
            return false;
        }
        if (r9.A00()) {
            activity = r9.A02;
        } else {
            Intent A01 = r4.A01(context, null, r9.A01);
            if (A01 == null) {
                return false;
            }
            activity = PendingIntent.getActivity(context, 0, A01, C88384Fl.A00 | 134217728);
        }
        if (activity == null) {
            return false;
        }
        int i2 = r9.A01;
        Intent A0D = C12990iw.A0D(context, GoogleApiActivity.class);
        A0D.putExtra("pending_intent", activity);
        A0D.putExtra("failing_client_id", i);
        A0D.putExtra("notify_manager", true);
        r4.A03(PendingIntent.getActivity(context, 0, A0D, C88344Fh.A00 | 134217728), context, i2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0253, code lost:
        if (r1.A02 != false) goto L_0x0255;
     */
    @Override // android.os.Handler.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean handleMessage(android.os.Message r12) {
        /*
        // Method dump skipped, instructions count: 972
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65863Lh.handleMessage(android.os.Message):boolean");
    }
}
