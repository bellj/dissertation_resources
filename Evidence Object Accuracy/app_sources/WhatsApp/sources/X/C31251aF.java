package X;

/* renamed from: X.1aF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31251aF {
    public final AbstractC31231aD A00;
    public final byte[] A01;

    public C31251aF(AbstractC31231aD r1, byte[] bArr) {
        this.A00 = r1;
        this.A01 = bArr;
    }

    public C32021bU A00(C31491ad r6, C31731b1 r7) {
        byte[] A02 = C31481ac.A02(r7.A00, r6);
        AbstractC31231aD r4 = this.A00;
        C90614Oo r1 = new C90614Oo(r4.A02(A02, this.A01, "WhisperRatchet".getBytes(), 64));
        return new C32021bU(new C31251aF(r4, r1.A01), new C31261aG(r4, r1.A00, 0));
    }
}
