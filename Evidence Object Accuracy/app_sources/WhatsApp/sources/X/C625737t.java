package X;

import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.37t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625737t extends AbstractC16350or {
    public int A00;
    public int A01;
    public C15580nU A02;
    public final C20660w7 A03;
    public final String A04;
    public final WeakReference A05;

    public C625737t(AcceptInviteLinkActivity acceptInviteLinkActivity, C20660w7 r3, String str, int i) {
        super(acceptInviteLinkActivity);
        this.A03 = r3;
        this.A05 = C12970iu.A10(acceptInviteLinkActivity);
        this.A04 = str;
        this.A01 = i;
    }
}
