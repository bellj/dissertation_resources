package X;

import android.text.TextUtils;
import android.util.JsonReader;

/* renamed from: X.19g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254019g extends AbstractC253919f {
    public C254019g(C18790t3 r1, C14830m7 r2, AnonymousClass018 r3, C20250vS r4, AnonymousClass197 r5, C16120oU r6, C253719d r7, C19930uu r8, AbstractC14440lR r9) {
        super(r1, r2, r3, r4, r5, r6, r7, r8, r9);
    }

    public static final AnonymousClass01T A00(JsonReader jsonReader) {
        int[] iArr = new int[2];
        jsonReader.beginObject();
        String str = null;
        String str2 = null;
        while (true) {
            int i = 0;
            if (jsonReader.hasNext()) {
                String nextName = jsonReader.nextName();
                switch (nextName.hashCode()) {
                    case -318184504:
                        if (!nextName.equals("preview")) {
                            break;
                        } else {
                            str2 = jsonReader.nextString();
                            continue;
                        }
                    case 116079:
                        if (!nextName.equals("url")) {
                            break;
                        } else {
                            str = jsonReader.nextString();
                            continue;
                        }
                    case 3083499:
                        if (!nextName.equals("dims")) {
                            break;
                        } else {
                            jsonReader.beginArray();
                            while (jsonReader.hasNext()) {
                                if (i < 2) {
                                    iArr[i] = jsonReader.nextInt();
                                    i++;
                                } else {
                                    jsonReader.skipValue();
                                }
                            }
                            jsonReader.endArray();
                            continue;
                        }
                }
                jsonReader.skipValue();
            } else {
                jsonReader.endObject();
                if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
                    return new AnonymousClass01T(null, null);
                }
                int i2 = iArr[0];
                int i3 = iArr[1];
                return new AnonymousClass01T(new C66003Lx(i2, str, i3), new C66003Lx(i2, str2, i3));
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 3, insn: 0x027f: IPUT  (r0v2 ?? I:java.lang.Long), (r3 I:X.318) X.318.A05 java.lang.Long, block:B:93:0x0273
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static /* synthetic */ X.AnonymousClass01T A01(
/*
[672] Method generation error in method: X.19g.A01(X.318, X.19g, java.lang.String):X.01T, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r21v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/
}
