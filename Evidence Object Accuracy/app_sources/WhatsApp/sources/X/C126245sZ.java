package X;

/* renamed from: X.5sZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126245sZ {
    public final AnonymousClass1V8 A00;

    public C126245sZ(String str, String str2) {
        C41141sy r4 = new C41141sy("elo");
        if (C117295Zj.A1V(str, 1, false)) {
            C41141sy.A01(r4, "network_device_id", str);
        }
        if (C117295Zj.A1W(str2, 1, false)) {
            C41141sy.A01(r4, "nonce", str2);
        }
        this.A00 = r4.A03();
    }
}
