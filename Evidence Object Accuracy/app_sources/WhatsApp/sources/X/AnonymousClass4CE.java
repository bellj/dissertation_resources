package X;

/* renamed from: X.4CE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CE extends Exception {
    public AnonymousClass4CE(String str) {
        super(str);
    }

    public AnonymousClass4CE(Throwable th) {
        super("Target package not found.", th);
    }
}
