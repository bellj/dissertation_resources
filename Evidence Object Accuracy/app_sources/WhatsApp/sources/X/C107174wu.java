package X;

/* renamed from: X.4wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107174wu implements AnonymousClass5X7 {
    public long A00;
    public long A01;
    public AnonymousClass5X6 A02;
    public AnonymousClass4UA A03;
    public String A04;
    public boolean A05;
    public final C92814Xn A06 = new C92814Xn(34);
    public final C92814Xn A07 = new C92814Xn(39);
    public final C92814Xn A08 = new C92814Xn(33);
    public final C92814Xn A09 = new C92814Xn(40);
    public final C92814Xn A0A = new C92814Xn(32);
    public final AnonymousClass4VF A0B;
    public final C95304dT A0C = new C95304dT();
    public final boolean[] A0D = new boolean[3];

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107174wu(AnonymousClass4VF r3) {
        this.A0B = r3;
    }

    public final void A00(byte[] bArr, int i, int i2) {
        AnonymousClass4UA r2 = this.A03;
        if (r2.A07) {
            int i3 = r2.A00;
            int i4 = (i + 2) - i3;
            if (i4 < i2) {
                r2.A06 = C12960it.A1S(bArr[i4] & 128);
                r2.A07 = false;
            } else {
                r2.A00 = i3 + (i2 - i);
            }
        }
        if (!this.A05) {
            this.A0A.A01(bArr, i, i2);
            this.A08.A01(bArr, i, i2);
            this.A06.A01(bArr, i, i2);
        }
        this.A07.A01(bArr, i, i2);
        this.A09.A01(bArr, i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03a4, code lost:
        if (r4 > 21) goto L_0x03a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x016d, code lost:
        if (r1 == 2) goto L_0x016f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x03b6  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r31) {
        /*
        // Method dump skipped, instructions count: 997
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107174wu.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r3, C92824Xo r4) {
        r4.A03();
        this.A04 = r4.A02();
        AnonymousClass5X6 Af4 = r3.Af4(r4.A01(), 2);
        this.A02 = Af4;
        this.A03 = new AnonymousClass4UA(Af4);
        this.A0B.A00(r3, r4);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A00 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A01 = 0;
        boolean A1W = C72453ed.A1W(this.A0D);
        C92814Xn r0 = this.A0A;
        r0.A02 = A1W;
        r0.A01 = A1W;
        C92814Xn r02 = this.A08;
        r02.A02 = A1W;
        r02.A01 = A1W;
        C92814Xn r03 = this.A06;
        r03.A02 = A1W;
        r03.A01 = A1W;
        C92814Xn r04 = this.A07;
        r04.A02 = A1W;
        r04.A01 = A1W;
        C92814Xn r05 = this.A09;
        r05.A02 = A1W;
        r05.A01 = A1W;
        AnonymousClass4UA r06 = this.A03;
        if (r06 != null) {
            r06.A07 = A1W;
            r06.A06 = A1W;
            r06.A05 = A1W;
            r06.A0A = A1W;
            r06.A09 = A1W;
        }
    }
}
