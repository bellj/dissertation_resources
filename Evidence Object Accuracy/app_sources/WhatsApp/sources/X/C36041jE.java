package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1jE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36041jE extends AnonymousClass015 {
    public final C15570nT A00;
    public final C15550nR A01;
    public final AnonymousClass11D A02;
    public final C22700zV A03;
    public final C15610nY A04;
    public final C14830m7 A05;
    public final C19990v2 A06;
    public final C15600nX A07;
    public final AnonymousClass11B A08;
    public final C15580nU A09;
    public final C36051jF A0A;
    public final C36081jI A0B = new C36081jI();
    public final C36081jI A0C = new C36081jI();
    public final C36081jI A0D = new C36081jI();
    public final C36081jI A0E = new C36081jI();
    public final C253118x A0F;
    public final C36161jQ A0G = new C36161jQ(0);
    public final AbstractC14440lR A0H;

    public C36041jE(C15570nT r3, C15550nR r4, AnonymousClass11D r5, C22700zV r6, C15610nY r7, C14830m7 r8, C19990v2 r9, C15600nX r10, AnonymousClass11B r11, C15580nU r12, C36051jF r13, C253118x r14, AbstractC14440lR r15) {
        this.A05 = r8;
        this.A0F = r14;
        this.A00 = r3;
        this.A0H = r15;
        this.A06 = r9;
        this.A01 = r4;
        this.A04 = r7;
        this.A03 = r6;
        this.A07 = r10;
        this.A08 = r11;
        this.A02 = r5;
        this.A09 = r12;
        this.A0A = r13;
    }

    public void A04(AbstractC29911Vf r3) {
        C36091jJ r0 = (C36091jJ) this.A0B.A01();
        if (r0 != null) {
            C15370n3.A06(r3, r0.A00);
            C36081jI r1 = this.A0E;
            Object A01 = r1.A01();
            if (A01 != null) {
                r1.A0A(A01);
            }
            C36081jI r12 = this.A0C;
            Object A012 = r12.A01();
            if (A012 != null) {
                r12.A0A(A012);
            }
        }
    }

    public boolean A05(UserJid userJid) {
        C36091jJ r0 = (C36091jJ) this.A0B.A01();
        return r0 != null && r0.A01.contains(userJid);
    }
}
