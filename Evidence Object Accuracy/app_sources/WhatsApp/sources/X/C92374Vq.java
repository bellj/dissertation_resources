package X;

/* renamed from: X.4Vq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92374Vq {
    public final int A00;
    public final C41301tJ A01;
    public final String A02;
    public final String A03;

    public C92374Vq(C41301tJ r1, String str, String str2, int i) {
        this.A00 = i;
        this.A03 = str;
        this.A02 = str2;
        this.A01 = r1;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CollectionError{code=");
        A0k.append(this.A00);
        A0k.append(", text='");
        A0k.append(this.A03);
        A0k.append(", collection='");
        A0k.append(this.A02);
        return C12970iu.A0v(A0k);
    }
}
