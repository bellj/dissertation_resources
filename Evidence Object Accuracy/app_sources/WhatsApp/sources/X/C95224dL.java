package X;

/* renamed from: X.4dL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95224dL {
    public static final AnonymousClass5FA A00 = new AnonymousClass5FA();
    public static final AnonymousClass5FB A01 = new AnonymousClass5FB();
    public static final AnonymousClass5F7 A02 = new AnonymousClass5F7();
    public static final AnonymousClass5F8 A03 = new AnonymousClass5F8();
    public static final AnonymousClass5F9 A04 = new AnonymousClass5F9();

    public static boolean A00(char c) {
        return c == '{' || c == '[' || c == ',' || c == '}' || c == ']' || c == ':' || c == '\'' || c == '\"';
    }

    public static boolean A01(char c) {
        if (c < 0) {
            return false;
        }
        if (c <= 31) {
            return true;
        }
        if (c < 127 || c > 159) {
            return c >= 8192 && c <= 8447;
        }
        return true;
    }

    public static boolean A02(String str) {
        String str2;
        if (str.length() >= 3) {
            char charAt = str.charAt(0);
            if (charAt == 'n') {
                str2 = "null";
            } else if (charAt == 't') {
                str2 = "true";
            } else if (charAt == 'f') {
                str2 = "false";
            } else if (charAt == 'N') {
                str2 = "NaN";
            }
            return str.equals(str2);
        }
        return false;
    }
}
