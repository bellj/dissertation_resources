package X;

/* renamed from: X.6Ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C134306Ec implements AbstractC14590lg {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ C14580lf A01;
    public final /* synthetic */ AnonymousClass61F A02;

    public /* synthetic */ C134306Ec(AnonymousClass016 r1, C14580lf r2, AnonymousClass61F r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        AnonymousClass61F r3 = this.A02;
        AnonymousClass016 r2 = this.A00;
        C14580lf r1 = this.A01;
        AbstractC28901Pl r5 = (AbstractC28901Pl) obj;
        if (r5 != null) {
            r3.A01 = AnonymousClass61F.A02(r5);
            r2.A0A(r5);
        }
        r1.A04();
    }
}
