package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.13M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13M {
    public final C15570nT A00;
    public final C16370ot A01;
    public final C15650ng A02;
    public final C20560vx A03;
    public final C20660w7 A04;
    public final AbstractC14440lR A05;

    public AnonymousClass13M(C15570nT r1, C16370ot r2, C15650ng r3, C20560vx r4, C20660w7 r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    public List A00(C15580nU r8, long j) {
        C20560vx r4 = this.A03;
        C15570nT r0 = this.A00;
        r0.A08();
        C27631Ih r6 = r0.A05;
        AnonymousClass009.A05(r6);
        ArrayList arrayList = new ArrayList();
        C18460sU r3 = r4.A01;
        String[] strArr = {Long.toString(r3.A01(r8)), Long.toString(r3.A01(r6)), Long.toString(j)};
        C16310on A01 = r4.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT chat_row_id, message_row_id FROM message_group_invite invite INNER JOIN message_view message ON invite.message_row_id = message._id WHERE invite.group_jid_row_id = ? AND invite.admin_jid_row_id = ? AND invite.expiration > ? AND invite.expired = 0", strArr);
            while (A09.moveToNext()) {
                arrayList.add(Long.valueOf(A09.getLong(A09.getColumnIndexOrThrow("message_row_id"))));
            }
            A09.close();
            A01.close();
            ArrayList arrayList2 = new ArrayList(arrayList.size());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                arrayList2.add(this.A01.A00(((Number) it.next()).longValue()));
            }
            return arrayList2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
