package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2kq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56412kq extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98744jB();
    public final int A00;
    public final int A01;
    public final int A02;
    public final boolean A03;
    public final boolean A04;

    public C56412kq(int i, int i2, int i3, boolean z, boolean z2) {
        this.A00 = i;
        this.A03 = z;
        this.A04 = z2;
        this.A01 = i2;
        this.A02 = i3;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A09(parcel, 2, this.A03);
        C95654e8.A09(parcel, 3, this.A04);
        C95654e8.A07(parcel, 4, this.A01);
        C95654e8.A07(parcel, 5, this.A02);
        C95654e8.A06(parcel, A01);
    }
}
