package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.07N  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07N {
    public AbstractC11640gc A00;
    public AbstractC11650gd A01;
    public final Context A02;
    public final View A03;
    public final AnonymousClass07H A04;
    public final C05530Px A05;

    public AnonymousClass07N(Context context, View view) {
        this(context, view, 0, R.attr.popupMenuStyle);
    }

    public AnonymousClass07N(Context context, View view, int i, int i2) {
        this.A02 = context;
        this.A03 = view;
        AnonymousClass07H r4 = new AnonymousClass07H(context);
        this.A04 = r4;
        r4.A0C(new AnonymousClass0XG(this));
        C05530Px r1 = new C05530Px(context, view, r4, i2, 0, false);
        this.A05 = r1;
        r1.A00 = i;
        r1.A02 = new AnonymousClass0X6(this);
    }

    public void A00() {
        if (!this.A05.A03()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }
}
