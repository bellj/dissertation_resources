package X;

/* renamed from: X.3Gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64723Gq {
    public static final C64723Gq A00 = new C64723Gq();

    public CharSequence A00(AnonymousClass28D r8) {
        int i = r8.A01;
        if (i != 13319) {
            C28691Op.A00("BloksTextProviderMapper", C12960it.A0W(i, "Unrecognized Text provider with style id "));
            return "";
        }
        return AnonymousClass3AB.A00(r8.A0J(38, "date"), AnonymousClass28D.A06(r8), r8.A0I(40), r8.A0I(41), r8.A0C(36));
    }
}
