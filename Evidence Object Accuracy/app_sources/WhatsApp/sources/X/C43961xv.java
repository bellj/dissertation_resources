package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43961xv {
    public final int A00;
    public final int A01;
    public final long A02;
    public final AbstractC14640lm A03;
    public final UserJid A04;
    public final AnonymousClass1IS A05;
    public final String A06;

    public C43961xv(AbstractC14640lm r2, UserJid userJid, AnonymousClass1IS r4, String str, int i, int i2, long j) {
        boolean z = true;
        if (!(i == 0 || i == 1)) {
            z = false;
        }
        AnonymousClass009.A0E(z);
        this.A05 = r4;
        this.A03 = r2;
        this.A02 = j;
        this.A01 = i;
        this.A06 = str;
        this.A04 = userJid;
        this.A00 = i2;
    }

    public static C43961xv A00(C28941Pp r9) {
        AnonymousClass1IS r3 = r9.A0C;
        if (r3 == null) {
            r3 = r9.A0i;
        }
        return new C43961xv(C15380n4.A00(r9.A08), null, r3, null, 1, r9.A00(), r9.A0f);
    }

    public static C43961xv A01(C30331Wz r9) {
        UserJid userJid;
        if (r9 instanceof C30321Wy) {
            userJid = ((C30321Wy) r9).A00;
        } else {
            userJid = null;
        }
        AnonymousClass1IS r3 = r9.A0z;
        return new C43961xv(r9.A0B(), userJid, r3, r9.A01, 0, 0, r9.A0I);
    }
}
