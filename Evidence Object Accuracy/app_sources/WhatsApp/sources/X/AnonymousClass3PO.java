package X;

import android.view.View;
import androidx.core.widget.NestedScrollView;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;

/* renamed from: X.3PO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3PO implements AbstractC11770gq {
    public final /* synthetic */ StickerStoreFeaturedTabFragment A00;

    public AnonymousClass3PO(StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment) {
        this.A00 = stickerStoreFeaturedTabFragment;
    }

    @Override // X.AbstractC11770gq
    public void AVa(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
        View view;
        C73893gy r1;
        StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = this.A00;
        boolean localVisibleRect = stickerStoreFeaturedTabFragment.A06.getLocalVisibleRect(stickerStoreFeaturedTabFragment.A00);
        int top = stickerStoreFeaturedTabFragment.A02.getTop();
        if (localVisibleRect) {
            if (top != stickerStoreFeaturedTabFragment.A02.getHeight()) {
                view = stickerStoreFeaturedTabFragment.A02;
                r1 = new C73893gy(stickerStoreFeaturedTabFragment.A02, view.getHeight());
            } else {
                return;
            }
        } else if (top != 0 && stickerStoreFeaturedTabFragment.A02.getAnimation() == null) {
            view = stickerStoreFeaturedTabFragment.A02;
            r1 = new C73893gy(view, 0);
        } else {
            return;
        }
        view.startAnimation(r1);
    }
}
