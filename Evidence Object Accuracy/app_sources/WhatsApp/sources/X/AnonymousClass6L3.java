package X;

import java.util.HashMap;
import java.util.HashSet;

/* renamed from: X.6L3  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6L3 extends HashMap<C1310160w, HashSet<C129845yO>> {
    public AnonymousClass6L3() {
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-CL00"), C1308760h.A00(new C129845yO(1440, 1080)));
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-CL10"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-L09"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-TL00"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-UL00"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI GRA-UL10"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-CL00"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-J1"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-L09"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-TL00"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-TL10"), A00());
        put(C117295Zj.A08("Huawei", "HUAWEI MT7-UL00"), A00());
    }

    public static HashSet A00() {
        return C1308760h.A00(new C129845yO(1440, 1080));
    }
}
