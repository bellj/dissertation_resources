package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4a3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93364a3 {
    public final int A00;
    public final List A01;

    public C93364a3(int i) {
        this.A01 = Collections.emptyList();
        this.A00 = i;
    }

    public C93364a3(List list) {
        this.A01 = Collections.unmodifiableList(list);
        this.A00 = 0;
    }
}
