package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.io.File;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25841Ba extends AbstractC18830t7 {
    public static final int A0C = ((int) TimeUnit.MINUTES.toMillis(60));
    public static final String A0D;
    public static final String A0E = C130795zz.A03;
    public static final String A0F;
    public C615330s A00;
    public Long A01;
    public String A02;
    public boolean A03;
    public final C14900mE A04;
    public final AnonymousClass1BZ A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C14820m6 A08;
    public final AnonymousClass018 A09;
    public final C16120oU A0A;
    public final C22710zW A0B;

    static {
        StringBuilder sb = new StringBuilder("downloadable");
        String str = File.separator;
        sb.append(str);
        sb.append("bloks_pay");
        String obj = sb.toString();
        A0D = obj;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(obj);
        sb2.append(str);
        sb2.append("layout");
        A0F = sb2.toString();
    }

    public C25841Ba(C14900mE r9, C18790t3 r10, AnonymousClass1BZ r11, C14830m7 r12, C16590pI r13, C14820m6 r14, AnonymousClass018 r15, C16120oU r16, C18810t5 r17, C22710zW r18, C18800t4 r19, AbstractC14440lR r20) {
        super(r10, r13, r17, r19, r20, 14);
        this.A06 = r12;
        this.A04 = r9;
        this.A07 = r13;
        this.A0A = r16;
        this.A09 = r15;
        this.A05 = r11;
        this.A08 = r14;
        this.A0B = r18;
        super.A00 = 15;
        super.A01 = 4;
    }

    public final C615330s A0A() {
        C615330s r2 = new C615330s();
        int i = 0;
        if ("BR".equals(((AbstractC25851Bb) this.A05).ABm().A03)) {
            i = 4;
        }
        r2.A02 = Long.valueOf((long) i);
        r2.A05 = "2.22.17.70";
        r2.A01 = Boolean.valueOf(this.A03);
        r2.A06 = this.A02;
        return r2;
    }

    public final String A0B() {
        String A00 = C130375zJ.A00(((AbstractC25851Bb) this.A05).ABm().A03);
        if (TextUtils.isEmpty(A00)) {
            return CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A00);
        sb.append("_p");
        return sb.toString();
    }

    public void A0C(AnonymousClass2DH r5, AnonymousClass2K5 r6, String str, boolean z) {
        this.A03 = z;
        this.A02 = str;
        if (super.A09) {
            super.A06.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(this, 23, r5));
            return;
        }
        String str2 = null;
        if (TextUtils.isEmpty(null)) {
            str2 = "2.22.17.70";
        }
        super.A03(r5, r6, null, C39461pw.A00(A0B(), this.A09.A06(), null, str2));
    }

    public void A0D(AnonymousClass2K5 r3, String str) {
        if (!A0E() || !A0F()) {
            A0C(null, r3, str, true);
        }
    }

    public boolean A0E() {
        return !A06(A00(A0E)) && !A06(A00(A0F));
    }

    public boolean A0F() {
        StringBuilder sb = new StringBuilder("2.22.17.70");
        sb.append(((AbstractC25851Bb) this.A05).ABm().A03);
        sb.append(" ");
        sb.append(this.A09.A06());
        return sb.toString().equals(this.A08.A00.getString("bloks_version", null));
    }
}
