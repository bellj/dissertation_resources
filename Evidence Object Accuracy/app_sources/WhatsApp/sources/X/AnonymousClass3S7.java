package X;

/* renamed from: X.3S7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S7 implements AnonymousClass070 {
    public final /* synthetic */ AnonymousClass018 A00;
    public final /* synthetic */ AnonymousClass242 A01;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public AnonymousClass3S7(AnonymousClass018 r1, AnonymousClass242 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        AnonymousClass242 r1 = this.A01;
        r1.A00 = i;
        if (!C28141Kv.A01(this.A00)) {
            i = (r1.A03.A01.length - i) - 1;
        }
        r1.A01(i);
        AbstractC116745Wq r0 = r1.A04;
        if (r0 != null) {
            r0.ATQ(i);
        }
    }
}
