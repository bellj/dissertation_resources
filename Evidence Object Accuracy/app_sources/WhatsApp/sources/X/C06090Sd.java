package X;

import android.graphics.Bitmap;
import java.util.Vector;

/* renamed from: X.0Sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06090Sd {
    public static final Vector A02 = new Vector();
    public int A00;
    public final Object[] A01;

    public C06090Sd(int i) {
        this.A01 = new Object[i];
        A02.add(this);
    }

    public synchronized Object A00() {
        int i = this.A00;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        Object[] objArr = this.A01;
        Object obj = objArr[i2];
        objArr[i2] = null;
        this.A00 = i2;
        return obj;
    }

    public synchronized void A01() {
        Object[] objArr = this.A01;
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            Object obj = objArr[i];
            if (obj != null && (obj instanceof Bitmap)) {
                ((Bitmap) obj).recycle();
            }
            objArr[i] = null;
        }
        this.A00 = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        if (r2 >= r1.length) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0014, code lost:
        r1[r2] = r4;
        r3.A00 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000f, code lost:
        r1 = r3.A01;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A02(java.lang.Object r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r1 = 0
        L_0x0002:
            int r2 = r3.A00     // Catch: all -> 0x001c
            if (r1 >= r2) goto L_0x000f
            java.lang.Object[] r0 = r3.A01     // Catch: all -> 0x001c
            r0 = r0[r1]     // Catch: all -> 0x001c
            if (r0 == r4) goto L_0x001a
            int r1 = r1 + 1
            goto L_0x0002
        L_0x000f:
            java.lang.Object[] r1 = r3.A01     // Catch: all -> 0x001c
            int r0 = r1.length     // Catch: all -> 0x001c
            if (r2 >= r0) goto L_0x001a
            r1[r2] = r4     // Catch: all -> 0x001c
            r0 = 1
            int r2 = r2 + r0
            r3.A00 = r2     // Catch: all -> 0x001c
        L_0x001a:
            monitor-exit(r3)
            return
        L_0x001c:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06090Sd.A02(java.lang.Object):void");
    }
}
