package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.3El  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64153El {
    public AnonymousClass4OL A00;
    public AnonymousClass4LB A01;
    public final int A02;
    public final Context A03;
    public final AnonymousClass4ON A04;
    public final C248116y A05;
    public final AnonymousClass1BD A06;
    public final AnonymousClass01H A07;

    public C64153El(Context context, C248116y r5, AnonymousClass1BD r6, AnonymousClass01H r7, int i, boolean z) {
        this.A03 = context;
        this.A05 = r5;
        this.A06 = r6;
        this.A07 = r7;
        this.A02 = i;
        boolean z2 = true;
        boolean z3 = !C12980iv.A0x(r5.A03.values()).isEmpty();
        this.A04 = new AnonymousClass4ON(z3, (z || !z3) ? false : z2);
        this.A01 = new AnonymousClass4LB(this);
    }

    public final void A00() {
        boolean z;
        AnonymousClass4OL r0 = this.A00;
        if (r0 != null) {
            AnonymousClass4ON r3 = this.A04;
            C69653a1 r2 = r0.A01;
            View view = r2.A01;
            if (view != null && (z = r3.A00) != C12960it.A1T(view.getVisibility())) {
                r2.A00(z, r3.A01);
            }
        }
    }

    public void A01(int i) {
        int i2 = this.A02;
        int i3 = 2;
        if (i != 1) {
            if (i == 2) {
                i3 = 3;
            } else {
                throw C12980iv.A0u(C12960it.A0W(i, "No Constant for Navigation Action: "));
            }
        } else if (i2 != 0) {
            i3 = 4;
        }
        C458223h r0 = this.A06.A00;
        if (r0 != null) {
            r0.A00 = i3;
        }
    }
}
