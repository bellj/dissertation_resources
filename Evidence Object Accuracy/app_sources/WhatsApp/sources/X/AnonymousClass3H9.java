package X;

import java.util.Arrays;

/* renamed from: X.3H9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3H9 {
    public int A00;
    public final int A01;
    public final AnonymousClass2DE A02;

    public AnonymousClass3H9(AnonymousClass2DE r3, int i) {
        int i2;
        this.A02 = r3;
        this.A00 = i;
        if (i > 0) {
            i2 = (r3.A01 - 1) - (i - 1);
        } else {
            i2 = -1;
        }
        this.A01 = i2;
    }

    public static boolean A00(double d) {
        double floor;
        if (d < 0.0d) {
            floor = Math.ceil(d);
        } else {
            floor = Math.floor(d);
        }
        return C12960it.A1T((floor > d ? 1 : (floor == d ? 0 : -1)));
    }

    public final Object A01(int i) {
        int i2 = this.A00;
        boolean z = true;
        if (C12980iv.A1V(i2, -1)) {
            if (i >= i2) {
                z = false;
            }
            C87914Dn.A00("invalid instr stack argument", z);
            return this.A02.A05[this.A01 + i];
        }
        throw C12960it.A0U("InstrStackArgs is not initialized");
    }

    public final void A02(Object obj) {
        int i = this.A00;
        if (C12980iv.A1V(i, -1)) {
            AnonymousClass2DE r3 = this.A02;
            int i2 = r3.A01 - i;
            r3.A01 = i2;
            Arrays.fill(r3.A05, i2, i + i2, (Object) null);
            r3.A0T(obj);
            this.A00 = -1;
            return;
        }
        throw C12960it.A0U("InstrStackArgs is not initialized");
    }
}
