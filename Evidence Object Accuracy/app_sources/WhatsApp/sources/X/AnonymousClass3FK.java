package X;

import android.os.Handler;
import android.widget.FrameLayout;
import com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import com.whatsapp.videoplayback.ExoPlayerErrorFrame;

/* renamed from: X.3FK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FK {
    public RunnableBRunnable0Shape1S1100000_I1 A00;
    public final Handler A01 = new Handler();
    public final ExoPlaybackControlView A02;
    public final ExoPlayerErrorFrame A03;
    public final boolean A04;

    public AnonymousClass3FK(ExoPlaybackControlView exoPlaybackControlView, ExoPlayerErrorFrame exoPlayerErrorFrame, boolean z) {
        this.A03 = exoPlayerErrorFrame;
        this.A02 = exoPlaybackControlView;
        this.A04 = z;
    }

    public void A00() {
        ExoPlayerErrorFrame exoPlayerErrorFrame = this.A03;
        exoPlayerErrorFrame.setLoadingViewVisibility(8);
        RunnableBRunnable0Shape1S1100000_I1 runnableBRunnable0Shape1S1100000_I1 = this.A00;
        if (runnableBRunnable0Shape1S1100000_I1 != null) {
            this.A01.removeCallbacks(runnableBRunnable0Shape1S1100000_I1);
        }
        if (exoPlayerErrorFrame.getErrorScreenVisibility() == 0) {
            ExoPlaybackControlView exoPlaybackControlView = this.A02;
            if (exoPlaybackControlView != null) {
                exoPlaybackControlView.setPlayControlVisibility(0);
            }
            FrameLayout frameLayout = exoPlayerErrorFrame.A02;
            if (frameLayout != null) {
                frameLayout.setVisibility(8);
            }
        }
    }

    public void A01(String str) {
        this.A03.setLoadingViewVisibility(0);
        if (this.A04) {
            RunnableBRunnable0Shape1S1100000_I1 runnableBRunnable0Shape1S1100000_I1 = this.A00;
            if (runnableBRunnable0Shape1S1100000_I1 != null) {
                this.A01.removeCallbacks(runnableBRunnable0Shape1S1100000_I1);
            } else {
                this.A00 = new RunnableBRunnable0Shape1S1100000_I1(5, str, this);
            }
            this.A01.postDelayed(this.A00, 5000);
        }
    }
}
