package X;

/* renamed from: X.5Iv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113745Iv extends AnonymousClass5BW implements AnonymousClass5ZV {
    public AbstractC113745Iv() {
        super(null, AnonymousClass5BW.A01, null, null, false);
    }

    public AbstractC113745Iv(Object obj) {
        super(C88244Ev.class, obj, "classSimpleName", "getClassSimpleName(Ljava/lang/Object;)Ljava/lang/String;", true);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof AbstractC113745Iv) {
                AnonymousClass5BW r4 = (AnonymousClass5BW) obj;
                if (!A00().equals(r4.A00()) || !this.name.equals(r4.name) || !this.signature.equals(r4.signature) || !C16700pc.A0O(this.receiver, r4.receiver)) {
                    return false;
                }
            } else if (!(obj instanceof AnonymousClass5ZV)) {
                return false;
            } else {
                AnonymousClass5ZW r0 = this.A00;
                if (r0 == null) {
                    r0 = this;
                    this.A00 = this;
                }
                return obj.equals(r0);
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((A00().hashCode() * 31) + this.name.hashCode()) * 31) + this.signature.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        AnonymousClass5ZW r0 = this.A00;
        if (r0 == null) {
            r0 = this;
            this.A00 = this;
        }
        if (r0 != this) {
            return r0.toString();
        }
        StringBuilder A0k = C12960it.A0k("property ");
        A0k.append(this.name);
        return C12960it.A0d(" (Kotlin reflection is not available)", A0k);
    }
}
