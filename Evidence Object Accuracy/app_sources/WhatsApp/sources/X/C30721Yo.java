package X;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.1Yo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30721Yo {
    public static HashMap A0F = new HashMap();
    public static HashMap A0G;
    public String A00;
    public String A01;
    public List A02;
    public List A03 = new ArrayList();
    public List A04;
    public List A05;
    public List A06;
    public Map A07;
    public AnonymousClass3DQ A08 = new AnonymousClass3DQ();
    public byte[] A09;
    public final C14650lo A0A;
    public final C15550nR A0B;
    public final C16590pI A0C;
    public final AnonymousClass018 A0D;
    public final C91734Sx A0E = new C91734Sx();

    static {
        HashMap hashMap = new HashMap();
        A0G = hashMap;
        hashMap.put("X-AIM", 0);
        A0G.put("X-MSN", 1);
        A0G.put("X-YAHOO", 2);
        A0G.put("X-GOOGLE-TALK", 5);
        A0G.put("X-GOOGLE TAL", 5);
        A0G.put("X-ICQ", 6);
        A0G.put("X-JABBER", 7);
        A0G.put("X-SKYPE-USERNAME", 3);
        A0F.put("X-AIM", "AIM");
        A0F.put("X-MSN", "Windows Live");
        A0F.put("X-YAHOO", "YAHOO");
        A0F.put("X-GOOGLE-TALK", "Google Talk");
        A0F.put("X-GOOGLE TAL", "Google Talk");
        A0F.put("X-ICQ", "ICQ");
        A0F.put("X-JABBER", "Jabber");
        A0F.put("X-SKYPE-USERNAME", "Skype");
        A0F.put("NICKNAME", "Nickname");
        A0F.put("BDAY", "Birthday");
    }

    public C30721Yo(C14650lo r2, C15550nR r3, C16590pI r4, AnonymousClass018 r5) {
        this.A0C = r4;
        this.A0B = r3;
        this.A0D = r5;
        this.A0A = r2;
    }

    public static String A00(C14650lo r9, C15550nR r10, C16590pI r11, AnonymousClass018 r12, String str) {
        C41411tU A04 = A04(str);
        if (A04 != null) {
            Iterator it = A04.A02.iterator();
            AnonymousClass3FO r4 = null;
            AnonymousClass3FO r3 = null;
            AnonymousClass3FO r2 = null;
            while (it.hasNext()) {
                AnonymousClass3FO r1 = (AnonymousClass3FO) it.next();
                String str2 = r1.A01;
                if (!TextUtils.isEmpty(r1.A02)) {
                    if ("FN".equals(str2)) {
                        r4 = r1;
                    } else if ("NAME".equals(str2)) {
                        r3 = r1;
                    } else if ("ORG".equals(str2) && r2 == null) {
                        r2 = r1;
                    }
                }
            }
            if (r4 != null) {
                return r4.A02;
            }
            if (r3 != null) {
                return r3.A02;
            }
            if (r2 != null) {
                return A01(r2.A03);
            }
            C30721Yo A06 = A06(r9, r10, r11, r12, A04);
            if (A06 != null) {
                return A06.A08();
            }
        }
        return null;
    }

    public static String A01(List list) {
        StringBuilder sb = new StringBuilder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            sb.append((String) it.next());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    public static List A02(C14650lo r8, C15550nR r9, C16590pI r10, AnonymousClass018 r11, List list) {
        C91734Sx r4 = new C91734Sx();
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            C30721Yo A05 = A05(r8, r9, r10, r11, str);
            if (A05 != null) {
                arrayList.add(new C30731Yp(str, A05));
                int i = r4.A01;
                C91734Sx r5 = A05.A0E;
                r4.A01 = i + r5.A01;
                r4.A00 += r5.A00;
                r4.A04 += r5.A04;
                r4.A02 += r5.A02;
                r4.A03 += r5.A03;
            }
        }
        int i2 = r4.A01;
        if (i2 > 0 || r4.A00 > 0) {
            StringBuilder sb = new StringBuilder("contactstruct/construct/too_long=");
            sb.append(i2);
            sb.append("; exceed_max=");
            sb.append(r4.A00);
            Log.w(sb.toString());
        }
        return arrayList;
    }

    public static Map A03(Context context, String str) {
        HashMap hashMap = new HashMap();
        Uri build = ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", context.getString(R.string.app_name)).appendQueryParameter("account_type", "com.whatsapp").build();
        Cursor query = context.getContentResolver().query(build, new String[]{"sync1", "_id"}, "contact_id=?", new String[]{str}, null);
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    String string = query.getString(query.getColumnIndexOrThrow("_id"));
                    UserJid nullable = UserJid.getNullable(query.getString(query.getColumnIndexOrThrow("sync1")));
                    if (nullable != null) {
                        hashMap.put(string, nullable);
                    }
                } catch (Throwable th) {
                    try {
                        query.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        if (query != null) {
            query.close();
        }
        return hashMap;
    }

    public static C41411tU A04(String str) {
        if (str != null) {
            C41391tS r1 = new C41391tS();
            C41401tT r0 = new C41401tT();
            try {
                r1.A01(str, r0);
                List list = r0.A04;
                if (list.size() > 0 && ((C41411tU) list.get(0)).A01.equals("VCARD")) {
                    return (C41411tU) list.get(0);
                }
            } catch (Exception e) {
                Log.e("Error parsing vcard", new C41351tO(e));
                return null;
            }
        }
        return null;
    }

    public static C30721Yo A05(C14650lo r10, C15550nR r11, C16590pI r12, AnonymousClass018 r13, String str) {
        long uptimeMillis = SystemClock.uptimeMillis();
        C41411tU A04 = A04(str);
        long uptimeMillis2 = SystemClock.uptimeMillis();
        if (A04 == null) {
            return null;
        }
        C30721Yo A06 = A06(r10, r11, r12, r13, A04);
        long uptimeMillis3 = SystemClock.uptimeMillis();
        if (A06 == null) {
            return null;
        }
        C91734Sx r2 = A06.A0E;
        r2.A04 = uptimeMillis2 - uptimeMillis;
        r2.A02 = uptimeMillis3 - uptimeMillis2;
        return A06;
    }

    public static C30721Yo A06(C14650lo r21, C15550nR r22, C16590pI r23, AnonymousClass018 r24, C41411tU r25) {
        List list;
        List list2;
        List list3;
        List list4;
        String str;
        String str2;
        AnonymousClass3HC r6;
        if (!r25.A01.equals("VCARD")) {
            Log.e("Non VCARD data is inserted.");
            return null;
        }
        C30721Yo r4 = new C30721Yo(r21, r22, r23, r24);
        Iterator it = r25.A02.iterator();
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (it.hasNext()) {
            AnonymousClass3FO r2 = (AnonymousClass3FO) it.next();
            String str3 = r2.A01;
            if (!TextUtils.isEmpty(r2.A02) && !str3.equals("VERSION")) {
                if (str3.equals("FN")) {
                    r4.A08.A01 = r2.A02;
                } else {
                    if (str3.equals("NAME")) {
                        AnonymousClass3DQ r3 = r4.A08;
                        if (r3.A01 == null) {
                            r3.A01 = r2.A02;
                        }
                    }
                    if (str3.equals("N")) {
                        A07(r2.A03, r4.A08);
                    } else {
                        if (str3.equals("SORT-STRING")) {
                            str = r2.A02;
                        } else {
                            if (!str3.equals("SOUND")) {
                                int i = -1;
                                if (str3.equals("ADR")) {
                                    List list5 = r2.A03;
                                    Iterator it2 = list5.iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            break;
                                        } else if (((String) it2.next()).length() > 0) {
                                            boolean z5 = false;
                                            String str4 = "";
                                            for (String str5 : r2.A04) {
                                                if (str5.equals("PREF") && !z2) {
                                                    z5 = true;
                                                    z2 = true;
                                                } else if (str5.equalsIgnoreCase("HOME")) {
                                                    str4 = "";
                                                    i = 1;
                                                } else if (str5.equalsIgnoreCase("WORK") || str5.equalsIgnoreCase("COMPANY")) {
                                                    str4 = "";
                                                    i = 2;
                                                } else if (!str5.equalsIgnoreCase("POSTAL") && !str5.equalsIgnoreCase("PARCEL") && !str5.equalsIgnoreCase("DOM") && !str5.equalsIgnoreCase("INTL")) {
                                                    if (str5.toUpperCase(Locale.US).startsWith("X-")) {
                                                        if (i < 0) {
                                                            str4 = str5.substring(2);
                                                            i = 0;
                                                        }
                                                    } else if (i < 0) {
                                                        str4 = str5;
                                                        i = 0;
                                                    }
                                                }
                                            }
                                            if (i < 0) {
                                                i = 1;
                                            }
                                            if (list5.size() > 1) {
                                                r6 = new AnonymousClass3HC();
                                                if (list5.size() > 2) {
                                                    r6.A03 = (String) list5.get(2);
                                                }
                                                if (list5.size() > 3) {
                                                    r6.A00 = (String) list5.get(3);
                                                }
                                                if (list5.size() > 4) {
                                                    r6.A02 = (String) list5.get(4);
                                                }
                                                if (list5.size() > 5) {
                                                    r6.A04 = (String) list5.get(5);
                                                }
                                                if (list5.size() > 6) {
                                                    r6.A01 = (String) list5.get(6);
                                                }
                                                if (list5.size() > 7) {
                                                    list5.get(7);
                                                }
                                                str2 = r6.toString().trim();
                                            } else {
                                                str2 = r2.A02;
                                                r6 = null;
                                            }
                                            List list6 = r4.A02;
                                            if (list6 == null) {
                                                list6 = new ArrayList();
                                                r4.A02 = list6;
                                            }
                                            AnonymousClass4TL r1 = new AnonymousClass4TL();
                                            r1.A01 = ContactsContract.CommonDataKinds.StructuredPostal.class;
                                            r1.A00 = i;
                                            r1.A02 = str2;
                                            r1.A04 = r6;
                                            r1.A03 = str4;
                                            r1.A05 = z5;
                                            list6.add(r1);
                                        }
                                    }
                                } else if (str3.equals("ORG")) {
                                    for (String str6 : r2.A04) {
                                        if (str6.equals("PREF") && !z4) {
                                            z4 = true;
                                        }
                                    }
                                    r4.A0B(A01(r2.A03), "");
                                } else if (str3.equals("TITLE") || str3.equals("ROLE")) {
                                    String str7 = r2.A02;
                                    List list7 = r4.A04;
                                    if (list7 == null) {
                                        list7 = new ArrayList();
                                        r4.A04 = list7;
                                    }
                                    int size = list7.size();
                                    if (size == 0) {
                                        r4.A0B("", null);
                                        size = 1;
                                    }
                                    ((C90624Op) r4.A04.get(size - 1)).A01 = str7;
                                } else if (str3.equals("PHOTO")) {
                                    byte[] bytes = r2.A02.getBytes();
                                    r4.A09 = null;
                                    if (bytes != null && bytes.length > 0) {
                                        try {
                                            r4.A09 = Base64.decode(bytes, 0);
                                        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | StringIndexOutOfBoundsException e) {
                                            Log.e("contactstruct/constructcontactfromvnode/base64-decode/error", e);
                                        }
                                    }
                                } else {
                                    String str8 = null;
                                    r8 = null;
                                    C27631Ih A02 = null;
                                    if (str3.equals("LOGO")) {
                                        Log.e("name/LOGO/we_don't_support");
                                    } else if (str3.equals("EMAIL")) {
                                        boolean z6 = false;
                                        for (String str9 : r2.A04) {
                                            if (str9.equals("PREF") && !z3) {
                                                z6 = true;
                                                z3 = true;
                                            } else if (str9.equalsIgnoreCase("HOME")) {
                                                i = 1;
                                            } else if (str9.equalsIgnoreCase("WORK")) {
                                                i = 2;
                                            } else if (str9.equalsIgnoreCase("CELL")) {
                                                i = 4;
                                            } else if (str9.toUpperCase(Locale.US).startsWith("X-")) {
                                                if (i < 0) {
                                                    str8 = str9.substring(2);
                                                    i = 0;
                                                }
                                            } else if (i < 0) {
                                                str8 = str9;
                                                i = 0;
                                            }
                                        }
                                        if (i < 0) {
                                            i = 3;
                                        }
                                        r4.A0C(r2.A02, str8, i, z6);
                                    } else if (str3.equals("TEL")) {
                                        Iterator it3 = r2.A04.iterator();
                                        boolean z7 = false;
                                        boolean z8 = false;
                                        String str10 = "HOME";
                                        while (true) {
                                            if (!it3.hasNext()) {
                                                break;
                                            }
                                            String str11 = (String) it3.next();
                                            if (z7) {
                                                if (str11.equals("HOME")) {
                                                    i = 5;
                                                } else if (str11.equals("WORK")) {
                                                    i = 4;
                                                }
                                            } else if (str11.equals("PREF") && !z) {
                                                z8 = true;
                                                z = true;
                                            } else if (str11.equalsIgnoreCase("HOME")) {
                                                i = 1;
                                            } else if (str11.equalsIgnoreCase("WORK")) {
                                                i = 3;
                                            } else if (str11.equalsIgnoreCase("CELL")) {
                                                i = 2;
                                            } else if (str11.equalsIgnoreCase("PAGER")) {
                                                i = 6;
                                            } else if (str11.equalsIgnoreCase("FAX")) {
                                                z7 = true;
                                            } else if (!str11.equalsIgnoreCase("VOICE") && !str11.equalsIgnoreCase("MSG")) {
                                                if (str11.toUpperCase(Locale.US).startsWith("X-")) {
                                                    if (i < 0) {
                                                        str10 = str11.substring(2);
                                                        i = 0;
                                                    }
                                                } else if (i < 0) {
                                                    str10 = str11;
                                                    i = 0;
                                                }
                                            }
                                        }
                                        if (i < 0) {
                                            i = 1;
                                        }
                                        String asString = r2.A00.getAsString("waId");
                                        if (asString != null) {
                                            try {
                                                A02 = C27631Ih.A02(asString);
                                            } catch (AnonymousClass1MW unused) {
                                            }
                                        }
                                        r4.A0A(A02, r2.A02, str10, i, z8);
                                    } else if (str3.equals("NOTE")) {
                                        r4.A03.add(r2.A02);
                                    } else if (str3.equals("BDAY")) {
                                        String str12 = r2.A02;
                                        if (str12 != null && str12.startsWith("1604")) {
                                            StringBuilder sb = new StringBuilder("-");
                                            sb.append(str12.substring(4));
                                            r2.A02 = sb.toString();
                                        }
                                    } else if (str3.equals("URL")) {
                                        String str13 = r2.A02;
                                        int i2 = -1;
                                        for (String str14 : r2.A04) {
                                            if (str14.equalsIgnoreCase("BLOG")) {
                                                i2 = 2;
                                            } else if (str14.equalsIgnoreCase("FTP")) {
                                                i2 = 6;
                                            } else if (str14.equalsIgnoreCase("HOME")) {
                                                i2 = 4;
                                            } else if (str14.equalsIgnoreCase("HOMEPAGE")) {
                                                i2 = 1;
                                            } else if (str14.equalsIgnoreCase("OTHER")) {
                                                i2 = 7;
                                            } else if (str14.equalsIgnoreCase("PROFILE")) {
                                                i2 = 3;
                                            } else if (str14.equalsIgnoreCase("WORK")) {
                                                i2 = 5;
                                            }
                                        }
                                        List list8 = r4.A06;
                                        if (list8 == null) {
                                            list8 = new ArrayList();
                                            r4.A06 = list8;
                                        }
                                        C90634Oq r0 = new C90634Oq();
                                        r0.A00 = i2;
                                        AnonymousClass009.A05(str13);
                                        r0.A01 = str13;
                                        list8.add(r0);
                                    } else if (!str3.equals("REV") && !str3.equals("UID") && !str3.equals("KEY") && !str3.equals("MAILER") && !str3.equals("TZ") && !str3.equals("GEO") && !str3.equals("NICKNAME") && !str3.equals("CLASS") && !str3.equals("PROFILE") && !str3.equals("CATEGORIES") && !str3.equals("SOURCE") && !str3.equals("PRODID")) {
                                        if (str3.equals("X-PHONETIC-FIRST-NAME")) {
                                            r4.A08.A04 = r2.A02;
                                        } else if (!str3.equals("X-PHONETIC-MIDDLE-NAME")) {
                                            if (str3.equals("X-PHONETIC-LAST-NAME")) {
                                                r4.A08.A05 = r2.A02;
                                            } else if (str3.equals("X-WA-BIZ-NAME")) {
                                                r4.A08.A08 = r2.A02;
                                            } else if (str3.equals("X-WA-BIZ-DESCRIPTION")) {
                                                r4.A01 = r2.A02;
                                            }
                                        }
                                    }
                                }
                            } else if (r2.A04.contains("X-IRMC-N") && r4.A00 == null) {
                                StringBuilder sb2 = new StringBuilder();
                                String str15 = r2.A02;
                                int length = str15.length();
                                for (int i3 = 0; i3 < length; i3++) {
                                    char charAt = str15.charAt(i3);
                                    if (charAt != ';') {
                                        sb2.append(charAt);
                                    }
                                }
                                str = sb2.toString();
                            }
                            r4.A0D(r2);
                        }
                        r4.A00 = str;
                    }
                }
            }
        }
        if (!z && (list4 = r4.A05) != null && list4.size() > 0) {
            ((C30741Yq) r4.A05.get(0)).A04 = true;
        }
        if (!z2 && (list3 = r4.A02) != null) {
            Iterator it4 = list3.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    break;
                }
                AnonymousClass4TL r26 = (AnonymousClass4TL) it4.next();
                if (r26.A01 == ContactsContract.CommonDataKinds.StructuredPostal.class) {
                    r26.A05 = true;
                    break;
                }
            }
        }
        if (!z3 && (list2 = r4.A02) != null) {
            Iterator it5 = list2.iterator();
            while (true) {
                if (!it5.hasNext()) {
                    break;
                }
                AnonymousClass4TL r27 = (AnonymousClass4TL) it5.next();
                if (r27.A01 == ContactsContract.CommonDataKinds.Email.class) {
                    r27.A05 = true;
                    break;
                }
            }
        }
        if (!z4 && (list = r4.A04) != null && list.size() > 0) {
            r4.A04.get(0);
        }
        r4.A09();
        return r4;
    }

    public static void A07(List list, AnonymousClass3DQ r4) {
        int size = list.size();
        if (size > 1) {
            r4.A00 = (String) list.get(0);
            r4.A02 = (String) list.get(1);
            if (size > 2 && ((String) list.get(2)).length() > 0) {
                r4.A03 = (String) list.get(2);
            }
            if (size > 3 && ((String) list.get(3)).length() > 0) {
                r4.A06 = (String) list.get(3);
            }
            if (size > 4 && ((String) list.get(4)).length() > 0) {
                r4.A07 = (String) list.get(4);
            }
        }
    }

    public String A08() {
        String str = this.A08.A01;
        if (str != null) {
            return str;
        }
        List list = this.A04;
        if (list != null && list.size() > 0) {
            return ((C90624Op) this.A04.get(0)).A00;
        }
        List list2 = this.A05;
        if (list2 != null && list2.size() > 0) {
            for (C30741Yq r1 : this.A05) {
                if (r1.A04) {
                    return r1.A02;
                }
            }
        }
        List list3 = this.A02;
        if (list3 == null || list3.size() <= 0) {
            return "";
        }
        for (AnonymousClass4TL r2 : this.A02) {
            if (r2.A01 == ContactsContract.CommonDataKinds.Email.class && r2.A05) {
                return r2.A02;
            }
        }
        return "";
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v4, types: [java.util.Map] */
    public final void A09() {
        boolean z;
        HashMap hashMap;
        String str;
        String A04;
        String str2;
        String str3;
        String str4;
        List list = this.A05;
        if (list != null) {
            ArrayList arrayList = new ArrayList(list.size());
            for (C30741Yq r1 : this.A05) {
                if (r1.A01 == null && (str4 = r1.A02) != null) {
                    int indexOf = str4.indexOf(44);
                    if (indexOf != -1) {
                        str4 = str4.substring(0, indexOf);
                    }
                    arrayList.add(PhoneNumberUtils.stripSeparators(str4.trim()));
                }
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            C21580xe r10 = this.A0B.A06;
            if (arrayList.isEmpty()) {
                hashMap = Collections.emptyMap();
            } else {
                boolean z2 = false;
                if (arrayList.size() <= 10) {
                    z2 = true;
                }
                AnonymousClass009.A0F(z2);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    String str5 = (String) it.next();
                    if (str5 != null) {
                        int length = str5.length();
                        z = true;
                        if (length <= 30) {
                            AnonymousClass009.A0F(z);
                        }
                    }
                    z = false;
                    AnonymousClass009.A0F(z);
                }
                HashMap hashMap2 = new HashMap(arrayList.size());
                StringBuilder sb = new StringBuilder("number IN (");
                int size = arrayList.size() - 1;
                StringBuilder sb2 = new StringBuilder(3 * size);
                for (int i = 0; i < size; i++) {
                    sb2.append("?, ");
                }
                sb.append(sb2.toString());
                sb.append("?)");
                String obj = sb.toString();
                C16310on A01 = ((AbstractC21570xd) r10).A00.get();
                try {
                    Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", obj, null, "CONTACTS", C21580xe.A0A, (String[]) arrayList.toArray(new String[0]));
                    if (A03 != null) {
                        while (A03.moveToNext()) {
                            UserJid nullable = UserJid.getNullable(A03.getString(0));
                            String string = A03.getString(1);
                            if (!(nullable == null || string == null)) {
                                hashMap2.put(string, nullable);
                            }
                        }
                        A03.close();
                    }
                    A01.close();
                    hashMap = hashMap2;
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            this.A0E.A03 = SystemClock.uptimeMillis() - uptimeMillis;
            for (C30741Yq r4 : this.A05) {
                AbstractC14640lm r0 = r4.A01;
                if (r0 == null) {
                    String str6 = r4.A02;
                    int indexOf2 = str6.indexOf(44);
                    if (indexOf2 != -1) {
                        str6 = str6.substring(0, indexOf2);
                    }
                    r0 = (AbstractC14640lm) hashMap.get(PhoneNumberUtils.stripSeparators(str6.trim()));
                    if (r0 == null) {
                        A04 = r4.A02.trim();
                        r4.A02 = A04;
                    }
                }
                A04 = C248917h.A04(r0);
                if (!(A04 == null || (str2 = r4.A02) == null || r4.A01 != null)) {
                    int indexOf3 = str2.indexOf(44);
                    if (indexOf3 != -1) {
                        str3 = str2.substring(indexOf3 + 1);
                    } else {
                        str3 = "";
                    }
                    String stripSeparators = PhoneNumberUtils.stripSeparators(str3.trim());
                    if (!TextUtils.isEmpty(stripSeparators)) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(A04);
                        sb3.append(',');
                        sb3.append(stripSeparators);
                        A04 = sb3.toString();
                    }
                }
                r4.A02 = A04;
            }
            for (C30741Yq r2 : this.A05) {
                if (r2.A00 == 0 && ((str = r2.A03) == null || str.equalsIgnoreCase("null"))) {
                    r2.A03 = this.A0C.A00.getString(R.string.no_phone_type);
                }
            }
        }
    }

    public void A0A(UserJid userJid, String str, String str2, int i, boolean z) {
        if (str == null) {
            StringBuilder sb = new StringBuilder("contactstruct/addphone/data is null; skipping (type=");
            sb.append(i);
            sb.append(" jidFromWaId=");
            sb.append(userJid);
            sb.append(" label=");
            sb.append(str2);
            sb.append(" isPrimary=");
            sb.append(z);
            sb.append(")");
            Log.w(sb.toString());
        } else if (str.length() > 30) {
            this.A0E.A01++;
        } else {
            List list = this.A05;
            if (list == null) {
                list = new ArrayList();
                this.A05 = list;
            }
            if (list.size() >= 10) {
                this.A0E.A00++;
                return;
            }
            C30741Yq r1 = new C30741Yq();
            r1.A00 = i;
            r1.A01 = userJid;
            r1.A02 = str;
            r1.A03 = str2;
            r1.A04 = z;
            this.A05.add(r1);
        }
    }

    public void A0B(String str, String str2) {
        List list = this.A04;
        if (list == null) {
            list = new ArrayList();
            this.A04 = list;
        }
        C90624Op r0 = new C90624Op();
        r0.A00 = str;
        r0.A01 = str2;
        list.add(r0);
    }

    public void A0C(String str, String str2, int i, boolean z) {
        List list = this.A02;
        if (list == null) {
            list = new ArrayList();
            this.A02 = list;
        }
        AnonymousClass4TL r0 = new AnonymousClass4TL();
        r0.A01 = ContactsContract.CommonDataKinds.Email.class;
        r0.A00 = i;
        r0.A02 = str;
        r0.A03 = str2;
        r0.A05 = z;
        list.add(r0);
    }

    public void A0D(AnonymousClass3FO r4) {
        List list;
        String str = r4.A02;
        if (str != null && str.length() != 0) {
            String str2 = r4.A01;
            Map map = this.A07;
            if (map == null) {
                map = new HashMap();
                this.A07 = map;
            }
            if (!map.containsKey(str2)) {
                list = new ArrayList();
                this.A07.put(str2, list);
            } else {
                list = (List) this.A07.get(str2);
            }
            list.add(r4);
        }
    }
}
