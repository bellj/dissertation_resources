package X;

import com.whatsapp.util.Log;

/* renamed from: X.6AT  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AT implements AnonymousClass6MQ {
    public final /* synthetic */ AnonymousClass1ZR A00;
    public final /* synthetic */ AnonymousClass1FK A01;
    public final /* synthetic */ C120515gJ A02;
    public final /* synthetic */ String A03;

    public AnonymousClass6AT(AnonymousClass1ZR r1, AnonymousClass1FK r2, C120515gJ r3, String str) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = str;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r7) {
        C120515gJ r0 = this.A02;
        AnonymousClass1ZR r1 = r7.A02;
        AnonymousClass009.A05(r1);
        String str = r7.A03;
        r0.A00(r1, this.A00, this.A01, str, this.A03);
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r2) {
        Log.w("PAY: IndiaUpiPaymentMethodAction: could not fetch VPA information to remove payment method");
        AnonymousClass1FK r0 = this.A01;
        if (r0 != null) {
            r0.AV3(r2);
        }
    }
}
