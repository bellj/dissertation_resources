package X;

/* renamed from: X.3tK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81013tK<K, V> extends AbstractC17950rf<V> {
    public static final long serialVersionUID = 0;
    public final transient AbstractC80953tE multimap;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public C81013tK(AbstractC80953tE r1) {
        this.multimap = r1;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return this.multimap.containsValue(obj);
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        AnonymousClass1I5 it = this.multimap.map.values().iterator();
        while (it.hasNext()) {
            i = ((AbstractC17950rf) it.next()).copyIntoArray(objArr, i);
        }
        return i;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return this.multimap.valueIterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        return this.multimap.size();
    }
}
