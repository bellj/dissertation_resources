package X;

/* renamed from: X.38E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38E extends AbstractC16350or {
    public boolean A00 = false;
    public final C18790t3 A01;
    public final AnonymousClass2DH A02;
    public final AnonymousClass2K5 A03;
    public final AbstractC18830t7 A04;
    public final C18810t5 A05;
    public final C18800t4 A06;
    public final AnonymousClass1VN A07 = new AnonymousClass1VN(1, 1000);
    public final Integer A08;
    public final Object A09;
    public final String A0A;

    public AnonymousClass38E(C18790t3 r6, AnonymousClass2DH r7, AnonymousClass2K5 r8, AbstractC18830t7 r9, C18810t5 r10, C18800t4 r11, Integer num, Object obj, String str) {
        this.A01 = r6;
        this.A06 = r11;
        this.A04 = r9;
        this.A05 = r10;
        this.A03 = r8;
        this.A0A = str;
        this.A09 = obj;
        this.A02 = r7;
        this.A08 = num;
        if (r9 instanceof C25841Ba) {
            C25841Ba r92 = (C25841Ba) r9;
            r92.A00 = r92.A0A();
            r92.A01 = Long.valueOf(r92.A06.A00());
            r92.A0A.A07(r92.A00);
        }
    }
}
