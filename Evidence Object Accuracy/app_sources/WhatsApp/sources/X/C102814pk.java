package X;

import android.content.Context;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;

/* renamed from: X.4pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102814pk implements AbstractC009204q {
    public final /* synthetic */ GoogleDriveNewUserSetupActivity A00;

    public C102814pk(GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity) {
        this.A00 = googleDriveNewUserSetupActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
