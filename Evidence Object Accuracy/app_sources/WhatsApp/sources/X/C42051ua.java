package X;

import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Map;

/* renamed from: X.1ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42051ua {
    public int A00 = 0;
    public int A01;
    public int A02;
    public int A03 = 0;
    public int A04 = 0;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public C42151uk A09;
    public C42121uh A0A;
    public AnonymousClass1MU A0B;
    public UserJid A0C;
    public String A0D;
    public String A0E = "preview";
    public String A0F;
    public List A0G;
    public Map A0H;
    public boolean A0I;
    public byte[] A0J;
}
