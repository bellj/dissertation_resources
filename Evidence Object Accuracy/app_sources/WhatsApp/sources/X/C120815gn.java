package X;

import android.content.Context;

/* renamed from: X.5gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120815gn extends C120895gv {
    public final /* synthetic */ AnonymousClass2SO A00;
    public final /* synthetic */ C120415g9 A01;
    public final /* synthetic */ C117955b2 A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120815gn(Context context, C14900mE r8, AnonymousClass2SO r9, C18650sn r10, C64513Fv r11, C120415g9 r12, C117955b2 r13) {
        super(context, r8, r10, r11, "deregister-alias");
        this.A01 = r12;
        this.A02 = r13;
        this.A00 = r9;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        this.A01.A03.AKa(r3, 23);
        super.A02(r3);
        C117955b2 r1 = this.A02;
        if (r1 != null) {
            r1.A04(this.A00, r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        this.A01.A03.AKa(r3, 23);
        super.A03(r3);
        C117955b2 r1 = this.A02;
        if (r1 != null) {
            r1.A04(this.A00, r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        AnonymousClass1V8 A0E;
        C117955b2 r2;
        C120415g9 r4 = this.A01;
        r4.A03.AKa(null, 23);
        super.A04(r6);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r6);
        if (A0c != null && (A0E = A0c.A0E("alias")) != null && (r2 = this.A02) != null) {
            try {
                r2.A04(C120895gv.A01(A0E), null);
            } catch (AnonymousClass1V9 unused) {
                r4.A04.A05("onDeregisterVpaAlias/onResponseSuccess/corrupt stream exception");
                r2.A04(null, new C452120p(500));
            }
        }
    }
}
