package X;

/* renamed from: X.1Ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27741Ja extends AbstractC16110oT {
    public Long A00;

    public C27741Ja() {
        super(2516, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdAppStateInitialKeyShare {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "keyCount", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
