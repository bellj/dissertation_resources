package X;

import java.util.List;

/* renamed from: X.1zM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44671zM {
    public AnonymousClass2SK A00;
    public Integer A01;
    public String A02;
    public final String A03;
    public final List A04;

    public C44671zM(AnonymousClass2SK r1, Integer num, String str, String str2, List list) {
        this.A03 = str;
        this.A02 = str2;
        this.A00 = r1;
        this.A01 = num;
        this.A04 = list;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C44671zM) {
                C44671zM r8 = (C44671zM) obj;
                if (AnonymousClass1US.A0D(this.A03, r8.A03) && AnonymousClass1US.A0D(this.A02, r8.A02) && this.A00.equals(r8.A00)) {
                    Integer num = this.A01;
                    Integer num2 = r8.A01;
                    if (num == null ? num2 == null : num.equals(num2)) {
                        List list = this.A04;
                        int size = list.size();
                        List list2 = r8.A04;
                        if (size == list2.size()) {
                            if (list != list2) {
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).equals(list2.get(i))) {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int intValue;
        int hashCode = (((217 + this.A03.hashCode()) * 31) + this.A02.hashCode()) * 31;
        Integer num = this.A01;
        if (num == null) {
            intValue = 0;
        } else {
            intValue = num.intValue();
        }
        int hashCode2 = ((hashCode + intValue) * 31) + this.A00.hashCode();
        for (C44691zO r0 : this.A04) {
            hashCode2 = (hashCode2 * 31) + r0.A0D.hashCode();
        }
        return hashCode2;
    }
}
