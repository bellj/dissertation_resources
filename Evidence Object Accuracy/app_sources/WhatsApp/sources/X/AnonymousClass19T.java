package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.19T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19T {
    public boolean A00;
    public boolean A01;
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final AnonymousClass016 A04 = new AnonymousClass016(Boolean.FALSE);
    public final AbstractC15710nm A05;
    public final C239213n A06;
    public final C14900mE A07;
    public final C15570nT A08;
    public final C18430sR A09;
    public final C14650lo A0A;
    public final C25821Ay A0B;
    public final C19850um A0C;
    public final AnonymousClass19Q A0D;
    public final C92164Uu A0E = new C92164Uu(this);
    public final C20100vD A0F;
    public final C18640sm A0G;
    public final C15680nj A0H;
    public final C14850m9 A0I;
    public final C19870uo A0J;
    public final C17220qS A0K;
    public final C19840ul A0L;
    public final List A0M = new ArrayList();
    public final List A0N = new ArrayList();
    public final Map A0O = new HashMap();

    public AnonymousClass19T(AbstractC15710nm r3, C239213n r4, C14900mE r5, C15570nT r6, C18430sR r7, C14650lo r8, C25821Ay r9, C19850um r10, AnonymousClass19Q r11, C20100vD r12, C18640sm r13, C15680nj r14, C14850m9 r15, C19870uo r16, C17220qS r17, C19840ul r18) {
        this.A0I = r15;
        this.A07 = r5;
        this.A05 = r3;
        this.A08 = r6;
        this.A0L = r18;
        this.A0K = r17;
        this.A0B = r9;
        this.A0C = r10;
        this.A0H = r14;
        this.A0D = r11;
        this.A0A = r8;
        this.A0J = r16;
        this.A0G = r13;
        this.A0F = r12;
        this.A06 = r4;
        this.A09 = r7;
    }

    public void A00(AnonymousClass5Vo r14, C92404Vt r15) {
        C90104Mp r4 = new C90104Mp(r14, this);
        C20100vD r2 = this.A0F;
        C19880up r1 = r2.A01;
        C17560r0 r7 = r1.A05;
        AbstractC14440lR r12 = r1.A0F;
        C17840rU r0 = r1.A06;
        C19810ui r6 = r1.A04;
        r2.A00(new C59232uD(r4, r15, r6, r7, new AnonymousClass4RQ(r6, r0), r1.A07, r1.A08, r1.A09, r12));
    }

    public void A01(AbstractC116475Vp r14, AnonymousClass28H r15) {
        boolean A07;
        C20100vD r3 = this.A0F;
        UserJid userJid = r15.A05;
        C15570nT r0 = r3.A00;
        r0.A08();
        if (userJid.equals(r0.A05) || !this.A0I.A07(1096)) {
            C18430sR r02 = this.A09;
            AnonymousClass3VF r5 = new AnonymousClass3VF(this);
            AnonymousClass01J r2 = r02.A03.A00.A01;
            C17220qS r11 = (C17220qS) r2.ABt.get();
            AnonymousClass3HN r6 = new AnonymousClass3HN(0);
            C15680nj r9 = (C15680nj) r2.A4e.get();
            C18640sm r7 = (C18640sm) r2.A3u.get();
            A07 = new C59072tv((AbstractC15710nm) r2.A4o.get(), (C14650lo) r2.A2V.get(), r5, r6, r7, r15, r9, (C19870uo) r2.A8U.get(), r11, (C19840ul) r2.A1Q.get()).A07();
        } else {
            A07 = r3.A01(new AnonymousClass3VE(this), r15);
        }
        if (A07) {
            this.A0O.put(r15, r14);
        } else {
            r14.AQN(r15, -1);
        }
    }

    public void A02(AnonymousClass281 r4, boolean z) {
        this.A07.A0H(new RunnableBRunnable0Shape0S0210000_I0(this, r4, 4, z));
    }

    public void A03(UserJid userJid, int i) {
        int i2 = 1;
        if (this.A08.A0F(userJid)) {
            i2 = 4;
        }
        int i3 = i2 * 6;
        C19850um r0 = this.A0C;
        r0.A0E(userJid, i3);
        if (r0.A0J(userJid)) {
            for (AbstractC13960kc r02 : this.A0B.A01()) {
                r02.AQI(userJid, true, true);
            }
            i3 <<= 1;
        }
        A05(userJid, i, i3, false);
    }

    public final void A04(UserJid userJid, int i, int i2, boolean z) {
        String str;
        C19850um r2 = this.A0C;
        C44711zQ A02 = r2.A02(userJid);
        if (A02 == null) {
            str = null;
        } else {
            str = A02.A00;
        }
        C44711zQ A022 = r2.A02(userJid);
        if ((A022 == null || A022.A01) && !this.A00) {
            this.A00 = true;
            AnonymousClass4Tm r15 = new AnonymousClass4Tm(AnonymousClass1VY.A00, userJid, this.A0D.A00, str, i, i, i2);
            C63913Dk r9 = new C63913Dk(new AnonymousClass4RO(this, userJid, str, z), this);
            C20100vD r5 = this.A0F;
            UserJid userJid2 = r15.A05;
            C15570nT r0 = r5.A00;
            r0.A08();
            if (userJid2.equals(r0.A05) || !this.A0I.A07(1327)) {
                C19840ul r52 = this.A0L;
                C17220qS r4 = this.A0K;
                AnonymousClass2SJ r3 = new AnonymousClass2SJ(new AnonymousClass3HN(0));
                C15680nj r22 = this.A0H;
                new C59062tu(this.A0A, r9, r15, r3, this.A0G, r22, this.A0J, r4, r52).A06();
                return;
            }
            C19880up r7 = r5.A01;
            C19840ul r42 = r7.A0D;
            C17220qS r8 = r7.A0C;
            AnonymousClass2SJ r6 = new AnonymousClass2SJ(new AnonymousClass3HN(0));
            C15680nj r32 = r7.A0A;
            C18640sm r23 = r7.A09;
            C59062tu r16 = new C59062tu(r7.A02, r9, r15, r6, r23, r32, r7.A0B, r8, r42);
            C17560r0 r11 = r7.A05;
            AbstractC14440lR r02 = r7.A0F;
            C19810ui r10 = r7.A04;
            r5.A00(new C59242uE(r9, r10, r11, new AnonymousClass4RQ(r10, r11), r7.A07, r7.A08, r15, r16, r23, r42, r02));
        }
    }

    public final void A05(UserJid userJid, int i, int i2, boolean z) {
        String str;
        C19850um r1 = this.A0C;
        synchronized (r1) {
            C44661zL r0 = (C44661zL) r1.A01.get(userJid);
            if (r0 != null) {
                C44711zQ r02 = r0.A01;
                if (r02 != null) {
                    str = r02.A00;
                }
            }
            str = null;
        }
        if (A08(userJid)) {
            this.A01 = true;
            if (z) {
                this.A04.A0B(Boolean.TRUE);
            }
            A01(new C68333Uz(this, userJid, str, z), new AnonymousClass28H(userJid, str, this.A0D.A00, i2, i, i));
        }
    }

    public final void A06(UserJid userJid, String str, int i, int i2, boolean z) {
        String str2;
        C19850um r1 = this.A0C;
        C44711zQ A03 = r1.A03(userJid, str);
        if (A03 == null) {
            str2 = null;
        } else {
            str2 = A03.A00;
        }
        C44711zQ A032 = r1.A03(userJid, str);
        if (A032 == null || A032.A01) {
            AnonymousClass016 r12 = this.A04;
            if (r12.A01() == null || !((Boolean) r12.A01()).booleanValue()) {
                r12.A0B(Boolean.TRUE);
                C91864Tn r122 = new C91864Tn(userJid, str, this.A0D.A00, str2, i2, i, i, z);
                C63923Dl r6 = new C63923Dl(new AnonymousClass4RP(this, userJid, str2, str), this);
                C20100vD r3 = this.A0F;
                UserJid userJid2 = r122.A03;
                C15570nT r0 = r3.A00;
                r0.A08();
                if (userJid2.equals(r0.A05) || !this.A0I.A07(1327)) {
                    C19840ul r4 = this.A0L;
                    C17220qS r32 = this.A0K;
                    C18640sm r2 = this.A0G;
                    new C59022tq(this.A0A, r6, r122, new AnonymousClass2SJ(new AnonymousClass3HN(0)), r2, this.A0J, r32, r4).A02();
                    return;
                }
                C19880up r42 = r3.A01;
                C19840ul r15 = r42.A0D;
                C17220qS r5 = r42.A0C;
                C18640sm r14 = r42.A09;
                C59022tq r13 = new C59022tq(r42.A02, r6, r122, new AnonymousClass2SJ(new AnonymousClass3HN(0)), r14, r42.A0B, r5, r15);
                C17560r0 r8 = r42.A05;
                AbstractC14440lR r02 = r42.A0F;
                C19810ui r7 = r42.A04;
                r3.A00(new C59252uF(r6, r7, r8, new AnonymousClass4RQ(r7, r8), r42.A07, r42.A08, r122, r13, r14, r15, r02));
            }
        }
    }

    public boolean A07(AnonymousClass4TA r32) {
        C20100vD r13 = this.A0F;
        UserJid userJid = r32.A00;
        C15570nT r0 = r13.A00;
        r0.A08();
        if (userJid.equals(r0.A05) || !this.A0I.A07(1096)) {
            C18430sR r02 = this.A09;
            C92164Uu r6 = this.A0E;
            AnonymousClass01J r3 = r02.A01.A00.A01;
            C19850um r5 = (C19850um) r3.A2v.get();
            C15680nj r9 = (C15680nj) r3.A4e.get();
            C59042ts r33 = new C59042ts((C14650lo) r3.A2V.get(), r5, r6, new AnonymousClass3HN(0), (C18640sm) r3.A3u.get(), r9, r32, (C19870uo) r3.A8U.get(), (C17220qS) r3.ABt.get(), (C19840ul) r3.A1Q.get());
            if (!r33.A03.A0B()) {
                r33.A01.A00(r33.A05, -1);
                return false;
            } else if (((AnonymousClass2EI) r33).A01.A08()) {
                r33.A02();
                return true;
            } else {
                r33.A03();
                return true;
            }
        } else {
            C92164Uu r03 = this.A0E;
            C19880up r1 = r13.A01;
            C19840ul r15 = r1.A0D;
            C15680nj r11 = r1.A0A;
            C19850um r10 = r1.A03;
            C17560r0 r92 = r1.A05;
            C19830uk r8 = r1.A08;
            AbstractC14440lR r7 = r1.A0F;
            C17220qS r62 = r1.A0C;
            C18640sm r52 = r1.A09;
            C19810ui r4 = r1.A04;
            return r13.A00(new C59262uG(r1.A02, r10, r03, (AnonymousClass19T) r1.A0G.get(), r4, r92, new AnonymousClass4RQ(r4, r92), r1.A07, r8, r52, r11, r32, r1.A0B, r62, r15, r7));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r0.A01 == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A08(com.whatsapp.jid.UserJid r3) {
        /*
            r2 = this;
            X.0um r1 = r2.A0C
            monitor-enter(r1)
            java.util.Map r0 = r1.A01     // Catch: all -> 0x0020
            java.lang.Object r0 = r0.get(r3)     // Catch: all -> 0x0020
            X.1zL r0 = (X.C44661zL) r0     // Catch: all -> 0x0020
            if (r0 == 0) goto L_0x0011
            X.1zQ r0 = r0.A01     // Catch: all -> 0x0020
            monitor-exit(r1)     // Catch: all -> 0x0020
            goto L_0x0013
        L_0x0011:
            monitor-exit(r1)     // Catch: all -> 0x0020
            goto L_0x0019
        L_0x0013:
            if (r0 == 0) goto L_0x0019
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x001e
        L_0x0019:
            boolean r1 = r2.A01
            r0 = 1
            if (r1 == 0) goto L_0x001f
        L_0x001e:
            r0 = 0
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0020
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19T.A08(com.whatsapp.jid.UserJid):boolean");
    }
}
