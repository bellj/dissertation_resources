package X;

import android.animation.ValueAnimator;

/* renamed from: X.0ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09280ce implements Runnable {
    public final /* synthetic */ AnonymousClass0F7 A00;

    public RunnableC09280ce(AnonymousClass0F7 r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0F7 r2 = this.A00;
        int i = r2.A02;
        if (i == 1) {
            r2.A0K.cancel();
        } else if (i != 2) {
            return;
        }
        r2.A02 = 3;
        ValueAnimator valueAnimator = r2.A0K;
        valueAnimator.setFloatValues(((Number) valueAnimator.getAnimatedValue()).floatValue(), 0.0f);
        valueAnimator.setDuration((long) 500);
        valueAnimator.start();
    }
}
