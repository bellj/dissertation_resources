package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/* renamed from: X.1Su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29461Su implements Parcelable {
    public static final C29461Su A06 = new C29461Su(null, null);
    public static final Parcelable.Creator CREATOR = new C98504in();
    public final int A00;
    public final TreeMap A01;
    public final TreeMap A02;
    public final TreeMap A03;
    public final TreeMap A04;
    public final TreeMap A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C29461Su(Parcel parcel) {
        this.A00 = -1;
        Bundle readBundle = parcel.readBundle(C29461Su.class.getClassLoader());
        Set<String> keySet = readBundle.keySet();
        if (!keySet.isEmpty()) {
            this.A03 = new TreeMap();
            for (String str : keySet) {
                this.A03.put(str, Integer.valueOf(readBundle.getInt(str)));
            }
        } else {
            this.A03 = null;
        }
        Bundle readBundle2 = parcel.readBundle(C29461Su.class.getClassLoader());
        Set<String> keySet2 = readBundle2.keySet();
        if (!keySet2.isEmpty()) {
            this.A01 = new TreeMap();
            for (String str2 : keySet2) {
                this.A01.put(str2, Boolean.valueOf(readBundle2.getBoolean(str2)));
            }
        } else {
            this.A01 = null;
        }
        Bundle readBundle3 = parcel.readBundle(C29461Su.class.getClassLoader());
        Set<String> keySet3 = readBundle3.keySet();
        if (!keySet3.isEmpty()) {
            this.A02 = new TreeMap();
            for (String str3 : keySet3) {
                this.A02.put(str3, readBundle3.getIntArray(str3));
            }
        } else {
            this.A02 = null;
        }
        Bundle readBundle4 = parcel.readBundle(C29461Su.class.getClassLoader());
        Set<String> keySet4 = readBundle4.keySet();
        if (!keySet4.isEmpty()) {
            this.A04 = new TreeMap();
            for (String str4 : keySet4) {
                this.A04.put(str4, readBundle4.getStringArrayList(str4));
            }
        } else {
            this.A04 = null;
        }
        Bundle readBundle5 = parcel.readBundle(C29461Su.class.getClassLoader());
        Set<String> keySet5 = readBundle5.keySet();
        if (!keySet5.isEmpty()) {
            this.A05 = new TreeMap();
            for (String str5 : keySet5) {
                this.A05.put(str5, new String(readBundle5.getCharArray(str5)));
            }
            return;
        }
        this.A05 = null;
    }

    public C29461Su(TreeMap treeMap, TreeMap treeMap2) {
        this.A03 = treeMap;
        this.A01 = treeMap2;
        this.A02 = null;
        this.A04 = null;
        this.A05 = null;
        this.A00 = -1;
    }

    public int A00(String str, int i) {
        Number number;
        TreeMap treeMap = this.A03;
        if (treeMap == null || (number = (Number) treeMap.get(str)) == null) {
            return i;
        }
        return number.intValue();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        TreeMap treeMap = this.A03;
        TreeMap treeMap2 = this.A01;
        TreeMap treeMap3 = this.A02;
        TreeMap treeMap4 = this.A04;
        TreeMap treeMap5 = this.A05;
        Bundle bundle = new Bundle();
        if (treeMap != null) {
            for (Map.Entry entry : treeMap.entrySet()) {
                bundle.putInt((String) entry.getKey(), ((Number) entry.getValue()).intValue());
            }
        }
        parcel.writeBundle(bundle);
        Bundle bundle2 = new Bundle();
        if (treeMap2 != null) {
            for (Map.Entry entry2 : treeMap2.entrySet()) {
                bundle2.putBoolean((String) entry2.getKey(), ((Boolean) entry2.getValue()).booleanValue());
            }
        }
        parcel.writeBundle(bundle2);
        Bundle bundle3 = new Bundle();
        if (treeMap3 != null) {
            for (Map.Entry entry3 : treeMap3.entrySet()) {
                bundle3.putIntArray((String) entry3.getKey(), (int[]) entry3.getValue());
            }
        }
        parcel.writeBundle(bundle3);
        Bundle bundle4 = new Bundle();
        if (treeMap4 != null) {
            for (Map.Entry entry4 : treeMap4.entrySet()) {
                bundle4.putStringArrayList((String) entry4.getKey(), (ArrayList) entry4.getValue());
            }
        }
        parcel.writeBundle(bundle4);
        Bundle bundle5 = new Bundle();
        if (treeMap5 != null) {
            for (Map.Entry entry5 : treeMap5.entrySet()) {
                bundle5.putCharArray((String) entry5.getKey(), ((String) entry5.getValue()).toCharArray());
            }
        }
        parcel.writeBundle(bundle5);
    }
}
