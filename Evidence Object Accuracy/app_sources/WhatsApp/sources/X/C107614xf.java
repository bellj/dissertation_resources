package X;

/* renamed from: X.4xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107614xf implements AbstractC116795Wx {
    public boolean A00;
    public final AbstractC116795Wx A01;
    public final /* synthetic */ C67673Sk A02;

    public C107614xf(C67673Sk r1, AbstractC116795Wx r2) {
        this.A02 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116795Wx
    public boolean AJx() {
        return !this.A02.A00() && this.A01.AJx();
    }

    @Override // X.AbstractC116795Wx
    public void ALR() {
        this.A01.ALR();
    }

    @Override // X.AbstractC116795Wx
    public int AZs(C89864Lr r11, C76763mA r12, boolean z) {
        C67673Sk r1 = this.A02;
        if (r1.A00()) {
            return -3;
        }
        if (this.A00) {
            r12.flags = 4;
            return -4;
        }
        int AZs = this.A01.AZs(r11, r12, z);
        if (AZs == -5) {
            C100614mC r5 = r11.A00;
            int i = r5.A07;
            if (!(i == 0 && r5.A08 == 0)) {
                int i2 = 0;
                if (r1.A00 == Long.MIN_VALUE) {
                    i2 = r5.A08;
                }
                C93844ap r13 = new C93844ap(r5);
                r13.A05 = i;
                r13.A06 = i2;
                r11.A00 = new C100614mC(r13);
            }
            return -5;
        }
        long j = r1.A00;
        if (j == Long.MIN_VALUE || (AZs != -4 ? !(AZs == -3 && r1.AB2() == Long.MIN_VALUE && !r12.A03) : r12.A00 < j)) {
            return AZs;
        }
        r12.clear();
        r12.flags = 4;
        this.A00 = true;
        return -4;
    }

    @Override // X.AbstractC116795Wx
    public int Ae2(long j) {
        if (this.A02.A00()) {
            return -3;
        }
        return this.A01.Ae2(j);
    }
}
