package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1nc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38161nc {
    public final C15550nR A00;
    public final C15610nY A01;
    public final C16590pI A02;

    public C38161nc(C15550nR r1, C15610nY r2, C16590pI r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public String A00(AnonymousClass1IR r6, String str) {
        String str2;
        AbstractC30891Zf r0;
        UserJid userJid = r6.A0D;
        if ((userJid == null || (str2 = this.A01.A08(this.A00.A0B(userJid))) == null) && ((r0 = r6.A0A) == null || TextUtils.isEmpty(r0.A0F()) || (str2 = r6.A0A.A0F()) == null)) {
            str2 = this.A02.A00.getString(R.string.unknown_payment_recipient);
        }
        return this.A02.A00.getString(R.string.payments_request_message_to_me_success, str2, str);
    }
}
