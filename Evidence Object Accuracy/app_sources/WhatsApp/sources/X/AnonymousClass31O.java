package X;

/* renamed from: X.31O  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31O extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;

    public AnonymousClass31O() {
        super(1630, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(16, this.A03);
        r3.Abe(15, this.A00);
        r3.Abe(7, this.A04);
        r3.Abe(8, this.A01);
        r3.Abe(6, this.A08);
        r3.Abe(4, this.A09);
        r3.Abe(2, this.A0A);
        r3.Abe(1, this.A05);
        r3.Abe(18, this.A0B);
        r3.Abe(9, this.A06);
        r3.Abe(10, this.A02);
        r3.Abe(11, this.A0C);
        r3.Abe(5, this.A0D);
        r3.Abe(19, this.A0E);
        r3.Abe(12, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCatalogView {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizPlatform", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cartToggle", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogEntryPoint", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogEventSampled", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogOwnerJid", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogReportReasonCode", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogViewAction", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "collectionIndex", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkOpenFrom", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isOrderMsgAttached", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "orderId", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productId", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productIndex", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "quantity", this.A07);
        return C12960it.A0d("}", A0k);
    }
}
