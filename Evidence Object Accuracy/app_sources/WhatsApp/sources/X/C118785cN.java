package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.5cN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118785cN extends AnonymousClass03U {
    public final WaImageView A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final WaTextView A03;
    public final C37071lG A04;

    public C118785cN(View view, C37071lG r3) {
        super(view);
        this.A00 = C12980iv.A0X(view, R.id.item_thumbnail);
        this.A03 = C12960it.A0N(view, R.id.item_title);
        this.A02 = C12960it.A0N(view, R.id.item_quantity);
        this.A01 = C12960it.A0N(view, R.id.item_price);
        this.A04 = r3;
    }
}
