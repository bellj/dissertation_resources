package X;

/* renamed from: X.0t4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18800t4 {
    public AnonymousClass1NT A00;
    public AnonymousClass2UX A01;
    public AnonymousClass1NX A02;
    public AnonymousClass1NX A03;
    public AnonymousClass1NW A04;
    public final C15450nH A05;
    public final C16590pI A06;
    public final C14850m9 A07;
    public final AnonymousClass14I A08;
    public final AnonymousClass14H A09;
    public final AnonymousClass14N A0A;
    public final AnonymousClass14M A0B;
    public final AnonymousClass14L A0C;
    public final AnonymousClass14F A0D;
    public final AnonymousClass14G A0E;
    public final AnonymousClass14K A0F;
    public final AnonymousClass14O A0G;
    public final String A0H = "TLS_AES_128_GCM_SHA256";

    public C18800t4(C15450nH r2, C16590pI r3, C14850m9 r4, AnonymousClass14I r5, AnonymousClass14H r6, AnonymousClass14N r7, AnonymousClass14M r8, AnonymousClass14L r9, AnonymousClass14F r10, AnonymousClass14G r11, AnonymousClass14K r12, AnonymousClass14O r13) {
        this.A07 = r4;
        this.A06 = r3;
        this.A05 = r2;
        this.A0B = r8;
        this.A0F = r12;
        this.A08 = r5;
        this.A0C = r9;
        this.A09 = r6;
        this.A0E = r11;
        this.A0A = r7;
        this.A0D = r10;
        this.A0G = r13;
    }

    public synchronized AnonymousClass1NT A00() {
        AnonymousClass1NT r2;
        r2 = this.A00;
        if (r2 == null) {
            r2 = new AnonymousClass1NT(this.A06.A00, this.A08);
            this.A00 = r2;
        }
        return r2;
    }

    public synchronized AnonymousClass1NX A01(boolean z) {
        AnonymousClass1NX r0;
        if (z) {
            r0 = this.A03;
            if (r0 == null) {
                C14850m9 r2 = this.A07;
                C15450nH r1 = this.A05;
                AnonymousClass14M r5 = this.A0B;
                AnonymousClass14K r9 = this.A0F;
                AnonymousClass14L r6 = this.A0C;
                r0 = new AnonymousClass1NY(r1, r2, this.A09, this.A0A, r5, r6, this.A0D, this.A0E, r9, this.A0G, this.A0H);
                this.A03 = r0;
            }
        } else {
            r0 = this.A02;
            if (r0 == null) {
                C14850m9 r22 = this.A07;
                C15450nH r12 = this.A05;
                AnonymousClass14M r52 = this.A0B;
                AnonymousClass14K r92 = this.A0F;
                AnonymousClass14L r62 = this.A0C;
                r0 = new AnonymousClass1NZ(r12, r22, this.A09, this.A0A, r52, r62, this.A0D, this.A0E, r92, this.A0G, this.A0H);
                this.A02 = r0;
            }
        }
        return r0;
    }

    public synchronized AnonymousClass1NW A02() {
        AnonymousClass1NW r2;
        r2 = this.A04;
        if (r2 == null) {
            r2 = new AnonymousClass1NW(this.A06.A00, this.A08);
            this.A04 = r2;
        }
        return r2;
    }
}
