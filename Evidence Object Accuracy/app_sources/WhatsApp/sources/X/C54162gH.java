package X;

import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.contact.picker.PhoneContactsSelector;

/* renamed from: X.2gH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54162gH extends AnonymousClass02M {
    public final /* synthetic */ PhoneContactsSelector A00;

    public /* synthetic */ C54162gH(PhoneContactsSelector phoneContactsSelector) {
        this.A00 = phoneContactsSelector;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.A0c.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r6, int i) {
        C75513jy r62 = (C75513jy) r6;
        PhoneContactsSelector phoneContactsSelector = this.A00;
        C51492Vb r3 = (C51492Vb) phoneContactsSelector.A0c.get(i);
        String str = r3.A05;
        boolean isEmpty = TextUtils.isEmpty(str);
        TextView textView = r62.A01;
        if (!isEmpty) {
            textView.setText(str);
        } else {
            textView.setText(r3.A06);
        }
        ThumbnailButton thumbnailButton = r62.A02;
        phoneContactsSelector.A0C.A05(thumbnailButton, R.drawable.avatar_contact);
        phoneContactsSelector.A0F.A05(thumbnailButton, r3);
        C12960it.A13(r62.A00, this, r3, 34);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75513jy(C12960it.A0F(this.A00.getLayoutInflater(), viewGroup, R.layout.selected_contact));
    }
}
