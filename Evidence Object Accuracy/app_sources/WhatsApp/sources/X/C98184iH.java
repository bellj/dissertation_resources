package X;

import android.media.audiofx.Visualizer;

/* renamed from: X.4iH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98184iH implements Visualizer.OnDataCaptureListener {
    public final /* synthetic */ C35191hP A00;

    @Override // android.media.audiofx.Visualizer.OnDataCaptureListener
    public void onFftDataCapture(Visualizer visualizer, byte[] bArr, int i) {
    }

    public C98184iH(C35191hP r1) {
        this.A00 = r1;
    }

    @Override // android.media.audiofx.Visualizer.OnDataCaptureListener
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] bArr, int i) {
        AbstractC116065Tz r0 = this.A00.A0L;
        if (r0 != null) {
            r0.AYS(bArr);
        }
    }
}
