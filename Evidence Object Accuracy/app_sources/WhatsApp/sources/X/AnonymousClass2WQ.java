package X;

/* renamed from: X.2WQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2WQ extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;

    public AnonymousClass2WQ() {
        super(3300, new AnonymousClass00E(1, 200, 1000), 2, 56300709);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A00);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamPsAppLaunch {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psAppLaunchCpuT", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psAppLaunchDestination", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psAppLaunchT", this.A03);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psAppLaunchTypeT", obj2);
        sb.append("}");
        return sb.toString();
    }
}
