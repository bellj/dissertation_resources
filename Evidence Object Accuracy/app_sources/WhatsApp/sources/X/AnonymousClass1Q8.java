package X;

import android.content.Context;
import android.os.Build;
import com.facebook.soloader.SoLoader;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1Q8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q8 {
    public static boolean A00;

    public static synchronized void A00(Context context) {
        AnonymousClass12A r0;
        synchronized (AnonymousClass1Q8.class) {
            if (A00) {
                Log.i("whatsappsoloader/init: already initialized");
            } else {
                if (Build.VERSION.SDK_INT < 23) {
                    String A02 = AnonymousClass1HP.A02();
                    if (!"armeabi-v7a".equals(A02) && !"x86".equals(A02)) {
                        r0 = AnonymousClass12A.A00();
                        SoLoader.A02(context, r0, 0);
                        AnonymousClass1QF r6 = new AnonymousClass1QF(new File(context.getFilesDir(), "decompressed/libs.spk.zst"), 1);
                        ReentrantReadWriteLock reentrantReadWriteLock = SoLoader.A09;
                        reentrantReadWriteLock.writeLock().lock();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Prepending to SO sources: ");
                        sb.append(r6);
                        android.util.Log.d("SoLoader", sb.toString());
                        SoLoader.A01();
                        r6.A04(SoLoader.A00());
                        AnonymousClass1QG[] r2 = SoLoader.A04;
                        int length = r2.length;
                        AnonymousClass1QG[] r02 = new AnonymousClass1QG[length + 1];
                        r02[0] = r6;
                        System.arraycopy(r2, 0, r02, 1, length);
                        SoLoader.A04 = r02;
                        SoLoader.A0B++;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Prepended to SO sources: ");
                        sb2.append(r6);
                        android.util.Log.d("SoLoader", sb2.toString());
                        reentrantReadWriteLock.writeLock().unlock();
                        A00 = true;
                    }
                }
                r0 = null;
                SoLoader.A02(context, r0, 0);
                AnonymousClass1QF r6 = new AnonymousClass1QF(new File(context.getFilesDir(), "decompressed/libs.spk.zst"), 1);
                ReentrantReadWriteLock reentrantReadWriteLock = SoLoader.A09;
                reentrantReadWriteLock.writeLock().lock();
                StringBuilder sb = new StringBuilder();
                sb.append("Prepending to SO sources: ");
                sb.append(r6);
                android.util.Log.d("SoLoader", sb.toString());
                SoLoader.A01();
                r6.A04(SoLoader.A00());
                AnonymousClass1QG[] r2 = SoLoader.A04;
                int length = r2.length;
                AnonymousClass1QG[] r02 = new AnonymousClass1QG[length + 1];
                r02[0] = r6;
                System.arraycopy(r2, 0, r02, 1, length);
                SoLoader.A04 = r02;
                SoLoader.A0B++;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Prepended to SO sources: ");
                sb2.append(r6);
                android.util.Log.d("SoLoader", sb2.toString());
                reentrantReadWriteLock.writeLock().unlock();
                A00 = true;
            }
        }
    }
}
