package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.bloks.ui.BloksDialogFragment;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.chromium.net.UrlRequest;

/* renamed from: X.5jc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121705jc extends AbstractActivityC119635el implements AnonymousClass1SM {
    public C25841Ba A00;
    public AnonymousClass1BZ A01;
    public C248917h A02;
    public C15890o4 A03;
    public C15650ng A04;
    public C20370ve A05;
    public C20660w7 A06;
    public C129925yW A07;
    public C124945qQ A08;
    public C21860y6 A09;
    public C18650sn A0A;
    public C18660so A0B;
    public C18600si A0C;
    public C18610sj A0D;
    public C17900ra A0E;
    public C18620sk A0F;
    public C17070qD A0G;
    public C129135xE A0H;
    public C128135vc A0I;
    public AnonymousClass60T A0J;
    public AnonymousClass17V A0K;
    public C30931Zj A0L = C117305Zk.A0V("PayBloksActivity", "bloks");
    public C129095xA A0M;
    public AnonymousClass61E A0N;
    public AnonymousClass605 A0O;
    public C130015yf A0P;
    public C125055qc A0Q;
    public C118025b9 A0R;
    public C129395xe A0S;
    public C18590sh A0T;
    public AnonymousClass1BY A0U;
    public C22120yY A0V;
    public C252018m A0W;
    public boolean A0X = false;

    public static /* synthetic */ AbstractC17450qp A0Z(AbstractActivityC121705jc r4) {
        return new AnonymousClass66W(super.A2e().AAO(), new C124895qI(), null);
    }

    public static void A0l(AnonymousClass3FE r2, Map map, int i) {
        if (map == null) {
            map = C12970iu.A11();
        }
        map.put("error_code", String.valueOf(i));
        r2.A01("on_failure", map);
    }

    public AbstractC17450qp A2h() {
        return super.A2e().AAO();
    }

    public void A2i() {
        if (!(this instanceof AbstractActivityC123635nW)) {
            A2m(null);
            return;
        }
        AbstractActivityC123635nW r1 = (AbstractActivityC123635nW) this;
        if (!((AbstractActivityC121705jc) r1).A00.A0E() || !((AbstractActivityC121705jc) r1).A00.A0F()) {
            C1310561a.A04(r1, ((ActivityC13810kN) r1).A05, ((AbstractActivityC121705jc) r1).A0A, ((AbstractActivityC121705jc) r1).A0D, new IDxAListenerShape19S0100000_3_I1(r1, 18), r1.A0A);
            return;
        }
        r1.A2m(null);
    }

    public final void A2j() {
        if (!(!((ActivityC13810kN) this).A0E)) {
            Bundle A0H = C12990iw.A0H(this);
            AnonymousClass009.A05(A0H);
            String string = A0H.getString("screen_name");
            HashMap hashMap = (HashMap) A0H.getSerializable("screen_params");
            AnonymousClass01F A0V = A0V();
            ((AbstractActivityC119645em) this).A05 = BloksDialogFragment.A00(string, hashMap);
            if (hashMap != null) {
                ((AbstractActivityC119645em) this).A09.A05(hashMap);
            }
            if (A0V.A03() == 0) {
                C004902f r2 = new C004902f(A0V);
                r2.A07(((AbstractActivityC119645em) this).A05, R.id.bloks_fragment_container);
                r2.A0F(string);
                r2.A02();
                return;
            }
            A2g();
            return;
        }
        this.A0X = true;
    }

    public void A2k(View.OnClickListener onClickListener, boolean z, boolean z2) {
        AnonymousClass018 r1 = ((ActivityC13830kP) this).A01;
        int i = R.drawable.ic_back;
        if (z) {
            i = R.drawable.ic_close;
        }
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(this, r1, i);
        Resources resources = getResources();
        int i2 = R.color.dark_gray;
        if (z2) {
            i2 = R.color.white;
        }
        C117305Zk.A13(resources, A00, i2);
        Toolbar A08 = C117305Zk.A08(this);
        A08.setNavigationIcon(A00);
        if (onClickListener != null) {
            A08.setNavigationOnClickListener(onClickListener);
        }
    }

    public void A2l(AnonymousClass3FE r12) {
        C128135vc r2 = this.A0I;
        C126975tk r1 = new C126975tk(r12, this);
        r2.A00 = "PENDING";
        Context context = r2.A03.A00;
        C14900mE r5 = r2.A01;
        C129565xv r9 = r2.A06;
        C128055vU r3 = new C128055vU(context, r5, r2.A02, r2.A04, r2.A05, r9);
        C128725wZ r8 = new C128725wZ(r2, r1);
        AnonymousClass1W9[] r22 = new AnonymousClass1W9[1];
        C117305Zk.A1O("action", "get-account-eligibility-state", r22);
        C117305Zk.A1I(r3.A04, new IDxRCallbackShape0S0200000_3_I1(r3.A00, r3.A01, r3.A03, r8, r3, 9), C117315Zl.A0G(r22));
    }

    public void A2m(AnonymousClass2K5 r5) {
        if (!this.A00.A0E() || !this.A00.A0F()) {
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.bloks_progress_bar);
            progressBar.setVisibility(0);
            this.A00.A0C(new AnonymousClass68I(progressBar, this), r5, "on_demand", false);
            return;
        }
        this.A0X = true;
    }

    @Override // X.AbstractC136496Mt
    public boolean AHz(int i) {
        if (i != 404 && i != 440 && i != 449) {
            return false;
        }
        this.A0L.A06(C12960it.A0W(i, "handleError/error="));
        this.A0F.A01(true, false);
        C004802e A0S = C12980iv.A0S(this);
        A0S.A06(R.string.payments_generic_error);
        A0S.A0B(false);
        C117295Zj.A0q(A0S, this, 69, R.string.ok);
        A0S.A05();
        return true;
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
    }

    @Override // X.AbstractC136496Mt
    public void AZC(AnonymousClass3FE r25, String str, Map map) {
        float f;
        BloksDialogFragment bloksDialogFragment;
        Boolean bool;
        AbstractActivityC119225dN.A0M(r25, str);
        short s = -1;
        switch (str.hashCode()) {
            case -2131583866:
                s = C117305Zk.A0t("change_pin", str);
                break;
            case -1828362259:
                s = C117305Zk.A0u("get_compliance_status", str);
                break;
            case -1432382994:
                s = C117305Zk.A0v("get_oldest_credential", str);
                break;
            case -1371677349:
                s = C117305Zk.A0w("remove_completed_step", str);
                break;
            case -1191424321:
                if (str.equals("get_abprop_value")) {
                    s = 4;
                    break;
                }
                break;
            case -1032682289:
                if (str.equals("verify_pin")) {
                    s = 5;
                    break;
                }
                break;
            case -457979232:
                if (str.equals("set_action_bar_title")) {
                    s = 6;
                    break;
                }
                break;
            case -214858504:
                if (str.equals("compliance_name_check")) {
                    s = 7;
                    break;
                }
                break;
            case 20864489:
                if (str.equals("reinitialize_payments")) {
                    s = 8;
                    break;
                }
                break;
            case 205988285:
                if (str.equals("set_completed_step")) {
                    s = 9;
                    break;
                }
                break;
            case 254954716:
                if (str.equals("compliance_dob_check")) {
                    s = 10;
                    break;
                }
                break;
            case 391773106:
                if (str.equals("check_camera_permission")) {
                    s = 11;
                    break;
                }
                break;
            case 641482247:
                if (str.equals("get_incentive_data")) {
                    s = 12;
                    break;
                }
                break;
            case 761629426:
                if (str.equals("remove_credential")) {
                    s = 13;
                    break;
                }
                break;
            case 927713295:
                if (str.equals("forward_to_payment_screen")) {
                    s = 14;
                    break;
                }
                break;
            case 928063522:
                if (str.equals("sync_incentive_data")) {
                    s = 15;
                    break;
                }
                break;
            case 1032047561:
                if (str.equals("get_methods")) {
                    s = 16;
                    break;
                }
                break;
            case 1369547730:
                if (str.equals("create_pin")) {
                    s = 17;
                    break;
                }
                break;
            case 1853333482:
                if (str.equals("set_sandbox")) {
                    s = 18;
                    break;
                }
                break;
            case 1877943783:
                if (str.equals("set_navigation_icon")) {
                    s = 19;
                    break;
                }
                break;
            case 1985308587:
                if (str.equals("set_bio")) {
                    s = 20;
                    break;
                }
                break;
        }
        String str2 = "on_success";
        String str3 = "1";
        switch (s) {
            case 0:
                AnonymousClass605 r7 = this.A0O;
                String A0X = C117295Zj.A0X("provider", map);
                String A0X2 = C117295Zj.A0X("old_pin", map);
                String A0X3 = C117295Zj.A0X("new_pin", map);
                AnonymousClass6CB r1 = new AnonymousClass6M0(r25, this) { // from class: X.6CB
                    public final /* synthetic */ AnonymousClass3FE A00;
                    public final /* synthetic */ AbstractActivityC121705jc A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6M0
                    public final void AVD(C452120p r4) {
                        AnonymousClass3FE r2 = this.A00;
                        if (r4 == null) {
                            C117315Zl.A0T(r2);
                        } else {
                            AbstractActivityC121705jc.A0l(r2, null, r4.A00);
                        }
                    }
                };
                r7.A01(new AbstractC136296Lz(r1, r7, A0X2, A0X3) { // from class: X.6C4
                    public final /* synthetic */ AnonymousClass6M0 A00;
                    public final /* synthetic */ AnonymousClass605 A01;
                    public final /* synthetic */ String A02;
                    public final /* synthetic */ String A03;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A03 = r4;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC136296Lz
                    public final void AVF(C128545wH r15) {
                        AnonymousClass605 r72 = this.A01;
                        String str4 = this.A02;
                        String str5 = this.A03;
                        AnonymousClass6M0 r4 = this.A00;
                        C129135xE r3 = r72.A06;
                        String[] strArr = new String[2];
                        C12990iw.A1P(str4, str5, strArr);
                        C1331269o r5 = new C1331269o(r4, r72, r15);
                        if ("token".equals(r15.A00.A03)) {
                            C128995x0 r8 = new C128995x0(r5);
                            int i = 0;
                            do {
                                ArrayList A0l = C12960it.A0l();
                                C12970iu.A1T("fbpay_pin", strArr[i], A0l);
                                AbstractC14440lR r0 = r3.A05;
                                AnonymousClass1BY r10 = r3.A03;
                                C18600si r6 = r3.A01;
                                C22120yY r11 = r3.A04;
                                C12990iw.A1N(new C120305fx(r3.A00, r6, r3.A02, r8, null, r10, r11, A0l, i), r0);
                                i++;
                            } while (i < 2);
                            return;
                        }
                        r5.AX8(strArr);
                    }
                }, r1, A0X);
                return;
            case 1:
                A2l(r25);
                return;
            case 2:
                C12990iw.A1N(new AnonymousClass5oY(r25, this.A0G), ((ActivityC13830kP) this).A05);
                return;
            case 3:
                String A0t = C12970iu.A0t("completed_step", map);
                if (C117305Zk.A1W("is_merchant", str3, map)) {
                    C18660so r12 = this.A0B;
                    r12.A05(r12.A01(A0t));
                    return;
                }
                C21860y6 r13 = this.A09;
                r13.A05(r13.A01(A0t));
                return;
            case 4:
                String A0t2 = C12970iu.A0t("name", map);
                String A0t3 = C12970iu.A0t("type", map);
                if (A0t3 == null || A0t2 == null) {
                    r25.A00("on_failure");
                    return;
                }
                HashMap A11 = C12970iu.A11();
                char c = 65535;
                try {
                    switch (A0t3.hashCode()) {
                        case -891985903:
                            if (A0t3.equals("string")) {
                                c = 3;
                                break;
                            }
                            break;
                        case 3271912:
                            if (A0t3.equals("json")) {
                                c = 4;
                                break;
                            }
                            break;
                        case 64711720:
                            if (A0t3.equals("boolean")) {
                                c = 0;
                                break;
                            }
                            break;
                        case 97526364:
                            if (A0t3.equals("float")) {
                                c = 2;
                                break;
                            }
                            break;
                        case 1958052158:
                            if (A0t3.equals("integer")) {
                                c = 1;
                                break;
                            }
                            break;
                    }
                    if (c == 0) {
                        if (!((ActivityC13810kN) this).A0C.A07(Integer.parseInt(A0t2))) {
                            str3 = "0";
                        }
                        A11.put("result", str3);
                    } else if (c == 1) {
                        A11.put("result", String.valueOf(((ActivityC13810kN) this).A0C.A02(Integer.parseInt(A0t2))));
                    } else if (c == 2) {
                        C14850m9 r8 = ((ActivityC13810kN) this).A0C;
                        int parseInt = Integer.parseInt(A0t2);
                        ConcurrentHashMap concurrentHashMap = r8.A09;
                        Integer valueOf = Integer.valueOf(parseInt);
                        Number number = (Number) concurrentHashMap.get(valueOf);
                        if (number != null) {
                            f = number.floatValue();
                        } else {
                            synchronized (r8) {
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append(parseInt);
                                r8.A06(C12960it.A0d("_expo_key", A0h));
                                Float f2 = (Float) r8.A02.get(valueOf);
                                if (f2 != null) {
                                    f = r8.A00.getFloat(Integer.toString(parseInt), f2.floatValue());
                                    concurrentHashMap.put(valueOf, Float.valueOf(f));
                                } else {
                                    StringBuilder A0h2 = C12960it.A0h();
                                    A0h2.append("Unknown FloatField: ");
                                    throw C12970iu.A0f(C12960it.A0f(A0h2, parseInt));
                                }
                            }
                        }
                        A11.put("result", String.valueOf(f));
                    } else if (c == 3) {
                        A11.put("result", ((ActivityC13810kN) this).A0C.A03(Integer.parseInt(A0t2)));
                    } else if (c != 4) {
                        r25.A00("on_failure");
                        return;
                    } else {
                        A11.put("result", ((ActivityC13810kN) this).A0C.A04(Integer.parseInt(A0t2)).toString());
                    }
                    r25.A02(str2, A11);
                    return;
                } catch (NumberFormatException unused) {
                    r25.A00("on_failure");
                    return;
                }
            case 5:
                AnonymousClass605 r5 = this.A0O;
                String A0X4 = C117295Zj.A0X("provider", map);
                String A0X5 = C117295Zj.A0X("pin", map);
                AnonymousClass6CA r14 = new AnonymousClass6M0(r25, this) { // from class: X.6CA
                    public final /* synthetic */ AnonymousClass3FE A00;
                    public final /* synthetic */ AbstractActivityC121705jc A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6M0
                    public final void AVD(C452120p r52) {
                        String str4;
                        AnonymousClass3FE r3 = this.A00;
                        if (r52 != null) {
                            HashMap A112 = C12970iu.A11();
                            int i = r52.A00;
                            if (i == 1440) {
                                A112.put("remaining_retries", String.valueOf(r52.A01));
                                str4 = "pin_incorrect";
                            } else if (i == 1441) {
                                A112.put("next_retry_ts", String.valueOf(r52.A02));
                                str4 = "pin_rate_limited";
                            } else {
                                AbstractActivityC121705jc.A0l(r3, A112, i);
                                return;
                            }
                            r3.A01(str4, A112);
                            return;
                        }
                        C117315Zl.A0T(r3);
                    }
                };
                r5.A01(new AbstractC136296Lz(r14, r5, A0X5) { // from class: X.6C3
                    public final /* synthetic */ AnonymousClass6M0 A00;
                    public final /* synthetic */ AnonymousClass605 A01;
                    public final /* synthetic */ String A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC136296Lz
                    public final void AVF(C128545wH r6) {
                        AnonymousClass605 r4 = this.A01;
                        r4.A06.A00(new C1330769j(this.A00, r4, r6), r6, this.A02);
                    }
                }, r14, A0X4);
                return;
            case 6:
                AbstractC005102i A1U = A1U();
                if (A1U != null && (bloksDialogFragment = ((AbstractActivityC119645em) this).A05) != null && (bool = bloksDialogFragment.A0A) != null && !bool.booleanValue()) {
                    A1U.A0I((CharSequence) map.get("action_bar_title"));
                    return;
                }
                return;
            case 7:
                String A0t4 = C12970iu.A0t("full_name", map);
                AnonymousClass009.A04(A0t4);
                String A0t5 = C12970iu.A0t("compliance_reason", map);
                AnonymousClass009.A04(A0t5);
                C128135vc r9 = this.A0I;
                C126955ti r82 = new C126955ti(r25, this);
                if (r9.A00.equals("UNSUPPORTED")) {
                    r82.A00.A00(str2);
                    return;
                }
                C128055vU r142 = new C128055vU(r9.A03.A00, r9.A01, r9.A02, r9.A04, r9.A05, r9.A06);
                C128865wn r10 = new C128865wn(this, r9, r82);
                AnonymousClass009.A04(A0t4);
                AnonymousClass1W9[] r4 = new AnonymousClass1W9[2];
                C12960it.A1M("action", "check-account-eligibility", r4, 0);
                C12960it.A1M("action-type", A0t5, r4, 1);
                AnonymousClass1W9[] r15 = new AnonymousClass1W9[1];
                C12960it.A1M("full", A0t4, r15, 0);
                C117305Zk.A1I(r142.A04, new IDxRCallbackShape0S0200000_3_I1(r142.A00, r142.A01, r142.A03, r10, r142, 10), new AnonymousClass1V8(new AnonymousClass1V8("name", r15), "account", r4));
                return;
            case 8:
                this.A0F.A01(C117305Zk.A1W("remove_tos", str3, map), false);
                return;
            case 9:
                String A0t6 = C12970iu.A0t("completed_step", map);
                if (C117305Zk.A1W("is_merchant", str3, map)) {
                    C18660so r16 = this.A0B;
                    r16.A06(r16.A01(A0t6));
                    return;
                }
                C21860y6 r17 = this.A09;
                r17.A06(r17.A01(A0t6));
                return;
            case 10:
                int[] A02 = C1308860i.A02(C117295Zj.A0X("dob", map));
                int i = A02[0];
                int i2 = A02[1];
                int i3 = A02[2];
                String A0t7 = C12970iu.A0t("compliance_reason", map);
                AnonymousClass009.A04(A0t7);
                C128135vc r102 = this.A0I;
                C126965tj r92 = new C126965tj(r25, this);
                if (r102.A00.equals("UNSUPPORTED")) {
                    r92.A00.A00(str2);
                    return;
                }
                C128055vU r143 = new C128055vU(r102.A03.A00, r102.A01, r102.A02, r102.A04, r102.A05, r102.A06);
                C128875wo r11 = new C128875wo(this, r102, r92);
                AnonymousClass1W9[] r3 = new AnonymousClass1W9[2];
                C117305Zk.A1O("action", "check-account-eligibility", r3);
                C117305Zk.A1P("action-type", A0t7, r3);
                C117305Zk.A1I(r143.A04, new IDxRCallbackShape0S0200000_3_I1(r143.A00, r143.A01, r143.A03, r11, r143, 11), new AnonymousClass1V8(r143.A05.A00(i3, i2, i), "account", r3));
                return;
            case 11:
                if (this instanceof AbstractActivityC123635nW) {
                    AbstractActivityC123635nW r132 = (AbstractActivityC123635nW) this;
                    C1310561a.A03(r132, ((AbstractActivityC121705jc) r132).A03, r132.A0A);
                    break;
                } else {
                    RequestPermissionActivity.A0T(this, this.A03, 30);
                    break;
                }
            case 12:
                AnonymousClass617 r2 = (AnonymousClass617) this.A0R.A01.A01();
                if (r2 == null || r2.A00 == 1) {
                    Log.e("PAY: PayBloksActivity/performAsyncRequest/get incentive data returned an error");
                    str2 = "on_failure";
                    break;
                } else {
                    AnonymousClass2S0 r22 = (AnonymousClass2S0) r2.A01;
                    HashMap A112 = C12970iu.A11();
                    if (r22 != null) {
                        C50942Ry r0 = r22.A01;
                        if (r0 != null) {
                            A112.put("param_incentive_offer_id", Long.valueOf(r0.A08.A01));
                        }
                        C50932Rx r23 = r22.A02;
                        if (r23 != null) {
                            A112.put("param_incentive_claim_info_is_eligible", Boolean.valueOf(r23.A04));
                            A112.put("param_incentive_claim_info_pending_count", Integer.valueOf(r23.A00));
                            A112.put("param_incentive_claim_info_redeemed_count", Integer.valueOf(r23.A01));
                        }
                    }
                    r25.A02(str2, A112);
                    return;
                }
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C1328268j r32 = new C1328268j(r25, this);
                if (C28421Nd.A00(C12970iu.A0t("remaining_cards", map), 0) <= 1) {
                    C14900mE r144 = ((ActivityC13810kN) this).A05;
                    AbstractC14440lR r83 = ((ActivityC13830kP) this).A05;
                    C18590sh r72 = this.A0T;
                    new C129965ya(this, r144, ((ActivityC13810kN) this).A07, this.A04, this.A0A, this.A0C, this.A0D, this.A0F, this.A0G, r72, r83).A00(r32);
                    return;
                }
                this.A0D.A0B(r32, null, C12970iu.A0t("credential_id", map), null);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                Intent A0D = C12990iw.A0D(getApplicationContext(), this.A0G.A02().AGb());
                Intent intent = getIntent();
                A0D.putExtra("extra_conversation_message_type", intent.getIntExtra("extra_conversation_message_type", 0));
                C117305Zk.A10(intent, A0D, "extra_jid");
                C117305Zk.A10(intent, A0D, "extra_receiver_jid");
                A0D.putExtra("extra_quoted_msg_row_id", intent.getLongExtra("extra_quoted_msg_row_id", 0));
                C117305Zk.A10(intent, A0D, "extra_payment_preset_amount");
                C117305Zk.A10(intent, A0D, "extra_transaction_id");
                C117305Zk.A10(intent, A0D, "extra_payment_preset_min_amount");
                C117305Zk.A10(intent, A0D, "extra_request_message_key");
                A0D.putExtra("extra_is_pay_money_only", intent.getBooleanExtra("extra_is_pay_money_only", true));
                C117305Zk.A10(intent, A0D, "extra_payment_note");
                A0D.putStringArrayListExtra("extra_mentioned_jids", intent.getStringArrayListExtra("extra_mentioned_jids"));
                C117305Zk.A10(intent, A0D, "extra_inviter_jid");
                A2D(A0D);
                finish();
                return;
            case 15:
                boolean A1W = C117305Zk.A1W("param_force_incentive_claim_info_sync", str3, map);
                C118025b9 r33 = this.A0R;
                if (A1W) {
                    r33.A05.Ab2(new AnonymousClass6HV(r33));
                    return;
                } else {
                    r33.A05.Ab2(new RunnableC135466Io(r33, true));
                    return;
                }
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                this.A0D.A08(new C1328368k(r25, this), 2);
                return;
            case 17:
                AnonymousClass605 r52 = this.A0O;
                String A0X6 = C117295Zj.A0X("provider", map);
                String A0X7 = C117295Zj.A0X("pin", map);
                AnonymousClass6C8 r18 = new AnonymousClass6M0(r25, this) { // from class: X.6C8
                    public final /* synthetic */ AnonymousClass3FE A00;
                    public final /* synthetic */ AbstractActivityC121705jc A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6M0
                    public final void AVD(C452120p r42) {
                        AnonymousClass3FE r24 = this.A00;
                        if (r42 == null) {
                            C117315Zl.A0T(r24);
                        } else {
                            AbstractActivityC121705jc.A0l(r24, null, r42.A00);
                        }
                    }
                };
                r52.A01(new AbstractC136296Lz(r18, r52, A0X7) { // from class: X.6C0
                    public final /* synthetic */ AnonymousClass6M0 A00;
                    public final /* synthetic */ AnonymousClass605 A01;
                    public final /* synthetic */ String A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC136296Lz
                    public final void AVF(C128545wH r6) {
                        AnonymousClass605 r42 = this.A01;
                        r42.A06.A00(new C1330669i(this.A00, r42, r6), r6, this.A02);
                    }
                }, r18, A0X6);
                return;
            case 18:
                this.A0C.A0L(C117305Zk.A1W("is_sandbox", str3, map));
                return;
            case 19:
                A2k(null, "close".equals(map.get("navigation_icon")), "white".equals(map.get("icon_color_filter")));
                return;
            case C43951xu.A01:
                AnonymousClass605 r53 = this.A0O;
                String A0X8 = C117295Zj.A0X("provider", map);
                String A0X9 = C117295Zj.A0X("pin", map);
                AnonymousClass6C9 r19 = new AnonymousClass6M0(r25, this) { // from class: X.6C9
                    public final /* synthetic */ AnonymousClass3FE A00;
                    public final /* synthetic */ AbstractActivityC121705jc A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6M0
                    public final void AVD(C452120p r42) {
                        AnonymousClass3FE r24 = this.A00;
                        if (r42 == null) {
                            C117315Zl.A0T(r24);
                        } else {
                            AbstractActivityC121705jc.A0l(r24, null, r42.A00);
                        }
                    }
                };
                r53.A01(new AnonymousClass6C1(r19, r53, A0X9), r19, A0X8);
                return;
            default:
                return;
        }
        r25.A00(str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:155:0x02c7 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02c8 A[RETURN] */
    @Override // X.AbstractC136496Mt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String AZE(java.lang.String r23, java.util.Map r24) {
        /*
        // Method dump skipped, instructions count: 1000
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121705jc.AZE(java.lang.String, java.util.Map):java.lang.String");
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 30) {
            return;
        }
        if (i2 == -1) {
            A2g();
        } else {
            finish();
        }
    }

    @Override // X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0R = this.A0S.A00(this);
        getWindow().setSoftInputMode(16);
    }

    @Override // X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0X) {
            A2j();
            this.A0X = false;
        }
    }
}
