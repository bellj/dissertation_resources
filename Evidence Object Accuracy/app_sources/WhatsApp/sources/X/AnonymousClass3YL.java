package X;

import android.content.Context;

/* renamed from: X.3YL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YL implements AbstractC470728v {
    public final C37471mS A00;
    public final AnonymousClass19M A01;

    @Override // X.AbstractC470728v
    public boolean A6v() {
        return true;
    }

    @Override // X.AbstractC470728v
    public boolean Aac() {
        return true;
    }

    public AnonymousClass3YL(C37471mS r1, AnonymousClass19M r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC470728v
    public AbstractC454821u A8X(Context context, AnonymousClass018 r5, boolean z) {
        AnonymousClass009.A00();
        return new AnonymousClass33B(context, this.A00, this.A01, z);
    }

    @Override // X.AbstractC470728v
    public C37471mS[] ACh() {
        return new C37471mS[]{this.A00};
    }

    @Override // X.AbstractC470728v
    public String AH5() {
        return C12960it.A0d(this.A00.toString(), C12960it.A0k("EmojiShapeCreator:"));
    }

    public boolean equals(Object obj) {
        if (obj instanceof AnonymousClass3YL) {
            return this.A00.equals(((AnonymousClass3YL) obj).A00);
        }
        return false;
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
