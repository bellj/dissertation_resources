package X;

import android.os.Build;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28401Na {
    public final int A00;
    public final String A01;

    public C28401Na(String str, int i) {
        this.A00 = i;
        this.A01 = str;
    }

    public String A00() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("app_version_code", 221770000);
            jSONObject.put("brand", Build.BRAND);
            jSONObject.put("memclass", this.A00);
            jSONObject.put("model", Build.MODEL);
            jSONObject.put("android_version", Build.VERSION.RELEASE);
            jSONObject.put("app_version_name", "2.22.17.70");
            jSONObject.put("app", "Whatsapp");
            jSONObject.put("process_name", "Main Process");
            jSONObject.put("uid", "1");
            jSONObject.putOpt("dump_cause", this.A01);
            jSONObject.put("platform_abi", AnonymousClass1HP.A02());
            return jSONObject.toString();
        } catch (JSONException e) {
            return String.format(Locale.US, "{ 'error' : '%s' }", e.getMessage());
        }
    }
}
