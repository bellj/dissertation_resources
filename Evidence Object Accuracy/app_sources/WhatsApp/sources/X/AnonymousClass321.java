package X;

import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.321  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass321 extends C54462gl {
    public final /* synthetic */ GifSearchContainer A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass321(AnonymousClass01d r7, C16120oU r8, C253719d r9, GifSearchContainer gifSearchContainer, AnonymousClass5UF r11, C16630pM r12) {
        super(r7, r8, r9, r11, r12);
        this.A00 = gifSearchContainer;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r6.A02 != false) goto L_0x001b;
     */
    @Override // X.C54462gl
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E(X.AnonymousClass3EY r6) {
        /*
            r5 = this;
            super.A0E(r6)
            com.whatsapp.gifsearch.GifSearchContainer r4 = r5.A00
            android.view.View r0 = r4.A03
            r3 = 8
            r0.setVisibility(r3)
            android.view.View r2 = r4.A04
            X.2gl r0 = r4.A0E
            int r0 = r0.A0D()
            if (r0 != 0) goto L_0x001b
            boolean r1 = r6.A02
            r0 = 0
            if (r1 == 0) goto L_0x001d
        L_0x001b:
            r0 = 8
        L_0x001d:
            r2.setVisibility(r0)
            android.view.View r1 = r4.A05
            X.2gl r0 = r4.A0E
            int r0 = r0.A0D()
            if (r0 != 0) goto L_0x002f
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x002f
            r3 = 0
        L_0x002f:
            r1.setVisibility(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass321.A0E(X.3EY):void");
    }
}
