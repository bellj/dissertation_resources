package X;

import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;

/* renamed from: X.1hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC35471hw extends AbstractActivityC35431hr {
    public boolean A00 = false;

    public AbstractActivityC35471hw() {
        A0R(new C103054q8(this));
    }

    @Override // X.AbstractActivityC13760kI, X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            StarredMessagesActivity starredMessagesActivity = (StarredMessagesActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) starredMessagesActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) starredMessagesActivity).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) starredMessagesActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) starredMessagesActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) starredMessagesActivity).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) starredMessagesActivity).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) starredMessagesActivity).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) starredMessagesActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) starredMessagesActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) starredMessagesActivity).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) starredMessagesActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) starredMessagesActivity).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) starredMessagesActivity).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) starredMessagesActivity).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) starredMessagesActivity).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) starredMessagesActivity).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) starredMessagesActivity).A09 = r3.A06();
            ((ActivityC13790kL) starredMessagesActivity).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) starredMessagesActivity).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) starredMessagesActivity).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) starredMessagesActivity).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) starredMessagesActivity).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) starredMessagesActivity).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) starredMessagesActivity).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) starredMessagesActivity).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) starredMessagesActivity).A08 = (C249317l) r2.A8B.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0K = (C16590pI) r2.AMg.get();
            starredMessagesActivity.A0Z = (AnonymousClass13H) r2.ABY.get();
            starredMessagesActivity.A0j = (C253018w) r2.AJS.get();
            starredMessagesActivity.A0o = (C253118x) r2.AAW.get();
            starredMessagesActivity.A0U = (C16120oU) r2.ANE.get();
            starredMessagesActivity.A0a = (C245115u) r2.A7s.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A05 = (C18850tA) r2.AKx.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A03 = (C16170oZ) r2.AM4.get();
            starredMessagesActivity.A0p = (C26501Ds) r2.AML.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0B = (C21270x9) r2.A4A.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A07 = (C15550nR) r2.A45.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0Q = (C20040v7) r2.AAK.get();
            starredMessagesActivity.A0m = (C252018m) r2.A7g.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A09 = (C15610nY) r2.AMe.get();
            starredMessagesActivity.A0d = (C17070qD) r2.AFC.get();
            starredMessagesActivity.A0f = (C239913u) r2.AC1.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0O = (C253218y) r2.AAF.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0M = (C15650ng) r2.A4m.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0R = (AnonymousClass12H) r2.AC5.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A08 = (AnonymousClass190) r2.AIZ.get();
            starredMessagesActivity.A0i = (AnonymousClass12F) r2.AJM.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0P = (C20000v3) r2.AAG.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A06 = (AnonymousClass116) r2.A3z.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0T = (AnonymousClass193) r2.A6S.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0S = (C242114q) r2.AJq.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0L = (C15890o4) r2.AN1.get();
            starredMessagesActivity.A0V = (C20710wC) r2.A8m.get();
            starredMessagesActivity.A0n = (AnonymousClass198) r2.A0J.get();
            starredMessagesActivity.A0h = (C240514a) r2.AJL.get();
            starredMessagesActivity.A0W = (AnonymousClass11G) r2.AKq.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0A = (AnonymousClass10T) r2.A47.get();
            starredMessagesActivity.A0Y = (C22370yy) r2.AB0.get();
            starredMessagesActivity.A0c = (C22710zW) r2.AF7.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A04 = (C14650lo) r2.A2V.get();
            starredMessagesActivity.A0l = (AnonymousClass1AB) r2.AKI.get();
            starredMessagesActivity.A0X = (AnonymousClass109) r2.AI8.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0N = (C15600nX) r2.A8x.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A02 = (C16210od) r2.A0Y.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0H = (AnonymousClass19D) r2.ABo.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0I = (AnonymousClass11P) r2.ABp.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0E = (AnonymousClass19I) r2.A4a.get();
            starredMessagesActivity.A0b = (AnonymousClass19J) r2.ACA.get();
            ((AbstractActivityC13750kH) starredMessagesActivity).A0C = (AnonymousClass19K) r2.AFh.get();
            starredMessagesActivity.A0q = (AnonymousClass19L) r2.A6l.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A01 = (C239613r) r2.AI9.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0L = (AnonymousClass12U) r2.AJd.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0G = (C231510o) r2.AHO.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0K = new C88054Ec();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0F = (C22180yf) r2.A5U.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0C = (C15240mn) r2.A8F.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A04 = (AnonymousClass10S) r2.A46.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A03 = (C22330yu) r2.A3I.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A02 = (AnonymousClass12T) r2.AJZ.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0D = (C16490p7) r2.ACJ.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0M = (C23000zz) r2.ALg.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A05 = (C22700zV) r2.AMN.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A09 = (C255419u) r2.AJp.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0A = (AnonymousClass15Q) r2.A6g.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0E = (C22100yW) r2.A3g.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0B = (C25641Ae) r2.A6i.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0J = (C16630pM) r2.AIc.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0H = (C244215l) r2.A8y.get();
            ((AbstractActivityC35431hr) starredMessagesActivity).A0N = C18000rk.A00(r2.A4v);
            ((AbstractActivityC35431hr) starredMessagesActivity).A08 = (AnonymousClass1A6) r2.A4u.get();
            starredMessagesActivity.A01 = (C22230yk) r2.ANT.get();
        }
    }
}
