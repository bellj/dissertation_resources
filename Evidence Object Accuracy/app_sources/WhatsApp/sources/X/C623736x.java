package X;

import com.whatsapp.community.AddGroupsToCommunityViewModel;
import java.util.Set;

/* renamed from: X.36x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C623736x extends AbstractC16350or {
    public final /* synthetic */ AddGroupsToCommunityViewModel A00;
    public final /* synthetic */ Set A01;

    public C623736x(AddGroupsToCommunityViewModel addGroupsToCommunityViewModel, Set set) {
        this.A00 = addGroupsToCommunityViewModel;
        this.A01 = set;
    }
}
