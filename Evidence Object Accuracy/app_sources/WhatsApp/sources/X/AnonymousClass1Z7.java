package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.1Z7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z7 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100254lc();
    public final List A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1Z7(Parcel parcel) {
        this.A00 = parcel.createTypedArrayList(AnonymousClass1Z9.CREATOR);
    }

    public AnonymousClass1Z7(List list) {
        this.A00 = list;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.A00);
    }
}
