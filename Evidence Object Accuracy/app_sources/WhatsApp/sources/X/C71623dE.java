package X;

import com.whatsapp.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.impl.client.RequestWrapper;
import org.apache.http.protocol.HttpContext;

/* renamed from: X.3dE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71623dE implements HttpResponseInterceptor {
    public final int A00 = 3;
    public final C18790t3 A01;

    public C71623dE(C18790t3 r2) {
        this.A01 = r2;
    }

    public void process(HttpResponse httpResponse, HttpContext httpContext) {
        RequestWrapper requestWrapper;
        String A0d;
        if (httpResponse.getEntity() == null) {
            requestWrapper = (RequestWrapper) httpContext.getAttribute("http.request");
            if (requestWrapper == null) {
                A0d = "gdrive-response-interceptor/process/response-without-entity-and-request-is-null";
                Log.e(A0d);
                return;
            }
            requestWrapper.getURI();
            return;
        }
        long contentLength = httpResponse.getEntity().getContentLength();
        if (contentLength <= 0) {
            requestWrapper = (RequestWrapper) httpContext.getAttribute("http.request");
            if (requestWrapper == null) {
                StringBuilder A0j = C12960it.A0j("gdrive-response-interceptor/process/length/");
                A0j.append(contentLength);
                A0d = C12960it.A0d("/request-is-null", A0j);
                Log.e(A0d);
                return;
            }
            requestWrapper.getURI();
            return;
        }
        httpResponse.setEntity(new C71603dC(this, httpResponse.getEntity()));
    }
}
