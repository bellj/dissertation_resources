package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import java.lang.ref.WeakReference;
import java.util.Map;

/* renamed from: X.0ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16290ol {
    public final Handler A00;
    public final Runnable A01;
    public final WeakReference A02;
    public final /* synthetic */ C16340oq A03;

    public C16290ol(AbstractC16320oo r5, C16340oq r6) {
        this.A03 = r6;
        Handler handler = new Handler();
        this.A00 = handler;
        this.A02 = new WeakReference(r5);
        RunnableBRunnable0Shape3S0100000_I0_3 runnableBRunnable0Shape3S0100000_I0_3 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 20);
        this.A01 = runnableBRunnable0Shape3S0100000_I0_3;
        handler.postDelayed(runnableBRunnable0Shape3S0100000_I0_3, C26061Bw.A0L);
    }

    public final void A00() {
        this.A00.removeCallbacks(this.A01);
        Map map = this.A03.A01.A01;
        if (map.containsKey("WA_BizDirectorySearch")) {
            ((C16160oY) map.get("WA_BizDirectorySearch")).A00.remove(this);
        }
    }
}
