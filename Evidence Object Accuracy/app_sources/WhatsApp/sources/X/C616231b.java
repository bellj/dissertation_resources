package X;

import android.database.ContentObserver;
import android.database.Cursor;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.gallery.DocumentsGalleryFragment;
import com.whatsapp.gallery.GalleryFragmentBase;
import java.util.Calendar;

/* renamed from: X.31b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616231b extends AbstractC54432gi implements AnonymousClass2TX {
    public int A00;
    public final ContentObserver A01 = new AnonymousClass2ZK(C12970iu.A0E(), this);
    public final /* synthetic */ DocumentsGalleryFragment A02;

    public C616231b(DocumentsGalleryFragment documentsGalleryFragment) {
        this.A02 = documentsGalleryFragment;
    }

    @Override // X.AbstractC54432gi, X.AnonymousClass02M
    public int A0D() {
        return this.A00;
    }

    @Override // X.AbstractC54432gi
    public Cursor A0E(Cursor cursor) {
        int i;
        Cursor cursor2 = ((AbstractC54432gi) this).A01;
        if (cursor2 != null) {
            cursor2.unregisterContentObserver(this.A01);
        }
        if (cursor != null) {
            cursor.registerContentObserver(this.A01);
            i = cursor.getCount();
        } else {
            i = 0;
        }
        this.A00 = i;
        return super.A0E(cursor);
    }

    @Override // X.AnonymousClass2TX
    public int ABl(int i) {
        return ((AnonymousClass2WL) ((GalleryFragmentBase) this.A02).A0I.get(i)).count;
    }

    @Override // X.AnonymousClass2TX
    public int ADH() {
        return ((GalleryFragmentBase) this.A02).A0I.size();
    }

    @Override // X.AnonymousClass2TX
    public long ADI(int i) {
        return -((Calendar) ((GalleryFragmentBase) this.A02).A0I.get(i)).getTimeInMillis();
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ void ANF(AnonymousClass03U r3, int i) {
        ((C75253jY) r3).A00.setText(((GalleryFragmentBase) this.A02).A0I.get(i).toString());
    }

    @Override // X.AbstractC54432gi, X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        Cursor cursor = ((AbstractC54432gi) this).A01;
        if (cursor != null && i < cursor.getCount()) {
            super.ANH(r2, i);
        }
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ AnonymousClass03U AOh(ViewGroup viewGroup) {
        DocumentsGalleryFragment documentsGalleryFragment = this.A02;
        View inflate = documentsGalleryFragment.A0C().getLayoutInflater().inflate(R.layout.media_gallery_section_row, viewGroup, false);
        inflate.setClickable(false);
        C12970iu.A18(documentsGalleryFragment.A01(), inflate, R.color.gallery_separator);
        return new C75253jY(inflate);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        DocumentsGalleryFragment documentsGalleryFragment = this.A02;
        return new C55102hn(C12960it.A0F(documentsGalleryFragment.A0C().getLayoutInflater(), viewGroup, R.layout.document_media_item), documentsGalleryFragment);
    }

    @Override // X.AnonymousClass2TX
    public boolean AWm(MotionEvent motionEvent, AnonymousClass03U r3, int i) {
        return false;
    }
}
