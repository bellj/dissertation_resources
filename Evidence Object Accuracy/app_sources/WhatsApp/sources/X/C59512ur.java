package X;

/* renamed from: X.2ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59512ur extends C37171lc {
    public final int A00;
    public final AbstractC49232Jx A01;

    public C59512ur(AbstractC49232Jx r2, int i) {
        super(AnonymousClass39o.A0G);
        this.A00 = i;
        this.A01 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || this.A00 != ((C59512ur) obj).A00) {
            return false;
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return super.A00.hashCode();
    }
}
