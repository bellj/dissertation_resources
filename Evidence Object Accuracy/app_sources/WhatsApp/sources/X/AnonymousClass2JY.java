package X;

/* renamed from: X.2JY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JY {
    public C48192Eu A00;
    public String A01;
    public boolean A02;
    public boolean A03;
    public final AnonymousClass17S A04;
    public final C22260yn A05;
    public final ActivityC13810kN A06;
    public final C14900mE A07;
    public final AnonymousClass18U A08;
    public final C15570nT A09;
    public final C15450nH A0A;
    public final C15450nH A0B;
    public final C250918b A0C;
    public final C251118d A0D;
    public final C15550nR A0E;
    public final C26311Cv A0F;
    public final C22700zV A0G;
    public final C15610nY A0H;
    public final C253318z A0I;
    public final C18640sm A0J;
    public final AnonymousClass01d A0K;
    public final C14830m7 A0L;
    public final C17170qN A0M;
    public final C22610zM A0N;
    public final C15680nj A0O;
    public final C14850m9 A0P;
    public final C16120oU A0Q;
    public final C17220qS A0R;
    public final C22410z2 A0S;
    public final C22710zW A0T;
    public final C17070qD A0U;
    public final AbstractC14440lR A0V;
    public final Integer A0W;
    public final boolean A0X;
    public final boolean A0Y;
    public volatile boolean A0Z;

    public AnonymousClass2JY(AnonymousClass17S r2, C22260yn r3, ActivityC13810kN r4, C14900mE r5, AnonymousClass18U r6, C15570nT r7, C15450nH r8, C250918b r9, C251118d r10, C15550nR r11, C26311Cv r12, C22700zV r13, C15610nY r14, C253318z r15, C18640sm r16, AnonymousClass01d r17, C14830m7 r18, C17170qN r19, C22610zM r20, C15680nj r21, C14850m9 r22, C16120oU r23, C17220qS r24, C22410z2 r25, C22710zW r26, C17070qD r27, AbstractC14440lR r28, Integer num, boolean z, boolean z2) {
        this.A06 = r4;
        this.A0P = r22;
        this.A0Q = r23;
        this.A04 = r2;
        this.A0W = num;
        this.A0A = r8;
        this.A08 = r6;
        this.A0K = r17;
        this.A0U = r27;
        this.A0Y = z;
        this.A0X = z2;
        this.A0T = r26;
        this.A0D = r10;
        this.A0C = r9;
        this.A0L = r18;
        this.A07 = r5;
        this.A09 = r7;
        this.A0V = r28;
        this.A0B = r8;
        this.A0R = r24;
        this.A0E = r11;
        this.A0H = r14;
        this.A05 = r3;
        this.A0I = r15;
        this.A0G = r13;
        this.A0O = r21;
        this.A0S = r25;
        this.A0J = r16;
        this.A0F = r12;
        this.A0N = r20;
        this.A0M = r19;
    }

    public C48192Eu A00(String str, int i, int i2, boolean z) {
        C14830m7 r7 = this.A0L;
        C14850m9 r10 = this.A0P;
        C14900mE r2 = this.A07;
        AbstractC14440lR r15 = this.A0V;
        C16120oU r11 = this.A0Q;
        C17220qS r12 = this.A0R;
        C15550nR r3 = this.A0E;
        C22260yn r1 = this.A05;
        C253318z r5 = this.A0I;
        C22700zV r4 = this.A0G;
        C22410z2 r13 = this.A0S;
        return new C48192Eu(r1, r2, r3, r4, r5, this.A0J, r7, this.A0M, this.A0N, r10, r11, r12, r13, this, r15, str, i, i2, z);
    }

    public final void A01(int i, int i2) {
        C16120oU r5 = this.A0Q;
        Integer valueOf = Integer.valueOf(i);
        Integer valueOf2 = Integer.valueOf(i2);
        Boolean bool = Boolean.TRUE;
        C51332Ty r0 = new C51332Ty();
        r0.A03 = valueOf;
        r0.A04 = null;
        r0.A05 = valueOf2;
        r0.A01 = bool;
        r0.A00 = null;
        r0.A02 = null;
        r5.A06(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02c6, code lost:
        if (r2.A01 == null) goto L_0x02c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0349, code lost:
        if (r20 != 3) goto L_0x031b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        if (r19.length() <= 17) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0195, code lost:
        if (r19.length() <= 22) goto L_0x0197;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(java.lang.String r19, int r20, boolean r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 1018
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2JY.A02(java.lang.String, int, boolean, boolean):boolean");
    }
}
