package X;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import java.util.List;

/* renamed from: X.2al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52412al extends TouchDelegate {
    public static final Rect A01 = C12980iv.A0J();
    public final List A00 = C12960it.A0l();

    public C52412al(AbstractC52532bB r5, List list) {
        super(A01, r5);
        for (int i = 0; i < list.size(); i++) {
            this.A00.add(new AnonymousClass3D2(r5, (AnonymousClass4TU) list.get(i)));
        }
    }

    public static boolean A00(Context context) {
        return C12970iu.A1W(C12980iv.A0H(context).getLayoutDirection());
    }

    @Override // android.view.TouchDelegate
    public boolean onTouchEvent(MotionEvent motionEvent) {
        List list = this.A00;
        for (int size = list.size() - 1; size >= 0; size--) {
            AnonymousClass3D2 r0 = (AnonymousClass3D2) list.get(size);
            if (r0 != null && r0.A00(motionEvent)) {
                return true;
            }
        }
        return false;
    }
}
