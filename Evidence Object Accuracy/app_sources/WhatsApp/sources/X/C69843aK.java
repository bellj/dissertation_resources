package X;

import android.os.Build;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.3aK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69843aK implements AbstractC116245Ur {
    public final /* synthetic */ Conversation A00;

    public C69843aK(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r10, Integer num, int i) {
        Conversation conversation = this.A00;
        if (!((AbstractActivityC13750kH) conversation).A0L.A07()) {
            int i2 = Build.VERSION.SDK_INT;
            int i3 = R.string.permission_storage_need_write_access_on_sending_media_v30;
            if (i2 < 30) {
                i3 = R.string.permission_storage_need_write_access_on_sending_media;
            }
            RequestPermissionActivity.A0L(conversation, R.string.permission_storage_need_write_access_on_sending_media_request, i3, 812);
        } else if (conversation.A1O.A0I(C15370n3.A05(conversation.A2c))) {
            C36021jC.A01(conversation, 106);
        } else {
            conversation.A3H.A02(false);
            C15260mp r0 = conversation.A2k;
            if (r0 != null && !r0.isShowing()) {
                conversation.A2w.A04(false);
            }
            C16170oZ r3 = ((AbstractActivityC13750kH) conversation).A03;
            AbstractC14640lm r4 = conversation.A2s;
            AnonymousClass009.A05(r4);
            r3.A0F(r4, conversation.A20.A07, r10, num, conversation.A4L);
            if (conversation.A3Z.A00 && num != null && 7 == num.intValue() && 1 == C12980iv.A0H(conversation).orientation) {
                MentionableEntry mentionableEntry = conversation.A2w;
                if (mentionableEntry != null) {
                    mentionableEntry.selectAll();
                }
                C15260mp r1 = conversation.A2k;
                if (r1 != null) {
                    r1.A08(r1.A0K);
                }
            }
        }
    }
}
