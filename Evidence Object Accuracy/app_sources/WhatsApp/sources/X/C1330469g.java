package X;

/* renamed from: X.69g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330469g implements AnonymousClass6MM {
    public final /* synthetic */ AnonymousClass607 A00;
    public final /* synthetic */ AbstractC1311361k A01;
    public final /* synthetic */ C128545wH A02;

    public C1330469g(AnonymousClass607 r1, AbstractC1311361k r2, C128545wH r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A01.APo(r2);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        C128545wH r1 = this.A02;
        AnonymousClass607 r3 = this.A00;
        Object[] objArr = new Object[0];
        AnonymousClass607.A00(r3, this.A01, r1.A02(C130775zx.A00(Boolean.TRUE, str, "AUTH", null, null, objArr, C117295Zj.A03(r3.A0O.A01))), C117295Zj.A0U(r3.A02, r3.A04));
    }
}
