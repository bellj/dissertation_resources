package X;

import com.whatsapp.contact.picker.ContactPickerFragment;

/* renamed from: X.41S  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41S extends AnonymousClass2Dn {
    public final /* synthetic */ ContactPickerFragment A00;

    public AnonymousClass41S(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r3) {
        ContactPickerFragment contactPickerFragment = this.A00;
        if (!ContactPickerFragment.A2d) {
            contactPickerFragment.A1L();
        }
    }
}
