package X;

import java.util.Map;

/* renamed from: X.5DS  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5DS implements Map.Entry {
    @Override // java.util.Map.Entry
    public abstract Object getKey();

    @Override // java.util.Map.Entry
    public abstract Object getValue();

    @Override // java.util.Map.Entry
    public abstract Object setValue(Object obj);

    @Override // java.util.Map.Entry, java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!AnonymousClass28V.A00(getKey(), entry.getKey()) || !AnonymousClass28V.A00(getValue(), entry.getValue())) {
            return false;
        }
        return true;
    }

    @Override // java.util.Map.Entry, java.lang.Object
    public int hashCode() {
        Object key = getKey();
        Object value = getValue();
        int i = 0;
        int A0D = C72453ed.A0D(key);
        if (value != null) {
            i = value.hashCode();
        }
        return A0D ^ i;
    }

    @Override // java.lang.Object
    public String toString() {
        String valueOf = String.valueOf(getKey());
        String valueOf2 = String.valueOf(getValue());
        StringBuilder A0t = C12980iv.A0t(valueOf.length() + 1 + valueOf2.length());
        A0t.append(valueOf);
        A0t.append("=");
        return C12960it.A0d(valueOf2, A0t);
    }
}
