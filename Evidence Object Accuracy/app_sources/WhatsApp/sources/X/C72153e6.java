package X;

import android.graphics.Bitmap;
import com.whatsapp.R;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/* renamed from: X.3e6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72153e6 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ int $selectedPosePosition;
    public final /* synthetic */ AvatarProfilePhotoViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72153e6(AvatarProfilePhotoViewModel avatarProfilePhotoViewModel, int i) {
        super(1);
        this.this$0 = avatarProfilePhotoViewModel;
        this.$selectedPosePosition = i;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Iterable iterable = (Iterable) obj;
        C16700pc.A0E(iterable, 0);
        AvatarProfilePhotoViewModel avatarProfilePhotoViewModel = this.this$0;
        int i = this.$selectedPosePosition;
        ArrayList A0E = C16760pi.A0E(iterable);
        int i2 = 0;
        for (Object obj2 : iterable) {
            int i3 = i2 + 1;
            if (i2 < 0) {
                throw new ArithmeticException("Index overflow has happened.");
            }
            A0E.add(new C58702rF((Bitmap) obj2, AnonymousClass00T.A00(avatarProfilePhotoViewModel.A04.A00.A00.getApplicationContext(), R.color.group_editor_color_highlight_1), C12960it.A1V(i2, i)));
            i2 = i3;
        }
        Iterator it = A0E.iterator();
        while (it.hasNext()) {
            C58702rF r7 = (C58702rF) it.next();
            if (r7.A02) {
                Object A01 = this.this$0.A00.A01();
                C16700pc.A0C(A01);
                List list = ((C27541Hx) A01).A02;
                Object A012 = this.this$0.A00.A01();
                C16700pc.A0C(A012);
                this.this$0.A00.A0B(new C27541Hx(((C27541Hx) A012).A00, r7, A0E, list, false, false));
                return AnonymousClass1WZ.A00;
            }
        }
        throw new NoSuchElementException("Collection contains no element matching the predicate.");
    }
}
