package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.0tP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19000tP extends AbstractC18500sY implements AbstractC19010tQ {
    public final C18460sU A00;
    public final C20040v7 A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19000tP(C18460sU r3, C20040v7 r4, C18480sW r5) {
        super(r5, "labeled_jid", 1);
        this.A00 = r3;
        this.A01 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("label_id");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("jid");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(columnIndexOrThrow);
            long j2 = cursor.getLong(columnIndexOrThrow2);
            String string = cursor.getString(columnIndexOrThrow3);
            AbstractC14640lm A01 = AbstractC14640lm.A01(string);
            if (A01 != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("label_id", Long.valueOf(j2));
                contentValues.put("jid_row_id", Long.valueOf(this.A00.A01(A01)));
                C16310on A02 = this.A05.A02();
                try {
                    A02.A03.A06(contentValues, "labeled_jid", 5);
                    A02.close();
                    i++;
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                StringBuilder sb = new StringBuilder("LabelJidStore/processBatch/invalid jid in original table, jid=");
                sb.append(string);
                Log.e(sb.toString());
            }
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("labeled_jid", null, null);
            C21390xL r1 = this.A06;
            r1.A03("labeled_jids_ready");
            r1.A03("migration_labeled_jid_index");
            r1.A03("migration_labeled_jid_retry");
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
