package X;

import android.widget.Filter;
import com.whatsapp.documentpicker.DocumentPickerActivity;
import java.util.ArrayList;

/* renamed from: X.2bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52872bp extends Filter {
    public int A00 = 0;
    public final /* synthetic */ DocumentPickerActivity A01;

    public /* synthetic */ C52872bp(DocumentPickerActivity documentPickerActivity) {
        this.A01 = documentPickerActivity;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:24:0x006b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0, types: [java.util.List, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r5v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // android.widget.Filter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.Filter.FilterResults performFiltering(java.lang.CharSequence r9) {
        /*
            r8 = this;
            if (r9 == 0) goto L_0x0067
            java.lang.String r1 = r9.toString()
            com.whatsapp.documentpicker.DocumentPickerActivity r0 = r8.A01
            X.018 r0 = r0.A0A
            java.util.ArrayList r7 = X.C32751cg.A02(r0, r1)
        L_0x000e:
            int r0 = r8.A00
            com.whatsapp.documentpicker.DocumentPickerActivity r6 = r8.A01
            int r1 = r6.A00
            if (r0 == r1) goto L_0x0026
            r8.A00 = r1
            X.018 r0 = r6.A0A
            java.util.List r3 = r6.A0I
            r2 = 1
            if (r1 == 0) goto L_0x0053
            if (r1 != r2) goto L_0x0026
            r0 = 15
            X.C12980iv.A1S(r3, r0)
        L_0x0026:
            if (r7 == 0) goto L_0x0069
            boolean r0 = r7.isEmpty()
            if (r0 != 0) goto L_0x0069
            java.util.ArrayList r5 = X.C12960it.A0l()
            java.util.List r0 = r6.A0I
            java.util.Iterator r4 = r0.iterator()
        L_0x0038:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x006b
            java.lang.Object r3 = r4.next()
            X.4XI r3 = (X.AnonymousClass4XI) r3
            java.lang.String r2 = r3.A03
            X.018 r1 = r6.A0A
            r0 = 1
            boolean r0 = X.C32751cg.A03(r1, r2, r7, r0)
            if (r0 == 0) goto L_0x0038
            r5.add(r3)
            goto L_0x0038
        L_0x0053:
            java.util.Locale r0 = X.C12970iu.A14(r0)
            java.text.Collator r1 = java.text.Collator.getInstance(r0)
            r1.setDecomposition(r2)
            X.5Ce r0 = new X.5Ce
            r0.<init>(r1)
            java.util.Collections.sort(r3, r0)
            goto L_0x0026
        L_0x0067:
            r7 = 0
            goto L_0x000e
        L_0x0069:
            java.util.List r5 = r6.A0I
        L_0x006b:
            android.widget.Filter$FilterResults r1 = new android.widget.Filter$FilterResults
            r1.<init>()
            r1.values = r5
            int r0 = r5.size()
            r1.count = r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52872bp.performFiltering(java.lang.CharSequence):android.widget.Filter$FilterResults");
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        if (filterResults != null) {
            this.A01.A0J = (ArrayList) filterResults.values;
        }
        DocumentPickerActivity documentPickerActivity = this.A01;
        documentPickerActivity.A0B.notifyDataSetChanged();
        DocumentPickerActivity.A02(documentPickerActivity);
    }
}
