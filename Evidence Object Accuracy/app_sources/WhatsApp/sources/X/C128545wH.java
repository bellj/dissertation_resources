package X;

/* renamed from: X.5wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128545wH {
    public final AnonymousClass6B7 A00;

    public C128545wH(AnonymousClass6B7 r1) {
        this.A00 = r1;
    }

    public static void A00(C452120p r2, AnonymousClass60T r3, C128545wH r4) {
        r3.A02(r2, r4.A00.A05, "PIN");
    }

    public static void A01(C128545wH r4, C130775zx r5, AbstractC21730xt r6, byte[] bArr, AnonymousClass1W9[] r8) {
        r5.A02.A0F(r6, new AnonymousClass1V8(r4.A02(bArr), "account", r8), "set", C26061Bw.A0L);
    }

    public AnonymousClass1V8 A02(byte[] bArr) {
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[3];
        AnonymousClass6B7 r2 = this.A00;
        C117305Zk.A1O("key-type", r2.A03, r3);
        C117305Zk.A1P("key-version", r2.A04, r3);
        C117295Zj.A1P("provider", r2.A05, r3);
        return new AnonymousClass1V8("pin", r2.A00.A9N(bArr, C003501n.A0D(16)), r3);
    }
}
