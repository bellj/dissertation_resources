package X;

import android.database.AbstractCursor;
import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.0pB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16530pB extends AbstractCursor {
    public static final String[] A03 = {"jid", "name"};
    public final C15550nR A00;
    public final C15610nY A01;
    public final C15680nj A02;

    @Override // android.database.AbstractCursor, android.database.Cursor
    public double getDouble(int i) {
        return 0.0d;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public float getFloat(int i) {
        return 0.0f;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getInt(int i) {
        return 0;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public long getLong(int i) {
        return 0;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public short getShort(int i) {
        return 0;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public boolean isNull(int i) {
        return false;
    }

    public C16530pB(C15550nR r1, C15610nY r2, C15680nj r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String[] getColumnNames() {
        return A03;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getCount() {
        return this.A02.A01();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String getString(int i) {
        if (i == 0) {
            List A04 = this.A02.A04();
            int position = getPosition();
            if (A04.size() > position) {
                return ((Jid) A04.get(position)).getRawString();
            }
        } else if (i != 1) {
            return "";
        }
        List A042 = this.A02.A04();
        int position2 = getPosition();
        if (A042.size() > position2) {
            return this.A01.A04(this.A00.A0B((AbstractC14640lm) A042.get(position2)));
        }
        return "";
    }
}
