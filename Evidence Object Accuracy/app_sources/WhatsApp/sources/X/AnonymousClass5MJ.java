package X;

/* renamed from: X.5MJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MJ extends AnonymousClass1TM {
    public AnonymousClass5NE A00;
    public C114675Mq A01;

    public AnonymousClass5MJ(AbstractC114775Na r3) {
        this.A00 = AnonymousClass5NE.A01(AbstractC114775Na.A00(r3));
        if (r3.A0B() > 1) {
            this.A01 = C114675Mq.A00(AnonymousClass5ND.A00(AnonymousClass5NU.A00((AnonymousClass5NU) r3.A0D(1))));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A00);
        C114675Mq r2 = this.A01;
        if (r2 != null) {
            C94954co.A02(r2, A00, 0, true);
        }
        return new AnonymousClass5NZ(A00);
    }
}
