package X;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S1700000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jobqueue.job.SendOrderStatusUpdateFailureReceiptJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1QC  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1QC {
    public static final Set A00 = Collections.unmodifiableSet(new HashSet(Arrays.asList(405, 416, 417, 418, 421)));
    public static final Set A01 = Collections.unmodifiableSet(new HashSet(Arrays.asList(401, 402, 403, 419, 420)));

    public static AnonymousClass1ZK A00(JSONObject jSONObject) {
        String str;
        AnonymousClass2Bg r7;
        String string = jSONObject.getString("status");
        String optString = jSONObject.optString("description");
        AnonymousClass1ZI A012 = A01(jSONObject.optJSONObject("subtotal"));
        AnonymousClass1ZI A013 = A01(jSONObject.optJSONObject("tax"));
        JSONObject optJSONObject = jSONObject.optJSONObject("discount");
        AnonymousClass1ZI A014 = A01(optJSONObject);
        if (optJSONObject != null) {
            str = optJSONObject.optString("discount_program_name");
        } else {
            str = null;
        }
        AnonymousClass1ZI A015 = A01(jSONObject.optJSONObject("shipping"));
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("items");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) optJSONArray.get(i);
                JSONObject jSONObject3 = jSONObject2.getJSONObject("amount");
                JSONObject optJSONObject2 = jSONObject2.optJSONObject("sale_amount");
                String optString2 = jSONObject2.optString("product_id");
                if (TextUtils.isEmpty(optString2)) {
                    optString2 = null;
                }
                arrayList.add(new C66023Lz(A01(jSONObject3), A01(optJSONObject2), jSONObject2.getString("retailer_id"), optString2, jSONObject2.getString("name"), jSONObject2.getInt("quantity")));
            }
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("expiration");
        if (optJSONObject3 != null) {
            long j = optJSONObject3.getLong("timestamp");
            String optString3 = optJSONObject3.optString("description");
            if (TextUtils.isEmpty(optString3)) {
                optString3 = null;
            }
            r7 = new AnonymousClass2Bg(optString3, j);
        } else {
            r7 = null;
        }
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        return new AnonymousClass1ZK(r7, A012, A013, A014, A015, string, optString, str, arrayList);
    }

    public static AnonymousClass1ZI A01(JSONObject jSONObject) {
        String str = null;
        if (jSONObject == null) {
            return null;
        }
        long j = jSONObject.getLong("value");
        int i = jSONObject.getInt("offset");
        String optString = jSONObject.optString("description");
        if (!TextUtils.isEmpty(optString)) {
            str = optString;
        }
        return new AnonymousClass1ZI(i, str, j);
    }

    public static AnonymousClass1ZD A02(AnonymousClass102 r15, JSONObject jSONObject) {
        byte[] bArr;
        String str = null;
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("thumb");
        if (!TextUtils.isEmpty(optString)) {
            bArr = Base64.decode(optString, 0);
        } else {
            bArr = null;
        }
        String optString2 = jSONObject.optString("title");
        AnonymousClass1ZI A012 = A01(jSONObject.optJSONObject("total_amount"));
        String string = jSONObject.getString("reference_id");
        AbstractC30791Yv A02 = r15.A02(jSONObject.optString("currency"));
        String optString3 = jSONObject.optString("payment_configuration");
        String optString4 = jSONObject.optString("transaction_id");
        if (TextUtils.isEmpty(optString4)) {
            optString4 = null;
        }
        String optString5 = jSONObject.optString("payment_method");
        if (TextUtils.isEmpty(optString5)) {
            optString5 = null;
        }
        long optLong = jSONObject.optLong("payment_timestamp");
        String optString6 = jSONObject.optString("type");
        if (!TextUtils.isEmpty(optString6)) {
            str = optString6;
        }
        return new AnonymousClass1ZD(A02, A00(jSONObject.getJSONObject("order")), A012, optString2, string, str, optString3, optString4, optString5, A05(jSONObject.optJSONArray("external_payment_configurations")), bArr, optLong, jSONObject.optBoolean("is_interactive"));
    }

    public static AbstractC16390ow A03(C15650ng r9, AbstractC14640lm r10, C22170ye r11, C27081Fy r12, AbstractC15340mz r13) {
        AnonymousClass1ZD r1;
        try {
            String A02 = AnonymousClass3J5.A02(r12);
            if (A02 != null) {
                JSONObject jSONObject = new JSONObject(A02);
                String string = jSONObject.getString("reference_id");
                String A03 = AnonymousClass3J5.A03(A02);
                if (A03 != null) {
                    String optString = jSONObject.getJSONObject("order").optString("description");
                    for (AbstractC15340mz r5 : r9.A0w.A01(r10)) {
                        if (r5 instanceof AbstractC16390ow) {
                            AbstractC16390ow r52 = (AbstractC16390ow) r5;
                            C16470p4 ABf = r52.ABf();
                            if (!(ABf == null || (r1 = ABf.A01) == null || !string.equals(r1.A07))) {
                                AnonymousClass1ZK r6 = r1.A04;
                                int A002 = AnonymousClass1ZD.A00(r6.A01);
                                int A003 = AnonymousClass1ZD.A00(A03);
                                Set set = (Set) AnonymousClass1ZD.A0D.get(Integer.valueOf(A002));
                                if (set == null || !set.contains(Integer.valueOf(A003))) {
                                    if (r11 != null) {
                                        C20670w8 r3 = r11.A00;
                                        AnonymousClass1IS r0 = r13.A0z;
                                        AbstractC14640lm r2 = r0.A00;
                                        AnonymousClass009.A05(r2);
                                        r3.A00(new SendOrderStatusUpdateFailureReceiptJob(r2, r0.A01));
                                    }
                                    throw new C43271wi(0);
                                }
                                r6.A01 = A03;
                                if (!TextUtils.isEmpty(optString)) {
                                    r6.A00 = optString;
                                }
                                r9.A0W((AbstractC15340mz) r52);
                                return r52;
                            }
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("InteractiveMessageCheckoutInfoConverter/updateOrderStatusInCheckoutInfoMessage: Checkout message should use InteractiveMessage interface. Message row id = ");
                            sb.append(r5.A11);
                            Log.e(sb.toString());
                        }
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("InteractiveMessageCheckoutInfoConverter/updateOrderStatusInCheckoutInfoMessage can not find origin checkout NFM with reference id: ");
                    sb2.append(string);
                    Log.e(sb2.toString());
                }
            }
            return null;
        } catch (JSONException e) {
            Log.e("InteractiveMessageCheckoutInfoConverter/updateOrderStatusInCheckoutInfoMessage failed to parse parameters json", e);
            return null;
        }
    }

    public static AbstractC16390ow A04(C15650ng r10, AbstractC14640lm r11, C27081Fy r12) {
        AnonymousClass1ZD r1;
        try {
            String A02 = AnonymousClass3J5.A02(r12);
            if (A02 != null) {
                String string = new JSONObject(A02).getString("reference_id");
                Pair A002 = AnonymousClass3J5.A00(A02);
                if (A002 != null) {
                    String str = (String) A002.first;
                    long longValue = ((Number) A002.second).longValue();
                    for (AbstractC15340mz r7 : r10.A0w.A01(r11)) {
                        if (r7 instanceof AbstractC16390ow) {
                            AbstractC16390ow r72 = (AbstractC16390ow) r7;
                            C16470p4 ABf = r72.ABf();
                            if (!(ABf == null || (r1 = ABf.A01) == null || !string.equals(r1.A07))) {
                                r1.A01 = str;
                                r1.A00 = longValue;
                                r10.A0W((AbstractC15340mz) r72);
                                return r72;
                            }
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("InteractiveMessageCheckoutInfoConverter/updateOrderPaymentMethodInCheckoutInfoMessage: Checkout message should use InteractiveMessage interface. Message row id = ");
                            sb.append(r7.A11);
                            Log.e(sb.toString());
                        }
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("InteractiveMessageCheckoutInfoConverter/updateOrderPaymentMethodInCheckoutInfoMessage can not find origin checkout NFM with reference id: ");
                    sb2.append(string);
                    Log.e(sb2.toString());
                }
            }
            return null;
        } catch (JSONException e) {
            Log.e("InteractiveMessageCheckoutInfoConverter/updateOrderPaymentMethodInCheckoutInfoMessage failed to parse parameters json", e);
            return null;
        }
    }

    public static List A05(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (!(jSONArray == null || jSONArray.length() == 0)) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                arrayList.add(new AnonymousClass1ZJ(jSONObject.getString("uri"), jSONObject.getString("type")));
            }
        }
        return arrayList;
    }

    public static JSONObject A06(AnonymousClass1ZK r6) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("status", r6.A01);
        Object obj = r6.A00;
        if (obj != null) {
            jSONObject.put("description", obj);
        }
        AnonymousClass1ZI r0 = r6.A05;
        if (r0 != null) {
            jSONObject.put("subtotal", A07(r0));
        }
        AnonymousClass1ZI r02 = r6.A06;
        if (r02 != null) {
            jSONObject.put("tax", A07(r02));
        }
        AnonymousClass1ZI r03 = r6.A03;
        if (r03 != null) {
            String str = r6.A07;
            JSONObject A07 = A07(r03);
            if (!TextUtils.isEmpty(str)) {
                A07.put("discount_program_name", str);
            }
            jSONObject.put("discount", A07);
        }
        AnonymousClass1ZI r04 = r6.A04;
        if (r04 != null) {
            jSONObject.put("shipping", A07(r04));
        }
        AnonymousClass2Bg r1 = r6.A02;
        if (r1 != null) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("timestamp", r1.A00);
            String str2 = r1.A01;
            if (!TextUtils.isEmpty(str2)) {
                jSONObject2.put("description", str2);
            }
            jSONObject.put("expiration", jSONObject2);
        }
        List<C66023Lz> list = r6.A08;
        JSONArray jSONArray = new JSONArray();
        for (C66023Lz r5 : list) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("retailer_id", r5.A05);
            String str3 = r5.A04;
            if (!TextUtils.isEmpty(str3)) {
                jSONObject3.put("product_id", str3);
            }
            jSONObject3.put("name", r5.A03);
            jSONObject3.put("amount", A07(r5.A01));
            jSONObject3.put("quantity", r5.A00);
            AnonymousClass1ZI r05 = r5.A02;
            if (r05 != null) {
                jSONObject3.put("sale_amount", A07(r05));
            }
            jSONArray.put(jSONObject3);
        }
        jSONObject.put("items", jSONArray);
        return jSONObject;
    }

    public static JSONObject A07(AnonymousClass1ZI r4) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("value", r4.A01);
        jSONObject.put("offset", r4.A00);
        String str = r4.A02;
        if (!TextUtils.isEmpty(str)) {
            jSONObject.put("description", str);
        }
        return jSONObject;
    }

    public static JSONObject A08(AnonymousClass1ZD r6, boolean z) {
        if (r6 == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        AbstractC30791Yv r0 = r6.A03;
        if (r0 != null) {
            jSONObject.put("currency", ((AbstractC30781Yu) r0).A04);
        }
        String str = r6.A06;
        if (str != null) {
            jSONObject.put("payment_configuration", str);
        }
        if (!z) {
            AnonymousClass1ZI r02 = r6.A05;
            if (r02 != null) {
                jSONObject.put("total_amount", A07(r02));
            }
            jSONObject.put("reference_id", r6.A07);
        }
        String str2 = r6.A09;
        if (str2 != null) {
            jSONObject.put("type", str2);
        }
        String str3 = r6.A01;
        if (str3 != null) {
            jSONObject.put("payment_method", str3);
        }
        long j = r6.A00;
        if (j > 0) {
            jSONObject.put("payment_timestamp", j);
        }
        jSONObject.put("order", A06(r6.A04));
        return jSONObject;
    }

    public static void A09(C14900mE r12, C15650ng r13, C20370ve r14, AnonymousClass1QA r15, AnonymousClass1A7 r16, AbstractC16390ow r17, AbstractC14440lR r18) {
        AnonymousClass1ZD r0;
        Set set;
        C16470p4 ABf = r17.ABf();
        if (!(ABf == null || (r0 = ABf.A01) == null)) {
            AnonymousClass1ZK r02 = r0.A04;
            AnonymousClass2Bg r1 = r02.A02;
            int A002 = AnonymousClass1ZD.A00(r02.A01);
            if (r1 != null && r1.A00 <= System.currentTimeMillis() / 1000 && (set = (Set) AnonymousClass1ZD.A0D.get(Integer.valueOf(A002))) != null && set.contains(4)) {
                String str = r17.ABf().A01.A02;
                if (TextUtils.isEmpty(str)) {
                    r18.Ab2(new RunnableBRunnable0Shape4S0200000_I0_4(r17, 47, r13));
                    r15.AWZ();
                    return;
                }
                r18.Ab2(new RunnableBRunnable0Shape0S1700000_I0(r12, r13, r14, r15, r16, r17, r18, str));
                return;
            }
        }
        r15.AWX();
    }
}
