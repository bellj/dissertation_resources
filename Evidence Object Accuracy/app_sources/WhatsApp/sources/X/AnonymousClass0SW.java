package X;

import java.util.IdentityHashMap;
import java.util.Map;

/* renamed from: X.0SW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SW {
    public static final Map A03 = new IdentityHashMap();
    public int A00 = 1;
    public Object A01;
    public final AbstractC12240hb A02;

    public AnonymousClass0SW(AbstractC12240hb r4, Object obj) {
        this.A01 = obj;
        this.A02 = r4;
        Map map = A03;
        synchronized (map) {
            Integer num = (Integer) map.get(obj);
            if (num == null) {
                map.put(obj, 1);
            } else {
                map.put(obj, Integer.valueOf(num.intValue() + 1));
            }
        }
    }

    public synchronized Object A00() {
        return this.A01;
    }

    public final void A01() {
        boolean z;
        synchronized (this) {
            z = false;
            if (this.A00 > 0) {
                z = true;
            }
        }
        if (!z) {
            throw new C10780fC();
        }
    }
}
