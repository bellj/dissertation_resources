package X;

import androidx.appcompat.view.menu.ActionMenuItemView;

/* renamed from: X.0CX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CX extends AnonymousClass0WW {
    public final /* synthetic */ ActionMenuItemView A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CX(ActionMenuItemView actionMenuItemView) {
        super(actionMenuItemView);
        this.A00 = actionMenuItemView;
    }

    @Override // X.AnonymousClass0WW
    public AbstractC12600iB A00() {
        AnonymousClass0CO r0;
        AnonymousClass0KC r02 = this.A00.A04;
        if (r02 == null || (r0 = ((AnonymousClass0CH) r02).A00.A0D) == null) {
            return null;
        }
        return r0.A00();
    }

    @Override // X.AnonymousClass0WW
    public boolean A03() {
        AbstractC12600iB A00;
        ActionMenuItemView actionMenuItemView = this.A00;
        AbstractC11620ga r2 = actionMenuItemView.A05;
        if (r2 == null || !r2.AJA(actionMenuItemView.A06) || (A00 = A00()) == null || !A00.AK4()) {
            return false;
        }
        return true;
    }
}
