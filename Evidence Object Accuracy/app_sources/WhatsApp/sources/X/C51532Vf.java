package X;

import android.view.View;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.Jid;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.2Vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51532Vf implements AbstractC33091dK {
    public final /* synthetic */ SearchViewModel A00;

    @Override // X.AbstractC33091dK
    public void A7D() {
    }

    @Override // X.AbstractC33091dK
    public AbstractC14640lm ADL() {
        return null;
    }

    @Override // X.AbstractC33091dK
    public boolean AaG(Jid jid) {
        return false;
    }

    public C51532Vf(SearchViewModel searchViewModel) {
        this.A00 = searchViewModel;
    }

    @Override // X.AbstractC33091dK
    public List AFi() {
        return this.A00.A0y.A0G.A02();
    }

    @Override // X.AbstractC33091dK
    public Set AGa() {
        return new HashSet();
    }

    @Override // X.AbstractC33091dK
    public void AO5(ViewHolder viewHolder, AbstractC14640lm r7, int i) {
        int A05;
        SearchViewModel searchViewModel = this.A00;
        searchViewModel.A0H.A0B(Boolean.FALSE);
        searchViewModel.A0O(1);
        if (r7 != null) {
            if (searchViewModel.A0Z()) {
                A05 = 117;
            } else {
                A05 = searchViewModel.A05();
            }
            searchViewModel.A0z.A00(5, A05);
            searchViewModel.A0O.A0B(r7);
        }
    }

    @Override // X.AbstractC33091dK
    public void AO6(View view, ViewHolder viewHolder, AbstractC14640lm r6, int i, int i2) {
        SearchViewModel searchViewModel = this.A00;
        searchViewModel.A0H.A0B(Boolean.FALSE);
        if (r6 != null) {
            searchViewModel.A0Q.A0B(r6);
        }
    }

    @Override // X.AbstractC33091dK
    public void AO7(ViewHolder viewHolder, AbstractC15340mz r3) {
        this.A00.A0S(r3);
    }

    @Override // X.AbstractC33091dK
    public void AO8(AnonymousClass1JV r2) {
        Log.e("SearchViewModel/pending group in search results");
    }

    @Override // X.AbstractC33091dK
    public void ASJ(View view, ViewHolder viewHolder, AbstractC14640lm r4, int i) {
        this.A00.A0P.A0B(r4);
    }
}
