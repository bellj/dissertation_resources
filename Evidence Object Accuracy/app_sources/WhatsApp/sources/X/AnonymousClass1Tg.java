package X;

import java.nio.MappedByteBuffer;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Tg  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Tg {
    public C29511Tj A00;
    public final ConcurrentHashMap A01 = new ConcurrentHashMap();
    public final ConcurrentHashMap A02 = new ConcurrentHashMap();

    public AnonymousClass1Tg(MappedByteBuffer mappedByteBuffer, List list) {
        if (mappedByteBuffer != null) {
            this.A00 = new C29511Tj(mappedByteBuffer, list);
        }
    }

    public String A00(AnonymousClass1Te r15, Object obj, int i) {
        ConcurrentHashMap concurrentHashMap = this.A01;
        Integer valueOf = Integer.valueOf(i);
        String[] strArr = (String[]) concurrentHashMap.get(valueOf);
        if (strArr == null) {
            C29511Tj r9 = this.A00;
            if (r9 != null) {
                synchronized (r9) {
                    int i2 = r9.A01.get(i);
                    if (i2 != 0) {
                        MappedByteBuffer mappedByteBuffer = r9.A03;
                        mappedByteBuffer.position(i2);
                        int position = mappedByteBuffer.position();
                        byte b = mappedByteBuffer.get(position);
                        int i3 = position + 1;
                        strArr = new String[6];
                        for (int i4 = 0; i4 < b; i4++) {
                            byte b2 = mappedByteBuffer.get(i3);
                            int i5 = i3 + 1;
                            int A01 = r9.A01(i5);
                            int i6 = i5 + 4;
                            int A00 = r9.A00(i6);
                            i3 = i6 + 2;
                            byte[] bArr = new byte[A00];
                            mappedByteBuffer.position(r9.A00 + A01);
                            mappedByteBuffer.get(bArr, 0, A00);
                            strArr[b2] = new String(bArr, 0, A00, r9.A04);
                        }
                        concurrentHashMap.put(valueOf, strArr);
                    }
                }
            }
            return null;
        }
        int A04 = r15.A04(obj);
        char c = 1;
        if (A04 != 1) {
            c = 2;
            if (A04 != 2) {
                c = 4;
                if (A04 == 4) {
                    c = 3;
                } else if (A04 != 8) {
                    c = 5;
                    if (A04 != 16) {
                        c = 0;
                    }
                }
            }
        }
        String str = strArr[c];
        if (str == null) {
            return strArr[0];
        }
        return str;
    }
}
