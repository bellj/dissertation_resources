package X;

import android.os.Bundle;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.15Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15Q {
    public long A00 = -1;
    public final C14830m7 A01;
    public final AnonymousClass10Y A02;
    public final Map A03 = new HashMap();

    public AnonymousClass15Q(C14830m7 r3, AnonymousClass10Y r4) {
        this.A01 = r3;
        this.A02 = r4;
    }

    public void A00(Bundle bundle) {
        if (bundle != null) {
            this.A00 = bundle.getLong("ephemeral_session_start", -1);
        }
    }

    public void A01(Bundle bundle) {
        bundle.putLong("ephemeral_session_start", this.A00);
    }
}
