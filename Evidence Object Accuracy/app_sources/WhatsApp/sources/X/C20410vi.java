package X;

/* renamed from: X.0vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20410vi implements AbstractC20200vN {
    public final C20380vf A00;
    public final C20400vh A01;
    public final C20390vg A02;

    public C20410vi(C20380vf r1, C20400vh r2, C20390vg r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC20200vN
    public void ANK() {
        this.A00.A01();
        this.A02.A01();
        this.A01.A00();
    }
}
