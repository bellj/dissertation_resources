package X;

/* renamed from: X.227  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass227 extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass227() {
        super(2602, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdAppStateOfflineNotifications {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "redundantCount", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
