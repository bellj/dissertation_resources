package X;

import java.util.List;

/* renamed from: X.2g4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54032g4 extends AnonymousClass0Q0 {
    public final /* synthetic */ C54502gp A00;
    public final /* synthetic */ List A01;

    public C54032g4(C54502gp r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.A04.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        return ((C89954Ma) this.A01.get(i)).A01.equals(((C89954Ma) this.A00.A04.get(i2)).A01);
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        return C12960it.A1T((((long) ((C89954Ma) this.A01.get(i)).A01.A00) > ((long) ((C89954Ma) this.A00.A04.get(i2)).A01.A00) ? 1 : (((long) ((C89954Ma) this.A01.get(i)).A01.A00) == ((long) ((C89954Ma) this.A00.A04.get(i2)).A01.A00) ? 0 : -1)));
    }
}
