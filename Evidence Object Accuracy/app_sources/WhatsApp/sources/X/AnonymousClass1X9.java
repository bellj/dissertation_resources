package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;

/* renamed from: X.1X9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1X9 extends AnonymousClass1Iv implements AbstractC16420oz {
    public long A00;
    public String A01;

    public AnonymousClass1X9(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 56, j);
    }

    public AnonymousClass1X9(AnonymousClass1IS r3, C40711sC r4, C40711sC r5, String str, long j, long j2, long j3) {
        super(r3, (byte) 56, j);
        ((AnonymousClass1Iv) this).A02 = r4;
        ((AnonymousClass1Iv) this).A01 = r5;
        ((AnonymousClass1Iv) this).A00 = j2;
        this.A01 = str;
        ((AbstractC15340mz) this).A01 = TextUtils.isEmpty(str) ? 7 : 0;
        this.A00 = j3;
    }

    public AnonymousClass1X9(AnonymousClass2RJ r5, AnonymousClass1IS r6, long j) {
        super(r6, (byte) 56, j);
        AnonymousClass1G8 r2 = r5.A02;
        r2 = r2 == null ? AnonymousClass1G8.A05 : r2;
        AbstractC14640lm A01 = AbstractC14640lm.A01(r2.A03);
        AnonymousClass009.A05(A01);
        AnonymousClass1IS r22 = new AnonymousClass1IS(A01, r2.A01, r2.A04);
        AnonymousClass1G8 r0 = r5.A02;
        ((AnonymousClass1Iv) this).A02 = new C40711sC(UserJid.getNullable((r0 == null ? AnonymousClass1G8.A05 : r0).A02), r22);
        String str = r5.A04;
        this.A01 = str;
        ((AbstractC15340mz) this).A01 = TextUtils.isEmpty(str) ? 7 : 0;
        this.A00 = r5.A01;
    }

    @Override // X.AnonymousClass1Iv
    public void A15(Cursor cursor, C18460sU r6, HashMap hashMap) {
        super.A15(cursor, r6, hashMap);
        int A00 = AnonymousClass1Tx.A00("reaction", hashMap);
        int A002 = AnonymousClass1Tx.A00("sender_timestamp", hashMap);
        String string = cursor.getString(A00);
        long j = cursor.getLong(A002);
        this.A01 = string;
        boolean isEmpty = TextUtils.isEmpty(string);
        int i = 0;
        if (isEmpty) {
            i = 7;
        }
        ((AbstractC15340mz) this).A01 = i;
        this.A00 = j;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r8) {
        AbstractC14640lm r2;
        String str;
        AnonymousClass1IS r0;
        AnonymousClass1G3 r5 = r8.A03;
        AnonymousClass2RJ r02 = ((C27081Fy) r5.A00).A0X;
        if (r02 == null) {
            r02 = AnonymousClass2RJ.A05;
        }
        AnonymousClass1G4 A0T = r02.A0T();
        AnonymousClass1G8 r03 = ((AnonymousClass2RJ) A0T.A00).A02;
        if (r03 == null) {
            r03 = AnonymousClass1G8.A05;
        }
        AnonymousClass1G9 r6 = (AnonymousClass1G9) r03.A0T();
        AnonymousClass1IS A14 = A14();
        C40711sC r04 = ((AnonymousClass1Iv) this).A02;
        if (r04 == null) {
            r2 = null;
        } else {
            r2 = r04.A00;
        }
        C40711sC r1 = ((AnonymousClass1Iv) this).A01;
        if (!(r1 == null || (r0 = r1.A01) == null)) {
            A14 = r0;
            r2 = r1.A00;
        }
        AnonymousClass009.A05(A14);
        C40721sD.A02(r2, r6, A14);
        A0T.A03();
        AnonymousClass2RJ r12 = (AnonymousClass2RJ) A0T.A00;
        r12.A02 = (AnonymousClass1G8) r6.A02();
        r12.A00 |= 1;
        if (!TextUtils.isEmpty(this.A01)) {
            str = this.A01;
        } else {
            str = "";
        }
        A0T.A03();
        AnonymousClass2RJ r13 = (AnonymousClass2RJ) A0T.A00;
        r13.A00 |= 2;
        r13.A04 = str;
        long j = this.A00;
        A0T.A03();
        AnonymousClass2RJ r14 = (AnonymousClass2RJ) A0T.A00;
        r14.A00 |= 8;
        r14.A01 = j;
        r5.A03();
        C27081Fy r15 = (C27081Fy) r5.A00;
        r15.A0X = (AnonymousClass2RJ) A0T.A02();
        r15.A01 |= 16;
    }
}
