package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;

/* renamed from: X.21q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C454521q implements AbstractC39501q0 {
    public final /* synthetic */ AnonymousClass21W A00;

    public C454521q(AnonymousClass21W r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC39501q0
    public void APk() {
        RecyclerView recyclerView = this.A00.A0A.A0Q;
        if (recyclerView != null) {
            recyclerView.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 11));
        }
    }

    @Override // X.AbstractC39501q0
    public /* bridge */ /* synthetic */ void AUc(Object obj) {
        RecyclerView recyclerView = this.A00.A0A.A0Q;
        if (recyclerView != null) {
            recyclerView.post(new RunnableBRunnable0Shape6S0200000_I0_6(this, 3, obj));
        }
    }
}
