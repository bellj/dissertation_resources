package X;

import com.whatsapp.util.Log;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0vS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20250vS implements AbstractC20260vT {
    public final C14830m7 A00;
    public final HashMap A01 = new HashMap();

    public C20250vS(C14830m7 r2) {
        this.A00 = r2;
    }

    public C29381Ry A00(String str) {
        C29381Ry r4;
        InetAddress[] inetAddressArr;
        StringBuilder sb = new StringBuilder("resolving ");
        sb.append(str);
        Log.i(sb.toString());
        synchronized (this) {
            HashMap hashMap = this.A01;
            List<C31041Zu> list = (List) hashMap.get(str);
            if (list == null) {
                r4 = null;
            } else {
                ArrayList arrayList = new ArrayList();
                HashSet hashSet = new HashSet();
                int i = 0;
                for (C31041Zu r5 : list) {
                    C14830m7 r1 = this.A00;
                    Long l = r5.A01;
                    if (l == null || r1.A00() < l.longValue()) {
                        arrayList.add(r5.A03);
                        i = r5.A00;
                    } else {
                        hashSet.add(r5);
                    }
                }
                list.removeAll(hashSet);
                if (list.isEmpty()) {
                    hashMap.remove(str);
                }
                r4 = new C29381Ry(new C31051Zv(i), (InetAddress[]) arrayList.toArray(new InetAddress[0]));
            }
        }
        if (r4 == null || (inetAddressArr = r4.A04) == null || inetAddressArr.length <= 0) {
            try {
                InetAddress[] allByName = InetAddress.getAllByName(str);
                A02(Arrays.asList(allByName), str, 0);
                r4 = new C29381Ry(new C31051Zv(0), allByName);
            } catch (UnknownHostException e) {
                StringBuilder sb2 = new StringBuilder("primary dns resolution failed for ");
                sb2.append(str);
                Log.w(sb2.toString(), e);
                try {
                    List<C31071Zx> A01 = C31061Zw.A01(str, 0);
                    ArrayList arrayList2 = new ArrayList(A01.size());
                    for (C31071Zx r0 : A01) {
                        arrayList2.add(r0.A01);
                    }
                    A02(arrayList2, str, 1);
                    r4 = new C29381Ry(new C31051Zv(1), (InetAddress[]) arrayList2.toArray(new InetAddress[0]));
                } catch (UnknownHostException e2) {
                    StringBuilder sb3 = new StringBuilder("secondary dns resolution failed for ");
                    sb3.append(str);
                    Log.w(sb3.toString(), e2);
                    try {
                        r4 = A01(str, true);
                    } catch (UnknownHostException e3) {
                        StringBuilder sb4 = new StringBuilder("hardcoded ip resolution failed for ");
                        sb4.append(str);
                        Log.w(sb4.toString(), e3);
                        throw e;
                    }
                }
            }
        }
        Arrays.toString(r4.A04);
        return r4;
    }

    public final C29381Ry A01(String str, boolean z) {
        List list = (List) C31081Zy.A00.get(str);
        if (list == null || list.isEmpty()) {
            StringBuilder sb = new StringBuilder("no hardcoded ips found for ");
            sb.append(str);
            throw new UnknownHostException(sb.toString());
        }
        if (z) {
            A02(list, str, 2);
        }
        return new C29381Ry(new C31051Zv(2), (InetAddress[]) list.toArray(new InetAddress[0]));
    }

    public final void A02(Iterable iterable, String str, int i) {
        long currentTimeMillis = System.currentTimeMillis() + 3600000;
        ArrayList arrayList = new ArrayList();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            arrayList.add(new C31041Zu(Long.valueOf(currentTimeMillis), null, (InetAddress) it.next(), i, false, false));
        }
        synchronized (this) {
            this.A01.put(str, arrayList);
        }
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r2) {
        synchronized (this) {
            this.A01.clear();
        }
    }
}
