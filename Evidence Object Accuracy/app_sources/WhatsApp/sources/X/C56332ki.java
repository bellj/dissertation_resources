package X;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

/* renamed from: X.2ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56332ki extends AbstractC77873o0 {
    public final C92544Wi A00;
    public final C13690kA A01;

    @Override // X.AnonymousClass3HY
    public final /* bridge */ /* synthetic */ void A02(AnonymousClass3CU r1, boolean z) {
    }

    public C56332ki(C92544Wi r2, C13690kA r3) {
        super(4);
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass3HY
    public final void A01(Status status) {
        this.A01.A00(new C630439z(status));
    }

    @Override // X.AnonymousClass3HY
    public final void A03(C14970mL r5) {
        try {
            r5.A09.remove(this.A00);
            C13690kA r0 = this.A01;
            Boolean bool = Boolean.FALSE;
            C13600jz r2 = r0.A00;
            synchronized (r2.A04) {
                if (!r2.A02) {
                    r2.A02 = true;
                    r2.A01 = bool;
                    r2.A03.A01(r2);
                }
            }
        } catch (DeadObjectException e) {
            A01(AnonymousClass3HY.A00(e));
            throw e;
        } catch (RemoteException e2) {
            A01(AnonymousClass3HY.A00(e2));
        } catch (RuntimeException e3) {
            this.A01.A00(e3);
        }
    }

    @Override // X.AnonymousClass3HY
    public final void A04(Exception exc) {
        this.A01.A00(exc);
    }

    @Override // X.AbstractC77873o0
    public final boolean A05(C14970mL r3) {
        r3.A09.get(this.A00);
        return false;
    }

    @Override // X.AbstractC77873o0
    public final C78603pB[] A06(C14970mL r3) {
        r3.A09.get(this.A00);
        return null;
    }
}
