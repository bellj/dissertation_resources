package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0wL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20800wL {
    public final C251818k A00;
    public final C22680zT A01;
    public final C15570nT A02;
    public final C18790t3 A03;
    public final C18640sm A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C14820m6 A09;
    public final AnonymousClass018 A0A;
    public final C20760wH A0B;
    public final C18810t5 A0C;
    public final C19370u0 A0D;
    public final C251418g A0E;
    public final C18800t4 A0F;
    public final C19930uu A0G;
    public final AbstractC14440lR A0H;
    public final C22870zm A0I;
    public final C251918l A0J;

    public C20800wL(C251818k r2, C22680zT r3, C15570nT r4, C18790t3 r5, C18640sm r6, AnonymousClass01d r7, C14830m7 r8, C16590pI r9, C15890o4 r10, C14820m6 r11, AnonymousClass018 r12, C20760wH r13, C18810t5 r14, C19370u0 r15, C251418g r16, C18800t4 r17, C19930uu r18, AbstractC14440lR r19, C22870zm r20, C251918l r21) {
        this.A07 = r9;
        this.A06 = r8;
        this.A02 = r4;
        this.A0G = r18;
        this.A0H = r19;
        this.A03 = r5;
        this.A0I = r20;
        this.A0D = r15;
        this.A05 = r7;
        this.A0A = r12;
        this.A01 = r3;
        this.A0E = r16;
        this.A0F = r17;
        this.A0C = r14;
        this.A08 = r10;
        this.A09 = r11;
        this.A00 = r2;
        this.A0B = r13;
        this.A04 = r6;
        this.A0J = r21;
    }

    public C29131Qz A00(AnonymousClass4V6 r22, String str, String str2, String str3, String str4, String str5, JSONObject jSONObject, byte[] bArr, boolean z) {
        byte[] A0C;
        int simState;
        String str6;
        String str7;
        A05();
        byte[] A08 = A08(str, str2);
        if (z) {
            A0C = A07(str2);
        } else {
            A0C = C003501n.A0C();
            C49352Kk.A00(this.A07.A00, this.A06, this.A09, str2, A0C);
        }
        boolean A082 = this.A08.A08();
        AnonymousClass01d r1 = this.A05;
        TelephonyManager A0N = r1.A0N();
        if (A0N == null) {
            simState = -1;
        } else {
            simState = A0N.getSimState();
        }
        String valueOf = String.valueOf(simState);
        TelephonyManager A0N2 = r1.A0N();
        String str8 = "";
        if (A0N2 != null) {
            str6 = A0N2.getNetworkOperatorName();
            str8 = A0N2.getSimOperatorName();
        } else {
            str6 = str8;
        }
        Log.i("http/registration/wamsys/checkifexists");
        HashMap hashMap = new HashMap();
        hashMap.put("token", str3.getBytes());
        hashMap.put("mistyped", str4.getBytes());
        if (jSONObject != null) {
            hashMap.put("offline_ab", jSONObject.toString().getBytes());
        }
        JSONObject A00 = r22.A00();
        try {
            A00.put("was_activated_from_stub", this.A07.A00.getSharedPreferences("downloader_stub", 0).getBoolean("activated", false));
        } catch (JSONException unused) {
            Log.e("Failed to add stub activation metric.");
        }
        hashMap.put("client_metrics", A00.toString().getBytes());
        if (A082) {
            str7 = "1";
        } else {
            str7 = "0";
        }
        hashMap.put("read_phone_permission_granted", str7.getBytes());
        hashMap.put("sim_state", valueOf.getBytes());
        hashMap.put("network_operator_name", str6.getBytes());
        hashMap.put("sim_operator_name", str8.getBytes());
        SharedPreferences sharedPreferences = this.A09.A00;
        String l = Long.toString(sharedPreferences.getLong("language_selector_time_spent", 0));
        String num = Integer.toString(sharedPreferences.getInt("language_selector_clicked_count", 0));
        hashMap.put("language_selector_time_spent", l.getBytes());
        hashMap.put("language_selector_clicked_count", num.getBytes());
        sharedPreferences.edit().remove("language_selector_clicked_count").apply();
        sharedPreferences.edit().remove("language_selector_time_spent").apply();
        A06(hashMap);
        return (C29131Qz) AbstractC29111Qx.A00(new C29101Qw(this.A00, this.A0J, str, str2, str5, AnonymousClass3HG.A00(this.A01, this.A07, str, "exist_entrypoint"), A02(), hashMap, A08, A0C, bArr));
    }

    public AnonymousClass1R5 A01(AnonymousClass4V6 r15, String str, String str2, String str3, String str4) {
        byte[] bArr;
        int i;
        A05();
        byte[] A08 = A08(str, str2);
        byte[] A07 = A07(str2);
        Log.i("http/registration/wamsys/resetsecuritycode");
        HashMap hashMap = new HashMap();
        hashMap.put("client_metrics", r15.A00().toString().getBytes());
        A06(hashMap);
        if (str4 != null) {
            bArr = str4.getBytes();
        } else {
            bArr = null;
        }
        if ("wipe".equals(str3)) {
            i = 2;
        } else {
            i = 0;
            if ("email".equals(str3)) {
                i = 1;
            }
        }
        return (AnonymousClass1R5) AbstractC29111Qx.A00(new AnonymousClass1R3(this.A00, this.A0J, null, str, str2, AnonymousClass3HG.A00(this.A01, this.A07, str, "security_entrypoint"), A02(), hashMap, bArr, A08, A07, i));
    }

    public final List A02() {
        ArrayList arrayList = new ArrayList();
        C20760wH r2 = this.A0B;
        try {
            r2.A00();
        } catch (IOException e) {
            Log.e("DomainFrontingManager/get-providers/error getting providers from the file", e);
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(r2.A07);
        arrayList2.addAll(r2.A08);
        arrayList2.addAll(r2.A06);
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            C91374Rm r3 = (C91374Rm) it.next();
            StringBuilder sb = new StringBuilder();
            sb.append(r3.A02);
            sb.append(" ");
            sb.append(r3.A01);
            sb.append(" ");
            sb.append(r3.A03);
            arrayList.add(sb.toString());
        }
        return arrayList;
    }

    public final Map A03(C863046q r6, AnonymousClass23L r7, AnonymousClass23L r8, String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        if (str2 != null) {
            hashMap.put("mistyped", str2.getBytes());
        }
        hashMap.put("reason", str.getBytes());
        if (str3 != null) {
            hashMap.put("hasav", str3.getBytes());
        }
        hashMap.put("client_metrics", r6.A00().toString().getBytes());
        hashMap.put("mcc", r7.A00.getBytes());
        hashMap.put("mnc", r7.A01.getBytes());
        hashMap.put("sim_mcc", r8.A00.getBytes());
        hashMap.put("sim_mnc", r8.A01.getBytes());
        SharedPreferences sharedPreferences = this.A09.A00;
        hashMap.put("education_screen_displayed", String.valueOf(sharedPreferences.getBoolean("pref_flash_call_education_screen_displayed", false)).getBytes());
        hashMap.put("prefer_sms_over_flash", String.valueOf(sharedPreferences.getBoolean("pref_prefer_sms_over_flash", false)).getBytes());
        A06(hashMap);
        return hashMap;
    }

    public final Map A04(C862946p r9, AnonymousClass4AY r10, String str) {
        String str2;
        String str3;
        int i = 1;
        switch (r10.ordinal()) {
            case 0:
                break;
            case 1:
                i = 2;
                break;
            case 2:
                i = 3;
                break;
            case 3:
                i = 4;
                break;
            default:
                Log.w("http/registration/entrymethod/unknown");
                i = 0;
                break;
        }
        String valueOf = String.valueOf(i);
        TelephonyManager A0N = this.A05.A0N();
        String str4 = null;
        if (A0N != null) {
            str2 = A0N.getNetworkOperator();
        } else {
            str2 = null;
        }
        AnonymousClass23L A00 = AnonymousClass23L.A00(str2);
        if (A0N != null) {
            str4 = A0N.getSimOperator();
        }
        AnonymousClass23L A002 = AnonymousClass23L.A00(str4);
        String str5 = "";
        if (A0N != null) {
            str3 = A0N.getNetworkOperatorName();
            str5 = A0N.getSimOperatorName();
        } else {
            str3 = str5;
        }
        Log.i("http/registration/wamsys/verifycode");
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put("mistyped", str.getBytes());
        }
        hashMap.put("client_metrics", r9.A00().toString().getBytes());
        hashMap.put("entered", valueOf.getBytes());
        hashMap.put("mcc", A00.A00.getBytes());
        hashMap.put("mnc", A00.A01.getBytes());
        hashMap.put("sim_mcc", A002.A00.getBytes());
        hashMap.put("sim_mnc", A002.A01.getBytes());
        hashMap.put("network_operator_name", str3.getBytes());
        hashMap.put("sim_operator_name", str5.getBytes());
        A06(hashMap);
        return hashMap;
    }

    public void A05() {
        String str = this.A0D.A00().A01;
        C22870zm r0 = this.A0I;
        C16590pI r2 = this.A07;
        C19930uu r6 = this.A0G;
        AbstractC14440lR r7 = this.A0H;
        r0.A03(this.A03, r2, this.A09, this.A0A, this.A0F, r6, r7, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002f, code lost:
        if (r2 < 6) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(java.util.Map r7) {
        /*
            r6 = this;
            r7.size()
            X.0sm r0 = r6.A04
            X.1I0 r0 = r0.A07()
            java.lang.Integer r0 = X.C22050yP.A01(r0)
            if (r0 != 0) goto L_0x0087
            r0 = -1
        L_0x0010:
            java.lang.String r0 = java.lang.String.valueOf(r0)
            byte[] r1 = r0.getBytes()
            java.lang.String r0 = "network_radio_type"
            r7.put(r0, r1)
            X.0pI r5 = r6.A07
            X.01d r1 = r6.A05
            X.0o4 r0 = r6.A08
            java.lang.String r0 = X.AnonymousClass23M.A0D(r1, r5, r0)
            if (r0 == 0) goto L_0x0031
            int r2 = r0.length()
            r1 = 6
            r0 = 1
            if (r2 >= r1) goto L_0x0032
        L_0x0031:
            r0 = 0
        L_0x0032:
            java.lang.String r4 = "1"
            java.lang.String r3 = "0"
            r1 = r3
            if (r0 == 0) goto L_0x003a
            r1 = r4
        L_0x003a:
            byte[] r1 = r1.getBytes()
            java.lang.String r0 = "simnum"
            r7.put(r0, r1)
            android.content.Context r0 = r5.A00
            java.io.File r2 = r0.getFilesDir()
            java.lang.String r1 = "rc2"
            java.io.File r0 = new java.io.File
            r0.<init>(r2, r1)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0058
            r4 = r3
        L_0x0058:
            byte[] r1 = r4.getBytes()
            java.lang.String r0 = "hasinrc"
            r7.put(r0, r1)
            int r0 = android.os.Process.myPid()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            byte[] r1 = r0.getBytes()
            java.lang.String r0 = "pid"
            r7.put(r0, r1)
            X.2TB r0 = X.AnonymousClass2TB.A01
            int r0 = r0.value
            java.lang.String r0 = java.lang.String.valueOf(r0)
            byte[] r1 = r0.getBytes()
            java.lang.String r0 = "rc"
            r7.put(r0, r1)
            r7.size()
            return
        L_0x0087:
            int r0 = r0.intValue()
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20800wL.A06(java.util.Map):void");
    }

    public final byte[] A07(String str) {
        Context context = this.A07.A00;
        byte[] A04 = C49352Kk.A04(context, str);
        if (A04 != null) {
            return A04;
        }
        byte[] A0C = C003501n.A0C();
        C49352Kk.A00(context, this.A06, this.A09, str, A0C);
        return A0C;
    }

    public byte[] A08(String str, String str2) {
        Context context = this.A07.A00;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        String A00 = C32591cP.A00(sb.toString());
        byte[] A0F = C003501n.A0F(context, A00);
        if (A0F != null) {
            return A0F;
        }
        byte[] A0C = C003501n.A0C();
        C003501n.A0A(context, A00, A0C);
        return A0C;
    }
}
