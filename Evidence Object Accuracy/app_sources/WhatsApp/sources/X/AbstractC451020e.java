package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.20e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC451020e implements AbstractC21730xt {
    public final Context A00;
    public final C14900mE A01;
    public final C18650sn A02;

    public abstract void A02(C452120p v);

    public abstract void A03(C452120p v);

    public abstract void A04(AnonymousClass1V8 v);

    public AbstractC451020e(Context context, C14900mE r2, C18650sn r3) {
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static List A00(AnonymousClass1V8 r7) {
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1V8 r6 : r7.A0J("error")) {
            if (r6 != null) {
                String A0I = r6.A0I("code", null);
                String A0I2 = r6.A0I("text", null);
                String A0I3 = r6.A0I("display_title", null);
                String A0I4 = r6.A0I("display_text", null);
                if (A0I != null) {
                    int parseInt = Integer.parseInt(A0I);
                    C452120p r1 = new C452120p();
                    r1.A00 = parseInt;
                    r1.A09 = A0I2;
                    r1.A08 = A0I3;
                    r1.A07 = A0I4;
                    arrayList.add(r1);
                    if (parseInt == 454) {
                        r1.A05 = r6.A0E("step_up");
                    }
                }
            }
        }
        return arrayList;
    }

    public List A01(AnonymousClass1V8 r2) {
        return A00(r2);
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C452120p r3 = new C452120p();
        int i = 6;
        if (C18640sm.A03(this.A00)) {
            i = -2;
        }
        r3.A00 = i;
        this.A01.A0I(new RunnableBRunnable0Shape6S0200000_I0_6(this, 32, r3));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        for (C452120p r3 : A01(r6)) {
            C18650sn r0 = this.A02;
            int i = r3.A00;
            C18620sk r2 = r0.A00;
            if (r2 != null && (i == 404 || i == 440 || i == 449)) {
                r2.A01(true, false);
            }
            this.A01.A0I(new RunnableBRunnable0Shape6S0200000_I0_6(this, 31, r3));
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        this.A01.A0I(new RunnableBRunnable0Shape6S0200000_I0_6(this, 33, r4));
    }
}
