package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5y0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129615y0 {
    public String A00;
    public final C130155yt A01;
    public final C129865yQ A02;
    public final C129675y7 A03;

    public C129615y0(C130155yt r1, C129865yQ r2, C129675y7 r3) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
    }

    public void A00(ActivityC13790kL r8) {
        C129675y7 r0 = this.A03;
        C1316663q r4 = r0.A01;
        int i = r0.A00;
        if (r4 == null) {
            Log.e("PAY: TDSStepUpManager/performAnswerTds TDS step up is unable to answer, since activeStepUp is null");
            return;
        }
        C1310460z A01 = AnonymousClass61S.A01("novi-answer-3ds-step-up-challenge");
        AnonymousClass61S[] r6 = new AnonymousClass61S[2];
        AnonymousClass61S.A05("entry_flow", r4.A03, r6, 0);
        C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r4.A04), r6, 1));
        ArrayList arrayList = A01.A02;
        arrayList.add(A0B);
        if (!TextUtils.isEmpty(this.A00)) {
            C117305Zk.A1R("step_up_challenge", arrayList, AnonymousClass61S.A02("challenge_id", this.A00));
        }
        C117305Zk.A1R("three_ds", arrayList, AnonymousClass61S.A02("partner_session_id", "cko-session-id"));
        C130155yt.A02(new AbstractC136196Lo(r8, r4, this, i) { // from class: X.6A2
            public final /* synthetic */ int A00;
            public final /* synthetic */ ActivityC13790kL A01;
            public final /* synthetic */ C1316663q A02;
            public final /* synthetic */ C129615y0 A03;

            {
                this.A03 = r3;
                this.A02 = r2;
                this.A00 = r4;
                this.A01 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r82) {
                Object obj;
                C129615y0 r62 = this.A03;
                C1316663q r5 = this.A02;
                int i2 = this.A00;
                ActivityC13790kL r3 = this.A01;
                if (r82.A06() && (obj = r82.A02) != null) {
                    AnonymousClass1V8 r2 = (AnonymousClass1V8) obj;
                    if (TextUtils.equals(C117295Zj.A0W(r2, "challenge_result"), "SUCCESS")) {
                        AnonymousClass1V8[] r02 = r2.A03;
                        if (r02 == null || r02.length == 0) {
                            C127415uS.A00(r5, r62.A03, "PASS", i2);
                            return;
                        }
                        return;
                    }
                }
                C1316663q r1 = r82.A01;
                if (r1 != null) {
                    C125005qW.A00(r3, r1, r62.A03, i2);
                } else {
                    r62.A02.A02(r82.A00, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0045: INVOKE  
                          (wrap: X.5yQ : 0x003b: IGET  (r3v1 X.5yQ A[REMOVE]) = (r6v0 'r62' X.5y0) X.5y0.A02 X.5yQ)
                          (wrap: X.20p : 0x003d: IGET  (r2v0 X.20p A[REMOVE]) = (r8v0 'r82' X.5zy) X.5zy.A00 X.20p)
                          (wrap: X.6JS : 0x0041: CONSTRUCTOR  (r1v1 X.6JS A[REMOVE]) = (r5v0 'r5' X.63q), (r6v0 'r62' X.5y0), (r4v0 'i2' int) call: X.6JS.<init>(X.63q, X.5y0, int):void type: CONSTRUCTOR)
                          (null java.lang.Runnable)
                         type: VIRTUAL call: X.5yQ.A02(X.20p, java.lang.Runnable, java.lang.Runnable):void in method: X.6A2.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0041: CONSTRUCTOR  (r1v1 X.6JS A[REMOVE]) = (r5v0 'r5' X.63q), (r6v0 'r62' X.5y0), (r4v0 'i2' int) call: X.6JS.<init>(X.63q, X.5y0, int):void type: CONSTRUCTOR in method: X.6A2.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 21 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6JS, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 27 more
                        */
                    /*
                        this = this;
                        X.5y0 r6 = r7.A03
                        X.63q r5 = r7.A02
                        int r4 = r7.A00
                        X.0kL r3 = r7.A01
                        boolean r0 = r8.A06()
                        if (r0 == 0) goto L_0x0031
                        java.lang.Object r2 = r8.A02
                        if (r2 == 0) goto L_0x0031
                        X.1V8 r2 = (X.AnonymousClass1V8) r2
                        java.lang.String r0 = "challenge_result"
                        java.lang.String r1 = X.C117295Zj.A0W(r2, r0)
                        java.lang.String r0 = "SUCCESS"
                        boolean r0 = android.text.TextUtils.equals(r1, r0)
                        if (r0 == 0) goto L_0x0031
                        X.1V8[] r0 = r2.A03
                        if (r0 == 0) goto L_0x0029
                        int r0 = r0.length
                        if (r0 != 0) goto L_0x0030
                    L_0x0029:
                        X.5y7 r1 = r6.A03
                        java.lang.String r0 = "PASS"
                        X.C127415uS.A00(r5, r1, r0, r4)
                    L_0x0030:
                        return
                    L_0x0031:
                        X.63q r1 = r8.A01
                        if (r1 == 0) goto L_0x003b
                        X.5y7 r0 = r6.A03
                        X.C125005qW.A00(r3, r1, r0, r4)
                        return
                    L_0x003b:
                        X.5yQ r3 = r6.A02
                        X.20p r2 = r8.A00
                        X.6JS r1 = new X.6JS
                        r1.<init>(r5, r6, r4)
                        r0 = 0
                        r3.A02(r2, r1, r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A2.AV8(X.5zy):void");
                }
            }, this.A01, A01, i);
        }

        public void A01(ActivityC13790kL r7) {
            C129675y7 r0 = this.A03;
            C1316663q r3 = r0.A01;
            int i = r0.A00;
            if (r3 == null) {
                Log.e("PAY: TDSStepUpManager/performStartTds TDS step up is unable to start, since activeStepUp is null ");
                return;
            }
            AnonymousClass61D.A01(new AbstractC136196Lo(r7, r3, this, i) { // from class: X.6A1
                public final /* synthetic */ int A00;
                public final /* synthetic */ ActivityC13790kL A01;
                public final /* synthetic */ C1316663q A02;
                public final /* synthetic */ C129615y0 A03;

                {
                    this.A03 = r3;
                    this.A01 = r1;
                    this.A02 = r2;
                    this.A00 = r4;
                }

                @Override // X.AbstractC136196Lo
                public final void AV8(C130785zy r9) {
                    C129615y0 r6 = this.A03;
                    ActivityC13790kL r5 = this.A01;
                    C1316663q r72 = this.A02;
                    int i2 = this.A00;
                    if (r9.A06()) {
                        Object obj = r9.A02;
                        if (obj instanceof C121375hk) {
                            C121375hk r1 = (C121375hk) obj;
                            String str = r1.A02;
                            if (!AnonymousClass1US.A0C(str)) {
                                String str2 = r1.A01;
                                if (!AnonymousClass1US.A0C(str2)) {
                                    r6.A00 = r1.A00;
                                    r5.startActivityForResult(C14960mK.A0a(r5, str, str2, false, true), 7387);
                                    return;
                                }
                            }
                        }
                    }
                    Log.e("PAY: TDSStepUpManager/performStartTds unable to parse TDS Challenge or failed response");
                    r6.A02.A02(r9.A00, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0045: INVOKE  
                          (wrap: X.5yQ : 0x003b: IGET  (r3v0 X.5yQ A[REMOVE]) = (r6v0 'r6' X.5y0) X.5y0.A02 X.5yQ)
                          (wrap: X.20p : 0x003d: IGET  (r2v0 X.20p A[REMOVE]) = (r9v0 'r9' X.5zy) X.5zy.A00 X.20p)
                          (wrap: X.6JT : 0x0041: CONSTRUCTOR  (r1v0 X.6JT A[REMOVE]) = (r7v0 'r72' X.63q), (r6v0 'r6' X.5y0), (r4v0 'i2' int) call: X.6JT.<init>(X.63q, X.5y0, int):void type: CONSTRUCTOR)
                          (null java.lang.Runnable)
                         type: VIRTUAL call: X.5yQ.A02(X.20p, java.lang.Runnable, java.lang.Runnable):void in method: X.6A1.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0041: CONSTRUCTOR  (r1v0 X.6JT A[REMOVE]) = (r7v0 'r72' X.63q), (r6v0 'r6' X.5y0), (r4v0 'i2' int) call: X.6JT.<init>(X.63q, X.5y0, int):void type: CONSTRUCTOR in method: X.6A1.AV8(X.5zy):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6JT, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.5y0 r6 = r8.A03
                        X.0kL r5 = r8.A01
                        X.63q r7 = r8.A02
                        int r4 = r8.A00
                        boolean r0 = r9.A06()
                        if (r0 == 0) goto L_0x0036
                        java.lang.Object r1 = r9.A02
                        boolean r0 = r1 instanceof X.C121375hk
                        if (r0 == 0) goto L_0x0036
                        X.5hk r1 = (X.C121375hk) r1
                        java.lang.String r3 = r1.A02
                        boolean r0 = X.AnonymousClass1US.A0C(r3)
                        if (r0 != 0) goto L_0x0036
                        java.lang.String r2 = r1.A01
                        boolean r0 = X.AnonymousClass1US.A0C(r2)
                        if (r0 != 0) goto L_0x0036
                        java.lang.String r0 = r1.A00
                        r6.A00 = r0
                        r1 = 0
                        r0 = 1
                        android.content.Intent r1 = X.C14960mK.A0a(r5, r3, r2, r1, r0)
                        r0 = 7387(0x1cdb, float:1.0351E-41)
                        r5.startActivityForResult(r1, r0)
                        return
                    L_0x0036:
                        java.lang.String r0 = "PAY: TDSStepUpManager/performStartTds unable to parse TDS Challenge or failed response"
                        com.whatsapp.util.Log.e(r0)
                        X.5yQ r3 = r6.A02
                        X.20p r2 = r9.A00
                        X.6JT r1 = new X.6JT
                        r1.<init>(r7, r6, r4)
                        r0 = 0
                        r3.A02(r2, r1, r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A1.AV8(X.5zy):void");
                }
            }, this.A01, null, r3, "TDS", i);
        }
    }
