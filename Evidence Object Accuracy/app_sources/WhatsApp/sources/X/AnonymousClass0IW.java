package X;

import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.os.RemoteException;
import java.util.Arrays;

/* renamed from: X.0IW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IW extends AnonymousClass0SQ {
    public final NetworkStats.Bucket A00 = new NetworkStats.Bucket();
    public final NetworkStatsManager A01;

    @Override // X.AnonymousClass0SQ
    public boolean A01() {
        return true;
    }

    public AnonymousClass0IW(Context context) {
        this.A01 = (NetworkStatsManager) context.getSystemService(NetworkStatsManager.class);
    }

    @Override // X.AnonymousClass0SQ
    public boolean A02(long[] jArr) {
        try {
            Arrays.fill(jArr, 0L);
            A03(jArr, 0, 2);
            A03(jArr, 1, 0);
            return true;
        } catch (RemoteException | IllegalArgumentException | NullPointerException unused) {
            return false;
        }
    }

    public final void A03(long[] jArr, int i, int i2) {
        NetworkStats querySummary = this.A01.querySummary(i, null, Long.MIN_VALUE, Long.MAX_VALUE);
        while (querySummary.hasNextBucket()) {
            NetworkStats.Bucket bucket = this.A00;
            querySummary.getNextBucket(bucket);
            int i3 = 4;
            if (bucket.getState() == 2) {
                i3 = 0;
            }
            int i4 = i2 | 0 | i3;
            jArr[i4] = jArr[i4] + bucket.getRxBytes();
            int i5 = i3 | i2 | 1;
            jArr[i5] = jArr[i5] + bucket.getTxBytes();
        }
        querySummary.close();
    }
}
