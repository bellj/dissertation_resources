package X;

import android.view.View;
import android.widget.AdapterView;
import androidx.appcompat.app.AlertController$RecycleListView;

/* renamed from: X.0Ww  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07150Ww implements AdapterView.OnItemClickListener {
    public final /* synthetic */ AnonymousClass0OC A00;
    public final /* synthetic */ AlertController$RecycleListView A01;
    public final /* synthetic */ AnonymousClass0U5 A02;

    public C07150Ww(AnonymousClass0OC r1, AlertController$RecycleListView alertController$RecycleListView, AnonymousClass0U5 r3) {
        this.A00 = r1;
        this.A01 = alertController$RecycleListView;
        this.A02 = r3;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        AnonymousClass0OC r2 = this.A00;
        boolean[] zArr = r2.A0N;
        if (zArr != null) {
            zArr[i] = this.A01.isItemChecked(i);
        }
        r2.A09.onClick(this.A02.A0W, i, this.A01.isItemChecked(i));
    }
}
