package X;

import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6Cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133966Cu implements AbstractC136536Mx {
    public final /* synthetic */ BrazilOrderDetailsActivity A00;

    @Override // X.AbstractC136536Mx
    public void ATb(C30821Yy r1, AbstractC16390ow r2, String str, String str2, List list) {
    }

    public C133966Cu(BrazilOrderDetailsActivity brazilOrderDetailsActivity) {
        this.A00 = brazilOrderDetailsActivity;
    }

    @Override // X.AbstractC136536Mx
    public void AOX(AbstractC14640lm r32, AbstractC16390ow r33) {
        C20320vZ r5;
        C16380ov r4;
        C16470p4 r2;
        AnonymousClass1ZD r22;
        AnonymousClass1ZD A0b = C117305Zk.A0b(r33);
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A00;
        long currentTimeMillis = System.currentTimeMillis();
        AnonymousClass1Z6 r15 = null;
        AnonymousClass1ZD r14 = new AnonymousClass1ZD(null, A0b.A04, null, null, A0b.A07, null, null, null, "confirm", null, null, currentTimeMillis, true);
        C16170oZ r9 = brazilOrderDetailsActivity.A01;
        AbstractC15340mz r7 = (AbstractC15340mz) r33;
        String str = null;
        try {
            JSONObject A08 = AnonymousClass1QC.A08(r14, false);
            if (A08 != null) {
                str = A08.toString();
            }
        } catch (JSONException unused) {
            Log.e("UserActions/userActionSendOrderUpdateMessage failed to build parameter json for order status message");
        }
        AnonymousClass1Z7 r11 = new AnonymousClass1Z7(Collections.singletonList(new AnonymousClass1Z9(new AnonymousClass1Z8("payment_method", str), false)));
        AnonymousClass1Z6 r6 = new AnonymousClass1Z6(null, null, null);
        if ("review_order".equals("payment_method") || "payment_method".equals("payment_method")) {
            r5 = r9.A1G;
            r4 = new C16380ov(r5.A07.A02(r32, true), (byte) 55, r9.A0R.A00());
            if (!TextUtils.isEmpty(null) || !TextUtils.isEmpty(null) || r6.A02 != null) {
                r15 = r6;
            }
            r2 = new C16470p4(r15, r11, "", (String) null, "");
        } else {
            r5 = r9.A1G;
            r4 = new C16380ov(r5.A07.A02(r32, true), (byte) 54, r9.A0R.A00());
            if (TextUtils.isEmpty(null) && TextUtils.isEmpty(null) && r6.A02 == null) {
                r6 = null;
            }
            r2 = new C16470p4(r14, r6, r11, (String) null, (String) null);
        }
        r4.Abv(r2);
        if (r7 != null) {
            r5.A04(r4, r7);
        }
        r9.A0M(r4);
        r9.A0f.A0S(r4);
        C16470p4 ABf = r33.ABf();
        if (!(ABf == null || (r22 = ABf.A01) == null)) {
            r22.A01 = "confirm";
            r22.A00 = currentTimeMillis;
            ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A09.A0W(r7);
        }
        brazilOrderDetailsActivity.finish();
    }

    @Override // X.AbstractC136536Mx
    public void ASj(AbstractC14640lm r5, AbstractC16390ow r6, long j) {
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A00;
        brazilOrderDetailsActivity.A0F.A00(r6, "WhatsappPay", 8);
        C117305Zk.A0z(brazilOrderDetailsActivity, r5, j);
    }

    @Override // X.AbstractC136536Mx
    public void ATG(AbstractC14640lm r5, AbstractC16390ow r6, String str) {
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A00;
        brazilOrderDetailsActivity.A0F.A00(r6, "WhatsappPay", 7);
        C25881Be r2 = brazilOrderDetailsActivity.A0B;
        AnonymousClass1IS r1 = ((AbstractC15340mz) r6).A0z;
        C16470p4 ABf = r6.ABf();
        AnonymousClass009.A05(ABf);
        Intent A00 = r2.A00(brazilOrderDetailsActivity, ABf.A01, r1, str);
        if (A00 == null) {
            Log.e("Pay: BrazilOrderDetailsActivity/onOpenTransactionDetailClicked the transaction details intent is null");
        } else {
            brazilOrderDetailsActivity.startActivity(A00);
        }
    }

    @Override // X.AbstractC136536Mx
    public void AVr(C30821Yy r8, AbstractC14640lm r9, AbstractC16390ow r10, String str) {
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A00;
        brazilOrderDetailsActivity.A0F.A00(r10, "WhatsappPay", 5);
        brazilOrderDetailsActivity.A2C(R.string.register_wait_message);
        AbstractC14440lR r6 = ((ActivityC13830kP) brazilOrderDetailsActivity).A05;
        C15650ng r1 = ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A09;
        AnonymousClass1A7 r4 = brazilOrderDetailsActivity.A05;
        AnonymousClass1QC.A09(((ActivityC13810kN) brazilOrderDetailsActivity).A05, r1, brazilOrderDetailsActivity.A03, new AnonymousClass68D(r8, this, r10, str), r4, r10, r6);
    }
}
