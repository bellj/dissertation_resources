package X;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.util.Date;

/* renamed from: X.3Ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65003Ht {
    public static int A00(AnonymousClass1M2 r1) {
        if (r1 == null) {
            return 1;
        }
        if (r1.A01()) {
            return 3;
        }
        String str = r1.A07;
        if (str == null || !str.startsWith("smb:")) {
            return 1;
        }
        return 2;
    }

    public static SpannableString A01(Context context, AnonymousClass3M7 r3, C30711Yn r4, AnonymousClass018 r5, BigDecimal bigDecimal, Date date) {
        if (bigDecimal == null || r4 == null) {
            return new SpannableString(context.getString(R.string.ask_for_price));
        }
        String A03 = r4.A03(r5, bigDecimal, true);
        if (r3 == null || !r3.A00(date)) {
            return new SpannableString(A03);
        }
        return A02(A03, r4.A03(r5, r3.A01, true));
    }

    public static SpannableString A02(String str, String str2) {
        StringBuilder A0j = C12960it.A0j(str2);
        A0j.append("  ");
        SpannableString spannableString = new SpannableString(C12960it.A0d(str, A0j));
        spannableString.setSpan(new StrikethroughSpan(), str2.length() + 1, spannableString.length(), 33);
        return spannableString;
    }
}
