package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.R;
import com.whatsapp.account.delete.DeleteAccountFeedback;
import com.whatsapp.phonematching.ConnectionUnavailableDialogFragment;
import com.whatsapp.phonematching.MatchPhoneNumberFragment;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.2aD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52082aD extends Handler {
    public final AbstractC115375Rh A00;
    public final WeakReference A01;

    public HandlerC52082aD(ActivityC13790kL r2, AbstractC115375Rh r3) {
        super(Looper.getMainLooper());
        this.A01 = C12970iu.A10(r2);
        this.A00 = r3;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        ActivityC13790kL r4 = (ActivityC13790kL) this.A01.get();
        if (r4 == null) {
            Log.w(C12960it.A0b("MatchPhoneNumberFragment was garbage collected with active messages still enqueued: ", message));
        }
        int i = message.what;
        if (i == 1) {
            Log.i("MatchPhoneNumberFragment/check-number/match");
            removeMessages(4);
            if (r4 != null) {
                MatchPhoneNumberFragment.A00(r4);
                ActivityC13810kN r1 = (ActivityC13810kN) this.A00;
                r1.A2G(C12990iw.A0D(r1, DeleteAccountFeedback.class), true);
            }
        } else if (i != 2) {
            if (i == 3) {
                Log.e("MatchPhoneNumberFragment/error");
            } else if (i == 4) {
                Log.w("MatchPhoneNumberFragment/timeout");
                removeMessages(4);
            } else {
                return;
            }
            if (r4 != null) {
                MatchPhoneNumberFragment.A00(r4);
                Bundle A0D = C12970iu.A0D();
                ConnectionUnavailableDialogFragment connectionUnavailableDialogFragment = new ConnectionUnavailableDialogFragment();
                connectionUnavailableDialogFragment.A0U(A0D);
                connectionUnavailableDialogFragment.A1F(r4.A0V(), "CONNECTION ERROR");
            }
        } else {
            Log.w("MatchPhoneNumberFragment/check-number/mismatch");
            removeMessages(4);
            if (r4 != null) {
                MatchPhoneNumberFragment.A00(r4);
                ((ActivityC13810kN) this.A00).Ado(R.string.delete_account_mismatch);
            }
        }
    }
}
