package X;

import com.whatsapp.jid.UserJid;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1ql  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39921ql {
    public final ConcurrentHashMap A00 = new ConcurrentHashMap();

    public boolean A00(UserJid userJid, int i, long j) {
        long j2;
        long j3;
        long j4 = j;
        if (j > 0) {
            ConcurrentHashMap concurrentHashMap = this.A00;
            C39931qm r8 = (C39931qm) concurrentHashMap.get(userJid);
            if (r8 == null) {
                r8 = C39931qm.A03;
            }
            if (i != 5) {
                if (i == 8) {
                    long j5 = r8.A01;
                    if (j5 > 0 && j5 <= j) {
                        return false;
                    }
                    r8 = new C39931qm(r8.A00, r8.A02, j4);
                } else if (i == 13) {
                    long j6 = r8.A02;
                    if (j6 <= 0 || j6 > j) {
                        j3 = r8.A00;
                        j2 = r8.A01;
                    }
                }
                concurrentHashMap.put(userJid, r8);
                return true;
            }
            long j7 = r8.A00;
            if (j7 > 0 && j7 <= j) {
                return false;
            }
            j4 = r8.A02;
            j2 = r8.A01;
            j3 = j4;
            r8 = new C39931qm(j3, j4, j2);
            concurrentHashMap.put(userJid, r8);
            return true;
        }
        return false;
    }
}
