package X;

import com.whatsapp.contact.picker.invite.InviteNonWhatsAppContactPickerActivity;

/* renamed from: X.2fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53952fr extends AnonymousClass0Yo {
    public final /* synthetic */ InviteNonWhatsAppContactPickerActivity A00;

    public C53952fr(InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity) {
        this.A00 = inviteNonWhatsAppContactPickerActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C36941ku.class)) {
            InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity = this.A00;
            return new C36941ku(inviteNonWhatsAppContactPickerActivity.getApplication(), inviteNonWhatsAppContactPickerActivity.A0D, inviteNonWhatsAppContactPickerActivity.A0F, inviteNonWhatsAppContactPickerActivity.A0L, inviteNonWhatsAppContactPickerActivity.A0M);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
