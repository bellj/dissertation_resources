package X;

/* renamed from: X.3W5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W5 implements AbstractC115925Tl {
    public final /* synthetic */ C71573d9 A00;

    public AnonymousClass3W5(C71573d9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC115925Tl
    public C68433Vj A7w(AbstractC49242Jy r14, AbstractC49252Jz r15) {
        C71573d9 r1 = this.A00;
        AnonymousClass01J r3 = r1.A04;
        AbstractC14440lR A0T = C12960it.A0T(r3);
        C15890o4 A0Y = C12970iu.A0Y(r3);
        AnonymousClass1CO r6 = (AnonymousClass1CO) r3.AHw.get();
        AnonymousClass2FL r2 = r1.A01;
        return new C68433Vj(C12990iw.A0W(r3), new AnonymousClass3ER((AbstractC48942In) r2.A0W.get(), (AnonymousClass1B5) r2.A1E.A5t.get()), r6, (AnonymousClass1B0) r3.A2K.get(), (AnonymousClass1B3) r3.AI0.get(), r14, r15, A0Y, A0T);
    }
}
