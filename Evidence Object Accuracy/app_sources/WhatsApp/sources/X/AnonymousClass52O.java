package X;

/* renamed from: X.52O  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52O implements AbstractC22060yS {
    public final AnonymousClass11P A00;
    public final AnonymousClass1A1 A01;

    @Override // X.AbstractC22060yS
    public void AMG() {
    }

    public AnonymousClass52O(AnonymousClass11P r1, AnonymousClass1A1 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        AnonymousClass11P r1 = this.A00;
        if (r1.A0B()) {
            r1.A04();
        }
    }
}
