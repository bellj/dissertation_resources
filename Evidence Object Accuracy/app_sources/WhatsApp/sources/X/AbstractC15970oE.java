package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0oE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15970oE extends AbstractC15590nW implements Parcelable {
    public AbstractC15970oE(Parcel parcel) {
        super(parcel);
    }

    public AbstractC15970oE(String str) {
        super(str);
    }
}
