package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.0ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22140ya {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C22700zV A02;
    public final AnonymousClass15O A03;

    public C22140ya(AbstractC15710nm r1, C15570nT r2, C22700zV r3, AnonymousClass15O r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static AnonymousClass1XB A00(AbstractC15710nm r7, AnonymousClass1IS r8, AnonymousClass1OT r9, int i, long j) {
        if (i == 6) {
            return new C30501Xq(r8, j);
        }
        if (i == 28 || i == 10) {
            return new C30511Xs(r7, r8, i, j);
        }
        if (i == 37) {
            return new C30561Xx(r8, j);
        }
        if (i == 39) {
            return new C30551Xw(r8, j);
        }
        if (i == 40 || i == 41 || i == 42 || i == 43 || i == 44 || i == 45) {
            return new C30541Xv(r8, i, j);
        }
        if (i == 64) {
            return new C32291bv(r8, j);
        }
        if (i == 65) {
            return new C32281bu(r8, j);
        }
        if (i == 66) {
            return new C32191bl(r8, j);
        }
        if (C32201bm.A00(i)) {
            if (i == 1) {
                return new C30451Xl(r8, j);
            }
            if (i == 56) {
                return new C32211bn(r8, j);
            }
            if (i == 3) {
                return new C32221bo(r8, j);
            }
            int i2 = 84;
            if (i != 84) {
                i2 = 85;
                if (i != 85) {
                    if (i == 83) {
                        return new C32231bp(r8, j);
                    }
                    if (i != 90) {
                        return new C30461Xm(r8, (AnonymousClass1OT) null, i, j);
                    }
                }
            }
            return new C30481Xo(r8, i2, j);
        } else if (C32201bm.A01(i)) {
            return new C30491Xp(r8, i, j);
        } else {
            if (i == 57) {
                return new C30521Xt(r8, j);
            }
            if (i == 71) {
                return new C32241bq(r8, j);
            }
            if (i == 58) {
                return new C30581Xz(r8, j);
            }
            if (i == 59) {
                return new C32251br(r8, j);
            }
            if (i == 60) {
                return new AnonymousClass1Y0(r8, j);
            }
            if (i == 19 || i == 67) {
                return new C30531Xu(r8, i, j);
            }
            if (i == 68) {
                return new AnonymousClass1XA(r8, j);
            }
            if (i == 76) {
                return new C32261bs(r8, j);
            }
            if (i == 61 || i == 69) {
                return new AnonymousClass1Y1(r8, i, j);
            }
            if (i == 70) {
                return new AnonymousClass1Y2(r8, j);
            }
            if (i == 75) {
                return new AnonymousClass1Y3(r8, r9, 75, j);
            }
            if (i == 95) {
                AnonymousClass1Y3 r72 = new AnonymousClass1Y3(r8, r9, 95, j);
                r72.A00 = 2;
                return r72;
            } else if (i != 87) {
                if (i == 77 || i == 78 || i == 88 || i == 89) {
                    return new AnonymousClass1Y5(r8, r9, i, j);
                }
                if (i == 80) {
                    return new C32271bt(r8, j);
                }
                return new AnonymousClass1XB(r8, i, j);
            }
        }
        return new AnonymousClass1Y4(r8, i, j);
    }

    public AnonymousClass1XB A01(AbstractC14640lm r7, UserJid userJid, int i, int i2, long j) {
        C30521Xt r0 = (C30521Xt) A00(this.A00, this.A03.A02(r7, true), null, 57, j);
        r0.A0e(userJid);
        r0.A00 = i;
        r0.A01 = i2;
        return r0;
    }

    public AnonymousClass1XB A02(AbstractC14640lm r7, UserJid userJid, long j) {
        AnonymousClass1XB A00 = A00(this.A00, this.A03.A02(r7, true), null, 71, j);
        A00.A0e(userJid);
        return A00;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [X.1XB] */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r10 == 3) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1XB A03(com.whatsapp.jid.GroupJid r7, java.lang.String r8, java.util.List r9, int r10, int r11, long r12) {
        /*
            r6 = this;
            r3 = 2
            if (r10 == r3) goto L_0x0007
            r0 = 3
            r2 = 0
            if (r10 != r0) goto L_0x0008
        L_0x0007:
            r2 = 1
        L_0x0008:
            java.lang.String r1 = "This method not suppose to be used for action = "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            X.AnonymousClass009.A0A(r0, r2)
            X.15O r2 = r6.A03
            r0 = 1
            X.1IS r1 = r2.A02(r7, r0)
            r4 = r12
            if (r10 != r3) goto L_0x0037
            X.1IS r1 = r2.A02(r7, r0)
            X.0nm r0 = r6.A00
            r2 = 0
            X.1XB r0 = A00(r0, r1, r2, r3, r4)
        L_0x002e:
            r0.A0l(r8)
            if (r9 == 0) goto L_0x0036
            r0.A0u(r9)
        L_0x0036:
            return r0
        L_0x0037:
            X.1bo r0 = new X.1bo
            r0.<init>(r1, r12)
            r0.A00 = r11
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22140ya.A03(com.whatsapp.jid.GroupJid, java.lang.String, java.util.List, int, int, long):X.1XB");
    }

    public AnonymousClass1XB A04(C15580nU r8, UserJid userJid, AnonymousClass1OT r10, int i, long j) {
        C32211bn r0 = (C32211bn) A07(null, r8, r10, 56, j);
        r0.A0e(userJid);
        r0.A00 = i;
        return r0;
    }

    public AnonymousClass1Y3 A05(GroupJid groupJid, C15580nU r8, UserJid userJid, AnonymousClass1OT r10, Integer num, int i, long j) {
        AnonymousClass1Y3 r1 = (AnonymousClass1Y3) A00(this.A00, this.A03.A02(r8, true), r10, 75, j);
        r1.A02 = num;
        r1.A00 = i;
        r1.A01 = groupJid;
        if (this.A01.A0F(userJid)) {
            ((C30461Xm) r1).A00 = 1;
        }
        if (num != null) {
            r1.A0e(userJid);
        }
        return r1;
    }

    public C30461Xm A06(AnonymousClass1YM r13, AbstractC15590nW r14, UserJid userJid, AnonymousClass1OT r16, List list, int i, long j, long j2) {
        StringBuilder sb = new StringBuilder("SystemMessageFactory/newParticipantsStatusMessage; stanzaKey=");
        sb.append(r16);
        sb.append("; gjid=");
        sb.append(r14);
        sb.append("; action=");
        sb.append(i);
        sb.append("; author=");
        sb.append(userJid);
        Log.i(sb.toString());
        C30461Xm A07 = A07(r13, r14, r16, i, j);
        A07.A0e(userJid);
        A07.A0u(list);
        A07.A03 = j2;
        if (C30041Vv.A0J(i)) {
            C15570nT r0 = this.A01;
            r0.A08();
            if (list.contains(r0.A05)) {
                A07.A00 = 1;
            }
        }
        return A07;
    }

    public C30461Xm A07(AnonymousClass1YM r9, AbstractC15590nW r10, AnonymousClass1OT r11, int i, long j) {
        C30461Xm r2;
        boolean A00 = C32201bm.A00(i);
        StringBuilder sb = new StringBuilder("Not supposed to be used for action = ");
        sb.append(i);
        AnonymousClass009.A0A(sb.toString(), A00);
        boolean z = true;
        if (r11 != null) {
            if (i == 1) {
                r2 = new C30451Xl(r9, r11, j);
            } else if (i == 56) {
                r2 = new C32211bn(r9, r11, j);
            } else {
                r2 = new C30461Xm(r9, r11, i, j);
            }
            r2.A14 = r11.A00;
            return r2;
        }
        if (r10 == null) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        return (C30461Xm) A00(this.A00, this.A03.A02(r10, true), null, i, j);
    }

    public C30461Xm A08(AbstractC15590nW r12, UserJid userJid, AnonymousClass1OT r14, int i, long j, long j2) {
        StringBuilder sb = new StringBuilder("SystemMessageFactory/newParticipantStatusMessage; stanzaKey=");
        sb.append(r14);
        sb.append("; gjid=");
        sb.append(r12);
        sb.append("; action=");
        sb.append(i);
        Log.i(sb.toString());
        C30461Xm A07 = A07(null, r12, r14, i, j);
        A07.A0e(userJid);
        A07.A03 = j2;
        if (i == 4 && this.A01.A0F(userJid)) {
            A07.A00 = 1;
        }
        return A07;
    }

    public AnonymousClass1Y4 A09(C15580nU r7, UserJid userJid, String str, long j) {
        AnonymousClass1Y4 r0 = (AnonymousClass1Y4) A00(this.A00, this.A03.A02(r7, true), null, 87, j);
        r0.A00 = str;
        r0.A0e(userJid);
        return r0;
    }

    public C30541Xv A0A(AbstractC14640lm r7, int i, long j) {
        return (C30541Xv) A00(this.A00, this.A03.A02(r7, true), null, i, j);
    }
}
