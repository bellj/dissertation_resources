package X;

import androidx.work.impl.WorkDatabase;

/* renamed from: X.0Zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07590Zj implements AbstractC11400gE {
    public final WorkDatabase A00;
    public final AbstractC11500gO A01;

    static {
        C06390Tk.A01("WorkProgressUpdater");
    }

    public C07590Zj(WorkDatabase workDatabase, AbstractC11500gO r2) {
        this.A00 = workDatabase;
        this.A01 = r2;
    }
}
