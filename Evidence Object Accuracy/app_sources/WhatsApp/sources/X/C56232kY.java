package X;

import java.util.HashMap;

/* renamed from: X.2kY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56232kY extends AbstractC64703Go {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public boolean A04;
    public boolean A05;

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        A11.put("hitType", this.A00);
        A11.put("clientId", this.A01);
        A11.put("userId", this.A02);
        A11.put("androidAdId", this.A03);
        A11.put("AdTargetingEnabled", Boolean.valueOf(this.A04));
        A11.put("sessionControl", null);
        A11.put("nonInteraction", Boolean.valueOf(this.A05));
        return AbstractC64703Go.A01("sampleRate", Double.valueOf(0.0d), A11);
    }
}
