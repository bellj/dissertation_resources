package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120775gj extends C120895gv {
    public final /* synthetic */ C120515gJ A00;
    public final /* synthetic */ C128635wQ A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120775gj(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120515gJ r11, C128635wQ r12) {
        super(context, r8, r9, r10, "upi-check-balance");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        this.A01.A00(null, null, r3);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        this.A01.A00(null, null, r3);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        super.A04(r6);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r6);
        if (A0c == null) {
            Log.e("PAY: IndiaUpiPaymentMethodAction sendCheckPin: empty account node");
        } else {
            C119715ez r3 = new C119715ez();
            r3.A01(this.A00.A04, A0c, 6);
            if (r3.A0E() == null && r3.A0K() && r3.A06() != null) {
                String A06 = r3.A06();
                int i = ((AbstractC30781Yu) C30771Yt.A05).A01;
                this.A01.A00(C30821Yy.A00(A06, i), C30821Yy.A00(r3.A0F(), i), null);
                return;
            }
        }
        this.A01.A00(null, null, C117305Zk.A0L());
    }
}
