package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;
import com.whatsapp.R;

/* renamed from: X.2aX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52272aX extends ReplacementSpan {
    public final int A00;
    public final Context A01;

    public C52272aX(Context context, int i) {
        this.A01 = context;
        this.A00 = i;
    }

    @Override // android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        paint.setColor(this.A00);
        canvas.drawText(charSequence, i, i2, (float) ((int) ((f + ((float) (getSize(paint, charSequence, i, i2, null) >> 1))) - (paint.measureText(charSequence, i, i2) / 2.0f))), (float) i4, paint);
    }

    @Override // android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        return Math.round(paint.measureText(charSequence, charSequence.length(), charSequence.length()) + this.A01.getResources().getDimension(R.dimen.code_input_field_padded_foreground_span_size));
    }
}
