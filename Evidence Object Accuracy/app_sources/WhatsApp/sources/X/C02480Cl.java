package X;

/* renamed from: X.0Cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02480Cl extends AbstractC05550Pz {
    @Override // X.AbstractC05550Pz
    public void A00(C06320Td r1, C06320Td r2) {
        r1.next = r2;
    }

    @Override // X.AbstractC05550Pz
    public void A01(C06320Td r1, Thread thread) {
        r1.thread = thread;
    }

    @Override // X.AbstractC05550Pz
    public boolean A02(C05910Rl r2, C05910Rl r3, AbstractC08750bn r4) {
        boolean z;
        synchronized (r4) {
            if (r4.listeners == r2) {
                r4.listeners = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    @Override // X.AbstractC05550Pz
    public boolean A03(C06320Td r2, C06320Td r3, AbstractC08750bn r4) {
        boolean z;
        synchronized (r4) {
            if (r4.waiters == r2) {
                r4.waiters = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    @Override // X.AbstractC05550Pz
    public boolean A04(AbstractC08750bn r3, Object obj, Object obj2) {
        boolean z;
        synchronized (r3) {
            if (r3.value == null) {
                r3.value = obj2;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }
}
