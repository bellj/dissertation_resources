package X;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.3dM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71693dM<F, T> extends AbstractCollection<T> {
    public final Collection fromCollection;
    public final AbstractC469028d function;

    public C71693dM(Collection collection, AbstractC469028d r2) {
        this.fromCollection = collection;
        this.function = r2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        this.fromCollection.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return this.fromCollection.isEmpty();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return AnonymousClass1I4.transform(this.fromCollection.iterator(), this.function);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        return this.fromCollection.size();
    }
}
