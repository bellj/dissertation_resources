package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5pT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124415pT extends AbstractC119405dv {
    public ViewGroup A00;
    public ImageView A01;
    public TextView A02;
    public String A03;
    public final C129125xD A04;

    public C124415pT(AnonymousClass018 r1, WaBloksActivity waBloksActivity, C129125xD r3) {
        super(r1, waBloksActivity);
        this.A04 = r3;
    }

    @Override // X.AbstractC119405dv
    public void A03(Intent intent, Bundle bundle) {
        A00().A0I("");
        ViewGroup viewGroup = (ViewGroup) C12960it.A0F(LayoutInflater.from(A00().A02()), (ViewGroup) super.A04.findViewById(R.id.bk_navigation_custom_view), R.layout.bk_navigation_bar);
        this.A00 = viewGroup;
        this.A01 = C12970iu.A0L(viewGroup, R.id.bk_navigation_logo);
        TextView A0J = C12960it.A0J(this.A00, R.id.bk_navigation_title);
        this.A02 = A0J;
        AnonymousClass009.A03(this.A00);
        AnonymousClass009.A03(this.A01);
        AnonymousClass009.A03(A0J);
        AnonymousClass23N.A04(A0J, true);
        if (bundle != null) {
            this.A03 = bundle.getString("bk_navigation_bar_logo");
            A05(super.A01);
        }
        A00().A0N(true);
        A00().A0F(this.A00);
    }

    @Override // X.AbstractC119405dv
    public void A04(AbstractC115815Ta r5) {
        try {
            AnonymousClass28D r3 = new C126195sU(r5.AAL()).A00;
            String A0I = r3.A0I(36);
            super.A01 = A0I;
            this.A03 = r3.A0J(45, "");
            A05(A0I);
        } catch (ClassCastException e) {
            Log.e(C12960it.A0b("Bloks: Invalid navigation bar type", e));
        }
    }

    public final void A05(String str) {
        String str2;
        TextView textView = this.A02;
        if (textView != null) {
            textView.setText(str);
            this.A02.setVisibility(0);
        }
        ImageView imageView = this.A01;
        if (imageView != null && (str2 = this.A03) != null) {
            this.A04.A00(imageView, str2, str2);
            this.A01.setVisibility(0);
        }
    }

    @Override // X.AbstractC119405dv, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        bundle.putString("bk_navigation_bar_logo", this.A03);
        super.onActivitySaveInstanceState(activity, bundle);
    }
}
