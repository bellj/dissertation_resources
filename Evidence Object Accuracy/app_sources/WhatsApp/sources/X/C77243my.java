package X;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/* renamed from: X.3my  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77243my extends AbstractC76783mE {
    public static final AnonymousClass4IK A01 = new AnonymousClass4IK(15);
    public static final AnonymousClass4PB A02 = new AnonymousClass4PB(1, 1, 30.0f);
    public static final Pattern A03 = Pattern.compile("^(\\d+) (\\d+)$");
    public static final Pattern A04 = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
    public static final Pattern A05 = Pattern.compile("^(([0-9]*.)?[0-9]+)(px|em|%)$");
    public static final Pattern A06 = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
    public static final Pattern A07 = Pattern.compile("^(\\d+\\.?\\d*?)% (\\d+\\.?\\d*?)%$");
    public static final Pattern A08 = Pattern.compile("^(\\d+\\.?\\d*?)px (\\d+\\.?\\d*?)px$");
    public static final Pattern A09 = Pattern.compile("^([-+]?\\d+\\.?\\d*?)%$");
    public final XmlPullParserFactory A00;

    public C77243my() {
        super("TtmlDecoder");
        try {
            XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
            this.A00 = newInstance;
            newInstance.setNamespaceAware(true);
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    public static long A01(AnonymousClass4PB r12, String str) {
        double d;
        double d2;
        double d3;
        double d4;
        Matcher matcher = A04.matcher(str);
        if (matcher.matches()) {
            double A00 = ((double) (AbstractC76783mE.A00(matcher, 1) * 3600)) + ((double) (AbstractC76783mE.A00(matcher, 2) * 60)) + ((double) AbstractC76783mE.A00(matcher, 3));
            String group = matcher.group(4);
            double d5 = 0.0d;
            if (group != null) {
                d3 = Double.parseDouble(group);
            } else {
                d3 = 0.0d;
            }
            double d6 = A00 + d3;
            String group2 = matcher.group(5);
            if (group2 != null) {
                d4 = (double) (((float) Long.parseLong(group2)) / r12.A00);
            } else {
                d4 = 0.0d;
            }
            double d7 = d6 + d4;
            String group3 = matcher.group(6);
            if (group3 != null) {
                d5 = (((double) Long.parseLong(group3)) / ((double) r12.A01)) / ((double) r12.A00);
            }
            return (long) ((d7 + d5) * 1000000.0d);
        }
        Matcher matcher2 = A06.matcher(str);
        if (matcher2.matches()) {
            double parseDouble = Double.parseDouble(matcher2.group(1));
            String group4 = matcher2.group(2);
            switch (group4.hashCode()) {
                case 102:
                    if (group4.equals("f")) {
                        d = (double) r12.A00;
                        parseDouble /= d;
                        break;
                    }
                    break;
                case 104:
                    if (group4.equals("h")) {
                        d2 = 3600.0d;
                        parseDouble *= d2;
                        break;
                    }
                    break;
                case 109:
                    if (group4.equals("m")) {
                        d2 = 60.0d;
                        parseDouble *= d2;
                        break;
                    }
                    break;
                case 116:
                    if (group4.equals("t")) {
                        d = (double) r12.A02;
                        parseDouble /= d;
                        break;
                    }
                    break;
                case 3494:
                    if (group4.equals("ms")) {
                        d = 1000.0d;
                        parseDouble /= d;
                        break;
                    }
                    break;
            }
            return (long) (parseDouble * 1000000.0d);
        }
        throw new C76723m6(C12960it.A0d(str, C12960it.A0k("Malformed time expression: ")));
    }

    public static AnonymousClass4WC A02(AnonymousClass4WC r0) {
        return r0 == null ? new AnonymousClass4WC() : r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0414, code lost:
        if (r0 != false) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b5, code lost:
        if (r0 == false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0122, code lost:
        if (r0 == false) goto L_0x0124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0146, code lost:
        if (r7.equals("dot") != false) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0155, code lost:
        if (r7.equals("sesame") == false) goto L_0x0157;
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x022c  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x02a9  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x001c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x001c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x001c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x001c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0202  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass4WC A03(X.AnonymousClass4WC r11, org.xmlpull.v1.XmlPullParser r12) {
        /*
        // Method dump skipped, instructions count: 1220
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77243my.A03(X.4WC, org.xmlpull.v1.XmlPullParser):X.4WC");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02ec, code lost:
        if (r1 != r8) goto L_0x02ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0478, code lost:
        if (r1 == false) goto L_0x047a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01ef, code lost:
        if (r6.equals("information") != false) goto L_0x01f1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0308 A[Catch: 3m6 -> 0x0317, XmlPullParserException -> 0x0598, IOException -> 0x058f, TryCatch #1 {3m6 -> 0x0317, blocks: (B:87:0x0214, B:89:0x0233, B:90:0x023f, B:92:0x0243, B:94:0x024b, B:96:0x0253, B:97:0x0259, B:99:0x0261, B:101:0x026b, B:102:0x026e, B:105:0x0272, B:107:0x027d, B:109:0x0285, B:110:0x028a, B:112:0x0292, B:113:0x0297, B:115:0x029f, B:116:0x02a4, B:118:0x02ac, B:121:0x02b4, B:123:0x02ba, B:127:0x02c9, B:130:0x02cf, B:136:0x02df, B:140:0x02e8, B:142:0x02ee, B:144:0x0308, B:146:0x030c, B:147:0x0312), top: B:263:0x0214 }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0456 A[Catch: XmlPullParserException -> 0x0598, IOException -> 0x058f, TryCatch #6 {IOException -> 0x058f, XmlPullParserException -> 0x0598, blocks: (B:3:0x0002, B:6:0x005e, B:8:0x0067, B:11:0x006f, B:13:0x0075, B:15:0x007f, B:16:0x0083, B:18:0x008d, B:20:0x0097, B:21:0x00a8, B:23:0x00b1, B:24:0x00b5, B:26:0x00be, B:27:0x00c2, B:29:0x00d1, B:31:0x00e1, B:34:0x00f1, B:37:0x0105, B:38:0x010b, B:39:0x0122, B:40:0x0123, B:42:0x0130, B:44:0x0139, B:46:0x0147, B:48:0x0156, B:49:0x016c, B:51:0x017a, B:53:0x0180, B:55:0x0188, B:57:0x0190, B:59:0x0198, B:61:0x01a0, B:63:0x01a8, B:65:0x01b0, B:67:0x01b8, B:69:0x01c0, B:71:0x01c8, B:73:0x01d0, B:75:0x01d8, B:77:0x01e0, B:79:0x01e8, B:84:0x01f6, B:85:0x020c, B:87:0x0214, B:89:0x0233, B:90:0x023f, B:92:0x0243, B:94:0x024b, B:96:0x0253, B:97:0x0259, B:99:0x0261, B:101:0x026b, B:102:0x026e, B:105:0x0272, B:107:0x027d, B:109:0x0285, B:110:0x028a, B:112:0x0292, B:113:0x0297, B:115:0x029f, B:116:0x02a4, B:118:0x02ac, B:121:0x02b4, B:123:0x02ba, B:127:0x02c9, B:130:0x02cf, B:136:0x02df, B:140:0x02e8, B:142:0x02ee, B:144:0x0308, B:146:0x030c, B:147:0x0312, B:149:0x0318, B:151:0x0321, B:153:0x032c, B:155:0x033b, B:157:0x0345, B:158:0x0348, B:160:0x034c, B:161:0x035c, B:162:0x0364, B:164:0x036c, B:166:0x0374, B:168:0x037c, B:170:0x0395, B:171:0x03aa, B:174:0x03b2, B:175:0x03cc, B:177:0x03d4, B:179:0x03e4, B:180:0x03f9, B:183:0x0401, B:184:0x041b, B:186:0x0423, B:188:0x0431, B:190:0x0439, B:191:0x043d, B:193:0x0447, B:195:0x0456, B:196:0x0460, B:198:0x0464, B:201:0x0472, B:204:0x047c, B:205:0x0495, B:206:0x049c, B:207:0x04a5, B:208:0x04aa, B:209:0x04b0, B:210:0x04b3, B:213:0x04bd, B:214:0x04c1, B:216:0x04c9, B:218:0x04d4, B:220:0x04dc, B:221:0x04e5, B:224:0x04ec, B:226:0x04f0, B:227:0x04f5, B:235:0x0503, B:237:0x050d, B:238:0x051c, B:240:0x0522, B:243:0x0528, B:244:0x052b, B:246:0x056a, B:247:0x0570, B:248:0x0573, B:249:0x057c, B:250:0x0583, B:253:0x0587, B:254:0x058e), top: B:269:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x0573 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0139 A[Catch: XmlPullParserException -> 0x0598, IOException -> 0x058f, TryCatch #6 {IOException -> 0x058f, XmlPullParserException -> 0x0598, blocks: (B:3:0x0002, B:6:0x005e, B:8:0x0067, B:11:0x006f, B:13:0x0075, B:15:0x007f, B:16:0x0083, B:18:0x008d, B:20:0x0097, B:21:0x00a8, B:23:0x00b1, B:24:0x00b5, B:26:0x00be, B:27:0x00c2, B:29:0x00d1, B:31:0x00e1, B:34:0x00f1, B:37:0x0105, B:38:0x010b, B:39:0x0122, B:40:0x0123, B:42:0x0130, B:44:0x0139, B:46:0x0147, B:48:0x0156, B:49:0x016c, B:51:0x017a, B:53:0x0180, B:55:0x0188, B:57:0x0190, B:59:0x0198, B:61:0x01a0, B:63:0x01a8, B:65:0x01b0, B:67:0x01b8, B:69:0x01c0, B:71:0x01c8, B:73:0x01d0, B:75:0x01d8, B:77:0x01e0, B:79:0x01e8, B:84:0x01f6, B:85:0x020c, B:87:0x0214, B:89:0x0233, B:90:0x023f, B:92:0x0243, B:94:0x024b, B:96:0x0253, B:97:0x0259, B:99:0x0261, B:101:0x026b, B:102:0x026e, B:105:0x0272, B:107:0x027d, B:109:0x0285, B:110:0x028a, B:112:0x0292, B:113:0x0297, B:115:0x029f, B:116:0x02a4, B:118:0x02ac, B:121:0x02b4, B:123:0x02ba, B:127:0x02c9, B:130:0x02cf, B:136:0x02df, B:140:0x02e8, B:142:0x02ee, B:144:0x0308, B:146:0x030c, B:147:0x0312, B:149:0x0318, B:151:0x0321, B:153:0x032c, B:155:0x033b, B:157:0x0345, B:158:0x0348, B:160:0x034c, B:161:0x035c, B:162:0x0364, B:164:0x036c, B:166:0x0374, B:168:0x037c, B:170:0x0395, B:171:0x03aa, B:174:0x03b2, B:175:0x03cc, B:177:0x03d4, B:179:0x03e4, B:180:0x03f9, B:183:0x0401, B:184:0x041b, B:186:0x0423, B:188:0x0431, B:190:0x0439, B:191:0x043d, B:193:0x0447, B:195:0x0456, B:196:0x0460, B:198:0x0464, B:201:0x0472, B:204:0x047c, B:205:0x0495, B:206:0x049c, B:207:0x04a5, B:208:0x04aa, B:209:0x04b0, B:210:0x04b3, B:213:0x04bd, B:214:0x04c1, B:216:0x04c9, B:218:0x04d4, B:220:0x04dc, B:221:0x04e5, B:224:0x04ec, B:226:0x04f0, B:227:0x04f5, B:235:0x0503, B:237:0x050d, B:238:0x051c, B:240:0x0522, B:243:0x0528, B:244:0x052b, B:246:0x056a, B:247:0x0570, B:248:0x0573, B:249:0x057c, B:250:0x0583, B:253:0x0587, B:254:0x058e), top: B:269:0x0002 }] */
    @Override // X.AbstractC76783mE
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC116805Wy A07(byte[] r39, int r40, boolean r41) {
        /*
        // Method dump skipped, instructions count: 1482
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77243my.A07(byte[], int, boolean):X.5Wy");
    }
}
