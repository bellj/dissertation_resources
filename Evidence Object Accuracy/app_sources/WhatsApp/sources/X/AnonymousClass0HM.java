package X;

/* renamed from: X.0HM  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0HM extends C04590Mh {
    public final int A00;
    public final C64893Hi A01;

    public AnonymousClass0HM(C64893Hi r2, int i) {
        super("BloksSurface_process_attach_to_view");
        this.A01 = r2;
        this.A00 = i;
    }

    public final C64893Hi A00() {
        return this.A01;
    }
}
