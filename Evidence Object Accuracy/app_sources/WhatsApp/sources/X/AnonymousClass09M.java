package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.09M  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09M extends AnimatorListenerAdapter {
    public int A00 = 0;
    public boolean A01 = false;
    public final /* synthetic */ AnonymousClass092 A02;

    public AnonymousClass09M(AnonymousClass092 r2) {
        this.A02 = r2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A01 = true;
        AnonymousClass092 r2 = this.A02;
        ArrayList<Animator.AnimatorListener> listeners = r2.getListeners();
        if (listeners != null) {
            Iterator<Animator.AnimatorListener> it = listeners.iterator();
            while (it.hasNext()) {
                it.next().onAnimationCancel(r2);
            }
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        int i;
        int i2 = this.A00 + 1;
        this.A00 = i2;
        if (this.A01 || ((i = this.A02.A00) != -1 && i2 >= i)) {
            AnonymousClass092 r2 = this.A02;
            ArrayList<Animator.AnimatorListener> listeners = r2.getListeners();
            if (listeners != null) {
                Iterator<Animator.AnimatorListener> it = listeners.iterator();
                while (it.hasNext()) {
                    it.next().onAnimationEnd(r2);
                }
                return;
            }
            return;
        }
        animator.start();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass092 r2 = this.A02;
        ArrayList<Animator.AnimatorListener> listeners = r2.getListeners();
        if (this.A00 == 0 && listeners != null) {
            Iterator<Animator.AnimatorListener> it = listeners.iterator();
            while (it.hasNext()) {
                it.next().onAnimationStart(r2);
            }
        }
    }
}
