package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* renamed from: X.2oK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58212oK extends AbstractC52172aN {
    public final /* synthetic */ ContactPickerFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58212oK(Context context, ContactPickerFragment contactPickerFragment) {
        super(context);
        this.A00 = contactPickerFragment;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ContactPickerFragment contactPickerFragment = this.A00;
        contactPickerFragment.A0I.A06(contactPickerFragment.A0p(), C12970iu.A0B(contactPickerFragment.A1c.A03("26000253")));
    }
}
