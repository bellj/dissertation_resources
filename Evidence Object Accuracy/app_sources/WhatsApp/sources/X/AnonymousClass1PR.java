package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1PR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PR {
    public final long A00;
    public final AbstractC15590nW A01;
    public final UserJid A02;
    public final boolean A03;

    public AnonymousClass1PR(AbstractC15590nW r1, UserJid userJid, long j, boolean z) {
        this.A01 = r1;
        this.A02 = userJid;
        this.A03 = z;
        this.A00 = j;
    }
}
