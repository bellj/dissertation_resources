package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0E8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E8 extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VN();
    public int A00;
    public Parcelable A01;
    public ClassLoader A02;

    public AnonymousClass0E8(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
        this.A00 = parcel.readInt();
        this.A01 = parcel.readParcelable(classLoader);
        this.A02 = classLoader;
    }

    public AnonymousClass0E8(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentPager.SavedState{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" position=");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
        parcel.writeParcelable(this.A01, i);
    }
}
