package X;

import com.whatsapp.conversationslist.ConversationsFragment;

/* renamed from: X.41i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C851441i extends AnonymousClass2Dn {
    public final /* synthetic */ ConversationsFragment A00;

    public C851441i(ConversationsFragment conversationsFragment) {
        this.A00 = conversationsFragment;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        ConversationsFragment.A03(this.A00);
    }

    @Override // X.AnonymousClass2Dn
    public void A02(AbstractC14640lm r2) {
        ConversationsFragment.A04(this.A00, r2);
    }
}
