package X;

import android.app.Application;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67383Rh implements AbstractC009404s {
    public final Application A00;
    public final AnonymousClass1FB A01;
    public final AnonymousClass3EX A02;
    public final C18640sm A03;
    public final UserJid A04;
    public final AnonymousClass1ZC A05;

    public C67383Rh(Application application, AnonymousClass1FB r2, AnonymousClass3EX r3, C18640sm r4, UserJid userJid, AnonymousClass1ZC r6) {
        this.A00 = application;
        this.A04 = userJid;
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new C53832fK(this.A00, this.A01, this.A02, this.A03, this.A04, this.A05);
    }
}
