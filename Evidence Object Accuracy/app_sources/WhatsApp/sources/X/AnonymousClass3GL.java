package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.util.SparseArray;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.3GL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GL {
    public static SparseArray A00() {
        SparseArray sparseArray = new SparseArray();
        sparseArray.put(C43951xu.A03, new AnonymousClass4TG(9.0f, 4.0f, 2.0f, R.id.search_media_filter_link, R.string.search_links, R.drawable.ic_link));
        sparseArray.put(105, new AnonymousClass4TG(9.0f, 4.0f, 0.0f, R.id.search_media_filter_image, R.string.search_images, R.drawable.msg_status_image));
        sparseArray.put(97, new AnonymousClass4TG(8.0f, 4.0f, 0.0f, R.id.search_media_filter_audio, R.string.search_audio, R.drawable.msg_status_audio));
        sparseArray.put(103, new AnonymousClass4TG(10.0f, 6.0f, 0.2f, R.id.search_media_filter_gif, R.string.search_gifs, R.drawable.msg_status_gif));
        sparseArray.put(118, new AnonymousClass4TG(9.0f, 5.0f, 0.0f, R.id.search_media_filter_video, R.string.search_videos, R.drawable.msg_status_video));
        sparseArray.put(100, new AnonymousClass4TG(7.0f, 3.0f, 0.0f, R.id.search_media_filter_doc, R.string.search_docs, R.drawable.msg_status_doc));
        sparseArray.put(117, new AnonymousClass4TG(9.0f, 5.0f, 0.0f, R.id.search_unread_filter, R.string.filter_unread, R.drawable.smart_filter_unread));
        return sparseArray;
    }

    public static void A01(Context context, Chip chip, int i, int i2) {
        InsetDrawable insetDrawable;
        AnonymousClass4TG r2 = (AnonymousClass4TG) A00().get(i);
        Drawable A04 = AnonymousClass00X.A04(null, context.getResources(), r2.A03);
        int A01 = AnonymousClass3G9.A01(context, r2.A01);
        if (A04 == null) {
            insetDrawable = null;
        } else {
            int max = Math.max(A04.getIntrinsicWidth(), A04.getIntrinsicHeight());
            int intrinsicWidth = ((max - A04.getIntrinsicWidth()) >> 1) + A01;
            int intrinsicHeight = ((max - A04.getIntrinsicHeight()) >> 1) + A01;
            insetDrawable = new InsetDrawable(A04, intrinsicWidth, intrinsicHeight, intrinsicWidth, intrinsicHeight);
        }
        AnonymousClass009.A05(insetDrawable);
        chip.setChipIcon(AnonymousClass2GE.A03(context, insetDrawable, i2));
        chip.setChipIconSize((float) AnonymousClass3G9.A01(context, 20.0f));
        chip.setChipStartPadding((float) AnonymousClass3G9.A01(context, 1.0f));
        chip.setTextStartPadding((float) AnonymousClass3G9.A01(context, 1.0f));
        chip.setIconStartPadding((float) AnonymousClass3G9.A01(context, r2.A02));
        chip.setIconEndPadding((float) AnonymousClass3G9.A01(context, r2.A00));
    }
}
