package X;

import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1aB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31211aB {
    public final C15450nH A00;
    public final C29251Rl A01;
    public final C16040oL A02;
    public final C31161a6 A03;
    public final AnonymousClass1RM A04;
    public final C29291Rp A05;
    public final AnonymousClass1RX A06;
    public final AnonymousClass1RP A07;
    public final C14830m7 A08;
    public final C15990oG A09;
    public final C18240s8 A0A;
    public final JniBridge A0B;

    public C31211aB(C15450nH r1, C29251Rl r2, C16040oL r3, C31161a6 r4, AnonymousClass1RM r5, C29291Rp r6, AnonymousClass1RX r7, AnonymousClass1RP r8, C14830m7 r9, C15990oG r10, C18240s8 r11, JniBridge jniBridge) {
        this.A08 = r9;
        this.A0B = jniBridge;
        this.A00 = r1;
        this.A0A = r11;
        this.A09 = r10;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A02 = r3;
        this.A07 = r8;
        this.A03 = r4;
        this.A01 = r2;
    }

    public final List A00(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            JniBridge jniBridge = this.A0B;
            Object jvidispatchOOO = JniBridge.jvidispatchOOO(1, jniBridge.wajContext.get(), ((AnonymousClass1PA) it.next()).A01);
            if (jvidispatchOOO == null) {
                Log.e("wamsys/convertToNativePublicKeyList/public-key-conversion-failed");
                return null;
            }
            arrayList.add(jvidispatchOOO);
        }
        return arrayList;
    }
}
