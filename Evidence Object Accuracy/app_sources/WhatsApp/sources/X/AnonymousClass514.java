package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.514  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass514 implements AbstractC13670k8 {
    public AbstractC115775Sw A00;
    public final Object A01 = C12970iu.A0l();
    public final Executor A02;

    public AnonymousClass514(AbstractC115775Sw r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r2;
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        synchronized (this.A01) {
            if (this.A00 != null) {
                this.A02.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 22, this));
            }
        }
    }
}
