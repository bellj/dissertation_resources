package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: X.29B  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass29B {
    public static int computeArrayListCapacity(int i) {
        C28251Mi.checkNonnegative(i, "arraySize");
        long j = ((long) i) + 5 + ((long) (i / 10));
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }

    public static boolean equalsImpl(List list, Object obj) {
        if (obj != list) {
            if (obj instanceof List) {
                List list2 = (List) obj;
                int size = list.size();
                if (size == list2.size()) {
                    if (!(list instanceof RandomAccess) || !(list2 instanceof RandomAccess)) {
                        return AnonymousClass1I4.elementsEqual(list.iterator(), list2.iterator());
                    }
                    for (int i = 0; i < size; i++) {
                        if (AnonymousClass28V.A00(list.get(i), list2.get(i))) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static int indexOfImpl(List list, Object obj) {
        if (list instanceof RandomAccess) {
            return indexOfRandomAccess(list, obj);
        }
        ListIterator listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (AnonymousClass28V.A00(obj, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }

    public static int indexOfRandomAccess(List list, Object obj) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (obj.equals(list.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOfImpl(List list, Object obj) {
        if (list instanceof RandomAccess) {
            return lastIndexOfRandomAccess(list, obj);
        }
        ListIterator listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (AnonymousClass28V.A00(obj, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    public static int lastIndexOfRandomAccess(List list, Object obj) {
        int size = list.size();
        do {
            size--;
            if (size < 0) {
                return -1;
            }
        } while (!obj.equals(list.get(size)));
        return size;
    }

    public static ArrayList newArrayList() {
        return new ArrayList();
    }

    public static ArrayList newArrayList(Iterable iterable) {
        if (iterable instanceof Collection) {
            return new ArrayList((Collection) iterable);
        }
        return newArrayList(iterable.iterator());
    }

    public static ArrayList newArrayList(Iterator it) {
        ArrayList newArrayList = newArrayList();
        AnonymousClass1I4.addAll(newArrayList, it);
        return newArrayList;
    }

    public static ArrayList newArrayList(Object... objArr) {
        ArrayList arrayList = new ArrayList(computeArrayListCapacity(objArr.length));
        Collections.addAll(arrayList, objArr);
        return arrayList;
    }
}
