package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.payments.ui.IndiaUpiSecureQrCodeDisplayActivity;

/* renamed from: X.652  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass652 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ IndiaUpiSecureQrCodeDisplayActivity A01;

    public AnonymousClass652(View view, IndiaUpiSecureQrCodeDisplayActivity indiaUpiSecureQrCodeDisplayActivity) {
        this.A01 = indiaUpiSecureQrCodeDisplayActivity;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        IndiaUpiSecureQrCodeDisplayActivity indiaUpiSecureQrCodeDisplayActivity = this.A01;
        View view = this.A00;
        if (!C252718t.A00(view)) {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            indiaUpiSecureQrCodeDisplayActivity.A31();
        }
    }
}
