package X;

/* renamed from: X.1Xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30451Xl extends C30461Xm {
    public String A00;
    public String A01;

    public C30451Xl(AnonymousClass1IS r7, long j) {
        super(r7, (AnonymousClass1OT) null, 1, j);
    }

    public C30451Xl(AnonymousClass1YM r7, AnonymousClass1OT r8, long j) {
        super(r7, r8, 1, j);
    }

    @Override // X.AbstractC15340mz
    public void A0l(String str) {
        A16(str);
    }

    public String A15() {
        String str;
        synchronized (this.A10) {
            str = this.A00;
        }
        return str;
    }

    public void A16(String str) {
        synchronized (this.A10) {
            this.A00 = str;
        }
    }
}
