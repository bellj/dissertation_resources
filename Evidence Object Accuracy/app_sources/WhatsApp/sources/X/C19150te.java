package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.util.Log;

/* renamed from: X.0te  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19150te extends AbstractC18500sY implements AbstractC19010tQ {
    public final C19780uf A00;
    public final C18460sU A01;
    public final C19770ue A02;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19150te(C19780uf r3, C18460sU r4, C19770ue r5, C18480sW r6) {
        super(r6, "message_frequent", 1);
        this.A01 = r4;
        this.A02 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("jid");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("type");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("message_count");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            C19770ue r14 = this.A02;
            AnonymousClass1YE A00 = r14.A00("INSERT INTO frequent(jid_row_id, type, message_count) VALUES (?, ?, ?)");
            j = cursor.getLong(columnIndexOrThrow);
            String string = cursor.getString(columnIndexOrThrow2);
            long j2 = cursor.getLong(columnIndexOrThrow3);
            long j3 = cursor.getLong(columnIndexOrThrow4);
            AbstractC14640lm A01 = AbstractC14640lm.A01(string);
            if (A01 != null) {
                long A012 = this.A01.A01(A01);
                A00.A01(1, A012);
                A00.A01(2, j2);
                A00.A01(3, j3);
                try {
                    A00.A00.executeInsert();
                } catch (SQLiteConstraintException unused) {
                    AnonymousClass1YE A002 = r14.A00("UPDATE frequent   SET message_count = ? WHERE jid_row_id = ? AND type = ?");
                    A002.A01(2, A012);
                    A002.A01(3, j2);
                    A002.A01(1, j3);
                    A002.A00();
                }
                i++;
            } else {
                StringBuilder sb = new StringBuilder("FrequentMessageStore/processBatch/invalid jid in original table, jid=");
                sb.append(string);
                Log.e(sb.toString());
            }
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("frequent", null, null);
            C21390xL r1 = this.A06;
            r1.A03("frequent_ready");
            r1.A03("migration_frequent_index");
            r1.A03("migration_frequent_retry");
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
