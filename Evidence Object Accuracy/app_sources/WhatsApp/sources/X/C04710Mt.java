package X;

import android.content.ComponentName;
import android.os.IBinder;

/* renamed from: X.0Mt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04710Mt {
    public final ComponentName A00;
    public final IBinder A01;

    public C04710Mt(ComponentName componentName, IBinder iBinder) {
        this.A00 = componentName;
        this.A01 = iBinder;
    }
}
