package X;

import java.util.List;

/* renamed from: X.0D2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0D2 extends AbstractC07280Xj {
    public C07270Xi A00;
    public C02580Cy A01 = null;

    public AnonymousClass0D2(AnonymousClass0QV r4) {
        super(r4);
        C07270Xi r2 = new C07270Xi(this);
        this.A00 = r2;
        this.A05.A04 = EnumC03760Ja.TOP;
        this.A04.A04 = EnumC03760Ja.BOTTOM;
        r2.A04 = EnumC03760Ja.BASELINE;
        super.A01 = 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0258  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:140:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:147:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01e4  */
    @Override // X.AbstractC07280Xj
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
        // Method dump skipped, instructions count: 768
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0D2.A06():void");
    }

    @Override // X.AbstractC07280Xj
    public void A07() {
        C07270Xi r2 = this.A05;
        if (r2.A0B) {
            this.A03.A0Q = r2.A02;
        }
    }

    @Override // X.AbstractC07280Xj
    public void A08() {
        this.A07 = null;
        this.A05.A00();
        this.A04.A00();
        this.A00.A00();
        this.A06.A00();
        this.A09 = false;
    }

    @Override // X.AbstractC07280Xj
    public boolean A0B() {
        if (this.A02 != AnonymousClass0JR.MATCH_CONSTRAINT || this.A03.A0C == 0) {
            return true;
        }
        return false;
    }

    public void A0C() {
        this.A09 = false;
        C07270Xi r0 = this.A05;
        r0.A00();
        r0.A0B = false;
        C07270Xi r02 = this.A04;
        r02.A00();
        r02.A0B = false;
        C07270Xi r03 = this.A00;
        r03.A00();
        r03.A0B = false;
        this.A06.A0B = false;
    }

    @Override // X.AbstractC07280Xj, X.AbstractC11720gk
    public void AfH(AbstractC11720gk r13) {
        int i;
        float f;
        if (3 - this.A08.ordinal() != 0) {
            C02580Cy r5 = this.A06;
            if (r5.A0A && !r5.A0B && this.A02 == AnonymousClass0JR.MATCH_CONSTRAINT) {
                AnonymousClass0QV r6 = this.A03;
                int i2 = r6.A0C;
                if (i2 == 2) {
                    AnonymousClass0QV r0 = r6.A0Z;
                    if (r0 != null) {
                        C02580Cy r2 = r0.A0d.A06;
                        if (r2.A0B) {
                            i = (int) ((((float) r2.A02) * r6.A03) + 0.5f);
                            r5.A01(i);
                        }
                    }
                } else if (i2 == 3) {
                    C02580Cy r22 = r6.A0c.A06;
                    if (r22.A0B) {
                        int i3 = r6.A08;
                        if (i3 != -1) {
                            if (i3 == 0) {
                                f = ((float) r22.A02) * r6.A01;
                                i = (int) (f + 0.5f);
                                r5.A01(i);
                            } else if (i3 != 1) {
                                i = 0;
                                r5.A01(i);
                            }
                        }
                        f = ((float) r22.A02) / r6.A01;
                        i = (int) (f + 0.5f);
                        r5.A01(i);
                    }
                }
            }
            C07270Xi r7 = this.A05;
            if (r7.A0A) {
                C07270Xi r62 = this.A04;
                if (!r62.A0A) {
                    return;
                }
                if (!r7.A0B || !r62.A0B || !r5.A0B) {
                    if (!r5.A0B) {
                        AnonymousClass0JR r8 = this.A02;
                        AnonymousClass0JR r23 = AnonymousClass0JR.MATCH_CONSTRAINT;
                        if (r8 == r23) {
                            AnonymousClass0QV r1 = this.A03;
                            if (r1.A0D == 0 && !r1.A0F()) {
                                int i4 = ((C07270Xi) r7.A08.get(0)).A02 + r7.A00;
                                int i5 = ((C07270Xi) r62.A08.get(0)).A02 + r62.A00;
                                r7.A01(i4);
                                r62.A01(i5);
                                r5.A01(i5 - i4);
                                return;
                            }
                        }
                        if (r8 == r23 && super.A00 == 1) {
                            List list = r7.A08;
                            if (list.size() > 0) {
                                List list2 = r62.A08;
                                if (list2.size() > 0) {
                                    int i6 = (((C07270Xi) list2.get(0)).A02 + r62.A00) - (((C07270Xi) list.get(0)).A02 + r7.A00);
                                    int i7 = r5.A00;
                                    if (i6 < i7) {
                                        r5.A01(i6);
                                    } else {
                                        r5.A01(i7);
                                    }
                                }
                            }
                        }
                    }
                    if (r5.A0B) {
                        List list3 = r7.A08;
                        if (list3.size() > 0) {
                            List list4 = r62.A08;
                            if (list4.size() > 0) {
                                C07270Xi r10 = (C07270Xi) list3.get(0);
                                C07270Xi r9 = (C07270Xi) list4.get(0);
                                int i8 = r10.A02;
                                int i9 = i8 + r7.A00;
                                int i10 = r9.A02;
                                int i11 = i10 + r62.A00;
                                float f2 = this.A03.A06;
                                if (r10 == r9) {
                                    i9 = i8;
                                    i11 = i10;
                                    f2 = 0.5f;
                                }
                                r7.A01((int) (((float) i9) + 0.5f + (((float) ((i11 - i9) - r5.A02)) * f2)));
                                r62.A01(r7.A02 + r5.A02);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        AnonymousClass0QV r02 = this.A03;
        A09(r02.A0Y, r02.A0S, 1);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VerticalRun ");
        sb.append(this.A03.A0f);
        return sb.toString();
    }
}
