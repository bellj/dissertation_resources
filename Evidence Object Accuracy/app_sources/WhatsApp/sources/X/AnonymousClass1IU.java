package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.InterceptingEditText;

/* renamed from: X.1IU  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1IU extends InterceptingEditText {
    public AnonymousClass1IU(Context context) {
        super(context);
        A02();
    }

    public AnonymousClass1IU(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AnonymousClass1IU(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }
}
