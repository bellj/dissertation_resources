package X;

import android.os.Build;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0yM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22020yM {
    public final Map A00 = new HashMap();

    public C22020yM() {
    }

    public C22020yM(C14820m6 r5) {
        String A0A = r5.A0A();
        Map map = this.A00;
        map.put("device_id", A0A);
        map.put("app_build", "release");
        map.put("release_channel", "release");
        map.put("app_version", "2.22.17.70");
        map.put("os_version", Build.VERSION.RELEASE);
        map.put("platform", "android");
    }
}
