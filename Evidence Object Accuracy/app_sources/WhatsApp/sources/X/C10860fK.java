package X;

/* renamed from: X.0fK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10860fK extends Throwable {
    public C10860fK(String str) {
        super(str);
    }

    @Override // java.lang.Throwable
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
