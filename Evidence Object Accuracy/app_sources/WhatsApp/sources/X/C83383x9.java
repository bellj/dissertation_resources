package X;

import android.view.animation.Animation;
import androidx.appcompat.widget.SearchView;
import com.whatsapp.group.GroupParticipantsSearchFragment;

/* renamed from: X.3x9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83383x9 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ SearchView A00;
    public final /* synthetic */ GroupParticipantsSearchFragment A01;

    public C83383x9(SearchView searchView, GroupParticipantsSearchFragment groupParticipantsSearchFragment) {
        this.A01 = groupParticipantsSearchFragment;
        this.A00 = searchView;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        GroupParticipantsSearchFragment groupParticipantsSearchFragment = this.A01;
        SearchView searchView = this.A00;
        if (groupParticipantsSearchFragment.A09) {
            searchView.setIconified(false);
            groupParticipantsSearchFragment.A09 = false;
            return;
        }
        groupParticipantsSearchFragment.A07.A01(searchView);
    }
}
