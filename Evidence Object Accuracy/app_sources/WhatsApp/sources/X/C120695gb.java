package X;

import android.content.Context;

/* renamed from: X.5gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120695gb extends C120895gv {
    public final /* synthetic */ C120385g6 A00;
    public final /* synthetic */ C128795wg A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120695gb(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120385g6 r11, C128795wg r12) {
        super(context, r8, r9, r10, "upi-get-p2m-config");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r7) {
        super.A02(r7);
        this.A01.A00(r7, null, null, null, null);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r7) {
        super.A03(r7);
        this.A01.A00(r7, null, null, null, null);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r8) {
        try {
            AnonymousClass1V8 A0F = r8.A0F("account");
            this.A01.A00(null, A0F.A0H("mcc"), A0F.A0H("receiver-vpa"), A0F.A0I("payee-name", null), A0F.A0I("purpose-code", null));
        } catch (AnonymousClass1V9 unused) {
            this.A01.A00(C117305Zk.A0L(), null, null, null, null);
        }
    }
}
