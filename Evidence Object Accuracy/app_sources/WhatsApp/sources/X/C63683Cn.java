package X;

import android.app.Activity;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.3Cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63683Cn {
    public final Activity A00;
    public final C15890o4 A01;
    public final C14960mK A02;

    public C63683Cn(Activity activity, C15890o4 r2, C14960mK r3) {
        this.A01 = r2;
        this.A00 = activity;
        this.A02 = r3;
    }

    public boolean A00() {
        C15890o4 r0 = this.A01;
        boolean z = !r0.A07();
        if (C12960it.A1S(r0.A02("android.permission.RECORD_AUDIO"))) {
            if (z) {
                Activity activity = this.A00;
                C35751ig r8 = new C35751ig(activity);
                r8.A09 = new int[]{R.drawable.permission_storage, R.drawable.permission_plus, R.drawable.permission_mic};
                r8.A02 = R.string.permission_storage_mic_on_audio_msg_request;
                r8.A0A = null;
                int i = Build.VERSION.SDK_INT;
                int i2 = R.string.permission_storage_mic_on_audio_msg_v30;
                if (i < 30) {
                    i2 = R.string.permission_storage_mic_on_audio_msg;
                }
                r8.A03 = i2;
                r8.A08 = null;
                r8.A0C = new String[]{"android.permission.RECORD_AUDIO", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
                activity.startActivity(r8.A00());
            } else {
                Activity activity2 = this.A00;
                C35751ig r1 = new C35751ig(activity2);
                r1.A01 = R.drawable.permission_mic;
                r1.A02 = R.string.permission_mic_access_on_audio_msg_request;
                r1.A03 = R.string.permission_mic_access_on_audio_msg;
                r1.A0C = new String[]{"android.permission.RECORD_AUDIO"};
                activity2.startActivity(r1.A00());
            }
        } else if (!z) {
            return true;
        } else {
            Activity activity3 = this.A00;
            int i3 = Build.VERSION.SDK_INT;
            int i4 = R.string.permission_storage_need_write_access_on_record_audio_v30;
            if (i3 < 30) {
                i4 = R.string.permission_storage_need_write_access_on_record_audio;
            }
            RequestPermissionActivity.A0K(activity3, R.string.permission_storage_need_write_access_on_record_audio_request, i4);
        }
        return false;
    }
}
