package X;

import com.whatsapp.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.6B1  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B1 implements AnonymousClass22W {
    public final /* synthetic */ C119215d9 A00;

    public AnonymousClass6B1(C119215d9 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass22Y
    public void AMI(C30921Zi r10, File file) {
        C119215d9 r7 = this.A00;
        List list = r7.A0K;
        synchronized (list) {
            ArrayList A0x = C12980iv.A0x(list);
            Iterator it = A0x.iterator();
            boolean z = false;
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C30921Zi r8 = ((C129025x3) it.next()).A03;
                if (r8 != null) {
                    String str = r8.A0F;
                    String str2 = r10.A0F;
                    if (str.equals(str2)) {
                        C129025x3 r1 = new C129025x3(r8);
                        if (file != null) {
                            r1.A00 = true;
                        } else {
                            r1.A00 = false;
                            r1.A01 = true;
                        }
                        C30921Zi r0 = r7.A08;
                        if (r0 != null && str2.equals(r0.A0F)) {
                            z = true;
                        }
                        r1.A02 = z;
                        A0x.set(i, r1);
                        r7.A09.A0E(A0x);
                        list.set(i, r1);
                    }
                }
                i++;
            }
        }
    }

    @Override // X.AnonymousClass22X
    public void APk() {
        this.A00.A01.postDelayed(new Runnable() { // from class: X.6Gz
            @Override // java.lang.Runnable
            public final void run() {
                C119215d9.A00(AnonymousClass6B1.this.A00);
            }
        }, 2000);
    }

    @Override // X.AnonymousClass22X
    public /* bridge */ /* synthetic */ void ASp(Object obj) {
        boolean z;
        List<C30921Zi> list = (List) obj;
        C119215d9 r5 = this.A00;
        r5.A05.setVisibility(0);
        List list2 = r5.A0K;
        synchronized (list2) {
            r5.A09.A01 = r5.A08;
            list2.clear();
            C129025x3 r1 = new C129025x3(null);
            r1.A00 = true;
            r1.A02 = C12980iv.A1X(r5.A08);
            list2.add(r1);
            for (C30921Zi r3 : list) {
                C129025x3 r2 = new C129025x3(r3);
                C30921Zi r0 = r5.A08;
                if (r0 != null) {
                    boolean equals = r0.A0F.equals(r3.A0F);
                    z = true;
                    if (equals) {
                        r2.A02 = z;
                        list2.add(r2);
                    }
                }
                z = false;
                r2.A02 = z;
                list2.add(r2);
            }
            r5.A09.A0E(list2);
        }
        r5.A06.setVisibility(8);
        r5.A03.setText(R.string.select_expressive_background_themes);
    }

    @Override // X.AnonymousClass22X
    public void AXX() {
        this.A00.A01.postDelayed(new Runnable() { // from class: X.6Gy
            @Override // java.lang.Runnable
            public final void run() {
                C119215d9.A00(AnonymousClass6B1.this.A00);
            }
        }, 2000);
    }
}
