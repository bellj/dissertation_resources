package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122665lp extends AbstractC118835cS {
    public final View A00;
    public final TextView A01;
    public final TextView A02;

    public C122665lp(View view) {
        super(view);
        this.A00 = AnonymousClass028.A0D(view, R.id.root);
        this.A01 = C12960it.A0I(view, R.id.key_name);
        this.A02 = C12960it.A0I(view, R.id.value_text);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r6, int i) {
        C123335mz r62 = (C123335mz) r6;
        this.A01.setText(r62.A02);
        this.A02.setText(r62.A03);
        View view = this.A00;
        int dimension = (int) view.getResources().getDimension(r62.A00);
        view.setPadding(view.getPaddingLeft(), (int) view.getResources().getDimension(r62.A01), view.getPaddingRight(), dimension);
    }
}
