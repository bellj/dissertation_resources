package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.mediacomposer.doodle.ColorPickerComponent;
import com.whatsapp.mediacomposer.doodle.textentry.DoodleEditText;

/* renamed from: X.28y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC471028y extends AbstractC471128z {
    public int A00;
    public View A01;
    public WaImageView A02;
    public WaTextView A03;
    public AnonymousClass01d A04;
    public AnonymousClass19M A05;
    public ColorPickerComponent A06;
    public AnonymousClass2ZW A07;
    public DoodleEditText A08;
    public boolean A09;

    public AbstractC471028y(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AbstractC471028y(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void A00(Window window, AnonymousClass290 r9, C91494Ry r10, int[] iArr, boolean z) {
        window.setLayout(-1, -1);
        window.setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        window.clearFlags(256);
        if (Build.VERSION.SDK_INT >= 28) {
            window.getAttributes().layoutInDisplayCutoutMode = 1;
        }
        C38241nl.A00(findViewById(R.id.main), window, this.A04);
        this.A07 = new AnonymousClass2ZW(getContext(), 0);
        this.A03 = (WaTextView) AnonymousClass028.A0D(this, R.id.font_picker_preview);
        this.A06 = (ColorPickerComponent) AnonymousClass028.A0D(this, R.id.color_picker_component);
        View A0D = AnonymousClass028.A0D(this, R.id.picker_button_container);
        this.A01 = A0D;
        A0D.setPadding(iArr[0], iArr[1], iArr[2], iArr[3]);
        int i = this.A00;
        if (i > 0) {
            this.A06.setMaxHeight(i);
        }
        if (this.A09) {
            this.A06.A00();
        }
        this.A06.setColorAndInvalidate(r10.A01);
        if (z) {
            this.A07.A01(r10.A01);
        } else {
            AnonymousClass3YM r6 = (AnonymousClass3YM) r9;
            ValueAnimator valueAnimator = r6.A01;
            valueAnimator.setInterpolator(AnonymousClass0L1.A00(0.5f, 1.35f, 0.4f, 1.0f));
            valueAnimator.setDuration(400L);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Jj
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    AnonymousClass3YM r0 = AnonymousClass3YM.this;
                    AbstractC471028y r1 = r0.A03;
                    int i2 = r0.A04.A01;
                    AnonymousClass2ZW r12 = r1.A07;
                    float A00 = C12960it.A00(valueAnimator2);
                    r12.A03 = i2;
                    r12.A01 = A00;
                    r12.invalidateSelf();
                }
            });
            valueAnimator.addListener(new C72513ej(r6));
            valueAnimator.start();
        }
        DoodleEditText doodleEditText = (DoodleEditText) AnonymousClass028.A0D(this, R.id.text);
        this.A08 = doodleEditText;
        doodleEditText.setTextColor(r10.A01);
        this.A08.setText(r10.A03);
        this.A08.setFontStyle(r10.A02);
        DoodleEditText doodleEditText2 = this.A08;
        int length = r10.A03.length();
        doodleEditText2.setSelection(length, length);
        this.A08.setOnEditorActionListener(new TextView.OnEditorActionListener(r9) { // from class: X.3OW
            public final /* synthetic */ AnonymousClass290 A01;

            {
                this.A01 = r2;
            }

            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                AbstractC471028y r3 = AbstractC471028y.this;
                AnonymousClass290 r2 = this.A01;
                if (i2 != 6) {
                    return false;
                }
                r3.A06.A05(r3.A09);
                AnonymousClass3YM r22 = (AnonymousClass3YM) r2;
                r22.A04.A03 = C12980iv.A0q(textView);
                r22.dismiss();
                return true;
            }
        });
        DoodleEditText doodleEditText3 = this.A08;
        doodleEditText3.A01 = new C1110257u(this, r9);
        doodleEditText3.addTextChangedListener(new AnonymousClass363(this, r9));
        this.A03.setTypeface(this.A08.getTypeface());
        WaImageView waImageView = (WaImageView) AnonymousClass028.A0D(this, R.id.font_picker_btn);
        this.A02 = waImageView;
        waImageView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(r9, 45));
        this.A02.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.3Mq
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                AnonymousClass3YM r4 = (AnonymousClass3YM) AnonymousClass290.this;
                C91494Ry r1 = r4.A04;
                int i2 = (r1.A02 - 1) % 4;
                r1.A02 = i2;
                AbstractC471028y r2 = r4.A03;
                int i3 = r1.A01;
                r2.A08.setFontStyle(i2);
                r2.A08.setTextColor(i3);
                r2.A03.setTypeface(r2.A08.getTypeface());
                AnonymousClass2Ab r0 = r4.A02;
                r0.A00 = i2;
                r0.A0Q.A0H.setFont(i2);
                return true;
            }
        });
        this.A06.A04(null, new AnonymousClass3YG(this, r10), null);
        this.A02.setImageDrawable(this.A07);
        View$OnTouchListenerC101354nO r3 = new View$OnTouchListenerC101354nO(this);
        AnonymousClass028.A0D(this, R.id.main).setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 34, r9));
        AnonymousClass028.A0D(this, R.id.main).setOnTouchListener(r3);
        window.setSoftInputMode(5);
        this.A08.post(new RunnableBRunnable0Shape6S0200000_I0_6(this, 2, r9));
        this.A08.A04(false);
    }

    public void setDelayShowColorPicker(boolean z) {
        this.A09 = z;
    }

    public void setEntryTextSize(float f) {
        this.A08.setTextSize(f);
    }

    public void setMaxColorPickerHeight(int i) {
        this.A00 = i;
    }
}
