package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.3BS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BS {
    public final View A00;
    public final ImageView A01;
    public final TextEmojiLabel A02;
    public final TextEmojiLabel A03;
    public final SelectionCheckView A04;

    public AnonymousClass3BS(View view) {
        this.A00 = view;
        this.A01 = C12970iu.A0L(view, R.id.contactpicker_row_photo);
        TextEmojiLabel A0U = C12970iu.A0U(view, R.id.name);
        this.A02 = A0U;
        AnonymousClass028.A0a(A0U, 2);
        C27531Hw.A06(A0U);
        this.A04 = (SelectionCheckView) view.findViewById(R.id.selection_check);
        this.A03 = C12970iu.A0U(view, R.id.phone_number);
    }
}
