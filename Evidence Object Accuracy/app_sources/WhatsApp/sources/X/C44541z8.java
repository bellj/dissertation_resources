package X;

import java.util.Arrays;

/* renamed from: X.1z8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44541z8 {
    public final C32821cn A00;
    public final byte[] A01;
    public final byte[] A02;

    public C44541z8(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5, byte[] bArr6) {
        this.A00 = new C32821cn(str, bArr, bArr2, bArr3, bArr5);
        this.A02 = bArr4;
        this.A01 = bArr6;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BackupKey [");
        sb.append(this.A00.toString());
        sb.append(", hashedGoogleId=");
        sb.append(Arrays.toString(this.A02));
        sb.append(", cipherKey=");
        sb.append(Arrays.toString(this.A01));
        sb.append("]");
        return sb.toString();
    }
}
