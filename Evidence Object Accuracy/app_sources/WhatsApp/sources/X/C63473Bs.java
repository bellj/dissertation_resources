package X;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.RoundCornerProgressBarV2;
import com.whatsapp.polls.PollVoterViewModel;

/* renamed from: X.3Bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63473Bs {
    public C27701Iu A00;
    public C27671Iq A01;
    public boolean A02 = true;
    public final View A03;
    public final CheckBox A04;
    public final TextView A05;
    public final TextView A06;
    public final RoundCornerProgressBarV2 A07;
    public final AnonymousClass018 A08;
    public final PollVoterViewModel A09;
    public final AnonymousClass1HC A0A;

    public C63473Bs(View view, AnonymousClass018 r5, PollVoterViewModel pollVoterViewModel) {
        this.A08 = r5;
        this.A09 = pollVoterViewModel;
        this.A03 = view;
        this.A05 = C12960it.A0I(view, R.id.poll_option_name);
        this.A06 = C12960it.A0I(view, R.id.poll_option_vote_count);
        this.A07 = (RoundCornerProgressBarV2) AnonymousClass028.A0D(view, R.id.poll_vote_ratio);
        CheckBox checkBox = (CheckBox) AnonymousClass028.A0D(view, R.id.poll_option_vote_checkbox);
        this.A04 = checkBox;
        this.A0A = new AnonymousClass1HC(AnonymousClass028.A0D(view, R.id.poll_option_voted_checkmark));
        C12960it.A14(checkBox, this, pollVoterViewModel, 22);
    }
}
