package X;

import org.chromium.net.UrlRequest;

/* renamed from: X.4Yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92944Yc {
    public static int A00(C95304dT r1, int i) {
        int i2;
        int i3;
        int i4;
        switch (i) {
            case 1:
                return 192;
            case 2:
            case 3:
            case 4:
            case 5:
                i2 = 576;
                i3 = i - 2;
                return i2 << i3;
            case 6:
                i4 = r1.A0C();
                return i4 + 1;
            case 7:
                i4 = r1.A0F();
                return i4 + 1;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
                i2 = 256;
                i3 = i - 8;
                return i2 << i3;
            default:
                return -1;
        }
    }

    public static boolean A01(AnonymousClass4IE r17, C95424dg r18, C95304dT r19, int i) {
        int i2;
        int A0F;
        long A0I = r19.A0I();
        long j = A0I >>> 16;
        if (j != ((long) i)) {
            return false;
        }
        boolean A1T = C12960it.A1T(((j & 1) > 1 ? 1 : ((j & 1) == 1 ? 0 : -1)));
        int i3 = (int) ((A0I >> 12) & 15);
        int i4 = (int) ((A0I >> 8) & 15);
        int i5 = (int) (15 & (A0I >> 4));
        int i6 = (int) ((A0I >> 1) & 7);
        boolean A1T2 = C12960it.A1T(((A0I & 1) > 1 ? 1 : ((A0I & 1) == 1 ? 0 : -1)));
        if (i5 <= 7) {
            i2 = r18.A02 - 1;
        } else if (i5 > 10) {
            return false;
        } else {
            i5 = r18.A02;
            i2 = 2;
        }
        if (i5 != i2) {
            return false;
        }
        if (!(i6 == 0 || i6 == r18.A01) || A1T2) {
            return false;
        }
        try {
            long A0K = r19.A0K();
            if (!A1T) {
                A0K *= (long) r18.A03;
            }
            r17.A00 = A0K;
            int A00 = A00(r19, i3);
            if (A00 == -1 || A00 > r18.A03) {
                return false;
            }
            int i7 = r18.A07;
            if (i4 != 0) {
                if (i4 > 11) {
                    if (i4 == 12) {
                        A0F = r19.A0C() * 1000;
                    } else if (i4 > 14) {
                        return false;
                    } else {
                        A0F = r19.A0F();
                        if (i4 == 14) {
                            A0F *= 10;
                        }
                    }
                    if (A0F != i7) {
                        return false;
                    }
                } else if (i4 != r18.A08) {
                    return false;
                }
            }
            int A0C = r19.A0C();
            int i8 = r19.A01;
            byte[] bArr = r19.A02;
            int i9 = i8 - 1;
            int i10 = 0;
            for (int i11 = r19.A01; i11 < i9; i11++) {
                i10 = AnonymousClass3JZ.A0C[i10 ^ (bArr[i11] & 255)];
            }
            if (A0C == i10) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return false;
        }
    }
}
