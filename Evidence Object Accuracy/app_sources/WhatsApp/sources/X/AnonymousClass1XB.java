package X;

/* renamed from: X.1XB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XB extends AbstractC15340mz {
    public final int A00;

    public AnonymousClass1XB(AnonymousClass1IS r2, int i, long j) {
        super(r2, (byte) 0, j);
        A0Y(6);
        this.A00 = i;
    }

    @Override // X.AbstractC15340mz
    public void A0Y(int i) {
        if (i != 6) {
            AnonymousClass009.A07("Cannot change status for FMessageSystem");
        }
        super.A0Y(i);
    }

    public boolean A14() {
        int i = this.A00;
        return i == 18 || i == 57 || i == 71 || i == 20 || i == 79 || i == 90 || i == 14 || i == 52 || i == 27 || i == 4 || i == 7 || i == 51 || i == 11 || i == 17 || i == 1 || i == 6 || i == 5 || i == 12 || i == 29 || i == 30 || i == 31 || i == 32 || i == 54 || i == 53 || i == 13 || i == 15 || i == 16 || i == 81 || i == 82 || i == 9 || i == 21 || i == 84 || i == 85 || i == 83 || i == 42 || i == 40 || i == 41 || i == 64 || i == 65 || i == 66 || i == 56 || i == 59 || i == 80 || i == 91 || i == 92 || i == 60 || i == 68 || i == 70 || i == 75 || i == 95 || i == 76 || i == 77 || i == 78 || i == 87 || i == 88 || i == 89;
    }
}
