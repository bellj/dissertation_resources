package X;

import android.content.DialogInterface;
import androidx.preference.MultiSelectListPreferenceDialogFragmentCompat;
import java.util.Set;

/* renamed from: X.0V8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V8 implements DialogInterface.OnMultiChoiceClickListener {
    public final /* synthetic */ MultiSelectListPreferenceDialogFragmentCompat A00;

    public AnonymousClass0V8(MultiSelectListPreferenceDialogFragmentCompat multiSelectListPreferenceDialogFragmentCompat) {
        this.A00 = multiSelectListPreferenceDialogFragmentCompat;
    }

    @Override // android.content.DialogInterface.OnMultiChoiceClickListener
    public void onClick(DialogInterface dialogInterface, int i, boolean z) {
        boolean remove;
        MultiSelectListPreferenceDialogFragmentCompat multiSelectListPreferenceDialogFragmentCompat = this.A00;
        boolean z2 = multiSelectListPreferenceDialogFragmentCompat.A01;
        Set set = multiSelectListPreferenceDialogFragmentCompat.A00;
        String charSequence = multiSelectListPreferenceDialogFragmentCompat.A03[i].toString();
        if (z) {
            remove = set.add(charSequence);
        } else {
            remove = set.remove(charSequence);
        }
        multiSelectListPreferenceDialogFragmentCompat.A01 = remove | z2;
    }
}
