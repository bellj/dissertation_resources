package X;

import android.view.Window;

/* renamed from: X.0XI  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0XI implements AbstractC12280hf {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public AnonymousClass0XI(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12280hf
    public void AOF(AnonymousClass07H r2, boolean z) {
        this.A00.A0U(r2);
    }

    @Override // X.AbstractC12280hf
    public boolean ATF(AnonymousClass07H r3) {
        Window.Callback callback = this.A00.A08.getCallback();
        if (callback == null) {
            return true;
        }
        callback.onMenuOpened(C43951xu.A03, r3);
        return true;
    }
}
