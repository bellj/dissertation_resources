package X;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30591Ya extends C006202y {
    public final /* synthetic */ AnonymousClass1YX A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C30591Ya(AnonymousClass1YX r2) {
        super(250);
        this.A00 = r2;
    }

    @Override // X.C006202y
    public /* bridge */ /* synthetic */ void A09(Object obj, Object obj2, Object obj3, boolean z) {
        if (z) {
            AnonymousClass1YX r1 = this.A00;
            Map map = r1.A02;
            map.put(obj, new WeakReference(obj2));
            int i = r1.A00 + 1;
            r1.A00 = i;
            if (i % 200 == 0) {
                ArrayList arrayList = new ArrayList();
                for (Map.Entry entry : map.entrySet()) {
                    if (((Reference) entry.getValue()).get() == null) {
                        arrayList.add(entry.getKey());
                    }
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    map.remove(it.next());
                }
            }
        }
    }
}
