package X;

/* renamed from: X.5sY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126235sY {
    public final AnonymousClass1V8 A00;

    public C126235sY(C126245sZ r11, AnonymousClass3CS r12, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-fetch-network-info");
        if (AnonymousClass3JT.A0E(str, 6, 6, false)) {
            C41141sy.A01(A0N, "bin", str);
        }
        A0N.A05(r11.A00);
        this.A00 = C117295Zj.A0I(A0N, A0M, r12);
    }
}
