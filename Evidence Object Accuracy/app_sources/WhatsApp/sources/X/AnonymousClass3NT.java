package X;

import android.view.ViewTreeObserver;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;

/* renamed from: X.3NT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NT implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ UserNoticeBottomSheetDialogFragment A00;

    public AnonymousClass3NT(UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment) {
        this.A00 = userNoticeBottomSheetDialogFragment;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = this.A00;
        C12980iv.A1E(userNoticeBottomSheetDialogFragment.A08, this);
        boolean z = false;
        if ((userNoticeBottomSheetDialogFragment.A02.getY() - C12990iw.A03(userNoticeBottomSheetDialogFragment.A08)) - ((float) userNoticeBottomSheetDialogFragment.A08.getScrollY()) < 0.0f) {
            z = true;
        }
        userNoticeBottomSheetDialogFragment.A1O(!z, false);
    }
}
