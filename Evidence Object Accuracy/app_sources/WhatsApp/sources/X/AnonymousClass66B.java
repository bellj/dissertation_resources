package X;

import android.hardware.camera2.CaptureRequest;

/* renamed from: X.66B  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66B implements AbstractC136176Lh {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ C130185yw A01;
    public final /* synthetic */ AnonymousClass66I A02;
    public final /* synthetic */ boolean A03;
    public final /* synthetic */ float[] A04;

    public AnonymousClass66B(CaptureRequest.Builder builder, C130185yw r2, AnonymousClass66I r3, float[] fArr, boolean z) {
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = fArr;
        this.A00 = builder;
        this.A03 = z;
    }

    @Override // X.AbstractC136176Lh
    public void AQg(boolean z) {
        EnumC124565pk r1;
        C130185yw r6 = this.A01;
        boolean z2 = r6.A09;
        AnonymousClass66I r5 = this.A02;
        if (z2) {
            r6.A0A(r5);
        } else {
            r5.A06 = null;
        }
        if (z) {
            r1 = EnumC124565pk.SUCCESS;
        } else {
            r1 = EnumC124565pk.FAILED;
        }
        r6.A09(r1, this.A04);
        if (!r6.A0E) {
            CaptureRequest.Builder builder = this.A00;
            Number number = (Number) builder.get(CaptureRequest.CONTROL_AE_MODE);
            long j = 4000;
            if (number == null || number.intValue() != 1) {
                if (!this.A03) {
                    j = 2000;
                }
                r6.A08(builder, r5, j);
                return;
            }
            if (!this.A03) {
                j = 2000;
            }
            r6.A07(builder, r5, j);
        }
    }
}
