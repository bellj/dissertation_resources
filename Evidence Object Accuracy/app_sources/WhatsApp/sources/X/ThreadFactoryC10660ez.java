package X;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.0ez  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ThreadFactoryC10660ez implements ThreadFactory {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass0SJ A01;

    public ThreadFactoryC10660ez(AnonymousClass0SJ r2) {
        this.A01 = r2;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
        StringBuilder sb = new StringBuilder("WorkManager-WorkTimer-thread-");
        sb.append(this.A00);
        newThread.setName(sb.toString());
        this.A00++;
        return newThread;
    }
}
