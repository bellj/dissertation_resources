package X;

import com.facebook.flexlayout.layoutoutput.LayoutOutput;

/* renamed from: X.4vI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106184vI implements AbstractC72393eW {
    public final int A00;
    public final int A01;
    public final LayoutOutput A02;
    public final AbstractC65073Ia A03;

    @Override // X.AbstractC72393eW
    public Object ADo() {
        return null;
    }

    @Override // X.AbstractC72393eW
    public int AEr() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEs() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEt() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEu() {
        return 0;
    }

    public /* synthetic */ C106184vI(LayoutOutput layoutOutput, AbstractC65073Ia r2, int i, int i2) {
        this.A02 = layoutOutput;
        this.A03 = r2;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AbstractC72393eW
    public AbstractC72393eW ABK(int i) {
        return (AbstractC72393eW) this.A02.measureResults[i];
    }

    @Override // X.AbstractC72393eW
    public int ABP() {
        return this.A02.measureResults.length;
    }

    @Override // X.AbstractC72393eW
    public int ADK() {
        return this.A00;
    }

    @Override // X.AbstractC72393eW
    public AbstractC65073Ia AG8() {
        return this.A03;
    }

    @Override // X.AbstractC72393eW
    public int AHm() {
        return this.A01;
    }

    @Override // X.AbstractC72393eW
    public int AHs(int i) {
        return (int) this.A02.arr[AnonymousClass49X.values().length + (i * AnonymousClass49W.values().length) + 0];
    }

    @Override // X.AbstractC72393eW
    public int AHt(int i) {
        return (int) this.A02.arr[AnonymousClass49X.values().length + (i * AnonymousClass49W.values().length) + 1];
    }

    @Override // X.AbstractC72393eW
    public int getHeight() {
        return (int) this.A02.arr[1];
    }

    @Override // X.AbstractC72393eW
    public int getWidth() {
        return (int) this.A02.arr[0];
    }
}
