package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.WaEditText;
import com.whatsapp.biz.catalog.view.PostcodeChangeBottomSheet;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;

/* renamed from: X.2oI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58192oI extends AbstractC52172aN {
    public final /* synthetic */ ProductDetailActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58192oI(Context context, ProductDetailActivity productDetailActivity) {
        super(context);
        this.A00 = productDetailActivity;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ProductDetailActivity productDetailActivity = this.A00;
        PostcodeChangeBottomSheet postcodeChangeBottomSheet = new PostcodeChangeBottomSheet(new AnonymousClass3VP(productDetailActivity), true);
        productDetailActivity.A0Y = postcodeChangeBottomSheet;
        String str = (String) productDetailActivity.A0a.A0A.A01();
        postcodeChangeBottomSheet.A08 = str;
        WaEditText waEditText = postcodeChangeBottomSheet.A04;
        if (waEditText != null) {
            waEditText.setText(str);
        }
        C42791vs.A01(productDetailActivity.A0Y, productDetailActivity.A0V());
    }
}
