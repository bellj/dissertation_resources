package X;

import android.text.Editable;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.5o0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123825o0 extends C469928m {
    public final /* synthetic */ PaymentView A00;

    public C123825o0(PaymentView paymentView) {
        this.A00 = paymentView;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        AnonymousClass1Ly r2;
        PaymentView paymentView = this.A00;
        if (paymentView.A17.A00 && (r2 = paymentView.A1A) != null && paymentView.A0i.A02) {
            r2.A00(editable.toString(), 200);
        }
    }
}
