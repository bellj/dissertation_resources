package X;

import java.util.Arrays;

/* renamed from: X.5O5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O5 extends AbstractC112995Fp {
    public int A00;
    public int A01;
    public AnonymousClass5XE A02;
    public boolean A03;
    public byte[] A04;
    public byte[] A05;
    public byte[] A06;
    public byte[] A07;

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = this.A00;
        A01(bArr, bArr2, i, i3, i2);
        return i3;
    }

    public AnonymousClass5O5(AnonymousClass5XE r3, int i) {
        super(r3);
        if (i > (r3.AAt() << 3) || i < 8 || i % 8 != 0) {
            StringBuilder A0k = C12960it.A0k("CFB");
            A0k.append(i);
            throw C12970iu.A0f(C12960it.A0d(" not supported", A0k));
        }
        this.A02 = r3;
        int i2 = i / 8;
        this.A00 = i2;
        int AAt = r3.AAt();
        this.A04 = new byte[AAt];
        this.A06 = new byte[AAt];
        this.A05 = new byte[AAt];
        this.A07 = new byte[i2];
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A02);
        A0h.append("/CFB");
        return C12960it.A0f(A0h, this.A00 << 3);
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r7, boolean z) {
        this.A03 = z;
        if (r7 instanceof C113075Fx) {
            C113075Fx r72 = (C113075Fx) r7;
            byte[] bArr = r72.A01;
            int length = bArr.length;
            byte[] bArr2 = this.A04;
            int length2 = bArr2.length;
            if (length < length2) {
                int i = length2 - length;
                System.arraycopy(bArr, 0, bArr2, i, length);
                for (int i2 = 0; i2 < i; i2++) {
                    bArr2[i2] = 0;
                }
            } else {
                System.arraycopy(bArr, 0, bArr2, 0, length2);
            }
            reset();
            r7 = r72.A00;
        } else {
            reset();
        }
        if (r7 != null) {
            this.A02.AIf(r7, true);
        }
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        byte[] bArr = this.A04;
        System.arraycopy(bArr, 0, this.A06, 0, bArr.length);
        Arrays.fill(this.A07, (byte) 0);
        this.A01 = 0;
        this.A02.reset();
    }
}
