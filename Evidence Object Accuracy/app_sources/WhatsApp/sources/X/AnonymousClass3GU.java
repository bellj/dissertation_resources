package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

/* renamed from: X.3GU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GU {
    public static Context A00;
    public static AnonymousClass5YK A01;

    public static Context A00(Context context) {
        Context context2 = A00;
        if (context2 == null) {
            try {
                context2 = C95564dy.A06(context, C95564dy.A09, "com.google.android.gms.maps_dynamite").A00;
            } catch (Exception e) {
                Log.e("zzca", "Failed to load maps module, use legacy", e);
                try {
                    context2 = context.createPackageContext("com.google.android.gms", 3);
                } catch (PackageManager.NameNotFoundException unused) {
                    context2 = null;
                }
            }
            A00 = context2;
        }
        return context2;
    }

    public static AnonymousClass5YK A01(Context context) {
        AnonymousClass5YK r3;
        C13020j0.A01(context);
        AnonymousClass5YK r0 = A01;
        if (r0 != null) {
            return r0;
        }
        int A002 = C472329r.A00(context, 13400000);
        if (A002 == 0) {
            Log.i("zzca", "Making Creator dynamically");
            ClassLoader classLoader = A00(context).getClassLoader();
            try {
                C13020j0.A01(classLoader);
                Class<?> loadClass = classLoader.loadClass("com.google.android.gms.maps.internal.CreatorImpl");
                try {
                    IBinder iBinder = (IBinder) loadClass.newInstance();
                    if (iBinder == null) {
                        r3 = null;
                    } else {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICreator");
                        if (queryLocalInterface instanceof AnonymousClass5YK) {
                            r3 = (AnonymousClass5YK) queryLocalInterface;
                        } else {
                            r3 = new C79843rI(iBinder);
                        }
                    }
                    A01 = r3;
                    try {
                        BinderC56502l7 r2 = new BinderC56502l7(A00(context).getResources());
                        C65873Li r32 = (C65873Li) r3;
                        Parcel A012 = r32.A01();
                        C65183In.A00(r2, A012);
                        A012.writeInt(12451000);
                        r32.A03(6, A012);
                        return A01;
                    } catch (RemoteException e) {
                        throw new C113245Gt(e);
                    }
                } catch (IllegalAccessException unused) {
                    throw C12960it.A0U(C12960it.A0c(loadClass.getName(), "Unable to call the default constructor of "));
                } catch (InstantiationException unused2) {
                    throw C12960it.A0U(C12960it.A0c(loadClass.getName(), "Unable to instantiate the dynamic class "));
                }
            } catch (ClassNotFoundException unused3) {
                throw C12960it.A0U("Unable to find dynamic class com.google.android.gms.maps.internal.CreatorImpl");
            }
        } else {
            throw new AnonymousClass29w(A002);
        }
    }
}
