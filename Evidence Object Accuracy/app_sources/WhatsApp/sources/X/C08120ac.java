package X;

import java.util.List;

/* renamed from: X.0ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08120ac implements AbstractC12590iA {
    public final AnonymousClass0H9 A00;
    public final AnonymousClass0H9 A01;

    public C08120ac(AnonymousClass0H9 r1, AnonymousClass0H9 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC12590iA
    public AnonymousClass0QR A88() {
        return new AnonymousClass0Gt(new AnonymousClass0H1(this.A00.A00), new AnonymousClass0H1(this.A01.A00));
    }

    @Override // X.AbstractC12590iA
    public List ADl() {
        throw new UnsupportedOperationException("Cannot call getKeyframes on AnimatableSplitDimensionPathValue.");
    }

    @Override // X.AbstractC12590iA
    public boolean AK5() {
        return this.A00.AK5() && this.A01.AK5();
    }
}
