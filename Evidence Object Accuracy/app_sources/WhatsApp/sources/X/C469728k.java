package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.28k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C469728k {
    public Context A00;
    public LayoutInflater A01;
    public View A02;
    public View A03;
    public View A04;
    public ViewGroup A05;
    public ImageView A06;
    public TextView A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public RecyclerView A0B;
    public TextEmojiLabel A0C;
    public TextEmojiLabel A0D;
    public AnonymousClass1J1 A0E;
    public C54362gb A0F;
    public boolean A0G;
    public final C15550nR A0H;
    public final C15610nY A0I;
    public final C14830m7 A0J;
    public final AnonymousClass018 A0K;
    public final C19990v2 A0L;
    public final C20710wC A0M;

    public C469728k(Context context, ViewGroup viewGroup, C15550nR r5, C15610nY r6, AnonymousClass1J1 r7, C14830m7 r8, AnonymousClass018 r9, C19990v2 r10, C20710wC r11) {
        this.A00 = context;
        this.A0J = r8;
        this.A0L = r10;
        this.A01 = LayoutInflater.from(context);
        this.A0H = r5;
        this.A0I = r6;
        this.A0K = r9;
        this.A0M = r11;
        this.A0E = r7;
        this.A07 = (TextView) viewGroup.findViewById(R.id.group_creator);
        this.A0D = (TextEmojiLabel) viewGroup.findViewById(R.id.group_name);
        this.A0C = (TextEmojiLabel) viewGroup.findViewById(R.id.group_description_preview);
        this.A05 = (ViewGroup) viewGroup.findViewById(R.id.participants_no_known_contacts);
        this.A0A = (TextView) viewGroup.findViewById(R.id.participants_header);
        this.A09 = (TextView) viewGroup.findViewById(R.id.participant_count);
        this.A06 = (ImageView) viewGroup.findViewById(R.id.group_photo);
        this.A08 = (TextView) viewGroup.findViewById(R.id.invite_expiration_time);
        this.A04 = viewGroup.findViewById(R.id.group_photo_container);
        this.A03 = viewGroup.findViewById(R.id.group_info);
        this.A02 = viewGroup.findViewById(R.id.background);
        this.A0B = (RecyclerView) viewGroup.findViewById(R.id.group_participants);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.A1Q(0);
        this.A0B.setLayoutManager(linearLayoutManager);
        C54362gb r1 = new C54362gb(this);
        this.A0F = r1;
        this.A0B.setAdapter(r1);
    }

    public void A00(Bitmap bitmap) {
        if (bitmap == null) {
            Log.e("GroupInviteInfoViewController/decode-photo-bytes-returns-null");
            return;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(100);
        alphaAnimation.setAnimationListener(new C58072o6(bitmap, this));
        this.A06.startAnimation(alphaAnimation);
    }

    public void A01(C63373Bi r13, long j) {
        C15370n3 r10;
        String A0I;
        UserJid userJid = r13.A04;
        String str = null;
        if (userJid != null) {
            r10 = this.A0H.A0B(userJid);
        } else {
            r10 = null;
        }
        this.A0D.A0G(null, r13.A07);
        if (r10 == null || !this.A0G) {
            this.A07.setVisibility(8);
        } else {
            TextView textView = this.A07;
            textView.setVisibility(0);
            boolean A0W = this.A0M.A0W(r13.A00);
            int i = R.string.join_group_creator_message;
            if (A0W) {
                i = R.string.join_parent_group_creator_message;
            }
            textView.setText(this.A00.getString(i, this.A0I.A08(r10)));
        }
        AnonymousClass1PD r0 = r13.A05;
        if (r0 != null) {
            str = r0.A02;
        }
        boolean isEmpty = TextUtils.isEmpty(str);
        TextEmojiLabel textEmojiLabel = this.A0C;
        if (!isEmpty) {
            textEmojiLabel.A0G(null, str);
            textEmojiLabel.setVisibility(0);
        } else {
            textEmojiLabel.setVisibility(8);
        }
        List list = r13.A08;
        boolean z = !list.isEmpty();
        TextView textView2 = this.A0A;
        int i2 = 8;
        if (z) {
            i2 = 0;
        }
        textView2.setVisibility(i2);
        ViewGroup viewGroup = this.A05;
        int i3 = 0;
        if (z) {
            i3 = 8;
        }
        viewGroup.setVisibility(i3);
        RecyclerView recyclerView = this.A0B;
        int i4 = 8;
        if (z) {
            i4 = 0;
        }
        recyclerView.setVisibility(i4);
        AnonymousClass018 r4 = this.A0K;
        int i5 = r13.A01;
        long j2 = (long) i5;
        Integer valueOf = Integer.valueOf(i5);
        textView2.setText(r4.A0I(new Object[]{valueOf}, R.plurals.participants_title, j2));
        this.A09.setText(r4.A0I(new Object[]{valueOf}, R.plurals.participants_title, j2));
        C54362gb r02 = this.A0F;
        r02.A01 = list;
        r02.A02();
        r02.A00 = i5;
        r02.A02();
        long A00 = j - this.A0J.A00();
        if (A00 > 0) {
            double d = (double) A00;
            int ceil = (int) Math.ceil(d / 8.64E7d);
            int floor = (int) Math.floor(d / 3600000.0d);
            TextView textView3 = this.A08;
            if (floor < 12) {
                A0I = this.A00.getString(R.string.invite_expiration_at_time, AnonymousClass3JK.A00(r4, j));
            } else {
                A0I = r4.A0I(new Object[]{Integer.valueOf(ceil)}, R.plurals.invite_expires_days, (long) ceil);
            }
            textView3.setText(A0I);
            textView3.setVisibility(0);
        } else {
            this.A08.setVisibility(8);
        }
        View view = this.A02;
        view.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NV(this, (float) view.getHeight()));
        this.A04.setVisibility(0);
    }
}
