package X;

/* renamed from: X.0kO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13820kO extends ActivityC13830kP {
    public boolean A00 = false;

    public AbstractActivityC13820kO() {
        A1J();
    }

    private void A1J() {
        A0R(new C48492Gm(this));
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ActivityC13810kN r1 = (ActivityC13810kN) this;
            AnonymousClass01J r2 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) r1).A05 = (AbstractC14440lR) r2.ANe.get();
            r1.A0C = (C14850m9) r2.A04.get();
            r1.A05 = (C14900mE) r2.A8X.get();
            r1.A03 = (AbstractC15710nm) r2.A4o.get();
            r1.A04 = (C14330lG) r2.A7B.get();
            r1.A0B = (AnonymousClass19M) r2.A6R.get();
            r1.A0A = (C18470sV) r2.AK8.get();
            r1.A06 = (C15450nH) r2.AII.get();
            r1.A08 = (AnonymousClass01d) r2.ALI.get();
            r1.A0D = (C18810t5) r2.AMu.get();
            r1.A09 = (C14820m6) r2.AN3.get();
            r1.A07 = (C18640sm) r2.A3u.get();
        }
    }
}
