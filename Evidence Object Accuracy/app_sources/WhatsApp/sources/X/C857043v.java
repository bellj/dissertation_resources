package X;

/* renamed from: X.43v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C857043v extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;

    public C857043v() {
        super(2938, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A00);
        r3.Abe(8, this.A01);
        r3.Abe(7, this.A02);
        r3.Abe(15, this.A03);
        r3.Abe(14, this.A04);
        r3.Abe(13, this.A05);
        r3.Abe(21, this.A06);
        r3.Abe(20, this.A07);
        r3.Abe(19, this.A08);
        r3.Abe(12, this.A09);
        r3.Abe(11, this.A0A);
        r3.Abe(10, this.A0B);
        r3.Abe(29, this.A0C);
        r3.Abe(30, this.A0D);
        r3.Abe(31, this.A0E);
        r3.Abe(22, this.A0F);
        r3.Abe(23, this.A0G);
        r3.Abe(24, this.A0H);
        r3.Abe(18, this.A0I);
        r3.Abe(17, this.A0J);
        r3.Abe(16, this.A0K);
        r3.Abe(3, this.A0L);
        r3.Abe(2, this.A0M);
        r3.Abe(1, this.A0N);
        r3.Abe(6, this.A0O);
        r3.Abe(5, this.A0P);
        r3.Abe(4, this.A0Q);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPttDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttCancelBroadcast", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttCancelGroup", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttCancelIndividual", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttDraftReviewBroadcast", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttDraftReviewGroup", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttDraftReviewIndividual", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttFastplaybackBroadcast", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttFastplaybackGroup", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttFastplaybackIndividual", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttLockBroadcast", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttLockGroup", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttLockIndividual", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttOutOfChatBroadcast", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttOutOfChatGroup", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttOutOfChatIndividual", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPausedRecordBroadcast", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPausedRecordGroup", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPausedRecordIndividual", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackBroadcast", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackGroup", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackIndividual", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttRecordBroadcast", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttRecordGroup", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttRecordIndividual", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttSendBroadcast", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttSendGroup", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttSendIndividual", this.A0Q);
        return C12960it.A0d("}", A0k);
    }
}
