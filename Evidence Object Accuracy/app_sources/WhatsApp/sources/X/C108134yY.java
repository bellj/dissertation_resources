package X;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4yY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108134yY implements AnonymousClass5SW {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ String A01;

    public C108134yY(String str, Bundle bundle) {
        this.A01 = str;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass5SW
    public final /* synthetic */ Object Ah8(IBinder iBinder) {
        AnonymousClass5Y6 r4;
        if (iBinder == null) {
            r4 = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.auth.IAuthManagerService");
            r4 = queryLocalInterface instanceof AnonymousClass5Y6 ? (AnonymousClass5Y6) queryLocalInterface : new C78893pi(iBinder);
        }
        String str = this.A01;
        Bundle bundle = this.A00;
        C98414ie r42 = (C98414ie) r4;
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(r42.A01);
        obtain.writeString(str);
        C95154dE.A00(obtain, bundle);
        Parcel A00 = r42.A00(2, obtain);
        Bundle bundle2 = (Bundle) C12970iu.A0F(A00, Bundle.CREATOR);
        A00.recycle();
        AnonymousClass3JL.A05(bundle2);
        String string = bundle2.getString("Error");
        if (bundle2.getBoolean("booleanResult")) {
            return null;
        }
        throw new AnonymousClass4CA(string);
    }
}
