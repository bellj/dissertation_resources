package X;

/* renamed from: X.1aP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31351aP {
    public final String A00;
    public final C31601ao A01;

    public C31351aP(String str, C31601ao r2) {
        this.A00 = str;
        this.A01 = r2;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C31351aP)) {
            return false;
        }
        C31351aP r4 = (C31351aP) obj;
        if (!this.A00.equals(r4.A00) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A00.hashCode() ^ this.A01.hashCode();
    }
}
