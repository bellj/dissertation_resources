package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: X.2Yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51812Yx extends BroadcastReceiver {
    public static C51812Yx A02;
    public final Handler A00 = C12970iu.A0E();
    public final ArrayList A01 = C12960it.A0l();

    @Override // android.content.BroadcastReceiver
    public synchronized void onReceive(Context context, Intent intent) {
        if (!isInitialStickyBroadcast()) {
            ArrayList arrayList = this.A01;
            int size = arrayList.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                } else if (((Reference) arrayList.get(size)).get() == null) {
                    arrayList.remove(size);
                }
            }
            for (int i = 0; i < arrayList.size(); i++) {
                C67733Sr r0 = (C67733Sr) ((WeakReference) arrayList.get(i)).get();
                if (r0 != null) {
                    C67733Sr.A01(r0);
                }
            }
        }
    }
}
