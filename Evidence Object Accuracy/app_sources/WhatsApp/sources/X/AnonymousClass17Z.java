package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.17Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17Z {
    public static final Object A0H = new Object();
    public int A00 = 0;
    public C50932Rx A01;
    public final C14900mE A02;
    public final C15550nR A03;
    public final C20730wE A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final AnonymousClass102 A08;
    public final C14850m9 A09;
    public final AnonymousClass1I3 A0A;
    public final C18650sn A0B;
    public final C18600si A0C;
    public final C18610sj A0D;
    public final C17070qD A0E;
    public final C50922Rw A0F;
    public final AbstractC14440lR A0G;

    public AnonymousClass17Z(C14900mE r4, C15550nR r5, C20730wE r6, C14830m7 r7, C16590pI r8, AnonymousClass018 r9, AnonymousClass102 r10, C14850m9 r11, C18650sn r12, C18600si r13, C18610sj r14, C17070qD r15, AbstractC14440lR r16) {
        this.A05 = r7;
        this.A09 = r11;
        this.A02 = r4;
        this.A06 = r8;
        this.A0G = r16;
        this.A07 = r9;
        this.A03 = r5;
        this.A0E = r15;
        this.A04 = r6;
        this.A0C = r13;
        this.A0D = r14;
        this.A08 = r10;
        this.A0B = r12;
        this.A0F = new C50922Rw(r7, r13);
        this.A0A = new AnonymousClass1I3();
        this.A0F.A00();
        String string = this.A0C.A01().getString("payment_incentive_user_claim_info", null);
        if (!TextUtils.isEmpty(string)) {
            try {
                synchronized (A0H) {
                    this.A01 = new C50932Rx(string);
                }
            } catch (JSONException unused) {
                A03();
            }
        }
    }

    public AnonymousClass2S0 A00() {
        C50932Rx r0;
        C50932Rx r1;
        C50942Ry A02 = A02();
        Object obj = A0H;
        synchronized (obj) {
            r0 = this.A01;
        }
        if (A02 == null || !(r0 == null || r0.A03 == A02.A08.A01)) {
            A03();
        }
        synchronized (obj) {
            r1 = this.A01;
        }
        return new AnonymousClass2S0(A02, r1);
    }

    public final C50952Rz A01() {
        String A03 = this.A09.A03(782);
        if (A03 == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(A03);
        return new C50952Rz(jSONObject.getInt("update_count"), jSONObject.getLong("offer_id"));
    }

    public C50942Ry A02() {
        try {
            C50952Rz A01 = A01();
            if (A01 == null || A01.A00 <= 0) {
                return null;
            }
            C50922Rw r3 = this.A0F;
            long j = A01.A01;
            ConcurrentHashMap concurrentHashMap = r3.A02;
            Long valueOf = Long.valueOf(j);
            C50942Ry r0 = (C50942Ry) concurrentHashMap.get(valueOf);
            if (r0 != null) {
                return r0;
            }
            concurrentHashMap.clear();
            r3.A00();
            return (C50942Ry) concurrentHashMap.get(valueOf);
        } catch (JSONException unused) {
            return null;
        }
    }

    public void A03() {
        synchronized (A0H) {
            this.A01 = null;
            C18600si r2 = this.A0C;
            if (!TextUtils.isEmpty(r2.A01().getString("payment_incentive_user_claim_info", null))) {
                r2.A01().edit().putString("payment_incentive_user_claim_info", null).apply();
            }
        }
    }

    public final void A04() {
        C50922Rw r5 = this.A0F;
        Iterator it = r5.A02.entrySet().iterator();
        while (it.hasNext()) {
            if (((C50942Ry) ((Map.Entry) it.next()).getValue()).A05 + TimeUnit.DAYS.toSeconds((long) 14) < TimeUnit.MILLISECONDS.toSeconds(r5.A00.A00())) {
                it.remove();
            }
        }
        r5.A01();
    }

    public void A05(int i, int i2) {
        try {
            C50942Ry A02 = A02();
            C50952Rz A01 = A01();
            if (A02 != null && A01 != null) {
                if (i >= 0) {
                    A02.A00 = i;
                }
                if (i2 >= 0) {
                    A02.A01 = i2;
                }
                this.A0F.A02(A02, A01.A01);
            }
        } catch (Exception e) {
            Log.e("PAY: PaymentIncentiveManager/processUiOfferDetails : Error while parsing ", e);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 3, insn: 0x00c0: INVOKE  (r3 I:X.17Z) type: VIRTUAL call: X.17Z.A04():void, block:B:25:0x00bb
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public void A06(
/*
[208] Method generation error in method: X.17Z.A06(X.2S2, boolean):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r18v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public void A07(AnonymousClass2S1 r13, long j) {
        AnonymousClass2S3 r5 = new AnonymousClass2S3(this.A02, this.A06, this.A0B, this.A0D);
        r5.A03.A0F(new AnonymousClass2S9(r5.A01.A00, r5.A00, r5.A02, new AnonymousClass2S8(r13, this, j), r5), new AnonymousClass1V8("account", new AnonymousClass1W9[]{new AnonymousClass1W9("action", "get-offer-eligibility"), new AnonymousClass1W9("offer_id", j)}), "get", C26061Bw.A0L);
    }

    public void A08(AnonymousClass2S1 r13, AnonymousClass1V8 r14, long j) {
        try {
            C50932Rx r6 = new C50932Rx(r14, j, this.A05.A00());
            synchronized (A0H) {
                C50942Ry r3 = (C50942Ry) this.A0F.A02.get(Long.valueOf(j));
                if (r3 != null && r3.A01 > 0) {
                    int i = 0;
                    if (r6.A00 + r6.A01 >= r3.A03) {
                        i = 1;
                    }
                    r3.A01 = i;
                }
                this.A01 = r6;
                C18600si r32 = this.A0C;
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("offer_id", r6.A03);
                jSONObject.put("is_eligible", r6.A04);
                jSONObject.put("pending_count", r6.A00);
                jSONObject.put("redeemed_count", r6.A01);
                jSONObject.put("last_sync_time_ms", r6.A02);
                r32.A01().edit().putString("payment_incentive_user_claim_info", jSONObject.toString()).apply();
            }
            if (r13 != null) {
                r13.AX0(r6);
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("processSuccessfulGetClaimStatus: Error while parsing: ");
            sb.append(e);
            Log.e(sb.toString());
            A03();
            if (r13 != null) {
                r13.APk();
            }
        }
    }

    public synchronized void A09(boolean z) {
        if (this.A00 == 1) {
            Log.i("PAY: PaymentIncentiveManager/syncIncentiveData iq requests have yet to be fulfilled, aborting this iq call");
        } else {
            this.A00 = 1;
            A06(new AnonymousClass2S2(new AnonymousClass2SB(this), this, z), false);
        }
    }

    public boolean A0A() {
        C50942Ry r1;
        C50932Rx r0;
        try {
            C50952Rz A01 = A01();
            if (A01 == null) {
                return false;
            }
            long j = A01.A01;
            if (j == 0 || (r1 = (C50942Ry) this.A0F.A02.get(Long.valueOf(j))) == null || r1.A04 != 0 || (r0 = this.A01) == null || !r0.A04) {
                return false;
            }
            return r1.A0A.A01;
        } catch (JSONException e) {
            Log.e("PAY: PaymentIncentiveManager/shouldSeedNewOffer : Error while fetching offer ID from ABProps ", e);
            e.printStackTrace();
            return false;
        }
    }
}
