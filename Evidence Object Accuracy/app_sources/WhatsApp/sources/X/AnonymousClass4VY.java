package X;

/* renamed from: X.4VY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VY {
    public final AnonymousClass4AD A00;
    public final Integer A01;
    public final Integer A02;

    public AnonymousClass4VY(AnonymousClass4AD r1, Integer num, Integer num2) {
        this.A01 = num;
        this.A02 = num2;
        this.A00 = r1;
    }

    public String toString() {
        String obj;
        StringBuilder A0k = C12960it.A0k("[");
        Integer num = this.A01;
        String str = "";
        if (num == null) {
            obj = str;
        } else {
            obj = num.toString();
        }
        A0k.append(obj);
        A0k.append(":");
        Integer num2 = this.A02;
        if (num2 != null) {
            str = num2.toString();
        }
        A0k.append(str);
        return C12960it.A0d("]", A0k);
    }
}
