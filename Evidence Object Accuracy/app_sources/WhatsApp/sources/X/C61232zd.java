package X;

/* renamed from: X.2zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61232zd extends AbstractCallableC112595Dz {
    public final C15570nT A00;
    public final C15550nR A01;
    public final AbstractC15340mz A02;

    public C61232zd(C15570nT r1, C15550nR r2, AbstractC15340mz r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractCallableC112595Dz
    public /* bridge */ /* synthetic */ Object A01() {
        C15370n3 A0B;
        C15550nR r1 = this.A01;
        AbstractC15340mz r3 = this.A02;
        C15370n3 A00 = C15550nR.A00(r1, r3.A0z.A00);
        super.A00.A02();
        AbstractC14640lm A002 = AnonymousClass3J0.A00(this.A00, A00, r3);
        if (A002 == null) {
            A0B = null;
        } else {
            A0B = r1.A0B(A002);
        }
        return new AnonymousClass4Q9(A00, A0B, r3);
    }
}
