package X;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import com.facebook.animated.gif.GifImage;
import com.facebook.common.time.RealtimeSinceBootClock;
import com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl;
import com.whatsapp.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38911ou implements Closeable {
    public static final C92764Xi A04;
    public static final C92764Xi A05;
    public static final ExecutorService A06 = Executors.newSingleThreadExecutor();
    public static final AtomicBoolean A07 = new AtomicBoolean(false);
    public final ParcelFileDescriptor A00;
    public final GifImage A01;
    public final AnonymousClass3FZ A02;
    public final C75873kb A03;

    static {
        C90654Os r1 = new C90654Os();
        r1.A00 = 4096;
        r1.A02 = true;
        A05 = new C92764Xi(r1);
        C90654Os r12 = new C90654Os();
        r12.A00 = 4096;
        A04 = new C92764Xi(r12);
    }

    public C38911ou(ParcelFileDescriptor parcelFileDescriptor, GifImage gifImage, C75873kb r9) {
        this.A00 = parcelFileDescriptor;
        this.A03 = r9;
        this.A01 = gifImage;
        C87754Cu r5 = new C87754Cu();
        this.A02 = new AnonymousClass3FZ(new AnonymousClass3HI(new Rect(0, 0, gifImage.getWidth(), gifImage.getHeight()), new C88924Hz(gifImage), r5, false), new C106044v2(this));
    }

    public static C38911ou A00(ParcelFileDescriptor parcelFileDescriptor, boolean z) {
        C75873kb r2;
        Throwable e;
        C92764Xi r22;
        GifImage nativeCreateFromFileDescriptor;
        AtomicBoolean atomicBoolean = A07;
        if (!atomicBoolean.get()) {
            ExecutorService executorService = A06;
            if (!executorService.isShutdown()) {
                try {
                    atomicBoolean.compareAndSet(false, ((Boolean) executorService.submit(new Callable() { // from class: X.5DU
                        @Override // java.util.concurrent.Callable
                        public final Object call() {
                            AnonymousClass1QL.A00("c++_shared");
                            AnonymousClass1QL.A00("gifimage");
                            return Boolean.TRUE;
                        }
                    }).get()).booleanValue());
                    executorService.shutdown();
                } catch (InterruptedException | ExecutionException e2) {
                    throw new IOException("Failed to initialize Fresco", e2);
                } catch (RejectedExecutionException unused) {
                }
            }
        }
        if (atomicBoolean.get()) {
            try {
                int fd = parcelFileDescriptor.getFd();
                if (z) {
                    r22 = A05;
                } else {
                    r22 = A04;
                }
                synchronized (GifImage.class) {
                    if (!GifImage.sInitialized) {
                        GifImage.sInitialized = true;
                        AnonymousClass1QL.A00("c++_shared");
                        AnonymousClass1QL.A00("gifimage");
                    }
                }
                nativeCreateFromFileDescriptor = GifImage.nativeCreateFromFileDescriptor(fd, r22.A00, r22.A03);
                try {
                    r2 = new C75873kb(new C88924Hz(nativeCreateFromFileDescriptor));
                } catch (IllegalArgumentException | IllegalStateException e3) {
                    e = e3;
                    r2 = null;
                }
            } catch (IllegalArgumentException | IllegalStateException e4) {
                e = e4;
                r2 = null;
            }
            try {
                return new C38911ou(parcelFileDescriptor, nativeCreateFromFileDescriptor, r2);
            } catch (IllegalArgumentException | IllegalStateException e5) {
                e = e5;
                if (nativeCreateFromFileDescriptor != null) {
                    nativeCreateFromFileDescriptor.dispose();
                }
                AnonymousClass1P1.A03(r2);
                if (parcelFileDescriptor != null) {
                    try {
                        parcelFileDescriptor.close();
                    } catch (Throwable th) {
                        Log.e(th);
                    }
                }
                throw new IOException(e);
            }
        } else {
            throw new IOException("Fresco failed to initialize");
        }
    }

    public static C38921ov A01(ContentResolver contentResolver, Uri uri, C15690nk r4) {
        if (contentResolver != null) {
            r4.A01(uri);
            try {
                ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(uri, "r");
                if (openFileDescriptor != null) {
                    r4.A02(openFileDescriptor);
                    C38921ov A02 = A02(openFileDescriptor);
                    openFileDescriptor.close();
                    return A02;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("gifdecoder/getmetadata/cannot open uri, pfd=null, uri=");
                sb.append(uri);
                throw new IOException(sb.toString());
            } catch (SecurityException e) {
                StringBuilder sb2 = new StringBuilder("gifdecoder/getmetadata/failed to read uri ");
                sb2.append(uri);
                Log.e(sb2.toString(), e);
                throw new IOException(e);
            }
        } else {
            throw new IOException("gifdecoder/getmetadata/cannot open uri, cr=null");
        }
    }

    public static C38921ov A02(ParcelFileDescriptor parcelFileDescriptor) {
        C38911ou A00 = A00(parcelFileDescriptor, true);
        try {
            GifImage gifImage = A00.A01;
            C38921ov r0 = new C38921ov(gifImage.getWidth(), gifImage.getHeight(), gifImage.isAnimated());
            A00.close();
            return r0;
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static C38921ov A03(File file) {
        ParcelFileDescriptor open = ParcelFileDescriptor.open(file, 268435456);
        try {
            C38921ov A02 = A02(open);
            if (open != null) {
                open.close();
            }
            return A02;
        } catch (Throwable th) {
            if (open != null) {
                try {
                    open.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }

    public Bitmap A04(int i) {
        boolean z = true;
        boolean z2 = false;
        if (i >= 0) {
            z2 = true;
        }
        AnonymousClass009.A0E(z2);
        GifImage gifImage = this.A01;
        if (i >= gifImage.getFrameCount()) {
            z = false;
        }
        AnonymousClass009.A0E(z);
        Bitmap createBitmap = Bitmap.createBitmap(gifImage.getWidth(), gifImage.getHeight(), Bitmap.Config.ARGB_8888);
        this.A02.A00(i, createBitmap);
        return createBitmap;
    }

    public AnonymousClass2Zb A05(Context context) {
        boolean z;
        C88924Hz r5;
        AnonymousClass5XK r9;
        AnonymousClass4SI r10;
        synchronized (C91834Ti.class) {
            z = false;
            if (C91834Ti.A07 != null) {
                z = true;
            }
        }
        if (!z) {
            AnonymousClass4SK r1 = new AnonymousClass4SK(context.getApplicationContext());
            r1.A02 = 1;
            AnonymousClass4UG r3 = new AnonymousClass4UG(r1);
            synchronized (C91834Ti.class) {
                if (C91834Ti.A07 != null) {
                    AbstractC12710iN r12 = AnonymousClass0UN.A00;
                    if (r12.AJi(5)) {
                        r12.Ag0(C91834Ti.class.getSimpleName(), "ImagePipelineFactory has already been initialized! `ImagePipelineFactory.initialize(...)` should only be called once to avoid unexpected behavior.");
                    }
                }
                C91834Ti.A07 = new C91834Ti(r3);
            }
            C87674Cl.A00 = false;
        }
        C91834Ti r6 = C91834Ti.A07;
        if (r6 != null) {
            AnimatedFactoryV2Impl animatedFactoryV2Impl = r6.A00;
            if (animatedFactoryV2Impl == null) {
                AnonymousClass4UM r7 = r6.A01;
                if (r7 == null) {
                    AnonymousClass4XR r72 = r6.A05.A0D;
                    AbstractC94334bd r0 = r6.A03;
                    if (r0 == null) {
                        int i = Build.VERSION.SDK_INT;
                        if (i >= 26) {
                            int i2 = r72.A08.A03.A00;
                            r0 = new C75943kk(new AnonymousClass0DQ(i2), r72.A00(), i2);
                        } else if (i >= 21 || !C87674Cl.A00) {
                            int i3 = r72.A08.A03.A00;
                            r0 = new C75933kj(new AnonymousClass0DQ(i3), r72.A00(), i3);
                        } else {
                            try {
                                Constructor<?> constructor = Class.forName("com.facebook.imagepipeline.platform.KitKatPurgeableDecoder").getConstructor(C89774Li.class);
                                Object[] objArr = new Object[1];
                                C89774Li r32 = r72.A04;
                                if (r32 == null) {
                                    C91954Tw r02 = r72.A08;
                                    r32 = new C89774Li(r02.A01, r02.A03);
                                    r72.A04 = r32;
                                }
                                objArr[0] = r32;
                                r0 = (AbstractC94334bd) constructor.newInstance(objArr);
                            } catch (ClassNotFoundException e) {
                                throw new RuntimeException("Wrong Native code setup, reflection failed.", e);
                            } catch (IllegalAccessException e2) {
                                throw new RuntimeException("Wrong Native code setup, reflection failed.", e2);
                            } catch (InstantiationException e3) {
                                throw new RuntimeException("Wrong Native code setup, reflection failed.", e3);
                            } catch (NoSuchMethodException e4) {
                                throw new RuntimeException("Wrong Native code setup, reflection failed.", e4);
                            } catch (InvocationTargetException e5) {
                                throw new RuntimeException("Wrong Native code setup, reflection failed.", e5);
                            }
                        }
                        r6.A03 = r0;
                    }
                    AnonymousClass4I1 r8 = r6.A04;
                    if (Build.VERSION.SDK_INT >= 21) {
                        r7 = new C75833kX(r8, r72.A00());
                    } else {
                        int i4 = !C87674Cl.A00 ? 1 : 0;
                        AbstractC11590gX r2 = r72.A01;
                        if (r2 == null) {
                            AbstractC75883ke A01 = r72.A01(i4);
                            StringBuilder sb = new StringBuilder("failed to get pool for chunk type: ");
                            sb.append(i4);
                            String obj = sb.toString();
                            if (A01 != null) {
                                AbstractC75883ke A012 = r72.A01(i4);
                                C04660Mo r13 = r72.A02;
                                if (r13 == null) {
                                    AbstractC12890ij r4 = r72.A00;
                                    if (r4 == null) {
                                        C91954Tw r14 = r72.A08;
                                        r4 = new C75923ki(r14.A01, r14.A05, r14.A08);
                                        r72.A00 = r4;
                                    }
                                    r13 = new C04660Mo(r4);
                                    r72.A02 = r13;
                                }
                                r2 = new C105944us(r13, A012);
                                r72.A01 = r2;
                            } else {
                                throw new NullPointerException(String.valueOf(obj));
                            }
                        }
                        r7 = new C75843kY(new C93234Zq(r2), r8, r0);
                    }
                    r6.A01 = r7;
                }
                AnonymousClass4UG r03 = r6.A05;
                AnonymousClass5Pk r92 = r03.A0A;
                C105914up r82 = r6.A02;
                if (r82 == null) {
                    r82 = new C105914up(r03.A03, r03.A06, new C106054v3());
                    r6.A02 = r82;
                }
                if (!C87714Cp.A01) {
                    try {
                        C87714Cp.A00 = (AnimatedFactoryV2Impl) AnimatedFactoryV2Impl.class.getConstructor(AnonymousClass4UM.class, AnonymousClass5Pk.class, C105914up.class, Boolean.TYPE).newInstance(r7, r92, r82, false);
                    } catch (Throwable unused) {
                    }
                    if (C87714Cp.A00 != null) {
                        C87714Cp.A01 = true;
                    }
                }
                animatedFactoryV2Impl = C87714Cp.A00;
                r6.A00 = animatedFactoryV2Impl;
                if (animatedFactoryV2Impl == null) {
                    throw new IOException("Failed to create gif drawable, no drawable factory");
                }
            }
            AbstractC11600gY r62 = animatedFactoryV2Impl.A02;
            C106084v6 r63 = r62;
            if (r62 == null) {
                C105874ul r73 = new C105874ul(animatedFactoryV2Impl);
                AnonymousClass0IZ r132 = new AnonymousClass0IZ(((C106074v5) animatedFactoryV2Impl.A05).A01);
                C105884um r83 = new C105884um(animatedFactoryV2Impl);
                C88914Hy r102 = animatedFactoryV2Impl.A00;
                if (r102 == null) {
                    r102 = new C88914Hy(animatedFactoryV2Impl);
                    animatedFactoryV2Impl.A00 = r102;
                }
                ScheduledExecutorServiceC10940fS r142 = ScheduledExecutorServiceC10940fS.A01;
                if (r142 == null) {
                    r142 = new ScheduledExecutorServiceC10940fS();
                    ScheduledExecutorServiceC10940fS.A01 = r142;
                }
                C106084v6 r64 = new C106084v6(r73, r83, RealtimeSinceBootClock.A00, r102, animatedFactoryV2Impl.A03, animatedFactoryV2Impl.A04, r132, r142);
                animatedFactoryV2Impl.A02 = r64;
                r63 = r64;
            }
            C75873kb r15 = this.A03;
            C106084v6 r65 = (C106084v6) r63;
            synchronized (r15) {
                r5 = r15.A00;
            }
            AbstractC39021p8 r04 = r5.A00;
            Rect rect = new Rect(0, 0, r04.getWidth(), r04.getHeight());
            AnimatedFactoryV2Impl animatedFactoryV2Impl2 = r65.A03.A00;
            C87754Cu r16 = animatedFactoryV2Impl2.A01;
            if (r16 == null) {
                r16 = new C87754Cu();
                animatedFactoryV2Impl2.A01 = r16;
            }
            AnonymousClass3HI r33 = new AnonymousClass3HI(rect, r5, r16, animatedFactoryV2Impl2.A06);
            int intValue = ((Number) r65.A00.get()).intValue();
            if (intValue == 1) {
                r5.hashCode();
                r9 = new C106024v0(new AnonymousClass4Vh(new C105854uj(), r65.A05), true);
            } else if (intValue == 2) {
                r5.hashCode();
                r9 = new C106024v0(new AnonymousClass4Vh(new C105854uj(), r65.A05), false);
            } else if (intValue != 3) {
                r9 = new C106004uy();
            } else {
                r9 = new C106014uz();
            }
            C63693Co r122 = new C63693Co(r9, r33);
            int intValue2 = ((Number) r65.A01.get()).intValue();
            C93144Zh r11 = null;
            if (intValue2 > 0) {
                r11 = new C93144Zh(intValue2);
                r10 = new AnonymousClass4SI(Bitmap.Config.ARGB_8888, r122, r65.A04, r65.A06);
            } else {
                r10 = null;
            }
            AnonymousClass3SN r74 = new AnonymousClass3SN(new C105994ux(r33), r9, r10, r11, r122, r65.A04);
            return new AnonymousClass2Zb(new AnonymousClass3SM(r65.A02, r74, r74, r65.A07));
        }
        throw new NullPointerException(String.valueOf("ImagePipelineFactory was not initialized!"));
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.dispose();
        AnonymousClass1P1.A03(this.A03);
        ParcelFileDescriptor parcelFileDescriptor = this.A00;
        if (parcelFileDescriptor != null) {
            try {
                parcelFileDescriptor.close();
            } catch (Throwable th) {
                Log.e(th);
            }
        }
    }
}
