package X;

import android.graphics.RectF;
import org.json.JSONObject;

/* renamed from: X.33G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33G extends AbstractC454821u {
    public AnonymousClass33G() {
        C12990iw.A13(this.A01);
    }

    public AnonymousClass33G(JSONObject jSONObject) {
        this();
        super.A0A(jSONObject);
    }

    @Override // X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        AbstractC454821u.A01(this, f3, f, f4, f2);
    }
}
