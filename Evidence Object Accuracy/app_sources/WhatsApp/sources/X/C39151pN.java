package X;

import android.os.PowerManager;

/* renamed from: X.1pN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39151pN {
    public static PowerManager.WakeLock A00(PowerManager powerManager, String str, int i) {
        StringBuilder sb = new StringBuilder("com.whatsapp");
        sb.append(":");
        sb.append(str);
        return powerManager.newWakeLock(i, sb.toString());
    }
}
