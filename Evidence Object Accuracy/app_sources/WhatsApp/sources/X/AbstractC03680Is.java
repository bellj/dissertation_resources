package X;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.0Is  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC03680Is extends OutputStream {
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            AnonymousClass0LX.A00(e);
            throw new RuntimeException(e);
        }
    }
}
