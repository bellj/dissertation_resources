package X;

/* renamed from: X.3bU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C70563bU implements AbstractC14590lg {
    public final /* synthetic */ String A00;

    public /* synthetic */ C70563bU(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        String str = this.A00;
        AnonymousClass1EV r8 = (AnonymousClass1EV) obj;
        C91774Tb r5 = (C91774Tb) r8.A03.get(str);
        if (r5 != null && r5.A05 == 0) {
            r5.A05 = System.currentTimeMillis();
            r5.A01 = true;
            AnonymousClass1Q5 r2 = r8.A02;
            int hashCode = str.hashCode();
            r2.A01(hashCode, "iq_send");
            r2.A02(hashCode, "iq_processing");
        }
    }
}
