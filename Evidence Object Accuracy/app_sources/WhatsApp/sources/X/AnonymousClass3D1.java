package X;

/* renamed from: X.3D1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D1 {
    public double A00 = Double.NaN;
    public double A01 = Double.NaN;
    public long A02 = 0;
    public final C18280sC A03;

    public AnonymousClass3D1(C18280sC r3) {
        this.A03 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r4 == Integer.MIN_VALUE) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public double A00() {
        /*
            r5 = this;
            X.0sC r0 = r5.A03
            X.1cA r0 = r0.A00
            double r2 = r0.A00()
            int r4 = r0.A02
            if (r4 == 0) goto L_0x0011
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = 1
            if (r4 != r1) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            java.lang.String r4 = java.lang.Boolean.toString(r0)
            java.lang.String r0 = "voip/call/battery_monitor percent = "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r2)
            java.lang.String r0 = ", charging = "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r4, r1)
            com.whatsapp.util.Log.i(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3D1.A00():double");
    }
}
