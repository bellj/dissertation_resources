package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0FD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FD extends AbstractC05270Ox {
    public boolean A00 = false;
    public final /* synthetic */ AnonymousClass0FB A01;

    public AnonymousClass0FD(AnonymousClass0FB r2) {
        this.A01 = r2;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        if (i == 0 && this.A00) {
            this.A00 = false;
            this.A01.A03();
        }
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i != 0 || i2 != 0) {
            this.A00 = true;
        }
    }
}
