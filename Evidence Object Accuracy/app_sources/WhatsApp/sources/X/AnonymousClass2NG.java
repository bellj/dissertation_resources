package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2NG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NG extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;
    public final /* synthetic */ byte[] A01;

    public AnonymousClass2NG(AnonymousClass2L8 r1, byte[] bArr) {
        this.A00 = r1;
        this.A01 = bArr;
    }

    @Override // X.AbstractC35941j2
    public void A01(AnonymousClass1V8 r10) {
        int A00 = C41151sz.A00(r10);
        if (A00 == 207) {
            for (AnonymousClass1V8 r0 : r10.A0J("error")) {
                for (AnonymousClass1V8 r3 : r0.A0J("error")) {
                    String A0I = r3.A0I("code", null);
                    String A0I2 = r3.A0I("text", null);
                    if (A0I != null) {
                        int A002 = C28421Nd.A00(A0I, 0);
                        C450720b r1 = this.A00.A0H;
                        Log.i("xmpp/reader/on-set-pre-key-error");
                        AbstractC450820c r4 = r1.A00;
                        Bundle bundle = new Bundle();
                        bundle.putInt("errorCode", A002);
                        bundle.putString("errorText", A0I2);
                        r4.AYY(Message.obtain(null, 0, 196, 0, bundle));
                    }
                }
            }
            return;
        }
        C450720b r12 = this.A00.A0H;
        Log.i("xmpp/reader/on-set-pre-key-error");
        AbstractC450820c r42 = r12.A00;
        Bundle bundle2 = new Bundle();
        bundle2.putInt("errorCode", A00);
        r42.AYY(Message.obtain(null, 0, 78, 0, bundle2));
    }
}
