package X;

import java.util.Arrays;

/* renamed from: X.3BH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BH {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3BH(C63603Cf r6, AnonymousClass3CT r7) {
        C41141sy A00 = C41141sy.A00();
        C41141sy.A01(A00, "xmlns", "pay_v2");
        C41141sy r3 = new C41141sy("fds");
        r3.A07(r6.A00, C12960it.A0l());
        r6.A00(r3, Arrays.asList(new String[0]));
        A00.A05(r3.A03());
        A00.A07(r7.A00, C12960it.A0l());
        r7.A00(A00, Arrays.asList(new String[0]));
        this.A00 = A00.A03();
    }
}
