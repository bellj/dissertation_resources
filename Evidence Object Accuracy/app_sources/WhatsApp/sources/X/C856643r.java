package X;

/* renamed from: X.43r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856643r extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;

    public C856643r() {
        super(1914, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(10, this.A04);
        r3.Abe(12, this.A05);
        r3.Abe(5, this.A06);
        r3.Abe(9, this.A07);
        r3.Abe(11, this.A08);
        r3.Abe(4, this.A09);
        r3.Abe(8, this.A0A);
        r3.Abe(7, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A0B);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidMediaFileScanEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "analyzeT", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesAnalyzed", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesMerged", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bytesRelinked", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filesAnalyzed", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filesMerged", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filesRelinked", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filesScanned", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "filesToAnalyze", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "origin", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "scanT", this.A0B);
        return C12960it.A0d("}", A0k);
    }
}
