package X;

/* renamed from: X.2M2  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass2M2 {
    A03(1),
    A04(2),
    A01(3),
    A02(0);
    
    public final int value;

    AnonymousClass2M2(int i) {
        this.value = i;
    }
}
