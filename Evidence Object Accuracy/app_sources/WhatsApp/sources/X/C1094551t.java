package X;

/* renamed from: X.51t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094551t implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r3, AbstractC94534c0 r4, AnonymousClass4RG r5) {
        boolean z = r3 instanceof C82953wS;
        if (!z && !(r4 instanceof C82953wS)) {
            throw new AnonymousClass5H8("Failed to evaluate exists expression");
        } else if (!z) {
            throw C82843wH.A00("Expected boolean node");
        } else {
            boolean booleanValue = ((C82953wS) r3).A00.booleanValue();
            if (!(r4 instanceof C82953wS)) {
                throw C82843wH.A00("Expected boolean node");
            }
            return C12960it.A1V(booleanValue ? 1 : 0, ((C82953wS) r4).A00.booleanValue() ? 1 : 0);
        }
    }
}
