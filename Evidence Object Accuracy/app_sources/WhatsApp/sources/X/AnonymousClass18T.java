package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.whatsapp.R;

/* renamed from: X.18T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18T implements AnonymousClass12Q {
    public final AnonymousClass12P A00;
    public final C14900mE A01;
    public final AnonymousClass18U A02;
    public final AnonymousClass018 A03;
    public final C21860y6 A04;
    public final C17900ra A05;
    public final C17070qD A06;
    public final C30931Zj A07 = C30931Zj.A00("PaymentActivityLauncher", "infra", "COMMON");

    @Override // X.AnonymousClass12Q
    public void AbA(Context context, Uri uri, int i) {
    }

    @Override // X.AnonymousClass12Q
    public void AbB(Context context, Uri uri, int i, int i2) {
    }

    public AnonymousClass18T(AnonymousClass12P r4, C14900mE r5, AnonymousClass18U r6, AnonymousClass018 r7, C21860y6 r8, C17900ra r9, C17070qD r10) {
        this.A01 = r5;
        this.A02 = r6;
        this.A00 = r4;
        this.A03 = r7;
        this.A06 = r10;
        this.A04 = r8;
        this.A05 = r9;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        if (r4.A06.A0E.A00.A05(X.AbstractC15460nI.A0u) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.Intent A00(android.content.Context r5, boolean r6, boolean r7) {
        /*
            r4 = this;
            r2 = 1
            if (r7 != 0) goto L_0x0015
            if (r6 != 0) goto L_0x0014
            X.0qD r0 = r4.A06
            X.0zW r0 = r0.A0E
            X.0nH r1 = r0.A00
            X.0nJ r0 = X.AbstractC15460nI.A0u
            boolean r0 = r1.A05(r0)
            r6 = 0
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            r6 = 1
        L_0x0015:
            java.lang.String r3 = "extra_setup_mode"
            X.0y6 r1 = r4.A04
            boolean r0 = r1.A09()
            if (r6 != 0) goto L_0x004a
            if (r0 != 0) goto L_0x006f
            boolean r0 = r1.A0C()
            if (r0 != 0) goto L_0x006f
            X.0qD r1 = r4.A06
            X.0pp r0 = r1.A02()
            boolean r0 = r0.A71()
            if (r0 != 0) goto L_0x006f
            X.0pp r0 = r1.A02()
            java.lang.Class r0 = r0.AAW()
            android.content.Intent r1 = new android.content.Intent
            r1.<init>(r5, r0)
            r0 = 2
            r1.putExtra(r3, r0)
        L_0x0044:
            java.lang.String r0 = "extra_is_pay_money_only"
            r1.putExtra(r0, r6)
            return r1
        L_0x004a:
            if (r0 != 0) goto L_0x006f
            boolean r0 = r1.A0B()
            if (r0 != 0) goto L_0x006f
            X.0qD r1 = r4.A06
            X.0pp r0 = r1.A02()
            boolean r0 = r0.A71()
            if (r0 != 0) goto L_0x006f
            X.0pp r0 = r1.A02()
            java.lang.Class r0 = r0.AAW()
            android.content.Intent r1 = new android.content.Intent
            r1.<init>(r5, r0)
            r1.putExtra(r3, r2)
            goto L_0x0044
        L_0x006f:
            X.0qD r0 = r4.A06
            X.0pp r0 = r0.A02()
            java.lang.Class r0 = r0.AGb()
            android.content.Intent r1 = new android.content.Intent
            r1.<init>(r5, r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18T.A00(android.content.Context, boolean, boolean):android.content.Intent");
    }

    @Override // X.AnonymousClass12Q
    public void Ab9(Context context, Uri uri) {
        if (uri == null) {
            this.A07.A05("start-activity/uri-is-null");
            return;
        }
        AnonymousClass1ZR r4 = new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, uri.getLastPathSegment(), "paymentHandle");
        if ("wapay".equals(uri.getScheme()) && !AnonymousClass1ZS.A02(r4)) {
            Intent A00 = A00(context, false, true);
            A00.putExtra("extra_payment_handle", r4);
            A00.putExtra("verify-vpa-in-background", true);
            this.A00.A06(context, A00);
        } else if ("upi".equals(uri.getScheme())) {
            this.A02.Ab9(context, uri);
        } else {
            this.A07.A05("start-activity/uri-is-not-wapay-compatible");
            this.A01.A07(R.string.activity_not_found, 0);
        }
    }
}
