package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Uw  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Uw implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ C08790br A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ C14220l3 A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public AnonymousClass0Uw(C08790br r1, C14230l4 r2, C14220l3 r3, AbstractC14200l1 r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        AbstractC14200l1 r2 = this.A03;
        C14250l6.A00(this.A01, this.A02, r2);
    }
}
