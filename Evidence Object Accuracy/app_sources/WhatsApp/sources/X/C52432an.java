package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.2an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52432an extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(58);
    public final float A00;
    public final int A01;

    public /* synthetic */ C52432an(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readInt();
        this.A00 = parcel.readFloat();
    }

    public /* synthetic */ C52432an(Parcelable parcelable, float f, int i) {
        super(parcelable);
        this.A01 = i;
        this.A00 = f;
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A01);
        parcel.writeFloat(this.A00);
    }
}
