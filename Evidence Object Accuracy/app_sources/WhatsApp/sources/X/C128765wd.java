package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.5wd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128765wd {
    public final /* synthetic */ BrazilPayBloksActivity A00;
    public final /* synthetic */ String A01;

    public /* synthetic */ C128765wd(BrazilPayBloksActivity brazilPayBloksActivity, String str) {
        this.A00 = brazilPayBloksActivity;
        this.A01 = str;
    }

    public final void A00(C452120p r5) {
        BrazilPayBloksActivity brazilPayBloksActivity = this.A00;
        String str = this.A01;
        AnonymousClass3FE r2 = brazilPayBloksActivity.A01;
        if (r2 == null) {
            Log.i("PAY: BrazilPayBloksActivity onActivityResult - appToAppBloksCallback is null!");
        } else if (r5 != null) {
            AbstractActivityC121705jc.A0l(r2, null, r5.A00);
        } else {
            HashMap A11 = C12970iu.A11();
            A11.put("app_to_app_authorization_code", str);
            brazilPayBloksActivity.A01.A01("on_success", A11);
        }
    }
}
