package X;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.WaPreferenceFragment;
import com.whatsapp.dialogs.FAQLearnMoreDialogFragment;
import com.whatsapp.settings.SettingsChatHistoryFragment;
import com.whatsapp.settings.SettingsJidNotificationFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1yU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC44201yU extends AbstractActivityC44211yV implements AbstractC13860kS {
    public int A00;
    public int A01;
    public long A02;
    public ProgressDialog A03;
    public Intent A04;
    public C14900mE A05;
    public WaPreferenceFragment A06;
    public Integer A07;
    @Deprecated
    public String A08;
    public boolean A09 = true;
    public boolean A0A;
    public final AbstractC18900tF A0B = new AbstractC18900tF() { // from class: X.560
        @Override // X.AbstractC18900tF
        public final void ASC() {
            ActivityC44201yU.this.A0A = true;
        }
    };

    @Override // X.AbstractC13860kS
    public boolean AJN() {
        return C36021jC.A03(this);
    }

    @Override // X.AbstractC13860kS
    public void AaN() {
        this.A03 = null;
        C36021jC.A00(this, 501);
    }

    @Override // X.AbstractC13860kS
    public void Adl(DialogFragment dialogFragment, String str) {
        throw new IllegalStateException("Unsupported operation");
    }

    @Override // X.AbstractC13860kS
    public void Adm(DialogFragment dialogFragment) {
        throw new IllegalStateException("Unsupported operation");
    }

    @Override // X.AbstractC13860kS
    public void Ado(int i) {
        this.A00 = i;
        C36021jC.A01(this, 500);
    }

    @Override // X.AbstractC13860kS
    @Deprecated
    public void Adp(String str) {
        this.A08 = str;
        C36021jC.A01(this, 500);
    }

    @Override // X.AbstractC13860kS
    public void Adq(AnonymousClass2GV r2, Object[] objArr, int i, int i2, int i3) {
        Adr(objArr, i, i2);
    }

    @Override // X.AbstractC13860kS
    public void Adr(Object[] objArr, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        C36021jC.A01(this, 500);
    }

    @Override // X.AbstractC13860kS
    public void Ady(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        C36021jC.A01(this, 501);
    }

    @Override // X.AbstractC13860kS
    public void AfX(String str) {
        ProgressDialog progressDialog = this.A03;
        if (progressDialog != null) {
            progressDialog.setMessage(str);
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A1V().A0F(view, layoutParams);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return (this.A09 || SystemClock.elapsedRealtime() - this.A02 > 500 || !(motionEvent.getActionMasked() == 0 || motionEvent.getActionMasked() == 2)) && super.dispatchTouchEvent(motionEvent);
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A09) {
            super.onBackPressed();
        } else {
            Log.e("dialogtoasttreferenceactivity/onbackpressed/activity no active");
        }
    }

    @Override // X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        ((ActivityC13830kP) this).A01.A0L();
        super.onConfigurationChanged(configuration);
        A1V().A0C(configuration);
    }

    @Override // X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.A0A = false;
        C42941w9.A0B(getWindow(), ((ActivityC13830kP) this).A01);
        getTheme().applyStyle(R.style.NoActionBar, true);
        getLayoutInflater().setFactory2(new LayoutInflater$Factory2C100744mP(A1V()));
        A1V().A0D(bundle);
        super.onCreate(bundle);
        View findViewById = findViewById(16908298);
        if (findViewById != null) {
            int paddingLeft = findViewById.getPaddingLeft();
            int paddingLeft2 = findViewById.getPaddingLeft();
            ViewParent parent = findViewById.getParent();
            if (parent instanceof View) {
                View view = (View) parent;
                paddingLeft += view.getPaddingLeft();
                paddingLeft2 += view.getPaddingRight();
                view.setPadding(0, 0, 0, 0);
            }
            findViewById.setPadding(paddingLeft, 0, paddingLeft2, 0);
            findViewById.setScrollBarStyle(33554432);
        }
        AnonymousClass018 r0 = ((ActivityC13830kP) this).A01;
        r0.A0B.add(this.A0B);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r2;
        String str;
        C004802e A05;
        AbstractC14640lm r1;
        if (i == 500) {
            r2 = new C004802e(this);
            if (TextUtils.isEmpty(this.A08)) {
                str = ((ActivityC13830kP) this).A01.A09(this.A00);
            } else {
                str = this.A08;
            }
            r2.A0A(str);
            r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.4fJ
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    C36021jC.A00(ActivityC44201yU.this, 500);
                }
            });
            int i2 = this.A01;
            if (i2 != 0) {
                r2.A07(i2);
            }
        } else if (i != 501) {
            WaPreferenceFragment waPreferenceFragment = this.A06;
            if (waPreferenceFragment instanceof SettingsJidNotificationFragment) {
                SettingsJidNotificationFragment settingsJidNotificationFragment = (SettingsJidNotificationFragment) waPreferenceFragment;
                if (i == 0) {
                    return FAQLearnMoreDialogFragment.A01(((WaPreferenceFragment) settingsJidNotificationFragment).A00, settingsJidNotificationFragment.A00, settingsJidNotificationFragment.A03, settingsJidNotificationFragment.A06, settingsJidNotificationFragment.A0I(R.string.popup_notification_disabled_message), "26000003", null, null);
                }
            } else if (waPreferenceFragment instanceof SettingsChatHistoryFragment) {
                SettingsChatHistoryFragment settingsChatHistoryFragment = (SettingsChatHistoryFragment) waPreferenceFragment;
                ActivityC44201yU r6 = ((WaPreferenceFragment) settingsChatHistoryFragment).A00;
                if (r6 != null) {
                    boolean z = false;
                    if (i == 3) {
                        C70143ao r7 = new C70143ao(settingsChatHistoryFragment);
                        C255719x r5 = settingsChatHistoryFragment.A0A;
                        if (r5.A08()) {
                            A05 = r5.A04(r6, r7, -1, 3, 1, true);
                        } else {
                            A05 = r5.A05(r6, r7, r6.getString(R.string.clear_all_chats_dialog_message), -1, false);
                        }
                        AnonymousClass04S create = A05.create();
                        create.show();
                        return create;
                    } else if (i == 4) {
                        C1114859o r8 = new C1114859o(settingsChatHistoryFragment);
                        C255719x r3 = settingsChatHistoryFragment.A0A;
                        Context A0p = settingsChatHistoryFragment.A0p();
                        r2 = r3.A08() ? r3.A04(A0p, new C1114959p(r8), -1, 0, 0, false) : r3.A03(A0p, r8, A0p.getString(R.string.delete_all_chats_ask), R.string.delete, -1, false);
                    } else if (i == 5) {
                        if (settingsChatHistoryFragment.A08.A02() > 0) {
                            z = true;
                        }
                        AnonymousClass3KX r12 = new DialogInterface.OnClickListener(z) { // from class: X.3KX
                            public final /* synthetic */ boolean A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // android.content.DialogInterface.OnClickListener
                            public final void onClick(DialogInterface dialogInterface, int i3) {
                                SettingsChatHistoryFragment settingsChatHistoryFragment2 = SettingsChatHistoryFragment.this;
                                boolean z2 = this.A01;
                                ActivityC44201yU r13 = ((WaPreferenceFragment) settingsChatHistoryFragment2).A00;
                                if (r13 != null) {
                                    C36021jC.A00(r13, 5);
                                    settingsChatHistoryFragment2.A1B();
                                    settingsChatHistoryFragment2.A0B.Ab2(new RunnableBRunnable0Shape0S0110000_I0(settingsChatHistoryFragment2, 19, z2));
                                }
                            }
                        };
                        r2 = new C004802e(settingsChatHistoryFragment.A0p());
                        int i3 = R.string.unarchive_all_chats_ask;
                        if (z) {
                            i3 = R.string.archive_all_chats_ask;
                        }
                        r2.A06(i3);
                        r2.setPositiveButton(R.string.ok, r12);
                        r2.setNegativeButton(R.string.cancel, null);
                    } else if (i == 10 && (r1 = settingsChatHistoryFragment.A09) != null) {
                        C15370n3 A0B = settingsChatHistoryFragment.A04.A0B(r1);
                        C255319t r13 = settingsChatHistoryFragment.A05;
                        ActivityC44201yU r0 = ((WaPreferenceFragment) settingsChatHistoryFragment).A00;
                        return r13.A00(r0, r0, A0B);
                    }
                }
            }
            return super.onCreateDialog(i);
        } else {
            ProgressDialog progressDialog = new ProgressDialog(this);
            int i4 = this.A01;
            if (i4 != 0) {
                progressDialog.setTitle(i4);
            }
            progressDialog.setMessage(((ActivityC13830kP) this).A01.A09(this.A00));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            this.A03 = progressDialog;
            return progressDialog;
        }
        return r2.create();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        A1V().A08();
        AnonymousClass018 r0 = ((ActivityC13830kP) this).A01;
        r0.A0B.remove(this.A0B);
        this.A04 = null;
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPause() {
        this.A05.A09(this);
        super.onPause();
        this.A09 = false;
        this.A02 = SystemClock.elapsedRealtime();
    }

    @Override // X.ActivityC000800j, android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        ((LayoutInflater$Factory2C011505o) A1V()).A0M();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onPostResume() {
        super.onPostResume();
        LayoutInflater$Factory2C011505o r0 = (LayoutInflater$Factory2C011505o) A1V();
        r0.A0O();
        AbstractC005102i r1 = r0.A0B;
        if (r1 != null) {
            r1.A0Q(true);
        }
    }

    @Override // android.app.Activity
    public void onPrepareDialog(int i, Dialog dialog) {
        String str;
        if (i != 500) {
            super.onPrepareDialog(i, dialog);
            return;
        }
        AnonymousClass04S r4 = (AnonymousClass04S) dialog;
        if (TextUtils.isEmpty(this.A08)) {
            str = ((ActivityC13830kP) this).A01.A09(this.A00);
        } else {
            str = this.A08;
        }
        AnonymousClass0U5 r0 = r4.A00;
        r0.A0Q = str;
        TextView textView = r0.A0K;
        if (textView != null) {
            textView.setText(str);
        }
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A08 = bundle.getString("dialogToastMessage");
        this.A00 = bundle.getInt("dialogToastMessageId", 0);
        this.A01 = bundle.getInt("dialogToastTitleId", 0);
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(".onResume");
        Log.i(sb.toString());
        if (this.A0A) {
            this.A0A = false;
            finish();
            startActivity(getIntent());
        }
        super.onResume();
        this.A05.A0B(this);
        this.A09 = true;
        Intent intent = this.A04;
        if (intent != null) {
            Integer num = this.A07;
            if (num != null) {
                startActivityForResult(intent, num.intValue());
            } else {
                startActivity(intent);
            }
            this.A04 = null;
            this.A07 = null;
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(".onSaveInstanceState");
        Log.i(sb.toString());
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("dialogToastMessage", this.A08);
        bundle.putInt("dialogToastMessageId", this.A00);
        bundle.putInt("dialogToastTitleId", this.A01);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        A1V().A09();
    }

    @Override // X.ActivityC000800j, android.app.Activity
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        A1V().A0H(charSequence);
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void setContentView(int i) {
        Toolbar toolbar = (Toolbar) getLayoutInflater().inflate(R.layout.toolbar, (ViewGroup) null, false);
        toolbar.setTitle(getTitle());
        if (Build.VERSION.SDK_INT >= 21) {
            toolbar.setElevation(getResources().getDimension(R.dimen.actionbar_elevation));
        }
        boolean z = ((ActivityC13830kP) this).A01.A04().A06;
        int i2 = R.drawable.abc_ic_ab_back_material;
        if (z) {
            i2 = R.drawable.ic_back_rtl;
        }
        toolbar.setNavigationIcon(i2);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.addView(toolbar, -1, getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material));
        ViewGroup frameLayout = new FrameLayout(this);
        getLayoutInflater().inflate(i, frameLayout, true);
        linearLayout.addView(frameLayout, -1, -1);
        setContentView(linearLayout);
        A1e(toolbar);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 8));
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void setContentView(View view) {
        A1V().A0E(view);
    }

    @Override // X.ActivityC000800j, X.ActivityC001000l, android.app.Activity
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A1V().A0G(view, layoutParams);
    }
}
