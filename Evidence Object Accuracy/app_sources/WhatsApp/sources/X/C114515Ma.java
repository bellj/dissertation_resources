package X;

import java.util.Enumeration;

/* renamed from: X.5Ma  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114515Ma extends AnonymousClass1TM {
    public AnonymousClass5MA A00;
    public C114725Mv A01;

    public static C114515Ma A00(Object obj) {
        if (obj instanceof C114515Ma) {
            return (C114515Ma) obj;
        }
        if (obj != null) {
            return new C114515Ma(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    public C114515Ma(AbstractC114775Na r3) {
        if (r3.A0B() == 2) {
            Enumeration A0C = r3.A0C();
            this.A01 = C114725Mv.A00(A0C.nextElement());
            this.A00 = AnonymousClass5MA.A00(A0C.nextElement());
            return;
        }
        throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r3.A0B()));
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        return C94954co.A01(this.A00, A00);
    }
}
