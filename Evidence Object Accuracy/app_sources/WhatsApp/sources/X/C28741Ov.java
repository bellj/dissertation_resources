package X;

/* renamed from: X.1Ov  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28741Ov extends C28751Ow {
    public C28741Ov(C28751Ow r1) {
        super(r1);
    }

    public C28741Ov(Object obj) {
        super(obj, -1, -1, -1, -1);
    }

    public C28741Ov(Object obj, int i, int i2, long j) {
        super(obj, i, i2, -1, j);
    }

    public C28741Ov(Object obj, long j, int i) {
        super(obj, -1, -1, i, j);
    }

    public C28741Ov A01(Object obj) {
        C28751Ow r1;
        if (this.A04.equals(obj)) {
            r1 = this;
        } else {
            r1 = new C28751Ow(obj, this.A00, this.A01, this.A02, this.A03);
        }
        return new C28741Ov(r1);
    }
}
