package X;

/* renamed from: X.3AB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3AB {
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:48:0x0089 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v4, types: [java.text.DateFormat] */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        if (r6.equals("full") != false) goto L_0x0086;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0011 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence A00(java.lang.String r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, long r9) {
        /*
            java.lang.String r1 = "DatetimeTextProviderUtils"
            if (r6 == 0) goto L_0x000e
            int r3 = X.AnonymousClass3JW.A06(r6)     // Catch: 491 -> 0x0009
            goto L_0x000f
        L_0x0009:
            java.lang.String r0 = "Error while parsing DateTime format"
            X.C28691Op.A00(r1, r0)
        L_0x000e:
            r3 = 2
        L_0x000f:
            if (r7 == 0) goto L_0x001b
            int r4 = X.AnonymousClass3JW.A06(r7)     // Catch: 491 -> 0x0016
            goto L_0x001c
        L_0x0016:
            java.lang.String r0 = "Error while parsing Time format"
            X.C28691Op.A00(r1, r0)
        L_0x001b:
            r4 = r3
        L_0x001c:
            if (r6 != 0) goto L_0x0020
            java.lang.String r6 = "medium"
        L_0x0020:
            int r0 = r5.hashCode()
            r2 = 2
            r1 = -1
            switch(r0) {
                case 3076014: goto L_0x0050;
                case 3560141: goto L_0x0045;
                case 1793702779: goto L_0x003b;
                default: goto L_0x0029;
            }
        L_0x0029:
            switch(r1) {
                case 0: goto L_0x0064;
                case 1: goto L_0x005f;
                case 2: goto L_0x005a;
                default: goto L_0x002c;
            }
        L_0x002c:
            java.lang.String r0 = "Unknown dateformat type: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r5, r0)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x003b:
            java.lang.String r0 = "datetime"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0029
            r1 = 2
            goto L_0x0029
        L_0x0045:
            java.lang.String r0 = "time"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0029
            r1 = 1
            goto L_0x0029
        L_0x0050:
            java.lang.String r0 = "date"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0029
            r1 = 0
            goto L_0x0029
        L_0x005a:
            java.text.DateFormat r2 = java.text.DateFormat.getDateTimeInstance(r3, r4)
            goto L_0x009e
        L_0x005f:
            java.text.DateFormat r2 = java.text.DateFormat.getTimeInstance(r4)
            goto L_0x009e
        L_0x0064:
            java.lang.String r0 = "short"
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0086
            java.lang.String r0 = "medium"
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0086
            java.lang.String r0 = "long"
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0086
            java.lang.String r0 = "full"
            boolean r1 = r6.equals(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0087
        L_0x0086:
            r0 = 1
        L_0x0087:
            if (r0 == 0) goto L_0x0092
            int r0 = X.AnonymousClass3JW.A06(r6)     // Catch: 491 -> 0x009a
            java.text.DateFormat r2 = java.text.DateFormat.getDateInstance(r0)     // Catch: 491 -> 0x009a
            goto L_0x009e
        L_0x0092:
            java.util.Locale r0 = java.util.Locale.US
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            r2.<init>(r6, r0)
            goto L_0x009e
        L_0x009a:
            java.text.DateFormat r2 = java.text.DateFormat.getDateInstance(r2)
        L_0x009e:
            if (r8 != 0) goto L_0x00ac
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.util.TimeZone r0 = r0.getTimeZone()
            java.lang.String r8 = r0.getID()
        L_0x00ac:
            java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r8)
            r2.setTimeZone(r0)
            r0 = 1000(0x3e8, double:4.94E-321)
            long r9 = r9 * r0
            java.lang.Long r0 = java.lang.Long.valueOf(r9)
            java.lang.String r0 = r2.format(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3AB.A00(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long):java.lang.CharSequence");
    }
}
