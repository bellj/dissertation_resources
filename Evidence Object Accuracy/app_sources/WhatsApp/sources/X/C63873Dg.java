package X;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/* renamed from: X.3Dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63873Dg {
    public final Set A00 = C12970iu.A12();

    public void A00() {
        Set<AbstractC14780m2> set = this.A00;
        synchronized (set) {
            if (!set.isEmpty()) {
                LinkedList linkedList = null;
                for (AbstractC14780m2 r1 : set) {
                    if (r1.AM3()) {
                        if (linkedList == null) {
                            linkedList = new LinkedList();
                        }
                        linkedList.add(r1);
                    }
                }
                if (linkedList != null) {
                    Iterator it = linkedList.iterator();
                    while (it.hasNext()) {
                        set.remove((AbstractC14780m2) it.next());
                    }
                }
            }
        }
    }

    public void A01(AbstractC14780m2 r3) {
        Set set = this.A00;
        synchronized (set) {
            set.add(r3);
        }
    }
}
