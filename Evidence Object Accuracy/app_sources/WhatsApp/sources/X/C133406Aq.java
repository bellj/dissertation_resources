package X;

/* renamed from: X.6Aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133406Aq implements AnonymousClass6MV {
    public final /* synthetic */ C133736Bx A00;
    public final /* synthetic */ byte[] A01;

    public C133406Aq(C133736Bx r1, byte[] bArr) {
        this.A00 = r1;
        this.A01 = bArr;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        this.A00.A01.A01();
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r4) {
        AnonymousClass1V8 A02 = new C128545wH(r4).A02(this.A01);
        C133736Bx r1 = this.A00;
        r1.A00.A1C();
        r1.A01.A05(null, A02);
    }
}
