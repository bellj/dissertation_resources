package X;

/* renamed from: X.1y4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44031y4 {
    public static final String A00;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(AnonymousClass1Ux.A01("message", C16500p8.A00));
        sb.append(" FROM ");
        sb.append("available_message_view AS message");
        sb.append(" JOIN ");
        sb.append("chat AS chat");
        sb.append(" ON message.chat_row_id = chat._id");
        sb.append(" WHERE ");
        sb.append("message.from_me = 0");
        sb.append(" AND ");
        sb.append("status != 16");
        sb.append(" AND ");
        sb.append("status != 10");
        sb.append(" AND ");
        sb.append("(chat.last_read_message_sort_id >= message.sort_id OR status = 17)");
        sb.append(" AND ");
        sb.append("chat.last_read_receipt_sent_message_sort_id < message.sort_id");
        sb.append(" AND ");
        sb.append("chat.last_read_receipt_sent_message_row_id > 0");
        sb.append(" AND ");
        sb.append("message.message_type NOT IN ('8', '10', '15')");
        sb.append(" AND ");
        sb.append("(chat.hidden IS NULL OR chat.hidden = 0)");
        sb.append(" ORDER BY message.sort_id DESC");
        sb.append(" LIMIT 4096");
        A00 = sb.toString();
    }
}
