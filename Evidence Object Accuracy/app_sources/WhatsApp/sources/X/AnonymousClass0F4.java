package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.0F4  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0F4 extends AbstractC018308n {
    public final List A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass0F4) && C16700pc.A0O(this.A00, ((AnonymousClass0F4) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GridItemDecoration(offset=");
        sb.append(this.A00);
        sb.append(')');
        return sb.toString();
    }

    public AnonymousClass0F4(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        C16700pc.A0E(rect, 0);
        C16700pc.A0E(view, 1);
        C16700pc.A0E(recyclerView, 2);
        Rect rect2 = (Rect) AnonymousClass01Y.A02(this.A00, RecyclerView.A00(view));
        if (rect2 != null) {
            rect.set(rect2);
        }
    }
}
