package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/* renamed from: X.2Tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51232Tk {
    public static final byte[] A06 = {69, 68, 0, 1};
    public static final byte[] A07 = {87, 65, 5, 2};
    public final C14830m7 A00;
    public final C51132Sz A01;
    public final C251418g A02;
    public final AnonymousClass20P A03;
    public final AnonymousClass20O A04;
    public final C51252Tm A05;

    public C51232Tk(C14830m7 r15, C51132Sz r16, C251418g r17, InputStream inputStream, OutputStream outputStream, C29361Rw r20, AnonymousClass2ST r21) {
        AnonymousClass20P r0;
        byte[] decode;
        int length;
        this.A00 = r15;
        this.A02 = r17;
        C29361Rw A00 = C29361Rw.A00();
        String string = this.A02.A00.A00.getString("routing_info", null);
        if (!TextUtils.isEmpty(string) && (decode = Base64.decode(string, 3)) != null && (length = decode.length) > 0) {
            outputStream.write(A06);
            outputStream.write(new byte[]{(byte) (length >> 16), (byte) (length >> 8), (byte) length});
            outputStream.write(decode);
        }
        byte[] bArr = A07;
        outputStream.write(bArr);
        AnonymousClass009.A05(r16);
        this.A01 = r16;
        this.A04 = new AnonymousClass20O(inputStream);
        this.A05 = new C51252Tm(outputStream);
        try {
            if (r21 == null) {
                C94444bp r7 = new C94444bp(C94444bp.A05, bArr);
                byte[] bArr2 = A00.A02.A01;
                r7.A03.A00(bArr2);
                AnonymousClass1G4 A0T = AnonymousClass25O.A04.A0T();
                AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
                A0T.A03();
                AnonymousClass25O r1 = (AnonymousClass25O) A0T.A00;
                r1.A00 |= 1;
                r1.A01 = A01;
                AnonymousClass1G4 A0T2 = AnonymousClass25M.A04.A0T();
                A0T2.A03();
                AnonymousClass25M r12 = (AnonymousClass25M) A0T2.A00;
                r12.A02 = (AnonymousClass25O) A0T.A02();
                r12.A00 |= 1;
                this.A05.write(A0T2.A02().A02());
                r0 = A01(A00(), A00, r20, r7);
            } else {
                try {
                    C94444bp r72 = new C94444bp(C94444bp.A06, bArr);
                    AnonymousClass2ST r3 = new AnonymousClass2ST(r72.A02(r21.A01));
                    byte[] bArr3 = A00.A02.A01;
                    AnonymousClass4VB r10 = r72.A03;
                    r10.A00(bArr3);
                    C29371Rx r9 = A00.A01;
                    r72.A01(AnonymousClass4F7.A00(r9, r3));
                    byte[] A03 = r72.A03(r20.A02.A01);
                    C29371Rx r8 = r20.A01;
                    r72.A01(AnonymousClass4F7.A00(r8, r3));
                    byte[] A032 = r72.A03(this.A01.A02());
                    AnonymousClass1G4 A0T3 = AnonymousClass25O.A04.A0T();
                    AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr3, 0, bArr3.length);
                    A0T3.A03();
                    AnonymousClass25O r13 = (AnonymousClass25O) A0T3.A00;
                    r13.A00 |= 1;
                    r13.A01 = A012;
                    AbstractC27881Jp A013 = AbstractC27881Jp.A01(A03, 0, A03.length);
                    A0T3.A03();
                    AnonymousClass25O r14 = (AnonymousClass25O) A0T3.A00;
                    r14.A00 |= 2;
                    r14.A03 = A013;
                    AbstractC27881Jp A014 = AbstractC27881Jp.A01(A032, 0, A032.length);
                    A0T3.A03();
                    AnonymousClass25O r18 = (AnonymousClass25O) A0T3.A00;
                    r18.A00 |= 4;
                    r18.A02 = A014;
                    AnonymousClass1G4 A0T4 = AnonymousClass25M.A04.A0T();
                    A0T4.A03();
                    AnonymousClass25M r19 = (AnonymousClass25M) A0T4.A00;
                    r19.A02 = (AnonymousClass25O) A0T3.A02();
                    r19.A00 |= 1;
                    this.A05.write(A0T4.A02().A02());
                    AnonymousClass25N A002 = A00();
                    if ((A002.A00 & 2) == 2) {
                        throw new AnonymousClass4C1(A002);
                    }
                    byte[] A04 = A002.A01.A04();
                    r10.A00(A04);
                    AnonymousClass2ST r110 = new AnonymousClass2ST(A04);
                    r72.A01(AnonymousClass4F7.A00(r9, r110));
                    r72.A01(AnonymousClass4F7.A00(r8, r110));
                    r72.A02(A002.A02.A04());
                    r0 = r72.A00(r3);
                } catch (AnonymousClass20R e) {
                    throw new AnonymousClass20S(e);
                }
            }
        } catch (AnonymousClass4C1 e2) {
            AnonymousClass25N r32 = e2.serverHello;
            C94444bp r2 = new C94444bp(C94444bp.A04, bArr);
            r2.A03.A00(A00.A02.A01);
            r0 = A01(r32, A00, r20, r2);
        }
        this.A03 = r0;
    }

    public final AnonymousClass25N A00() {
        AnonymousClass20O r2 = this.A04;
        byte[] A00 = r2.A00(3);
        if (!Arrays.equals(AnonymousClass20O.A01, A00)) {
            AnonymousClass25M r22 = (AnonymousClass25M) AbstractC27091Fz.A0E(AnonymousClass25M.A04, r2.A00(C16050oM.A01(A00)));
            if ((r22.A00 & 2) == 2) {
                AnonymousClass25N r0 = r22.A03;
                if (r0 == null) {
                    return AnonymousClass25N.A04;
                }
                return r0;
            }
            throw new IOException("Handshake message does not contain server hello!");
        }
        throw new C51452Uu();
    }

    public final AnonymousClass20P A01(AnonymousClass25N r11, C29361Rw r12, C29361Rw r13, C94444bp r14) {
        C28971Pt e;
        String str;
        C57082mR r2;
        byte[] A04;
        C57492n9 r5;
        AnonymousClass2ST r7;
        String obj;
        StringBuilder sb;
        String str2;
        try {
            byte[] A042 = r11.A01.A04();
            r14.A03.A00(A042);
            AnonymousClass2ST r3 = new AnonymousClass2ST(A042);
            C29371Rx r1 = r12.A01;
            r14.A01(AnonymousClass4F7.A00(r1, r3));
            AnonymousClass2ST r4 = new AnonymousClass2ST(r14.A02(r11.A03.A04()));
            r14.A01(AnonymousClass4F7.A00(r1, r4));
            byte[] A02 = r14.A02(r11.A02.A04());
            C14830m7 r6 = this.A00;
            try {
                r2 = (C57082mR) AbstractC27091Fz.A0E(C57082mR.A03, A02);
                A04 = r2.A01.A04();
            } catch (C28971Pt e2) {
                e = e2;
                str = "noise certificate parsing failed";
            }
            try {
                r5 = (C57492n9) AbstractC27091Fz.A0E(C57492n9.A06, A04);
                r7 = (AnonymousClass2ST) C88634Gl.A00.get(r5.A04);
            } catch (C28971Pt e3) {
                e = e3;
                str = "noise certificate details parsing failed";
                Log.e(str, e);
                throw new AnonymousClass2UW(this);
            }
            if (r7 == null) {
                sb = new StringBuilder();
                str2 = "noise certificate issued by unknown source; issuer=";
            } else {
                if (!C32001bS.A00().A02(r7.A01, A04, r2.A02.A04())) {
                    sb = new StringBuilder();
                    str2 = "invalid signature on noise certificate; issuer=";
                } else if (!Arrays.equals(r5.A03.A04(), r4.A01)) {
                    sb = new StringBuilder();
                    str2 = "noise certificate key does not match proposed server static key; issuer=";
                } else if ((r5.A00 & 4) != 4 || r5.A02 >= r6.A00() / 1000) {
                    byte[] A03 = r14.A03(r13.A02.A01);
                    r14.A01(AnonymousClass4F7.A00(r13.A01, r3));
                    byte[] A032 = r14.A03(this.A01.A02());
                    AnonymousClass1G4 A0T = AnonymousClass25P.A03.A0T();
                    AbstractC27881Jp A01 = AbstractC27881Jp.A01(A03, 0, A03.length);
                    A0T.A03();
                    AnonymousClass25P r15 = (AnonymousClass25P) A0T.A00;
                    r15.A00 |= 1;
                    r15.A02 = A01;
                    AbstractC27881Jp A012 = AbstractC27881Jp.A01(A032, 0, A032.length);
                    A0T.A03();
                    AnonymousClass25P r16 = (AnonymousClass25P) A0T.A00;
                    r16.A00 |= 2;
                    r16.A01 = A012;
                    AnonymousClass1G4 A0T2 = AnonymousClass25M.A04.A0T();
                    A0T2.A03();
                    AnonymousClass25M r17 = (AnonymousClass25M) A0T2.A00;
                    r17.A01 = (AnonymousClass25P) A0T.A02();
                    r17.A00 |= 4;
                    this.A05.write(A0T2.A02().A02());
                    return r14.A00(r4);
                } else {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ", Locale.US);
                    StringBuilder sb2 = new StringBuilder("noise certificate expired; issuer=");
                    sb2.append(r5.A04);
                    sb2.append("; expires=");
                    sb2.append(simpleDateFormat.format(new Date(r5.A02 * 1000)));
                    obj = sb2.toString();
                    Log.e(obj);
                    throw new AnonymousClass2UW(this);
                }
            }
            sb.append(str2);
            sb.append(r5.A04);
            obj = sb.toString();
            Log.e(obj);
            throw new AnonymousClass2UW(this);
        } catch (AnonymousClass20R e4) {
            throw new AnonymousClass20S(e4);
        }
    }
}
