package X;

import android.view.ViewTreeObserver;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.4oQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101994oQ implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C63483Bt A00;
    public final /* synthetic */ boolean A01;

    public ViewTreeObserver$OnPreDrawListenerC101994oQ(C63483Bt r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        SelectionCheckView selectionCheckView = this.A00.A08;
        C12980iv.A1G(selectionCheckView, this);
        selectionCheckView.A04(this.A01, true);
        return false;
    }
}
