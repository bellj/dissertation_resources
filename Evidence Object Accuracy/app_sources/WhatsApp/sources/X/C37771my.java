package X;

/* renamed from: X.1my  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37771my implements AbstractC37731mu {
    public int A00 = 0;
    public boolean A01;
    public final long A02;
    public final C28481Nj A03;
    public final C37761mx A04;

    public C37771my(C37761mx r9, String str, long j) {
        this.A03 = new C28481Nj(str, str, "", null, null, 0, false);
        this.A04 = r9;
        this.A02 = j;
    }

    @Override // X.AbstractC37731mu
    public long AAo() {
        return this.A01 ? this.A04.AAo() : this.A02;
    }

    @Override // X.AbstractC37731mu
    public C28481Nj ACB() {
        if (!this.A01) {
            return this.A03;
        }
        return this.A04.A02;
    }

    @Override // X.AbstractC37731mu
    public void APu(boolean z, int i) {
        if (this.A01) {
            this.A04.APu(z, i);
        }
        if (!z || this.A00 > 1) {
            this.A01 = true;
        }
        this.A00++;
    }
}
