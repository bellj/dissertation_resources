package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.RemoteException;
import com.whatsapp.util.Log;
import java.util.HashSet;

/* renamed from: X.1Bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25961Bm {
    public AnonymousClass4SL A00;
    public AbstractC15710nm A01;
    public byte[] A02 = null;
    public final C16590pI A03;

    public C25961Bm(C16590pI r2) {
        this.A03 = r2;
    }

    public final void A00() {
        Context context = this.A03.A00;
        PackageManager packageManager = context.getPackageManager();
        ContentResolver contentResolver = context.getContentResolver();
        AnonymousClass4EZ r12 = new AnonymousClass4EZ();
        AnonymousClass3CB r10 = new AnonymousClass3CB(r12);
        HashSet hashSet = new HashSet();
        C81053tO builder = C81073tQ.builder();
        for (Signature signature : AnonymousClass4HL.A02) {
            builder.put((Object) AnonymousClass4FX.A00, (Object) signature);
        }
        this.A00 = new AnonymousClass4SL(contentResolver, new C63613Cg(packageManager, builder.build(), AbstractC17940re.of()), r10, new C64553Fz(r12, hashSet), r12);
    }

    public byte[] A01() {
        C92054Ui r2;
        byte[] bArr = this.A02;
        if (bArr == null) {
            if (this.A00 == null) {
                A00();
            }
            bArr = null;
            try {
                AnonymousClass4SL r6 = this.A00;
                AnonymousClass009.A05(r6);
                C92044Uh r0 = new C92044Uh();
                r0.A00();
                AnonymousClass4IA r8 = new AnonymousClass4IA(r0.A00);
                new Bundle();
                try {
                    C64553Fz r7 = r6.A03;
                    try {
                        r7.A02(r8);
                    } catch (Exception e) {
                        Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
                    }
                    Bundle A00 = AnonymousClass3G1.A00(r6.A00, AnonymousClass3GR.A00, C65273Iw.A00(r8.A00), r6.A01, "query");
                    AnonymousClass3G1.A01(A00, r6.A02, "query");
                    if (A00 == null) {
                        r2 = null;
                    } else {
                        r2 = new C92054Ui(C65273Iw.A00(A00));
                    }
                    try {
                        r7.A03(r2);
                    } catch (Exception e2) {
                        Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e2);
                    }
                } catch (Exception e3) {
                    try {
                        r6.A03.A09(e3);
                    } catch (Exception e4) {
                        Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e4);
                    }
                    throw e3;
                }
            } catch (C87404Bj | RemoteException | IllegalArgumentException | SecurityException e5) {
                Log.e("AutoconfManagerConsumerImpl/acquireClientCapabilities", e5);
                this.A01.AaV("AutoconfManagerConsumerImpl/acquireClientCapabilities/error", e5.getMessage(), true);
                r2 = null;
            }
            if (r2 != null) {
                bArr = r2.A00();
            }
            this.A02 = bArr;
        }
        return bArr;
    }
}
