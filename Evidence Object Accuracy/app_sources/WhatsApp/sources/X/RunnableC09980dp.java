package X;

/* renamed from: X.0dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09980dp implements Runnable {
    public AnonymousClass0NN A00;
    public AnonymousClass022 A01;
    public String A02;

    public RunnableC09980dp(AnonymousClass0NN r1, AnonymousClass022 r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.A03.A04(this.A00, this.A02);
    }
}
