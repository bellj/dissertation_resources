package X;

import com.whatsapp.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

/* renamed from: X.66y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1324566y implements AbstractC25851Bb, AnonymousClass1BZ {
    public final C14900mE A00;
    public final C16590pI A01;
    public final C14820m6 A02;
    public final AbstractC14440lR A03;

    public C1324566y(C14900mE r1, C16590pI r2, C14820m6 r3, AbstractC14440lR r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass1BZ
    public void AAv(AbstractC116535Vv r4, String str, HashMap hashMap) {
        AnonymousClass009.A0B(C12960it.A0d(str, C12960it.A0k("BloksPayloadHelperImpl/getBloks/invalid screen name: ")), !C14350lI.A0R(str));
        this.A00.A06(0, R.string.loading_spinner);
        this.A03.Ab6(new Runnable(r4, str) { // from class: X.6J5
            public final /* synthetic */ AbstractC116535Vv A01;
            public final /* synthetic */ String A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C1324566y r42 = C1324566y.this;
                String str2 = this.A02;
                AbstractC116535Vv r3 = this.A01;
                StringBuilder A0h = C12960it.A0h();
                A0h.append(File.separator);
                A0h.append(str2);
                String A0d = C12960it.A0d(".json", A0h);
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append(C25841Ba.A0F);
                try {
                    FileInputStream fileInputStream = new FileInputStream(new File(r42.A01.A00.getFilesDir(), C12960it.A0d(A0d, A0h2)));
                    AnonymousClass3AL.A00(r3, AnonymousClass1P1.A00(fileInputStream));
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override // X.AbstractC25851Bb
    public C17930rd ABm() {
        return C17930rd.A01(this.A02.A0B());
    }
}
