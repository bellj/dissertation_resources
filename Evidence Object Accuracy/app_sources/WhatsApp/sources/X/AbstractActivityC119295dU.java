package X;

import com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity;

/* renamed from: X.5dU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119295dU extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119295dU() {
        C117295Zj.A0p(this, 88);
    }

    public static void A02(AnonymousClass01J r1, C249317l r2, NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity) {
        ((ActivityC13790kL) noviPayHubTransactionHistoryActivity).A08 = r2;
        noviPayHubTransactionHistoryActivity.A0A = (AnonymousClass14X) r1.AFM.get();
        noviPayHubTransactionHistoryActivity.A00 = (C15550nR) r1.A45.get();
        noviPayHubTransactionHistoryActivity.A01 = (C15610nY) r1.AMe.get();
        noviPayHubTransactionHistoryActivity.A06 = (AnonymousClass60Y) r1.ADK.get();
        noviPayHubTransactionHistoryActivity.A05 = (C17070qD) r1.AFC.get();
        noviPayHubTransactionHistoryActivity.A02 = (AnonymousClass102) r1.AEL.get();
        noviPayHubTransactionHistoryActivity.A07 = (C130095yn) r1.ADc.get();
        noviPayHubTransactionHistoryActivity.A04 = (AnonymousClass61M) r1.AF1.get();
        noviPayHubTransactionHistoryActivity.A03 = (C243515e) r1.AEt.get();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity = (NoviPayHubTransactionHistoryActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, noviPayHubTransactionHistoryActivity);
            ActivityC13810kN.A10(A1M, noviPayHubTransactionHistoryActivity);
            A02(A1M, ActivityC13790kL.A0S(r3, A1M, noviPayHubTransactionHistoryActivity, ActivityC13790kL.A0Y(A1M, noviPayHubTransactionHistoryActivity)), noviPayHubTransactionHistoryActivity);
        }
    }
}
