package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.1mp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC37681mp extends Handler {
    public final /* synthetic */ C22600zL A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC37681mp(Looper looper, C22600zL r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (message.what == 0) {
            this.A00.A0A();
        }
    }
}
