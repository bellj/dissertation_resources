package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.quickcontact.QuickContactActivity;

/* renamed from: X.2Un  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51402Un extends AbstractC51412Uo {
    public View A00;
    public View A01;
    public TextView A02;
    public WaImageButton A03;
    public final /* synthetic */ QuickContactActivity A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C51402Un(QuickContactActivity quickContactActivity) {
        super(quickContactActivity);
        this.A04 = quickContactActivity;
    }

    @Override // X.AbstractC51412Uo
    public void A04() {
        super.A04();
        QuickContactActivity quickContactActivity = this.A04;
        quickContactActivity.A07.setContentDescription(quickContactActivity.getResources().getString(R.string.profile_photo));
    }
}
