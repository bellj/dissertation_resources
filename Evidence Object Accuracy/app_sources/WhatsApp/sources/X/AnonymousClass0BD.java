package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.ListView;

/* renamed from: X.0BD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BD extends ViewGroup implements AbstractC016507u, AnonymousClass02E {
    public static final int[] A0Z = {16842766};
    public float A00;
    public float A01;
    public float A02 = -1.0f;
    public float A03;
    public int A04 = -1;
    public int A05;
    public int A06 = -1;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public View A0E;
    public Animation.AnimationListener A0F = new animation.Animation$AnimationListenerC07000Wh(this);
    public Animation A0G;
    public Animation A0H;
    public Animation A0I;
    public Animation A0J;
    public C02350Bi A0K;
    public AnonymousClass0A7 A0L;
    public AbstractC11370gB A0M;
    public AbstractC11900h3 A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R = false;
    public final Animation A0S = new AnonymousClass0BH(this);
    public final Animation A0T = new AnonymousClass0BI(this);
    public final DecelerateInterpolator A0U;
    public final AnonymousClass0UY A0V;
    public final C04740Mw A0W;
    public final int[] A0X = new int[2];
    public final int[] A0Y = new int[2];

    public AnonymousClass0BD(Context context) {
        super(context, null);
        this.A0D = ViewConfiguration.get(context).getScaledTouchSlop();
        this.A0A = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.A0U = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.A05 = (int) (displayMetrics.density * 40.0f);
        this.A0K = new C02350Bi(getContext());
        AnonymousClass0A7 r1 = new AnonymousClass0A7(getContext());
        this.A0L = r1;
        r1.A00(1);
        this.A0K.setImageDrawable(this.A0L);
        this.A0K.setVisibility(8);
        addView(this.A0K);
        setChildrenDrawingOrderEnabled(true);
        int i = (int) (displayMetrics.density * 64.0f);
        this.A0C = i;
        this.A02 = (float) i;
        this.A0W = new C04740Mw();
        this.A0V = new AnonymousClass0UY(this);
        setNestedScrollingEnabled(true);
        int i2 = -this.A05;
        this.A07 = i2;
        this.A0B = i2;
        int i3 = this.A09;
        setTargetOffsetTopAndBottom((i3 + ((int) (((float) (i2 - i3)) * 1.0f))) - this.A0K.getTop());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, A0Z);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    public static boolean A00(ListView listView) {
        if (Build.VERSION.SDK_INT >= 19) {
            return C05790Qz.A01(listView, -1);
        }
        if (listView.getChildCount() == 0) {
            return false;
        }
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int top = listView.getChildAt(0).getTop();
        if (firstVisiblePosition > 0 || top < listView.getListPaddingTop()) {
            return true;
        }
        return false;
    }

    public void A01() {
        C02350Bi r2 = this.A0K;
        r2.clearAnimation();
        this.A0L.stop();
        r2.setVisibility(8);
        setColorViewAlpha(255);
        setTargetOffsetTopAndBottom(this.A0B - this.A07);
        this.A07 = r2.getTop();
    }

    public final void A02() {
        if (this.A0E == null) {
            for (int i = 0; i < getChildCount(); i++) {
                View childAt = getChildAt(i);
                if (!childAt.equals(this.A0K)) {
                    this.A0E = childAt;
                    return;
                }
            }
        }
    }

    public final void A03(float f) {
        if (f > this.A02) {
            A06(true, true);
            return;
        }
        this.A0R = false;
        AnonymousClass0A7 r5 = this.A0L;
        AnonymousClass0OB r4 = r5.A05;
        r4.A04 = 0.0f;
        r4.A01 = 0.0f;
        r5.invalidateSelf();
        animation.Animation$AnimationListenerC07010Wi r3 = new animation.Animation$AnimationListenerC07010Wi(this);
        this.A09 = this.A07;
        Animation animation = this.A0T;
        animation.reset();
        animation.setDuration(200);
        animation.setInterpolator(this.A0U);
        C02350Bi r0 = this.A0K;
        r0.A01 = r3;
        r0.clearAnimation();
        r0.startAnimation(animation);
        if (r4.A0F) {
            r4.A0F = false;
        }
        r5.invalidateSelf();
    }

    public final void A04(float f) {
        Animation animation;
        Animation animation2;
        AnonymousClass0A7 r8 = this.A0L;
        AnonymousClass0OB r5 = r8.A05;
        if (!r5.A0F) {
            r5.A0F = true;
        }
        r8.invalidateSelf();
        float f2 = this.A02;
        float min = Math.min(1.0f, Math.abs(f / f2));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f) - f2;
        int i = this.A08;
        if (i <= 0) {
            i = this.A0C;
        }
        float f3 = (float) i;
        double max2 = (double) (Math.max(0.0f, Math.min(abs, f3 * 2.0f) / f3) / 4.0f);
        float pow = ((float) (max2 - Math.pow(max2, 2.0d))) * 2.0f;
        int i2 = this.A0B + ((int) ((f3 * min) + (f3 * pow * 2.0f)));
        C02350Bi r7 = this.A0K;
        if (r7.getVisibility() != 0) {
            r7.setVisibility(0);
        }
        r7.setScaleX(1.0f);
        r7.setScaleY(1.0f);
        int i3 = (f > this.A02 ? 1 : (f == this.A02 ? 0 : -1));
        int i4 = r5.A09;
        if (i3 < 0) {
            if (i4 > 76 && ((animation2 = this.A0H) == null || !animation2.hasStarted() || animation2.hasEnded())) {
                AnonymousClass0BJ r2 = new AnonymousClass0BJ(this, r5.A09, 76);
                r2.setDuration(300);
                r7.A01 = null;
                r7.clearAnimation();
                r7.startAnimation(r2);
                this.A0H = r2;
            }
        } else if (i4 < 255 && ((animation = this.A0G) == null || !animation.hasStarted() || animation.hasEnded())) {
            AnonymousClass0BJ r22 = new AnonymousClass0BJ(this, r5.A09, 255);
            r22.setDuration(300);
            r7.A01 = null;
            r7.clearAnimation();
            r7.startAnimation(r22);
            this.A0G = r22;
        }
        float min2 = Math.min(0.8f, max * 0.8f);
        r5.A04 = 0.0f;
        r5.A01 = min2;
        r8.invalidateSelf();
        float min3 = Math.min(1.0f, max);
        if (min3 != r5.A00) {
            r5.A00 = min3;
        }
        r8.invalidateSelf();
        r5.A03 = (((max * 0.4f) - 16.0f) + (pow * 2.0f)) * 0.5f;
        r8.invalidateSelf();
        setTargetOffsetTopAndBottom(i2 - this.A07);
    }

    public final void A05(float f) {
        float f2 = this.A00;
        float f3 = (float) this.A0D;
        if (f - f2 > f3 && !this.A0O) {
            this.A01 = f2 + f3;
            this.A0O = true;
            this.A0L.setAlpha(76);
        }
    }

    public final void A06(boolean z, boolean z2) {
        if (this.A0R != z) {
            this.A0Q = z2;
            A02();
            this.A0R = z;
            if (z) {
                int i = this.A07;
                Animation.AnimationListener animationListener = this.A0F;
                this.A09 = i;
                Animation animation = this.A0S;
                animation.reset();
                animation.setDuration(200);
                animation.setInterpolator(this.A0U);
                if (animationListener != null) {
                    this.A0K.A01 = animationListener;
                }
                C02350Bi r0 = this.A0K;
                r0.clearAnimation();
                r0.startAnimation(animation);
                return;
            }
            Animation.AnimationListener animationListener2 = this.A0F;
            AnonymousClass0BG r2 = new AnonymousClass0BG(this);
            this.A0J = r2;
            r2.setDuration(150);
            C02350Bi r1 = this.A0K;
            r1.A01 = animationListener2;
            r1.clearAnimation();
            r1.startAnimation(this.A0J);
        }
    }

    @Override // android.view.View
    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.A0V.A02(f, f2, z);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.A0V.A01(f, f2);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.A0V.A04(iArr, iArr2, i, i2, 0);
    }

    @Override // android.view.View
    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.A0V.A05(iArr, null, i, i2, i3, i4, 0);
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i, int i2) {
        int i3 = this.A06;
        if (i3 < 0) {
            return i2;
        }
        if (i2 == i - 1) {
            return i3;
        }
        return i2 >= i3 ? i2 + 1 : i2;
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        C04740Mw r0 = this.A0W;
        return r0.A01 | r0.A00;
    }

    public int getProgressCircleDiameter() {
        return this.A05;
    }

    public int getProgressViewEndOffset() {
        return this.A0C;
    }

    public int getProgressViewStartOffset() {
        return this.A0B;
    }

    @Override // android.view.View
    public boolean hasNestedScrollingParent() {
        return this.A0V.A01 != null;
    }

    @Override // android.view.View, X.AnonymousClass02E
    public boolean isNestedScrollingEnabled() {
        return this.A0V.A02;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A01();
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean canScrollVertically;
        A02();
        int actionMasked = motionEvent.getActionMasked();
        if (isEnabled()) {
            View view = this.A0E;
            if (view instanceof ListView) {
                canScrollVertically = A00((ListView) view);
            } else {
                canScrollVertically = view.canScrollVertically(-1);
            }
            if (!canScrollVertically && !this.A0R && !this.A0P) {
                if (actionMasked != 0) {
                    if (actionMasked != 1) {
                        if (actionMasked == 2) {
                            int i = this.A04;
                            if (i == -1) {
                                Log.e("SwipeRefreshLayout", "Got ACTION_MOVE event but don't have an active pointer id.");
                                return false;
                            }
                            int findPointerIndex = motionEvent.findPointerIndex(i);
                            if (findPointerIndex >= 0) {
                                A05(motionEvent.getY(findPointerIndex));
                            }
                        } else if (actionMasked != 3) {
                            if (actionMasked == 6) {
                                int actionIndex = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex) == this.A04) {
                                    int i2 = 0;
                                    if (actionIndex == 0) {
                                        i2 = 1;
                                    }
                                    this.A04 = motionEvent.getPointerId(i2);
                                }
                            }
                        }
                        return this.A0O;
                    }
                    this.A0O = false;
                    this.A04 = -1;
                    return this.A0O;
                }
                setTargetOffsetTopAndBottom(this.A0B - this.A0K.getTop());
                int pointerId = motionEvent.getPointerId(0);
                this.A04 = pointerId;
                this.A0O = false;
                int findPointerIndex2 = motionEvent.findPointerIndex(pointerId);
                if (findPointerIndex2 >= 0) {
                    this.A00 = motionEvent.getY(findPointerIndex2);
                    return this.A0O;
                }
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.A0E == null) {
                A02();
            }
            View view = this.A0E;
            if (view != null) {
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                C02350Bi r5 = this.A0K;
                int measuredWidth2 = r5.getMeasuredWidth();
                int measuredHeight2 = r5.getMeasuredHeight();
                int i5 = measuredWidth >> 1;
                int i6 = measuredWidth2 >> 1;
                int i7 = this.A07;
                r5.layout(i5 - i6, i7, i5 + i6, measuredHeight2 + i7);
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A0E == null) {
            A02();
        }
        View view = this.A0E;
        if (view != null) {
            view.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            C02350Bi r2 = this.A0K;
            r2.measure(View.MeasureSpec.makeMeasureSpec(this.A05, 1073741824), View.MeasureSpec.makeMeasureSpec(this.A05, 1073741824));
            this.A06 = -1;
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                if (getChildAt(i3) == r2) {
                    this.A06 = i3;
                    return;
                }
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        return dispatchNestedFling(f, f2, z);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedPreFling(View view, float f, float f2) {
        return this.A0V.A01(f, f2);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        float f;
        if (i2 > 0) {
            float f2 = this.A03;
            if (f2 > 0.0f) {
                float f3 = (float) i2;
                if (f3 > f2) {
                    iArr[1] = i2 - ((int) f2);
                    this.A03 = 0.0f;
                    f = 0.0f;
                } else {
                    f = f2 - f3;
                    this.A03 = f;
                    iArr[1] = i2;
                }
                A04(f);
            }
        }
        int[] iArr2 = this.A0Y;
        if (dispatchNestedPreScroll(i - iArr[0], i2 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr[1] + iArr2[1];
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        boolean canScrollVertically;
        int[] iArr = this.A0X;
        dispatchNestedScroll(i, i2, i3, i4, iArr);
        int i5 = i4 + iArr[1];
        if (i5 < 0) {
            View view2 = this.A0E;
            if (view2 instanceof ListView) {
                canScrollVertically = A00((ListView) view2);
            } else {
                canScrollVertically = view2.canScrollVertically(-1);
            }
            if (!canScrollVertically) {
                float abs = this.A03 + ((float) Math.abs(i5));
                this.A03 = abs;
                A04(abs);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.A0W.A01 = i;
        startNestedScroll(i & 2);
        this.A03 = 0.0f;
        this.A0P = true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onStartNestedScroll(View view, View view2, int i) {
        return isEnabled() && !this.A0R && (i & 2) != 0;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onStopNestedScroll(View view) {
        this.A0W.A01 = 0;
        this.A0P = false;
        float f = this.A03;
        if (f > 0.0f) {
            A03(f);
            this.A03 = 0.0f;
        }
        stopNestedScroll();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean canScrollVertically;
        String str;
        String str2;
        int actionIndex;
        int actionMasked = motionEvent.getActionMasked();
        if (isEnabled()) {
            View view = this.A0E;
            if (view instanceof ListView) {
                canScrollVertically = A00((ListView) view);
            } else {
                canScrollVertically = view.canScrollVertically(-1);
            }
            if (!canScrollVertically && !this.A0R && !this.A0P) {
                if (actionMasked == 0) {
                    this.A04 = motionEvent.getPointerId(0);
                    this.A0O = false;
                    return true;
                } else if (actionMasked == 1) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.A04);
                    if (findPointerIndex < 0) {
                        str = "SwipeRefreshLayout";
                        str2 = "Got ACTION_UP event but don't have an active pointer id.";
                        Log.e(str, str2);
                        return false;
                    }
                    if (this.A0O) {
                        this.A0O = false;
                        A03((motionEvent.getY(findPointerIndex) - this.A01) * 0.5f);
                    }
                    this.A04 = -1;
                } else if (actionMasked == 2) {
                    int findPointerIndex2 = motionEvent.findPointerIndex(this.A04);
                    if (findPointerIndex2 < 0) {
                        str = "SwipeRefreshLayout";
                        str2 = "Got ACTION_MOVE event but have an invalid active pointer id.";
                        Log.e(str, str2);
                        return false;
                    }
                    float y = motionEvent.getY(findPointerIndex2);
                    A05(y);
                    if (this.A0O) {
                        float f = (y - this.A01) * 0.5f;
                        if (f > 0.0f) {
                            A04(f);
                            return true;
                        }
                    }
                    return true;
                } else if (actionMasked != 3) {
                    if (actionMasked != 5) {
                        if (actionMasked == 6) {
                            int actionIndex2 = motionEvent.getActionIndex();
                            if (motionEvent.getPointerId(actionIndex2) == this.A04) {
                                actionIndex = 0;
                                if (actionIndex2 == 0) {
                                    actionIndex = 1;
                                }
                            }
                        }
                        return true;
                    }
                    actionIndex = motionEvent.getActionIndex();
                    if (actionIndex < 0) {
                        str = "SwipeRefreshLayout";
                        str2 = "Got ACTION_POINTER_DOWN event but have an invalid action index.";
                        Log.e(str, str2);
                        return false;
                    }
                    this.A04 = motionEvent.getPointerId(actionIndex);
                    return true;
                }
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (Build.VERSION.SDK_INT >= 21 || !(this.A0E instanceof AbsListView)) {
            View view = this.A0E;
            if (view == null || AnonymousClass028.A0s(view)) {
                super.requestDisallowInterceptTouchEvent(z);
            }
        }
    }

    public void setAnimationProgress(float f) {
        C02350Bi r0 = this.A0K;
        r0.setScaleX(f);
        r0.setScaleY(f);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeColors(int... iArr) {
        A02();
        AnonymousClass0A7 r3 = this.A0L;
        AnonymousClass0OB r2 = r3.A05;
        r2.A0G = iArr;
        r2.A0C = 0;
        int i = iArr[0];
        r2.A0D = i;
        r2.A0C = 0;
        r2.A0D = i;
        r3.invalidateSelf();
    }

    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int length = iArr.length;
        int[] iArr2 = new int[length];
        for (int i = 0; i < length; i++) {
            iArr2[i] = AnonymousClass00T.A00(context, iArr[i]);
        }
        setColorSchemeColors(iArr2);
    }

    private void setColorViewAlpha(int i) {
        this.A0K.getBackground().setAlpha(i);
        this.A0L.setAlpha(i);
    }

    public void setDistanceToTriggerSync(int i) {
        this.A02 = (float) i;
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (!z) {
            A01();
        }
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void setNestedScrollingEnabled(boolean z) {
        AnonymousClass0UY r1 = this.A0V;
        if (r1.A02) {
            AnonymousClass028.A0T(r1.A04);
        }
        r1.A02 = z;
    }

    public void setOnChildScrollUpCallback(AbstractC11370gB r1) {
        this.A0M = r1;
    }

    public void setOnRefreshListener(AbstractC11900h3 r1) {
        this.A0N = r1;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i) {
        setProgressBackgroundColorSchemeResource(i);
    }

    public void setProgressBackgroundColorSchemeColor(int i) {
        this.A0K.setBackgroundColor(i);
    }

    public void setProgressBackgroundColorSchemeResource(int i) {
        setProgressBackgroundColorSchemeColor(AnonymousClass00T.A00(getContext(), i));
    }

    public void setRefreshing(boolean z) {
        if (!z || this.A0R == z) {
            A06(z, false);
            return;
        }
        this.A0R = z;
        setTargetOffsetTopAndBottom((this.A0C + this.A0B) - this.A07);
        this.A0Q = false;
        Animation.AnimationListener animationListener = this.A0F;
        C02350Bi r3 = this.A0K;
        r3.setVisibility(0);
        this.A0L.setAlpha(255);
        AnonymousClass0BF r2 = new AnonymousClass0BF(this);
        this.A0I = r2;
        r2.setDuration((long) this.A0A);
        if (animationListener != null) {
            r3.A01 = animationListener;
        }
        r3.clearAnimation();
        r3.startAnimation(this.A0I);
    }

    public void setSize(int i) {
        if (i == 0 || i == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            float f = 40.0f;
            if (i == 0) {
                f = 56.0f;
            }
            this.A05 = (int) (displayMetrics.density * f);
            C02350Bi r1 = this.A0K;
            r1.setImageDrawable(null);
            AnonymousClass0A7 r0 = this.A0L;
            r0.A00(i);
            r1.setImageDrawable(r0);
        }
    }

    public void setSlingshotDistance(int i) {
        this.A08 = i;
    }

    public void setTargetOffsetTopAndBottom(int i) {
        C02350Bi r0 = this.A0K;
        r0.bringToFront();
        AnonymousClass028.A0Y(r0, i);
        this.A07 = r0.getTop();
    }

    @Override // android.view.View
    public boolean startNestedScroll(int i) {
        return this.A0V.A03(i, 0);
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void stopNestedScroll() {
        this.A0V.A00(0);
    }
}
