package X;

/* renamed from: X.3tN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81043tN extends AnonymousClass4Y8 {
    @Override // X.AnonymousClass4Y8
    public C81063tP build() {
        return (C81063tP) super.build();
    }

    @Override // X.AnonymousClass4Y8
    public C81043tN putAll(Object obj, Iterable iterable) {
        super.putAll(obj, iterable);
        return this;
    }

    @Override // X.AnonymousClass4Y8
    public C81043tN putAll(Object obj, Object... objArr) {
        super.putAll(obj, objArr);
        return this;
    }
}
