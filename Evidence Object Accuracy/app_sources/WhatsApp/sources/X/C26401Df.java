package X;

import com.whatsapp.voipcalling.Voip;
import org.json.JSONObject;

/* renamed from: X.1Df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26401Df extends AnonymousClass1DY {
    public final C26391De A00;
    public final C20970wc A01;
    public final AnonymousClass13C A02;

    public C26401Df(C26391De r1, C20970wc r2, AnonymousClass13C r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public String A00(C15430nF r3, JSONObject jSONObject) {
        if (!this.A02.A02(r3, jSONObject.getJSONObject("payload").getString("call_id")).equals(Voip.getCurrentCallId())) {
            return AnonymousClass1DY.A00(17, "There is no call with that call id");
        }
        this.A01.A00(this.A00);
        return AnonymousClass1DY.A01(null);
    }
}
