package X;

/* renamed from: X.309  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass309 extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass309() {
        super(2524, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdWifiSpeedbump {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "estimatedSizeOfCompressedPayloadKb", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
