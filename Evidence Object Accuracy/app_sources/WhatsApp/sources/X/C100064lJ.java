package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100064lJ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29991Vn(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29991Vn[i];
    }
}
