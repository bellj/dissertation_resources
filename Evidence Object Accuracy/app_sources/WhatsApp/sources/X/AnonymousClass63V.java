package X;

import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import com.whatsapp.camera.litecamera.LiteCameraView;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.63V  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63V implements Handler.Callback {
    public static void A00(AnonymousClass60Q r7) {
        if (r7 != null) {
            int A05 = C12960it.A05(r7.A00(AnonymousClass60Q.A0J));
            int A052 = C12960it.A05(r7.A00(AnonymousClass60Q.A0I));
            String str = (String) r7.A01(AnonymousClass60Q.A0M);
            int A053 = C12960it.A05(r7.A00(AnonymousClass60Q.A0K));
            int A054 = C12960it.A05(r7.A00(AnonymousClass60Q.A0H));
            int i = 0;
            if (A054 != 0) {
                i = 1;
                if (1 != A054) {
                    throw C12990iw.A0m(C12960it.A0W(A054, "Could not convert camera facing from optic: "));
                }
            }
            if (C12970iu.A1Y(r7.A01(AnonymousClass60Q.A0N))) {
                ((Number) r7.A01(AnonymousClass60Q.A0L)).intValue();
            }
            ((Number) r7.A01(AnonymousClass60Q.A0R)).intValue();
            new C129635y3(str, A05, A052, A053, i);
        }
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        C129845yO r0;
        AbstractC467527b r1;
        int i;
        switch (message.what) {
            case 1:
                List list = (List) message.obj;
                for (int i2 = 0; i2 < list.size(); i2++) {
                    LiteCameraView liteCameraView = ((AnonymousClass4K1) list.get(i2)).A00;
                    liteCameraView.A0J = true;
                    liteCameraView.A07 = false;
                    liteCameraView.A01();
                    AbstractC467527b r02 = liteCameraView.A00;
                    if (r02 != null) {
                        r02.AUF();
                    }
                }
                return false;
            case 2:
                List list2 = (List) message.obj;
                for (int i3 = 0; i3 < list2.size(); i3++) {
                    LiteCameraView liteCameraView2 = ((AnonymousClass4K1) list2.get(i3)).A00;
                    C12970iu.A1B(liteCameraView2.A0B.edit(), "camera_facing", liteCameraView2.A0D.A00);
                    LiteCameraView liteCameraView3 = ((AnonymousClass4K1) list2.get(i3)).A00;
                    liteCameraView3.A0J = true;
                    liteCameraView3.A07 = false;
                    liteCameraView3.A01();
                    AbstractC467527b r03 = liteCameraView3.A00;
                    if (r03 != null) {
                        r03.AUF();
                    }
                }
                return false;
            case 3:
                List list3 = (List) ((Object[]) message.obj)[0];
                for (int i4 = 0; i4 < list3.size(); i4++) {
                    LiteCameraView liteCameraView4 = ((AnonymousClass4K1) list3.get(i4)).A00;
                    liteCameraView4.A0J = false;
                    if (!liteCameraView4.A07) {
                        liteCameraView4.A07 = true;
                        liteCameraView4.pause();
                        liteCameraView4.Aar();
                    } else {
                        AbstractC467527b r04 = liteCameraView4.A00;
                        if (r04 != null) {
                            r04.ANb(1);
                        }
                    }
                }
                return false;
            case 4:
                List list4 = (List) ((Object[]) message.obj)[0];
                for (int i5 = 0; i5 < list4.size(); i5++) {
                    LiteCameraView liteCameraView5 = ((AnonymousClass4K1) list4.get(i5)).A00;
                    liteCameraView5.A0J = false;
                    AbstractC467527b r05 = liteCameraView5.A00;
                    if (r05 != null) {
                        r05.ANb(2);
                    }
                }
                return false;
            case 5:
                ((AnonymousClass4NI) message.obj).A00.onShutter();
                return false;
            case 6:
                Object[] objArr = (Object[]) message.obj;
                AnonymousClass4NI r5 = (AnonymousClass4NI) objArr[0];
                byte[] bArr = (byte[]) objArr[1];
                AnonymousClass60J r3 = (AnonymousClass60J) objArr[2];
                if (r3 != null) {
                    r3.A00(AnonymousClass60J.A0N);
                    r3.A00(AnonymousClass60J.A0O);
                    r3.A00(AnonymousClass60J.A0M);
                    int A05 = C12960it.A05(r3.A00(AnonymousClass60J.A0L));
                    if (A05 == 0 || 1 == A05) {
                        r3.A01(AnonymousClass60J.A0U);
                        r3.A01(AnonymousClass60J.A0b);
                        r3.A01(AnonymousClass60J.A0P);
                        r3.A01(AnonymousClass60J.A0W);
                        r3.A01(AnonymousClass60J.A0Q);
                        r3.A01(AnonymousClass60J.A0X);
                        r3.A01(AnonymousClass60J.A0S);
                    } else {
                        throw C12990iw.A0m(C12960it.A0W(A05, "Could not convert camera facing from optic: "));
                    }
                }
                r5.A00.ATk(bArr, r5.A01.AJS());
                return false;
            case 7:
                r1 = ((AnonymousClass4NI) ((Object[]) message.obj)[0]).A01.A00;
                if (r1 != null) {
                    i = 3;
                    r1.ANb(i);
                    return false;
                }
                return false;
            case 8:
                Object[] objArr2 = (Object[]) message.obj;
                A00((AnonymousClass60Q) objArr2[1]);
                AbstractC467527b r06 = ((AnonymousClass4K3) objArr2[0]).A00.A00;
                if (r06 != null) {
                    r06.AYD();
                    return false;
                }
                return false;
            case 9:
                A00((AnonymousClass60Q) ((Object[]) message.obj)[1]);
                return false;
            case 10:
                r1 = ((AnonymousClass4K3) ((Object[]) message.obj)[0]).A00.A00;
                if (r1 != null) {
                    i = 4;
                    r1.ANb(i);
                    return false;
                }
                return false;
            case 11:
                Object[] objArr3 = (Object[]) message.obj;
                Point point = (Point) objArr3[1];
                AbstractC467527b r32 = ((AnonymousClass4K4) objArr3[0]).A00.A00;
                if (r32 != null) {
                    r32.AMh((float) point.x, (float) point.y);
                    return false;
                }
                return false;
            case 12:
                LiteCameraView liteCameraView6 = ((AnonymousClass4K4) ((Object[]) message.obj)[0]).A00;
                liteCameraView6.A0D.A0B = null;
                AbstractC467527b r07 = liteCameraView6.A00;
                if (r07 != null) {
                    r07.AMi(true);
                    return false;
                }
                return false;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                LiteCameraView liteCameraView7 = ((AnonymousClass4K4) message.obj).A00;
                liteCameraView7.A0D.A0B = null;
                AbstractC467527b r08 = liteCameraView7.A00;
                if (r08 != null) {
                    r08.AMi(false);
                    return false;
                }
                return false;
            case 15:
                Object[] objArr4 = (Object[]) message.obj;
                AnonymousClass643 r33 = (AnonymousClass643) objArr4[0];
                C127255uC r4 = (C127255uC) objArr4[1];
                int A052 = C12960it.A05(objArr4[2]);
                int A053 = C12960it.A05(objArr4[3]);
                if (A052 > 0 && A053 > 0 && (r0 = (C129845yO) r4.A02.A03(AbstractC130685zo.A0m)) != null) {
                    Matrix matrix = new Matrix();
                    AbstractC1311761o r52 = r33.A0N;
                    r52.AdG(matrix, A052, A053, r0.A02, r0.A01, r33.A0C);
                    r52.AIr(matrix, A052, A053, r4.A00);
                    if (!AnonymousClass66P.A0C) {
                        r33.A0J.setTransform(matrix);
                        return false;
                    }
                }
                return false;
            default:
                return false;
        }
    }
}
