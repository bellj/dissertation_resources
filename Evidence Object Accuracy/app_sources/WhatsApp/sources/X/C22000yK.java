package X;

import android.os.Handler;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0yK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22000yK {
    public boolean A00 = false;
    public final int A01;
    public final RunnableBRunnable0Shape13S0100000_I0_13 A02 = new RunnableBRunnable0Shape13S0100000_I0_13(this);
    public final AbstractC15710nm A03;
    public final C14830m7 A04;
    public final C15880o3 A05;
    public final C16490p7 A06;
    public final AbstractC14440lR A07;
    public final Runnable A08;
    public final List A09 = new ArrayList();
    public final Map A0A = new HashMap();
    public final Map A0B = new HashMap();
    public final Map A0C = new HashMap();

    public C22000yK(AbstractC15710nm r3, C14830m7 r4, C15880o3 r5, C16490p7 r6, C14850m9 r7, AbstractC14440lR r8) {
        RunnableBRunnable0Shape0S0000000_I0 runnableBRunnable0Shape0S0000000_I0 = new RunnableBRunnable0Shape0S0000000_I0(10);
        this.A04 = r4;
        this.A03 = r3;
        this.A07 = r8;
        this.A05 = r5;
        this.A06 = r6;
        this.A01 = r7.A02(757) * 1000;
        this.A08 = runnableBRunnable0Shape0S0000000_I0;
    }

    public void A00() {
        synchronized (this) {
            this.A00 = false;
            for (Map.Entry entry : this.A0A.entrySet()) {
                Handler handler = (Handler) entry.getKey();
                this.A0B.put(handler, Boolean.TRUE);
                handler.postAtFrontOfQueue((Runnable) entry.getValue());
            }
        }
        this.A07.AbK(this.A02, "StuckDbHandlerThreadDetector/monitor", 120000);
    }

    public void A01(Handler handler) {
        synchronized (this) {
            this.A0A.put(handler, new RunnableBRunnable0Shape8S0200000_I0_8(handler, this));
        }
    }

    public final void A02(String str) {
        C15880o3 r2 = this.A05;
        boolean equals = Boolean.TRUE.equals(r2.A03.A01());
        boolean z = false;
        if (System.currentTimeMillis() - r2.A06() < 240000) {
            z = true;
        }
        if (equals || z) {
            this.A07.AbK(this.A02, "StuckDbHandlerThreadDetector/heartbeat", 120000);
            return;
        }
        int i = this.A01;
        if (i > 0) {
            long uptimeMillis = SystemClock.uptimeMillis();
            Map map = this.A0C;
            if (!map.containsKey(str)) {
                map.put(str, Long.valueOf(uptimeMillis));
                this.A07.AbK(this.A02, "StuckDbHandlerThreadDetector/recovery", (long) i);
            } else if (uptimeMillis - ((Number) map.get(str)).longValue() >= ((long) i)) {
                this.A08.run();
            }
        }
        if (!this.A00) {
            StringBuilder sb = new StringBuilder("StuckDbHandlerThreadDetector/not responsive, debugName:");
            sb.append(str);
            sb.append(" msgStoreReadLock:");
            sb.append((String) null);
            Log.w(sb.toString());
            AnonymousClass01I.A00();
            this.A03.AaV("db-thread-stuck", str, false);
            this.A00 = true;
        }
    }
}
