package X;

import android.view.ViewGroup;
import android.view.animation.Animation;

/* renamed from: X.3xJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83483xJ extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C14680lr A00;

    public C83483xJ(C14680lr r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        ViewGroup viewGroup = this.A00.A04;
        viewGroup.setVisibility(4);
        viewGroup.clearAnimation();
    }
}
