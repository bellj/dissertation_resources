package X;

/* renamed from: X.0Pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05410Pl {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;

    public C05410Pl(boolean z, boolean z2, boolean z3, boolean z4) {
        this.A00 = z;
        this.A03 = z2;
        this.A01 = z3;
        this.A02 = z4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C05410Pl)) {
            return false;
        }
        C05410Pl r4 = (C05410Pl) obj;
        if (this.A00 == r4.A00 && this.A03 == r4.A03 && this.A01 == r4.A01 && this.A02 == r4.A02) {
            return true;
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int hashCode() {
        /*
            r2 = this;
            boolean r1 = r2.A00
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x0008
            int r1 = r1 + 16
        L_0x0008:
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x000e
            int r1 = r1 + 256
        L_0x000e:
            boolean r0 = r2.A02
            if (r0 == 0) goto L_0x0014
            int r1 = r1 + 4096
        L_0x0014:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05410Pl.hashCode():int");
    }

    public String toString() {
        return String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", Boolean.valueOf(this.A00), Boolean.valueOf(this.A03), Boolean.valueOf(this.A01), Boolean.valueOf(this.A02));
    }
}
