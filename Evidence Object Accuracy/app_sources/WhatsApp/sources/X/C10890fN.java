package X;

import android.os.Build;
import com.whatsapp.util.Log;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.SecureRandomSpi;

/* renamed from: X.0fN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10890fN extends SecureRandomSpi {
    public static DataInputStream A00;
    public static OutputStream A01;
    public static final File A02 = new File("/dev/urandom");
    public static final Object A03 = new Object();
    public boolean seeded;

    @Override // java.security.SecureRandomSpi
    public byte[] engineGenerateSeed(int i) {
        byte[] bArr = new byte[i];
        engineNextBytes(bArr);
        return bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[Catch: all -> 0x0049, TRY_ENTER, TryCatch #4 {IOException -> 0x0052, blocks: (B:5:0x000b, B:6:0x000d, B:16:0x0043, B:17:0x0044, B:18:0x0047, B:7:0x000e, B:15:0x0042, B:22:0x004e), top: B:29:0x000b }] */
    @Override // java.security.SecureRandomSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void engineNextBytes(byte[] r5) {
        /*
            r4 = this;
            boolean r0 = r4.seeded
            if (r0 != 0) goto L_0x000b
            byte[] r0 = X.AnonymousClass00F.A01()
            r4.engineSetSeed(r0)
        L_0x000b:
            java.lang.Object r3 = X.C10890fN.A03     // Catch: IOException -> 0x0052
            monitor-enter(r3)     // Catch: IOException -> 0x0052
            monitor-enter(r3)     // Catch: all -> 0x004f
            java.io.DataInputStream r1 = X.C10890fN.A00     // Catch: all -> 0x004c
            if (r1 != 0) goto L_0x0041
            java.io.File r1 = X.C10890fN.A02     // Catch: IOException -> 0x0022, all -> 0x004c
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch: IOException -> 0x0022, all -> 0x004c
            r0.<init>(r1)     // Catch: IOException -> 0x0022, all -> 0x004c
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch: IOException -> 0x0022, all -> 0x004c
            r1.<init>(r0)     // Catch: IOException -> 0x0022, all -> 0x004c
            X.C10890fN.A00 = r1     // Catch: IOException -> 0x0022, all -> 0x004c
            goto L_0x0041
        L_0x0022:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x004c
            r1.<init>()     // Catch: all -> 0x004c
            java.lang.String r0 = "Failed to open "
            r1.append(r0)     // Catch: all -> 0x004c
            java.io.File r0 = X.C10890fN.A02     // Catch: all -> 0x004c
            r1.append(r0)     // Catch: all -> 0x004c
            java.lang.String r0 = " for reading"
            r1.append(r0)     // Catch: all -> 0x004c
            java.lang.String r1 = r1.toString()     // Catch: all -> 0x004c
            java.lang.SecurityException r0 = new java.lang.SecurityException     // Catch: all -> 0x004c
            r0.<init>(r1, r2)     // Catch: all -> 0x004c
            throw r0     // Catch: all -> 0x004c
        L_0x0041:
            monitor-exit(r3)     // Catch: all -> 0x004c
            monitor-exit(r3)     // Catch: all -> 0x004f
            monitor-enter(r1)     // Catch: IOException -> 0x0052
            r1.readFully(r5)     // Catch: all -> 0x0049
            monitor-exit(r1)     // Catch: all -> 0x0049
            return
        L_0x0049:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0049
            throw r0     // Catch: IOException -> 0x0052
        L_0x004c:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x004c
            throw r0     // Catch: all -> 0x004f
        L_0x004f:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x004f
            throw r0     // Catch: IOException -> 0x0052
        L_0x0052:
            r2 = move-exception
            java.lang.String r0 = "Failed to read from "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.io.File r0 = X.C10890fN.A02
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.SecurityException r0 = new java.lang.SecurityException
            r0.<init>(r1, r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10890fN.engineNextBytes(byte[]):void");
    }

    @Override // java.security.SecureRandomSpi
    public void engineSetSeed(byte[] bArr) {
        OutputStream outputStream;
        if ("samsung".equalsIgnoreCase(Build.MANUFACTURER)) {
            this.seeded = true;
            return;
        }
        try {
            synchronized (A03) {
                outputStream = A01;
                if (outputStream == null) {
                    outputStream = new FileOutputStream(A02);
                    A01 = outputStream;
                }
            }
            outputStream.write(bArr);
            outputStream.flush();
            this.seeded = true;
        } catch (IOException e) {
            Log.w("unable to seed PRNG", e);
        }
    }
}
