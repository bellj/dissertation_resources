package X;

import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.components.Button;

/* renamed from: X.3e5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72143e5 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ BlockReasonListFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72143e5(BlockReasonListFragment blockReasonListFragment) {
        super(1);
        this.this$0 = blockReasonListFragment;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Button button = this.this$0.A03;
        if (button == null) {
            throw C16700pc.A06("blockButton");
        }
        button.setEnabled(true);
        return AnonymousClass1WZ.A00;
    }
}
