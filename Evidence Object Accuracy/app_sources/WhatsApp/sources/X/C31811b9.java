package X;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.LinkedList;

/* renamed from: X.1b9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31811b9 {
    public final C31401aU A00;

    public C31811b9(C31401aU r1) {
        this.A00 = r1;
    }

    public C31821bA A00(C31351aP r15) {
        C31821bA r0;
        synchronized (C31411aV.A02) {
            try {
                C31401aU r5 = this.A00;
                C29271Rn A00 = r5.A00(r15);
                LinkedList linkedList = A00.A00;
                if (linkedList.isEmpty() || (1 << 3) != new C31501ae(A00.A00().A00.A02).A01.length) {
                    try {
                        byte[] bArr = new byte[32];
                        SecureRandom.getInstance("SHA1PRNG").nextBytes(bArr);
                        C31501ae r2 = new C31501ae(bArr);
                        try {
                            int nextInt = SecureRandom.getInstance("SHA1PRNG").nextInt(Integer.MAX_VALUE);
                            byte[][] bArr2 = r2.A01;
                            C31731b1 A01 = C31481ac.A01();
                            linkedList.clear();
                            linkedList.add(new C31451aZ(A01.A01, new C31691ax(A01.A00), bArr2, nextInt, 0));
                            r5.A01(r15, A00);
                        } catch (NoSuchAlgorithmException e) {
                            throw new AssertionError(e);
                        }
                    } catch (NoSuchAlgorithmException e2) {
                        throw new AssertionError(e2);
                    }
                }
                C31451aZ A002 = A00.A00();
                C31461aa r02 = A002.A00;
                int i = r02.A01;
                int i2 = new C31501ae(r02.A02).A00;
                byte[][] bArr3 = new C31501ae(A002.A00.A02).A01;
                C31471ab r03 = A002.A00.A03;
                if (r03 == null) {
                    r03 = C31471ab.A03;
                }
                r0 = new C31821bA(C31481ac.A00(r03.A02.A04()), bArr3, i, i2);
            } catch (C31541ai | C31561ak e3) {
                throw new AssertionError(e3);
            }
        }
        return r0;
    }
}
