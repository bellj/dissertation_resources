package X;

/* renamed from: X.3cA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C70963cA implements AnonymousClass5WN {
    public final /* synthetic */ C91074Qi A00;

    public C70963cA(C91074Qi r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r3) {
        throw new C87394Bi(C16700pc.A08("An operation is not implemented: ", "Not yet implemented"));
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r13) {
        C91074Qi r2 = this.A00;
        if (r13 == null || r13.A00 != 5) {
            AnonymousClass3D5 r0 = r2.A00;
            if (r0 != null) {
                C27661Io r1 = r0.A01;
                C63963Dq r02 = r1.A02;
                if (r02 == null) {
                    throw C16700pc.A06("fcsLoadingEventManager");
                }
                r02.A00.A02(r02.A01).A01(new AnonymousClass6ED("onLoadingFailure", r1.A05));
                return;
            }
            return;
        }
        C19600uN r4 = r2.A01;
        C18840t8 r5 = r4.A01;
        C65963Lt r3 = r4.A02;
        String str = r3.A01;
        StringBuilder A0j = C12960it.A0j(r2.A02);
        A0j.append("BloksLayoutData:8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666:");
        r5.A03(r13, str, C12970iu.A0s(C12970iu.A14(r4.A00), A0j), Long.valueOf(r3.A00).longValue(), r3.A02);
        AnonymousClass3D5 r03 = r2.A00;
        if (r03 != null) {
            r03.A00();
        }
    }
}
