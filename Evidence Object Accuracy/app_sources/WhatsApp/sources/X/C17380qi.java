package X;

import java.util.Map;

/* renamed from: X.0qi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17380qi implements AbstractC17390qj {
    public final AnonymousClass18I A00;

    public C17380qi(AnonymousClass18I r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC17390qj
    public C89994Me Ad6(C14260l7 r7, C14270l8 r8, AnonymousClass4ZD r9, C93484aF r10, C14240l5 r11) {
        String str;
        C89994Me A00;
        Map map = r10.A02;
        if (map == null || (str = (String) map.get("key")) == null) {
            throw new IllegalStateException("Key not defined in data manifest");
        }
        Object A002 = AnonymousClass3AE.A00(r11, map);
        Object obj = map.get("mode");
        String str2 = r10.A00;
        boolean equals = "fetch".equals(obj);
        AnonymousClass18I r2 = this.A00;
        synchronized (r2) {
            if (equals) {
                Map map2 = r2.A01;
                Object obj2 = map2.get(str);
                if (obj2 == null) {
                    map2.put(str, A002);
                } else {
                    A002 = obj2;
                }
                A00 = r2.A00(r8, A002, str, str2);
            } else {
                r2.A01(str, A002);
                A00 = r2.A00(r8, A002, str, str2);
            }
        }
        return A00;
    }
}
