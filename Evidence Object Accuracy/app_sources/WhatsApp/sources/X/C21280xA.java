package X;

import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.0xA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21280xA {
    public static boolean A00() {
        try {
            Voip.CallState currentCallState = Voip.getCurrentCallState();
            if (currentCallState == null) {
                return false;
            }
            if (currentCallState != Voip.CallState.NONE) {
                return true;
            }
            return false;
        } catch (UnsatisfiedLinkError e) {
            Log.e("unable to query for current call state", e);
            return false;
        }
    }

    public static boolean A01() {
        try {
            Voip.CallState currentCallState = Voip.getCurrentCallState();
            if (currentCallState == null || currentCallState == Voip.CallState.NONE) {
                return false;
            }
            if (currentCallState != Voip.CallState.ACTIVE_ELSEWHERE) {
                return true;
            }
            return false;
        } catch (UnsatisfiedLinkError e) {
            Log.e("unable to query for current call state", e);
            return false;
        }
    }

    public static boolean A02() {
        try {
            Voip.CallState currentCallState = Voip.getCurrentCallState();
            if (currentCallState == null) {
                return false;
            }
            if (currentCallState == Voip.CallState.ACTIVE_ELSEWHERE) {
                return true;
            }
            return false;
        } catch (UnsatisfiedLinkError e) {
            Log.e("unable to query for current call state", e);
            return false;
        }
    }

    public boolean A03() {
        CallInfo callInfo;
        if (!A00() || (callInfo = Voip.getCallInfo()) == null || !callInfo.videoEnabled) {
            return false;
        }
        return true;
    }
}
