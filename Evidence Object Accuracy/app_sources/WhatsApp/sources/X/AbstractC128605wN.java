package X;

import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.5wN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC128605wN {
    public final AnonymousClass1Q5 A00;

    public AbstractC128605wN(C14830m7 r8, C16120oU r9, C21230x5 r10, AbstractC21180x0 r11, String str, int i) {
        AnonymousClass1Q5 r0 = new AnonymousClass1Q5(r8, r9, r10, r11, str, i);
        this.A00 = r0;
        r0.A06.A03 = true;
    }

    public void A00(Intent intent) {
        if (intent != null) {
            long longExtra = intent.getLongExtra("perf_start_time_ns", -1);
            String stringExtra = intent.getStringExtra("perf_origin");
            if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
                this.A00.A0D(stringExtra, longExtra);
                return;
            }
        }
        Log.e("Expect to have origin for perf tracking.");
        this.A00.A0D("unknown", -1);
    }
}
