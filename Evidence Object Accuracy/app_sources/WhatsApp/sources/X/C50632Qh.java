package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.StateSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.2Qh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50632Qh {
    public static final TimeInterpolator A0P = C50732Qs.A01;
    public static final int[] A0Q = new int[0];
    public static final int[] A0R = {16842910};
    public static final int[] A0S = {16842908, 16842910};
    public static final int[] A0T = {16843623, 16842910};
    public static final int[] A0U = {16843623, 16842908, 16842910};
    public static final int[] A0V = {16842919, 16842910};
    public float A00;
    public float A01;
    public float A02 = 1.0f;
    public float A03;
    public float A04;
    public int A05 = 0;
    public int A06;
    public Animator A07;
    public Drawable A08;
    public Drawable A09;
    public Drawable A0A;
    public ViewTreeObserver.OnPreDrawListener A0B;
    public C50612Qf A0C;
    public C50612Qf A0D;
    public C50612Qf A0E;
    public C50612Qf A0F;
    public C50702Qp A0G;
    public AnonymousClass2R6 A0H;
    public final Matrix A0I = new Matrix();
    public final Rect A0J = new Rect();
    public final RectF A0K = new RectF();
    public final RectF A0L = new RectF();
    public final C50742Qt A0M;
    public final AnonymousClass2QW A0N;
    public final AbstractC50652Qj A0O;

    public void A07() {
    }

    public void A0F(Rect rect) {
    }

    public boolean A0H() {
        return true;
    }

    public C50632Qh(AnonymousClass2QW r4, AbstractC50652Qj r5) {
        this.A0N = r4;
        this.A0O = r5;
        C50742Qt r2 = new C50742Qt();
        this.A0M = r2;
        r2.A00(A00(new AnonymousClass2Qu(this)), A0V);
        r2.A00(A00(new C50762Qw(this)), A0U);
        r2.A00(A00(new C50762Qw(this)), A0S);
        r2.A00(A00(new C50762Qw(this)), A0T);
        r2.A00(A00(new C50772Qx(this)), A0R);
        r2.A00(A00(new AnonymousClass2Qz(this)), A0Q);
        this.A04 = r4.getRotation();
    }

    public static final ValueAnimator A00(AbstractC50752Qv r3) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(A0P);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(r3);
        valueAnimator.addUpdateListener(r3);
        valueAnimator.setFloatValues(0.0f, 1.0f);
        return valueAnimator;
    }

    public float A01() {
        return this.A00;
    }

    public final AnimatorSet A02(C50612Qf r10, float f, float f2, float f3) {
        ArrayList arrayList = new ArrayList();
        AnonymousClass2QW r6 = this.A0N;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(r6, View.ALPHA, f);
        r10.A03("opacity").A00(ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(r6, View.SCALE_X, f2);
        r10.A03("scale").A00(ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(r6, View.SCALE_Y, f2);
        r10.A03("scale").A00(ofFloat3);
        arrayList.add(ofFloat3);
        Matrix matrix = this.A0I;
        A0D(matrix, f3);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(r6, new AnonymousClass2R1(), new AnonymousClass2R2(), new Matrix(matrix));
        r10.A03("iconScale").A00(ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        AnonymousClass2R4.A00(animatorSet, arrayList);
        return animatorSet;
    }

    public GradientDrawable A03() {
        return new GradientDrawable();
    }

    public C50702Qp A04() {
        return new C50702Qp();
    }

    public C50702Qp A05(ColorStateList colorStateList, int i) {
        Context context = this.A0N.getContext();
        C50702Qp A04 = A04();
        int A00 = AnonymousClass00T.A00(context, R.color.design_fab_stroke_top_outer_color);
        int A002 = AnonymousClass00T.A00(context, R.color.design_fab_stroke_top_inner_color);
        int A003 = AnonymousClass00T.A00(context, R.color.design_fab_stroke_end_inner_color);
        int A004 = AnonymousClass00T.A00(context, R.color.design_fab_stroke_end_outer_color);
        A04.A06 = A00;
        A04.A05 = A002;
        A04.A03 = A003;
        A04.A02 = A004;
        float f = (float) i;
        if (A04.A00 != f) {
            A04.A00 = f;
            A04.A09.setStrokeWidth(f * 1.3333f);
            A04.A08 = true;
            A04.invalidateSelf();
        }
        if (colorStateList != null) {
            A04.A04 = colorStateList.getColorForState(A04.getState(), A04.A04);
        }
        A04.A07 = colorStateList;
        A04.A08 = true;
        A04.invalidateSelf();
        return A04;
    }

    public void A06() {
        C50742Qt r1 = this.A0M;
        ValueAnimator valueAnimator = r1.A00;
        if (valueAnimator != null) {
            valueAnimator.end();
            r1.A00 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r1 != 1) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08() {
        /*
            r5 = this;
            X.2QW r4 = r5.A0N
            float r3 = r4.getRotation()
            float r0 = r5.A04
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x004b
            r5.A04 = r3
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 19
            if (r1 != r0) goto L_0x0027
            r0 = 1119092736(0x42b40000, float:90.0)
            float r3 = r3 % r0
            r0 = 0
            r2 = 0
            int r0 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            int r1 = r4.getLayerType()
            if (r0 == 0) goto L_0x004c
            r0 = 1
            if (r1 == r0) goto L_0x0027
        L_0x0024:
            r4.setLayerType(r0, r2)
        L_0x0027:
            X.2R6 r2 = r5.A0H
            if (r2 == 0) goto L_0x0039
            float r0 = r5.A04
            float r1 = -r0
            float r0 = r2.A03
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0039
            r2.A03 = r1
            r2.invalidateSelf()
        L_0x0039:
            X.2Qp r2 = r5.A0G
            if (r2 == 0) goto L_0x004b
            float r0 = r5.A04
            float r1 = -r0
            float r0 = r2.A01
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x004b
            r2.A01 = r1
            r2.invalidateSelf()
        L_0x004b:
            return
        L_0x004c:
            if (r1 == 0) goto L_0x0027
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C50632Qh.A08():void");
    }

    public final void A09() {
        Rect rect = this.A0J;
        A0E(rect);
        A0F(rect);
        AbstractC50652Qj r0 = this.A0O;
        int i = rect.left;
        int i2 = rect.top;
        int i3 = rect.right;
        int i4 = rect.bottom;
        AnonymousClass2QV r1 = ((C50642Qi) r0).A00;
        r1.A0C.set(i, i2, i3, i4);
        int i5 = r1.A02;
        r1.setPadding(i + i5, i2 + i5, i3 + i5, i4 + i5);
    }

    public void A0A(float f, float f2, float f3) {
        AnonymousClass2R6 r1 = this.A0H;
        if (r1 != null) {
            r1.A00(f, this.A03 + f);
            A09();
        }
    }

    public void A0B(ColorStateList colorStateList) {
        Drawable drawable = this.A09;
        if (drawable != null) {
            C015607k.A04(AnonymousClass2RB.A02(colorStateList), drawable);
        }
    }

    public void A0C(ColorStateList colorStateList, ColorStateList colorStateList2, PorterDuff.Mode mode, int i) {
        Drawable[] drawableArr;
        GradientDrawable A03 = A03();
        A03.setShape(1);
        A03.setColor(-1);
        Drawable A032 = C015607k.A03(A03);
        this.A0A = A032;
        C015607k.A04(colorStateList, A032);
        if (mode != null) {
            C015607k.A07(mode, this.A0A);
        }
        GradientDrawable A033 = A03();
        A033.setShape(1);
        A033.setColor(-1);
        Drawable A034 = C015607k.A03(A033);
        this.A09 = A034;
        C015607k.A04(AnonymousClass2RB.A02(colorStateList2), A034);
        if (i > 0) {
            C50702Qp A05 = A05(colorStateList, i);
            this.A0G = A05;
            drawableArr = new Drawable[]{A05, this.A0A, this.A09};
        } else {
            this.A0G = null;
            drawableArr = new Drawable[]{this.A0A, this.A09};
        }
        this.A08 = new LayerDrawable(drawableArr);
        Context context = this.A0N.getContext();
        Drawable drawable = this.A08;
        AnonymousClass2QV r2 = ((C50642Qi) this.A0O).A00;
        float f = this.A00;
        AnonymousClass2R6 r3 = new AnonymousClass2R6(context, drawable, ((float) r2.A01(r2.A04)) / 2.0f, f, f + this.A03);
        this.A0H = r3;
        r3.A06 = false;
        r3.invalidateSelf();
        C50632Qh.super.setBackgroundDrawable(this.A0H);
    }

    public final void A0D(Matrix matrix, float f) {
        matrix.reset();
        Drawable drawable = this.A0N.getDrawable();
        if (drawable != null && this.A06 != 0) {
            RectF rectF = this.A0K;
            RectF rectF2 = this.A0L;
            rectF.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            float f2 = (float) this.A06;
            rectF2.set(0.0f, 0.0f, f2, f2);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            float f3 = ((float) this.A06) / 2.0f;
            matrix.postScale(f, f, f3, f3);
        }
    }

    public void A0E(Rect rect) {
        this.A0H.getPadding(rect);
    }

    public void A0G(int[] iArr) {
        AnonymousClass2RA r1;
        ValueAnimator valueAnimator;
        C50742Qt r5 = this.A0M;
        ArrayList arrayList = r5.A03;
        int size = arrayList.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                r1 = null;
                break;
            }
            r1 = (AnonymousClass2RA) arrayList.get(i);
            if (StateSet.stateSetMatches(r1.A01, iArr)) {
                break;
            }
            i++;
        }
        AnonymousClass2RA r0 = r5.A01;
        if (r1 != r0) {
            if (!(r0 == null || (valueAnimator = r5.A00) == null)) {
                valueAnimator.cancel();
                r5.A00 = null;
            }
            r5.A01 = r1;
            if (r1 != null) {
                ValueAnimator valueAnimator2 = r1.A00;
                r5.A00 = valueAnimator2;
                valueAnimator2.start();
            }
        }
    }
}
