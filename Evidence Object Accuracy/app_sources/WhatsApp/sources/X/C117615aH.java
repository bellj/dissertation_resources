package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.List;

/* renamed from: X.5aH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117615aH extends ArrayAdapter {
    public AbstractC136326Mc A00;
    public AbstractC1311261j A01;
    public List A02 = C12960it.A0l();
    public final AnonymousClass018 A03;
    public final C17070qD A04;

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getViewTypeCount() {
        return 1;
    }

    public C117615aH(Context context, AnonymousClass018 r4, C17070qD r5, AbstractC1311261j r6) {
        super(context, (int) R.layout.payment_method_row, C12960it.A0l());
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r6;
        this.A01 = r6;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        List list = this.A02;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A02.get(i);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        PaymentMethodRow paymentMethodRow;
        TextView textView;
        int i2;
        WaImageView waImageView;
        int i3;
        if (view == null) {
            paymentMethodRow = new PaymentMethodRow(getContext());
        } else {
            paymentMethodRow = (PaymentMethodRow) view;
        }
        AbstractC28901Pl A08 = C117315Zl.A08(this.A02, i);
        if (A08 != null) {
            AbstractC1311261j r4 = this.A01;
            String AEQ = r4.AEQ(A08);
            if (r4.AdV()) {
                r4.Adj(A08, paymentMethodRow);
            } else {
                C1311161i.A0A(A08, paymentMethodRow);
            }
            if (TextUtils.isEmpty(AEQ)) {
                AEQ = C1311161i.A02(getContext(), this.A03, A08, this.A04, true);
            }
            paymentMethodRow.A05.setText(AEQ);
            paymentMethodRow.A02(r4.AEP(A08));
            paymentMethodRow.A03(!r4.AdN(A08));
            String AEN = r4.AEN(A08);
            if (!TextUtils.isEmpty(AEN)) {
                paymentMethodRow.A03.setText(AEN);
                textView = paymentMethodRow.A03;
                i2 = 0;
            } else {
                textView = paymentMethodRow.A03;
                i2 = 8;
            }
            textView.setVisibility(i2);
            int AEM = r4.AEM(A08);
            if (AEM == 0) {
                waImageView = paymentMethodRow.A08;
                i3 = 8;
            } else {
                paymentMethodRow.A08.setImageResource(AEM);
                waImageView = paymentMethodRow.A08;
                i3 = 0;
            }
            waImageView.setVisibility(i3);
            AnonymousClass028.A0D(paymentMethodRow, R.id.account_number_divider).setVisibility(C12960it.A02(r4.AdT() ? 1 : 0));
        }
        return paymentMethodRow;
    }
}
