package X;

import java.util.Iterator;

/* renamed from: X.3to  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81313to extends AnonymousClass1I5 {
    public final /* synthetic */ AbstractC80953tE this$0;
    public Iterator valueCollectionItr;
    public Iterator valueItr = AnonymousClass1I4.emptyIterator();

    public C81313to(AbstractC80953tE r2) {
        this.this$0 = r2;
        this.valueCollectionItr = r2.map.values().iterator();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.valueItr.hasNext() || this.valueCollectionItr.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        if (!this.valueItr.hasNext()) {
            this.valueItr = ((AbstractC17950rf) this.valueCollectionItr.next()).iterator();
        }
        return this.valueItr.next();
    }
}
