package X;

import java.nio.ByteBuffer;

/* renamed from: X.3m0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76663m0 extends AbstractC106504vo {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public boolean A05;
    public byte[] A06 = AnonymousClass3JZ.A0A;

    @Override // X.AbstractC106504vo, X.AnonymousClass5Xx
    public ByteBuffer AEn() {
        int i;
        if (super.AJN() && (i = this.A00) > 0) {
            ByteBuffer A00 = A00(i);
            A00.put(this.A06, 0, this.A00);
            A00.flip();
            this.A00 = 0;
        }
        ByteBuffer byteBuffer = super.A05;
        super.A05 = AnonymousClass5Xx.A00;
        return byteBuffer;
    }

    @Override // X.AbstractC106504vo, X.AnonymousClass5Xx
    public boolean AJN() {
        return super.AJN() && this.A00 == 0;
    }

    @Override // X.AnonymousClass5Xx
    public void AZk(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i = limit - position;
        if (i != 0) {
            int i2 = this.A01;
            int min = Math.min(i, i2);
            this.A04 += (long) (min / super.A00.A00);
            this.A01 = i2 - min;
            byteBuffer.position(position + min);
            if (this.A01 <= 0) {
                int i3 = i - min;
                int length = (this.A00 + i3) - this.A06.length;
                ByteBuffer A00 = A00(length);
                int A03 = C72463ee.A03(length, this.A00, 0);
                A00.put(this.A06, 0, A03);
                int A032 = C72463ee.A03(length - A03, i3, 0);
                byteBuffer.limit(byteBuffer.position() + A032);
                A00.put(byteBuffer);
                byteBuffer.limit(limit);
                int i4 = i3 - A032;
                int i5 = this.A00 - A03;
                this.A00 = i5;
                byte[] bArr = this.A06;
                System.arraycopy(bArr, A03, bArr, 0, i5);
                byteBuffer.get(this.A06, this.A00, i4);
                this.A00 += i4;
                A00.flip();
            }
        }
    }
}
