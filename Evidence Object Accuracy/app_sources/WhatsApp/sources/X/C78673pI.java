package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.List;

/* renamed from: X.3pI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78673pI extends AnonymousClass1U5 implements AnonymousClass5SX {
    public static final Parcelable.Creator CREATOR = new C99124jn();
    public final String A00;
    public final List A01;

    public C78673pI(String str, List list) {
        this.A01 = list;
        this.A00 = str;
    }

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        if (this.A00 != null) {
            return Status.A09;
        }
        return Status.A05;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0E(parcel, this.A01, 1);
        C95654e8.A0D(parcel, this.A00, 2, false);
        C95654e8.A06(parcel, A00);
    }
}
