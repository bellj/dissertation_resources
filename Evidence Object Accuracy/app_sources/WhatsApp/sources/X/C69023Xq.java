package X;

import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69023Xq implements AnonymousClass13P {
    public final /* synthetic */ ContactInfoActivity A00;

    @Override // X.AnonymousClass13P
    public void AUg(C30751Yr r1) {
    }

    public C69023Xq(ContactInfoActivity contactInfoActivity) {
        this.A00 = contactInfoActivity;
    }

    @Override // X.AnonymousClass13P
    public void AUh(AbstractC14640lm r4, UserJid userJid) {
        ContactInfoActivity contactInfoActivity = this.A00;
        if (r4.equals(contactInfoActivity.A2v())) {
            C14900mE.A01(((ActivityC13810kN) contactInfoActivity).A05, contactInfoActivity, 6);
        }
    }

    @Override // X.AnonymousClass13P
    public void AUi(AbstractC14640lm r4, UserJid userJid) {
        ContactInfoActivity contactInfoActivity = this.A00;
        if (r4.equals(contactInfoActivity.A2v())) {
            C14900mE.A01(((ActivityC13810kN) contactInfoActivity).A05, contactInfoActivity, 7);
        }
    }
}
