package X;

import android.os.Bundle;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43841xg extends C43851xh {
    public final C43881xk A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final List A08;

    public C43841xg(C43881xk r1, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List list) {
        super(str3);
        this.A06 = str;
        this.A05 = str2;
        this.A08 = list;
        this.A07 = str4;
        this.A01 = str5;
        this.A00 = r1;
        this.A02 = str6;
        this.A04 = str7;
        this.A03 = str8;
    }

    public static C43841xg A00(Bundle bundle) {
        C43871xj r4;
        File file;
        File file2;
        String string = bundle.getString("icon_light_url");
        AnonymousClass009.A05(string);
        String string2 = bundle.getString("icon_dark_url");
        AnonymousClass009.A05(string2);
        String string3 = bundle.getString("icon_description");
        AnonymousClass009.A05(string3);
        String string4 = bundle.getString("title");
        AnonymousClass009.A05(string4);
        int i = bundle.getInt("bullets_size", 0);
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            StringBuilder sb = new StringBuilder("bullet_text_");
            sb.append(i2);
            String string5 = bundle.getString(sb.toString());
            AnonymousClass009.A05(string5);
            StringBuilder sb2 = new StringBuilder("bullet_icon_light_url_");
            sb2.append(i2);
            String string6 = bundle.getString(sb2.toString());
            StringBuilder sb3 = new StringBuilder("bullet_icon_dark_url_");
            sb3.append(i2);
            arrayList.add(new C92274Vf(string5, string6, bundle.getString(sb3.toString())));
        }
        String string7 = bundle.getString("agree_button_text");
        AnonymousClass009.A05(string7);
        long j = bundle.getLong("start_time_millis");
        C43871xj r3 = null;
        if (j != 0) {
            r4 = new C43871xj(j);
        } else {
            r4 = null;
        }
        C43861xi r7 = new C43861xi(bundle.getLongArray("duration_repeat"), bundle.getLong("duration_static", -1));
        long j2 = bundle.getLong("end_time_millis");
        if (j2 != 0) {
            r3 = new C43871xj(j2);
        }
        C43841xg r8 = new C43841xg(new C43881xk(r7, r4, r3), string, string2, string3, string4, string7, bundle.getString("body"), bundle.getString("footer"), bundle.getString("dismiss_button_text"), arrayList);
        String string8 = bundle.getString("light_icon_path");
        if (string8 == null) {
            file = null;
        } else {
            file = new File(string8);
        }
        ((C43851xh) r8).A01 = file;
        String string9 = bundle.getString("dark_icon_path");
        if (string9 == null) {
            file2 = null;
        } else {
            file2 = new File(string9);
        }
        ((C43851xh) r8).A00 = file2;
        return r8;
    }

    public void A01(Bundle bundle) {
        bundle.putString("icon_light_url", this.A06);
        bundle.putString("icon_dark_url", this.A05);
        bundle.putString("icon_description", super.A02);
        bundle.putString("title", this.A07);
        List list = this.A08;
        bundle.putInt("bullets_size", list.size());
        for (int i = 0; i < list.size(); i++) {
            C92274Vf r4 = (C92274Vf) list.get(i);
            StringBuilder sb = new StringBuilder("bullet_text_");
            sb.append(i);
            bundle.putString(sb.toString(), r4.A02);
            StringBuilder sb2 = new StringBuilder("bullet_icon_light_url_");
            sb2.append(i);
            bundle.putString(sb2.toString(), r4.A01);
            StringBuilder sb3 = new StringBuilder("bullet_icon_dark_url_");
            sb3.append(i);
            bundle.putString(sb3.toString(), r4.A00);
        }
        bundle.putString("agree_button_text", this.A01);
        C43881xk r42 = this.A00;
        C43871xj r0 = r42.A02;
        if (r0 != null) {
            bundle.putLong("start_time_millis", r0.A00);
        }
        C43861xi r3 = r42.A00;
        if (r3 != null) {
            bundle.putLong("duration_static", r3.A00);
            bundle.putLongArray("duration_repeat", r3.A01);
        }
        C43871xj r02 = r42.A01;
        if (r02 != null) {
            bundle.putLong("end_time_millis", r02.A00);
        }
        bundle.putString("body", this.A02);
        bundle.putString("footer", this.A04);
        bundle.putString("dismiss_button_text", this.A03);
        File file = super.A01;
        if (file != null) {
            bundle.putString("light_icon_path", file.getAbsolutePath());
        }
        File file2 = super.A00;
        if (file2 != null) {
            bundle.putString("dark_icon_path", file2.getAbsolutePath());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UserNoticeModal{iconLightUrl='");
        sb.append(this.A06);
        sb.append('\'');
        sb.append(", iconDarkUrl='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", iconDescription='");
        sb.append(super.A02);
        sb.append('\'');
        sb.append(", title='");
        sb.append(this.A07);
        sb.append('\'');
        sb.append(", bulletPoints=");
        sb.append(this.A08);
        sb.append(", agreeButtonText='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append(", timing=");
        sb.append(this.A00);
        sb.append(", body='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", footer='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", dismissButtonText='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
