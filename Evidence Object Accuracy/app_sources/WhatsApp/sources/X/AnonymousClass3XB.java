package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaMediaThumbnailView;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3XB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3XB implements AnonymousClass23E {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ C54972ha A01;
    public final /* synthetic */ AnonymousClass23D A02;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XB(AbstractC35611iN r1, C54972ha r2, AnonymousClass23D r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        C54972ha r0 = this.A01;
        WaMediaThumbnailView waMediaThumbnailView = r0.A03;
        waMediaThumbnailView.setBackgroundColor(r0.A01);
        waMediaThumbnailView.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        int i;
        C16700pc.A0E(bitmap, 0);
        C54972ha r5 = this.A01;
        WaMediaThumbnailView waMediaThumbnailView = r5.A03;
        if (waMediaThumbnailView.getTag() != this.A02) {
            return;
        }
        if (bitmap.equals(MediaGalleryFragmentBase.A0U)) {
            waMediaThumbnailView.setScaleType(ImageView.ScaleType.CENTER);
            int type = this.A00.getType();
            if (type == 1 || type == 2) {
                waMediaThumbnailView.setBackgroundColor(r5.A01);
                i = R.drawable.ic_missing_thumbnail_video;
            } else {
                waMediaThumbnailView.setBackgroundColor(r5.A01);
                i = R.drawable.ic_missing_thumbnail_picture;
            }
            waMediaThumbnailView.setImageResource(i);
            return;
        }
        C12990iw.A1E(waMediaThumbnailView);
        waMediaThumbnailView.setBackgroundResource(0);
        waMediaThumbnailView.setThumbnail(bitmap);
        if (!z) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(waMediaThumbnailView.getResources(), bitmap);
            Drawable[] drawableArr = new Drawable[2];
            drawableArr[0] = r5.A02;
            C12960it.A15(waMediaThumbnailView, bitmapDrawable, drawableArr);
        }
    }
}
