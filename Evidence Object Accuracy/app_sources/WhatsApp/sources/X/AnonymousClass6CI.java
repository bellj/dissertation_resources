package X;

import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.6CI  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass6CI implements AnonymousClass18Q {
    public int A00 = 0;
    public final C18420sQ A01;
    public final C14850m9 A02;
    public final C17900ra A03;
    public final Map A04;
    public final Map A05;

    public AnonymousClass6CI(C18420sQ r2, C14850m9 r3, C17900ra r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A05 = C12970iu.A11();
        this.A04 = C12970iu.A11();
    }

    public final AnonymousClass1Q5 A00(Integer num, String str) {
        Object obj;
        if (!this.A02.A07(1546)) {
            return null;
        }
        if (num == null) {
            obj = this.A05.get(str);
        } else {
            obj = this.A05.get(C12970iu.A0s(num, C12960it.A0j(str)));
        }
        return (AnonymousClass1Q5) obj;
    }

    public Integer A01(String str, int i) {
        AnonymousClass1Q5 A00;
        if (!this.A02.A07(1546)) {
            return null;
        }
        if (str == null) {
            str = "unknown";
        }
        Integer valueOf = Integer.valueOf(this.A00);
        if (valueOf == null) {
            A00 = A00(null, "p2p_flow_tag");
        } else {
            A00 = A00(valueOf, "p2p_flow_tag");
        }
        if (A00 == null) {
            AnonymousClass1Q6 r1 = new AnonymousClass1Q6(i);
            A00 = this.A01.A00(r1, "p2p_flow_tag");
            r1.A03 = true;
            if (valueOf == null) {
                this.A05.put("p2p_flow_tag", A00);
            } else {
                this.A05.put(C12970iu.A0s(valueOf, C12960it.A0j("p2p_flow_tag")), A00);
            }
        }
        A00.A04(this.A00, str, false);
        A00.A07.AL0("entry_point", str, A00.A06.A05, this.A00);
        int i2 = this.A00;
        C17930rd A01 = this.A03.A01();
        if (A01 != null) {
            String str2 = A01.A03;
            AnonymousClass1Q5 A002 = A00(Integer.valueOf(i2), "p2p_flow_tag");
            if (A002 != null) {
                A002.A07.AL0("country", str2, A002.A06.A05, i2);
            }
        }
        AL9("new_payment", this.A00);
        int i3 = this.A00;
        this.A00 = i3 + 1;
        return Integer.valueOf(i3);
    }

    public void A02(int i, String str) {
        if (this.A02.A07(1546)) {
            AnonymousClass1Q5 A00 = A00(null, str);
            if (A00 == null) {
                AnonymousClass1Q6 r1 = new AnonymousClass1Q6(i);
                A00 = this.A01.A00(r1, str);
                r1.A03 = true;
                this.A05.put(str, A00);
            }
            A00.A0D("unknown", -1);
            C17930rd A01 = this.A03.A01();
            if (A01 != null) {
                String str2 = A01.A03;
                AnonymousClass1Q5 A002 = A00(null, str);
                if (A002 != null) {
                    A002.A0A("country", str2, false);
                }
            }
        }
    }

    public void A03(int i, String str, long j) {
        AnonymousClass1Q5 A00 = A00(Integer.valueOf(i), "p2p_flow_tag");
        if (A00 != null) {
            A00.A07.AKz(str, A00.A06.A05, i, j);
        }
    }

    public void A04(int i, short s) {
        AnonymousClass1Q5 A00 = A00(Integer.valueOf(i), "p2p_flow_tag");
        if (A00 != null) {
            Map map = this.A04;
            String A0t = C12970iu.A0t(C12960it.A0f(C12960it.A0j("p2p_flow_tag"), i), map);
            if (A0t != null) {
                A00.A01(i, A0t);
                map.remove(C12960it.A0f(C12960it.A0j("p2p_flow_tag"), i));
            }
            A00.A05(i, s);
            this.A05.remove(C12960it.A0f(C12960it.A0j("p2p_flow_tag"), i));
        }
    }

    public void A05(C452120p r7, String str) {
        if (r7 == null) {
            r7 = C117305Zk.A0L();
        }
        long j = (long) r7.A00;
        AnonymousClass1Q5 A00 = A00(null, str);
        if (A00 != null) {
            A00.A07.AKv(A00.A06.A05, "error_code", j);
        }
        A06(str, 3);
    }

    public void A06(String str, short s) {
        AnonymousClass1Q5 A00 = A00(null, str);
        if (A00 != null) {
            A00.A0C(s);
            this.A05.remove(str);
        }
    }

    @Override // X.AnonymousClass18Q
    public void AL9(String str, int i) {
        AnonymousClass1Q5 A00 = A00(Integer.valueOf(i), "p2p_flow_tag");
        if (A00 != null) {
            Map map = this.A04;
            String A0t = C12970iu.A0t(C12960it.A0f(C12960it.A0j("p2p_flow_tag"), i), map);
            if (A0t != null) {
                if (A0t.equals(str)) {
                    Log.i(C12960it.A0d(str, C12960it.A0k("P2pFlowPerfTrackerManager.markerFlowPoint used the same qpl flow point twice: ")));
                    return;
                }
                A00.A01(i, A0t);
            }
            A00.A02(i, str);
            map.put(C12960it.A0f(C12960it.A0j("p2p_flow_tag"), i), str);
        }
    }
}
