package X;

import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;

/* renamed from: X.65O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65O implements AnonymousClass07L {
    public final /* synthetic */ NoviWithdrawCashStoreLocatorActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        return false;
    }

    public AnonymousClass65O(NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity) {
        this.A00 = noviWithdrawCashStoreLocatorActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        C128365vz r2 = new AnonymousClass610("LOCATION_SEARCH_CLICK", "WITHDRAW_MONEY", "SELECT_LOCATION", "INPUT_BOX").A00;
        r2.A0c = str;
        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = this.A00;
        noviWithdrawCashStoreLocatorActivity.A0I.A05(r2);
        noviWithdrawCashStoreLocatorActivity.A2g(str);
        return false;
    }
}
