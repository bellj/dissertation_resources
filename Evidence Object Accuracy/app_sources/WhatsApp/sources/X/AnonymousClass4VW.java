package X;

import java.util.ArrayList;

/* renamed from: X.4VW  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4VW {
    public Object A00;
    public boolean A01 = false;
    public final /* synthetic */ AbstractC95064d1 A02;

    public AnonymousClass4VW(AbstractC95064d1 r2, Object obj) {
        this.A02 = r2;
        this.A00 = obj;
    }

    public final void A00() {
        synchronized (this) {
            this.A00 = null;
        }
        ArrayList arrayList = this.A02.A0P;
        synchronized (arrayList) {
            arrayList.remove(this);
        }
    }
}
