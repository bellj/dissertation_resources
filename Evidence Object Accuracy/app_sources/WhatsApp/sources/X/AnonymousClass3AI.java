package X;

import com.whatsapp.util.Log;

/* renamed from: X.3AI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AI {
    public static void A00(C16150oX r3, AbstractC16130oV r4, byte[] bArr) {
        int length = bArr.length;
        if (length == 32) {
            C37891nB A8r = new AnonymousClass2QH(C14370lK.A01(r4.A0y, ((AbstractC15340mz) r4).A08)).A8r(bArr);
            r3.A0U = bArr;
            r3.A0Q = A8r.A00;
            r3.A0S = A8r.A02;
            r3.A0T = A8r.A01;
            return;
        }
        StringBuilder A0k = C12960it.A0k("MediaKeysUtil/setMediaKeyForMediaData/media key incorrect length; length=");
        A0k.append(length);
        A0k.append("; message.key=");
        Log.w(C12970iu.A0s(r4.A0z, A0k));
        throw new C43271wi(16);
    }
}
