package X;

/* renamed from: X.0aT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08030aT implements AbstractC12030hG {
    public final /* synthetic */ AbstractC08070aX A00;

    public C08030aT(AbstractC08070aX r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        AbstractC08070aX r2 = this.A00;
        boolean z = false;
        if (r2.A01.A09() == 1.0f) {
            z = true;
        }
        if (z != r2.A07) {
            r2.A07 = z;
            r2.A0K.invalidateSelf();
        }
    }
}
