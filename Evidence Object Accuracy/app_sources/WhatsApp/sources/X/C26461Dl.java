package X;

import android.app.Notification;
import android.content.Context;
import android.os.Build;
import com.whatsapp.R;

/* renamed from: X.1Dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26461Dl {
    public final C15450nH A00;
    public final C16590pI A01;
    public final C245716a A02;
    public final C14840m8 A03;
    public final AnonymousClass17R A04;

    public C26461Dl(C15450nH r1, C16590pI r2, C245716a r3, C14840m8 r4, AnonymousClass17R r5) {
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public Notification A00(boolean z) {
        int i;
        Context context = this.A01.A00;
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "other_notifications@1";
        A00.A09 = AnonymousClass1UY.A00(context, 0, C65303Iz.A02(context), 0);
        int i2 = -2;
        if (Build.VERSION.SDK_INT >= 26) {
            i2 = -1;
        }
        A00.A03 = i2;
        if (z) {
            A00.A0B(context.getString(R.string.notification_ticker_portal_client));
            A00.A0A(context.getString(R.string.notification_ticker_portal_client));
            i = R.string.notification_text_portal_client;
        } else {
            A00.A0B(context.getString(R.string.notification_ticker_web_client));
            A00.A0A(context.getString(R.string.notification_ticker_web_client));
            i = R.string.notification_text_web_client;
        }
        A00.A09(context.getString(i));
        C18360sK.A01(A00, R.drawable.notify_web_client_connected);
        return A00.A01();
    }
}
