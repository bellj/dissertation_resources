package X;

import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: X.25A  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25A extends FilterInputStream {
    public int A00;

    public AnonymousClass25A(InputStream inputStream, int i) {
        super(inputStream);
        this.A00 = i;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() {
        return Math.min(super.available(), this.A00);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() {
        if (this.A00 <= 0) {
            return -1;
        }
        int read = super.read();
        if (read < 0) {
            return read;
        }
        this.A00--;
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int i3 = this.A00;
        if (i3 <= 0) {
            return -1;
        }
        int read = super.read(bArr, i, Math.min(i2, i3));
        if (read < 0) {
            return read;
        }
        this.A00 -= read;
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) {
        long skip = super.skip(Math.min(j, (long) this.A00));
        if (skip >= 0) {
            this.A00 = (int) (((long) this.A00) - skip);
        }
        return skip;
    }
}
