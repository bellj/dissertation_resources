package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BN  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4BN extends Enum {
    public static final /* synthetic */ AnonymousClass4BN[] A00 = {new C82603vt(), new C82613vu(), new C82623vv(), new C82633vw(), new C82643vx(), new C82653vy(), new C82663vz(), new C82673w0()};
    /* Fake field, exist only in values array */
    AnonymousClass4BN EF4;

    public /* synthetic */ AnonymousClass4BN(String str, int i) {
    }

    public boolean A00(int i, int i2) {
        int i3;
        if (this instanceof C82673w0) {
            return (((i + i2) + ((i * i2) % 3)) & 1) == 0;
        }
        if (this instanceof C82663vz) {
            return C12990iw.A1Y((i * i2) % 6, 3);
        }
        if (!(this instanceof C82653vy)) {
            if (!(this instanceof C82643vx)) {
                if (this instanceof C82633vw) {
                    i3 = (i + i2) % 3;
                } else if (!(this instanceof C82623vv)) {
                    if (!(this instanceof C82613vu)) {
                        i += i2;
                    }
                    if ((i & 1) == 0) {
                        return true;
                    }
                } else {
                    i3 = i2 % 3;
                }
            } else if ((((i >> 1) + (i2 / 3)) & 1) == 0) {
                return true;
            }
            return false;
        }
        i3 = (i * i2) % 6;
        return C12960it.A1T(i3);
    }

    public static AnonymousClass4BN valueOf(String str) {
        return (AnonymousClass4BN) Enum.valueOf(AnonymousClass4BN.class, str);
    }

    public static AnonymousClass4BN[] values() {
        return (AnonymousClass4BN[]) A00.clone();
    }
}
