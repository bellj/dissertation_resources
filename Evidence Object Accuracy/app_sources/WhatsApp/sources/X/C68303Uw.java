package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68303Uw implements AnonymousClass5Vo {
    public final /* synthetic */ AnonymousClass5Vo A00;
    public final /* synthetic */ AnonymousClass2ET A01;

    public C68303Uw(AnonymousClass5Vo r1, AnonymousClass2ET r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5Vo
    public void AQJ(C92404Vt r2, int i) {
        C16700pc.A0E(r2, 0);
        this.A00.AQJ(r2, i);
    }

    @Override // X.AnonymousClass5Vo
    public void AQK(C92404Vt r4, AnonymousClass3CI r5) {
        C16700pc.A0E(r4, 0);
        boolean isEmpty = r4.A04.isEmpty();
        C25801Aw r1 = this.A01.A03;
        UserJid userJid = r4.A02;
        C16700pc.A0B(userJid);
        r1.A03(r5, userJid, isEmpty);
        this.A00.AQK(r4, r5);
    }
}
