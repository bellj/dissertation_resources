package X;

import android.content.Context;

/* renamed from: X.0LW  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LW {
    public static final boolean A00(Context context) {
        C16700pc.A0E(context, 0);
        return (context.getResources().getConfiguration().uiMode & 48) == 32;
    }
}
