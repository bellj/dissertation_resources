package X;

/* renamed from: X.0rb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17910rb extends AbstractC17770rM implements AbstractC17920rc {
    public AnonymousClass2DR A00;
    public String A01;
    public final C14900mE A02;
    public final C16590pI A03;
    public final C17070qD A04;
    public final AbstractC14440lR A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C17910rb(C14900mE r2, C16590pI r3, C17070qD r4, C19630uQ r5, AbstractC14440lR r6) {
        super(r5);
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r3, 2);
        C16700pc.A0E(r6, 3);
        C16700pc.A0E(r4, 4);
        C16700pc.A0E(r5, 5);
        this.A02 = r2;
        this.A03 = r3;
        this.A05 = r6;
        this.A04 = r4;
    }

    @Override // X.AbstractC17920rc
    public void AIu(String str) {
        this.A01 = str;
    }
}
