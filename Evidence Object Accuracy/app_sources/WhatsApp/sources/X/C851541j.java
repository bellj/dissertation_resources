package X;

import com.whatsapp.search.SearchFragment;
import com.whatsapp.search.SearchViewModel;

/* renamed from: X.41j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C851541j extends AnonymousClass2Dn {
    public final /* synthetic */ SearchFragment A00;

    public C851541j(SearchFragment searchFragment) {
        this.A00 = searchFragment;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        SearchFragment.A01(r2, this.A00);
    }

    @Override // X.AnonymousClass2Dn
    public void A01(AbstractC14640lm r3) {
        SearchViewModel searchViewModel = this.A00.A1G;
        if (searchViewModel.A0Z()) {
            searchViewModel.A0G();
        }
    }

    @Override // X.AnonymousClass2Dn
    public void A02(AbstractC14640lm r2) {
        SearchFragment.A01(r2, this.A00);
    }
}
