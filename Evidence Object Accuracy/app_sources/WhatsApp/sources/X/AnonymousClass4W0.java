package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.4W0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4W0 {
    public Jid A00;
    public UserJid A01;
    public Boolean A02;
    public Long A03;
    public String A04;
    public final C20320vZ A05;

    public AnonymousClass4W0(C20320vZ r1) {
        this.A05 = r1;
    }

    public C28941Pp A00() {
        Jid jid = this.A00;
        boolean z = true;
        AnonymousClass009.A0B("remoteJid must be provided", C12960it.A1W(jid));
        String str = this.A04;
        AnonymousClass009.A0B("id must be provided", C12960it.A1W(str));
        Long l = this.A03;
        AnonymousClass009.A0B("timestampMillis must be provided", C12960it.A1W(l));
        Boolean bool = this.A02;
        if (bool == null) {
            z = false;
        }
        AnonymousClass009.A0B("fromMe must be provided", z);
        return new C28941Pp(jid, this.A01, this.A05, str, l.longValue(), bool.booleanValue());
    }
}
