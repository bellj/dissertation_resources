package X;

import java.util.List;

/* renamed from: X.1mq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37691mq {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final long A06;
    public final long A07;
    public final String A08;
    public final String A09;
    public final List A0A;
    public final boolean A0B;

    public C37691mq(String str, String str2, List list, int i, int i2, long j, long j2, long j3, long j4, boolean z) {
        this.A08 = str;
        this.A05 = j;
        this.A03 = j2;
        this.A06 = j3;
        this.A0A = list;
        this.A07 = j4;
        this.A04 = (j * 1000) + j4;
        this.A02 = j4 + (j2 * 1000);
        this.A09 = str2;
        this.A0B = z;
        this.A00 = i;
        this.A01 = i2;
    }
}
