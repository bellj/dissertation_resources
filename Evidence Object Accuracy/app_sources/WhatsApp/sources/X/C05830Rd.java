package X;

import android.animation.Animator;
import android.view.animation.Animation;

/* renamed from: X.0Rd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05830Rd {
    public final Animator A00;
    public final Animation A01;

    public C05830Rd(Animator animator) {
        this.A01 = null;
        this.A00 = animator;
    }

    public C05830Rd(Animation animation) {
        this.A01 = animation;
        this.A00 = null;
    }
}
