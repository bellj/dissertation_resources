package X;

import java.io.File;

/* renamed from: X.4Sm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91634Sm {
    public int A00;
    public C37971nJ A01;
    public C37971nJ A02;
    public C37931nF A03;
    public File A04;

    public C91634Sm(C37971nJ r1, C37971nJ r2, C37931nF r3, File file, int i) {
        this.A00 = i;
        this.A02 = r1;
        this.A01 = r2;
        this.A03 = r3;
        this.A04 = file;
    }
}
