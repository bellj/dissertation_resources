package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.3k3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75563k3 extends AnonymousClass03U {
    public final ImageView A00;
    public final TextView A01;
    public final TextView A02;
    public final TextEmojiLabel A03;

    public C75563k3(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.reactions_bottom_sheet_row_primary_text);
        this.A03 = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.reactions_bottom_sheet_row_emoji);
        this.A02 = C12960it.A0I(view, R.id.reactions_bottom_sheet_row_secondary_text);
        this.A00 = C12970iu.A0K(view, R.id.reactions_bottom_sheet_row_contact_image);
    }
}
