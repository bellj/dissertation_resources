package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78343ol extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99464kL();
    public final long A00;
    public final String A01;
    public final String A02;

    public C78343ol(String str, String str2, long j) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0D(parcel, this.A02, 3, C95654e8.A0K(parcel, this.A01));
        C95654e8.A08(parcel, 4, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
