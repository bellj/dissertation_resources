package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1GT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1GT extends AbstractC16230of {
    public void A05(Collection collection) {
        Integer valueOf;
        for (AnonymousClass1GS r5 : A01()) {
            if (!(r5 instanceof AnonymousClass1VV)) {
                AnonymousClass1GR r52 = (AnonymousClass1GR) r5;
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    C29871Vb r2 = r52.A00.A04;
                    r2.A01.remove(((C15370n3) it.next()).A0B(AbstractC14640lm.class));
                }
                r52.A00.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(r52, 26, collection));
            } else {
                AnonymousClass1VV r53 = (AnonymousClass1VV) r5;
                C21580xe r4 = r53.A01;
                synchronized (r4.A07) {
                    int A00 = r53.A00(collection);
                    Integer num = r4.A00;
                    if (num == null) {
                        valueOf = Integer.valueOf(A00);
                        r4.A00 = valueOf;
                    } else {
                        valueOf = Integer.valueOf(num.intValue() + A00);
                        r4.A00 = valueOf;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("contact-mgr-db/onContactsAddedOrUpdated individualContactCount = ");
                    sb.append(valueOf);
                    Log.i(sb.toString());
                }
            }
        }
    }
}
