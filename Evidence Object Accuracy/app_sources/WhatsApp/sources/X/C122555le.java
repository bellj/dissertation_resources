package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5le  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122555le extends AbstractC118835cS {
    public final TextView A00;

    public C122555le(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.text);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        this.A00.setText(((C123075mZ) r3).A00);
    }
}
