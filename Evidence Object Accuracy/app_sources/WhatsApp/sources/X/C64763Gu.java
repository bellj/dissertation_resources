package X;

import java.util.HashMap;

/* renamed from: X.3Gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64763Gu {
    public static final int[][] A01;
    public static final int[][] A02;
    public static final int[][] A03;
    public static final int[][] A04;
    public HashMap A00;

    static {
        int[][] iArr = new int[31];
        int[] iArr2 = new int[1];
        iArr2[0] = 10084;
        iArr[0] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128525;
        iArr[1] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128536;
        iArr[2] = iArr4;
        int[] iArr5 = new int[1];
        iArr5[0] = 128149;
        iArr[3] = iArr5;
        int[] iArr6 = new int[1];
        iArr6[0] = 128571;
        iArr[4] = iArr6;
        int[] iArr7 = new int[1];
        iArr7[0] = 128145;
        iArr[5] = iArr7;
        iArr[6] = new int[]{128105, 8205, 10084, 8205, 128105};
        iArr[7] = new int[]{128104, 8205, 10084, 8205, 128104};
        int[] iArr8 = new int[1];
        iArr8[0] = 128143;
        iArr[8] = iArr8;
        iArr[9] = new int[]{128105, 8205, 10084, 8205, 128139, 8205, 128105};
        iArr[10] = new int[]{128104, 8205, 10084, 8205, 128139, 8205, 128104};
        int[] iArr9 = new int[1];
        iArr9[0] = 10084;
        iArr[11] = iArr9;
        int[] iArr10 = new int[1];
        iArr10[0] = 129505;
        iArr[12] = iArr10;
        int[] iArr11 = new int[1];
        iArr11[0] = 128155;
        iArr[13] = iArr11;
        int[] iArr12 = new int[1];
        iArr12[0] = 128154;
        iArr[14] = iArr12;
        int[] iArr13 = new int[1];
        iArr13[0] = 128153;
        iArr[15] = iArr13;
        int[] iArr14 = new int[1];
        iArr14[0] = 128156;
        iArr[16] = iArr14;
        int[] iArr15 = new int[1];
        iArr15[0] = 128420;
        iArr[17] = iArr15;
        int[] iArr16 = new int[1];
        iArr16[0] = 128148;
        iArr[18] = iArr16;
        int[] iArr17 = new int[1];
        iArr17[0] = 10083;
        iArr[19] = iArr17;
        C12960it.A1Q(iArr, new int[]{128149});
        A02 = iArr;
        int[][] iArr18 = new int[13];
        iArr18[4] = C12960it.A1a(iArr18);
        int[] iArr19 = new int[1];
        iArr19[0] = 128576;
        iArr18[5] = iArr19;
        int[] iArr20 = new int[1];
        iArr20[0] = 128561;
        iArr18[6] = iArr20;
        int[] iArr21 = new int[1];
        iArr21[0] = 129327;
        iArr18[7] = iArr21;
        int[] iArr22 = new int[1];
        iArr22[0] = 128563;
        iArr18[8] = iArr22;
        int[] iArr23 = new int[1];
        iArr23[0] = 128576;
        iArr18[9] = iArr23;
        int[] iArr24 = new int[1];
        iArr24[0] = 10071;
        iArr18[10] = iArr24;
        int[] iArr25 = new int[1];
        iArr25[0] = 10069;
        iArr18[11] = iArr25;
        int[] iArr26 = new int[1];
        iArr26[0] = 129325;
        iArr18[12] = iArr26;
        A04 = iArr18;
        int[][] A1b = C12960it.A1b();
        int[] iArr27 = new int[1];
        iArr27[0] = 128546;
        A1b[5] = iArr27;
        int[] A1Z = C12980iv.A1Z(A1b, 6, 7);
        A1Z[0] = 128532;
        A1b[8] = A1Z;
        int[] iArr28 = new int[1];
        iArr28[0] = 128543;
        A1b[9] = iArr28;
        int[] iArr29 = new int[1];
        iArr29[0] = 128533;
        A1b[10] = iArr29;
        int[] iArr30 = new int[1];
        iArr30[0] = 128548;
        A1b[11] = iArr30;
        int[] iArr31 = new int[1];
        iArr31[0] = 128544;
        A1b[12] = iArr31;
        int[] iArr32 = new int[1];
        iArr32[0] = 128549;
        A1b[13] = iArr32;
        int[] iArr33 = new int[1];
        iArr33[0] = 128560;
        A1b[14] = iArr33;
        int[] iArr34 = new int[1];
        iArr34[0] = 128552;
        A1b[15] = iArr34;
        int[] iArr35 = new int[1];
        iArr35[0] = 128575;
        A1b[16] = iArr35;
        int[] iArr36 = new int[1];
        iArr36[0] = 128574;
        A1b[17] = iArr36;
        int[] iArr37 = new int[1];
        iArr37[0] = 128531;
        A1b[18] = iArr37;
        A1b[19] = new int[]{128589, 8205, 9794};
        A1b[20] = new int[]{128589, 8205, 9792};
        A03 = A1b;
        int[][] iArr38 = new int[18];
        iArr38[4] = C12960it.A1Y(iArr38);
        int[] iArr39 = new int[1];
        iArr39[0] = 128517;
        iArr38[5] = iArr39;
        int[] iArr40 = new int[1];
        iArr40[0] = 128514;
        iArr38[6] = iArr40;
        int[] iArr41 = new int[1];
        iArr41[0] = 129315;
        iArr38[7] = iArr41;
        int[] iArr42 = new int[1];
        iArr42[0] = 128578;
        iArr38[8] = iArr42;
        int[] iArr43 = new int[1];
        iArr43[0] = 128539;
        iArr38[9] = iArr43;
        int[] iArr44 = new int[1];
        iArr44[0] = 128541;
        iArr38[10] = iArr44;
        int[] iArr45 = new int[1];
        iArr45[0] = 128540;
        iArr38[11] = iArr45;
        int[] iArr46 = new int[1];
        iArr46[0] = 129322;
        iArr38[12] = iArr46;
        int[] iArr47 = new int[1];
        iArr47[0] = 129303;
        iArr38[13] = iArr47;
        int[] iArr48 = new int[1];
        iArr48[0] = 128570;
        iArr38[14] = iArr48;
        int[] iArr49 = new int[1];
        iArr49[0] = 128568;
        iArr38[15] = iArr49;
        int[] iArr50 = new int[1];
        iArr50[0] = 128569;
        iArr38[16] = iArr50;
        int[] iArr51 = new int[1];
        iArr51[0] = 9786;
        iArr38[17] = iArr51;
        A01 = iArr38;
    }

    public C64763Gu() {
        HashMap hashMap = this.A00;
        if (hashMap == null) {
            this.A00 = C12970iu.A11();
        } else {
            hashMap.clear();
        }
        A00(A01, 1);
        A00(A04, 8);
        A00(A03, 4);
        A00(A02, 2);
    }

    public final void A00(int[][] iArr, int i) {
        int i2;
        for (int[] iArr2 : iArr) {
            C37471mS r2 = new C37471mS(iArr2);
            HashMap hashMap = this.A00;
            if (hashMap.containsKey(r2)) {
                i2 = C12960it.A05(hashMap.get(r2));
            } else {
                i2 = 0;
            }
            C12960it.A1K(r2, hashMap, i2 | i);
        }
    }
}
