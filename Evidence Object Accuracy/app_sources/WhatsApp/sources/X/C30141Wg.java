package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30141Wg implements Parcelable {
    public static final String[] A0L = {"category_id", "category_name"};
    public static final String[] A0M = {"time_zone", "hours_note", "day_of_week", "mode", "open_time", "close_time", "wa_biz_profiles_hours._id"};
    public static final String[] A0N = {"account_id", "account_type", "account_display_name", "account_fan_count"};
    public static final Parcelable.Creator CREATOR = new C99674kg();
    public final C30201Wm A00;
    public final C30181Wk A01;
    public final C30231Wp A02;
    public final C30171Wj A03;
    public final UserJid A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final List A0E;
    public final List A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C30141Wg(C30201Wm r2, C30181Wk r3, C30231Wp r4, C30171Wj r5, UserJid userJid, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, List list, List list2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.A04 = userJid;
        this.A0C = str;
        this.A0E = Collections.unmodifiableList(list);
        this.A0F = Collections.unmodifiableList(list2);
        this.A0A = str2;
        this.A09 = str3;
        this.A03 = r5;
        this.A00 = r2;
        this.A0I = z;
        this.A05 = str4;
        this.A0B = str5;
        this.A06 = str6;
        this.A0G = z2;
        this.A0D = str7;
        this.A0H = z3;
        this.A0K = z4;
        this.A02 = r4;
        this.A08 = str8;
        this.A07 = str9;
        this.A0J = z5;
        this.A01 = r3;
    }

    public C30141Wg(Parcel parcel) {
        this.A04 = (UserJid) parcel.readParcelable(UserJid.class.getClassLoader());
        this.A0C = parcel.readString();
        this.A0E = Collections.unmodifiableList(parcel.createTypedArrayList(C30211Wn.CREATOR));
        this.A0F = Collections.unmodifiableList(parcel.createStringArrayList());
        this.A0A = parcel.readString();
        this.A09 = parcel.readString();
        C30171Wj r0 = (C30171Wj) parcel.readParcelable(C30171Wj.class.getClassLoader());
        this.A03 = r0 == null ? C30171Wj.A04 : r0;
        this.A00 = (C30201Wm) parcel.readParcelable(C30201Wm.class.getClassLoader());
        boolean z = true;
        this.A0I = parcel.readByte() != 0;
        this.A0D = parcel.readString();
        this.A05 = parcel.readString();
        this.A0B = parcel.readString();
        this.A06 = parcel.readString();
        this.A0G = parcel.readByte() != 0;
        this.A0H = parcel.readByte() != 0;
        this.A0K = parcel.readByte() != 0;
        this.A02 = (C30231Wp) parcel.readParcelable(C30231Wp.class.getClassLoader());
        this.A08 = parcel.readString();
        this.A07 = parcel.readString();
        this.A0J = parcel.readByte() == 0 ? false : z;
        this.A01 = (C30181Wk) parcel.readParcelable(C30181Wk.class.getClassLoader());
    }

    public boolean A00() {
        Iterator it = this.A0F.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!TextUtils.isEmpty((String) it.next())) {
                    break;
                }
            } else if (!TextUtils.isEmpty(this.A09) || !TextUtils.isEmpty(this.A0A) || !this.A03.equals(C30171Wj.A04) || this.A00 != null) {
                break;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30141Wg)) {
            return false;
        }
        C30141Wg r9 = (C30141Wg) obj;
        if (!C29941Vi.A00(this.A04, r9.A04) || !AnonymousClass1US.A0D(this.A0C, r9.A0C) || !this.A0E.equals(r9.A0E)) {
            return false;
        }
        List list = this.A0F;
        List list2 = r9.A0F;
        ArrayList arrayList = new ArrayList(list);
        ArrayList arrayList2 = new ArrayList(list2);
        arrayList.removeAll(Arrays.asList("", null));
        arrayList2.removeAll(Arrays.asList("", null));
        if (!arrayList.equals(arrayList2) || !AnonymousClass1US.A0D(this.A0A, r9.A0A) || !AnonymousClass1US.A0D(this.A09, r9.A09) || !C29941Vi.A00(this.A03, r9.A03) || !C29941Vi.A00(this.A00, r9.A00) || this.A0I != r9.A0I || !AnonymousClass1US.A0D(this.A05, r9.A05) || !AnonymousClass1US.A0D(this.A0B, r9.A0B) || this.A0G != r9.A0G || !AnonymousClass1US.A0D(this.A0D, r9.A0D) || !AnonymousClass1US.A0D(this.A06, r9.A06) || this.A0H != r9.A0H || this.A0K != r9.A0K || !C29941Vi.A00(this.A02, r9.A02) || !AnonymousClass1US.A0D(this.A07, r9.A07) || !AnonymousClass1US.A0D(this.A08, r9.A08) || this.A0J != r9.A0J || !C29941Vi.A00(this.A01, r9.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        UserJid userJid = this.A04;
        int i14 = 0;
        if (userJid != null) {
            i = userJid.hashCode();
        } else {
            i = 0;
        }
        int i15 = i * 31;
        String str = this.A0C;
        if (str != null) {
            i2 = str.hashCode();
        } else {
            i2 = 0;
        }
        int i16 = (i15 + i2) * 31;
        String str2 = this.A0D;
        if (str2 != null) {
            i3 = str2.hashCode();
        } else {
            i3 = 0;
        }
        int hashCode = (((((i16 + i3) * 31) + this.A0E.hashCode()) * 31) + this.A0F.hashCode()) * 31;
        String str3 = this.A0A;
        if (str3 != null) {
            i4 = str3.hashCode();
        } else {
            i4 = 0;
        }
        int i17 = (hashCode + i4) * 31;
        String str4 = this.A09;
        if (str4 != null) {
            i5 = str4.hashCode();
        } else {
            i5 = 0;
        }
        int i18 = (i17 + i5) * 31;
        C30171Wj r0 = this.A03;
        if (r0 != null) {
            i6 = r0.hashCode();
        } else {
            i6 = 0;
        }
        int i19 = (i18 + i6) * 31;
        C30201Wm r02 = this.A00;
        if (r02 != null) {
            i7 = r02.hashCode();
        } else {
            i7 = 0;
        }
        int i20 = (((i19 + i7) * 31) + (this.A0I ? 1 : 0)) * 31;
        String str5 = this.A05;
        if (str5 != null) {
            i8 = str5.hashCode();
        } else {
            i8 = 0;
        }
        int i21 = (i20 + i8) * 31;
        String str6 = this.A0B;
        if (str6 != null) {
            i9 = str6.hashCode();
        } else {
            i9 = 0;
        }
        int i22 = (i21 + i9) * 31;
        String str7 = this.A06;
        if (str7 != null) {
            i10 = str7.hashCode();
        } else {
            i10 = 0;
        }
        int i23 = (((((((i22 + i10) * 31) + (this.A0G ? 1 : 0)) * 31) + (this.A0H ? 1 : 0)) * 31) + (this.A0K ? 1 : 0)) * 31;
        C30231Wp r03 = this.A02;
        if (r03 != null) {
            i11 = r03.hashCode();
        } else {
            i11 = 0;
        }
        int i24 = (i23 + i11) * 31;
        String str8 = this.A08;
        if (str8 != null) {
            i12 = str8.hashCode();
        } else {
            i12 = 0;
        }
        int i25 = (i24 + i12) * 31;
        String str9 = this.A07;
        if (str9 != null) {
            i13 = str9.hashCode();
        } else {
            i13 = 0;
        }
        int i26 = (((i25 + i13) * 31) + (this.A0J ? 1 : 0)) * 31;
        C30181Wk r04 = this.A01;
        if (r04 != null) {
            i14 = r04.hashCode();
        }
        return i26 + i14;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("BusinessProfile{jid='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", tag='");
        sb.append(this.A0C);
        sb.append('\'');
        sb.append(", websites=");
        sb.append(this.A0F);
        sb.append(", email='");
        sb.append(this.A0A);
        sb.append('\'');
        sb.append(", description='");
        sb.append(this.A09);
        sb.append('\'');
        sb.append(", address='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", vertical='");
        sb.append(this.A0D);
        sb.append('\'');
        sb.append(", categories='");
        sb.append(this.A0E.toString());
        sb.append('\'');
        sb.append(", hours='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", has_catalog='");
        sb.append(this.A0I);
        sb.append('\'');
        sb.append(", commerceExperience='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", shopUrl='");
        sb.append(this.A0B);
        sb.append('\'');
        sb.append(", commerceManagerUrl='");
        sb.append(this.A06);
        sb.append('\'');
        sb.append(", cart_enabled='");
        sb.append(this.A0G);
        sb.append('\'');
        sb.append(", directConnectionEnabled='");
        sb.append(this.A0H);
        sb.append('\'');
        sb.append(", shopBanned='");
        sb.append(this.A0K);
        sb.append('\'');
        sb.append(", isGalaxyBusiness='");
        sb.append(this.A0J);
        sb.append(", coverPhoto='");
        C30181Wk r1 = this.A01;
        String str = "null";
        if (r1 != null) {
            str = r1.toString();
        }
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A04, i);
        parcel.writeString(this.A0C);
        parcel.writeTypedList(this.A0E);
        parcel.writeStringList(this.A0F);
        parcel.writeString(this.A0A);
        parcel.writeString(this.A09);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A00, i);
        parcel.writeByte(this.A0I ? (byte) 1 : 0);
        parcel.writeString(this.A0D);
        parcel.writeString(this.A05);
        parcel.writeString(this.A0B);
        parcel.writeString(this.A06);
        parcel.writeByte(this.A0G ? (byte) 1 : 0);
        parcel.writeByte(this.A0H ? (byte) 1 : 0);
        parcel.writeByte(this.A0K ? (byte) 1 : 0);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A08);
        parcel.writeString(this.A07);
        parcel.writeByte(this.A0J ? (byte) 1 : 0);
        parcel.writeParcelable(this.A01, i);
    }
}
