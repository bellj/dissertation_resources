package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.5zX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130515zX {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "1";
        A02 = C12960it.A0m("2", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "mobile_number";
        A01 = C12960it.A0m("numeric_id", strArr2, 1);
    }

    public C130515zX(C128665wT r12, String str, String str2, String str3, String str4, String str5) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy r3 = new C41141sy("account");
        C41141sy.A01(r3, "action", "upi-get-vpa-name");
        if (str != null && C117295Zj.A1W(str, 1, true)) {
            C41141sy.A01(r3, "vpa", str);
        }
        if (C117295Zj.A1V(str2, 1, false)) {
            C41141sy.A01(r3, "device-id", str2);
        }
        if (str3 != null && AnonymousClass3JT.A0E(str3, 8, 10, true)) {
            C41141sy.A01(r3, "value", str3);
        }
        ArrayList arrayList = A02;
        if (str4 != null) {
            r3.A0A(str4, "version", arrayList);
        }
        ArrayList arrayList2 = A01;
        if (str5 != null) {
            r3.A0A(str5, "type", arrayList2);
        }
        C117295Zj.A1H(r3, A0M);
        AnonymousClass1V8 r32 = r12.A00;
        A0M.A07(r32, C12960it.A0l());
        A0M.A09(r32, Arrays.asList(new String[0]), C12960it.A0l());
        this.A00 = A0M.A03();
    }
}
