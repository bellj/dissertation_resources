package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.4VP  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4VP {
    public final String A00;
    public final String A01;

    public AnonymousClass4VP(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    public void A00(Map map, AnonymousClass5ZQ r12) {
        Object A00;
        JSONObject jSONObject;
        Object A002;
        Object A003;
        if (this instanceof AnonymousClass46Q) {
            AnonymousClass46Q r3 = (AnonymousClass46Q) this;
            if (map == null) {
                map = C71373cp.A00;
            }
            Object A004 = AnonymousClass46T.A00(map, r3.A00);
            if (A004 != null) {
                Object A005 = AnonymousClass46T.A00(A004, r3.A01);
                if (A005 != null) {
                    r12.AJ5(((AnonymousClass4VP) r3).A00, A005);
                    return;
                }
                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
            }
            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
        } else if (this instanceof AnonymousClass46T) {
            throw new C87394Bi(C16700pc.A08("An operation is not implemented: ", "Not yet implemented"));
        } else if (!(this instanceof AnonymousClass46S)) {
            AnonymousClass46R r5 = (AnonymousClass46R) this;
            String str = null;
            if (map == null || (A003 = AnonymousClass46T.A00(map, r5.A01)) == null) {
                jSONObject = new JSONObject();
            } else {
                jSONObject = new JSONObject((Map) A003);
            }
            List list = r5.A03;
            String str2 = r5.A00;
            C16700pc.A0E(str2, 1);
            try {
                ArrayList A0l = C12960it.A0l();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    JSONObject jSONObject2 = new JSONObject(C12970iu.A0x(it));
                    if (jSONObject2.has("next")) {
                        String string = jSONObject2.getString("next");
                        AnonymousClass5UW A006 = AnonymousClass4ET.A00(jSONObject2);
                        if (A006 != null) {
                            C16700pc.A0B(string);
                            A0l.add(new AnonymousClass4O2(A006, string));
                        }
                    }
                }
                AnonymousClass009.A0A("expected at least 1 choice", C12960it.A1T(A0l.isEmpty() ? 1 : 0));
                AnonymousClass4O3 r4 = new AnonymousClass4O3(str2, A0l);
                String obj = jSONObject.toString();
                C16700pc.A0B(obj);
                AnonymousClass4V4 r32 = new AnonymousClass4V4(obj);
                Iterator it2 = r4.A01.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        str = r4.A00;
                        break;
                    }
                    AnonymousClass4O2 r1 = (AnonymousClass4O2) it2.next();
                    if (r1.A00.A9g(r32) && (str = r1.A01) != null) {
                        break;
                    }
                }
            } catch (JSONException unused) {
            }
            if (map == null || (A002 = AnonymousClass46T.A00(map, r5.A02)) == null) {
                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any>");
            }
            r12.AJ5(str, A002);
        } else {
            AnonymousClass46S r52 = (AnonymousClass46S) this;
            if (map == null) {
                map = C71373cp.A00;
            }
            Object A007 = AnonymousClass46T.A00(map, r52.A00);
            if (A007 != null) {
                Map map2 = (Map) A007;
                Map map3 = r52.A03;
                C16700pc.A0E(map2, 0);
                AnonymousClass4O5 r0 = new AnonymousClass4O5(map2);
                if (map3 == null) {
                    A00 = r0.A01;
                } else {
                    A00 = C95334dX.A00(r0, map3);
                    if (A00 == null) {
                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.Any");
                    }
                }
                Object A01 = C95334dX.A01(A00, r52.A02, map);
                if (A01 != null) {
                    Object A008 = AnonymousClass46T.A00(A01, r52.A01);
                    if (A008 != null) {
                        r12.AJ5(((AnonymousClass4VP) r52).A00, A008);
                        return;
                    }
                    throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
                }
                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
            }
            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
        }
    }
}
