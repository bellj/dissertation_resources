package X;

import android.util.Pair;
import java.util.Stack;

/* renamed from: X.3IG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IG {
    public static final C93944b0 A00;
    public static final C93944b0 A01;
    public static final C93944b0 A02;

    static {
        C93944b0 r3 = new C93944b0("({[", ")}]");
        A02 = r3;
        C93944b0 r2 = new C93944b0("*~_", "*~_");
        A00 = r2;
        C93944b0[] r1 = new C93944b0[2];
        C12970iu.A1U(r3, r2, r1);
        A01 = new C93944b0(r1);
    }

    public static Pair A00(Pair pair, C93944b0 r6, CharSequence charSequence) {
        char c;
        Object valueOf;
        Object obj;
        int A05 = C12960it.A05(pair.first);
        char c2 = 0;
        if (A05 != 0) {
            c = charSequence.charAt(A05 - 1);
        } else {
            c = 0;
        }
        if (C12960it.A05(pair.second) != charSequence.length()) {
            c2 = charSequence.charAt(C12960it.A05(pair.second));
        }
        char charAt = charSequence.charAt(C12960it.A05(pair.first));
        char charAt2 = charSequence.charAt(C12960it.A05(pair.second) - 1);
        if (!r6.A00(c, c2)) {
            if (r6.A00(c, charAt2)) {
                valueOf = pair.first;
            } else if (r6.A00(charAt, c2)) {
                valueOf = Integer.valueOf(C12960it.A05(pair.first) + 1);
                obj = pair.second;
                return C12990iw.A0L(valueOf, obj);
            } else if (r6.A00(charAt, charAt2)) {
                valueOf = Integer.valueOf(C12960it.A05(pair.first) + 1);
            }
            obj = Integer.valueOf(C12960it.A05(pair.second) - 1);
            return C12990iw.A0L(valueOf, obj);
        }
        return pair;
    }

    public static boolean A01(Pair pair, CharSequence charSequence) {
        C93944b0 r5 = A02;
        Stack stack = new Stack();
        for (int A05 = C12960it.A05(pair.first); A05 < C12960it.A05(pair.second); A05++) {
            char charAt = charSequence.charAt(A05);
            String valueOf = String.valueOf(charAt);
            if ("({[".contains(valueOf)) {
                stack.push(Character.valueOf(charAt));
            } else if (!")}]".contains(valueOf)) {
                continue;
            } else if (stack.empty() || !r5.A00(((Character) stack.peek()).charValue(), charAt)) {
                return false;
            } else {
                stack.pop();
            }
        }
        return stack.empty();
    }
}
