package X;

import java.util.Collections;
import java.util.Map;

/* renamed from: X.4bU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94244bU {
    public final Map A00;
    public final Map A01;
    public final Map A02;
    public final Map A03;

    public C94244bU() {
        this.A02 = Collections.emptyMap();
        this.A01 = Collections.emptyMap();
        this.A03 = Collections.emptyMap();
        this.A00 = Collections.emptyMap();
    }

    public C94244bU(Map map, Map map2, Map map3, Map map4) {
        this.A02 = map;
        this.A01 = map2;
        this.A03 = map3;
        this.A00 = map4;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C94244bU)) {
            return false;
        }
        C94244bU r4 = (C94244bU) obj;
        if (this.A01 == r4.A01 && this.A00 == r4.A00 && this.A02 == r4.A02 && this.A03.equals(r4.A03)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((this.A02.hashCode() ^ this.A01.hashCode()) ^ this.A03.hashCode()) ^ this.A00.hashCode();
    }
}
