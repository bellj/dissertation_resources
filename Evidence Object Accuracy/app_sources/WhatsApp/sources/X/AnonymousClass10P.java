package X;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.10P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10P implements AbstractC18870tC, AbstractC20260vT {
    public boolean A00;
    public boolean A01;
    public final C14900mE A02;
    public final C16240og A03;
    public final C18640sm A04;
    public final C21860y6 A05;
    public final C241314i A06;
    public final C18600si A07;
    public final AnonymousClass1FJ A08;
    public final AnonymousClass1A7 A09;
    public final C30931Zj A0A = C30931Zj.A00("PaymentsConnectivityManager", "network", "COMMON");

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARC() {
    }

    public AnonymousClass10P(C14900mE r4, C16240og r5, C18640sm r6, C21860y6 r7, C241314i r8, C18600si r9, AnonymousClass1FJ r10, AnonymousClass1A7 r11) {
        this.A02 = r4;
        this.A03 = r5;
        this.A07 = r9;
        this.A05 = r7;
        this.A06 = r8;
        this.A09 = r11;
        this.A04 = r6;
        this.A08 = r10;
    }

    public final void A00() {
        Map map;
        Set keySet;
        AnonymousClass1FK r2;
        C241314i r6 = this.A06;
        synchronized (r6) {
            map = r6.A00;
            keySet = map.keySet();
        }
        for (Object obj : keySet) {
            synchronized (r6) {
                r2 = (AnonymousClass1FK) map.get(obj);
            }
            C452120p r1 = new C452120p(7);
            synchronized (r6) {
                r6.A02.remove(obj);
            }
            r2.AVA(r1);
        }
        r6.A00();
        this.A01 = false;
    }

    @Override // X.AbstractC20260vT
    public synchronized void AOa(AnonymousClass1I1 r5) {
        C30931Zj r3 = this.A0A;
        StringBuilder sb = new StringBuilder();
        sb.append("Connectivity connected: ");
        boolean z = r5.A01;
        sb.append(z);
        r3.A06(sb.toString());
        if (this.A00 && !z) {
            A00();
        }
    }

    @Override // X.AbstractC18870tC
    public synchronized void AR9() {
        C21860y6 r0;
        this.A0A.A06("ChatConnectivity connected");
        if (this.A00 && (r0 = this.A05) != null && r0.A0C()) {
            C18600si r1 = this.A07;
            if (r1.A01.A00() - r1.A01().getLong("payments_pending_transactions_last_sync_time", 0) > TimeUnit.DAYS.toMillis(1)) {
                this.A08.A00(new AbstractC452320r() { // from class: X.58B
                    @Override // X.AbstractC452320r
                    public final void Aeg(AnonymousClass1IR r5) {
                        AnonymousClass10P r02 = AnonymousClass10P.this;
                        r02.A09.A01(r02.A08, r5.A0K, r5.A0J());
                    }
                });
            }
        }
    }

    @Override // X.AbstractC18870tC
    public synchronized void ARB() {
        this.A0A.A06("ChatConnectivity disconnected");
        if (this.A00) {
            A00();
        }
    }
}
