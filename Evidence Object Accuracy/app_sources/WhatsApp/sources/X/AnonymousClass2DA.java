package X;

/* renamed from: X.2DA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DA extends AnonymousClass1OF implements AnonymousClass2D8 {
    public final AnonymousClass2D8 A00;
    public final C244315m A01;

    public AnonymousClass2DA(AnonymousClass2D8 r1, C244315m r2, C32881ct r3, AbstractC14440lR r4) {
        super(r3, r4);
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2D8
    public void AWu() {
        super.A00.A01();
        this.A00.AWu();
    }
}
