package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/* renamed from: X.24w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C461824w {
    public final long A00;
    public final AbstractC14640lm A01;
    public final UserJid A02;
    public final String A03;
    public final boolean A04;

    public C461824w(AbstractC14640lm r1, UserJid userJid, String str, long j, boolean z) {
        this.A00 = j;
        this.A04 = z;
        this.A03 = str;
        this.A01 = r1;
        this.A02 = userJid;
    }

    public AnonymousClass25S A00() {
        UserJid userJid;
        AnonymousClass1G9 r7 = (AnonymousClass1G9) AnonymousClass1G8.A05.A0T();
        r7.A05(this.A03);
        boolean z = this.A04;
        r7.A08(z);
        AbstractC14640lm r1 = this.A01;
        r7.A07(r1.getRawString());
        if (C15380n4.A0J(r1) && !z && (userJid = this.A02) != null) {
            r7.A06(userJid.getRawString());
        }
        AnonymousClass1G4 A0T = AnonymousClass25S.A03.A0T();
        long seconds = TimeUnit.MILLISECONDS.toSeconds(this.A00);
        if (seconds > 0) {
            A0T.A03();
            AnonymousClass25S r12 = (AnonymousClass25S) A0T.A00;
            r12.A00 |= 2;
            r12.A01 = seconds;
        }
        A0T.A03();
        AnonymousClass25S r13 = (AnonymousClass25S) A0T.A00;
        r13.A02 = (AnonymousClass1G8) r7.A02();
        r13.A00 |= 1;
        return (AnonymousClass25S) A0T.A02();
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C461824w r5 = (C461824w) obj;
            if (this.A04 != r5.A04 || !this.A03.equals(r5.A03) || !this.A01.equals(r5.A01) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Boolean.valueOf(this.A04), this.A03, this.A01, this.A02});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdMessage{timestamp=");
        sb.append(this.A00);
        sb.append(", isFromMe=");
        sb.append(this.A04);
        sb.append(", messageId=");
        sb.append(this.A03);
        sb.append(", remoteJid=");
        sb.append(this.A01);
        sb.append(", participant=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
