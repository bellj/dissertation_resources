package X;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.List;

/* renamed from: X.2B7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2B7 {
    public ObjectAnimator A00;
    public AbstractC15290ms A01;
    public C91584Sh A02;
    public C69893aP A03;
    public AbstractC116245Ur A04;
    public AnonymousClass341 A05;
    public final AnonymousClass1AC A06;
    public final C22210yi A07;
    public final AnonymousClass15H A08;
    public final AnonymousClass1AB A09;
    public final AnonymousClass146 A0A;
    public final C235512c A0B;
    public final AbstractC116245Ur A0C = new C69863aM(this);
    public final C255519v A0D;
    public final AnonymousClass1BQ A0E;
    public final C255619w A0F;
    public final AnonymousClass1KT A0G;
    public final AbstractView$OnClickListenerC34281fs A0H = new ViewOnClickCListenerShape14S0100000_I0_1(this, 27);

    public AnonymousClass2B7(AnonymousClass1AC r3, C22210yi r4, AnonymousClass15H r5, AnonymousClass1AB r6, AnonymousClass146 r7, C235512c r8, C255519v r9, AnonymousClass1BQ r10, C255619w r11, AnonymousClass1KT r12) {
        this.A07 = r4;
        this.A0G = r12;
        this.A0A = r7;
        this.A0B = r8;
        this.A06 = r3;
        this.A09 = r6;
        this.A08 = r5;
        this.A0F = r11;
        this.A0E = r10;
        this.A0D = r9;
    }

    public void A00() {
        this.A02.A02.setVisibility(0);
        boolean z = this.A06.A04.A00.getBoolean("sticker_store_onboarding_badge_shown", false);
        C91584Sh r0 = this.A02;
        if (!z) {
            r0.A01.setVisibility(0);
            ObjectAnimator objectAnimator = this.A00;
            if (objectAnimator == null) {
                objectAnimator = ObjectAnimator.ofPropertyValuesHolder(this.A02.A01, PropertyValuesHolder.ofFloat("scaleX", 1.4f), PropertyValuesHolder.ofFloat("scaleY", 1.4f), PropertyValuesHolder.ofFloat("alpha", 0.4f, 0.0f));
                this.A00 = objectAnimator;
            }
            objectAnimator.setDuration(1500L);
            this.A00.setRepeatCount(-1);
            this.A00.setRepeatMode(1);
            this.A00.start();
            return;
        }
        r0.A01.setVisibility(8);
    }

    public boolean A01() {
        AnonymousClass1BQ r0;
        if (this.A0D.A00 && (r0 = this.A0E) != null) {
            AnonymousClass016 r1 = r0.A03;
            if (r1.A01() != null && !((List) r1.A01()).isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
