package X;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3AM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AM {
    public static void A00(View view, C14260l7 r8, AnonymousClass28D r9, AbstractC21000wf r10) {
        TextView A0I = C12960it.A0I(view, R.id.text);
        String A0I2 = r9.A0I(38);
        if (A0I2 != null) {
            A0I.setText(A0I2);
        }
        View A0D = AnonymousClass028.A0D(view, R.id.checkbox);
        View A0D2 = AnonymousClass028.A0D(view, R.id.wabloks_checkbox);
        if (r9.A0B(41, 0) > 0) {
            float x = A0D2.getX();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(x, ((float) A0D2.getResources().getDimensionPixelSize(R.dimen.error_wiggle_animation_offset)) + x);
            ofFloat.setInterpolator(new LinearInterpolator());
            ofFloat.setRepeatCount(3);
            ofFloat.setRepeatMode(2);
            ofFloat.setDuration(50L);
            C12980iv.A11(ofFloat, A0D2, 3);
            ofFloat.addListener(new C72763f8(A0D2, x));
            ofFloat.start();
            r10.AeY();
        }
        C12990iw.A1C(A0D2, A0D, r9, r8, 4);
    }
}
