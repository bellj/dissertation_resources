package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/* renamed from: X.1Pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC28901Pl implements Parcelable {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 0;
    public int A04;
    public long A05;
    public long A06;
    public C17930rd A07 = C17930rd.A0F;
    public AnonymousClass1ZY A08;
    public AnonymousClass1ZR A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public byte[] A0D;

    public static String A02(int i) {
        switch (i) {
            case 1:
                return "Debit";
            case 2:
                return "Bank Account";
            case 3:
                return "PaymentWallet";
            case 4:
                return "Credit";
            case 5:
                return "Business Account";
            case 6:
                return "Combo";
            case 7:
            default:
                return null;
            case 8:
                return "Prepaid";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r2) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            r1 = 2
            if (r0 != 0) goto L_0x000e
            int r0 = r2.hashCode()
            switch(r0) {
                case -1211756856: goto L_0x001f;
                case 35394935: goto L_0x0017;
                case 2066319421: goto L_0x0010;
                default: goto L_0x000e;
            }
        L_0x000e:
            r1 = 0
        L_0x000f:
            return r1
        L_0x0010:
            java.lang.String r0 = "FAILED"
            boolean r0 = r2.equals(r0)
            goto L_0x0026
        L_0x0017:
            java.lang.String r0 = "PENDING"
            boolean r0 = r2.equals(r0)
            r1 = 1
            goto L_0x0026
        L_0x001f:
            java.lang.String r0 = "VERIFIED"
            boolean r0 = r2.equals(r0)
            r1 = 3
        L_0x0026:
            if (r0 != 0) goto L_0x000f
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28901Pl.A00(java.lang.String):int");
    }

    public static AbstractC28901Pl A01(C17930rd r10, String str, String str2, String str3, int i) {
        if (i != 1) {
            if (i == 2) {
                C30861Zc r2 = new C30861Zc(r10, 0, 0, -1, -1);
                r2.A0A = str2;
                r2.A0A(str3);
                r2.A0B = str;
                return r2;
            } else if (i == 3) {
                C30841Za r22 = new C30841Za(r10, str2, str3, BigDecimal.ZERO, r10.A05, 0, 0);
                r22.A08 = null;
                return r22;
            } else if (!(i == 4 || i == 6 || i == 8)) {
                return null;
            }
        }
        return new C30881Ze(r10, str2, str, str3, i, 0, 0, 0, 0, C30881Ze.A05(str));
    }

    public static List A03(C17930rd r5, List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl r2 = (AbstractC28901Pl) it.next();
            if (C37871n9.A03(r5.A09, r2.A04())) {
                if (r2.A01 == 2) {
                    arrayList.add(0, r2);
                } else {
                    arrayList.add(r2);
                }
            }
        }
        return arrayList;
    }

    public int A04() {
        if (this instanceof C30841Za) {
            return 3;
        }
        if (this instanceof AnonymousClass1ZW) {
            return 5;
        }
        if (!(this instanceof C30881Ze)) {
            return 2;
        }
        return ((C30881Ze) this).A00;
    }

    public Bitmap A05() {
        byte[] bArr;
        int A04 = A04();
        if ((A04 == 1 || A04 == 2 || A04 == 3 || A04 == 4 || A04 == 6 || A04 == 7) && (bArr = this.A0D) != null) {
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        }
        return null;
    }

    public void A06(int i) {
        if (!(this instanceof C30841Za) && !(this instanceof AnonymousClass1ZW) && (this instanceof C30881Ze)) {
            ((C30881Ze) this).A00 = i;
        }
    }

    public void A07(int i) {
        int A04;
        if (i != 1 || this.A07.A01 == (A04 = A04())) {
            this.A00 = i;
            return;
        }
        StringBuilder sb = new StringBuilder("PAY: ");
        sb.append(A04);
        sb.append(" in country cannot be legacy primary account type");
        throw new IllegalArgumentException(sb.toString());
    }

    public void A08(int i) {
        int A04;
        if (i != 1 || this.A07.A00 == (A04 = A04())) {
            this.A01 = i;
            return;
        }
        StringBuilder sb = new StringBuilder("PAY: ");
        sb.append(A04);
        sb.append(" in country cannot be legacy primary account type");
        throw new IllegalArgumentException(sb.toString());
    }

    public void A09(Parcel parcel) {
        this.A0A = parcel.readString();
        this.A07 = C17930rd.A00(parcel.readString().trim().toUpperCase(Locale.US));
        this.A09 = (AnonymousClass1ZR) parcel.readParcelable(AnonymousClass1ZR.class.getClassLoader());
        this.A0B = parcel.readString();
        this.A0C = parcel.readString();
        this.A04 = parcel.readInt();
        this.A05 = parcel.readLong();
        this.A06 = parcel.readLong();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A02 = parcel.readInt();
        int readInt = parcel.readInt();
        this.A0D = null;
        if (readInt != 0) {
            byte[] bArr = new byte[readInt];
            this.A0D = bArr;
            parcel.readByteArray(bArr);
        }
        this.A08 = null;
        if (parcel.readByte() == 1) {
            this.A08 = (AnonymousClass1ZY) parcel.readParcelable(AnonymousClass1ZP.class.getClassLoader());
        }
    }

    public void A0A(String str) {
        this.A09 = new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, str, "bankName");
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        String str;
        if (this == obj) {
            return true;
        }
        if ((obj instanceof AbstractC28901Pl) && (str = ((AbstractC28901Pl) obj).A0A) != null && str.equals(this.A0A)) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.A0A;
        if (str == null) {
            return super.hashCode();
        }
        return str.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("credential-id: ");
        sb.append(this.A0A);
        sb.append(" country: ");
        sb.append(this.A07.A03);
        sb.append(" issuerName: ");
        sb.append(this.A0B);
        sb.append(" payment-mode: ");
        sb.append(this.A01);
        sb.append(" payout-mode: ");
        sb.append(this.A00);
        sb.append(" merchant-credential-id: ");
        sb.append(this.A0C);
        sb.append(" payout-verification-status: ");
        sb.append(this.A04);
        sb.append(" countrydata: ");
        sb.append(this.A08);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int length;
        parcel.writeString(this.A0A);
        parcel.writeString(this.A07.A03);
        parcel.writeParcelable(this.A09, i);
        parcel.writeString(this.A0B);
        parcel.writeString(this.A0C);
        parcel.writeInt(this.A04);
        parcel.writeLong(this.A05);
        parcel.writeLong(this.A06);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
        byte[] bArr = this.A0D;
        if (bArr == null) {
            length = 0;
        } else {
            length = bArr.length;
        }
        parcel.writeInt(length);
        byte[] bArr2 = this.A0D;
        if (bArr2 != null) {
            parcel.writeByteArray(bArr2);
        }
        byte b = 0;
        if (this.A08 != null) {
            b = 1;
        }
        parcel.writeByte(b);
        AnonymousClass1ZY r0 = this.A08;
        if (r0 != null) {
            parcel.writeParcelable(r0, 0);
        }
    }
}
