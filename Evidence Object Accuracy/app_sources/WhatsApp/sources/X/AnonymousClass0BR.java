package X;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

/* renamed from: X.0BR  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0BR extends BaseAdapter implements AbstractC12540i5, Filterable {
    public int A00;
    public Context A01;
    public Cursor A02;
    public DataSetObserver A03;
    public C020009n A04;
    public AnonymousClass0BW A05;
    public boolean A06;
    public boolean A07;

    public abstract View A03(Context context, Cursor cursor, ViewGroup viewGroup);

    public abstract void A04(View view, Context context, Cursor cursor);

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    @Deprecated
    public AnonymousClass0BR(Context context) {
        A02(context);
    }

    public AnonymousClass0BR(Context context, Cursor cursor, boolean z) {
        A02(context);
    }

    public Cursor A00(Cursor cursor) {
        Cursor cursor2 = this.A02;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            C020009n r0 = this.A04;
            if (r0 != null) {
                cursor2.unregisterContentObserver(r0);
            }
            DataSetObserver dataSetObserver = this.A03;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.A02 = cursor;
        if (cursor != null) {
            C020009n r02 = this.A04;
            if (r02 != null) {
                cursor.registerContentObserver(r02);
            }
            DataSetObserver dataSetObserver2 = this.A03;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.A00 = cursor.getColumnIndexOrThrow("_id");
            this.A07 = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.A00 = -1;
        this.A07 = false;
        notifyDataSetInvalidated();
        return cursor2;
    }

    public View A01(Context context, Cursor cursor, ViewGroup viewGroup) {
        return A03(context, cursor, viewGroup);
    }

    public void A02(Context context) {
        this.A06 = true;
        this.A02 = null;
        this.A07 = false;
        this.A01 = context;
        this.A00 = -1;
        this.A04 = new C020009n(this);
        this.A03 = new C020209p(this);
    }

    @Override // X.AbstractC12540i5
    public void A77(Cursor cursor) {
        Cursor A00 = A00(cursor);
        if (A00 != null) {
            A00.close();
        }
    }

    @Override // X.AbstractC12540i5
    public CharSequence A7k(Cursor cursor) {
        return cursor == null ? "" : cursor.toString();
    }

    @Override // X.AbstractC12540i5
    public Cursor Ab7(CharSequence charSequence) {
        return this.A02;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        Cursor cursor;
        if (!this.A07 || (cursor = this.A02) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    @Override // android.widget.BaseAdapter, android.widget.SpinnerAdapter
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (!this.A07) {
            return null;
        }
        this.A02.moveToPosition(i);
        if (view == null) {
            view = A01(this.A01, this.A02, viewGroup);
        }
        A04(view, this.A01, this.A02);
        return view;
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        AnonymousClass0BW r0 = this.A05;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0BW r02 = new AnonymousClass0BW(this);
        this.A05 = r02;
        return r02;
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        Cursor cursor;
        if (!this.A07 || (cursor = this.A02) == null) {
            return null;
        }
        cursor.moveToPosition(i);
        return this.A02;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        Cursor cursor;
        if (!this.A07 || (cursor = this.A02) == null || !cursor.moveToPosition(i)) {
            return 0;
        }
        return this.A02.getLong(this.A00);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (!this.A07) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.A02.moveToPosition(i)) {
            if (view == null) {
                view = A03(this.A01, this.A02, viewGroup);
            }
            A04(view, this.A01, this.A02);
            return view;
        } else {
            StringBuilder sb = new StringBuilder("couldn't move cursor to position ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
    }
}
