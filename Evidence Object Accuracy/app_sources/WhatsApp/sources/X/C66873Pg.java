package X;

import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.settings.SettingsChat;

/* renamed from: X.3Pg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C66873Pg implements AnonymousClass02B {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    public /* synthetic */ C66873Pg(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // X.AnonymousClass02B
    public final void ANq(Object obj) {
        RestoreFromBackupActivity restoreFromBackupActivity = this.A00;
        restoreFromBackupActivity.A06.setText(SettingsChat.A09(restoreFromBackupActivity, ((ActivityC13830kP) restoreFromBackupActivity).A01, C12980iv.A0G(obj)));
    }
}
