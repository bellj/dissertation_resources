package X;

import org.json.JSONObject;

/* renamed from: X.23Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23Z {
    public int A00;
    public long A01;
    public final AnonymousClass12Z A02;
    public final AnonymousClass12X A03;
    public final JSONObject A04;

    public AnonymousClass23Z(AnonymousClass12Z r1, AnonymousClass12X r2, JSONObject jSONObject) {
        this.A02 = r1;
        this.A03 = r2;
        this.A04 = jSONObject;
    }
}
