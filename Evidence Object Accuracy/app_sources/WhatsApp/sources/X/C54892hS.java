package X;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.whatsapp.R;

/* renamed from: X.2hS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54892hS extends AnonymousClass03U {
    public C54892hS(Activity activity, View view, AnonymousClass1BF r12, C14820m6 r13, AnonymousClass11F r14, C22050yP r15) {
        super(view);
        View view2 = this.A0H;
        C27531Hw.A06(C12960it.A0I(view2, R.id.community_subject));
        ImageView A0K = C12970iu.A0K(view2, R.id.community_icon);
        AnonymousClass51K r3 = AnonymousClass51K.A00;
        A0K.setImageDrawable(r14.A00(activity.getTheme(), activity.getResources(), r3, R.drawable.avatar_parent_large));
        AnonymousClass028.A0D(view2, R.id.community_mark).setVisibility(0);
        view.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(r12, r15, r13, activity, 4));
    }
}
