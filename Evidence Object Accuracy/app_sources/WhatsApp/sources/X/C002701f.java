package X;

import android.net.Uri;
import android.provider.MediaStore;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.01f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C002701f {
    public ExecutorC27271Gr A00;
    public final C14330lG A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final C003001i A04;
    public final C16630pM A05;
    public final AbstractC14440lR A06;

    public C002701f(C14330lG r1, AnonymousClass01d r2, C16590pI r3, C003001i r4, C16630pM r5, AbstractC14440lR r6) {
        this.A03 = r3;
        this.A06 = r6;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r5;
    }

    public int A00(File file, byte b, int i, boolean z) {
        if (!A0A(file)) {
            return 0;
        }
        int A01 = this.A04.A01(file.getAbsolutePath(), i);
        if (z && A01 < 0) {
            A06(file, b);
        }
        return A01;
    }

    public final synchronized ExecutorC27271Gr A01() {
        ExecutorC27271Gr r1;
        r1 = this.A00;
        if (r1 == null) {
            r1 = new ExecutorC27271Gr(this.A06);
            this.A00 = r1;
        }
        return r1;
    }

    public File A02() {
        return this.A01.A0A();
    }

    public File A03(String str) {
        File A04 = A04(str);
        if (!A04.exists()) {
            return null;
        }
        A08(A04, 1, true);
        return A04;
    }

    public File A04(String str) {
        File A02 = A02();
        StringBuilder sb = new StringBuilder();
        sb.append(str.replace('/', '-'));
        sb.append(".webp");
        return new File(A02, sb.toString());
    }

    public void A05(File file, byte b) {
        Uri uri;
        if (b == 1) {
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else if (b == 2) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else if (b == 3 || b == 13) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = null;
        }
        A01().execute(new RunnableBRunnable0Shape0S0300000_I0(this, uri, file, 5));
    }

    public void A06(File file, byte b) {
        C14350lI.A0M(file);
        A05(file, b);
    }

    public void A07(File file, int i, boolean z) {
        if (A0A(file)) {
            A08(file, i, z);
        }
    }

    public final void A08(File file, int i, boolean z) {
        if (!z) {
            i--;
        }
        this.A04.A02(file.getAbsolutePath(), i);
    }

    public void A09(String str) {
        File A04 = A04(str);
        if (this.A04.A01(A04.getAbsolutePath(), 1) < 0) {
            C14350lI.A0M(A04);
        }
    }

    public final boolean A0A(File file) {
        try {
            C14330lG r1 = this.A01;
            if (!r1.A0T(file) && !r1.A0S(file)) {
                if (!r1.A0U(file)) {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            Log.e("ReferenceCountedFileManager/isExternalManagedMediaFile ", e);
            return false;
        }
    }
}
