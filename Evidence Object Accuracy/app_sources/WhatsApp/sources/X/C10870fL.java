package X;

/* renamed from: X.0fL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10870fL extends Throwable {
    public C10870fL() {
        super("Failure occurred while trying to finish a future.");
    }

    @Override // java.lang.Throwable
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
