package X;

/* renamed from: X.4CG  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CG extends Exception {
    public final C100614mC format;

    public AnonymousClass4CG(C100614mC r1, String str) {
        super(str);
        this.format = r1;
    }

    public AnonymousClass4CG(C100614mC r1, Throwable th) {
        super(th);
        this.format = r1;
    }
}
