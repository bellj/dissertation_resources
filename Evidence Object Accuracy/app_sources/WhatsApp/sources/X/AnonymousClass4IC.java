package X;

import android.content.Context;
import android.os.PowerManager;

/* renamed from: X.4IC  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4IC {
    public final PowerManager A00;

    public AnonymousClass4IC(Context context) {
        this.A00 = (PowerManager) context.getApplicationContext().getSystemService("power");
    }
}
