package X;

import android.accounts.Account;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3H6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3H6 {
    public Account A00;
    public String A01;
    public String A02;
    public String A03;
    public Map A04 = C12970iu.A11();
    public Set A05 = C12970iu.A12();
    public boolean A06;
    public boolean A07;
    public boolean A08;

    public AnonymousClass3H6() {
    }

    public AnonymousClass3H6(GoogleSignInOptions googleSignInOptions) {
        this.A05 = new HashSet(googleSignInOptions.A08);
        this.A06 = googleSignInOptions.A09;
        this.A07 = googleSignInOptions.A0A;
        this.A08 = googleSignInOptions.A06;
        this.A01 = googleSignInOptions.A01;
        this.A00 = googleSignInOptions.A00;
        this.A02 = googleSignInOptions.A02;
        ArrayList arrayList = googleSignInOptions.A04;
        HashMap A11 = C12970iu.A11();
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C78283of r1 = (C78283of) it.next();
                C12960it.A1J(r1, A11, r1.A00);
            }
        }
        this.A04 = A11;
        this.A03 = googleSignInOptions.A03;
    }

    public GoogleSignInOptions A00() {
        Set set = this.A05;
        if (set.contains(GoogleSignInOptions.A0H)) {
            Scope scope = GoogleSignInOptions.A0G;
            if (set.contains(scope)) {
                set.remove(scope);
            }
        }
        boolean z = this.A08;
        if (z && (this.A00 == null || !set.isEmpty())) {
            set.add(GoogleSignInOptions.A0F);
        }
        ArrayList A0x = C12980iv.A0x(set);
        Account account = this.A00;
        boolean z2 = this.A06;
        boolean z3 = this.A07;
        return new GoogleSignInOptions(account, this.A01, this.A02, this.A03, A0x, this.A04, 3, z, z2, z3);
    }
}
