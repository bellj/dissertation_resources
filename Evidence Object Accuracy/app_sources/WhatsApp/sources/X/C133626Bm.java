package X;

import android.graphics.BitmapFactory;
import java.util.Map;

/* renamed from: X.6Bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133626Bm implements AbstractC16960q2 {
    public final C16590pI A00;

    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124655pt.class;
    }

    public C133626Bm(C16590pI r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r7, Object obj, Map map) {
        AnonymousClass1ZR r0;
        AnonymousClass1ZY r8 = (AnonymousClass1ZY) obj;
        EnumC124655pt r72 = (EnumC124655pt) r7;
        C16700pc.A0E(r8, 0);
        C16700pc.A0E(r72, 1);
        if (!(r8 instanceof C119755f3)) {
            return null;
        }
        switch (C117315Zl.A01(r72, C125235qv.A00)) {
            case 1:
                r0 = ((C119755f3) r8).A03;
                break;
            case 2:
                return ((C119755f3) r8).A0A;
            case 3:
                String str = ((C119755f3) r8).A0B;
                if (str == null) {
                    return "UNKNOWN";
                }
                switch (str.hashCode()) {
                    case -1704036199:
                        if (str.equals("SAVINGS")) {
                            return "SAVINGS";
                        }
                        return "UNKNOWN";
                    case -240997565:
                        if (str.equals("OD_SECURED")) {
                            return "OD_SECURED";
                        }
                        return "UNKNOWN";
                    case 358786314:
                        if (str.equals("OD_UNSECURED")) {
                            return "OD_UNSECURED";
                        }
                        return "UNKNOWN";
                    case 1844922713:
                        if (!str.equals("CURRENT")) {
                            return "UNKNOWN";
                        }
                        return "CURRENT";
                    default:
                        return "UNKNOWN";
                }
            case 4:
                return ((AbstractC30851Zb) r8).A03;
            case 5:
                r0 = ((C119755f3) r8).A05;
                break;
            case 6:
                r0 = ((C119755f3) r8).A09;
                break;
            case 7:
                return ((C119755f3) r8).A0F;
            case 8:
                return C37501mV.A07(BitmapFactory.decodeResource(C16590pI.A00(this.A00), C125125qj.A00(((C119755f3) r8).A0A).A00));
            case 9:
                r0 = ((C119755f3) r8).A06;
                break;
            default:
                throw new C113285Gx();
        }
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }
}
