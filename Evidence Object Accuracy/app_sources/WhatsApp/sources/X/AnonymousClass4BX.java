package X;

/* renamed from: X.4BX  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BX {
    A06(0),
    A03(1),
    A05(2),
    A01(3),
    A02(4),
    A04(5);
    
    public final int value;

    AnonymousClass4BX(int i) {
        this.value = i;
    }

    public static AnonymousClass4BX A00(int i) {
        if (i == 0) {
            return A06;
        }
        if (i == 1) {
            return A03;
        }
        if (i == 2) {
            return A05;
        }
        if (i == 3) {
            return A01;
        }
        if (i == 4) {
            return A02;
        }
        if (i != 5) {
            return null;
        }
        return A04;
    }
}
