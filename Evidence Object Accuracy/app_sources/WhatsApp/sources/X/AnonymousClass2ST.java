package X;

import java.util.Arrays;
import javax.security.auth.Destroyable;

/* renamed from: X.2ST  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ST implements Destroyable {
    public boolean A00;
    public final byte[] A01;

    public AnonymousClass2ST(byte[] bArr) {
        int length = bArr.length;
        if (length == 32) {
            this.A01 = bArr;
            return;
        }
        StringBuilder sb = new StringBuilder("Wrong length: ");
        sb.append(length);
        throw new IllegalArgumentException(sb.toString());
    }

    @Override // javax.security.auth.Destroyable
    public void destroy() {
        if (!this.A00) {
            Arrays.fill(this.A01, (byte) 0);
            this.A00 = true;
        }
    }

    @Override // javax.security.auth.Destroyable
    public boolean isDestroyed() {
        return this.A00;
    }
}
