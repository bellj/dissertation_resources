package X;

/* renamed from: X.1f4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33781f4 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;
    public Long A0R;
    public Long A0S;
    public Long A0T;
    public Long A0U;
    public Long A0V;
    public Long A0W;

    public C33781f4() {
        super(1994, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(16, this.A00);
        r3.Abe(36, this.A0J);
        r3.Abe(26, this.A0D);
        r3.Abe(11, this.A0K);
        r3.Abe(12, this.A0L);
        r3.Abe(1, this.A0M);
        r3.Abe(15, this.A01);
        r3.Abe(21, this.A0N);
        r3.Abe(17, this.A0E);
        r3.Abe(33, this.A02);
        r3.Abe(27, this.A03);
        r3.Abe(9, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(24, this.A06);
        r3.Abe(29, this.A07);
        r3.Abe(18, this.A0O);
        r3.Abe(3, this.A0F);
        r3.Abe(30, this.A08);
        r3.Abe(31, this.A09);
        r3.Abe(4, this.A0G);
        r3.Abe(14, this.A0A);
        r3.Abe(37, this.A0P);
        r3.Abe(34, this.A0Q);
        r3.Abe(28, this.A0B);
        r3.Abe(39, this.A0R);
        r3.Abe(13, this.A0S);
        r3.Abe(10, this.A0T);
        r3.Abe(2, this.A0H);
        r3.Abe(40, this.A0U);
        r3.Abe(23, this.A0V);
        r3.Abe(25, this.A0C);
        r3.Abe(19, this.A0W);
        r3.Abe(38, this.A0I);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        StringBuilder sb = new StringBuilder("WamAndroidMessageSendPerf {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "appRestart", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceCount", this.A0J);
        Integer num = this.A0D;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceSizeBucket", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "durationAbs", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "durationRelative", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "durationT", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "fetchPrekeys", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "fetchPrekeysPercentage", this.A0N);
        Integer num2 = this.A0E;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupSizeBucket", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isDirectedMessage", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isE2eBackfill", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isMessageFanout", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isMessageForward", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isRevokeMessage", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isViewOnce", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "jobsInQueue", this.A0O);
        Integer num3 = this.A0F;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaType", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsFirstUserMessage", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsInvisible", this.A09);
        Integer num4 = this.A0G;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "networkWasDisconnected", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "participantCount", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "phoneCores", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "prekeysEligibleForPrallelProcessing", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "receiverDeviceCount", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sendCount", this.A0S);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sendRetryCount", this.A0T);
        Integer num5 = this.A0H;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sendStage", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderDeviceCount", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderKeyDistributionCountPercentage", this.A0V);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sessionsMissingWhenComposing", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "threadsInExecution", this.A0W);
        Integer num6 = this.A0I;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "typeOfGroup", obj6);
        sb.append("}");
        return sb.toString();
    }
}
