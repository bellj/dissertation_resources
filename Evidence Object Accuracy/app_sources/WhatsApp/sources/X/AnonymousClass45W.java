package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.45W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45W extends AnonymousClass2PA {
    public final AnonymousClass4S3 A00;

    public AnonymousClass45W(Jid jid, AnonymousClass4S3 r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
