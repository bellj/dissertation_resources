package X;

import java.util.Iterator;

/* renamed from: X.28g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C469328g extends AnonymousClass1I5 {
    public final /* synthetic */ Iterator val$iterator;

    public C469328g(Iterator it) {
        this.val$iterator = it;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.val$iterator.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        return this.val$iterator.next();
    }
}
