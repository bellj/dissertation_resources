package X;

import android.os.AsyncTask;
import android.util.Log;

/* renamed from: X.0AI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AI extends AsyncTask {
    public final /* synthetic */ AnonymousClass0PI A00;
    public final /* synthetic */ AbstractC11810gu A01;

    public AnonymousClass0AI(AnonymousClass0PI r1, AbstractC11810gu r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        try {
            return this.A00.A00();
        } catch (Exception e) {
            Log.e("Palette", "Exception thrown during async generate", e);
            return null;
        }
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.A01.AQr((C06030Rx) obj);
    }
}
