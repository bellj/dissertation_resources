package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1VH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VH {
    public long A00;
    public Jid A01;
    public Jid A02;
    public UserJid A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public final Map A09 = new HashMap();

    public AnonymousClass1OT A00() {
        ArrayList arrayList;
        Map map = this.A09;
        if (map.isEmpty()) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(map.values());
        }
        return new AnonymousClass1OT(this.A01, this.A02, this.A03, this.A05, this.A07, this.A08, this.A04, this.A06, arrayList, this.A00);
    }

    public void A01(String str) {
        this.A09.put("error", new AnonymousClass1W9("error", str));
    }
}
