package X;

import java.util.HashMap;

/* renamed from: X.0qN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17170qN {
    public static HashMap A01;
    public final C16630pM A00;

    static {
        HashMap hashMap = new HashMap();
        A01 = hashMap;
        hashMap.put("smax_payment_br.xml", "payments/smax_payment_br.xml");
        A01.put("smax_payment_in.xml", "payments/smax_payment_in.xml");
        A01.put("smax_payment_compliance.xml", "payments/smax_payment_compliance.xml");
        A01.put("smax_payment_br_dogfooding.xml", "payments/smax_payment_br_dogfooding.xml");
        A01.put("smax_payment_in_dogfooding.xml", "payments/smax_payment_in_dogfooding.xml");
        A01.put("smax_payment_us_dogfooding.xml", "payments/smax_payment_us_dogfooding.xml");
        A01.put("smax_payment_id.xml", "");
        A01.put("smax_payment_mx.xml", "");
        A01.put("smax_payment_id_dogfooding.xml", "");
        A01.put("smax_payment_mx_dogfooding.xml", "");
    }

    public C17170qN(C16630pM r1) {
        this.A00 = r1;
    }
}
