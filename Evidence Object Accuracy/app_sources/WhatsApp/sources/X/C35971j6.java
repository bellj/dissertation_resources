package X;

import android.graphics.Point;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.1j6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35971j6 {
    public AnonymousClass0U9 A00;
    public AnonymousClass3F0 A01;

    public C35971j6(AnonymousClass0U9 r4) {
        this.A00 = r4;
        r4.A04(new AnonymousClass03T(0.0d, 0.0d));
    }

    public C35971j6(AnonymousClass3F0 r4) {
        this.A01 = r4;
        r4.A00(new LatLng(0.0d, 0.0d));
    }

    public Point A00(LatLng latLng) {
        AnonymousClass3F0 r0 = this.A01;
        if (r0 != null) {
            return r0.A00(latLng);
        }
        return this.A00.A04(new AnonymousClass03T(latLng.A00, latLng.A01));
    }
}
