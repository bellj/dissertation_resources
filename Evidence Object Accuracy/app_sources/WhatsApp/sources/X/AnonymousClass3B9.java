package X;

import android.os.Bundle;

/* renamed from: X.3B9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3B9 {
    public final Bundle A00;

    public /* synthetic */ AnonymousClass3B9(Bundle bundle) {
        C65273Iw.A03("challenge", bundle);
        C65273Iw.A01(bundle, Bundle.class, "auxArguments");
        C65273Iw.A01(bundle, Bundle.class, "additionalKeyMaterial");
        C65273Iw.A01(bundle, Boolean.class, "refreshVerifier");
        C65273Iw.A01(bundle, Boolean.class, "useDebugKey");
        this.A00 = bundle;
    }
}
