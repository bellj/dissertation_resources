package X;

import android.view.View;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;

/* renamed from: X.6Di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134106Di implements AnonymousClass5Wu {
    public View.OnClickListener A00;
    public View A01;
    public TextView A02;
    public TextView A03;
    public ShimmerFrameLayout A04;
    public String A05;

    public C134106Di(String str) {
        this.A05 = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* renamed from: A00 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6Q(X.AnonymousClass4OZ r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x0013
            int r2 = r4.A00
            r0 = -2
            r1 = 0
            if (r2 == r0) goto L_0x001f
            r0 = -1
            if (r2 == r0) goto L_0x001f
            if (r2 == 0) goto L_0x0045
            r0 = 1
            if (r2 == r0) goto L_0x0014
            r0 = 2
            if (r2 == r0) goto L_0x001f
        L_0x0013:
            return
        L_0x0014:
            android.view.View r0 = r3.A01
            r0.setVisibility(r1)
            com.facebook.shimmer.ShimmerFrameLayout r0 = r3.A04
            r0.A01()
            goto L_0x0029
        L_0x001f:
            android.view.View r0 = r3.A01
            r0.setVisibility(r1)
            com.facebook.shimmer.ShimmerFrameLayout r0 = r3.A04
            r0.A00()
        L_0x0029:
            java.lang.Object r2 = r4.A01
            if (r2 == 0) goto L_0x0013
            X.5to r2 = (X.C127015to) r2
            android.widget.TextView r1 = r3.A02
            java.lang.CharSequence r0 = r2.A00
            r1.setText(r0)
            android.widget.TextView r1 = r3.A03
            boolean r0 = r2.A01
            r1.setEnabled(r0)
            android.widget.TextView r1 = r3.A03
            android.view.View$OnClickListener r0 = r3.A00
            r1.setOnClickListener(r0)
            return
        L_0x0045:
            android.view.View r1 = r3.A01
            r0 = 8
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C134106Di.A6Q(X.4OZ):void");
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_withdraw_amount_input_summary;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A01 = view;
        this.A02 = C12960it.A0I(view, R.id.withdraw_amount_conversion);
        this.A04 = (ShimmerFrameLayout) AnonymousClass028.A0D(view, R.id.withdraw_amount_conversion_shimmer);
        TextView A0I = C12960it.A0I(view, R.id.withdraw_full_balance);
        this.A03 = A0I;
        if (this.A05.equals("deposit")) {
            A0I.setVisibility(8);
        }
    }
}
