package X;

import java.io.IOException;
import java.security.Principal;

/* renamed from: X.5Np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114925Np extends C114735Mw implements Principal {
    @Override // java.security.Principal
    public String getName() {
        return toString();
    }

    public C114925Np(AnonymousClass5N2 r2) {
        super(r2.A01);
    }

    @Override // X.AnonymousClass1TM
    public byte[] A01() {
        try {
            return A02("DER");
        } catch (IOException e) {
            throw C12990iw.A0m(e.toString());
        }
    }
}
