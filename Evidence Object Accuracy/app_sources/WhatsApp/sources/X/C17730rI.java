package X;

import java.util.Map;

/* renamed from: X.0rI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17730rI implements AnonymousClass6MD {
    public final C14900mE A00;
    public final AnonymousClass018 A01;
    public final C20440vl A02;
    public final AbstractC14440lR A03;
    public final C18840t8 A04;

    public C17730rI(C14900mE r2, AnonymousClass018 r3, C20440vl r4, AbstractC14440lR r5, C18840t8 r6) {
        C16700pc.A0E(r6, 1);
        C16700pc.A0E(r2, 2);
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r3, 4);
        this.A04 = r6;
        this.A00 = r2;
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
    }

    public static /* synthetic */ void A00(C17730rI r5, C65963Lt r6, C129625y2 r7, String str, Map map) {
        C16700pc.A0E(r5, 0);
        C16700pc.A0E(str, 1);
        C16700pc.A0E(r7, 3);
        C20440vl r3 = r5.A02;
        Object obj = map.get("static_url");
        if (obj != null) {
            String str2 = (String) obj;
            C68733Wn r1 = new C68733Wn(r5, r6, r7, str);
            C16700pc.A0E(str2, 1);
            r3.A00 = str;
            r3.A03(r1, null, null, str2);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }

    @Override // X.AnonymousClass6MD
    public void AZP(C65963Lt r5, C129625y2 r6, String str, String str2) {
        C16700pc.A0E(str, 0);
        if (r5 != null) {
            C18840t8 r3 = this.A04;
            String str3 = r5.A01;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(":8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666:");
            sb.append(AnonymousClass018.A00(this.A01.A00));
            String str4 = (String) r3.A01(str3, sb.toString());
            if (str4 != null) {
                r6.A01(str4);
                return;
            }
        }
        if (str2 != null) {
            this.A00.A0I(new Runnable(r5, r6, str, C65053Hy.A01(str2)) { // from class: X.3lN
                public final /* synthetic */ C65963Lt A01;
                public final /* synthetic */ C129625y2 A02;
                public final /* synthetic */ String A03;
                public final /* synthetic */ Map A04;

                {
                    this.A03 = r4;
                    this.A04 = r5;
                    this.A02 = r3;
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C17730rI r4 = C17730rI.this;
                    String str5 = this.A03;
                    Map map = this.A04;
                    C17730rI.A00(r4, this.A01, this.A02, str5, map);
                }
            });
            return;
        }
        throw new IllegalArgumentException("Required value was null.");
    }
}
