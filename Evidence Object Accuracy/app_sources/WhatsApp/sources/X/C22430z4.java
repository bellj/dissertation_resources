package X;

import android.content.ContentValues;
import android.database.SQLException;
import com.facebook.redex.RunnableBRunnable0Shape0S1100100_I0;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.0z4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C22430z4 {
    public final C18460sU A00;
    public final AnonymousClass11E A01;

    public C22430z4(C18460sU r2, AnonymousClass11E r3) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r3, 2);
        this.A00 = r2;
        this.A01 = r3;
    }

    public void A00(AnonymousClass1MU r13, String str) {
        C16700pc.A0E(r13, 0);
        C16700pc.A0E(str, 1);
        long A01 = this.A00.A01(r13);
        if (A01 >= 0) {
            AnonymousClass11E r7 = this.A01;
            Map map = r7.A01;
            Long valueOf = Long.valueOf(A01);
            if (!str.equals(map.get(valueOf))) {
                try {
                    C16310on A02 = r7.A00.A02();
                    AnonymousClass1Lx A00 = A02.A00();
                    try {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("lid_row_id", valueOf);
                        contentValues.put("display_name", str);
                        A02.A03.A06(contentValues, "lid_display_name", 5);
                        A00.A00();
                        A02.A03(new RunnableBRunnable0Shape0S1100100_I0(r7, str, 0, A01));
                        A00.close();
                        A02.close();
                    } catch (Throwable th) {
                        try {
                            A00.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (SQLException e) {
                    Log.e("LidDisplayNameStore/upsertDisplayNameForLid", e);
                }
            }
        }
    }
}
