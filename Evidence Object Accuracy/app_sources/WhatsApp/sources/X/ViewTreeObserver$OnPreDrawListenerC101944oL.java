package X;

import android.view.ViewTreeObserver;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;

/* renamed from: X.4oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101944oL implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ SettingsTwoFactorAuthActivity A00;

    public ViewTreeObserver$OnPreDrawListenerC101944oL(SettingsTwoFactorAuthActivity settingsTwoFactorAuthActivity) {
        this.A00 = settingsTwoFactorAuthActivity;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        SettingsTwoFactorAuthActivity settingsTwoFactorAuthActivity = this.A00;
        C12980iv.A1G(settingsTwoFactorAuthActivity.A05, this);
        settingsTwoFactorAuthActivity.A2e();
        return false;
    }
}
