package X;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import androidx.work.impl.foreground.SystemForegroundService;
import java.util.UUID;

/* renamed from: X.0e1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10090e1 implements Runnable {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C05360Pg A01;
    public final /* synthetic */ C07570Zh A02;
    public final /* synthetic */ AnonymousClass040 A03;
    public final /* synthetic */ UUID A04;

    public RunnableC10090e1(Context context, C05360Pg r2, C07570Zh r3, AnonymousClass040 r4, UUID uuid) {
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = uuid;
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            AnonymousClass040 r4 = this.A03;
            if (!r4.isCancelled()) {
                String obj = this.A04.toString();
                C07570Zh r1 = this.A02;
                EnumC03840Ji AGv = r1.A01.AGv(obj);
                if (AGv == null || AGv.A00()) {
                    throw new IllegalStateException("Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                }
                AbstractC11450gJ r3 = r1.A00;
                C05360Pg r6 = this.A01;
                C07650Zp r32 = (C07650Zp) r3;
                synchronized (r32.A09) {
                    C06390Tk.A00().A04(C07650Zp.A0B, String.format("Moving WorkSpec (%s) to the foreground", obj), new Throwable[0]);
                    RunnableC10250eH r2 = (RunnableC10250eH) r32.A06.remove(obj);
                    if (r2 != null) {
                        if (r32.A01 == null) {
                            PowerManager.WakeLock A00 = AnonymousClass0RM.A00(r32.A00, "ProcessorForegroundLck");
                            r32.A01 = A00;
                            A00.acquire();
                        }
                        r32.A07.put(obj, r2);
                        Context context = r32.A00;
                        Intent intent = new Intent(context, SystemForegroundService.class);
                        intent.setAction("ACTION_START_FOREGROUND");
                        intent.putExtra("KEY_WORKSPEC_ID", obj);
                        intent.putExtra("KEY_NOTIFICATION_ID", r6.A01);
                        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", r6.A00);
                        intent.putExtra("KEY_NOTIFICATION", r6.A02);
                        intent.putExtra("KEY_WORKSPEC_ID", obj);
                        AnonymousClass00T.A0F(context, intent);
                    }
                }
                Context context2 = this.A00;
                Intent intent2 = new Intent(context2, SystemForegroundService.class);
                intent2.setAction("ACTION_NOTIFY");
                intent2.putExtra("KEY_NOTIFICATION_ID", r6.A01);
                intent2.putExtra("KEY_FOREGROUND_SERVICE_TYPE", r6.A00);
                intent2.putExtra("KEY_NOTIFICATION", r6.A02);
                intent2.putExtra("KEY_WORKSPEC_ID", obj);
                context2.startService(intent2);
            }
            r4.A09(null);
        } catch (Throwable th) {
            this.A03.A0A(th);
        }
    }
}
