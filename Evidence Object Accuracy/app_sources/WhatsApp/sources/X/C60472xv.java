package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60472xv extends AnonymousClass1OY {
    public AnonymousClass11L A00;
    public boolean A01;
    public final TextView A02;

    public C60472xv(Context context, AbstractC13890kV r3, AnonymousClass1XK r4) {
        super(context, r3, r4);
        A0Z();
        setClickable(false);
        setLongClickable(false);
        TextView A0J = C12960it.A0J(this, R.id.info);
        this.A02 = A0J;
        AnonymousClass1OY.A0D(context, this, A0J);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A00 = (AnonymousClass11L) A08.ALG.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public final void A1M() {
        AbstractC14640lm r2;
        AnonymousClass1XK r4 = (AnonymousClass1XK) ((AbstractC28551Oa) this).A0O;
        AnonymousClass11L r3 = this.A00;
        AnonymousClass1IS r1 = r4.A0z;
        if (r1.A02) {
            C15570nT r0 = ((AnonymousClass1OY) this).A0L;
            r0.A08();
            r2 = r0.A05;
        } else {
            r2 = r1.A00;
        }
        String A04 = r3.A04(r2, r4.A00, true);
        Drawable A06 = AnonymousClass1OY.A06(this);
        TextView textView = this.A02;
        textView.setText(C52252aV.A01(textView.getPaint(), A06, A04));
        C12960it.A0y(textView, this, 35);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public AnonymousClass1XK getFMessage() {
        return (AnonymousClass1XK) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XK);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
