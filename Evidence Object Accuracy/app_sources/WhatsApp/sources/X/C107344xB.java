package X;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;

/* renamed from: X.4xB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107344xB implements AnonymousClass5X8 {
    @Override // X.AnonymousClass5X8
    public boolean AJO(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2) {
        return false;
    }

    @Override // X.AnonymousClass5X8
    public boolean AbO() {
        return false;
    }

    @Override // X.AnonymousClass5X8
    public int ABS() {
        return MediaCodecList.getCodecCount();
    }

    @Override // X.AnonymousClass5X8
    public MediaCodecInfo ABT(int i) {
        return MediaCodecList.getCodecInfoAt(i);
    }

    @Override // X.AnonymousClass5X8
    public boolean AJP(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2) {
        return "secure-playback".equals(str) && "video/avc".equals(str2);
    }
}
