package X;

import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/* renamed from: X.0Ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06540Ua {
    public static HashSet A07;
    public float A00 = 96.0f;
    public Canvas A01;
    public AnonymousClass0Q5 A02;
    public AnonymousClass0S1 A03;
    public Stack A04;
    public Stack A05;
    public Stack A06;

    public C06540Ua(Canvas canvas) {
        this.A01 = canvas;
    }

    public static int A00(int i, float f) {
        int i2 = 255;
        int round = Math.round(((float) ((i >> 24) & 255)) * f);
        if (round < 0) {
            i2 = 0;
        } else if (round <= 255) {
            i2 = round;
        }
        return (i & 16777215) | (i2 << 24);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0049 A[PHI: r3 
      PHI: (r3v2 float) = (r3v0 float), (r3v1 float) binds: [B:14:0x0046, B:18:0x0058] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.graphics.Matrix A01(X.AnonymousClass0SV r12, X.AnonymousClass0SG r13, X.AnonymousClass0SG r14) {
        /*
            android.graphics.Matrix r5 = new android.graphics.Matrix
            r5.<init>()
            X.0Jc r11 = r12.A00
            if (r11 == 0) goto L_0x0030
            float r9 = r13.A03
            float r8 = r14.A03
            float r10 = r9 / r8
            float r7 = r13.A00
            float r6 = r14.A00
            float r2 = r7 / r6
            float r0 = r14.A01
            float r4 = -r0
            float r0 = r14.A02
            float r3 = -r0
            X.0SV r0 = X.AnonymousClass0SV.A03
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0031
            float r1 = r13.A01
            float r0 = r13.A02
            r5.preTranslate(r1, r0)
            r5.preScale(r10, r2)
        L_0x002d:
            r5.preTranslate(r4, r3)
        L_0x0030:
            return r5
        L_0x0031:
            X.0JA r1 = r12.A01
            X.0JA r0 = X.AnonymousClass0JA.slice
            if (r1 != r0) goto L_0x0060
            float r2 = java.lang.Math.max(r10, r2)
        L_0x003b:
            float r9 = r9 / r2
            float r7 = r7 / r2
            int r1 = r11.ordinal()
            r0 = 1073741824(0x40000000, float:2.0)
            switch(r1) {
                case 2: goto L_0x005c;
                case 3: goto L_0x005a;
                case 4: goto L_0x0046;
                case 5: goto L_0x005c;
                case 6: goto L_0x005a;
                case 7: goto L_0x0046;
                case 8: goto L_0x005c;
                case 9: goto L_0x005a;
                default: goto L_0x0046;
            }
        L_0x0046:
            switch(r1) {
                case 4: goto L_0x0056;
                case 5: goto L_0x0056;
                case 6: goto L_0x0056;
                case 7: goto L_0x0054;
                case 8: goto L_0x0054;
                case 9: goto L_0x0054;
                default: goto L_0x0049;
            }
        L_0x0049:
            float r1 = r13.A01
            float r0 = r13.A02
            r5.preTranslate(r1, r0)
            r5.preScale(r2, r2)
            goto L_0x002d
        L_0x0054:
            float r6 = r6 - r7
            goto L_0x0058
        L_0x0056:
            float r6 = r6 - r7
            float r6 = r6 / r0
        L_0x0058:
            float r3 = r3 - r6
            goto L_0x0049
        L_0x005a:
            float r8 = r8 - r9
            goto L_0x005e
        L_0x005c:
            float r8 = r8 - r9
            float r8 = r8 / r0
        L_0x005e:
            float r4 = r4 - r8
            goto L_0x0046
        L_0x0060:
            float r2 = java.lang.Math.min(r10, r2)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A01(X.0SV, X.0SG, X.0SG):android.graphics.Matrix");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.graphics.Typeface A02(X.AnonymousClass0JK r3, java.lang.Integer r4, java.lang.String r5) {
        /*
            X.0JK r0 = X.AnonymousClass0JK.Italic
            r2 = 0
            if (r3 != r0) goto L_0x0006
            r2 = 1
        L_0x0006:
            int r1 = r4.intValue()
            r0 = 500(0x1f4, float:7.0E-43)
            if (r1 <= r0) goto L_0x0046
            r1 = 1
            if (r2 == 0) goto L_0x0012
            r1 = 3
        L_0x0012:
            int r0 = r5.hashCode()
            switch(r0) {
                case -1536685117: goto L_0x0037;
                case -1431958525: goto L_0x002c;
                case -1081737434: goto L_0x0029;
                case 109326717: goto L_0x001e;
                case 1126973893: goto L_0x001b;
                default: goto L_0x0019;
            }
        L_0x0019:
            r0 = 0
            return r0
        L_0x001b:
            java.lang.String r0 = "cursive"
            goto L_0x0039
        L_0x001e:
            java.lang.String r0 = "serif"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0019
            android.graphics.Typeface r0 = android.graphics.Typeface.SERIF
            goto L_0x0041
        L_0x0029:
            java.lang.String r0 = "fantasy"
            goto L_0x0039
        L_0x002c:
            java.lang.String r0 = "monospace"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0019
            android.graphics.Typeface r0 = android.graphics.Typeface.MONOSPACE
            goto L_0x0041
        L_0x0037:
            java.lang.String r0 = "sans-serif"
        L_0x0039:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0019
            android.graphics.Typeface r0 = android.graphics.Typeface.SANS_SERIF
        L_0x0041:
            android.graphics.Typeface r0 = android.graphics.Typeface.create(r0, r1)
            return r0
        L_0x0046:
            r1 = 0
            if (r2 == 0) goto L_0x0012
            r1 = 2
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A02(X.0JK, java.lang.Integer, java.lang.String):android.graphics.Typeface");
    }

    public static final AnonymousClass0SG A03(Path path) {
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        return new AnonymousClass0SG(rectF.left, rectF.top, rectF.width(), rectF.height());
    }

    public static void A04(AbstractC12670iJ r29, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
        double d;
        if (!(f == f6 && f2 == f7)) {
            if (f3 == 0.0f || f4 == 0.0f) {
                r29.AKS(f6, f7);
                return;
            }
            float abs = Math.abs(f3);
            float abs2 = Math.abs(f4);
            double radians = (double) ((float) Math.toRadians(((double) f5) % 360.0d));
            float cos = (float) Math.cos(radians);
            float sin = (float) Math.sin(radians);
            float f8 = (f - f6) / 2.0f;
            float f9 = (f2 - f7) / 2.0f;
            float f10 = (cos * f8) + (sin * f9);
            float f11 = ((-sin) * f8) + (f9 * cos);
            float f12 = abs * abs;
            float f13 = abs2 * abs2;
            float f14 = f10 * f10;
            float f15 = f11 * f11;
            float f16 = (f14 / f12) + (f15 / f13);
            if (f16 > 1.0f) {
                float sqrt = (float) Math.sqrt((double) f16);
                abs *= sqrt;
                abs2 *= sqrt;
                f12 = abs * abs;
                f13 = abs2 * abs2;
            }
            float f17 = 1.0f;
            if (z == z2) {
                f17 = -1.0f;
            }
            float f18 = f12 * f13;
            float f19 = f12 * f15;
            float f20 = f13 * f14;
            float f21 = ((f18 - f19) - f20) / (f19 + f20);
            if (f21 < 0.0f) {
                f21 = 0.0f;
            }
            float sqrt2 = (float) (((double) f17) * Math.sqrt((double) f21));
            float f22 = ((abs * f11) / abs2) * sqrt2;
            float f23 = sqrt2 * (-((abs2 * f10) / abs));
            float f24 = ((f + f6) / 2.0f) + ((cos * f22) - (sin * f23));
            float f25 = ((f2 + f7) / 2.0f) + (sin * f22) + (cos * f23);
            float f26 = (f10 - f22) / abs;
            float f27 = (f11 - f23) / abs2;
            float f28 = ((-f10) - f22) / abs;
            float f29 = ((-f11) - f23) / abs2;
            float f30 = (f26 * f26) + (f27 * f27);
            float sqrt3 = (float) Math.sqrt((double) f30);
            float f31 = 1.0f;
            if (f27 < 0.0f) {
                f31 = -1.0f;
            }
            float degrees = (float) Math.toDegrees(((double) f31) * Math.acos((double) (f26 / sqrt3)));
            float sqrt4 = (float) Math.sqrt((double) (f30 * ((f28 * f28) + (f29 * f29))));
            float f32 = (f26 * f28) + (f27 * f29);
            float f33 = 1.0f;
            if ((f26 * f29) - (f27 * f28) < 0.0f) {
                f33 = -1.0f;
            }
            double degrees2 = Math.toDegrees(((double) f33) * Math.acos((double) (f32 / sqrt4)));
            if (z2 || degrees2 <= 0.0d) {
                d = 360.0d;
                if (z2 && degrees2 < 0.0d) {
                    degrees2 += 360.0d;
                }
            } else {
                d = 360.0d;
                degrees2 -= 360.0d;
            }
            double d2 = degrees2 % d;
            double d3 = (double) (degrees % 360.0f);
            int ceil = (int) Math.ceil(Math.abs(d2) / 90.0d);
            double radians2 = Math.toRadians(d3);
            float radians3 = (float) (Math.toRadians(d2) / ((double) ceil));
            double d4 = (double) radians3;
            double d5 = d4 / 2.0d;
            double sin2 = (Math.sin(d5) * 1.3333333333333333d) / (Math.cos(d5) + 1.0d);
            int i = ceil * 6;
            float[] fArr = new float[i];
            int i2 = 0;
            for (int i3 = 0; i3 < ceil; i3++) {
                double d6 = ((double) (((float) i3) * radians3)) + radians2;
                double cos2 = Math.cos(d6);
                double sin3 = Math.sin(d6);
                int i4 = i2 + 1;
                fArr[i2] = (float) (cos2 - (sin2 * sin3));
                int i5 = i4 + 1;
                fArr[i4] = (float) (sin3 + (cos2 * sin2));
                double d7 = d6 + d4;
                double cos3 = Math.cos(d7);
                double sin4 = Math.sin(d7);
                int i6 = i5 + 1;
                fArr[i5] = (float) ((sin2 * sin4) + cos3);
                int i7 = i6 + 1;
                fArr[i6] = (float) (sin4 - (sin2 * cos3));
                int i8 = i7 + 1;
                fArr[i7] = (float) cos3;
                i2 = i8 + 1;
                fArr[i8] = (float) sin4;
            }
            Matrix matrix = new Matrix();
            matrix.postScale(abs, abs2);
            matrix.postRotate(f5);
            matrix.postTranslate(f24, f25);
            matrix.mapPoints(fArr);
            fArr[i - 2] = f6;
            fArr[i - 1] = f7;
            for (int i9 = 0; i9 < i; i9 += 6) {
                r29.A8d(fArr[i9], fArr[i9 + 1], fArr[i9 + 2], fArr[i9 + 3], fArr[i9 + 4], fArr[i9 + 5]);
            }
        }
    }

    public static final void A05(AbstractC08890c1 r3, AnonymousClass0S1 r4, boolean z) {
        Float f;
        C03360Hm r32;
        Paint paint;
        C08900c2 r2 = r4.A04;
        if (z) {
            f = r2.A0O;
        } else {
            f = r2.A0T;
        }
        float floatValue = f.floatValue();
        if (r3 instanceof C03360Hm) {
            r32 = (C03360Hm) r3;
        } else if (r3 instanceof C03350Hl) {
            r32 = r2.A02;
        } else {
            return;
        }
        int A00 = A00(r32.A00, floatValue);
        if (z) {
            paint = r4.A00;
        } else {
            paint = r4.A01;
        }
        paint.setColor(A00);
    }

    public static void A06(String str, Object... objArr) {
        Log.e("SVGAndroidRenderer", String.format(str, objArr));
    }

    public static final boolean A07(C08900c2 r3, long j) {
        return (j & r3.A00) != 0;
    }

    public final float A08(AbstractC03250Hb r2) {
        AnonymousClass0I3 r0 = new AnonymousClass0I3(this);
        A0f(r2, r0);
        return r0.A00;
    }

    public final Path A09(AnonymousClass0SG r8, AnonymousClass0I0 r9) {
        Path A0E;
        AnonymousClass0OO A04 = ((AnonymousClass0OO) r9).A01.A04(this.A03.A04.A0W);
        boolean z = false;
        if (A04 == null) {
            A06("ClipPath reference '%s' not found", this.A03.A04.A0W);
            return null;
        }
        AnonymousClass0HY r5 = (AnonymousClass0HY) A04;
        this.A06.push(this.A03);
        AnonymousClass0S1 r1 = new AnonymousClass0S1(this);
        A0X(C08900c2.A00(), r1);
        A0e(r5, r1);
        this.A03 = r1;
        Boolean bool = r5.A00;
        if (bool == null || bool.booleanValue()) {
            z = true;
        }
        Matrix matrix = new Matrix();
        if (!z) {
            matrix.preTranslate(r8.A01, r8.A02);
            matrix.preScale(r8.A03, r8.A00);
        }
        Matrix matrix2 = ((C03260Hc) r5).A00;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
        Path path = new Path();
        for (AnonymousClass0OO r12 : ((AbstractC03490Hz) r5).A01) {
            if ((r12 instanceof AnonymousClass0I0) && (A0E = A0E((AnonymousClass0I0) r12, true)) != null) {
                path.op(A0E, Path.Op.UNION);
            }
        }
        if (this.A03.A04.A0W != null) {
            AnonymousClass0SG r0 = ((AnonymousClass0I0) r5).A00;
            if (r0 == null) {
                r0 = A03(path);
                ((AnonymousClass0I0) r5).A00 = r0;
            }
            Path A09 = A09(r0, r5);
            if (A09 != null) {
                path.op(A09, Path.Op.INTERSECT);
            }
        }
        path.transform(matrix);
        this.A03 = (AnonymousClass0S1) this.A06.pop();
        return path;
    }

    public final Path A0A(AnonymousClass0HR r24) {
        float f;
        C08910c3 r0 = r24.A00;
        float f2 = 0.0f;
        if (r0 != null) {
            f = r0.A02(this);
        } else {
            f = 0.0f;
        }
        C08910c3 r02 = r24.A01;
        if (r02 != null) {
            f2 = r02.A03(this);
        }
        float A01 = r24.A02.A01(this);
        float f3 = f - A01;
        float f4 = f2 - A01;
        float f5 = f + A01;
        float f6 = f2 + A01;
        if (((AnonymousClass0I0) r24).A00 == null) {
            float f7 = 2.0f * A01;
            ((AnonymousClass0I0) r24).A00 = new AnonymousClass0SG(f3, f4, f7, f7);
        }
        float f8 = 0.5522848f * A01;
        Path path = new Path();
        path.moveTo(f, f4);
        float f9 = f + f8;
        float f10 = f2 - f8;
        path.cubicTo(f9, f4, f5, f10, f5, f2);
        float f11 = f2 + f8;
        path.cubicTo(f5, f11, f9, f6, f, f6);
        float f12 = f - f8;
        path.cubicTo(f12, f6, f3, f11, f3, f2);
        path.cubicTo(f3, f10, f12, f4, f, f4);
        path.close();
        return path;
    }

    public final Path A0B(AnonymousClass0HS r27) {
        float f;
        C08910c3 r0 = r27.A00;
        float f2 = 0.0f;
        if (r0 != null) {
            f = r0.A02(this);
        } else {
            f = 0.0f;
        }
        C08910c3 r02 = r27.A01;
        if (r02 != null) {
            f2 = r02.A03(this);
        }
        float A02 = r27.A02.A02(this);
        float A03 = r27.A03.A03(this);
        float f3 = f - A02;
        float f4 = f2 - A03;
        float f5 = f + A02;
        float f6 = f2 + A03;
        if (((AnonymousClass0I0) r27).A00 == null) {
            ((AnonymousClass0I0) r27).A00 = new AnonymousClass0SG(f3, f4, A02 * 2.0f, 2.0f * A03);
        }
        float f7 = A02 * 0.5522848f;
        float f8 = 0.5522848f * A03;
        Path path = new Path();
        path.moveTo(f, f4);
        float f9 = f + f7;
        float f10 = f2 - f8;
        path.cubicTo(f9, f4, f5, f10, f5, f2);
        float f11 = f8 + f2;
        path.cubicTo(f5, f11, f9, f6, f, f6);
        float f12 = f - f7;
        path.cubicTo(f12, f6, f3, f11, f3, f2);
        path.cubicTo(f3, f10, f12, f4, f, f4);
        path.close();
        return path;
    }

    public final Path A0C(AnonymousClass0HQ r6) {
        Path path = new Path();
        float[] fArr = r6.A00;
        path.moveTo(fArr[0], fArr[1]);
        int i = 2;
        while (true) {
            float[] fArr2 = r6.A00;
            if (i >= fArr2.length) {
                break;
            }
            path.lineTo(fArr2[i], fArr2[i + 1]);
            i += 2;
        }
        if (r6 instanceof AnonymousClass0HZ) {
            path.close();
        }
        if (((AnonymousClass0I0) r6).A00 == null) {
            ((AnonymousClass0I0) r6).A00 = A03(path);
        }
        return path;
    }

    public final Path A0D(AnonymousClass0HU r25) {
        float f;
        float f2;
        float f3;
        float f4;
        C08910c3 r1 = r25.A01;
        C08910c3 r0 = r25.A02;
        if (r1 != null) {
            f = r1.A02(this);
            if (r0 != null) {
                f2 = r25.A02.A03(this);
            }
            f2 = f;
        } else if (r0 == null) {
            f = 0.0f;
            f2 = 0.0f;
        } else {
            f = r0.A03(this);
            f2 = f;
        }
        float min = Math.min(f, r25.A03.A02(this) / 2.0f);
        float min2 = Math.min(f2, r25.A00.A03(this) / 2.0f);
        C08910c3 r02 = r25.A04;
        if (r02 != null) {
            f3 = r02.A02(this);
        } else {
            f3 = 0.0f;
        }
        C08910c3 r03 = r25.A05;
        if (r03 != null) {
            f4 = r03.A03(this);
        } else {
            f4 = 0.0f;
        }
        float A02 = r25.A03.A02(this);
        float A03 = r25.A00.A03(this);
        if (((AnonymousClass0I0) r25).A00 == null) {
            ((AnonymousClass0I0) r25).A00 = new AnonymousClass0SG(f3, f4, A02, A03);
        }
        float f5 = f3 + A02;
        float f6 = f4 + A03;
        Path path = new Path();
        if (min == 0.0f || min2 == 0.0f) {
            path.moveTo(f3, f4);
            path.lineTo(f5, f4);
            path.lineTo(f5, f6);
            path.lineTo(f3, f6);
            path.lineTo(f3, f4);
        } else {
            float f7 = min * 0.5522848f;
            float f8 = 0.5522848f * min2;
            float f9 = f4 + min2;
            path.moveTo(f3, f9);
            float f10 = f9 - f8;
            float f11 = f3 + min;
            float f12 = f11 - f7;
            path.cubicTo(f3, f10, f12, f4, f11, f4);
            float f13 = f5 - min;
            path.lineTo(f13, f4);
            float f14 = f13 + f7;
            path.cubicTo(f14, f4, f5, f10, f5, f9);
            float f15 = f6 - min2;
            path.lineTo(f5, f15);
            float f16 = f15 + f8;
            path.cubicTo(f5, f16, f14, f6, f13, f6);
            path.lineTo(f11, f6);
            path.cubicTo(f12, f6, f3, f16, f3, f15);
            path.lineTo(f3, f9);
        }
        path.close();
        return path;
    }

    public final Path A0E(AnonymousClass0I0 r11, boolean z) {
        Path path;
        Path A09;
        float f;
        float f2;
        float f3;
        Matrix matrix;
        Path.FillType fillType;
        this.A06.push(this.A03);
        AnonymousClass0S1 r0 = new AnonymousClass0S1(this.A03, this);
        this.A03 = r0;
        A0c(r11, r0);
        if (A0h() && A0j()) {
            if (r11 instanceof AnonymousClass0HW) {
                if (!z) {
                    A06("<use> elements inside a <clipPath> cannot reference another <use>", new Object[0]);
                }
                AnonymousClass0HW r3 = (AnonymousClass0HW) r11;
                AnonymousClass0OO A04 = ((AnonymousClass0OO) r11).A01.A04(r3.A04);
                if (A04 == null) {
                    A06("Use reference '%s' not found", r3.A04);
                } else if (A04 instanceof AnonymousClass0I0) {
                    path = A0E((AnonymousClass0I0) A04, false);
                    if (path != null) {
                        if (((AnonymousClass0I0) r3).A00 == null) {
                            ((AnonymousClass0I0) r3).A00 = A03(path);
                        }
                        Matrix matrix2 = ((C03260Hc) r3).A00;
                        if (matrix2 != null) {
                            path.transform(matrix2);
                        }
                        if (!(this.A03.A04.A0W == null || (A09 = A09(r11.A00, r11)) == null)) {
                            path.op(A09, Path.Op.INTERSECT);
                        }
                        this.A03 = (AnonymousClass0S1) this.A06.pop();
                        return path;
                    }
                    return null;
                }
            } else {
                if (r11 instanceof AbstractC03280He) {
                    AbstractC03280He r1 = (AbstractC03280He) r11;
                    if (r11 instanceof AnonymousClass0HP) {
                        path = new C08500bI(((AnonymousClass0HP) r11).A00, this).A02;
                        if (r11.A00 == null) {
                            r11.A00 = A03(path);
                        }
                    } else if (r11 instanceof AnonymousClass0HU) {
                        path = A0D((AnonymousClass0HU) r11);
                    } else if (r11 instanceof AnonymousClass0HR) {
                        path = A0A((AnonymousClass0HR) r11);
                    } else if (r11 instanceof AnonymousClass0HS) {
                        path = A0B((AnonymousClass0HS) r11);
                    } else {
                        if (r11 instanceof AnonymousClass0HQ) {
                            path = A0C((AnonymousClass0HQ) r11);
                        }
                        return null;
                    }
                    if (path != null) {
                        if (((AnonymousClass0I0) r1).A00 == null) {
                            ((AnonymousClass0I0) r1).A00 = A03(path);
                        }
                        matrix = r1.A00;
                    }
                    return null;
                } else if (r11 instanceof C03440Hu) {
                    C03440Hu r32 = (C03440Hu) r11;
                    List list = ((AbstractC03460Hw) r32).A02;
                    float f4 = 0.0f;
                    if (list == null || list.size() == 0) {
                        f = 0.0f;
                    } else {
                        f = ((C08910c3) ((AbstractC03460Hw) r32).A02.get(0)).A02(this);
                    }
                    List list2 = ((AbstractC03460Hw) r32).A03;
                    if (list2 == null || list2.size() == 0) {
                        f2 = 0.0f;
                    } else {
                        f2 = ((C08910c3) ((AbstractC03460Hw) r32).A03.get(0)).A03(this);
                    }
                    List list3 = ((AbstractC03460Hw) r32).A00;
                    if (list3 == null || list3.size() == 0) {
                        f3 = 0.0f;
                    } else {
                        f3 = ((C08910c3) ((AbstractC03460Hw) r32).A00.get(0)).A02(this);
                    }
                    List list4 = ((AbstractC03460Hw) r32).A01;
                    if (!(list4 == null || list4.size() == 0)) {
                        f4 = ((C08910c3) ((AbstractC03460Hw) r32).A01.get(0)).A03(this);
                    }
                    if (this.A03.A04.A0C != AnonymousClass0JO.Start) {
                        float A08 = A08(r32);
                        if (this.A03.A04.A0C == AnonymousClass0JO.Middle) {
                            A08 /= 2.0f;
                        }
                        f -= A08;
                    }
                    if (((AnonymousClass0I0) r32).A00 == null) {
                        AnonymousClass0I6 r02 = new AnonymousClass0I6(this, f, f2);
                        A0f(r32, r02);
                        RectF rectF = r02.A02;
                        ((AnonymousClass0I0) r32).A00 = new AnonymousClass0SG(rectF.left, rectF.top, rectF.width(), rectF.height());
                    }
                    path = new Path();
                    A0f(r32, new AnonymousClass0I5(path, this, f + f3, f2 + f4));
                    matrix = r32.A00;
                } else {
                    A06("Invalid %s element found in clipPath definition", r11.A00());
                    return null;
                }
                if (matrix != null) {
                    path.transform(matrix);
                }
                AnonymousClass0JC r12 = this.A03.A04.A06;
                if (r12 == null || r12 != AnonymousClass0JC.EvenOdd) {
                    fillType = Path.FillType.WINDING;
                } else {
                    fillType = Path.FillType.EVEN_ODD;
                }
                path.setFillType(fillType);
                if (this.A03.A04.A0W == null) {
                    path.op(A09, Path.Op.INTERSECT);
                }
                this.A03 = (AnonymousClass0S1) this.A06.pop();
                return path;
            }
        }
        this.A03 = (AnonymousClass0S1) this.A06.pop();
        return null;
    }

    public final AnonymousClass0SG A0F(C08910c3 r6, C08910c3 r7, C08910c3 r8, C08910c3 r9) {
        float f;
        float f2;
        float f3;
        float f4 = 0.0f;
        if (r6 != null) {
            f = r6.A02(this);
        } else {
            f = 0.0f;
        }
        if (r7 != null) {
            f4 = r7.A03(this);
        }
        AnonymousClass0S1 r1 = this.A03;
        AnonymousClass0SG r0 = r1.A02;
        if (r0 == null) {
            r0 = r1.A03;
        }
        if (r8 != null) {
            f2 = r8.A02(this);
        } else {
            f2 = r0.A03;
        }
        if (r9 != null) {
            f3 = r9.A03(this);
        } else {
            f3 = r0.A00;
        }
        return new AnonymousClass0SG(f, f4, f2, f3);
    }

    public final AnonymousClass0JO A0G() {
        AnonymousClass0JO r1;
        C08900c2 r2 = this.A03.A04;
        if (r2.A0E == AnonymousClass0JD.LTR || (r1 = r2.A0C) == AnonymousClass0JO.Middle) {
            return r2.A0C;
        }
        AnonymousClass0JO r0 = AnonymousClass0JO.Start;
        return r1 == r0 ? AnonymousClass0JO.End : r0;
    }

    public final String A0H(String str, boolean z, boolean z2) {
        String str2;
        if (this.A03.A07) {
            str2 = "[\\n\\t]";
        } else {
            str = str.replaceAll("\\n", "").replaceAll("\\t", " ");
            if (z) {
                str = str.replaceAll("^\\s+", "");
            }
            if (z2) {
                str = str.replaceAll("\\s+$", "");
            }
            str2 = "\\s{2,}";
        }
        return str.replaceAll(str2, " ");
    }

    public final void A0I() {
        this.A01.restore();
        this.A03 = (AnonymousClass0S1) this.A06.pop();
    }

    public final void A0J() {
        this.A01.save();
        this.A06.push(this.A03);
        this.A03 = new AnonymousClass0S1(this.A03, this);
    }

    public final void A0K() {
        C03360Hm r1;
        C08900c2 r2 = this.A03.A04;
        AbstractC08890c1 r12 = r2.A0K;
        if (r12 instanceof C03360Hm) {
            r1 = (C03360Hm) r12;
        } else if (r12 instanceof C03350Hl) {
            r1 = r2.A02;
        } else {
            return;
        }
        int i = r1.A00;
        Float f = r2.A0U;
        if (f != null) {
            i = A00(i, f.floatValue());
        }
        this.A01.drawColor(i);
    }

    public final void A0L(float f, float f2, float f3, float f4) {
        float f5 = f3 + f;
        float f6 = f4 + f2;
        C04880Nk r0 = this.A03.A04.A01;
        if (r0 != null) {
            f += r0.A01.A02(this);
            f2 += this.A03.A04.A01.A03.A03(this);
            f5 -= this.A03.A04.A01.A02.A02(this);
            f6 -= this.A03.A04.A01.A00.A03(this);
        }
        this.A01.clipRect(f, f2, f5, f6);
    }

    public final void A0M(Matrix matrix, Path path, AnonymousClass0OO r13, boolean z) {
        Object[] objArr;
        String str;
        Path A0C;
        AnonymousClass0SG r0;
        float f;
        float f2;
        float f3;
        Path.FillType fillType;
        AnonymousClass0I0 r132;
        if (A0h()) {
            Canvas canvas = this.A01;
            canvas.save();
            this.A06.push(this.A03);
            AnonymousClass0S1 r3 = new AnonymousClass0S1(this.A03, this);
            this.A03 = r3;
            if (r13 instanceof AnonymousClass0HW) {
                if (z) {
                    AnonymousClass0HW r133 = (AnonymousClass0HW) r13;
                    A0c(r133, r3);
                    if (A0h() && A0j()) {
                        Matrix matrix2 = ((C03260Hc) r133).A00;
                        if (matrix2 != null) {
                            matrix.preConcat(matrix2);
                        }
                        AnonymousClass0OO A04 = ((AnonymousClass0OO) r133).A01.A04(r133.A04);
                        if (A04 == null) {
                            objArr = new Object[]{r133.A04};
                            str = "Use reference '%s' not found";
                        } else {
                            A0R(((AnonymousClass0I0) r133).A00, r133);
                            A0M(matrix, path, A04, false);
                        }
                    }
                    canvas.restore();
                    this.A03 = (AnonymousClass0S1) this.A06.pop();
                }
                objArr = new Object[0];
                str = "<use> elements inside a <clipPath> cannot reference another <use>";
                A06(str, objArr);
                canvas.restore();
                this.A03 = (AnonymousClass0S1) this.A06.pop();
            }
            if (r13 instanceof AnonymousClass0HP) {
                AnonymousClass0HP r134 = (AnonymousClass0HP) r13;
                A0c(r134, r3);
                if (A0h() && A0j()) {
                    Matrix matrix3 = ((AbstractC03280He) r134).A00;
                    if (matrix3 != null) {
                        matrix.preConcat(matrix3);
                    }
                    A0C = new C08500bI(r134.A00, this).A02;
                    r0 = ((AnonymousClass0I0) r134).A00;
                    r132 = r134;
                    if (r0 == null) {
                        r0 = A03(A0C);
                        ((AnonymousClass0I0) r134).A00 = r0;
                        r132 = r134;
                    }
                    A0R(r0, r132);
                }
                canvas.restore();
                this.A03 = (AnonymousClass0S1) this.A06.pop();
            }
            if (r13 instanceof C03440Hu) {
                C03440Hu r135 = (C03440Hu) r13;
                A0c(r135, r3);
                if (A0h()) {
                    Matrix matrix4 = r135.A00;
                    if (matrix4 != null) {
                        matrix.preConcat(matrix4);
                    }
                    List list = ((AbstractC03460Hw) r135).A02;
                    float f4 = 0.0f;
                    if (list == null || list.size() == 0) {
                        f = 0.0f;
                    } else {
                        f = ((C08910c3) ((AbstractC03460Hw) r135).A02.get(0)).A02(this);
                    }
                    List list2 = ((AbstractC03460Hw) r135).A03;
                    if (list2 == null || list2.size() == 0) {
                        f2 = 0.0f;
                    } else {
                        f2 = ((C08910c3) ((AbstractC03460Hw) r135).A03.get(0)).A03(this);
                    }
                    List list3 = ((AbstractC03460Hw) r135).A00;
                    if (list3 == null || list3.size() == 0) {
                        f3 = 0.0f;
                    } else {
                        f3 = ((C08910c3) ((AbstractC03460Hw) r135).A00.get(0)).A02(this);
                    }
                    List list4 = ((AbstractC03460Hw) r135).A01;
                    if (!(list4 == null || list4.size() == 0)) {
                        f4 = ((C08910c3) ((AbstractC03460Hw) r135).A01.get(0)).A03(this);
                    }
                    if (this.A03.A04.A0C != AnonymousClass0JO.Start) {
                        float A08 = A08(r135);
                        if (this.A03.A04.A0C == AnonymousClass0JO.Middle) {
                            A08 /= 2.0f;
                        }
                        f -= A08;
                    }
                    AnonymousClass0SG r7 = ((AnonymousClass0I0) r135).A00;
                    if (r7 == null) {
                        AnonymousClass0I6 r02 = new AnonymousClass0I6(this, f, f2);
                        A0f(r135, r02);
                        RectF rectF = r02.A02;
                        r7 = new AnonymousClass0SG(rectF.left, rectF.top, rectF.width(), rectF.height());
                        ((AnonymousClass0I0) r135).A00 = r7;
                    }
                    A0R(r7, r135);
                    A0C = new Path();
                    A0f(r135, new AnonymousClass0I5(A0C, this, f + f3, f2 + f4));
                }
            } else if (r13 instanceof AbstractC03280He) {
                AbstractC03280He r136 = (AbstractC03280He) r13;
                A0c(r136, r3);
                if (A0h() && A0j()) {
                    Matrix matrix5 = r136.A00;
                    if (matrix5 != null) {
                        matrix.preConcat(matrix5);
                    }
                    if (r136 instanceof AnonymousClass0HU) {
                        A0C = A0D((AnonymousClass0HU) r136);
                    } else if (r136 instanceof AnonymousClass0HR) {
                        A0C = A0A((AnonymousClass0HR) r136);
                    } else if (r136 instanceof AnonymousClass0HS) {
                        A0C = A0B((AnonymousClass0HS) r136);
                    } else if (r136 instanceof AnonymousClass0HQ) {
                        A0C = A0C((AnonymousClass0HQ) r136);
                    }
                    r0 = ((AnonymousClass0I0) r136).A00;
                    r132 = r136;
                    A0R(r0, r132);
                }
            } else {
                objArr = new Object[]{r13.toString()};
                str = "Invalid %s element found in clipPath definition";
                A06(str, objArr);
            }
            canvas.restore();
            this.A03 = (AnonymousClass0S1) this.A06.pop();
            AnonymousClass0JC r2 = this.A03.A04.A06;
            if (r2 == null || r2 != AnonymousClass0JC.EvenOdd) {
                fillType = Path.FillType.WINDING;
            } else {
                fillType = Path.FillType.EVEN_ODD;
            }
            path.setFillType(fillType);
            path.addPath(A0C, matrix);
            canvas.restore();
            this.A03 = (AnonymousClass0S1) this.A06.pop();
        }
    }

    public final void A0N(Path path) {
        AnonymousClass0S1 r2 = this.A03;
        if (r2.A04.A0F == AnonymousClass0JE.NonScalingStroke) {
            Canvas canvas = this.A01;
            Matrix matrix = canvas.getMatrix();
            Path path2 = new Path();
            path.transform(matrix, path2);
            canvas.setMatrix(new Matrix());
            Shader shader = this.A03.A01.getShader();
            Matrix matrix2 = new Matrix();
            if (shader != null) {
                shader.getLocalMatrix(matrix2);
                Matrix matrix3 = new Matrix(matrix2);
                matrix3.postConcat(matrix);
                shader.setLocalMatrix(matrix3);
            }
            canvas.drawPath(path2, this.A03.A01);
            canvas.setMatrix(matrix);
            if (shader != null) {
                shader.setLocalMatrix(matrix2);
                return;
            }
            return;
        }
        this.A01.drawPath(path, r2.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0190, code lost:
        if (r4 != false) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r0.booleanValue() == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0O(android.graphics.Path r20, X.AnonymousClass0I0 r21) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0O(android.graphics.Path, X.0I0):void");
    }

    public final void A0P(AnonymousClass0SV r5, AnonymousClass0SG r6, AnonymousClass0SG r7, C03370Hn r8) {
        if (r6.A03 != 0.0f && r6.A00 != 0.0f) {
            if (r5 == null && (r5 = ((AbstractC03240Ha) r8).A00) == null) {
                r5 = AnonymousClass0SV.A02;
            }
            A0c(r8, this.A03);
            if (A0h()) {
                AnonymousClass0S1 r0 = this.A03;
                r0.A03 = r6;
                if (!r0.A04.A0M.booleanValue()) {
                    A0L(r6.A01, r6.A02, r6.A03, r6.A00);
                }
                A0R(this.A03.A03, r8);
                Canvas canvas = this.A01;
                AnonymousClass0SG r02 = this.A03.A03;
                if (r7 != null) {
                    canvas.concat(A01(r5, r02, r7));
                    this.A03.A02 = ((AbstractC03420Hs) r8).A00;
                } else {
                    canvas.translate(r02.A01, r02.A02);
                }
                boolean A0i = A0i();
                A0K();
                A0Y(r8, true);
                if (A0i) {
                    A0a(r8);
                }
                A0b(r8);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        if (r0.booleanValue() == false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x010d, code lost:
        if (r0.booleanValue() == false) goto L_0x010f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0232  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0352  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0Q(X.AnonymousClass0SG r23, X.C03340Hk r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 878
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0Q(X.0SG, X.0Hk, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        if (r0.booleanValue() != false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0R(X.AnonymousClass0SG r8, X.AnonymousClass0I0 r9) {
        /*
            r7 = this;
            X.0S1 r0 = r7.A03
            X.0c2 r0 = r0.A04
            java.lang.String r2 = r0.A0W
            if (r2 == 0) goto L_0x0019
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 19
            if (r1 < r0) goto L_0x001a
            android.graphics.Path r1 = r7.A09(r8, r9)
            if (r1 == 0) goto L_0x0019
            android.graphics.Canvas r0 = r7.A01
            r0.clipPath(r1)
        L_0x0019:
            return
        L_0x001a:
            X.0Q5 r0 = r9.A01
            X.0OO r6 = r0.A04(r2)
            r4 = 1
            r3 = 0
            if (r6 != 0) goto L_0x0034
            java.lang.Object[] r1 = new java.lang.Object[r4]
            X.0S1 r0 = r7.A03
            X.0c2 r0 = r0.A04
            java.lang.String r0 = r0.A0W
            r1[r3] = r0
            java.lang.String r0 = "ClipPath reference '%s' not found"
            A06(r0, r1)
            return
        L_0x0034:
            X.0HY r6 = (X.AnonymousClass0HY) r6
            java.util.List r0 = r6.A01
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0044
            android.graphics.Canvas r0 = r7.A01
            r0.clipRect(r3, r3, r3, r3)
            return
        L_0x0044:
            java.lang.Boolean r0 = r6.A00
            if (r0 == 0) goto L_0x004f
            boolean r0 = r0.booleanValue()
            r2 = 0
            if (r0 == 0) goto L_0x0050
        L_0x004f:
            r2 = 1
        L_0x0050:
            boolean r0 = r9 instanceof X.C03260Hc
            if (r0 == 0) goto L_0x006a
            if (r2 != 0) goto L_0x006a
            java.lang.Object[] r1 = new java.lang.Object[r4]
            java.lang.String r0 = r9.A00()
            r1[r3] = r0
            java.lang.String r0 = "<clipPath clipPathUnits=\"objectBoundingBox\"> is not supported when referenced from container elements (like %s)"
            java.lang.String r1 = java.lang.String.format(r0, r1)
            java.lang.String r0 = "SVGAndroidRenderer"
            android.util.Log.w(r0, r1)
            return
        L_0x006a:
            android.graphics.Canvas r5 = r7.A01
            r5.save()
            java.util.Stack r1 = r7.A06
            X.0S1 r0 = r7.A03
            r1.push(r0)
            X.0S1 r1 = r7.A03
            X.0S1 r0 = new X.0S1
            r0.<init>(r1, r7)
            r7.A03 = r0
            if (r2 != 0) goto L_0x0097
            android.graphics.Matrix r2 = new android.graphics.Matrix
            r2.<init>()
            float r1 = r8.A01
            float r0 = r8.A02
            r2.preTranslate(r1, r0)
            float r1 = r8.A03
            float r0 = r8.A00
            r2.preScale(r1, r0)
            r5.concat(r2)
        L_0x0097:
            android.graphics.Matrix r0 = r6.A00
            if (r0 == 0) goto L_0x009e
            r5.concat(r0)
        L_0x009e:
            X.0S1 r1 = new X.0S1
            r1.<init>(r7)
            X.0c2 r0 = X.C08900c2.A00()
            r7.A0X(r0, r1)
            r7.A0e(r6, r1)
            r7.A03 = r1
            X.0SG r0 = r6.A00
            r7.A0R(r0, r6)
            android.graphics.Path r3 = new android.graphics.Path
            r3.<init>()
            java.util.List r0 = r6.A01
            java.util.Iterator r2 = r0.iterator()
        L_0x00bf:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00d4
            java.lang.Object r1 = r2.next()
            X.0OO r1 = (X.AnonymousClass0OO) r1
            android.graphics.Matrix r0 = new android.graphics.Matrix
            r0.<init>()
            r7.A0M(r0, r3, r1, r4)
            goto L_0x00bf
        L_0x00d4:
            r5.clipPath(r3)
            r5.restore()
            java.util.Stack r0 = r7.A06
            java.lang.Object r0 = r0.pop()
            X.0S1 r0 = (X.AnonymousClass0S1) r0
            r7.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0R(X.0SG, X.0I0):void");
    }

    public final void A0S(AbstractC03300Hg r5, String str) {
        Object[] objArr;
        String str2;
        AnonymousClass0OO A04 = ((AnonymousClass0OO) r5).A01.A04(str);
        if (A04 == null) {
            Log.w("SVGAndroidRenderer", String.format("Gradient reference '%s' not found", str));
            return;
        }
        if (!(A04 instanceof AbstractC03300Hg)) {
            objArr = new Object[0];
            str2 = "Gradient href attributes must point to other gradient elements";
        } else if (A04 == r5) {
            objArr = new Object[]{str};
            str2 = "Circular reference in gradient href attribute '%s'";
        } else {
            AbstractC03300Hg r2 = (AbstractC03300Hg) A04;
            if (r5.A02 == null) {
                r5.A02 = r2.A02;
            }
            if (r5.A00 == null) {
                r5.A00 = r2.A00;
            }
            if (r5.A01 == null) {
                r5.A01 = r2.A01;
            }
            if (r5.A04.isEmpty()) {
                r5.A04 = r2.A04;
            }
            try {
                if (r5 instanceof AnonymousClass0HN) {
                    AnonymousClass0HN r1 = (AnonymousClass0HN) r5;
                    AnonymousClass0HN r3 = (AnonymousClass0HN) A04;
                    if (r1.A00 == null) {
                        r1.A00 = r3.A00;
                    }
                    if (r1.A02 == null) {
                        r1.A02 = r3.A02;
                    }
                    if (r1.A01 == null) {
                        r1.A01 = r3.A01;
                    }
                    if (r1.A03 == null) {
                        r1.A03 = r3.A03;
                    }
                } else {
                    AnonymousClass0HO r12 = (AnonymousClass0HO) r5;
                    AnonymousClass0HO r32 = (AnonymousClass0HO) A04;
                    if (r12.A00 == null) {
                        r12.A00 = r32.A00;
                    }
                    if (r12.A01 == null) {
                        r12.A01 = r32.A01;
                    }
                    if (r12.A04 == null) {
                        r12.A04 = r32.A04;
                    }
                    if (r12.A02 == null) {
                        r12.A02 = r32.A02;
                    }
                    if (r12.A03 == null) {
                        r12.A03 = r32.A03;
                    }
                }
            } catch (ClassCastException unused) {
            }
            String str3 = r2.A03;
            if (str3 != null) {
                A0S(r5, str3);
                return;
            }
            return;
        }
        A06(str2, objArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00dc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0T(X.AbstractC03280He r25) {
        /*
        // Method dump skipped, instructions count: 497
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0T(X.0He):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d0, code lost:
        if (r9 != 8) goto L_0x00d2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0U(X.C03400Hq r14, X.C05460Pq r15) {
        /*
        // Method dump skipped, instructions count: 346
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0U(X.0Hq, X.0Pq):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r0.booleanValue() == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0V(X.C03270Hd r7, X.AnonymousClass0I0 r8) {
        /*
            r6 = this;
            r4 = 0
            java.lang.Boolean r0 = r7.A05
            if (r0 == 0) goto L_0x000c
            boolean r0 = r0.booleanValue()
            r1 = 1
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            r1 = 0
        L_0x000d:
            r5 = 1065353216(0x3f800000, float:1.0)
            X.0c3 r0 = r7.A01
            if (r1 == 0) goto L_0x0075
            if (r0 == 0) goto L_0x0070
            float r2 = r0.A02(r6)
        L_0x0019:
            X.0c3 r0 = r7.A00
            if (r0 == 0) goto L_0x006b
            float r3 = r0.A03(r6)
        L_0x0021:
            r1 = 0
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x006a
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x006a
            r6.A0J()
            X.0S1 r1 = new X.0S1
            r1.<init>(r6)
            X.0c2 r0 = X.C08900c2.A00()
            r6.A0X(r0, r1)
            r6.A0e(r7, r1)
            r6.A03 = r1
            X.0c2 r1 = r1.A04
            java.lang.Float r0 = java.lang.Float.valueOf(r5)
            r1.A0P = r0
            java.lang.Boolean r0 = r7.A04
            if (r0 == 0) goto L_0x0064
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0064
            android.graphics.Canvas r2 = r6.A01
            X.0SG r0 = r8.A00
            float r1 = r0.A01
            float r0 = r0.A02
            r2.translate(r1, r0)
            X.0SG r0 = r8.A00
            float r1 = r0.A03
            float r0 = r0.A00
            r2.scale(r1, r0)
        L_0x0064:
            r6.A0Y(r7, r4)
            r6.A0I()
        L_0x006a:
            return
        L_0x006b:
            X.0SG r0 = r8.A00
            float r3 = r0.A00
            goto L_0x0021
        L_0x0070:
            X.0SG r0 = r8.A00
            float r2 = r0.A03
            goto L_0x0019
        L_0x0075:
            r3 = 1067030938(0x3f99999a, float:1.2)
            if (r0 == 0) goto L_0x008f
            float r2 = r0.A04(r6, r5)
        L_0x007e:
            X.0c3 r0 = r7.A00
            if (r0 == 0) goto L_0x0086
            float r3 = r0.A04(r6, r5)
        L_0x0086:
            X.0SG r1 = r8.A00
            float r0 = r1.A03
            float r2 = r2 * r0
            float r0 = r1.A00
            float r3 = r3 * r0
            goto L_0x0021
        L_0x008f:
            r2 = 1067030938(0x3f99999a, float:1.2)
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0V(X.0Hd, X.0I0):void");
    }

    public final void A0W(C03410Hr r5, String str) {
        Object[] objArr;
        String str2;
        AnonymousClass0OO A04 = ((AnonymousClass0OO) r5).A01.A04(str);
        if (A04 == null) {
            Log.w("SVGAndroidRenderer", String.format("Pattern reference '%s' not found", str));
            return;
        }
        if (!(A04 instanceof C03410Hr)) {
            objArr = new Object[0];
            str2 = "Pattern href attributes must point to other pattern elements";
        } else if (A04 == r5) {
            objArr = new Object[]{str};
            str2 = "Circular reference in pattern href attribute '%s'";
        } else {
            C03410Hr r1 = (C03410Hr) A04;
            if (r5.A06 == null) {
                r5.A06 = r1.A06;
            }
            if (r5.A05 == null) {
                r5.A05 = r1.A05;
            }
            if (r5.A00 == null) {
                r5.A00 = r1.A00;
            }
            if (r5.A03 == null) {
                r5.A03 = r1.A03;
            }
            if (r5.A04 == null) {
                r5.A04 = r1.A04;
            }
            if (r5.A02 == null) {
                r5.A02 = r1.A02;
            }
            if (r5.A01 == null) {
                r5.A01 = r1.A01;
            }
            if (((AbstractC03490Hz) r5).A01.isEmpty()) {
                ((AbstractC03490Hz) r5).A01 = ((AbstractC03490Hz) r1).A01;
            }
            if (((AbstractC03420Hs) r5).A00 == null) {
                ((AbstractC03420Hs) r5).A00 = ((AbstractC03420Hs) r1).A00;
            }
            if (((AbstractC03240Ha) r5).A00 == null) {
                ((AbstractC03240Ha) r5).A00 = ((AbstractC03240Ha) r1).A00;
            }
            String str3 = r1.A07;
            if (str3 != null) {
                A0W(r5, str3);
                return;
            }
            return;
        }
        A06(str2, objArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        if (r4 == X.C03360Hm.A02) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0078, code lost:
        if (r4 == X.C03360Hm.A02) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0X(X.C08900c2 r13, X.AnonymousClass0S1 r14) {
        /*
        // Method dump skipped, instructions count: 964
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0X(X.0c2, X.0S1):void");
    }

    public final void A0Y(AbstractC12490i0 r3, boolean z) {
        if (z) {
            this.A05.push(r3);
            this.A04.push(this.A01.getMatrix());
        }
        for (AnonymousClass0OO r0 : r3.ABO()) {
            A0d(r0);
        }
        if (z) {
            this.A05.pop();
            this.A04.pop();
        }
    }

    public final void A0Z(AnonymousClass0I0 r4) {
        AbstractC08890c1 r2 = this.A03.A04.A0G;
        if (r2 instanceof C03340Hk) {
            A0Q(r4.A00, (C03340Hk) r2, true);
        }
        AbstractC08890c1 r22 = this.A03.A04.A0J;
        if (r22 instanceof C03340Hk) {
            A0Q(r4.A00, (C03340Hk) r22, false);
        }
    }

    public final void A0a(AnonymousClass0I0 r8) {
        if (this.A03.A04.A0a != null) {
            Paint paint = new Paint();
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            Canvas canvas = this.A01;
            canvas.saveLayer(null, paint, 31);
            Paint paint2 = new Paint();
            paint2.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.2127f, 0.7151f, 0.0722f, 0.0f, 0.0f})));
            canvas.saveLayer(null, paint2, 31);
            C03270Hd r3 = (C03270Hd) this.A02.A04(this.A03.A04.A0a);
            A0V(r3, r8);
            canvas.restore();
            Paint paint3 = new Paint();
            paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            canvas.saveLayer(null, paint3, 31);
            A0V(r3, r8);
            canvas.restore();
            canvas.restore();
        }
        A0I();
    }

    public final void A0b(AnonymousClass0I0 r11) {
        if (((AnonymousClass0OO) r11).A00 != null && r11.A00 != null) {
            Matrix matrix = new Matrix();
            if (((Matrix) this.A04.peek()).invert(matrix)) {
                AnonymousClass0SG r3 = r11.A00;
                float f = r3.A01;
                float f2 = r3.A02;
                float f3 = f + r3.A03;
                int i = 2;
                float f4 = f2 + r3.A00;
                float[] fArr = {f, f2, f3, f2, f3, f4, f, f4};
                matrix.preConcat(this.A01.getMatrix());
                matrix.mapPoints(fArr);
                float f5 = fArr[0];
                float f6 = fArr[1];
                RectF rectF = new RectF(f5, f6, f5, f6);
                do {
                    if (fArr[i] < rectF.left) {
                        rectF.left = fArr[i];
                    }
                    if (fArr[i] > rectF.right) {
                        rectF.right = fArr[i];
                    }
                    int i2 = i + 1;
                    if (fArr[i2] < rectF.top) {
                        rectF.top = fArr[i2];
                    }
                    if (fArr[i2] > rectF.bottom) {
                        rectF.bottom = fArr[i2];
                    }
                    i += 2;
                } while (i <= 6);
                AnonymousClass0I0 r5 = (AnonymousClass0I0) this.A05.peek();
                AnonymousClass0SG r32 = r5.A00;
                float f7 = rectF.left;
                if (r32 == null) {
                    float f8 = rectF.top;
                    r5.A00 = new AnonymousClass0SG(f7, f8, rectF.right - f7, rectF.bottom - f8);
                    return;
                }
                float f9 = rectF.top;
                AnonymousClass0SG r52 = new AnonymousClass0SG(f7, f9, rectF.right - f7, rectF.bottom - f9);
                float f10 = r52.A01;
                float f11 = r32.A01;
                if (f10 < f11) {
                    r32.A01 = f10;
                    f11 = f10;
                }
                float f12 = r52.A02;
                float f13 = r32.A02;
                if (f12 < f13) {
                    r32.A02 = f12;
                    f13 = f12;
                }
                float f14 = r52.A01 + r52.A03;
                if (f14 > r32.A03 + f11) {
                    r32.A03 = f14 - f11;
                }
                float f15 = r52.A02 + r52.A00;
                if (f15 > r32.A00 + f13) {
                    r32.A00 = f15 - f13;
                }
            }
        }
    }

    public final void A0c(AnonymousClass0I1 r5, AnonymousClass0S1 r6) {
        boolean z = false;
        if (((AnonymousClass0OO) r5).A00 == null) {
            z = true;
        }
        C08900c2 r3 = r6.A04;
        Boolean bool = Boolean.TRUE;
        r3.A0L = bool;
        if (!z) {
            bool = Boolean.FALSE;
        }
        r3.A0M = bool;
        r3.A01 = null;
        r3.A0W = null;
        Float valueOf = Float.valueOf(1.0f);
        r3.A0P = valueOf;
        r3.A0I = C03360Hm.A01;
        r3.A0R = valueOf;
        r3.A0a = null;
        r3.A0H = null;
        r3.A0Q = valueOf;
        r3.A0K = null;
        r3.A0U = valueOf;
        r3.A0F = AnonymousClass0JE.None;
        C08900c2 r0 = r5.A00;
        if (r0 != null) {
            A0X(r0, r6);
        }
        List list = this.A02.A00.A00;
        if (list != null && !list.isEmpty()) {
            for (AnonymousClass0OU r2 : this.A02.A00.A00) {
                if (AnonymousClass0UX.A02(null, r2.A00, r5)) {
                    A0X(r2.A02, r6);
                }
            }
        }
        C08900c2 r02 = r5.A01;
        if (r02 != null) {
            A0X(r02, r6);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:409:0x0219 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:90:0x0219 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v0, types: [X.0Ua] */
    /* JADX WARN: Type inference failed for: r12v1, types: [X.0I0] */
    /* JADX WARN: Type inference failed for: r12v2, types: [X.0I0] */
    /* JADX WARN: Type inference failed for: r12v3 */
    /* JADX WARN: Type inference failed for: r12v13, types: [X.0Hc, X.0I0, X.0i0, X.0I1] */
    /* JADX WARN: Type inference failed for: r12v14, types: [X.0Hz, X.0Hc, X.0I0, X.0I1] */
    /* JADX WARN: Type inference failed for: r12v15, types: [X.0HW, X.0OO, X.0Hc, java.lang.Object, X.0I0, X.0I1] */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01ba, code lost:
        if (r1 != false) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0219, code lost:
        if (r8 != false) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x021b, code lost:
        A0a(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x021e, code lost:
        A0b(r12);
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0d(X.AnonymousClass0OO r12) {
        /*
        // Method dump skipped, instructions count: 1938
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0d(X.0OO):void");
    }

    public final void A0e(AnonymousClass0OO r3, AnonymousClass0S1 r4) {
        ArrayList arrayList = new ArrayList();
        while (true) {
            if (r3 instanceof AnonymousClass0I1) {
                arrayList.add(0, r3);
            }
            AbstractC12490i0 r32 = r3.A00;
            if (r32 == null) {
                break;
            }
            r3 = (AnonymousClass0OO) r32;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            A0c((AnonymousClass0I1) it.next(), r4);
        }
        AnonymousClass0S1 r1 = this.A03;
        r4.A02 = r1.A02;
        r4.A03 = r1.A03;
    }

    public final void A0f(AbstractC03250Hb r13, AnonymousClass0P4 r14) {
        float f;
        float f2;
        float f3;
        AnonymousClass0JO A0G;
        float A02;
        float f4;
        if (A0h()) {
            Iterator it = ((AbstractC03490Hz) r13).A01.iterator();
            boolean z = true;
            while (it.hasNext()) {
                AnonymousClass0OO r1 = (AnonymousClass0OO) it.next();
                if (r1 instanceof C03330Hj) {
                    r14.A00(A0H(((C03330Hj) r1).A00, z, !it.hasNext()));
                } else if (r14.A01((AbstractC03250Hb) r1)) {
                    if (r1 instanceof C03480Hy) {
                        A0J();
                        C03480Hy r12 = (C03480Hy) r1;
                        A0c(r12, this.A03);
                        if (A0h() && A0j()) {
                            AnonymousClass0OO A04 = ((AnonymousClass0OO) r12).A01.A04(r12.A02);
                            if (A04 == null) {
                                A06("TextPath reference '%s' not found", r12.A02);
                            } else {
                                AnonymousClass0HP r4 = (AnonymousClass0HP) A04;
                                Path path = new C08500bI(r4.A00, this).A02;
                                Matrix matrix = ((AbstractC03280He) r4).A00;
                                if (matrix != null) {
                                    path.transform(matrix);
                                }
                                PathMeasure pathMeasure = new PathMeasure(path, false);
                                C08910c3 r2 = r12.A00;
                                if (r2 != null) {
                                    f4 = r2.A04(this, pathMeasure.getLength());
                                } else {
                                    f4 = 0.0f;
                                }
                                AnonymousClass0JO A0G2 = A0G();
                                if (A0G2 != AnonymousClass0JO.Start) {
                                    float A08 = A08(r12);
                                    if (A0G2 == AnonymousClass0JO.Middle) {
                                        A08 /= 2.0f;
                                    }
                                    f4 -= A08;
                                }
                                A0Z(r12.A01);
                                boolean A0i = A0i();
                                A0f(r12, new AnonymousClass0I2(path, this, f4));
                                if (A0i) {
                                    A0a(r12);
                                }
                            }
                        }
                    } else {
                        boolean z2 = true;
                        if (r1 instanceof C03450Hv) {
                            A0J();
                            C03450Hv r15 = (C03450Hv) r1;
                            A0c(r15, this.A03);
                            if (A0h()) {
                                List list = ((AbstractC03460Hw) r15).A02;
                                if (list == null || list.size() <= 0) {
                                    z2 = false;
                                }
                                boolean z3 = r14 instanceof AnonymousClass0I4;
                                float f5 = 0.0f;
                                if (z3) {
                                    if (!z2) {
                                        A02 = ((AnonymousClass0I4) r14).A00;
                                    } else {
                                        A02 = ((C08910c3) ((AbstractC03460Hw) r15).A02.get(0)).A02(this);
                                    }
                                    List list2 = ((AbstractC03460Hw) r15).A03;
                                    if (list2 == null || list2.size() == 0) {
                                        f2 = ((AnonymousClass0I4) r14).A01;
                                    } else {
                                        f2 = ((C08910c3) ((AbstractC03460Hw) r15).A03.get(0)).A03(this);
                                    }
                                    List list3 = ((AbstractC03460Hw) r15).A00;
                                    if (list3 == null || list3.size() == 0) {
                                        f3 = 0.0f;
                                    } else {
                                        f3 = ((C08910c3) ((AbstractC03460Hw) r15).A00.get(0)).A02(this);
                                    }
                                    List list4 = ((AbstractC03460Hw) r15).A01;
                                    if (!(list4 == null || list4.size() == 0)) {
                                        f5 = ((C08910c3) ((AbstractC03460Hw) r15).A01.get(0)).A03(this);
                                    }
                                    f = f5;
                                    f5 = A02;
                                } else {
                                    f = 0.0f;
                                    f2 = 0.0f;
                                    f3 = 0.0f;
                                }
                                if (z2 && (A0G = A0G()) != AnonymousClass0JO.Start) {
                                    float A082 = A08(r15);
                                    if (A0G == AnonymousClass0JO.Middle) {
                                        A082 /= 2.0f;
                                    }
                                    f5 -= A082;
                                }
                                A0Z(r15.A00);
                                if (z3) {
                                    AnonymousClass0I4 r0 = (AnonymousClass0I4) r14;
                                    r0.A00 = f5 + f3;
                                    r0.A01 = f2 + f;
                                }
                                boolean A0i2 = A0i();
                                A0f(r15, r14);
                                if (A0i2) {
                                    A0a(r15);
                                }
                            }
                        } else if (r1 instanceof C03470Hx) {
                            A0J();
                            C03470Hx r42 = (C03470Hx) r1;
                            A0c(r42, this.A03);
                            if (A0h()) {
                                A0Z(r42.A00);
                                AnonymousClass0OO A042 = r1.A01.A04(r42.A01);
                                if (A042 == null || !(A042 instanceof AbstractC03250Hb)) {
                                    A06("Tref reference '%s' not found", r42.A01);
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    A0g((AbstractC03250Hb) A042, sb);
                                    if (sb.length() > 0) {
                                        r14.A00(sb.toString());
                                    }
                                }
                            }
                        }
                    }
                    A0I();
                }
                z = false;
            }
        }
    }

    public final void A0g(AbstractC03250Hb r6, StringBuilder sb) {
        Iterator it = ((AbstractC03490Hz) r6).A01.iterator();
        boolean z = true;
        while (it.hasNext()) {
            AnonymousClass0OO r1 = (AnonymousClass0OO) it.next();
            if (r1 instanceof AbstractC03250Hb) {
                A0g((AbstractC03250Hb) r1, sb);
            } else if (r1 instanceof C03330Hj) {
                sb.append(A0H(((C03330Hj) r1).A00, z, !it.hasNext()));
            }
            z = false;
        }
    }

    public final boolean A0h() {
        Boolean bool = this.A03.A04.A0L;
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r2.A0a != null) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0i() {
        /*
            r5 = this;
            X.0S1 r0 = r5.A03
            X.0c2 r2 = r0.A04
            java.lang.Float r0 = r2.A0P
            float r1 = r0.floatValue()
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x0015
            java.lang.String r1 = r2.A0a
            r0 = 0
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            r4 = 0
            if (r0 != 0) goto L_0x001a
            return r4
        L_0x001a:
            android.graphics.Canvas r2 = r5.A01
            X.0S1 r0 = r5.A03
            X.0c2 r0 = r0.A04
            java.lang.Float r0 = r0.A0P
            float r1 = r0.floatValue()
            r0 = 1132462080(0x43800000, float:256.0)
            float r1 = r1 * r0
            int r1 = (int) r1
            r0 = 255(0xff, float:3.57E-43)
            if (r1 >= 0) goto L_0x006e
            r1 = 0
        L_0x002f:
            r0 = 31
            r3 = 0
            r2.saveLayerAlpha(r3, r1, r0)
            java.util.Stack r1 = r5.A06
            X.0S1 r0 = r5.A03
            r1.push(r0)
            X.0S1 r1 = r5.A03
            X.0S1 r0 = new X.0S1
            r0.<init>(r1, r5)
            r5.A03 = r0
            X.0c2 r0 = r0.A04
            java.lang.String r1 = r0.A0a
            r2 = 1
            if (r1 == 0) goto L_0x006d
            X.0Q5 r0 = r5.A02
            X.0OO r0 = r0.A04(r1)
            if (r0 == 0) goto L_0x0058
            boolean r0 = r0 instanceof X.C03270Hd
            if (r0 != 0) goto L_0x006d
        L_0x0058:
            java.lang.Object[] r1 = new java.lang.Object[r2]
            X.0S1 r0 = r5.A03
            X.0c2 r0 = r0.A04
            java.lang.String r0 = r0.A0a
            r1[r4] = r0
            java.lang.String r0 = "Mask reference '%s' not found"
            A06(r0, r1)
            X.0S1 r0 = r5.A03
            X.0c2 r0 = r0.A04
            r0.A0a = r3
        L_0x006d:
            return r2
        L_0x006e:
            if (r1 <= r0) goto L_0x002f
            r1 = 255(0xff, float:3.57E-43)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06540Ua.A0i():boolean");
    }

    public final boolean A0j() {
        Boolean bool = this.A03.A04.A0N;
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }
}
