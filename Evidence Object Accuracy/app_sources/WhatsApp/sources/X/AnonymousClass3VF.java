package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3VF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VF implements AbstractC116505Vs {
    public final /* synthetic */ AnonymousClass19T A00;

    public AnonymousClass3VF(AnonymousClass19T r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116505Vs
    public void AQ9(AnonymousClass28H r4, int i) {
        this.A00.A07.A0H(new RunnableBRunnable0Shape1S0201000_I1(r4, this, i, 5));
    }

    @Override // X.AbstractC116505Vs
    public void AXA(C44721zR r4, AnonymousClass28H r5) {
        this.A00.A07.A0H(new RunnableBRunnable0Shape3S0300000_I1(this, r5, r4, 9));
    }
}
