package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78323oj extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99144jp();
    public final int A00;
    public final C56492ky A01;
    public final C78463ox A02;

    public C78323oj(C56492ky r1, C78463ox r2, int i) {
        this.A00 = i;
        this.A01 = r1;
        this.A02 = r2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0B(parcel, this.A01, 2, i, false);
        C95654e8.A0B(parcel, this.A02, 3, i, false);
        C95654e8.A06(parcel, A00);
    }
}
