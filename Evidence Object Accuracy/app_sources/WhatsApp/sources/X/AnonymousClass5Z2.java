package X;

import java.util.Collection;
import java.util.Set;

/* renamed from: X.5Z2  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Z2<E> extends Collection<E> {
    int add(Object obj, int i);

    @Override // java.util.Collection, X.AnonymousClass5Z2
    boolean add(Object obj);

    @Override // java.util.Collection, X.AnonymousClass5Z2
    boolean contains(Object obj);

    @Override // java.util.Collection
    boolean containsAll(Collection collection);

    int count(Object obj);

    Set elementSet();

    Set entrySet();

    int remove(Object obj, int i);

    @Override // java.util.Collection, X.AnonymousClass5Z2
    boolean remove(Object obj);

    boolean setCount(Object obj, int i, int i2);

    @Override // java.util.Collection, X.AnonymousClass5Z2
    int size();
}
