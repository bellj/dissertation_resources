package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Locale;

/* renamed from: X.242  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass242 {
    public int A00;
    public LayoutInflater A01;
    public AbstractC05270Ox A02;
    public C58372oi A03;
    public AbstractC116745Wq A04;
    public final int A05;
    public final int A06;
    public final Context A07;
    public final AbstractC05270Ox A08 = new C54832hM(this);
    public final AbstractC05270Ox A09 = new C75113jK(this);
    public final ViewPager A0A;
    public final AnonymousClass018 A0B;

    public AnonymousClass242(Context context, ViewGroup viewGroup, AbstractC05270Ox r5, AnonymousClass018 r6, int i) {
        this.A07 = context;
        this.A0B = r6;
        this.A01 = LayoutInflater.from(context);
        ViewPager viewPager = (ViewPager) viewGroup.findViewById(i);
        this.A0A = viewPager;
        this.A02 = r5;
        this.A05 = AnonymousClass00T.A00(context, R.color.emoji_popup_body);
        this.A06 = AnonymousClass00T.A00(context, R.color.paletteElevationOverlay);
        viewPager.A0G(new AnonymousClass3S7(r6, this));
    }

    public int A00() {
        ViewPager viewPager;
        int length;
        AnonymousClass018 r7 = this.A0B;
        if (!r7.A04().A06) {
            viewPager = this.A0A;
            length = viewPager.getCurrentItem();
        } else {
            viewPager = this.A0A;
            length = (this.A03.A01.length - 1) - viewPager.getCurrentItem();
        }
        if (length < 0) {
            Log.i(String.format(Locale.US, "ContentPicker/getCurrentPageIndex < 0, isLtr: %s, pagerAdapter.getCount(): %d, viewPager.getCurrentItem(): %d", Boolean.valueOf(!r7.A04().A06), Integer.valueOf(this.A03.A01.length), Integer.valueOf(viewPager.getCurrentItem())));
        }
        return length;
    }

    public void A01(int i) {
        AnonymousClass35q r1;
        C622235r r12;
        if (this instanceof AnonymousClass341) {
            AnonymousClass341 r4 = (AnonymousClass341) this;
            AbstractC69213Yj r6 = (AbstractC69213Yj) r4.A0I.get(i);
            r6.A06 = true;
            C54392ge r13 = r6.A05;
            if (r13 != null) {
                r13.A04 = true;
                r13.A00 = 2;
                r13.A02();
            }
            AbstractC69213Yj r14 = r4.A0E;
            if (!(r14 == null || r14 == r6)) {
                r14.A06 = false;
                C54392ge r15 = r14.A05;
                if (r15 != null) {
                    r15.A04 = false;
                    r15.A00 = 1;
                    r15.A02();
                }
            }
            r4.A0E = r6;
            if (r6 instanceof C622335s) {
                AnonymousClass1KZ r5 = ((C622335s) r6).A04;
                r5.A07 = false;
                C235512c r3 = r4.A0Y;
                r3.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r3, 3, r5));
            }
            if (!(r6.getId().equals("recents") || (r12 = r4.A0C) == null || ((AbstractC69213Yj) r12).A04 == null)) {
                r12.A01();
            }
            if (!r6.getId().equals("starred") && (r1 = r4.A0D) != null && ((AbstractC69213Yj) r1).A04 != null) {
                r1.A01();
            }
        }
    }

    public void A02(int i, boolean z) {
        int length;
        if (!this.A0B.A04().A06) {
            length = i;
        } else {
            length = (this.A03.A01.length - 1) - i;
        }
        if (length < 0) {
            Log.i(String.format(Locale.US, "ContentPicker/selectPageByIndex/absoluteIndex < 0, pagerAdapter.getCount(): %d, index: %d", Integer.valueOf(this.A03.A01.length), Integer.valueOf(i)));
        }
        C58372oi r1 = this.A03;
        if (r1 != null && i >= 0 && i < r1.A01.length && this.A00 != length) {
            this.A0A.A0F(length, z);
        }
    }

    public void A03(C58372oi r4) {
        this.A03 = r4;
        AbstractC05270Ox r2 = this.A08;
        HashSet hashSet = r4.A05;
        if (!hashSet.contains(r2)) {
            hashSet.add(r2);
        }
        C58372oi r0 = this.A03;
        AbstractC05270Ox r22 = this.A09;
        HashSet hashSet2 = r0.A05;
        if (!hashSet2.contains(r22)) {
            hashSet2.add(r22);
        }
        this.A0A.setAdapter(this.A03);
    }
}
