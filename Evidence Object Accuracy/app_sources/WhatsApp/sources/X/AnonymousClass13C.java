package X;

import android.util.Base64;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.13C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13C {
    public final C15520nO A00;

    public AnonymousClass13C(C15520nO r1) {
        this.A00 = r1;
    }

    public static byte[] A00(byte[] bArr, byte[] bArr2) {
        boolean z = true;
        byte[] A0H = C003501n.A0H(bArr, bArr2);
        if (A0H.length < 20) {
            z = false;
        }
        AnonymousClass009.A0E(z);
        byte[] bArr3 = new byte[20];
        System.arraycopy(A0H, 0, bArr3, 0, 20);
        return bArr3;
    }

    public static byte[] A01(byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr3, "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            Cipher cipher = (Cipher) AnonymousClass3IF.A00(instance, "AES/CBC/PKCS5Padding", instance.getProvider().getName());
            cipher.init(i, secretKeySpec, ivParameterSpec);
            return cipher.doFinal(bArr2);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new SecurityException(e);
        }
    }

    public String A02(C15430nF r9, String str) {
        if (r9.A03) {
            byte[] decode = Base64.decode(str, 11);
            try {
                byte[][] A07 = C16050oM.A07(decode, 20, 16, (decode.length - 20) - 16);
                byte[] bArr = A07[0];
                byte[] bArr2 = A07[1];
                byte[] bArr3 = A07[2];
                byte[] A04 = A04(r9);
                if (Arrays.equals(bArr, A00(A04, C16050oM.A05(bArr2, bArr3)))) {
                    return new String(A01(bArr2, bArr3, A04, 2));
                }
                throw new SecurityException("Data mac corrupt");
            } catch (ParseException e) {
                throw new SecurityException(e);
            }
        } else {
            throw new SecurityException("Caller isn't trusted");
        }
    }

    public String A03(C15430nF r8, String str) {
        if (r8.A03) {
            byte[] A04 = A04(r8);
            byte[] A00 = A00(A04, str.getBytes());
            boolean z = false;
            if (A00.length >= 16) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            byte[] bArr = new byte[16];
            System.arraycopy(A00, 0, bArr, 0, 16);
            byte[] A05 = C16050oM.A05(bArr, A01(bArr, str.getBytes(), A04, 1));
            return Base64.encodeToString(C16050oM.A05(A00(A04, A05), A05), 11);
        }
        throw new SecurityException("Caller isn't trusted");
    }

    public final synchronized byte[] A04(C15430nF r8) {
        String string;
        C15520nO r4 = this.A00;
        String str = r8.A01;
        string = r4.A01().getString(C15520nO.A00(str, "auth/encryption_key"), null);
        if (string == null) {
            try {
                KeyGenerator instance = KeyGenerator.getInstance("AES");
                KeyGenerator keyGenerator = (KeyGenerator) AnonymousClass3IF.A00(instance, "AES", instance.getProvider().getName());
                SecureRandom A01 = AnonymousClass3IF.A01();
                byte[] encoded = keyGenerator.generateKey().getEncoded();
                keyGenerator.init(256, A01);
                string = Base64.encodeToString(encoded, 11);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                string = null;
            }
            r4.A01().edit().putString(C15520nO.A00(str, "auth/encryption_key"), string).apply();
        }
        return Base64.decode(string, 11);
    }
}
