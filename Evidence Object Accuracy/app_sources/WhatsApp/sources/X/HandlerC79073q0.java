package X;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.concurrent.locks.Lock;

/* renamed from: X.3q0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class HandlerC79073q0 extends HandlerC472529t {
    public final /* synthetic */ C77733nl A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC79073q0(Looper looper, C77733nl r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            C77733nl r2 = this.A00;
            Lock lock = r2.A0K;
            lock.lock();
            try {
                if (r2.A0E()) {
                    r2.A0D();
                }
            } finally {
                lock.unlock();
            }
        } else if (i != 2) {
            Log.w("GoogleApiClientImpl", C12960it.A0e("Unknown message id: ", C12980iv.A0t(31), i));
        } else {
            C77733nl.A01(this.A00);
        }
    }
}
