package X;

/* renamed from: X.437  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass437 extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass437() {
        super(2512, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdAppStateFirstCompanionRegistration {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeBetweenLastCompanionDeregistrationAndFirstCompanionRegistration", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
