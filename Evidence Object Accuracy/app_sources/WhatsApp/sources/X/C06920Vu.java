package X;

import android.transition.Transition;

/* renamed from: X.0Vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06920Vu implements Transition.TransitionListener {
    public final /* synthetic */ AnonymousClass0EI A00;
    public final /* synthetic */ Runnable A01;

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionCancel(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionPause(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionResume(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
    }

    public C06920Vu(AnonymousClass0EI r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        this.A01.run();
    }
}
