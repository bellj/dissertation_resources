package X;

import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreTabFragment;

/* renamed from: X.3NR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NR implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ StickerStoreTabFragment A00;

    public AnonymousClass3NR(StickerStoreTabFragment stickerStoreTabFragment) {
        this.A00 = stickerStoreTabFragment;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        StickerStoreTabFragment stickerStoreTabFragment = this.A00;
        AnonymousClass03U A0C = stickerStoreTabFragment.A04.A0C(stickerStoreTabFragment.A03.A1A());
        if (A0C instanceof C55132hq) {
            RecyclerView recyclerView = ((C55132hq) A0C).A0G;
            int min = Math.min(recyclerView.getWidth() / (C12960it.A09(recyclerView).getDimensionPixelSize(R.dimen.sticker_store_row_preview_item) + stickerStoreTabFragment.A02().getDimensionPixelSize(R.dimen.sticker_store_row_preview_padding)), 5);
            if (stickerStoreTabFragment.A00 != min) {
                stickerStoreTabFragment.A00 = min;
                stickerStoreTabFragment.A0D.A02();
            }
        }
    }
}
