package X;

import android.os.Bundle;

/* renamed from: X.058  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass058 {
    public Bundle A00;
    public AnonymousClass03E A01 = new AnonymousClass03E();
    public C019108z A02;
    public boolean A03 = true;
    public boolean A04;

    public Bundle A00(String str) {
        if (this.A04) {
            Bundle bundle = this.A00;
            if (bundle == null) {
                return null;
            }
            Bundle bundle2 = bundle.getBundle(str);
            this.A00.remove(str);
            if (this.A00.isEmpty()) {
                this.A00 = null;
            }
            return bundle2;
        }
        throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component");
    }

    public void A01() {
        if (this.A03) {
            if (this.A02 == null) {
                this.A02 = new C019108z(this);
            }
            try {
                C018908x.class.getDeclaredConstructor(new Class[0]);
                C019108z r0 = this.A02;
                r0.A00.add(C018908x.class.getName());
            } catch (NoSuchMethodException e) {
                StringBuilder sb = new StringBuilder("Class");
                sb.append(C018908x.class.getSimpleName());
                sb.append(" must have default constructor in order to be automatically recreated");
                throw new IllegalArgumentException(sb.toString(), e);
            }
        } else {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public void A02(AnonymousClass05A r3, String str) {
        if (this.A01.A02(str, r3) != null) {
            throw new IllegalArgumentException("SavedStateProvider with the given key is already registered");
        }
    }
}
