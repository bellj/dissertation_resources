package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.53U  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass53U implements AnonymousClass1W3 {
    public final /* synthetic */ C90864Pn A00;
    public final /* synthetic */ AnonymousClass2EH A01;

    public AnonymousClass53U(C90864Pn r1, AnonymousClass2EH r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        this.A01.APB(userJid);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        this.A01.A02(this.A00);
    }
}
