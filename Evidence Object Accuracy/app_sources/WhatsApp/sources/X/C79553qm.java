package X;

import android.os.IBinder;
import com.google.android.gms.dynamic.IObjectWrapper;

/* renamed from: X.3qm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79553qm extends C98394ic implements IObjectWrapper {
    public C79553qm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
    }
}
