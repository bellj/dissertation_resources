package X;

import android.content.DialogInterface;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;

/* renamed from: X.3K6  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3K6 implements DialogInterface.OnClickListener {
    public final /* synthetic */ CallsHistoryFragment.ClearCallLogDialogFragment A00;

    public /* synthetic */ AnonymousClass3K6(CallsHistoryFragment.ClearCallLogDialogFragment clearCallLogDialogFragment) {
        this.A00 = clearCallLogDialogFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        CallsHistoryFragment.ClearCallLogDialogFragment clearCallLogDialogFragment = this.A00;
        ProgressDialogFragment A00 = ProgressDialogFragment.A00(R.string.processing, R.string.register_wait_message);
        A00.A1F(clearCallLogDialogFragment.A0H, null);
        clearCallLogDialogFragment.A04.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(clearCallLogDialogFragment, 33, A00));
    }
}
