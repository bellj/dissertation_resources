package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.1AJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AJ {
    public final Handler A00;
    public final Executor A01;

    public AnonymousClass1AJ() {
        Handler handler = new Handler(Looper.getMainLooper());
        this.A00 = handler;
        this.A01 = new AnonymousClass5E1(handler);
    }
}
