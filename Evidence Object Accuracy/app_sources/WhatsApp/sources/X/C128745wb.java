package X;

/* renamed from: X.5wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128745wb {
    public final C17070qD A00;
    public final AnonymousClass61F A01;

    public C128745wb(C17070qD r1, AnonymousClass61F r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0051, code lost:
        if (r1.equals(r0) != false) goto L_0x0085;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Class A00(android.os.Bundle r5) {
        /*
            r4 = this;
            java.lang.Class<com.whatsapp.payments.ui.NoviPayBloksActivity> r3 = com.whatsapp.payments.ui.NoviPayBloksActivity.class
            java.lang.String r0 = "nfm_action"
            java.lang.String r1 = r5.getString(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            r2 = 0
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "[PAY]: NoviPayNFMController -- NFM action not passed"
        L_0x0011:
            com.whatsapp.util.Log.e(r0)
            return r2
        L_0x0015:
            int r0 = r1.hashCode()
            switch(r0) {
                case -1160928146: goto L_0x006a;
                case -526565947: goto L_0x005f;
                case -278171023: goto L_0x0054;
                case 1543993918: goto L_0x004b;
                case 1585331439: goto L_0x0040;
                case 1598227708: goto L_0x003d;
                case 1642412650: goto L_0x002a;
                case 2109082526: goto L_0x0027;
                default: goto L_0x001c;
            }
        L_0x001c:
            java.lang.String r0 = "[PAY]: NoviPayNFMController -- Unsupported NFM action: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r1, r0)
            goto L_0x0011
        L_0x0027:
            java.lang.String r0 = "novi_login"
            goto L_0x004d
        L_0x002a:
            java.lang.String r0 = "novi_hub"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001c
            X.61F r0 = r4.A01
            boolean r0 = r0.A0D()
            if (r0 == 0) goto L_0x0085
            java.lang.Class<com.whatsapp.payments.ui.NoviPayHubActivity> r0 = com.whatsapp.payments.ui.NoviPayHubActivity.class
            return r0
        L_0x003d:
            java.lang.String r0 = "novi_view_code"
            goto L_0x0042
        L_0x0040:
            java.lang.String r0 = "novi_view_transaction"
        L_0x0042:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001c
            java.lang.Class<com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity> r0 = com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity.class
            return r0
        L_0x004b:
            java.lang.String r0 = "novi_report_transaction"
        L_0x004d:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0085
            goto L_0x001c
        L_0x0054:
            java.lang.String r0 = "novi_view_card_detail"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001c
            java.lang.Class<com.whatsapp.payments.ui.NoviPaymentCardDetailsActivity> r0 = com.whatsapp.payments.ui.NoviPaymentCardDetailsActivity.class
            return r0
        L_0x005f:
            java.lang.String r0 = "novi_view_bank_detail"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001c
            java.lang.Class<com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity> r0 = com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity.class
            return r0
        L_0x006a:
            java.lang.String r0 = "novi_tpp_complete_transaction"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001c
            X.61F r0 = r4.A01
            boolean r0 = r0.A0H()
            if (r0 == 0) goto L_0x0085
            X.0qD r0 = r4.A00
            X.0pp r0 = r0.A02()
            java.lang.Class r0 = r0.AGb()
            return r0
        L_0x0085:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C128745wb.A00(android.os.Bundle):java.lang.Class");
    }
}
