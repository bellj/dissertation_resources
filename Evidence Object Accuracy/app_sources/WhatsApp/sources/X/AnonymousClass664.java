package X;

import android.graphics.Point;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;

/* renamed from: X.664  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass664 implements AbstractC136156Lf {
    public final /* synthetic */ AnonymousClass6KT A00;

    public AnonymousClass664(AnonymousClass6KT r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136156Lf
    public void AQe(Point point, EnumC124565pk r8) {
        long j;
        AnonymousClass661 r5 = this.A00.A01;
        if (!r5.A0e) {
            if (r5.A0D) {
                j = 4000;
            } else {
                j = 2000;
            }
            synchronized (r5) {
                r5.A09();
                r5.A0b = r5.A0T.A02("reset_focus", new IDxCallableShape15S0100000_3_I1(r5, 3), j);
            }
        }
    }
}
