package X;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.lang.reflect.Method;

/* renamed from: X.0zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22670zS {
    public Boolean A00;
    public final AnonymousClass21I A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C14850m9 A04;

    public C22670zS(C14830m7 r3, C16590pI r4, C14820m6 r5, C14850m9 r6) {
        AnonymousClass21I r0;
        this.A02 = r3;
        this.A04 = r6;
        this.A03 = r5;
        boolean A07 = r6.A07(266);
        Context context = r4.A00;
        if (A07) {
            r0 = new AnonymousClass21H(context);
        } else {
            r0 = new AnonymousClass21J(context);
        }
        this.A01 = r0;
    }

    public void A00(Activity activity) {
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                Method method = Activity.class.getMethod("setDisablePreviewScreenshots", Boolean.TYPE);
                method.setAccessible(true);
                method.invoke(activity, Boolean.valueOf(A03()));
            } catch (Exception e) {
                Log.e("Could not invoke setDisablePreviewScreenshots()", e);
            }
        }
    }

    public void A01(boolean z) {
        Boolean bool = this.A00;
        if (bool == null || bool.booleanValue() != z) {
            this.A00 = Boolean.valueOf(z);
            StringBuilder sb = new StringBuilder("AppAuthManager/setIsAuthenticationNeeded: ");
            sb.append(z);
            Log.i(sb.toString());
            this.A03.A00.edit().putBoolean("fingerprint_authentication_needed", z).apply();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r3.A01.AIC() == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02() {
        /*
            r3 = this;
            boolean r0 = r3.A04()
            if (r0 == 0) goto L_0x000f
            X.21I r0 = r3.A01
            boolean r0 = r0.AIC()
            r2 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r2 = 0
        L_0x0010:
            java.lang.String r1 = "AppAuthManager/hasEnrolledBiometrics: enrolled: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22670zS.A02():boolean");
    }

    public boolean A03() {
        return Build.VERSION.SDK_INT >= 23 && this.A03.A00.getBoolean("privacy_fingerprint_enabled", false) && this.A01.A6u();
    }

    public boolean A04() {
        return Build.VERSION.SDK_INT >= 23 && this.A01.AJT();
    }

    public boolean A05() {
        SharedPreferences sharedPreferences = this.A03.A00;
        boolean z = sharedPreferences.getBoolean("privacy_fingerprint_enabled", false);
        boolean z2 = sharedPreferences.getBoolean("fingerprint_authentication_needed", false);
        boolean A02 = A02();
        if (!A02 || !z || !z2) {
            StringBuilder sb = new StringBuilder("AppAuthManager/shouldShowAuthPrompt: No prompt: ");
            sb.append(!A02);
            sb.append(" || ");
            sb.append(!z);
            sb.append(" || ");
            sb.append(!z2);
            Log.i(sb.toString());
            return false;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = sharedPreferences.getLong("app_background_time", 0);
        long j2 = sharedPreferences.getLong("privacy_fingerprint_timeout", 60000);
        StringBuilder sb2 = new StringBuilder("AppAuthManager/shouldShowAuthPrompt: show prompt if necessary: ");
        long j3 = j + j2;
        boolean z3 = false;
        if (j3 < elapsedRealtime) {
            z3 = true;
        }
        sb2.append(z3);
        Log.i(sb2.toString());
        if (j3 < elapsedRealtime) {
            return true;
        }
        return false;
    }

    public boolean A06() {
        return !A03() || this.A03.A00.getBoolean("privacy_fingerprint_show_notification_content", true);
    }
}
