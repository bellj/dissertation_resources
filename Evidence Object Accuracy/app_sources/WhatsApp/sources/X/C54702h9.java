package X;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2h9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54702h9 extends AbstractC05270Ox {
    public final /* synthetic */ AbstractActivityC37081lH A00;

    public C54702h9(AbstractActivityC37081lH r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        C44711zQ A02;
        if (recyclerView.A0B != 0 && C12980iv.A0A((LinearLayoutManager) recyclerView.getLayoutManager()) <= 4) {
            AbstractActivityC37081lH r0 = this.A00;
            C53842fM r5 = r0.A0F;
            UserJid userJid = r0.A0J;
            boolean A0F = r5.A0B.A0F(userJid);
            C14850m9 r1 = r5.A0L;
            int i3 = 582;
            if (A0F) {
                i3 = 451;
            }
            if (!r1.A07(i3) || ((A02 = r5.A0C.A02(userJid)) != null && !A02.A01)) {
                AnonymousClass19T r3 = r5.A0G;
                int i4 = r5.A04;
                int i5 = 1;
                if (r3.A08.A0F(userJid)) {
                    i5 = 4;
                }
                r3.A05(userJid, i4, i5 * 6, false);
            } else {
                AnonymousClass19T r32 = r5.A0G;
                r32.A04(userJid, r5.A04, C12980iv.A03(r32.A08.A0F(userJid) ? 1 : 0) << 2, false);
            }
            recyclerView.post(new RunnableBRunnable0Shape10S0200000_I1(this, 45, recyclerView));
        }
    }
}
