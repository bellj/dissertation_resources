package X;

/* renamed from: X.43d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855243d extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;

    public C855243d() {
        super(1676, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(4, this.A02);
        r3.Abe(2, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusAvailableCountDaily", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusAvailableRowsCountDaily", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusViewedCountDaily", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusViewedRowsCountDaily", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
