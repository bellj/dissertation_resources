package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.widget.TextView;

/* renamed from: X.3a7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69713a7 implements AbstractC116215Uo {
    @Override // X.AbstractC116215Uo
    public Layout A8K(TextPaint textPaint, TextView textView, CharSequence charSequence, int i) {
        return StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), textPaint, i).setAlignment(Layout.Alignment.ALIGN_CENTER).setBreakStrategy(textView.getBreakStrategy()).setHyphenationFrequency(textView.getHyphenationFrequency()).setUseLineSpacingFromFallbacks(textView.isFallbackLineSpacing()).build();
    }
}
