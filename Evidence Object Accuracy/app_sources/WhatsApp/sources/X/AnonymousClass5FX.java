package X;

/* renamed from: X.5FX  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5FX implements AbstractC117195Yx {
    public int A00;
    public long A01;
    public C94484bt A02;
    public boolean A03;
    public final C10730f6 A04;
    public final AbstractC02750Dv A05;

    public AnonymousClass5FX(AbstractC02750Dv r2) {
        this.A05 = r2;
        C10730f6 AB0 = r2.AB0();
        this.A04 = AB0;
        C94484bt r0 = AB0.A01;
        this.A02 = r0;
        this.A00 = r0 != null ? r0.A01 : -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006c, code lost:
        if (r0 <= 0) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006e, code lost:
        if (r4 != null) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0074, code lost:
        throw X.C72453ed.A0m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0075, code lost:
        r5 = r4.A01();
        r3 = r5.A01 + ((int) r10);
        r5.A01 = r3;
        r5.A00 = java.lang.Math.min(r3 + ((int) r0), r5.A00);
        r2 = r15.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008b, code lost:
        if (r2 != null) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008d, code lost:
        r5.A03 = r5;
        r5.A02 = r5;
        r15.A01 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0093, code lost:
        r0 = r0 - ((long) (r5.A00 - r5.A01));
        r4 = r4.A02;
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009f, code lost:
        r2 = r2.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a1, code lost:
        if (r2 != null) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a7, code lost:
        throw X.C72453ed.A0m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a8, code lost:
        r2.A04(r5);
     */
    @Override // X.AbstractC117195Yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AZp(X.C10730f6 r15, long r16) {
        /*
            r14 = this;
            boolean r1 = r14.A03
            r0 = 1
            r1 = r1 ^ r0
            if (r1 == 0) goto L_0x00b9
            X.4bt r1 = r14.A02
            if (r1 == 0) goto L_0x0016
            X.0f6 r0 = r14.A04
            X.4bt r0 = r0.A01
            if (r1 != r0) goto L_0x00b2
            int r1 = r14.A00
            int r0 = r0.A01
            if (r1 != r0) goto L_0x00b2
        L_0x0016:
            X.0Dv r4 = r14.A05
            long r0 = r14.A01
            r2 = r16
            long r0 = r0 + r16
            r4.AaY(r0)
            X.4bt r0 = r14.A02
            if (r0 != 0) goto L_0x0031
            X.0f6 r0 = r14.A04
            X.4bt r0 = r0.A01
            if (r0 == 0) goto L_0x0031
            r14.A02 = r0
            int r0 = r0.A01
            r14.A00 = r0
        L_0x0031:
            X.0f6 r4 = r14.A04
            long r8 = r4.A00
            long r10 = r14.A01
            long r0 = r8 - r10
            long r12 = java.lang.Math.min(r2, r0)
            r6 = 0
            int r0 = (r12 > r6 ? 1 : (r12 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x0046
            r0 = -1
            return r0
        L_0x0046:
            r0 = r12
            X.AnonymousClass4F1.A00(r8, r10, r12)
            int r2 = (r12 > r6 ? 1 : (r12 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x00ac
            long r2 = r15.A00
            long r2 = r2 + r12
            r15.A00 = r2
            X.4bt r4 = r4.A01
        L_0x0055:
            if (r4 != 0) goto L_0x005c
            java.lang.RuntimeException r0 = X.C72453ed.A0m()
            throw r0
        L_0x005c:
            int r3 = r4.A00
            int r2 = r4.A01
            int r3 = r3 - r2
            long r2 = (long) r3
            int r5 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r5 < 0) goto L_0x006a
            long r10 = r10 - r2
            X.4bt r4 = r4.A02
            goto L_0x0055
        L_0x006a:
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x00ac
            if (r4 != 0) goto L_0x0075
            java.lang.RuntimeException r0 = X.C72453ed.A0m()
            throw r0
        L_0x0075:
            X.4bt r5 = r4.A01()
            int r3 = r5.A01
            int r2 = (int) r10
            int r3 = r3 + r2
            r5.A01 = r3
            int r2 = (int) r0
            int r3 = r3 + r2
            int r2 = r5.A00
            int r2 = java.lang.Math.min(r3, r2)
            r5.A00 = r2
            X.4bt r2 = r15.A01
            if (r2 != 0) goto L_0x009f
            r5.A03 = r5
            r5.A02 = r5
            r15.A01 = r5
        L_0x0093:
            int r3 = r5.A00
            int r2 = r5.A01
            int r3 = r3 - r2
            long r2 = (long) r3
            long r0 = r0 - r2
            X.4bt r4 = r4.A02
            r10 = 0
            goto L_0x006a
        L_0x009f:
            X.4bt r2 = r2.A03
            if (r2 != 0) goto L_0x00a8
            java.lang.RuntimeException r0 = X.C72453ed.A0m()
            throw r0
        L_0x00a8:
            r2.A04(r5)
            goto L_0x0093
        L_0x00ac:
            long r0 = r14.A01
            long r0 = r0 + r12
            r14.A01 = r0
            return r12
        L_0x00b2:
            java.lang.String r0 = "Peek source is invalid because upstream source was used"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x00b9:
            java.lang.String r0 = "closed"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5FX.AZp(X.0f6, long):long");
    }

    @Override // X.AbstractC117195Yx, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        this.A03 = true;
    }
}
