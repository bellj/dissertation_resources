package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1cT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32631cT implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100144lR();
    public final AbstractC460224d A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C32631cT(AbstractC460224d r1, String str, String str2, String str3, boolean z) {
        this.A01 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A00 = r1;
        this.A03 = str3;
    }

    public /* synthetic */ C32631cT(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
        this.A04 = parcel.readInt() != 1 ? false : true;
        this.A00 = (AbstractC460224d) parcel.readParcelable(AbstractC460224d.class.getClassLoader());
        this.A03 = parcel.readString();
    }

    public static C32631cT A00(AnonymousClass1V8 r9) {
        AbstractC460224d r4;
        try {
            String A0H = r9.A0H("step_up_id");
            String A0H2 = r9.A0H("service");
            boolean z = false;
            if (r9.A05("sticky_service_hub_cta", 1) == 1) {
                z = true;
            }
            String A0I = r9.A0I("step_up_reason", null);
            AnonymousClass1V8 A0F = r9.A0F("step_up_challenge");
            String A0H3 = A0F.A0H("challenge_id");
            AnonymousClass1V8 A0C = A0F.A0C();
            String str = A0C.A00;
            if (str.equals("webview")) {
                try {
                    boolean z2 = true;
                    if (A0C.A05("auth_required", 1) != 1) {
                        z2 = false;
                    }
                    r4 = new C460324e(A0H3, z2);
                } catch (AnonymousClass1V9 e) {
                    Log.e("PAY: Can't build WebViewChallenge ", e);
                }
            } else {
                if (str.equals("document_upload")) {
                    r4 = new AnonymousClass46U(A0H3);
                }
                r4 = null;
            }
            return new C32631cT(r4, A0H2, A0H, A0I, z);
        } catch (AnonymousClass1V9 e2) {
            Log.e("PAY: PaymentStepUpInfo/fromProtocolTreeNode ", e2);
            return null;
        }
    }

    public static C32631cT A01(String str) {
        AbstractC460224d r4;
        String string;
        if (!AnonymousClass1US.A0C(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string2 = jSONObject.getString("service");
                String string3 = jSONObject.getString("step_up_id");
                boolean optBoolean = jSONObject.optBoolean("sticky_service_hub_cta", true);
                JSONObject jSONObject2 = jSONObject.getJSONObject("step_up_challenge");
                try {
                    string = jSONObject2.getString("type");
                } catch (JSONException e) {
                    Log.e("PAY: PaymentStepUpChallenge fromJsonObject threw exception ", e);
                }
                if (string.equals("WEBVIEW")) {
                    try {
                        r4 = new C460324e(jSONObject2.getString("challenge_id"), jSONObject2.getBoolean("auth_required"));
                    } catch (JSONException e2) {
                        Log.e("PAY: WebViewChallenge fromJsonObject threw exception ", e2);
                    }
                } else {
                    if (string.equals("DOC_UPLOAD")) {
                        try {
                            r4 = new AnonymousClass46U(jSONObject2.getString("challenge_id"));
                        } catch (JSONException e3) {
                            Log.e("PAY: DocumentUploadChallenge fromJsonObject threw exception ", e3);
                        }
                    }
                    r4 = null;
                }
                return new C32631cT(r4, string2, string3, jSONObject.optString("step_up_reason", null), optBoolean);
            } catch (JSONException e4) {
                Log.e("PAY: PaymentStepUpInfo fromJsonString threw exception ", e4);
            }
        }
        return null;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A04 ? 1 : 0);
        parcel.writeParcelable(this.A00, 0);
        String str = this.A03;
        if (str != null) {
            parcel.writeString(str);
        }
    }
}
