package X;

import android.content.SharedPreferences;

/* renamed from: X.0s0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18160s0 {
    public final C16630pM A00;
    public final AbstractC16710pd A01 = AnonymousClass4Yq.A00(new C42741vk(this));

    public C18160s0(C16630pM r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public final Boolean A00() {
        AbstractC16710pd r1 = this.A01;
        Object value = r1.getValue();
        C16700pc.A0B(value);
        if (!((SharedPreferences) value).contains("pref_has_avatar_config")) {
            return null;
        }
        Object value2 = r1.getValue();
        C16700pc.A0B(value2);
        return Boolean.valueOf(((SharedPreferences) value2).getBoolean("pref_has_avatar_config", false));
    }

    public final String A01() {
        Object value = this.A01.getValue();
        C16700pc.A0B(value);
        return ((SharedPreferences) value).getString("pref_avatar_access_token", null);
    }

    public final void A02(boolean z) {
        Object value = this.A01.getValue();
        C16700pc.A0B(value);
        ((SharedPreferences) value).edit().putBoolean("pref_has_avatar_config", z).apply();
    }
}
