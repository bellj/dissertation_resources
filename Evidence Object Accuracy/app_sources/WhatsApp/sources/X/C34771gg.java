package X;

import android.text.TextUtils;
import android.util.Base64;

/* renamed from: X.1gg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34771gg extends AbstractC30271Wt {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public byte[] A0H;

    public C34771gg(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 35, j);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r8) {
        AnonymousClass4BZ r2;
        AnonymousClass1G3 r4 = r8.A03;
        C57692nT r0 = ((C27081Fy) r4.A00).A0W;
        if (r0 == null) {
            r0 = C57692nT.A0D;
        }
        C56882m6 r3 = (C56882m6) r0.A0T();
        C57612nL r02 = ((C57692nT) r3.A00).A08;
        if (r02 == null) {
            r02 = C57612nL.A0A;
        }
        AnonymousClass1G4 A0T = r02.A0T();
        long j = this.A05;
        A0T.A03();
        C57612nL r6 = (C57612nL) A0T.A00;
        r6.A00 |= 2;
        r6.A04 = j;
        int i = this.A03;
        if (i == 0) {
            r2 = AnonymousClass4BZ.A02;
        } else if (i == 1) {
            r2 = AnonymousClass4BZ.A03;
        } else if (i == 2) {
            r2 = AnonymousClass4BZ.A06;
        } else if (i == 3) {
            r2 = AnonymousClass4BZ.A01;
        } else if (i == 4) {
            r2 = AnonymousClass4BZ.A05;
        } else if (i == 5) {
            r2 = AnonymousClass4BZ.A04;
        } else {
            StringBuilder sb = new StringBuilder("Unexpected type (");
            sb.append(i);
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        A0T.A03();
        C57612nL r1 = (C57612nL) A0T.A00;
        r1.A00 |= 32;
        r1.A03 = r2.value;
        int i2 = this.A00;
        if (i2 > 0) {
            A0T.A03();
            C57612nL r12 = (C57612nL) A0T.A00;
            r12.A00 |= 64;
            r12.A01 = i2;
        }
        String str = this.A0B;
        if (str != null) {
            A0T.A03();
            C57612nL r13 = (C57612nL) A0T.A00;
            r13.A00 |= 16;
            r13.A08 = str;
        }
        String str2 = this.A0D;
        if (str2 != null) {
            byte[] decode = Base64.decode(str2, 0);
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(decode, 0, decode.length);
            A0T.A03();
            C57612nL r14 = (C57612nL) A0T.A00;
            r14.A00 |= 1;
            r14.A06 = A01;
        }
        String str3 = this.A0C;
        if (str3 != null) {
            byte[] decode2 = Base64.decode(str3, 0);
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(decode2, 0, decode2.length);
            A0T.A03();
            C57612nL r15 = (C57612nL) A0T.A00;
            r15.A00 |= 8;
            r15.A05 = A012;
        }
        byte[] bArr = this.A0H;
        if (bArr != null) {
            AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
            A0T.A03();
            C57612nL r16 = (C57612nL) A0T.A00;
            r16.A00 |= 4;
            r16.A07 = A013;
        }
        if (!TextUtils.isEmpty(this.A0E)) {
            String str4 = this.A0E;
            A0T.A03();
            C57612nL r17 = (C57612nL) A0T.A00;
            r17.A00 |= 128;
            r17.A09 = str4;
        }
        int i3 = this.A03;
        if (i3 == 2 || i3 == 3) {
            int i4 = this.A01;
            A0T.A03();
            C57612nL r18 = (C57612nL) A0T.A00;
            r18.A00 |= 256;
            r18.A02 = i4;
        }
        r3.A05(EnumC87324Bb.A06);
        r3.A03();
        C57692nT r19 = (C57692nT) r3.A00;
        r19.A08 = (C57612nL) A0T.A02();
        r19.A00 |= 16;
        r4.A08(r3);
    }
}
