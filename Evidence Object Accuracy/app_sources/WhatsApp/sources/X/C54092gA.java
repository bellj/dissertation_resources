package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.polls.PollCreatorActivity;
import com.whatsapp.polls.PollCreatorViewModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2gA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54092gA extends AnonymousClass0Eu {
    public final /* synthetic */ PollCreatorActivity A00;

    public C54092gA(PollCreatorActivity pollCreatorActivity) {
        this.A00 = pollCreatorActivity;
    }

    @Override // X.AnonymousClass0Eu, X.AbstractC06200So
    public int A01(AnonymousClass03U r2, RecyclerView recyclerView) {
        if (r2 instanceof AnonymousClass348) {
            return 0;
        }
        return super.A01(r2, recyclerView);
    }

    @Override // X.AbstractC06200So
    public void A03(AnonymousClass03U r4, int i) {
        if (i == 2 && r4 != null) {
            this.A00.A01.hideSoftInputFromWindow(r4.A0H.getWindowToken(), 0);
        }
    }

    @Override // X.AbstractC06200So
    public boolean A06(AnonymousClass03U r2, AnonymousClass03U r3, RecyclerView recyclerView) {
        return !(r3 instanceof AnonymousClass348);
    }

    @Override // X.AbstractC06200So
    public boolean A07(AnonymousClass03U r7, AnonymousClass03U r8, RecyclerView recyclerView) {
        int A00 = r7.A00() - 1;
        int A002 = r8.A00() - 1;
        PollCreatorActivity pollCreatorActivity = this.A00;
        PollCreatorViewModel pollCreatorViewModel = pollCreatorActivity.A06;
        if (A00 == A002 || A00 < 0) {
            return false;
        }
        List list = pollCreatorViewModel.A0F;
        if (A00 >= list.size() || A002 < 0 || A002 >= list.size()) {
            return false;
        }
        ArrayList A0x = C12980iv.A0x(list);
        Collections.swap(A0x, A00, A002);
        list.clear();
        list.addAll(A0x);
        pollCreatorViewModel.A04();
        pollCreatorActivity.A00.vibrate(3);
        return true;
    }
}
