package X;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;

/* renamed from: X.4j4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98674j4 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        PendingIntent pendingIntent = null;
        C56492ky r5 = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i2 = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c == 3) {
                pendingIntent = (PendingIntent) C95664e9.A07(parcel, PendingIntent.CREATOR, readInt);
            } else if (c == 4) {
                r5 = (C56492ky) C95664e9.A07(parcel, C56492ky.CREATOR, readInt);
            } else if (c != 1000) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new Status(pendingIntent, r5, str, i, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new Status[i];
    }
}
