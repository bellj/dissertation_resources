package X;

/* renamed from: X.4Wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92664Wv {
    public String A00;
    public String A01;

    public C92664Wv(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92664Wv r5 = (C92664Wv) obj;
            if (!this.A00.equals(r5.A00) || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A00;
        return C12960it.A06(this.A01, A1a);
    }
}
