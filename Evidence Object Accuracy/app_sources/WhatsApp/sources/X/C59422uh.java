package X;

import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.2uh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59422uh extends AbstractC16320oo {
    public final int A00 = 256;
    public final LatLng A01;
    public final AnonymousClass2K1 A02;

    public C59422uh(LatLng latLng, AbstractC15710nm r14, C14900mE r15, C16430p0 r16, AnonymousClass2K1 r17, AnonymousClass1B4 r18, C16340oq r19, C17170qN r20, AnonymousClass018 r21, C14850m9 r22, AbstractC14440lR r23) {
        super(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23);
        this.A01 = latLng;
        this.A02 = r17;
    }
}
