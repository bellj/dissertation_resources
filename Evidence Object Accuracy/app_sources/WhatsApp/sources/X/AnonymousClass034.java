package X;

import java.util.Random;

/* renamed from: X.034  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass034 {
    public static final Random A00 = new Random();

    public static String A00(long j) {
        if (j < 0) {
            StringBuilder sb = new StringBuilder("Cannot internalEncode negative integer ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (j <= (1 << Math.min(63, 66)) - 1) {
            StringBuilder sb2 = new StringBuilder();
            int i = 0;
            do {
                sb2.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((int) (j % 64)));
                j >>= 6;
                i++;
            } while (i < 11);
            if (j <= 0) {
                sb2.reverse();
                return sb2.toString();
            }
            throw new IllegalArgumentException("Number won't fit in string");
        } else {
            StringBuilder sb3 = new StringBuilder("Cannot internalEncode integer ");
            sb3.append(j);
            sb3.append(" in ");
            sb3.append(11);
            sb3.append(" chars");
            throw new IllegalArgumentException(sb3.toString());
        }
    }
}
