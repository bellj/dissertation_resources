package X;

import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.3C5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C5 {
    public final byte A00;
    public final int A01;
    public final int A02;
    public final Uri A03;
    public final Bundle A04;
    public final C44691zO A05;
    public final C32701cb A06;
    public final C15370n3 A07;
    public final AbstractC14640lm A08;
    public final C15580nU A09;
    public final UserJid A0A;
    public final File A0B;
    public final Long A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final String A0K;
    public final String A0L;
    public final String A0M;
    public final String A0N;
    public final String A0O;
    public final String A0P;
    public final ArrayList A0Q;
    public final ArrayList A0R;
    public final boolean A0S;
    public final boolean A0T;
    public final boolean A0U;
    public final boolean A0V;
    public final boolean A0W;
    public final boolean A0X;
    public final boolean A0Y;
    public final boolean A0Z;
    public final boolean A0a;
    public final boolean A0b;

    public AnonymousClass3C5(Uri uri, Bundle bundle, C44691zO r4, C32701cb r5, C15370n3 r6, AbstractC14640lm r7, C15580nU r8, UserJid userJid, File file, Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, ArrayList arrayList, ArrayList arrayList2, byte b, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
        this.A02 = i;
        this.A08 = r7;
        this.A07 = r6;
        this.A0V = z;
        this.A0a = z2;
        this.A0U = z3;
        this.A0b = z4;
        this.A0W = z5;
        this.A0Z = z6;
        this.A0Y = z7;
        this.A0X = z8;
        this.A00 = b;
        this.A0T = z9;
        this.A01 = i2;
        this.A0H = str;
        this.A0O = str2;
        this.A0P = str3;
        this.A0L = str4;
        this.A0N = str5;
        this.A0R = arrayList;
        this.A0Q = arrayList2;
        this.A03 = uri;
        this.A04 = bundle;
        this.A05 = r4;
        this.A0A = userJid;
        this.A0B = file;
        this.A09 = r8;
        this.A0J = str6;
        this.A0I = str7;
        this.A0D = str8;
        this.A0K = str9;
        this.A0M = str10;
        this.A0E = str11;
        this.A0G = str12;
        this.A0F = str13;
        this.A0C = l;
        this.A06 = r5;
        this.A0S = z10;
    }
}
