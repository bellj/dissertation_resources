package X;

import java.io.File;

/* renamed from: X.37e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C624237e extends AbstractC16350or {
    public final C16170oZ A00;
    public final AnonymousClass1KC A01;
    public final C30421Xi A02;
    public final File A03;

    public C624237e(C16170oZ r1, AnonymousClass1KC r2, C30421Xi r3, File file) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = file;
    }
}
