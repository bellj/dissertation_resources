package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.0W5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W5 implements View.OnAttachStateChangeListener {
    public final /* synthetic */ AnonymousClass0CM A00;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public AnonymousClass0W5(AnonymousClass0CM r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        AnonymousClass0CM r2 = this.A00;
        ViewTreeObserver viewTreeObserver = r2.A07;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                r2.A07 = view.getViewTreeObserver();
            }
            r2.A07.removeGlobalOnLayoutListener(r2.A0L);
        }
        view.removeOnAttachStateChangeListener(this);
    }
}
