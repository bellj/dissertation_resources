package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/* renamed from: X.1H7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H7 extends AbstractC250017s {
    public final C15570nT A00;
    public final C250217u A01;
    public final C233411h A02;
    public final C21370xJ A03;
    public final C16970q3 A04;
    public final C14830m7 A05;
    public final C14820m6 A06;
    public final C16510p9 A07;
    public final C19990v2 A08;
    public final C15680nj A09;

    public AnonymousClass1H7(C15570nT r1, C250217u r2, C233411h r3, C21370xJ r4, C16970q3 r5, C14830m7 r6, C14820m6 r7, C16510p9 r8, C19990v2 r9, C15680nj r10, C233511i r11) {
        super(r11);
        this.A05 = r6;
        this.A07 = r8;
        this.A00 = r1;
        this.A08 = r9;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A09 = r10;
    }

    public List A08() {
        this.A00.A08();
        Log.i("unarchive-chats-setting-handler/onUnarchiveChatsSettingChanged");
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet(this.A09.A05());
        C233511i r0 = super.A00;
        ArrayList arrayList2 = new ArrayList();
        C16310on A01 = r0.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT chat_jid FROM syncd_mutations WHERE mutation_name = ? AND are_dependencies_missing = ?", new String[]{"archive", String.valueOf(0)});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("chat_jid");
            while (A09.moveToNext()) {
                AbstractC14640lm A012 = AbstractC14640lm.A01(A09.getString(columnIndexOrThrow));
                AnonymousClass009.A05(A012);
                arrayList2.add(A012);
            }
            A09.close();
            A01.close();
            ListIterator listIterator = arrayList2.listIterator();
            SharedPreferences sharedPreferences = this.A06.A00;
            if (sharedPreferences.getBoolean("notify_new_message_for_archived_chats", false)) {
                while (listIterator.hasNext()) {
                    if (!hashSet.contains(listIterator.next())) {
                        listIterator.remove();
                    }
                }
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    AbstractC14640lm r10 = (AbstractC14640lm) it.next();
                    this.A02.A06(1);
                    C14830m7 r6 = this.A05;
                    arrayList.add(new C34651gU(r10, r6.A00(), false));
                    arrayList.add(new C34631gS(this.A01.A04(r10, false), r10, r6.A00(), true));
                }
            } else {
                while (listIterator.hasNext()) {
                    if (hashSet.contains(listIterator.next())) {
                        listIterator.remove();
                    }
                }
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    AbstractC14640lm r11 = (AbstractC14640lm) it2.next();
                    arrayList.add(new C34631gS(this.A01.A04(r11, false), r11, this.A05.A00(), false));
                }
            }
            ArrayList arrayList3 = new ArrayList(arrayList);
            arrayList3.add(new C34481gD(null, null, this.A05.A00(), sharedPreferences.getBoolean("notify_new_message_for_archived_chats", false)));
            return arrayList3;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
