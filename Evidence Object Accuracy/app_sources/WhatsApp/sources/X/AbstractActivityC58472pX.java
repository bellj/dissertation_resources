package X;

import com.whatsapp.gallery.MediaGalleryActivity;

/* renamed from: X.2pX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58472pX extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58472pX() {
        ActivityC13830kP.A1P(this, 68);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            MediaGalleryActivity mediaGalleryActivity = (MediaGalleryActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, mediaGalleryActivity);
            ActivityC13810kN.A10(A1M, mediaGalleryActivity);
            ((ActivityC13790kL) mediaGalleryActivity).A08 = ActivityC13790kL.A0S(r3, A1M, mediaGalleryActivity, ActivityC13790kL.A0Y(A1M, mediaGalleryActivity));
            mediaGalleryActivity.A0m = (AnonymousClass1CY) A1M.ABO.get();
            mediaGalleryActivity.A0c = (AnonymousClass13H) A1M.ABY.get();
            mediaGalleryActivity.A0h = (C253018w) A1M.AJS.get();
            mediaGalleryActivity.A07 = (C239613r) A1M.AI9.get();
            mediaGalleryActivity.A0i = ActivityC13830kP.A1N(A1M);
            mediaGalleryActivity.A0W = C12970iu.A0b(A1M);
            mediaGalleryActivity.A0A = (C18850tA) A1M.AKx.get();
            mediaGalleryActivity.A08 = (C16170oZ) A1M.AM4.get();
            mediaGalleryActivity.A0U = (C231510o) A1M.AHO.get();
            mediaGalleryActivity.A0f = new C88054Ec();
            mediaGalleryActivity.A0B = C12960it.A0O(A1M);
            mediaGalleryActivity.A0T = (C22180yf) A1M.A5U.get();
            mediaGalleryActivity.A0k = C12980iv.A0g(A1M);
            mediaGalleryActivity.A0D = C12960it.A0P(A1M);
            mediaGalleryActivity.A0L = (C15240mn) A1M.A8F.get();
            mediaGalleryActivity.A0O = (C253218y) A1M.AAF.get();
            mediaGalleryActivity.A0K = (C15650ng) A1M.A4m.get();
            mediaGalleryActivity.A0P = (C20050v8) A1M.AAV.get();
            mediaGalleryActivity.A0Q = (AnonymousClass12H) A1M.AC5.get();
            mediaGalleryActivity.A0g = C12990iw.A0f(A1M);
            mediaGalleryActivity.A0X = C12980iv.A0e(A1M);
            mediaGalleryActivity.A0I = (C15410nB) A1M.ALJ.get();
            mediaGalleryActivity.A09 = (AnonymousClass12T) A1M.AJZ.get();
            mediaGalleryActivity.A0Y = (AnonymousClass11G) A1M.AKq.get();
            mediaGalleryActivity.A0V = (AnonymousClass193) A1M.A6S.get();
            mediaGalleryActivity.A0R = (C242114q) A1M.AJq.get();
            mediaGalleryActivity.A0l = (C23000zz) A1M.ALg.get();
            mediaGalleryActivity.A0C = C12980iv.A0a(A1M);
            mediaGalleryActivity.A0n = C18000rk.A00(A1M.A4v);
            mediaGalleryActivity.A0G = (AnonymousClass1A6) A1M.A4u.get();
            mediaGalleryActivity.A0J = C12970iu.A0Y(A1M);
            mediaGalleryActivity.A0b = (C22370yy) A1M.AB0.get();
            mediaGalleryActivity.A0j = (AnonymousClass1AB) A1M.AKI.get();
            mediaGalleryActivity.A0S = (C22100yW) A1M.A3g.get();
            mediaGalleryActivity.A0H = (C255419u) A1M.AJp.get();
            mediaGalleryActivity.A0a = (AnonymousClass109) A1M.AI8.get();
            mediaGalleryActivity.A0N = C12980iv.A0d(A1M);
            mediaGalleryActivity.A0e = C12990iw.A0e(A1M);
            mediaGalleryActivity.A0E = (AnonymousClass19I) A1M.A4a.get();
            mediaGalleryActivity.A0d = (AnonymousClass19J) A1M.ACA.get();
        }
    }
}
