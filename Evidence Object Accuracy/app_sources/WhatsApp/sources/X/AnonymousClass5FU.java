package X;

/* renamed from: X.5FU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FU implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FU(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        boolean[] zArr = (boolean[]) obj;
        appendable.append('[');
        boolean z = false;
        for (boolean z2 : zArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Boolean.toString(z2));
        }
        appendable.append(']');
    }
}
