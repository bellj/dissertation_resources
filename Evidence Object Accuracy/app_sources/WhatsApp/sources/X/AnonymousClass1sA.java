package X;

import android.os.Handler;
import android.os.Looper;
import com.whatsapp.camera.recording.RecordingView;
import java.util.List;

/* renamed from: X.1sA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1sA {
    public long A00;
    public final Handler A01 = new AnonymousClass2a4(Looper.getMainLooper(), this);
    public final C15450nH A02;
    public final AnonymousClass2U7 A03;
    public final RecordingView A04;
    public final C14830m7 A05;
    public final AnonymousClass018 A06;
    public final List A07;

    public AnonymousClass1sA(C15450nH r3, AnonymousClass2U7 r4, RecordingView recordingView, C14830m7 r6, AnonymousClass018 r7, List list) {
        this.A03 = r4;
        this.A04 = recordingView;
        this.A07 = list;
        this.A02 = r3;
        this.A06 = r7;
        this.A05 = r6;
    }
}
