package X;

/* renamed from: X.5Ez  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112855Ez implements AnonymousClass5VF {
    public final AnonymousClass5X4 A00;

    public C112855Ez(AnonymousClass5X4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VF
    public AnonymousClass5X4 ABk() {
        return this.A00;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CoroutineScope(coroutineContext=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
