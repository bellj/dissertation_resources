package X;

import androidx.window.extensions.WindowExtensionsProvider;

/* renamed from: X.0fa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11000fa extends AnonymousClass1WI implements AnonymousClass1WK {
    public C11000fa() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        ClassLoader classLoader = AnonymousClass0UQ.class.getClassLoader();
        if (classLoader == null || !AnonymousClass0UQ.A01(AnonymousClass0UQ.A00, classLoader)) {
            return null;
        }
        try {
            return WindowExtensionsProvider.getWindowExtensions().getWindowLayoutComponent();
        } catch (UnsupportedOperationException unused) {
            return null;
        }
    }
}
