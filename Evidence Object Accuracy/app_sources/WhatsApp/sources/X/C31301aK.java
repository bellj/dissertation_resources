package X;

import java.util.Comparator;

/* renamed from: X.1aK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31301aK implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        int length;
        int i;
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = (byte[]) obj2;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            length = bArr.length;
            if (i2 < length && i3 < bArr2.length) {
                length = bArr[i2] & 255;
                i = bArr2[i3] & 255;
                if (length != i) {
                    break;
                }
                i2++;
                i3++;
            } else {
                break;
            }
        }
        i = bArr2.length;
        return length - i;
    }
}
