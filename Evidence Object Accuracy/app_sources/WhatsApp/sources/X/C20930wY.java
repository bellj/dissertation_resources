package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.data.ProfilePhotoChange;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.0wY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20930wY {
    public final AbstractC15710nm A00;
    public final C16510p9 A01;
    public final C18460sU A02;
    public final C16490p7 A03;
    public final C247516s A04;
    public final C21390xL A05;
    public final C20320vZ A06;

    public C20930wY(AbstractC15710nm r1, C16510p9 r2, C18460sU r3, C16490p7 r4, C247516s r5, C21390xL r6, C20320vZ r7) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = r7;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
    }

    public long A00(AbstractC14640lm r6) {
        String str;
        C16310on A01 = this.A03.get();
        try {
            String[] strArr = {String.valueOf(this.A01.A02(r6))};
            if (A02()) {
                str = "SELECT _id FROM message_system JOIN message_view ON message_view._id = message_system.message_row_id  WHERE chat_row_id = ?  AND (action_type = 19 OR action_type = 67)";
            } else {
                str = "SELECT _id FROM message_view WHERE chat_row_id = ? AND from_me = 1 AND message_type = 7 AND (media_size = 19 OR media_size = 67)";
            }
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09.moveToFirst()) {
                long j = (long) A09.getInt(0);
                A09.close();
                A01.close();
                return j;
            }
            A09.close();
            A01.close();
            return -1;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A01(AnonymousClass1XB r19) {
        long j;
        long j2;
        C16490p7 r8 = this.A03;
        C16310on A02 = r8.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(r19.A11));
            contentValues.put("action_type", Integer.valueOf(r19.A00));
            C16330op r3 = A02.A03;
            r3.A06(contentValues, "message_system", 5);
            if (r19 instanceof C30451Xl) {
                C30451Xl r10 = (C30451Xl) r19;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("message_row_id", Long.valueOf(r10.A11));
                C30021Vq.A04(contentValues2, "old_data", r10.A01);
                r3.A06(contentValues2, "message_system_value_change", 5);
            }
            if (r19 instanceof C30481Xo) {
                C30481Xo r102 = (C30481Xo) r19;
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("message_row_id", Long.valueOf(r102.A11));
                C30021Vq.A04(contentValues3, "old_data", r102.A00);
                r3.A06(contentValues3, "message_system_value_change", 5);
            }
            if (r19 instanceof C30491Xp) {
                C30491Xp r103 = (C30491Xp) r19;
                ContentValues contentValues4 = new ContentValues();
                contentValues4.put("message_row_id", Long.valueOf(r103.A11));
                C30021Vq.A04(contentValues4, "old_data", r103.A00);
                r3.A06(contentValues4, "message_system_value_change", 5);
            }
            if (r19 instanceof C30461Xm) {
                C30461Xm r9 = (C30461Xm) r19;
                ContentValues contentValues5 = new ContentValues();
                contentValues5.put("message_row_id", Long.valueOf(r9.A11));
                contentValues5.put("is_me_joined", Integer.valueOf(r9.A00));
                r3.A06(contentValues5, "message_system_group", 5);
                for (UserJid userJid : r9.A01) {
                    long A01 = this.A02.A01(userJid);
                    if (A01 >= 0) {
                        ContentValues contentValues6 = new ContentValues();
                        contentValues6.put("message_row_id", Long.valueOf(r9.A11));
                        contentValues6.put("user_jid_row_id", Long.valueOf(A01));
                        r3.A06(contentValues6, "message_system_chat_participant", 5);
                    }
                }
            }
            if (r19 instanceof C30501Xq) {
                C30501Xq r104 = (C30501Xq) r19;
                ContentValues contentValues7 = new ContentValues();
                contentValues7.put("message_row_id", Long.valueOf(r104.A11));
                C30021Vq.A04(contentValues7, "new_photo_id", r104.A15());
                ProfilePhotoChange profilePhotoChange = r104.A00;
                if (profilePhotoChange != null) {
                    C30021Vq.A06(contentValues7, "old_photo", profilePhotoChange.oldPhoto);
                    C30021Vq.A06(contentValues7, "new_photo", r104.A00.newPhoto);
                    C30021Vq.A04(contentValues7, "new_photo_id", String.valueOf(r104.A00.newPhotoId));
                }
                if (!TextUtils.isEmpty(r104.A15()) || r104.A00 != null) {
                    r3.A06(contentValues7, "message_system_photo_change", 5);
                }
            }
            if (r19 instanceof C30511Xs) {
                C30511Xs r12 = (C30511Xs) r19;
                UserJid userJid2 = r12.A01;
                if (userJid2 != null) {
                    j = this.A02.A01(userJid2);
                } else {
                    j = -1;
                }
                UserJid userJid3 = r12.A00;
                if (userJid3 != null) {
                    j2 = this.A02.A01(userJid3);
                } else {
                    j2 = -1;
                }
                if (!(j == -1 && j2 == -1)) {
                    ContentValues contentValues8 = new ContentValues();
                    contentValues8.put("message_row_id", Long.valueOf(r12.A11));
                    Long valueOf = Long.valueOf(j);
                    if (valueOf == null) {
                        contentValues8.putNull("old_jid_row_id");
                    } else {
                        contentValues8.put("old_jid_row_id", valueOf);
                    }
                    Long valueOf2 = Long.valueOf(j2);
                    if (valueOf2 == null) {
                        contentValues8.putNull("new_jid_row_id");
                    } else {
                        contentValues8.put("new_jid_row_id", valueOf2);
                    }
                    r3.A06(contentValues8, "message_system_number_change", 5);
                }
            }
            if (r19 instanceof C30521Xt) {
                C30521Xt r105 = (C30521Xt) r19;
                ContentValues contentValues9 = new ContentValues();
                contentValues9.put("message_row_id", Long.valueOf(r105.A11));
                Long valueOf3 = Long.valueOf((long) r105.A00);
                if (valueOf3 == null) {
                    contentValues9.putNull("device_added_count");
                } else {
                    contentValues9.put("device_added_count", valueOf3);
                }
                Long valueOf4 = Long.valueOf((long) r105.A01);
                if (valueOf4 == null) {
                    contentValues9.putNull("device_removed_count");
                } else {
                    contentValues9.put("device_removed_count", valueOf4);
                }
                r3.A06(contentValues9, "message_system_device_change", 5);
            }
            if (r19 instanceof C30531Xu) {
                C30531Xu r106 = (C30531Xu) r19;
                ContentValues contentValues10 = new ContentValues();
                contentValues10.put("message_row_id", Long.valueOf(r106.A11));
                contentValues10.put("biz_state_id", Integer.valueOf(r106.A00));
                r3.A06(contentValues10, "message_system_initial_privacy_provider", 5);
            }
            if (r19 instanceof C30541Xv) {
                C30541Xv r107 = (C30541Xv) r19;
                ContentValues contentValues11 = new ContentValues();
                contentValues11.put("message_row_id", Long.valueOf(r107.A11));
                UserJid userJid4 = r107.A01;
                if (userJid4 != null) {
                    contentValues11.put("sender_jid_row_id", Long.valueOf(this.A02.A01(userJid4)));
                }
                UserJid userJid5 = r107.A00;
                if (userJid5 != null) {
                    contentValues11.put("receiver_jid_row_id", Long.valueOf(this.A02.A01(userJid5)));
                }
                C30021Vq.A04(contentValues11, "amount_with_symbol", r107.A03);
                AnonymousClass1IS r108 = r107.A02;
                if (r108 != null) {
                    AbstractC14640lm r1 = r108.A00;
                    if (r1 != null) {
                        contentValues11.put("remote_message_sender_jid_row_id", Long.valueOf(this.A02.A01(r1)));
                    }
                    C30021Vq.A05(contentValues11, "remote_message_from_me", r108.A02);
                    C30021Vq.A04(contentValues11, "remote_message_key", r108.A01);
                }
                r3.A06(contentValues11, "message_payment", 5);
                if (r19 instanceof C30551Xw) {
                    C30551Xw r109 = (C30551Xw) r19;
                    ContentValues contentValues12 = new ContentValues();
                    contentValues12.put("message_row_id", Long.valueOf(r109.A11));
                    C30021Vq.A04(contentValues12, "web_stub", r109.A02);
                    C30021Vq.A04(contentValues12, "amount", r109.A01);
                    C30021Vq.A04(contentValues12, "transfer_date", r109.A04);
                    C30021Vq.A04(contentValues12, "payment_sender_name", r109.A03);
                    contentValues12.put("expiration", Integer.valueOf(r109.A00));
                    r3.A06(contentValues12, "message_payment_transaction_reminder", 5);
                }
                if (r19 instanceof C30561Xx) {
                    C30561Xx r1010 = (C30561Xx) r19;
                    ContentValues contentValues13 = new ContentValues();
                    contentValues13.put("message_row_id", Long.valueOf(r1010.A11));
                    C30021Vq.A04(contentValues13, "transaction_info", r1010.A03);
                    C30021Vq.A04(contentValues13, "transaction_data", r1010.A01);
                    C30021Vq.A04(contentValues13, "init_timestamp", r1010.A02);
                    C30021Vq.A04(contentValues13, "update_timestamp", r1010.A04);
                    C30021Vq.A04(contentValues13, "amount_data", r1010.A00);
                    r3.A06(contentValues13, "message_payment_status_update", 5);
                }
            }
            if (r19 instanceof AbstractC30571Xy) {
                AbstractC30571Xy r11 = (AbstractC30571Xy) r19;
                C16310on A022 = this.A04.A00.A02();
                ContentValues contentValues14 = new ContentValues(3);
                contentValues14.put("message_row_id", Long.valueOf(r11.A11));
                contentValues14.put("service", Integer.valueOf(r11.A00));
                contentValues14.put("invite_used", Boolean.valueOf(r11.A01));
                A022.A03.A02(contentValues14, "message_system_payment_invite_setup");
                A022.close();
            }
            if (r19 instanceof C30581Xz) {
                C30581Xz r1011 = (C30581Xz) r19;
                ContentValues contentValues15 = new ContentValues();
                contentValues15.put("message_row_id", Long.valueOf(r1011.A11));
                contentValues15.put("is_blocked", Integer.valueOf(r1011.A00 ? 1 : 0));
                r3.A06(contentValues15, "message_system_block_contact", 5);
            }
            if (r19 instanceof AnonymousClass1Y0) {
                AnonymousClass1Y0 r1012 = (AnonymousClass1Y0) r19;
                ContentValues contentValues16 = new ContentValues();
                contentValues16.put("message_row_id", Long.valueOf(r1012.A11));
                contentValues16.put("setting_duration", Integer.valueOf(r1012.A00));
                r3.A06(contentValues16, "message_system_ephemeral_setting_not_applied", 5);
            }
            if (r19 instanceof AnonymousClass1Y1) {
                AnonymousClass1Y1 r1013 = (AnonymousClass1Y1) r19;
                C16310on A023 = r8.A02();
                ContentValues contentValues17 = new ContentValues();
                contentValues17.put("message_row_id", Long.valueOf(r1013.A11));
                contentValues17.put("business_name", r1013.A01);
                contentValues17.put("privacy_message_type", Integer.valueOf(r1013.A00));
                A023.A03.A02(contentValues17, "message_system_business_state");
                A023.close();
            }
            if (r19 instanceof AnonymousClass1Y2) {
                AnonymousClass1Y2 r92 = (AnonymousClass1Y2) r19;
                ContentValues contentValues18 = new ContentValues();
                contentValues18.put("message_row_id", Long.valueOf(r92.A11));
                C30021Vq.A04(contentValues18, "call_id", r92.A00);
                contentValues18.put("is_video_call", Integer.valueOf(r92.A01 ? 1 : 0));
                r3.A06(contentValues18, "message_system_linked_group_call", 5);
            }
            if (r19 instanceof AnonymousClass1Y3) {
                AnonymousClass1Y3 r93 = (AnonymousClass1Y3) r19;
                ContentValues contentValues19 = new ContentValues();
                contentValues19.put("message_row_id", Long.valueOf(r93.A11));
                Integer num = r93.A02;
                Long valueOf5 = num == null ? null : Long.valueOf(num.longValue());
                if (valueOf5 == null) {
                    contentValues19.putNull("old_group_type");
                } else {
                    contentValues19.put("old_group_type", valueOf5);
                }
                contentValues19.put("new_group_type", Integer.valueOf(r93.A00));
                GroupJid groupJid = r93.A01;
                if (groupJid == null) {
                    contentValues19.put("linked_parent_group_jid_row_id", (Integer) -1);
                } else {
                    contentValues19.put("linked_parent_group_jid_row_id", Long.valueOf(this.A02.A01(groupJid)));
                }
                r3.A06(contentValues19, "message_system_community_link_changed", 5);
            }
            if (r19 instanceof AnonymousClass1Y4) {
                AnonymousClass1Y4 r94 = (AnonymousClass1Y4) r19;
                ContentValues contentValues20 = new ContentValues();
                contentValues20.put("message_row_id", Long.valueOf(r94.A11));
                String str = r94.A00;
                if (str != null) {
                    contentValues20.put("linked_parent_group_name", str);
                }
                r3.A06(contentValues20, "message_system_group_with_parent", 5);
            }
            if (r19 instanceof AnonymousClass1Y5) {
                AnonymousClass1Y5 r6 = (AnonymousClass1Y5) r19;
                for (AnonymousClass1OU r95 : r6.A03) {
                    ContentValues contentValues21 = new ContentValues();
                    contentValues21.put("message_row_id", Long.valueOf(r6.A11));
                    contentValues21.put("subgroup_raw_jid", r95.A02.getRawString());
                    C30021Vq.A04(contentValues21, "subgroup_subject", r95.A03);
                    C15580nU r13 = r6.A01;
                    if (r13 == null) {
                        contentValues21.put("parent_group_jid_row_id", (Integer) -1);
                    } else {
                        contentValues21.put("parent_group_jid_row_id", Long.valueOf(this.A02.A01(r13)));
                    }
                    r3.A06(contentValues21, "message_system_sibling_group_link_change", 5);
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0027, code lost:
        if (r7.A05.A01("system_message_ready", 0) == 2) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02() {
        /*
            r7 = this;
            X.0p7 r0 = r7.A03
            X.0on r6 = r0.get()
            r0.A04()     // Catch: all -> 0x002e
            X.1To r1 = r0.A05     // Catch: all -> 0x002e
            X.0op r0 = r6.A03     // Catch: all -> 0x002e
            java.lang.Boolean r0 = r1.A06(r0)     // Catch: all -> 0x002e
            boolean r0 = r0.booleanValue()     // Catch: all -> 0x002e
            if (r0 != 0) goto L_0x0029
            X.0xL r3 = r7.A05     // Catch: all -> 0x002e
            java.lang.String r2 = "system_message_ready"
            r0 = 0
            long r4 = r3.A01(r2, r0)     // Catch: all -> 0x002e
            r2 = 2
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 1
        L_0x002a:
            r6.close()
            return r0
        L_0x002e:
            r0 = move-exception
            r6.close()     // Catch: all -> 0x0032
        L_0x0032:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20930wY.A02():boolean");
    }
}
