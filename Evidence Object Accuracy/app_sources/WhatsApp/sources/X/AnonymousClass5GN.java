package X;

import java.security.Signature;

/* renamed from: X.5GN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GN implements AnonymousClass5VR {
    public final /* synthetic */ String A00;
    public final /* synthetic */ AbstractC113435Ho A01;

    public AnonymousClass5GN(String str, AbstractC113435Ho r2) {
        this.A01 = r2;
        this.A00 = str;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        String str2 = this.A00;
        return str2 != null ? Signature.getInstance(str, str2) : Signature.getInstance(str);
    }
}
