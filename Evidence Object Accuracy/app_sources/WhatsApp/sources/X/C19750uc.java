package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0uc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19750uc {
    public final Map A00 = new LinkedHashMap();

    public final synchronized AnonymousClass17Q A00(String str) {
        C16700pc.A0E(str, 0);
        return (AnonymousClass17Q) this.A00.get(str);
    }
}
