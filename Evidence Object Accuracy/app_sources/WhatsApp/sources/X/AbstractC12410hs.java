package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12410hs {
    float ADB(View view, ViewGroup viewGroup);

    float ADC(View view, ViewGroup viewGroup);
}
