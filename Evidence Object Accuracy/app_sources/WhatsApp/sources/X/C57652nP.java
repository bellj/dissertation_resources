package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57652nP extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57652nP A0C;
    public static volatile AnonymousClass255 A0D;
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public C40821sO A04;
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";
    public String A09 = "";
    public String A0A = "";
    public String A0B = "";

    static {
        C57652nP r0 = new C57652nP();
        A0C = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C81963ur r1;
        switch (r15.ordinal()) {
            case 0:
                return A0C;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57652nP r2 = (C57652nP) obj2;
                this.A04 = (C40821sO) r7.Aft(this.A04, r2.A04);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                String str = this.A08;
                int i2 = r2.A00;
                this.A08 = r7.Afy(str, r2.A08, A1V, C12960it.A1V(i2 & 2, 2));
                this.A0A = r7.Afy(this.A0A, r2.A0A, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A06 = r7.Afy(this.A06, r2.A06, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A05 = r7.Afy(this.A05, r2.A05, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A02 = r7.Afs(this.A02, r2.A02, C12960it.A1V(i & 32, 32), C12960it.A1V(i2 & 32, 32));
                this.A09 = r7.Afy(this.A09, r2.A09, C12960it.A1V(i & 64, 64), C12960it.A1V(i2 & 64, 64));
                this.A0B = r7.Afy(this.A0B, r2.A0B, C12960it.A1V(i & 128, 128), C12960it.A1V(i2 & 128, 128));
                this.A01 = r7.Afp(this.A01, r2.A01, C12960it.A1V(i & 256, 256), C12960it.A1V(i2 & 256, 256));
                this.A07 = r7.Afy(this.A07, r2.A07, C12960it.A1V(i & 512, 512), C12960it.A1V(i2 & 512, 512));
                this.A03 = r7.Afs(this.A03, r2.A03, C12960it.A1V(i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), C12960it.A1V(i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
                if (r7 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                if ((this.A00 & 1) == 1) {
                                    r1 = (C81963ur) this.A04.A0T();
                                } else {
                                    r1 = null;
                                }
                                C40821sO r0 = (C40821sO) AbstractC27091Fz.A0H(r72, r22, C40821sO.A0R);
                                this.A04 = r0;
                                if (r1 != null) {
                                    this.A04 = (C40821sO) AbstractC27091Fz.A0C(r1, r0);
                                }
                                this.A00 |= 1;
                                break;
                            case 18:
                                String A0A = r72.A0A();
                                this.A00 |= 2;
                                this.A08 = A0A;
                                break;
                            case 26:
                                String A0A2 = r72.A0A();
                                this.A00 |= 4;
                                this.A0A = A0A2;
                                break;
                            case 34:
                                String A0A3 = r72.A0A();
                                this.A00 |= 8;
                                this.A06 = A0A3;
                                break;
                            case 42:
                                String A0A4 = r72.A0A();
                                this.A00 |= 16;
                                this.A05 = A0A4;
                                break;
                            case 48:
                                this.A00 |= 32;
                                this.A02 = r72.A06();
                                break;
                            case 58:
                                String A0A5 = r72.A0A();
                                this.A00 |= 64;
                                this.A09 = A0A5;
                                break;
                            case 66:
                                String A0A6 = r72.A0A();
                                this.A00 |= 128;
                                this.A0B = A0A6;
                                break;
                            case C43951xu.A02:
                                this.A00 |= 256;
                                this.A01 = r72.A02();
                                break;
                            case 90:
                                String A0A7 = r72.A0A();
                                this.A00 |= 512;
                                this.A07 = A0A7;
                                break;
                            case 96:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A03 = r72.A06();
                                break;
                            default:
                                if (A0a(r72, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57652nP();
            case 5:
                return new C82273vM();
            case 6:
                break;
            case 7:
                if (A0D == null) {
                    synchronized (C57652nP.class) {
                        if (A0D == null) {
                            A0D = AbstractC27091Fz.A09(A0C);
                        }
                    }
                }
                return A0D;
            default:
                throw C12970iu.A0z();
        }
        return A0C;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C40821sO r0 = this.A04;
            if (r0 == null) {
                r0 = C40821sO.A0R;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A08, i2);
        }
        if ((this.A00 & 4) == 4) {
            i2 = AbstractC27091Fz.A04(3, this.A0A, i2);
        }
        if ((this.A00 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A06, i2);
        }
        if ((this.A00 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A05, i2);
        }
        int i3 = this.A00;
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A05(6, this.A02);
        }
        if ((i3 & 64) == 64) {
            i2 = AbstractC27091Fz.A04(7, this.A09, i2);
        }
        if ((this.A00 & 128) == 128) {
            i2 = AbstractC27091Fz.A04(8, this.A0B, i2);
        }
        int i4 = this.A00;
        if ((i4 & 256) == 256) {
            i2 = AbstractC27091Fz.A02(9, this.A01, i2);
        }
        if ((i4 & 512) == 512) {
            i2 = AbstractC27091Fz.A04(11, this.A07, i2);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A05(12, this.A03);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C40821sO r0 = this.A04;
            if (r0 == null) {
                r0 = C40821sO.A0R;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A08);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A0A);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A06);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A05);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0H(6, this.A02);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A09);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A0B);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0F(9, this.A01);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0I(11, this.A07);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0H(12, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
