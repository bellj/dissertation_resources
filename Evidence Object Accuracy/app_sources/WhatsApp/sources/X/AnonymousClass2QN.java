package X;

import com.whatsapp.R;

/* renamed from: X.2QN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2QN {
    public static final int[] A00 = {R.attr.arflAspectRatio, R.attr.resize_mode};
    public static final int[] A01 = {R.attr.dateWrapper_marginLeft, R.attr.dateWrapper_paddingRight, R.attr.hasStatusView};
    public static final int[] A02 = {R.attr.playButton_marginLeft, R.attr.playButton_size, R.attr.progressSpinner_size, R.attr.seekbarContainer_paddingRight, R.attr.seekbar_marginLeft, R.attr.seekbar_paddingBottom, R.attr.seekbar_paddingTop};
    public static final int[] A03 = {R.attr.gridColumns};
    public static final int[] A04 = {R.attr.bpfvEllipsize, R.attr.bpfvHintText, R.attr.bpfvImage, R.attr.bpfvSingleLine, R.attr.bpfvTextColor, R.attr.bpfvTextColorHint};
    public static final int[] A05 = {R.attr.cpvStrokeMax, R.attr.cpvStrokeMin, R.attr.cpvWidth};
    public static final int[] A06 = {R.attr.icIcon, R.attr.icTitle, R.attr.icTitleTextColor};
    public static final int[] A07 = {R.attr.clltGlowColor, R.attr.clltGlowSize, R.attr.clltStackSize};
    public static final int[] A08 = {R.attr.cstErrorColor, R.attr.cstSeenColor, R.attr.cstUnseenColor};
    public static final int[] A09 = {R.attr.articleID, R.attr.educationString};
    public static final int[] A0A = {R.attr.gDriveRestoreFromIcon, R.attr.gDriveRestoreFromIconTint, R.attr.gDriveRestoreStraightLoadingAnimation, R.attr.gDriveRestoreToIcon, R.attr.gDriveRestoreToIconTint};
    public static final int[] A0B = {R.attr.settingsRowInfoSubText, R.attr.settingsRowTitleText};
    public static final int[] A0C = {R.attr.mcInfo, R.attr.mcTitle};
    public static final int[] A0D = {R.attr.mctPhotoSpacing, R.attr.mctRadius};
    public static final int[] A0E = {R.attr.pstsDividerColor, R.attr.pstsIndicatorColor, R.attr.pstsTabPadding};
    public static final int[] A0F = {R.attr.pneBarColor};
    public static final int[] A0G = {R.attr.psritDescription, R.attr.psritIcon, R.attr.psritIconColor, R.attr.psritSecondaryIcon, R.attr.psritSecondaryIconColor, R.attr.psritSubText, R.attr.psritTitleText};
    public static final int[] A0H = {R.attr.qsoShape, R.attr.qsoText, R.attr.qsoTextSize, R.attr.qsoTextTopMargin};
    public static final int[] A0I = {R.attr.collapsible};
    public static final int[] A0J = {R.attr.settingsRowBadgeIconColor, R.attr.settingsRowIconColor, R.attr.settingsRowIconTextBadgeIcon, R.attr.settingsRowIconTextIcon, R.attr.settingsRowIconTextIconRTLSupported, R.attr.settingsRowIconTextSubText, R.attr.settingsRowIconTextText, R.attr.settingsRowSubTextMaxLines};
    public static final int[] A0K = {R.attr.stvUnitTextSize, R.attr.stvValueTextSize};
    public static final int[] A0L = {R.attr.filledDrawable, R.attr.maxRating, R.attr.unfilledDrawable};
    public static final int[] A0M = {R.attr.subgroupPileView_borderColor, R.attr.subgroupPileView_pileSize, R.attr.subgroupPileView_separationSpace};
    public static final int[] A0N = {R.attr.accountType, R.attr.editButtonColor, R.attr.isEditable};
    public static final int[] A0O = {R.attr.vtvBorderColor, R.attr.vtvBorderSize, R.attr.vtvDimColor, R.attr.vtvThumbColor, R.attr.vtvThumbColorPressed, R.attr.vtvThumbSize, R.attr.vtvThumbSizePressed};
    public static final int[] A0P = {R.attr.micOverlay_background, R.attr.micOverlay_backgroundTint, R.attr.micOverlay_marginBottom, R.attr.micOverlay_marginLeft, R.attr.profileFrame_paddingBottom, R.attr.profileFrame_paddingLeft, R.attr.profileFrame_paddingTop, R.attr.profileView_layoutSize, R.attr.profileView_padding};
    public static final int[] A0Q = {R.attr.vnsbBackgroundColor, R.attr.vnsbProgressColor, R.attr.vnsbThumbSize, R.attr.vnsbTrackWidth};
    public static final int[] A0R = {R.attr.progressBubbleColor, R.attr.progressBubbleRadius, R.attr.progressColor, R.attr.segmentColor, R.attr.segmentSpacing, R.attr.segmentWidth};
    public static final int[] A0S = {R.attr.roundBottomCorners};
}
