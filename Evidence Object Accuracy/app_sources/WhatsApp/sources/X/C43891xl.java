package X;

/* renamed from: X.1xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43891xl {
    public final int A00;
    public final int A01;
    public final C43911xp A02;
    public final C43841xg A03;
    public final C43841xg A04;

    public C43891xl(C43911xp r1, C43841xg r2, C43841xg r3, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = r1;
        this.A04 = r2;
        this.A03 = r3;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UserNoticeContent{policyVersion=");
        sb.append(this.A01);
        sb.append(", banner=");
        sb.append(this.A02);
        sb.append(", modal=");
        sb.append(this.A04);
        sb.append(", blockingModal=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
