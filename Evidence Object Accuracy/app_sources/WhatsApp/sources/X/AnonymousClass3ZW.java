package X;

import android.os.SystemClock;
import android.util.Pair;
import android.widget.Toast;
import com.whatsapp.R;
import com.whatsapp.community.ManageGroupsInCommunityActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3ZW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZW implements AbstractC116755Wr {
    public final /* synthetic */ long A00;
    public final /* synthetic */ ManageGroupsInCommunityActivity A01;

    public AnonymousClass3ZW(ManageGroupsInCommunityActivity manageGroupsInCommunityActivity, long j) {
        this.A01 = manageGroupsInCommunityActivity;
        this.A00 = j;
    }

    @Override // X.AbstractC116755Wr
    public void APl(int i) {
        Log.e(C12960it.A0W(i, "ManageGroupsInCommunityActivityLinkSubgroupsProtocolHelper/error = "));
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A01;
        Toast.makeText(manageGroupsInCommunityActivity, manageGroupsInCommunityActivity.getString(R.string.something_went_wrong), 0).show();
        manageGroupsInCommunityActivity.AaN();
    }

    @Override // X.AbstractC116755Wr
    public void ARp(Set set) {
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A01;
        manageGroupsInCommunityActivity.A0E.A04(12, SystemClock.uptimeMillis() - this.A00);
        ArrayList A0l = C12960it.A0l();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            if (-1 != C12960it.A05(pair.second)) {
                Object obj = pair.first;
                AnonymousClass009.A05(obj);
                A0l.add(obj);
            }
        }
        manageGroupsInCommunityActivity.AaN();
    }

    @Override // X.AbstractC116755Wr
    public void AXX() {
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A01;
        Toast.makeText(manageGroupsInCommunityActivity, manageGroupsInCommunityActivity.getString(R.string.something_went_wrong), 0).show();
        manageGroupsInCommunityActivity.AaN();
    }
}
