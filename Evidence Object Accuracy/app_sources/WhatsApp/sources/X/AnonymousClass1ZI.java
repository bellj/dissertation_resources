package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.1ZI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZI implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100224lZ();
    public final int A00;
    public final long A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZI(int i, String str, long j) {
        this.A01 = j;
        this.A00 = i;
        this.A02 = str;
    }

    public AnonymousClass1ZI(Parcel parcel) {
        Long valueOf = Long.valueOf(parcel.readLong());
        AnonymousClass009.A05(valueOf);
        this.A01 = valueOf.longValue();
        Integer valueOf2 = Integer.valueOf(parcel.readInt());
        AnonymousClass009.A05(valueOf2);
        this.A00 = valueOf2.intValue();
        this.A02 = parcel.readString();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1ZI r7 = (AnonymousClass1ZI) obj;
            if (!(this.A01 == r7.A01 && this.A00 == r7.A00 && C29941Vi.A00(this.A02, r7.A02))) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.A01), Integer.valueOf(this.A00), this.A02});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A02);
    }
}
