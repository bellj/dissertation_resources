package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.location.PlaceInfo;

/* renamed from: X.3T8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3T8 implements AbstractC115685Sn {
    public final View A00;
    public final /* synthetic */ LocationPicker2 A01;

    public AnonymousClass3T8(LocationPicker2 locationPicker2) {
        this.A01 = locationPicker2;
        this.A00 = C12960it.A0F(locationPicker2.getLayoutInflater(), null, R.layout.place_map_info_window);
    }

    @Override // X.AbstractC115685Sn
    public View ADT(C36311jg r6) {
        View view = this.A00;
        TextView A0J = C12960it.A0J(view, R.id.place_name);
        TextView A0J2 = C12960it.A0J(view, R.id.place_address);
        if (r6.A01() instanceof PlaceInfo) {
            PlaceInfo placeInfo = (PlaceInfo) r6.A01();
            A0J.setText(placeInfo.A06);
            A0J2.setText(placeInfo.A0B);
        }
        return view;
    }
}
