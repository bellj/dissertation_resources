package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75393jm extends AnonymousClass03U {
    public ImageView A00;
    public TextView A01;

    public C75393jm(View view) {
        super(view);
        this.A01 = C12960it.A0J(view, R.id.name);
        this.A00 = C12970iu.A0L(view, R.id.icon);
    }
}
