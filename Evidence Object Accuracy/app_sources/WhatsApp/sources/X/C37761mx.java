package X;

import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/* renamed from: X.1mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37761mx implements AbstractC37731mu {
    public int A00 = 0;
    public AnonymousClass1n2 A01;
    public C28481Nj A02;
    public final int A03;
    public final int A04;
    public final long A05;
    public final long A06;
    public final C16240og A07;
    public final C19940uv A08;
    public final AnonymousClass1n2 A09;
    public final AnonymousClass1n2 A0A;
    public final C22600zL A0B;
    public final C32881ct A0C;
    public final AnonymousClass14O A0D;
    public final String A0E;
    public final String A0F;
    public final boolean A0G;

    public C37761mx(C16240og r6, C19940uv r7, AnonymousClass1n2 r8, AnonymousClass1n2 r9, C22600zL r10, AnonymousClass14O r11, String str, String str2, int i, int i2, long j, long j2, boolean z, boolean z2) {
        C32881ct r4;
        this.A0B = r10;
        this.A0A = r8;
        this.A09 = r9;
        this.A0E = str;
        this.A05 = j;
        this.A0F = str2;
        this.A0D = r11;
        this.A07 = r6;
        this.A08 = r7;
        this.A04 = i;
        this.A03 = i2;
        this.A06 = j2;
        if (i2 <= 0 || !z) {
            r4 = null;
        } else {
            r4 = new C32881ct(new Random(), i2, 3000);
        }
        this.A0C = r4;
        this.A0G = z2;
        A03(false);
    }

    public static AnonymousClass1n2 A00(String str, String str2, String str3, Collection collection, boolean z) {
        Set set;
        Set set2;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1n2 r2 = (AnonymousClass1n2) it.next();
            if (r2.A04.endsWith(".whatsapp.net") && (str == null || str.equals(r2.A08))) {
                if (z) {
                    set = r2.A0B;
                } else {
                    set = r2.A09;
                }
                if (set == null || set.contains(str2)) {
                    if (str3 == null || ((set2 = r2.A0A) != null && set2.contains(str3))) {
                        return r2;
                    }
                }
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:43:0x00ae */
    /* JADX DEBUG: Multi-variable search result rejected for r2v6, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v8, types: [long] */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006d, code lost:
        if (r4 == null) goto L_0x006f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C91004Qb A01(X.AbstractC15710nm r13, X.C15450nH r14, X.C14830m7 r15, X.C37691mq r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37761mx.A01(X.0nm, X.0nH, X.0m7, X.1mq, java.lang.String, java.lang.String, java.lang.String, boolean):X.4Qb");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        if (r37 != 0) goto L_0x0049;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C37761mx A02(X.AbstractC15710nm r25, X.C15450nH r26, X.C16240og r27, X.C14830m7 r28, X.C14850m9 r29, X.C19940uv r30, X.C22600zL r31, X.C37691mq r32, X.AnonymousClass14O r33, java.lang.String r34, java.lang.String r35, java.lang.String r36, int r37, int r38, long r39, boolean r41) {
        /*
            r0 = 256(0x100, float:3.59E-43)
            r4 = r29
            boolean r0 = r4.A07(r0)
            r7 = r37
            if (r0 == 0) goto L_0x0084
            if (r37 != 0) goto L_0x0084
            r0 = 258(0x102, float:3.62E-43)
            int r18 = r4.A02(r0)
        L_0x0014:
            r3 = 257(0x101, float:3.6E-43)
            r14 = r33
            r8 = r32
            r24 = r41
            r10 = r30
            r21 = r39
            r9 = r27
            r13 = r31
            if (r32 != 0) goto L_0x0039
            r11 = 0
            r19 = 0
            int r17 = 4 - r38
            boolean r23 = r4.A07(r3)
            r15 = r11
            r16 = r11
            r12 = r11
        L_0x0033:
            X.1mx r8 = new X.1mx
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r21, r23, r24)
            return r8
        L_0x0039:
            long r0 = r8.A04
            java.lang.String r15 = r8.A08
            int r6 = 4 - r38
            r5 = 1
            if (r5 != r7) goto L_0x006a
            int r6 = r8.A00
        L_0x0044:
            int r6 = r6 + r5
            r32 = 1
            if (r37 == 0) goto L_0x004b
        L_0x0049:
            r32 = 0
        L_0x004b:
            r30 = r35
            r29 = r34
            r31 = r36
            r27 = r28
            r28 = r8
            X.4Qb r2 = A01(r25, r26, r27, r28, r29, r30, r31, r32)
            X.1n2 r11 = r2.A01
            X.1n2 r12 = r2.A00
            java.lang.String r2 = r2.A02
            boolean r23 = r4.A07(r3)
            r19 = r0
            r17 = r6
            r16 = r2
            goto L_0x0033
        L_0x006a:
            if (r37 == 0) goto L_0x0081
            r2 = 2
            if (r2 == r7) goto L_0x0081
            java.lang.String r5 = "Mms4RouteSupplier/invalid mode = "
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r5)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            com.whatsapp.util.Log.e(r2)
            goto L_0x0049
        L_0x0081:
            int r6 = r8.A01
            goto L_0x0044
        L_0x0084:
            r18 = 0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37761mx.A02(X.0nm, X.0nH, X.0og, X.0m7, X.0m9, X.0uv, X.0zL, X.1mq, X.14O, java.lang.String, java.lang.String, java.lang.String, int, int, long, boolean):X.1mx");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r15.A07.A05 == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0040, code lost:
        if (r15.A0G == false) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(boolean r16) {
        /*
        // Method dump skipped, instructions count: 256
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37761mx.A03(boolean):void");
    }

    @Override // X.AbstractC37731mu
    public long AAo() {
        C32881ct r2 = this.A0C;
        if (r2 != null && this.A00 >= this.A04) {
            Long A00 = r2.A00();
            if (A00 != null) {
                return A00.longValue();
            }
            Log.e("Mms4RouteSupplier/getBackoffTime unexpected exponential backoff of null");
        }
        return this.A06;
    }

    @Override // X.AbstractC37731mu
    public C28481Nj ACB() {
        return this.A02;
    }

    @Override // X.AbstractC37731mu
    public void APu(boolean z, int i) {
        if (i != -1) {
            C22600zL r2 = this.A0B;
            StringBuilder sb = new StringBuilder("routeselector/onmediatransfererrororresponsecode/code ");
            sb.append(i);
            Log.i(sb.toString());
            if (i == 401 || i == 403) {
                r2.A0A();
            }
        }
        this.A00++;
        A03(z);
    }
}
