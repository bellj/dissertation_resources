package X;

import android.net.Uri;
import android.os.Build;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.135  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass135 {
    public final C16590pI A00;
    public final C15490nL A01;

    public AnonymousClass135(C16590pI r1, C15490nL r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(String str) {
        int i = Build.VERSION.SDK_INT;
        Uri parse = Uri.parse("content://com.whatsapp.provider.instrumentation");
        if (i >= 26) {
            this.A00.A00.revokeUriPermission(str, parse, 3);
        } else {
            this.A00.A00.revokeUriPermission(parse, 3);
        }
    }

    public void A01(String str, List list) {
        this.A01.A02(str);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Uri uri = (Uri) it.next();
            if ("com.whatsapp.provider.instrumentation".equals(uri.getAuthority())) {
                this.A00.A00.grantUriPermission(str, uri, 129);
            } else {
                StringBuilder sb = new StringBuilder("Unexpected authority in URI: ");
                sb.append(uri);
                throw new SecurityException(sb.toString());
            }
        }
    }
}
