package X;

import android.graphics.Paint;

/* renamed from: X.5J8  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5J8 extends AnonymousClass1WI implements AnonymousClass1WK {
    public AnonymousClass5J8() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        Paint paint = new Paint(1);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        paint.setColor(-1);
        return paint;
    }
}
