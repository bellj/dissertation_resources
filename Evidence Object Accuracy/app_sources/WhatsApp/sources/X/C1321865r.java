package X;

import android.net.Uri;
import com.whatsapp.payments.ui.BrazilViralityLinkVerifierActivity;
import com.whatsapp.payments.ui.ViralityLinkVerifierActivity;

/* renamed from: X.65r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1321865r implements AbstractC009404s {
    public final /* synthetic */ Uri A00;
    public final /* synthetic */ ViralityLinkVerifierActivity A01;

    public C1321865r(Uri uri, ViralityLinkVerifierActivity viralityLinkVerifierActivity) {
        this.A01 = viralityLinkVerifierActivity;
        this.A00 = uri;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C128405w3 r9;
        if (cls.equals(C118115bI.class)) {
            Uri uri = this.A00;
            ViralityLinkVerifierActivity viralityLinkVerifierActivity = this.A01;
            C14830m7 r4 = ((ActivityC13790kL) viralityLinkVerifierActivity).A05;
            if (!(viralityLinkVerifierActivity instanceof BrazilViralityLinkVerifierActivity)) {
                r9 = new C128405w3();
            } else {
                r9 = new C123605nQ();
            }
            C18590sh r10 = viralityLinkVerifierActivity.A0C;
            C17070qD r8 = viralityLinkVerifierActivity.A0B;
            return new C118115bI(uri, ((ActivityC13810kN) viralityLinkVerifierActivity).A07, r4, viralityLinkVerifierActivity.A08, viralityLinkVerifierActivity.A09, viralityLinkVerifierActivity.A0A, r8, r9, r10);
        }
        throw C12970iu.A0f(C12960it.A0b("Not aware about view model :", cls));
    }
}
