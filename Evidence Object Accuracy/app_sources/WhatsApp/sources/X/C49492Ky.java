package X;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.2Ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49492Ky extends AbstractC49422Kr {
    public final C251218e A00;
    public final C450720b A01;
    public final C251418g A02;
    public final List A03;

    public C49492Ky(AbstractC15710nm r7, C14850m9 r8, C16120oU r9, C251218e r10, C450720b r11, C251418g r12, List list, Map map) {
        super(r7, r8, r9, r11, map);
        this.A00 = r10;
        this.A02 = r12;
        this.A03 = list;
        this.A01 = r11;
    }

    @Override // X.AbstractC49422Kr
    public void A01(AnonymousClass1V8 r14) {
        AbstractC450820c r4;
        Message message;
        AbstractC450820c r11;
        Message obtain;
        AbstractC450820c r42;
        Message obtain2;
        C47882Dc r2;
        AnonymousClass1V8[] r22;
        AnonymousClass1W9[] r3;
        int i = 0;
        AnonymousClass1V8 A0D = r14.A0D(0);
        if (AnonymousClass1V8.A02(A0D, "offline")) {
            String A0I = A0D.A0I("count", null);
            if (A0I != null) {
                try {
                    C450720b r32 = this.A01;
                    int parseInt = Integer.parseInt(A0I);
                    StringBuilder sb = new StringBuilder("xmpp/reader/read/offline-complete count=");
                    sb.append(parseInt);
                    Log.i(sb.toString());
                    AbstractC450820c r43 = r32.A00;
                    Bundle bundle = new Bundle();
                    bundle.putInt("messageCount", parseInt);
                    r43.AYY(Message.obtain(null, 0, 15, 0, bundle));
                    for (C49432Ks r8 : this.A03) {
                        try {
                            LinkedHashMap linkedHashMap = r8.A00;
                            for (AnonymousClass1V8 r9 : linkedHashMap.values()) {
                                AnonymousClass1W9[] A0L = r9.A0L();
                                if (A0L != null) {
                                    int length = A0L.length;
                                    ArrayList arrayList = new ArrayList(length);
                                    for (AnonymousClass1W9 r1 : A0L) {
                                        if (!TextUtils.equals("offline", r1.A02)) {
                                            arrayList.add(r1);
                                        }
                                    }
                                    r3 = (AnonymousClass1W9[]) arrayList.toArray(C49432Ks.A0I);
                                } else {
                                    r3 = null;
                                }
                                r8.A01(new AnonymousClass1V8(r9.A00, r3, r9.A03));
                            }
                            r8.A02.clear();
                            linkedHashMap.clear();
                        } catch (AnonymousClass1V9 | IOException e) {
                            Log.w("WebNotificationStanzaHandler/onOfflineCompleteReceived", e);
                        }
                    }
                } catch (NumberFormatException unused) {
                }
            }
        } else {
            if (AnonymousClass1V8.A02(A0D, "dirty")) {
                C450720b r44 = this.A01;
                AnonymousClass1V8[] r23 = r14.A03;
                AnonymousClass009.A05(r23);
                boolean z = true;
                if (r23.length != 1) {
                    z = false;
                }
                AnonymousClass009.A0E(z);
                AnonymousClass1V8 r5 = r23[0];
                AnonymousClass009.A0E(AnonymousClass1V8.A02(r5, "dirty"));
                String A0I2 = r5.A0I("type", null);
                AnonymousClass009.A05(A0I2);
                if ("account_sync".equals(A0I2) && (r22 = r5.A03) != null) {
                    HashSet hashSet = new HashSet();
                    for (AnonymousClass1V8 r0 : r22) {
                        hashSet.add(r0.A00);
                    }
                    r2 = new C47882Dc(null, A0I2, hashSet);
                } else if ("syncd_app_state".equals(A0I2)) {
                    String A0I3 = r5.A0I("timestamp", null);
                    Long l = null;
                    if (A0I3 != null) {
                        try {
                            l = Long.valueOf(Long.parseLong(A0I3));
                        } catch (Exception unused2) {
                            StringBuilder sb2 = new StringBuilder("Timestamp is not a number: ");
                            sb2.append(A0I3);
                            Log.e(sb2.toString());
                        }
                    }
                    r2 = new C47882Dc(l, A0I2, Collections.emptySet());
                } else {
                    r2 = new C47882Dc(null, A0I2, Collections.emptySet());
                }
                StringBuilder sb3 = new StringBuilder("onDirty/category: ");
                sb3.append(r2.A01);
                Log.i(sb3.toString());
                r4 = r44.A00;
                message = Message.obtain(null, 0, 8, 0, r2);
            } else if (AnonymousClass1V8.A02(A0D, "streamdebug")) {
                String A0I4 = A0D.A0I("ip", null);
                String A0I5 = A0D.A0I("reconnect", null);
                String A0I6 = A0D.A0I("stanzalogcount", null);
                if (A0I6 != null) {
                    i = Integer.parseInt(A0I6);
                }
                boolean equals = "1".equals(A0I5);
                StringBuilder sb4 = new StringBuilder("xmpp/reader/read/stream/debug host=");
                sb4.append(A0I4);
                sb4.append(" reconnect=");
                sb4.append(equals);
                sb4.append(" size=");
                sb4.append(i);
                Log.i(sb4.toString());
                return;
            } else if (AnonymousClass1V8.A02(A0D, "location")) {
                UserJid userJid = (UserJid) r14.A0A(super.A01, UserJid.class, "from");
                if (A0D != null) {
                    AnonymousClass1V8 A0D2 = A0D.A0D(0);
                    if (A0D2 == null || !"enc".equals(A0D2.A00)) {
                        throw new AnonymousClass1V9("invalid location node");
                    }
                    C15930o9 A00 = AnonymousClass2SE.A00(A0D2);
                    String A0I7 = A0D.A0I("elapsed", null);
                    long j = 0;
                    if (A0I7 != null) {
                        j = (long) Integer.parseInt(A0I7);
                    }
                    this.A01.A00(userJid, A00, j);
                    return;
                }
                throw new AnonymousClass1V9("Missing location node");
            } else if (AnonymousClass1V8.A02(A0D, "sonar")) {
                String A0H = A0D.A0H("url");
                C450720b r24 = this.A01;
                StringBuilder sb5 = new StringBuilder("xmpp/reader/read/sonar/url = ");
                sb5.append(A0H);
                Log.i(sb5.toString());
                r4 = r24.A00;
                message = Message.obtain(null, 0, 101, 0, A0H);
            } else if (AnonymousClass1V8.A02(A0D, "edge_routing")) {
                AnonymousClass1V8 A0E = A0D.A0E("routing_info");
                if (!(A0E == null || TextUtils.isEmpty(A0E.A0G()))) {
                    this.A02.A00(A0E.A01);
                    return;
                }
                return;
            } else if (AnonymousClass1V8.A02(A0D, "fbip")) {
                String A0G = A0D.A0G();
                if (A0G != null) {
                    this.A00.A01(A0G.split(","));
                    return;
                }
                return;
            } else {
                if (AnonymousClass1V8.A02(A0D, "client_expiration")) {
                    long A08 = A0D.A08("t", -1);
                    C450720b r45 = this.A01;
                    if (A08 != -1) {
                        A08 *= 1000;
                    }
                    Log.i("xmpp/reader/on-expiration-change");
                    r42 = r45.A00;
                    Bundle bundle2 = new Bundle();
                    bundle2.putLong("timestampMs", A08);
                    obtain2 = Message.obtain(null, 0, 159, 0, bundle2);
                } else if (AnonymousClass1V8.A02(A0D, "attestation")) {
                    String A0H2 = A0D.A0H("nonce");
                    String A0H3 = A0D.A0H("key");
                    C450720b r12 = this.A01;
                    Log.i("xmpp/reader/on-attestation-request");
                    r42 = r12.A00;
                    obtain2 = Message.obtain(null, 0, 179, 0);
                    Bundle data = obtain2.getData();
                    data.putString("nonce", A0H2);
                    data.putString("apiKey", A0H3);
                } else if (AnonymousClass1V8.A02(A0D, "safetynet")) {
                    AnonymousClass1V8[] r10 = A0D.A03;
                    if (r10 != null) {
                        int length2 = r10.length;
                        while (i < length2) {
                            AnonymousClass1V8 r13 = r10[i];
                            if (AnonymousClass1V8.A02(r13, "attestation")) {
                                String A0H4 = r13.A0H("nonce");
                                String A0H5 = r13.A0H("key");
                                C450720b r15 = this.A01;
                                Log.i("xmpp/reader/on-attestation-request");
                                r11 = r15.A00;
                                obtain = Message.obtain(null, 0, 179, 0);
                                Bundle data2 = obtain.getData();
                                data2.putString("nonce", A0H4);
                                data2.putString("apiKey", A0H5);
                            } else if (AnonymousClass1V8.A02(r13, "verify_apps")) {
                                int A06 = r13.A06(r13.A0H("count"), "count");
                                C450720b r16 = this.A01;
                                Log.i("xmpp/reader/on-safetynet-verifyapps-request");
                                r11 = r16.A00;
                                obtain = Message.obtain(null, 0, 223, 0);
                                obtain.getData().putInt("maxAppsCount", A06);
                            } else {
                                i++;
                            }
                            r11.AYY(obtain);
                            i++;
                        }
                        return;
                    }
                    return;
                } else if (AnonymousClass1V8.A02(A0D, "peer_device_presence")) {
                    C450720b r52 = this.A01;
                    boolean equalsIgnoreCase = "true".equalsIgnoreCase(A0D.A0H("presence"));
                    StringBuilder sb6 = new StringBuilder("xmpp/reader/on-peer-device-presence: ");
                    sb6.append(equalsIgnoreCase);
                    Log.i(sb6.toString());
                    r4 = r52.A00;
                    message = Message.obtain(null, 0, 215, equalsIgnoreCase ? 1 : 0);
                } else {
                    C14850m9 r25 = super.A02;
                    if (!r25.A07(366) && AnonymousClass1V8.A02(A0D, "notice")) {
                        List<AnonymousClass1V8> A0J = r14.A0J("notice");
                        ArrayList arrayList2 = new ArrayList();
                        for (AnonymousClass1V8 r26 : A0J) {
                            arrayList2.add(new C43831xf(r26.A06(r26.A0H("id"), "id"), r26.A06(r26.A0H("stage"), "stage"), r26.A06(r26.A0H("version"), "version"), 1000 * r26.A09(r26.A0H("t"), "t")));
                        }
                        C450720b r17 = this.A01;
                        Log.i("xmpp/reader/on-user-notice-received");
                        r17.A00.AYY(Message.obtain(null, 0, 216, 0, arrayList2));
                        return;
                    } else if (r25.A07(877) && AnonymousClass1V8.A02(A0D, "tos")) {
                        this.A01.A01(A0D, null, 249);
                        return;
                    } else {
                        return;
                    }
                }
                r42.AYY(obtain2);
                return;
            }
            r4.AYY(message);
        }
    }
}
