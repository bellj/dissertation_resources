package X;

/* renamed from: X.5Gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113195Gn extends RuntimeException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C113195Gn(int r3, int r4, int r5, int r6) {
        /*
            r2 = this;
            java.lang.String r0 = "Pool hard cap violation? Hard cap = "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r3)
            java.lang.String r0 = " Used size = "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " Free size = "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = " Request size = "
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r6)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113195Gn.<init>(int, int, int, int):void");
    }
}
