package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.20k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C451620k extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C451620k(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C18610sj r5) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A00.AV3(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        this.A00.AVA(r2);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r4) {
        this.A01.A06(new AbstractC451720l() { // from class: X.56G
            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                AnonymousClass1FK r1 = AnonymousClass1FK.this;
                AnonymousClass46O r0 = new AnonymousClass46O();
                r0.A00 = list;
                r1.AVB(r0);
            }
        }, r4, true);
    }
}
