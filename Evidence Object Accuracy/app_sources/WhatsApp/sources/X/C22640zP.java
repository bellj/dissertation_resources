package X;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.widget.Toast;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.R;
import com.whatsapp.community.SubgroupPileView;
import com.whatsapp.jid.GroupJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0zP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22640zP {
    public boolean A00 = false;
    public final AnonymousClass12P A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C26681Ek A04;
    public final C15550nR A05;
    public final C14830m7 A06;
    public final C14820m6 A07;
    public final C19990v2 A08;
    public final C241214h A09;
    public final C25621Ac A0A;
    public final C15600nX A0B;
    public final C26721Eo A0C;
    public final AnonymousClass15K A0D;
    public final AnonymousClass1CA A0E;
    public final AnonymousClass1CB A0F;
    public final C14850m9 A0G;
    public final AnonymousClass1E8 A0H;
    public final AnonymousClass1E5 A0I;
    public final C15860o1 A0J;
    public final AbstractC14440lR A0K;

    public C22640zP(AnonymousClass12P r2, C14900mE r3, C15570nT r4, C26681Ek r5, C15550nR r6, C14830m7 r7, C14820m6 r8, C19990v2 r9, C241214h r10, C25621Ac r11, C15600nX r12, C26721Eo r13, AnonymousClass15K r14, AnonymousClass1CA r15, AnonymousClass1CB r16, C14850m9 r17, AnonymousClass1E8 r18, AnonymousClass1E5 r19, C15860o1 r20, AbstractC14440lR r21) {
        this.A06 = r7;
        this.A0G = r17;
        this.A02 = r3;
        this.A03 = r4;
        this.A0K = r21;
        this.A08 = r9;
        this.A01 = r2;
        this.A0D = r14;
        this.A05 = r6;
        this.A0A = r11;
        this.A0J = r20;
        this.A0I = r19;
        this.A09 = r10;
        this.A07 = r8;
        this.A0H = r18;
        this.A0C = r13;
        this.A0E = r15;
        this.A0B = r12;
        this.A0F = r16;
        this.A04 = r5;
    }

    public int A00(C15580nU r6) {
        AnonymousClass15K r1 = this.A0D;
        int i = 0;
        String[] strArr = {r6.getRawString()};
        C16310on A01 = r1.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM subgroup_info subgroups INNER JOIN group_relationship relationship ON subgroups.subgroup_raw_jid = relationship.subgroup_raw_id WHERE relationship.parent_raw_jid = ?", strArr);
            if (A09.moveToNext()) {
                i = A09.getInt(0);
            }
            A09.close();
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A01(C15580nU r6) {
        List<AnonymousClass1OU> A01 = this.A0D.A01(r6);
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1OU r2 : A01) {
            if (this.A0B.A0C(r2.A02)) {
                arrayList.add(r2);
            }
        }
        return arrayList;
    }

    public void A02(int i) {
        SharedPreferences.Editor editor;
        SharedPreferences.Editor editor2;
        String str;
        SharedPreferences sharedPreferences;
        if (i != 0) {
            if (i == 1) {
                this.A00 = false;
                AnonymousClass1CA r1 = this.A0E;
                editor2 = r1.A00().edit();
                sharedPreferences = r1.A00();
                str = "community_tab_to_home_views";
            } else if (i == 2) {
                this.A00 = false;
                AnonymousClass1CA r12 = this.A0E;
                editor2 = r12.A00().edit();
                sharedPreferences = r12.A00();
                str = "community_tab_group_navigation";
            } else if (i != 3) {
                this.A00 = false;
                StringBuilder sb = new StringBuilder("CommunityChatManagerincrementTabActionLoggingCount/action type is not recognized/action type = ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            } else if (this.A00) {
                AnonymousClass1CA r13 = this.A0E;
                r13.A00().edit().putInt("community_tab_no_action_view", r13.A00().getInt("community_tab_no_action_view", 0) + 1).apply();
                this.A00 = false;
                return;
            } else {
                return;
            }
            editor = editor2.putInt(str, sharedPreferences.getInt(str, 0) + 1);
        } else {
            this.A00 = true;
            AnonymousClass1CA r14 = this.A0E;
            editor = r14.A00().edit().putInt("community_tab_daily_views", r14.A00().getInt("community_tab_daily_views", 0) + 1);
        }
        editor.apply();
    }

    public void A03(Context context, GroupJid groupJid) {
        if (this.A08.A0F(groupJid)) {
            Toast.makeText(context, (int) R.string.community_no_longer_exists, 0).show();
            return;
        }
        this.A01.A06(context, C14960mK.A0H(context, groupJid));
    }

    public void A04(GroupJid groupJid, int i) {
        String str;
        groupJid.getRawString();
        AnonymousClass1CB r1 = this.A0F;
        long A01 = r1.A00.A01(groupJid);
        if (i == 0) {
            str = "home_view_count";
        } else if (i == 1) {
            str = "home_group_navigation_count";
        } else if (i == 2) {
            str = "home_group_discovery_count";
        } else if (i == 3) {
            str = "home_group_join_count";
        } else {
            StringBuilder sb = new StringBuilder("CommunityActionLoggingStore/getActionColumnName/action type is wrong. Action Type = ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        C16310on A02 = r1.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r8 = A02.A03;
            StringBuilder sb2 = new StringBuilder("UPDATE community_home_action_logging SET ");
            sb2.append(str);
            sb2.append(" = ");
            sb2.append(str);
            sb2.append(" + ?");
            sb2.append(" WHERE ");
            sb2.append("jid_row_id");
            sb2.append(" = ?");
            AnonymousClass1YE A0A = r8.A0A(sb2.toString());
            A0A.A01(1, 1);
            A0A.A01(2, A01);
            if (A0A.A00.executeUpdateDelete() == 0) {
                ContentValues contentValues = new ContentValues(2);
                contentValues.put("jid_row_id", Long.valueOf(A01));
                contentValues.put(str, (Integer) 1);
                r8.A02(contentValues, "community_home_action_logging");
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(C15580nU r6, boolean z) {
        boolean z2;
        AnonymousClass1PE A06 = this.A08.A06(r6);
        if (A06 != null) {
            C26721Eo r3 = this.A0C;
            synchronized (A06) {
                z2 = A06.A0g;
            }
            if (z2 != z) {
                synchronized (A06) {
                    A06.A0g = z;
                }
                r3.A00.A01(new RunnableBRunnable0Shape4S0200000_I0_4(r3, 20, A06), 60);
            }
        }
    }

    public boolean A06() {
        if (!A07() || !this.A0G.A07(1173)) {
            return false;
        }
        this.A03.A08();
        return true;
    }

    public boolean A07() {
        if (!this.A0G.A07(982)) {
            return false;
        }
        this.A03.A08();
        return true;
    }

    public boolean A08(AbstractC14640lm r5) {
        if (this.A07.A00.getBoolean("about_community_nux", false) || !(r5 instanceof C15580nU)) {
            return false;
        }
        C19990v2 r2 = this.A08;
        GroupJid groupJid = (GroupJid) r5;
        if (r2.A02(groupJid) == 1 || r2.A02(groupJid) == 2 || r2.A02(groupJid) == 3) {
            return true;
        }
        return false;
    }

    public boolean A09(C15580nU r7) {
        C19990v2 r4;
        AnonymousClass1PE A06;
        boolean z;
        if (!(r7 == null || !this.A0G.A07(1864) || (A06 = (r4 = this.A08).A06(r7)) == null)) {
            C15600nX r1 = this.A0B;
            if (r1.A0D(r7) && !r1.A0G(r7) && r4.A02(r7) == 1) {
                synchronized (A06) {
                    z = A06.A0g;
                }
                if (!z) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean A0A(C15580nU r3) {
        return A07() && SubgroupPileView.A00(this.A08, this.A0B, r3);
    }
}
