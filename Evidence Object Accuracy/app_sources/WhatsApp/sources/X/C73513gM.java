package X;

import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.support.faq.FaqItemActivity;

/* renamed from: X.3gM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73513gM extends ClickableSpan {
    public final /* synthetic */ FaqItemActivity A00;
    public final /* synthetic */ Runnable A01;

    public C73513gM(FaqItemActivity faqItemActivity, Runnable runnable) {
        this.A00 = faqItemActivity;
        this.A01 = runnable;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        this.A01.run();
    }
}
