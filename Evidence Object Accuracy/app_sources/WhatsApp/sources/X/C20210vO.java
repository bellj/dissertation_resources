package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.0vO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20210vO {
    public final C18460sU A00;
    public final C16490p7 A01;

    public C20210vO(C18460sU r1, C16490p7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public AnonymousClass1YT A00(C30381Xe r27) {
        if (((AbstractC30391Xf) r27).A00) {
            return r27.A14();
        }
        C16310on A01 = this.A01.get();
        try {
            C16330op r7 = A01.A03;
            Cursor A09 = r7.A09("SELECT _id, timestamp, video_call, group_jid_row_id, is_joinable_group_call FROM missed_call_logs WHERE message_row_id = ? ORDER BY timestamp ASC", new String[]{Long.toString(r27.A11)});
            if (A09.moveToNext()) {
                Cursor A092 = r7.A09("SELECT _id, jid, call_result FROM missed_call_log_participant WHERE call_logs_row_id = ? ORDER BY _id ASC", new String[]{Long.toString(A09.getLong(A09.getColumnIndexOrThrow("_id")))});
                try {
                    long j = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                    long j2 = A09.getLong(A09.getColumnIndexOrThrow("timestamp"));
                    boolean z = false;
                    if (A09.getInt(A09.getColumnIndexOrThrow("video_call")) > 0) {
                        z = true;
                    }
                    int i = A09.getInt(A09.getColumnIndexOrThrow("group_jid_row_id"));
                    boolean z2 = false;
                    if (A09.getInt(A09.getColumnIndexOrThrow("is_joinable_group_call")) > 0) {
                        z2 = true;
                    }
                    ArrayList arrayList = new ArrayList();
                    while (A092.moveToNext()) {
                        long j3 = A092.getLong(A092.getColumnIndexOrThrow("_id"));
                        UserJid nullable = UserJid.getNullable(A092.getString(A092.getColumnIndexOrThrow("jid")));
                        if (C15380n4.A0L(nullable)) {
                            arrayList.add(new AnonymousClass1YV(nullable, A092.getInt(A092.getColumnIndexOrThrow("call_result")), j3));
                        }
                    }
                    AnonymousClass1YT A00 = AnonymousClass1YT.A00(GroupJid.of(this.A00.A03((long) i)), r27, arrayList, 0, 0, 2, j, j2, 0, z, true, z2);
                    A092.close();
                    A09.close();
                    A01.close();
                    return A00;
                } catch (Throwable th) {
                    if (A092 != null) {
                        try {
                            A092.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            } else {
                A09.close();
                A01.close();
                return null;
            }
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public void A01(C30381Xe r19) {
        long j;
        if (r19.A11 != -1) {
            try {
                try {
                    C16490p7 r3 = this.A01;
                    C16310on A02 = r3.A02();
                    try {
                        for (AnonymousClass1YT r7 : r19.A15()) {
                            if (r7.A0C()) {
                                ContentValues contentValues = new ContentValues();
                                if (r7.A02() != -1) {
                                    contentValues.put("_id", Long.valueOf(r7.A02()));
                                }
                                contentValues.put("message_row_id", Long.valueOf(r19.A11));
                                contentValues.put("timestamp", Long.valueOf(r7.A09));
                                contentValues.put("video_call", Boolean.valueOf(r7.A0H));
                                GroupJid groupJid = r7.A04;
                                if (groupJid != null) {
                                    j = this.A00.A01(groupJid);
                                } else {
                                    j = 0;
                                }
                                contentValues.put("group_jid_row_id", Long.valueOf(j));
                                contentValues.put("is_joinable_group_call", Boolean.valueOf(r7.A0G));
                                r7.A07(A02.A03.A04(contentValues, "missed_call_logs"));
                                r7.A05();
                                if (r7.A02() != -1) {
                                    try {
                                        try {
                                            A02 = r3.A02();
                                            try {
                                                for (Object obj : r7.A04()) {
                                                    AnonymousClass1YV r9 = (AnonymousClass1YV) obj;
                                                    if (r9.A01()) {
                                                        ContentValues contentValues2 = new ContentValues();
                                                        contentValues2.put("call_logs_row_id", Long.valueOf(r7.A02()));
                                                        if (r9.A00() != -1) {
                                                            contentValues2.put("_id", Long.valueOf(r9.A00()));
                                                        }
                                                        contentValues2.put("jid", r9.A02.getRawString());
                                                        contentValues2.put("call_result", Integer.valueOf(r9.A00));
                                                        long A04 = A02.A03.A04(contentValues2, "missed_call_log_participant");
                                                        synchronized (obj) {
                                                            r9.A01 = A04;
                                                        }
                                                        synchronized (obj) {
                                                            r9.A03 = false;
                                                        }
                                                    }
                                                }
                                                synchronized (r7) {
                                                }
                                                A02.close();
                                            } catch (Throwable th) {
                                                throw th;
                                                break;
                                            }
                                        } catch (Error | RuntimeException e) {
                                            Log.e(e);
                                            throw e;
                                        }
                                    } catch (SQLiteDatabaseCorruptException e2) {
                                        Log.e("MissedCallLogStore/insertOfUpdateCallLogParticipants", e2);
                                    }
                                } else {
                                    throw new IllegalArgumentException("CallLog row_id is not set");
                                }
                            }
                        }
                        A02.close();
                    } finally {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                    }
                } catch (SQLiteDatabaseCorruptException e3) {
                    Log.e("MissedCallLogStore/insertOfUpdateCallLogs", e3);
                }
            } catch (Error | RuntimeException e4) {
                Log.e(e4);
                throw e4;
            }
        } else {
            throw new IllegalArgumentException("message.row_id is not set");
        }
    }
}
