package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119745f2 extends AbstractC30851Zb {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(10);
    public int A00;
    public int A01 = 1;
    public String A02;
    public String A03;
    public boolean A04;

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A06() {
        return null;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r4, AnonymousClass1V8 r5, int i) {
        this.A05 = r5.A0I("country", null);
        this.A06 = r5.A0I("credential-id", null);
        super.A02 = AnonymousClass1ZS.A00(r5.A0I("account-number", null), "bankAccountNumber");
        super.A01 = AnonymousClass1ZS.A00(r5.A0I("bank-name", null), "bankName");
        String A0I = r5.A0I("code", null);
        this.A02 = A0I;
        if (A0I == null) {
            this.A02 = r5.A0I("bank-code", null);
        }
        this.A00 = AbstractC28901Pl.A00(r5.A0I("verification-status", null));
        this.A03 = r5.A0I("short-name", null);
        super.A03 = r5.A0I("bank-image", null);
        this.A04 = "1".equals(r5.A0I("accept-savings", null));
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: BrazilBankAccountMethodData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        Object obj;
        try {
            JSONObject A0C = A0C();
            A0C.put("v", this.A01);
            AnonymousClass1ZR r0 = super.A01;
            if (r0 == null || AnonymousClass1ZS.A02(r0)) {
                obj = "";
            } else {
                obj = super.A01.A00;
            }
            A0C.put("bankName", obj);
            A0C.put("bankCode", this.A02);
            A0C.put("verificationStatus", this.A00);
            return A0C.toString();
        } catch (JSONException e) {
            Log.w(C12960it.A0b("PAY: BrazilBankAccountMethodData toDBString threw: ", e));
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                A0D(A05);
                this.A01 = A05.optInt("v", 1);
                String optString = A05.optString("bankName");
                super.A01 = C117305Zk.A0I(C117305Zk.A0J(), optString.getClass(), optString, "bankName");
                this.A02 = A05.optString("bankCode");
                this.A00 = A05.optInt("verificationStatus");
            } catch (JSONException e) {
                Log.w(C12960it.A0b("PAY: BrazilBankAccountMethodData fromDBString threw: ", e));
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C17930rd A00 = C17930rd.A00("BR");
        if (A00 == null) {
            return null;
        }
        String str = this.A06;
        C30861Zc r4 = new C30861Zc(A00, 0, 0, super.A00, -1);
        r4.A0A = str;
        r4.A0A("");
        r4.A0B = (String) C117295Zj.A0R(super.A01);
        r4.A0D = null;
        r4.A08 = this;
        r4.A04 = this.A00;
        return r4;
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return new LinkedHashSet(Collections.singletonList(C30771Yt.A04));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ credentialId: ");
        A0k.append(this.A06);
        A0k.append("maskedAccountNumber: ");
        A0k.append(super.A02);
        A0k.append(" bankName: ");
        A0k.append(super.A01);
        A0k.append(" bankCode: ");
        A0k.append(this.A02);
        A0k.append(" verificationStatus: ");
        A0k.append(this.A00);
        A0k.append(" bankShortName: ");
        A0k.append(this.A03);
        A0k.append(" acceptSavings: ");
        A0k.append(this.A04);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(super.A01, i);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A03);
        parcel.writeString(super.A03);
        parcel.writeInt(this.A04 ? 1 : 0);
        parcel.writeString(this.A05);
        parcel.writeString(this.A06);
        parcel.writeParcelable(super.A02, i);
        parcel.writeLong(super.A00);
    }
}
