package X;

import java.util.List;

/* renamed from: X.3n6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C77323n6 extends AbstractC107814xz {
    public final AnonymousClass5QN A00;
    public final AnonymousClass5Xd A01;
    public final AnonymousClass1Mr A02;

    public C77323n6(C100554m6 r2, AnonymousClass5QN r3, AnonymousClass5Xd r4, List list, int[] iArr) {
        super(r2, iArr);
        this.A00 = r3;
        this.A02 = AnonymousClass1Mr.copyOf(list);
        this.A01 = r4;
    }

    public static void A00(List list, long[] jArr) {
        long j = 0;
        for (long j2 : jArr) {
            j += j2;
        }
        for (int i = 0; i < list.size(); i++) {
            AnonymousClass29A r5 = (AnonymousClass29A) list.get(i);
            if (r5 != null) {
                r5.add((Object) new C92514Wf(j, jArr[i]));
            }
        }
    }
}
