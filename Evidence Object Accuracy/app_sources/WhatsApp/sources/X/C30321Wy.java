package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1Wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30321Wy extends C30331Wz {
    public UserJid A00;

    public C30321Wy(AnonymousClass1IS r7, long j) {
        super(r7, (byte) 64, 8, j);
    }

    public C30321Wy(AnonymousClass1IS r2, C30321Wy r3, long j) {
        super(r2, r3, j);
        this.A00 = r3.A00;
    }

    @Override // X.C30331Wz
    public void A14(AnonymousClass1G9 r2) {
        super.A14(r2);
        r2.A08(false);
        UserJid A0C = A0C();
        if (A0C != null) {
            r2.A06(A0C.getRawString());
        }
    }
}
