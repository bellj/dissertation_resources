package X;

/* renamed from: X.5MO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MO extends AnonymousClass1TM {
    public C114595Mi[] A00;

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return new AnonymousClass5NZ(this.A00);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: X.5Mi[] */
    /* JADX WARN: Multi-variable type inference failed */
    public AnonymousClass5MO(AbstractC114775Na r5) {
        if (r5.A0B() >= 1) {
            this.A00 = new C114595Mi[r5.A0B()];
            for (int i = 0; i != r5.A0B(); i++) {
                C114595Mi[] r2 = this.A00;
                AnonymousClass1TN A0D = r5.A0D(i);
                if (!(A0D instanceof C114595Mi)) {
                    A0D = A0D != null ? new C114595Mi(AbstractC114775Na.A04(A0D)) : null;
                }
                r2[i] = A0D;
            }
            return;
        }
        throw C12970iu.A0f("sequence may not be empty");
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AuthorityInformationAccess: Oid(");
        A0k.append(this.A00[0].A00.A01);
        return C12960it.A0d(")", A0k);
    }
}
