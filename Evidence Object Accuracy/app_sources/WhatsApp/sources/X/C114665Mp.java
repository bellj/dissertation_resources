package X;

/* renamed from: X.5Mp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114665Mp extends AnonymousClass1TM {
    public static final AnonymousClass5NG A06 = new AnonymousClass5NG(0);
    public AnonymousClass5NE A00;
    public AnonymousClass5NG A01;
    public AbstractC114775Na A02;
    public C114745Mx A03;
    public AnonymousClass5MX A04;
    public boolean A05;

    public C114665Mp(AbstractC114775Na r6) {
        AnonymousClass5N2 A00;
        C114745Mx r1;
        AnonymousClass5NH A04;
        C114745Mx r3;
        int i = 0;
        if (!(r6.A0D(0) instanceof AnonymousClass5NU) || ((AnonymousClass5NU) r6.A0D(0)).A00 != 0) {
            this.A01 = A06;
        } else {
            this.A05 = true;
            this.A01 = AnonymousClass5NG.A00(AnonymousClass5NU.A00((AnonymousClass5NU) r6.A0D(0)));
            i = 1;
        }
        int i2 = i + 1;
        AnonymousClass1TN A0D = r6.A0D(i);
        if (A0D instanceof C114745Mx) {
            r3 = (C114745Mx) A0D;
        } else {
            if (A0D instanceof AnonymousClass5N5) {
                A04 = (AnonymousClass5NH) A0D;
            } else {
                if (A0D instanceof AnonymousClass5NU) {
                    AnonymousClass5NU r32 = (AnonymousClass5NU) A0D;
                    if (r32.A00 == 1) {
                        A00 = AnonymousClass5N2.A00(AbstractC114775Na.A05(r32, true));
                    } else {
                        A04 = AnonymousClass5NH.A04(r32, true);
                    }
                } else {
                    A00 = AnonymousClass5N2.A00(A0D);
                }
                r1 = new C114745Mx(A00);
                r3 = r1;
            }
            r1 = new C114745Mx(A04);
            r3 = r1;
        }
        this.A03 = r3;
        int i3 = i2 + 1;
        this.A00 = AnonymousClass5NE.A01(r6.A0D(i2));
        int i4 = i3 + 1;
        this.A02 = (AbstractC114775Na) r6.A0D(i3);
        if (r6.A0B() > i4) {
            this.A04 = AnonymousClass5MX.A01(AbstractC114775Na.A05((AnonymousClass5NU) r6.A0D(i4), true));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(5);
        if (this.A05 || !this.A01.A04(A06)) {
            C94954co.A02(this.A01, r3, 0, true);
        }
        r3.A06(this.A03);
        r3.A06(this.A00);
        r3.A06(this.A02);
        AnonymousClass5MX r0 = this.A04;
        if (r0 != null) {
            C94954co.A03(r0, r3, true);
        }
        return new AnonymousClass5NZ(r3);
    }
}
