package X;

import android.util.SparseIntArray;

/* renamed from: X.3IC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IC {
    public static final SparseIntArray A00 = new SparseIntArray();
    public static final SparseIntArray A01 = new SparseIntArray();

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01ab, code lost:
        if (r5 == r1.AK0(r12, 0, r12.length())) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01b6, code lost:
        if (r1.AK0(r12, 0, r12.length()) != false) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01c1, code lost:
        if (r5 == r1.AK0(r12, 0, r12.length())) goto L_0x01c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01a0, code lost:
        if (r1.AK0(r12, 0, r12.length()) != false) goto L_0x01c3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.text.Layout A00(android.content.Context r10, X.C71163cU r11, java.lang.CharSequence r12, int r13) {
        /*
        // Method dump skipped, instructions count: 542
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3IC.A00(android.content.Context, X.3cU, java.lang.CharSequence, int):android.text.Layout");
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0230  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C106194vJ A01(X.C92304Vj r23, X.AnonymousClass2k8 r24, X.C71163cU r25, java.lang.CharSequence r26, int r27, int r28) {
        /*
        // Method dump skipped, instructions count: 644
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3IC.A01(X.4Vj, X.2k8, X.3cU, java.lang.CharSequence, int, int):X.4vJ");
    }
}
