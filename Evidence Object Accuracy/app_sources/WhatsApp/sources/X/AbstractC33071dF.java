package X;

import android.view.ViewGroup;

/* renamed from: X.1dF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC33071dF {
    int ADJ(int i);

    boolean AJU(int i);

    void ANH(AnonymousClass03U v, int i);

    AnonymousClass03U AOl(ViewGroup viewGroup, int i);

    boolean AdR();

    int getItemViewType(int i);
}
