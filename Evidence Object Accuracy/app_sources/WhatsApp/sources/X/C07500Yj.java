package X;

import android.util.Log;
import android.view.View;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.0Yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07500Yj implements AnonymousClass02B {
    public final /* synthetic */ DialogFragment A00;

    public C07500Yj(DialogFragment dialogFragment) {
        this.A00 = dialogFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        if (obj != null) {
            DialogFragment dialogFragment = this.A00;
            if (dialogFragment.A0E) {
                View A05 = dialogFragment.A05();
                if (A05.getParent() != null) {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                } else if (dialogFragment.A03 != null) {
                    if (AnonymousClass01F.A01(3)) {
                        StringBuilder sb = new StringBuilder("DialogFragment ");
                        sb.append(this);
                        sb.append(" setting the content view on ");
                        sb.append(dialogFragment.A03);
                        Log.d("FragmentManager", sb.toString());
                    }
                    dialogFragment.A03.setContentView(A05);
                }
            }
        }
    }
}
