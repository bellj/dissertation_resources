package X;

import android.util.Pair;
import com.whatsapp.payments.ui.NoviPayBloksActivity;

/* renamed from: X.5ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117845ao extends AnonymousClass0PW {
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ AnonymousClass3FE A01;
    public final /* synthetic */ AnonymousClass61D A02;
    public final /* synthetic */ NoviPayBloksActivity A03;
    public final /* synthetic */ String A04;

    public C117845ao(Pair pair, AnonymousClass3FE r2, AnonymousClass61D r3, NoviPayBloksActivity noviPayBloksActivity, String str) {
        this.A03 = noviPayBloksActivity;
        this.A02 = r3;
        this.A04 = str;
        this.A00 = pair;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass0PW
    public void A00() {
        this.A03.A30(null);
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r6) {
        this.A03.A2w(this.A00, this.A01, this.A02, this.A04);
    }
}
