package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/* renamed from: X.5z5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C130275z5 {
    public static CharSequence A00(Context context, AnonymousClass018 r8, AnonymousClass63Y r9, C1315863i r10) {
        AbstractC30791Yv r5 = r9.A02;
        Object[] objArr = new Object[2];
        int i = 0;
        objArr[0] = r5.AAB(r8, BigDecimal.ONE, 2);
        AbstractC30791Yv r2 = r9.A01;
        BigDecimal bigDecimal = r10.A03.A05;
        if (!BigDecimal.ONE.equals(bigDecimal)) {
            i = 4;
        }
        return r5.AA7(context, C12960it.A0X(context, C117305Zk.A0k(r8, r2, bigDecimal, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate));
    }

    public static BigDecimal A01(BigDecimal bigDecimal, BigDecimal bigDecimal2) {
        return bigDecimal.multiply(bigDecimal2, new MathContext(bigDecimal.toPlainString().split("\\.")[0].length() + 4, RoundingMode.HALF_EVEN));
    }
}
