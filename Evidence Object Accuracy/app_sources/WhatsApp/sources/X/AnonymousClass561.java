package X;

import com.whatsapp.registration.EULA;

/* renamed from: X.561  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass561 implements AbstractC18900tF {
    public final /* synthetic */ EULA A00;

    public AnonymousClass561(EULA eula) {
        this.A00 = eula;
    }

    @Override // X.AbstractC18900tF
    public void ASC() {
        this.A00.A0U = true;
    }
}
