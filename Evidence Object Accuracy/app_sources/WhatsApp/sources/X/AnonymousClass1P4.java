package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1P4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1P4 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100404lr();
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1P4(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public AnonymousClass1P4(String str) {
        AnonymousClass009.A05(str);
        this.A01 = str;
        AnonymousClass009.A05("set");
        this.A00 = "set";
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
