package X;

/* renamed from: X.0Ji  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03840Ji {
    ENQUEUED,
    RUNNING,
    SUCCEEDED,
    FAILED,
    BLOCKED,
    CANCELLED;

    public boolean A00() {
        return this == SUCCEEDED || this == FAILED || this == CANCELLED;
    }
}
