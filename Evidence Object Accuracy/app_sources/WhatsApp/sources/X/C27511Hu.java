package X;

import java.security.AccessController;
import java.security.Provider;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27511Hu extends Provider implements AbstractC27521Hv {
    public static final Map A00 = new HashMap();
    public static final AnonymousClass1T5 A01 = new AnonymousClass1T5();
    public static final String[] A02 = {"MD5", "SHA1", "SHA256", "SHA384"};
    public static final String[] A03 = {"AES", "XSalsa20"};
    public static final String[] A04 = {"PBEPBKDF2"};

    public C27511Hu() {
        super("SC", 1.68d, "SpongyCastle Security Provider v1.68");
        AccessController.doPrivileged(new AnonymousClass1T9(this));
    }

    public final void A00(String str, String[] strArr) {
        int i = 0;
        while (i != strArr.length) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(strArr[i]);
            sb.append("$Mappings");
            Class A002 = AnonymousClass1TA.A00(C27511Hu.class, sb.toString());
            if (A002 != null) {
                try {
                    ((AnonymousClass1TD) A002.newInstance()).A00(this);
                    continue;
                } catch (Exception e) {
                    StringBuilder sb2 = new StringBuilder("cannot create instance of ");
                    sb2.append(str);
                    sb2.append(strArr[i]);
                    sb2.append("$Mappings : ");
                    sb2.append(e);
                    throw new InternalError(sb2.toString());
                }
            }
            i++;
        }
    }

    @Override // X.AbstractC27521Hv
    public void A5d(String str, String str2) {
        if (!containsKey(str)) {
            put(str, str2);
            return;
        }
        StringBuilder sb = new StringBuilder("duplicate provider key (");
        sb.append(str);
        sb.append(") found");
        throw new IllegalStateException(sb.toString());
    }
}
