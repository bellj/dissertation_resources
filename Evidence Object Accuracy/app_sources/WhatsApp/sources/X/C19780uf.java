package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0uf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19780uf {
    public Map A00;
    public final C14830m7 A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C18460sU A04;
    public final C16490p7 A05;
    public final C21390xL A06;
    public final C19770ue A07;

    public C19780uf(C14830m7 r1, C16510p9 r2, C19990v2 r3, C18460sU r4, C16490p7 r5, C21390xL r6, C19770ue r7) {
        this.A01 = r1;
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
        this.A06 = r6;
        this.A07 = r7;
        this.A05 = r5;
    }

    public static final void A00(C16310on r4, AbstractC14640lm r5, byte b, int i) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("jid", r5.getRawString());
        contentValues.put("type", Byte.valueOf(b));
        contentValues.put("message_count", Integer.valueOf(i));
        if (r4.A03.A02(contentValues, "frequents") == -1) {
            StringBuilder sb = new StringBuilder("frequentMessageStore/insertFrequents/failed jid=");
            sb.append(r5);
            sb.append(" type=");
            sb.append((int) b);
            Log.e(sb.toString());
        }
    }

    public List A01(AbstractC33941fK r8) {
        Map A02 = A02();
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : A02.entrySet()) {
            AbstractC14640lm r4 = ((C33931fJ) entry.getKey()).A01;
            byte b = ((C33931fJ) entry.getKey()).A00;
            int intValue = ((Number) entry.getValue()).intValue();
            if (r8 != null) {
                intValue *= r8.AHj(b);
            }
            if (intValue != 0) {
                C33951fL r2 = (C33951fL) hashMap.get(r4);
                if (r2 == null) {
                    r2 = new C33951fL();
                    r2.A01 = this.A03.A05(r4);
                }
                r2.A00 += intValue;
                hashMap.put(r4, r2);
            }
        }
        ArrayList arrayList = new ArrayList(hashMap.entrySet());
        Collections.sort(arrayList, new Comparator() { // from class: X.1fM
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                C33951fL r82 = (C33951fL) ((Map.Entry) obj2).getValue();
                C33951fL r7 = (C33951fL) ((Map.Entry) obj).getValue();
                if (r7 == r82) {
                    return 0;
                }
                long j = (long) (r82.A00 - r7.A00);
                if (j == 0) {
                    j = r82.A01 - r7.A01;
                }
                if (j < 0) {
                    return -1;
                }
                if (j > 0) {
                    return 1;
                }
                return 0;
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Map.Entry entry2 = (Map.Entry) it.next();
            arrayList2.add(entry2.getKey());
            entry2.getKey();
            entry2.getValue();
            entry2.getValue();
        }
        return arrayList2;
    }

    public Map A02() {
        if (this.A00 == null) {
            boolean A06 = A06();
            this.A00 = new ConcurrentHashMap();
            C16310on A01 = this.A05.get();
            if (A06) {
                try {
                    Cursor A09 = A01.A03.A09("SELECT jid_row_id, type, message_count FROM frequent", null);
                    if (A09 != null) {
                        int columnIndexOrThrow = A09.getColumnIndexOrThrow("jid_row_id");
                        int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("type");
                        int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("message_count");
                        while (A09.moveToNext()) {
                            AbstractC14640lm r1 = (AbstractC14640lm) this.A04.A07(AbstractC14640lm.class, A09.getLong(columnIndexOrThrow));
                            if (r1 != null) {
                                this.A00.put(new C33931fJ(r1, (byte) A09.getInt(columnIndexOrThrow2)), Integer.valueOf(A09.getInt(columnIndexOrThrow3)));
                            }
                        }
                        A09.close();
                    }
                } finally {
                }
            } else {
                try {
                    Cursor A092 = A01.A03.A09("SELECT jid, type, message_count FROM frequents", null);
                    if (A092 != null) {
                        int columnIndexOrThrow4 = A092.getColumnIndexOrThrow("jid");
                        int columnIndexOrThrow5 = A092.getColumnIndexOrThrow("type");
                        int columnIndexOrThrow6 = A092.getColumnIndexOrThrow("message_count");
                        while (A092.moveToNext()) {
                            AbstractC14640lm A012 = AbstractC14640lm.A01(A092.getString(columnIndexOrThrow4));
                            if (A012 != null) {
                                this.A00.put(new C33931fJ(A012, (byte) A092.getInt(columnIndexOrThrow5)), Integer.valueOf(A092.getInt(columnIndexOrThrow6)));
                            }
                        }
                        A092.close();
                    }
                } finally {
                }
            }
            A01.close();
        }
        return this.A00;
    }

    public void A03() {
        int i;
        int valueOf;
        C28181Ma r5 = new C28181Ma(false);
        r5.A04("frequentmsgstore/updateFrequents");
        C16310on A02 = this.A05.A02();
        try {
            C16330op r12 = A02.A03;
            long currentTimeMillis = System.currentTimeMillis() - 691200000;
            long j = 0;
            Cursor A09 = r12.A09("SELECT sort_id, received_timestamp FROM available_message_view ORDER BY sort_id DESC LIMIT 4096", null);
            if (A09 != null) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("received_timestamp");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("sort_id");
                i = 0;
                while (A09.moveToNext()) {
                    long j2 = A09.getLong(columnIndexOrThrow);
                    j = A09.getLong(columnIndexOrThrow2);
                    i++;
                    if (j2 <= currentTimeMillis) {
                        break;
                    }
                }
                A09.close();
            } else {
                i = 0;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("frequentmsgstore/updateFrequents/start sort_id:");
            sb.append(j);
            sb.append(" ");
            sb.append(i);
            Log.i(sb.toString());
            String[] strArr = {Long.toString(j), Integer.toString(6)};
            HashMap hashMap = new HashMap();
            Cursor A092 = r12.A09("SELECT chat_row_id, message_type FROM available_message_view WHERE sort_id >= ? AND from_me = 1 AND status != ?", strArr);
            if (A092 != null) {
                int columnIndexOrThrow3 = A092.getColumnIndexOrThrow("message_type");
                while (A092.moveToNext()) {
                    AbstractC14640lm A06 = this.A02.A06(A092);
                    if (A06 != null && !C15380n4.A0N(A06)) {
                        C33931fJ r1 = new C33931fJ(A06, (byte) A092.getInt(columnIndexOrThrow3));
                        Integer num = (Integer) hashMap.get(r1);
                        if (num == null) {
                            valueOf = 1;
                        } else {
                            valueOf = Integer.valueOf(num.intValue() + 1);
                        }
                        hashMap.put(r1, valueOf);
                    }
                }
                A092.close();
            } else {
                Log.e("frequentmsgstore/updateFrequents/cursor is null");
            }
            hashMap.size();
            AnonymousClass1Lx A00 = A02.A00();
            r12.A01("frequents", null, null);
            if (A06() || this.A06.A01("migration_frequent_index", 0) > 0) {
                r12.A01("frequent", null, null);
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                A00(A02, ((C33931fJ) entry.getKey()).A01, ((C33931fJ) entry.getKey()).A00, ((Integer) entry.getValue()).intValue());
            }
            for (Map.Entry entry2 : hashMap.entrySet()) {
                A05(((C33931fJ) entry2.getKey()).A01, ((C33931fJ) entry2.getKey()).A00, ((Integer) entry2.getValue()).intValue(), true);
            }
            A00.A00();
            A00.close();
            this.A00 = new ConcurrentHashMap(hashMap);
            this.A06.A05("frequents", this.A01.A00());
            A02.close();
            r5.A01();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(AbstractC14640lm r9) {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("frequents", "jid = ?", new String[]{r9.getRawString()});
            if (A06() || this.A06.A01("migration_frequent_index", 0) > 0) {
                long A01 = this.A04.A01(r9);
                AnonymousClass1YE A002 = this.A07.A00("DELETE FROM frequent WHERE jid_row_id = ?");
                A002.A01(1, A01);
                A002.A00();
            }
            A00.A00();
            A00.close();
            A02.close();
            Map map = this.A00;
            if (map != null) {
                ArrayList arrayList = new ArrayList();
                for (C33931fJ r1 : map.keySet()) {
                    if (r9.equals(r1.A01)) {
                        arrayList.add(r1);
                    }
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    this.A00.remove(it.next());
                }
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A05(AbstractC14640lm r9, byte b, int i, boolean z) {
        if (A06() || this.A06.A01("migration_frequent_index", 0) > 0) {
            long A01 = this.A04.A01(r9);
            if (!z) {
                AnonymousClass1YE A00 = this.A07.A00("UPDATE frequent   SET message_count = ? WHERE jid_row_id = ? AND type = ?");
                A00.A01(2, A01);
                A00.A01(3, (long) b);
                A00.A01(1, (long) i);
                if (A00.A00.executeUpdateDelete() == 1) {
                    return;
                }
            }
            if (A06()) {
                AnonymousClass1YE A002 = this.A07.A00("INSERT INTO frequent(jid_row_id, type, message_count) VALUES (?, ?, ?)");
                A002.A01(1, A01);
                A002.A01(2, (long) b);
                A002.A01(3, (long) i);
                if (A002.A00.executeInsert() == -1) {
                    StringBuilder sb = new StringBuilder("frequentMessageStore/insertOrUpdateFrequent/failed jid=");
                    sb.append(r9);
                    sb.append(" type=");
                    sb.append((int) b);
                    Log.e(sb.toString());
                }
            }
        }
    }

    public boolean A06() {
        return this.A04.A0C() && this.A06.A01("frequent_ready", 0) == 1;
    }
}
