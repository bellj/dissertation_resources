package X;

/* renamed from: X.5Mv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114725Mv extends AnonymousClass1TM {
    public AnonymousClass1TN A00;
    public AnonymousClass1TK A01;

    public C114725Mv(AnonymousClass1TK r1) {
        this.A01 = r1;
    }

    public C114725Mv(AnonymousClass1TN r1, AnonymousClass1TK r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static C114725Mv A00(Object obj) {
        if (obj instanceof C114725Mv) {
            return (C114725Mv) obj;
        }
        if (obj != null) {
            return new C114725Mv(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    public C114725Mv(AbstractC114775Na r4) {
        if (r4.A0B() < 1 || r4.A0B() > 2) {
            throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r4.A0B()));
        }
        this.A01 = AnonymousClass1TK.A00(AbstractC114775Na.A00(r4));
        this.A00 = r4.A0B() == 2 ? r4.A0D(1) : null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        AnonymousClass1TN r0 = this.A00;
        if (r0 != null) {
            A00.A06(r0);
        }
        return new AnonymousClass5NZ(A00);
    }
}
