package X;

import android.util.Pair;
import com.whatsapp.util.Log;
import com.whatsapp.util.NativeUtils;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28471Ni {
    public long A00;
    public long A01;
    public Boolean A02;
    public String A03;
    public final C18790t3 A04;
    public final AbstractC28461Nh A05;
    public final C18800t4 A06;
    public final Integer A07;
    public final String A08;
    public final String A09;
    public final List A0A = new LinkedList();
    public final List A0B = new LinkedList();
    public final List A0C = new LinkedList();
    public final List A0D = new LinkedList();
    public final AtomicBoolean A0E = new AtomicBoolean();
    public final AtomicReference A0F = new AtomicReference();
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;

    public C28471Ni(C18790t3 r2, AbstractC28461Nh r3, C18800t4 r4, String str, String str2, int i, boolean z, boolean z2, boolean z3) {
        this.A06 = r4;
        this.A08 = str;
        this.A09 = str2;
        this.A05 = r3;
        this.A0H = z;
        this.A0I = z2;
        this.A04 = r2;
        this.A07 = Integer.valueOf(i);
        this.A0G = z3;
    }

    public static String A00(URL url) {
        if (!(url == null || url.getHost() == null)) {
            try {
                return InetAddress.getByName(url.getHost()).getHostAddress();
            } catch (UnknownHostException unused) {
            }
        }
        return null;
    }

    public static void A01(C43161wW r1, Exception exc, URL url) {
        r1.A0A(exc);
        r1.A0O = A00(url);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0404 A[LOOP:4: B:104:0x03fe->B:106:0x0404, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02(X.C28481Nj r24) {
        /*
        // Method dump skipped, instructions count: 1123
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28471Ni.A02(X.1Nj):int");
    }

    public final Pair A03() {
        boolean z;
        Socket socket = (Socket) this.A0F.get();
        int i = 0;
        if (socket != null) {
            try {
                i = NativeUtils.getFileDescriptorForSocket(socket);
                z = true;
            } catch (UnsatisfiedLinkError | UnsupportedOperationException e) {
                Log.w("httpsformpost/getSocketInfo", e);
            }
            return new Pair(Integer.valueOf(i), Boolean.valueOf(z));
        }
        z = false;
        return new Pair(Integer.valueOf(i), Boolean.valueOf(z));
    }

    public final void A04(Pair pair, AnonymousClass23S r9, OutputStream outputStream, AtomicLong atomicLong) {
        long j = r9.A02;
        long j2 = j;
        while (j > 0) {
            j -= r9.A03.skip(j);
        }
        byte[] bArr = new byte[16384];
        do {
            int read = r9.A03.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                j2 += (long) read;
                int intValue = ((Number) pair.first).intValue();
                int i = 0;
                if (((Boolean) pair.second).booleanValue()) {
                    try {
                        i = NativeUtils.getBytesInSocketOutputQueue(intValue);
                    } catch (UnsupportedOperationException unused) {
                    }
                }
                long j3 = j2 - ((long) i);
                AbstractC28461Nh r4 = this.A05;
                if (r4 != null) {
                    r4.AOt(j3);
                }
                if (Thread.currentThread().isInterrupted()) {
                    throw new InterruptedIOException();
                }
            } else {
                atomicLong.set(j2);
                return;
            }
        } while (!this.A0E.getAndSet(false));
        throw new C867748t();
    }

    public void A05(InputStream inputStream, String str, String str2, long j, long j2) {
        this.A0A.add(new AnonymousClass23S(inputStream, str, str2, 2, j, j2));
    }

    public void A06(String str, String str2) {
        this.A0C.add(Pair.create(str, str2));
    }

    public void A07(String str, String str2) {
        this.A0D.add(Pair.create(str, str2));
    }
}
