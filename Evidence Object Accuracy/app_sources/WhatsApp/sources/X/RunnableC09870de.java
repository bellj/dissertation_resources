package X;

import androidx.sharetarget.ShortcutInfoCompatSaverImpl;

/* renamed from: X.0de  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09870de implements Runnable {
    public final /* synthetic */ C02510Co A00;
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A01;
    public final /* synthetic */ AbstractFutureC44231yX A02;

    public RunnableC09870de(C02510Co r1, ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, AbstractFutureC44231yX r3) {
        this.A01 = shortcutInfoCompatSaverImpl;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A02.get();
            this.A00.A07(null);
        } catch (Exception e) {
            this.A00.A06(e);
        }
    }
}
