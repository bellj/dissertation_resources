package X;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;

/* renamed from: X.3AJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AJ {
    public static Dialog A00(Context context, AnonymousClass12P r7, AnonymousClass5TF r8, C252018m r9, boolean z) {
        C004802e A0S = C12980iv.A0S(context);
        IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(r8, 3);
        DialogInterface$OnClickListenerC65673Ko r3 = new DialogInterface.OnClickListener(context, r7, r8, r9) { // from class: X.3Ko
            public final /* synthetic */ Context A00;
            public final /* synthetic */ AnonymousClass12P A01;
            public final /* synthetic */ AnonymousClass5TF A02;
            public final /* synthetic */ C252018m A03;

            {
                this.A02 = r3;
                this.A03 = r4;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AnonymousClass5TF r0 = this.A02;
                C252018m r5 = this.A03;
                AnonymousClass12P r4 = this.A01;
                Context context2 = this.A00;
                r0.AUr();
                Intent A0B = C12970iu.A0B(r5.A02(null, "android", "26000068", null));
                A0B.addFlags(268435456);
                r4.A06(context2, A0B);
            }
        };
        DialogInterface$OnCancelListenerC96124et r2 = new DialogInterface.OnCancelListener() { // from class: X.4et
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                AnonymousClass5TF.this.AUr();
            }
        };
        Resources resources = context.getResources();
        int i = R.string.revoke_multiple_messages_nux;
        if (z) {
            i = R.string.revoke_single_message_nux;
        }
        A0S.A0A(resources.getString(i));
        A0S.setPositiveButton(R.string.ok, iDxCListenerShape9S0100000_2_I1);
        A0S.setNegativeButton(R.string.learn_more, r3);
        A0S.A0B(true);
        A0S.A08(r2);
        return A0S.create();
    }
}
