package X;

import android.widget.AbsListView;
import com.whatsapp.HomeActivity;
import com.whatsapp.collections.observablelistview.ObservableListView;

/* renamed from: X.3Ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66533Ny implements AbsListView.OnScrollListener {
    public final /* synthetic */ ObservableListView A00;

    public C66533Ny(ObservableListView observableListView) {
        this.A00 = observableListView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a2, code lost:
        if (r5 == 0) goto L_0x00af;
     */
    @Override // android.widget.AbsListView.OnScrollListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onScroll(android.widget.AbsListView r11, int r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C66533Ny.onScroll(android.widget.AbsListView, int, int, int):void");
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        ObservableListView A2j;
        ObservableListView observableListView = this.A00;
        AbsListView.OnScrollListener onScrollListener = observableListView.A08;
        if (onScrollListener != null) {
            onScrollListener.onScrollStateChanged(absListView, i);
        }
        AbstractC14760m0 r4 = observableListView.A09;
        if (r4 != null && i == 0) {
            HomeActivity homeActivity = (HomeActivity) r4;
            if (observableListView != homeActivity.A2j()) {
                return;
            }
            if ((-homeActivity.A01) <= C13000ix.A00(homeActivity.A0I) || ((A2j = homeActivity.A2j()) != null && A2j.A04 < homeActivity.A0I.getHeight())) {
                homeActivity.A2p();
                return;
            }
            int i2 = -homeActivity.A0I.getHeight();
            float f = (float) i2;
            if (homeActivity.A08.getTranslationY() != f) {
                homeActivity.A08.animate().cancel();
                homeActivity.A08.animate().translationY(f).setDuration(250).start();
                homeActivity.A01 = i2;
            }
            homeActivity.A30(false);
        }
    }
}
