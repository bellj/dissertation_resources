package X;

import android.text.Editable;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.3MN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MN implements ActionMode.Callback {
    public final /* synthetic */ C37011l7 A00;

    @Override // android.view.ActionMode.Callback
    public void onDestroyActionMode(ActionMode actionMode) {
    }

    @Override // android.view.ActionMode.Callback
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    public AnonymousClass3MN(C37011l7 r1) {
        this.A00 = r1;
    }

    @Override // android.view.ActionMode.Callback
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        String str;
        C37011l7 r2 = this.A00;
        int selectionStart = r2.getSelectionStart();
        int selectionEnd = r2.getSelectionEnd();
        if (selectionStart > selectionEnd) {
            selectionEnd = selectionStart;
            selectionStart = selectionEnd;
        }
        int itemId = menuItem.getItemId();
        if (itemId == R.id.bold) {
            str = "*";
        } else if (itemId == R.id.italic) {
            str = "_";
        } else if (itemId == R.id.strikethrough) {
            str = "~";
        } else if (itemId != R.id.monospace) {
            return false;
        } else {
            str = "```";
        }
        Editable text = r2.getText();
        while (selectionEnd > selectionStart) {
            int i = selectionEnd - 1;
            if (!Character.isSpaceChar(text.charAt(i))) {
                break;
            }
            selectionEnd = i;
        }
        if (selectionEnd < text.length()) {
            char charAt = text.charAt(selectionEnd);
            if (!(Character.isSpaceChar(charAt) || charAt == '*' || charAt == '_' || charAt == '~')) {
                text.insert(selectionEnd, " ");
            }
        }
        text.insert(selectionEnd, str);
        while (selectionStart < selectionEnd && Character.isSpaceChar(text.charAt(selectionStart))) {
            selectionStart++;
        }
        if (selectionStart > 0) {
            char charAt2 = text.charAt(selectionStart - 1);
            if (!(Character.isSpaceChar(charAt2) || charAt2 == '*' || charAt2 == '_' || charAt2 == '~')) {
                text.insert(selectionStart, " ");
                selectionStart++;
            }
        }
        text.insert(selectionStart, str);
        return true;
    }

    @Override // android.view.ActionMode.Callback
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater menuInflater = actionMode.getMenuInflater();
        if (menuInflater != null) {
            menuInflater.inflate(R.menu.text_style, menu);
            return true;
        }
        Log.w("conversation-text-entry/action-mode-with-null-menu-inflater");
        return true;
    }
}
