package X;

import android.app.Activity;
import com.whatsapp.deeplink.DeepLinkActivity;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.3WQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WQ implements AbstractC116675Wj {
    public final /* synthetic */ DeepLinkActivity A00;
    public final /* synthetic */ WeakReference A01;

    @Override // X.AbstractC116675Wj
    public void AUW() {
    }

    @Override // X.AbstractC116675Wj
    public void AXH() {
    }

    public AnonymousClass3WQ(DeepLinkActivity deepLinkActivity, WeakReference weakReference) {
        this.A00 = deepLinkActivity;
        this.A01 = weakReference;
    }

    @Override // X.AbstractC116675Wj
    public void AXG(C42351v4 r3) {
        String str;
        if (r3 != null) {
            int i = r3.A00;
            if (i == 0) {
                str = "Conversation/createSyncContactTaskCallback/onSyncCompleted/NETWORK_UNAVAILABLE/";
            } else if (i == 4) {
                str = "Conversation/createSyncContactTaskCallback/onSyncCompleted/SYNC_REQUEST_FAILED/";
            }
            Log.w(str);
        }
        Activity activity = (Activity) this.A01.get();
        if (activity != null && !C36021jC.A03(activity)) {
            activity.finish();
        }
    }
}
