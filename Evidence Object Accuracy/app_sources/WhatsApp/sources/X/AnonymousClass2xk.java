package X;

import android.content.Context;

/* renamed from: X.2xk  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2xk extends AnonymousClass1OY {
    public boolean A00;

    public AnonymousClass2xk(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            C60842yj r2 = (C60842yj) this;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J A08 = AnonymousClass1OY.A08(r3, r2);
            AnonymousClass1OY.A0L(A08, r2);
            AnonymousClass1OY.A0M(A08, r2);
            AnonymousClass1OY.A0K(A08, r2);
            AnonymousClass1OY.A0I(r3, A08, r2, AnonymousClass1OY.A09(A08, r2, AnonymousClass1OY.A0B(A08, r2)));
            r2.A06 = (C22180yf) A08.A5U.get();
            r2.A05 = A08.A2e();
            r2.A04 = (C26171Ch) A08.A2a.get();
        }
    }
}
