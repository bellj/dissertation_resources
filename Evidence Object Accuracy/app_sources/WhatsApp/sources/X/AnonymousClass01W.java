package X;

import com.whatsapp.bridge.wafflecal.WaffleCalModule;
import com.whatsapp.bridge.wafflex.di.WaffleXProductModule;
import com.whatsapp.calling.di.VoipModule;
import com.whatsapp.common.di.CommonModule;
import com.whatsapp.cron.di.CronModule;
import com.whatsapp.dependencybridge.di.DependencyBridgeModule;
import com.whatsapp.devicetype.DeviceTypeModule;
import com.whatsapp.di.CompanionModeModule;
import com.whatsapp.di.MigrationModule;
import com.whatsapp.stickers.di.RecentStickersModule;
import com.whatsapp.systemreceivers.di.SystemReceiversModule;
import com.whatsapp.wabloks.commerce.di.CommerceBloksModule;

/* renamed from: X.01W  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass01W {
    public WaffleCalModule A00;
    public WaffleXProductModule A01;
    public VoipModule A02;
    public CommonModule A03;
    public CronModule A04;
    public DependencyBridgeModule A05;
    public DeviceTypeModule A06;
    public CompanionModeModule A07;
    public MigrationModule A08;
    public RecentStickersModule A09;
    public SystemReceiversModule A0A;
    public CommerceBloksModule A0B;
    public AnonymousClass01X A0C;

    public AnonymousClass01J A00() {
        C16640pN.A00(AnonymousClass01X.class, this.A0C);
        CommerceBloksModule commerceBloksModule = this.A0B;
        if (commerceBloksModule == null) {
            commerceBloksModule = new CommerceBloksModule();
            this.A0B = commerceBloksModule;
        }
        CommonModule commonModule = this.A03;
        if (commonModule == null) {
            commonModule = new CommonModule();
            this.A03 = commonModule;
        }
        CompanionModeModule companionModeModule = this.A07;
        if (companionModeModule == null) {
            companionModeModule = new CompanionModeModule();
            this.A07 = companionModeModule;
        }
        CronModule cronModule = this.A04;
        if (cronModule == null) {
            cronModule = new CronModule();
            this.A04 = cronModule;
        }
        DependencyBridgeModule dependencyBridgeModule = this.A05;
        if (dependencyBridgeModule == null) {
            dependencyBridgeModule = new DependencyBridgeModule();
            this.A05 = dependencyBridgeModule;
        }
        DeviceTypeModule deviceTypeModule = this.A06;
        if (deviceTypeModule == null) {
            deviceTypeModule = new DeviceTypeModule();
            this.A06 = deviceTypeModule;
        }
        MigrationModule migrationModule = this.A08;
        if (migrationModule == null) {
            migrationModule = new MigrationModule();
            this.A08 = migrationModule;
        }
        RecentStickersModule recentStickersModule = this.A09;
        if (recentStickersModule == null) {
            recentStickersModule = new RecentStickersModule();
            this.A09 = recentStickersModule;
        }
        SystemReceiversModule systemReceiversModule = this.A0A;
        if (systemReceiversModule == null) {
            systemReceiversModule = new SystemReceiversModule();
            this.A0A = systemReceiversModule;
        }
        VoipModule voipModule = this.A02;
        if (voipModule == null) {
            voipModule = new VoipModule();
            this.A02 = voipModule;
        }
        WaffleCalModule waffleCalModule = this.A00;
        if (waffleCalModule == null) {
            waffleCalModule = new WaffleCalModule();
            this.A00 = waffleCalModule;
        }
        WaffleXProductModule waffleXProductModule = this.A01;
        if (waffleXProductModule == null) {
            waffleXProductModule = new WaffleXProductModule();
            this.A01 = waffleXProductModule;
        }
        return new AnonymousClass01J(waffleCalModule, waffleXProductModule, voipModule, commonModule, cronModule, dependencyBridgeModule, companionModeModule, migrationModule, recentStickersModule, systemReceiversModule, commerceBloksModule, this.A0C);
    }
}
