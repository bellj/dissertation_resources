package X;

/* renamed from: X.1Kn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28081Kn {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r2 != 2) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.C15550nR r3, X.C22700zV r4, X.C14850m9 r5, X.AbstractC15340mz r6, X.C23000zz r7) {
        /*
            java.lang.String r0 = "20210210"
            int r2 = r7.A00(r0)
            r0 = 791(0x317, float:1.108E-42)
            boolean r0 = r5.A07(r0)
            if (r0 == 0) goto L_0x0012
            r1 = 2
            r0 = 1
            if (r2 == r1) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r2 = 0
            if (r0 == 0) goto L_0x003f
            if (r6 == 0) goto L_0x003f
            X.1IS r0 = r6.A0z
            X.0lm r0 = r0.A00
            X.AnonymousClass009.A05(r0)
            X.0n3 r1 = r3.A08(r0)
            if (r1 == 0) goto L_0x003f
            java.lang.Class<com.whatsapp.jid.UserJid> r0 = com.whatsapp.jid.UserJid.class
            com.whatsapp.jid.Jid r1 = r1.A0B(r0)
            com.whatsapp.jid.UserJid r1 = (com.whatsapp.jid.UserJid) r1
            boolean r0 = X.AnonymousClass3GN.A01(r5, r1)
            if (r0 != 0) goto L_0x003f
            X.1nr r0 = new X.1nr
            r0.<init>(r4, r1)
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x003f
            r2 = 1
        L_0x003f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28081Kn.A00(X.0nR, X.0zV, X.0m9, X.0mz, X.0zz):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r2 != 2) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01(X.C22700zV r3, X.C14850m9 r4, com.whatsapp.jid.UserJid r5, X.C23000zz r6) {
        /*
            java.lang.String r0 = "20210210"
            int r2 = r6.A00(r0)
            r0 = 791(0x317, float:1.108E-42)
            boolean r0 = r4.A07(r0)
            if (r0 == 0) goto L_0x0012
            r1 = 2
            r0 = 1
            if (r2 == r1) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r1 = 0
            if (r0 == 0) goto L_0x002a
            if (r5 == 0) goto L_0x002a
            boolean r0 = X.AnonymousClass3GN.A01(r4, r5)
            if (r0 != 0) goto L_0x002a
            X.1nr r0 = new X.1nr
            r0.<init>(r3, r5)
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x002a
            r1 = 1
        L_0x002a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28081Kn.A01(X.0zV, X.0m9, com.whatsapp.jid.UserJid, X.0zz):boolean");
    }
}
