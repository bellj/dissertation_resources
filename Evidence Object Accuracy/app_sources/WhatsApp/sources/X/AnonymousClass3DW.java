package X;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;

/* renamed from: X.3DW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DW {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public Rect A08;
    public Uri A09;
    public Uri A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public boolean A0E = true;
    public boolean A0F = true;
    public boolean A0G;
    public boolean A0H = true;
    public boolean A0I = true;
    public final Context A0J;

    public AnonymousClass3DW(Context context) {
        this.A0J = context;
    }

    public Intent A00() {
        Context context = this.A0J;
        Uri uri = this.A09;
        Uri uri2 = this.A0A;
        String str = this.A0C;
        int i = this.A00;
        int i2 = this.A01;
        int i3 = this.A06;
        int i4 = this.A07;
        int i5 = this.A04;
        int i6 = this.A03;
        Rect rect = this.A08;
        boolean z = this.A0E;
        boolean z2 = this.A0H;
        boolean z3 = this.A0I;
        boolean z4 = this.A0F;
        int i7 = this.A05;
        boolean z5 = this.A0G;
        String str2 = this.A0D;
        String str3 = this.A0B;
        int i8 = this.A02;
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.crop.CropImage");
        A0A.setData(uri);
        A0A.putExtra("output", uri2);
        A0A.putExtra("outputFormat", str);
        A0A.putExtra("circleCrop", (String) null);
        A0A.putExtra("aspectX", i);
        A0A.putExtra("aspectY", i2);
        A0A.putExtra("outputX", i3);
        A0A.putExtra("outputY", i4);
        A0A.putExtra("minCrop", i5);
        A0A.putExtra("maxCrop", i6);
        A0A.putExtra("initialRect", rect);
        A0A.putExtra("cropByOutputSize", z);
        A0A.putExtra("scale", z2);
        A0A.putExtra("scaleUpIfNeeded", z3);
        A0A.putExtra("maxFileSize", 0);
        A0A.putExtra("flattenRotation", z4);
        A0A.putExtra("rotation", i7);
        A0A.putExtra("flipH", z5);
        A0A.putExtra("flipV", false);
        A0A.putExtra("webImageSource", str2);
        A0A.putExtra("doodle", str3);
        A0A.putExtra("filter", i8);
        A0A.putExtra("rotateAspect", false);
        return A0A;
    }
}
