package X;

/* renamed from: X.4EY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EY {
    public static C33711ex A00(C16470p4 r1, int i) {
        if (i == 1) {
            return new C33731ez(r1);
        }
        if (i == 2) {
            return new AnonymousClass34K(r1);
        }
        if (i == 3) {
            return new C34991h2(r1);
        }
        if (i == 4) {
            return new C33721ey(r1);
        }
        if (i == 5) {
            return new C33741f0(r1);
        }
        throw C12960it.A0U(C12960it.A0W(i, "Unknown type of interactive message does not support customizations: "));
    }
}
