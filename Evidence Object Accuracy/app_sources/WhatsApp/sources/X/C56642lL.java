package X;

import android.os.IBinder;
import android.os.Parcel;

/* renamed from: X.2lL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56642lL extends C98334iW implements AnonymousClass5YQ {
    public C56642lL(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @Override // X.AnonymousClass5YQ
    public final void AhP(C78413os r6) {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.A01);
        obtain.writeInt(1);
        r6.writeToParcel(obtain, 0);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.A00.transact(59, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
