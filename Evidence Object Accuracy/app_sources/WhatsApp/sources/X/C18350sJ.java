package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.Conversation;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0sJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18350sJ {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C16210od A01;
    public final C22690zU A02;
    public final C22490zA A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final C22920zr A06;
    public final C237913a A07;
    public final C15450nH A08;
    public final C20670w8 A09;
    public final C18330sH A0A;
    public final C22730zY A0B;
    public final C26551Dx A0C;
    public final C238013b A0D;
    public final C18850tA A0E;
    public final C22700zV A0F;
    public final AnonymousClass10V A0G;
    public final C20730wE A0H;
    public final C18230s7 A0I;
    public final AnonymousClass01d A0J;
    public final C14830m7 A0K;
    public final C16590pI A0L;
    public final C18360sK A0M;
    public final C14820m6 A0N;
    public final AnonymousClass018 A0O;
    public final C20650w6 A0P;
    public final C18260sA A0Q;
    public final C20850wQ A0R;
    public final C16490p7 A0S;
    public final C26041Bu A0T;
    public final C22100yW A0U;
    public final C20710wC A0V;
    public final AnonymousClass107 A0W;
    public final C17220qS A0X;
    public final C19890uq A0Y;
    public final C20660w7 A0Z;
    public final C20220vP A0a;
    public final C18620sk A0b;
    public final C16630pM A0c;
    public final C22660zR A0d;
    public final C15510nN A0e;
    public final AnonymousClass10O A0f;
    public final C26531Dv A0g;
    public final C20630w4 A0h;
    public final C26971Fn A0i;
    public final C20780wJ A0j;
    public final AnonymousClass105 A0k;
    public final AnonymousClass12O A0l;
    public final AbstractC14440lR A0m;
    public final C14860mA A0n;
    public final AnonymousClass01H A0o;
    public final List A0p = new CopyOnWriteArrayList();

    public C18350sJ(C16210od r3, C22690zU r4, C22490zA r5, C14900mE r6, C15570nT r7, C22920zr r8, C237913a r9, C15450nH r10, C20670w8 r11, C18330sH r12, C22730zY r13, C26551Dx r14, C238013b r15, C18850tA r16, C22700zV r17, AnonymousClass10V r18, C20730wE r19, C18230s7 r20, AnonymousClass01d r21, C14830m7 r22, C16590pI r23, C18360sK r24, C14820m6 r25, AnonymousClass018 r26, C20650w6 r27, C18260sA r28, C20850wQ r29, C16490p7 r30, C26041Bu r31, C22100yW r32, C20710wC r33, AnonymousClass107 r34, C17220qS r35, C19890uq r36, C20660w7 r37, C20220vP r38, C18620sk r39, C16630pM r40, C22660zR r41, C15510nN r42, AnonymousClass10O r43, C26531Dv r44, C20630w4 r45, C26971Fn r46, C20780wJ r47, AnonymousClass105 r48, AnonymousClass12O r49, AbstractC14440lR r50, C14860mA r51, AnonymousClass01H r52) {
        this.A0L = r23;
        this.A0K = r22;
        this.A0h = r45;
        this.A04 = r6;
        this.A07 = r9;
        this.A0I = r20;
        this.A05 = r7;
        this.A0m = r50;
        this.A0g = r44;
        this.A0i = r46;
        this.A0n = r51;
        this.A0P = r27;
        this.A0Z = r37;
        this.A08 = r10;
        this.A0E = r16;
        this.A0d = r41;
        this.A09 = r11;
        this.A0X = r35;
        this.A0Y = r36;
        this.A0J = r21;
        this.A0O = r26;
        this.A0D = r15;
        this.A0V = r33;
        this.A06 = r8;
        this.A0l = r49;
        this.A0A = r12;
        this.A0H = r19;
        this.A0Q = r28;
        this.A02 = r4;
        this.A0G = r18;
        this.A0C = r14;
        this.A0S = r30;
        this.A0F = r17;
        this.A0N = r25;
        this.A0a = r38;
        this.A0f = r43;
        this.A0M = r24;
        this.A0U = r32;
        this.A0j = r47;
        this.A0k = r48;
        this.A0W = r34;
        this.A0b = r39;
        this.A0e = r42;
        this.A0c = r40;
        this.A01 = r3;
        this.A0T = r31;
        this.A0B = r13;
        this.A0R = r29;
        this.A03 = r5;
        this.A0o = r52;
    }

    public static String A00(Context context, Class cls) {
        String packageName = context.getPackageName();
        String name = cls.getName();
        int length = packageName.length();
        return (!name.startsWith(packageName) || name.length() <= length || name.charAt(length) != '.') ? name : name.substring(length + 1);
    }

    public Intent A01() {
        A08();
        C237913a r2 = this.A07;
        r2.A01 = false;
        r2.A00 = null;
        r2.A08.A0t(null, null);
        C238013b r9 = this.A0D;
        r9.A04();
        this.A0Y.A0G(false);
        C15570nT r4 = this.A05;
        r4.A08();
        C27621Ig r22 = r4.A01;
        if (r22 != null) {
            AnonymousClass10V r1 = this.A0G;
            r1.A05.A03(r22);
            r1.A00(r22, 0, 0);
        }
        Context context = this.A0L.A00;
        File file = new File(context.getFilesDir(), "me");
        if (file.exists()) {
            file.delete();
        }
        r4.A06();
        A0C(null, null, null);
        this.A0T.A00();
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.registration.RegisterPhone");
        className.putExtra("com.whatsapp.registration.RegisterPhone.resetstate", true);
        className.putExtra("com.whatsapp.registration.RegisterPhone.clear_phone_number", true);
        className.addFlags(32768);
        A0A(1);
        this.A0g.A05();
        AnonymousClass12O r0 = this.A0l;
        r0.A03();
        r0.A05();
        this.A0S.A00 = false;
        this.A0Q.A00 = true;
        C32781cj.A08(context);
        C14820m6 r7 = this.A0N;
        SharedPreferences sharedPreferences = r7.A00;
        sharedPreferences.edit().putInt("gdrive_successive_backup_failed_count", 0).apply();
        if (sharedPreferences.getBoolean("encrypted_backup_enabled", false) && !sharedPreferences.getBoolean("encrypted_backup_using_encryption_key", false)) {
            r7.A16(true);
        }
        this.A0n.A07();
        this.A0m.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(this, 42));
        r9.A05();
        r7.A1A(true);
        this.A0d.A05(false);
        r7.A1B(false);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove("show_post_reg_logged_out_dialog");
        edit.apply();
        sharedPreferences.edit().putString("contact_qr_code", null).apply();
        sharedPreferences.edit().putBoolean("support_ban_appeal_screen_before_verification", false).apply();
        Conversation.A0K(this.A03, null);
        return className;
    }

    public final C005602s A02(String str, String str2, String str3) {
        Context context = this.A0L.A00;
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "critical_app_alerts@1";
        A00.A03 = 1;
        A00.A0B(str3);
        A00.A05(this.A0K.A00());
        A00.A02(3);
        A00.A0D(true);
        A00.A0A(str);
        A00.A09(str2);
        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
        notificationCompat$BigTextStyle.A09(str2);
        A00.A08(notificationCompat$BigTextStyle);
        A00.A09 = AnonymousClass1UY.A00(context, 1, C14960mK.A04(context), 0);
        C18360sK.A01(A00, R.drawable.notifybar);
        return A00;
    }

    public Me A03() {
        C14820m6 r0 = this.A0N;
        return new Me(r0.A0B(), r0.A0C(), r0.A00.getString("registration_jid", null));
    }

    public void A04() {
        PendingIntent A01 = AnonymousClass1UY.A01(this.A0L.A00, 0, new Intent("com.whatsapp.alarm.REGISTRATION_RETRY").setPackage("com.whatsapp"), 536870912);
        if (A01 != null) {
            AlarmManager A04 = this.A0J.A04();
            if (A04 != null) {
                A04.cancel(A01);
            } else {
                Log.w("RegistrationManager/cancelRegistrationRetryAlarm AlarmManager is null");
            }
            A01.cancel();
        }
    }

    public void A05() {
        C15570nT r0 = this.A05;
        Me A01 = r0.A01();
        if (A01 != null) {
            r0.A07();
            this.A02.A00();
            AnonymousClass107 r2 = this.A0W;
            if (r2.A04.A00.A05(AbstractC15460nI.A18)) {
                r2.A05.A01();
            }
            this.A0g.A05();
            C238013b r1 = this.A0D;
            r1.A05();
            r1.A0F(null);
            this.A0V.A0F(3, true, false);
            this.A00.post(new RunnableBRunnable0Shape7S0200000_I0_7(this, 34, A01));
            return;
        }
        Log.w("registrationmanager/response/ok already changed?");
    }

    public void A06() {
        this.A0A.A01();
        if (!this.A0e.A01()) {
            Log.i("registrationmanager/loginfailed/ignore as registration not verified");
        } else {
            this.A04.A0I(new RunnableBRunnable0Shape10S0100000_I0_10(this, 43));
        }
    }

    public void A07() {
        C15570nT r1 = this.A05;
        r1.A08();
        if (r1.A00 != null) {
            Log.i("xmpp/service/reset-registered/updateparams");
            r1.A08();
            C27631Ih r2 = r1.A05;
            C19890uq r12 = this.A0Y;
            if (r12.A0x) {
                r12.A05 = r2;
            }
        }
    }

    public void A08() {
        C16630pM r2 = this.A0c;
        Context context = this.A0L.A00;
        r2.A01(A00(context, RegisterPhone.class)).edit().clear().apply();
        r2.A01(A00(context, VerifyPhoneNumber.class)).edit().clear().apply();
    }

    public void A09() {
        this.A05.A06();
        this.A0N.A0J();
        C19990v2 r1 = this.A0P.A09;
        synchronized (r1) {
            r1.A0B().clear();
            r1.A00 = false;
        }
        A0A(1);
    }

    public void A0A(int i) {
        C14820m6 r3 = this.A0N;
        if (r3.A00.getInt("registration_state", -1) != i) {
            AnonymousClass10O r1 = this.A0f;
            r1.A00 = null;
            r1.A01 = false;
            r1.A02 = false;
            r1.A03 = false;
            r1.A06 = false;
            r1.A07 = false;
            r1.A04 = false;
            r1.A05 = true;
            r3.A0H();
        }
        this.A0e.A00.A00.edit().putInt("registration_state", i).apply();
    }

    public void A0B(long j) {
        if (j >= 60000) {
            if (!this.A0I.A02(AnonymousClass1UY.A01(this.A0L.A00, 0, new Intent("com.whatsapp.alarm.REGISTRATION_RETRY").setPackage("com.whatsapp"), 134217728), 2, SystemClock.elapsedRealtime() + j)) {
                Log.w("RegistrationManager/startRegistrationRetryAlarm AlarmManager is null");
            }
        }
    }

    public void A0C(String str, String str2, String str3) {
        C14820m6 r2 = this.A0N;
        SharedPreferences sharedPreferences = r2.A00;
        sharedPreferences.edit().putString("registration_jid", str3).remove("registration_wipe_type").remove("registration_wipe_token").remove("registration_wipe_wait").remove("registration_wipe_expiry").remove("registration_wipe_server_time").apply();
        sharedPreferences.edit().remove("registration_wipe_info_timestamp").apply();
        r2.A0v(str, str2);
    }

    public boolean A0D() {
        C20660w7 r2 = this.A0Z;
        r2.A05();
        C19890uq r11 = this.A0Y;
        r11.A0G(false);
        Log.i("registrationmanager/complete-change-number");
        Me A03 = A03();
        boolean z = false;
        if (A03.jabber_id != null) {
            z = true;
        }
        Log.a(z);
        C15570nT r1 = this.A05;
        r1.A08();
        if (r1.A0D(A03, "me")) {
            r1.A08();
            r1.A0A(A03);
            C16490p7 r0 = this.A0S;
            r0.A04();
            boolean z2 = r0.A01;
            if (!z2) {
                C20850wQ r4 = this.A0R;
                if (r4.A02.A07(null, false)) {
                    Log.i("registration-manager/complete-change-number/msgstoredb/healthy");
                    this.A0P.A07(false);
                    r4.A01();
                }
            }
            A07();
            r11.A04();
            if (!z2) {
                this.A0H.A04();
            }
            A0A(3);
            this.A0Q.A00 = true;
            C16590pI r7 = this.A0L;
            C32781cj.A08(r7.A00);
            Log.i("registration-manager/complete-change-number/changenumber/setregverified");
            C20730wE r42 = this.A0H;
            r42.A01.A08();
            C42271uw r12 = new C42271uw(AnonymousClass1JA.A03);
            r12.A00 = AnonymousClass1JB.A09;
            r12.A04 = true;
            r42.A03(r12.A01(), true);
            this.A0b.A01(true, false);
            Log.i("registration-manager/complete-change-number/reinitalized-payments");
            C15450nH r43 = this.A08;
            synchronized (AbstractC15460nI.class) {
                AbstractC15460nI.A08 = 0;
                r43.A00.edit().putLong("groups_server_props_last_refresh_time", 0).apply();
            }
            this.A0n.A07();
            AbstractC14440lR r10 = this.A0m;
            r10.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(this, 41));
            C14830m7 r6 = this.A0K;
            C17220qS r9 = this.A0X;
            C14820m6 r8 = this.A0N;
            new C452420s(r6, r7, r8, r9, r10).A00();
            r2.A05();
            this.A0D.A05();
            r8.A0U(3);
            r11.A0C(0, true, false, false, false);
            return true;
        }
        Log.i("registration-manager/complete-change-number/error-saving");
        return false;
    }

    public boolean A0E() {
        Log.i("registrationmanager/revert-to-old");
        C15570nT r2 = this.A05;
        Me A01 = r2.A01();
        r2.A08();
        if (!r2.A0D(A01, "me")) {
            return false;
        }
        r2.A08();
        r2.A0A(A01);
        this.A0N.A12(false);
        r2.A07();
        this.A02.A00();
        C20850wQ r22 = this.A0R;
        if (r22.A02.A07(null, false)) {
            Log.i("registrationmanager/revert/msgstoredb/healthy");
            this.A0P.A07(false);
            r22.A01();
            this.A0Y.A04();
            this.A0H.A04();
            return true;
        }
        this.A0Y.A05();
        return true;
    }

    public boolean A0F() {
        Me A03 = A03();
        if (A03.jabber_id == null) {
            return false;
        }
        C15570nT r0 = this.A05;
        r0.A08();
        r0.A0A(A03);
        C19890uq r1 = this.A0Y;
        r1.A0w = false;
        Log.i("registrationmanager/finishRegistration/set-connection/passive");
        A07();
        r1.A04();
        return true;
    }
}
