package X;

import android.graphics.Rect;
import android.view.View;

/* renamed from: X.3D2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D2 {
    public Rect A00;
    public View A01;
    public boolean A02;
    public final AbstractC52532bB A03;
    public final AnonymousClass4TU A04;

    public AnonymousClass3D2(AbstractC52532bB r1, AnonymousClass4TU r2) {
        this.A04 = r2;
        this.A03 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0115, code lost:
        if (r5.contains(r9, r8) != false) goto L_0x0117;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(android.view.MotionEvent r11) {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3D2.A00(android.view.MotionEvent):boolean");
    }
}
