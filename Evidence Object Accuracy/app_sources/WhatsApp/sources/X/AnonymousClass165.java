package X;

import android.content.SharedPreferences;

/* renamed from: X.165  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass165 {
    public SharedPreferences A00;
    public final C16630pM A01;
    public final Object A02 = new Object();
    public final Object A03 = new Object();
    public final Object A04 = new Object();
    public final Object A05 = new Object();

    public AnonymousClass165(C16630pM r2) {
        this.A01 = r2;
    }

    public final SharedPreferences A00() {
        SharedPreferences sharedPreferences = this.A00;
        if (sharedPreferences != null) {
            return sharedPreferences;
        }
        SharedPreferences A01 = this.A01.A01("stickers");
        this.A00 = A01;
        return A01;
    }
}
