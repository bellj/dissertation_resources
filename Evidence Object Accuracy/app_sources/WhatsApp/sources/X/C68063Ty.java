package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68063Ty implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final C14850m9 A05;
    public final C14960mK A06;
    public final C17070qD A07;
    public final C25901Bg A08;
    public final C26111Cb A09;
    public final C25681Ai A0A;

    public C68063Ty(C53042cM r1, C14830m7 r2, C16590pI r3, C14820m6 r4, C14850m9 r5, C14960mK r6, C17070qD r7, C25901Bg r8, C26111Cb r9, C25681Ai r10) {
        this.A02 = r2;
        this.A05 = r5;
        this.A03 = r3;
        this.A06 = r6;
        this.A09 = r9;
        this.A07 = r7;
        this.A01 = r1;
        this.A04 = r4;
        this.A0A = r10;
        this.A08 = r8;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (this.A00 == null) {
            C53042cM r2 = this.A01;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.education_banner_row);
            this.A00 = A0F;
            A0F.setBackgroundResource(R.drawable.selector_orange_gradient);
            r2.addView(this.A00);
        }
        if (this.A00 == null) {
            C53042cM r22 = this.A01;
            View A0F2 = C12960it.A0F(C12960it.A0E(r22), r22, R.layout.education_banner_row);
            this.A00 = A0F2;
            A0F2.setBackgroundResource(R.drawable.selector_orange_gradient);
        }
        this.A00.setVisibility(8);
    }
}
