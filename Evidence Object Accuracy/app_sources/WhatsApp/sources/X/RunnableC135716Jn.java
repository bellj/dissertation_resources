package X;

import java.util.List;
import java.util.UUID;

/* renamed from: X.6Jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135716Jn implements Runnable {
    public final /* synthetic */ AnonymousClass661 A00;
    public final /* synthetic */ List A01;
    public final /* synthetic */ UUID A02;
    public final /* synthetic */ boolean A03;

    public RunnableC135716Jn(AnonymousClass661 r1, List list, UUID uuid, boolean z) {
        this.A00 = r1;
        this.A01 = list;
        this.A03 = z;
        this.A02 = uuid;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A01;
        if (0 < list.size()) {
            list.get(0);
            throw C12980iv.A0n("onError");
        } else if (this.A03) {
            AnonymousClass661 r2 = this.A00;
            r2.A0S.A02(this.A02);
            r2.A8z(null);
        }
    }
}
