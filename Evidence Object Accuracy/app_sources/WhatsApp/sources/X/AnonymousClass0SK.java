package X;

import android.app.Notification;
import android.app.Person;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import java.util.List;

/* renamed from: X.0SK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0SK {
    public Uri A00;
    public Bundle A01 = new Bundle();
    public String A02;
    public final long A03;
    public final C007303s A04;
    public final CharSequence A05;

    public AnonymousClass0SK(C007303s r2, CharSequence charSequence, long j) {
        this.A05 = charSequence;
        this.A03 = j;
        this.A04 = r2;
    }

    public static Bundle[] A00(List list) {
        Bundle[] bundleArr = new Bundle[list.size()];
        int size = list.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass0SK r6 = (AnonymousClass0SK) list.get(i);
            Bundle bundle = new Bundle();
            CharSequence charSequence = r6.A05;
            if (charSequence != null) {
                bundle.putCharSequence("text", charSequence);
            }
            bundle.putLong("time", r6.A03);
            C007303s r7 = r6.A04;
            if (r7 != null) {
                bundle.putCharSequence("sender", r7.A01);
                if (Build.VERSION.SDK_INT >= 28) {
                    bundle.putParcelable("sender_person", r7.A01());
                } else {
                    bundle.putBundle("person", r7.A02());
                }
            }
            String str = r6.A02;
            if (str != null) {
                bundle.putString("type", str);
            }
            Uri uri = r6.A00;
            if (uri != null) {
                bundle.putParcelable("uri", uri);
            }
            bundle.putBundle("extras", r6.A01);
            bundleArr[i] = bundle;
        }
        return bundleArr;
    }

    public Notification.MessagingStyle.Message A01() {
        Notification.MessagingStyle.Message message;
        C007303s r6 = this.A04;
        int i = Build.VERSION.SDK_INT;
        CharSequence charSequence = null;
        Person person = null;
        CharSequence charSequence2 = this.A05;
        long j = this.A03;
        if (i >= 28) {
            if (r6 != null) {
                person = r6.A01();
            }
            message = new Notification.MessagingStyle.Message(charSequence2, j, person);
        } else {
            if (r6 != null) {
                charSequence = r6.A01;
            }
            message = new Notification.MessagingStyle.Message(charSequence2, j, charSequence);
        }
        String str = this.A02;
        if (str != null) {
            message.setData(str, this.A00);
        }
        return message;
    }
}
