package X;

import android.view.ViewTreeObserver;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.4nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101634nq implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ TextEmojiLabel A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101634nq(TextEmojiLabel textEmojiLabel) {
        this.A00 = textEmojiLabel;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        TextEmojiLabel textEmojiLabel = this.A00;
        C12980iv.A1E(textEmojiLabel, this);
        textEmojiLabel.requestLayout();
    }
}
