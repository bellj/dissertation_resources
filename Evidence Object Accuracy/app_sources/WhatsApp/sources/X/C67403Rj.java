package X;

import com.whatsapp.countrygating.viewmodel.CountryGatingViewModel;
import com.whatsapp.tosgating.viewmodel.ToSGatingViewModel;

/* renamed from: X.3Rj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67403Rj implements AbstractC009404s {
    public final /* synthetic */ AnonymousClass2IN A00;
    public final /* synthetic */ C14310lE A01;
    public final /* synthetic */ CountryGatingViewModel A02;
    public final /* synthetic */ C15370n3 A03;
    public final /* synthetic */ AbstractC14640lm A04;
    public final /* synthetic */ ToSGatingViewModel A05;

    public C67403Rj(AnonymousClass2IN r1, C14310lE r2, CountryGatingViewModel countryGatingViewModel, C15370n3 r4, AbstractC14640lm r5, ToSGatingViewModel toSGatingViewModel) {
        this.A00 = r1;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = countryGatingViewModel;
        this.A05 = toSGatingViewModel;
        this.A03 = r4;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass2IN r0 = this.A00;
        AbstractC14640lm r8 = this.A04;
        C14310lE r14 = this.A01;
        CountryGatingViewModel countryGatingViewModel = this.A02;
        ToSGatingViewModel toSGatingViewModel = this.A05;
        C15370n3 r5 = this.A03;
        AnonymousClass01J r6 = r0.A00.A03;
        C14850m9 A0S = C12960it.A0S(r6);
        C15550nR A0O = C12960it.A0O(r6);
        AnonymousClass10S A0Z = C12990iw.A0Z(r6);
        AnonymousClass1E5 r3 = (AnonymousClass1E5) r6.AKs.get();
        C22330yu r10 = (C22330yu) r6.A3I.get();
        C22640zP r11 = (C22640zP) r6.A3Z.get();
        C244215l r2 = (C244215l) r6.A8y.get();
        AnonymousClass11A r1 = (AnonymousClass11A) r6.A8o.get();
        return new C36641kF(r10, r11, A0O, A0Z, r14, countryGatingViewModel, C12980iv.A0c(r6), (C20830wO) r6.A4W.get(), C12980iv.A0d(r6), r5, A0S, C12980iv.A0e(r6), r1, r2, r3, r8, toSGatingViewModel, (AnonymousClass168) r6.ANe.get());
    }
}
