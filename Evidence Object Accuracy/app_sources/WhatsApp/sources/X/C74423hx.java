package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3hx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74423hx extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new C98474ik();
    public final int A00;

    public C74423hx(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A00 = parcel.readInt();
    }

    public C74423hx(Parcelable parcelable, int i) {
        super(parcelable);
        this.A00 = i;
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
    }
}
