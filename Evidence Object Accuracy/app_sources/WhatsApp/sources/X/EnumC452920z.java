package X;

import com.whatsapp.R;

/* renamed from: X.20z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC452920z implements AnonymousClass210 {
    /* Fake field, exist only in values array */
    PEOPLE(0, R.id.emoji_people_btn, R.id.emoji_people_marker, R.string.emoji_label_people),
    /* Fake field, exist only in values array */
    NATURE(1, R.id.emoji_nature_btn, R.id.emoji_nature_marker, R.string.emoji_label_nature),
    /* Fake field, exist only in values array */
    FOOD(2, R.id.emoji_food_btn, R.id.emoji_food_marker, R.string.emoji_label_food),
    /* Fake field, exist only in values array */
    ACTIVITY(3, R.id.emoji_activity_btn, R.id.emoji_activity_marker, R.string.emoji_label_activity),
    /* Fake field, exist only in values array */
    TRAVEL(4, R.id.emoji_travel_btn, R.id.emoji_travel_marker, R.string.emoji_label_travel),
    /* Fake field, exist only in values array */
    OBJECTS(5, R.id.emoji_objects_btn, R.id.emoji_objects_marker, R.string.emoji_label_objects),
    /* Fake field, exist only in values array */
    SYMBOLS(6, R.id.emoji_symbols_btn, R.id.emoji_symbols_marker, R.string.emoji_label_symbols),
    /* Fake field, exist only in values array */
    FLAGS(7, R.id.emoji_flags_btn, R.id.emoji_flags_marker, R.string.emoji_label_flags);
    
    public final int buttonId;
    public final int[][] emojiData;
    public final int markerId;
    public final int titleResId;

    EnumC452920z(int i, int i2, int i3, int i4) {
        this.emojiData = r2;
        this.buttonId = i2;
        this.markerId = i3;
        this.titleResId = i4;
    }
}
