package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2QG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QG extends AnonymousClass2PA {
    public final AnonymousClass1JX A00;

    public AnonymousClass2QG(Jid jid, AnonymousClass1JX r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
