package X;

/* renamed from: X.1yL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44161yL extends AbstractC16350or {
    public final boolean A00;
    public final boolean A01;
    public final boolean A02;
    public final /* synthetic */ AbstractC44141yJ A03;

    public abstract void A08();

    public AbstractC44161yL(AbstractC44141yJ r1, boolean z, boolean z2, boolean z3) {
        this.A03 = r1;
        this.A00 = z;
        this.A02 = z2;
        this.A01 = z3;
    }
}
