package X;

import android.content.ContentValues;
import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1tT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41401tT {
    public int A00 = 0;
    public String A01;
    public String A02 = DefaultCrypto.UTF_8;
    public String A03 = DefaultCrypto.UTF_8;
    public List A04 = new ArrayList();
    public AnonymousClass3FO A05;
    public C41411tU A06;

    public void A00(String str) {
        String str2 = this.A01;
        if (str2 == null || str2.equalsIgnoreCase("TYPE")) {
            this.A05.A04.add(str);
        } else {
            this.A05.A00.put(str2, str);
        }
        this.A01 = null;
    }

    public void A01(List list) {
        AnonymousClass3FO r6;
        String str;
        byte[] bArr;
        UnsupportedEncodingException e;
        StringBuilder sb;
        byte[] bArr2;
        if (list == null || list.size() == 0) {
            AnonymousClass3FO r1 = this.A05;
            r1.A06 = null;
            r1.A03.clear();
            str = "";
            this.A05.A03.add(str);
            r6 = this.A05;
        } else {
            ContentValues contentValues = this.A05.A00;
            String str2 = DefaultCrypto.UTF_8;
            String asString = contentValues.getAsString("ENCODING");
            if (str2.length() == 0) {
                str2 = this.A03;
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String str3 = (String) it.next();
                AnonymousClass3FO r12 = this.A05;
                List list2 = r12.A03;
                if (asString != null) {
                    int i = 0;
                    if (asString.equals("BASE64") || asString.equals("B")) {
                        r12.A06 = Base64.decode(str3.getBytes(), 0);
                    } else if (asString.equals("QUOTED-PRINTABLE")) {
                        String replaceAll = str3.replaceAll("= ", " ").replaceAll("=\t", "\t");
                        StringBuilder sb2 = new StringBuilder();
                        int length = replaceAll.length();
                        ArrayList arrayList = new ArrayList();
                        int i2 = 0;
                        while (i2 < length) {
                            char charAt = replaceAll.charAt(i2);
                            if (charAt == '\n') {
                                arrayList.add(sb2.toString());
                                sb2 = new StringBuilder();
                            } else if (charAt == '\r') {
                                arrayList.add(sb2.toString());
                                sb2 = new StringBuilder();
                                if (i2 < length - 1) {
                                    int i3 = i2 + 1;
                                    if (replaceAll.charAt(i3) == '\n') {
                                        i2 = i3;
                                    }
                                }
                            } else {
                                sb2.append(charAt);
                            }
                            i2++;
                        }
                        String obj = sb2.toString();
                        if (obj.length() > 0) {
                            arrayList.add(obj);
                        }
                        String[] strArr = (String[]) arrayList.toArray(new String[0]);
                        StringBuilder sb3 = new StringBuilder();
                        for (String str4 : strArr) {
                            if (str4.endsWith("=")) {
                                str4 = str4.substring(0, str4.length() - 1);
                            }
                            sb3.append(str4);
                        }
                        try {
                            bArr2 = sb3.toString().getBytes(this.A02);
                        } catch (UnsupportedEncodingException e2) {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Failed to encode: charset=");
                            sb4.append(this.A02);
                            Log.e(sb4.toString(), e2);
                            bArr2 = sb3.toString().getBytes();
                        }
                        if (bArr2 == null) {
                            bArr = null;
                        } else {
                            try {
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                while (i < bArr2.length) {
                                    byte b = bArr2[i];
                                    if (b == 61) {
                                        int i4 = i + 1;
                                        try {
                                            byte b2 = bArr2[i4];
                                            int digit = Character.digit((char) b2, 16);
                                            if (digit != -1) {
                                                i = i4 + 1;
                                                byte b3 = bArr2[i];
                                                int digit2 = Character.digit((char) b3, 16);
                                                if (digit2 != -1) {
                                                    byteArrayOutputStream.write((char) ((digit << 4) + digit2));
                                                } else {
                                                    StringBuilder sb5 = new StringBuilder("Invalid URL encoding: not a valid digit (radix 16): ");
                                                    sb5.append((int) b3);
                                                    throw new AnonymousClass4CF(sb5.toString());
                                                }
                                            } else {
                                                StringBuilder sb6 = new StringBuilder("Invalid URL encoding: not a valid digit (radix 16): ");
                                                sb6.append((int) b2);
                                                throw new AnonymousClass4CF(sb6.toString());
                                            }
                                        } catch (ArrayIndexOutOfBoundsException e3) {
                                            throw new AnonymousClass4CF(e3);
                                        }
                                    } else {
                                        byteArrayOutputStream.write(b);
                                    }
                                    i++;
                                }
                                bArr = byteArrayOutputStream.toByteArray();
                            } catch (AnonymousClass4CF e4) {
                                Log.e("Failed to decode quoted-printable: ", e4);
                                str3 = "";
                            }
                        }
                        try {
                            str3 = new String(bArr, str2);
                        } catch (UnsupportedEncodingException e5) {
                            e = e5;
                            sb = new StringBuilder();
                            sb.append("Failed to encode: charset=");
                            sb.append(str2);
                            Log.e(sb.toString(), e);
                            str3 = new String(bArr);
                            list2.add(str3);
                        }
                    }
                    list2.add(str3);
                }
                String str5 = this.A02;
                if (!str5.equalsIgnoreCase(str2)) {
                    ByteBuffer encode = Charset.forName(str5).encode(str3);
                    bArr = new byte[encode.remaining()];
                    encode.get(bArr);
                    try {
                        str3 = new String(bArr, str2);
                    } catch (UnsupportedEncodingException e6) {
                        e = e6;
                        sb = new StringBuilder("Failed to encode: charset=");
                        sb.append(str2);
                        Log.e(sb.toString(), e);
                        str3 = new String(bArr);
                        list2.add(str3);
                    }
                }
                list2.add(str3);
            }
            r6 = this.A05;
            List<String> list3 = r6.A03;
            int size = list3.size();
            if (size > 1) {
                StringBuilder sb7 = new StringBuilder();
                for (String str6 : list3) {
                    sb7.append(str6);
                    sb7.append(";");
                }
                int length2 = sb7.length();
                if (length2 > 0) {
                    int i5 = length2 - 1;
                    if (sb7.charAt(i5) == ';') {
                        str = sb7.substring(0, i5);
                    }
                }
                str = sb7.toString();
            } else {
                str = size == 1 ? (String) list3.get(0) : "";
            }
        }
        r6.A02 = str;
    }
}
