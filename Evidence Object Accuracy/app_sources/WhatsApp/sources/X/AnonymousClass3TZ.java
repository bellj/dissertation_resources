package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;

/* renamed from: X.3TZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3TZ implements AnonymousClass5T0 {
    public final /* synthetic */ AnonymousClass39w A00;
    public final /* synthetic */ AnonymousClass39w A01;
    public final /* synthetic */ C47762Cm A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass3TZ(AnonymousClass39w r1, AnonymousClass39w r2, C47762Cm r3, boolean z) {
        this.A02 = r3;
        this.A03 = z;
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AnonymousClass5T0
    public boolean Afj(int i, Object obj) {
        EnumC87104Af r5;
        float f;
        float[] fArr;
        int i2;
        int i3;
        C47762Cm r1;
        int i4;
        try {
            if (i != 35) {
                if (i == 36) {
                    r1 = this.A02;
                    f = ((Number) obj).floatValue();
                    if (C47762Cm.A01(f)) {
                        return false;
                    }
                    fArr = C12980iv.A1Y(r1);
                    i2 = r1.A00;
                    i3 = i2 + 1;
                    r1.A00 = i3;
                    i4 = 22;
                } else if (i != 38) {
                    if (i != 63) {
                        switch (i) {
                            case 40:
                                if (this.A03) {
                                    AnonymousClass3JQ.A05(this.A00, this.A02, (String) obj);
                                    return false;
                                }
                                C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'end' value since 'position' is not 'absolute'");
                                return false;
                            case 41:
                                r1 = this.A02;
                                f = ((Number) obj).floatValue();
                                if (f != 0.0f) {
                                    fArr = C12980iv.A1Y(r1);
                                    i2 = r1.A00;
                                    i3 = i2 + 1;
                                    r1.A00 = i3;
                                    i4 = 1;
                                    break;
                                } else {
                                    return false;
                                }
                            case 42:
                                String str = (String) obj;
                                if (str.endsWith("%")) {
                                    r1 = this.A02;
                                    f = AnonymousClass3JW.A00(str);
                                    if (!C47762Cm.A01(f)) {
                                        fArr = C12980iv.A1Y(r1);
                                        i2 = r1.A00;
                                        i3 = i2 + 1;
                                        r1.A00 = i3;
                                        i4 = 14;
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else if (str.equals("auto")) {
                                    return false;
                                } else {
                                    r1 = this.A02;
                                    f = AnonymousClass3JW.A01(str);
                                    if (!C47762Cm.A01(f)) {
                                        fArr = C12980iv.A1Y(r1);
                                        i2 = r1.A00;
                                        i3 = i2 + 1;
                                        r1.A00 = i3;
                                        i4 = 13;
                                        break;
                                    } else {
                                        return false;
                                    }
                                }
                            case 43:
                                if (this.A03) {
                                    AnonymousClass3JQ.A05(AnonymousClass39w.A02, this.A02, (String) obj);
                                    return false;
                                }
                                C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'left' value since 'position' is not 'absolute'");
                                return false;
                            case 44:
                                AnonymousClass3JQ.A04(AnonymousClass39w.A01, this.A02, (String) obj);
                                return false;
                            case 45:
                                AnonymousClass3JQ.A04(this.A00, this.A02, (String) obj);
                                return false;
                            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                                AnonymousClass3JQ.A04(AnonymousClass39w.A02, this.A02, (String) obj);
                                return false;
                            default:
                                switch (i) {
                                    case 48:
                                        AnonymousClass3JQ.A04(AnonymousClass39w.A03, this.A02, (String) obj);
                                        return false;
                                    case 49:
                                        AnonymousClass3JQ.A04(this.A01, this.A02, (String) obj);
                                        return false;
                                    case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                        AnonymousClass3JQ.A04(AnonymousClass39w.A04, this.A02, (String) obj);
                                        return false;
                                    case 51:
                                        String str2 = (String) obj;
                                        if (str2.endsWith("%")) {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A00(str2);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 19;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A01(str2);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 18;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        }
                                    case 52:
                                        String str3 = (String) obj;
                                        if (str3.endsWith("%")) {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A00(str3);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 12;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A01(str3);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 11;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        }
                                    case 53:
                                        String str4 = (String) obj;
                                        if (str4.endsWith("%")) {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A00(str4);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 17;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A01(str4);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 16;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        }
                                    case 54:
                                        String str5 = (String) obj;
                                        if (str5.endsWith("%")) {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A00(str5);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 10;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        } else {
                                            r1 = this.A02;
                                            f = AnonymousClass3JW.A01(str5);
                                            if (!C47762Cm.A01(f)) {
                                                fArr = C12980iv.A1Y(r1);
                                                i2 = r1.A00;
                                                i3 = i2 + 1;
                                                r1.A00 = i3;
                                                i4 = 9;
                                                break;
                                            } else {
                                                return false;
                                            }
                                        }
                                    default:
                                        switch (i) {
                                            case 65:
                                                r1 = this.A02;
                                                f = ((Number) obj).floatValue();
                                                if (f != 1.0f) {
                                                    i4 = 2;
                                                    C47762Cm.A00(r1, 2);
                                                    fArr = r1.A01;
                                                    i2 = r1.A00;
                                                    i3 = i2 + 1;
                                                    r1.A00 = i3;
                                                    break;
                                                } else {
                                                    return false;
                                                }
                                            case 66:
                                                if (this.A03) {
                                                    AnonymousClass3JQ.A05(this.A01, this.A02, (String) obj);
                                                    return false;
                                                }
                                                C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'start' value since 'position' is not 'absolute'");
                                                return false;
                                            case 67:
                                                if (this.A03) {
                                                    AnonymousClass3JQ.A05(AnonymousClass39w.A04, this.A02, (String) obj);
                                                    return false;
                                                }
                                                C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'top' value since 'position' is not 'absolute'");
                                                return false;
                                            case 68:
                                                String str6 = (String) obj;
                                                if (str6.endsWith("%")) {
                                                    r1 = this.A02;
                                                    f = AnonymousClass3JW.A00(str6);
                                                    if (!C47762Cm.A01(f)) {
                                                        fArr = C12980iv.A1Y(r1);
                                                        i2 = r1.A00;
                                                        i3 = i2 + 1;
                                                        r1.A00 = i3;
                                                        i4 = 7;
                                                        break;
                                                    } else {
                                                        return false;
                                                    }
                                                } else if (str6.equals("auto")) {
                                                    return false;
                                                } else {
                                                    r1 = this.A02;
                                                    f = AnonymousClass3JW.A01(str6);
                                                    if (!C47762Cm.A01(f)) {
                                                        fArr = C12980iv.A1Y(r1);
                                                        i2 = r1.A00;
                                                        i3 = i2 + 1;
                                                        r1.A00 = i3;
                                                        i4 = 6;
                                                        break;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                            default:
                                                return false;
                                        }
                                }
                        }
                    } else if (this.A03) {
                        AnonymousClass3JQ.A05(AnonymousClass39w.A03, this.A02, (String) obj);
                        return false;
                    } else {
                        C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'right' value since 'position' is not 'absolute'");
                        return false;
                    }
                } else if (this.A03) {
                    AnonymousClass3JQ.A05(AnonymousClass39w.A01, this.A02, (String) obj);
                    return false;
                } else {
                    C28691Op.A00("BloksFlexLayoutProvider", "Ignoring 'bottom' value since 'position' is not 'absolute'");
                    return false;
                }
                fArr[i2] = (float) i4;
                r1.A00 = i3 + 1;
                fArr[i3] = f;
                return false;
            }
            C47762Cm r4 = this.A02;
            String str7 = (String) obj;
            switch (str7.hashCode()) {
                case -1881872635:
                    if (str7.equals("stretch")) {
                        r5 = EnumC87104Af.STRETCH;
                        break;
                    }
                    r5 = EnumC87104Af.AUTO;
                    break;
                case -1720785339:
                    if (str7.equals("baseline")) {
                        r5 = EnumC87104Af.BASELINE;
                        break;
                    }
                    r5 = EnumC87104Af.AUTO;
                    break;
                case -1364013995:
                    if (str7.equals("center")) {
                        r5 = EnumC87104Af.CENTER;
                        break;
                    }
                    r5 = EnumC87104Af.AUTO;
                    break;
                case 1384876188:
                    if (str7.equals("flex_start")) {
                        r5 = EnumC87104Af.FLEX_START;
                        break;
                    }
                    r5 = EnumC87104Af.AUTO;
                    break;
                case 1744442261:
                    if (str7.equals("flex_end")) {
                        r5 = EnumC87104Af.FLEX_END;
                        break;
                    }
                    r5 = EnumC87104Af.AUTO;
                    break;
                default:
                    r5 = EnumC87104Af.AUTO;
                    break;
            }
            if (r5 == C47762Cm.A02) {
                return false;
            }
            float[] A1Y = C12980iv.A1Y(r4);
            int i5 = r4.A00;
            int i6 = i5 + 1;
            r4.A00 = i6;
            A1Y[i5] = (float) 20;
            r4.A00 = i6 + 1;
            A1Y[i6] = (float) r5.ordinal();
            return false;
        } catch (AnonymousClass491 e) {
            C28691Op.A01("BloksFlexLayoutProvider", "Error parsing flexbox style value", e);
            return false;
        }
    }
}
