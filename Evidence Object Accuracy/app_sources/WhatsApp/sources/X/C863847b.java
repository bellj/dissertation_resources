package X;

import android.view.View;
import android.widget.FrameLayout;

/* renamed from: X.47b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C863847b extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ FrameLayout A02;
    public final /* synthetic */ AnonymousClass3IJ A03;
    public final /* synthetic */ AnonymousClass35V A04;

    public C863847b(FrameLayout frameLayout, AnonymousClass3IJ r2, AnonymousClass35V r3, int i, int i2) {
        this.A04 = r3;
        this.A02 = frameLayout;
        this.A01 = i;
        this.A00 = i2;
        this.A03 = r2;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        this.A04.A0D(this.A02, this.A03, this.A01, this.A00);
        view.setOnClickListener(null);
    }
}
