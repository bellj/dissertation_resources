package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.OutputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.3YD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YD implements AbstractC116725Wo {
    public final AbstractC116725Wo A00;
    public final C14370lK A01;
    public final String A02;
    public final String A03;
    public final MessageDigest A04;
    public final MessageDigest A05;

    @Override // X.AbstractC116725Wo
    public long AEj() {
        return 0;
    }

    @Override // X.AbstractC116725Wo
    public void AfU() {
    }

    public AnonymousClass3YD(AbstractC116725Wo r3, C14370lK r4, String str, String str2) {
        MessageDigest messageDigest;
        MessageDigest messageDigest2;
        this.A00 = r3;
        this.A02 = str;
        this.A03 = str2;
        this.A01 = r4;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.e("encryptedstreamdownload/digest error", e);
            messageDigest = null;
        }
        this.A05 = messageDigest;
        try {
            messageDigest2 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e2) {
            Log.e("encryptedstreamdownload/digest error", e2);
            messageDigest2 = null;
        }
        this.A04 = messageDigest2;
    }

    @Override // X.AbstractC116725Wo
    public OutputStream AYn(AbstractC37631mk r7) {
        MessageDigest messageDigest;
        MessageDigest messageDigest2 = this.A05;
        if (messageDigest2 != null && (messageDigest = this.A04) != null) {
            return new DigestOutputStream(new C629539h(new AnonymousClass2QH(this.A01).A8r(Base64.decode(this.A02, 0)), new DigestOutputStream(this.A00.AYn(r7), messageDigest), r7.getContentLength()), messageDigest2);
        }
        throw new AnonymousClass4C5(1);
    }
}
