package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import java.lang.ref.WeakReference;
import java.util.Set;

/* renamed from: X.3gC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ResultReceiverC73413gC extends ResultReceiver {
    public final WeakReference A00;
    public final WeakReference A01;

    public ResultReceiverC73413gC(Handler handler, Runnable runnable, Set set) {
        super(handler);
        set.add(runnable);
        this.A00 = C12970iu.A10(runnable);
        this.A01 = C12970iu.A10(set);
    }

    @Override // android.os.ResultReceiver
    public void onReceiveResult(int i, Bundle bundle) {
        Runnable runnable = (Runnable) this.A00.get();
        if (runnable != null) {
            runnable.run();
            Set set = (Set) this.A01.get();
            if (set != null) {
                set.remove(runnable);
            }
        }
    }
}
