package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1sQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40841sQ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40841sQ A0E;
    public static volatile AnonymousClass255 A0F;
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public AbstractC27881Jp A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public C43261wh A09;
    public String A0A;
    public String A0B = "";
    public String A0C = "";
    public boolean A0D;

    static {
        C40841sQ r0 = new C40841sQ();
        A0E = r0;
        r0.A0W();
    }

    public C40841sQ() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A05 = r0;
        this.A06 = r0;
        this.A04 = r0;
        this.A0A = "";
        this.A07 = r0;
        this.A08 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C81603uH r2;
        switch (r16.ordinal()) {
            case 0:
                return A0E;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C40841sQ r1 = (C40841sQ) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A0C;
                int i2 = r1.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A0C = r8.Afy(str, r1.A0C, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0B;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A0B = r8.Afy(str2, r1.A0B, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r3 = this.A05;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A05 = r8.Afm(r3, r1.A05, z5, z6);
                int i3 = this.A00;
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                long j = this.A02;
                int i4 = r1.A00;
                boolean z8 = false;
                if ((i4 & 8) == 8) {
                    z8 = true;
                }
                this.A02 = r8.Afs(j, r1.A02, z7, z8);
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                int i5 = this.A01;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A01 = r8.Afp(i5, r1.A01, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                boolean z12 = this.A0D;
                boolean z13 = false;
                if ((i4 & 32) == 32) {
                    z13 = true;
                }
                this.A0D = r8.Afl(z11, z12, z13, r1.A0D);
                boolean z14 = false;
                if ((i3 & 64) == 64) {
                    z14 = true;
                }
                AbstractC27881Jp r32 = this.A06;
                boolean z15 = false;
                if ((i4 & 64) == 64) {
                    z15 = true;
                }
                this.A06 = r8.Afm(r32, r1.A06, z14, z15);
                boolean z16 = false;
                if ((this.A00 & 128) == 128) {
                    z16 = true;
                }
                AbstractC27881Jp r4 = this.A04;
                boolean z17 = false;
                if ((r1.A00 & 128) == 128) {
                    z17 = true;
                }
                this.A04 = r8.Afm(r4, r1.A04, z16, z17);
                int i6 = this.A00;
                boolean z18 = false;
                if ((i6 & 256) == 256) {
                    z18 = true;
                }
                String str3 = this.A0A;
                int i7 = r1.A00;
                boolean z19 = false;
                if ((i7 & 256) == 256) {
                    z19 = true;
                }
                this.A0A = r8.Afy(str3, r1.A0A, z18, z19);
                boolean z20 = false;
                if ((i6 & 512) == 512) {
                    z20 = true;
                }
                long j2 = this.A03;
                boolean z21 = false;
                if ((i7 & 512) == 512) {
                    z21 = true;
                }
                this.A03 = r8.Afs(j2, r1.A03, z20, z21);
                this.A09 = (C43261wh) r8.Aft(this.A09, r1.A09);
                boolean z22 = false;
                if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z22 = true;
                }
                AbstractC27881Jp r42 = this.A07;
                boolean z23 = false;
                if ((r1.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z23 = true;
                }
                this.A07 = r8.Afm(r42, r1.A07, z22, z23);
                boolean z24 = false;
                if ((this.A00 & 4096) == 4096) {
                    z24 = true;
                }
                AbstractC27881Jp r43 = this.A08;
                boolean z25 = false;
                if ((r1.A00 & 4096) == 4096) {
                    z25 = true;
                }
                this.A08 = r8.Afm(r43, r1.A08, z24, z25);
                if (r8 == C463025i.A00) {
                    this.A00 |= r1.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r82.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A0C = A0A;
                                    break;
                                case 18:
                                    String A0A2 = r82.A0A();
                                    this.A00 |= 2;
                                    this.A0B = A0A2;
                                    break;
                                case 26:
                                    this.A00 |= 4;
                                    this.A05 = r82.A08();
                                    break;
                                case 32:
                                    this.A00 |= 8;
                                    this.A02 = r82.A06();
                                    break;
                                case 40:
                                    this.A00 |= 16;
                                    this.A01 = r82.A02();
                                    break;
                                case 48:
                                    this.A00 |= 32;
                                    this.A0D = r82.A0F();
                                    break;
                                case 58:
                                    this.A00 |= 64;
                                    this.A06 = r82.A08();
                                    break;
                                case 66:
                                    this.A00 |= 128;
                                    this.A04 = r82.A08();
                                    break;
                                case 74:
                                    String A0A3 = r82.A0A();
                                    this.A00 |= 256;
                                    this.A0A = A0A3;
                                    break;
                                case 80:
                                    this.A00 |= 512;
                                    this.A03 = r82.A06();
                                    break;
                                case 138:
                                    if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                        r2 = (C81603uH) this.A09.A0T();
                                    } else {
                                        r2 = null;
                                    }
                                    C43261wh r0 = (C43261wh) r82.A09(r12, C43261wh.A0O.A0U());
                                    this.A09 = r0;
                                    if (r2 != null) {
                                        r2.A04(r0);
                                        this.A09 = (C43261wh) r2.A01();
                                    }
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    break;
                                case 146:
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A07 = r82.A08();
                                    break;
                                case 154:
                                    this.A00 |= 4096;
                                    this.A08 = r82.A08();
                                    break;
                                default:
                                    if (A0a(r82, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (IOException e) {
                            C28971Pt r13 = new C28971Pt(e.getMessage());
                            r13.unfinishedMessage = this;
                            throw new RuntimeException(r13);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C40841sQ();
            case 5:
                return new C81763uX();
            case 6:
                break;
            case 7:
                if (A0F == null) {
                    synchronized (C40841sQ.class) {
                        if (A0F == null) {
                            A0F = new AnonymousClass255(A0E);
                        }
                    }
                }
                return A0F;
            default:
                throw new UnsupportedOperationException();
        }
        return A0E;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A0C);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A0B);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A05, 3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A06(4, this.A02);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A04(5, this.A01);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A00(6);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A09(this.A06, 7);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A09(this.A04, 8);
        }
        if ((i3 & 256) == 256) {
            i2 += CodedOutputStream.A07(9, this.A0A);
        }
        int i4 = this.A00;
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A05(10, this.A03);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C43261wh r0 = this.A09;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 += CodedOutputStream.A0A(r0, 17);
        }
        int i5 = this.A00;
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A09(this.A07, 18);
        }
        if ((i5 & 4096) == 4096) {
            i2 += CodedOutputStream.A09(this.A08, 19);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0C);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0B);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A05, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A01);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0J(6, this.A0D);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0K(this.A06, 7);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0K(this.A04, 8);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0I(9, this.A0A);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0H(10, this.A03);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C43261wh r0 = this.A09;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0K(this.A07, 18);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0K(this.A08, 19);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
