package X;

import android.content.Context;

/* renamed from: X.0GZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0GZ extends AnonymousClass0Zt {
    public AnonymousClass0GZ(Context context, AbstractC11500gO r3) {
        super(C05990Rt.A00(context, r3).A00);
    }

    @Override // X.AnonymousClass0Zt
    public boolean A01(C004401z r2) {
        return r2.A09.A05;
    }

    @Override // X.AnonymousClass0Zt
    public boolean A02(Object obj) {
        return !((Boolean) obj).booleanValue();
    }
}
