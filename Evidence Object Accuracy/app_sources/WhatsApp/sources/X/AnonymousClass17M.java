package X;

import org.json.JSONObject;

/* renamed from: X.17M  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass17M {
    public final void A00(String str, String str2, String str3) {
        C16700pc.A0E(str, 1);
        C16700pc.A0E(str2, 2);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("config", str);
        jSONObject.put("isStartingState", true);
        jSONObject.put("sessionId", str2);
        jSONObject.put("referral", str3);
    }
}
