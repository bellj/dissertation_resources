package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZO extends Drawable {
    public final Paint A00;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZO() {
        Paint A0A = C12960it.A0A();
        this.A00 = A0A;
        C12970iu.A16(285212672, A0A);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), (float) (bounds.width() >> 1), this.A00);
    }
}
