package X;

import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.util.JsonReader;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26061Bw {
    public static final long A0L = 30000;
    public static final String A0M = "GoogleMigrateIntegrationManager/";
    public CancellationSignal A00;
    public CountDownLatch A01;
    public final C20850wQ A02;
    public final C26011Br A03;
    public final AbstractC15710nm A04;
    public final C14330lG A05;
    public final C14830m7 A06;
    public final C26041Bu A07;
    public final C21390xL A08;
    public final C25981Bo A09;
    public final C26031Bt A0A;
    public final C26021Bs A0B;
    public final C26001Bq A0C;
    public final C25991Bp A0D;
    public final C26051Bv A0E;
    public final C25651Af A0F;
    public final C18350sJ A0G;
    public final C25661Ag A0H;
    public final C25671Ah A0I;
    public final AnonymousClass01H A0J;
    public final AtomicBoolean A0K = new AtomicBoolean(false);

    public C26061Bw(C14830m7 r3, AbstractC15710nm r4, C14330lG r5, C25661Ag r6, C25981Bo r7, C25991Bp r8, C26001Bq r9, C21390xL r10, AnonymousClass01H r11, C26011Br r12, C18350sJ r13, C26021Bs r14, C26031Bt r15, C25651Af r16, C26041Bu r17, C20850wQ r18, C25671Ah r19, C26051Bv r20) {
        this.A06 = r3;
        this.A04 = r4;
        this.A05 = r5;
        this.A0H = r6;
        this.A0J = r11;
        this.A09 = r7;
        this.A0D = r8;
        this.A0C = r9;
        this.A08 = r10;
        this.A03 = r12;
        this.A0G = r13;
        this.A0B = r14;
        this.A0A = r15;
        this.A0F = r16;
        this.A07 = r17;
        this.A02 = r18;
        this.A0I = r19;
        this.A0E = r20;
    }

    private void A00() {
        A0D();
        File file = this.A05.A04().A0A;
        C14330lG.A03(file, false);
        C14350lI.A0C(file);
        File file2 = this.A05.A04().A0A;
        C14330lG.A03(file2, false);
        if (file2.exists()) {
            this.A04.AaV(A0M, "cancelImport/could not delete media folder", false);
            Log.e("GoogleMigrateIntegrationManager/cleanUpAfterCancellation()/could not delete media folder");
        }
        A02();
    }

    private void A01() {
        Log.i("GoogleMigrateIntegrationManager/cleanUpAfterImportCompleted()");
        this.A08.A04("cross_migration_data_cleanup_needed", 1);
        C26021Bs r2 = this.A0B;
        r2.A05.A04(r2.A04);
        r2.A03.A04(r2.A06);
        r2.A08.A04(r2.A07);
        synchronized (this) {
            this.A01 = null;
            this.A00 = null;
        }
    }

    private void A02() {
        A04(this.A02.A00());
    }

    private void A03() {
        if (!this.A0G.A0F()) {
            throw new AnonymousClass2GB(301, "GoogleMigrateIntegrationManager/can not find jabber Id");
        }
    }

    private void A04(C29851Uy r4) {
        int i = r4.A00;
        if (i == 2 || i == 1) {
            C25651Af r0 = this.A0F;
            r0.A00();
            r0.A01();
        }
    }

    public int A05() {
        int i;
        C26021Bs r1 = this.A0B;
        synchronized (r1) {
            i = r1.A00;
        }
        return i;
    }

    public int A06() {
        int i;
        C26021Bs r2 = this.A0B;
        synchronized (r2) {
            StringBuilder sb = new StringBuilder();
            sb.append("GoogleMigrate/getCurrentScreen = ");
            sb.append(r2.A01);
            Log.i(sb.toString());
            i = r2.A01;
        }
        return i;
    }

    public void A07() {
        CountDownLatch countDownLatch;
        Log.i("GoogleMigrateIntegrationManager/cancelImport()");
        boolean z = true;
        if (!this.A0K.getAndSet(true)) {
            synchronized (this) {
                if (this.A00 == null || this.A01 == null) {
                    Log.i("GoogleMigrateIntegrationManager/cancellationSignal or importCompleted is null");
                    A00();
                } else {
                    z = false;
                }
                countDownLatch = this.A01;
                if (this.A00 != null) {
                    Log.i("GoogleMigrateIntegrationManager/cancelImport()/cancellationSignal.cancel");
                    this.A00.cancel();
                }
            }
            if (z) {
                this.A0A.A05();
                A01();
                return;
            }
            try {
                try {
                    for (AnonymousClass5XP r0 : this.A0A.A01()) {
                        r0.ANh();
                    }
                    if (countDownLatch != null) {
                        countDownLatch.await(A0L, TimeUnit.MILLISECONDS);
                    }
                } catch (InterruptedException e) {
                    this.A0A.A06(2);
                    Log.e("GoogleMigrateIntegrationManager/cancelImportProcess()/InterruptedException", e);
                }
            } finally {
                A00();
                this.A0A.A05();
                A01();
                this.A0K.set(false);
            }
        } else {
            Log.e("GoogleMigrateIntegrationManager/concurrent cancelImport requested, not supported");
            throw new IllegalStateException("Multiple concurrent operations are not supported.");
        }
    }

    public void A08() {
        if (this.A08.A01("cross_migration_data_cleanup_needed", 0) == 1) {
            this.A09.A09();
            this.A0J.get();
            if (this.A03.A04()) {
                try {
                    this.A03.A03();
                } catch (IOException e) {
                    AbstractC15710nm r2 = this.A04;
                    StringBuilder sb = new StringBuilder("failed to delete remote data: ");
                    sb.append(e.toString());
                    r2.A02("xpm-integration-delete-failed", sb.toString(), e);
                    Log.e("GoogleMigrateIntegrationManager/deferredCleanup()/could not delete all data from Google Migrate");
                }
            }
            this.A08.A03("cross_migration_data_cleanup_needed");
            return;
        }
        Log.i("GoogleMigrateIntegrationManager/deferredCleanup()/does not need to cleanup");
    }

    public void A09() {
        this.A03.A04();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0148 A[LOOP:1: B:44:0x0142->B:46:0x0148, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A() {
        /*
        // Method dump skipped, instructions count: 574
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26061Bw.A0A():void");
    }

    public void A0B() {
        this.A02.A00();
    }

    public void A0C() {
        C26021Bs r1 = this.A0B;
        synchronized (r1) {
            r1.A01 = 0;
        }
    }

    public void A0D() {
        this.A07.A00();
        C16490p7 r0 = this.A02.A02;
        r0.A04();
        r0.A05();
    }

    public void A0E() {
        Log.i("GoogleMigrateIntegrationManager/saveLoggingInfoFromiOS()");
        try {
            ParcelFileDescriptor A00 = this.A03.A00("migration/metadata.json");
            FileInputStream fileInputStream = new FileInputStream(A00.getFileDescriptor());
            JsonReader jsonReader = new JsonReader(new InputStreamReader(fileInputStream));
            try {
                jsonReader.beginObject();
                String str = null;
                String str2 = null;
                while (jsonReader.hasNext()) {
                    if ("attemptInfo".equals(jsonReader.nextName())) {
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String nextName = jsonReader.nextName();
                            if ("attemptCompletionTime".equals(nextName)) {
                                str2 = Double.toString(jsonReader.nextDouble());
                            } else if ("attemptID".equals(nextName)) {
                                str = jsonReader.nextString();
                            }
                        }
                        jsonReader.endObject();
                    } else {
                        jsonReader.skipValue();
                    }
                }
                jsonReader.endObject();
                if (str == null) {
                    throw new IOException("Invalid metadata file: iOSFunnelId is missing.");
                } else if (str2 != null) {
                    C90464Nz r4 = new C90464Nz(str, str2);
                    jsonReader.close();
                    this.A0I.A00().edit().putString("google_migrate_ios_export_duration", r4.A00).apply();
                    this.A0I.A00().edit().putString("google_migrate_ios_funnel_id", r4.A01).apply();
                    fileInputStream.close();
                    A00.close();
                } else {
                    throw new IOException("Invalid metadata file: iOSExportDuration is missing.");
                }
            } catch (Throwable th) {
                try {
                    jsonReader.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException e) {
            Log.e("GoogleMigrateIntegrationManager/saveLoggingInfoFromiOS()/", e);
            this.A04.A02("xpm-integration-no-funnel-id", "saveLoggingInfoFromiOS;", e);
        }
    }

    public void A0F(int i) {
        try {
            for (AnonymousClass5XP r0 : this.A0A.A01()) {
                r0.AU9();
            }
            A0D();
        } finally {
            for (AnonymousClass5XP r02 : this.A0A.A01()) {
                r02.AU8(i);
            }
        }
    }

    public boolean A0G() {
        try {
            return this.A08.A00("cross_platform_migration_completed", 0) == 1;
        } catch (RuntimeException unused) {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0008, code lost:
        if (r2.A01 == null) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0H() {
        /*
            r2 = this;
            monitor-enter(r2)
            android.os.CancellationSignal r0 = r2.A00     // Catch: all -> 0x000d
            if (r0 == 0) goto L_0x000a
            java.util.concurrent.CountDownLatch r1 = r2.A01     // Catch: all -> 0x000d
            r0 = 1
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            monitor-exit(r2)
            return r0
        L_0x000d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26061Bw.A0H():boolean");
    }
}
