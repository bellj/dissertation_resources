package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43801xc extends AbstractC16280ok {
    public final Context A00;
    public final C231410n A01;

    public C43801xc(Context context, AbstractC15710nm r9, C231410n r10, C14850m9 r11) {
        super(context, r9, "hsmpacks.db", null, 2, r11.A07(781));
        this.A00 = context;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteException e) {
            Log.e("failed to open pack store", e);
            A04();
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        }
    }

    public synchronized void A04() {
        close();
        Log.i("deleting HSM pack database...");
        File databasePath = this.A00.getDatabasePath("hsmpacks.db");
        boolean delete = databasePath.delete();
        StringBuilder sb = new StringBuilder();
        sb.append("language-pack-store/deleted HSM pack database; databaseDeleted=");
        sb.append(delete);
        Log.i(sb.toString());
        AnonymousClass1Tx.A04(databasePath, "language-pack-store");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS packs");
        sQLiteDatabase.execSQL("CREATE TABLE packs (_id INTEGER PRIMARY KEY AUTOINCREMENT, lg TEXT NOT NULL, lc TEXT NOT NULL, hash TEXT NOT NULL, namespace TEXT NOT NULL, timestamp INTEGER NOT NULL, data BLOB NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS pack_index ON packs (lg, lc, namespace)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("language-pack-store/downgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("language-pack-store/upgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        if (i != 1) {
            Log.e("language-pack-store/upgrade unknown old version");
        }
        onCreate(sQLiteDatabase);
    }
}
