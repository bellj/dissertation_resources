package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5hG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121105hG extends AnonymousClass6BG {
    public final AnonymousClass12P A00;
    public final C14900mE A01;
    public final C15450nH A02;
    public final C18790t3 A03;
    public final C21740xu A04;
    public final C18640sm A05;
    public final C14830m7 A06;
    public final AnonymousClass018 A07;
    public final C20370ve A08;
    public final AnonymousClass102 A09;
    public final C14850m9 A0A;
    public final C17220qS A0B;
    public final AnonymousClass68Z A0C;
    public final C1308460e A0D;
    public final C1329668y A0E;
    public final AnonymousClass698 A0F;
    public final C21860y6 A0G;
    public final C18650sn A0H;
    public final C18660so A0I;
    public final AnonymousClass18P A0J;
    public final AbstractC17860rW A0K;
    public final C18600si A0L;
    public final C243515e A0M;
    public final C18610sj A0N;
    public final C22710zW A0O;
    public final AnonymousClass6BE A0P;
    public final AnonymousClass17Z A0Q;
    public final AnonymousClass69A A0R;
    public final AnonymousClass69E A0S;
    public final AnonymousClass18O A0T;
    public final C121265hX A0U;
    public final C1310060v A0V;
    public final C18590sh A0W;
    public final AbstractC14440lR A0X;

    @Override // X.AbstractC16830pp
    public int AGi() {
        return 1;
    }

    public C121105hG(AnonymousClass12P r9, C14900mE r10, C15450nH r11, C18790t3 r12, C21740xu r13, C15550nR r14, C15610nY r15, C18640sm r16, C14830m7 r17, C16590pI r18, AnonymousClass018 r19, C20370ve r20, AnonymousClass102 r21, C14850m9 r22, C17220qS r23, AnonymousClass68Z r24, C1308460e r25, C1329668y r26, AnonymousClass698 r27, C21860y6 r28, C18650sn r29, C18660so r30, AnonymousClass18P r31, AbstractC17860rW r32, C18600si r33, C243515e r34, C18610sj r35, C22710zW r36, C17070qD r37, AnonymousClass6BE r38, AnonymousClass17Z r39, AnonymousClass69A r40, AnonymousClass69E r41, AnonymousClass18O r42, C121265hX r43, C1310060v r44, C18590sh r45, AnonymousClass14X r46, AbstractC14440lR r47) {
        super(r14, r15, r18, r37, r46, "UPI");
        this.A06 = r17;
        this.A04 = r13;
        this.A0A = r22;
        this.A01 = r10;
        this.A0X = r47;
        this.A03 = r12;
        this.A02 = r11;
        this.A00 = r9;
        this.A0B = r23;
        this.A07 = r19;
        this.A0W = r45;
        this.A0V = r44;
        this.A0L = r33;
        this.A0S = r41;
        this.A0G = r28;
        this.A0D = r25;
        this.A0N = r35;
        this.A0O = r36;
        this.A0R = r40;
        this.A09 = r21;
        this.A0Q = r39;
        this.A08 = r20;
        this.A0K = r32;
        this.A0C = r24;
        this.A0P = r38;
        this.A05 = r16;
        this.A0F = r27;
        this.A0H = r29;
        this.A0T = r42;
        this.A0E = r26;
        this.A0J = r31;
        this.A0I = r30;
        this.A0M = r34;
        this.A0U = r43;
    }

    @Override // X.AnonymousClass6BG, X.AbstractC16830pp
    public List AF1(AnonymousClass1IR r11, AnonymousClass1IS r12) {
        ArrayList arrayList;
        ArrayList A0l;
        List AF1 = super.AF1(r11, r12);
        AbstractC30891Zf r3 = r11.A0A;
        if (r3 instanceof C119835fB) {
            C119835fB r32 = (C119835fB) r3;
            if (!TextUtils.isEmpty(r32.A0P)) {
                arrayList = C12960it.A0l();
                AnonymousClass1W9[] r8 = new AnonymousClass1W9[1];
                C12960it.A1M("token", r32.A0P, r8, 0);
                C38171nd r0 = ((AbstractC30891Zf) r32).A02;
                AnonymousClass1V8 r2 = null;
                if (r0 != null) {
                    ArrayList A0l2 = C12960it.A0l();
                    if (!TextUtils.isEmpty(r0.A01)) {
                        C38171nd r22 = ((AbstractC30891Zf) r32).A02;
                        C117295Zj.A1M("order-id", r22.A01, A0l2);
                        long j = r22.A00;
                        if (j != 0) {
                            A0l2.add(new AnonymousClass1W9("expiry-ts", j));
                        }
                        r2 = new AnonymousClass1V8("order", C117305Zk.A1b(A0l2));
                    }
                }
                arrayList.add(new AnonymousClass1V8(r2, "upi", r8));
                A0l = C12960it.A0l();
                if (AF1 != null && !AF1.isEmpty()) {
                    A0l.addAll(AF1);
                }
                if (arrayList != null && !arrayList.isEmpty()) {
                    A0l.addAll(arrayList);
                }
                return A0l;
            }
        }
        arrayList = null;
        A0l = C12960it.A0l();
        if (AF1 != null) {
            A0l.addAll(AF1);
        }
        if (arrayList != null) {
            A0l.addAll(arrayList);
        }
        return A0l;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0084  */
    @Override // X.AbstractC16830pp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C47822Cw AHU(X.AnonymousClass1ZO r11, com.whatsapp.jid.UserJid r12, java.lang.String r13) {
        /*
            r10 = this;
            r4 = 0
            X.0m9 r8 = r10.A0A
            r0 = 733(0x2dd, float:1.027E-42)
            boolean r0 = r8.A07(r0)
            r5 = 0
            if (r0 == 0) goto L_0x008b
            X.0y6 r1 = r10.A0G
            boolean r0 = r1.A0C()
            if (r0 != 0) goto L_0x001a
            boolean r0 = r1.A0B()
            if (r0 == 0) goto L_0x008b
        L_0x001a:
            r9 = 1
        L_0x001b:
            java.util.HashMap r3 = X.C12970iu.A11()
            java.util.HashMap r2 = X.C12970iu.A11()
            java.lang.String r6 = ""
            if (r9 == 0) goto L_0x003b
            if (r11 == 0) goto L_0x0089
            r1 = 3
            X.1ZQ r0 = r11.A07()
            java.util.Map r0 = r0.A01
            java.lang.Object r1 = X.C12990iw.A0l(r0, r1)
        L_0x0034:
            java.lang.Integer r0 = X.C12960it.A0V()
            r3.put(r0, r1)
        L_0x003b:
            X.17Z r7 = r10.A0Q
            boolean r0 = r7.A0A()
            if (r0 == 0) goto L_0x007e
            r0 = 888(0x378, float:1.244E-42)
            boolean r0 = r8.A07(r0)
            if (r0 == 0) goto L_0x007e
            r1 = 2
            X.0zW r0 = r10.A0O
            if (r12 == 0) goto L_0x0084
            int r0 = r0.A00(r12)
        L_0x0054:
            if (r0 != r1) goto L_0x007e
            if (r12 == 0) goto L_0x0066
            X.0nR r0 = r7.A03
            X.0n3 r0 = r0.A0A(r12)
            if (r0 == 0) goto L_0x007e
            boolean r0 = r0.A0I()
            if (r0 == 0) goto L_0x007e
        L_0x0066:
            r5 = 1
            if (r11 == 0) goto L_0x007b
            r1 = 3
            X.1Zh r0 = r11.A03
            if (r0 != 0) goto L_0x0075
            X.1Zh r0 = new X.1Zh
            r0.<init>()
            r11.A03 = r0
        L_0x0075:
            java.util.Map r0 = r0.A01
            java.lang.Object r6 = X.C12990iw.A0l(r0, r1)
        L_0x007b:
            X.C12960it.A1J(r6, r2, r5)
        L_0x007e:
            if (r9 != 0) goto L_0x008d
            if (r5 != 0) goto L_0x008d
            r0 = 0
            return r0
        L_0x0084:
            int r0 = r0.A01(r13)
            goto L_0x0054
        L_0x0089:
            r1 = r6
            goto L_0x0034
        L_0x008b:
            r9 = 0
            goto L_0x001b
        L_0x008d:
            X.2Cw r0 = new X.2Cw
            r0.<init>(r4, r12, r3, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C121105hG.AHU(X.1ZO, com.whatsapp.jid.UserJid, java.lang.String):X.2Cw");
    }
}
