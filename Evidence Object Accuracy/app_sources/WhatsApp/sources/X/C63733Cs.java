package X;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3Cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63733Cs {
    public final Application.ActivityLifecycleCallbacks A00;
    public final WeakReference A01;
    public final List A02 = C12960it.A0l();
    public volatile boolean A03;

    public C63733Cs(Context context) {
        C96074eo r2 = new C96074eo(this);
        this.A00 = r2;
        Context context2 = context;
        while ((context2 instanceof ContextWrapper) && !(context2 instanceof Activity) && !(context2 instanceof Application) && !(context2 instanceof Service)) {
            context2 = ((ContextWrapper) context2).getBaseContext();
        }
        this.A01 = C12970iu.A10(context2);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(r2);
    }

    public void A00() {
        int i;
        Runnable[] runnableArr;
        List list = this.A02;
        synchronized (list) {
            this.A03 = true;
        }
        Context context = (Context) this.A01.get();
        if (context != null) {
            ((Application) context.getApplicationContext()).unregisterActivityLifecycleCallbacks(this.A00);
        }
        synchronized (list) {
            runnableArr = (Runnable[]) list.toArray(new Runnable[0]);
        }
        for (Runnable runnable : runnableArr) {
            runnable.run();
        }
        synchronized (list) {
            list.removeAll(Arrays.asList(runnableArr));
        }
    }
}
