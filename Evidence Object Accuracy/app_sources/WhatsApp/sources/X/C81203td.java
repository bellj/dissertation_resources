package X;

import java.io.Serializable;
import java.util.Comparator;

/* renamed from: X.3td  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81203td extends AbstractC112285Cu implements Serializable {
    public static final long serialVersionUID = 0;
    public final Comparator comparator;

    public C81203td(Comparator comparator) {
        this.comparator = comparator;
    }

    @Override // X.AbstractC112285Cu, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return this.comparator.compare(obj, obj2);
    }

    @Override // java.util.Comparator, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C81203td) {
            return this.comparator.equals(((C81203td) obj).comparator);
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.comparator.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        return this.comparator.toString();
    }
}
