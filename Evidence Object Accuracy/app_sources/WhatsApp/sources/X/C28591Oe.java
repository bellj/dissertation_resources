package X;

/* renamed from: X.1Oe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28591Oe {
    public final Boolean A00;

    public C28591Oe(Boolean bool) {
        this.A00 = bool;
    }

    public String toString() {
        Boolean bool = this.A00;
        if (bool == null) {
            return "undefined";
        }
        return Boolean.toString(bool.booleanValue());
    }
}
