package X;

import android.os.Build;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.0Tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06410Tm {
    public final Object A00;

    public C06410Tm(Object obj) {
        this.A00 = obj;
    }

    public static C06410Tm A00(int i) {
        AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(0, 1, i, 1, false);
        } else {
            collectionItemInfo = null;
        }
        return new C06410Tm(collectionItemInfo);
    }

    public static C06410Tm A01(int i, int i2, int i3, int i4, boolean z, boolean z2) {
        AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo;
        int i5 = Build.VERSION.SDK_INT;
        if (i5 >= 21) {
            collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2);
        } else if (i5 >= 19) {
            collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z);
        } else {
            collectionItemInfo = null;
        }
        return new C06410Tm(collectionItemInfo);
    }

    public int A02() {
        if (Build.VERSION.SDK_INT >= 19) {
            return ((AccessibilityNodeInfo.CollectionItemInfo) this.A00).getColumnIndex();
        }
        return 0;
    }

    public int A03() {
        if (Build.VERSION.SDK_INT >= 19) {
            return ((AccessibilityNodeInfo.CollectionItemInfo) this.A00).getColumnSpan();
        }
        return 0;
    }

    public int A04() {
        if (Build.VERSION.SDK_INT >= 19) {
            return ((AccessibilityNodeInfo.CollectionItemInfo) this.A00).getRowIndex();
        }
        return 0;
    }

    public int A05() {
        if (Build.VERSION.SDK_INT >= 19) {
            return ((AccessibilityNodeInfo.CollectionItemInfo) this.A00).getRowSpan();
        }
        return 0;
    }

    public boolean A06() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((AccessibilityNodeInfo.CollectionItemInfo) this.A00).isSelected();
        }
        return false;
    }
}
