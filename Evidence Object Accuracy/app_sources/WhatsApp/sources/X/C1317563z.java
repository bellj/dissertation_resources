package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.payments.ui.stepup.NoviTextInputQuestionRow;

/* renamed from: X.63z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1317563z implements TextWatcher {
    public final /* synthetic */ NoviTextInputQuestionRow A00;
    public final /* synthetic */ C127455uW A01;
    public final /* synthetic */ C129445xj A02;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C1317563z(NoviTextInputQuestionRow noviTextInputQuestionRow, C127455uW r2, C129445xj r3) {
        this.A00 = noviTextInputQuestionRow;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        this.A01.A02 = editable.toString();
        this.A02.A00();
    }
}
