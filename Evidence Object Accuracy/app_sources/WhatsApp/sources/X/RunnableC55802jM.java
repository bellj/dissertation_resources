package X;

import android.database.Cursor;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.2jM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55802jM extends EmptyBaseRunnable0 implements Runnable {
    public AnonymousClass4KT A00;
    public final int A01 = 100;
    public final long A02;
    public final long A03;
    public final C19990v2 A04;
    public final C15650ng A05;
    public final AnonymousClass134 A06;
    public final AbstractC14640lm A07;
    public final String A08;
    public final Set A09;

    public RunnableC55802jM(AnonymousClass4KT r2, C19990v2 r3, C15650ng r4, AnonymousClass134 r5, AbstractC14640lm r6, String str, Set set, long j, long j2) {
        this.A02 = j;
        this.A08 = str;
        this.A05 = r4;
        this.A07 = r6;
        this.A03 = j2;
        this.A04 = r3;
        this.A06 = r5;
        this.A09 = set;
        this.A00 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC15340mz r2;
        int i;
        try {
            Log.i("conversation/more-messages/loading/start");
            C15650ng r7 = this.A05;
            AbstractC14640lm r8 = this.A07;
            C30031Vu A09 = r7.A09(r8, this.A01, this.A03, this.A02);
            Cursor cursor = A09.A00;
            cursor.getCount();
            boolean A07 = this.A06.A07(r8, A09.A02);
            if (cursor.moveToFirst()) {
                r2 = r7.A0K.A02(cursor, r8, false, true);
            } else {
                r2 = null;
            }
            AnonymousClass4KT r1 = this.A00;
            if (r1 != null) {
                AnonymousClass1PE r0 = (AnonymousClass1PE) this.A04.A0B().get(r8);
                if (r0 == null) {
                    i = 0;
                } else {
                    i = r0.A02;
                }
                AnonymousClass3ET r4 = new AnonymousClass3ET(A09, r2, i, A07);
                C15360n1 r3 = r1.A00;
                r3.A0B = r4.A03;
                r3.A00 = r4.A00;
                C30031Vu r22 = r4.A01;
                r3.A09(r22.A01);
                r3.A0A(r22.A02);
                r3.A0D.A0A(r4);
            }
            Log.i("conversation/more-messages/loading/end");
            this.A00 = null;
        } finally {
            this.A09.remove(this.A08);
        }
    }
}
