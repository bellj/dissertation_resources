package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5ez  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119715ez extends AnonymousClass1ZP {
    public static final HashSet A01;
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(6);
    public Bundle A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        String[] strArr = new String[7];
        strArr[0] = "vpa";
        strArr[1] = "keys";
        strArr[2] = "vpaName";
        strArr[3] = "balance";
        strArr[4] = "usableBalance";
        strArr[5] = "updatedSenderVpa";
        A01 = C12970iu.A13("sufficientBalance", strArr, 6);
    }

    public static final String A00(AnonymousClass1V8 r6, String str) {
        AnonymousClass1V8 A0E = r6.A0E(str);
        if (A0E == null) {
            return C117295Zj.A0W(r6, str);
        }
        try {
            AnonymousClass1V8 A0F = A0E.A0F("money");
            return String.valueOf(((double) ((long) A0F.A04("value"))) / ((double) ((long) A0F.A04("offset"))));
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: IndiaUpiPaymentData parseBalance failure");
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r16, AnonymousClass1V8 r17, int i) {
        ArrayList<String> A0l;
        ArrayList<String> A0l2;
        String A0I;
        Bundle bundle;
        String str;
        if (i == 4) {
            A0I = C117295Zj.A0W(r17, "credential-id");
            if (A0I != null) {
                bundle = C12970iu.A0D();
                this.A00 = bundle;
                str = "credentialId";
            } else {
                return;
            }
        } else if (i == 5) {
            String A0W = C117295Zj.A0W(r17, "keys");
            if (A0W != null) {
                Bundle A0D = C12970iu.A0D();
                this.A00 = A0D;
                A0D.putString("keys", A0W);
                return;
            }
            return;
        } else {
            if (i == 6) {
                this.A00 = C12970iu.A0D();
                String A0I2 = r17.A0I("vpa-mismatch", null);
                if (A0I2 != null) {
                    this.A00.putString("updatedVpaFor", A0I2);
                    if (C117295Zj.A1T(r17, "vpa-mismatch", null, "sender")) {
                        this.A00.putString("updatedSenderVpa", r17.A0I("vpa", null));
                        this.A00.putString("updatedSenderVpaId", r17.A0I("vpa-id", null));
                        return;
                    }
                    return;
                }
                String A0I3 = r17.A0I("valid", null);
                if (A0I3 != null) {
                    this.A00.putString("valid", A0I3);
                }
                String A00 = A00(r17, "balance");
                if (A00 != null) {
                    this.A00.putString("balance", A00);
                }
                this.A00.putString("sufficientBalance", r17.A0I("sufficient-balance", null));
            } else if (i == 8) {
                this.A00 = C12970iu.A0D();
                String A0I4 = r17.A0I("vpa-mismatch", null);
                if (A0I4 != null) {
                    this.A00.putString("updatedVpaFor", A0I4);
                    if (C117295Zj.A1T(r17, "vpa-mismatch", null, "sender")) {
                        this.A00.putString("updatedSenderVpa", r17.A0I("vpa", null));
                        this.A00.putString("updatedSenderVpaId", r17.A0I("vpa-id", null));
                    }
                    String A002 = A00(r17, "balance");
                    if (A002 != null) {
                        this.A00.putString("balance", A002);
                    }
                } else {
                    return;
                }
            } else if (i == 7) {
                this.A00 = C12970iu.A0D();
                this.A00.putString("vpa", r17.A0I("vpa", null));
                this.A00.putString("vpaId", r17.A0I("vpa-id", null));
                this.A00.putString("vpaName", r17.A0I("vpa-name", null));
                this.A00.putString("vpaValid", r17.A0I("valid", null));
                this.A00.putString("jid", r17.A0I("user", null));
                this.A00.putString("blocked", r17.A0I("blocked", null));
                this.A00.putString("token", r17.A0I("token", null));
                this.A00.putString("merchant", r17.A0I("merchant", null));
                A0I = r17.A0I("verified-merchant", null);
                bundle = this.A00;
                str = "verifiedMerchant";
            } else if (i == 2) {
                Bundle A0D2 = C12970iu.A0D();
                this.A00 = A0D2;
                String str2 = r17.A00;
                if ("psp".equals(str2)) {
                    A0D2.putString("providerType", r17.A0I("provider-type", null));
                    Bundle bundle2 = this.A00;
                    String A0I5 = r17.A0I("sms-gateways", null);
                    if (!TextUtils.isEmpty(A0I5)) {
                        A0l2 = C12980iv.A0x(Arrays.asList(A0I5.split(",")));
                    } else {
                        A0l2 = C12960it.A0l();
                    }
                    bundle2.putStringArrayList("smsGateways", A0l2);
                    this.A00.putString("smsPrefix", r17.A0I("sms-prefix", null));
                    this.A00.putString("transactionPrefix", r17.A0I("transaction-prefix", null));
                    return;
                } else if ("psp-routing".equals(str2)) {
                    String A0W2 = C117295Zj.A0W(r17, "providers");
                    if (!TextUtils.isEmpty(A0W2)) {
                        A0l = C12980iv.A0x(Arrays.asList(A0W2.split(",")));
                    } else {
                        A0l = C12960it.A0l();
                    }
                    A0D2.putStringArrayList("pspRouting", A0l);
                    return;
                } else {
                    return;
                }
            } else {
                return;
            }
            String A003 = A00(r17, "usable-balance");
            if (A003 != null) {
                this.A00.putString("usableBalance", A003);
                return;
            }
            return;
        }
        bundle.putString(str, A0I);
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: IndiaUpiPaymentData does not support toNetwork");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        throw C12980iv.A0u("PAY: IndiaUpiPaymentData does not support toDBString");
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        throw C12980iv.A0u("PAY: IndiaUpiPaymentData does not support fromDBString");
    }

    public AnonymousClass1ZR A05() {
        String str;
        AnonymousClass2SM A0J = C117305Zk.A0J();
        Bundle bundle = this.A00;
        if (bundle != null) {
            str = bundle.getString("updatedSenderVpa");
        } else {
            str = null;
        }
        return C117305Zk.A0I(A0J, String.class, str, "upiHandle");
    }

    public String A06() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("balance");
        }
        return null;
    }

    public String A07() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("jid");
        }
        return null;
    }

    public String A08() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("keys");
        }
        return null;
    }

    public String A09() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("providerType");
        }
        return null;
    }

    public String A0A() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("smsPrefix");
        }
        return null;
    }

    public String A0B() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("token");
        }
        return null;
    }

    public String A0C() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("transactionPrefix");
        }
        return null;
    }

    public String A0D() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("updatedSenderVpaId");
        }
        return null;
    }

    public String A0E() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("updatedVpaFor");
        }
        return null;
    }

    public String A0F() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("usableBalance");
        }
        return null;
    }

    public String A0G() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("vpa");
        }
        return null;
    }

    public String A0H() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("vpaId");
        }
        return null;
    }

    public String A0I() {
        Bundle bundle = this.A00;
        if (bundle != null) {
            return bundle.getString("vpaName");
        }
        return null;
    }

    public boolean A0J() {
        Bundle bundle = this.A00;
        return bundle != null && "1".equals(bundle.getString("merchant"));
    }

    public boolean A0K() {
        Bundle bundle = this.A00;
        return bundle != null && "1".equals(bundle.getString("valid"));
    }

    public boolean A0L() {
        Bundle bundle = this.A00;
        return bundle != null && "1".equals(bundle.getString("verifiedMerchant"));
    }

    public boolean A0M() {
        Bundle bundle = this.A00;
        return bundle != null && "1".equals(bundle.getString("blocked"));
    }

    public boolean A0N() {
        Bundle bundle = this.A00;
        return bundle != null && "1".equals(bundle.getString("vpaValid"));
    }

    @Override // java.lang.Object
    public String toString() {
        ArrayList A0l = C12960it.A0l();
        Iterator<String> it = this.A00.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            boolean contains = A01.contains(A0x);
            StringBuilder A0h = C12960it.A0h();
            if (!contains) {
                A0h.append(A0x);
                A0h.append("=");
                A0h.append(this.A00.get(A0x));
            } else {
                A0h.append(A0x);
                A0h.append("=SCRUBBED");
            }
            A0l.add(A0h.toString());
        }
        StringBuilder A0k = C12960it.A0k(" [ bundle: {");
        A0k.append(TextUtils.join(", ", A0l));
        return C12960it.A0d("}]", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.A00);
    }
}
