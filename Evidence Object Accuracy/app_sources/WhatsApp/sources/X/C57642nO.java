package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57642nO extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57642nO A0B;
    public static volatile AnonymousClass255 A0C;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public String A07;
    public String A08;
    public String A09 = "";
    public boolean A0A;

    static {
        C57642nO r0 = new C57642nO();
        A0B = r0;
        r0.A0W();
    }

    public C57642nO() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A05 = r0;
        this.A06 = r0;
        this.A08 = "";
        this.A07 = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A0B;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57642nO r6 = (C57642nO) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A09;
                int i2 = r6.A00;
                this.A09 = r7.Afy(str, r6.A09, A1R, C12960it.A1R(i2));
                this.A05 = r7.Afm(this.A05, r6.A05, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A06 = r7.Afm(this.A06, r6.A06, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r6.A00 & 4, 4));
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 8, 8);
                String str2 = this.A08;
                int i4 = r6.A00;
                this.A08 = r7.Afy(str2, r6.A08, A1V, C12960it.A1V(i4 & 8, 8));
                this.A02 = r7.Afp(this.A02, r6.A02, C12960it.A1V(i3 & 16, 16), C12960it.A1V(i4 & 16, 16));
                this.A03 = r7.Afp(this.A03, r6.A03, C12960it.A1V(i3 & 32, 32), C12960it.A1V(i4 & 32, 32));
                this.A07 = r7.Afy(this.A07, r6.A07, C12960it.A1V(i3 & 64, 64), C12960it.A1V(i4 & 64, 64));
                this.A04 = r7.Afs(this.A04, r6.A04, C12960it.A1V(i3 & 128, 128), C12960it.A1V(i4 & 128, 128));
                this.A0A = r7.Afl(C12960it.A1V(i3 & 256, 256), this.A0A, C12960it.A1V(i4 & 256, 256), r6.A0A);
                this.A01 = r7.Afp(this.A01, r6.A01, C12960it.A1V(i3 & 512, 512), C12960it.A1V(i4 & 512, 512));
                if (r7 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r72.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A09 = A0A;
                                    break;
                                case 18:
                                    this.A00 |= 2;
                                    this.A05 = r72.A08();
                                    break;
                                case 26:
                                    this.A00 |= 4;
                                    this.A06 = r72.A08();
                                    break;
                                case 34:
                                    String A0A2 = r72.A0A();
                                    this.A00 |= 8;
                                    this.A08 = A0A2;
                                    break;
                                case 40:
                                    this.A00 |= 16;
                                    this.A02 = r72.A02();
                                    break;
                                case 48:
                                    this.A00 |= 32;
                                    this.A03 = r72.A02();
                                    break;
                                case 58:
                                    String A0A3 = r72.A0A();
                                    this.A00 |= 64;
                                    this.A07 = A0A3;
                                    break;
                                case 64:
                                    this.A00 |= 128;
                                    this.A04 = r72.A06();
                                    break;
                                case C43951xu.A02:
                                    this.A00 |= 256;
                                    this.A0A = r72.A0F();
                                    break;
                                case 80:
                                    this.A00 |= 512;
                                    this.A01 = r72.A02();
                                    break;
                                default:
                                    if (A0a(r72, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57642nO();
            case 5:
                return new C82473vg();
            case 6:
                break;
            case 7:
                if (A0C == null) {
                    synchronized (C57642nO.class) {
                        if (A0C == null) {
                            A0C = AbstractC27091Fz.A09(A0B);
                        }
                    }
                }
                return A0C;
            default:
                throw C12970iu.A0z();
        }
        return A0B;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A09, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A05, 2, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A06, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A08, i2);
        }
        int i4 = this.A00;
        if ((i4 & 16) == 16) {
            i2 = AbstractC27091Fz.A02(5, this.A02, i2);
        }
        if ((i4 & 32) == 32) {
            i2 = AbstractC27091Fz.A02(6, this.A03, i2);
        }
        if ((i4 & 64) == 64) {
            i2 = AbstractC27091Fz.A04(7, this.A07, i2);
        }
        int i5 = this.A00;
        if ((i5 & 128) == 128) {
            i2 += CodedOutputStream.A06(8, this.A04);
        }
        if ((i5 & 256) == 256) {
            i2 += CodedOutputStream.A00(9);
        }
        if ((i5 & 512) == 512) {
            i2 = AbstractC27091Fz.A02(10, this.A01, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A09);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A05, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A06, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A08);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A02);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A03);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A07);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0H(8, this.A04);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0J(9, this.A0A);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0F(10, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
