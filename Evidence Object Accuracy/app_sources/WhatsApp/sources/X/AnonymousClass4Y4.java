package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.4Y4  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4Y4 {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public AbstractC14070ko A06;
    public AnonymousClass5X6 A07;
    public AbstractC116585Wa A08;
    public C89944Lz A09 = new C89944Lz();
    public boolean A0A;
    public boolean A0B;
    public final C92314Vk A0C = new C92314Vk();

    public void A00(boolean z) {
        int i;
        if (z) {
            this.A09 = new C89944Lz();
            this.A04 = 0;
            i = 0;
        } else {
            i = 1;
        }
        this.A01 = i;
        this.A05 = -1;
        this.A02 = 0;
    }

    public boolean A01(C89944Lz r7, C95304dT r8, long j) {
        if (!(this instanceof C76913mR)) {
            C76893mP r3 = (C76893mP) this;
            byte[] bArr = r8.A02;
            C95424dg r4 = r3.A00;
            if (r4 == null) {
                C95424dg r2 = new C95424dg(bArr, 17);
                r3.A00 = r2;
                r7.A00 = r2.A03(null, Arrays.copyOfRange(bArr, 9, r8.A00));
                return true;
            }
            byte b = bArr[0];
            if ((b & Byte.MAX_VALUE) == 3) {
                C89884Lt A00 = AnonymousClass4DF.A00(r8);
                C95424dg A04 = r4.A04(A00);
                r3.A00 = A04;
                r3.A01 = new C107084wl(A00, A04);
                return true;
            } else if (b != -1) {
                return true;
            } else {
                C107084wl r0 = r3.A01;
                if (r0 == null) {
                    return false;
                }
                r0.A00 = j;
                r7.A01 = r0;
                return false;
            }
        } else {
            C76913mR r5 = (C76913mR) this;
            boolean z = true;
            if (!r5.A00) {
                byte[] copyOf = Arrays.copyOf(r8.A02, r8.A00);
                List A002 = AnonymousClass4DE.A00(copyOf);
                C93844ap A003 = C93844ap.A00();
                A003.A0R = "audio/opus";
                A003.A04 = copyOf[9] & 255;
                A003.A0D = 48000;
                A003.A0S = A002;
                r7.A00 = new C100614mC(A003);
                r5.A00 = true;
                return true;
            }
            if (r8.A07() != 1332770163) {
                z = false;
            }
            r8.A0S(0);
            return z;
        }
    }
}
