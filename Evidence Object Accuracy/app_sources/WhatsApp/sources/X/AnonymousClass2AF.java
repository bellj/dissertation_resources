package X;

import java.util.concurrent.LinkedBlockingDeque;

/* renamed from: X.2AF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AF extends LinkedBlockingDeque<Runnable> {
    @Override // java.util.concurrent.LinkedBlockingDeque, java.util.concurrent.BlockingDeque, java.util.concurrent.BlockingQueue, java.util.Queue, java.util.Deque
    public /* bridge */ /* synthetic */ boolean offer(Object obj) {
        return isEmpty() && super.offer(obj);
    }
}
