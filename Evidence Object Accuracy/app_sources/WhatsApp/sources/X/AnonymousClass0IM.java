package X;

import android.content.Context;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;

/* renamed from: X.0IM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IM extends AnonymousClass0eL {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass0IU A01;

    public AnonymousClass0IM(Context context, AnonymousClass0IU r2) {
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        String A00;
        String str;
        File cacheDir = this.A00.getCacheDir();
        long freeSpace = cacheDir.getFreeSpace();
        if (freeSpace >= 30) {
            int i = 2097152;
            if (freeSpace >= 100) {
                i = 5242880;
            }
            File file = new File(cacheDir, ".facebook_cache");
            long j = (long) i;
            try {
                if (j > 0) {
                    File file2 = new File(file, "journal.bkp");
                    if (file2.exists()) {
                        File file3 = new File(file, "journal");
                        if (file3.exists()) {
                            file2.delete();
                        } else if (!file2.renameTo(file3)) {
                            throw new IOException();
                        }
                    }
                    C08860by r7 = new C08860by(file, j);
                    File file4 = r7.A08;
                    if (file4.exists()) {
                        try {
                            FileInputStream fileInputStream = new FileInputStream(file4);
                            Charset charset = C08860by.A0F;
                            C08830bv r17 = new C08830bv(r7, fileInputStream, charset);
                            try {
                                String A002 = r17.A00();
                                String A003 = r17.A00();
                                String A004 = r17.A00();
                                String A005 = r17.A00();
                                String A006 = r17.A00();
                                if (!"libcore.io.DiskLruCache".equals(A002) || !"1".equals(A003) || !Integer.toString(2).equals(A004) || !Integer.toString(1).equals(A005) || !"".equals(A006)) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("unexpected journal header: [");
                                    sb.append(A002);
                                    sb.append(", ");
                                    sb.append(A003);
                                    sb.append(", ");
                                    sb.append(A005);
                                    sb.append(", ");
                                    sb.append(A006);
                                    sb.append("]");
                                    throw new IOException(sb.toString());
                                }
                                int i2 = 0;
                                while (true) {
                                    try {
                                        A00 = r17.A00();
                                        int indexOf = A00.indexOf(32);
                                        if (indexOf != -1) {
                                            int i3 = indexOf + 1;
                                            int indexOf2 = A00.indexOf(32, i3);
                                            if (indexOf2 == -1) {
                                                str = A00.substring(i3);
                                                if (indexOf == 6 && A00.startsWith("REMOVE")) {
                                                    r7.A0B.remove(str);
                                                    i2++;
                                                }
                                            } else {
                                                str = A00.substring(i3, indexOf2);
                                            }
                                            LinkedHashMap linkedHashMap = r7.A0B;
                                            AnonymousClass0PP r11 = (AnonymousClass0PP) linkedHashMap.get(str);
                                            if (r11 == null) {
                                                r11 = new AnonymousClass0PP(r7, str);
                                                linkedHashMap.put(str, r11);
                                            }
                                            if (indexOf2 != -1) {
                                                if (indexOf == 5 && A00.startsWith("CLEAN")) {
                                                    String[] split = A00.substring(indexOf2 + 1).split(" ");
                                                    r11.A02 = true;
                                                    r11.A01 = null;
                                                    int length = split.length;
                                                    if (length == r11.A05.A06) {
                                                        for (int i4 = 0; i4 < length; i4++) {
                                                            try {
                                                                r11.A04[i4] = Long.parseLong(split[i4]);
                                                            } catch (NumberFormatException unused) {
                                                                StringBuilder sb2 = new StringBuilder("unexpected journal line: ");
                                                                sb2.append(Arrays.toString(split));
                                                                throw new IOException(sb2.toString());
                                                            }
                                                        }
                                                        continue;
                                                        i2++;
                                                    } else {
                                                        StringBuilder sb3 = new StringBuilder("unexpected journal line: ");
                                                        sb3.append(Arrays.toString(split));
                                                        throw new IOException(sb3.toString());
                                                    }
                                                } else if (indexOf2 != -1) {
                                                    if (indexOf2 != -1) {
                                                        break;
                                                    }
                                                    if (indexOf == 4 || !A00.startsWith("READ")) {
                                                        break;
                                                    }
                                                    i2++;
                                                }
                                            }
                                            if (indexOf == 5) {
                                                if (!A00.startsWith("DIRTY")) {
                                                    break;
                                                }
                                                r11.A01 = new C05120Oi(r11, r7);
                                                i2++;
                                            }
                                            if (indexOf == 4) {
                                                break;
                                                break;
                                            }
                                            i2++;
                                        } else {
                                            StringBuilder sb4 = new StringBuilder();
                                            sb4.append("unexpected journal line: ");
                                            sb4.append(A00);
                                            throw new IOException(sb4.toString());
                                        }
                                    } catch (EOFException unused2) {
                                        LinkedHashMap linkedHashMap2 = r7.A0B;
                                        r7.A00 = i2 - linkedHashMap2.size();
                                        C08860by.A01(r17);
                                        C08860by.A03(r7.A0A);
                                        Iterator it = linkedHashMap2.values().iterator();
                                        while (it.hasNext()) {
                                            AnonymousClass0PP r112 = (AnonymousClass0PP) it.next();
                                            if (r112.A01 == null) {
                                                r7.A03 += r112.A04[0];
                                            } else {
                                                r112.A01 = null;
                                                C08860by.A03(r112.A00(0));
                                                C08860by.A03(r112.A01(0));
                                                it.remove();
                                            }
                                        }
                                        r7.A04 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file4, true), charset));
                                    }
                                }
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("unexpected journal line: ");
                                sb5.append(A00);
                                throw new IOException(sb5.toString());
                            } catch (Throwable th) {
                                C08860by.A01(r17);
                                throw th;
                            }
                        } catch (IOException e) {
                            PrintStream printStream = System.out;
                            StringBuilder sb6 = new StringBuilder("DiskLruCache ");
                            sb6.append(file);
                            sb6.append(" is corrupt: ");
                            sb6.append(e.getMessage());
                            sb6.append(", removing");
                            printStream.println(sb6.toString());
                            r7.close();
                            C08860by.A02(r7.A07);
                        }
                    }
                    file.mkdirs();
                    r7 = new C08860by(file, j);
                    r7.A06();
                    AnonymousClass0IU.A05 = r7;
                    Thread thread = new Thread(new RunnableC08980cA());
                    AnonymousClass0IU.A02 = thread;
                    thread.start();
                    return;
                }
                throw new IllegalArgumentException("maxSize <= 0");
            } catch (IOException unused3) {
                C06160Sk.A08.A00();
            }
        }
    }
}
