package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.3ZU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZU implements AnonymousClass5WD {
    public final /* synthetic */ AnonymousClass024 A00;
    public final /* synthetic */ AnonymousClass4KF A01;
    public final /* synthetic */ AnonymousClass1CS A02;
    public final /* synthetic */ C15580nU A03;

    public AnonymousClass3ZU(AnonymousClass024 r1, AnonymousClass4KF r2, AnonymousClass1CS r3, C15580nU r4) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
    }

    @Override // X.AnonymousClass5WD
    public void APl(int i) {
        AnonymousClass4KF r0 = this.A01;
        C15580nU r4 = this.A03;
        C468027s r2 = r0.A00;
        C63373Bi r3 = new C63373Bi(r4, null, null, null, null, null, 0, 2, 0);
        AnonymousClass016 r1 = r2.A0R;
        if (i != -1) {
            r1.A0A(new C92634Ws(r3, i));
            r2.A05(5);
            return;
        }
        throw C12960it.A0U("Error code expected but default success code '-1' was provided.");
    }

    @Override // X.AnonymousClass5WD
    public void AWt(C15580nU r13, UserJid userJid, AnonymousClass1PD r15, String str, Map map, int i, int i2, long j, long j2) {
        this.A00.accept(new C63373Bi(r13, userJid, r15, str, null, this.A02.A01.A0B(map), i, i2, j));
    }
}
