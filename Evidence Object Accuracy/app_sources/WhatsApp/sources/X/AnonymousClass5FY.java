package X;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;

/* renamed from: X.5FY  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5FY implements AbstractC117195Yx {
    public final InputStream A00;
    public final C92014Uc A01;

    public AnonymousClass5FY(InputStream inputStream, C92014Uc r2) {
        this.A00 = inputStream;
        this.A01 = r2;
    }

    @Override // X.AbstractC117195Yx
    public long AZp(C10730f6 r6, long j) {
        String message;
        if (j == 0) {
            return 0;
        }
        if (j >= 0) {
            try {
                if (!Thread.interrupted()) {
                    C94484bt A09 = r6.A09(1);
                    int i = A09.A00;
                    int read = this.A00.read(A09.A06, i, (int) Math.min(j, (long) (8192 - i)));
                    if (read == -1) {
                        return -1;
                    }
                    A09.A00 += read;
                    long j2 = (long) read;
                    r6.A00 += j2;
                    return j2;
                }
                Thread.currentThread().interrupt();
                throw new InterruptedIOException("interrupted");
            } catch (AssertionError e) {
                if (e.getCause() == null || (message = e.getMessage()) == null || !AnonymousClass03B.A0G(message, "getsockname failed")) {
                    throw e;
                }
                throw new IOException(e);
            }
        } else {
            throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("byteCount < 0: "), j));
        }
    }

    @Override // X.AbstractC117195Yx, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        this.A00.close();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("source(");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
