package X;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;

/* renamed from: X.0XM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XM implements AbstractC12280hf, DialogInterface.OnDismissListener, DialogInterface.OnClickListener, DialogInterface.OnKeyListener {
    public AnonymousClass04S A00;
    public AnonymousClass0XO A01;
    public AnonymousClass07H A02;

    public AnonymousClass0XM(AnonymousClass07H r1) {
        this.A02 = r1;
    }

    @Override // X.AbstractC12280hf
    public void AOF(AnonymousClass07H r2, boolean z) {
        AnonymousClass04S r0;
        if ((z || r2 == this.A02) && (r0 = this.A00) != null) {
            r0.dismiss();
        }
    }

    @Override // X.AbstractC12280hf
    public boolean ATF(AnonymousClass07H r2) {
        return false;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        AnonymousClass07H r3 = this.A02;
        AnonymousClass0XO r1 = this.A01;
        AnonymousClass0BP r0 = r1.A04;
        if (r0 == null) {
            r0 = new AnonymousClass0BP(r1);
            r1.A04 = r0;
        }
        r3.A0K((C07340Xp) r0.getItem(i), null, 0);
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        this.A01.AOF(this.A02, true);
    }

    @Override // android.content.DialogInterface.OnKeyListener
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.A00.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.A00.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.A02.A0F(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.A02.performShortcut(i, keyEvent, 0);
    }
}
