package X;

import android.os.Build;

/* renamed from: X.1Eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26641Eg {
    public final String A00 = Build.DEVICE;
    public final String A01 = Build.BOARD;
    public final String A02 = Build.DISPLAY;
    public final String A03 = Build.MANUFACTURER;
    public final String A04 = Build.VERSION.RELEASE;
}
