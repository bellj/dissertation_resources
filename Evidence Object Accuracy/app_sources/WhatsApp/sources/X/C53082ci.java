package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import com.whatsapp.R;

/* renamed from: X.2ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53082ci extends LinearLayout {
    public AnonymousClass4AT A00;
    public final ImageView A01;
    public final ImageView A02;
    public final Space A03;

    public C53082ci(Context context) {
        super(context, null, 0);
        View inflate = LinearLayout.inflate(getContext(), R.layout.action_button_container, this);
        this.A02 = C12970iu.A0K(inflate, R.id.rate_button);
        this.A01 = C12970iu.A0K(inflate, R.id.action_button);
        this.A03 = (Space) AnonymousClass028.A0D(inflate, R.id.space);
    }

    public final void A00() {
        Space space;
        int i;
        if (this.A02.getVisibility() == 0 && this.A01.getVisibility() == 0) {
            space = this.A03;
            i = 0;
        } else {
            space = this.A03;
            i = 8;
        }
        space.setVisibility(i);
    }

    public ImageView getActionButton() {
        return this.A01;
    }

    public ImageView getRateButton() {
        return this.A02;
    }

    public void setupActionButton(AnonymousClass4AT r6, View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener) {
        int i;
        Drawable drawable;
        int i2;
        this.A00 = r6;
        ImageView imageView = this.A01;
        imageView.setVisibility(0);
        imageView.setOnClickListener(onClickListener);
        imageView.setOnLongClickListener(onLongClickListener);
        switch (r6.ordinal()) {
            case 0:
                i = R.string.accessibility_content_description_message_information;
                drawable = AnonymousClass2GE.A01(getContext(), R.drawable.ic_offline_info, R.color.quickActionIconTint);
                i2 = R.dimen.more_info_button_padding;
                C12960it.A0r(getContext(), imageView, i);
                imageView.setImageDrawable(drawable);
                int dimensionPixelSize = C12960it.A09(this).getDimensionPixelSize(i2);
                imageView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                break;
            case 1:
                i = R.string.action_search_web;
                drawable = AnonymousClass2GE.A01(getContext(), R.drawable.ic_search_normal, R.color.white);
                i2 = R.dimen.search_button_padding;
                C12960it.A0r(getContext(), imageView, i);
                imageView.setImageDrawable(drawable);
                int dimensionPixelSize = C12960it.A09(this).getDimensionPixelSize(i2);
                imageView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                break;
            case 2:
                i = R.string.forward_to;
                drawable = C12970iu.A0C(getContext(), R.drawable.ic_action_forward);
                i2 = R.dimen.forward_button_padding;
                C12960it.A0r(getContext(), imageView, i);
                imageView.setImageDrawable(drawable);
                int dimensionPixelSize = C12960it.A09(this).getDimensionPixelSize(i2);
                imageView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                break;
            case 3:
                i = R.string.accessibility_content_description_outgoing_avatar_sticker;
                drawable = AnonymousClass2GE.A01(getContext(), R.drawable.ic_action_avatar, R.color.quickActionIconTint);
                i2 = R.dimen.avocado_button_padding;
                C12960it.A0r(getContext(), imageView, i);
                imageView.setImageDrawable(drawable);
                int dimensionPixelSize = C12960it.A09(this).getDimensionPixelSize(i2);
                imageView.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                break;
            default:
                imageView.setVisibility(8);
                break;
        }
        A00();
    }

    public void setupRateButton(View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener) {
        ImageView imageView = this.A02;
        imageView.setOnClickListener(onClickListener);
        imageView.setOnLongClickListener(onLongClickListener);
        imageView.setVisibility(0);
        A00();
    }
}
