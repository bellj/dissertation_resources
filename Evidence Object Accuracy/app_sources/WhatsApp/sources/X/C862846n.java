package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.46n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C862846n extends C75563k3 {
    public final ImageView A00;

    public C862846n(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.media_thumbnail);
    }
}
