package X;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.10K  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass10K implements AnonymousClass10L, AnonymousClass10M, AbstractC20260vT {
    public int A00;
    public int A01 = 0;
    public int A02;
    public long A03;
    public long A04;
    public BroadcastReceiver A05;
    public BroadcastReceiver A06;
    public BroadcastReceiver A07;
    public BroadcastReceiver A08;
    public String A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public final Handler A0D = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A0E;
    public final AnonymousClass10I A0F;
    public final C22730zY A0G;
    public final AnonymousClass10J A0H;
    public final C18640sm A0I;
    public final C16590pI A0J;
    public final C18360sK A0K;
    public final C14820m6 A0L;
    public final AnonymousClass018 A0M;
    public final C14850m9 A0N;
    public final AbstractC14440lR A0O;
    public final C21710xr A0P;
    public final AtomicReference A0Q = new AtomicReference(10);
    public volatile Notification A0R;

    @Override // X.AnonymousClass10L
    public void ALq(boolean z) {
    }

    @Override // X.AnonymousClass10M
    public void AMu() {
    }

    @Override // X.AnonymousClass10M
    public void AMv() {
    }

    @Override // X.AnonymousClass10M
    public void AMx() {
    }

    @Override // X.AnonymousClass10L
    public void APg() {
    }

    @Override // X.AnonymousClass10L
    public void ASl(boolean z) {
    }

    @Override // X.AnonymousClass10L
    public void ASm(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void ASn() {
    }

    @Override // X.AnonymousClass10L
    public void AY3() {
    }

    public AnonymousClass10K(AbstractC15710nm r3, AnonymousClass10I r4, C22730zY r5, AnonymousClass10J r6, C18640sm r7, C16590pI r8, C18360sK r9, C14820m6 r10, AnonymousClass018 r11, C14850m9 r12, AbstractC14440lR r13, C21710xr r14) {
        this.A0J = r8;
        this.A0N = r12;
        this.A0E = r3;
        this.A0O = r13;
        this.A0M = r11;
        this.A0P = r14;
        this.A0L = r10;
        this.A0K = r9;
        this.A0F = r4;
        this.A0I = r7;
        this.A0G = r5;
        this.A0H = r6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
        if (r0 == false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r1 != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.Notification A00(android.content.res.Resources r5, java.lang.String r6) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof X.AnonymousClass046
            if (r0 == 0) goto L_0x0008
            X.046 r5 = (X.AnonymousClass046) r5
            android.content.res.Resources r5 = r5.A00
        L_0x0008:
            X.02s r2 = r4.A02()
            java.lang.String r0 = "action_restore"
            boolean r3 = r0.equals(r6)
            if (r3 != 0) goto L_0x001f
            java.lang.String r0 = "action_restore_media"
            boolean r1 = r0.equals(r6)
            r0 = 2131888422(0x7f120926, float:1.9411479E38)
            if (r1 == 0) goto L_0x0022
        L_0x001f:
            r0 = 2131888453(0x7f120945, float:1.9411542E38)
        L_0x0022:
            java.lang.String r0 = r5.getString(r0)
            r2.A0A(r0)
            java.lang.String r0 = "action_backup"
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x004e
            if (r3 != 0) goto L_0x005d
            java.lang.String r0 = "action_restore_media"
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = "action_change_number"
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x004e
            java.lang.String r0 = "action_delete"
            boolean r0 = r0.equals(r6)
            r1 = 2131887651(0x7f120623, float:1.9409915E38)
            if (r0 != 0) goto L_0x0051
        L_0x004e:
            r1 = 2131888401(0x7f120911, float:1.9411436E38)
        L_0x0051:
            java.lang.String r0 = r5.getString(r1)
            r2.A09(r0)
            android.app.Notification r0 = r2.A01()
            return r0
        L_0x005d:
            r1 = 2131888447(0x7f12093f, float:1.941153E38)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10K.A00(android.content.res.Resources, java.lang.String):android.app.Notification");
    }

    public final PendingIntent A01(String str) {
        Intent intent = new Intent(str);
        intent.setPackage("com.whatsapp");
        return AnonymousClass1UY.A01(this.A0J.A00, 0, intent, 0);
    }

    public final C005602s A02() {
        Context context = this.A0J.A00;
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "chat_history_backup@1";
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.backup.google.SettingsGoogleDrive");
        A00.A09 = AnonymousClass1UY.A00(context, 0, intent, 0);
        C18360sK.A01(A00, R.drawable.notifybar);
        if (Build.VERSION.SDK_INT >= 21) {
            A00.A06 = 1;
        }
        return A00;
    }

    public void A03() {
        this.A0R = null;
        C18360sK r2 = this.A0K;
        r2.A04(5, null);
        r2.A04(46, null);
    }

    public synchronized void A04() {
        int i = this.A01 + 1;
        this.A01 = i;
        if (i <= 1) {
            Log.i("gdrive-notification-manager/register");
            this.A0Q.set(10);
            this.A0C = false;
            this.A0B = false;
            this.A0A = false;
            this.A00 = 0;
            this.A02 = 0;
            this.A03 = 0;
            this.A04 = 0;
            this.A09 = null;
            if (this.A0R != null) {
                AbstractC15710nm r3 = this.A0E;
                StringBuilder sb = new StringBuilder();
                sb.append("numOfClientsRegistered=");
                sb.append(this.A01);
                r3.AaV("gdrive-notification-manager/register called with non-null last notification", sb.toString(), false);
            }
            this.A0R = null;
            this.A0D.post(new RunnableBRunnable0Shape2S0100000_I0_2(this, 28));
            this.A0H.A02(this);
            AnonymousClass10I r1 = this.A0F;
            r1.A00.A03(this);
            C22730zY r12 = r1.A01;
            if (r12.A0Z.get() || !r12.A0a.get()) {
                AMv();
            }
        }
    }

    public synchronized void A05() {
        Notification notification = this.A0R;
        Integer num = (Integer) this.A0Q.get();
        if (!(notification == null || num == null)) {
            A03();
            int intValue = num.intValue();
            if ((intValue == 15 || intValue == 27) && A07(this.A0C)) {
                Log.i("gdrive-notification-manager/re-posting important notification");
                this.A0K.A03(46, notification);
            }
        }
        int i = this.A01 - 1;
        this.A01 = i;
        if (i <= 0) {
            Log.i("gdrive-notification-manager/unregister");
            this.A0D.post(new RunnableBRunnable0Shape2S0100000_I0_2(this, 29));
            this.A0H.A01.A04(this);
            this.A0F.A00.A04(this);
            BroadcastReceiver broadcastReceiver = this.A05;
            if (broadcastReceiver != null) {
                try {
                    this.A0J.A00.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
            }
            BroadcastReceiver broadcastReceiver2 = this.A07;
            if (broadcastReceiver2 != null) {
                try {
                    this.A0J.A00.unregisterReceiver(broadcastReceiver2);
                } catch (IllegalArgumentException unused2) {
                }
            }
            BroadcastReceiver broadcastReceiver3 = this.A06;
            if (broadcastReceiver3 != null) {
                try {
                    this.A0J.A00.unregisterReceiver(broadcastReceiver3);
                } catch (IllegalArgumentException unused3) {
                }
            }
            BroadcastReceiver broadcastReceiver4 = this.A08;
            if (broadcastReceiver4 != null) {
                try {
                    this.A0J.A00.unregisterReceiver(broadcastReceiver4);
                } catch (IllegalArgumentException unused4) {
                }
            }
        }
    }

    public final void A06(AnonymousClass03l r6, String str, String str2, int i, int i2, boolean z, boolean z2) {
        this.A09 = str2;
        if (this.A0A) {
            A03();
        }
        C005602s A02 = A02();
        boolean z3 = true;
        if (i == 1) {
            A02.A03(0, 0, false);
        } else if (i == 2) {
            A02.A03(100, i2, true);
        } else if (i == 3) {
            A02.A03(100, i2, false);
        } else {
            StringBuilder sb = new StringBuilder("unexpected value for progress bar style");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        A02.A0D(z2);
        A02.A0E(z);
        A02.A0A(str);
        A02.A09(str2);
        this.A0B = z;
        if (r6 == null) {
            z3 = false;
        }
        this.A0A = z3;
        if (r6 != null) {
            A02.A0N.add(r6);
        }
        Notification A01 = A02.A01();
        this.A0R = A01;
        this.A0K.A03(5, A01);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A07(boolean r11) {
        /*
            r10 = this;
            r7 = 1
            if (r11 == 0) goto L_0x0009
            java.lang.String r0 = "gdrive-notification-manager/backup-error/backup-user-initiated/true"
            com.whatsapp.util.Log.i(r0)
            return r7
        L_0x0009:
            X.0m6 r3 = r10.A0L
            int r6 = r3.A01()
            r8 = 5184000000(0x134fd9000, double:2.561236308E-314)
            r4 = 2
            if (r6 == 0) goto L_0x008c
            if (r6 == r7) goto L_0x0087
            if (r6 == r4) goto L_0x0083
            r0 = 3
            if (r6 == r0) goto L_0x0032
            r0 = 4
            if (r6 == r0) goto L_0x008c
            java.lang.String r1 = "gdrive-notification-manager/backup-error/unexpected-frequency/"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x0032:
            android.content.SharedPreferences r2 = r3.A00
            java.lang.String r1 = "gdrive_successive_backup_failed_count"
            r0 = 0
            int r5 = r2.getInt(r1, r0)
            int r0 = r5 + 1
            int r0 = r0 % r4
            r2 = 1
            if (r0 == 0) goto L_0x0057
            r2 = 0
            java.lang.String r0 = r3.A09()
            if (r0 == 0) goto L_0x007d
            long r3 = r3.A07(r0)
            long r1 = java.lang.System.currentTimeMillis()
            long r1 = r1 - r3
            int r0 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r0 > 0) goto L_0x0056
            r7 = 0
        L_0x0056:
            r2 = r7
        L_0x0057:
            java.lang.String r0 = "gdrive-notification-manager/backup-error/frequency="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = X.C44771zW.A05(r6)
            r1.append(r0)
            java.lang.String r0 = "/success-backup-fail-count="
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = "/show-notification="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            return r2
        L_0x007d:
            java.lang.String r0 = "gdrive-notification-manager/backup-error/google-account-is-null/unexpected"
            com.whatsapp.util.Log.e(r0)
            goto L_0x0057
        L_0x0083:
            r8 = 1209600000(0x48190800, double:5.97621805E-315)
            goto L_0x0032
        L_0x0087:
            r4 = 5
            r8 = 432000000(0x19bfcc00, double:2.13436359E-315)
            goto L_0x0032
        L_0x008c:
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            r4 = 1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10K.A07(boolean):boolean");
    }

    @Override // X.AnonymousClass10L
    public void AMs() {
        A03();
    }

    @Override // X.AnonymousClass10L
    public synchronized void AMt(boolean z) {
        AnonymousClass03l r6;
        AtomicReference atomicReference = this.A0Q;
        int intValue = ((Integer) atomicReference.get()).intValue();
        if (intValue != 23) {
            if (z || intValue != 15 || !A07(this.A0C)) {
                atomicReference.set(23);
            }
            Log.i("gdrive-notification-manager/backup-end");
            this.A00 = 0;
            Context context = this.A0J.A00;
            int i = R.string.gdrive_backup_notification_title_finished_failed;
            if (z) {
                i = R.string.gdrive_backup_notification_title_finished_success;
            }
            String string = context.getString(i);
            String string2 = context.getString(R.string.gdrive_backup_notification_string_finished);
            if (C44771zW.A0M(this.A0N)) {
                context.registerReceiver(new C44861zg(this), new IntentFilter("clear_backup_worker"));
                r6 = new AnonymousClass03l(R.drawable.ic_action_cancel, context.getString(R.string.google_drive_notification_dismiss), A01("clear_backup_worker"));
            } else {
                r6 = null;
            }
            A06(r6, string, string2, 1, -1, false, true);
        }
    }

    @Override // X.AnonymousClass10M
    public synchronized void AMw(long j, long j2) {
    }

    @Override // X.AnonymousClass10M
    public synchronized void AMy(boolean z) {
    }

    @Override // X.AnonymousClass10L
    public void AMz(long j, long j2) {
        int i;
        Log.i("gdrive-notification-manager/backup-paused-for-data-connection");
        if (((Number) this.A0Q.getAndSet(17)).intValue() != 17) {
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_backup_title_paused), context.getString(R.string.gdrive_media_restore_notification_string_paused_for_data_connection), 3, i, false, false);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN0(long j, long j2) {
        int i;
        Log.i("gdrive-notification-manager/backup-paused-for-low-battery");
        if (((Number) this.A0Q.getAndSet(18)).intValue() != 18) {
            if (this.A06 == null) {
                C44871zh r2 = new C44871zh(this);
                this.A06 = r2;
                this.A0J.A00.registerReceiver(r2, new IntentFilter("enable_backup_over_low_battery"));
            }
            Context context = this.A0J.A00;
            AnonymousClass03l r6 = new AnonymousClass03l(R.drawable.ic_action_refresh, context.getString(R.string.gdrive_media_restore_notification_resume_now), A01("enable_backup_over_low_battery"));
            if (j2 > 0) {
                i = (int) ((100 * j) / j2);
            } else {
                i = -1;
            }
            A06(r6, context.getString(R.string.gdrive_backup_title_paused), context.getString(R.string.gdrive_media_restore_notification_string_paused_for_battery), 3, i, false, false);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN1(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(20)).intValue() != 20) {
            Log.i("gdrive-notification-manager/backup-paused-for-sdcard-missing");
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_backup_title_paused), context.getString(R.string.msg_store_backup_skipped_due_to_missing_sdcard_title), 3, i, false, false);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN2(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(19)).intValue() != 19) {
            Log.i("gdrive-notification-manager/backup-paused-for-sdcard-unmounted");
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_backup_title_paused), context.getString(R.string.msg_store_backup_skipped_due_to_unmounted_sdcard_title), 3, i, false, false);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN3(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(16)).intValue() != 16) {
            Log.i("gdrive-notification-manager/backup-paused-wifi-unavailable");
            if (this.A05 == null) {
                C44881zi r2 = new C44881zi(this);
                this.A05 = r2;
                this.A0J.A00.registerReceiver(r2, new IntentFilter("enable_backup_over_cellular"));
            }
            AnonymousClass03l r5 = null;
            if (this.A0I.A05(true) == 2) {
                r5 = new AnonymousClass03l(R.drawable.ic_action_refresh, this.A0J.A00.getString(R.string.gdrive_media_restore_notification_resume_now), A01("enable_backup_over_cellular"));
            }
            if (j2 > 0) {
                i = (int) ((100 * j) / j2);
            } else {
                i = -1;
            }
            Context context = this.A0J.A00;
            A06(r5, context.getString(R.string.gdrive_backup_title_paused), context.getString(R.string.gdrive_media_restore_notification_string_paused_for_wifi), 3, i, false, false);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN4(int i) {
        AtomicReference atomicReference = this.A0Q;
        boolean z = false;
        if (((Number) atomicReference.get()).intValue() != 12) {
            z = true;
        }
        if (((Number) atomicReference.getAndSet(12)).intValue() != 12 || System.currentTimeMillis() - this.A04 >= 200) {
            this.A04 = System.currentTimeMillis();
            Context context = this.A0J.A00;
            String string = context.getString(R.string.gdrive_backup_title);
            if (i >= 0 || z) {
                A06(null, string, context.getString(R.string.gdrive_backup_notification_string_preparation_message_with_percentage_placeholder, this.A0M.A0K().format(((double) i) / 100.0d)), 2, -1, true, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void AN5() {
        if (((Number) this.A0Q.getAndSet(11)).intValue() != 11) {
            Log.i("gdrive-notification-manager/backup-prep-start");
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_backup_title), context.getString(R.string.gdrive_backup_notification_string_preparation_message), 2, -1, true, false);
        }
    }

    @Override // X.AnonymousClass10L
    public synchronized void AN6(long j, long j2) {
        int i;
        AtomicReference atomicReference = this.A0Q;
        boolean z = false;
        if (((Integer) atomicReference.get()).intValue() != 14) {
            z = true;
        }
        if (((Integer) atomicReference.getAndSet(14)).intValue() != 14 || System.currentTimeMillis() - this.A04 >= 200) {
            this.A04 = System.currentTimeMillis();
            if (j2 > 0) {
                i = (int) ((100 * j) / j2);
            } else {
                i = 0;
            }
            if (i - this.A00 > 0 || z) {
                Log.i(String.format(Locale.ENGLISH, "gdrive-notification-manager/backup-progress %d/%d (%d)", Long.valueOf(j), Long.valueOf(j2), Integer.valueOf(i)));
                this.A00 = i;
                Context context = this.A0J.A00;
                String string = context.getString(R.string.gdrive_backup_title);
                AnonymousClass018 r7 = this.A0M;
                String string2 = context.getString(R.string.settings_gdrive_backup_progress_message_with_percentage, C44891zj.A03(r7, j), C44891zj.A03(r7, j2), r7.A0K().format(((double) i) / 100.0d));
                if (!string2.equals(this.A09)) {
                    A06(null, string, string2, 3, i, true, false);
                }
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void AN7() {
        this.A0Q.getAndSet(13);
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r4) {
        this.A0O.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 30));
    }

    @Override // X.AnonymousClass10L
    public synchronized void APw(int i, Bundle bundle) {
        if (i != 10) {
            if (((Integer) this.A0Q.getAndSet(15)).intValue() != 15) {
                StringBuilder sb = new StringBuilder();
                sb.append("gdrive-notification-manager/backup-error/");
                sb.append(C44771zW.A04(i));
                Log.i(sb.toString());
                if (A07(this.A0C)) {
                    Context context = this.A0J.A00;
                    A06(null, context.getString(R.string.gdrive_backup_notification_title_finished_failed), context.getString(R.string.gdrive_backup_notification_string_finished), 1, -1, false, true);
                }
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void APx(int i, Bundle bundle) {
        if (i != 10 && ((Number) this.A0Q.getAndSet(27)).intValue() != 27) {
            StringBuilder sb = new StringBuilder("gdrive-notification-manager/media-restore-error/");
            sb.append(C44771zW.A04(i));
            Log.i(sb.toString());
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_media_restore_title_failed), context.getString(R.string.gdrive_backup_notification_string_finished), 1, -1, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public void APy(int i, Bundle bundle) {
        StringBuilder sb = new StringBuilder("gdrive-notification-manager/msgstore-restore-error/");
        sb.append(C44771zW.A04(i));
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass10L
    public void ASU() {
        A03();
    }

    @Override // X.AnonymousClass10L
    public void ASV(long j, long j2, boolean z) {
        String string;
        String str;
        if (((Number) this.A0Q.getAndSet(33)).intValue() != 33) {
            StringBuilder sb = new StringBuilder("gdrive-notification-manager/restore-end/");
            if (z) {
                str = "success";
            } else {
                str = "failed";
            }
            sb.append(str);
            sb.append(" total: ");
            sb.append(j2);
            sb.append(" failed: ");
            sb.append(j);
            Log.i(sb.toString());
        }
        if (j2 == 0) {
            A03();
            return;
        }
        Context context = this.A0J.A00;
        String string2 = context.getString(R.string.gdrive_media_restore_title_finished);
        if (j > 0) {
            AnonymousClass018 r4 = this.A0M;
            string = context.getString(R.string.gdrive_media_restore_notification_string_finished_with_failures, C44891zj.A03(r4, j2 - j), C44891zj.A03(r4, j));
        } else {
            string = context.getString(R.string.gdrive_media_restore_notification_string_finished_no_failures, C44891zj.A03(this.A0M, j2));
        }
        A06(null, string2, string, 1, -1, false, true);
    }

    @Override // X.AnonymousClass10L
    public void ASW(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(29)).intValue() != 29) {
            Log.i("gdrive-notification-manager/restore-paused-data-unavailable");
            Context context = this.A0J.A00;
            String string = context.getString(R.string.gdrive_media_restore_title_paused);
            String string2 = context.getString(R.string.gdrive_media_restore_notification_string_paused_for_data_connection);
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            A06(null, string, string2, 3, i, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public void ASX(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(30)).intValue() != 30) {
            Log.i("gdrive-notification-manager/restore-paused-for-battery");
            if (this.A08 == null) {
                C44901zk r2 = new C44901zk(this);
                this.A08 = r2;
                this.A0J.A00.registerReceiver(r2, new IntentFilter("enable_restore_over_low_battery"));
            }
            Context context = this.A0J.A00;
            AnonymousClass03l r6 = new AnonymousClass03l(R.drawable.ic_action_refresh, context.getString(R.string.gdrive_media_restore_notification_resume_now), A01("enable_restore_over_low_battery"));
            String string = context.getString(R.string.gdrive_media_restore_title_paused);
            String string2 = context.getString(R.string.gdrive_media_restore_notification_string_paused_for_battery);
            if (j2 > 0) {
                i = (int) ((100 * j) / j2);
            } else {
                i = -1;
            }
            A06(r6, string, string2, 3, i, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public void ASY(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(32)).intValue() != 32) {
            Log.i("gdrive-notification-manager/restore-paused-sdcard-missing");
            Context context = this.A0J.A00;
            String string = context.getString(R.string.gdrive_media_restore_title_paused);
            String string2 = context.getString(R.string.msg_store_backup_skipped_due_to_missing_sdcard_title);
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            A06(null, string, string2, 3, i, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public void ASZ(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(31)).intValue() != 31) {
            Log.i("gdrive-notification-manager/restore-paused-sdcard-unmounted");
            A03();
            Context context = this.A0J.A00;
            String string = context.getString(R.string.gdrive_media_restore_title_paused);
            String string2 = context.getString(R.string.msg_store_backup_skipped_due_to_unmounted_sdcard_title);
            if (j2 > 0) {
                i = (int) ((j * 100) / j2);
            } else {
                i = -1;
            }
            A06(null, string, string2, 3, i, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public void ASa(long j, long j2) {
        int i;
        if (((Number) this.A0Q.getAndSet(28)).intValue() != 28) {
            Log.i("gdrive-notification-manager/restore-paused-wifi-unavailable");
            if (this.A07 == null) {
                C44911zl r2 = new C44911zl(this);
                this.A07 = r2;
                this.A0J.A00.registerReceiver(r2, new IntentFilter("enable_restore_over_cellular"));
            }
            AnonymousClass03l r5 = null;
            if (this.A0I.A05(true) == 2) {
                r5 = new AnonymousClass03l(R.drawable.ic_action_refresh, this.A0J.A00.getString(R.string.gdrive_media_restore_notification_resume_now), A01("enable_restore_over_cellular"));
            }
            Context context = this.A0J.A00;
            String string = context.getString(R.string.gdrive_media_restore_title_paused);
            String string2 = context.getString(R.string.gdrive_media_restore_notification_string_paused_for_wifi);
            if (j2 > 0) {
                i = (int) ((100 * j) / j2);
            } else {
                i = -1;
            }
            A06(r5, string, string2, 3, i, false, true);
        }
    }

    @Override // X.AnonymousClass10L
    public synchronized void ASb(int i) {
        Context context = this.A0J.A00;
        String string = context.getString(R.string.gdrive_media_restore_title_running);
        AtomicReference atomicReference = this.A0Q;
        if (((Integer) atomicReference.get()).intValue() != 25 || System.currentTimeMillis() - this.A03 >= 200) {
            this.A03 = System.currentTimeMillis();
            boolean z = false;
            if (((Integer) atomicReference.getAndSet(25)).intValue() != 25) {
                z = true;
            }
            if (i > 0 || z) {
                A06(null, string, context.getString(R.string.gdrive_media_restore_notification_string_preparation_message_with_percentage_placeholder, this.A0M.A0K().format(((double) i) / 100.0d)), 2, i, true, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASc() {
        Context context = this.A0J.A00;
        String string = context.getString(R.string.gdrive_media_restore_title_running);
        if (((Number) this.A0Q.getAndSet(24)).intValue() != 24) {
            Log.i("gdrive-notification-manager/restore-prep-start");
        }
        A06(null, string, context.getString(R.string.gdrive_media_restore_notification_string_preparation_message), 2, -1, true, false);
    }

    @Override // X.AnonymousClass10L
    public synchronized void ASd(long j, long j2, long j3) {
        int i;
        String str;
        Context context = this.A0J.A00;
        String string = context.getString(R.string.gdrive_media_restore_title_running);
        AtomicReference atomicReference = this.A0Q;
        if (((Integer) atomicReference.get()).intValue() != 26 || System.currentTimeMillis() - this.A04 >= 200) {
            this.A04 = System.currentTimeMillis();
            if (((Integer) atomicReference.getAndSet(26)).intValue() != 26) {
                StringBuilder sb = new StringBuilder();
                sb.append("gdrive-notification-manager/restore-progress ");
                sb.append(j);
                sb.append("/");
                sb.append(j3);
                sb.append(" bytes (");
                sb.append(j2);
                sb.append(" bytes failed).");
                Log.i(sb.toString());
            }
            if (j3 > 0) {
                i = (int) ((100 * j) / j3);
            } else {
                i = -1;
            }
            this.A02 = i;
            if (j2 > 0) {
                AnonymousClass018 r7 = this.A0M;
                str = context.getString(R.string.gdrive_media_restore_notification_string_with_failures_and_percentage, C44891zj.A03(r7, j), C44891zj.A03(r7, j3), r7.A0K().format(((double) this.A02) / 100.0d));
            } else {
                AnonymousClass018 r72 = this.A0M;
                str = context.getString(R.string.gdrive_media_restore_notification_string_no_failures_with_placeholder, C44891zj.A03(r72, j), C44891zj.A03(r72, j3), r72.A0K().format(((double) this.A02) / 100.0d));
            }
            if (!str.equals(this.A09)) {
                A06(null, string, str, 3, this.A02, true, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void AVb() {
        if (((Number) this.A0Q.getAndSet(21)).intValue() != 21) {
            Log.i("gdrive-notification-manager/backup-scrub-start");
            Context context = this.A0J.A00;
            A06(null, context.getString(R.string.gdrive_backup_title), context.getString(R.string.settings_gdrive_backup_finishing_message), 2, -1, true, false);
        }
    }
}
