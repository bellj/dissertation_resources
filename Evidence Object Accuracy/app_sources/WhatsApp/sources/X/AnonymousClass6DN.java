package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.6DN  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DN implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ AnonymousClass60K A01;
    public final /* synthetic */ C126685tH A02;

    public AnonymousClass6DN(AnonymousClass3FE r1, AnonymousClass60K r2, C126685tH r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("Bloks: IQRequestHelper/sendIQRequest onDeliveryFailure");
        this.A01.A00.A0I(new Runnable() { // from class: X.6FK
            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass3FE r0 = AnonymousClass3FE.this;
                if (r0 != null) {
                    C117315Zl.A0S(r0);
                }
            }
        });
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        Log.e(C12960it.A0d(r5.toString(), C12960it.A0k("Bloks: IQRequestHelper/sendIQRequest onError: ")));
        this.A01.A00.A0I(new Runnable(this.A00, this, this.A02, r5) { // from class: X.6Jr
            public final /* synthetic */ AnonymousClass3FE A00;
            public final /* synthetic */ AnonymousClass6DN A01;
            public final /* synthetic */ C126685tH A02;
            public final /* synthetic */ AnonymousClass1V8 A03;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass6DN r6 = this.A01;
                C126685tH r4 = this.A02;
                AnonymousClass1V8 r3 = this.A03;
                AnonymousClass3FE r52 = this.A00;
                if (r4 != null) {
                    Iterator it = AbstractC451020e.A00(r3).iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (((C452120p) it.next()).A00 == 453) {
                                r4.A00.A0M(true);
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (r52 != null) {
                    Map A01 = r6.A01.A01(r3);
                    if (r52.A00) {
                        r52.A03.A00(new RunnableBRunnable0Shape1S1200000_I1(A01, r52, "on_failure", 10));
                    }
                }
            }
        });
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        this.A01.A00.A0I(new Runnable(this.A00, this, r4) { // from class: X.6J6
            public final /* synthetic */ AnonymousClass3FE A00;
            public final /* synthetic */ AnonymousClass6DN A01;
            public final /* synthetic */ AnonymousClass1V8 A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass6DN r0 = this.A01;
                AnonymousClass3FE r5 = this.A00;
                AnonymousClass1V8 r1 = this.A02;
                if (r5 != null) {
                    Map A01 = r0.A01.A01(r1);
                    if (r5.A00) {
                        r5.A03.A00(new RunnableBRunnable0Shape1S1200000_I1(A01, r5, "on_success", 10));
                    }
                }
            }
        });
    }
}
