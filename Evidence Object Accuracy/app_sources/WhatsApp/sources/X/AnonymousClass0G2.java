package X;

import java.util.AbstractCollection;

/* renamed from: X.0G2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0G2 extends AnonymousClass08s {
    public final /* synthetic */ AnonymousClass00N A00;
    public final /* synthetic */ AnonymousClass0WB A01;

    public AnonymousClass0G2(AnonymousClass00N r1, AnonymousClass0WB r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXq(AnonymousClass072 r3) {
        ((AbstractCollection) this.A00.get(this.A01.A00)).remove(r3);
    }
}
