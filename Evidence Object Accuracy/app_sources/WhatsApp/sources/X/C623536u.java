package X;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import com.whatsapp.R;
import com.whatsapp.accountsync.LoginActivity;

/* renamed from: X.36u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C623536u extends AbstractC16350or {
    public final ProgressDialog A00;
    public final /* synthetic */ LoginActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C623536u(Context context, LoginActivity loginActivity) {
        super(loginActivity);
        this.A01 = loginActivity;
        ProgressDialog show = ProgressDialog.show(context, "", loginActivity.getString(R.string.account_sync_authenticating), true, false);
        this.A00 = show;
        show.setCancelable(true);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        SystemClock.sleep(2000);
        LoginActivity loginActivity = this.A01;
        Account account = new Account(loginActivity.getString(R.string.app_name), "com.whatsapp");
        if (!AccountManager.get(loginActivity).addAccountExplicitly(account, null, null)) {
            return Boolean.FALSE;
        }
        Bundle A0D = C12970iu.A0D();
        A0D.putString("authAccount", account.name);
        A0D.putString("accountType", account.type);
        ((ActivityC58572qz) loginActivity).A01 = A0D;
        return Boolean.TRUE;
    }
}
