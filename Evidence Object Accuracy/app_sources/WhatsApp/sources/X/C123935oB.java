package X;

import android.view.View;

/* renamed from: X.5oB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123935oB extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ C134146Dm A00;

    public C123935oB(C134146Dm r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        C134146Dm r2 = this.A00;
        r2.A06.setVisibility(8);
        r2.A0B = null;
        r2.A0D = null;
        r2.A09.setVisibility(0);
        r2.A05.setVisibility(0);
    }
}
