package X;

/* renamed from: X.4WE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WE {
    public float A00 = -1.0f;
    public float A01 = -1.0f;
    public float A02 = -1.0f;
    public float A03 = -1.0f;
    public float A04 = -1.0f;
    public float A05 = -1.0f;
    public float A06 = -1.0f;
    public float A07 = -1.0f;
    public float A08 = 0.0f;
    public float A09 = 0.0f;
    public float A0A = 0.0f;
    public float A0B = -1.0f;
    public float A0C = -1.0f;
    public int A0D = -1;
    public int A0E;
    public int A0F = 1;
    public int A0G = -1;
    public int A0H = -1;
    public int A0I = -1;
    public int A0J;
    public int A0K = -1;
    public int A0L = 0;
    public int A0M = -1;
    public int A0N = -1;
    public int A0O;
    public int A0P = 1000;
    public int A0Q = 200;
    public int A0R;
    public int A0S;
    public int A0T = -1;
    public int A0U = 8000;
    public int A0V = -1;
    public int A0W;
    public int A0X = -1;
    public long A0Y = 0;
    public long A0Z = 0;
    public C112295Cv A0a;
    public AnonymousClass4XD A0b;
    public AnonymousClass5X6 A0c;
    public AnonymousClass4W2 A0d;
    public String A0e;
    public String A0f = "eng";
    public String A0g;
    public boolean A0h = true;
    public boolean A0i;
    public boolean A0j = false;
    public boolean A0k;
    public byte[] A0l;
    public byte[] A0m;
    public byte[] A0n = null;
    public byte[] A0o;

    public final byte[] A00(String str) {
        byte[] bArr = this.A0l;
        if (bArr != null) {
            return bArr;
        }
        throw AnonymousClass496.A00(C12960it.A0d(str, C12960it.A0k("Missing CodecPrivate for codec ")));
    }
}
