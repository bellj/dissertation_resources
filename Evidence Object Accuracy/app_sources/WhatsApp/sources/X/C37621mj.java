package X;

import java.io.InputStream;
import java.net.HttpURLConnection;

/* renamed from: X.1mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37621mj implements AbstractC37631mk {
    public final Boolean A00;
    public final HttpURLConnection A01;

    public C37621mj(Boolean bool, HttpURLConnection httpURLConnection) {
        this.A01 = httpURLConnection;
        this.A00 = bool;
    }

    @Override // X.AbstractC37631mk
    public int A7O() {
        return this.A01.getResponseCode();
    }

    @Override // X.AbstractC37631mk
    public InputStream AAZ(C18790t3 r3, Integer num, Integer num2) {
        return new AnonymousClass1oJ(r3, this.A01.getInputStream(), num, num2);
    }

    @Override // X.AbstractC37631mk
    public String AIP(String str) {
        return this.A01.getHeaderField(str);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.disconnect();
    }

    @Override // X.AbstractC37631mk
    public long getContentLength() {
        return (long) this.A01.getContentLength();
    }
}
