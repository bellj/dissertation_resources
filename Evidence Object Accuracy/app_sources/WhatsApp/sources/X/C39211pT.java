package X;

import android.net.Uri;
import android.os.PowerManager;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1pT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39211pT extends AbstractRunnableC39141pM {
    public final PowerManager.WakeLock A00;
    public final AbstractC15710nm A01;
    public final C15450nH A02;
    public final AnonymousClass01d A03;
    public final C14950mJ A04;
    public final C15660nh A05;
    public final C39201pS A06;
    public final AnonymousClass1EK A07;
    public final AnonymousClass160 A08;
    public final C22190yg A09;

    public C39211pT(PowerManager.WakeLock wakeLock, AbstractC15710nm r2, C15450nH r3, AnonymousClass01d r4, C14950mJ r5, C15660nh r6, C39201pS r7, AnonymousClass1EK r8, AnonymousClass160 r9, C22190yg r10) {
        super(r7);
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A09 = r10;
        this.A03 = r4;
        this.A08 = r9;
        this.A07 = r8;
        this.A06 = r7;
        this.A00 = wakeLock;
        this.A05 = r6;
    }

    /* JADX INFO: finally extract failed */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractRunnableC39141pM
    public X.AbstractC39731qS A00() {
        /*
        // Method dump skipped, instructions count: 1768
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39211pT.A00():X.1qS");
    }

    public AnonymousClass45H A01(AnonymousClass45D r10, AbstractC16130oV r11, File file) {
        C16150oX r0;
        File file2;
        if (!(r11 == null || r11.A05 == null || (r0 = r11.A02) == null || (file2 = r0.A0F) == null || !file2.exists())) {
            try {
                InputStream A0C = this.A09.A0C(Uri.fromFile(r11.A02.A0F), true);
                C14350lI.A0P(file, A0C);
                if (!r11.A05.equals(C38531oF.A00(file))) {
                    C14350lI.A0M(file);
                    A0C.close();
                    return null;
                }
                AnonymousClass160 r1 = this.A08;
                C16460p3 A0G = r11.A0G();
                AnonymousClass009.A05(A0G);
                r1.A01(A0G);
                AnonymousClass1EK r12 = this.A07;
                C38101nW A14 = r11.A14();
                AnonymousClass009.A05(A14);
                r12.A00(A14);
                int[] A06 = r11.A14().A06();
                if (A06 != null && A06.length == 4) {
                    boolean z = false;
                    if (((long) (A06[0] + A06[1] + A06[2] + A06[3])) == file.length()) {
                        r10.A05 = r11.A14().A06();
                        if (r11.A14().A06() != null) {
                            z = true;
                        }
                        r10.A04 = z;
                    }
                }
                C16150oX r13 = r11.A02;
                r10.A02 = r13.A06;
                r10.A03 = r13.A08;
                r10.A00 = r13.A02;
                r10.A01 = r13.A03;
                ((AbstractC91424Rr) r10).A03 = r11.A0G().A07();
                ((AbstractC91424Rr) r10).A00 = file;
                ((AbstractC91424Rr) r10).A02 = true;
                AnonymousClass45H A00 = r10.A00();
                A0C.close();
                return A00;
            } catch (IOException unused) {
                StringBuilder sb = new StringBuilder("ProcessImageTask/processImage/failed to get bitmap stream from file ");
                sb.append(r11.A02.A0F);
                Log.e(sb.toString());
                C14350lI.A0M(file);
            }
        }
        return null;
    }
}
