package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Process;
import java.lang.ref.Reference;
import java.util.PriorityQueue;

/* renamed from: X.1qC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39611qC extends AnonymousClass1MS {
    public final C39591qA A00;
    public volatile boolean A01;
    public final /* synthetic */ AnonymousClass1AB A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C39611qC(C39591qA r2, AnonymousClass1AB r3) {
        super("StickerImageFileLoader");
        this.A02 = r3;
        this.A00 = r2;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        AnonymousClass1qE r3;
        Drawable drawable;
        Context context;
        Process.setThreadPriority(10);
        AnonymousClass1qE r6 = null;
        while (!this.A01) {
            try {
                C39591qA r5 = this.A00;
                synchronized (r5) {
                    PriorityQueue priorityQueue = r5.A00;
                    if (priorityQueue.isEmpty()) {
                        r5.wait(5000);
                    }
                    r3 = !priorityQueue.isEmpty() ? (AnonymousClass1qE) priorityQueue.remove() : null;
                }
                if (!this.A01) {
                    if (r3 != null) {
                        if (!r3.A05) {
                            AnonymousClass1AB r2 = this.A02;
                            r2.A05(r2.A02, r3, r2.A09);
                        } else {
                            boolean z = r3 instanceof C39651qH;
                            if (z) {
                                C39651qH r0 = (C39651qH) r3;
                                if (!r0.A04.equals(r0.A00.getTag())) {
                                }
                            }
                            AnonymousClass1AB r62 = this.A02;
                            Reference reference = (Reference) r62.A09.get(r3.A04);
                            if (reference != null) {
                                drawable = (Drawable) reference.get();
                                if (drawable == null) {
                                }
                                r3.A00(drawable, r62.A02);
                            } else {
                                drawable = null;
                            }
                            if (!z) {
                                context = ((AnonymousClass1qI) r3).A00;
                            } else {
                                context = ((C39651qH) r3).A00.getContext();
                            }
                            AnonymousClass1KS r22 = r3.A03;
                            byte[] A01 = AnonymousClass1AB.A01(context, r62.A01, r22);
                            if (A01 != null) {
                                String str = r22.A0C;
                                AnonymousClass009.A05(str);
                                drawable = r62.A02(r3, str, A01);
                            }
                            r3.A00(drawable, r62.A02);
                        }
                    } else if (r6 == null) {
                        this.A02.A00 = null;
                        interrupt();
                    }
                    r6 = r3;
                } else {
                    return;
                }
            } catch (InterruptedException unused) {
                return;
            }
        }
    }
}
