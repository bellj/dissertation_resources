package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;

/* renamed from: X.5ww  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128955ww {
    public int A00;
    public Context A01;
    public AnonymousClass1In A02;

    public C128955ww(Context context) {
        this.A01 = context;
    }

    public View A00(ViewGroup viewGroup, AnonymousClass1IR r6) {
        if (r6.A03 != 1000 || !r6.A0P) {
            return new PeerPaymentTransactionRow(this.A01, this.A02, this.A00);
        }
        return C12960it.A0F(LayoutInflater.from(this.A01), viewGroup, R.layout.payment_transaction_interop_shimmer);
    }
}
