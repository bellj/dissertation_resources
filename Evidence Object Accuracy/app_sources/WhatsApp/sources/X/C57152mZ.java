package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2mZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57152mZ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57152mZ A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01 = 0;
    public Object A02;

    static {
        C57152mZ r0 = new C57152mZ();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        if (r6.A01 == 1) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004d, code lost:
        if (r6.A01 == 2) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004f, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0050, code lost:
        r6.A02 = r8.Afv(r6.A02, r9.A02, r4);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r7, java.lang.Object r8, java.lang.Object r9) {
        /*
        // Method dump skipped, instructions count: 284
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57152mZ.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.A01 == 1) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 1, 0);
        }
        if (this.A01 == 2) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A02, 2, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 1);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A02, 2);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
