package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59852vU extends AbstractC37191le {
    public final View A00;
    public final WaTextView A01;
    public final C54142gF A02;

    public C59852vU(View view, C54142gF r3) {
        super(view);
        this.A00 = view;
        this.A02 = r3;
        this.A01 = (WaTextView) C16700pc.A02(view, R.id.view_all_btn);
        C12990iw.A0R(view, R.id.business_list).setAdapter(r3);
    }
}
