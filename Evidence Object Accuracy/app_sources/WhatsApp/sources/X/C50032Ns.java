package X;

import java.util.List;

/* renamed from: X.2Ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50032Ns extends AnonymousClass1JW {
    public final int A00;
    public final String A01;
    public final List A02;

    public C50032Ns(AbstractC15710nm r1, C15450nH r2, String str, List list, int i) {
        super(r1, r2);
        this.A01 = str;
        this.A00 = i;
        this.A02 = list;
    }
}
