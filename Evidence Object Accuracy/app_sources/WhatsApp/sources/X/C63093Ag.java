package X;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.style.URLSpan;
import android.widget.TextView;

/* renamed from: X.3Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63093Ag {
    public static void A00(TextView textView, Object[] objArr, int i) {
        Context context = textView.getContext();
        Spannable spannable = (Spannable) Html.fromHtml(context.getString(i, objArr));
        URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan uRLSpan : uRLSpanArr) {
            spannable.setSpan(new C52302aa(context, uRLSpan.getURL()), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 0);
            spannable.removeSpan(uRLSpan);
        }
        textView.setText(spannable);
        C52162aM.A00(textView);
    }
}
