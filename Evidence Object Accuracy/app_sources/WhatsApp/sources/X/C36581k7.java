package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.util.ViewOnClickCListenerShape2S0300000_I0;
import java.util.List;

/* renamed from: X.1k7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36581k7 extends ArrayAdapter {
    public final /* synthetic */ ListChatInfo A00;

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getViewTypeCount() {
        return 2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36581k7(Context context, ListChatInfo listChatInfo, List list) {
        super(context, 0, list);
        this.A00 = listChatInfo;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return this.A00.A0d.size();
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getItemViewType(int i) {
        Object item = getItem(i);
        C15610nY r1 = this.A00.A0A;
        AnonymousClass009.A05(item);
        return r1.A0L((C15370n3) item, -1) ? 1 : 0;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass4RZ r0;
        TextEmojiLabel textEmojiLabel;
        String str;
        if (view == null) {
            int itemViewType = getItemViewType(i);
            int i2 = R.layout.group_chat_info_row_unknown_contact;
            if (itemViewType == 0) {
                i2 = R.layout.group_chat_info_row;
            }
            ListChatInfo listChatInfo = this.A00;
            view = listChatInfo.getLayoutInflater().inflate(i2, viewGroup, false);
            r0 = new AnonymousClass4RZ();
            r0.A02 = new C28801Pb(view, listChatInfo.A0A, listChatInfo.A0U, (int) R.id.name);
            r0.A01 = (TextEmojiLabel) view.findViewById(R.id.status);
            r0.A00 = (ImageView) view.findViewById(R.id.avatar);
            view.setTag(r0);
        } else {
            r0 = (AnonymousClass4RZ) view.getTag();
        }
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        C15370n3 r4 = (C15370n3) item;
        r0.A03 = r4;
        r0.A02.A06(r4);
        ImageView imageView = r0.A00;
        StringBuilder sb = new StringBuilder();
        sb.append(new AnonymousClass2TT(getContext()).A00(R.string.transition_avatar));
        sb.append(C15380n4.A03(r4.A0D));
        AnonymousClass028.A0k(imageView, sb.toString());
        ListChatInfo listChatInfo2 = this.A00;
        listChatInfo2.A0B.A06(r0.A00, r4);
        r0.A00.setOnClickListener(new ViewOnClickCListenerShape2S0300000_I0(this, r4, r0, 2));
        if (listChatInfo2.A0A.A0L(r4, -1)) {
            r0.A01.setVisibility(0);
            textEmojiLabel = r0.A01;
            str = listChatInfo2.A0A.A09(r4);
        } else {
            String str2 = r4.A0R;
            TextEmojiLabel textEmojiLabel2 = r0.A01;
            if (str2 != null) {
                textEmojiLabel2.setVisibility(0);
                textEmojiLabel = r0.A01;
                str = r4.A0R;
            } else {
                textEmojiLabel2.setVisibility(8);
                return view;
            }
        }
        textEmojiLabel.A0G(null, str);
        return view;
    }
}
