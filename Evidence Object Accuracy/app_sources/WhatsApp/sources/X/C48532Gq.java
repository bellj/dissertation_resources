package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;

/* renamed from: X.2Gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48532Gq implements AbstractC48542Gr {
    public final /* synthetic */ C36051jF A00;
    public final /* synthetic */ AbstractC14780m2 A01;

    @Override // X.AbstractC48542Gr
    public int AFu() {
        return 1;
    }

    public C48532Gq(C36051jF r1, AbstractC14780m2 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC48542Gr
    public void AM4(long j) {
        Handler handler = new Handler(Looper.getMainLooper());
        Message obtain = Message.obtain(handler, new RunnableBRunnable0Shape9S0100000_I0_9(this.A01, 26));
        C87924Do.A00(obtain);
        handler.sendMessageAtFrontOfQueue(obtain);
    }
}
