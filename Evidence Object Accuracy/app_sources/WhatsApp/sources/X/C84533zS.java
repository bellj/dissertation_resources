package X;

/* renamed from: X.3zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84533zS extends AnonymousClass2SH {
    public final /* synthetic */ AbstractActivityC37081lH A00;

    public C84533zS(AbstractActivityC37081lH r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2SH
    public void A00(String str) {
        AbstractActivityC37081lH r2 = this.A00;
        C44691zO A05 = r2.A09.A05(null, str);
        if (A05 != null) {
            r2.A0E.A0O(A05);
        }
    }

    @Override // X.AnonymousClass2SH
    public void A01(String str) {
        AbstractActivityC37081lH r2 = this.A00;
        C44691zO A05 = r2.A09.A05(null, str);
        if (A05 != null) {
            r2.A0E.A0O(A05);
        }
    }
}
