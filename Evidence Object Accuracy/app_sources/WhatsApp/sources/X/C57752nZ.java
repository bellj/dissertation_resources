package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2nZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57752nZ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57752nZ A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public int A01 = 0;
    public C43261wh A02;
    public C56992mI A03;
    public C57002mJ A04;
    public C57722nW A05;
    public Object A06;

    static {
        C57752nZ r0 = new C57752nZ();
        A07 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0076, code lost:
        if (r9.A01 == 5) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007b, code lost:
        if (r9.A01 == 4) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0080, code lost:
        if (r9.A01 == 6) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0082, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0083, code lost:
        r9.A06 = r11.Afv(r9.A06, r12.A06, r7);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r10, java.lang.Object r11, java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 556
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57752nZ.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public AnonymousClass4BE A0b() {
        int i = this.A01;
        if (i == 0) {
            return AnonymousClass4BE.A02;
        }
        if (i == 4) {
            return AnonymousClass4BE.A04;
        }
        if (i == 5) {
            return AnonymousClass4BE.A01;
        }
        if (i != 6) {
            return null;
        }
        return AnonymousClass4BE.A03;
    }

    public C57172mb A0c() {
        if (this.A01 == 6) {
            return (C57172mb) this.A06;
        }
        return C57172mb.A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C57722nW r0 = this.A05;
            if (r0 == null) {
                r0 = C57722nW.A06;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        if ((this.A00 & 2) == 2) {
            C56992mI r02 = this.A03;
            if (r02 == null) {
                r02 = C56992mI.A02;
            }
            i2 = AbstractC27091Fz.A08(r02, 2, i2);
        }
        if ((this.A00 & 4) == 4) {
            C57002mJ r03 = this.A04;
            if (r03 == null) {
                r03 = C57002mJ.A02;
            }
            i2 = AbstractC27091Fz.A08(r03, 3, i2);
        }
        if (this.A01 == 4) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A06, 4, i2);
        }
        if (this.A01 == 5) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A06, 5, i2);
        }
        if (this.A01 == 6) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A06, 6, i2);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r04 = this.A02;
            if (r04 == null) {
                r04 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r04, 15, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C57722nW r0 = this.A05;
            if (r0 == null) {
                r0 = C57722nW.A06;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C56992mI r02 = this.A03;
            if (r02 == null) {
                r02 = C56992mI.A02;
            }
            codedOutputStream.A0L(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            C57002mJ r03 = this.A04;
            if (r03 == null) {
                r03 = C57002mJ.A02;
            }
            codedOutputStream.A0L(r03, 3);
        }
        if (this.A01 == 4) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A06, 4);
        }
        if (this.A01 == 5) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A06, 5);
        }
        if (this.A01 == 6) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A06, 6);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r04 = this.A02;
            if (r04 == null) {
                r04 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r04, 15);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
