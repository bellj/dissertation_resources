package X;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.04n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC008904n {
    public C10410ea A00;
    public C009004o A01;
    public C10280eM A02;

    public abstract int A01();

    public abstract int A02(Object obj);

    public abstract Object A03(int i, int i2);

    public abstract Object A04(int i, Object obj);

    public abstract Map A05();

    public abstract void A06();

    public abstract void A07(int i);

    public static boolean A00(Collection collection, Map map) {
        int size = map.size();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    public Object[] A08(Object[] objArr, int i) {
        int A01 = A01();
        if (objArr.length < A01) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), A01);
        }
        for (int i2 = 0; i2 < A01; i2++) {
            objArr[i2] = A03(i2, i);
        }
        if (objArr.length > A01) {
            objArr[A01] = null;
        }
        return objArr;
    }
}
