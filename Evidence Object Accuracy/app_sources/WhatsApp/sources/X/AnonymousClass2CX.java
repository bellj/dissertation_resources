package X;

import java.util.List;

/* renamed from: X.2CX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CX extends AbstractC34011fR {
    public final int A00;
    public final int A01;
    public final C22470z8 A02;
    public final AnonymousClass1IS A03;
    public final String A04;
    public final List A05;
    public final boolean A06;

    public AnonymousClass2CX(C22470z8 r1, AnonymousClass1IS r2, String str, List list, int i, int i2, boolean z) {
        this.A02 = r1;
        this.A04 = str;
        this.A05 = list;
        this.A00 = i;
        this.A06 = z;
        this.A01 = i2;
        this.A03 = r2;
    }
}
