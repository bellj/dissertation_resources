package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.SystemClock;
import java.io.File;
import java.util.EnumSet;

/* renamed from: X.0I8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I8 extends AnonymousClass03S {
    public float A00;
    public float A01;
    public float A02;
    public long A03;
    public Bitmap A04;
    public String A05;
    public String A06;
    public EnumSet A07 = EnumSet.noneOf(EnumC03800Je.class);
    public boolean A08;
    public final float A09;
    public final Paint A0A = new Paint(1);

    public AnonymousClass0I8(AnonymousClass04Q r4) {
        super(r4);
        String str;
        float f = super.A05;
        if (f >= 2.0f) {
            str = "https://www.facebook.com/images/here_maps/here_maps_logo_64px.png";
        } else {
            str = "https://www.facebook.com/images/here_maps/here_maps_logo_32px.png";
        }
        this.A06 = str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.A08.getFilesDir().getAbsolutePath());
        sb.append(File.separator);
        sb.append("copyright_logo");
        this.A05 = sb.toString();
        this.A09 = f * 12.0f;
        super.A03 = 5;
        super.A02 = Float.MAX_VALUE;
    }

    @Override // X.AnonymousClass03S
    public void A07() {
        AnonymousClass04Q r3 = super.A09;
        int height = r3.A0E.getHeight();
        float f = this.A09;
        this.A02 = ((float) 0) + f;
        this.A01 = (((float) (height - r3.A04)) - this.A00) - f;
    }

    @Override // X.AnonymousClass03S
    public void A08(Canvas canvas) {
        if (!this.A07.contains(EnumC03800Je.OSM) && this.A07.contains(EnumC03800Je.HERE)) {
            Bitmap bitmap = this.A04;
            if (bitmap != null) {
                canvas.drawBitmap(bitmap, this.A02, this.A01, this.A0A);
            } else if (!this.A08 && SystemClock.uptimeMillis() - this.A03 > 10000) {
                this.A08 = true;
                this.A03 = SystemClock.uptimeMillis();
                AnonymousClass0UE.A01(new AnonymousClass0IH(this));
            }
        }
    }
}
