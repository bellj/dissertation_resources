package X;

import java.util.List;

/* renamed from: X.42Z  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42Z extends AbstractC38081nU {
    public final /* synthetic */ C38051nR A00;
    public final /* synthetic */ List A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass42Z(C38051nR r1, Runnable runnable, List list) {
        super(runnable);
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return Boolean.valueOf(this.A00.A00.A0l(this.A01));
    }
}
