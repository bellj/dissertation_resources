package X;

/* renamed from: X.0Jl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03870Jl {
    FILE_NOT_FOUND(0),
    INVALID_PARAMETER(1),
    INTERNAL_ERROR(2);
    
    public final String text;

    EnumC03870Jl(int i) {
        this.text = r2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.text;
    }
}
