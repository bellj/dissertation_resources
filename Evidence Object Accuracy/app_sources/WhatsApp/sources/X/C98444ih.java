package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4ih  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98444ih implements IInterface, AnonymousClass5Y4 {
    public final IBinder A00;
    public final String A01 = "com.google.android.gms.auth.api.phone.internal.ISmsRetrieverApiService";

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }

    public C98444ih(IBinder iBinder) {
        this.A00 = iBinder;
    }
}
