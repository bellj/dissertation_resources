package X;

import android.content.Context;
import android.graphics.Canvas;
import java.util.Comparator;

/* renamed from: X.03S  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass03S {
    public static int A0D;
    public static final Comparator A0E = new C10340eS();
    public double A00;
    public double A01;
    public float A02;
    public int A03 = 1;
    public boolean A04 = true;
    public final float A05;
    public final int A06;
    public final int A07;
    public final Context A08;
    public final AnonymousClass04Q A09;
    public final AnonymousClass0U9 A0A;
    public final C05050Ob A0B = new C05050Ob();
    public final float[] A0C = new float[2];

    public int A00(float f, float f2) {
        return 0;
    }

    public void A02() {
    }

    public void A03(float f, float f2) {
    }

    public void A04(float f, float f2) {
    }

    public boolean A05(float f, float f2) {
        return false;
    }

    public boolean A06(float f, float f2, float f3, float f4) {
        return false;
    }

    public void A07() {
    }

    public abstract void A08(Canvas canvas);

    public AnonymousClass03S(AnonymousClass04Q r3) {
        int i = A0D;
        A0D = i + 1;
        this.A06 = i;
        this.A09 = r3;
        this.A0A = r3.A0R;
        Context context = r3.A0E.getContext();
        this.A08 = context;
        this.A05 = context.getResources().getDisplayMetrics().density;
        this.A07 = r3.A0O;
    }

    public void A01() {
        this.A09.A0E.invalidate();
    }

    public void A09(boolean z) {
        this.A04 = z;
        A01();
    }
}
