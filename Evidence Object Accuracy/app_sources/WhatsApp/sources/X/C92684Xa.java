package X;

/* renamed from: X.4Xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92684Xa {
    public final C94324bc A00;
    public final C94324bc A01;

    public C92684Xa(C94324bc r1, C94324bc r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static C92684Xa A00(C94324bc r2, long j, long j2) {
        return new C92684Xa(r2, new C94324bc(j, j2));
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C92684Xa.class != obj.getClass()) {
                return false;
            }
            C92684Xa r5 = (C92684Xa) obj;
            if (!this.A00.equals(r5.A00) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return C12990iw.A08(this.A01, this.A00.hashCode() * 31);
    }

    public String toString() {
        String A0b;
        StringBuilder A0k = C12960it.A0k("[");
        C94324bc r0 = this.A00;
        A0k.append(r0);
        C94324bc r1 = this.A01;
        if (r0.equals(r1)) {
            A0b = "";
        } else {
            A0b = C12960it.A0b(", ", r1);
        }
        A0k.append(A0b);
        return C12960it.A0d("]", A0k);
    }
}
