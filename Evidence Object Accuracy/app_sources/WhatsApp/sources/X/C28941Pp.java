package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28941Pp {
    public int A00;
    public int A01 = 0;
    public int A02;
    public int A03;
    public int A04 = 0;
    public int A05 = 0;
    public long A06;
    public AnonymousClass1IR A07;
    public Jid A08;
    public C15930o9 A09;
    public C15930o9 A0A;
    public AbstractC15340mz A0B;
    public AnonymousClass1IS A0C;
    public AnonymousClass4AW A0D;
    public C32141bg A0E;
    public Boolean A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public String A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public String A0V;
    public String A0W;
    public String A0X;
    public String A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public byte[] A0c;
    public byte[] A0d;
    public byte[] A0e;
    public final long A0f;
    public final Jid A0g;
    public final UserJid A0h;
    public final AnonymousClass1IS A0i;
    public final C20320vZ A0j;
    public final String A0k;

    public /* synthetic */ C28941Pp(Jid jid, UserJid userJid, C20320vZ r5, String str, long j, boolean z) {
        this.A0j = r5;
        AnonymousClass009.A05(jid);
        this.A0g = jid;
        this.A0h = userJid;
        this.A0k = str;
        this.A0f = j;
        AbstractC14640lm A00 = C15380n4.A00(jid);
        AnonymousClass009.A05(str);
        this.A0i = new AnonymousClass1IS(A00, str, z);
        if (userJid != null) {
            this.A0C = new AnonymousClass1IS(userJid, str, true);
        }
    }

    public int A00() {
        Integer num = this.A0J;
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public Jid A01() {
        Jid jid = this.A0g;
        return (C15380n4.A0J(jid) || C15380n4.A0F(jid)) ? this.A08 : jid;
    }

    public AbstractC15340mz A02(byte b) {
        AbstractC15340mz r0 = this.A0B;
        if (r0 == null) {
            C20320vZ r3 = this.A0j;
            AnonymousClass1IS r2 = this.A0C;
            if (r2 == null) {
                r2 = this.A0i;
            }
            r0 = r3.A01(r2, b, this.A0f);
            this.A0B = r0;
        }
        A04(r0);
        return this.A0B;
    }

    public String A03() {
        AnonymousClass1IS r0 = this.A0C;
        if (r0 == null) {
            r0 = this.A0i;
        }
        return r0.toString();
    }

    public void A04(AbstractC15340mz r5) {
        int i;
        this.A0B = r5;
        r5.A17 = DeviceJid.of(A01());
        AnonymousClass1IS r0 = this.A0C;
        if (r0 == null) {
            r0 = this.A0i;
        }
        if (r0.A02) {
            this.A0B.A0Y(4);
            this.A0B.A1B = true;
        } else {
            Jid jid = this.A08;
            if (jid != null) {
                this.A0B.A0e(C15380n4.A00(jid));
            }
        }
        Integer num = this.A0J;
        if (num != null) {
            this.A0B.A0B = num.intValue();
        }
        Integer num2 = this.A0I;
        if (num2 != null) {
            this.A0B.A0A = num2.intValue();
        }
        AbstractC15340mz r1 = this.A0B;
        r1.A0I = this.A0f;
        Long l = this.A0L;
        if (l != null) {
            r1.A0G = l.longValue();
        }
        r1.A14 = this.A06;
        Integer num3 = this.A0H;
        if (num3 != null) {
            r1.A0W = num3;
        }
        String str = this.A0U;
        if (str != null) {
            r1.A0k = str;
        }
        String str2 = this.A0T;
        if (str2 != null) {
            r1.A0g = str2;
        }
        String str3 = this.A0X;
        if (str3 != null) {
            r1.A0n = str3;
        }
        r1.A0r = this.A0Z;
        Long l2 = this.A0P;
        if (l2 != null) {
            r1.A0c = l2;
        }
        r1.A0E = this.A05;
        r1.A0U = this.A0E;
        r1.A0b = this.A0O;
        r1.A0a = this.A0N;
        r1.A0Z = this.A0M;
        int i2 = this.A01;
        if (i2 != 0) {
            r1.A01 = i2;
        }
        int i3 = this.A03;
        if (i3 != 0) {
            r1.A0T(i3);
        }
        AnonymousClass1IR r02 = this.A07;
        if (r02 != null) {
            AbstractC15340mz r12 = this.A0B;
            r12.A0L = r02;
            String str4 = r02.A0K;
            if (str4 == null) {
                str4 = "UNSET";
            }
            r12.A0m = str4;
        }
        AbstractC15340mz r2 = this.A0B;
        r2.A0u = this.A0b;
        if (r2.A0r && r2.A0B == 0) {
            r2.A0V(0);
            this.A0B.A0j(null);
        } else if (r2.A04 <= 0 && (i = this.A02) != 0 && C15380n4.A0J(r2.A0z.A00)) {
            r2.A0V(i);
        }
    }
}
