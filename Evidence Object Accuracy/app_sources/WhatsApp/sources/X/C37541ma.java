package X;

import com.facebook.msys.mci.DefaultUUID;
import com.facebook.msys.util.Provider;

/* renamed from: X.1ma  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37541ma implements Provider {
    public final /* synthetic */ C22870zm A00;

    public C37541ma(C22870zm r1) {
        this.A00 = r1;
    }

    @Override // com.facebook.msys.util.Provider
    public Object get() {
        return DefaultUUID.mUUID;
    }
}
