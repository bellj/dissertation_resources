package X;

import android.content.Intent;
import android.view.ViewGroup;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity;

/* renamed from: X.5iB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121465iB extends AbstractActivityC121775jy {
    public C130105yo A00;
    public C128375w0 A01;

    @Override // X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i != 1007) {
            return super.A2e(viewGroup, i);
        }
        return new C122775m5(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_divider));
    }

    public void A2f() {
        Intent A0D = C12990iw.A0D(this, Conversation.class);
        A0D.putExtra("jid", C1310561a.A01(C12980iv.A0p(this.A00.A02(), "env_tier")));
        ((ActivityC13790kL) this).A00.A06(this, A0D);
    }

    public void A2g(C127055ts r3) {
        AnonymousClass5s9 r0;
        if (r3.A00 == 405 && (r0 = r3.A01) != null) {
            C1310561a.A06(this, new C126025sD((String) r0.A00));
        }
    }

    public boolean A2h() {
        if (this.A00.A03() == null) {
            return true;
        }
        Intent A0D = C12990iw.A0D(this, NoviPayLimitationsBloksActivity.class);
        A0D.putExtra("limitation_origin", 2);
        startActivity(A0D);
        return false;
    }
}
