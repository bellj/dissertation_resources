package X;

import android.animation.Animator;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;

/* renamed from: X.4eF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95724eF implements Animator.AnimatorListener {
    public final /* synthetic */ MaximizedParticipantVideoDialogFragment A00;
    public final /* synthetic */ Runnable A01;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }

    public C95724eF(MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment, Runnable runnable) {
        this.A00 = maximizedParticipantVideoDialogFragment;
        this.A01 = runnable;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A01.run();
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.run();
    }
}
