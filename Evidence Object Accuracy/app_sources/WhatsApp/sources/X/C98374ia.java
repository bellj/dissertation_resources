package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4ia  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98374ia implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C98374ia(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }

    public final void A00(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            C12990iw.A18(this.A00, parcel, obtain, i);
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
