package X;

import android.view.SurfaceHolder;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;

/* renamed from: X.3MU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MU implements SurfaceHolder.Callback {
    public final /* synthetic */ AnonymousClass27M A00;

    public AnonymousClass3MU(AnonymousClass27M r1) {
        this.A00 = r1;
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        String str;
        AnonymousClass27M r4 = this.A00;
        if (r4.A03 != null) {
            SurfaceHolder surfaceHolder2 = r4.A0J;
            if (surfaceHolder2.getSurface() == null) {
                str = "qrview/surfacechanged: no surface";
            } else {
                r4.A04.post(new RunnableBRunnable0Shape7S0200000_I0_7(r4, 25, surfaceHolder2));
                return;
            }
        } else if (r4.A04 == null) {
            str = "qrview/surfacechanged: no camera";
        } else {
            return;
        }
        Log.e(str);
        r4.A00();
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i("qrview/surfaceCreated");
        AnonymousClass27M r3 = this.A00;
        r3.A04.post(new RunnableBRunnable0Shape10S0100000_I0_10(r3, 8));
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i("qrview/surfacedestroyed");
        AnonymousClass27M r3 = this.A00;
        r3.A04.post(new RunnableBRunnable0Shape10S0100000_I0_10(r3, 12));
    }
}
