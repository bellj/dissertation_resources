package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.53X  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass53X implements AnonymousClass2EO {
    public final /* synthetic */ C19810ui A00;
    public final /* synthetic */ AnonymousClass32K A01;
    public final /* synthetic */ UserJid A02;

    @Override // X.AbstractC13940ka
    public void AR1() {
    }

    public AnonymousClass53X(C19810ui r1, AnonymousClass32K r2, UserJid userJid) {
        this.A00 = r1;
        this.A02 = userJid;
        this.A01 = r2;
    }

    @Override // X.AbstractC13940ka
    public void AR0() {
        this.A01.A02();
    }

    @Override // X.AnonymousClass2EO
    public void AR2(C30141Wg r4) {
        this.A00.A00(this.A01, r4, this.A02);
    }
}
