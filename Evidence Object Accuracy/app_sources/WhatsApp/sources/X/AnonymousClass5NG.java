package X;

import java.math.BigInteger;
import java.util.Arrays;

/* renamed from: X.5NG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NG extends AnonymousClass1TL {
    public final int A00;
    public final byte[] A01;

    public AnonymousClass5NG(long j) {
        this.A01 = BigInteger.valueOf(j).toByteArray();
        this.A00 = 0;
    }

    public AnonymousClass5NG(BigInteger bigInteger) {
        this.A01 = bigInteger.toByteArray();
        this.A00 = 0;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A01, 2, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A01);
    }

    public AnonymousClass5NG(byte[] bArr, boolean z) {
        int length = bArr.length;
        if (length == 0 || (length != 1 && bArr[0] == (bArr[1] >> 7) && !C94664cJ.A01("org.spongycastle.asn1.allow_unsafe_integer"))) {
            throw C12970iu.A0f("malformed integer");
        }
        this.A01 = z ? AnonymousClass1TT.A02(bArr) : bArr;
        int i = length - 1;
        int i2 = 0;
        while (i2 < i) {
            int i3 = i2 + 1;
            if (bArr[i2] != (bArr[i3] >> 7)) {
                break;
            }
            i2 = i3;
        }
        this.A00 = i2;
    }

    public static AnonymousClass5NG A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NG)) {
            return (AnonymousClass5NG) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5NG) AnonymousClass1TL.A03((byte[]) obj);
            } catch (Exception e) {
                throw C12970iu.A0f(C12960it.A0d(e.toString(), C12960it.A0k("encoding error in getInstance: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    public static AnonymousClass5NG A01(AnonymousClass5NU r2) {
        AnonymousClass1TL A00 = AnonymousClass5NU.A00(r2);
        if (A00 instanceof AnonymousClass5NG) {
            return A00(A00);
        }
        return new AnonymousClass5NG(AnonymousClass5NH.A05(A00), true);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A01);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NG)) {
            return false;
        }
        return Arrays.equals(this.A01, ((AnonymousClass5NG) r3).A01);
    }

    public int A0B() {
        byte[] bArr = this.A01;
        int length = bArr.length;
        int i = this.A00;
        if (length - i <= 4) {
            int max = Math.max(i, length - 4);
            int i2 = -1 & bArr[max];
            while (true) {
                max++;
                if (max >= length) {
                    return i2;
                }
                i2 = (i2 << 8) | (bArr[max] & 255);
            }
        } else {
            throw new ArithmeticException("ASN.1 Integer out of int range");
        }
    }

    public boolean A0C(BigInteger bigInteger) {
        if (bigInteger != null) {
            byte[] bArr = this.A01;
            int i = this.A00;
            int length = bArr.length;
            int max = Math.max(i, length - 4);
            int i2 = -1 & bArr[max];
            while (true) {
                max++;
                if (max >= length) {
                    break;
                }
                i2 = (i2 << 8) | (bArr[max] & 255);
            }
            if (i2 == bigInteger.intValue() && new BigInteger(bArr).equals(bigInteger)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return new BigInteger(this.A01).toString();
    }
}
