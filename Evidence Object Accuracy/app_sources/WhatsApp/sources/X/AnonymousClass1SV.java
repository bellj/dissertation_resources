package X;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.1SV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SV implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ Handler A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ Runnable A02;

    public AnonymousClass1SV(Handler handler, View view, Runnable runnable) {
        this.A00 = handler;
        this.A02 = runnable;
        this.A01 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        Handler handler = this.A00;
        Message obtain = Message.obtain(handler, this.A02);
        C87924Do.A00(obtain);
        handler.sendMessageAtFrontOfQueue(obtain);
        this.A01.getViewTreeObserver().removeOnPreDrawListener(this);
        return true;
    }
}
