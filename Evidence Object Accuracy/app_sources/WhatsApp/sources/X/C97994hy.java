package X;

import android.media.AudioManager;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;

/* renamed from: X.4hy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97994hy implements AudioManager.OnAudioFocusChangeListener {
    public final Handler A00;
    public final /* synthetic */ AnonymousClass3FG A01;

    public C97994hy(Handler handler, AnonymousClass3FG r2) {
        this.A01 = r2;
        this.A00 = handler;
    }

    @Override // android.media.AudioManager.OnAudioFocusChangeListener
    public void onAudioFocusChange(int i) {
        this.A00.post(new RunnableBRunnable0Shape1S0101000_I1(this, i, 0));
    }
}
