package X;

import android.text.TextUtils;
import android.util.Log;

/* renamed from: X.5qK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124915qK {
    public static Object A00(Object obj, int i, boolean z) {
        StringBuilder A0k;
        if (obj != null) {
            boolean z2 = false;
            if (i != 1) {
                if (i == 2) {
                    return Long.valueOf(C12980iv.A0G(obj));
                }
                if (i == 3) {
                    return Float.valueOf(C72453ed.A02(obj));
                }
                if (i == 4) {
                    return obj;
                }
                if (i != 5) {
                    A0k = C12960it.A0k("BloksFieldStatParser/parseValue/unknown type/type=");
                    A0k.append(i);
                } else {
                    String str = (String) obj;
                    if (str != null) {
                        String[] split = TextUtils.split(str, "\\|");
                        int length = split.length;
                        if (length != 2) {
                            A0k = C12960it.A0k("BloksFieldStatParser/parseValue/invalid enum format/length=");
                            A0k.append(length);
                        } else if (z) {
                            return split[1];
                        } else {
                            return Integer.valueOf(AnonymousClass3GC.A00(split[0]));
                        }
                    }
                }
                Log.d("Whatsapp", A0k.toString());
            } else if (obj instanceof Boolean) {
                return obj;
            } else {
                if (1 == C12960it.A05(obj)) {
                    z2 = true;
                }
                return Boolean.valueOf(z2);
            }
        }
        return null;
    }
}
