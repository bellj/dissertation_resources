package X;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129815yL {
    public final SharedPreferences A00;
    public final C129995yd A01;

    public C129815yL(SharedPreferences sharedPreferences, C129995yd r2) {
        this.A01 = r2;
        this.A00 = sharedPreferences;
    }

    public static void A00(byte[] bArr) {
        if (bArr != null) {
            Arrays.fill(bArr, (byte) 0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004f, code lost:
        if (r2.length <= 0) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0085, code lost:
        if (r4 != null) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a5, code lost:
        if (r4 == null) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a7, code lost:
        java.util.Arrays.fill(r4, (byte) 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00aa, code lost:
        if (r3 == null) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ac, code lost:
        java.util.Arrays.fill(r3, (byte) 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00af, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b1, code lost:
        java.util.Arrays.fill(r2, (byte) 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b4, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.content.Context r13, java.lang.String r14, java.lang.String r15, java.util.Date r16, java.util.Date r17, byte[] r18) {
        /*
            r12 = this;
            r0 = 16
            byte[] r7 = X.C117305Zk.A1a(r0)
            byte[] r6 = X.C117305Zk.A1a(r0)
            r5 = 0
            java.lang.String r1 = "AES/CBC/PKCS5Padding"
            java.lang.String r0 = "AES"
            javax.crypto.spec.SecretKeySpec r3 = new javax.crypto.spec.SecretKeySpec     // Catch: Exception -> 0x00b5
            r3.<init>(r7, r0)     // Catch: Exception -> 0x00b5
            javax.crypto.Cipher r2 = javax.crypto.Cipher.getInstance(r1)     // Catch: Exception -> 0x00b5
            javax.crypto.spec.IvParameterSpec r1 = new javax.crypto.spec.IvParameterSpec     // Catch: Exception -> 0x00b5
            r1.<init>(r6)     // Catch: Exception -> 0x00b5
            r0 = 1
            r2.init(r0, r3, r1)     // Catch: Exception -> 0x00b5
            r0 = r18
            byte[] r4 = r2.doFinal(r0)     // Catch: Exception -> 0x00b5
            r0 = 2
            byte[][] r0 = new byte[r0]
            X.C12990iw.A1P(r7, r6, r0)
            byte[] r3 = X.C16050oM.A05(r0)
            r2 = 0
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r1 < r0) goto L_0x0051
            X.5yd r1 = r12.A01
            if (r1 == 0) goto L_0x0051
            r0 = r17
            r8 = r16
            java.security.KeyPair r0 = r1.A00(r13, r15, r8, r0)
            if (r0 == 0) goto L_0x0051
            byte[] r2 = r1.A03(r3, r15)
            if (r2 == 0) goto L_0x0051
            int r0 = r2.length
            r10 = 1
            r11 = r2
            if (r0 > 0) goto L_0x0053
        L_0x0051:
            r10 = 0
            r11 = r3
        L_0x0053:
            org.json.JSONObject r8 = X.C117295Zj.A0a()     // Catch: JSONException -> 0x009f, all -> 0x0088
            r9 = 2
            java.lang.String r1 = android.util.Base64.encodeToString(r4, r9)     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.lang.String r0 = "key"
            r8.put(r0, r1)     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.lang.String r1 = android.util.Base64.encodeToString(r11, r9)     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.lang.String r0 = "protection-key"
            r8.put(r0, r1)     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.lang.String r0 = "protection-encrypted"
            r8.put(r0, r10)     // Catch: JSONException -> 0x009f, all -> 0x0088
            android.content.SharedPreferences r0 = r12.A00     // Catch: JSONException -> 0x009f, all -> 0x0088
            android.content.SharedPreferences$Editor r1 = r0.edit()     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.lang.String r0 = r8.toString()     // Catch: JSONException -> 0x009f, all -> 0x0088
            r1.putString(r14, r0)     // Catch: JSONException -> 0x009f, all -> 0x0088
            r1.apply()     // Catch: JSONException -> 0x009f, all -> 0x0088
            java.util.Arrays.fill(r7, r5)
            java.util.Arrays.fill(r6, r5)
            if (r4 == 0) goto L_0x00aa
            goto L_0x00a7
        L_0x0088:
            r0 = move-exception
            java.util.Arrays.fill(r7, r5)
            java.util.Arrays.fill(r6, r5)
            if (r4 == 0) goto L_0x0094
            java.util.Arrays.fill(r4, r5)
        L_0x0094:
            if (r3 == 0) goto L_0x0099
            java.util.Arrays.fill(r3, r5)
        L_0x0099:
            if (r2 == 0) goto L_0x009e
            java.util.Arrays.fill(r2, r5)
        L_0x009e:
            throw r0
        L_0x009f:
            java.util.Arrays.fill(r7, r5)
            java.util.Arrays.fill(r6, r5)
            if (r4 == 0) goto L_0x00aa
        L_0x00a7:
            java.util.Arrays.fill(r4, r5)
        L_0x00aa:
            if (r3 == 0) goto L_0x00af
            java.util.Arrays.fill(r3, r5)
        L_0x00af:
            if (r2 == 0) goto L_0x00b4
            java.util.Arrays.fill(r2, r5)
        L_0x00b4:
            return
        L_0x00b5:
            java.util.Arrays.fill(r7, r5)
            java.util.Arrays.fill(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129815yL.A01(android.content.Context, java.lang.String, java.lang.String, java.util.Date, java.util.Date, byte[]):void");
    }

    public void A02(String str, String str2) {
        C129995yd r0;
        SharedPreferences sharedPreferences = this.A00;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (sharedPreferences.contains(str)) {
            edit.remove(str);
        }
        edit.apply();
        if (Build.VERSION.SDK_INT >= 18 && (r0 = this.A01) != null) {
            r0.A01(str2);
        }
    }

    public byte[] A03(String str, String str2) {
        byte[] bArr;
        byte[] bArr2 = null;
        String string = this.A00.getString(str, null);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(string);
            boolean z = A05.getBoolean("protection-encrypted");
            String string2 = A05.getString("key");
            byte[] decode = Base64.decode(A05.getString("protection-key"), 2);
            if (z) {
                C129995yd r0 = this.A01;
                if (r0 == null) {
                    A00(decode);
                    return null;
                }
                bArr = r0.A02(decode, str2);
            } else {
                bArr = decode;
            }
            byte[][] A06 = C16050oM.A06(bArr, 16, 16);
            byte[] bArr3 = A06[0];
            byte[] bArr4 = A06[1];
            byte[] decode2 = Base64.decode(string2, 2);
            try {
                SecretKeySpec secretKeySpec = new SecretKeySpec(bArr3, "AES");
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                instance.init(2, secretKeySpec, new IvParameterSpec(bArr4));
                bArr2 = instance.doFinal(decode2);
            } catch (Exception unused) {
            } catch (Throwable th) {
                A00(bArr3);
                A00(bArr4);
                A00(decode);
                A00(decode2);
                A00(bArr);
                throw th;
            }
            A00(bArr3);
            A00(bArr4);
            A00(decode);
            A00(decode2);
            A00(bArr);
            return bArr2;
        } catch (JSONException unused2) {
            return null;
        }
    }
}
