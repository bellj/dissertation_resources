package X;

/* renamed from: X.5wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128805wh {
    public final /* synthetic */ AbstractC16870pt A00;
    public final /* synthetic */ C123575nN A01;

    public C128805wh(AbstractC16870pt r1, C123575nN r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(AnonymousClass1IR r4, C452120p r5) {
        if (r5 != null || r4 == null) {
            C123575nN r2 = this.A01;
            r2.A0d.A06(C12960it.A0b("send UpiRaiseComplaint: onRequestError: ", r5));
            AbstractC16870pt r1 = this.A00;
            if (r1 != null) {
                r1.AKa(r5, 18);
            }
            C118185bP.A01(r2, new C123535nJ(C43951xu.A03));
            r2.A0O(false);
            return;
        }
        C123575nN r12 = this.A01;
        C17070qD r0 = r12.A0a;
        r0.A03();
        r0.A08.A0h(r4);
        r12.A0G.A0H(new Runnable(r4, this) { // from class: X.6Ik
            public final /* synthetic */ AnonymousClass1IR A00;
            public final /* synthetic */ C128805wh A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C128805wh r02 = this.A01;
                AnonymousClass1IR r13 = this.A00;
                C123575nN r22 = r02.A01;
                r22.A08.A05(r13);
                C118185bP.A01(r22, new C123535nJ(111));
            }
        });
    }
}
