package X;

import android.content.Context;
import com.whatsapp.gallerypicker.PhotoViewPager;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.443  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass443 extends PhotoViewPager {
    public final /* synthetic */ MediaViewBaseFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass443(Context context, MediaViewBaseFragment mediaViewBaseFragment) {
        super(context, null);
        this.A00 = mediaViewBaseFragment;
        this.A0W = new C105734uX(this, mediaViewBaseFragment);
        ((PhotoViewPager) this).A04 = new AnonymousClass5UE() { // from class: X.56x
            @Override // X.AnonymousClass5UE
            public final int AXb(float f, float f2) {
                MediaViewBaseFragment mediaViewBaseFragment2 = AnonymousClass443.this.A00;
                return (mediaViewBaseFragment2.A0C().isFinishing() || !MediaViewBaseFragment.A08(mediaViewBaseFragment2)) ? 0 : 3;
            }
        };
    }
}
