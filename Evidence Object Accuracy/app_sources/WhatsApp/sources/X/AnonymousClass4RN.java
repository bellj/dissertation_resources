package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.4RN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4RN {
    public final int A00;
    public final int A01;
    public final UserJid A02;
    public final List A03;

    public AnonymousClass4RN(UserJid userJid, List list, int i, int i2) {
        this.A02 = userJid;
        this.A03 = list;
        this.A01 = i;
        this.A00 = i2;
    }
}
