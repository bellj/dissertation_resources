package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59722vH extends AbstractC37191le {
    public final WaTextView A00;

    public C59722vH(View view) {
        super(view);
        this.A00 = C12960it.A0N(view, R.id.load_all_btn);
    }
}
