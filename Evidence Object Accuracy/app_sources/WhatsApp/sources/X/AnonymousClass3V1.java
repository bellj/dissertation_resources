package X;

/* renamed from: X.3V1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3V1 implements AnonymousClass285 {
    public final /* synthetic */ AnonymousClass19T A00;
    public final /* synthetic */ AnonymousClass4Q7 A01;
    public final /* synthetic */ String A02;

    public AnonymousClass3V1(AnonymousClass19T r1, AnonymousClass4Q7 r2, String str) {
        this.A02 = str;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass285
    public void AQP(String str, int i) {
        if (this.A02.equals(str)) {
            AnonymousClass4Q7 r3 = this.A01;
            AnonymousClass1ZC r0 = r3.A02.A04;
            if (r0 != null) {
                AnonymousClass1ZH r1 = r0.A01;
                if (!r1.A00) {
                    r1.A00 = true;
                    r3.A00.A03.A0a(r3.A01, -1);
                }
            }
            this.A00.A0M.remove(this);
        }
    }

    @Override // X.AnonymousClass285
    public void AQQ(AnonymousClass4TA r5, String str) {
        if (this.A02.equals(str)) {
            AnonymousClass4Q7 r3 = this.A01;
            AnonymousClass1ZC r0 = r3.A02.A04;
            if (r0 != null) {
                AnonymousClass1ZH r1 = r0.A01;
                if (r1.A00) {
                    r1.A00 = false;
                    r3.A00.A03.A0a(r3.A01, -1);
                }
            }
            this.A00.A0M.remove(this);
        }
    }
}
