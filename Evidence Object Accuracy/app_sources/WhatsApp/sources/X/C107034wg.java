package X;

/* renamed from: X.4wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107034wg implements AbstractC117075Yd {
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final long[] A05;

    public C107034wg(long[] jArr, int i, long j, long j2, long j3) {
        this.A03 = j;
        this.A00 = i;
        this.A04 = j2;
        this.A05 = jArr;
        this.A02 = j3;
        this.A01 = j3 != -1 ? j + j3 : -1;
    }

    @Override // X.AbstractC117075Yd
    public long ACO() {
        return this.A01;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A04;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        long A0X;
        long max;
        long j2;
        double d;
        long[] jArr = this.A05;
        if (!C12960it.A1W(jArr)) {
            A0X = 0;
            j2 = this.A03;
            max = (long) this.A00;
        } else {
            long j3 = this.A04;
            A0X = C72453ed.A0X(j, j3);
            double d2 = (((double) A0X) * 100.0d) / ((double) j3);
            double d3 = 0.0d;
            if (d2 > 0.0d) {
                if (d2 >= 100.0d) {
                    d3 = 256.0d;
                } else {
                    int i = (int) d2;
                    C95314dV.A01(jArr);
                    double d4 = (double) jArr[i];
                    if (i == 99) {
                        d = 256.0d;
                    } else {
                        d = (double) jArr[i + 1];
                    }
                    d3 = d4 + ((d2 - ((double) i)) * (d - d4));
                }
            }
            long j4 = this.A02;
            max = Math.max((long) this.A00, Math.min(Math.round((d3 / 256.0d) * ((double) j4)), j4 - 1));
            j2 = this.A03;
        }
        C94324bc r1 = new C94324bc(A0X, j2 + max);
        return new C92684Xa(r1, r1);
    }

    @Override // X.AbstractC117075Yd
    public long AHD(long j) {
        long j2;
        double d;
        long j3 = j - this.A03;
        long[] jArr = this.A05;
        if (!C12960it.A1W(jArr) || j3 <= ((long) this.A00)) {
            return 0;
        }
        C95314dV.A01(jArr);
        double d2 = (((double) j3) * 256.0d) / ((double) this.A02);
        int A06 = AnonymousClass3JZ.A06(jArr, (long) d2, true);
        long j4 = this.A04;
        long j5 = (j4 * ((long) A06)) / 100;
        long j6 = jArr[A06];
        int i = A06 + 1;
        long j7 = (j4 * ((long) i)) / 100;
        if (A06 == 99) {
            j2 = 256;
        } else {
            j2 = jArr[i];
        }
        if (j6 == j2) {
            d = 0.0d;
        } else {
            d = (d2 - ((double) j6)) / ((double) (j2 - j6));
        }
        return j5 + Math.round(d * ((double) (j7 - j5)));
    }

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return C12960it.A1W(this.A05);
    }
}
