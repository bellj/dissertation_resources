package X;

/* renamed from: X.5HQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HQ extends UnsupportedOperationException {
    public final C78603pB zza;

    public AnonymousClass5HQ(C78603pB r1) {
        this.zza = r1;
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        return "Missing ".concat(String.valueOf(this.zza));
    }
}
