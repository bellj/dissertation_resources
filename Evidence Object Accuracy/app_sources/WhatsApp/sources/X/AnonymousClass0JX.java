package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0JX  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JX {
    /* Fake field, exist only in values array */
    svg,
    /* Fake field, exist only in values array */
    a,
    /* Fake field, exist only in values array */
    circle,
    /* Fake field, exist only in values array */
    clipPath,
    /* Fake field, exist only in values array */
    defs,
    desc,
    /* Fake field, exist only in values array */
    ellipse,
    /* Fake field, exist only in values array */
    g,
    /* Fake field, exist only in values array */
    image,
    /* Fake field, exist only in values array */
    line,
    /* Fake field, exist only in values array */
    linearGradient,
    /* Fake field, exist only in values array */
    marker,
    /* Fake field, exist only in values array */
    mask,
    /* Fake field, exist only in values array */
    path,
    /* Fake field, exist only in values array */
    pattern,
    /* Fake field, exist only in values array */
    polygon,
    /* Fake field, exist only in values array */
    polyline,
    /* Fake field, exist only in values array */
    radialGradient,
    /* Fake field, exist only in values array */
    rect,
    /* Fake field, exist only in values array */
    solidColor,
    /* Fake field, exist only in values array */
    stop,
    /* Fake field, exist only in values array */
    style,
    SWITCH,
    /* Fake field, exist only in values array */
    symbol,
    /* Fake field, exist only in values array */
    text,
    /* Fake field, exist only in values array */
    textPath,
    title,
    /* Fake field, exist only in values array */
    tref,
    /* Fake field, exist only in values array */
    tspan,
    /* Fake field, exist only in values array */
    use,
    /* Fake field, exist only in values array */
    view,
    UNSUPPORTED;
    
    public static final Map A00 = new HashMap();

    static {
        AnonymousClass0JX[] values = values();
        for (AnonymousClass0JX r2 : values) {
            if (r2 == SWITCH) {
                A00.put("switch", r2);
            } else if (r2 != UNSUPPORTED) {
                A00.put(r2.name(), r2);
            }
        }
    }
}
