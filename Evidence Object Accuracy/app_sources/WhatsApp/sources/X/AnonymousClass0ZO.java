package X;

import java.util.ArrayList;

/* renamed from: X.0ZO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZO implements AbstractC018608t {
    public final /* synthetic */ AnonymousClass0EH A00;
    public final /* synthetic */ Object A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ ArrayList A03;
    public final /* synthetic */ ArrayList A04;

    @Override // X.AbstractC018608t
    public void AXq(AnonymousClass072 r1) {
    }

    @Override // X.AbstractC018608t
    public void AXr(AnonymousClass072 r1) {
    }

    @Override // X.AbstractC018608t
    public void AXs(AnonymousClass072 r1) {
    }

    public AnonymousClass0ZO(AnonymousClass0EH r1, Object obj, Object obj2, ArrayList arrayList, ArrayList arrayList2) {
        this.A00 = r1;
        this.A01 = obj;
        this.A03 = arrayList;
        this.A02 = obj2;
        this.A04 = arrayList2;
    }

    @Override // X.AbstractC018608t
    public void AXt(AnonymousClass072 r5) {
        Object obj = this.A01;
        if (obj != null) {
            this.A00.A0H(obj, this.A03, null);
        }
        Object obj2 = this.A02;
        if (obj2 != null) {
            this.A00.A0H(obj2, this.A04, null);
        }
    }
}
