package X;

/* renamed from: X.5Yg  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Yg extends AnonymousClass2BW {
    @Override // X.AnonymousClass2BW
    long AYZ(AnonymousClass3H3 v);

    @Override // X.AnonymousClass2BW
    void close();

    @Override // X.AnonymousClass2BY
    int read(byte[] bArr, int i, int i2);
}
