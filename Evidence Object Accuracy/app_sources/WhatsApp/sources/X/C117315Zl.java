package X;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.IDxCreatorShape2S0000000_3_I1;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.5Zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C117315Zl {
    public static int A00(AbstractC30781Yu r2) {
        return (int) Math.log10((double) r2.A02);
    }

    public static int A01(Enum r0, int[] iArr) {
        return iArr[r0.ordinal()];
    }

    public static long A02(ActivityC13790kL r1) {
        return r1.A05.A00();
    }

    public static Parcelable A03(Bundle bundle, String str) {
        Parcelable parcelable = bundle.getParcelable(str);
        AnonymousClass009.A05(parcelable);
        return parcelable;
    }

    public static ViewGroup A04(View view, int i) {
        return (ViewGroup) AnonymousClass028.A0D(view, i);
    }

    public static AnonymousClass01T A05(Object obj, Object obj2) {
        return new AnonymousClass01T(obj, obj2);
    }

    public static AnonymousClass02A A06(AbstractC009404s r1, AbstractC001400p r2) {
        return new AnonymousClass02A(r1, r2);
    }

    public static IDxCreatorShape2S0000000_3_I1 A07(int i) {
        return new IDxCreatorShape2S0000000_3_I1(i);
    }

    public static AbstractC28901Pl A08(List list, int i) {
        return (AbstractC28901Pl) list.get(i);
    }

    public static AnonymousClass1IR A09(Iterator it) {
        return (AnonymousClass1IR) it.next();
    }

    public static C18600si A0A(AnonymousClass01J r0) {
        return (C18600si) r0.AEo.get();
    }

    public static C1310460z A0B(String str, ArrayList arrayList) {
        return new C1310460z(str, arrayList);
    }

    public static AnonymousClass6B7 A0C(AnonymousClass60T r1, String str, String str2) {
        return r1.A01(str, str2, true);
    }

    public static C30931Zj A0D(String str) {
        return C30931Zj.A00(str, "onboarding", "IN");
    }

    public static C128375w0 A0E(AnonymousClass01J r0) {
        return (C128375w0) r0.ADW.get();
    }

    public static C18590sh A0F(AnonymousClass01J r0) {
        return (C18590sh) r0.AES.get();
    }

    public static AnonymousClass1V8 A0G(AnonymousClass1W9[] r2) {
        return new AnonymousClass1V8("account", r2);
    }

    public static C253118x A0H(AnonymousClass01J r0) {
        return (C253118x) r0.AAW.get();
    }

    public static Object A0I(List list) {
        return list.get(1);
    }

    public static RuntimeException A0J(Throwable th) {
        return new RuntimeException(th);
    }

    public static String A0K(AnonymousClass1ZR r0) {
        Object obj = r0.A00;
        AnonymousClass009.A05(obj);
        return (String) obj;
    }

    public static JSONArray A0L() {
        return new JSONArray();
    }

    public static void A0M(Intent intent, Parcelable parcelable) {
        intent.putExtra("extra_bank_account", parcelable);
    }

    public static void A0N(TextView textView, Object obj) {
        textView.setText((CharSequence) obj);
    }

    public static void A0O(AnonymousClass01E r0) {
        ActivityC000900k A0B = r0.A0B();
        if (A0B != null) {
            A0B.finish();
        }
    }

    public static void A0P(AnonymousClass01E r1, AnonymousClass01E r2, PaymentBottomSheet paymentBottomSheet) {
        r1.A0X(r2, 0);
        paymentBottomSheet.A1L(r1);
    }

    public static void A0Q(AnonymousClass017 r1) {
        r1.A0B(Boolean.TRUE);
    }

    public static void A0R(C14900mE r0, C14580lf r1, AbstractC14590lg r2) {
        r1.A01(r2, r0.A06);
    }

    public static void A0S(AnonymousClass3FE r1) {
        r1.A00("on_failure");
    }

    public static void A0T(AnonymousClass3FE r1) {
        r1.A00("on_success");
    }

    public static void A0U(AnonymousClass1ZR r0, String str, JSONObject jSONObject) {
        jSONObject.put(str, r0.A00);
    }

    public static void A0V(C64513Fv r0, String str, StringBuilder sb) {
        sb.append(r0.A00(str));
    }

    public static void A0W(AnonymousClass6F2 r0, String str, JSONObject jSONObject) {
        jSONObject.put(str, r0.A07());
    }

    public static void A0X(C41141sy r1, String str, long j) {
        r1.A04(new AnonymousClass1W9(str, j));
    }

    public static void A0Y(Object obj, String str, JSONArray jSONArray, JSONObject jSONObject) {
        jSONArray.put(jSONObject.put(str, obj));
    }

    public static void A0Z(Object[] objArr, long j) {
        objArr[1] = Long.valueOf(j);
    }

    public static byte[] A0a(String str) {
        return str.getBytes(AnonymousClass01V.A08);
    }
}
