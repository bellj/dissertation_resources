package X;

/* renamed from: X.5wV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128685wV {
    public final long A00;
    public final long A01;

    public /* synthetic */ C128685wV(long j, long j2) {
        if (j2 == 0) {
            this.A01 = 0;
            this.A00 = 1;
            return;
        }
        this.A01 = j;
        this.A00 = j2;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01);
        A0h.append("/");
        A0h.append(this.A00);
        return A0h.toString();
    }
}
