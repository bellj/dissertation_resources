package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122355lK extends AbstractC118825cR {
    public ImageView A00;
    public TextView A01;

    public C122355lK(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.card_icon);
        this.A01 = C12960it.A0I(view, R.id.card_number);
    }
}
