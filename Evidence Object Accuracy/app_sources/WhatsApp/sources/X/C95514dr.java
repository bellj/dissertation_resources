package X;

/* renamed from: X.4dr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95514dr {
    public final int A00;
    public final AbstractC37731mu A01;
    public final Object A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;

    public C95514dr(AbstractC37731mu r3, boolean z) {
        this.A02 = null;
        this.A05 = false;
        this.A04 = z;
        this.A00 = 1;
        this.A03 = true;
        this.A01 = r3;
    }

    public C95514dr(Object obj, int i, boolean z, boolean z2, boolean z3) {
        this.A02 = obj;
        this.A05 = z;
        this.A04 = z2;
        this.A00 = i;
        this.A03 = z3;
        this.A01 = null;
    }

    public static C95514dr A00(Object obj) {
        return new C95514dr(obj, -1, false, false, true);
    }

    public static C95514dr A01(Object obj) {
        return new C95514dr(obj, -1, false, false, false);
    }

    public static C95514dr A02(Object obj) {
        return new C95514dr(obj, -1, true, true, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        if (r7 == 507) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C95514dr A03(java.lang.Object r6, int r7) {
        /*
            r0 = 400(0x190, float:5.6E-43)
            r2 = r7
            if (r7 < r0) goto L_0x0009
            r0 = 500(0x1f4, float:7.0E-43)
            if (r7 < r0) goto L_0x0012
        L_0x0009:
            r0 = 505(0x1f9, float:7.08E-43)
            if (r7 == r0) goto L_0x0012
            r0 = 507(0x1fb, float:7.1E-43)
            r5 = 1
            if (r7 != r0) goto L_0x0013
        L_0x0012:
            r5 = 0
        L_0x0013:
            r3 = 0
            r4 = 0
            r1 = r6
            X.4dr r0 = new X.4dr
            r0.<init>(r1, r2, r3, r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95514dr.A03(java.lang.Object, int):X.4dr");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        if (r7 == 507) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C95514dr A04(java.lang.Object r6, int r7, boolean r8) {
        /*
            r0 = 400(0x190, float:5.6E-43)
            r2 = r7
            if (r7 < r0) goto L_0x0009
            r0 = 500(0x1f4, float:7.0E-43)
            if (r7 < r0) goto L_0x0012
        L_0x0009:
            r0 = 505(0x1f9, float:7.08E-43)
            if (r7 == r0) goto L_0x0012
            r0 = 507(0x1fb, float:7.1E-43)
            r5 = 1
            if (r7 != r0) goto L_0x0013
        L_0x0012:
            r5 = 0
        L_0x0013:
            r3 = 0
            r4 = r8
            r1 = r6
            X.4dr r0 = new X.4dr
            r0.<init>(r1, r2, r3, r4, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95514dr.A04(java.lang.Object, int, boolean):X.4dr");
    }
}
