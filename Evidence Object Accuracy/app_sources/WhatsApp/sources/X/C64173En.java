package X;

import android.app.ProgressDialog;
import android.util.SparseArray;
import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3En  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64173En {
    public final SparseArray A00;
    public final AnonymousClass4MX A01 = new AnonymousClass4MX(new AnonymousClass4D4());
    public final C90194My A02;
    public final AnonymousClass1AF A03;
    public final String A04;
    public final WeakReference A05;
    public final WeakReference A06;
    public final WeakReference A07;
    public final Map A08;

    public C64173En(ActivityC000800j r3, AnonymousClass01F r4, AnonymousClass1AF r5, String str, Map map, boolean z) {
        this.A04 = str;
        this.A05 = C12970iu.A10(r3);
        this.A06 = C12970iu.A10(r4);
        this.A07 = C12970iu.A10(new ProgressDialog(r3));
        this.A08 = map;
        this.A02 = new C90194My(this, z);
        this.A03 = r5;
        this.A00 = new SparseArray();
    }

    public SparseArray A00() {
        C67923Tk r1;
        SparseArray sparseArray = new SparseArray();
        HashMap A11 = C12970iu.A11();
        String str = this.A04;
        if (str != null) {
            AnonymousClass1AF r5 = this.A03;
            synchronized (r5) {
                String str2 = r5.A01;
                boolean z = false;
                if (str2 != null && !str2.equals(str)) {
                    z = true;
                }
                r1 = r5.A00;
                if (r1 == null || z) {
                    r5.A01 = str;
                    r1 = new C67923Tk(new C63903Dj());
                    r5.A00 = r1;
                }
            }
            A11.put("gs", r1);
        }
        A11.put("ls", new C67913Tj());
        Map map = this.A08;
        if (map != null && !map.isEmpty()) {
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                A11.put(A15.getKey(), ((AbstractC17370qh) A15.getValue()).AAM());
            }
        }
        sparseArray.put(R.id.bk_context_key_data_modules, A11);
        sparseArray.put(R.id.bloks_host_activity, this.A05.get());
        sparseArray.put(R.id.bloks_host_progress_dialog, this.A07.get());
        sparseArray.put(R.id.bloks_host_fragment_manager, this.A06.get());
        return sparseArray;
    }

    public AnonymousClass4MX A01() {
        return this.A01;
    }
}
