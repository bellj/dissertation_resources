package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.io.File;

/* renamed from: X.00Y  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00Y {
    public static Drawable A00(Context context, int i) {
        return context.getDrawable(i);
    }

    public static File A01(Context context) {
        return context.getNoBackupFilesDir();
    }
}
