package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.0sZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18510sZ extends C18520sa {
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fa, code lost:
        if (r2.equals("reverse") != false) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011c, code lost:
        if (r2.equals("radial") != false) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x013e, code lost:
        if (r0 == false) goto L_0x00da;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0141  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass3C3 A00(X.AbstractC64543Fy r8, X.C14260l7 r9, X.AnonymousClass28D r10) {
        /*
        // Method dump skipped, instructions count: 490
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18510sZ.A00(X.3Fy, X.0l7, X.28D):X.3C3");
    }

    @Override // X.C18520sa
    public Drawable A01(C14260l7 r9, AnonymousClass28D r10, AnonymousClass28D r11) {
        AnonymousClass3C3 A00;
        EnumC03810Jf r4;
        int i;
        int i2 = r10.A01;
        if (i2 == 13761) {
            AnonymousClass2ZV r2 = new AnonymousClass2ZV();
            if (r10.A0O(41, false)) {
                A00 = A00(new C56012kB(), r9, r10);
            } else {
                A00 = A00(new C76483li(), r9, r10);
            }
            r2.A02(A00);
            return r2;
        } else if (i2 != 15775) {
            return super.A01(r9, r10, r11);
        } else {
            String A0J = r10.A0J(38, "primary");
            if ("circular".equals(A0J)) {
                return new AnonymousClass0A4(r9.A00, r9);
            }
            if ("elevated".equals(A0J)) {
                r4 = EnumC03810Jf.ELEVATED;
            } else if ("persistent".equals(A0J)) {
                r4 = EnumC03810Jf.PERSISTENT;
            } else {
                r4 = EnumC03810Jf.PRIMARY;
            }
            AnonymousClass28D A0F = r10.A0F(36);
            if (A0F != null) {
                i = AnonymousClass4Di.A00(r9, A0F);
            } else {
                i = 0;
            }
            int i3 = 0;
            for (String str : r10.A0M(35)) {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            i3 |= 8;
                            break;
                        } else {
                            break;
                        }
                    case 96673:
                        if (str.equals("all")) {
                            i3 |= 15;
                            break;
                        } else {
                            break;
                        }
                    case 115029:
                        if (str.equals("top")) {
                            i3 |= 1;
                            break;
                        } else {
                            break;
                        }
                    case 3317767:
                        if (str.equals("left")) {
                            i3 |= 2;
                            break;
                        } else {
                            break;
                        }
                    case 108511772:
                        if (str.equals("right")) {
                            i3 |= 4;
                            break;
                        } else {
                            break;
                        }
                }
            }
            return new AnonymousClass0A5(r9.A00, r4, r9, i, i3);
        }
    }
}
