package X;

import android.os.IInterface;
import android.os.Parcel;
import android.view.View;

/* renamed from: X.2lP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56682lP extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115685Sn A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC56682lP(AbstractC115685Sn r2) {
        super("com.google.android.gms.maps.internal.IInfoWindowAdapter");
        this.A00 = r2;
    }

    @Override // X.AbstractBinderC73293fz
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        View view;
        if (i == 1) {
            view = this.A00.ADT(new C36311jg(AbstractBinderC79903rO.A00(parcel.readStrongBinder())));
        } else if (i != 2) {
            return false;
        } else {
            new C36311jg(AbstractBinderC79903rO.A00(parcel.readStrongBinder()));
            view = null;
        }
        BinderC56502l7 r0 = new BinderC56502l7(view);
        parcel2.writeNoException();
        C65183In.A00(r0, parcel2);
        return true;
    }
}
