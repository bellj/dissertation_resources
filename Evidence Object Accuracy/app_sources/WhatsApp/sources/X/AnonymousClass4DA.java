package X;

import android.view.View;

/* renamed from: X.4DA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4DA {
    public static boolean A00(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i);
        int size2 = View.MeasureSpec.getSize(i);
        if (i == i2) {
            return true;
        }
        if (mode2 == 0 && mode == 0) {
            return true;
        }
        float f = (float) i3;
        if (mode == 1073741824 && Math.abs(((float) size) - f) < 0.5f) {
            return true;
        }
        if (mode == Integer.MIN_VALUE && mode2 == 0) {
            if (((float) size) >= f) {
                return true;
            }
            return false;
        } else if (mode2 != Integer.MIN_VALUE || mode != Integer.MIN_VALUE || size2 <= size || f > ((float) size)) {
            return false;
        } else {
            return true;
        }
    }
}
