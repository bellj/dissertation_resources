package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.AbstractList;
import org.npci.commonlibrary.NPCIFragment;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.5ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117795ag extends LinearLayout implements AbstractC136546My {
    public int A00;
    public int A01;
    public Button A02;
    public ImageView A03;
    public LinearLayout A04;
    public ProgressBar A05;
    public TextView A06;
    public Object A07;
    public String A08;
    public String A09 = "";
    public FormItemEditText A0A;
    public AbstractC136396Mj A0B;
    public boolean A0C = false;
    public boolean A0D = false;
    public boolean A0E;

    public C117795ag(Context context) {
        super(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, C125355r7.A01);
        if (obtainStyledAttributes != null) {
            this.A08 = obtainStyledAttributes.getString(2);
            this.A00 = obtainStyledAttributes.getInteger(1, 6);
            this.A0E = obtainStyledAttributes.getBoolean(0, false);
            obtainStyledAttributes.recycle();
        }
        LinearLayout.inflate(context, R.layout.npci_layout_form_item, this);
        this.A04 = (LinearLayout) findViewById(R.id.form_item_action_bar);
        this.A06 = C12960it.A0J(this, R.id.form_item_title);
        this.A0A = (FormItemEditText) findViewById(R.id.form_item_input);
        this.A02 = (Button) findViewById(R.id.form_item_button);
        this.A05 = (ProgressBar) findViewById(R.id.form_item_progress);
        this.A03 = C12970iu.A0L(this, R.id.form_item_image);
        this.A0A.setInputType(0);
        setTitle(this.A08);
        setInputLength(this.A00);
        this.A0A.addTextChangedListener(new C123835o1(this));
        this.A0A.setOnTouchListener(new View.OnTouchListener() { // from class: X.64x
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                C117795ag r2 = C117795ag.this;
                view.performClick();
                if (r2.A0B == null || motionEvent.getAction() != 1) {
                    return false;
                }
                r2.A0B.AQl(r2.A01);
                return false;
            }
        });
        setActionBarPositionTop(this.A0E);
    }

    public static C117795ag A00(AbstractList abstractList, NPCIFragment nPCIFragment) {
        C117795ag r2 = (C117795ag) abstractList.get(nPCIFragment.A00);
        AnonymousClass0QQ A01 = r2.A01(r2.A05, false);
        A01.A08(new AccelerateDecelerateInterpolator());
        A01.A01();
        C117795ag r4 = (C117795ag) abstractList.get(nPCIFragment.A00);
        if (!TextUtils.isEmpty("")) {
            r4.A02.setText("");
        }
        Button button = r4.A02;
        r4.A01(button, false);
        button.setEnabled(false);
        button.setOnClickListener(null);
        return (C117795ag) abstractList.get(nPCIFragment.A00);
    }

    public final AnonymousClass0QQ A01(View view, boolean z) {
        AnonymousClass0QQ A0F = AnonymousClass028.A0F(view);
        float f = 0.0f;
        float f2 = 1.0f;
        float f3 = 0.0f;
        if (z) {
            f3 = 1.0f;
        }
        A0F.A04(f3);
        if (z) {
            f = 1.0f;
        }
        A0F.A03(f);
        A0F.A08(new AccelerateInterpolator());
        A0F.A09(new C117865aq(this, z));
        if (!z) {
            f2 = 0.5f;
        }
        A0F.A02(f2);
        return A0F;
    }

    @Override // X.AbstractC136546My
    public boolean AA5() {
        this.A0A.requestFocus();
        return true;
    }

    @Override // X.AbstractC136546My
    public void Aez(Drawable drawable, View.OnClickListener onClickListener, String str, int i, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str)) {
            this.A02.setText(str);
        }
        Button button = this.A02;
        button.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        button.setOnClickListener(onClickListener);
        button.setEnabled(z2);
        A01(button, z);
    }

    @Override // X.AbstractC136546My
    public boolean Af0() {
        if (!this.A0D) {
            this.A0D = true;
            setText(this.A09);
        } else {
            this.A0D = false;
            this.A0A.setText(this.A09.replaceAll(".", "●"));
        }
        return this.A0D;
    }

    @Override // X.AbstractC136546My
    public Object getFormDataTag() {
        return this.A07;
    }

    public FormItemEditText getFormInputView() {
        return this.A0A;
    }

    public AbstractC136396Mj getFormItemListener() {
        return this.A0B;
    }

    public int getInputLength() {
        return this.A00;
    }

    @Override // X.AbstractC136546My
    public String getInputValue() {
        if (this.A0C || this.A0D) {
            return C12970iu.A0p(this.A0A);
        }
        return this.A09;
    }

    public void setActionBarPositionTop(boolean z) {
        LinearLayout linearLayout = this.A04;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) linearLayout.getLayoutParams();
        if (z) {
            layoutParams.addRule(10);
            layoutParams.addRule(8, 0);
        } else {
            layoutParams.addRule(10, 0);
            layoutParams.addRule(8, R.id.form_item_input);
        }
        linearLayout.setLayoutParams(layoutParams);
    }

    public void setFormDataTag(Object obj) {
        this.A07 = obj;
    }

    public void setFormItemListener(AbstractC136396Mj r1) {
        this.A0B = r1;
    }

    public void setFormItemTag(int i) {
        this.A01 = i;
    }

    public void setInputLength(int i) {
        this.A0A.setMaxLength(i);
        this.A00 = i;
    }

    public void setText(String str) {
        FormItemEditText formItemEditText = this.A0A;
        formItemEditText.setText(str);
        formItemEditText.setSelection(str.length());
    }

    public void setTitle(String str) {
        this.A06.setText(str);
        this.A08 = str;
    }
}
