package X;

import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.29S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29S implements AnonymousClass1OI {
    public final /* synthetic */ EncBackupViewModel A00;

    public AnonymousClass29S(EncBackupViewModel encBackupViewModel) {
        this.A00 = encBackupViewModel;
    }

    @Override // X.AnonymousClass1OI
    public void APs(String str, int i, int i2, int i3, int i4) {
        AnonymousClass016 r2;
        int i5;
        int i6;
        EncBackupViewModel encBackupViewModel = this.A00;
        if (i == 0) {
            Log.i("EncBackupViewModel/successfully retrieved and saved backup key");
            encBackupViewModel.A04.A0A(3);
            r2 = encBackupViewModel.A07;
            i6 = -1;
        } else if (i == 404) {
            Log.i("EncBackupViewModel/account not found");
            r2 = encBackupViewModel.A04;
            i6 = 7;
        } else if (i == 8) {
            Log.i("EncBackupViewModel/invalid password");
            encBackupViewModel.A06.A0A(Integer.valueOf(i3));
            if (i4 > 0) {
                long j = ((long) i4) * 1000;
                encBackupViewModel.A0A(4);
                encBackupViewModel.A08.A0A(Long.valueOf(j));
                CountDownTimerC51912Zq r0 = new CountDownTimerC51912Zq(encBackupViewModel, j);
                encBackupViewModel.A00 = r0;
                r0.start();
            }
            r2 = encBackupViewModel.A04;
            i6 = 5;
        } else if (i == 408) {
            Log.i("EncBackupViewModel/request timeout");
            try {
                long parseInt = ((long) Integer.parseInt(str)) * 1000;
                encBackupViewModel.A0A(4);
                encBackupViewModel.A08.A0A(Long.valueOf(parseInt));
                CountDownTimerC51912Zq r02 = new CountDownTimerC51912Zq(encBackupViewModel, parseInt);
                encBackupViewModel.A00 = r02;
                r02.start();
                encBackupViewModel.A04.A0A(6);
                return;
            } catch (NumberFormatException unused) {
                Log.e("EncBackupViewModel/request timeout returned from server without timeout value");
                encBackupViewModel.A04.A0A(4);
                return;
            }
        } else {
            if (i == 3) {
                Log.e("EncBackupViewModel/failed to retrieve and save backup key due to a connection error");
                r2 = encBackupViewModel.A04;
                i5 = 8;
            } else {
                Log.e("EncBackupViewModel/failed to retrieve and save backup key due to a server error");
                r2 = encBackupViewModel.A04;
                i5 = 4;
            }
            r2.A0A(i5);
        }
        i5 = Integer.valueOf(i6);
        r2.A0A(i5);
    }

    @Override // X.AnonymousClass1OI
    public void AWu() {
        EncBackupViewModel encBackupViewModel = this.A00;
        Log.i("EncBackupViewModel/successfully retrieved and saved backup key");
        encBackupViewModel.A04.A0A(3);
        encBackupViewModel.A07.A0A(-1);
    }
}
