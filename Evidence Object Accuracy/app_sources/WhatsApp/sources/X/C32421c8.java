package X;

/* renamed from: X.1c8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32421c8 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;

    public C32421c8() {
        super(2312, new AnonymousClass00E(1, 10, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(4, this.A03);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamChatAction {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatActionChatType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatActionEntryPoint", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatActionMuteDuration", this.A03);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatActionType", obj3);
        sb.append("}");
        return sb.toString();
    }
}
