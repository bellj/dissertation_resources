package X;

import android.text.Editable;
import android.widget.EditText;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.calling.callrating.CallRatingActivity;

/* renamed from: X.35v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622635v extends AnonymousClass367 {
    public final /* synthetic */ CallRatingActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C622635v(EditText editText, CallRatingActivity callRatingActivity, AnonymousClass01d r13, AnonymousClass018 r14, AnonymousClass19M r15, C16630pM r16) {
        super(editText, null, r13, r14, r15, r16, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 0, false);
        this.A00 = callRatingActivity;
    }

    @Override // X.AnonymousClass367, X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        super.afterTextChanged(editable);
        this.A00.A22();
    }
}
