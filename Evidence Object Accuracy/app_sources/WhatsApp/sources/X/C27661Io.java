package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.util.Log;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

/* renamed from: X.1Io */
/* loaded from: classes2.dex */
public final class C27661Io {
    public int A00;
    public C65133Ig A01;
    public C63963Dq A02;
    public AnonymousClass4VP A03;
    public AnonymousClass4O6 A04;
    public String A05;
    public String A06;
    public boolean A07;
    public final AbstractC22060yS A08 = new AnonymousClass52N(this);
    public final C16210od A09;
    public final AnonymousClass18J A0A;
    public final AnonymousClass18K A0B;
    public final C14900mE A0C;
    public final AnonymousClass17M A0D;
    public final C19630uQ A0E;
    public final AbstractC17760rL A0F;
    public final C89684Kx A0G = new C89684Kx(this);
    public final AnonymousClass17O A0H;
    public final C17120qI A0I;
    public final String A0J;
    public final String A0K;
    public final String A0L;
    public final String A0M;

    public C27661Io(C16210od r3, AnonymousClass18J r4, AnonymousClass18K r5, C14900mE r6, AnonymousClass17M r7, C19630uQ r8, AbstractC17760rL r9, AnonymousClass17O r10, C17120qI r11, String str, String str2, String str3) {
        C16700pc.A0E(r6, 1);
        C16700pc.A0E(r7, 2);
        C16700pc.A0E(r11, 4);
        C16700pc.A0E(r4, 5);
        C16700pc.A0E(r3, 6);
        C16700pc.A0E(r8, 7);
        C16700pc.A0E(r5, 8);
        C16700pc.A0E(str3, 11);
        this.A0C = r6;
        this.A0D = r7;
        this.A0F = r9;
        this.A0I = r11;
        this.A0A = r4;
        this.A09 = r3;
        this.A0E = r8;
        this.A0B = r5;
        this.A0J = str;
        this.A0M = str2;
        this.A0K = str3;
        this.A0H = r10;
        UUID randomUUID = UUID.randomUUID();
        C16700pc.A0B(randomUUID);
        this.A0L = C16700pc.A08("FCS_STATE_MACHINE", randomUUID);
    }

    public static final /* synthetic */ void A00(C27661Io r1, String str) {
        C19630uQ r0 = r1.A0E;
        r0.A02.A03(r1.A00, str);
    }

    public static final /* synthetic */ void A01(C27661Io r2, String str, Map map, short s) {
        C19630uQ r0 = r2.A0E;
        r0.A02.A05(r2.A00, s);
        if (str == null) {
            str = r2.A06;
            if (str == null) {
                r2.A06(map);
                return;
            }
            r2.A06 = null;
        }
        r2.A05(str, map, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:84:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(X.AbstractC17770rM r21, X.AnonymousClass46T r22, java.util.Map r23) {
        /*
        // Method dump skipped, instructions count: 704
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27661Io.A02(X.0rM, X.46T, java.util.Map):void");
    }

    public final void A03(AnonymousClass4VP r4, Map map) {
        this.A03 = r4;
        int hashCode = r4.hashCode();
        this.A00 = hashCode;
        C19630uQ r0 = this.A0E;
        String str = this.A0J;
        r0.A00(hashCode, str);
        A04("state_name", r4.A01);
        A04("config_name", str);
        C16700pc.A0B(new JSONObject().put("input", map));
    }

    public final void A04(String str, String str2) {
        C19630uQ r3 = this.A0E;
        r3.A02(this.A00, str, str2);
        r3.A02(this.A00, "session_id", this.A0M);
    }

    public final void A05(String str, Map map, Map map2) {
        Map map3 = map2;
        AnonymousClass4VP r6 = (AnonymousClass4VP) this.A0H.A00.get(str);
        if (r6 == null) {
            throw new AssertionError(C16700pc.A08("state not found for name: ", str));
        } else if (r6 instanceof AnonymousClass46R) {
            A03(r6, map);
            r6.A00(map, new AnonymousClass5KS(this));
        } else if (r6 instanceof AnonymousClass46T) {
            AnonymousClass3CA r2 = new AnonymousClass3CA();
            AnonymousClass46T r62 = (AnonymousClass46T) r6;
            String str2 = r62.A01;
            C16700pc.A0E(str2, 2);
            if (map != null) {
                if (map2 != null && "merge".equals(str2)) {
                    map = r2.A00(map2, map);
                }
                map3 = map;
            } else if (map2 == null) {
                map3 = C71373cp.A00;
            }
            AbstractC17760rL r0 = this.A0F;
            String str3 = r62.A03;
            AbstractC17770rM AGE = r0.AGE(str3);
            if (AGE == null) {
                Log.e(C16700pc.A08("Resource not found: ", str3));
                A06(null);
                return;
            }
            if (AGE instanceof AbstractC19730ua) {
                C65133Ig r02 = this.A01;
                if (r02 == null) {
                    C16700pc.A0K("backNavManager");
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                } else if (r02.A03().size() < 1 || !this.A07) {
                    this.A07 = true;
                } else {
                    C63963Dq r03 = this.A02;
                    if (r03 == null) {
                        C16700pc.A0K("fcsLoadingEventManager");
                        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                    }
                    r03.A00.A02(r03.A01).A01(new AnonymousClass6ED("onStartLoading", this.A05));
                    ((AbstractC19730ua) AGE).AZR(new AnonymousClass3D5(AGE, this, r62, str, map3), r62.A01(map3), this.A00);
                    return;
                }
            }
            A03(r62, map3);
            A02(AGE, r62, map3);
        } else if ((r6 instanceof AnonymousClass46Q) || (r6 instanceof AnonymousClass46S)) {
            A03(r6, map);
            r6.A00(map, new AnonymousClass5KT(this));
        } else {
            throw new IllegalArgumentException("Unsupported Type");
        }
    }

    public final void A06(Map map) {
        this.A0F.AWR();
        C65133Ig r0 = this.A01;
        if (r0 == null) {
            C16700pc.A0K("backNavManager");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        C64373Fh r1 = r0.A06;
        r1.A00.removeAllElements();
        r1.A01.removeAllElements();
        C65133Ig r02 = this.A01;
        if (r02 == null) {
            C16700pc.A0K("backNavManager");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        r02.A07.A01(new AnonymousClass6E7(AnonymousClass1WF.A00));
        C65133Ig r12 = this.A01;
        if (r12 == null) {
            C16700pc.A0K("backNavManager");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        r12.A07.A03(r12);
        C17120qI r2 = r12.A08;
        String str = r12.A09;
        synchronized (r2) {
            C17120qI.A02.remove(str);
        }
        this.A0C.A0I(new RunnableBRunnable0Shape9S0100000_I0_9(this, 20));
        this.A03 = null;
        AnonymousClass4O6 r03 = this.A04;
        if (r03 != null) {
            AnonymousClass17Q r4 = r03.A01;
            AnonymousClass5UZ r3 = r03.A00;
            C16700pc.A0E(r4, 0);
            C19750uc r22 = r4.A09;
            String str2 = r4.A0B;
            synchronized (r22) {
                C16700pc.A0E(str2, 0);
                r22.A00.remove(str2);
            }
            r4.A08.A00.clear();
            if (r3 != null) {
                r3.AQd(map);
            }
        }
        this.A04 = null;
    }
}
