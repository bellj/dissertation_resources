package X;

import java.util.List;

/* renamed from: X.6FI  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6FI implements Runnable {
    public final /* synthetic */ AnonymousClass61Q A00;

    public AnonymousClass6FI(AnonymousClass61Q r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A00.A0L.A00;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((C128425w5) list.get(i)).A00();
        }
    }
}
