package X;

import android.content.Context;
import android.view.ViewGroup;

/* renamed from: X.34h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620634h extends AbstractC621034l {
    public boolean A00;

    public C620634h(Context context) {
        super(context);
        A01();
        setLayoutParams(new ViewGroup.MarginLayoutParams(-1, -2));
    }

    @Override // X.AbstractC74153hP
    public void A01() {
        if (!this.A00) {
            this.A00 = true;
            ((AbstractC621034l) this).A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }
}
