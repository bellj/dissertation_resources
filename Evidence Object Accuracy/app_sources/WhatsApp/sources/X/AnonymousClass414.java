package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.414  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass414 extends AnonymousClass4EB {
    public final int A00;
    public final UserJid A01;
    public final String A02;
    public final String A03;

    public AnonymousClass414(UserJid userJid, String str, String str2, int i) {
        C16700pc.A0E(userJid, 3);
        this.A02 = str;
        this.A03 = str2;
        this.A01 = userJid;
        this.A00 = i;
    }
}
