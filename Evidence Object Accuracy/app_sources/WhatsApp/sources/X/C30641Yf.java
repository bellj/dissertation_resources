package X;

/* renamed from: X.1Yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30641Yf {
    public final AbstractC15590nW A00;

    public C30641Yf(AbstractC15590nW r1) {
        this.A00 = r1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AxolotlSenderKeyEvent{groupJid='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
