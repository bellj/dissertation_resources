package X;

import android.util.Pair;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* renamed from: X.3HQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HQ {
    public static List A02;
    public final List A00 = C12960it.A0l();
    public final List A01 = C12960it.A0l();

    static {
        Integer[] numArr = new Integer[5];
        C12960it.A1O(numArr, 128105);
        C12980iv.A1T(numArr, 128104);
        C12990iw.A1V(numArr, 129489);
        numArr[3] = 129777;
        numArr[4] = 129778;
        A02 = Arrays.asList(numArr);
    }

    public AnonymousClass3HQ(int[] iArr) {
        ArrayList arrayList = null;
        char c = 0;
        int i = 0;
        for (int i2 : iArr) {
            if (A02.contains(Integer.valueOf(i2))) {
                if (arrayList != null) {
                    this.A00.add(arrayList);
                }
                i++;
                this.A01.add(C12960it.A0D(Integer.valueOf(i2), -1));
                arrayList = C12960it.A0l();
                c = 1;
            } else {
                int[] iArr2 = AnonymousClass3JU.A05;
                int length = iArr2.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (iArr2[i3] != i2) {
                        i3++;
                    } else if (c == 1) {
                        List list = this.A01;
                        int i4 = i - 1;
                        list.set(i4, C12960it.A0D(C12990iw.A0k(list, i4), i2));
                        c = 2;
                    }
                }
                if (i2 == 8205) {
                    AnonymousClass009.A05(arrayList);
                    C12980iv.A1R(arrayList, i2);
                    c = 3;
                } else if (c == 3) {
                    AnonymousClass009.A05(arrayList);
                    C12980iv.A1R(arrayList, i2);
                }
            }
        }
    }

    public AnonymousClass3HQ A00(int i, int i2) {
        int i3 = i - 1;
        List list = this.A01;
        if (i3 >= list.size()) {
            StringBuilder A0k = C12960it.A0k("MultiSkinToneEmoji/createSkinTonedEmoji/error/person index ");
            A0k.append(i3);
            A0k.append(" is bigger than the total length of sequence ");
            Log.e(C12960it.A0f(A0k, list.size()));
            return this;
        }
        AnonymousClass3HQ r2 = new AnonymousClass3HQ(A02());
        List list2 = r2.A01;
        if (list2.size() > i3) {
            list2.set(i3, C12960it.A0D(C12990iw.A0k(list2, i3), i2));
        }
        return r2;
    }

    public List A01() {
        List<Pair> list = this.A01;
        ArrayList A0y = C12980iv.A0y(list);
        for (Pair pair : list) {
            int A05 = C12960it.A05(pair.second);
            if (A05 > 0) {
                A0y.add(Integer.toString(A05));
            }
        }
        return A0y;
    }

    public int[] A02() {
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        while (true) {
            List list = this.A01;
            if (i >= list.size()) {
                return AnonymousClass4Db.A00(A0l);
            }
            A0l.add(C12990iw.A0k(list, i));
            if (!(list.size() <= i || list.get(i) == null || C12960it.A05(((Pair) list.get(i)).second) == -1)) {
                A0l.add(((Pair) list.get(i)).second);
            }
            List list2 = this.A00;
            if (list2.size() > i && list2.get(i) != null) {
                A0l.addAll((Collection) list2.get(i));
            }
            i++;
        }
    }
}
