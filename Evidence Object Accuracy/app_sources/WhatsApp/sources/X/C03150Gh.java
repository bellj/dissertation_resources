package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* renamed from: X.0Gh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03150Gh extends AnonymousClass0Gj {
    public static final String A00 = C06390Tk.A01("BatteryNotLowTracker");

    public C03150Gh(Context context, AbstractC11500gO r2) {
        super(context, r2);
    }

    @Override // X.AbstractC06170Sl
    public /* bridge */ /* synthetic */ Object A00() {
        Intent registerReceiver = this.A01.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver == null) {
            C06390Tk.A00().A03(A00, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        float intExtra2 = ((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
        if (intExtra == 1 || intExtra2 > 0.15f) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    @Override // X.AnonymousClass0Gj
    public IntentFilter A05() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }

    @Override // X.AnonymousClass0Gj
    public void A06(Context context, Intent intent) {
        Boolean bool;
        if (intent.getAction() != null) {
            C06390Tk.A00().A02(A00, String.format("Received %s", intent.getAction()), new Throwable[0]);
            String action = intent.getAction();
            if (action.equals("android.intent.action.BATTERY_OKAY")) {
                bool = Boolean.TRUE;
            } else if (action.equals("android.intent.action.BATTERY_LOW")) {
                bool = Boolean.FALSE;
            } else {
                return;
            }
            A04(bool);
        }
    }
}
