package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;

/* renamed from: X.21W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21W extends AnonymousClass02M {
    public boolean A00 = false;
    public Bitmap[] A01;
    public boolean[] A02;
    public boolean[] A03;
    public final float A04;
    public final float A05;
    public final Context A06;
    public final Drawable A07;
    public final AnonymousClass018 A08;
    public final AnonymousClass1BI A09;
    public final AnonymousClass21U A0A;

    public AnonymousClass21W(Context context, AnonymousClass018 r5, AnonymousClass1BI r6, AnonymousClass21U r7) {
        this.A06 = context;
        this.A09 = r6;
        this.A08 = r5;
        this.A0A = r7;
        this.A04 = context.getResources().getDimension(R.dimen.filter_selected_thumb_height) / ((float) r7.A0I);
        this.A05 = context.getResources().getDimension(R.dimen.filter_selected_thumb_width) / ((float) r7.A0J);
        this.A07 = new ColorDrawable(AnonymousClass00T.A00(context, R.color.camera_thumb));
        int size = C470128o.A00.size() - 1;
        this.A01 = new Bitmap[size];
        this.A03 = new boolean[size];
        this.A02 = new boolean[size];
        A0E(0);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return C470128o.A00.size();
    }

    public void A0E(int i) {
        this.A00 = true;
        int i2 = 0;
        while (true) {
            boolean[] zArr = this.A02;
            if (i2 < zArr.length) {
                if (!zArr[i2]) {
                    A03(i2);
                }
                i2++;
            } else {
                this.A09.A0F(new C454521q(this), i);
                return;
            }
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r17, int i) {
        float f;
        boolean[] zArr;
        AnonymousClass018 r7;
        Bitmap[] bitmapArr;
        boolean[] zArr2;
        View$OnClickListenerC55222hz r10 = (View$OnClickListenerC55222hz) r17;
        AnonymousClass21U r9 = this.A0A;
        boolean z = false;
        if (i == r9.A01) {
            z = true;
        }
        boolean booleanValue = Boolean.valueOf(z).booleanValue();
        float f2 = 1.0f;
        if (booleanValue) {
            f = this.A05;
            f2 = this.A04;
        } else {
            f = 1.0f;
        }
        r10.A04.A04(booleanValue, false);
        View view = r10.A00;
        view.setScaleX(f);
        view.setScaleY(f2);
        view.setPivotX(((float) r9.A0J) / 2.0f);
        view.setPivotY((float) r9.A0I);
        Context context = this.A06;
        String string = context.getString(C470128o.A00(i).A01);
        TextView textView = r10.A03;
        textView.setText(string);
        ImageView imageView = r10.A02;
        imageView.setBackground(this.A07);
        imageView.setImageDrawable(null);
        View view2 = r10.A01;
        view2.setOnClickListener(r10);
        if (r9.A02 != null) {
            if (i == 0) {
                r7 = this.A08;
                bitmapArr = this.A01;
                zArr2 = this.A03;
                zArr = this.A02;
            } else {
                zArr = this.A02;
                if (zArr[i - 1]) {
                    r7 = this.A08;
                    bitmapArr = this.A01;
                    zArr2 = this.A03;
                } else if (this.A00) {
                    textView.setText(R.string.filter_loading);
                    imageView.setBackgroundColor(AnonymousClass00T.A00(context, R.color.filter_loading_background_color));
                    view2.setOnClickListener(null);
                    imageView.setClickable(false);
                    return;
                } else {
                    textView.setText(R.string.filter_error);
                    imageView.setClickable(true);
                    view2.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 46));
                    return;
                }
            }
            ((AbstractC16350or) new AnonymousClass38B(context, r7, this.A09, r9, r10, this, bitmapArr, zArr2, zArr, i)).A02.executeOnExecutor(r9.A0W, new Void[0]);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new View$OnClickListenerC55222hz(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_thumb_item, viewGroup, false), this);
    }
}
