package X;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/* renamed from: X.2TS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TS {
    public int A00;
    public int A01 = 1;
    public int A02 = 5;
    public AbstractC14640lm A03 = null;
    public AnonymousClass1IS A04 = null;
    public boolean A05 = false;
    public boolean A06 = false;
    public boolean A07 = false;
    public final Context A08;

    public AnonymousClass2TS(Context context) {
        this.A08 = context;
    }

    public Intent A00() {
        Intent intent = new Intent();
        intent.setClassName(this.A08.getPackageName(), "com.whatsapp.mediaview.MediaViewActivity");
        intent.putExtra("start_t", SystemClock.uptimeMillis());
        intent.putExtra("video_play_origin", this.A02);
        intent.putExtra("nogallery", this.A07);
        intent.putExtra("gallery", this.A05);
        intent.putExtra("menu_style", this.A01);
        intent.putExtra("menu_set_wallpaper", this.A06);
        intent.putExtra("origin", this.A00);
        AnonymousClass1IS r0 = this.A04;
        if (r0 != null) {
            C38211ni.A00(intent, r0);
        }
        AbstractC14640lm r02 = this.A03;
        if (r02 != null) {
            intent.putExtra("jid", r02.getRawString());
        }
        return intent;
    }
}
