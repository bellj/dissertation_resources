package X;

import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;

/* renamed from: X.240  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass240 {
    public AnonymousClass4QC A00;
    public AbstractC15290ms A01;
    public AbstractC116245Ur A02;
    public AnonymousClass241 A03;
    public final AnonymousClass1AC A04;
    public final AnonymousClass1AB A05;
    public final C235512c A06;
    public final AbstractC116245Ur A07 = new C69853aL(this);
    public final C255519v A08;
    public final AnonymousClass1BQ A09;
    public final C255619w A0A;
    public final AbstractView$OnClickListenerC34281fs A0B = new ViewOnClickCListenerShape14S0100000_I0_1(this, 26);

    public AnonymousClass240(AnonymousClass1AC r3, AnonymousClass1AB r4, C235512c r5, C255519v r6, AnonymousClass1BQ r7, C255619w r8) {
        this.A06 = r5;
        this.A05 = r4;
        this.A0A = r8;
        this.A04 = r3;
        this.A09 = r7;
        this.A08 = r6;
    }
}
