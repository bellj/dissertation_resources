package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.4SB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SB {
    public final TextView A00;
    public final TextView A01;
    public final TextView A02;
    public final AnonymousClass018 A03;

    public AnonymousClass4SB(View view, AnonymousClass018 r4) {
        this.A03 = r4;
        C12970iu.A0K(view, R.id.voice_cancel_animation).setImageResource(R.drawable.recording_mic_red);
        C12970iu.A0K(view, R.id.voice_cancel_trashcan_lid).setImageResource(R.drawable.rec_bucket_lid);
        C12970iu.A0K(view, R.id.voice_cancel_trashcan_body).setImageResource(R.drawable.rec_bucket_body);
        this.A00 = C12960it.A0I(view, R.id.voice_note_info);
        this.A01 = C12960it.A0I(view, R.id.voice_note_info_v2);
        this.A02 = C12960it.A0I(view, R.id.voice_note_tip);
    }
}
