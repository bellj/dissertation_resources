package X;

/* renamed from: X.20X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20X {
    public final short A00;
    public final short A01;
    public final boolean A02;

    public AnonymousClass20X(int i, int i2, boolean z) {
        this.A02 = z;
        this.A01 = (short) i;
        this.A00 = (short) i2;
    }
}
