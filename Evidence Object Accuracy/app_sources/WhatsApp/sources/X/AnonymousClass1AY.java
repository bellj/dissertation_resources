package X;

import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1AY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AY {
    public final C15550nR A00;
    public final C19990v2 A01;
    public final C14850m9 A02;
    public final List A03 = new ArrayList();
    public final Map A04 = new HashMap();

    public AnonymousClass1AY(C15550nR r2, C19990v2 r3, C14850m9 r4) {
        this.A02 = r4;
        this.A01 = r3;
        this.A00 = r2;
    }

    public synchronized AbstractC33171dZ A00(C49662Lr r3) {
        Map map;
        map = this.A04;
        if (map.isEmpty()) {
            A02();
        }
        return (AbstractC33171dZ) map.get(Integer.valueOf(r3.A01));
    }

    public synchronized List A01() {
        List list;
        list = this.A03;
        if (list.isEmpty()) {
            A02();
        }
        return list;
    }

    public final void A02() {
        List list = this.A03;
        list.clear();
        list.add(new C49662Lr(0, R.id.search_contact_filter, R.string.filter_contacts, R.drawable.smart_filter_contacts));
        list.add(new C49662Lr(1, R.id.search_non_contact_filter, R.string.filter_non_contacts, R.drawable.smart_filter_non_contacts));
        C14850m9 r5 = this.A02;
        if (!r5.A07(1608)) {
            list.add(new C49662Lr(2, R.id.search_unread_filter, R.string.filter_unread, R.drawable.smart_filter_unread));
        }
        Map map = this.A04;
        map.clear();
        C15550nR r2 = this.A00;
        map.put(0, new C1103055a(r2));
        map.put(1, new C1103155b(r2));
        map.put(2, new C1103255c(this.A01, r5));
    }
}
