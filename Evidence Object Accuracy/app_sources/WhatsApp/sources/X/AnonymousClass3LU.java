package X;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.3LU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LU implements LocationListener {
    public Location A00;
    public View A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public TextView A05;
    public CircularProgressBar A06;
    public ActivityC13790kL A07;
    public C48122Ek A08;
    public Double A09;
    public Double A0A;
    public Float A0B = Float.valueOf(16.0f);
    public String A0C;
    public boolean A0D = false;
    public boolean A0E = false;
    public boolean A0F = false;
    public boolean A0G = false;
    public final C244615p A0H;
    public final C15570nT A0I;
    public final AnonymousClass1B0 A0J;
    public final AnonymousClass01d A0K;
    public final WhatsAppLibLoader A0L;
    public final /* synthetic */ DirectorySetLocationMapActivity A0M;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AnonymousClass3LU(C244615p r2, C15570nT r3, AnonymousClass1B0 r4, DirectorySetLocationMapActivity directorySetLocationMapActivity, AnonymousClass01d r6, WhatsAppLibLoader whatsAppLibLoader) {
        this.A0M = directorySetLocationMapActivity;
        this.A0I = r3;
        this.A0K = r6;
        this.A0L = whatsAppLibLoader;
        this.A0H = r2;
        this.A0J = r4;
    }

    public void A00() {
        LocationManager A0F = this.A0K.A0F();
        if (A0F != null && !A0F.isProviderEnabled("gps") && !A0F.isProviderEnabled("network")) {
            C36021jC.A01(this.A07, 2);
        }
    }

    public void A01(C89324Jn r7) {
        View A0N = C12980iv.A0N(this.A07, R.layout.permissions_request);
        TextView A0I = C12960it.A0I(A0N, R.id.permission_message);
        ImageView A0K = C12970iu.A0K(A0N, R.id.permission_image_1);
        View A0D = AnonymousClass028.A0D(A0N, R.id.submit);
        View A0D2 = AnonymousClass028.A0D(A0N, R.id.cancel);
        A0I.setText(R.string.permission_location_info_on_searching_businesses);
        A0K.setImageResource(R.drawable.permission_location);
        C004802e A0S = C12980iv.A0S(this.A07);
        A0S.setView(A0N);
        A0S.A0B(true);
        AnonymousClass04S create = A0S.create();
        if (create.getWindow() != null) {
            create.getWindow().setBackgroundDrawable(C12980iv.A0L(this.A07, R.color.transparent));
        }
        C12990iw.A1C(A0D, this, r7, create, 6);
        C12960it.A0z(A0D2, create, 13);
        create.show();
        this.A0E = true;
        C12960it.A0t(this.A0J.A02.A00().edit(), "DIRECTORY_LOCATION_INFO_SHOWN", true);
    }

    public void A02(String str) {
        this.A0C = str;
        if (!TextUtils.isEmpty(str)) {
            this.A05.setText(str);
            C12960it.A0s(this.A07, this.A05, R.color.list_item_title);
        }
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (this.A00 == null) {
                DirectorySetLocationMapActivity directorySetLocationMapActivity = this.A0M;
                if (directorySetLocationMapActivity.A01 != null && this.A09 == null && this.A0A == null) {
                    directorySetLocationMapActivity.A0B.setLocationMode(1);
                    directorySetLocationMapActivity.A01.A0A(C65193Io.A01(AnonymousClass1U5.A01(location)));
                }
            }
            DirectorySetLocationMapActivity directorySetLocationMapActivity2 = this.A0M;
            if (directorySetLocationMapActivity2.A08.A0F && directorySetLocationMapActivity2.A01 != null) {
                directorySetLocationMapActivity2.A01.A09(C65193Io.A01(AnonymousClass1U5.A01(location)));
            }
            directorySetLocationMapActivity2.A0B.A06 = location;
            if (AnonymousClass13N.A03(location, this.A00)) {
                this.A00 = location;
            }
        }
    }
}
