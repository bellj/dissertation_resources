package X;

import com.facebook.redex.IDxAListenerShape0S1200000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6BP  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6BP implements AbstractC136286Ly {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ C129235xO A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ AnonymousClass6BP(AnonymousClass016 r1, C129235xO r2, String str) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = str;
    }

    @Override // X.AbstractC136286Ly
    public final void AT6(C1315463e r11) {
        C129235xO r7 = this.A01;
        AnonymousClass016 r6 = this.A00;
        String str = this.A02;
        if (r11 == null) {
            C130785zy r0 = new C130785zy(new C452120p(404), null);
            r0.A01 = null;
            r6.A0A(r0);
            return;
        }
        long A00 = r7.A00.A00();
        String A0n = C12990iw.A0n();
        ArrayList A0l = C12960it.A0l();
        AnonymousClass61S.A03("action", "novi-remove-method", A0l);
        AnonymousClass61S.A03("credential-id", str, A0l);
        if (r7.A01.A07(822)) {
            AnonymousClass61C r4 = r7.A06;
            String str2 = r11.A02;
            JSONObject A04 = r4.A04(A00);
            AnonymousClass61C.A01(A0n, A04);
            try {
                A04.put("account_id", str2);
            } catch (JSONException unused) {
                Log.e("PAY: SignedIntentPayloadManager/addNoviAccountId/toJson can't construct json");
            }
            try {
                A04.put("financial_instrument_id", str);
            } catch (JSONException unused2) {
                Log.e("PAY: IntentPayloadHelper/getRemoveMethodIntent/toJson can't construct json");
            }
            AnonymousClass61S.A03("remove_method_signed_intent", C129585xx.A00(r7.A04, new C129585xx(r4.A04, "REMOVE_FINANCIAL_INSTRUMENT", A04)), A0l);
        }
        r7.A03.A0B(new IDxAListenerShape0S1200000_3_I1(r6, r7, str, 2), C117305Zk.A0Q(A0l), "set", 5);
    }
}
