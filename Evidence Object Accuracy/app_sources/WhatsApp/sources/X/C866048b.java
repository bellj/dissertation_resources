package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.48b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C866048b extends AnonymousClass1JW {
    public final UserJid A00;

    public C866048b(AbstractC15710nm r1, C15450nH r2, UserJid userJid) {
        super(r1, r2);
        this.A00 = userJid;
    }
}
