package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerView;

/* renamed from: X.3WO  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3WO implements AnonymousClass2MF {
    public int A00 = -1;
    public final ConversationRowAudioPreview A01;
    public final AnonymousClass5U0 A02;
    public final AnonymousClass5U1 A03;
    public final AudioPlayerView A04;

    public AnonymousClass3WO(ConversationRowAudioPreview conversationRowAudioPreview, AnonymousClass5U0 r3, AnonymousClass5U1 r4, AudioPlayerView audioPlayerView) {
        this.A04 = audioPlayerView;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = conversationRowAudioPreview;
    }

    @Override // X.AnonymousClass2MF
    public void APa(boolean z) {
        AnonymousClass5U1 r0;
        View findViewById;
        if (this instanceof C60932z2) {
            C60932z2 r1 = (C60932z2) this;
            C35191hP A00 = ((AbstractC44071y9) r1.A01).A03.A00();
            if (A00 != null && A00.A0b == null) {
                r0 = r1.A00;
            } else {
                return;
            }
        } else if (!(this instanceof C60922z1)) {
            AnonymousClass2z0 r12 = (AnonymousClass2z0) this;
            if (r12.A01.A0b == null && (findViewById = AnonymousClass12P.A02(r12.A00).findViewById(R.id.proximity_overlay)) != null) {
                int i = 4;
                if (z) {
                    i = 0;
                }
                findViewById.setVisibility(i);
                return;
            }
            return;
        } else {
            C60922z1 r13 = (C60922z1) this;
            C35191hP A002 = ((AbstractC44071y9) r13.A01).A03.A00();
            if (A002 != null && A002.A0b == null) {
                r0 = r13.A00;
            } else {
                return;
            }
        }
        r0.AW0(z);
    }

    @Override // X.AnonymousClass2MF
    public void ATS(int i) {
        AudioPlayerView audioPlayerView = this.A04;
        audioPlayerView.setPlayButtonState(0);
        audioPlayerView.setSeekbarMax(((AbstractC16130oV) ACr()).A00 * 1000);
        audioPlayerView.setSeekbarProgress(i);
        audioPlayerView.setSeekbarContentDescription((long) i);
        this.A02.APZ(((AbstractC16130oV) ACr()).A00);
        ConversationRowAudioPreview conversationRowAudioPreview = this.A01;
        if (conversationRowAudioPreview != null) {
            conversationRowAudioPreview.A00();
        }
    }

    @Override // X.AnonymousClass2MF
    public void AUL(int i) {
        int i2 = i / 1000;
        if (this.A00 != i2) {
            this.A00 = i2;
            this.A02.APZ(i2);
        }
        AudioPlayerView audioPlayerView = this.A04;
        audioPlayerView.setSeekbarProgress(i);
        audioPlayerView.setSeekbarContentDescription((long) i);
    }

    @Override // X.AnonymousClass2MF
    public void AVR() {
        this.A04.setPlayButtonState(1);
        C12980iv.A1L(this.A01);
    }

    @Override // X.AnonymousClass2MF
    public void AWG(int i) {
        AudioPlayerView audioPlayerView = this.A04;
        audioPlayerView.setPlayButtonState(1);
        audioPlayerView.setSeekbarMax(i);
        this.A00 = -1;
        C12980iv.A1L(this.A01);
    }

    @Override // X.AnonymousClass2MF
    public void AWo(int i, boolean z) {
        AudioPlayerView audioPlayerView = this.A04;
        audioPlayerView.setPlayButtonState(0);
        if (z) {
            audioPlayerView.setSeekbarProgress(0);
        }
        this.A02.APZ(i / 1000);
        ConversationRowAudioPreview conversationRowAudioPreview = this.A01;
        if (conversationRowAudioPreview != null) {
            conversationRowAudioPreview.A00();
        }
        this.A03.AW0(false);
    }
}
