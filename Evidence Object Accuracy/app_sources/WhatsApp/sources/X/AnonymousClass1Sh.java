package X;

import android.content.Context;
import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.1Sh  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Sh {
    public static final FilenameFilter A07 = new AnonymousClass5BJ();
    public static final FilenameFilter A08 = new AnonymousClass5BK();
    public int A00 = 0;
    public long A01 = 0;
    public AnonymousClass4TN A02 = new AnonymousClass4TN();
    public File A03;
    public File A04;
    public File A05;
    public final File A06;

    public AnonymousClass1Sh(Context context, File file) {
        if (file.exists() || file.mkdirs()) {
            this.A06 = file;
        } else {
            File file2 = new File(context.getFilesDir(), "profilo");
            this.A06 = file2;
            File file3 = new File(context.getCacheDir(), "profilo");
            if (file3.exists()) {
                file3.renameTo(file2);
            }
            if (!file2.exists() && !file2.mkdirs()) {
                throw new IllegalStateException("Unable to initialize Profilo folder");
            }
        }
        this.A05 = new File(this.A06, "upload");
        this.A03 = new File(this.A06, "crash_dumps");
        this.A04 = new File(this.A06, "mmap_buffer");
    }
}
