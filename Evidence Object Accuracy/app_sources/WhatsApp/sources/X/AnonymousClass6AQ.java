package X;

/* renamed from: X.6AQ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AQ implements AnonymousClass6MQ {
    public final /* synthetic */ AnonymousClass6MQ A00;
    public final /* synthetic */ C120495gH A01;

    public AnonymousClass6AQ(AnonymousClass6MQ r1, C120495gH r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r4) {
        if (r4 == null || AnonymousClass1ZS.A02(r4.A02)) {
            this.A00.APo(C117305Zk.A0L());
            return;
        }
        this.A01.A06.A0F(r4.A02, r4.A03);
        this.A00.AOb(r4);
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r2) {
        this.A00.APo(r2);
    }
}
