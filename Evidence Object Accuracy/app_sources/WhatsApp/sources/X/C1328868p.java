package X;

/* renamed from: X.68p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328868p implements AnonymousClass1FK {
    public final /* synthetic */ AbstractC28681Oo A00;
    public final /* synthetic */ AbstractC28681Oo A01;
    public final /* synthetic */ C128275vq A02;
    public final /* synthetic */ Object A03;

    public C1328868p(AbstractC28681Oo r1, AbstractC28681Oo r2, C128275vq r3, Object obj) {
        this.A02 = r3;
        this.A03 = obj;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r5) {
        this.A02.A00.A0I(new Runnable(this.A00, this.A03) { // from class: X.6Hs
            public final /* synthetic */ AbstractC28681Oo A00;
            public final /* synthetic */ Object A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C119625ek.A00(this.A00.AAN(), this.A01);
            }
        });
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r5) {
        this.A02.A00.A0I(new Runnable(this.A00, this.A03) { // from class: X.6Hr
            public final /* synthetic */ AbstractC28681Oo A00;
            public final /* synthetic */ Object A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C119625ek.A00(this.A00.AAN(), this.A01);
            }
        });
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        C128275vq r3 = this.A02;
        r3.A07.A01(true, false);
        r3.A00.A0I(new Runnable(this.A01, this.A03) { // from class: X.6Hq
            public final /* synthetic */ AbstractC28681Oo A00;
            public final /* synthetic */ Object A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C119625ek.A00(this.A00.AAN(), this.A01);
            }
        });
    }
}
