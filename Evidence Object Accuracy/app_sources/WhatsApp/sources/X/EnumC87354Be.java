package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87354Be extends Enum implements AbstractC115135Qi {
    public static final AbstractC115605Sf A00 = new C108564zI();
    public static final /* synthetic */ EnumC87354Be[] A01;
    public static final EnumC87354Be A02;
    public static final EnumC87354Be A03;
    public static final EnumC87354Be A04;
    public static final EnumC87354Be A05;
    public static final EnumC87354Be A06;
    public static final EnumC87354Be A07;
    public static final EnumC87354Be A08;
    public static final EnumC87354Be A09;
    public static final EnumC87354Be A0A;
    public static final EnumC87354Be A0B;
    public static final EnumC87354Be A0C;
    public static final EnumC87354Be A0D;
    public static final EnumC87354Be A0E;
    public static final EnumC87354Be A0F;
    public static final EnumC87354Be A0G;
    public static final EnumC87354Be A0H;
    public static final EnumC87354Be A0I;
    public static final EnumC87354Be A0J;
    public static final EnumC87354Be A0K;
    public final int value;

    public EnumC87354Be(int i, String str, int i2) {
        this.value = i2;
    }

    public static EnumC87354Be[] values() {
        return (EnumC87354Be[]) A01.clone();
    }

    static {
        EnumC87354Be r25 = new EnumC87354Be(0, "NONE", -1);
        A02 = r25;
        EnumC87354Be r24 = new EnumC87354Be(1, "MOBILE", 0);
        A03 = r24;
        EnumC87354Be r23 = new EnumC87354Be(2, "WIFI", 1);
        A04 = r23;
        EnumC87354Be r22 = new EnumC87354Be(3, "MOBILE_MMS", 2);
        A05 = r22;
        EnumC87354Be r21 = new EnumC87354Be(4, "MOBILE_SUPL", 3);
        A06 = r21;
        EnumC87354Be r20 = new EnumC87354Be(5, "MOBILE_DUN", 4);
        A07 = r20;
        EnumC87354Be r19 = new EnumC87354Be(6, "MOBILE_HIPRI", 5);
        A08 = r19;
        EnumC87354Be r18 = new EnumC87354Be(7, "WIMAX", 6);
        A09 = r18;
        EnumC87354Be r17 = new EnumC87354Be(8, "BLUETOOTH", 7);
        A0A = r17;
        EnumC87354Be r15 = new EnumC87354Be(9, "DUMMY", 8);
        A0B = r15;
        EnumC87354Be r14 = new EnumC87354Be(10, "ETHERNET", 9);
        A0C = r14;
        EnumC87354Be r13 = new EnumC87354Be(11, "MOBILE_FOTA", 10);
        A0D = r13;
        EnumC87354Be r12 = new EnumC87354Be(12, "MOBILE_IMS", 11);
        A0E = r12;
        EnumC87354Be r11 = new EnumC87354Be(13, "MOBILE_CBS", 12);
        A0F = r11;
        EnumC87354Be r10 = new EnumC87354Be(14, "WIFI_P2P", 13);
        A0G = r10;
        EnumC87354Be r9 = new EnumC87354Be(15, "MOBILE_IA", 14);
        A0H = r9;
        EnumC87354Be r8 = new EnumC87354Be(16, "MOBILE_EMERGENCY", 15);
        A0I = r8;
        EnumC87354Be r6 = new EnumC87354Be(17, "PROXY", 16);
        A0J = r6;
        EnumC87354Be r5 = new EnumC87354Be(18, "VPN", 17);
        A0K = r5;
        EnumC87354Be[] r4 = new EnumC87354Be[19];
        C72453ed.A1F(r25, r24, r23, r22, r4);
        r4[4] = r21;
        C12970iu.A1R(r20, r19, r18, r17, r4);
        C72453ed.A1G(r15, r14, r13, r12, r4);
        C72453ed.A1H(r11, r10, r9, r8, r4);
        r4[17] = r6;
        r4[18] = r5;
        A01 = r4;
    }
}
