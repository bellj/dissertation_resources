package X;

import android.os.SystemClock;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.3IS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IS {
    public static final byte[] A07 = {102, 116, 121, 112};
    public static final byte[] A08 = {109, 100, 97, 116};
    public static final byte[] A09 = {109, 111, 111, 118};
    public int A00 = 0;
    public long A01 = 262144;
    public final long A02;
    public final AbstractC15710nm A03;
    public final Mp4Ops A04;
    public final C16590pI A05;
    public final File A06;

    public AnonymousClass3IS(AbstractC15710nm r3, Mp4Ops mp4Ops, C16590pI r5, File file, long j) {
        this.A05 = r5;
        this.A04 = mp4Ops;
        this.A03 = r3;
        this.A06 = file;
        this.A02 = j;
    }

    public static int A00(byte[] bArr) {
        return ((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
    }

    public static boolean A01(byte[] bArr, byte[] bArr2) {
        int length = bArr.length - 4;
        int length2 = bArr2.length;
        if (length >= length2) {
            for (int i = 0; i < length2; i++) {
                if (bArr[4 + i] == bArr2[i]) {
                }
            }
            return true;
        }
        return false;
    }

    public final int A02(boolean z) {
        Mp4Ops.LibMp4StreamCheckResult mp4streamcheck;
        try {
            File file = this.A06;
            long j = this.A02;
            Log.i("mp4ops/check/start");
            int i = 0;
            while (true) {
                try {
                    mp4streamcheck = Mp4Ops.mp4streamcheck(file.getAbsolutePath(), z, j);
                    if (mp4streamcheck.success || !mp4streamcheck.ioException) {
                        break;
                    }
                    SystemClock.sleep(100);
                    i++;
                    if (i >= 5) {
                        break;
                    }
                } catch (Error e) {
                    Log.e("mp4ops/integration fail/", e);
                    throw new C39361pl(0, "stream integrity check error");
                }
            }
            AnonymousClass009.A05(mp4streamcheck);
            if (mp4streamcheck.success) {
                Log.i("mp4ops/streamcheck/finished");
                long j2 = mp4streamcheck.bytesRequiredToExtractThumbnail;
                if (j2 <= 0) {
                    return 1;
                }
                this.A01 = j2;
                return 1;
            }
            Log.e(C12960it.A0d(mp4streamcheck.errorMessage, C12960it.A0k("mp4ops/streamcheck/error_message/")));
            int i2 = mp4streamcheck.errorCode;
            throw new C39361pl(i2, C12960it.A0W(i2, "integrity check failed, error_code: "));
        } catch (C39361pl e2) {
            Log.e("Mp4StreamCheck/failed/exception", e2);
            Mp4Ops.A00(this.A05.A00, this.A03, this.A06, e2, "stream check on download");
            return 2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(long r12) {
        /*
        // Method dump skipped, instructions count: 218
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3IS.A03(long):boolean");
    }
}
