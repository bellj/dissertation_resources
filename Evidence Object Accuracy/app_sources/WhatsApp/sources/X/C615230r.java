package X;

/* renamed from: X.30r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615230r extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public String A05;

    public C615230r() {
        super(2870, new AnonymousClass00E(1, 1, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A05);
        r3.Abe(1, this.A00);
        r3.Abe(4, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(5, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerOrphanFileCleanUp {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numberOfStickersDeleted", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "orphanFileCleanUpFailureReason", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "orphanFileCleanUpResult", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "spaceSavedInKb", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeToDeleteMs", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeToQueryMs", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
