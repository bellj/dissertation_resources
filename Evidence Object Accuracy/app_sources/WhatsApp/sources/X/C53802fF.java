package X;

import android.app.Application;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2fF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53802fF extends AnonymousClass014 {
    public final int A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass19T A02;
    public final AnonymousClass3EX A03;
    public final UserJid A04;

    public C53802fF(Application application, AnonymousClass19T r5, AnonymousClass3EX r6, UserJid userJid) {
        super(application);
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A01 = A0T;
        this.A04 = userJid;
        this.A03 = r6;
        this.A02 = r5;
        this.A00 = application.getResources().getDimensionPixelSize(R.dimen.product_catalog_list_thumb_size);
        r6.A00 = A0T;
    }
}
