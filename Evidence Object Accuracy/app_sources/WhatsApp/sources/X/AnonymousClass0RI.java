package X;

/* renamed from: X.0RI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RI {
    public static final long A00;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        if (r3 > 0) goto L_0x0014;
     */
    static {
        /*
            r8 = 100
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0018
            int r0 = android.system.OsConstants._SC_CLK_TCK
            long r3 = android.system.Os.sysconf(r0)
        L_0x000e:
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0015
        L_0x0014:
            r8 = r3
        L_0x0015:
            X.AnonymousClass0RI.A00 = r8
            return
        L_0x0018:
            java.lang.String r1 = "_SC_CLK_TCK"
            java.lang.String r0 = "libcore.io.OsConstants"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.reflect.Field r0 = r0.getField(r1)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            r2 = 0
            int r7 = r0.getInt(r2)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.String r0 = "libcore.io.Libcore"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.String r0 = "libcore.io.Os"
            java.lang.Class r6 = java.lang.Class.forName(r0)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.String r0 = "os"
            java.lang.reflect.Field r0 = r1.getField(r0)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Object r5 = r0.get(r2)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.String r2 = "sysconf"
            r4 = 1
            java.lang.Class[] r1 = new java.lang.Class[r4]     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            r3 = 0
            r1[r3] = r0     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.reflect.Method r2 = r6.getMethod(r2, r1)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Object[] r1 = new java.lang.Object[r4]     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            r1[r3] = r0     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Object r0 = r2.invoke(r5, r1)     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            java.lang.Number r0 = (java.lang.Number) r0     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            long r3 = r0.longValue()     // Catch: NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | ClassNotFoundException -> 0x0060
            goto L_0x000e
        L_0x0060:
            r3 = 100
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0RI.<clinit>():void");
    }
}
