package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.R;

/* renamed from: X.4m5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100544m5 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(48);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100544m5(int i, int i2, int i3) {
        this.A00 = i;
        this.A01 = R.string.call_link_type;
        this.A02 = i2;
        this.A04 = R.string.select_call_type;
        this.A03 = i3;
        this.A05 = R.array.call_link_types;
    }

    public C100544m5(Parcel parcel) {
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A02 = parcel.readInt();
        this.A04 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A05 = parcel.readInt();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A04);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A05);
    }
}
