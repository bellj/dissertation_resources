package X;

import android.graphics.Rect;
import android.util.Property;
import android.view.View;

/* renamed from: X.0An  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02230An extends Property {
    public C02230An() {
        super(Rect.class, "clipBounds");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return AnonymousClass028.A0A((View) obj);
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        AnonymousClass028.A0f((View) obj, (Rect) obj2);
    }
}
