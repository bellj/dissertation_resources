package X;

import android.view.View;
import android.widget.ListView;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2eN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53412eN extends AbstractC000600h {
    public final /* synthetic */ MediaAlbumActivity A00;

    public C53412eN(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AbstractC000600h
    public void A03(List list, Map map) {
        View A06;
        View A062;
        MediaAlbumActivity mediaAlbumActivity = this.A00;
        List list2 = mediaAlbumActivity.A08.A00;
        if (list2 != null) {
            Iterator it = list2.iterator();
            int i = 0;
            boolean z = false;
            while (it.hasNext()) {
                AbstractC15340mz A0f = C12980iv.A0f(it);
                i++;
                if (i <= 3) {
                    ListView A2e = mediaAlbumActivity.A2e();
                    AnonymousClass1IS r5 = A0f.A0z;
                    View findViewWithTag = A2e.findViewWithTag(r5);
                    if (findViewWithTag == null || z || (findViewWithTag.getTop() < 0 && (findViewWithTag.getTop() >= 0 || findViewWithTag.getBottom() < mediaAlbumActivity.A2e().getHeight()))) {
                        map.remove(AbstractC28561Ob.A0W(r5));
                        map.remove(AbstractC42671vd.A0Y(A0f));
                    } else {
                        String A0W = AbstractC28561Ob.A0W(r5);
                        if (!map.containsKey(A0W) && (A062 = AbstractC454421p.A06(mediaAlbumActivity.A2e(), A0W)) != null) {
                            list.add(A0W);
                            map.put(A0W, A062);
                        }
                        String A0Y = AbstractC42671vd.A0Y(A0f);
                        if (!map.containsKey(A0Y) && (A06 = AbstractC454421p.A06(mediaAlbumActivity.A2e(), A0Y)) != null) {
                            list.add(A0Y);
                            map.put(A0Y, A06);
                        }
                        z = true;
                    }
                } else {
                    return;
                }
            }
        }
    }
}
