package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapeItemView;

/* renamed from: X.2aB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52062aB extends Handler {
    public final Context A00;
    public final /* synthetic */ AnonymousClass1KW A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC52062aB(Context context, Looper looper, AnonymousClass1KW r3) {
        super(looper);
        this.A01 = r3;
        AnonymousClass009.A05(looper);
        this.A00 = context;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (message.obj != null) {
            String A01 = AnonymousClass1KW.A01(message.getData());
            ShapeItemView shapeItemView = (ShapeItemView) message.obj;
            if (A01.equals(shapeItemView.A01)) {
                C90414Nu r1 = (C90414Nu) shapeItemView.getTag();
                shapeItemView.setImageDrawable(r1.A00);
                shapeItemView.setContentDescription(r1.A01);
            }
        }
    }
}
