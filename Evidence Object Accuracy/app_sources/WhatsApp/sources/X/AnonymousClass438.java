package X;

/* renamed from: X.438  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass438 extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass438() {
        super(2522, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdAppStateMessageRange {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "additionalMessagesCount", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
