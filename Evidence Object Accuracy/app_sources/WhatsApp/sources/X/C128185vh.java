package X;

import java.util.List;

/* renamed from: X.5vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128185vh {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final List A08;

    public C128185vh(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List list) {
        this.A01 = str;
        this.A03 = str2;
        this.A05 = str3;
        this.A06 = str4;
        this.A04 = str5;
        this.A02 = str6;
        this.A00 = str7;
        this.A07 = str8;
        this.A08 = list;
    }
}
