package X;

import android.os.ConditionVariable;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import com.whatsapp.util.Log;
import java.util.Collection;

/* renamed from: X.009  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass009 {
    public static ConditionVariable A00 = new ConditionVariable();
    public static volatile Boolean A01;

    public static void A00() {
        A0A("should not be run in main thread", !AnonymousClass01I.A01());
    }

    public static void A01() {
        A0A("should be run in ui main thread", AnonymousClass01I.A01());
    }

    public static void A02(Handler handler) {
        boolean z = false;
        if (handler.getLooper() == Looper.myLooper()) {
            z = true;
        }
        A0F(z);
    }

    public static void A03(View view) {
        A06(view, "");
    }

    public static void A04(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            throw new IllegalArgumentException("");
        }
    }

    public static void A05(Object obj) {
        A06(obj, "");
    }

    public static void A06(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException(str);
        }
    }

    public static void A07(String str) {
        A0A(str, false);
    }

    public static void A08(String str, Throwable th) {
        A05(th);
        A05(str);
        Log.e(str, th);
    }

    public static void A09(String str, Collection collection) {
        if (collection == null || collection.isEmpty()) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void A0A(String str, boolean z) {
        if (!z) {
            Log.e(new AssertionError(str));
        }
    }

    public static void A0B(String str, boolean z) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void A0C(String str, boolean z) {
        if (!z) {
            throw new IllegalStateException(str);
        }
    }

    public static void A0D(Throwable th) {
        A05(th);
        Log.e(th);
    }

    public static void A0E(boolean z) {
        A0B("", z);
    }

    public static void A0F(boolean z) {
        A0C("", z);
    }

    public static void A0G(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("");
        }
    }

    public static void A0H(Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            throw new IllegalArgumentException("");
        }
    }
}
