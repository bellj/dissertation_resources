package X;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Ys  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30761Ys {
    public long A00 = -1;
    public boolean A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;
    public final AtomicInteger A06;

    public C30761Ys(String str, String str2, int i, int i2, int i3, boolean z) {
        this.A04 = str;
        this.A05 = str2;
        this.A03 = i;
        this.A01 = z;
        this.A02 = i2;
        this.A06 = new AtomicInteger(i3);
    }
}
