package X;

import android.view.View;
import androidx.core.widget.NestedScrollView;

/* renamed from: X.0YG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0YG implements AbstractC11770gq {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass0U5 A02;

    public AnonymousClass0YG(View view, View view2, AnonymousClass0U5 r3) {
        this.A02 = r3;
        this.A01 = view;
        this.A00 = view2;
    }

    @Override // X.AbstractC11770gq
    public void AVa(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
        AnonymousClass0U5.A01(nestedScrollView, this.A01, this.A00);
    }
}
