package X;

import com.whatsapp.gallery.NewMediaPickerFragment;
import com.whatsapp.gallerypicker.MediaPicker;
import com.whatsapp.gallerypicker.MediaPickerFragment;

/* renamed from: X.2pY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58482pY extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58482pY() {
        ActivityC13830kP.A1P(this, 69);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            MediaPicker mediaPicker = (MediaPicker) this;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, mediaPicker);
            ActivityC13810kN.A10(A1M, mediaPicker);
            ((ActivityC13790kL) mediaPicker).A08 = ActivityC13790kL.A0S(r2, A1M, mediaPicker, ActivityC13790kL.A0Y(A1M, mediaPicker));
            C14850m9 A0S = C12960it.A0S(A1M);
            C48862Id r22 = new C48862Id(new NewMediaPickerFragment());
            AnonymousClass01E mediaPickerFragment = new MediaPickerFragment();
            if (!A0S.A07(1857) || (mediaPickerFragment = (AnonymousClass01E) r22.reference) != null) {
                mediaPicker.A00 = mediaPickerFragment;
                return;
            }
            throw C12980iv.A0n("Cannot return null from a non-@Nullable @Provides method");
        }
    }
}
