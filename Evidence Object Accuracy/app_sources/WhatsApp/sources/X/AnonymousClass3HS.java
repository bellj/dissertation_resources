package X;

import android.content.Context;
import android.os.Build;
import com.whatsapp.R;

/* renamed from: X.3HS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HS {
    public static final int[] A03 = (Build.VERSION.SDK_INT >= 29 ? new int[]{R.string.settings_theme_follow_system, R.string.settings_theme_light, R.string.settings_theme_dark} : new int[]{R.string.settings_theme_light, R.string.settings_theme_dark});
    public final Context A00;
    public final C14820m6 A01;
    public final AnonymousClass018 A02;

    public AnonymousClass3HS(Context context, C14820m6 r2, AnonymousClass018 r3) {
        this.A00 = context;
        this.A02 = r3;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        if (r4 != 2) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r4 != 2) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        r0 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A00() {
        /*
            r7 = this;
            android.content.Context r6 = r7.A00
            int[] r5 = X.AnonymousClass3HS.A03
            X.0m6 r0 = r7.A01
            int r4 = r0.A04()
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 1
            r1 = 2
            r0 = 29
            if (r3 < r0) goto L_0x001f
            if (r4 == r2) goto L_0x0022
            r0 = 2
            if (r4 == r1) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            r0 = r5[r0]
            java.lang.String r0 = r6.getString(r0)
            return r0
        L_0x001f:
            if (r4 == r1) goto L_0x0022
            goto L_0x0017
        L_0x0022:
            r0 = 1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HS.A00():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        if (r5 != 2) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0011, code lost:
        if (r5 != 2) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        r4 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.ActivityC13810kN r7) {
        /*
            r6 = this;
            X.0m6 r0 = r6.A01
            int r5 = r0.A04()
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 1
            r1 = 2
            r0 = 29
            if (r3 < r0) goto L_0x0030
            if (r5 == r2) goto L_0x0033
            r4 = 2
            if (r5 == r1) goto L_0x0014
        L_0x0013:
            r4 = 0
        L_0x0014:
            X.018 r1 = r6.A02
            int[] r0 = X.AnonymousClass3HS.A03
            java.lang.String[] r3 = r1.A0S(r0)
            r2 = 3
            r0 = 2131891781(0x7f121645, float:1.9418292E38)
            com.whatsapp.SingleSelectionDialogFragment r1 = new com.whatsapp.SingleSelectionDialogFragment
            r1.<init>()
            android.os.Bundle r0 = com.whatsapp.SingleSelectionDialogFragment.A00(r3, r2, r4, r0)
            r1.A0U(r0)
            r7.Adm(r1)
            return
        L_0x0030:
            if (r5 == r1) goto L_0x0033
            goto L_0x0013
        L_0x0033:
            r4 = 1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HS.A01(X.0kN):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r7 != 1) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(int r7) {
        /*
            r6 = this;
            X.0m6 r5 = r6.A01
            int r4 = r5.A04()
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 2
            r1 = 1
            r0 = 29
            if (r3 < r0) goto L_0x0023
            if (r7 == r1) goto L_0x0025
            if (r7 == r2) goto L_0x0013
            r2 = -1
        L_0x0013:
            if (r4 == r2) goto L_0x0027
            android.content.SharedPreferences$Editor r1 = X.C12960it.A08(r5)
            java.lang.String r0 = "night_mode"
            X.C12970iu.A1B(r1, r0, r2)
            X.AnonymousClass025.A00(r2)
            r0 = 1
            return r0
        L_0x0023:
            if (r7 == r1) goto L_0x0013
        L_0x0025:
            r2 = 1
            goto L_0x0013
        L_0x0027:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HS.A02(int):boolean");
    }
}
