package X;

/* renamed from: X.5ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119625ek extends AbstractC17440qo {
    public final C128275vq A00;

    public C119625ek(C128275vq r4) {
        super("wa.action.novi.EncryptLogEventV2", "bk.action.waffle.HasPaymentAccount", "bk.action.waffle.StartPaymentOnboarding", "bk.action.waffle.DeletePaymentAccount", "bk.action.waffle.EligibleToShowPaymentsRow", "bk.action.waffle.ShowPaymentSettings");
        this.A00 = r4;
    }

    public static void A00(AbstractC14200l1 r2, Object obj) {
        new C14250l6(AnonymousClass1AI.A02(obj)).A01(C14220l3.A01, r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        if (r1.equals("bk.action.waffle.HasPaymentAccount") == false) goto L_0x0011;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b1, code lost:
        if (r1.A0A.A05("BR") == false) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
        if (r1.equals("bk.action.waffle.EligibleToShowPaymentsRow") == false) goto L_0x0011;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0107  */
    @Override // X.AbstractC17450qp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A9j(X.C14220l3 r21, X.C1093651k r22, X.C14240l5 r23) {
        /*
        // Method dump skipped, instructions count: 358
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119625ek.A9j(X.0l3, X.51k, X.0l5):java.lang.Object");
    }
}
