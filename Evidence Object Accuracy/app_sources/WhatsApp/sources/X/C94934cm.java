package X;

import java.nio.ByteBuffer;

/* renamed from: X.4cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94934cm {
    public long A00;
    public AnonymousClass4SR A01;
    public AnonymousClass4SR A02;
    public AnonymousClass4SR A03;
    public final int A04;
    public final AnonymousClass5VZ A05;
    public final C95304dT A06 = C95304dT.A05(32);

    public C94934cm(AnonymousClass5VZ r5) {
        this.A05 = r5;
        int i = ((C107834y1) r5).A04;
        this.A04 = i;
        AnonymousClass4SR r0 = new AnonymousClass4SR(0, i);
        this.A01 = r0;
        this.A02 = r0;
        this.A03 = r0;
    }

    public static AnonymousClass4SR A00(AnonymousClass4SR r8, ByteBuffer byteBuffer, int i, long j) {
        while (j >= r8.A03) {
            r8 = r8.A00;
        }
        while (i > 0) {
            long j2 = r8.A03;
            int min = Math.min(i, (int) (j2 - j));
            byteBuffer.put(r8.A01.A00, ((int) (j - r8.A04)) + 0, min);
            i -= min;
            j += (long) min;
            if (j == j2) {
                r8 = r8.A00;
            }
        }
        return r8;
    }

    public static AnonymousClass4SR A01(AnonymousClass4SR r9, byte[] bArr, int i, long j) {
        while (j >= r9.A03) {
            r9 = r9.A00;
        }
        int i2 = i;
        while (i2 > 0) {
            long j2 = r9.A03;
            int min = Math.min(i2, (int) (j2 - j));
            System.arraycopy(r9.A01.A00, ((int) (j - r9.A04)) + 0, bArr, i - i2, min);
            i2 -= min;
            j += (long) min;
            if (j == j2) {
                r9 = r9.A00;
            }
        }
        return r9;
    }

    public final int A02(int i) {
        AnonymousClass4IL r4;
        AnonymousClass4SR r5 = this.A03;
        AnonymousClass4SR r6 = r5;
        if (!r5.A02) {
            C107834y1 r3 = (C107834y1) this.A05;
            synchronized (r3) {
                r3.A00++;
                int i2 = r3.A01;
                if (i2 > 0) {
                    AnonymousClass4IL[] r2 = r3.A03;
                    int i3 = i2 - 1;
                    r3.A01 = i3;
                    r4 = r2[i3];
                    r2[i3] = null;
                } else {
                    r4 = new AnonymousClass4IL(new byte[r3.A04]);
                }
            }
            r6 = this.A03;
            AnonymousClass4SR r0 = new AnonymousClass4SR(r6.A03, this.A04);
            r5.A01 = r4;
            r5.A00 = r0;
            r5.A02 = true;
        }
        return Math.min(i, (int) (r6.A03 - this.A00));
    }

    public void A03(long j) {
        AnonymousClass4SR r5;
        if (j != -1) {
            while (true) {
                r5 = this.A01;
                if (j < r5.A03) {
                    break;
                }
                AnonymousClass5VZ r3 = this.A05;
                AnonymousClass4IL r2 = r5.A01;
                C107834y1 r32 = (C107834y1) r3;
                synchronized (r32) {
                    AnonymousClass4IL[] r1 = r32.A06;
                    r1[0] = r2;
                    r32.Aa5(r1);
                }
                AnonymousClass4SR r22 = this.A01;
                r22.A01 = null;
                AnonymousClass4SR r0 = r22.A00;
                r22.A00 = null;
                this.A01 = r0;
            }
            if (this.A02.A04 < r5.A04) {
                this.A02 = r5;
            }
        }
    }
}
