package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.R;

/* renamed from: X.24Y  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass24Y {
    public Context A00;
    public C17070qD A01;
    public final C30931Zj A02 = C30931Zj.A00("PaymentMethodNotificationUtil", "notification", "COMMON");

    public abstract String A01(AbstractC28901Pl v, AnonymousClass1V8 v2);

    public AnonymousClass24Y(Context context, C17070qD r5) {
        this.A00 = context;
        this.A01 = r5;
    }

    public PendingIntent A00(Context context, AbstractC28901Pl r8, String str) {
        Intent intent;
        AbstractC16830pp A02 = this.A01.A02();
        if (r8 != null) {
            intent = new Intent(context, A02.AAV());
            intent.addFlags(335544320);
            intent.putExtra("extra_bank_account", r8);
        } else {
            Class AFa = A02.AFa();
            C30931Zj r2 = this.A02;
            StringBuilder sb = new StringBuilder("getPendingIntent for ");
            sb.append(str);
            r2.A06(sb.toString());
            intent = new Intent(context, AFa);
            intent.addFlags(335544320);
        }
        return AnonymousClass1UY.A00(context, 0, intent, 0);
    }

    public String A02(AbstractC28901Pl r3, String str) {
        return this.A00.getString(R.string.view);
    }

    public String A03(AbstractC28901Pl r4, String str) {
        return this.A00.getResources().getQuantityString(R.plurals.notification_new_payment_method_update, 1);
    }
}
