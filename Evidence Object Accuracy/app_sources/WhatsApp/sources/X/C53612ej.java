package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53612ej extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass2G6 A00;

    public C53612ej(AnonymousClass2G6 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A00(View view, int i) {
        if (i != 4) {
            super.A00(view, i);
        }
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
        accessibilityNodeInfo.setClickable(false);
        accessibilityNodeInfo.setSelected(false);
        for (C007804a r2 : r6.A03()) {
            if (r2.A00() == 16 || r2.A00() == 4) {
                r6.A0A(r2);
            }
        }
    }
}
