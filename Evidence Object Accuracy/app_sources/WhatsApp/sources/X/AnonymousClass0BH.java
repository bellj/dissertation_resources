package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0BH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BH extends Animation {
    public final /* synthetic */ AnonymousClass0BD A00;

    public AnonymousClass0BH(AnonymousClass0BD r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass0BD r3 = this.A00;
        int i = r3.A09;
        r3.setTargetOffsetTopAndBottom((i + ((int) (((float) ((r3.A0C - Math.abs(r3.A0B)) - i)) * f))) - r3.A0K.getTop());
        AnonymousClass0A7 r32 = r3.A0L;
        float f2 = 1.0f - f;
        AnonymousClass0OB r1 = r32.A05;
        if (f2 != r1.A00) {
            r1.A00 = f2;
        }
        r32.invalidateSelf();
    }
}
