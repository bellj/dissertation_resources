package X;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import com.whatsapp.R;

/* renamed from: X.2Yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC51712Yf extends Dialog implements AnonymousClass291 {
    public final AbstractC471028y A00;
    public final AnonymousClass3YM A01;

    public DialogC51712Yf(Activity activity, AnonymousClass2Ab r3, AbstractC471028y r4, C91494Ry r5, int[] iArr) {
        super(activity, R.style.DoodleTextDialog);
        this.A00 = r4;
        this.A01 = new AnonymousClass3YM(r3, r4, r5, iArr);
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(this.A00);
        AnonymousClass3YM r2 = this.A01;
        Window window = getWindow();
        r2.A00 = this;
        r2.A03.A00(window, r2, r2.A04, r2.A05, false);
    }
}
