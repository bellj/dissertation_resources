package X;

import android.view.View;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.4ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105384ty implements AnonymousClass02B {
    public final /* synthetic */ VoipActivityV2 A00;

    public C105384ty(VoipActivityV2 voipActivityV2) {
        this.A00 = voipActivityV2;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        VoipActivityV2 voipActivityV2 = this.A00;
        if (AnonymousClass23N.A05(((ActivityC13810kN) voipActivityV2).A08.A0P())) {
            View view = voipActivityV2.A0R;
            int i = 0;
            if (obj != null) {
                i = 8;
            }
            view.setVisibility(i);
        } else if (obj != null) {
            voipActivityV2.A2t();
        }
    }
}
