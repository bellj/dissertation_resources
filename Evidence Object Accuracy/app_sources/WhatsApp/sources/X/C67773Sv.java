package X;

import android.content.Context;
import android.net.Uri;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3Sv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67773Sv implements AnonymousClass2BW {
    public long A00;
    public Uri A01;
    public boolean A02;
    public final AnonymousClass2BW A03;
    public final Object A04 = C12970iu.A0l();

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r1) {
    }

    public C67773Sv(Context context, Uri uri) {
        this.A03 = new C56112kL(context);
        this.A01 = uri;
    }

    @Override // X.AnonymousClass2BW
    public /* synthetic */ Map AGF() {
        return Collections.emptyMap();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        Uri uri;
        synchronized (this.A04) {
            uri = this.A01;
        }
        return uri;
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r8) {
        long j;
        Uri uri;
        synchronized (this.A04) {
            j = r8.A03;
            this.A00 = j;
            uri = this.A01;
        }
        if (uri != null) {
            return this.A03.AYZ(new AnonymousClass3H3(uri, j, -1));
        }
        throw C12990iw.A0i("Uri not set");
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A03.close();
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        long j;
        Uri uri;
        Object obj = this.A04;
        synchronized (obj) {
            j = this.A00;
            if (this.A02) {
                this.A02 = false;
                uri = this.A01;
            } else {
                uri = null;
            }
        }
        if (uri != null) {
            AnonymousClass2BW r0 = this.A03;
            r0.close();
            r0.AYZ(new AnonymousClass3H3(uri, j, -1));
        }
        int read = this.A03.read(bArr, i, i2);
        synchronized (obj) {
            this.A00 += (long) read;
        }
        return read;
    }
}
