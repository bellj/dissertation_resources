package X;

import android.content.Context;
import android.os.Environment;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.18p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252318p implements AbstractC15840nz {
    public final C15570nT A00;
    public final C15820nx A01;
    public final C15810nw A02;
    public final C17050qB A03;
    public final C16590pI A04;
    public final C15890o4 A05;
    public final C19490uC A06;
    public final C21780xy A07;
    public final C16600pJ A08;
    public final AnonymousClass15D A09;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "wallpaper-v2";
    }

    public C252318p(C15570nT r1, C15820nx r2, C15810nw r3, C17050qB r4, C16590pI r5, C15890o4 r6, C19490uC r7, C21780xy r8, C16600pJ r9, AnonymousClass15D r10) {
        this.A04 = r5;
        this.A09 = r10;
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A06 = r7;
        this.A03 = r4;
        this.A05 = r6;
        this.A08 = r9;
        this.A07 = r8;
    }

    public boolean A00(EnumC16570pG r19) {
        String obj;
        C15810nw r3 = this.A02;
        EnumC16570pG r0 = EnumC16570pG.A08;
        File A02 = r3.A02();
        if (r19 == r0) {
            obj = "wallpaper.bkup";
        } else {
            StringBuilder sb = new StringBuilder("wallpaper.bkup.crypt");
            sb.append(r19.version);
            obj = sb.toString();
        }
        File file = new File(A02, obj);
        List A07 = C32781cj.A07(EnumC16570pG.A06, EnumC16570pG.A00());
        File file2 = new File(r3.A02(), "wallpaper.bkup");
        ArrayList<File> A06 = C32781cj.A06(file2, A07);
        C32781cj.A0C(file2, A06);
        for (File file3 : A06) {
            if (!file3.equals(file) && file3.exists()) {
                C14350lI.A0N(file3);
            }
        }
        Context context = this.A04.A00;
        File file4 = new File(context.getFilesDir(), "wallpaper.jpg");
        if (!file4.exists()) {
            return true;
        }
        File parentFile = file.getParentFile();
        AnonymousClass009.A05(parentFile);
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        if (!this.A05.A0A(Environment.getExternalStorageState())) {
            StringBuilder sb2 = new StringBuilder("wallpaper/backup/sdcard_unavailable ");
            sb2.append(Environment.getExternalStorageState());
            Log.i(sb2.toString());
            return false;
        }
        try {
            AnonymousClass15D r02 = this.A09;
            C15570nT r8 = this.A00;
            C19490uC r13 = this.A06;
            C15820nx r11 = this.A01;
            C17050qB r12 = this.A03;
            C16600pJ r15 = this.A08;
            AbstractC33251dh A00 = C33231df.A00(r8, new C33211dd(file), null, r11, r12, r13, this.A07, r15, r19, r02);
            if (!A00.A04(context)) {
                Log.e("wallpaper/backup/failed to prepare for backup");
                return false;
            }
            A00.A03(null, file4);
            return true;
        } catch (Exception e) {
            Log.w("wallpaper/backup/error ", e);
            return false;
        }
    }

    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r0;
        if (this.A01.A04()) {
            r0 = EnumC16570pG.A07;
        } else {
            r0 = EnumC16570pG.A06;
        }
        return A00(r0);
    }
}
