package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;

/* renamed from: X.2a3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a3 extends Handler {
    public final /* synthetic */ VerifyTwoFactorAuthCodeDialogFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a3(Looper looper, VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment) {
        super(looper);
        this.A00 = verifyTwoFactorAuthCodeDialogFragment;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (message.what == 0) {
            VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment = this.A00;
            if (verifyTwoFactorAuthCodeDialogFragment.A00 == 0) {
                verifyTwoFactorAuthCodeDialogFragment.A08.Ab6(new RunnableBRunnable0Shape0S1100000_I0(3, (String) message.obj, verifyTwoFactorAuthCodeDialogFragment));
            }
        }
    }
}
