package X;

/* renamed from: X.0zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22780zd {
    public final AnonymousClass123 A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;

    public C22780zd(C16630pM r2, AnonymousClass123 r3) {
        this.A01 = new AnonymousClass01H() { // from class: X.1NH
            @Override // X.AnonymousClass01H
            public final Object get() {
                return C16630pM.this.A01(AnonymousClass01V.A07);
            }
        };
        this.A02 = new AnonymousClass01H() { // from class: X.1NI
            @Override // X.AnonymousClass01H
            public final Object get() {
                return C16630pM.this.A01("server_prop_preferences");
            }
        };
        this.A00 = r3;
    }
}
