package X;

import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5fB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119835fB extends AbstractC30891Zf {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(8);
    public int A00;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 1;
    public long A04;
    public long A05 = -1;
    public AnonymousClass1ZR A06;
    public AnonymousClass1ZR A07;
    public AnonymousClass1ZR A08;
    public AnonymousClass1ZR A09;
    public AnonymousClass1ZR A0A;
    public AnonymousClass60R A0B;
    public AnonymousClass686 A0C;
    public Boolean A0D = null;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public String A0Q;

    @Override // X.AnonymousClass1ZP
    public String A03() {
        return null;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r6, AnonymousClass1V8 r7, int i) {
        String A0I = r7.A0I("seq-no", null);
        if (!TextUtils.isEmpty(A0I)) {
            this.A0N = A0I;
        }
        String A0I2 = r7.A0I("ref-url", null);
        if (!TextUtils.isEmpty(A0I2)) {
            this.A0Q = A0I2;
        }
        String A0I3 = r7.A0I("sync-status", null);
        if (!TextUtils.isEmpty(A0I3)) {
            this.A0O = A0I3;
        }
        String A0I4 = r7.A0I("upi-bank-info", null);
        if (A0I4 != null) {
            this.A09 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0I4, "bankInfo");
        }
        String A0I5 = r7.A0I("sender-name", null);
        if (A0I5 != null) {
            this.A08 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0I5, "legalName");
        }
        String A0I6 = r7.A0I("receiver-name", null);
        if (A0I6 != null) {
            this.A07 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0I6, "legalName");
        }
        AnonymousClass1V8 A0E = r7.A0E("mandate");
        if (A0E != null) {
            this.A0B = new AnonymousClass60R(r6, A0E);
        }
        String A0I7 = r7.A0I("is-complaint-eligible", null);
        AnonymousClass1V8 A0E2 = r7.A0E("complaint");
        if (!(A0I7 == null && A0E2 == null)) {
            this.A0C = new AnonymousClass686(A0E2, A0I7);
        }
        String A0I8 = r7.A0I("mandate-transaction-id", null);
        if (!TextUtils.isEmpty(A0I8)) {
            this.A0G = A0I8;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        String str;
        if (!AnonymousClass1ZS.A02(this.A0A)) {
            C117295Zj.A1O("mpin", (String) C117295Zj.A0R(this.A0A), list);
        }
        if (!TextUtils.isEmpty(this.A0N)) {
            C117295Zj.A1O("seq-no", this.A0N, list);
        }
        if (!TextUtils.isEmpty(this.A0L)) {
            C117295Zj.A1O("sender-vpa", this.A0L, list);
        }
        if (!TextUtils.isEmpty(this.A0M)) {
            C117295Zj.A1O("sender-vpa-id", this.A0M, list);
        }
        if (!TextUtils.isEmpty(this.A0J)) {
            C117295Zj.A1O("receiver-vpa", this.A0J, list);
        }
        if (!TextUtils.isEmpty(this.A0K)) {
            C117295Zj.A1O("receiver-vpa-id", this.A0K, list);
        }
        if (!AnonymousClass1ZS.A02(this.A07)) {
            C117295Zj.A1O("receiver-name", (String) this.A07.A00, list);
        }
        if (!AnonymousClass1ZS.A02(this.A08)) {
            C117295Zj.A1O("sender-name", (String) this.A08.A00, list);
        }
        if (!TextUtils.isEmpty(this.A0E)) {
            C117295Zj.A1O("device-id", this.A0E, list);
        }
        if (!AnonymousClass1ZS.A02(this.A09)) {
            C117295Zj.A1O("upi-bank-info", (String) C117295Zj.A0R(this.A09), list);
        }
        if (!TextUtils.isEmpty(this.A0H)) {
            C117295Zj.A1O("mcc", this.A0H, list);
        }
        Boolean bool = this.A0D;
        if (bool != null) {
            if (bool.booleanValue()) {
                str = "1";
            } else {
                str = "0";
            }
            C117295Zj.A1O("is_first_send", str, list);
        }
        C38171nd r0 = super.A02;
        if (r0 != null) {
            C117295Zj.A1O("ref-id", r0.A01, list);
        }
        if (!TextUtils.isEmpty(this.A0I)) {
            C117295Zj.A1O("purpose-code", this.A0I, list);
        }
        if (!TextUtils.isEmpty(this.A0G)) {
            C117295Zj.A1O("mandate-transaction-id", this.A0G, list);
        }
    }

    @Override // X.AbstractC30891Zf, X.AnonymousClass1ZP
    public void A04(String str) {
        Object obj;
        Object obj2;
        Object obj3;
        Object obj4;
        Object obj5;
        try {
            super.A04(str);
            JSONObject A05 = C13000ix.A05(str);
            this.A03 = A05.optInt("v", 1);
            this.A0N = A05.optString("seqNum", this.A0N);
            this.A00 = A05.optInt("counter", 0);
            this.A0E = A05.optString("deviceId", this.A0E);
            this.A0L = A05.optString("senderVpa", this.A0L);
            this.A0M = A05.optString("senderVpaId", this.A0M);
            AnonymousClass2SM A0J = C117305Zk.A0J();
            AnonymousClass1ZR r0 = this.A08;
            if (r0 == null) {
                obj = null;
            } else {
                obj = r0.A00;
            }
            this.A08 = C117305Zk.A0I(A0J, String.class, A05.optString("senderName", (String) obj), "legalName");
            this.A0J = A05.optString("receiverVpa", this.A0J);
            this.A0K = A05.optString("receiverVpaId", this.A0K);
            AnonymousClass2SM A0J2 = C117305Zk.A0J();
            AnonymousClass1ZR r02 = this.A07;
            if (r02 == null) {
                obj2 = null;
            } else {
                obj2 = r02.A00;
            }
            this.A07 = C117305Zk.A0I(A0J2, String.class, A05.optString("receiverName", (String) obj2), "legalName");
            AnonymousClass2SM A0J3 = C117305Zk.A0J();
            AnonymousClass1ZR r03 = this.A0A;
            if (r03 == null) {
                obj3 = null;
            } else {
                obj3 = r03.A00;
            }
            this.A0A = C117305Zk.A0I(A0J3, String.class, A05.optString("blob", (String) obj3), "pin");
            this.A0P = A05.optString("token", this.A0P);
            this.A04 = A05.optLong("expiryTs", this.A04);
            this.A01 = A05.optInt("previousStatus", this.A01);
            this.A02 = A05.optInt("previousType", this.A02);
            this.A0Q = A05.optString("url", this.A0Q);
            AnonymousClass2SM A0J4 = C117305Zk.A0J();
            AnonymousClass1ZR r04 = this.A09;
            if (r04 == null) {
                obj4 = null;
            } else {
                obj4 = r04.A00;
            }
            this.A09 = C117305Zk.A0I(A0J4, String.class, A05.optString("upiBankInfo", (String) obj4), "bankInfo");
            this.A0O = A05.optString("syncStatus", this.A0O);
            this.A0H = A05.optString("mcc", this.A0H);
            this.A0I = A05.optString("purposeCode", this.A0I);
            if (A05.has("indiaUpiMandateMetadata")) {
                this.A0B = new AnonymousClass60R(A05.optString("indiaUpiMandateMetadata", null));
            }
            if (A05.has("isFirstSend")) {
                this.A0D = Boolean.valueOf(A05.optBoolean("isFirstSend", false));
            }
            if (A05.has("indiaUpiTransactionComplaintData")) {
                this.A0C = new AnonymousClass686(A05.optString("indiaUpiTransactionComplaintData", null));
            }
            this.A0G = A05.optString("mandateTransactionId", this.A0G);
            AnonymousClass2SM A0J5 = C117305Zk.A0J();
            AnonymousClass1ZR r05 = this.A06;
            if (r05 == null) {
                obj5 = null;
            } else {
                obj5 = r05.A00;
            }
            this.A06 = C117305Zk.A0I(A0J5, String.class, A05.optString("note", (String) obj5), "interopNote");
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiTransactionMetadata fromDBString threw: ", e);
        }
    }

    @Override // X.AbstractC30891Zf
    public int A05() {
        return this.A00;
    }

    @Override // X.AbstractC30891Zf
    public int A06() {
        return this.A01;
    }

    @Override // X.AbstractC30891Zf
    public long A07() {
        return this.A04;
    }

    @Override // X.AbstractC30891Zf
    public long A08() {
        return this.A05;
    }

    @Override // X.AbstractC30891Zf
    public long A09() {
        return this.A05 * 1000;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass5YY A0A() {
        return this.A0C;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0B() {
        return this.A06;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0C() {
        return this.A07;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0D() {
        return this.A08;
    }

    @Override // X.AbstractC30891Zf
    public String A0E() {
        return this.A0N;
    }

    @Override // X.AbstractC30891Zf
    public String A0F() {
        return this.A0J;
    }

    @Override // X.AbstractC30891Zf
    public String A0G() {
        return this.A0L;
    }

    @Override // X.AbstractC30891Zf
    public String A0H() {
        Object obj;
        try {
            JSONObject A0J = A0J();
            A0J.put("v", this.A03);
            String str = this.A0N;
            if (str != null) {
                A0J.put("seqNum", str);
            }
            String str2 = this.A0E;
            if (str2 != null) {
                A0J.put("deviceId", str2);
            }
            long j = this.A04;
            if (j > 0) {
                A0J.put("expiryTs", j);
            }
            int i = this.A01;
            if (i > 0) {
                A0J.put("previousStatus", i);
            }
            String str3 = this.A0J;
            if (str3 != null) {
                A0J.put("receiverVpa", str3);
            }
            String str4 = this.A0K;
            if (str4 != null) {
                A0J.put("receiverVpaId", str4);
            }
            AnonymousClass1ZR r1 = this.A07;
            if (!AnonymousClass1ZS.A03(r1)) {
                C117315Zl.A0U(r1, "receiverName", A0J);
            }
            String str5 = this.A0L;
            if (str5 != null) {
                A0J.put("senderVpa", str5);
            }
            String str6 = this.A0M;
            if (str6 != null) {
                A0J.put("senderVpaId", str6);
            }
            AnonymousClass1ZR r12 = this.A08;
            if (!AnonymousClass1ZS.A03(r12)) {
                C117315Zl.A0U(r12, "senderName", A0J);
            }
            int i2 = this.A00;
            if (i2 > 0) {
                A0J.put("counter", i2);
            }
            int i3 = this.A02;
            if (i3 > 0) {
                A0J.put("previousType", i3);
            }
            String str7 = this.A0Q;
            if (str7 != null) {
                A0J.put("url", str7);
            }
            String str8 = this.A0O;
            if (str8 != null) {
                A0J.put("syncStatus", str8);
            }
            AnonymousClass1ZR r3 = this.A09;
            if (!AnonymousClass1ZS.A03(r3)) {
                if (r3 == null) {
                    obj = null;
                } else {
                    obj = r3.A00;
                }
                A0J.put("upiBankInfo", obj);
            }
            String str9 = this.A0H;
            if (str9 != null) {
                A0J.put("mcc", str9);
            }
            String str10 = this.A0I;
            if (str10 != null) {
                A0J.put("purposeCode", str10);
            }
            AnonymousClass60R r0 = this.A0B;
            if (r0 != null) {
                A0J.put("indiaUpiMandateMetadata", r0.A01());
            }
            Boolean bool = this.A0D;
            if (bool != null) {
                A0J.put("isFirstSend", bool);
            }
            AnonymousClass686 r02 = this.A0C;
            if (r02 != null) {
                A0J.put("indiaUpiTransactionComplaintData", r02.A00());
            }
            String str11 = this.A0G;
            if (str11 != null) {
                A0J.put("mandateTransactionId", str11);
            }
            if (!AnonymousClass1ZS.A02(this.A06)) {
                C117315Zl.A0U(this.A06, "note", A0J);
            }
            return A0J.toString();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiTransactionMetadata toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AbstractC30891Zf
    public String A0I() {
        Object obj;
        Object obj2;
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("v", this.A03);
            AnonymousClass1ZR r2 = this.A0A;
            if (!AnonymousClass1ZS.A03(r2)) {
                if (r2 == null) {
                    obj2 = null;
                } else {
                    obj2 = r2.A00;
                }
                A0a.put("blob", obj2);
            }
            if (!TextUtils.isEmpty(this.A0P)) {
                A0a.put("token", this.A0P);
            }
            String str = this.A0L;
            if (str != null) {
                A0a.put("senderVpa", str);
            }
            String str2 = this.A0M;
            if (str2 != null) {
                A0a.put("senderVpaId", str2);
            }
            AnonymousClass1ZR r1 = this.A08;
            if (!AnonymousClass1ZS.A03(r1)) {
                C117315Zl.A0U(r1, "senderName", A0a);
            }
            String str3 = this.A0J;
            if (str3 != null) {
                A0a.put("receiverVpa", str3);
            }
            String str4 = this.A0K;
            if (str4 != null) {
                A0a.put("receiverVpaId", str4);
            }
            AnonymousClass1ZR r12 = this.A07;
            if (!AnonymousClass1ZS.A03(r12)) {
                C117315Zl.A0U(r12, "receiverName", A0a);
            }
            String str5 = this.A0E;
            if (str5 != null) {
                A0a.put("deviceId", str5);
            }
            AnonymousClass1ZR r22 = this.A09;
            if (!AnonymousClass1ZS.A03(r22)) {
                if (r22 == null) {
                    obj = null;
                } else {
                    obj = r22.A00;
                }
                A0a.put("upiBankInfo", obj);
            }
            if (!AnonymousClass1ZS.A02(this.A06)) {
                C117315Zl.A0U(this.A06, "note", A0a);
            }
            return A0a.toString();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiTransactionMetadata toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0K(int i) {
        this.A00 = i;
    }

    @Override // X.AbstractC30891Zf
    public void A0L(int i) {
        this.A01 = i;
    }

    @Override // X.AbstractC30891Zf
    public void A0M(int i) {
        this.A02 = i;
    }

    @Override // X.AbstractC30891Zf
    public void A0N(long j) {
        this.A04 = j;
    }

    @Override // X.AbstractC30891Zf
    public void A0O(long j) {
        this.A05 = j;
    }

    @Override // X.AbstractC30891Zf
    public void A0R(AbstractC30891Zf r8) {
        super.A0R(r8);
        C119835fB r82 = (C119835fB) r8;
        String str = r82.A0N;
        if (str != null) {
            this.A0N = str;
        }
        String str2 = r82.A0E;
        if (str2 != null) {
            this.A0E = str2;
        }
        String str3 = r82.A0J;
        if (str3 != null) {
            this.A0J = str3;
        }
        String str4 = r82.A0K;
        if (str4 != null) {
            this.A0K = str4;
        }
        AnonymousClass1ZR r0 = r82.A07;
        if (r0 != null) {
            this.A07 = r0;
        }
        String str5 = r82.A0L;
        if (str5 != null) {
            this.A0L = str5;
        }
        String str6 = r82.A0M;
        if (str6 != null) {
            this.A0M = str6;
        }
        AnonymousClass1ZR r1 = r82.A08;
        if (!AnonymousClass1ZS.A03(r1)) {
            this.A08 = r1;
        }
        long j = r82.A04;
        if (j > 0) {
            this.A04 = j;
        }
        int i = r82.A01;
        if (i > 0) {
            this.A01 = i;
        }
        int i2 = r82.A00;
        if (i2 > 0) {
            this.A00 = i2;
        }
        int i3 = r82.A02;
        if (i3 > 0) {
            this.A02 = i3;
        }
        String str7 = r82.A0Q;
        if (str7 != null) {
            this.A0Q = str7;
        }
        AnonymousClass1ZR r12 = r82.A09;
        if (!AnonymousClass1ZS.A03(r12)) {
            this.A09 = r12;
        }
        String str8 = r82.A0O;
        if (str8 != null) {
            this.A0O = str8;
        }
        String str9 = r82.A0H;
        if (str9 != null) {
            this.A0H = str9;
        }
        String str10 = r82.A0I;
        if (str10 != null) {
            this.A0I = str10;
        }
        AnonymousClass60R r4 = r82.A0B;
        if (r4 != null) {
            AnonymousClass60R r3 = this.A0B;
            AnonymousClass1ZR r02 = r4.A08;
            if (r02 != null) {
                r3.A08 = r02;
            }
            AnonymousClass1ZR r03 = r4.A07;
            if (r03 != null) {
                r3.A07 = r03;
            }
            AnonymousClass1ZR r04 = r4.A0A;
            if (r04 != null) {
                r3.A0A = r04;
            }
            r3.A0G = r4.A0G;
            String str11 = r4.A0F;
            if (str11 != null) {
                r3.A0F = str11;
            }
            r3.A0K = r4.A0K;
            r3.A0L = r4.A0L;
            long j2 = r4.A02;
            if (j2 > 0) {
                r3.A02 = j2;
            }
            long j3 = r4.A01;
            if (j3 > 0) {
                r3.A01 = j3;
            }
            String str12 = r4.A0D;
            if (str12 != null) {
                r3.A0D = str12;
            }
            String str13 = r4.A0H;
            if (str13 != null) {
                r3.A0H = str13;
            }
            long j4 = r4.A04;
            if (j4 > 0) {
                r3.A04 = j4;
            }
            long j5 = r4.A03;
            if (j5 > 0) {
                r3.A03 = j5;
            }
            int i4 = r4.A00;
            if (i4 > 0) {
                r3.A00 = i4;
            }
            AnonymousClass1ZR r05 = r4.A09;
            if (r05 != null) {
                r3.A09 = r05;
            }
            AnonymousClass20C r06 = r4.A05;
            if (r06 != null) {
                r3.A05 = r06;
            }
            AnonymousClass1ZR r07 = r4.A06;
            if (r07 != null) {
                r3.A06 = r07;
            }
            String str14 = r4.A0E;
            if (str14 != null) {
                r3.A0E = str14;
            }
            String str15 = r4.A0J;
            if (str15 != null) {
                r3.A0J = str15;
            }
            String str16 = r4.A0I;
            if (str16 != null) {
                r3.A0I = str16;
            }
            r3.A0C = r4.A0C;
            r3.A0M = r4.A0M;
            r3.A0B = r4.A0B;
        }
        Boolean bool = r82.A0D;
        if (bool != null) {
            this.A0D = bool;
        }
        AnonymousClass686 r42 = r82.A0C;
        if (r42 != null) {
            AnonymousClass686 r32 = this.A0C;
            if (r32 == null) {
                this.A0C = new AnonymousClass686(r42.A00());
            } else {
                r32.A03 = r42.A03;
                long j6 = r42.A00;
                if (j6 > 0) {
                    r32.A00 = j6;
                }
                long j7 = r42.A01;
                if (j7 > 0) {
                    r32.A01 = j7;
                }
                String str17 = r42.A02;
                if (str17 != null) {
                    r32.A02 = str17;
                }
            }
        }
        String str18 = r82.A0G;
        if (str18 != null) {
            this.A0G = str18;
        }
        AnonymousClass1ZR r13 = r82.A06;
        if (!AnonymousClass1ZS.A03(r13)) {
            this.A06 = r13;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0S(String str) {
        this.A0N = str;
    }

    @Override // X.AbstractC30891Zf
    public void A0T(String str) {
        this.A06 = C117305Zk.A0I(C117305Zk.A0J(), String.class, str, "interopNote");
    }

    @Override // X.AbstractC30891Zf
    public void A0U(String str) {
        this.A0J = str;
    }

    @Override // X.AbstractC30891Zf
    public void A0V(String str) {
        this.A0L = str;
    }

    @Override // X.AbstractC30891Zf
    public boolean A0X() {
        return "MISSING_FIELD_NOT_PARTIAL".equals(this.A0O);
    }

    @Override // X.AbstractC30891Zf
    public boolean A0Y(AnonymousClass1IR r3) {
        if (TextUtils.isEmpty(this.A0L) || TextUtils.isEmpty(this.A0J)) {
            return true;
        }
        return !"FULL".equals(this.A0O) && r3.A0A() && TextUtils.isEmpty(r3.A0F);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String str;
        String str2;
        Object valueOf;
        AnonymousClass60R r0 = this.A0B;
        String str3 = "null";
        if (r0 == null) {
            obj = str3;
        } else {
            obj = r0.toString();
        }
        String str4 = "order = [";
        AnonymousClass686 r02 = this.A0C;
        if (r02 != null) {
            str3 = r02.toString();
        }
        C38171nd r2 = super.A02;
        if (r2 != null) {
            StringBuilder A0j = C12960it.A0j(str4);
            A0j.append("id: ");
            StringBuilder A0j2 = C12960it.A0j(C12960it.A0d(C1309060l.A00(r2.A01), A0j));
            A0j2.append("expiryTsInSec:");
            C38171nd r22 = super.A02;
            A0j2.append(r22.A00);
            StringBuilder A0j3 = C12960it.A0j(A0j2.toString());
            A0j3.append("messageId:");
            str4 = C12960it.A0d(C1309060l.A00(r22.A02), A0j3);
        }
        String A0d = C12960it.A0d("]", C12960it.A0j(str4));
        StringBuilder A0k = C12960it.A0k("[ seq-no: ");
        C1309060l.A03(A0k, this.A0N);
        A0k.append(" timestamp: ");
        A0k.append(this.A05);
        A0k.append(" deviceId: ");
        A0k.append(this.A0E);
        A0k.append(" sender: ");
        A0k.append(C1309060l.A02(this.A0L));
        A0k.append(" senderVpaId: ");
        A0k.append(this.A0M);
        A0k.append(" senderName: ");
        AnonymousClass1ZR r03 = this.A08;
        String str5 = null;
        if (r03 != null) {
            str = r03.toString();
        } else {
            str = null;
        }
        C1309060l.A03(A0k, str);
        A0k.append(" receiver: ");
        A0k.append(C1309060l.A02(this.A0J));
        A0k.append(" receiverVpaId: ");
        A0k.append(C1309060l.A02(this.A0K));
        A0k.append(" receiverName : ");
        AnonymousClass1ZR r04 = this.A07;
        if (r04 != null) {
            str2 = r04.toString();
        } else {
            str2 = null;
        }
        C1309060l.A03(A0k, str2);
        A0k.append(" encryptedKeyLength: ");
        AnonymousClass1ZR r7 = this.A0A;
        if (AnonymousClass1ZS.A03(r7)) {
            valueOf = "0";
        } else {
            valueOf = Integer.valueOf(((String) r7.A00).length());
        }
        A0k.append(valueOf);
        A0k.append(" previousType: ");
        A0k.append(this.A02);
        A0k.append(" previousStatus: ");
        A0k.append(this.A01);
        A0k.append(" token: ");
        C1309060l.A03(A0k, this.A0P);
        A0k.append(" url: ");
        C1309060l.A03(A0k, this.A0Q);
        A0k.append(" upiBankInfo: ");
        A0k.append(this.A09);
        A0k.append(" order : ");
        A0k.append(A0d);
        A0k.append(" mcc: ");
        C1309060l.A03(A0k, this.A0H);
        A0k.append(" purposeCode: ");
        C1309060l.A03(A0k, this.A0I);
        A0k.append(" isFirstSend: ");
        A0k.append(this.A0D);
        A0k.append(" indiaUpiMandateMetadata: {");
        A0k.append(obj);
        A0k.append("} ] indiaUpiTransactionComplaintData: {");
        A0k.append(str3);
        A0k.append("}  mandateTransactionId: ");
        C1309060l.A03(A0k, this.A0G);
        A0k.append(" note : ");
        AnonymousClass1ZR r05 = this.A06;
        if (r05 != null) {
            str5 = r05.toString();
        }
        C1309060l.A03(A0k, str5);
        return C12960it.A0d("]", A0k);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v31 */
    /* JADX WARN: Type inference failed for: r0v35 */
    /* JADX WARN: Type inference failed for: r0v39 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC30891Zf, android.os.Parcelable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeToParcel(android.os.Parcel r3, int r4) {
        /*
            r2 = this;
            super.writeToParcel(r3, r4)
            X.1ZR r0 = r2.A0A
            r3.writeParcelable(r0, r4)
            java.lang.String r0 = r2.A0P
            r3.writeString(r0)
            java.lang.String r0 = r2.A0N
            r3.writeString(r0)
            java.lang.String r0 = r2.A0L
            r3.writeString(r0)
            java.lang.String r0 = r2.A0M
            r3.writeString(r0)
            X.1ZR r0 = r2.A08
            java.lang.Object r0 = X.C117295Zj.A0R(r0)
            java.lang.String r0 = (java.lang.String) r0
            r3.writeString(r0)
            java.lang.String r0 = r2.A0J
            r3.writeString(r0)
            java.lang.String r0 = r2.A0K
            r3.writeString(r0)
            X.1ZR r0 = r2.A07
            java.lang.Object r0 = X.C117295Zj.A0R(r0)
            java.lang.String r0 = (java.lang.String) r0
            r3.writeString(r0)
            long r0 = r2.A05
            r3.writeLong(r0)
            java.lang.String r0 = r2.A0E
            r3.writeString(r0)
            long r0 = r2.A04
            r3.writeLong(r0)
            int r0 = r2.A01
            r3.writeInt(r0)
            int r0 = r2.A00
            r3.writeInt(r0)
            int r0 = r2.A02
            r3.writeInt(r0)
            java.lang.String r0 = r2.A0Q
            r3.writeString(r0)
            X.1ZR r0 = r2.A09
            r3.writeParcelable(r0, r4)
            java.lang.String r0 = r2.A0F
            r3.writeString(r0)
            java.lang.String r0 = r2.A0O
            r3.writeString(r0)
            java.lang.String r0 = r2.A0H
            r3.writeString(r0)
            java.lang.String r0 = r2.A0I
            r3.writeString(r0)
            X.60R r0 = r2.A0B
            r1 = 0
            if (r0 != 0) goto L_0x00a8
            r0 = r1
        L_0x007e:
            r3.writeString(r0)
            java.lang.Boolean r0 = r2.A0D
            if (r0 != 0) goto L_0x009f
            r0 = -1
        L_0x0086:
            r3.writeInt(r0)
            X.686 r0 = r2.A0C
            if (r0 == 0) goto L_0x0091
            java.lang.String r1 = r0.A00()
        L_0x0091:
            r3.writeString(r1)
            java.lang.String r0 = r2.A0G
            r3.writeString(r0)
            X.1ZR r0 = r2.A06
            r3.writeParcelable(r0, r4)
            return
        L_0x009f:
            boolean r0 = r0.booleanValue()
            boolean r0 = X.C12960it.A1S(r0)
            goto L_0x0086
        L_0x00a8:
            java.lang.String r0 = r0.A01()
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119835fB.writeToParcel(android.os.Parcel, int):void");
    }
}
