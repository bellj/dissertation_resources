package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0aW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08060aW implements AbstractC12030hG, AbstractC12860ig, AbstractC12870ih {
    public AnonymousClass0QR A00;
    public AnonymousClass0Gu A01;
    public final int A02;
    public final Paint A03;
    public final Path A04;
    public final RectF A05;
    public final AnonymousClass036 A06 = new AnonymousClass036();
    public final AnonymousClass036 A07 = new AnonymousClass036();
    public final AnonymousClass0AA A08;
    public final AnonymousClass0QR A09;
    public final AnonymousClass0QR A0A;
    public final AnonymousClass0QR A0B;
    public final AnonymousClass0QR A0C;
    public final AnonymousClass0J4 A0D;
    public final AbstractC08070aX A0E;
    public final String A0F;
    public final List A0G;
    public final boolean A0H;

    public C08060aW(AnonymousClass0AA r5, C08160ag r6, AbstractC08070aX r7) {
        Path path = new Path();
        this.A04 = path;
        this.A03 = new C020609t(1);
        this.A05 = new RectF();
        this.A0G = new ArrayList();
        this.A0E = r7;
        this.A0F = r6.A06;
        this.A0H = r6.A07;
        this.A08 = r5;
        this.A0D = r6.A05;
        path.setFillType(r6.A00);
        C05540Py r2 = r5.A04;
        this.A02 = (int) (((float) ((long) (((r2.A00 - r2.A02) / r2.A01) * 1000.0f))) / 32.0f);
        AnonymousClass0Gw r1 = new AnonymousClass0Gw(r6.A01.A00);
        this.A09 = r1;
        r1.A07.add(this);
        r7.A03(r1);
        AnonymousClass0H0 r12 = new AnonymousClass0H0(r6.A02.A00);
        this.A0B = r12;
        r12.A07.add(this);
        r7.A03(r12);
        AnonymousClass0H2 r13 = new AnonymousClass0H2(r6.A04.A00);
        this.A0C = r13;
        r13.A07.add(this);
        r7.A03(r13);
        AnonymousClass0H2 r14 = new AnonymousClass0H2(r6.A03.A00);
        this.A0A = r14;
        r14.A07.add(this);
        r7.A03(r14);
    }

    public final int A00() {
        float f = this.A0C.A02;
        float f2 = (float) this.A02;
        int round = Math.round(f * f2);
        int round2 = Math.round(this.A0A.A02 * f2);
        int round3 = Math.round(this.A09.A02 * f2);
        int i = 17;
        if (round != 0) {
            i = 527 * round;
        }
        if (round2 != 0) {
            i = i * 31 * round2;
        }
        return round3 != 0 ? i * 31 * round3 : i;
    }

    public final int[] A01(int[] iArr) {
        AnonymousClass0Gu r0 = this.A01;
        if (r0 != null) {
            Integer[] numArr = (Integer[]) r0.A03();
            int length = iArr.length;
            int length2 = numArr.length;
            int i = 0;
            if (length != length2) {
                iArr = new int[length2];
                while (i < length2) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            } else {
                while (i < length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            }
        }
        return iArr;
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r4, Object obj) {
        AbstractC08070aX r1;
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A0S) {
            this.A0B.A08(r4);
            return;
        }
        if (obj == AbstractC12810iX.A00) {
            AnonymousClass0QR r12 = this.A00;
            if (r12 != null) {
                this.A0E.A0O.remove(r12);
            }
            if (r4 == null) {
                this.A00 = null;
                return;
            }
            AnonymousClass0Gu r02 = new AnonymousClass0Gu(r4, null);
            this.A00 = r02;
            r02.A07.add(this);
            r1 = this.A0E;
            r0 = this.A00;
        } else if (obj == AbstractC12810iX.A0V) {
            AnonymousClass0Gu r13 = this.A01;
            if (r13 != null) {
                this.A0E.A0O.remove(r13);
            }
            if (r4 == null) {
                this.A01 = null;
                return;
            }
            this.A06.A05();
            this.A07.A05();
            AnonymousClass0Gu r03 = new AnonymousClass0Gu(r4, null);
            this.A01 = r03;
            r03.A07.add(this);
            r1 = this.A0E;
            r0 = this.A01;
        } else {
            return;
        }
        r1.A03(r0);
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        Shader shader;
        if (!this.A0H) {
            Path path = this.A04;
            path.reset();
            int i2 = 0;
            while (true) {
                List list = this.A0G;
                if (i2 >= list.size()) {
                    break;
                }
                path.addPath(((AbstractC12850if) list.get(i2)).AF0(), matrix);
                i2++;
            }
            path.computeBounds(this.A05, false);
            AnonymousClass0J4 r3 = this.A0D;
            AnonymousClass0J4 r1 = AnonymousClass0J4.LINEAR;
            int A00 = A00();
            if (r3 == r1) {
                AnonymousClass036 r5 = this.A06;
                long j = (long) A00;
                shader = (Shader) r5.A04(j, null);
                if (shader == null) {
                    PointF pointF = (PointF) this.A0C.A03();
                    PointF pointF2 = (PointF) this.A0A.A03();
                    AnonymousClass0N8 r4 = (AnonymousClass0N8) this.A09.A03();
                    shader = new LinearGradient(pointF.x, pointF.y, pointF2.x, pointF2.y, A01(r4.A01), r4.A00, Shader.TileMode.CLAMP);
                    r5.A09(j, shader);
                }
            } else {
                AnonymousClass036 r7 = this.A07;
                long j2 = (long) A00;
                shader = (Shader) r7.A04(j2, null);
                if (shader == null) {
                    PointF pointF3 = (PointF) this.A0C.A03();
                    PointF pointF4 = (PointF) this.A0A.A03();
                    AnonymousClass0N8 r12 = (AnonymousClass0N8) this.A09.A03();
                    int[] A01 = A01(r12.A01);
                    float[] fArr = r12.A00;
                    float f = pointF3.x;
                    float f2 = pointF3.y;
                    float hypot = (float) Math.hypot((double) (pointF4.x - f), (double) (pointF4.y - f2));
                    if (hypot <= 0.0f) {
                        hypot = 0.001f;
                    }
                    shader = new RadialGradient(f, f2, hypot, A01, fArr, Shader.TileMode.CLAMP);
                    r7.A09(j2, shader);
                }
            }
            shader.setLocalMatrix(matrix);
            Paint paint = this.A03;
            paint.setShader(shader);
            AnonymousClass0QR r0 = this.A00;
            if (r0 != null) {
                paint.setColorFilter((ColorFilter) r0.A03());
            }
            paint.setAlpha(Math.max(0, Math.min(255, (int) ((((((float) i) / 255.0f) * ((float) ((Number) this.A0B.A03()).intValue())) / 100.0f) * 255.0f))));
            canvas.drawPath(path, paint);
            AnonymousClass0MI.A00();
        }
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        Path path = this.A04;
        path.reset();
        int i = 0;
        while (true) {
            List list = this.A0G;
            if (i < list.size()) {
                path.addPath(((AbstractC12850if) list.get(i)).AF0(), matrix);
                i++;
            } else {
                path.computeBounds(rectF, false);
                rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
                return;
            }
        }
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A08.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list2.size(); i++) {
            Object obj = list2.get(i);
            if (obj instanceof AbstractC12850if) {
                this.A0G.add(obj);
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A0F;
    }
}
