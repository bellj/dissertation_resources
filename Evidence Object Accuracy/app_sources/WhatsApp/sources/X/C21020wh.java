package X;

import android.content.Context;

/* renamed from: X.0wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21020wh extends AbstractC18220s6 {
    public final Context A00;
    public final C14820m6 A01;
    public final C14860mA A02;

    public C21020wh(Context context, C14820m6 r2, C14860mA r3) {
        super(context);
        this.A00 = context;
        this.A02 = r3;
        this.A01 = r2;
    }
}
