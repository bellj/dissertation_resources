package X;

import android.text.TextUtils;
import android.view.View;
import com.google.android.material.tabs.TabLayout;

/* renamed from: X.3FN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FN {
    public int A00 = -1;
    public View A01;
    public C53092ck A02;
    public TabLayout A03;
    public CharSequence A04;
    public CharSequence A05;
    public Object A06;

    public void A00() {
        TabLayout tabLayout = this.A03;
        if (tabLayout != null) {
            tabLayout.A0G(this, true);
            return;
        }
        throw C12970iu.A0f("Tab not attached to a TabLayout");
    }

    public void A01(int i) {
        TabLayout tabLayout = this.A03;
        if (tabLayout != null) {
            A02(tabLayout.getResources().getText(i));
            return;
        }
        throw C12970iu.A0f("Tab not attached to a TabLayout");
    }

    public void A02(CharSequence charSequence) {
        if (TextUtils.isEmpty(this.A04) && !TextUtils.isEmpty(charSequence)) {
            this.A02.setContentDescription(charSequence);
        }
        this.A05 = charSequence;
        C53092ck r0 = this.A02;
        if (r0 != null) {
            r0.A00();
        }
    }
}
