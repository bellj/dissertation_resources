package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.concurrent.Executor;

/* renamed from: X.0k7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass0k7 implements AbstractC13670k8 {
    public AbstractC13620k1 A00;
    public final Object A01 = new Object();
    public final Executor A02;

    public AnonymousClass0k7(AbstractC13620k1 r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r2;
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        if (r4.A05) {
            synchronized (this.A01) {
                if (this.A00 != null) {
                    this.A02.execute(new RunnableBRunnable0Shape0S0100000_I0(this, 9));
                }
            }
        }
    }
}
