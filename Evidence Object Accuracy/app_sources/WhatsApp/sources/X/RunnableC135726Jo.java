package X;

import android.graphics.Point;

/* renamed from: X.6Jo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135726Jo implements Runnable {
    public final /* synthetic */ Point A00;
    public final /* synthetic */ EnumC124565pk A01;
    public final /* synthetic */ AbstractC136156Lf A02;
    public final /* synthetic */ C129305xV A03;

    public RunnableC135726Jo(Point point, EnumC124565pk r2, AbstractC136156Lf r3, C129305xV r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = point;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A02.AQe(this.A00, this.A01);
    }
}
