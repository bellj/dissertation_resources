package X;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.widget.ImageView;

/* renamed from: X.0Bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02330Bg extends ImageView {
    public Animatable A00;
    public boolean A01;

    public C02330Bg(Context context) {
        super(context);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        Animatable animatable;
        super.onAttachedToWindow();
        if (this.A01 && (animatable = this.A00) != null && !animatable.isRunning()) {
            this.A00.start();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Animatable animatable = this.A00;
        if (animatable != null && animatable.isRunning()) {
            this.A00.stop();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void setVisibility(int i) {
        Animatable animatable;
        super.setVisibility(i);
        if (getVisibility() != 0) {
            Animatable animatable2 = this.A00;
            if (animatable2 != null && animatable2.isRunning()) {
                this.A00.stop();
            }
        } else if (this.A01 && (animatable = this.A00) != null && !animatable.isRunning()) {
            this.A00.start();
        }
    }
}
