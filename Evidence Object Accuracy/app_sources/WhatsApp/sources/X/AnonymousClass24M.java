package X;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/* renamed from: X.24M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24M extends AbstractC16350or implements LocationListener {
    public int A00 = 0;
    public int A01 = 15;
    public Location A02 = null;
    public Location A03 = null;
    public Location A04;
    public boolean A05;
    public final C22490zA A06;
    public final AbstractC15710nm A07;
    public final C244615p A08;
    public final C18790t3 A09;
    public final C14830m7 A0A;
    public final C15890o4 A0B;
    public final C15650ng A0C;
    public final C15600nX A0D;
    public final AnonymousClass12H A0E;
    public final C22830zi A0F;
    public final C18810t5 A0G;
    public final C16030oK A0H;
    public final AnonymousClass1XP A0I;

    public AnonymousClass24M(C22490zA r7, AbstractC15710nm r8, C244615p r9, C18790t3 r10, C14830m7 r11, C15890o4 r12, C15650ng r13, C15600nX r14, AnonymousClass12H r15, C22830zi r16, C18810t5 r17, C16030oK r18, AnonymousClass1XP r19) {
        this.A0A = r11;
        this.A09 = r10;
        this.A0I = r19;
        this.A07 = r8;
        this.A0C = r13;
        this.A0E = r15;
        this.A0G = r17;
        this.A0B = r12;
        this.A0F = r16;
        this.A0H = r18;
        this.A0D = r14;
        this.A06 = r7;
        if (r19.A00 == 0.0d || r19.A01 == 0.0d) {
            this.A08 = r9;
            return;
        }
        Location location = new Location("");
        this.A04 = location;
        location.setLatitude(r19.A00);
        this.A04.setLongitude(r19.A01);
        this.A04.setTime(r19.A0I);
        this.A05 = true;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:91:0x00ac */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:32:0x00f9 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:37:0x0102 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:39:0x0107 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:63:0x01aa */
    /* JADX DEBUG: Multi-variable search result rejected for r10v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r11v1, resolved type: android.graphics.Bitmap */
    /* JADX DEBUG: Multi-variable search result rejected for r3v7, resolved type: android.graphics.Bitmap */
    /* JADX DEBUG: Multi-variable search result rejected for r10v8, resolved type: java.lang.StringBuilder */
    /* JADX DEBUG: Multi-variable search result rejected for r4v12, resolved type: byte[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v2 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r11v8, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0100, code lost:
        if (0 != 0) goto L_0x0102;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] A00(X.C18790t3 r17, X.C37581mf r18, double r19, double r21, int r23) {
        /*
        // Method dump skipped, instructions count: 438
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass24M.A00(X.0t3, X.1mf, double, double, int):byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ba, code lost:
        if (r10 != false) goto L_0x012a;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A07(java.lang.Object r25) {
        /*
        // Method dump skipped, instructions count: 568
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass24M.A07(java.lang.Object):void");
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        System.currentTimeMillis();
        location.getTime();
        location.getAccuracy();
        if (AnonymousClass13N.A03(location, this.A02)) {
            this.A02 = location;
        }
        int i = this.A00 + 1;
        this.A00 = i;
        if (i >= 2 || location.getAccuracy() < 80.0f) {
            this.A03 = location;
            this.A05 = true;
        }
    }

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
