package X;

import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;

/* renamed from: X.3Uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68323Uy implements AbstractC116475Vp {
    public final /* synthetic */ AnonymousClass19X A00;
    public final /* synthetic */ UserJid A01;

    public C68323Uy(AnonymousClass19X r1, UserJid userJid) {
        this.A00 = r1;
        this.A01 = userJid;
    }

    @Override // X.AbstractC116475Vp
    public void AQN(AnonymousClass28H r6, int i) {
        AnonymousClass19X r2 = this.A00;
        r2.A00 = false;
        if (i == 406 || i == 404) {
            r2.A05.A0D(this.A01);
        }
        AnonymousClass19S r0 = r2.A04;
        UserJid userJid = this.A01;
        Iterator A00 = AbstractC16230of.A00(r0);
        while (A00.hasNext()) {
            AnonymousClass3VN r1 = (AnonymousClass3VN) A00.next();
            CatalogMediaCard catalogMediaCard = r1.A06;
            if (C29941Vi.A00(catalogMediaCard.A0G, userJid)) {
                AnonymousClass19X r02 = r1.A05;
                if (!r02.A05.A0J(catalogMediaCard.A0G)) {
                    int i2 = R.string.catalog_error_retrieving_products;
                    if (i != -1) {
                        i2 = R.string.catalog_error_no_products;
                        if (i != 404) {
                            i2 = R.string.catalog_server_error_retrieving_products;
                        }
                    }
                    catalogMediaCard.setError(i2);
                }
            }
        }
    }

    @Override // X.AbstractC116475Vp
    public void AQO(C44721zR r5, AnonymousClass28H r6) {
        AnonymousClass19X r3 = this.A00;
        r3.A00 = false;
        if (r6.A06 == null) {
            C19850um r0 = r3.A05;
            UserJid userJid = this.A01;
            r0.A0B(r5, userJid, false);
            r3.A04.A05(userJid);
        }
    }
}
