package X;

/* renamed from: X.5wA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128475wA {
    public final /* synthetic */ AnonymousClass66H A00;

    public C128475wA(AnonymousClass66H r1) {
        this.A00 = r1;
    }

    public void A00(C130635zj r5) {
        C129875yR r0;
        if (r5.A04 == null && r5.A01 == null) {
            AnonymousClass66H r2 = this.A00;
            r2.A08 = Boolean.FALSE;
            r2.A06 = new AnonymousClass6L0("Could not retrieve data from photo processor.");
            r0 = r2.A03;
        } else {
            AnonymousClass66H r3 = this.A00;
            r3.A08 = Boolean.TRUE;
            r3.A07 = r5;
            if (r3.A05) {
                C129475xm r22 = r3.A01;
                if (r22.A01[((r22.A00 + 3) - 1) % 3] == null) {
                    return;
                }
            }
            r0 = r3.A03;
        }
        r0.A01();
    }
}
