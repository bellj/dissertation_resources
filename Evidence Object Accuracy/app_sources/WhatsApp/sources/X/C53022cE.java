package X;

import android.content.Context;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.status.playback.content.BlurFrameLayout;
import com.whatsapp.status.playback.widget.VoiceStatusContentView;

/* renamed from: X.2cE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53022cE extends FrameLayout implements AnonymousClass004 {
    public C30421Xi A00;
    public BlurFrameLayout A01;
    public VoiceStatusContentView A02;
    public AnonymousClass2P7 A03;
    public boolean A04;

    public C53022cE(Context context) {
        super(context);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        FrameLayout.inflate(context, R.layout.status_playback_voice, this);
        this.A02 = (VoiceStatusContentView) AnonymousClass028.A0D(this, R.id.message_voice);
        this.A01 = (BlurFrameLayout) AnonymousClass028.A0D(this, R.id.blur_container);
        this.A02.A03 = new AnonymousClass59Q(this);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    private void setBackgroundColorFromMessage(C30421Xi r4) {
        C12970iu.A18(getContext(), this, R.color.group_iris);
        C12970iu.A18(getContext(), this.A01, R.color.group_iris);
    }

    public void setBlurEnabled(boolean z) {
        this.A01.setBlurEnabled(z);
    }

    public final void setMessage(C30421Xi r2, AnonymousClass1J1 r3) {
        this.A00 = r2;
        setBackgroundColorFromMessage(r2);
        this.A02.setVoiceMessage(r2, r3);
    }
}
