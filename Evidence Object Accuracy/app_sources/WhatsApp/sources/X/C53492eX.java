package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.InteractiveMessageButton;

/* renamed from: X.2eX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53492eX extends AnonymousClass04v {
    public final /* synthetic */ InteractiveMessageButton A00;

    public C53492eX(InteractiveMessageButton interactiveMessageButton) {
        this.A00 = interactiveMessageButton;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        C12970iu.A1O(r4, this.A00.getContext().getString(R.string.accessibility_action_click_product_list_button));
    }
}
