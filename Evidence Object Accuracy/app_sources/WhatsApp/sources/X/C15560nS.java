package X;

import java.util.regex.Pattern;

/* renamed from: X.0nS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15560nS {
    public static final AnonymousClass01H A03 = new C002601e(null, new AnonymousClass01N() { // from class: X.5EE
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return Pattern.compile("\\+12225551[0-9]{3}");
        }
    });
    public final C248917h A00;
    public final C15600nX A01;
    public final C15440nG A02;

    public C15560nS(C248917h r1, C15600nX r2, C15440nG r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean A00(C15370n3 r3) {
        String A02 = C248917h.A02((AbstractC14640lm) r3.A0B(AbstractC14640lm.class));
        if (A02 == null) {
            return false;
        }
        return ((Pattern) A03.get()).matcher(A02).matches();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(X.AbstractC14640lm r5) {
        /*
            r4 = this;
            X.0nG r0 = r4.A02
            X.0nH r1 = r0.A00
            X.0oW r0 = X.AbstractC15460nI.A29
            int r2 = r1.A02(r0)
            r1 = 20
            r0 = 0
            if (r2 == r1) goto L_0x0010
            r0 = 1
        L_0x0010:
            r3 = 1
            if (r0 == 0) goto L_0x006b
            boolean r0 = X.C15380n4.A0J(r5)
            if (r0 == 0) goto L_0x0054
            X.0nU r1 = X.C15580nU.A02(r5)
            X.AnonymousClass009.A05(r1)
            X.0nX r0 = r4.A01
            X.1YM r0 = r0.A02(r1)
            X.1JO r0 = r0.A06()
            java.util.Iterator r2 = r0.iterator()
        L_0x002e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x006b
            java.lang.Object r0 = r2.next()
            X.0lm r0 = (X.AbstractC14640lm) r0
            java.lang.String r1 = X.C248917h.A02(r0)
            if (r1 == 0) goto L_0x0052
            X.01H r0 = X.C15560nS.A03
            java.lang.Object r0 = r0.get()
            java.util.regex.Pattern r0 = (java.util.regex.Pattern) r0
            java.util.regex.Matcher r0 = r0.matcher(r1)
            boolean r0 = r0.matches()
            if (r0 != 0) goto L_0x002e
        L_0x0052:
            r0 = 0
            return r0
        L_0x0054:
            java.lang.String r1 = X.C248917h.A02(r5)
            if (r1 == 0) goto L_0x0052
            X.01H r0 = X.C15560nS.A03
            java.lang.Object r0 = r0.get()
            java.util.regex.Pattern r0 = (java.util.regex.Pattern) r0
            java.util.regex.Matcher r0 = r0.matcher(r1)
            boolean r0 = r0.matches()
            return r0
        L_0x006b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15560nS.A01(X.0lm):boolean");
    }
}
