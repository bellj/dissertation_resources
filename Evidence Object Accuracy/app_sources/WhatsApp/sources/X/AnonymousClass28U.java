package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.28U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28U extends AnonymousClass1I5 {
    public int index = 0;
    public final /* synthetic */ Iterator[] val$elements;

    public AnonymousClass28U(Iterator[] itArr) {
        this.val$elements = itArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.index < this.val$elements.length;
    }

    @Override // java.util.Iterator
    public Iterator next() {
        if (hasNext()) {
            Iterator[] itArr = this.val$elements;
            int i = this.index;
            Iterator it = itArr[i];
            itArr[i] = null;
            this.index = i + 1;
            return it;
        }
        throw new NoSuchElementException();
    }
}
