package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.4Ps  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90914Ps {
    public final RecyclerView A00;
    public final C54122gD A01 = new C54122gD();
    public final AbstractC116555Vx A02;

    public C90914Ps(RecyclerView recyclerView, AbstractC116555Vx r4) {
        this.A00 = recyclerView;
        this.A02 = r4;
        RecyclerView recyclerView2 = this.A00;
        recyclerView2.setNestedScrollingEnabled(true);
        recyclerView2.setAdapter(this.A01);
    }
}
