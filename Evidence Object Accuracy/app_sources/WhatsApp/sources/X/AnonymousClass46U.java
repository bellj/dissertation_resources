package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.46U  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass46U extends AbstractC460224d {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(62);

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass46U(String str) {
        super(str, "DOC_UPLOAD");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
    }
}
