package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.1sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40981sh {
    public static long A00(AnonymousClass1V8 r3) {
        String A0I = r3.A0I("last", null);
        if (A0I == null) {
            return System.currentTimeMillis();
        }
        if ("deny".equals(A0I) || "error".equals(A0I) || "none".equals(A0I)) {
            return 0;
        }
        return Long.parseLong(A0I) * 1000;
    }

    public static C28601Of A01(AbstractC15710nm r7, AnonymousClass1V8 r8) {
        AnonymousClass1YB r6 = new AnonymousClass1YB();
        if (r8 != null) {
            for (AnonymousClass1V8 r4 : r8.A0J("device")) {
                r6.A01(r4.A0B(r7, DeviceJid.class, "jid"), Long.valueOf(r4.A08("key-index", 0)));
            }
        }
        return r6.A00();
    }
}
