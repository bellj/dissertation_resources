package X;

import android.view.Menu;
import android.view.MenuItem;

/* renamed from: X.0XC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XC implements AnonymousClass02Q {
    public AnonymousClass02Q A00;
    public final /* synthetic */ LayoutInflater$Factory2C011505o A01;

    public AnonymousClass0XC(LayoutInflater$Factory2C011505o r1, AnonymousClass02Q r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r3) {
        return this.A00.ALr(menuItem, r3);
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r3) {
        return this.A00.AOg(menu, r3);
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        this.A00.AP3(r4);
        LayoutInflater$Factory2C011505o r2 = this.A01;
        if (r2.A09 != null) {
            r2.A08.getDecorView().removeCallbacks(r2.A0P);
        }
        if (r2.A0L != null) {
            AnonymousClass0QQ r0 = r2.A0N;
            if (r0 != null) {
                r0.A00();
            }
            AnonymousClass0QQ A0F = AnonymousClass028.A0F(r2.A0L);
            A0F.A02(0.0f);
            r2.A0N = A0F;
            A0F.A09(new AnonymousClass0Dc(this));
        }
        AbstractC002200x r1 = r2.A0l;
        if (r1 != null) {
            r1.AXC(r2.A0K);
        }
        r2.A0K = null;
        AnonymousClass028.A0R(r2.A07);
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r3) {
        AnonymousClass028.A0R(this.A01.A07);
        return this.A00.AU7(menu, r3);
    }
}
