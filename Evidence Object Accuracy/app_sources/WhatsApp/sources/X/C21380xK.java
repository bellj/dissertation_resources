package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;

/* renamed from: X.0xK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21380xK {
    public final Handler A00;
    public final Handler A01;
    public final Handler A02;
    public final C21370xJ A03;
    public final AnonymousClass12H A04;
    public final C243515e A05;

    public C21380xK(C21370xJ r8, C19990v2 r9, C21320xE r10, AnonymousClass12H r11, C18470sV r12, C243515e r13) {
        this.A03 = r8;
        this.A04 = r11;
        this.A05 = r13;
        this.A00 = new AnonymousClass1iP(Looper.getMainLooper(), r8, this, r11);
        this.A02 = new HandlerC35631iQ(Looper.getMainLooper(), r10, this, r11);
        this.A01 = new HandlerC35641iR(Looper.getMainLooper(), r8, r9, this, r11, r12);
    }

    public void A00(AbstractC15340mz r4, int i) {
        this.A02.post(new RunnableBRunnable0Shape0S0201000_I0(this, r4, i, 19));
    }

    public void A01(AbstractC15340mz r4, int i) {
        this.A04.A08(r4, i);
        C21370xJ r2 = this.A03;
        AbstractC14640lm r1 = r4.A0z.A00;
        AnonymousClass009.A05(r1);
        r2.A03(r1, false);
        AnonymousClass1IR r12 = r4.A0L;
        if (r12 != null) {
            this.A05.A05(r12);
        }
    }
}
