package X;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.21e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C453421e {
    public final Map A00 = new HashMap();

    public C453421e() {
    }

    public C453421e(C39341ph r2) {
        A03(r2);
    }

    public C39341ph A00(Uri uri) {
        Map map = this.A00;
        C39341ph r0 = (C39341ph) map.get(uri);
        if (r0 != null) {
            return r0;
        }
        Log.e("mediapreviewparams/get/item should be explicitly added");
        C39341ph r02 = new C39341ph(uri);
        map.put(uri, r02);
        return r02;
    }

    public void A01(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("media_preview_params");
        if (bundle2 != null) {
            Map map = this.A00;
            map.clear();
            ArrayList parcelableArrayList = bundle2.getParcelableArrayList("items");
            if (parcelableArrayList != null) {
                Iterator it = parcelableArrayList.iterator();
                while (it.hasNext()) {
                    C39341ph r1 = ((C51242Tl) it.next()).A00;
                    map.put(r1.A0G, r1);
                }
            }
        }
    }

    public final void A02(Bundle bundle) {
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
        for (C39341ph r1 : this.A00.values()) {
            arrayList.add(new C51242Tl(r1));
        }
        bundle.putParcelableArrayList("items", arrayList);
    }

    public void A03(C39341ph r4) {
        Map map = this.A00;
        Uri uri = r4.A0G;
        if (map.containsKey(uri)) {
            Log.e("mediapreviewparams/add/item was already added");
        }
        map.put(uri, r4);
    }
}
