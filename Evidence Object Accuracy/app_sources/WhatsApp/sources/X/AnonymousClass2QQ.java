package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.PagerSlidingTabStrip;

/* renamed from: X.2QQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QQ extends AnonymousClass04v {
    public final /* synthetic */ PagerSlidingTabStrip A00;

    public AnonymousClass2QQ(PagerSlidingTabStrip pagerSlidingTabStrip) {
        this.A00 = pagerSlidingTabStrip;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        if (view.isSelected()) {
            r4.A0A(C007804a.A05);
            r4.A02.setClickable(false);
        }
        if (Build.VERSION.SDK_INT >= 22) {
            view.setAccessibilityTraversalBefore(this.A00.A0N.getId());
        }
    }
}
