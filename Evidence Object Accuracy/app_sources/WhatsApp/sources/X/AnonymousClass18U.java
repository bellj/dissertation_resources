package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.community.JoinGroupBottomSheetFragment;
import com.whatsapp.util.Log;

/* renamed from: X.18U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18U implements AnonymousClass12Q {
    public final AnonymousClass12P A00;
    public final C22640zP A01;
    public final C22180yf A02;
    public final AnonymousClass18V A03;

    public AnonymousClass18U(AnonymousClass12P r1, C22640zP r2, C22180yf r3, AnonymousClass18V r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass12Q
    public void Ab9(Context context, Uri uri) {
        AbA(context, uri, 0);
    }

    @Override // X.AnonymousClass12Q
    public void AbA(Context context, Uri uri, int i) {
        AbB(context, uri, i, 4);
    }

    @Override // X.AnonymousClass12Q
    public void AbB(Context context, Uri uri, int i, int i2) {
        Intent intent;
        if (uri == null) {
            Log.e("linklauncher/start-activity/uri-is-null");
            return;
        }
        String A02 = AcceptInviteLinkActivity.A02(uri);
        if (!TextUtils.isEmpty(A02)) {
            Activity A00 = AnonymousClass12P.A00(context);
            if (!this.A01.A07() || !(A00 instanceof ActivityC000800j)) {
                intent = new Intent().setClassName(context.getPackageName(), "com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity");
                intent.putExtra("code", A02);
            } else {
                C42791vs.A01(JoinGroupBottomSheetFragment.A01(A02, i, false), ((ActivityC000900k) A00).A0V());
                return;
            }
        } else if (this.A02.A05(uri) != 1) {
            intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.deeplink.DeepLinkActivity");
            intent.setData(uri);
            intent.putExtra("source", 2);
            intent.putExtra("extra_entry_point", i2);
        } else if (!this.A03.AI1(context, uri)) {
            this.A00.Ab9(context, uri);
            return;
        } else {
            return;
        }
        this.A00.A06(context, intent);
    }
}
