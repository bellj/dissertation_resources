package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.3ZE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZE implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1OE A00;
    public final /* synthetic */ C244315m A01;
    public final /* synthetic */ byte[] A02;

    public AnonymousClass3ZE(AnonymousClass1OE r1, C244315m r2, byte[] bArr) {
        this.A01 = r2;
        this.A02 = bArr;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendBeginLoginIq/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r17, String str) {
        byte[] bArr;
        C89654Ku r2;
        int i;
        byte[] bArr2 = this.A02;
        AnonymousClass1OE r4 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/beginLoginOnSuccess id=")));
        byte[] A01 = C244315m.A01(r17, r4, "l2");
        byte[] A012 = C244315m.A01(r17, r4, "l2_sig");
        if (A01 != null && A012 != null) {
            AnonymousClass1V8 A0E = r17.A0E("timeout");
            int i2 = 0;
            if (A0E != null) {
                i2 = C28421Nd.A00(A0E.A0I("value", null), 0);
            }
            byte[] bArr3 = C244315m.A01;
            int length = bArr3.length;
            int length2 = A01.length;
            byte[] bArr4 = new byte[length + length2];
            System.arraycopy(bArr3, 0, bArr4, 0, length);
            System.arraycopy(A01, 0, bArr4, length, length2);
            if (!C16550pE.A03(bArr4, A012, bArr2)) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/beginLoginOnSuccess/l2 cannot be verified with l2_sig and ed_pub id=")));
                r4.APr("l2 cannot be verified with l2_sig and ed_pub", 2);
                return;
            }
            ((AnonymousClass1OF) r4).A00.A01();
            Object obj = r4.A0C;
            synchronized (obj) {
                bArr = r4.A06;
                r2 = r4.A03;
                i = r4.A01;
            }
            AnonymousClass1OH r7 = new AnonymousClass1OH((NativeHolder) JniBridge.jvidispatchOIOOO(3, (long) 100000, r2.A00, bArr, A01));
            JniBridge.getInstance();
            NativeHolder nativeHolder = r7.A00;
            int jvidispatchIIO = (int) JniBridge.jvidispatchIIO(1, (long) 66, nativeHolder);
            if (jvidispatchIIO == -1) {
                r4.A08.APs("Login Failure Invalid Password", 8, 2, i - 1, i2);
            } else if (jvidispatchIIO != 0) {
                r4.A08.APs(C12960it.A0W(jvidispatchIIO, "WESOpaqueClientCreateLoginFinish failed with WESOpaqueStatusType="), 4, 2, -1, 0);
            } else {
                JniBridge.getInstance();
                byte[] bArr5 = (byte[]) JniBridge.jvidispatchOIO(0, (long) 69, nativeHolder);
                synchronized (obj) {
                    r4.A02 = r7;
                    r4.A07 = bArr5;
                    r4.A00 = 2;
                }
                r4.A00();
            }
        }
    }
}
