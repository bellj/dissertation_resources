package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

/* renamed from: X.2m1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56832m1 extends C27861Jn {
    public static final long serialVersionUID = 1;
    public final int bytesLength;
    public final int bytesOffset;

    public C56832m1(byte[] bArr, int i, int i2) {
        super(bArr);
        AbstractC27881Jp.A00(i, i + i2, bArr.length);
        this.bytesOffset = i;
        this.bytesLength = i2;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("BoundedByteStream instances are not to be serialized directly");
    }

    public Object writeReplace() {
        return new C27861Jn(A04());
    }
}
