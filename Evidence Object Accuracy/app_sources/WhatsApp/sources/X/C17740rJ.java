package X;

import java.util.Map;

/* renamed from: X.0rJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17740rJ implements AnonymousClass6ME {
    @Override // X.AnonymousClass6ME
    public boolean AJM(String str, String str2) {
        Boolean bool;
        if (str2 == null) {
            return false;
        }
        Map A01 = C65053Hy.A01(str2);
        Object obj = A01.get("should_load_bloks_through_cdn");
        if (!(obj instanceof Boolean) || (bool = (Boolean) obj) == null || !bool.booleanValue() || A01.get("static_url") == null) {
            return false;
        }
        return true;
    }
}
