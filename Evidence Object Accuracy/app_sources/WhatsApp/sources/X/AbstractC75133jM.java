package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3jM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75133jM extends AbstractC05270Ox {
    public int A00 = 1;

    public abstract void A02();

    public abstract boolean A03();

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        int i3;
        if (!A03()) {
            int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset();
            int height = recyclerView.getHeight();
            if (height != 0 && (i3 = this.A00) <= computeVerticalScrollOffset / height) {
                this.A00 = i3 + 2;
                A02();
            }
        }
    }
}
