package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;

/* renamed from: X.1eH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33521eH {
    public boolean A00;
    public final View A01;
    public final ViewStub A02;
    public final TextView A03;
    public final TextView A04;
    public final C14900mE A05;
    public final AnonymousClass2Dn A06;
    public final C22330yu A07;
    public final C15550nR A08;
    public final C27131Gd A09;
    public final AnonymousClass10S A0A;
    public final C15610nY A0B;
    public final AnonymousClass1J1 A0C;
    public final C21270x9 A0D;
    public final C14830m7 A0E;
    public final C14820m6 A0F;
    public final AnonymousClass018 A0G;
    public final AbstractC33331dp A0H;
    public final C244215l A0I;
    public final C33531eI A0J;
    public final AnonymousClass01H A0K;
    public final Runnable A0L = new RunnableBRunnable0Shape12S0100000_I0_12(this, 11);

    public C33521eH(ViewGroup viewGroup, C14900mE r11, C22330yu r12, C15550nR r13, AnonymousClass10S r14, C15610nY r15, C21270x9 r16, C14830m7 r17, C14820m6 r18, AnonymousClass018 r19, C244215l r20, AnonymousClass01H r21) {
        C36831ki r4 = new C36831ki(this);
        this.A09 = r4;
        C851141f r3 = new C851141f(this);
        this.A06 = r3;
        C859044p r2 = new C859044p(this);
        this.A0H = r2;
        this.A0E = r17;
        this.A05 = r11;
        this.A0D = r16;
        this.A08 = r13;
        this.A0B = r15;
        this.A0G = r19;
        this.A0A = r14;
        this.A07 = r12;
        this.A0F = r18;
        this.A0I = r20;
        this.A0K = r21;
        this.A01 = viewGroup;
        this.A0C = r16.A04(viewGroup.getContext(), "status-details-panel");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.status_details, viewGroup, true);
        ViewStub viewStub = (ViewStub) inflate.findViewById(R.id.panel_action_buttons);
        viewStub.setLayoutResource(R.layout.status_details_action_buttons);
        viewStub.inflate();
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(16908298);
        viewGroup.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager());
        this.A02 = (ViewStub) AnonymousClass028.A0D(inflate, R.id.list_container_header_stub);
        TextView textView = (TextView) inflate.findViewById(16908292);
        this.A03 = textView;
        textView.setText(r18.A00.getBoolean("read_receipts_enabled", true) ? R.string.no_one_saw_your_status : R.string.no_one_saw_your_status_because_you_disabled_read_receipts);
        C33531eI r1 = new C33531eI(this);
        this.A0J = r1;
        TextView textView2 = (TextView) viewGroup.findViewById(R.id.title);
        this.A04 = textView2;
        C27531Hw.A06(textView2);
        recyclerView.setAdapter(r1);
        r14.A03(r4);
        r12.A03(r3);
        r20.A03(r2);
    }

    public final void A00() {
        C14900mE r6 = this.A05;
        Runnable runnable = this.A0L;
        r6.A0G(runnable);
        C33531eI r1 = this.A0J;
        if (r1.A00.size() > 0) {
            long j = 0;
            for (AnonymousClass4OQ r0 : r1.A00) {
                long j2 = r0.A00;
                if (j2 > j) {
                    j = j2;
                }
            }
            r6.A0J(runnable, (C38121nY.A01(j) - System.currentTimeMillis()) + 1000);
        }
    }
}
