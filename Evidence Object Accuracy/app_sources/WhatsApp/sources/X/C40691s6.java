package X;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;

/* renamed from: X.1s6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40691s6 {
    public static Pair A00(Activity activity, View view, View view2, C14900mE r27, C15570nT r28, C15550nR r29, C15610nY r30, AnonymousClass1J1 r31, C21270x9 r32, AnonymousClass19D r33, AnonymousClass11P r34, C14820m6 r35, AnonymousClass018 r36, C14850m9 r37, AbstractC14440lR r38, AnonymousClass01H r39, AnonymousClass01H r40, String str) {
        AnonymousClass1J1 r11 = r31;
        View view3 = view2;
        if (r31 == null) {
            Context baseContext = activity.getBaseContext();
            StringBuilder sb = new StringBuilder("out-of-chat-");
            sb.append(str);
            r11 = r32.A04(baseContext, sb.toString());
        }
        if (view2 == null) {
            view3 = AnonymousClass028.A0D(view, R.id.out_of_chat_playback_holder);
            activity.getLayoutInflater().inflate(R.layout.out_of_chat_playback_layout, (ViewGroup) view3, true);
            AnonymousClass028.A0D(view3, R.id.out_of_chat_close_btn).setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r34, 39, r39));
            AnonymousClass028.A0D(view3, R.id.out_of_chat_playback_btn).setOnClickListener(new View$OnClickListenerC55962jy(activity, view3, r28, r29, r30, r11, r33, r34, r35, r36));
            AnonymousClass028.A0D(view3, R.id.out_of_chat_layout).setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r34, 40, r39));
        }
        C35191hP A00 = r34.A00();
        if (A00 != null) {
            A00.A0J = new AnonymousClass3WN(activity, view3, r27, r28, r29, r30, r11, r33, r34, r35, r36, r37, r38, r40);
            A00.A0C = activity;
        }
        C35191hP A002 = r34.A00();
        if (A002 != null) {
            A002.A0N = new AnonymousClass4QW(view3, r34, r39);
        }
        A01(view3, r28, r29, r30, r11, r34, r36);
        view3.setVisibility(0);
        AnonymousClass1A1 r2 = (AnonymousClass1A1) r39.get();
        if (!r2.A01) {
            r2.A06.A03(r2.A08);
            r2.A01 = true;
        }
        if (r34.A0B()) {
            r34.A05();
        }
        C35191hP A003 = r34.A00();
        if (A003 != null) {
            A003.A0X = true;
        }
        return new Pair(view3, r11);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.view.View r9, X.C15570nT r10, X.C15550nR r11, X.C15610nY r12, X.AnonymousClass1J1 r13, X.AnonymousClass11P r14, X.AnonymousClass018 r15) {
        /*
        // Method dump skipped, instructions count: 377
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C40691s6.A01(android.view.View, X.0nT, X.0nR, X.0nY, X.1J1, X.11P, X.018):void");
    }

    public static void A02(View view, AnonymousClass11P r3) {
        AnonymousClass4QW r1;
        C35191hP A00 = r3.A00();
        if (A00 != null && view != null && (r1 = A00.A0N) != null && r1.A00 == view) {
            r1.A00 = null;
        }
    }

    public static void A03(View view, AnonymousClass11P r2) {
        r2.A0A(false);
        View findViewById = view.findViewById(R.id.out_of_chat_playback_holder);
        if (findViewById != null && findViewById.getVisibility() == 0) {
            r2.A04();
        }
    }

    public static void A04(View view, AnonymousClass11P r4, AnonymousClass01H r5) {
        r4.A0A(false);
        View findViewById = view.findViewById(R.id.out_of_chat_playback_holder);
        if (findViewById != null && findViewById.getVisibility() == 0) {
            ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
            if (viewGroup.getLayoutTransition() == null) {
                LayoutTransition layoutTransition = new LayoutTransition();
                layoutTransition.addTransitionListener(new C95744eH(layoutTransition, findViewById, viewGroup));
                viewGroup.setLayoutTransition(layoutTransition);
            }
            findViewById.setVisibility(8);
            A09(r4, r5);
        }
    }

    public static void A05(ImageButton imageButton) {
        imageButton.setImageResource(R.drawable.inline_audio_pause);
        imageButton.setContentDescription(imageButton.getContext().getString(R.string.pause));
    }

    public static void A06(ImageButton imageButton) {
        imageButton.setImageDrawable(AnonymousClass00T.A04(imageButton.getContext(), R.drawable.inline_audio_play));
        imageButton.setContentDescription(imageButton.getContext().getString(R.string.play));
    }

    public static void A07(AnonymousClass11P r1) {
        C35191hP A00 = r1.A00();
        if (A00 != null) {
            A00.A0C = null;
        }
    }

    public static void A08(AnonymousClass11P r3, C14820m6 r4, AbstractC14640lm r5) {
        if (r3.A0B()) {
            synchronized (r3) {
                C35191hP r1 = r3.A00;
                if (r1 != null) {
                    r1.A0Y = false;
                    AnonymousClass19C r12 = r1.A0o;
                    r12.A06 = true;
                    r12.A0F.A04(14, null);
                }
            }
            r3.A0A(true);
            A0B(r4, r5);
        }
    }

    public static void A09(AnonymousClass11P r4, AnonymousClass01H r5) {
        r4.A0A(false);
        r4.A06();
        r4.A07();
        AnonymousClass1A1 r2 = (AnonymousClass1A1) r5.get();
        if (r2.A01) {
            r2.A06.A04(r2.A08);
            r2.A01 = false;
        }
        ((AnonymousClass1A1) r5.get()).A00 = null;
    }

    public static void A0A(AnonymousClass11P r2, AnonymousClass01H r3) {
        C30421Xi r0;
        synchronized (r2) {
            C35191hP r02 = r2.A00;
            if (r02 != null) {
                r0 = r02.A0O;
            } else {
                r0 = r2.A01;
            }
            r2.A02 = r0;
        }
        r2.A0A(false);
        r2.A06();
        r2.A07();
        AnonymousClass1A1 r22 = (AnonymousClass1A1) r3.get();
        if (r22.A01) {
            r22.A06.A04(r22.A08);
            r22.A01 = false;
        }
        ((AnonymousClass1A1) r3.get()).A00 = null;
    }

    public static void A0B(C14820m6 r4, AbstractC14640lm r5) {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        if (r5 != null) {
            if (C15380n4.A0F(r5)) {
                sharedPreferences = r4.A00;
                edit = sharedPreferences.edit();
                str = "ptt_out_of_chat_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r5);
                sharedPreferences = r4.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_out_of_chat_group";
                } else {
                    str = "ptt_out_of_chat_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
    }
}
