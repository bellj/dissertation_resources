package X;

import androidx.recyclerview.widget.RecyclerView;

@Deprecated
/* renamed from: X.0FG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FG extends AnonymousClass0DV {
    public final AnonymousClass04v A00 = ((AnonymousClass0DV) this).A00;
    public final AnonymousClass04v A01 = new AnonymousClass0DR(this);
    public final RecyclerView A02;

    public AnonymousClass0FG(RecyclerView recyclerView) {
        super(recyclerView);
        this.A02 = recyclerView;
    }

    @Override // X.AnonymousClass0DV
    public AnonymousClass04v A07() {
        return this.A01;
    }
}
