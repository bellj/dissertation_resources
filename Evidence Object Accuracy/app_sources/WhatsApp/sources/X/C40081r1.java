package X;

/* renamed from: X.1r1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40081r1 extends AnonymousClass1JX {
    public final C15570nT A00;
    public final C14650lo A01;
    public final C15550nR A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final C17650rA A06;
    public final AnonymousClass102 A07;
    public final C14850m9 A08;
    public final AnonymousClass13N A09;
    public final AnonymousClass11H A0A;
    public final C17070qD A0B;
    public final AnonymousClass1EX A0C;
    public final C22940zt A0D;
    public final C20320vZ A0E;
    public final C26601Ec A0F;
    public final boolean A0G;

    public C40081r1(AbstractC15710nm r3, C15570nT r4, C15450nH r5, C14650lo r6, C15550nR r7, C14830m7 r8, C16590pI r9, AnonymousClass018 r10, C17650rA r11, AnonymousClass102 r12, C14850m9 r13, AnonymousClass13N r14, AnonymousClass11H r15, C17070qD r16, AnonymousClass1EX r17, C22940zt r18, AbstractC15340mz r19, C20320vZ r20, C26601Ec r21, boolean z, boolean z2) {
        super(r3, r5, r19, z);
        super.A00 = 0;
        this.A03 = r8;
        this.A08 = r13;
        this.A00 = r4;
        this.A04 = r9;
        this.A02 = r7;
        this.A05 = r10;
        this.A0E = r20;
        this.A0B = r16;
        this.A0C = r17;
        this.A09 = r14;
        this.A0D = r18;
        this.A0A = r15;
        this.A01 = r6;
        this.A07 = r12;
        this.A06 = r11;
        this.A0F = r21;
        this.A0G = z2;
    }

    public C40081r1(AbstractC15710nm r2, C15570nT r3, C15450nH r4, C14850m9 r5, AnonymousClass13N r6, AnonymousClass11H r7, AnonymousClass1EX r8, C22940zt r9, AbstractC15340mz r10, C26601Ec r11) {
        super(r2, r4, r10, false);
        super.A00 = 0;
        this.A08 = r5;
        this.A00 = r3;
        this.A0C = r8;
        this.A09 = r6;
        this.A0D = r9;
        this.A0A = r7;
        this.A0F = r11;
        this.A0G = false;
        this.A03 = null;
        this.A04 = null;
        this.A02 = null;
        this.A05 = null;
        this.A0E = null;
        this.A0B = null;
        this.A01 = null;
        this.A07 = null;
        this.A06 = null;
    }

    public static final int A00(AnonymousClass27B r0, boolean z) {
        switch (r0.ordinal()) {
            case 0:
                return 7;
            case 1:
            default:
                return 0;
            case 2:
                return 4;
            case 3:
                return 5;
            case 4:
                return 13;
            case 5:
                return z ? 8 : 9;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0112, code lost:
        if (r1 != 10) goto L_0x0114;
     */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x02f4  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x015a  */
    @Override // X.AnonymousClass1JX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1G7 A02() {
        /*
        // Method dump skipped, instructions count: 852
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C40081r1.A02():X.1G7");
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x01fb  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a9  */
    @Override // X.AnonymousClass1JX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC15340mz A03(X.AnonymousClass1G6 r39, X.AnonymousClass1IS r40, long r41) {
        /*
        // Method dump skipped, instructions count: 547
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C40081r1.A03(X.1G6, X.1IS, long):X.0mz");
    }
}
