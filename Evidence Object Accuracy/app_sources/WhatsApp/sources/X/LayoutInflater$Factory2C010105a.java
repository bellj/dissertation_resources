package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

/* renamed from: X.05a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class LayoutInflater$Factory2C010105a implements LayoutInflater.Factory2 {
    public final AnonymousClass01F A00;

    public LayoutInflater$Factory2C010105a(AnonymousClass01F r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a5, code lost:
        if (r3 != null) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ad, code lost:
        if (r6 == null) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00af, code lost:
        if (r5 == -1) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b1, code lost:
        r6 = r11.A00.A07(r5);
     */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0160  */
    @Override // android.view.LayoutInflater.Factory2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View onCreateView(android.view.View r12, java.lang.String r13, android.content.Context r14, android.util.AttributeSet r15) {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: X.LayoutInflater$Factory2C010105a.onCreateView(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    @Override // android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
