package X;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaEditText;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;

/* renamed from: X.0mp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15260mp extends C15270mq implements AbstractC15290ms {
    public int A00;
    public View A01;
    public View A02;
    public C69893aP A03;
    public boolean A04 = false;
    public final AbstractC05270Ox A05 = new C54782hH(this);
    public final C15450nH A06;
    public final AnonymousClass240 A07;
    public final AnonymousClass4TX A08;
    public final AnonymousClass3XR A09;
    public final AnonymousClass2B7 A0A;
    public final C14850m9 A0B;
    public final AnonymousClass12V A0C;
    public final C20980wd A0D;
    public final AbstractC17410ql A0E = new C69323Yu(this);
    public final C235812f A0F;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C15260mp(Activity activity, ImageButton imageButton, AbstractC15710nm r22, KeyboardPopupLayout keyboardPopupLayout, C15450nH r24, WaEditText waEditText, AnonymousClass01d r26, C14820m6 r27, AnonymousClass018 r28, AnonymousClass240 r29, AnonymousClass4TX r30, AnonymousClass3XR r31, AnonymousClass2B7 r32, C14850m9 r33, C16630pM r34, AnonymousClass12V r35, C20980wd r36, C235812f r37, C252718t r38) {
        super(activity, imageButton, r22, keyboardPopupLayout, waEditText, r26, r27, r28, r30.A03, r30.A04, r30.A05, r34, r38);
        AnonymousClass1BQ r0;
        AnonymousClass240 r2 = r29;
        this.A0B = r33;
        this.A06 = r24;
        this.A0F = r37;
        this.A0D = r36;
        this.A0C = r35;
        this.A0A = r32;
        this.A09 = r31;
        this.A08 = r30;
        this.A07 = !r33.A07(1396) ? null : r2;
        if (r32 != null) {
            r32.A01 = this;
            C22210yi r23 = r32.A07;
            RunnableBRunnable0Shape12S0100000_I0_12 runnableBRunnable0Shape12S0100000_I0_12 = new RunnableBRunnable0Shape12S0100000_I0_12(r23, 15);
            if (((C22220yj) r23).A03 != null) {
                runnableBRunnable0Shape12S0100000_I0_12.run();
            } else {
                r23.A09.execute(runnableBRunnable0Shape12S0100000_I0_12);
            }
            if (r32.A0D.A00 && (r0 = r32.A0E) != null) {
                r0.A03.A05((AbstractC001200n) activity, new AnonymousClass02B() { // from class: X.3QM
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r5v0 */
                    /* JADX WARN: Type inference failed for: r5v2 */
                    /* JADX WARN: Type inference failed for: r5v4 */
                    /* JADX WARNING: Unknown variable types count: 1 */
                    @Override // X.AnonymousClass02B
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final void ANq(java.lang.Object r10) {
                        /*
                            r9 = this;
                            X.2B7 r4 = X.AnonymousClass2B7.this
                            java.util.List r10 = (java.util.List) r10
                            int r7 = r10.size()
                            if (r7 <= 0) goto L_0x001c
                            X.0ms r0 = r4.A01
                            android.widget.PopupWindow r0 = (android.widget.PopupWindow) r0
                            boolean r0 = r0.isShowing()
                            if (r0 == 0) goto L_0x0030
                            X.1BQ r0 = r4.A0E
                            if (r0 == 0) goto L_0x0030
                            boolean r0 = r0.A02
                            if (r0 == 0) goto L_0x0030
                        L_0x001c:
                            X.0ms r0 = r4.A01
                            android.widget.PopupWindow r0 = (android.widget.PopupWindow) r0
                            boolean r0 = r0.isShowing()
                            boolean r5 = X.C12960it.A1S(r0)
                        L_0x0028:
                            X.0ms r0 = r4.A01
                            X.0mp r0 = (X.C15260mp) r0
                            r0.A0J(r5)
                            return
                        L_0x0030:
                            r5 = 2
                            X.19w r3 = r4.A0F
                            X.AnonymousClass009.A05(r3)
                            monitor-enter(r3)
                            X.0m6 r0 = r3.A01     // Catch: all -> 0x0087
                            java.lang.Object r8 = r0.A04     // Catch: all -> 0x0087
                            monitor-enter(r8)     // Catch: all -> 0x0087
                            android.content.SharedPreferences r6 = r0.A00     // Catch: all -> 0x0084
                            java.lang.String r2 = "sticker_suggestion_triggered_count"
                            int r0 = X.C12970iu.A01(r6, r2)     // Catch: all -> 0x0084
                            android.content.SharedPreferences$Editor r1 = r6.edit()     // Catch: all -> 0x0084
                            int r0 = r0 + 1
                            X.C12970iu.A1B(r1, r2, r0)     // Catch: all -> 0x0084
                            monitor-exit(r8)     // Catch: all -> 0x0084
                            org.json.JSONArray r1 = r3.A00     // Catch: all -> 0x0087
                            if (r1 != 0) goto L_0x006f
                            java.lang.String r1 = "sticker_suggestion_num_suggestions_array"
                            java.lang.String r0 = "[]"
                            java.lang.String r0 = r6.getString(r1, r0)     // Catch: all -> 0x0087
                            org.json.JSONArray r1 = new org.json.JSONArray     // Catch: JSONException -> 0x0062, all -> 0x0087
                            r1.<init>(r0)     // Catch: JSONException -> 0x0062, all -> 0x0087
                            goto L_0x006d
                        L_0x0062:
                            r1 = move-exception
                            java.lang.String r0 = "StickerSuggestionLogger/getNumStickerSuggestionsJsonArray Error getting JSONArray"
                            com.whatsapp.util.Log.e(r0, r1)     // Catch: all -> 0x0087
                            org.json.JSONArray r1 = new org.json.JSONArray     // Catch: all -> 0x0087
                            r1.<init>()     // Catch: all -> 0x0087
                        L_0x006d:
                            r3.A00 = r1     // Catch: all -> 0x0087
                        L_0x006f:
                            r1.put(r7)     // Catch: all -> 0x0087
                            org.json.JSONArray r0 = r3.A00     // Catch: all -> 0x0087
                            java.lang.String r2 = r0.toString()     // Catch: all -> 0x0087
                            android.content.SharedPreferences$Editor r1 = r6.edit()     // Catch: all -> 0x0087
                            java.lang.String r0 = "sticker_suggestion_num_suggestions_array"
                            X.C12970iu.A1D(r1, r0, r2)     // Catch: all -> 0x0087
                            monitor-exit(r3)     // Catch: all -> 0x0087
                            goto L_0x0028
                        L_0x0084:
                            r0 = move-exception
                            monitor-exit(r8)     // Catch: all -> 0x0084
                            throw r0     // Catch: all -> 0x0087
                        L_0x0087:
                            r0 = move-exception
                            monitor-exit(r3)     // Catch: all -> 0x0087
                            throw r0
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3QM.ANq(java.lang.Object):void");
                    }
                });
            }
        }
    }

    @Override // X.C15270mq, X.AbstractC15280mr
    public void A06() {
        AnonymousClass341 r2;
        String id;
        AnonymousClass241 r22;
        AnonymousClass341 r7;
        super.A06();
        AnonymousClass3XR r0 = this.A09;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass2B7 r4 = this.A0A;
        if (!(r4 == null || (r7 = r4.A05) == null)) {
            ((AnonymousClass242) r7).A0A.getViewTreeObserver().addOnGlobalLayoutListener(r7.A0M);
            r7.A0d.A04(r7, r7.A0Q.A07(1396) ? 1 : 0);
            r7.A0R.A07(new C853942q());
            AnonymousClass165 r6 = r7.A0U.A01;
            synchronized (r6.A04) {
                r6.A00().edit().putInt("sticker_picker_opened_count", r6.A00().getInt("sticker_picker_opened_count", 0) + 1).apply();
            }
            r7.A0J = true;
        }
        AnonymousClass240 r02 = this.A07;
        if (!(r02 == null || (r22 = r02.A03) == null)) {
            r22.A0H.A04(r22, 2);
            r22.A05 = true;
        }
        if (r4 != null && r4.A01() && (r2 = r4.A05) != null) {
            if (this.A00 == 2) {
                AbstractC69213Yj r03 = r2.A0E;
                if (r03 == null) {
                    id = null;
                } else {
                    id = r03.getId();
                }
                if ("contextual_suggestion".equals(id)) {
                    return;
                }
            }
            A0L("contextual_suggestion");
        }
    }

    @Override // X.C15270mq
    public void A0A() {
        super.A0A();
        AnonymousClass3XR r4 = this.A09;
        if (r4 != null) {
            AbstractC05270Ox r3 = this.A05;
            AnonymousClass3DT r6 = new AnonymousClass3DT(((AbstractC15280mr) this).A03, ((C15270mq) this).A05);
            C69893aP r0 = this.A03;
            r4.A01 = r6;
            r4.A04 = r0;
            r4.A00 = this;
            r6.A09.setOnClickListener(r4.A08);
            AnonymousClass320 r5 = new AnonymousClass320(r4);
            r4.A03 = r5;
            ViewGroup viewGroup = r6.A08;
            r6.A02 = AnonymousClass028.A0D(viewGroup, R.id.gif_trending);
            r6.A00 = AnonymousClass028.A0D(viewGroup, R.id.gif_trending_additions);
            RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(viewGroup, R.id.gif_trending_grid);
            r6.A05 = recyclerView;
            recyclerView.A0n(r3);
            r6.A05.A0l(new C74913iz(r6, r6.A07.getResources().getDimensionPixelSize(R.dimen.selected_contacts_top_offset)));
            r6.A01 = AnonymousClass028.A0D(viewGroup, R.id.progress_container);
            r6.A03 = AnonymousClass028.A0D(viewGroup, R.id.no_results);
            r6.A04 = AnonymousClass028.A0D(viewGroup, R.id.retry_panel);
            WaButton waButton = (WaButton) AnonymousClass028.A0D(viewGroup, R.id.retry_button);
            r6.A06 = waButton;
            waButton.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(r6, 39, r5));
            r6.A05.setAdapter(r5);
            r4.A01.A09.setVisibility(0);
        }
        AnonymousClass4QD r32 = new AnonymousClass4QD(((C15270mq) this).A05);
        AnonymousClass4TX r02 = this.A08;
        r02.A00 = this;
        r02.A01 = r32;
        r32.A02.setOnClickListener(r02.A06);
        this.A01 = ((C15270mq) this).A05.findViewById(R.id.search_button);
        this.A02 = getContentView().findViewById(R.id.store_button_view);
        AnonymousClass2B7 r42 = this.A0A;
        if (r42 != null) {
            ViewGroup viewGroup2 = ((C15270mq) this).A05;
            Activity activity = ((AbstractC15280mr) this).A03;
            C91584Sh r62 = new C91584Sh(activity, viewGroup2);
            AnonymousClass1AC r1 = r42.A06;
            C14850m9 r03 = r1.A06;
            AbstractC15710nm r04 = r1.A00;
            AbstractC14440lR r05 = r1.A0D;
            C16120oU r06 = r1.A07;
            AnonymousClass01d r07 = r1.A03;
            C22210yi r08 = r42.A07;
            AnonymousClass018 r09 = r1.A05;
            AnonymousClass146 r15 = r42.A0A;
            C235512c r52 = r42.A0B;
            C14820m6 r10 = r1.A04;
            AnonymousClass15H r14 = r42.A08;
            ViewGroup viewGroup3 = r62.A03;
            C69893aP r12 = this.A03;
            AnonymousClass1AB r11 = r42.A09;
            AbstractC05270Ox r9 = this.A05;
            AnonymousClass1KT r7 = r42.A0G;
            AnonymousClass341 r33 = new AnonymousClass341(activity, viewGroup3, r9, r04, r07, r10, r09, r03, r06, r12, r08, r14, r11, r15, r52, r42.A0D, r42.A0E, r7, r05);
            r42.A03 = this.A03;
            r42.A02 = r62;
            r42.A05 = r33;
            r62.A04.setOnClickListener(r42.A0H);
            if (r42.A04 != null) {
                r33.A06 = r42.A0C;
                if (!r10.A00.getBoolean("sticker_picker_initial_download", false)) {
                    r52.A0Y.Aaz(new AnonymousClass38H(new AnonymousClass2B5(r42), r52), new Object[0]);
                } else {
                    r42.A00();
                }
            }
            if (r42.A01()) {
                ((C15260mp) r42.A01).A0I(2);
            }
        }
        AnonymousClass240 r92 = this.A07;
        if (r92 != null) {
            ViewGroup viewGroup4 = ((C15270mq) this).A05;
            Activity activity2 = ((AbstractC15280mr) this).A03;
            AnonymousClass4QC r13 = new AnonymousClass4QC(activity2, viewGroup4);
            AnonymousClass1AC r34 = r92.A04;
            C14850m9 r010 = this.A0B;
            C14900mE r152 = r34.A01;
            AbstractC14440lR r122 = r34.A0D;
            ViewGroup viewGroup5 = r13.A01;
            C235812f r102 = this.A0F;
            AnonymousClass1KT r8 = r34.A0B;
            AnonymousClass018 r72 = r34.A05;
            C235512c r63 = r92.A06;
            C22210yi r53 = r34.A0A;
            AnonymousClass241 r35 = new AnonymousClass241(activity2, viewGroup5, this.A05, r152, r72, r010, r34.A09, r102, r53, r92.A05, r63, r8, r122);
            r92.A00 = r13;
            r92.A01 = this;
            r92.A03 = r35;
            r13.A02.setOnClickListener(r92.A0B);
            if (r92.A02 != null) {
                r35.A01 = r92.A07;
            }
            r92.A00.A02.setVisibility(A0F());
        }
        View view = this.A01;
        int i = 8;
        if (this.A0O.A02) {
            i = 0;
        }
        view.setVisibility(i);
        this.A01.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 30));
        A0I(((AbstractC15280mr) this).A07.A00.getInt("emoji_popup_window_tab_state", 0));
        if (this.A0B.A07(1396)) {
            this.A0D.A03(this.A0E);
            this.A0F.A01(2);
        }
    }

    public final int A0F() {
        if (!this.A0B.A07(1396) || !Boolean.valueOf(this.A0C.A01()).booleanValue()) {
            return 8;
        }
        return 0;
    }

    public void A0G() {
        AnonymousClass241 r0;
        if (isShowing()) {
            dismiss();
        }
        AnonymousClass2B7 r2 = this.A0A;
        if (r2 != null) {
            AnonymousClass341 r02 = r2.A05;
            if (r02 != null) {
                AnonymousClass1KT r1 = r02.A0d;
                r1.A04 = null;
                r1.A02 = null;
            }
            ObjectAnimator objectAnimator = r2.A00;
            if (objectAnimator != null) {
                objectAnimator.cancel();
                r2.A00 = null;
            }
        }
        AnonymousClass240 r03 = this.A07;
        if (!(r03 == null || (r0 = r03.A03) == null)) {
            AnonymousClass1KT r12 = r0.A0H;
            r12.A04 = null;
            r12.A02 = null;
            r12.A03();
        }
        if (this.A0B.A07(1396)) {
            this.A0D.A04(this.A0E);
            this.A0F.A00(2);
        }
    }

    public final void A0H() {
        ImageButton imageButton = this.A0I;
        if (imageButton != null) {
            Activity activity = ((AbstractC15280mr) this).A03;
            imageButton.setImageDrawable(AnonymousClass2GE.A01(activity, ((C15270mq) this).A00, R.color.ibEmojiIconTint));
            imageButton.setContentDescription(activity.getString(R.string.emoji_button_description));
            imageButton.setOnClickListener(super.A0F);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0024, code lost:
        if (r13 != 0) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0035, code lost:
        if (r13 != 2) goto L_0x0037;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0108  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0I(int r13) {
        /*
        // Method dump skipped, instructions count: 307
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15260mp.A0I(int):void");
    }

    public final void A0J(int i) {
        ImageButton imageButton = this.A0I;
        if (imageButton == null) {
            return;
        }
        if (i == 0) {
            int i2 = ((C15270mq) this).A01;
            if (i2 != 0) {
                if (i2 == 2) {
                    ObjectAnimator ofFloat = ObjectAnimator.ofFloat(imageButton, "alpha", 1.0f, 0.7f);
                    ofFloat.setDuration(75L);
                    ofFloat.addListener(new C95714eE(this));
                    ofFloat.start();
                } else {
                    A0H();
                }
                ((C15270mq) this).A01 = 0;
            }
        } else if (i != 1) {
            if (i == 2 && ((C15270mq) this).A01 != 2) {
                Activity activity = ((AbstractC15280mr) this).A03;
                imageButton.setImageDrawable(AnonymousClass2GE.A01(activity, R.drawable.sticker_rec, R.color.stickerSuggestionIconTint));
                imageButton.setContentDescription(activity.getString(R.string.sticker_contexual_suggestion_content_description));
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(imageButton, "alpha", 0.7f, 1.0f);
                ofFloat2.setDuration(100L);
                ofFloat2.start();
                imageButton.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 4));
                ((C15270mq) this).A01 = 2;
            }
        } else if (((C15270mq) this).A01 != 1) {
            A0B();
        }
    }

    public void A0K(AbstractC116245Ur r2) {
        AnonymousClass2B7 r0 = this.A0A;
        if (r0 != null) {
            r0.A04 = r2;
        }
        AnonymousClass240 r02 = this.A07;
        if (r02 != null) {
            r02.A02 = r2;
        }
    }

    public void A0L(String str) {
        AnonymousClass2B7 r1 = this.A0A;
        if (r1 != null) {
            if (r1.A05 == null || !isShowing()) {
                A06();
            }
            A0I(2);
            AnonymousClass341 r2 = r1.A05;
            if (r2 != null) {
                r2.A0F = str;
                AbstractC69213Yj A01 = AnonymousClass341.A01(str, r2.A0I);
                if (A01 != null) {
                    r2.A02(r2.A0I.indexOf(A01), true);
                    r2.A0F = null;
                }
            }
        }
    }

    @Override // X.AbstractC15280mr, android.widget.PopupWindow
    public void dismiss() {
        AnonymousClass341 r2;
        AnonymousClass2B7 r0 = this.A0A;
        if (!(r0 == null || (r2 = r0.A05) == null)) {
            ((AnonymousClass242) r2).A0A.getViewTreeObserver().removeGlobalOnLayoutListener(r2.A0M);
            r2.A0J = false;
        }
        AnonymousClass3XR r1 = this.A09;
        if (r1 != null) {
            r1.A05.A0D.Ab2(new RunnableBRunnable0Shape15S0100000_I1_1(r1.A07, 28));
        }
        ((AbstractC15280mr) this).A07.A00.edit().putInt("emoji_popup_window_tab_state", this.A00).apply();
        super.dismiss();
    }
}
