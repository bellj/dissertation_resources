package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.1HV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HV extends BroadcastReceiver {
    public final /* synthetic */ C20640w5 A00;

    public AnonymousClass1HV(C20640w5 r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        C20640w5 r1 = this.A00;
        r1.A00 = null;
        if (!r1.A03()) {
            r1.A03.A04(8, null);
        }
    }
}
