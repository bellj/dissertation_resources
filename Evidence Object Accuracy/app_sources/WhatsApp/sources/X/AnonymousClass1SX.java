package X;

import com.facebook.profilo.core.ProvidersRegistry;

/* renamed from: X.1SX  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1SX extends AnonymousClass1SY {
    public static final int A00 = ProvidersRegistry.A00.A02("qpl");

    @Override // X.AnonymousClass1SY
    public void disable() {
    }

    @Override // X.AnonymousClass1SY
    public void enable() {
    }

    public AnonymousClass1SX() {
        super(null);
    }

    @Override // X.AnonymousClass1SY
    public int getSupportedProviders() {
        return A00;
    }

    @Override // X.AnonymousClass1SY
    public int getTracingProviders() {
        C29441Sr r0 = this.A01;
        if (r0 == null) {
            return 0;
        }
        return r0.A02 & A00;
    }
}
