package X;

import android.view.View;
import android.view.animation.Animation;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.3x7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83363x7 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ SelectionCheckView A01;

    public C83363x7(View view, SelectionCheckView selectionCheckView) {
        this.A01 = selectionCheckView;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.setVisibility(0);
    }
}
