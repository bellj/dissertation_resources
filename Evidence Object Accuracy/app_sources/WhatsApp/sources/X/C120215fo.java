package X;

import android.content.Context;

/* renamed from: X.5fo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120215fo extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass6MU A00;
    public final /* synthetic */ C129065x7 A01;
    public final /* synthetic */ C126375sm A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120215fo(Context context, C14900mE r2, C18650sn r3, AnonymousClass6MU r4, C129065x7 r5, C126375sm r6) {
        super(context, r2, r3);
        this.A01 = r5;
        this.A00 = r4;
        this.A02 = r6;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A00.APo(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        this.A00.APo(r2);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r4) {
        EnumC124545pi r0;
        try {
            AnonymousClass60A r02 = new AnonymousClass60A(this.A01.A00, r4, this.A02);
            AnonymousClass6MU r2 = this.A00;
            String str = r02.A03;
            if ("INACTIVE".equals(str)) {
                r0 = EnumC124545pi.A02;
            } else if ("ACTIVE".equals(str)) {
                r0 = EnumC124545pi.A01;
            } else {
                r0 = EnumC124545pi.A03;
            }
            r2.AX1(r0);
        } catch (AnonymousClass1V9 e) {
            C117305Zk.A1N("PaymentMerchantAccountActions", e.getMessage());
        }
    }
}
