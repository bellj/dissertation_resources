package X;

import java.io.InputStream;

/* renamed from: X.4Xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92754Xh {
    public final int A00;
    public final InputStream A01;
    public final byte[][] A02 = new byte[11];

    public C92754Xh(InputStream inputStream, int i) {
        this.A01 = inputStream;
        this.A00 = i;
    }

    public AnonymousClass1TN A00() {
        C92754Xh r1;
        InputStream inputStream = this.A01;
        int read = inputStream.read();
        if (read == -1) {
            return null;
        }
        boolean z = false;
        if (inputStream instanceof C114885Nl) {
            C114885Nl r0 = (C114885Nl) inputStream;
            r0.A02 = false;
            r0.A01();
        }
        int A00 = AnonymousClass1TO.A00(inputStream, read);
        boolean A1S = C12960it.A1S(read & 32);
        int i = this.A00;
        if (A00 == 4 || A00 == 16 || A00 == 17 || A00 == 8) {
            z = true;
        }
        int A01 = AnonymousClass1TO.A01(inputStream, i, z);
        if (A01 >= 0) {
            C72353eS r2 = new C72353eS(inputStream, A01, i);
            if ((read & 64) != 0) {
                return new AnonymousClass5M8(r2.A01(), A00, A1S);
            }
            if ((read & 128) != 0) {
                return new C112945Fk(new C92754Xh(r2, AnonymousClass1TQ.A02(r2)), A00, A1S);
            }
            if (A1S) {
                if (A00 == 4) {
                    r1 = new C92754Xh(r2, AnonymousClass1TQ.A02(r2));
                    return new C112885Fe(r1);
                } else if (A00 == 8) {
                    r1 = new C92754Xh(r2, AnonymousClass1TQ.A02(r2));
                    return new C112865Fc(r1);
                } else if (A00 == 16) {
                    return new C112915Fh(new C92754Xh(r2, AnonymousClass1TQ.A02(r2)));
                } else {
                    if (A00 == 17) {
                        return new C112935Fj(new C92754Xh(r2, AnonymousClass1TQ.A02(r2)));
                    }
                    StringBuilder A0k = C12960it.A0k("unknown tag ");
                    A0k.append(A00);
                    throw C12990iw.A0i(C12960it.A0d(" encountered", A0k));
                }
            } else if (A00 == 4) {
                return new C112895Ff(r2);
            } else {
                try {
                    return AnonymousClass1TO.A03(r2, this.A02, A00);
                } catch (IllegalArgumentException e) {
                    throw new AnonymousClass492("corrupted stream detected", e);
                }
            }
        } else if (A1S) {
            r1 = new C92754Xh(new C114885Nl(inputStream, i), i);
            if ((read & 64) != 0) {
                return new C112875Fd(r1, A00);
            }
            if ((read & 128) != 0) {
                return new C112945Fk(r1, A00, true);
            }
            if (A00 != 4) {
                if (A00 != 8) {
                    if (A00 == 16) {
                        return new C112905Fg(r1);
                    }
                    if (A00 == 17) {
                        return new C112925Fi(r1);
                    }
                    throw new AnonymousClass492(C12960it.A0d(Integer.toHexString(A00), C12960it.A0k("unknown BER object encountered: 0x")));
                }
                return new C112865Fc(r1);
            }
            return new C112885Fe(r1);
        } else {
            throw C12990iw.A0i("indefinite-length primitive encoding encountered");
        }
    }

    public C94954co A01() {
        AnonymousClass1TN A00 = A00();
        if (A00 == null) {
            return new C94954co(0);
        }
        C94954co r1 = new C94954co(10);
        do {
            r1.A06(A00 instanceof AnonymousClass5VQ ? ((AnonymousClass5VQ) A00).ADw() : A00.Aer());
            A00 = A00();
        } while (A00 != null);
        return r1;
    }

    public AnonymousClass1TL A02(int i, boolean z) {
        if (!z) {
            return new C114825Nf(new AnonymousClass5N5(((C72353eS) this.A01).A01()), i, false);
        }
        C94954co A01 = A01();
        boolean z2 = this.A01 instanceof C114885Nl;
        int i2 = A01.A00;
        if (z2) {
            if (i2 == 1) {
                return new C114815Ne(A01.A05(0), i, true);
            }
            return new C114815Ne(i2 < 1 ? AnonymousClass4HH.A00 : new AnonymousClass5NX(A01), i, false);
        } else if (i2 == 1) {
            return new C114825Nf(A01.A05(0), i, true);
        } else {
            return new C114825Nf(i2 < 1 ? AnonymousClass4HI.A00 : new AnonymousClass5NY(A01), i, false);
        }
    }
}
