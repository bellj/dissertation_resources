package X;

/* renamed from: X.4de  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95404de {
    public int A00;
    public C95404de A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final long A05;
    public final String A06;
    public final String A07;
    public final String A08;

    public C95404de(int i, int i2, int i3, long j) {
        this.A03 = i;
        this.A04 = i2;
        this.A07 = null;
        this.A06 = null;
        this.A08 = null;
        this.A05 = j;
        this.A02 = i3;
    }

    public C95404de(String str, int i, int i2, int i3) {
        this.A03 = i;
        this.A04 = i2;
        this.A07 = null;
        this.A06 = null;
        this.A08 = str;
        this.A05 = 0;
        this.A02 = i3;
    }

    public C95404de(String str, int i, int i2, long j) {
        this.A03 = i;
        this.A04 = 129;
        this.A07 = null;
        this.A06 = null;
        this.A08 = str;
        this.A05 = j;
        this.A02 = i2;
    }

    public C95404de(String str, String str2, int i, int i2) {
        this.A03 = i;
        this.A04 = 12;
        this.A07 = null;
        this.A06 = str;
        this.A08 = str2;
        this.A05 = 0;
        this.A02 = i2;
    }

    public C95404de(String str, String str2, String str3, int i, int i2, int i3, long j) {
        this.A03 = i;
        this.A04 = i2;
        this.A07 = str;
        this.A06 = str2;
        this.A08 = str3;
        this.A05 = j;
        this.A02 = i3;
    }
}
