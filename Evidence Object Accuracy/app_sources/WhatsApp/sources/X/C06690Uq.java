package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06690Uq implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0A2 A00;

    public C06690Uq(AnonymousClass0A2 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        AnonymousClass0A2 r1 = this.A00;
        r1.setAlpha(((Number) valueAnimator.getAnimatedValue()).intValue());
        r1.invalidateSelf();
    }
}
