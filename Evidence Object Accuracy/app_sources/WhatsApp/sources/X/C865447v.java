package X;

import android.content.Context;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.47v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865447v extends VideoSurfaceView {
    public final /* synthetic */ C865347u A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C865447v(Context context, C865347u r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // com.whatsapp.videoplayback.VideoSurfaceView, android.widget.MediaController.MediaPlayerControl
    public void start() {
        C865347u r1;
        AnonymousClass5V4 r0;
        if (A05() && (r0 = (r1 = this.A00).A03) != null) {
            r0.AWJ(r1);
        }
        super.start();
    }
}
