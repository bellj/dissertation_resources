package X;

import android.content.Context;
import android.view.ViewGroup;

/* renamed from: X.4vX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106334vX implements AnonymousClass5WW {
    public final /* synthetic */ C55992k9 A00;

    public C106334vX(C55992k9 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((ViewGroup) obj).setClipChildren(false);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return false;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((ViewGroup) obj).setClipChildren(true);
    }
}
