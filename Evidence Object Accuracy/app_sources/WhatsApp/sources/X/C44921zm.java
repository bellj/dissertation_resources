package X;

import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44921zm {
    public static final Set A00 = new HashSet();

    public static Object A00(AnonymousClass4UU r9, AbstractC44761zV r10, String str, int i) {
        int i2;
        int i3;
        int i4;
        C32881ct r2 = new C32881ct(new Random(), i, 3600000);
        try {
            Thread currentThread = Thread.currentThread();
            Set set = A00;
            synchronized (set) {
                set.add(currentThread);
            }
            set.size();
            int i5 = 0;
            while (true) {
                if (r10 != null && !r10.A00()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("gdrive-retry-task/condition-failed/");
                    sb.append(r10);
                    Log.i(sb.toString());
                    break;
                }
                int i6 = -1;
                try {
                    synchronized (r2) {
                        i4 = r2.A00;
                    }
                    Object A002 = r9.A00(i4);
                    if (A002 != null) {
                        if (i4 > 0) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("gdrive-retry-task/execute/attempt-");
                            sb2.append(i4);
                            sb2.append("/success: ");
                            sb2.append(str);
                            Log.i(sb2.toString());
                        }
                        A02(currentThread);
                        return A002;
                    }
                } catch (C84173yX e) {
                    i5++;
                    Log.e("gdrive-retry-task/execute", e);
                    i6 = e.retryAfter;
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("gdrive-retry-task/execute/attempt-");
                synchronized (r2) {
                    i2 = r2.A00;
                }
                sb3.append(i2);
                sb3.append("/failed: ");
                sb3.append(str);
                Log.i(sb3.toString());
                Long A003 = r2.A00();
                if (A003 != null) {
                    if (i6 > 0) {
                        A003 = Long.valueOf(TimeUnit.SECONDS.toMillis((long) i6));
                    }
                    try {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("gdrive/gdrive-retry-task backoff for ");
                        sb4.append(A003);
                        sb4.append(" milliseconds");
                        Log.i(sb4.toString());
                        Thread.sleep(A003.longValue());
                    } catch (InterruptedException e2) {
                        Log.i("gdrive-retry-task/interrupted", e2);
                    }
                } else if (i5 > 0) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Google Drive failures/total attempts: ");
                    sb5.append(i5);
                    sb5.append("/");
                    synchronized (r2) {
                        i3 = r2.A00;
                    }
                    sb5.append(i3);
                    throw new C84113yR(sb5.toString());
                }
            }
            A02(currentThread);
            return null;
        } catch (Throwable th) {
            A02(Thread.currentThread());
            throw th;
        }
    }

    public static void A01() {
        Set<Thread> set = A00;
        synchronized (set) {
            for (Thread thread : set) {
                thread.interrupt();
            }
        }
        StringBuilder sb = new StringBuilder("gdrive-retry-task/interrupt-active-tasks/size/");
        sb.append(set.size());
        Log.i(sb.toString());
    }

    public static void A02(Thread thread) {
        Set set = A00;
        synchronized (set) {
            set.remove(thread);
        }
        set.size();
    }
}
