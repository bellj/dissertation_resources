package X;

import com.whatsapp.group.NewGroup;

/* renamed from: X.3UF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UF implements AbstractC116455Vm {
    public final /* synthetic */ NewGroup A00;

    public AnonymousClass3UF(NewGroup newGroup) {
        this.A00 = newGroup;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A07);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        NewGroup newGroup = this.A00;
        AbstractC36671kL.A08(newGroup.A07, iArr, ((ActivityC13810kN) newGroup).A06.A02(AbstractC15460nI.A2A));
    }
}
