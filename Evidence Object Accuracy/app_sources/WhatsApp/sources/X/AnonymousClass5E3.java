package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.5E3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5E3 implements Executor {
    public final Handler A00;

    public AnonymousClass5E3(Looper looper) {
        this.A00 = new HandlerC73383g9(looper);
    }

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.A00.post(runnable);
    }
}
