package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.logger.BufferLogger;
import com.facebook.profilo.mmapbuf.core.Buffer;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.1Sl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29411Sl implements AbstractC29401Sk {
    @Override // X.AbstractC29401Sk
    public boolean A72(C29441Sr r2, File file) {
        return true;
    }

    @Override // X.AbstractC29401Sk
    public void AXc(C29441Sr r1) {
    }

    @Override // X.AbstractC29401Sk
    public void AXd(int i, int i2, int i3, int i4) {
    }

    @Override // X.AbstractC29401Sk
    public void AXe(C29441Sr r1) {
    }

    @Override // X.AbstractC29401Sk
    public void AXf(C29441Sr r1) {
    }

    @Override // X.AbstractC29401Sk
    public void AXg(C29441Sr r1) {
    }

    @Override // X.AbstractC29391Sf
    public void AXh(C29441Sr r1, int i) {
    }

    @Override // X.AbstractC29391Sf
    public void AXi(C29441Sr r1) {
    }

    @Override // X.AbstractC29391Sf
    public void AXj(C29441Sr r1, Throwable th) {
    }

    @Override // X.AbstractC29391Sf
    public void AXk(C29441Sr r1) {
    }

    @Override // X.AbstractC29401Sk
    public void AUP(C29441Sr r14) {
        long nanoTime = System.nanoTime();
        Buffer buffer = r14.A09;
        BufferLogger.writeBytesEntry(buffer, 0, 83, BufferLogger.writeStandardEntry(buffer, 4, 21, nanoTime, 0, 0, 0, 0), "Profilo.ProvidersInitialized");
        BufferLogger.writeStandardEntry(buffer, 4, 22, nanoTime, 0, 0, 0, 0);
    }

    @Override // X.AbstractC29401Sk
    public void AUQ(C29441Sr r15, int i) {
        C29451St r0 = ProvidersRegistry.A00;
        HashSet hashSet = new HashSet();
        ArrayList arrayList = r0.A01;
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            int i2 = 1;
            while (it.hasNext()) {
                Object next = it.next();
                if ((i2 & i) != 0) {
                    hashSet.add(next);
                }
                i2 <<= 1;
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            String str = (String) it2.next();
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append(str);
        }
        Buffer buffer = r15.A09;
        BufferLogger.writeBytesEntry(buffer, 0, 57, BufferLogger.writeBytesEntry(buffer, 0, 56, BufferLogger.writeStandardEntry(buffer, 6, 52, 0, 0, 8126514, 0, 0), "Active providers"), sb.toString());
    }
}
