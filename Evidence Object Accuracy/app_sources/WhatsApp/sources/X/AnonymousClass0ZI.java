package X;

import android.database.sqlite.SQLiteProgram;

/* renamed from: X.0ZI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZI implements AbstractC12940io {
    public final SQLiteProgram A00;

    public AnonymousClass0ZI(SQLiteProgram sQLiteProgram) {
        this.A00 = sQLiteProgram;
    }

    @Override // X.AbstractC12940io
    public void A6P(int i, byte[] bArr) {
        this.A00.bindBlob(i, bArr);
    }

    @Override // X.AbstractC12940io
    public void A6R(int i, double d) {
        this.A00.bindDouble(i, d);
    }

    @Override // X.AbstractC12940io
    public void A6S(int i, long j) {
        this.A00.bindLong(i, j);
    }

    @Override // X.AbstractC12940io
    public void A6T(int i) {
        this.A00.bindNull(i);
    }

    @Override // X.AbstractC12940io
    public void A6U(int i, String str) {
        this.A00.bindString(i, str);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.close();
    }
}
