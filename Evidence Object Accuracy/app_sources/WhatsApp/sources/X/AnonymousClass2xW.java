package X;

import android.text.SpannableStringBuilder;
import android.view.ViewGroup;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.text.ReadMoreTextView;

/* renamed from: X.2xW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xW extends AbstractC47392An {
    public C15370n3 A00;
    public final float A01;
    public final ViewGroup A02;
    public final C15450nH A03;
    public final AnonymousClass01d A04;
    public final C19990v2 A05;
    public final AnonymousClass19M A06;
    public final C16630pM A07;
    public final C253118x A08;
    public final boolean A09;

    public AnonymousClass2xW(ViewGroup viewGroup, Conversation conversation, C15450nH r4, AnonymousClass01d r5, C19990v2 r6, C15370n3 r7, AnonymousClass19M r8, C16630pM r9, C253118x r10, boolean z) {
        super(conversation, 10);
        this.A02 = viewGroup;
        this.A01 = C12970iu.A0N(conversation).A00();
        this.A08 = r10;
        this.A05 = r6;
        this.A06 = r8;
        this.A03 = r4;
        this.A04 = r5;
        this.A07 = r9;
        this.A00 = r7;
        this.A09 = z;
    }

    public final void A05() {
        ViewGroup viewGroup = this.A02;
        AbstractView$OnClickListenerC34281fs.A01(viewGroup, this, 1);
        ReadMoreTextView readMoreTextView = (ReadMoreTextView) viewGroup.findViewById(R.id.group_description_text);
        readMoreTextView.A02 = new AbstractC116255Us() { // from class: X.3aS
            @Override // X.AbstractC116255Us
            public final boolean AO3() {
                AnonymousClass2xW r0 = AnonymousClass2xW.this;
                Conversation conversation = ((AbstractC47392An) r0).A01;
                conversation.startActivity(C14960mK.A0O(conversation, r0.A00.A0D, false, true, true));
                return true;
            }
        };
        AnonymousClass01d r5 = this.A04;
        C16630pM r4 = this.A07;
        String str = this.A00.A0G.A02;
        Conversation conversation = super.A01;
        SpannableStringBuilder A0J = C12990iw.A0J(C42971wC.A03(r5, r4, AbstractC36671kL.A03(conversation, readMoreTextView.getPaint(), this.A06, str)));
        this.A08.A04(conversation, A0J);
        readMoreTextView.A0G(null, A0J);
    }

    public final void A06(AnonymousClass4KL r6) {
        ViewGroup viewGroup = this.A02;
        if (viewGroup.getVisibility() != 8) {
            AbstractC005102i A0N = C12970iu.A0N(super.A01);
            float A00 = A0N.A00();
            float f = this.A01;
            if (A00 != f) {
                A0N.A07(f);
                AnonymousClass028.A0V(viewGroup, 0.0f);
            }
            viewGroup.setVisibility(8);
            r6.A00.A01 = null;
        }
    }
}
