package X;

import java.io.IOException;

/* renamed from: X.20S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20S extends IOException {
    public AnonymousClass20S() {
    }

    public AnonymousClass20S(Throwable th) {
        initCause(th);
    }
}
