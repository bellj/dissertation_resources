package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.3gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC73673gc extends SurfaceView implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC73673gc(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AbstractC73673gc(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AbstractC73673gc(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public static C98164iF A00(VideoSurfaceView videoSurfaceView) {
        videoSurfaceView.A02 = 0;
        videoSurfaceView.A06 = 0;
        videoSurfaceView.A0E = null;
        videoSurfaceView.A0C = null;
        videoSurfaceView.A03 = -1;
        return new C98164iF(videoSurfaceView);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
