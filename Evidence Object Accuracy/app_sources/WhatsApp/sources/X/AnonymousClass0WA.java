package X;

import android.app.Activity;
import android.os.IBinder;
import android.view.View;
import java.lang.ref.WeakReference;

/* renamed from: X.0WA  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0WA implements View.OnAttachStateChangeListener {
    public final AnonymousClass0ZY A00;
    public final WeakReference A01;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
    }

    public AnonymousClass0WA(Activity activity, AnonymousClass0ZY r3) {
        this.A00 = r3;
        this.A01 = new WeakReference(activity);
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        C16700pc.A0E(view, 0);
        view.removeOnAttachStateChangeListener(this);
        Activity activity = (Activity) this.A01.get();
        IBinder A00 = AnonymousClass0LL.A00(activity);
        if (activity != null && A00 != null) {
            this.A00.A02(activity, A00);
        }
    }
}
