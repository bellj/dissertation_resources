package X;

import android.media.AudioDeviceCallback;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;

/* renamed from: X.39M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39M extends AnonymousClass3FX {
    public final AudioDeviceCallback A00 = new C73163fm(this);
    public final AnonymousClass01d A01;

    public /* synthetic */ AnonymousClass39M(AnonymousClass01d r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass3FX
    public void A01() {
        AudioManager A0G = this.A01.A0G();
        if (A0G != null) {
            A0G.registerAudioDeviceCallback(this.A00, null);
        }
    }

    @Override // X.AnonymousClass3FX
    public void A02() {
        AudioManager A0G = this.A01.A0G();
        if (A0G != null) {
            A0G.unregisterAudioDeviceCallback(this.A00);
        }
    }

    @Override // X.AnonymousClass3FX
    public boolean A03() {
        AudioManager A0G = this.A01.A0G();
        if (A0G != null) {
            for (AudioDeviceInfo audioDeviceInfo : A0G.getDevices(2)) {
                int type = audioDeviceInfo.getType();
                if (type == 4 || type == 3 || type == 11) {
                    return true;
                }
                if (Build.VERSION.SDK_INT >= 26 && type == 22) {
                    return true;
                }
            }
        }
        return false;
    }
}
