package X;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3H5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3H5 {
    public int A00;
    public final String A01;
    public final String A02;
    public final JSONArray A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;

    public AnonymousClass3H5(String str) {
        this.A00 = -1;
        this.A02 = str;
        this.A01 = str;
        this.A03 = null;
        this.A05 = false;
        this.A06 = false;
        this.A07 = false;
        this.A04 = false;
    }

    public AnonymousClass3H5(JSONObject jSONObject) {
        try {
            this.A00 = jSONObject.getInt("code");
        } catch (JSONException e) {
            AnonymousClass009.A08("GraphqlError : Failed to get error code", e);
            this.A00 = 0;
        }
        this.A03 = jSONObject.optJSONArray("path");
        this.A02 = jSONObject.optString("message");
        this.A05 = jSONObject.optBoolean("is_silent");
        this.A06 = jSONObject.optBoolean("is_transient");
        this.A01 = jSONObject.optString("description");
        this.A07 = jSONObject.optBoolean("requires_reauth");
        this.A04 = jSONObject.optBoolean("allow_user_retry");
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("GraphqlError{code=");
        A0k.append(this.A00);
        A0k.append(", message='");
        char A00 = C12990iw.A00(this.A02, A0k);
        A0k.append(", isSilent=");
        A0k.append(this.A05);
        A0k.append(", description='");
        A0k.append(this.A01);
        A0k.append(A00);
        A0k.append(", isTransient=");
        A0k.append(this.A06);
        A0k.append(", requiresReAuth=");
        A0k.append(this.A07);
        A0k.append(", allowUserRetry=");
        A0k.append(this.A04);
        return C12970iu.A0v(A0k);
    }
}
