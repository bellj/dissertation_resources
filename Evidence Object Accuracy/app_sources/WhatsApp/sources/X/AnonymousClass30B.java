package X;

/* renamed from: X.30B  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30B extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;

    public AnonymousClass30B() {
        super(3042, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAdvProtoMissing {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "advCompanionIdentityMissing", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "missingProtoType", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
