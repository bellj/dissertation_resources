package X;

import kotlin.coroutines.jvm.internal.DebugMetadata;

@DebugMetadata(c = "kotlinx.coroutines.flow.AbstractFlow", f = "Flow.kt", i = {0}, l = {227}, m = "collect", n = {"safeCollector"}, s = {"L$0"})
/* renamed from: X.5Ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113645Ik extends AbstractC113685Io {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ AnonymousClass5F6 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113645Ik(AnonymousClass5WO r2, AnonymousClass5F6 r3) {
        super(r2, r2.ABi());
        this.this$0 = r3;
    }
}
