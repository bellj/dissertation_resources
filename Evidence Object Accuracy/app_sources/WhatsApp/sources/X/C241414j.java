package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: X.14j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241414j {
    public AnonymousClass1IQ A00;
    public AbstractC248417c A01;
    public final AbstractC15710nm A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final AnonymousClass102 A05;
    public final C231410n A06;
    public volatile boolean A07;

    public C241414j(AbstractC15710nm r1, C14830m7 r2, C16590pI r3, AnonymousClass102 r4, C231410n r5) {
        this.A02 = r1;
        AnonymousClass009.A05(r3);
        this.A04 = r3;
        AnonymousClass009.A05(r2);
        this.A03 = r2;
        this.A06 = r5;
        this.A05 = r4;
    }

    public static AbstractC28901Pl A00(String str, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl r1 = (AbstractC28901Pl) it.next();
            if (str.equals(r1.A0A)) {
                return r1;
            }
        }
        return null;
    }

    public static AbstractC28901Pl A01(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl r2 = (AbstractC28901Pl) it.next();
            if (r2.A01 == 2) {
                return r2;
            }
        }
        return null;
    }

    public static final String A02(int[] iArr, int i) {
        int length;
        if (iArr == null || (length = iArr.length) == 0 || i == 0) {
            return null;
        }
        String[] strArr = new String[length];
        int i2 = 0;
        do {
            StringBuilder sb = new StringBuilder("consumer_status & ");
            int i3 = i << 2;
            sb.append(15 << i3);
            sb.append(" = ");
            sb.append((long) (iArr[i2] << i3));
            strArr[i2] = sb.toString();
            i2++;
        } while (i2 < length);
        return TextUtils.join(" OR ", strArr);
    }

    public static final void A03(Cursor cursor, AnonymousClass1ZO r4, UserJid userJid) {
        r4.A05 = userJid;
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("merchant")) != 1) {
            z = false;
        }
        r4.A0B(z);
        r4.A07().A00 = cursor.getLong(cursor.getColumnIndexOrThrow("consumer_status"));
        r4.A09(cursor.getInt(cursor.getColumnIndexOrThrow("default_payment_type")));
        r4.A04(cursor.getString(cursor.getColumnIndexOrThrow("country_data")));
    }

    public static boolean A04(List list) {
        if (list != null && list.size() > 0) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC28901Pl r1 = (AbstractC28901Pl) it.next();
                if (r1 != null) {
                    if (TextUtils.isEmpty(r1.A0A) || r1.A04() == 0) {
                        Log.w("PAY: PaymentsHelper sanitizePaymentMethods got empty credential id or account type");
                        return false;
                    } else if (AnonymousClass1ZS.A02(r1.A09)) {
                        r1.A0A(AbstractC28901Pl.A02(r1.A04()));
                    }
                }
            }
            return true;
        }
        return false;
    }

    public synchronized AnonymousClass1ZO A05(UserJid userJid) {
        AnonymousClass1ZO r3;
        String[] strArr = {userJid.getRawString()};
        r3 = null;
        AbstractC16840pq AGg = this.A01.AGg(C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(userJid))).A03, null);
        if (!(AGg == null || (r3 = AGg.AIi()) == null)) {
            C16310on A01 = this.A00.get();
            Cursor A08 = A01.A03.A08("contacts", "jid=?", null, null, AnonymousClass1ZU.A00, strArr);
            while (A08.moveToNext()) {
                A03(A08, r3, userJid);
            }
            A08.close();
            A01.close();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("PAY: PaymentStore readContactInfo returned: ");
        sb.append(r3);
        Log.i(sb.toString());
        return r3;
    }

    public AbstractC28901Pl A06() {
        for (AbstractC28901Pl r2 : A0B()) {
            if (r2.A01 == 2) {
                return r2;
            }
        }
        return null;
    }

    public final AbstractC28901Pl A07(Cursor cursor) {
        AnonymousClass1ZZ r3;
        AnonymousClass1ZX r32;
        String str;
        boolean z;
        boolean z2;
        String string = cursor.getString(cursor.getColumnIndexOrThrow("country"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("type"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("credential_id"));
        C17930rd A00 = C17930rd.A00(string);
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow("country_data"));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow("readable_name"));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow("issuer_name"));
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("subtype"));
        long j = ((long) cursor.getInt(cursor.getColumnIndexOrThrow("creation_ts"))) * 1000;
        long j2 = ((long) cursor.getInt(cursor.getColumnIndexOrThrow("updated_ts"))) * 1000;
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("debit_mode"));
        int i4 = cursor.getInt(cursor.getColumnIndexOrThrow("credit_mode"));
        int i5 = cursor.getInt(cursor.getColumnIndexOrThrow("p2m_debit_mode"));
        int i6 = cursor.getInt(cursor.getColumnIndexOrThrow("p2m_credit_mode"));
        byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow("icon"));
        AbstractC30871Zd r13 = null;
        String str2 = null;
        r13 = null;
        LinkedHashSet linkedHashSet = null;
        AbstractC30851Zb r132 = null;
        switch (i) {
            case 1:
            case 4:
            case 6:
            case 7:
            case 8:
                AbstractC16840pq AGg = this.A01.AGg(string, null);
                if (!(AGg == null || (r13 = AGg.AIh()) == null)) {
                    r13.A04(string3);
                }
                C30881Ze A06 = C30881Ze.A06(A00, r13, string2, string4, i, i3, i4, i5, i6, i2, j);
                A06.A0D = blob;
                return A06;
            case 2:
                AbstractC16840pq AGg2 = this.A01.AGg(string, null);
                if (!(AGg2 == null || (r132 = AGg2.AIg()) == null)) {
                    r132.A04(string3);
                }
                C30861Zc r0 = new C30861Zc(A00, i3, i4, j, j2);
                r0.A0A = string2;
                r0.A0A(string4);
                r0.A0B = string5;
                r0.A0D = blob;
                r0.A08 = r132;
                return r0;
            case 3:
                BigDecimal scaleByPowerOfTen = new BigDecimal(cursor.getLong(cursor.getColumnIndexOrThrow("balance_1000"))).scaleByPowerOfTen(-3);
                int i7 = cursor.getInt(cursor.getColumnIndexOrThrow("balance_ts"));
                AbstractC16840pq AGg3 = this.A01.AGg(string, null);
                if (AGg3 != null) {
                    r3 = AGg3.AIl();
                    if (r3 != null) {
                        r3.A04(string3);
                        linkedHashSet = r3.A09();
                    }
                } else {
                    r3 = null;
                }
                C30841Za r02 = new C30841Za(A00, string2, string4, scaleByPowerOfTen, linkedHashSet, i3, i4);
                r02.A08 = r3;
                r02.A0B = string5;
                r02.A00 = ((long) i7) * 1000;
                r02.A0D = blob;
                return r02;
            case 5:
                AbstractC16840pq AGg4 = this.A01.AGg(string, null);
                if (AGg4 != null) {
                    r32 = AGg4.AIj();
                    if (r32 != null) {
                        r32.A04(string3);
                        if (!TextUtils.isEmpty(string2)) {
                            r32.A0C = A0D();
                        }
                        str = r32.A08;
                        z = r32.A0D;
                        z2 = r32.A0E;
                        str2 = r32.A07;
                        return new AnonymousClass1ZW(A00, r32, string2, str2, str, string4, z, z2);
                    }
                    str = null;
                } else {
                    str = null;
                    r32 = null;
                }
                z = false;
                z2 = false;
                return new AnonymousClass1ZW(A00, r32, string2, str2, str, string4, z, z2);
            default:
                return null;
        }
    }

    public AbstractC28901Pl A08(String str) {
        String[] strArr = {str};
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("methods", "credential_id=?", null, null, AnonymousClass1ZV.A00, strArr);
            AbstractC28901Pl A07 = A08.moveToLast() ? A07(A08) : null;
            A08.close();
            A01.close();
            return A07;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A09() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("methods", "type = ?", null, null, AnonymousClass1ZV.A00, new String[]{String.valueOf(5)});
            while (A08.moveToNext()) {
                AbstractC28901Pl A07 = A07(A08);
                if (A07 != null) {
                    arrayList.add((AnonymousClass1ZW) A07);
                }
            }
            A08.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A0A() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("methods", null, "debit_mode DESC", null, AnonymousClass1ZV.A00, null);
            while (A08.moveToNext()) {
                AbstractC28901Pl A07 = A07(A08);
                if (A07 != null) {
                    arrayList.add(A07);
                }
            }
            A08.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A0B() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("methods", "type != ?", "debit_mode DESC", null, AnonymousClass1ZV.A00, new String[]{String.valueOf(5)});
            while (A08.moveToNext()) {
                AbstractC28901Pl A07 = A07(A08);
                if (A07 != null) {
                    arrayList.add(A07);
                }
            }
            A08.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public synchronized List A0C() {
        return A0E(null, 0);
    }

    public synchronized List A0D() {
        List emptyList;
        C16310on A01 = this.A00.get();
        emptyList = Collections.emptyList();
        A01.close();
        return emptyList;
    }

    public synchronized List A0E(int[] iArr, int i) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        C16310on A01 = this.A00.get();
        Cursor A08 = A01.A03.A08("contacts", A02(iArr, i), null, null, AnonymousClass1ZU.A00, null);
        while (true) {
            try {
                AnonymousClass1ZO r7 = null;
                if (!A08.moveToNext()) {
                    break;
                }
                UserJid nullable = UserJid.getNullable(A08.getString(A08.getColumnIndexOrThrow("jid")));
                if (nullable == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("PAY: PaymentTransactionStore/readContactInfos: Skipping Jid because it is not valid: ");
                    sb.append(A08.getString(A08.getColumnIndexOrThrow("jid")));
                    Log.i(sb.toString());
                } else {
                    AbstractC16840pq AGg = this.A01.AGg(C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(nullable))).A03, null);
                    if (AGg != null) {
                        r7 = AGg.AIi();
                    }
                    if (r7 != null) {
                        A03(A08, r7, nullable);
                        arrayList.add(r7);
                    }
                }
            } catch (Throwable th) {
                if (A08 != null) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
        A08.close();
        A01.close();
        if (iArr != null) {
            ArrayList arrayList2 = new ArrayList();
            for (int i2 : iArr) {
                arrayList2.add(Integer.valueOf(i2));
            }
        }
        return arrayList;
    }

    public synchronized void A0F() {
        AnonymousClass1IQ r0 = this.A00;
        if (r0 != null) {
            r0.close();
        }
        File databasePath = this.A04.A00.getDatabasePath("payments.db");
        AnonymousClass1Tx.A04(databasePath, "PAY");
        if (databasePath.delete()) {
            StringBuilder sb = new StringBuilder();
            sb.append("PAY: PaymentStore deleteStore deleted ");
            sb.append(databasePath);
            Log.i(sb.toString());
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("PAY: PaymentStore failed to delete ");
            sb2.append(databasePath);
            Log.e(sb2.toString());
        }
        this.A07 = false;
    }

    public void A0G(AbstractC30891Zf r12, String str) {
        String[] strArr = {str};
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("tmp_transactions", "tmp_id=?", null, null, C30901Zg.A00, strArr);
            while (A08.moveToNext()) {
                String string = A08.getString(A08.getColumnIndexOrThrow("tmp_metadata"));
                long j = ((long) A08.getInt(A08.getColumnIndexOrThrow("tmp_ts"))) * 1000;
                r12.A0S(str);
                r12.A04(string);
                if (j > -1) {
                    r12.A0O(j);
                }
            }
            A08.close();
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public synchronized void A0H(UserJid userJid) {
        AnonymousClass1ZO A05;
        if (this.A01 != null) {
            String str = C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(userJid))).A03;
            if (!TextUtils.isEmpty(str) && !str.equals("UNSET") && (A05 = A05(userJid)) != null && A05.A05 != null) {
                A05.A01 = this.A03.A00() + TimeUnit.DAYS.toMillis(1);
                A0J(A05);
            }
        }
    }

    public boolean A0I() {
        boolean z;
        C16310on A02 = this.A00.A02();
        try {
            int A01 = A02.A03.A01("methods", null, null);
            if (A01 >= 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("PAY: PaymentStore removeAllPaymentMethods deleted num rows: ");
                sb.append(A01);
                Log.i(sb.toString());
                z = true;
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("PAY: PaymentStore removeAllPaymentMethods could not delete all rows: ");
                sb2.append(A01);
                Log.w(sb2.toString());
                z = false;
            }
            A02.close();
            return z;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public synchronized boolean A0J(AnonymousClass1ZO r18) {
        boolean z;
        long j;
        ArrayList arrayList = new ArrayList();
        arrayList.add(r18);
        C16310on A02 = this.A00.A02();
        AnonymousClass1Lx A00 = A02.A00();
        try {
            Iterator it = arrayList.iterator();
            long j2 = 0;
            while (true) {
                z = true;
                int i = 1;
                if (!it.hasNext()) {
                    break;
                }
                AnonymousClass1ZO r7 = (AnonymousClass1ZO) it.next();
                UserJid userJid = r7.A05;
                if (userJid != null) {
                    AnonymousClass1ZO A05 = A05(userJid);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("jid", userJid.getRawString());
                    contentValues.put("country_data", r7.A03());
                    int i2 = 0;
                    if (r7.A0C()) {
                        i2 = 1;
                    }
                    contentValues.put("merchant", Integer.valueOf(i2));
                    contentValues.put("consumer_status", Long.valueOf(r7.A07().A00));
                    contentValues.put("default_payment_type", Integer.valueOf(r7.A05()));
                    if (A05 == null || A05.A05 == null) {
                        j = A02.A03.A02(contentValues, "contacts");
                    } else {
                        j = (long) A02.A03.A00("contacts", contentValues, "jid=?", new String[]{userJid.getRawString()});
                    }
                    if (j < 0) {
                        i = 0;
                    }
                    j2 += (long) i;
                }
            }
            A00.A00();
            A00.close();
            A02.close();
            StringBuilder sb = new StringBuilder("PAY: PaymentStore storeContacts stored: ");
            sb.append(j2);
            sb.append(" rows with contacts size: ");
            sb.append(arrayList.size());
            Log.i(sb.toString());
            if (j2 != ((long) arrayList.size())) {
                z = false;
            }
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
        if (r4 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0128, code lost:
        if (r1 != false) goto L_0x012a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0K(com.whatsapp.jid.UserJid r13, java.lang.Boolean r14, java.lang.String r15, java.util.HashMap r16, java.util.HashMap r17) {
        /*
        // Method dump skipped, instructions count: 306
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C241414j.A0K(com.whatsapp.jid.UserJid, java.lang.Boolean, java.lang.String, java.util.HashMap, java.util.HashMap):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004d, code lost:
        if (r2 >= 0) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0L(java.lang.String r8) {
        /*
            r7 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            r6 = 0
            if (r0 == 0) goto L_0x000d
            java.lang.String r0 = "PAY: PaymentStore removePaymentMethod called with empty credentialId"
            com.whatsapp.util.Log.w(r0)
            return r6
        L_0x000d:
            X.1IQ r0 = r7.A00
            X.0on r5 = r0.A02()
            X.0op r4 = r5.A03     // Catch: all -> 0x0054
            java.lang.String r3 = "methods"
            java.lang.String r2 = "credential_id=?"
            r1 = 1
            java.lang.String[] r0 = new java.lang.String[r1]     // Catch: all -> 0x0054
            r0[r6] = r8     // Catch: all -> 0x0054
            int r2 = r4.A01(r3, r2, r0)     // Catch: all -> 0x0054
            if (r2 != r1) goto L_0x0039
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0054
            r1.<init>()     // Catch: all -> 0x0054
            java.lang.String r0 = "PAY: PaymentStore removePaymentMethod deleted: "
            r1.append(r0)     // Catch: all -> 0x0054
            r1.append(r8)     // Catch: all -> 0x0054
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0054
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x0054
            goto L_0x004f
        L_0x0039:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0054
            r1.<init>()     // Catch: all -> 0x0054
            java.lang.String r0 = "PAY: PaymentStore removePaymentMethod could not delete: "
            r1.append(r0)     // Catch: all -> 0x0054
            r1.append(r8)     // Catch: all -> 0x0054
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0054
            com.whatsapp.util.Log.w(r0)     // Catch: all -> 0x0054
            if (r2 < 0) goto L_0x0050
        L_0x004f:
            r6 = 1
        L_0x0050:
            r5.close()
            return r6
        L_0x0054:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0058
        L_0x0058:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C241414j.A0L(java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c6 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d5 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0137 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0156 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x015f A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0166 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0176 A[Catch: all -> 0x0200, TryCatch #2 {all -> 0x0205, blocks: (B:7:0x002a, B:64:0x01be, B:77:0x01f9, B:8:0x002e, B:9:0x0034, B:11:0x003a, B:13:0x0058, B:17:0x0062, B:20:0x0075, B:32:0x0091, B:34:0x0098, B:36:0x009e, B:37:0x00a2, B:38:0x00a5, B:40:0x00c6, B:41:0x00cf, B:43:0x00d5, B:44:0x00dc, B:47:0x0137, B:49:0x0156, B:50:0x015b, B:52:0x015f, B:54:0x0166, B:55:0x0176, B:59:0x0185, B:60:0x019b, B:62:0x01a1, B:63:0x01a9, B:67:0x01c5, B:68:0x01c9, B:70:0x01cf, B:72:0x01df, B:75:0x01f3, B:76:0x01f6), top: B:84:0x002a }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0182  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01a9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0M(java.util.List r31) {
        /*
        // Method dump skipped, instructions count: 522
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C241414j.A0M(java.util.List):boolean");
    }
}
