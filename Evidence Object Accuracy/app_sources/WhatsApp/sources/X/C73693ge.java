package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/* renamed from: X.3ge  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73693ge extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(52);
    public int A00;
    public int A01;
    public boolean A02;

    public /* synthetic */ C73693ge(Parcel parcel) {
        super(parcel);
        this.A02 = C12970iu.A1W(parcel.readInt());
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
    }

    public C73693ge(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A02 ? 1 : 0);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
    }
}
