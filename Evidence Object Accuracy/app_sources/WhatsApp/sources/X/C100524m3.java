package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4m3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100524m3 implements Parcelable {
    @Deprecated
    public static final Parcelable.Creator CREATOR = new C98914jS();
    public String A00;
    public String A01;
    public String A02;

    @Override // android.os.Parcelable
    @Deprecated
    public final int describeContents() {
        return 0;
    }

    @Deprecated
    public C100524m3() {
    }

    @Deprecated
    public C100524m3(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
    }

    @Override // android.os.Parcelable
    @Deprecated
    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
    }
}
