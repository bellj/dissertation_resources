package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78053oI extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98884jP();
    public final int A00 = 1;
    public final String A01;

    public C78053oI(String str) {
        C13020j0.A01(str);
        this.A01 = str;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0C(parcel, this.A01, 2, A00);
    }
}
