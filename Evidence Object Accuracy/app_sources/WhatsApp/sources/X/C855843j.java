package X;

/* renamed from: X.43j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855843j extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public String A02;
    public String A03;
    public String A04;

    public C855843j() {
        super(3248, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(4, null);
        r3.Abe(5, this.A01);
        r3.Abe(6, null);
        r3.Abe(7, this.A03);
        r3.Abe(9, this.A04);
        r3.Abe(8, null);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDirectoryServerRequests {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "directorySessionId", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "endResponseTime", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "httpProtocolVersion", null);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "httpResponseCode", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkEngineVersion", null);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "neworkLibraryType", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "requestEndpoint", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "startResponseTime", null);
        return C12960it.A0d("}", A0k);
    }
}
