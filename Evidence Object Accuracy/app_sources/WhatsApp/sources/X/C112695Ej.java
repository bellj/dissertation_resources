package X;

/* renamed from: X.5Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112695Ej implements AnonymousClass5ZU, AbstractC115495Rt {
    @Override // X.AnonymousClass5ZU
    public AbstractC115495Rt getKey() {
        return this;
    }

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r3) {
        C16700pc.A0E(r3, 2);
        return r3.AJ5(obj, this);
    }

    @Override // X.AnonymousClass5ZU, X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return C95104d9.A01(this, r2);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r2) {
        return C95104d9.A02(this, r2);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r2) {
        return C95104d9.A03(this, r2);
    }
}
