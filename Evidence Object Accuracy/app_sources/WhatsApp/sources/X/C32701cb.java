package X;

import android.os.Bundle;

/* renamed from: X.1cb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32701cb {
    public final String A00;
    public final String A01;
    public final boolean A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;

    public /* synthetic */ C32701cb(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.A00 = str;
        this.A01 = str2;
        this.A03 = z;
        this.A06 = z2;
        this.A04 = z3;
        this.A02 = z4;
        this.A05 = z5;
        this.A07 = z6;
    }

    public static C32701cb A00(Bundle bundle) {
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        String str = null;
        String str2 = null;
        if (bundle != null) {
            str2 = bundle.getString("data", null);
            str = bundle.getString("source", null);
            z = bundle.getBoolean("has_ib");
            z2 = bundle.getBoolean("show_biz_preview");
            z3 = bundle.getBoolean("has_wm");
            z4 = bundle.getBoolean("ads_logging_requires_tos");
            z5 = bundle.getBoolean("show_ad_attribution");
            z6 = bundle.getBoolean("show_keyboard");
        }
        return new C32701cb(str2, str, z, z2, z3, z4, z5, z6);
    }

    public Bundle A01() {
        Bundle bundle = new Bundle();
        bundle.putString("data", this.A00);
        bundle.putString("source", this.A01);
        bundle.putBoolean("has_ib", this.A03);
        bundle.putBoolean("show_biz_preview", this.A06);
        bundle.putBoolean("has_wm", this.A04);
        bundle.putBoolean("ads_logging_requires_tos", this.A02);
        bundle.putBoolean("show_ad_attribution", this.A05);
        bundle.putBoolean("show_keyboard", this.A07);
        return bundle;
    }
}
