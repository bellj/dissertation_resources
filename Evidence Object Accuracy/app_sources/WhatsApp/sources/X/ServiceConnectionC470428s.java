package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.util.Log;

/* renamed from: X.28s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ServiceConnectionC470428s implements ServiceConnection {
    public final String A00;
    public final String A01;
    public final boolean A02;
    public final /* synthetic */ AnonymousClass13A A03;

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
    }

    public ServiceConnectionC470428s(AnonymousClass13A r1, String str, String str2, boolean z) {
        this.A03 = r1;
        this.A00 = str;
        this.A01 = str2;
        this.A02 = z;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        AnonymousClass13A r2 = this.A03;
        try {
            if (r2.A02.A01(componentName.getPackageName()).A03 && this.A01.equals(componentName.getPackageName())) {
                r2.A05.execute(new RunnableBRunnable0Shape5S0200000_I0_5(this, 24, iBinder));
                return;
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        Log.e("CallbackServiceProxy/service component mismatch.");
        r2.A00.A00.unbindService(this);
    }
}
