package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.1Dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26531Dv {
    public C16440p1 A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C14650lo A05;
    public final C15550nR A06;
    public final C15610nY A07;
    public final C17050qB A08;
    public final C14830m7 A09;
    public final C16590pI A0A;
    public final C18360sK A0B;
    public final C14820m6 A0C;
    public final AnonymousClass018 A0D;
    public final C14950mJ A0E;
    public final C17650rA A0F;
    public final C15650ng A0G;
    public final AnonymousClass12H A0H;
    public final AnonymousClass102 A0I;
    public final C14850m9 A0J;
    public final C16120oU A0K;
    public final C20330va A0L;
    public final C22370yy A0M;
    public final C17070qD A0N;
    public final C22940zt A0O;
    public final C20320vZ A0P;

    public C26531Dv(AbstractC15710nm r2, C14330lG r3, C14900mE r4, C15570nT r5, C14650lo r6, C15550nR r7, C15610nY r8, C17050qB r9, C14830m7 r10, C16590pI r11, C18360sK r12, C14820m6 r13, AnonymousClass018 r14, C14950mJ r15, C17650rA r16, C15650ng r17, AnonymousClass12H r18, AnonymousClass102 r19, C14850m9 r20, C16120oU r21, C20330va r22, C22370yy r23, C17070qD r24, C22940zt r25, C20320vZ r26) {
        this.A0A = r11;
        this.A09 = r10;
        this.A0J = r20;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A0K = r21;
        this.A0E = r15;
        this.A06 = r7;
        this.A07 = r8;
        this.A0D = r14;
        this.A0P = r26;
        this.A0N = r24;
        this.A0G = r17;
        this.A0H = r18;
        this.A08 = r9;
        this.A0O = r25;
        this.A0L = r22;
        this.A0C = r13;
        this.A0M = r23;
        this.A0B = r12;
        this.A05 = r6;
        this.A0I = r19;
        this.A0F = r16;
    }

    public static C16440p1 A00(AbstractC15710nm r26, C15570nT r27, C14650lo r28, C15550nR r29, C16590pI r30, AnonymousClass018 r31, C17650rA r32, AnonymousClass102 r33, C14850m9 r34, C17070qD r35, C22940zt r36, C20320vZ r37, byte[] bArr) {
        try {
            C27081Fy A01 = C27081Fy.A01(bArr);
            if (A01 != null) {
                return (C16440p1) C32401c6.A05(r26, r27, r28, r29, r30, r31, r32, r33, r34, null, r35, r36, C32401c6.A01(r34, A01), A01, new AnonymousClass1IS(C29971Vl.A00, "", false), r37, 0, false, false, false);
            }
            Log.e("gdpr/create-gdpr-message/null");
            return null;
        } catch (C28971Pt | C43271wi e) {
            Log.e("gdpr/create-gdpr-message", e);
            return null;
        }
    }

    public synchronized int A01() {
        return this.A0C.A00.getInt("gdpr_report_state", 0);
    }

    public synchronized long A02() {
        return this.A0C.A00.getLong("gdpr_report_timestamp", -1);
    }

    public C16440p1 A03() {
        if (this.A00 == null) {
            C16590pI r4 = this.A0A;
            byte[] A0G = C003501n.A0G(new File(r4.A00.getFilesDir(), "gdpr.info"));
            if (A0G != null) {
                C14850m9 r8 = this.A0J;
                AbstractC15710nm r0 = this.A01;
                C15550nR r3 = this.A06;
                AnonymousClass018 r5 = this.A0D;
                C22940zt r10 = this.A0O;
                C14650lo r2 = this.A05;
                C15570nT r1 = this.A04;
                C20320vZ r11 = this.A0P;
                C17070qD r9 = this.A0N;
                this.A00 = A00(r0, r1, r2, r3, r4, r5, this.A0F, this.A0I, r8, r9, r10, r11, A0G);
            }
        }
        return this.A00;
    }

    public synchronized void A04() {
        Log.i("gdpr/on-report-deleted");
        A05();
    }

    public synchronized void A05() {
        Log.i("gdpr/reset");
        this.A00 = null;
        File file = new File(this.A0A.A00.getFilesDir(), "gdpr.info");
        if (file.exists() && !file.delete()) {
            Log.e("gdpr/reset/failed-delete-report-info");
        }
        C14350lI.A0D(this.A02.A08(), 0);
        this.A0C.A0M();
    }

    public synchronized void A06(long j) {
        Log.i("gdpr/on-report-requested");
        C14820m6 r1 = this.A0C;
        r1.A0T(1);
        r1.A0q("gdpr_report_timestamp", j);
    }

    public synchronized void A07(byte[] bArr, long j, long j2) {
        Log.i("gdpr/on-report-available");
        try {
            C16590pI r4 = this.A0A;
            C003501n.A08(new File(r4.A00.getFilesDir(), "gdpr.info"), bArr);
            C14850m9 r8 = this.A0J;
            AbstractC15710nm r0 = this.A01;
            C15550nR r3 = this.A06;
            AnonymousClass018 r5 = this.A0D;
            C22940zt r10 = this.A0O;
            C14650lo r2 = this.A05;
            C15570nT r1 = this.A04;
            C20320vZ r11 = this.A0P;
            C17070qD r9 = this.A0N;
            C16440p1 A00 = A00(r0, r1, r2, r3, r4, r5, this.A0F, this.A0I, r8, r9, r10, r11, bArr);
            this.A00 = A00;
            if (A00 == null) {
                Log.e("gdpr/on-report-available/cannot-create-message");
            } else {
                C14820m6 r12 = this.A0C;
                r12.A0T(2);
                r12.A0q("gdpr_report_timestamp", j);
                r12.A00.edit().putLong("gdpr_report_expiration_timestamp", j2).apply();
            }
        } catch (IOException e) {
            Log.e("gdpr/on-report-available/cannot-save", e);
        }
    }
}
