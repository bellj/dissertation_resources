package X;

import android.os.Looper;
import android.util.Log;
import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3Ep  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64193Ep {
    public int A00;
    public Looper A01;
    public Object A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public final AnonymousClass5Px A06;
    public final AnonymousClass5SG A07;
    public final Timeline A08;
    public final AnonymousClass5Xd A09;

    public C64193Ep(Looper looper, AnonymousClass5Px r2, AnonymousClass5SG r3, Timeline timeline, AnonymousClass5Xd r5) {
        this.A06 = r2;
        this.A07 = r3;
        this.A08 = timeline;
        this.A01 = looper;
        this.A09 = r5;
    }

    public void A00() {
        C95314dV.A04(!this.A05);
        this.A05 = true;
        C107544xV r2 = (C107544xV) this.A06;
        synchronized (r2) {
            if (r2.A0F || !r2.A0L.isAlive()) {
                Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
                A01(false);
            } else {
                ((C107914yA) r2.A0Z).A00.obtainMessage(14, this).sendToTarget();
            }
        }
    }

    public synchronized void A01(boolean z) {
        this.A03 = z | this.A03;
        this.A04 = true;
        notifyAll();
    }
}
