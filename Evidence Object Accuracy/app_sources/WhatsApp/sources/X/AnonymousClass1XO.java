package X;

import android.database.Cursor;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;

/* renamed from: X.1XO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XO extends AnonymousClass1XP implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public String A00;
    public String A01;
    public String A02;

    public AnonymousClass1XO(AnonymousClass1IS r2, byte b, long j) {
        super(r2, (byte) 30, j);
    }

    public AnonymousClass1XO(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 5, j);
    }

    public AnonymousClass1XO(AnonymousClass1IS r2, AnonymousClass1XO r3, byte b, long j, boolean z) {
        super(r2, r3, b, j, z);
        this.A01 = r3.A01;
        this.A00 = r3.A00;
        this.A02 = r3.A02;
    }

    @Override // X.AnonymousClass1XP
    public void A14(Cursor cursor) {
        super.A14(cursor);
        this.A01 = cursor.getString(cursor.getColumnIndexOrThrow("place_name"));
        this.A00 = cursor.getString(cursor.getColumnIndexOrThrow("place_address"));
        this.A02 = cursor.getString(cursor.getColumnIndexOrThrow("url"));
    }

    @Override // X.AnonymousClass1XP
    public void A15(Cursor cursor, C15570nT r3) {
        super.A15(cursor, r3);
        this.A01 = cursor.getString(cursor.getColumnIndexOrThrow("place_name"));
        this.A00 = cursor.getString(cursor.getColumnIndexOrThrow("place_address"));
        this.A02 = cursor.getString(cursor.getColumnIndexOrThrow("url"));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if (r1.startsWith(r2) != false) goto L_0x001d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A16() {
        /*
            r4 = this;
            java.lang.String r2 = r4.A01
            java.lang.String r1 = r4.A00
            java.lang.String r0 = r4.A02
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x006c
            if (r1 == 0) goto L_0x005e
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x001d
            X.AnonymousClass009.A05(r2)
            boolean r0 = r1.startsWith(r2)
            if (r0 == 0) goto L_0x005e
        L_0x001d:
            java.lang.String r3 = r4.A00
        L_0x001f:
            X.AnonymousClass009.A05(r3)
        L_0x0022:
            r1 = 300(0x12c, float:4.2E-43)
            int r0 = r3.length()
            if (r0 <= r1) goto L_0x002f
            r0 = 0
            java.lang.String r3 = r3.substring(r0, r1)
        L_0x002f:
            java.lang.String r0 = "https://maps.google.com/maps?q="
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            java.lang.String r1 = "\\s+"
            java.lang.String r0 = "+"
            java.lang.String r0 = r3.replaceAll(r1, r0)
            java.lang.String r0 = android.net.Uri.encode(r0)
            r2.append(r0)
            java.lang.String r0 = "&sll="
            r2.append(r0)
            double r0 = r4.A00
            r2.append(r0)
            java.lang.String r0 = ","
            r2.append(r0)
            double r0 = r4.A01
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            return r0
        L_0x005e:
            java.lang.String r0 = r4.A17()
            if (r0 == 0) goto L_0x0069
            java.lang.String r3 = r4.A17()
            goto L_0x001f
        L_0x0069:
            java.lang.String r3 = ""
            goto L_0x0022
        L_0x006c:
            java.lang.String r0 = r4.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1XO.A16():java.lang.String");
    }

    public String A17() {
        String str = this.A01;
        String str2 = this.A00;
        if (str2 == null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("\n");
        sb.append(str2);
        return sb.toString();
    }

    public void A18(C15570nT r9, C14850m9 r10, C82173vC r11, AnonymousClass1PG r12, byte[] bArr, boolean z) {
        C16460p3 A0G = A0G();
        double d = ((AnonymousClass1XP) this).A00;
        r11.A03();
        C57672nR r1 = (C57672nR) r11.A00;
        r1.A04 |= 1;
        r1.A00 = d;
        double d2 = ((AnonymousClass1XP) this).A01;
        r11.A03();
        C57672nR r13 = (C57672nR) r11.A00;
        r13.A04 |= 2;
        r13.A01 = d2;
        if (!TextUtils.isEmpty(this.A02)) {
            String str = this.A02;
            r11.A03();
            C57672nR r14 = (C57672nR) r11.A00;
            r14.A04 |= 16;
            r14.A0B = str;
        }
        if (!TextUtils.isEmpty(this.A01)) {
            String str2 = this.A01;
            r11.A03();
            C57672nR r15 = (C57672nR) r11.A00;
            r15.A04 |= 4;
            r15.A0A = str2;
        }
        if (!TextUtils.isEmpty(this.A00)) {
            String str3 = this.A00;
            r11.A03();
            C57672nR r16 = (C57672nR) r11.A00;
            r16.A04 |= 8;
            r16.A08 = str3;
        }
        if (!z && A0G.A07() != null) {
            byte[] A07 = A0G.A07();
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(A07, 0, A07.length);
            r11.A03();
            C57672nR r17 = (C57672nR) r11.A00;
            r17.A04 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            r17.A06 = A01;
        }
        if (C32411c7.A0U(r12, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r9, r10, r12, this, bArr, z);
            r11.A03();
            C57672nR r18 = (C57672nR) r11.A00;
            r18.A07 = A0P;
            r18.A04 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
    }

    public void A19(C57672nR r4, boolean z) {
        ((AnonymousClass1XP) this).A00 = r4.A00;
        ((AnonymousClass1XP) this).A01 = r4.A01;
        int i = r4.A04;
        if ((i & 16) == 16) {
            this.A02 = r4.A0B;
        }
        if ((i & 4) == 4) {
            this.A01 = r4.A0A;
        }
        if ((i & 8) == 8) {
            this.A00 = r4.A08;
        }
        if ((i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            A0G().A03(r4.A06.A04(), z);
            ((AnonymousClass1XP) this).A02 = 2;
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r13) {
        AbstractC27091Fz r0;
        C28891Pk r02;
        if (!(this instanceof AnonymousClass1XN)) {
            AnonymousClass1G3 r2 = r13.A03;
            C57672nR r03 = ((C27081Fy) r2.A00).A0Q;
            if (r03 == null) {
                r03 = C57672nR.A0D;
            }
            C82173vC r8 = (C82173vC) r03.A0T();
            A18(r13.A00, r13.A02, r8, r13.A04, r13.A09, r13.A06);
            if (C35011h5.A04(this)) {
                C57552nF r04 = ((C27081Fy) r2.A00).A03;
                if (r04 == null) {
                    r04 = C57552nF.A08;
                }
                C56852m3 r3 = (C56852m3) r04.A0T();
                C35011h5.A03(r3, A0F().A00);
                r3.A03();
                C57552nF r1 = (C57552nF) r3.A00;
                r1.A05 = r8.A02();
                r1.A01 = 5;
                r3.A05(AnonymousClass39x.A04);
                r2.A06((C57552nF) r3.A02());
                return;
            }
            r2.A03();
            C27081Fy r12 = (C27081Fy) r2.A00;
            r12.A0Q = (C57672nR) r8.A02();
            r12.A00 |= 16;
            return;
        }
        AnonymousClass1XN r5 = (AnonymousClass1XN) this;
        AnonymousClass1G3 r4 = r13.A03;
        AnonymousClass2Lw r32 = (AnonymousClass2Lw) r4.A05().A0T();
        AnonymousClass2Ly r22 = r4.A05().A03;
        if (r22 == null) {
            r22 = AnonymousClass2Ly.A07;
        }
        if (r22.A01 == 5) {
            r0 = (AbstractC27091Fz) r22.A03;
        } else {
            r0 = C57672nR.A0D;
        }
        C82173vC r82 = (C82173vC) r0.A0T();
        r5.A18(r13.A00, r13.A02, r82, r13.A04, r13.A09, r13.A06);
        if (r82 == null || (r02 = r5.A00) == null) {
            StringBuilder sb = new StringBuilder("FMessageTemplateLocation/buildE2eMessage/Error: cannot send encrypted template location message, ");
            sb.append((int) r5.A0y);
            Log.w(sb.toString());
            return;
        }
        AnonymousClass2Lx A00 = C63173Ao.A00(r4, r02);
        A00.A03();
        AnonymousClass2Ly r14 = (AnonymousClass2Ly) A00.A00;
        r14.A03 = r82.A02();
        r14.A01 = 5;
        r32.A06(A00);
        r32.A05(A00);
        r4.A0B(r32);
    }

    @Override // X.AbstractC16410oy
    public /* bridge */ /* synthetic */ AbstractC15340mz A7L(AnonymousClass1IS r8, long j) {
        byte b;
        if (!(this instanceof AnonymousClass1XN)) {
            b = this.A0y;
        } else {
            b = 5;
        }
        return new AnonymousClass1XO(r8, this, b, j, false);
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r10) {
        if (!(this instanceof AnonymousClass1XN)) {
            return new AnonymousClass1XO(r10, this, this.A0y, this.A0I, true);
        }
        AnonymousClass1XN r4 = (AnonymousClass1XN) this;
        return new AnonymousClass1XN(r10, r4, r4.A0I);
    }
}
