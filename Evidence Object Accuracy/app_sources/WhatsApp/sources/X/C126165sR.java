package X;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5sR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C126165sR {
    public final Map A00 = new AnonymousClass00N();

    public C126165sR(Map map) {
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object key = A15.getKey();
            Object value = A15.getValue();
            AnonymousClass009.A05(key);
            for (Object obj : (Set) key) {
                this.A00.put(obj, value);
            }
        }
    }
}
