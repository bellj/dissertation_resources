package X;

import com.google.android.gms.common.api.Status;
import java.util.List;

/* renamed from: X.516  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass516 implements AbstractC117115Ym {
    public final Status A00;
    public final List A01;

    public AnonymousClass516(Status status, List list) {
        this.A00 = status;
        this.A01 = list;
    }

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        return this.A00;
    }
}
