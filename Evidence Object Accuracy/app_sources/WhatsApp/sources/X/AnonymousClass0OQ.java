package X;

import java.util.ArrayList;

/* renamed from: X.0OQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OQ {
    public C02560Cw A00;
    public AnonymousClass0O5 A01 = new AnonymousClass0O5();
    public final ArrayList A02 = new ArrayList();

    public AnonymousClass0OQ(C02560Cw r2) {
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if (r8.A01 <= 0.0f) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        if (r8.A01 <= 0.0f) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A00(X.AnonymousClass0QV r8, X.AbstractC11710gj r9, boolean r10) {
        /*
            r7 = this;
            X.0O5 r3 = r7.A01
            X.0JR[] r1 = r8.A0o
            r0 = 0
            r4 = r1[r0]
            r3.A05 = r4
            r6 = 1
            r5 = r1[r6]
            r3.A06 = r5
            int r0 = r8.A01()
            r3.A00 = r0
            int r0 = r8.A00()
            r3.A04 = r0
            r2 = 0
            r3.A08 = r2
            r3.A09 = r10
            X.0JR r1 = X.AnonymousClass0JR.MATCH_CONSTRAINT
            r0 = 0
            if (r4 != r1) goto L_0x0025
            r0 = 1
        L_0x0025:
            r4 = 0
            if (r5 != r1) goto L_0x0029
            r4 = 1
        L_0x0029:
            r1 = 0
            if (r0 == 0) goto L_0x0033
            float r0 = r8.A01
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            r5 = 1
            if (r0 > 0) goto L_0x0034
        L_0x0033:
            r5 = 0
        L_0x0034:
            if (r4 == 0) goto L_0x003d
            float r0 = r8.A01
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            r4 = 1
            if (r0 > 0) goto L_0x003e
        L_0x003d:
            r4 = 0
        L_0x003e:
            r1 = 4
            if (r5 == 0) goto L_0x004b
            int[] r0 = r8.A0l
            r0 = r0[r2]
            if (r0 != r1) goto L_0x004b
            X.0JR r0 = X.AnonymousClass0JR.FIXED
            r3.A05 = r0
        L_0x004b:
            if (r4 == 0) goto L_0x0057
            int[] r0 = r8.A0l
            r0 = r0[r6]
            if (r0 != r1) goto L_0x0057
            X.0JR r0 = X.AnonymousClass0JR.FIXED
            r3.A06 = r0
        L_0x0057:
            r9.ALU(r8, r3)
            int r0 = r3.A03
            r8.A06(r0)
            int r0 = r3.A02
            r8.A05(r0)
            boolean r0 = r3.A07
            r8.A0h = r0
            int r1 = r3.A01
            r8.A07 = r1
            r0 = 0
            if (r1 <= 0) goto L_0x0070
            r0 = 1
        L_0x0070:
            r8.A0h = r0
            r3.A09 = r2
            boolean r0 = r3.A08
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0OQ.A00(X.0QV, X.0gj, boolean):boolean");
    }
}
