package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65993Lw implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(37);
    public final String A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C65993Lw(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public C65993Lw(String str, String str2, String str3) {
        this.A00 = str;
        this.A02 = str2;
        this.A01 = str3;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("DeviceSimInfo{countryCode='");
        char A00 = C12990iw.A00(this.A00, A0k);
        A0k.append(", phoneNumber='");
        A0k.append(this.A02);
        A0k.append(A00);
        A0k.append(", networkOperatorName='");
        A0k.append(this.A01);
        A0k.append(A00);
        return C12970iu.A0v(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
    }
}
