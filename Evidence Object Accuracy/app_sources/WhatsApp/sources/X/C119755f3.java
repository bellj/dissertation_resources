package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119755f3 extends AbstractC30851Zb {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(5);
    public int A00 = -1;
    public int A01;
    public int A02 = 1;
    public AnonymousClass1ZR A03;
    public AnonymousClass1ZR A04;
    public AnonymousClass1ZR A05;
    public AnonymousClass1ZR A06;
    public AnonymousClass1ZR A07;
    public AnonymousClass1ZR A08;
    public AnonymousClass1ZR A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public ArrayList A0G;
    public boolean A0H = false;
    public boolean A0I;

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        return null;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r14, AnonymousClass1V8 r15, int i) {
        String str;
        try {
            if (i == 2) {
                str = null;
                super.A01 = AnonymousClass1ZS.A00(r15.A0I("name", null), "bankName");
                this.A0A = r15.A0I("bank-ref-id", null);
                super.A03 = r15.A0I("image", null);
                this.A0C = r15.A0I("code", null);
                super.A04 = r15.A0I("bank-phone-number", null);
                this.A0I = C117295Zj.A1T(r15, "popular-bank", null, "1");
                String A0I = r15.A0I("psp-routing", null);
                if (!TextUtils.isEmpty(A0I)) {
                    this.A0G = C12980iv.A0x(Arrays.asList(A0I.split(",")));
                }
                if (this.A00 == -1) {
                    this.A00 = C28421Nd.A00(r15.A0I("version", null), -1);
                }
            } else {
                str = null;
                this.A0A = r15.A0I("provider", null);
                this.A03 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r15.A0I("account-name", null), "accountHolderName");
                boolean z = false;
                this.A05 = C117305Zk.A0I(C117305Zk.A0J(), Boolean.class, Boolean.valueOf(C12960it.A1V(C28421Nd.A00(r15.A0I("is-mpin-set", null), 0), 1)), "isPinSet");
                this.A07 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(C28421Nd.A00(r15.A0I("otp-length", null), 0)), "otpLength");
                this.A04 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(C28421Nd.A00(r15.A0I("atm-pin-length", null), 0)), "atmPinLength");
                this.A08 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(C28421Nd.A00(r15.A0I("mpin-length", null), 0)), "pinLength");
                this.A09 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r15.A0I("vpa", null), "upiHandle");
                this.A0F = r15.A0I("vpa-id", null);
                this.A0C = r15.A0I("code", null);
                this.A01 = C28421Nd.A00(r15.A0I("pin-format-version", null), 0);
                this.A06 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r15.A0I("upi-bank-info", null), "bankInfo");
                super.A03 = r15.A0I("image", null);
                super.A04 = r15.A0I("bank-phone-number", null);
                super.A09 = null;
                super.A01 = AnonymousClass1ZS.A00(r15.A0I("bank-name", null), "bankName");
                super.A06 = r15.A0I("credential-id", null);
                super.A02 = AnonymousClass1ZS.A00(r15.A0I("account-number", null), "bankAccountNumber");
                super.A00 = C28421Nd.A01(r15.A0I("created", null), 0) * 1000;
                super.A07 = C12960it.A1V(C28421Nd.A00(r15.A0I("default-credit", null), 0), 1);
                if (C28421Nd.A00(r15.A0I("default-debit", null), 0) == 1) {
                    z = true;
                }
                super.A08 = z;
                this.A0B = r15.A0I("account-type", null);
            }
            String A0I2 = r15.A0I("transaction-prefix", str);
            if (!TextUtils.isEmpty(A0I2)) {
                this.A0E = A0I2;
            }
        } catch (Exception e) {
            Log.e("PAY: IndiaUpiMethodData fromNetwork", e);
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: IndiaUpiMethodData toNetwork is unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        Object obj;
        Object obj2;
        try {
            JSONObject A0C = A0C();
            A0C.put("v", this.A02);
            String str = this.A0A;
            if (str != null) {
                A0C.put("accountProvider", str);
            }
            if (!AnonymousClass1ZS.A02(this.A03)) {
                AnonymousClass1ZR r0 = this.A03;
                if (r0 == null) {
                    obj2 = null;
                } else {
                    obj2 = r0.A00;
                }
                A0C.put("accountHolderName", obj2);
            }
            Object obj3 = this.A07.A00;
            if (C12960it.A05(obj3) >= 0) {
                A0C.put("otpLength", obj3);
            }
            Object obj4 = this.A04.A00;
            if (C12960it.A05(obj4) >= 0) {
                A0C.put("atmPinLength", obj4);
            }
            Object obj5 = this.A08.A00;
            if (C12960it.A05(obj5) >= 0) {
                A0C.put("upiPinLength", obj5);
            }
            AnonymousClass1ZR r3 = this.A06;
            if (!AnonymousClass1ZS.A03(r3)) {
                if (r3 == null) {
                    obj = null;
                } else {
                    obj = r3.A00;
                }
                A0C.put("miscBankInfo", obj);
            }
            AnonymousClass1ZR r1 = this.A09;
            if (!AnonymousClass1ZS.A03(r1)) {
                C117315Zl.A0U(r1, "vpaHandle", A0C);
            }
            String str2 = this.A0F;
            if (str2 != null) {
                A0C.put("vpaId", str2);
            }
            String str3 = this.A0C;
            if (str3 != null) {
                A0C.put("bankCode", str3);
            }
            int i = this.A01;
            if (i >= 0) {
                A0C.put("pinFormat", i);
            }
            C117315Zl.A0U(this.A05, "isMpinSet", A0C);
            String str4 = this.A0B;
            if (str4 != null) {
                A0C.put("accountType", str4);
            }
            return A0C.toString();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiMethodData toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                A0D(A05);
                this.A02 = A05.optInt("v", 1);
                this.A0A = A05.optString("accountProvider", null);
                this.A03 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("accountHolderName", null), "accountHolderName");
                this.A07 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(A05.optInt("otpLength", -1)), "otpLength");
                this.A04 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(A05.optInt("atmPinLength", -1)), "atmPinLength");
                this.A08 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(A05.optInt("upiPinLength", -1)), "pinLength");
                this.A06 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("miscBankInfo", null), "bankInfo");
                this.A09 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("vpaHandle", null), "upiHandle");
                this.A0F = A05.optString("vpaId", null);
                this.A0C = A05.optString("bankCode", null);
                this.A01 = A05.optInt("pinFormat", 0);
                this.A05 = C117305Zk.A0I(C117305Zk.A0J(), Boolean.class, Boolean.valueOf(A05.optBoolean("isMpinSet", false)), "isPinSet");
                this.A0B = A05.optString("accountType", null);
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiMethodData fromDBString threw: ", e);
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A06() {
        Object obj;
        if (!AnonymousClass1ZS.A02(this.A03)) {
            return this.A03;
        }
        try {
            AnonymousClass1ZR r0 = this.A06;
            if (r0 == null) {
                obj = null;
            } else {
                obj = r0.A00;
            }
            return C117305Zk.A0I(C117305Zk.A0J(), String.class, C13000ix.A05((String) obj).optString("account_name"), "accountHolderName");
        } catch (JSONException unused) {
            return null;
        }
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return new LinkedHashSet(Collections.singletonList(C30771Yt.A05));
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A0E() {
        /*
            r2 = this;
            java.lang.String r1 = r2.A0B
            if (r1 == 0) goto L_0x000b
            int r0 = r1.hashCode()
            switch(r0) {
                case -1704036199: goto L_0x0026;
                case -240997565: goto L_0x001c;
                case 358786314: goto L_0x0019;
                case 1844922713: goto L_0x000f;
                default: goto L_0x000b;
            }
        L_0x000b:
            r1 = 2131887098(0x7f1203fa, float:1.9408793E38)
        L_0x000e:
            return r1
        L_0x000f:
            java.lang.String r0 = "CURRENT"
            boolean r0 = r1.equals(r0)
            r1 = 2131887095(0x7f1203f7, float:1.9408787E38)
            goto L_0x002f
        L_0x0019:
            java.lang.String r0 = "OD_UNSECURED"
            goto L_0x001e
        L_0x001c:
            java.lang.String r0 = "OD_SECURED"
        L_0x001e:
            boolean r0 = r1.equals(r0)
            r1 = 2131887096(0x7f1203f8, float:1.940879E38)
            goto L_0x002f
        L_0x0026:
            java.lang.String r0 = "SAVINGS"
            boolean r0 = r1.equals(r0)
            r1 = 2131887097(0x7f1203f9, float:1.9408791E38)
        L_0x002f:
            if (r0 != 0) goto L_0x000e
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119755f3.A0E():int");
    }

    @Override // java.lang.Object
    public String toString() {
        int i;
        StringBuilder A0k = C12960it.A0k("[ accountProvider: ");
        A0k.append(this.A0A);
        A0k.append(" issuerName: ");
        A0k.append(super.A01);
        A0k.append(" bankImageUrl: ");
        A0k.append(super.A03);
        A0k.append(" icon length: ");
        byte[] bArr = super.A09;
        if (bArr != null) {
            i = bArr.length;
        } else {
            i = 0;
        }
        A0k.append(i);
        A0k.append(" otpLength: ");
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A07);
        C1309060l.A03(A0k, A0h.toString());
        A0k.append(" upiPinLength: ");
        StringBuilder A0h2 = C12960it.A0h();
        A0h2.append(this.A08);
        C1309060l.A03(A0k, C12960it.A0d("", A0h2));
        A0k.append(" atmPinLength: ");
        StringBuilder A0h3 = C12960it.A0h();
        A0h3.append(this.A04);
        C1309060l.A03(A0k, C12960it.A0d("", A0h3));
        A0k.append(" vpaHandle: ");
        A0k.append(this.A09);
        A0k.append(" vpaId: ");
        A0k.append(this.A0F);
        A0k.append(" bankPhoneNumber: ");
        A0k.append(super.A04);
        A0k.append(" bankCode: ");
        A0k.append(this.A0C);
        A0k.append(" pinFormat: ");
        StringBuilder A0h4 = C12960it.A0h();
        A0h4.append(this.A01);
        C1309060l.A03(A0k, C12960it.A0d("", A0h4));
        A0k.append(" pspRouting: ");
        A0k.append(this.A0G);
        A0k.append(" supportPhoneNumber: ");
        A0k.append(this.A0D);
        A0k.append(" transactionPrefix: ");
        A0k.append(this.A0E);
        A0k.append(" banksListVersion: ");
        A0k.append(this.A00);
        return C12960it.A0d(" ]", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int length;
        parcel.writeString(this.A0A);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A07, i);
        parcel.writeParcelable(this.A04, i);
        parcel.writeParcelable(this.A08, i);
        parcel.writeInt(this.A0H ? 1 : 0);
        parcel.writeParcelable(this.A06, i);
        parcel.writeString(super.A03);
        parcel.writeString(super.A04);
        parcel.writeParcelable(this.A09, i);
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0C);
        parcel.writeInt(this.A01);
        parcel.writeString(this.A0D);
        parcel.writeString(this.A0E);
        parcel.writeStringList(this.A0G);
        byte[] bArr = super.A09;
        if (bArr == null) {
            length = 0;
        } else {
            length = bArr.length;
        }
        parcel.writeInt(length);
        byte[] bArr2 = super.A09;
        if (bArr2 != null) {
            parcel.writeByteArray(bArr2);
        }
        parcel.writeString(super.A06);
        parcel.writeParcelable(super.A01, i);
        parcel.writeParcelable(super.A02, i);
        parcel.writeLong(super.A00);
        parcel.writeInt(super.A07 ? 1 : 0);
        parcel.writeInt(super.A08 ? 1 : 0);
        parcel.writeString(this.A0B);
        parcel.writeInt(this.A0I ? 1 : 0);
    }
}
