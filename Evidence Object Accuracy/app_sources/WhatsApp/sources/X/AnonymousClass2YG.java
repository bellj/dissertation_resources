package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2YG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YG extends AnimatorListenerAdapter {
    public final /* synthetic */ C55122hp A00;

    public AnonymousClass2YG(C55122hp r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        int i;
        C55122hp r2 = this.A00;
        r2.A02 = null;
        boolean z = r2.A03;
        WaImageView waImageView = r2.A09;
        if (z) {
            waImageView.setAlpha(1.0f);
        } else {
            waImageView.setAlpha(0.0f);
            waImageView.setVisibility(8);
        }
        WaTextView waTextView = r2.A0A;
        if (r2.A03) {
            i = r2.A04;
        } else {
            i = r2.A05;
        }
        waTextView.setTextColor(i);
    }
}
