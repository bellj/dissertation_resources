package X;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.2gS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54272gS extends AnonymousClass02M {
    public final Activity A00;
    public final AnonymousClass0QI A01;
    public final C15570nT A02;
    public final AnonymousClass4KH A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass1J1 A06;
    public final C19990v2 A07;
    public final C15600nX A08;
    public final AnonymousClass11F A09;
    public final AnonymousClass12F A0A;
    public final boolean A0B;

    public C54272gS(Activity activity, C15570nT r5, AnonymousClass4KH r6, C15550nR r7, C15610nY r8, AnonymousClass1J1 r9, C19990v2 r10, C15600nX r11, AnonymousClass11F r12, AnonymousClass12F r13, boolean z) {
        this.A02 = r5;
        this.A07 = r10;
        this.A04 = r7;
        this.A05 = r8;
        this.A0A = r13;
        this.A09 = r12;
        this.A06 = r9;
        this.A08 = r11;
        this.A00 = activity;
        this.A0B = z;
        this.A01 = new AnonymousClass0QI(new C55302iA(this, r10), AnonymousClass1OU.class);
        this.A03 = r6;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.A03;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r10, int i) {
        CharSequence string;
        int i2;
        C55092hm r102 = (C55092hm) r10;
        AnonymousClass1OU r3 = (AnonymousClass1OU) this.A01.A02(i);
        AnonymousClass1J1 r8 = this.A06;
        AnonymousClass4KH r2 = this.A03;
        C15550nR r0 = r102.A05;
        GroupJid groupJid = r3.A02;
        C15370n3 A0B = r0.A0B(groupJid);
        C19990v2 r4 = r102.A07;
        int A02 = r4.A02(groupJid);
        ThumbnailButton thumbnailButton = r102.A04;
        if (A02 == 3) {
            thumbnailButton.setImageResource(R.drawable.avatar_announcement);
        } else {
            r8.A06(thumbnailButton, A0B);
        }
        r102.A02.A08(r3.A03);
        if (r4.A02(groupJid) == 3) {
            string = r102.A01.getResources().getText(R.string.communities_announcement_group_subtitle);
        } else if (r102.A08.A02(groupJid).A0H(r102.A00)) {
            string = r102.A06.A0E(groupJid, 1, true);
        } else {
            string = r102.A01.getResources().getString(R.string.community_you_are_not_a_member_of_this_group);
        }
        r102.A01.A0G(null, string);
        int A022 = r4.A02(groupJid);
        WaImageButton waImageButton = r102.A03;
        if (A022 != 3) {
            C12960it.A13(waImageButton, r2, r3, 29);
            i2 = 0;
        } else {
            waImageButton.setOnClickListener(null);
            i2 = 4;
        }
        waImageButton.setVisibility(i2);
        if (!this.A0B) {
            waImageButton.setOnClickListener(null);
            waImageButton.setVisibility(4);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AnonymousClass11F r7 = this.A09;
        C15570nT r2 = this.A02;
        C19990v2 r5 = this.A07;
        C15550nR r3 = this.A04;
        C15610nY r4 = this.A05;
        AnonymousClass12F r8 = this.A0A;
        return new C55092hm(C12960it.A0F(LayoutInflater.from(this.A00), viewGroup, R.layout.add_groups_to_parent_group_row_item), r2, r3, r4, r5, this.A08, r7, r8);
    }
}
