package X;

/* renamed from: X.5fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120105fd extends AnonymousClass4UZ {
    public final /* synthetic */ AbstractActivityC121525iS A00;

    public C120105fd(AbstractActivityC121525iS r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass4UZ
    public void A00() {
        AbstractActivityC121525iS r2 = this.A00;
        C124235op r1 = r2.A0U;
        if (r1 != null) {
            r1.A03(true);
            r2.A0U = null;
        }
        if (AbstractActivityC119235dO.A1l(r2)) {
            C124235op r12 = new C124235op(r2);
            r2.A0U = r12;
            C12960it.A1E(r12, ((ActivityC13830kP) r2).A05);
        }
    }
}
