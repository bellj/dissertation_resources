package X;

import androidx.work.ListenableWorker;
import com.whatsapp.data.ConversationDeleteWorker;

/* renamed from: X.56E  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass56E implements AbstractC38351nw {
    public final /* synthetic */ ConversationDeleteWorker A00;
    public final /* synthetic */ C27441Hl A01;

    public AnonymousClass56E(ConversationDeleteWorker conversationDeleteWorker, C27441Hl r2) {
        this.A00 = conversationDeleteWorker;
        this.A01 = r2;
    }

    @Override // X.AbstractC38351nw
    public void AQZ() {
        ConversationDeleteWorker.A0D.addAndGet(-1);
        this.A00.A05();
    }

    @Override // X.AbstractC38351nw
    public void AUM(int i, int i2) {
        this.A00.A06(this.A01.A07, i);
    }

    @Override // X.AbstractC38351nw
    public void AWF() {
        ConversationDeleteWorker.A0D.addAndGet(1);
    }

    @Override // X.AbstractC38361nx
    public boolean Ada() {
        return ((ListenableWorker) this.A00).A04;
    }
}
