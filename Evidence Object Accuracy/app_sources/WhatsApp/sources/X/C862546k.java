package X;

/* renamed from: X.46k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C862546k extends AbstractC35941j2 {
    public final /* synthetic */ AbstractC34031fT A00;
    public final /* synthetic */ AbstractC32491cF A01;
    public final /* synthetic */ AnonymousClass2L8 A02;

    public C862546k(AbstractC34031fT r1, AbstractC32491cF r2, AnonymousClass2L8 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        AbstractC32491cF r0 = this.A01;
        if (r0 != null) {
            r0.Aaw(i);
        }
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r1) {
    }

    @Override // X.AbstractC35941j2
    public void A03(Exception exc) {
        AbstractC34031fT r0 = this.A00;
        if (r0 != null) {
            r0.Ab0(exc);
        }
    }
}
