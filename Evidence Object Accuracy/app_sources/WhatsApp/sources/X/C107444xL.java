package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4xL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107444xL implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(25);
    public final float A00;
    public final int A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107444xL(float f, int i) {
        this.A00 = f;
        this.A01 = i;
    }

    public /* synthetic */ C107444xL(Parcel parcel) {
        this.A00 = parcel.readFloat();
        this.A01 = parcel.readInt();
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107444xL.class != obj.getClass()) {
                return false;
            }
            C107444xL r5 = (C107444xL) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A05(Float.valueOf(this.A00).hashCode()) + this.A01;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("smta: captureFrameRate=");
        A0k.append(this.A00);
        A0k.append(", svcTemporalLayerCount=");
        return C12960it.A0f(A0k, this.A01);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.A00);
        parcel.writeInt(this.A01);
    }
}
