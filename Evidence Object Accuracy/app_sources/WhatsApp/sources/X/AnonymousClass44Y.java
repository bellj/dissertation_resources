package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.Set;

/* renamed from: X.44Y  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44Y extends AbstractC33331dp {
    public final /* synthetic */ ContactPickerFragment A00;

    public AnonymousClass44Y(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        ContactPickerFragment contactPickerFragment = this.A00;
        if (!ContactPickerFragment.A2d) {
            contactPickerFragment.A1L();
        }
    }
}
