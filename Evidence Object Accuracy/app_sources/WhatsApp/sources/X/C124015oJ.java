package X;

import com.whatsapp.payments.ui.IndiaUpiChangePinActivity;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5oJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124015oJ extends AbstractC16350or {
    public final /* synthetic */ IndiaUpiChangePinActivity A00;

    public /* synthetic */ C124015oJ(IndiaUpiChangePinActivity indiaUpiChangePinActivity) {
        this.A00 = indiaUpiChangePinActivity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return C117295Zj.A0Z(((AbstractActivityC121685jC) this.A00).A0P);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AbstractC28901Pl r1;
        List list = (List) obj;
        if (list != null && list.size() == 1) {
            IndiaUpiChangePinActivity indiaUpiChangePinActivity = this.A00;
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r1 = null;
                    break;
                }
                r1 = C117305Zk.A0H(it);
                if (r1.A04() == 2) {
                    break;
                }
            }
            indiaUpiChangePinActivity.A02 = (C30861Zc) r1;
        }
        IndiaUpiChangePinActivity indiaUpiChangePinActivity2 = this.A00;
        ((AbstractActivityC121545iU) indiaUpiChangePinActivity2).A06.A02("pin-entry-ui");
        if (indiaUpiChangePinActivity2.A02 != null) {
            ((AbstractActivityC121545iU) indiaUpiChangePinActivity2).A09.A00();
            return;
        }
        indiaUpiChangePinActivity2.A05.A06("could not find bank account; showErrorAndFinish");
        indiaUpiChangePinActivity2.A39();
    }
}
