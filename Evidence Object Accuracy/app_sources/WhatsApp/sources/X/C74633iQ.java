package X;

/* renamed from: X.3iQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74633iQ extends AnonymousClass02K {
    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        return ((AnonymousClass4QG) obj).A02.A00.equals(((AnonymousClass4QG) obj2).A02.A00);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        return ((AnonymousClass4QG) obj).A02.equals(((AnonymousClass4QG) obj2).A02);
    }
}
