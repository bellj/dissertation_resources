package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.36s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C623336s extends AbstractC16350or {
    public final Set A00;
    public final /* synthetic */ AbstractActivityC36551k4 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C623336s(AbstractActivityC36551k4 r2, Set set) {
        super(r2);
        this.A01 = r2;
        HashSet A12 = C12970iu.A12();
        this.A00 = A12;
        A12.addAll(set);
    }
}
