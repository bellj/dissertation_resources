package X;

import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.R;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.2gf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54402gf extends AnonymousClass02M {
    public List A00 = C12960it.A0l();
    public List A01 = C12960it.A0l();
    public List A02;
    public List A03 = C12960it.A0l();
    public Map A04 = Collections.emptyMap();
    public boolean A05 = false;
    public boolean A06 = false;
    public final AnonymousClass12P A07;
    public final C14900mE A08;
    public final AnonymousClass3CN A09;
    public final C233711k A0A;
    public final AnonymousClass01d A0B;
    public final C14830m7 A0C;
    public final AnonymousClass018 A0D;
    public final C22100yW A0E;
    public final C22130yZ A0F;
    public final C14850m9 A0G;
    public final C14840m8 A0H;
    public final C252018m A0I;
    public final C14860mA A0J;

    public C54402gf(AnonymousClass12P r2, C14900mE r3, AnonymousClass3CN r4, C233711k r5, AnonymousClass01d r6, C14830m7 r7, AnonymousClass018 r8, C22100yW r9, C22130yZ r10, C14850m9 r11, C14840m8 r12, C252018m r13, C14860mA r14) {
        this.A09 = r4;
        this.A0C = r7;
        this.A0G = r11;
        this.A08 = r3;
        this.A0J = r14;
        this.A07 = r2;
        this.A0I = r13;
        this.A0B = r6;
        this.A0D = r8;
        this.A0H = r12;
        this.A0F = r10;
        this.A0E = r9;
        this.A0A = r5;
        A0E();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0058, code lost:
        if (r4.A03() == false) goto L_0x005a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x013d A[LOOP:2: B:60:0x0137->B:62:0x013d, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0E() {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54402gf.A0E():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0034, code lost:
        if (r2.A0N() == false) goto L_0x0036;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0124  */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r11, int r12) {
        /*
        // Method dump skipped, instructions count: 573
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54402gf.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (i) {
            case 1:
                C14900mE r5 = this.A08;
                AnonymousClass12P r4 = this.A07;
                C252018m r7 = this.A0I;
                return new C54872hQ(new ViewOnClickCListenerShape8S0100000_I1_2(this, 19), C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.adv_warning_banner_layout), r4, r5, this.A0B, r7, "seeing-devices-out-of-sync", R.string.device_linking_adv_warning_message);
            case 2:
                C14900mE r52 = this.A08;
                AnonymousClass12P r42 = this.A07;
                C252018m r72 = this.A0I;
                return new C54872hQ(new ViewOnClickCListenerShape8S0100000_I1_2(this, 18), C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.adv_warning_banner_layout), r42, r52, this.A0B, r72, "seeing-devices-logged-out-unexpected-issue", R.string.device_linking_fatal_error_warning_message);
            case 3:
                return new C55022hf(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.pre_md_linked_devices_header_layout), this.A09);
            case 4:
                return new C54932hW(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.linked_devices_header_layout), this.A09);
            case 5:
                return new C75153jO(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.device_section_layout));
            case 6:
                C14830m7 r43 = this.A0C;
                AnonymousClass018 r3 = this.A0D;
                return new C55042hh(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.linked_device_list_item_layout), this.A09, r43, r3);
            case 7:
                C14830m7 r32 = this.A0C;
                C14860mA r53 = this.A0J;
                AnonymousClass018 r44 = this.A0D;
                return new C851941p(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.linked_device_list_item_layout), this.A09, r32, r44, r53);
            case 8:
                C14830m7 r45 = this.A0C;
                AnonymousClass018 r33 = this.A0D;
                return new C851841o(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.linked_device_list_item_layout), this.A09, r45, r33);
            case 9:
                C14900mE r34 = this.A08;
                return new C54882hR(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.no_devices_place_holder_layout), this.A07, r34, this.A0B, this.A0I);
            default:
                throw C12980iv.A0u(C12960it.A0W(i, "Invalid viewType: "));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4NL) this.A02.get(i)).A00;
    }
}
