package X;

/* renamed from: X.50I  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50I implements AbstractC115645Sj {
    @Override // X.AbstractC115645Sj
    public final byte[] Agu(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
