package X;

/* renamed from: X.0FN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FN extends AbstractC02880Ff {
    public final /* synthetic */ C07700Zw A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FN(AnonymousClass0QN r1, C07700Zw r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC02880Ff
    public void A03(AbstractC12830ic r4, Object obj) {
        AnonymousClass0PB r5 = (AnonymousClass0PB) obj;
        String str = r5.A01;
        if (str == null) {
            r4.A6T(1);
        } else {
            r4.A6U(1, str);
        }
        Long l = r5.A00;
        if (l == null) {
            r4.A6T(2);
        } else {
            r4.A6S(2, l.longValue());
        }
    }
}
