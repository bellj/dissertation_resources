package X;

import android.text.TextUtils;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.2Kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49342Kj extends AbstractC16350or {
    public AnonymousClass1R5 A00;
    public final int A01;
    public final int A02;
    public final C14900mE A03;
    public final C14820m6 A04;
    public final AnonymousClass018 A05;
    public final C20800wL A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final WeakReference A0B;
    public final boolean A0C;

    public C49342Kj(C14900mE r4, C14820m6 r5, AnonymousClass018 r6, C20800wL r7, VerifyTwoFactorAuth verifyTwoFactorAuth, String str, String str2, String str3, String str4, int i, boolean z) {
        super(verifyTwoFactorAuth);
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
        this.A06 = r7;
        this.A09 = str;
        this.A08 = str2;
        this.A07 = str3;
        this.A0A = str4;
        this.A0C = z;
        this.A02 = i;
        this.A0B = new WeakReference(verifyTwoFactorAuth);
        int i2 = 33;
        if (i != 1) {
            i2 = 34;
            if (i != 2) {
                i2 = 31;
            }
        }
        this.A01 = i2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C64323Fc r1;
        int i;
        int i2;
        AnonymousClass1R4 r9 = (AnonymousClass1R4) obj;
        VerifyTwoFactorAuth verifyTwoFactorAuth = (VerifyTwoFactorAuth) this.A0B.get();
        if (verifyTwoFactorAuth != null) {
            verifyTwoFactorAuth.A0M = null;
            C36021jC.A00(verifyTwoFactorAuth, this.A01);
            verifyTwoFactorAuth.A08.setEnabled(true);
            verifyTwoFactorAuth.A05.setProgress(100);
            verifyTwoFactorAuth.A0X = false;
            C18640sm r0 = ((ActivityC13810kN) verifyTwoFactorAuth).A07;
            AbstractC20260vT r5 = verifyTwoFactorAuth.A0Z;
            r0.A04(r5);
            switch (r9.ordinal()) {
                case 0:
                    int i3 = this.A02;
                    if (i3 == 1) {
                        Log.i("verifytwofactorauth/verifycodetask/reset-email-sent");
                        verifyTwoFactorAuth.Ado(R.string.two_factor_auth_reset_successful);
                        verifyTwoFactorAuth.A2M("forgotPinDialogTag");
                        verifyTwoFactorAuth.A2h(this.A00);
                        verifyTwoFactorAuth.A2j(false);
                        verifyTwoFactorAuth.A0Y.postDelayed(verifyTwoFactorAuth.A0a, 0);
                        return;
                    } else if (!TextUtils.isEmpty(this.A00.A06)) {
                        Log.i("verifytwofactorauth/verifycodetask/verified");
                        verifyTwoFactorAuth.A2j(true);
                        this.A04.A12(this.A00.A09);
                        if (i3 == 0) {
                            Log.i("RegistrationUtils/showVerificationCompleteDialog");
                            AnonymousClass04S r12 = null;
                            if (!AnonymousClass12P.A00(verifyTwoFactorAuth).isFinishing()) {
                                View inflate = View.inflate(verifyTwoFactorAuth, R.layout.dialog_verification_complete, null);
                                C004802e r02 = new C004802e(verifyTwoFactorAuth);
                                r02.setView(inflate);
                                r12 = r02.A05();
                            }
                            verifyTwoFactorAuth.A07 = r12;
                        }
                        RunnableBRunnable0Shape7S0200000_I0_7 runnableBRunnable0Shape7S0200000_I0_7 = new RunnableBRunnable0Shape7S0200000_I0_7(this, 35, verifyTwoFactorAuth);
                        if (verifyTwoFactorAuth.A07 != null) {
                            this.A03.A0J(runnableBRunnable0Shape7S0200000_I0_7, 1000);
                            return;
                        } else {
                            runnableBRunnable0Shape7S0200000_I0_7.run();
                            return;
                        }
                    } else {
                        return;
                    }
                case 1:
                    Log.w("verifytwofactorauth/verifycodetask/unspecified");
                    if (!super.A02.isCancelled()) {
                        verifyTwoFactorAuth.A0X = true;
                        try {
                            ((ActivityC13810kN) verifyTwoFactorAuth).A07.A03(r5);
                        } catch (IllegalStateException unused) {
                        }
                    }
                    i2 = 109;
                    C36021jC.A01(verifyTwoFactorAuth, i2);
                    return;
                case 2:
                case 9:
                    Log.w("verifytwofactorauth/verifycodetask/connectivity");
                    if (verifyTwoFactorAuth.A0G.A02 || verifyTwoFactorAuth.AJN()) {
                        AnonymousClass23M.A0G(verifyTwoFactorAuth, verifyTwoFactorAuth.A0C, 32);
                        return;
                    } else {
                        C36021jC.A01(verifyTwoFactorAuth, 32);
                        return;
                    }
                case 3:
                    Log.w("verifytwofactorauth/verifycodetask/incorrect");
                    verifyTwoFactorAuth.A2j(true);
                    r1 = verifyTwoFactorAuth.A0G;
                    i = R.string.register_verify_again;
                    r1.A02(i);
                    return;
                case 4:
                    boolean equals = this.A07.equals(this.A04.A00.getString("registration_code", null));
                    StringBuilder sb = new StringBuilder("verifytwofactorauth/verifycodetask/mismatch ");
                    sb.append(equals);
                    Log.i(sb.toString());
                    verifyTwoFactorAuth.A08.setText("");
                    C64323Fc r13 = verifyTwoFactorAuth.A0G;
                    int i4 = R.string.two_factor_auth_wrong_code_message;
                    if (equals) {
                        i4 = R.string.two_factor_auth_accidental_sms_error;
                    }
                    r13.A02(i4);
                    try {
                        verifyTwoFactorAuth.A2g(Long.parseLong(this.A00.A05) * 1000);
                        return;
                    } catch (NumberFormatException e) {
                        StringBuilder sb2 = new StringBuilder("verifytwofactorauth/verifycodetask/mismatch failed to parse: ");
                        sb2.append(this.A00.A05);
                        Log.w(sb2.toString(), e);
                        return;
                    }
                case 5:
                    Log.w("verifytwofactorauth/verifycodetask/too-many-guesses");
                    verifyTwoFactorAuth.A2j(true);
                    r1 = verifyTwoFactorAuth.A0G;
                    i = R.string.two_factor_auth_too_many_tries;
                    r1.A02(i);
                    return;
                case 6:
                    Log.w("verifytwofactorauth/verifycodetask/guessed-too-fast");
                    try {
                        long parseLong = Long.parseLong(this.A00.A05) * 1000;
                        verifyTwoFactorAuth.A0G.A03(verifyTwoFactorAuth.getString(R.string.register_guessed_too_fast_with_time, C38131nZ.A08(this.A05, parseLong)));
                        verifyTwoFactorAuth.A2g(parseLong);
                        return;
                    } catch (NumberFormatException e2) {
                        StringBuilder sb3 = new StringBuilder("verifytwofactorauth/verifycodetask/too_fast failed to parse: ");
                        sb3.append(this.A00.A05);
                        Log.w(sb3.toString(), e2);
                        verifyTwoFactorAuth.A0G.A02(R.string.two_factor_auth_too_many_tries);
                        return;
                    }
                case 7:
                    Log.w("verifytwofactorauth/verifycodetask/reset-too-soon");
                    r1 = verifyTwoFactorAuth.A0G;
                    i = R.string.two_factor_auth_reset_too_soon_message;
                    r1.A02(i);
                    return;
                case 8:
                    Log.w("verifytwofactorauth/verifycodetask/stale");
                    int A2e = verifyTwoFactorAuth.A2e();
                    verifyTwoFactorAuth.A2h(this.A00);
                    int A2e2 = verifyTwoFactorAuth.A2e();
                    if (this.A0C || A2e != A2e2) {
                        verifyTwoFactorAuth.A2j(true);
                        r1 = verifyTwoFactorAuth.A0G;
                        i = R.string.register_stale;
                        r1.A02(i);
                        return;
                    }
                    verifyTwoFactorAuth.A2f(this.A02, this.A07, true);
                    return;
                case 10:
                    Log.w("verifytwofactorauth/verifycodetask/blocked");
                    verifyTwoFactorAuth.A2j(true);
                    i2 = 124;
                    C36021jC.A01(verifyTwoFactorAuth, i2);
                    return;
                default:
                    return;
            }
        }
    }
}
