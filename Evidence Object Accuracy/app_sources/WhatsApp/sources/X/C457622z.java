package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;

/* renamed from: X.22z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C457622z implements AbstractC21730xt {
    public final int A00;
    public final int A01;
    public final /* synthetic */ C20790wK A02;

    public C457622z(C20790wK r1, int i, int i2) {
        this.A02 = r1;
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C20790wK r3 = this.A02;
        r3.A01.A0I(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 21));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        C20790wK r3 = this.A02;
        r3.A01.A0I(new RunnableBRunnable0Shape6S0100000_I0_6(r3, 21));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        this.A02.A01.A0I(new RunnableBRunnable0Shape6S0100000_I0_6(this, 20));
    }
}
