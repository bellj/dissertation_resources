package X;

import android.content.Intent;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;

/* renamed from: X.5l8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122235l8 extends AbstractC1308360d {
    public final C1329568x A00;
    public final C17070qD A01;
    public final AbstractC14440lR A02;

    public C122235l8(C14900mE r20, C15570nT r21, ActivityC13790kL r22, C18640sm r23, C14830m7 r24, C18050rp r25, C1329568x r26, C21860y6 r27, C20400vh r28, C18650sn r29, C18600si r30, C18610sj r31, AnonymousClass61M r32, C17070qD r33, C129135xE r34, AnonymousClass60T r35, AnonymousClass61E r36, C130015yf r37, AnonymousClass6M6 r38, AbstractC14440lR r39) {
        super(r20, r21, r22, r23, r24, r25, r27, r28, r29, r30, r31, r32, r34, r35, r36, r37, r38);
        this.A02 = r39;
        this.A01 = r33;
        this.A00 = r26;
    }

    @Override // X.AbstractC1308360d
    public void A04(ActivityC13790kL r5, String str) {
        C32631cT r0;
        AbstractC460224d r02;
        if (str != null) {
            super.A02.add(str);
        }
        super.A04(r5, str);
        C460124c A01 = this.A08.A01(str);
        if (A01 != null && (r0 = A01.A03) != null && (r02 = r0.A00) != null && r02.A00.equals("DOC_UPLOAD")) {
            ActivityC13790kL r3 = this.A05;
            Intent A0D = C12990iw.A0D(r3, BrazilPayBloksActivity.class);
            A0D.putExtra("screen_name", "brpay_p_doc_upload_intro");
            r3.startActivityForResult(A0D, super.A00);
        }
    }
}
