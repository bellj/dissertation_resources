package X;

import android.app.Dialog;
import android.content.DialogInterface;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.0V3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V3 implements DialogInterface.OnCancelListener {
    public final /* synthetic */ DialogFragment A00;

    public AnonymousClass0V3(DialogFragment dialogFragment) {
        this.A00 = dialogFragment;
    }

    @Override // android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        DialogFragment dialogFragment = this.A00;
        Dialog dialog = dialogFragment.A03;
        if (dialog != null) {
            dialogFragment.onCancel(dialog);
        }
    }
}
