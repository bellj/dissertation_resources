package X;

import java.util.Arrays;

/* renamed from: X.1xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43831xf {
    public final int A00;
    public final int A01;
    public final int A02;
    public final long A03;

    public C43831xf(int i, int i2, int i3, long j) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = j;
        this.A02 = i3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C43831xf)) {
            return false;
        }
        C43831xf r7 = (C43831xf) obj;
        if (this.A00 == r7.A00 && this.A01 == r7.A01 && this.A03 == r7.A03 && this.A02 == r7.A02) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), Integer.valueOf(this.A01), Long.valueOf(this.A03), Integer.valueOf(this.A02)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UserNoticeMetadata{noticeId=");
        sb.append(this.A00);
        sb.append(", stage=");
        sb.append(this.A01);
        sb.append(", timestamp=");
        sb.append(this.A03);
        sb.append(", version=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
