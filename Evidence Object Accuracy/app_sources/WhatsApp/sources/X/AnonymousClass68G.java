package X;

import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.util.Log;

/* renamed from: X.68G  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68G implements AnonymousClass2DH {
    public final /* synthetic */ NoviPayBloksActivity A00;

    public AnonymousClass68G(NoviPayBloksActivity noviPayBloksActivity) {
        this.A00 = noviPayBloksActivity;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        Log.e("PAY: NoviPayBloksActivity/Payment Currency Metadata Fetch Failed");
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        Log.e("PAY: NoviPayBloksActivity/Payment Currency Metadata Fetch Failed");
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        ((ActivityC13810kN) this.A00).A09.A0k("payment_currency_metadata_last_sync_timestamp");
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        Log.e("PAY: NoviPayBloksActivity/Payment Currency Metadata Fetch Timed Out");
    }
}
