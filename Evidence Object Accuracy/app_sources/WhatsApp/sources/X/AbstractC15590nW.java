package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;

/* renamed from: X.0nW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15590nW extends AbstractC14640lm implements Parcelable {
    public AbstractC15590nW(Parcel parcel) {
        super(parcel);
    }

    public AbstractC15590nW(String str) {
        super(str);
    }

    public static AbstractC15590nW A05(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Jid jid = Jid.get(str);
            if (jid instanceof AbstractC15590nW) {
                return (AbstractC15590nW) jid;
            }
            throw new AnonymousClass1MW(str);
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }
}
