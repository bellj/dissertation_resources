package X;

import android.view.ViewTreeObserver;

/* renamed from: X.2AT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AT implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass2AS A00;
    public final /* synthetic */ Runnable A01;

    public AnonymousClass2AT(AnonymousClass2AS r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass2AS r4 = this.A00;
        r4.getViewTreeObserver().removeOnPreDrawListener(this);
        r4.A0E = true;
        int height = r4.getHeight();
        r4.getLayoutParams().height = 0;
        r4.requestLayout();
        C73923h1 r2 = new C73923h1(this, height);
        r2.setAnimationListener(new C58122oB(this));
        r2.setDuration(250);
        r4.startAnimation(r2);
        return false;
    }
}
