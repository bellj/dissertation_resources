package X;

import java.util.List;

/* renamed from: X.0Gv  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gv extends AnonymousClass0H3 {
    public AnonymousClass0Gv(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public Object A04(AnonymousClass0U8 r2, float f) {
        Object obj;
        if (f != 1.0f || (obj = r2.A09) == null) {
            return r2.A0F;
        }
        return obj;
    }
}
