package X;

import android.os.Bundle;

/* renamed from: X.3pS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC78733pS extends AnonymousClass4VW {
    public final int A00;
    public final Bundle A01;
    public final /* synthetic */ AbstractC95064d1 A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AbstractC78733pS(Bundle bundle, AbstractC95064d1 r3, int i) {
        super(r3, Boolean.TRUE);
        this.A02 = r3;
        this.A00 = i;
        this.A01 = bundle;
    }
}
