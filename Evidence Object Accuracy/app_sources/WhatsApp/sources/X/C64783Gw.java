package X;

import java.util.HashSet;
import java.util.Map;

/* renamed from: X.3Gw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64783Gw {
    public static final int[][] A01 = {new int[]{129324}, new int[]{128545}, new int[]{128544}, new int[]{128548}, new int[]{128127}, new int[]{128580}, new int[]{128530}, new int[]{128574}, new int[]{128162}, new int[]{128495}, new int[]{128122}};
    public static final int[][] A02;
    public static final int[][] A03 = {new int[]{128075}};
    public static final int[][] A04;
    public static final int[][] A05 = {new int[]{128105, 8205, 10084, 65039, 8205, 128139, 8205, 128104}, new int[]{128104, 8205, 10084, 65039, 8205, 128139, 8205, 128104}, new int[]{128105, 8205, 10084, 65039, 8205, 128139, 8205, 128105}, new int[]{128105, 8205, 10084, 65039, 8205, 128104}, new int[]{128104, 8205, 10084, 65039, 8205, 128104}, new int[]{128105, 8205, 10084, 65039, 8205, 128105}, new int[]{128109}, new int[]{128107}, new int[]{128108}, new int[]{128525}, new int[]{129392}, new int[]{128536}, new int[]{128538}, new int[]{128537}, new int[]{10084}, new int[]{128139}, new int[]{10083}, new int[]{128149}, new int[]{128158}, new int[]{128151}, new int[]{128147}, new int[]{128152}, new int[]{128157}, new int[]{128150}, new int[]{128068}, new int[]{127801}, new int[]{128159}, new int[]{128571}, new int[]{128573}};
    public static final int[][] A06;
    public final Map A00 = C12970iu.A11();

    static {
        int[][] iArr = new int[14];
        iArr[4] = C12960it.A1Y(iArr);
        int[] iArr2 = new int[1];
        iArr2[0] = 129315;
        iArr[5] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128514;
        iArr[6] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128578;
        iArr[7] = iArr4;
        int[] iArr5 = new int[1];
        iArr5[0] = 128521;
        iArr[8] = iArr5;
        int[] iArr6 = new int[1];
        iArr6[0] = 128522;
        iArr[9] = iArr6;
        int[] iArr7 = new int[1];
        iArr7[0] = 129303;
        iArr[10] = iArr7;
        int[] iArr8 = new int[1];
        iArr8[0] = 128524;
        iArr[11] = iArr8;
        int[] iArr9 = new int[1];
        iArr9[0] = 128570;
        iArr[12] = iArr9;
        int[] iArr10 = new int[1];
        iArr10[0] = 9786;
        iArr[13] = iArr10;
        A04 = iArr;
        int[][] iArr11 = new int[15];
        int[] iArr12 = new int[1];
        iArr12[0] = 128148;
        iArr11[0] = iArr12;
        int[] iArr13 = new int[1];
        iArr13[0] = 128546;
        iArr11[1] = iArr13;
        int[] A1Z = C12980iv.A1Z(iArr11, 2, 3);
        A1Z[0] = 128533;
        iArr11[4] = A1Z;
        int[] iArr14 = new int[1];
        iArr14[0] = 128577;
        iArr11[5] = iArr14;
        int[] iArr15 = new int[1];
        iArr15[0] = 9785;
        iArr11[6] = iArr15;
        int[] iArr16 = new int[1];
        iArr16[0] = 128532;
        iArr11[7] = iArr16;
        int[] iArr17 = new int[1];
        iArr17[0] = 129301;
        iArr11[8] = iArr17;
        int[] iArr18 = new int[1];
        iArr18[0] = 129402;
        iArr11[9] = iArr18;
        int[] iArr19 = new int[1];
        iArr19[0] = 128575;
        iArr11[10] = iArr19;
        int[] iArr20 = new int[1];
        iArr20[0] = 9748;
        iArr11[11] = iArr20;
        int[] iArr21 = new int[1];
        iArr21[0] = 9928;
        iArr11[12] = iArr21;
        int[] iArr22 = new int[1];
        iArr22[0] = 127783;
        iArr11[13] = iArr22;
        int[] iArr23 = new int[1];
        iArr23[0] = 127785;
        iArr11[14] = iArr23;
        A06 = iArr11;
        int[][] iArr24 = new int[20];
        iArr24[4] = C12960it.A1Z(iArr24);
        iArr24[5] = new int[]{128111, 8205, 9794, 65039};
        iArr24[6] = new int[]{128111, 8205, 9792, 65039};
        int[] iArr25 = new int[1];
        iArr25[0] = 128131;
        iArr24[7] = iArr25;
        int[] iArr26 = new int[1];
        iArr26[0] = 128378;
        iArr24[8] = iArr26;
        int[] iArr27 = new int[1];
        iArr27[0] = 128293;
        iArr24[9] = iArr27;
        int[] iArr28 = new int[1];
        iArr28[0] = 11088;
        iArr24[10] = iArr28;
        int[] iArr29 = new int[1];
        iArr29[0] = 10024;
        iArr24[11] = iArr29;
        int[] iArr30 = new int[1];
        iArr30[0] = 128171;
        iArr24[12] = iArr30;
        int[] iArr31 = new int[1];
        iArr31[0] = 127879;
        iArr24[13] = iArr31;
        int[] iArr32 = new int[1];
        iArr32[0] = 127878;
        iArr24[14] = iArr32;
        int[] iArr33 = new int[1];
        iArr33[0] = 127867;
        iArr24[15] = iArr33;
        int[] iArr34 = new int[1];
        iArr34[0] = 129346;
        iArr24[16] = iArr34;
        int[] iArr35 = new int[1];
        iArr35[0] = 127870;
        iArr24[17] = iArr35;
        int[] iArr36 = new int[1];
        iArr36[0] = 127874;
        iArr24[18] = iArr36;
        int[] iArr37 = new int[1];
        iArr37[0] = 127856;
        iArr24[19] = iArr37;
        A02 = iArr24;
    }

    public C64783Gw() {
        A00(A05, 1);
        A00(A03, 2);
        A00(A04, 3);
        A00(A06, 4);
        A00(A01, 5);
        A00(A02, 6);
    }

    public final void A00(int[][] iArr, int i) {
        HashSet A12 = C12970iu.A12();
        for (int[] iArr2 : iArr) {
            A12.add(new C37471mS(iArr2));
        }
        this.A00.put(Integer.valueOf(i), A12);
    }
}
