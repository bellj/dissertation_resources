package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/* renamed from: X.1q8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39581q8 extends Drawable {
    public final float A00;
    public final Paint A01 = new Paint();
    public final int[] A02;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    public C39581q8(int[] iArr, float f) {
        this.A02 = iArr;
        this.A00 = f;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int width = getBounds().width();
        int height = getBounds().height();
        Paint paint = this.A01;
        paint.setTextSize(((float) width) * this.A00);
        paint.setStyle(Paint.Style.FILL);
        paint.setAlpha(255);
        paint.setTextAlign(Paint.Align.CENTER);
        int i = width >> 1;
        int descent = (int) (((float) (height >> 1)) - ((paint.descent() + paint.ascent()) / 2.0f));
        int[] iArr = this.A02;
        StringBuilder sb = new StringBuilder();
        for (int i2 : iArr) {
            sb.appendCodePoint(i2);
        }
        String obj = sb.toString();
        if (!AnonymousClass0RB.A00(paint, obj)) {
            obj = "□";
        }
        canvas.drawText(obj, (float) i, (float) descent, paint);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A01.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A01.setColorFilter(colorFilter);
    }
}
