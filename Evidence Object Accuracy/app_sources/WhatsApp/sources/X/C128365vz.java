package X;

import android.text.TextUtils;
import com.whatsapp.payments.ui.NoviEditTransactionDescriptionFragment;

/* renamed from: X.5vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128365vz {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public String A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public String A0V;
    public String A0W;
    public String A0X;
    public String A0Y;
    public String A0Z;
    public String A0a;
    public String A0b;
    public String A0c;
    public String A0d;
    public String A0e;
    public String A0f;
    public String A0g;
    public String A0h;
    public String A0i;
    public String A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public String A0n;

    public static void A00(AbstractC28901Pl r4, AnonymousClass60Y r5, String str) {
        AnonymousClass610 r1 = new AnonymousClass610(str, "NOVI_HUB", "FI_INFO", "BUTTON");
        String A02 = AbstractC28901Pl.A02(r4.A04());
        C128365vz r12 = r1.A00;
        r12.A0T = A02;
        r12.A0S = r4.A0A;
        r5.A05(r12);
    }

    public static void A01(C128365vz r1, CharSequence charSequence) {
        r1.A00 = Boolean.valueOf(!TextUtils.isEmpty(charSequence));
    }

    public static void A02(NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment, String str, String str2) {
        C128365vz r1 = new AnonymousClass610(str, "SEND_MONEY", "REVIEW_TRANSACTION", str2).A00;
        r1.A0i = "ADD_TRANSACTION_MESSAGE";
        r1.A00 = Boolean.valueOf(!TextUtils.isEmpty(noviEditTransactionDescriptionFragment.A06));
        noviEditTransactionDescriptionFragment.A04.A06(r1);
    }
}
