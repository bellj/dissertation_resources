package X;

/* renamed from: X.04U  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04U extends RuntimeException {
    public AnonymousClass04U(String str) {
        super(str != null ? str.toString() : "The operation has been canceled.");
    }
}
