package X;

import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.5l1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122165l1 extends AbstractC118815cQ {
    public final LinearLayout A00;
    public final LinearLayout A01;
    public final WaImageView A02;
    public final WaTextView A03;

    public C122165l1(View view) {
        super(view);
        this.A01 = C117305Zk.A07(view, R.id.payment_details);
        this.A00 = C117305Zk.A07(view, R.id.message_biz);
        this.A02 = C12980iv.A0X(view, R.id.payment_currency_symbol_icon);
        this.A03 = C12960it.A0N(view, R.id.message_biz_title);
    }
}
