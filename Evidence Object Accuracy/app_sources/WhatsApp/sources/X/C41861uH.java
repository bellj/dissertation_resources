package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.1uH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41861uH {
    public static boolean A00(C14850m9 r2, Jid jid) {
        if (jid == null || jid.getType() != 7) {
            return false;
        }
        return r2.A07(1844);
    }
}
