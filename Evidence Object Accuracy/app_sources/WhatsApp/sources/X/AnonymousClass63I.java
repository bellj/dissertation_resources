package X;

import android.hardware.display.DisplayManager;

/* renamed from: X.63I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63I implements DisplayManager.DisplayListener {
    public final /* synthetic */ DisplayManager A00;
    public final /* synthetic */ C1309960u A01;

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayChanged(int i) {
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayRemoved(int i) {
    }

    public AnonymousClass63I(DisplayManager displayManager, C1309960u r2) {
        this.A01 = r2;
        this.A00 = displayManager;
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayAdded(int i) {
        C1309960u r2 = this.A01;
        if (r2.A03()) {
            AnonymousClass6MA r0 = r2.A01;
            if (r0 != null) {
                r0.AVY();
            }
            this.A00.unregisterDisplayListener(r2.A00);
        }
    }
}
