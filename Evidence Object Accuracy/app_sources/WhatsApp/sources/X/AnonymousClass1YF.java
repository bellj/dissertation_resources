package X;

import java.util.Arrays;

/* renamed from: X.1YF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YF {
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;

    public AnonymousClass1YF(int i, long j, long j2, long j3, long j4) {
        this.A00 = i;
        this.A04 = j;
        this.A01 = j2;
        this.A03 = j3;
        this.A02 = j4;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || !(obj instanceof AnonymousClass1YF)) {
                return false;
            }
            AnonymousClass1YF r7 = (AnonymousClass1YF) obj;
            if (!(this.A00 == r7.A00 && this.A04 == r7.A04 && this.A01 == r7.A01 && this.A03 == r7.A03 && this.A02 == r7.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), Long.valueOf(this.A04), Long.valueOf(this.A01), Long.valueOf(this.A03), Long.valueOf(this.A02)});
    }
}
