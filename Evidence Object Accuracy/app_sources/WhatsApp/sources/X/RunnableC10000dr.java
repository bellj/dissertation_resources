package X;

import android.os.PowerManager;

/* renamed from: X.0dr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10000dr implements Runnable {
    public final /* synthetic */ PowerManager.WakeLock A00;
    public final /* synthetic */ AnonymousClass0NG A01;
    public final /* synthetic */ Runnable A02;

    public RunnableC10000dr(PowerManager.WakeLock wakeLock, AnonymousClass0NG r2, Runnable runnable) {
        this.A01 = r2;
        this.A02 = runnable;
        this.A00 = wakeLock;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A02.run();
        } finally {
            this.A00.release();
        }
    }
}
