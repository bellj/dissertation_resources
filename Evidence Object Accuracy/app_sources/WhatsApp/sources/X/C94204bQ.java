package X;

import android.opengl.GLES20;
import com.whatsapp.util.Log;
import java.nio.Buffer;
import java.nio.FloatBuffer;

/* renamed from: X.4bQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94204bQ {
    public int A00;

    public C94204bQ() {
        int A00 = A00(35633, "varying vec2 interp_tc;\nattribute vec4 in_pos;\nattribute vec4 in_tc;\n\nuniform mat4 texMatrix;\n\nvoid main() {\n    gl_Position = in_pos;\n    interp_tc = (texMatrix * in_tc).xy;\n}\n");
        int A002 = A00(35632, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n");
        int glCreateProgram = GLES20.glCreateProgram();
        this.A00 = glCreateProgram;
        if (glCreateProgram != 0) {
            GLES20.glAttachShader(glCreateProgram, A00);
            GLES20.glAttachShader(this.A00, A002);
            GLES20.glLinkProgram(this.A00);
            int[] iArr = {0};
            GLES20.glGetProgramiv(this.A00, 35714, iArr, 0);
            if (iArr[0] == 1) {
                GLES20.glDeleteShader(A00);
                GLES20.glDeleteShader(A002);
                C93054Yu.A01("Creating GlShader");
                return;
            }
            Log.e(C12960it.A0d(GLES20.glGetProgramInfoLog(this.A00), C12960it.A0k("GlShader Could not link program: ")));
            throw C12990iw.A0m(GLES20.glGetProgramInfoLog(this.A00));
        }
        throw C12990iw.A0m(C12960it.A0f(C12960it.A0k("glCreateProgram() failed. GLES20 error: "), GLES20.glGetError()));
    }

    public static int A00(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        if (glCreateShader != 0) {
            GLES20.glShaderSource(glCreateShader, str);
            GLES20.glCompileShader(glCreateShader);
            int[] iArr = {0};
            GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
            if (iArr[0] == 1) {
                C93054Yu.A01("compileShader");
                return glCreateShader;
            }
            StringBuilder A0k = C12960it.A0k("GlShader Could not compile shader ");
            A0k.append(i);
            A0k.append(":");
            Log.e(C12960it.A0d(GLES20.glGetShaderInfoLog(glCreateShader), A0k));
            throw C12990iw.A0m(GLES20.glGetShaderInfoLog(glCreateShader));
        }
        throw C12990iw.A0m(C12960it.A0f(C12960it.A0k("glCreateShader() failed. GLES20 error: "), GLES20.glGetError()));
    }

    public int A01(String str) {
        int i = this.A00;
        if (i != -1) {
            int glGetUniformLocation = GLES20.glGetUniformLocation(i, str);
            if (glGetUniformLocation >= 0) {
                return glGetUniformLocation;
            }
            StringBuilder A0k = C12960it.A0k("Could not locate uniform '");
            A0k.append(str);
            throw C12990iw.A0m(C12960it.A0d("' in program", A0k));
        }
        throw C12990iw.A0m("The program has been released");
    }

    public void A02(String str, FloatBuffer floatBuffer) {
        int i = this.A00;
        if (i == -1) {
            throw C12990iw.A0m("The program has been released");
        } else if (i != -1) {
            int glGetAttribLocation = GLES20.glGetAttribLocation(i, str);
            if (glGetAttribLocation >= 0) {
                GLES20.glEnableVertexAttribArray(glGetAttribLocation);
                GLES20.glVertexAttribPointer(glGetAttribLocation, 2, 5126, false, 0, (Buffer) floatBuffer);
                C93054Yu.A01("setVertexAttribArray");
                return;
            }
            StringBuilder A0k = C12960it.A0k("Could not locate '");
            A0k.append(str);
            throw C12990iw.A0m(C12960it.A0d("' in program", A0k));
        } else {
            throw C12990iw.A0m("The program has been released");
        }
    }
}
