package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

/* renamed from: X.3bC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70383bC implements AbstractC41521tf {
    public final int A00;
    public final ImageView A01;
    public final AnonymousClass19O A02;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public /* synthetic */ C70383bC(ImageView imageView, AnonymousClass19O r2, int i) {
        this.A02 = r2;
        this.A01 = imageView;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A02.A03(this.A01.getContext());
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        ImageView imageView = this.A01;
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(this.A00);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C12990iw.A1D(this.A01);
    }
}
