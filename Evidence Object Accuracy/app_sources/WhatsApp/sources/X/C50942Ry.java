package X;

import org.json.JSONObject;

/* renamed from: X.2Ry  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50942Ry {
    public int A00;
    public int A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final long A05;
    public final long A06;
    public final AnonymousClass20C A07;
    public final C50952Rz A08;
    public final AnonymousClass2SC A09;
    public final AnonymousClass2SA A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;

    public C50942Ry(AnonymousClass102 r8, C50952Rz r9, AnonymousClass1V8 r10, String str) {
        int i;
        AnonymousClass20C r0;
        this.A08 = r9;
        this.A0D = str;
        AnonymousClass1V8 A0F = r10.A0F("account").A0F("offer");
        this.A0F = A0F.A0H("title");
        this.A05 = A0F.A09(A0F.A0H("end_ts"), "end_ts");
        this.A0E = A0F.A0H("terms_url");
        this.A06 = A0F.A09(A0F.A0H("start_ts"), "start_ts");
        this.A0B = A0F.A0H("description");
        this.A0C = A0F.A0H("fine_print_url");
        this.A03 = A0F.A06(A0F.A0H("redeem_limit"), "redeem_limit");
        String A0H = A0F.A0H("state");
        if (A0H.equalsIgnoreCase("active")) {
            i = 0;
        } else if (A0H.equalsIgnoreCase("inactive")) {
            i = 1;
        } else if (A0H.equalsIgnoreCase("archived")) {
            i = 2;
        } else {
            StringBuilder sb = new StringBuilder("invalid state for offer: ");
            sb.append(A0H);
            throw new AnonymousClass1V9(sb.toString());
        }
        this.A04 = i;
        AnonymousClass1V8 A0F2 = A0F.A0F("offer_amount").A0F("money");
        AbstractC30791Yv A02 = r8.A02(A0F2.A0H("currency"));
        long A06 = (long) A0F2.A06(A0F2.A0H("value"), "value");
        int A062 = A0F2.A06(A0F2.A0H("offset"), "offset");
        if (A062 <= 0) {
            r0 = new AnonymousClass20C(A02, A06);
        } else {
            r0 = new AnonymousClass20C(A02, A062, A06);
        }
        this.A07 = r0;
        AnonymousClass1V8 A0F3 = A0F.A0F("eligibility");
        this.A09 = new AnonymousClass2SC(r8, this, A0F3.A0F("payment"));
        this.A0A = new AnonymousClass2SA(this, A0F3.A0E("receiver"));
        this.A00 = 0;
        this.A01 = 0;
        this.A02 = false;
    }

    public C50942Ry(String str) {
        JSONObject jSONObject = new JSONObject(str);
        this.A04 = jSONObject.getInt("state");
        this.A05 = jSONObject.getLong("end_ts");
        this.A0F = jSONObject.getString("title");
        this.A0D = jSONObject.getString("locale");
        this.A06 = jSONObject.getLong("start_ts");
        this.A0E = jSONObject.getString("terms_url");
        this.A03 = jSONObject.getInt("redeem_limit");
        this.A0B = jSONObject.getString("description");
        this.A0C = jSONObject.getString("fine_print_url");
        this.A02 = jSONObject.getBoolean("interactive_sync_done");
        this.A00 = jSONObject.getInt("kill_switch_info_viewed");
        this.A01 = jSONObject.getInt("sender_maxed_info_viewed");
        this.A08 = new C50952Rz(jSONObject.getString("id"));
        new AnonymousClass20C(C30771Yt.A06, 0);
        this.A07 = AnonymousClass20C.A00(new JSONObject(jSONObject.getString("offer_amount")));
        this.A09 = new AnonymousClass2SC(this, jSONObject.getString("payment"));
        this.A0A = new AnonymousClass2SA(this, jSONObject.getString("receiver"));
    }
}
