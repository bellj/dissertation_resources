package X;

import android.content.Context;
import com.whatsapp.gallerypicker.GalleryPicker;

/* renamed from: X.4qB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103084qB implements AbstractC009204q {
    public final /* synthetic */ GalleryPicker A00;

    public C103084qB(GalleryPicker galleryPicker) {
        this.A00 = galleryPicker;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
