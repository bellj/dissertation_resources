package X;

import android.view.animation.Interpolator;

/* renamed from: X.0PS  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PS {
    public int A00 = -1;
    public int A01;
    public int A02;
    public Interpolator A03;
    public Interpolator A04;
    public AbstractC12100hN A05;
    public AbstractC12110hO A06;
    public AnonymousClass3JI A07;
    public String A08;
    public boolean A09 = false;
    public final C14260l7 A0A;

    public AnonymousClass0PS(C14260l7 r2) {
        this.A0A = r2;
    }

    public C06060Sa A00() {
        AnonymousClass3JI r7 = this.A07;
        if (r7 != null) {
            int i = this.A00;
            if (i != -1) {
                C14260l7 r6 = this.A0A;
                int i2 = this.A02;
                int i3 = this.A01;
                return new C06060Sa(this.A04, this.A03, null, this.A05, this.A06, r6, r7, this.A08, i, i2, i3, this.A09);
            }
            throw new IllegalArgumentException("Auto-dismiss duration must be >= 0ms");
        }
        throw new IllegalArgumentException("Bloks content cannot be null in popover window");
    }

    public void A01(AnonymousClass3JI r1) {
        this.A07 = r1;
    }
}
