package X;

import java.util.Set;

/* renamed from: X.0rq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18060rq {
    public final Set A00;

    public C18060rq(Set set) {
        C16700pc.A0E(set, 1);
        this.A00 = set;
    }

    public final void A00() {
        for (AbstractC16890pv r0 : this.A00) {
            r0.AMJ();
        }
    }
}
