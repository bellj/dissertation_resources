package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60T  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60T {
    public static final byte[] A04 = Base64.decode("l6AjIyMhJYdTCB0+urtee7k2HmerRdr4c6seZyY2Pmw=", 2);
    public static final byte[] A05 = Base64.decode("oHsO+vVXYHOZXitgkZS2DI9N4+L+klLpMby3+JOPVGo=", 2);
    public final C14830m7 A00;
    public final C129575xw A01;
    public final C30931Zj A02 = C117305Zk.A0V("PaymentsProviderKeyManager", "infra");
    public final C22120yY A03;

    public AnonymousClass60T(C14830m7 r3, C129575xw r4, C22120yY r5) {
        this.A00 = r3;
        this.A03 = r5;
        this.A01 = r4;
    }

    public AnonymousClass6B7 A00(AnonymousClass1V8 r15) {
        Long valueOf;
        byte[] bytes;
        String A0H = r15.A0H("key-type");
        String A0H2 = r15.A0H("provider");
        String A0H3 = r15.A0H("key-version");
        String A0H4 = r15.A0H("key-scope");
        byte[] bArr = null;
        String A0I = r15.A0I("expiry-ts", null);
        if (!"none".equals(A0H)) {
            bArr = r15.A0F("data").A01;
        }
        if (TextUtils.isEmpty(A0I)) {
            valueOf = null;
        } else {
            valueOf = Long.valueOf(C28421Nd.A01(A0I, 0));
        }
        AnonymousClass6B7 r7 = new AnonymousClass6B7(valueOf, A0H2, A0H4, A0H, A0H3, bArr);
        if (!"DOC-UPLOAD".equals(A0H4) && !"DYI-REPORT".equals(A0H4)) {
            byte[] bArr2 = r15.A0F("signature").A01;
            AnonymousClass009.A05(bArr2);
            AnonymousClass2ST r4 = new AnonymousClass2ST(A04);
            byte[][] bArr3 = new byte[6];
            bArr3[0] = r7.A05.getBytes();
            bArr3[1] = r7.A03.getBytes();
            bArr3[2] = r7.A04.getBytes();
            byte[] bArr4 = r7.A06;
            if (bArr4 == null) {
                bArr4 = new byte[0];
            }
            bArr3[3] = bArr4;
            bArr3[4] = r7.A02.getBytes();
            Long l = r7.A01;
            if (l == null) {
                bytes = new byte[0];
            } else {
                bytes = String.valueOf(l).getBytes();
            }
            bArr3[5] = bytes;
            if (!C32001bS.A00().A02(r4.A01, C16050oM.A05(bArr3), bArr2)) {
                return null;
            }
        }
        return r7;
    }

    public AnonymousClass6B7 A01(String str, String str2, boolean z) {
        AnonymousClass6B7 r8;
        JSONObject A052;
        String optString;
        String optString2;
        String optString3;
        Long l;
        C129575xw r3 = this.A01;
        SharedPreferences A00 = r3.A00();
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("::");
        String string = A00.getString(C12960it.A0d(str2, A0j), null);
        if (TextUtils.isEmpty(string)) {
            C30931Zj r4 = r3.A01;
            StringBuilder A0j2 = C12960it.A0j("getProviderKey/provider=");
            A0j2.append(str);
            r4.A04(C12960it.A0d(" is null", A0j2));
        } else {
            try {
                A052 = C13000ix.A05(string);
                optString = A052.optString("key_type");
                optString2 = A052.optString("key_version");
                optString3 = A052.optString("key_data");
            } catch (JSONException e) {
                C30931Zj r42 = r3.A01;
                StringBuilder A0j3 = C12960it.A0j("getProviderKey/provider=");
                A0j3.append(str);
                r42.A0A(C12960it.A0d(" threw: ", A0j3), e);
                r8 = null;
            }
            if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2) || (!"none".equals(optString) && TextUtils.isEmpty(optString3))) {
                StringBuilder A0j4 = C12960it.A0j("getProviderKey/provider=");
                A0j4.append(str);
                StringBuilder A0k = C12960it.A0k(C30931Zj.A01("PaymentProviderKeySharedPrefs", C12960it.A0d(" providerKey is null", A0j4)));
                if (TextUtils.isEmpty(optString)) {
                    A0k.append(" keyType is null");
                }
                if (TextUtils.isEmpty(optString2)) {
                    A0k.append(" keyVersion is null");
                }
                if (!"none".equals(optString) && TextUtils.isEmpty(optString3)) {
                    A0k.append(" keyData is null");
                }
                Log.e(A0k.toString());
            } else {
                String optString4 = A052.optString("key_expiry");
                r8 = new AnonymousClass6B7(TextUtils.isEmpty(optString4) ? null : Long.valueOf(C28421Nd.A01(optString4, 0)), str, str2, optString, optString2, Base64.decode(optString3, 2));
                if (z || r8 == null || (l = r8.A01) == null || l.longValue() * 1000 >= this.A00.A00()) {
                    return r8;
                }
                r3.A01(str, str2);
                return null;
            }
        }
        r8 = null;
        if (z) {
        }
        return r8;
    }

    public void A02(C452120p r4, String str, String str2) {
        this.A01.A01(str, str2);
        AnonymousClass1V8 r0 = r4.A03;
        if (r0 != null) {
            try {
                AnonymousClass6B7 A00 = A00(r0);
                if (A00 != null) {
                    A03(A00);
                }
            } catch (AnonymousClass1V9 e) {
                this.A02.A05(C12960it.A0b("handleStaleKey/failed to parse key node/exception: ", e));
            }
        }
    }

    public void A03(AnonymousClass6B7 r9) {
        C129575xw r4 = this.A01;
        String str = r9.A02;
        SharedPreferences.Editor edit = r4.A00().edit();
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("key_type", r9.A03).put("key_version", r9.A04);
            byte[] bArr = r9.A06;
            if (bArr != null) {
                A0a.put("key_data", C117305Zk.A0n(bArr));
            }
            Long l = r9.A01;
            if (l != null) {
                A0a.put("key_expiry", C12960it.A0Z(l, "", C12960it.A0h()));
            }
            StringBuilder A0j = C12960it.A0j(r9.A05);
            A0j.append("::");
            edit.putString(C12960it.A0d(str, A0j), A0a.toString());
            edit.apply();
        } catch (JSONException e) {
            r4.A01.A05(C12960it.A0b("storeProviderKey threw ", e));
        }
    }
}
