package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55152hs extends AnonymousClass03U {
    public boolean A00 = true;
    public final View A01;
    public final View A02;
    public final View A03;
    public final View A04;
    public final View A05;
    public final ViewGroup A06;
    public final ViewGroup A07;
    public final AnonymousClass018 A08;

    public C55152hs(Context context, ViewGroup viewGroup, AnonymousClass018 r5) {
        super(C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.sticker_picker_reactions));
        this.A08 = r5;
        View view = this.A0H;
        this.A01 = view.findViewById(R.id.reactions_bg);
        this.A02 = view.findViewById(R.id.reaction_lol);
        this.A04 = view.findViewById(R.id.reaction_sad);
        this.A03 = view.findViewById(R.id.reaction_love);
        this.A05 = view.findViewById(R.id.reaction_wow);
        this.A07 = C12980iv.A0P(view, R.id.reactions_right);
        this.A06 = C12980iv.A0P(view, R.id.reactions_left);
    }

    public void A08() {
        if (!this.A00) {
            this.A01.setBackgroundResource(0);
            AnonymousClass073.A01((ViewGroup) this.A0H, new C03070Fz());
            this.A05.setVisibility(8);
            this.A02.setVisibility(8);
            this.A04.setVisibility(8);
            this.A03.setSelected(false);
            this.A00 = true;
        }
    }
}
