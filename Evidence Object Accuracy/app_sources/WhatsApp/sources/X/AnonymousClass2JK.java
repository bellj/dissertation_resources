package X;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import java.util.HashMap;

/* renamed from: X.2JK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JK extends AnonymousClass2JJ implements AbstractC35581iK {
    public static final String[] A00 = {"image/jpeg", "image/png"};
    public static final String[] A01 = {"_id", "_data", "datetaken", "mini_thumb_magic", "orientation", "title", "mime_type", "date_modified", "_size"};

    public AnonymousClass2JK(Uri uri, C16590pI r2, C19390u2 r3, String str, int i) {
        super(uri, r2, r3, str, i);
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        String str;
        String[] strArr;
        Uri build = this.A04.buildUpon().appendQueryParameter("distinct", "true").build();
        ContentResolver contentResolver = this.A03;
        String[] strArr2 = {"bucket_display_name", "bucket_id"};
        String str2 = this.A07;
        if (str2 == null) {
            str = "(mime_type in (?, ?))";
        } else {
            str = "(mime_type in (?, ?)) AND bucket_id = ?";
        }
        String[] strArr3 = A00;
        if (str2 != null) {
            int length = strArr3.length;
            strArr = new String[length + 1];
            System.arraycopy(strArr3, 0, strArr, 0, length);
            strArr[length] = str2;
        } else {
            strArr = strArr3;
        }
        Cursor query = MediaStore.Images.Media.query(contentResolver, build, strArr2, str, strArr, null);
        try {
            HashMap hashMap = new HashMap();
            if (query != null) {
                while (query.moveToNext()) {
                    String string = query.getString(0);
                    String string2 = query.getString(1);
                    if (string == null) {
                        string = "";
                    }
                    hashMap.put(string2, string);
                }
            }
            if (query != null) {
                query.close();
            }
            return hashMap;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }
}
