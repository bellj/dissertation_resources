package X;

import java.util.Arrays;

/* renamed from: X.51T  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass51T implements AnonymousClass27D {
    @Override // X.AnonymousClass27D
    public byte[] A7n(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
