package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09940dl implements Runnable {
    public final /* synthetic */ RunnableC10250eH A00;
    public final /* synthetic */ AnonymousClass040 A01;
    public final /* synthetic */ String A02;

    public RunnableC09940dl(RunnableC10250eH r1, AnonymousClass040 r2, String str) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = str;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            try {
                try {
                    AnonymousClass043 r8 = (AnonymousClass043) this.A01.get();
                    if (r8 == null) {
                        C06390Tk.A00().A03(RunnableC10250eH.A0J, String.format("%s returned a null result. Treating it as a failure.", this.A00.A08.A0G), new Throwable[0]);
                    } else {
                        C06390Tk A00 = C06390Tk.A00();
                        String str = RunnableC10250eH.A0J;
                        RunnableC10250eH r2 = this.A00;
                        A00.A02(str, String.format("%s returned a %s result.", r2.A08.A0G, r8), new Throwable[0]);
                        r2.A02 = r8;
                    }
                } catch (InterruptedException | ExecutionException e) {
                    C06390Tk.A00().A03(RunnableC10250eH.A0J, String.format("%s failed because it threw an exception/error", this.A02), e);
                }
            } catch (CancellationException e2) {
                C06390Tk.A00().A04(RunnableC10250eH.A0J, String.format("%s was cancelled", this.A02), e2);
            }
        } finally {
            this.A00.A02();
        }
    }
}
