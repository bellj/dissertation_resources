package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* renamed from: X.4cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94944cn {
    public int A00;
    public byte[] A01;
    public byte[] A02;

    public static byte[] A00(char[] cArr) {
        int length;
        int i = 0;
        if (cArr == null || (length = cArr.length) <= 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[(length + 1) << 1];
        do {
            int i2 = i << 1;
            char c = cArr[i];
            C72463ee.A0W(bArr, c, i2);
            bArr[i2 + 1] = (byte) c;
            i++;
        } while (i != length);
        return bArr;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:38:0x0059 */
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: char[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: char */
    /* JADX DEBUG: Multi-variable search result rejected for r5v1, resolved type: char */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1 */
    public static byte[] A01(char[] cArr) {
        int i;
        int i2;
        if (cArr == 0) {
            return new byte[0];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i3 = 0;
        while (true) {
            try {
                int length = cArr.length;
                if (i3 >= length) {
                    return byteArrayOutputStream.toByteArray();
                }
                char c = cArr[i3];
                if (c >= 128) {
                    int i4 = (c >> 6) | 192;
                    int i5 = c;
                    if (c >= 2048) {
                        if (c < 55296 || c > 57343) {
                            i2 = (c >> 12) | 224;
                            i = c;
                        } else {
                            i3++;
                            if (i3 < length) {
                                char c2 = cArr[i3];
                                if (c <= 56319) {
                                    int i6 = (((c & 1023) << 10) | (c2 & 1023)) + 65536;
                                    byteArrayOutputStream.write((i6 >> 18) | 240);
                                    i2 = ((i6 >> 12) & 63) | 128;
                                    i = i6;
                                } else {
                                    throw C12960it.A0U("invalid UTF-16 codepoint");
                                }
                            } else {
                                throw C12960it.A0U("invalid UTF-16 codepoint");
                            }
                        }
                        byteArrayOutputStream.write(i2);
                        i4 = ((i >> 6) & 63) | 128;
                        i5 = i;
                    }
                    byteArrayOutputStream.write(i4);
                    c = ((i5 == 1 ? 1 : 0) & 63) | 128;
                }
                byteArrayOutputStream.write(c == true ? 1 : 0);
                i3++;
            } catch (IOException unused) {
                throw C12960it.A0U("cannot encode string to byte array!");
            }
        }
    }

    public AnonymousClass20L A02(int i) {
        if ((this instanceof C115015Ny) || (this instanceof C114995Nw) || !(this instanceof C115005Nx)) {
            return A03(i);
        }
        int i2 = i >> 3;
        return new AnonymousClass20K(((C115005Nx) this).A05(3, i2), i2);
    }

    public AnonymousClass20L A03(int i) {
        int i2;
        byte[] A05;
        if (this instanceof C115015Ny) {
            i2 = i >> 3;
            A05 = ((C115015Ny) this).A05(i2);
        } else if (this instanceof C114995Nw) {
            C114995Nw r1 = (C114995Nw) this;
            i2 = i >> 3;
            if (i2 <= r1.A00.ACZ()) {
                A05 = r1.A05();
            } else {
                StringBuilder A0k = C12960it.A0k("Can't generate a derived key ");
                A0k.append(i2);
                throw C12970iu.A0f(C12960it.A0d(" bytes long.", A0k));
            }
        } else if (!(this instanceof C115005Nx)) {
            i2 = i >> 3;
            A05 = ((C114985Nv) this).A05(i2);
        } else {
            i2 = i >> 3;
            A05 = ((C115005Nx) this).A05(1, i2);
        }
        return new AnonymousClass20K(A05, i2);
    }

    public AnonymousClass20L A04(int i, int i2) {
        int i3;
        int i4;
        byte[] A05;
        if (this instanceof C115015Ny) {
            i3 = i >> 3;
            i4 = i2 >> 3;
            A05 = ((C115015Ny) this).A05(i3 + i4);
        } else if (this instanceof C114995Nw) {
            C114995Nw r1 = (C114995Nw) this;
            i3 = i >> 3;
            i4 = i2 >> 3;
            int i5 = i3 + i4;
            if (i5 <= r1.A00.ACZ()) {
                A05 = r1.A05();
            } else {
                StringBuilder A0k = C12960it.A0k("Can't generate a derived key ");
                A0k.append(i5);
                throw C12970iu.A0f(C12960it.A0d(" bytes long.", A0k));
            }
        } else if (!(this instanceof C115005Nx)) {
            i3 = i >> 3;
            i4 = i2 >> 3;
            A05 = ((C114985Nv) this).A05(i3 + i4);
        } else {
            C115005Nx r12 = (C115005Nx) this;
            int i6 = i >> 3;
            int i7 = i2 >> 3;
            byte[] A052 = r12.A05(1, i6);
            return new C113075Fx(new AnonymousClass20K(A052, i6), r12.A05(2, i7), 0, i7);
        }
        return new C113075Fx(new AnonymousClass20K(A05, i3), A05, i3, i4);
    }
}
