package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0Tl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06400Tl {
    public ConcurrentHashMap A00 = new ConcurrentHashMap();

    public static long A02(Typeface typeface) {
        if (typeface == null) {
            return 0;
        }
        try {
            Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            return ((Number) declaredField.get(typeface)).longValue();
        } catch (IllegalAccessException | NoSuchFieldException e) {
            Log.e("TypefaceCompatBaseImpl", "Could not retrieve font from family.", e);
            return 0;
        }
    }

    public static Object A03(AbstractC12340hl r10, Object[] objArr, int i) {
        int i2 = 700;
        if ((i & 1) == 0) {
            i2 = 400;
        }
        boolean z = false;
        if ((i & 2) != 0) {
            z = true;
        }
        Object obj = null;
        int i3 = Integer.MAX_VALUE;
        for (Object obj2 : objArr) {
            int abs = Math.abs(r10.AHk(obj2) - i2) << 1;
            int i4 = 1;
            if (r10.AJg(obj2) == z) {
                i4 = 0;
            }
            int i5 = abs + i4;
            if (obj == null || i3 > i5) {
                obj = obj2;
                i3 = i5;
            }
        }
        return obj;
    }

    public Typeface A04(Context context, Resources resources, AnonymousClass0MQ r13, int i) {
        C05020Ny r0 = (C05020Ny) A03(new C07330Xo(this), r13.A00, i);
        if (r0 == null) {
            return null;
        }
        int i2 = r0.A00;
        String str = r0.A03;
        Typeface A05 = AnonymousClass082.A01.A05(context, resources, str, i2, i);
        if (A05 != null) {
            AnonymousClass082.A00.A08(AnonymousClass082.A01(resources, str, i2, 0, i), A05);
        }
        long A02 = A02(A05);
        if (A02 == 0) {
            return A05;
        }
        this.A00.put(Long.valueOf(A02), r13);
        return A05;
    }

    public Typeface A05(Context context, Resources resources, String str, int i, int i2) {
        File A00 = C06500Tw.A00(context);
        if (A00 == null) {
            return null;
        }
        try {
            if (!C06500Tw.A02(resources, A00, i)) {
                return null;
            }
            return Typeface.createFromFile(A00.getPath());
        } catch (RuntimeException unused) {
            return null;
        } finally {
            A00.delete();
        }
    }

    public Typeface A06(Context context, CancellationSignal cancellationSignal, C04920No[] r6, int i) {
        InputStream inputStream;
        Throwable th;
        InputStream inputStream2 = null;
        if (r6.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(A08(r6, i).A03);
            try {
                Typeface A07 = A07(context, inputStream);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                    }
                }
                return A07;
            } catch (IOException unused2) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused3) {
                    }
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                inputStream2 = inputStream;
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException unused4) {
                    }
                }
                throw th;
            }
        } catch (IOException unused5) {
            inputStream = null;
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public Typeface A07(Context context, InputStream inputStream) {
        File A00 = C06500Tw.A00(context);
        if (A00 == null) {
            return null;
        }
        try {
            if (!C06500Tw.A03(A00, inputStream)) {
                return null;
            }
            return Typeface.createFromFile(A00.getPath());
        } catch (RuntimeException unused) {
            return null;
        } finally {
            A00.delete();
        }
    }

    public C04920No A08(C04920No[] r2, int i) {
        return (C04920No) A03(new C07320Xn(this), r2, i);
    }
}
