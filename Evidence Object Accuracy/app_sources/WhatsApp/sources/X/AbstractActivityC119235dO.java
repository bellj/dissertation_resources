package X;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiBankAccountAddedLandingActivity;
import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;
import org.json.JSONObject;

/* renamed from: X.5dO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119235dO extends ActivityC13790kL {
    public AbstractActivityC119235dO() {
        C117295Zj.A0p(this, 7);
    }

    public static View A0D(ActivityC000800j r2) {
        View findViewById = r2.findViewById(R.id.account_layout);
        AnonymousClass028.A0D(findViewById, R.id.progress).setVisibility(8);
        AnonymousClass028.A0D(findViewById, R.id.divider).setVisibility(8);
        AnonymousClass028.A0D(findViewById, R.id.radio_button).setVisibility(8);
        return findViewById;
    }

    public static AbstractC005102i A0K(AbstractActivityC121665jA r2) {
        r2.A2t(R.drawable.onboarding_actionbar_home_close, R.id.scroll_view);
        return r2.A1U();
    }

    public static Button A0L(TextView textView, IndiaUpiBankAccountAddedLandingActivity indiaUpiBankAccountAddedLandingActivity, int i) {
        textView.setText(indiaUpiBankAccountAddedLandingActivity.getString(i));
        indiaUpiBankAccountAddedLandingActivity.A01.setText(indiaUpiBankAccountAddedLandingActivity.getString(R.string.bankaccount_linking_confirmation_desc_first_account));
        indiaUpiBankAccountAddedLandingActivity.A03.setText(indiaUpiBankAccountAddedLandingActivity.getString(R.string.bankaccount_linking_confirmation_button_text_send_a_payment));
        indiaUpiBankAccountAddedLandingActivity.A04.setText(indiaUpiBankAccountAddedLandingActivity.getString(R.string.bankaccount_linking_confirmation_button_text_done));
        return indiaUpiBankAccountAddedLandingActivity.A03;
    }

    public static AnonymousClass60W A0M(AnonymousClass2FL r1, AnonymousClass01J r2, AbstractActivityC121525iS r3, AnonymousClass01N r4) {
        r3.A0J = (AnonymousClass18S) r4.get();
        r3.A0T = (C126885tb) r2.AEm.get();
        r3.A0H = (C248217a) r2.AE5.get();
        r3.A07 = (C20370ve) r2.AEu.get();
        r3.A0L = (C18620sk) r2.AFB.get();
        r3.A0F = (AnonymousClass68Z) r2.A9T.get();
        r3.A0Z = (C16630pM) r2.AIc.get();
        r3.A0K = (AnonymousClass61M) r2.AF1.get();
        r3.A0P = (C22460z7) r2.AEF.get();
        r3.A0I = (C243515e) r2.AEt.get();
        r3.A0O = (C22540zF) r2.AEC.get();
        return r1.A0F();
    }

    public static String A0N(Activity activity) {
        return activity.getIntent().getStringExtra("extra_referral_screen");
    }

    public static String A0O(AbstractActivityC121665jA r1) {
        return r1.A2o(r1.A0B.A06());
    }

    public static String A0Z(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity) {
        if (!TextUtils.isEmpty(((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B.A07())) {
            return ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B.A07();
        }
        return ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A05(indiaUpiDeviceBindStepActivity.A0A);
    }

    public static AnonymousClass01N A0l(AnonymousClass01J r1, C249317l r2, AbstractActivityC121685jC r3) {
        ((ActivityC13790kL) r3).A08 = r2;
        r3.A07 = (C16590pI) r1.AMg.get();
        r3.A0D = r1.A3M();
        r3.A0H = (C17220qS) r1.ABt.get();
        r3.A0Z = (C21190x1) r1.A3G.get();
        r3.A0Q = (C129135xE) r1.AFj.get();
        r3.A05 = (C25841Ba) r1.A1d.get();
        r3.A06 = (C20730wE) r1.A4J.get();
        r3.A0b = (C20320vZ) r1.A7A.get();
        r3.A0P = (C17070qD) r1.AFC.get();
        r3.A09 = (C15650ng) r1.A4m.get();
        return r1.AF7;
    }

    public static AnonymousClass01N A0m(AnonymousClass01J r1, AbstractActivityC121525iS r2) {
        r2.A05 = (C21270x9) r1.A4A.get();
        r2.A01 = (AnonymousClass130) r1.A41.get();
        r2.A03 = (C15610nY) r1.AMe.get();
        r2.A00 = (C238013b) r1.A1Z.get();
        r2.A02 = (AnonymousClass10S) r1.A46.get();
        r2.A06 = (C15890o4) r1.AN1.get();
        r2.A0X = (C129515xq) r1.A9e.get();
        return r1.AEw;
    }

    public static void A0n(Intent intent, AbstractActivityC121665jA r2) {
        r2.A2v(intent);
        r2.A2G(intent, true);
    }

    public static void A0p(LayoutInflater layoutInflater, ViewGroup viewGroup, AbstractActivityC121665jA r4) {
        ((ImageView) AnonymousClass028.A0D(layoutInflater.inflate(R.layout.india_upi_psp_footer_row, viewGroup, true), R.id.psp_logo)).setImageResource(C125125qj.A00(r4.A0B.A07()).A00);
    }

    public static void A1J(ActivityC000900k r1, AnonymousClass60V r2) {
        MessageDialogFragment.A00(r2.A01(r1)).A01().A1F(r1.A0V(), null);
    }

    public static void A1R(AnonymousClass2FL r1, AnonymousClass01J r2, C18620sk r3, BrazilPaymentActivity brazilPaymentActivity) {
        brazilPaymentActivity.A0G = r3;
        brazilPaymentActivity.A0X = (C16630pM) r2.AIc.get();
        brazilPaymentActivity.A0Q = (C25871Bd) r2.ABZ.get();
        brazilPaymentActivity.A0O = (AnonymousClass61E) r2.AEY.get();
        brazilPaymentActivity.A0J = (AnonymousClass60T) r2.AFI.get();
        brazilPaymentActivity.A0I = (C22460z7) r2.AEF.get();
        brazilPaymentActivity.A0F = (C243515e) r2.AEt.get();
        brazilPaymentActivity.A0D = (C18660so) r2.AEf.get();
        brazilPaymentActivity.A0H = (C22540zF) r2.AEC.get();
        brazilPaymentActivity.A0S = (C130065yk) r2.A1z.get();
        brazilPaymentActivity.A0N = (C121255hW) r2.A1x.get();
        brazilPaymentActivity.A0U = r1.A0C();
        brazilPaymentActivity.A08 = r1.A08();
    }

    public static void A1S(AnonymousClass2FL r1, AnonymousClass01J r2, AbstractActivityC121685jC r3, AnonymousClass01N r4) {
        r3.A0O = (C22710zW) r4.get();
        r3.A0V = r1.A0D();
        r3.A0N = (C17900ra) r2.AF5.get();
        r3.A0L = r2.A3X();
        r3.A0T = (AnonymousClass17Z) r2.AEZ.get();
        r3.A0I = (C21860y6) r2.AE6.get();
        r3.A0W = r1.A0E();
        r3.A0K = (C18650sn) r2.AEe.get();
        r3.A0M = (C18610sj) r2.AF0.get();
        r3.A08 = (C20830wO) r2.A4W.get();
        r3.A0d = (AnonymousClass1AB) r2.AKI.get();
        r3.A0R = r1.A09();
        r3.A0S = r2.A3c();
        r3.A0Y = (C129395xe) r2.AEy.get();
    }

    public static void A1U(AnonymousClass2FL r1, AnonymousClass01J r2, AbstractActivityC121495iO r3) {
        r3.A01 = (C122205l5) r2.A9Z.get();
        r3.A00 = (C127425uT) r1.A0z.get();
    }

    public static void A1V(AnonymousClass2FL r1, AnonymousClass01J r2, AbstractActivityC121655j9 r3) {
        r3.A03 = (C241414j) r2.AEr.get();
        r3.A00 = (AnonymousClass130) r2.A41.get();
        r3.A01 = (C15890o4) r2.AN1.get();
        r3.A06 = (AnonymousClass18S) r2.AEw.get();
        r3.A0A = (C14920mG) r2.ALr.get();
        r3.A05 = (C248217a) r2.AE5.get();
        r3.A07 = (C18620sk) r2.AFB.get();
        r3.A09 = r1.A0F();
    }

    public static void A1W(AnonymousClass01J r1, BrazilPaymentActivity brazilPaymentActivity) {
        brazilPaymentActivity.A03 = (C15610nY) r1.AMe.get();
        brazilPaymentActivity.A04 = (AnonymousClass018) r1.ANb.get();
        brazilPaymentActivity.A0W = (C18590sh) r1.AES.get();
        brazilPaymentActivity.A07 = (C1309660r) r1.A1p.get();
    }

    public static void A1X(AnonymousClass01J r1, BrazilPaymentActivity brazilPaymentActivity) {
        brazilPaymentActivity.A0E = (C18600si) r1.AEo.get();
        brazilPaymentActivity.A06 = (C1329568x) r1.A1o.get();
        brazilPaymentActivity.A0L = (C129385xd) r1.A1v.get();
        brazilPaymentActivity.A0R = (C129945yY) r1.A1q.get();
        brazilPaymentActivity.A0P = (C130015yf) r1.AEk.get();
        brazilPaymentActivity.A0K = (AbstractC16870pt) r1.A20.get();
        brazilPaymentActivity.A0M = (C129095xA) r1.ACy.get();
        brazilPaymentActivity.A05 = (AnonymousClass102) r1.AEL.get();
        brazilPaymentActivity.A0C = (C248217a) r1.AE5.get();
        brazilPaymentActivity.A0B = (C129925yW) r1.AEW.get();
        brazilPaymentActivity.A09 = (AnonymousClass60Z) r1.A24.get();
    }

    public static void A1Y(AnonymousClass01J r1, AbstractActivityC121665jA r2) {
        r2.A05 = (C16120oU) r1.ANE.get();
        r2.A0C = (C18600si) r1.AEo.get();
        r2.A0A = (C1308460e) r1.A9c.get();
        r2.A0E = (C1309960u) r1.A1b.get();
        r2.A0B = (C1329668y) r1.A9d.get();
        r2.A0D = (AnonymousClass6BE) r1.A9X.get();
        r2.A09 = (C119865fE) r1.AEV.get();
    }

    public static void A1Z(AnonymousClass01J r1, AbstractActivityC121545iU r2) {
        r2.A0F = (C130165yu) r1.A2Z.get();
        r2.A02 = (AnonymousClass102) r1.AEL.get();
        r2.A01 = (AnonymousClass018) r1.ANb.get();
        r2.A0C = (C18590sh) r1.AES.get();
        r2.A0A = (AnonymousClass69E) r1.A9W.get();
        r2.A07 = (AnonymousClass162) r1.AFE.get();
        r2.A03 = (C129925yW) r1.AEW.get();
        r2.A0B = (C121265hX) r1.A9a.get();
    }

    public static void A1a(C30861Zc r0, AbstractActivityC121665jA r1, boolean z) {
        r1.startActivity(IndiaUpiPinPrimerFullSheetActivity.A02(r1, r0, z));
        r1.A2q();
        r1.finish();
    }

    public static void A1b(AnonymousClass2SP r1, AbstractActivityC121665jA r2) {
        r2.A0D.AKf(r1);
    }

    public static void A1c(AbstractActivityC121685jC r2, Object obj, JSONObject jSONObject) {
        jSONObject.put("step", obj);
        jSONObject.put("completedSteps", r2.A0I.A02().toString());
        jSONObject.put("isCompleteWith2FA", r2.A0I.A0B());
        jSONObject.put("isCompleteWithout2FA", r2.A0I.A0C());
    }

    public static void A1d(AbstractActivityC121665jA r4) {
        if (!r4.A0C.A01().getBoolean("payment_usync_triggered", false)) {
            ((ActivityC13830kP) r4).A05.Ab2(new Runnable() { // from class: X.6Fk
                @Override // java.lang.Runnable
                public final void run() {
                    C20730wE r2 = C20730wE.this;
                    C42271uw r1 = new C42271uw(AnonymousClass1JA.A03);
                    r1.A02 = true;
                    r1.A01 = false;
                    r1.A00 = AnonymousClass1JB.A0G;
                    r2.A01(r1.A01());
                }
            });
            r4.A0C.A01().edit().putBoolean("payment_usync_triggered", true).apply();
        }
    }

    public static void A1e(AbstractActivityC121665jA r0, int i) {
        C36021jC.A00(r0, i);
        r0.A2q();
        r0.finish();
    }

    public static void A1f(AbstractActivityC121515iQ r3) {
        Intent intent = new Intent();
        intent.putExtra("extra_bank_account", r3.A00);
        r3.setResult(-1, intent);
        r3.finish();
    }

    public static void A1g(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity) {
        ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B.A8l(((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A05(indiaUpiDeviceBindStepActivity.A0A), true);
    }

    public static void A1h(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity, int i, boolean z) {
        indiaUpiDeviceBindStepActivity.A39(new AnonymousClass60V(i), z);
    }

    public static void A1i(AbstractActivityC121545iU r1) {
        ((AbstractActivityC121665jA) r1).A0B.A0C();
        r1.AaN();
        r1.A2C(R.string.payments_still_working);
        r1.A09.A00();
    }

    public static void A1j(AbstractC128605wN r0, short s) {
        r0.A00.A0C(s);
    }

    public static boolean A1k(AbstractC28901Pl r2, AbstractActivityC121525iS r3) {
        return r3.A0X.A01(r2, r3.A0o);
    }

    public static boolean A1l(AbstractActivityC121685jC r0) {
        return r0.A0I.A0A();
    }
}
