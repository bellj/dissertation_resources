package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.util.Log;

/* renamed from: X.3Z3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z3 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1ON A00;
    public final /* synthetic */ C244315m A01;

    public AnonymousClass3Z3(AnonymousClass1ON r1, C244315m r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendFinishRegIq/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        AnonymousClass1ON r3 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/finishRegOnSuccess id=")));
        if (r5.A0E("success") == null) {
            Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/finishRegOnSuccess was empty id=")));
            r3.APr("success was empty", 1);
            return;
        }
        ((AnonymousClass1OF) r3).A00.A01();
        C14820m6 r1 = r3.A09;
        r1.A14(true);
        r1.A15(false);
        r1.A0X(0);
        r1.A16(false);
        ((AnonymousClass1OF) r3).A01.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r3, 15));
    }
}
