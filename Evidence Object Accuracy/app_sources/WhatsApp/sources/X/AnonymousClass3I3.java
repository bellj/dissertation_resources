package X;

import android.content.Context;
import android.graphics.Rect;
import com.facebook.rendercore.RenderTreeNode;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3I3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I3 {
    public static final AbstractC65073Ia A00 = new C76473lh();

    public static RenderTreeNode A00(Rect rect, AbstractC72393eW r10, RenderTreeNode renderTreeNode, AbstractC65073Ia r12) {
        Rect rect2;
        int i;
        List list;
        int AEs = r10.AEs();
        if (AEs == 0 && r10.AEu() == 0 && r10.AEt() == 0 && r10.AEr() == 0) {
            rect2 = null;
        } else {
            rect2 = new Rect(AEs, r10.AEu(), r10.AEt(), r10.AEr());
        }
        Object ADo = r10.ADo();
        if (renderTreeNode == null || (list = renderTreeNode.A00) == null) {
            i = 0;
        } else {
            i = list.size();
        }
        return new RenderTreeNode(rect, rect2, renderTreeNode, r12, ADo, i);
    }

    public static void A01(Context context, AbstractC72393eW r9, RenderTreeNode renderTreeNode, ArrayList arrayList, int i, int i2) {
        RenderTreeNode renderTreeNode2 = renderTreeNode;
        if (r9 != null) {
            int width = r9.getWidth();
            if (!(width == 0 && r9.getHeight() == 0)) {
                Rect rect = new Rect(i, i2, width + i, r9.getHeight() + i2);
                arrayList.size();
                AbstractC65073Ia AG8 = r9.AG8();
                if (AG8 != null) {
                    int ABP = r9.ABP();
                    RenderTreeNode A002 = A00(rect, r9, renderTreeNode, AG8);
                    arrayList.add(A002);
                    List list = renderTreeNode.A00;
                    if (ABP > 0) {
                        if (list == null) {
                            list = C12980iv.A0w(4);
                            renderTreeNode.A00 = list;
                        }
                        list.add(A002);
                        renderTreeNode2 = A002;
                    } else {
                        if (list == null) {
                            list = C12980iv.A0w(4);
                            renderTreeNode.A00 = list;
                        }
                        list.add(A002);
                    }
                    i = 0;
                    i2 = 0;
                }
                for (int i3 = 0; i3 < r9.ABP(); i3++) {
                    A01(context, r9.ABK(i3), renderTreeNode2, arrayList, r9.AHs(i3) + i, r9.AHt(i3) + i2);
                }
            }
        }
    }
}
