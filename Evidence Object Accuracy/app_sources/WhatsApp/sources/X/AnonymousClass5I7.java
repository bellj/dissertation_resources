package X;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.5I7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5I7 extends AbstractSet<K> {
    public final /* synthetic */ AnonymousClass5I4 this$0;

    public AnonymousClass5I7(AnonymousClass5I4 r1) {
        this.this$0 = r1;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public void clear() {
        this.this$0.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        return this.this$0.containsKey(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator iterator() {
        return this.this$0.keySetIterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        AnonymousClass5I4 r1 = this.this$0;
        Map delegateOrNull = r1.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.keySet().remove(obj);
        }
        return C12960it.A1X(r1.removeHelper(obj), AnonymousClass5I4.NOT_FOUND);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.this$0.size();
    }
}
