package X;

import java.io.IOException;

/* renamed from: X.5N1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N1 extends AnonymousClass1TM implements AbstractC115545Ry {
    public int A00;
    public AnonymousClass1TN A01;

    public AnonymousClass5N1(AnonymousClass1TN r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public AnonymousClass5N1(AnonymousClass5N2 r2) {
        this.A01 = r2;
        this.A00 = 4;
    }

    public AnonymousClass5N1(String str) {
        this.A00 = 1;
        this.A01 = new AnonymousClass5NT(str);
    }

    public static String A00(AnonymousClass5N1 r0) {
        return AnonymousClass1T7.A02(AnonymousClass5NT.A00(r0.A01).A00);
    }

    public static AnonymousClass5N1 A01(Object obj) {
        AnonymousClass5NT r1;
        AnonymousClass1TN r0;
        if (obj == null || (obj instanceof AnonymousClass5N1)) {
            return (AnonymousClass5N1) obj;
        }
        if (obj instanceof AnonymousClass5NU) {
            AnonymousClass5NU r4 = (AnonymousClass5NU) obj;
            int i = r4.A00;
            switch (i) {
                case 0:
                case 3:
                case 5:
                    return new AnonymousClass5N1(AbstractC114775Na.A05(r4, false), i);
                case 1:
                case 2:
                case 6:
                    AnonymousClass1TL A00 = AnonymousClass5NU.A00(r4);
                    if (A00 instanceof AnonymousClass5NT) {
                        r1 = AnonymousClass5NT.A00(A00);
                    } else {
                        r1 = new AnonymousClass5NT(AnonymousClass5NH.A05(A00));
                    }
                    return new AnonymousClass5N1(r1, i);
                case 4:
                    return new AnonymousClass5N1(AnonymousClass5N2.A00(AbstractC114775Na.A05(r4, true)), i);
                case 7:
                    return new AnonymousClass5N1(AnonymousClass5NH.A04(r4, false), i);
                case 8:
                    AnonymousClass1TL A002 = AnonymousClass5NU.A00(r4);
                    if (A002 instanceof AnonymousClass1TK) {
                        r0 = AnonymousClass1TK.A00(A002);
                    } else {
                        byte[] A05 = AnonymousClass5NH.A05(A002);
                        r0 = (AnonymousClass1TM) AnonymousClass1TK.A02.get(new AnonymousClass1TS(A05));
                        if (r0 == null) {
                            r0 = new AnonymousClass1TK(A05);
                        }
                    }
                    return new AnonymousClass5N1(r0, i);
                default:
                    throw C12970iu.A0f(C12960it.A0W(i, "unknown tag: "));
            }
        } else if (obj instanceof byte[]) {
            try {
                return A01(AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException unused) {
                throw C12970iu.A0f("unable to parse encoded general name");
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        int i = this.A00;
        return new C114835Ng(this.A01, i, C12960it.A1V(i, 4));
    }

    public String toString() {
        String A00;
        StringBuffer stringBuffer = new StringBuffer();
        int i = this.A00;
        stringBuffer.append(i);
        stringBuffer.append(": ");
        if (!(i == 1 || i == 2)) {
            if (i == 4) {
                A00 = AnonymousClass5N2.A00(this.A01).toString();
            } else if (i != 6) {
                A00 = this.A01.toString();
            }
            stringBuffer.append(A00);
            return stringBuffer.toString();
        }
        A00 = A00(this);
        stringBuffer.append(A00);
        return stringBuffer.toString();
    }
}
