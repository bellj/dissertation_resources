package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.util.Log;

/* renamed from: X.6Ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133386Ao implements AnonymousClass6MV {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;

    public C133386Ao(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        Log.e("PAY: BrazilPayBloksActivity/provider key iq returned null");
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r4) {
        this.A01.A2p(this.A00, r4.A05);
    }
}
