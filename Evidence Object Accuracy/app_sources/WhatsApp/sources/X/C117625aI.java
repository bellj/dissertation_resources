package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117625aI extends BaseAdapter {
    public final Context A00;
    public final C123145mg A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public C117625aI(Context context, C123145mg r2) {
        this.A00 = context;
        this.A01 = r2;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A01.A00.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A01.A00.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context context = this.A00;
        View inflate = LayoutInflater.from(context).inflate(R.layout.payment_transaction_details_status_timeline_item, (ViewGroup) null);
        C128035vS r4 = (C128035vS) this.A01.A00.get(i);
        ImageView A0K = C12970iu.A0K(inflate, R.id.status_icon);
        A0K.setColorFilter(context.getResources().getColor(r4.A00), PorterDuff.Mode.SRC_IN);
        A0K.setImageResource(r4.A01);
        TextView A0I = C12960it.A0I(inflate, R.id.transaction_status);
        A0I.setText(r4.A05);
        C12980iv.A14(context.getResources(), A0I, r4.A03);
        TextView A0I2 = C12960it.A0I(inflate, R.id.status_subtitle);
        A0I2.setText(r4.A04);
        C12980iv.A14(context.getResources(), A0I2, r4.A02);
        View A0D = AnonymousClass028.A0D(inflate, R.id.line);
        if (i == getCount() - 1) {
            A0D.setVisibility(8);
        }
        return inflate;
    }
}
