package X;

import android.security.keystore.KeyGenParameterSpec;

/* renamed from: X.0Tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06490Tv {
    public static KeyGenParameterSpec.Builder A00() {
        return new KeyGenParameterSpec.Builder("androidxBiometric", 3);
    }

    public static KeyGenParameterSpec A01(KeyGenParameterSpec.Builder builder) {
        return builder.build();
    }

    public static void A02(KeyGenParameterSpec.Builder builder) {
        builder.setBlockModes("CBC");
    }

    public static void A03(KeyGenParameterSpec.Builder builder) {
        builder.setEncryptionPaddings("PKCS7Padding");
    }
}
