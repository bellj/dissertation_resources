package X;

import android.content.Context;
import android.graphics.Canvas;
import com.whatsapp.R;

/* renamed from: X.1m9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37331m9 extends AbstractC37341mA {
    public int A00;

    @Override // X.AnonymousClass1OY
    public void A0s() {
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r1, boolean z) {
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return 0;
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onDraw(Canvas canvas) {
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    public C37331m9(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_album_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_album_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(0, this.A00);
    }

    public void setFixedHeight(int i) {
        this.A00 = i;
    }
}
