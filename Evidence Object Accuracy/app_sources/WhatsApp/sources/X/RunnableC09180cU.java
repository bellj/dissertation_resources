package X;

import java.lang.ref.WeakReference;

/* renamed from: X.0cU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09180cU implements Runnable {
    public final WeakReference A00;

    public RunnableC09180cU(AnonymousClass0EP r2) {
        this.A00 = new WeakReference(r2);
    }

    @Override // java.lang.Runnable
    public void run() {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null) {
            ((AnonymousClass0EP) weakReference.get()).A0M = false;
        }
    }
}
