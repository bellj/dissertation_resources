package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.5cA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118655cA extends AbstractC018308n {
    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r7, RecyclerView recyclerView) {
        super.A01(rect, view, r7, recyclerView);
        int A00 = RecyclerView.A00(view);
        AnonymousClass02M r0 = recyclerView.A0N;
        if (r0 == null) {
            return;
        }
        if (A00 == 0 || A00 == r0.A0D() - 1) {
            AnonymousClass028.A0e(view, AnonymousClass028.A07(view), (int) view.getResources().getDimension(R.dimen.product_margin_8dp), AnonymousClass028.A06(view), view.getPaddingBottom());
        }
    }
}
