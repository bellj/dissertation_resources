package X;

/* renamed from: X.1f9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33831f9 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Integer A04;
    public Integer A05;

    public C33831f9() {
        super(1840, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(6, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(5, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(1, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamStickerSend {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsAnimated", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsAvatar", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsFirstParty", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsFromStickerMaker", this.A03);
        Integer num = this.A04;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerSendMessageType", obj);
        Integer num2 = this.A05;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerSendOrigin", obj2);
        sb.append("}");
        return sb.toString();
    }
}
