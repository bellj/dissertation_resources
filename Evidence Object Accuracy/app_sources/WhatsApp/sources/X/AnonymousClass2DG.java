package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;

/* renamed from: X.2DG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DG implements AnonymousClass2DH {
    public final /* synthetic */ C30921Zi A00;
    public final /* synthetic */ AbstractC117185Yw A01;
    public final /* synthetic */ C22460z7 A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass2DG(C30921Zi r1, AbstractC117185Yw r2, C22460z7 r3, boolean z) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = z;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        this.A02.A0A.A06();
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        this.A02.A0A.A06();
        this.A01.APk();
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        this.A02.A0D.Ab2(new RunnableBRunnable0Shape0S0310000_I0(this, this.A00, this.A01, 9, this.A03));
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        this.A02.A0A.A06();
        this.A01.AXX();
    }
}
