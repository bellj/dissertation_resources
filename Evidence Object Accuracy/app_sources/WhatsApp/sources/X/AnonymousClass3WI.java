package X;

import java.util.List;

/* renamed from: X.3WI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WI implements AbstractC116005Tt {
    public final String A00;
    public final List A01;

    public AnonymousClass3WI(String str, List list) {
        this.A00 = str;
        this.A01 = list;
    }

    @Override // X.AbstractC116005Tt
    public C15370n3 ABZ() {
        return (C15370n3) C12980iv.A0o(this.A01);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NonWhatsAppContactListItem{displayName='");
        A0k.append(this.A00);
        A0k.append('\'');
        A0k.append(", waContactList=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
