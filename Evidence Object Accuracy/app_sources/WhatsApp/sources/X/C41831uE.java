package X;

import java.net.URL;

/* renamed from: X.1uE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41831uE {
    public byte[] A00;
    public final int A01;
    public final int A02;
    public final AbstractC14640lm A03;
    public final String A04;
    public final URL A05;

    public C41831uE(AbstractC14640lm r1, String str, URL url, byte[] bArr, int i, int i2) {
        this.A03 = r1;
        this.A00 = bArr;
        this.A05 = url;
        this.A04 = str;
        this.A01 = i;
        this.A02 = i2;
    }
}
