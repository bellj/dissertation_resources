package X;

import android.telecom.CallAudioState;
import android.telecom.Connection;
import android.telecom.DisconnectCause;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.2aJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52142aJ extends Connection {
    public AnonymousClass2OH A00;
    public String A01;

    public C52142aJ(AnonymousClass2OH r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    public String A00() {
        return this.A01;
    }

    public void A01(int i) {
        if (this.A00 != null) {
            StringBuilder A0k = C12960it.A0k("voip/SelfManagedConnection/setDisconnected ");
            A0k.append(this.A01);
            A0k.append(", cause: ");
            A0k.append(i);
            C12960it.A1F(A0k);
            setDisconnected(new DisconnectCause(i));
            destroy();
            this.A00.A0D(this);
            this.A00 = null;
        }
    }

    public void A02(String str) {
        this.A01 = str;
    }

    @Override // android.telecom.Connection
    public void onAbort() {
        Log.i("voip/SelfManagedConnection/onAbort");
        super.onAbort();
    }

    @Override // android.telecom.Connection
    public void onAnswer() {
        onAnswer(0);
    }

    @Override // android.telecom.Connection
    public void onAnswer(int i) {
        Log.i("voip/SelfManagedConnection/onAnswer");
        AnonymousClass2OH r2 = this.A00;
        if (r2 != null) {
            r2.A0F(this.A01, 2);
        }
        setActive();
    }

    @Override // android.telecom.Connection
    public void onCallAudioStateChanged(CallAudioState callAudioState) {
        Log.i(C12960it.A0b("voip/SelfManagedConnection/onCallAudioStateChanged ", callAudioState));
        super.onCallAudioStateChanged(callAudioState);
        AnonymousClass2OH r1 = this.A00;
        if (r1 != null) {
            r1.A09(callAudioState, this.A01);
        }
    }

    @Override // android.telecom.Connection
    public void onDisconnect() {
        Log.i("voip/SelfManagedConnection/onDisconnect");
        AnonymousClass2OH r2 = this.A00;
        if (r2 != null) {
            r2.A0F(this.A01, 4);
        }
        A01(2);
    }

    @Override // android.telecom.Connection
    public void onHold() {
        StringBuilder A0k = C12960it.A0k("voip/SelfManagedConnection/onHold, AudioModeIsVoip: ");
        A0k.append(getAudioModeIsVoip());
        C12960it.A1F(A0k);
        AnonymousClass2OH r2 = this.A00;
        if (r2 != null) {
            r2.A0F(this.A01, 0);
        }
        setOnHold();
    }

    @Override // android.telecom.Connection
    public void onReject() {
        Log.i("voip/SelfManagedConnection/onReject");
        AnonymousClass2OH r2 = this.A00;
        if (r2 != null) {
            r2.A0F(this.A01, 3);
        }
        A01(6);
    }

    @Override // android.telecom.Connection
    public void onReject(String str) {
        Log.i(C12960it.A0d(str, C12960it.A0k("voip/SelfManagedConnection/onReject ")));
        onReject();
    }

    @Override // android.telecom.Connection
    public void onShowIncomingCallUi() {
        Log.i("voip/SelfManagedConnection/onShowIncomingCallUi");
        AnonymousClass2OH r0 = this.A00;
        if (r0 != null) {
            String str = this.A01;
            r0.A02();
            Iterator A00 = AbstractC16230of.A00(r0);
            while (A00.hasNext()) {
                ((AnonymousClass2ON) A00.next()).A01(str);
            }
        }
    }

    @Override // android.telecom.Connection
    public void onStateChanged(int i) {
        Log.i(C12960it.A0W(i, "voip/SelfManagedConnection/onStateChanged "));
        super.onStateChanged(i);
    }

    @Override // android.telecom.Connection
    public void onUnhold() {
        StringBuilder A0k = C12960it.A0k("voip/SelfManagedConnection/onUnhold, AudioModeIsVoip: ");
        A0k.append(getAudioModeIsVoip());
        C12960it.A1F(A0k);
        AnonymousClass2OH r2 = this.A00;
        if (r2 != null) {
            r2.A0F(this.A01, 1);
        }
        setAudioModeIsVoip(true);
        setActive();
    }

    @Override // java.lang.Object
    public String toString() {
        return C12960it.A0d(this.A01, C12960it.A0k("SelfManagedConnection: "));
    }
}
