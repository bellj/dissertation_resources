package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.170  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass170 {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C18460sU A02;
    public final C16490p7 A03;
    public final AbstractC14440lR A04;

    public AnonymousClass170(AbstractC15710nm r1, C15570nT r2, C18460sU r3, C16490p7 r4, AbstractC14440lR r5) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
    }

    public void A00(AnonymousClass1JO r11, AbstractC15590nW r12, UserJid userJid, long j) {
        StringBuilder sb = new StringBuilder("participant-device-store/addParticipantDevices/");
        sb.append(r12);
        sb.append(" ");
        sb.append(userJid);
        sb.append(" ");
        sb.append(j);
        sb.append(" ");
        sb.append(r11);
        Log.i(sb.toString());
        AnonymousClass009.A0B("participant-device-store/addParticipantDevices/empty devices", !r11.A00.isEmpty());
        C18460sU r3 = this.A02;
        long A01 = r3.A01(r12);
        C16310on A02 = this.A03.A02();
        try {
            AnonymousClass1Lx A012 = A02.A01();
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT INTO group_participant_device (group_participant_row_id, device_jid_row_id, sent_sender_key) SELECT _id, ?, ? FROM group_participant_user WHERE group_jid_row_id = ? AND user_jid_row_id = ?");
            A0A.A01(3, A01);
            A0A.A01(4, j);
            Iterator it = r11.iterator();
            while (it.hasNext()) {
                AnonymousClass1YP r7 = (AnonymousClass1YP) it.next();
                DeviceJid deviceJid = r7.A01;
                if (deviceJid.getUserJid().equals(userJid)) {
                    A0A.A01(1, r3.A01(deviceJid));
                    A0A.A01(2, r7.A00 ? 1 : 0);
                    A0A.A00.executeInsert();
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("participant-device-store/incorrect device jid ");
                    sb2.append(deviceJid);
                    sb2.append(" for user ");
                    sb2.append(userJid);
                    throw new RuntimeException(sb2.toString());
                }
            }
            A012.A00();
            A012.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(AnonymousClass1JO r10, AbstractC15590nW r11, UserJid userJid, long j) {
        StringBuilder sb = new StringBuilder("participant-device-store/updateParticipantDevices/");
        sb.append(r11);
        sb.append(" ");
        sb.append(userJid);
        sb.append(" ");
        sb.append(j);
        sb.append(" ");
        sb.append(r10);
        Log.i(sb.toString());
        C16490p7 r2 = this.A03;
        C16310on A02 = r2.A02();
        try {
            AnonymousClass1Lx A01 = A02.A01();
            StringBuilder sb2 = new StringBuilder("participant-device-store/deleteParticipantDevices/");
            sb2.append(r11);
            sb2.append(" ");
            sb2.append(j);
            Log.i(sb2.toString());
            long A012 = this.A02.A01(r11);
            C16310on A022 = r2.A02();
            AnonymousClass1YE A0A = A022.A03.A0A("DELETE FROM group_participant_device WHERE group_participant_row_id IN (SELECT _id FROM group_participant_user WHERE group_jid_row_id = ? AND user_jid_row_id = ?)");
            A0A.A00.bindAllArgsAsStrings(new String[]{String.valueOf(A012), String.valueOf(j)});
            A0A.A00();
            A022.close();
            A00(r10, r11, userJid, j);
            A01.A00();
            A01.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(AbstractC15590nW r8) {
        StringBuilder sb = new StringBuilder("participant-device-store/resetSentSenderKeyForAllParticipants/");
        sb.append(r8);
        Log.i(sb.toString());
        long A01 = this.A02.A01(r8);
        C16310on A02 = this.A03.A02();
        try {
            AnonymousClass1YE A0A = A02.A03.A0A("UPDATE group_participant_device SET sent_sender_key = ? WHERE group_participant_row_id IN (SELECT _id FROM group_participant_user WHERE group_jid_row_id = ?)");
            A0A.A00.bindAllArgsAsStrings(new String[]{"0", String.valueOf(A01)});
            A0A.A00();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
