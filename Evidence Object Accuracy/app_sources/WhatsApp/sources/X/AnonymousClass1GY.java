package X;

import java.util.Collection;

/* renamed from: X.1GY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1GY {
    public void A00() {
    }

    public void A01(Collection collection, boolean z) {
        AnonymousClass1GX r0 = (AnonymousClass1GX) this;
        if (z) {
            C15550nR r1 = r0.A00;
            r1.A04.A01.clear();
            AnonymousClass10S r2 = r1.A07;
            AnonymousClass1GB r02 = r2.A00;
            if (r02 != null) {
                C15610nY r12 = r02.A00;
                r12.A08.clear();
                r12.A09.clear();
                for (C27131Gd r03 : r2.A01()) {
                    r03.A06(collection);
                }
                return;
            }
            throw new IllegalStateException("observer not set");
        }
    }
}
