package X;

/* renamed from: X.609  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass609 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public AnonymousClass609(int i, int i2, int i3, int i4) {
        this.A02 = i;
        this.A01 = i2;
        this.A00 = i3;
        this.A03 = i4;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass609.class != obj.getClass()) {
                return false;
            }
            AnonymousClass609 r5 = (AnonymousClass609) obj;
            if (!(this.A02 == r5.A02 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((961 + this.A02) * 31) + this.A01;
    }
}
