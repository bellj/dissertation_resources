package X;

import android.app.Application;
import java.util.Set;

/* renamed from: X.2IG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2IG {
    public final Application A00;
    public final AnonymousClass2IF A01;
    public final Set A02;

    public AnonymousClass2IG(Application application, AnonymousClass2IF r2, Set set) {
        this.A00 = application;
        this.A02 = set;
        this.A01 = r2;
    }
}
