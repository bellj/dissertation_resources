package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.06G  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass06G implements AnonymousClass06H {
    public static final AnonymousClass06G A00 = new AnonymousClass06G();

    @Override // X.AnonymousClass06H
    public int A78(CharSequence charSequence, int i, int i2) {
        int i3 = i2 + 0;
        int i4 = 2;
        for (int i5 = 0; i5 < i3 && i4 == 2; i5++) {
            byte directionality = Character.getDirectionality(charSequence.charAt(i5));
            i4 = 1;
            if (directionality != 0) {
                if (!(directionality == 1 || directionality == 2)) {
                    switch (directionality) {
                        case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        case 15:
                            break;
                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        case 17:
                            break;
                        default:
                            i4 = 2;
                            break;
                    }
                }
                i4 = 0;
            }
        }
        return i4;
    }
}
