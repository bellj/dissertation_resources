package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.SystemClock;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.2ZL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2ZL extends SQLiteOpenHelper {
    public final /* synthetic */ C15120mb A00;

    @Override // android.database.sqlite.SQLiteOpenHelper
    public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2ZL(Context context, C15120mb r5) {
        super(context, "google_analytics_v4.db", (SQLiteDatabase.CursorFactory) null, 1);
        this.A00 = r5;
    }

    public static final Set A00(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet A12 = C12970iu.A12();
        StringBuilder A0t = C12980iv.A0t(str.length() + 22);
        A0t.append("SELECT * FROM ");
        A0t.append(str);
        Cursor rawQuery = sQLiteDatabase.rawQuery(C12960it.A0d(" LIMIT 0", A0t), null);
        try {
            for (String str2 : rawQuery.getColumnNames()) {
                A12.add(str2);
            }
            return A12;
        } finally {
            rawQuery.close();
        }
    }

    private final boolean A01(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor = null;
        try {
            try {
                cursor = sQLiteDatabase.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                boolean moveToFirst = cursor.moveToFirst();
                cursor.close();
                return moveToFirst;
            } catch (SQLiteException e) {
                this.A00.A07(str, e, "Error querying for table");
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public final SQLiteDatabase getWritableDatabase() {
        C15120mb r3 = this.A00;
        C93924ay r4 = r3.A01;
        if (r4.A00(3600000)) {
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException unused) {
                r4.A00 = SystemClock.elapsedRealtime();
                r3.A08("Opening the database failed, dropping the table and recreating it");
                ((C15050mT) r3).A00.A00.getDatabasePath("google_analytics_v4.db").delete();
                try {
                    SQLiteDatabase writableDatabase = super.getWritableDatabase();
                    r4.A00 = 0;
                    return writableDatabase;
                } catch (SQLiteException e) {
                    r3.A0C("Failed to open freshly created database", e);
                    throw e;
                }
            }
        } else {
            throw new SQLiteException("Database open failed");
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String path = sQLiteDatabase.getPath();
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 9) {
                File file = new File(path);
                file.setReadable(false, false);
                file.setWritable(false, false);
                file.setReadable(true, true);
                file.setWritable(true, true);
            }
        } catch (NumberFormatException unused) {
            AnonymousClass3A6.A00("Invalid version number", Build.VERSION.SDK);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    @Override // android.database.sqlite.SQLiteOpenHelper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onOpen(android.database.sqlite.SQLiteDatabase r11) {
        /*
            r10 = this;
            java.lang.String r1 = "hits2"
            boolean r0 = r10.A01(r11, r1)
            r9 = 3
            r8 = 2
            r6 = 4
            r5 = 0
            r7 = 1
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = X.C15120mb.A03
        L_0x000f:
            r11.execSQL(r0)
        L_0x0012:
            java.lang.String r1 = "properties"
            boolean r0 = r10.A01(r11, r1)
            if (r0 == 0) goto L_0x00ac
            java.util.Set r4 = A00(r11, r1)
            r3 = 6
            java.lang.String[] r2 = new java.lang.String[r3]
            java.lang.String r0 = "app_uid"
            r2[r5] = r0
            java.lang.String r0 = "cid"
            r2[r7] = r0
            java.lang.String r0 = "tid"
            r2[r8] = r0
            java.lang.String r0 = "params"
            r2[r9] = r0
            java.lang.String r0 = "adid"
            r2[r6] = r0
            r1 = 5
            java.lang.String r0 = "hits_count"
            r2[r1] = r0
        L_0x003b:
            r1 = r2[r5]
            boolean r0 = r4.remove(r1)
            if (r0 != 0) goto L_0x0053
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r0 = "Database properties is missing required column: "
            java.lang.String r1 = X.C12960it.A0c(r1, r0)
            android.database.sqlite.SQLiteException r0 = new android.database.sqlite.SQLiteException
            r0.<init>(r1)
            throw r0
        L_0x0053:
            int r5 = r5 + 1
            if (r5 < r3) goto L_0x003b
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x00a4
            return
        L_0x005e:
            java.util.Set r4 = A00(r11, r1)
            java.lang.String[] r3 = new java.lang.String[r6]
            java.lang.String r0 = "hit_id"
            r3[r5] = r0
            java.lang.String r0 = "hit_string"
            r3[r7] = r0
            java.lang.String r0 = "hit_time"
            r3[r8] = r0
            java.lang.String r0 = "hit_url"
            r3[r9] = r0
            r2 = 0
        L_0x0075:
            r1 = r3[r2]
            boolean r0 = r4.remove(r1)
            if (r0 != 0) goto L_0x008d
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r0 = "Database hits2 is missing required column: "
            java.lang.String r1 = X.C12960it.A0c(r1, r0)
            android.database.sqlite.SQLiteException r0 = new android.database.sqlite.SQLiteException
            r0.<init>(r1)
            throw r0
        L_0x008d:
            int r2 = r2 + 1
            if (r2 < r6) goto L_0x0075
            java.lang.String r0 = "hit_app_id"
            boolean r1 = r4.remove(r0)
            r1 = r1 ^ r7
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x00b2
            if (r1 == 0) goto L_0x0012
            java.lang.String r0 = "ALTER TABLE hits2 ADD COLUMN hit_app_id INTEGER"
            goto L_0x000f
        L_0x00a4:
            java.lang.String r1 = "Database properties table has extra columns"
            android.database.sqlite.SQLiteException r0 = new android.database.sqlite.SQLiteException
            r0.<init>(r1)
            throw r0
        L_0x00ac:
            java.lang.String r0 = "CREATE TABLE IF NOT EXISTS properties ( app_uid INTEGER NOT NULL, cid TEXT NOT NULL, tid TEXT NOT NULL, params TEXT NOT NULL, adid INTEGER NOT NULL, hits_count INTEGER NOT NULL, PRIMARY KEY (app_uid, cid, tid)) ;"
            r11.execSQL(r0)
            return
        L_0x00b2:
            java.lang.String r1 = "Database hits2 has extra columns"
            android.database.sqlite.SQLiteException r0 = new android.database.sqlite.SQLiteException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2ZL.onOpen(android.database.sqlite.SQLiteDatabase):void");
    }
}
