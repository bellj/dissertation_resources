package X;

import android.content.Context;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;

/* renamed from: X.5m3  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5m3 extends AbstractC118835cS {
    public final Context A00;
    public final View A01;
    public final ImageView A02;
    public final ProgressBar A03;
    public final RelativeLayout A04;
    public final TextView A05;
    public final TextView A06;
    public final TextView A07;
    public final TextEmojiLabel A08;
    public final WaImageView A09;
    public final C252818u A0A;
    public final AnonymousClass130 A0B;
    public final C21270x9 A0C;
    public final AnonymousClass01d A0D;
    public final C253118x A0E;

    public AnonymousClass5m3(View view, C252818u r4, AnonymousClass130 r5, C21270x9 r6, AnonymousClass01d r7, C253118x r8) {
        super(view);
        this.A0C = r6;
        this.A0B = r5;
        this.A0E = r8;
        this.A0A = r4;
        this.A0D = r7;
        this.A00 = view.getContext();
        this.A06 = C12960it.A0I(view, R.id.payment_send_action);
        this.A07 = C12960it.A0I(view, R.id.payment_send_action_time);
        this.A05 = C12960it.A0I(view, R.id.payment_people_info);
        this.A04 = (RelativeLayout) AnonymousClass028.A0D(view, R.id.payment_people_container);
        this.A02 = C12970iu.A0K(view, R.id.payment_people_icon);
        this.A03 = (ProgressBar) AnonymousClass028.A0D(view, R.id.payment_people_progress_bar);
        View A0D = AnonymousClass028.A0D(view, R.id.incentive_info_container);
        this.A01 = A0D;
        this.A08 = C12970iu.A0T(A0D, R.id.incentive_info_text);
        this.A09 = C12980iv.A0X(view, R.id.open_indicator);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r12, int i) {
        ImageView imageView;
        C123315mx r122 = (C123315mx) r12;
        if (!TextUtils.isEmpty(r122.A09)) {
            this.A06.setText(r122.A09);
            this.A05.setText(r122.A08);
            if (!TextUtils.isEmpty(r122.A0A)) {
                this.A07.setText(r122.A0A);
            }
        } else {
            this.A04.setVisibility(8);
        }
        if (r122.A05 != null) {
            AnonymousClass1J1 A04 = this.A0C.A04(this.A00, "payment-transaction-payee-payer-detail");
            C15370n3 r0 = r122.A05;
            imageView = this.A02;
            A04.A06(imageView, r0);
        } else {
            AnonymousClass130 r1 = this.A0B;
            imageView = this.A02;
            r1.A05(imageView, r122.A00);
        }
        View.OnClickListener onClickListener = r122.A04;
        if (onClickListener != null) {
            this.A04.setOnClickListener(onClickListener);
            this.A09.setVisibility(0);
        } else {
            this.A09.setVisibility(4);
        }
        imageView.setVisibility(r122.A01);
        this.A03.setVisibility(r122.A02);
        if (!TextUtils.isEmpty(r122.A07) && !TextUtils.isEmpty(r122.A06)) {
            String[] strArr = new String[1];
            C117295Zj.A1A(this.A0A, r122.A06, strArr, 0);
            AbstractC28491Nn.A05(this.A08, this.A0D, this.A0E.A01(this.A00, r122.A07, new Runnable[]{new Runnable() { // from class: X.6FC
                @Override // java.lang.Runnable
                public final void run() {
                }
            }}, new String[]{"incentive-blurb-cashback-help"}, strArr));
        } else if (TextUtils.isEmpty(r122.A07) || r122.A03 == null) {
            this.A01.setVisibility(8);
            return;
        } else {
            Spanned fromHtml = Html.fromHtml(r122.A07);
            String obj = fromHtml.toString();
            SpannableString spannableString = new SpannableString(obj);
            Object[] spans = fromHtml.getSpans(0, obj.length(), Object.class);
            for (Object obj2 : spans) {
                spannableString.setSpan(new C117525a6(this, r122), fromHtml.getSpanStart(obj2), fromHtml.getSpanEnd(obj2), 33);
            }
            TextEmojiLabel textEmojiLabel = this.A08;
            C12990iw.A1F(textEmojiLabel);
            textEmojiLabel.setText(spannableString);
        }
        this.A01.setVisibility(0);
    }
}
