package X;

import android.graphics.BitmapFactory;

/* renamed from: X.1tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41591tm {
    public final int A00;
    public final int A01;
    public final BitmapFactory.Options A02;
    public final Long A03;
    public final boolean A04;

    public C41591tm(int i, int i2) {
        this(null, null, i, i2, false);
    }

    public C41591tm(BitmapFactory.Options options, Long l, int i, int i2, boolean z) {
        boolean z2;
        if (i < 2 || i2 < 2 || ((l != null && l.longValue() <= 0) || (z && i != i2))) {
            z2 = false;
        } else {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("bitmaputils/bitmapspec/wrong/ ");
        sb.append(i);
        sb.append(",");
        sb.append(i2);
        sb.append(",");
        sb.append(l);
        sb.append(",");
        sb.append(z);
        AnonymousClass009.A0A(sb.toString(), z2);
        if (!z2) {
            this.A01 = Integer.MAX_VALUE;
            this.A00 = Integer.MAX_VALUE;
            this.A03 = null;
            this.A04 = false;
        } else {
            this.A01 = i;
            this.A00 = i2;
            this.A03 = l;
            this.A04 = z;
        }
        this.A02 = options == null ? new BitmapFactory.Options() : options;
    }
}
