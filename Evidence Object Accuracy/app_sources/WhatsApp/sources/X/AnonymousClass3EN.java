package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;

/* renamed from: X.3EN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EN {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EM A01;
    public final AnonymousClass3E7 A02;

    public AnonymousClass3EN(AbstractC15710nm r3, AnonymousClass1V8 r4, AnonymousClass3BI r5) {
        Object obj;
        AnonymousClass1V8.A01(r4, "iq");
        this.A01 = (AnonymousClass3EM) AnonymousClass3JT.A05(r4, new AbstractC116095Uc(r3, r5.A00) { // from class: X.58x
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r42) {
                return new AnonymousClass3EM(this.A00, r42, this.A01);
            }
        }, new String[0]);
        try {
            obj = new IDxNFunctionShape17S0100000_2_I1(r3, 34).A63(r4);
        } catch (AnonymousClass1V9 unused) {
            obj = null;
        }
        this.A02 = (AnonymousClass3E7) obj;
        this.A00 = r4;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EN.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EN r5 = (AnonymousClass3EN) obj;
            if (!this.A01.equals(r5.A01) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A02, A1a);
    }
}
