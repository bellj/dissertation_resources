package X;

import android.graphics.RectF;

/* renamed from: X.45J  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass45J extends AbstractC454821u {
    public abstract float A0R();

    @Override // X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        float A0R = A0R();
        if (A0R != 0.0f) {
            float f5 = f3 - f;
            float f6 = f4 - f2;
            if (f5 / f6 < A0R) {
                f6 = f5 / A0R;
            } else {
                f5 = f6 * A0R;
            }
            float f7 = (f + f3) / 2.0f;
            float f8 = (f2 + f4) / 2.0f;
            float f9 = f5 / 2.0f;
            float f10 = f6 / 2.0f;
            super.A0Q(rectF, f7 - f9, f8 - f10, f7 + f9, f8 + f10);
            return;
        }
        super.A0Q(rectF, f, f2, f3, f4);
    }
}
