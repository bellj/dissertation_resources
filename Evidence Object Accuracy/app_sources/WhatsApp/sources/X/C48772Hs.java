package X;

import com.whatsapp.voipcalling.camera.VoipCameraManager;

/* renamed from: X.2Hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48772Hs implements AnonymousClass01N {
    public final int A00;
    public final C48722Hj A01;
    public final AnonymousClass01J A02;

    public C48772Hs(C48722Hj r1, AnonymousClass01J r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        int i = this.A00;
        if (i == 0) {
            return new C48802Hv();
        }
        if (i == 1) {
            AnonymousClass01J r1 = this.A02;
            return new C48792Hu(AbstractC18030rn.A00(r1.AO3), (C14850m9) r1.A04.get());
        } else if (i == 2) {
            AnonymousClass01J r12 = this.A02;
            return new C48782Ht((C14830m7) r12.ALb.get(), (C15890o4) r12.AN1.get(), (C14850m9) r12.A04.get(), (VoipCameraManager) r12.AMV.get());
        } else {
            throw new AssertionError(i);
        }
    }
}
