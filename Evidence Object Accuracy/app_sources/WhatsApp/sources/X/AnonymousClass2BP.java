package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2BP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BP extends AnonymousClass02M {
    public int A00 = 2;
    public List A01;
    public boolean A02 = false;
    public final Context A03;
    public final LayoutInflater A04;
    public final AnonymousClass2TT A05;
    public final C15610nY A06;
    public final AnonymousClass1J1 A07;
    public final AnonymousClass018 A08;
    public final AnonymousClass12F A09;

    public AnonymousClass2BP(Context context, AnonymousClass2TT r4, C15610nY r5, AnonymousClass1J1 r6, AnonymousClass018 r7, AnonymousClass12F r8) {
        this.A03 = context;
        this.A04 = LayoutInflater.from(context);
        this.A06 = r5;
        this.A08 = r7;
        this.A05 = r4;
        this.A09 = r8;
        this.A07 = r6;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        List list = this.A01;
        if (list == null) {
            return 0;
        }
        int size = list.size();
        int i = this.A00;
        if (size <= i || this.A02) {
            return size;
        }
        return i + 1;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r11, int i) {
        int i2;
        int size;
        C55012he r112 = (C55012he) r11;
        if (this.A02 || i != (i2 = this.A00)) {
            List list = this.A01;
            if (list != null) {
                C90274Ng r6 = (C90274Ng) list.get(i);
                C15370n3 r5 = r6.A00;
                C28801Pb r2 = r112.A05;
                r2.A06(r5);
                r2.A04(AnonymousClass00T.A00(this.A03, R.color.list_item_title));
                ImageView imageView = r112.A01;
                StringBuilder sb = new StringBuilder();
                sb.append(this.A05.A00(R.string.transition_avatar));
                sb.append(C15380n4.A03(r5.A0D));
                AnonymousClass028.A0k(imageView, sb.toString());
                AnonymousClass1J1 r3 = this.A07;
                r3.A06(imageView, r5);
                C15610nY r1 = this.A06;
                if (r1.A0L(r5, -1) && r5.A0U != null) {
                    TextEmojiLabel textEmojiLabel = r112.A03;
                    textEmojiLabel.setVisibility(0);
                    textEmojiLabel.A0G(null, r1.A09(r5));
                }
                if (r5.A0R != null) {
                    TextEmojiLabel textEmojiLabel2 = r112.A04;
                    textEmojiLabel2.setVisibility(0);
                    textEmojiLabel2.A0G(null, r5.A0R);
                } else {
                    r112.A04.setVisibility(8);
                }
                r112.A0H.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(this, r5, r6, 10));
                r3.A06(imageView, r5);
                return;
            }
            return;
        }
        List list2 = this.A01;
        if (list2 == null) {
            size = 0;
        } else {
            size = list2.size();
        }
        int i3 = size - i2;
        C28801Pb r62 = r112.A05;
        r62.A08(this.A08.A0I(new Object[]{Integer.valueOf(i3)}, R.plurals.n_more, (long) i3));
        r62.A04(AnonymousClass00T.A00(this.A03, R.color.list_item_sub_title));
        r112.A04.setVisibility(8);
        r112.A01.setImageResource(R.drawable.ic_more_participants);
        r112.A0H.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 23));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C55012he(this.A04.inflate(R.layout.group_invite_row, viewGroup, false), this.A06, this.A09);
    }
}
