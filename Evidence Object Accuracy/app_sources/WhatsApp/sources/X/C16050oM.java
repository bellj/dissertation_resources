package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0oM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16050oM {
    public static int A00(byte[] bArr) {
        return (bArr[3] & 255) | ((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8);
    }

    public static int A01(byte[] bArr) {
        return (bArr[2] & 255) | ((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8);
    }

    public static void A02(byte[] bArr, int i, long j) {
        bArr[i + 7] = (byte) ((int) j);
        bArr[i + 6] = (byte) ((int) (j >> 8));
        bArr[i + 5] = (byte) ((int) (j >> 16));
        bArr[i + 4] = (byte) ((int) (j >> 24));
        bArr[i + 3] = (byte) ((int) (j >> 32));
        bArr[i + 2] = (byte) ((int) (j >> 40));
        bArr[i + 1] = (byte) ((int) (j >> 48));
        bArr[i] = (byte) ((int) (j >> 56));
    }

    public static byte[] A03(int i) {
        return new byte[]{(byte) (i >> 24), (byte) (i >> 16), (byte) (i >> 8), (byte) i};
    }

    public static byte[] A04(Collection collection) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                byteArrayOutputStream.write((byte[]) it.next());
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] A05(byte[]... bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (byte[] bArr2 : bArr) {
                byteArrayOutputStream.write(bArr2);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[][] A06(byte[] bArr, int i, int i2) {
        System.arraycopy(bArr, 0, r3[0], 0, i);
        byte[][] bArr2 = {new byte[i], new byte[i2]};
        System.arraycopy(bArr, i, bArr2[1], 0, i2);
        return bArr2;
    }

    public static byte[][] A07(byte[] bArr, int i, int i2, int i3) {
        if (i2 >= 0 && i3 >= 0) {
            int i4 = i + i2;
            if (bArr.length >= i4 + i3) {
                System.arraycopy(bArr, 0, r2[0], 0, i);
                System.arraycopy(bArr, i, r2[1], 0, i2);
                byte[][] bArr2 = {new byte[i], new byte[i2], new byte[i3]};
                System.arraycopy(bArr, i4, bArr2[2], 0, i3);
                return bArr2;
            }
        }
        StringBuilder sb = new StringBuilder("Input too small: ");
        sb.append(AnonymousClass4ZN.A00(bArr));
        throw new ParseException(sb.toString(), 0);
    }
}
