package X;

import android.util.Base64;

/* renamed from: X.0mD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14890mD {
    public AnonymousClass1UP A00;
    public final C16630pM A01;

    public C14890mD(C16630pM r1) {
        this.A01 = r1;
    }

    public synchronized AnonymousClass1UP A00() {
        AnonymousClass1UP r1;
        r1 = this.A00;
        if (r1 == null) {
            r1 = new AnonymousClass1UP(this.A01);
            this.A00 = r1;
        }
        return r1;
    }

    public void A01(String str, boolean z) {
        AnonymousClass1UP A00 = A00();
        byte[] decode = Base64.decode(str, 0);
        A00.A07 = decode;
        if (decode.length == 64) {
            byte[] bArr = new byte[32];
            A00.A05 = bArr;
            A00.A06 = new byte[32];
            System.arraycopy(decode, 0, bArr, 0, 32);
            System.arraycopy(A00.A07, 32, A00.A06, 0, 32);
        }
        if (z) {
            A00.A08.edit().putString("key", str).commit();
        }
    }

    public boolean A02() {
        return A00().A03 != null;
    }

    public boolean A03(String str) {
        String str2 = A00().A00;
        return str2 != null && str2.equals(str);
    }
}
