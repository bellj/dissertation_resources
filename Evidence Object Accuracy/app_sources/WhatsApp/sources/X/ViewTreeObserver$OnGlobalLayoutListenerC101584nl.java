package X;

import android.view.ViewTreeObserver;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.4nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101584nl implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101584nl(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A00 = groupAdminPickerActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        GroupAdminPickerActivity groupAdminPickerActivity = this.A00;
        C12980iv.A1F(groupAdminPickerActivity.A02, this);
        groupAdminPickerActivity.A06.A0M(3);
    }
}
