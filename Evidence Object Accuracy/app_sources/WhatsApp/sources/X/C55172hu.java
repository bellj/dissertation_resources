package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.ContactStatusThumbnail;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* renamed from: X.2hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55172hu extends AnonymousClass03U {
    public int A00;
    public UserJid A01;
    public final Context A02;
    public final Resources A03;
    public final FrameLayout A04;
    public final ImageView A05;
    public final ImageView A06;
    public final ImageView A07;
    public final TextView A08;
    public final AnonymousClass01J A09;
    public final C28801Pb A0A;
    public final AnonymousClass1J1 A0B;
    public final C70433bH A0C;
    public final AbstractC93424a9 A0D = new AnonymousClass48N();
    public final boolean A0E;

    public C55172hu(Context context, View view, AnonymousClass01J r8, AnonymousClass1J1 r9) {
        super(view);
        this.A02 = context;
        this.A03 = context.getResources();
        this.A09 = r8;
        this.A0C = new C70433bH(context);
        this.A0B = r9;
        boolean A07 = r8.A3L().A07(1533);
        this.A0E = r8.A3L().A07(1875);
        Boolean valueOf = Boolean.valueOf(A07);
        ImageView A0K = C12970iu.A0K(view, R.id.contact_photo);
        ImageView A0K2 = C12970iu.A0K(view, R.id.wdsProfilePicture);
        if (valueOf.booleanValue()) {
            A0K.setVisibility(8);
            A0K2.setVisibility(0);
        } else {
            A0K.setVisibility(0);
            A0K2.setVisibility(8);
            A0K2 = A0K;
        }
        this.A07 = A0K2;
        A0K2.setClickable(false);
        AnonymousClass028.A0D(view, R.id.contact_selector).setClickable(false);
        C28801Pb r1 = new C28801Pb(view, C12960it.A0P(r8), C12990iw.A0f(r8), (int) R.id.contact_name);
        this.A0A = r1;
        this.A08 = C12960it.A0I(view, R.id.date_time);
        this.A04 = (FrameLayout) AnonymousClass028.A0D(view, R.id.action);
        this.A05 = C12970iu.A0K(view, R.id.action_icon);
        this.A06 = C12970iu.A0K(view, R.id.contact_mark);
        C27531Hw.A06(r1.A01);
    }

    public final void A08(int i, int i2) {
        EnumC87284Ax r0;
        ImageView imageView = this.A07;
        if (imageView instanceof ContactStatusThumbnail) {
            ContactStatusThumbnail contactStatusThumbnail = (ContactStatusThumbnail) imageView;
            contactStatusThumbnail.A06.clear();
            contactStatusThumbnail.A03(i, i2);
        } else if (imageView instanceof WDSProfilePhoto) {
            WDSProfilePhoto wDSProfilePhoto = (WDSProfilePhoto) imageView;
            if (i > 0) {
                r0 = EnumC87284Ax.A01;
            } else {
                r0 = EnumC87284Ax.A02;
            }
            wDSProfilePhoto.setStatusIndicatorState(r0);
            wDSProfilePhoto.setStatusIndicatorEnabled(C12960it.A1U(i2));
        }
    }
}
