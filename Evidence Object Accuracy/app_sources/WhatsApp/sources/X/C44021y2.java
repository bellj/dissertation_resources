package X;

/* renamed from: X.1y2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44021y2 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Double A02;
    public Double A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Long A09;
    public Long A0A;
    public String A0B;

    public C44021y2() {
        super(2054, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(15, this.A00);
        r3.Abe(4, this.A04);
        r3.Abe(9, this.A05);
        r3.Abe(8, this.A06);
        r3.Abe(1, this.A09);
        r3.Abe(16, this.A0B);
        r3.Abe(2, this.A02);
        r3.Abe(11, this.A01);
        r3.Abe(14, this.A0A);
        r3.Abe(5, this.A07);
        r3.Abe(7, this.A03);
        r3.Abe(6, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        StringBuilder sb = new StringBuilder("WamAndroidRegDirectMigrationFlow {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "didReceiveRcFromConsumer", this.A00);
        String str = null;
        Integer num = this.A04;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "firstMigrationFailureReason", str);
        Integer num2 = this.A05;
        if (num2 == null) {
            obj = null;
        } else {
            obj = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrateMediaResult", obj);
        Integer num3 = this.A06;
        if (num3 == null) {
            obj2 = null;
        } else {
            obj2 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migratePhoneNumberScreenAction", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationDurationT", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationSessionId", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationTotalSize", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "otherFilesMigrationFailed", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "providerAppVersionCode", this.A0A);
        Integer num4 = this.A07;
        if (num4 == null) {
            obj3 = null;
        } else {
            obj3 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "secondMigrationFailureReason", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "spacePredictedToNeed", this.A03);
        Integer num5 = this.A08;
        if (num5 == null) {
            obj4 = null;
        } else {
            obj4 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "thirdMigrationFailureReason", obj4);
        sb.append("}");
        return sb.toString();
    }
}
