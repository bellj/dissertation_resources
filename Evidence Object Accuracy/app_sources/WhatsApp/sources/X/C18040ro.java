package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.AlarmService;

/* renamed from: X.0ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18040ro implements AbstractC16890pv {
    public final Context A00;

    public C18040ro(Context context) {
        this.A00 = context;
    }

    @Override // X.AbstractC16890pv
    public void AMJ() {
        Context context = this.A00;
        AbstractServiceC003801r.A00(context, new Intent("com.whatsapp.action.SETUP", null, context, AlarmService.class), AlarmService.class, 3);
    }
}
