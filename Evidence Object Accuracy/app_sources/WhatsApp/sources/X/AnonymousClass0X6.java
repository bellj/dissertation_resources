package X;

import android.widget.PopupWindow;

/* renamed from: X.0X6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X6 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ AnonymousClass07N A00;

    public AnonymousClass0X6(AnonymousClass07N r1) {
        this.A00 = r1;
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        AnonymousClass07N r1 = this.A00;
        AbstractC11640gc r0 = r1.A00;
        if (r0 != null) {
            r0.APH(r1);
        }
    }
}
