package X;

import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.payments.ui.IndiaUpiQrCodeScannedDialogFragment;

/* renamed from: X.5bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118425bn extends AnonymousClass0Yo {
    public final /* synthetic */ IndiaUpiQrCodeScannedDialogFragment A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public C118425bn(IndiaUpiQrCodeScannedDialogFragment indiaUpiQrCodeScannedDialogFragment, String str, String str2) {
        this.A00 = indiaUpiQrCodeScannedDialogFragment;
        this.A02 = str;
        this.A01 = str2;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117995b6.class)) {
            IndiaUpiQrCodeScannedDialogFragment indiaUpiQrCodeScannedDialogFragment = this.A00;
            C16590pI r14 = indiaUpiQrCodeScannedDialogFragment.A0D;
            C117995b6 r12 = new C117995b6(indiaUpiQrCodeScannedDialogFragment.A0B, r14, indiaUpiQrCodeScannedDialogFragment.A0E, indiaUpiQrCodeScannedDialogFragment.A0H, indiaUpiQrCodeScannedDialogFragment.A0I, indiaUpiQrCodeScannedDialogFragment.A0J);
            IDxObserverShape5S0100000_3_I1 A0B = C117305Zk.A0B(this, 56);
            IDxObserverShape5S0100000_3_I1 A0B2 = C117305Zk.A0B(this, 58);
            IDxObserverShape5S0100000_3_I1 A0B3 = C117305Zk.A0B(this, 54);
            IDxObserverShape5S0100000_3_I1 A0B4 = C117305Zk.A0B(this, 53);
            IDxObserverShape5S0100000_3_I1 A0B5 = C117305Zk.A0B(this, 57);
            IDxObserverShape5S0100000_3_I1 A0B6 = C117305Zk.A0B(this, 59);
            IDxObserverShape5S0100000_3_I1 A0B7 = C117305Zk.A0B(this, 55);
            IDxObserverShape5S0100000_3_I1 A0B8 = C117305Zk.A0B(this, 52);
            IDxObserverShape5S0100000_3_I1 A0B9 = C117305Zk.A0B(this, 60);
            r12.A02.A05(indiaUpiQrCodeScannedDialogFragment, A0B);
            r12.A05.A05(indiaUpiQrCodeScannedDialogFragment, A0B2);
            r12.A09.A05(indiaUpiQrCodeScannedDialogFragment, A0B3);
            r12.A08.A05(indiaUpiQrCodeScannedDialogFragment, A0B4);
            r12.A01.A05(indiaUpiQrCodeScannedDialogFragment, A0B5);
            r12.A00.A05(indiaUpiQrCodeScannedDialogFragment, A0B6);
            r12.A03.A05(indiaUpiQrCodeScannedDialogFragment, A0B7);
            r12.A07.A05(indiaUpiQrCodeScannedDialogFragment, A0B8);
            r12.A04.A05(indiaUpiQrCodeScannedDialogFragment, A0B9);
            r12.A0A.A05(indiaUpiQrCodeScannedDialogFragment, C117305Zk.A0B(this, 51));
            r12.A04(this.A02, this.A01);
            return r12;
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
