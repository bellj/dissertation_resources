package X;

import com.whatsapp.util.Log;
import java.io.DataOutputStream;
import java.io.IOException;

/* renamed from: X.0zR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22660zR extends AbstractC16230of {
    public boolean A00;
    public final C16590pI A01;

    public C22660zR(C16590pI r1, AnonymousClass01H r2) {
        super(r2);
        this.A01 = r1;
    }

    public void A05(boolean z) {
        if (this.A00 != z) {
            this.A00 = z;
            try {
                DataOutputStream dataOutputStream = new DataOutputStream(this.A01.A00.openFileOutput("login_failed", 0));
                dataOutputStream.writeBoolean(this.A00);
                dataOutputStream.close();
            } catch (IOException e) {
                Log.w("loginmanager/failed/save login_failed", e);
            }
            for (AbstractC18630sl r0 : A01()) {
                r0.ASI(z);
            }
        }
    }
}
