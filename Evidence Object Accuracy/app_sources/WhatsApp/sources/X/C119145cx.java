package X;

import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.5cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119145cx extends AnonymousClass2UH {
    public final /* synthetic */ BottomSheetBehavior A00;
    public final /* synthetic */ PinBottomSheetDialogFragment A01;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C119145cx(BottomSheetBehavior bottomSheetBehavior, PinBottomSheetDialogFragment pinBottomSheetDialogFragment) {
        this.A01 = pinBottomSheetDialogFragment;
        this.A00 = bottomSheetBehavior;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 1) {
            this.A00.A0M(3);
        }
    }
}
