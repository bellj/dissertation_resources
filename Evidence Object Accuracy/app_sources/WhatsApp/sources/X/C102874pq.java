package X;

import android.content.Context;
import com.whatsapp.calling.callhistory.CallLogActivity;

/* renamed from: X.4pq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102874pq implements AbstractC009204q {
    public final /* synthetic */ CallLogActivity A00;

    public C102874pq(CallLogActivity callLogActivity) {
        this.A00 = callLogActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
