package X;

import android.media.MediaCodec;

/* renamed from: X.4Zy */
/* loaded from: classes3.dex */
public final class C93314Zy {
    public final MediaCodec.CryptoInfo.Pattern A00 = new MediaCodec.CryptoInfo.Pattern(0, 0);
    public final MediaCodec.CryptoInfo A01;

    public /* synthetic */ C93314Zy(MediaCodec.CryptoInfo cryptoInfo) {
        this.A01 = cryptoInfo;
    }

    public static /* synthetic */ void A00(C93314Zy r2, int i, int i2) {
        MediaCodec.CryptoInfo.Pattern pattern = r2.A00;
        pattern.set(i, i2);
        r2.A01.setPattern(pattern);
    }
}
