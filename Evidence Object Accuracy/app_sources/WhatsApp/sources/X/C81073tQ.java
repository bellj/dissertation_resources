package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3tQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81073tQ extends AbstractC80953tE implements AnonymousClass5XH {
    public static final long serialVersionUID = 0;
    public final transient AbstractC17940re emptySet = emptySet(null);

    public C81073tQ(AbstractC17190qP r2, int i, Comparator comparator) {
        super(r2, i);
    }

    public static C81053tO builder() {
        return new C81053tO();
    }

    public static C81073tQ copyOf(AnonymousClass5XH r1) {
        return copyOf(r1, null);
    }

    public static C81073tQ copyOf(AnonymousClass5XH r2, Comparator comparator) {
        if (r2.isEmpty()) {
            return of();
        }
        if (r2 instanceof C81073tQ) {
            return (C81073tQ) r2;
        }
        return fromMapEntries(r2.asMap().entrySet(), null);
    }

    public static AbstractC17940re emptySet(Comparator comparator) {
        if (comparator == null) {
            return AbstractC17940re.of();
        }
        return AbstractC81103tT.emptySet(comparator);
    }

    public static C81073tQ fromMapEntries(Collection collection, Comparator comparator) {
        if (collection.isEmpty()) {
            return of();
        }
        C28241Mh r5 = new C28241Mh(collection.size());
        int i = 0;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry A15 = C12970iu.A15(it);
            Object key = A15.getKey();
            AbstractC17940re valueSet = valueSet(null, (Collection) A15.getValue());
            if (!valueSet.isEmpty()) {
                r5.put(key, valueSet);
                i += valueSet.size();
            }
        }
        return new C81073tQ(r5.build(), i, null);
    }

    public AbstractC17940re get(Object obj) {
        Object obj2 = this.map.get(obj);
        AbstractC17940re r0 = this.emptySet;
        if (obj2 == null) {
            if (r0 != null) {
                obj2 = r0;
            } else {
                throw C12980iv.A0n("Both parameters are null");
            }
        }
        return (AbstractC17940re) obj2;
    }

    public static C81073tQ of() {
        return C81123tV.INSTANCE;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        Comparator comparator = (Comparator) objectInputStream.readObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            C28241Mh builder = AbstractC17190qP.builder();
            int i = 0;
            for (int i2 = 0; i2 < readInt; i2++) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    C17960rg valuesBuilder = valuesBuilder(comparator);
                    int i3 = 0;
                    do {
                        valuesBuilder.add(objectInputStream.readObject());
                        i3++;
                    } while (i3 < readInt2);
                    AbstractC17940re build = valuesBuilder.build();
                    if (build.size() == readInt2) {
                        builder.put(readObject, build);
                        i += readInt2;
                    } else {
                        String valueOf = String.valueOf(readObject);
                        StringBuilder A0t = C12980iv.A0t(valueOf.length() + 40);
                        A0t.append("Duplicate key-value pairs exist for key ");
                        throw new InvalidObjectException(C12960it.A0d(valueOf, A0t));
                    }
                } else {
                    throw new InvalidObjectException(C12960it.A0e("Invalid value count ", C12980iv.A0t(31), readInt2));
                }
            }
            try {
                AnonymousClass4H7.MAP_FIELD_SETTER.set(this, builder.build());
                AnonymousClass4H7.SIZE_FIELD_SETTER.set(this, i);
                C88434Fq.EMPTY_SET_FIELD_SETTER.set(this, emptySet(comparator));
            } catch (IllegalArgumentException e) {
                throw new InvalidObjectException(e.getMessage()).initCause(e);
            }
        } else {
            throw new InvalidObjectException(C12960it.A0e("Invalid key count ", C12980iv.A0t(29), readInt));
        }
    }

    public Comparator valueComparator() {
        AbstractC17940re r1 = this.emptySet;
        if (r1 instanceof AbstractC81103tT) {
            return ((AbstractC81103tT) r1).comparator();
        }
        return null;
    }

    public static AbstractC17940re valueSet(Comparator comparator, Collection collection) {
        return AbstractC17940re.copyOf(collection);
    }

    public static C17960rg valuesBuilder(Comparator comparator) {
        if (comparator == null) {
            return new C17960rg();
        }
        return new C81083tR(comparator);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(valueComparator());
        C95324dW.writeMultimap(this, objectOutputStream);
    }
}
