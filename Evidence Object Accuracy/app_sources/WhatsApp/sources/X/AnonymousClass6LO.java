package X;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/* renamed from: X.6LO  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6LO extends DefaultHandler {
    public static String A00;
    public static C127545uf A01;
    public static final List A02 = C12960it.A0l();

    @Override // java.lang.Object
    public void finalize() {
    }

    public AnonymousClass6LO(String str) {
        try {
            SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new StringReader(str)), this);
        } catch (IOException | ParserConfigurationException | SAXException unused) {
            throw new C124795q8(EnumC124485pc.A02);
        }
    }

    @Override // org.xml.sax.helpers.DefaultHandler, org.xml.sax.ContentHandler
    public void characters(char[] cArr, int i, int i2) {
        A00 = String.copyValueOf(cArr, i, i2).trim();
    }

    @Override // org.xml.sax.helpers.DefaultHandler, org.xml.sax.ContentHandler
    public void endElement(String str, String str2, String str3) {
        if (str3.equals("key")) {
            A02.add(A01);
        } else if (str3.equals("keyValue")) {
            A01.A02 = A00;
        }
    }

    @Override // org.xml.sax.helpers.DefaultHandler, org.xml.sax.ContentHandler
    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str3.equals("key")) {
            C127545uf r1 = new C127545uf();
            A01 = r1;
            r1.A01 = attributes.getValue("ki");
            A01.A00 = attributes.getValue("owner");
        }
    }
}
