package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/* renamed from: X.0IH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IH extends AnonymousClass0eL {
    public final /* synthetic */ AnonymousClass0I8 A00;

    public AnonymousClass0IH(AnonymousClass0I8 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        AnonymousClass0I8 r2 = this.A00;
        String str = r2.A05;
        Bitmap decodeFile = BitmapFactory.decodeFile(str);
        if (decodeFile == null) {
            FileOutputStream fileOutputStream = null;
            try {
                try {
                    InputStream openStream = new URL(r2.A06).openStream();
                    try {
                        fileOutputStream = ((AnonymousClass03S) r2).A08.openFileOutput("copyright_logo", 0);
                        byte[] bArr = new byte[EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH];
                        while (true) {
                            int read = openStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        decodeFile = BitmapFactory.decodeFile(str);
                        try {
                            openStream.close();
                        } catch (IOException unused) {
                        }
                        fileOutputStream.close();
                    } catch (IOException unused2) {
                        if (openStream != null) {
                            try {
                                openStream.close();
                            } catch (IOException unused3) {
                            }
                        }
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                        }
                    } catch (Throwable th) {
                        if (openStream != null) {
                            try {
                                openStream.close();
                            } catch (IOException unused4) {
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                                throw th;
                            } catch (IOException unused5) {
                                throw th;
                            }
                        } else {
                            throw th;
                        }
                    }
                } catch (IOException unused6) {
                }
            } catch (Throwable th2) {
                throw th2;
            }
        }
        AnonymousClass0UE.A02.post(new AnonymousClass0IN(decodeFile, this));
    }
}
