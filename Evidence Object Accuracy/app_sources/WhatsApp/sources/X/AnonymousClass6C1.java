package X;

/* renamed from: X.6C1  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6C1 implements AbstractC136296Lz {
    public final /* synthetic */ AnonymousClass6M0 A00;
    public final /* synthetic */ AnonymousClass605 A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ AnonymousClass6C1(AnonymousClass6M0 r1, AnonymousClass605 r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // X.AbstractC136296Lz
    public final void AVF(C128545wH r6) {
        AnonymousClass605 r4 = this.A01;
        r4.A06.A00(new C1330869k(this.A00, r4, r6), r6, this.A02);
    }
}
