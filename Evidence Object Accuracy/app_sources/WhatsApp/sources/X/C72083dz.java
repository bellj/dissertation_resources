package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.productinfra.avatar.ui.stickers.upsell.AvatarStickerUpsellView;

/* renamed from: X.3dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72083dz extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ AvatarStickerUpsellView this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72083dz(Context context, AvatarStickerUpsellView avatarStickerUpsellView) {
        super(0);
        this.$context = context;
        this.this$0 = avatarStickerUpsellView;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Activity A00 = AnonymousClass12P.A00(this.$context);
        C18160s0 avatarSharedPreferences = this.this$0.getAvatarSharedPreferences();
        C18120rw avatarEditorLauncherProxy = this.this$0.getAvatarEditorLauncherProxy();
        C235812f avatarLogger = this.this$0.getAvatarLogger();
        return new AnonymousClass3DF(A00, this.this$0, this.this$0.getAvatarConfigRepository(), avatarSharedPreferences, avatarEditorLauncherProxy, avatarLogger);
    }
}
