package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0ZL  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0ZL implements AbstractC12410hs {
    @Override // X.AbstractC12410hs
    public float ADC(View view, ViewGroup viewGroup) {
        return view.getTranslationY();
    }
}
