package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1Fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27021Fs implements AbstractC15840nz {
    public final C15570nT A00;
    public final C15820nx A01;
    public final C15810nw A02;
    public final C17050qB A03;
    public final C16590pI A04;
    public final C19490uC A05;
    public final C21760xw A06;
    public final C21780xy A07;
    public final C16600pJ A08;
    public final AnonymousClass15D A09;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "commerce-db";
    }

    public C27021Fs(C15570nT r1, C15820nx r2, C15810nw r3, C17050qB r4, C16590pI r5, C19490uC r6, C21760xw r7, C21780xy r8, C16600pJ r9, AnonymousClass15D r10) {
        this.A04 = r5;
        this.A09 = r10;
        this.A00 = r1;
        this.A02 = r3;
        this.A05 = r6;
        this.A01 = r2;
        this.A06 = r7;
        this.A03 = r4;
        this.A08 = r9;
        this.A07 = r8;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r6;
        ReentrantReadWriteLock.WriteLock writeLock;
        File databasePath;
        File A02;
        String obj;
        C15820nx r13 = this.A01;
        if (r13.A04()) {
            r6 = EnumC16570pG.A07;
        } else {
            r6 = EnumC16570pG.A06;
        }
        C17050qB r14 = this.A03;
        if (r14.A02()) {
            Log.i("commerce_backup_store/backup/skip no media or read-only media");
            return false;
        }
        C21760xw r2 = this.A06;
        ReentrantReadWriteLock reentrantReadWriteLock = r2.A00().A04;
        if (reentrantReadWriteLock != null) {
            writeLock = reentrantReadWriteLock.writeLock();
        } else {
            writeLock = null;
        }
        writeLock.lock();
        try {
            Log.i("commerce_backup_store/backup/close-backup-db");
            r2.A01();
            try {
                databasePath = r2.A02.A00.getDatabasePath(r2.A04);
            } catch (Exception e) {
                Log.w("commerce_backup_store/backup/error", e);
            }
            if (!databasePath.exists()) {
                StringBuilder sb = new StringBuilder();
                sb.append("commerce_backup_store/backup/db-file-not-found");
                sb.append(databasePath);
                Log.i(sb.toString());
            } else {
                C15810nw r8 = this.A02;
                EnumC16570pG r7 = EnumC16570pG.A05;
                if (r6 == r7) {
                    A02 = r8.A02();
                    obj = "commerce_backup.db.crypt1";
                } else {
                    A02 = r8.A02();
                    StringBuilder sb2 = new StringBuilder("commerce_backup.db.crypt");
                    sb2.append(r6.version);
                    obj = sb2.toString();
                }
                File file = new File(A02, obj);
                List A07 = C32781cj.A07(r7, EnumC16570pG.A00());
                A07.add(".crypt1");
                File file2 = new File(r8.A02(), "commerce_backup.db");
                ArrayList A06 = C32781cj.A06(file2, A07);
                C32781cj.A0C(file2, A06);
                Iterator it = A06.iterator();
                while (it.hasNext()) {
                    File file3 = (File) it.next();
                    if (!file3.equals(file) && file3.exists()) {
                        file3.delete();
                    }
                }
                AnonymousClass15D r22 = this.A09;
                C15570nT r10 = this.A00;
                C19490uC r15 = this.A05;
                C16600pJ r1 = this.A08;
                AbstractC33251dh A00 = C33231df.A00(r10, new C33211dd(file), null, r13, r14, r15, this.A07, r1, r6, r22);
                if (!A00.A04(this.A04.A00)) {
                    Log.w("commerce_backup_store/backup/failed to prepare for backup");
                    writeLock.unlock();
                    return false;
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("commerce_backup_store/backup/to ");
                sb3.append(file);
                Log.i(sb3.toString());
                A00.A03(null, databasePath);
            }
            writeLock.unlock();
            return true;
        } catch (Throwable th) {
            writeLock.unlock();
            throw th;
        }
    }
}
