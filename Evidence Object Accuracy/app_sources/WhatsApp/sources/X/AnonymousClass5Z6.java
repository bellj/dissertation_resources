package X;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.5Z6  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Z6<E> extends List<E>, RandomAccess {
    AnonymousClass5Z6 Agl(int i);
}
