package X;

/* renamed from: X.1fC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33861fC extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public C33861fC() {
        super(3656, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(5, this.A05);
        r3.Abe(6, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamRevokeMessageSend {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageSendResultIsTerminal", this.A00);
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resendCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revokeDuration", this.A05);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revokeType", obj2);
        sb.append("}");
        return sb.toString();
    }
}
