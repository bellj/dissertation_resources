package X;

import com.whatsapp.community.CommunityHomeActivity;

/* renamed from: X.42V  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42V extends C27151Gf {
    public final /* synthetic */ CommunityHomeActivity A00;

    public AnonymousClass42V(CommunityHomeActivity communityHomeActivity) {
        this.A00 = communityHomeActivity;
    }

    @Override // X.C27151Gf
    public void A01(AbstractC14640lm r3) {
        CommunityHomeActivity communityHomeActivity = this.A00;
        if (r3.equals(communityHomeActivity.A0g)) {
            communityHomeActivity.A2e();
        }
    }
}
