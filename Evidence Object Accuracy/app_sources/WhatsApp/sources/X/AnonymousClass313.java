package X;

/* renamed from: X.313  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass313 extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Double A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public String A07;

    public AnonymousClass313() {
        super(2444, new AnonymousClass00E(1, 20, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A03);
        r3.Abe(7, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(5, this.A04);
        r3.Abe(2, this.A07);
        r3.Abe(1, this.A05);
        r3.Abe(4, this.A02);
        r3.Abe(8, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamExitReasonEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitImportance", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitLowMemorySupported", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitPss", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitReason", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitReasonDescription", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitReasonTimestamp", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitRss", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitStatus", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
