package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.util.Log;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Iterator;

/* renamed from: X.1RO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RO extends AbstractC16280ok {
    public C31181a8 A00;
    public final C14830m7 A01;
    public final C16590pI A02;
    public final C231410n A03;
    public final String A04 = "axolotl.db";
    public final boolean A05 = true;

    public AnonymousClass1RO(AbstractC15710nm r8, C14830m7 r9, C16590pI r10, C231410n r11) {
        super(r10.A00, r8, "axolotl.db", null, 13, true);
        this.A02 = r10;
        this.A01 = r9;
        this.A03 = r11;
    }

    public static final void A00(SQLiteDatabase sQLiteDatabase, String str) {
        StringBuilder sb = new StringBuilder("UPDATE ");
        sb.append(str);
        sb.append(" SET ");
        sb.append("device_id");
        sb.append(" = ");
        sb.append(0);
        sQLiteDatabase.execSQL(sb.toString());
    }

    public static final void A01(SQLiteDatabase sQLiteDatabase, String str, String str2, String[] strArr) {
        StringBuilder sb = new StringBuilder("ALTER TABLE ");
        sb.append(str);
        sb.append(" RENAME TO old_");
        sb.append(str);
        sb.append(";");
        sQLiteDatabase.execSQL(sb.toString());
        sQLiteDatabase.execSQL(str2);
        String join = TextUtils.join(", ", strArr);
        StringBuilder sb2 = new StringBuilder("INSERT INTO ");
        sb2.append(str);
        sb2.append(" (");
        sb2.append(join);
        sb2.append(") SELECT ");
        sb2.append(join);
        sb2.append(" FROM old_");
        sb2.append(str);
        sQLiteDatabase.execSQL(sb2.toString());
        StringBuilder sb3 = new StringBuilder("DROP TABLE old_");
        sb3.append(str);
        sQLiteDatabase.execSQL(sb3.toString());
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A03);
        } catch (SQLiteException e) {
            Log.e("failed to open axolotl store", e);
            C31181a8 r0 = this.A00;
            if (r0 != null) {
                r0.A00.A0R();
            }
            return AnonymousClass1Tx.A01(super.A00(), this.A03);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        C31181a8 r0;
        Log.i("creating axolotl database version 13");
        sQLiteDatabase.execSQL("CREATE TABLE identities (_id INTEGER PRIMARY KEY AUTOINCREMENT, recipient_id INTEGER, recipient_type INTEGER NOT NULL DEFAULT 0, device_id INTEGER, registration_id INTEGER, public_key BLOB, private_key BLOB, next_prekey_id INTEGER, timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS identities_idx ON identities(recipient_id, recipient_type, device_id)");
        sQLiteDatabase.execSQL("CREATE TABLE prekeys (_id INTEGER PRIMARY KEY AUTOINCREMENT, prekey_id INTEGER UNIQUE, sent_to_server BOOLEAN, record BLOB, direct_distribution BOOLEAN, upload_timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE TABLE prekey_uploads (_id INTEGER PRIMARY KEY AUTOINCREMENT, upload_timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE TABLE sessions (_id INTEGER PRIMARY KEY AUTOINCREMENT, recipient_id INTEGER, recipient_type INTEGER NOT NULL DEFAULT 0, device_id INTEGER, record BLOB, timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sessions_idx ON sessions(recipient_id, recipient_type, device_id)");
        sQLiteDatabase.execSQL("CREATE TABLE signed_prekeys (_id INTEGER PRIMARY KEY AUTOINCREMENT, prekey_id INTEGER UNIQUE, timestamp INTEGER, record BLOB)");
        sQLiteDatabase.execSQL("CREATE TABLE message_base_key (_id INTEGER PRIMARY KEY AUTOINCREMENT, msg_key_remote_jid TEXT NOT NULL, msg_key_from_me BOOLEAN NOT NULL, msg_key_id TEXT NOT NULL, recipient_id INTEGER, recipient_type INTEGER NOT NULL DEFAULT 0, device_id INTEGER NOT NULL DEFAULT 0, last_alice_base_key BLOB NOT NULL, timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS message_base_key_idx ON message_base_key (msg_key_remote_jid, msg_key_from_me, msg_key_id, recipient_id, recipient_type, device_id)");
        sQLiteDatabase.execSQL("CREATE TABLE sender_keys (_id INTEGER PRIMARY KEY AUTOINCREMENT, group_id TEXT NOT NULL, sender_id INTEGER NOT NULL, sender_type INTEGER NOT NULL DEFAULT 0, device_id INTEGER NOT NULL DEFAULT 0, record BLOB NOT NULL, timestamp INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sender_keys_idx ON sender_keys (group_id, sender_id, sender_type, device_id)");
        sQLiteDatabase.execSQL("CREATE TABLE fast_ratchet_sender_keys (_id INTEGER PRIMARY KEY AUTOINCREMENT, group_id TEXT NOT NULL, sender_id INTEGER NOT NULL, sender_type INTEGER NOT NULL DEFAULT 0, device_id INTEGER NOT NULL DEFAULT 0, record BLOB NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS fast_ratchet_sender_keys_idx ON fast_ratchet_sender_keys (group_id, sender_id, sender_type, device_id)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS decryption_journal (msg_key_id TEXT NOT NULL, sender_id TEXT NOT NULL, device_id TEXT NOT NULL, plain_text BLOB NOT NULL, receive_timestamp INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS decryption_journal_idx ON decryption_journal(msg_key_id, sender_id, device_id)");
        Log.i("created axolotl database version 13");
        if (this.A05 && (r0 = this.A00) != null) {
            C15990oG r4 = r0.A00;
            long A00 = r4.A0D.A00() / 1000;
            C31291aJ A002 = C15940oB.A00();
            C31951bN r1 = new C31951bN(A002.A00, new AnonymousClass1JS(A002.A01));
            C16040oL r12 = r4.A07;
            byte[] A003 = r1.A01.A00.A00();
            C32051bX r2 = r1.A00;
            byte[] bArr = r2.A01;
            try {
                int nextInt = SecureRandom.getInstance("SHA1PRNG").nextInt(2147483646) + 1;
                ContentValues contentValues = new ContentValues();
                contentValues.put("recipient_id", (Integer) -1);
                contentValues.put("recipient_type", (Integer) 0);
                contentValues.put("device_id", (Integer) 0);
                contentValues.put("registration_id", Integer.valueOf(nextInt));
                contentValues.put("public_key", A003);
                contentValues.put("private_key", bArr);
                try {
                    contentValues.put("next_prekey_id", Integer.valueOf(SecureRandom.getInstance("SHA1PRNG").nextInt(16777214) + 1));
                    Long valueOf = Long.valueOf(A00);
                    contentValues.put("timestamp", valueOf);
                    sQLiteDatabase.insertOrThrow("identities", null, contentValues);
                    Log.i("axolotl inserted identity key pair");
                    r12.A01 = new C29331Rt(A003, bArr);
                    r12.A00 = nextInt;
                    C31201aA r6 = r4.A00;
                    try {
                        C31291aJ A004 = C15940oB.A00();
                        AnonymousClass1PA r5 = A004.A01;
                        byte[] A05 = C15940oB.A05(r2, r5.A00());
                        AnonymousClass1G4 A0T = C29191Rf.A06.A0T();
                        A0T.A03();
                        C29191Rf r13 = (C29191Rf) A0T.A00;
                        r13.A00 |= 1;
                        r13.A01 = 0;
                        byte[] A005 = r5.A00();
                        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A005, 0, A005.length);
                        A0T.A03();
                        C29191Rf r14 = (C29191Rf) A0T.A00;
                        r14.A00 |= 2;
                        r14.A04 = A01;
                        byte[] bArr2 = A004.A00.A01;
                        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
                        A0T.A03();
                        C29191Rf r15 = (C29191Rf) A0T.A00;
                        r15.A00 |= 4;
                        r15.A03 = A012;
                        AbstractC27881Jp A013 = AbstractC27881Jp.A01(A05, 0, A05.length);
                        A0T.A03();
                        C29191Rf r16 = (C29191Rf) A0T.A00;
                        r16.A00 |= 8;
                        r16.A05 = A013;
                        long A006 = r6.A01.A00();
                        A0T.A03();
                        C29191Rf r17 = (C29191Rf) A0T.A00;
                        r17.A00 |= 16;
                        r17.A02 = A006;
                        byte[] A02 = A0T.A02().A02();
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put("prekey_id", (Integer) 0);
                        contentValues2.put("timestamp", valueOf);
                        contentValues2.put("record", A02);
                        sQLiteDatabase.insertOrThrow("signed_prekeys", null, contentValues2);
                        Log.i("axolotl inserted signed prekey");
                        r4.A0I.A00.submit(new RunnableBRunnable0Shape5S0100000_I0_5(r4, 43));
                        r4.A0E.A1C(true);
                        Iterator it = r4.A0H.A01().iterator();
                        if (it.hasNext()) {
                            it.next();
                            throw new NullPointerException("onSignalStoreCreated");
                        }
                    } catch (AnonymousClass1RY e) {
                        throw new RuntimeException(e);
                    }
                } catch (NoSuchAlgorithmException e2) {
                    throw new AssertionError(e2);
                }
            } catch (NoSuchAlgorithmException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("axolotl upgrading db from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        if (i2 == 13) {
            long A00 = this.A01.A00() / 1000;
            switch (i) {
                case 1:
                    sQLiteDatabase.execSQL("ALTER TABLE sessions ADD COLUMN timestamp INTEGER");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("timestamp", Long.valueOf(A00));
                    sQLiteDatabase.update("sessions", contentValues, null, null);
                case 2:
                    sQLiteDatabase.execSQL("CREATE TABLE message_base_key (_id INTEGER PRIMARY KEY AUTOINCREMENT, msg_key_remote_jid TEXT NOT NULL, msg_key_from_me BOOLEAN NOT NULL, msg_key_id TEXT NOT NULL, last_alice_base_key BLOB NOT NULL, timestamp INTEGER)");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS message_base_key_idx ON message_base_key (msg_key_remote_jid, msg_key_from_me, msg_key_id)");
                case 3:
                case 4:
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sender_keys");
                    sQLiteDatabase.execSQL("CREATE TABLE sender_keys (_id INTEGER PRIMARY KEY AUTOINCREMENT, group_id TEXT NOT NULL, sender_id INTEGER NOT NULL, record BLOB NOT NULL)");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sender_keys_idx ON sender_keys (group_id, sender_id)");
                case 5:
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sender_keys_idx ON sender_keys (group_id, sender_id)");
                case 6:
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS fast_ratchet_sender_keys");
                    sQLiteDatabase.execSQL("CREATE TABLE fast_ratchet_sender_keys (_id INTEGER PRIMARY KEY AUTOINCREMENT, group_id TEXT NOT NULL, sender_id INTEGER NOT NULL, record BLOB NOT NULL)");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS fast_ratchet_sender_keys_idx ON fast_ratchet_sender_keys (group_id, sender_id)");
                case 7:
                    sQLiteDatabase.execSQL("ALTER TABLE sender_keys ADD COLUMN timestamp INTEGER");
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("timestamp", Long.valueOf(A00));
                    sQLiteDatabase.update("sender_keys", contentValues2, null, null);
                case 8:
                    sQLiteDatabase.execSQL("ALTER TABLE prekeys ADD COLUMN direct_distribution BOOLEAN");
                    ContentValues contentValues3 = new ContentValues();
                    contentValues3.put("direct_distribution", Boolean.FALSE);
                    sQLiteDatabase.update("prekeys", contentValues3, null, null);
                case 9:
                    sQLiteDatabase.execSQL("ALTER TABLE prekeys ADD COLUMN upload_timestamp INTEGER");
                    ContentValues contentValues4 = new ContentValues();
                    Long valueOf = Long.valueOf(A00);
                    contentValues4.put("upload_timestamp", valueOf);
                    sQLiteDatabase.update("prekeys", contentValues4, "sent_to_server != 0", null);
                    sQLiteDatabase.execSQL("CREATE TABLE prekey_uploads (_id INTEGER PRIMARY KEY AUTOINCREMENT, upload_timestamp INTEGER)");
                    ContentValues contentValues5 = new ContentValues();
                    contentValues5.put("upload_timestamp", valueOf);
                    sQLiteDatabase.insert("prekey_uploads", null, contentValues5);
                case 10:
                    A01(sQLiteDatabase, "identities", "CREATE TABLE identities (_id INTEGER PRIMARY KEY AUTOINCREMENT, recipient_id INTEGER, device_id INTEGER, registration_id INTEGER, public_key BLOB, private_key BLOB, next_prekey_id INTEGER, timestamp INTEGER)", new String[]{"recipient_id", "registration_id", "public_key", "private_key", "next_prekey_id", "timestamp"});
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS identities_idx ON identities(recipient_id, device_id)");
                    A00(sQLiteDatabase, "identities");
                    A01(sQLiteDatabase, "sessions", "CREATE TABLE sessions (_id INTEGER PRIMARY KEY AUTOINCREMENT, recipient_id INTEGER, device_id INTEGER, record BLOB, timestamp INTEGER)", new String[]{"recipient_id", "record", "timestamp"});
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sessions_idx ON sessions(recipient_id, device_id)");
                    A00(sQLiteDatabase, "sessions");
                    sQLiteDatabase.execSQL("ALTER TABLE sender_keys ADD COLUMN device_id INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS sender_keys_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sender_keys_idx ON sender_keys (group_id, sender_id, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE fast_ratchet_sender_keys ADD COLUMN device_id INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS fast_ratchet_sender_keys_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS fast_ratchet_sender_keys_idx ON fast_ratchet_sender_keys (group_id, sender_id, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE message_base_key ADD COLUMN recipient_id INTEGER ");
                    sQLiteDatabase.execSQL("ALTER TABLE message_base_key ADD COLUMN device_id INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DELETE FROM message_base_key WHERE msg_key_remote_jid NOT GLOB '[0-9]*@s.whatsapp.net'");
                    sQLiteDatabase.execSQL("UPDATE message_base_key SET recipient_id = CAST(REPLACE(msg_key_remote_jid, '@s.whatsapp.net', '') AS INTEGER)");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS message_base_key_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS message_base_key_idx ON message_base_key (msg_key_remote_jid, msg_key_from_me, msg_key_id, recipient_id, device_id)");
                case 11:
                    sQLiteDatabase.execSQL("ALTER TABLE identities ADD COLUMN recipient_type INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS identities_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS identities_idx ON identities(recipient_id, recipient_type, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE sessions ADD COLUMN recipient_type INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS sessions_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sessions_idx ON sessions(recipient_id, recipient_type, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE message_base_key ADD COLUMN recipient_type INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS message_base_key_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS message_base_key_idx ON message_base_key (msg_key_remote_jid, msg_key_from_me, msg_key_id, recipient_id, recipient_type, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE sender_keys ADD COLUMN sender_type INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS sender_keys_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS sender_keys_idx ON sender_keys (group_id, sender_id, sender_type, device_id)");
                    sQLiteDatabase.execSQL("ALTER TABLE fast_ratchet_sender_keys ADD COLUMN sender_type INTEGER NOT NULL DEFAULT 0");
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS fast_ratchet_sender_keys_idx");
                    sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS fast_ratchet_sender_keys_idx ON fast_ratchet_sender_keys (group_id, sender_id, sender_type, device_id)");
                    break;
                case 12:
                    break;
                default:
                    StringBuilder sb2 = new StringBuilder("Unknown upgrade from ");
                    sb2.append(i);
                    sb2.append(" to ");
                    sb2.append(i2);
                    throw new SQLiteException(sb2.toString());
            }
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS decryption_journal (msg_key_id TEXT NOT NULL, sender_id TEXT NOT NULL, device_id TEXT NOT NULL, plain_text BLOB NOT NULL, receive_timestamp INTEGER NOT NULL)");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS decryption_journal_idx ON decryption_journal(msg_key_id, sender_id, device_id)");
            Log.i("axolotl upgraded successfully");
            return;
        }
        StringBuilder sb3 = new StringBuilder("Unknown upgrade destination version: ");
        sb3.append(i);
        sb3.append(" -> ");
        sb3.append(i2);
        throw new SQLiteException(sb3.toString());
    }
}
