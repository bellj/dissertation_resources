package X;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/* renamed from: X.5MX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MX extends AnonymousClass1TM {
    public Hashtable A00 = new Hashtable();
    public Vector A01 = new Vector();

    public static AnonymousClass5MX A01(Object obj) {
        if (obj instanceof AnonymousClass5MX) {
            return (AnonymousClass5MX) obj;
        }
        if (obj != null) {
            return new AnonymousClass5MX(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        Vector vector = this.A01;
        C94954co r3 = new C94954co(vector.size());
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            r3.A06((AnonymousClass1TM) this.A00.get(elements.nextElement()));
        }
        return new AnonymousClass5NZ(r3);
    }

    public AnonymousClass5MX(AbstractC114775Na r5) {
        Enumeration A0C = r5.A0C();
        while (A0C.hasMoreElements()) {
            Object nextElement = A0C.nextElement();
            C114715Mu r2 = nextElement instanceof C114715Mu ? (C114715Mu) nextElement : nextElement != null ? new C114715Mu(AbstractC114775Na.A04(nextElement)) : null;
            Hashtable hashtable = this.A00;
            AnonymousClass1TK r1 = r2.A00;
            if (!hashtable.containsKey(r1)) {
                this.A00.put(r1, r2);
                this.A01.addElement(r1);
            } else {
                throw C12970iu.A0f(C12960it.A0b("repeated extension found: ", r1));
            }
        }
    }

    public static C114715Mu A00(Object obj, AnonymousClass5MX r2) {
        return (C114715Mu) r2.A00.get(obj);
    }
}
