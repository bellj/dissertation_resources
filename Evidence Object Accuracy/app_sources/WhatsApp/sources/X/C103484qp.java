package X;

import android.content.Context;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaper;

/* renamed from: X.4qp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103484qp implements AbstractC009204q {
    public final /* synthetic */ SolidColorWallpaper A00;

    public C103484qp(SolidColorWallpaper solidColorWallpaper) {
        this.A00 = solidColorWallpaper;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
