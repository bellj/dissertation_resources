package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import org.json.JSONObject;

/* renamed from: X.33D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33D extends AnonymousClass45J implements AbstractC116225Up {
    public int A00;
    public Drawable A01;
    public String A02;
    public String A03;
    public final int A04;
    public final Context A05;
    public final AnonymousClass1AB A06;
    public final String A07;

    @Override // X.AbstractC454821u
    public String A0G() {
        return "sticker";
    }

    @Override // X.AbstractC454821u
    public boolean A0J() {
        return false;
    }

    @Override // X.AbstractC454821u
    public boolean A0K() {
        return false;
    }

    @Override // X.AbstractC454821u
    public boolean A0L() {
        return true;
    }

    public AnonymousClass33D(Context context, AnonymousClass1AB r6, JSONObject jSONObject) {
        this.A05 = context;
        this.A04 = jSONObject.getInt("sticker_size");
        this.A06 = r6;
        if (jSONObject.has("file_path") && jSONObject.has("plain_file_hash") && jSONObject.has("file_storage_location")) {
            this.A02 = jSONObject.getString("file_path");
            this.A03 = jSONObject.getString("plain_file_hash");
            this.A00 = jSONObject.getInt("file_storage_location");
            A0S();
        }
        this.A07 = jSONObject.getString("content_description");
        super.A0A(jSONObject);
        if (this.A01 == null) {
            throw C12970iu.A0f("loadedDrawable was not loaded correctly");
        }
    }

    public AnonymousClass33D(Context context, AnonymousClass1KS r3, AnonymousClass1AB r4, int i) {
        this.A05 = context;
        this.A02 = r3.A08;
        this.A04 = i;
        this.A00 = r3.A01;
        this.A03 = r3.A0C;
        this.A07 = C28111Kr.A01(context, r3);
        this.A06 = r4;
        A0S();
    }

    public static void A02(Canvas canvas, AnonymousClass33D r6) {
        canvas.save();
        Drawable drawable = r6.A01;
        if (drawable instanceof C39631qF) {
            C39631qF r4 = (C39631qF) drawable;
            r6.A0T(canvas, r4.getBounds().right, r4.getBounds().bottom);
            canvas.drawBitmap(r4.A07.A09, (Rect) null, r4.getBounds(), r4.A06);
            return;
        }
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        r6.A0T(canvas, bitmapDrawable.getBitmap().getWidth(), bitmapDrawable.getBitmap().getHeight());
        bitmapDrawable.draw(canvas);
    }

    @Override // X.AbstractC454821u
    public Drawable A0F() {
        return this.A01;
    }

    @Override // X.AbstractC454821u
    public String A0H(Context context) {
        return this.A07;
    }

    @Override // X.AbstractC454821u
    public void A0I(Canvas canvas) {
        if (this.A01 != null) {
            canvas.save();
            if (this.A01 != null) {
                A02(canvas, this);
                canvas.restore();
            }
            canvas.restore();
        }
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        String str;
        super.A0N(jSONObject);
        String str2 = this.A02;
        if (!(str2 == null || (str = this.A03) == null)) {
            jSONObject.put("file_path", str2);
            jSONObject.put("plain_file_hash", str);
            jSONObject.put("file_storage_location", this.A00);
        }
        jSONObject.put("sticker_size", this.A04);
        jSONObject.put("content_description", this.A07);
    }

    @Override // X.AbstractC454821u
    public void A0P(Canvas canvas) {
        if (this.A01 != null) {
            A02(canvas, this);
            canvas.restore();
        }
    }

    @Override // X.AnonymousClass45J
    public float A0R() {
        float width;
        int height;
        Drawable drawable = this.A01;
        if (drawable == null) {
            return 0.0f;
        }
        if (drawable instanceof C39631qF) {
            width = (float) drawable.getBounds().right;
            height = this.A01.getBounds().bottom;
        } else {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            width = (float) bitmapDrawable.getBitmap().getWidth();
            height = bitmapDrawable.getBitmap().getHeight();
        }
        return width / ((float) height);
    }

    public final void A0S() {
        String str = this.A02;
        AnonymousClass009.A05(str);
        String str2 = this.A03;
        AnonymousClass009.A05(str2);
        int i = this.A04;
        AnonymousClass009.A0F(C12960it.A1U(i));
        AnonymousClass1KS r4 = new AnonymousClass1KS();
        int i2 = this.A00;
        r4.A08 = str;
        r4.A01 = i2;
        r4.A0C = str2;
        this.A06.A05(null, new AnonymousClass1qI(this.A05, r4, this, AnonymousClass1AB.A00(r4, i, i), i, i), null);
    }

    public final void A0T(Canvas canvas, int i, int i2) {
        RectF rectF = super.A02;
        float width = rectF.width() / ((float) i);
        float height = rectF.height() / ((float) i2);
        canvas.translate(rectF.centerX(), rectF.centerY());
        canvas.scale(width, height);
        canvas.rotate(super.A00);
        float f = ((float) (-i)) / 2.0f;
        canvas.translate(f, f);
    }

    @Override // X.AbstractC116225Up
    public void AWc(Drawable drawable) {
        this.A01 = drawable;
        RectF rectF = super.A02;
        A0Q(rectF, rectF.left, rectF.top, rectF.right, rectF.bottom);
    }
}
