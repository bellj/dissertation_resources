package X;

/* renamed from: X.5MS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MS extends AnonymousClass1TM {
    public AbstractC114775Na A00;
    public AnonymousClass5MX A01;

    public AnonymousClass5MX A03() {
        if (this.A01 == null) {
            AbstractC114775Na r2 = this.A00;
            if (r2.A0B() == 3) {
                this.A01 = AnonymousClass5MX.A01(r2.A0D(2));
            }
        }
        return this.A01;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public AnonymousClass5MS(AbstractC114775Na r3) {
        if (r3.A0B() < 2 || r3.A0B() > 3) {
            throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r3.A0B()));
        }
        this.A00 = r3;
    }
}
