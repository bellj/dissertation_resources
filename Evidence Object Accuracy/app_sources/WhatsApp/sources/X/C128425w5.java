package X;

import com.whatsapp.camera.litecamera.LiteCameraView;

/* renamed from: X.5w5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128425w5 {
    public final /* synthetic */ AnonymousClass643 A00;

    public C128425w5(AnonymousClass643 r1) {
        this.A00 = r1;
    }

    public void A00() {
        AnonymousClass4K2 r0 = this.A00.A0A;
        if (r0 != null) {
            LiteCameraView liteCameraView = r0.A00;
            liteCameraView.A01.A01("LiteCamera");
            if (!liteCameraView.A09 || liteCameraView.AJS()) {
                liteCameraView.A0E.A01();
                return;
            }
            AnonymousClass643 r2 = liteCameraView.A0D;
            AnonymousClass3HX r1 = liteCameraView.A0E;
            r2.A0I(r1.A01);
            if (!r1.A08) {
                r1.A03.A01();
                r1.A08 = true;
            }
        }
    }
}
