package X;

import android.content.SharedPreferences;
import android.util.Base64;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.2tt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59052tt extends AbstractC59082tw {
    public final C14650lo A00;
    public final C246816l A01;
    public final AnonymousClass4JY A02;
    public final AbstractC116495Vr A03;
    public final C17220qS A04;
    public final String A05;

    public C59052tt(C14650lo r1, C246816l r2, AnonymousClass4JY r3, AbstractC116495Vr r4, UserJid userJid, C17220qS r6, String str) {
        super(r1, userJid);
        this.A05 = str;
        this.A04 = r6;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A06() {
        C246816l r3 = this.A01;
        UserJid userJid = ((AbstractC59082tw) this).A00;
        String str = this.A05;
        synchronized (r3) {
            r3.A02 = str;
            Map map = r3.A03;
            List list = (List) map.get(userJid);
            if (list != null) {
                list.add(this);
            } else {
                ArrayList A0l = C12960it.A0l();
                A0l.add(this);
                map.put(userJid, A0l);
                if (C12980iv.A0p(r3.A08.A00, C12960it.A0d(userJid.getRawString(), C12960it.A0k("smb_business_direct_connection_public_key_"))) == null) {
                    new AnonymousClass1W4(userJid, r3.A0B).A00(new AnonymousClass1W5(r3));
                } else {
                    r3.A03(userJid);
                }
            }
        }
    }

    public void A07(String str) {
        C246816l r2 = this.A01;
        synchronized (r2) {
            r2.A02 = null;
            r2.A01 = null;
            r2.A00 = 0;
        }
        this.A03.AU2(str);
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A07("error");
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r11, String str) {
        String str2;
        boolean z;
        String str3;
        String A0G;
        UserJid userJid = ((AbstractC59082tw) this).A00;
        AnonymousClass4JY r2 = this.A02;
        AnonymousClass1V8 A0E = r11.A0E("result_code");
        if (A0E == null || A0E.A0G() == null) {
            str2 = "invalid_postcode";
        } else {
            str2 = A0E.A0G();
        }
        AnonymousClass1V8 A0E2 = r11.A0E("encrypted_location_name");
        String str4 = null;
        if (!(A0E2 == null || (A0G = A0E2.A0G()) == null)) {
            C246916m r6 = r2.A00;
            byte[] decode = Base64.decode(A0G, 0);
            try {
                byte[] bArr = r6.A00;
                if (bArr != null) {
                    SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, 0, bArr.length, "AES");
                    byte[] bArr2 = r6.A01;
                    if (bArr2 != null) {
                        C30071Vz r4 = new C30071Vz(secretKeySpec, decode, bArr2);
                        IvParameterSpec ivParameterSpec = new IvParameterSpec(r4.A02);
                        Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                        instance.init(2, r4.A00, ivParameterSpec);
                        String str5 = new String(instance.doFinal(r4.A01));
                        r6.A00 = null;
                        r6.A01 = null;
                        str4 = str5;
                    }
                }
            } catch (GeneralSecurityException e) {
                Log.e(e.getMessage());
            }
            str2 = "error";
        }
        C90134Ms r62 = new C90134Ms(str2, str4);
        if (r62.A00 == null) {
            str3 = r62.A01;
        } else {
            C246816l r5 = this.A01;
            synchronized (r5) {
                String str6 = r5.A01;
                if (str6 == null || r5.A00 == 0) {
                    z = false;
                } else {
                    C14820m6 r0 = r5.A08;
                    String rawString = userJid.getRawString();
                    SharedPreferences sharedPreferences = r0.A00;
                    C12970iu.A1D(sharedPreferences.edit(), C12960it.A0d(rawString, C12960it.A0k("smb_business_direct_connection_enc_string_")), str6);
                    C12970iu.A1C(sharedPreferences.edit(), C12960it.A0d(userJid.getRawString(), C12960it.A0k("smb_business_direct_connection_enc_string_expired_timestamp_")), r5.A00);
                    r5.A02 = null;
                    r5.A01 = null;
                    r5.A00 = 0;
                    z = true;
                }
            }
            if (z) {
                this.A03.AU3(r62);
                return;
            }
            str3 = "error";
        }
        A07(str3);
    }
}
