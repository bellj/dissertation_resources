package X;

import android.util.Log;
import java.io.File;

/* renamed from: X.0Rj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05890Rj {
    public final File A00;
    public final File A01;
    public final File A02;

    public C05890Rj(File file) {
        this.A00 = file;
        StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(".new");
        this.A02 = new File(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(file.getPath());
        sb2.append(".bak");
        this.A01 = new File(sb2.toString());
    }

    public static void A00(File file, File file2) {
        if (file2.isDirectory() && !file2.delete()) {
            StringBuilder sb = new StringBuilder("Failed to delete file which is a directory ");
            sb.append(file2);
            Log.e("AtomicFile", sb.toString());
        }
        if (!file.renameTo(file2)) {
            StringBuilder sb2 = new StringBuilder("Failed to rename ");
            sb2.append(file);
            sb2.append(" to ");
            sb2.append(file2);
            Log.e("AtomicFile", sb2.toString());
        }
    }
}
