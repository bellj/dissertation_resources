package X;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.3UV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3UV implements AbstractC42541vN {
    public final Activity A00;
    public final AbstractC13860kS A01;
    public final C14900mE A02;
    public final AbstractC116685Wk A03;
    public final AnonymousClass4NU A04;
    public final AnonymousClass01d A05;
    public final AbstractC15850o0 A06;

    public AnonymousClass3UV(Activity activity, AbstractC13860kS r2, C14900mE r3, AbstractC116685Wk r4, AnonymousClass4NU r5, AnonymousClass01d r6, AbstractC15850o0 r7) {
        this.A00 = activity;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
    }

    public final void A00(Uri uri, AbstractC14640lm r10, int i, int i2, int i3, boolean z) {
        AbstractC15850o0 r5;
        Activity activity;
        AnonymousClass2JR A05;
        if (uri == null && i == -1) {
            r5 = this.A06;
            activity = this.A00;
            A05 = r5.A05(activity, null, r10, z);
        } else {
            r5 = this.A06;
            activity = this.A00;
            if (uri != null) {
                A05 = r5.A05(activity, uri, r10, z);
            } else if (!(r5 instanceof C252218o)) {
                C252118n r2 = (C252118n) r5;
                StringBuilder sb = new StringBuilder("wallpaper/set with resId with size (width x height): ");
                sb.append(i2);
                sb.append("x");
                sb.append(i3);
                Log.i(sb.toString());
                Drawable A02 = AnonymousClass2JP.A02(activity, r2.A04, i, i2, i3);
                r2.A00 = A02;
                if (A02 != null) {
                    r2.A0B(activity, A02);
                }
                A05 = new AnonymousClass2JR(r2.A00, 0, "DOWNLOADED", true);
            } else {
                C252218o r22 = (C252218o) r5;
                Drawable A022 = AnonymousClass2JP.A02(activity, r22.A01, i, i2, i3);
                if (A022 == null) {
                    A05 = r22.A06(activity, r10);
                } else {
                    boolean z2 = false;
                    if (r10 == null) {
                        z2 = true;
                    }
                    A05 = r22.A0B(activity, r22.A0C(activity, (BitmapDrawable) A022, r10), z2);
                }
            }
        }
        this.A03.AdA(r5.A03(A05));
        if (z) {
            C22200yh.A0P(activity, uri);
        }
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        AbstractC14640lm A01;
        String str;
        String str2;
        Uri data;
        int i3;
        boolean z;
        int i4;
        int intExtra;
        AnonymousClass4NU r0 = this.A04;
        int i5 = r0.A00;
        if (i == i5) {
            if (i2 == -1) {
                if (!(intent == null || intent.getData() == null)) {
                    A01 = AbstractC14640lm.A01(intent.getStringExtra("chat_jid"));
                    data = intent.getData();
                    i4 = 0;
                    z = true;
                    i3 = -1;
                }
            } else if (i2 == 0 && intent != null && (intExtra = intent.getIntExtra("error_message_id", -1)) > 0) {
                C14900mE r2 = this.A02;
                AbstractC13860kS r02 = this.A01;
                if (r02 != null) {
                    r02.Ado(intExtra);
                    return true;
                }
                r2.A05(intExtra, 0);
                return true;
            }
            return true;
        }
        if (i == r0.A01) {
            if (i2 == -1 && intent != null) {
                A01 = AbstractC14640lm.A01(intent.getStringExtra("chat_jid"));
                boolean booleanExtra = intent.getBooleanExtra("is_using_global_wallpaper", false);
                Activity activity = this.A00;
                Point A00 = AbstractC15850o0.A00(activity);
                if (intent.getData() != null) {
                    Log.i(C12960it.A0d(intent.getData().toString(), C12960it.A0k("conversation/wallpaper/setup/src:")));
                    ContentResolver A0C = this.A05.A0C();
                    if (intent.getBooleanExtra("FROM_INTERNAL_DOWNLOADS_KEY", false)) {
                        data = intent.getData();
                        i3 = -1;
                        z = false;
                        i4 = 0;
                    } else {
                        if (A0C == null) {
                            Log.w("conversation/wallpaper/setup cr=null");
                        } else {
                            Cursor query = A0C.query(intent.getData(), null, null, null, null);
                            if (query != null) {
                                try {
                                    boolean moveToFirst = query.moveToFirst();
                                    int columnIndex = query.getColumnIndex("bucket_display_name");
                                    if (moveToFirst && columnIndex >= 0 && "WallPaper".equals(query.getString(columnIndex))) {
                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        try {
                                            InputStream openInputStream = A0C.openInputStream(intent.getData());
                                            try {
                                                BitmapFactory.decodeStream(openInputStream, null, options);
                                                if (options.outWidth == A00.x && options.outHeight == A00.y) {
                                                    A00(intent.getData(), A01, -1, 0, 0, true);
                                                    if (openInputStream != null) {
                                                        openInputStream.close();
                                                    }
                                                    query.close();
                                                    return true;
                                                } else if (openInputStream != null) {
                                                    openInputStream.close();
                                                }
                                            } catch (Throwable th) {
                                                if (openInputStream != null) {
                                                    try {
                                                        openInputStream.close();
                                                    } catch (Throwable unused) {
                                                    }
                                                }
                                                throw th;
                                            }
                                        } catch (FileNotFoundException e) {
                                            Log.e(e);
                                        } catch (IOException e2) {
                                            Log.e(e2);
                                        }
                                    }
                                    query.close();
                                } catch (Throwable th2) {
                                    try {
                                        query.close();
                                    } catch (Throwable unused2) {
                                    }
                                    throw th2;
                                }
                            }
                        }
                        Uri data2 = intent.getData();
                        Uri A04 = this.A06.A04();
                        Intent className = C12970iu.A0A().setClassName(activity.getPackageName(), "com.whatsapp.settings.chat.wallpaper.GalleryWallpaperPreview");
                        className.setData(data2);
                        className.putExtra("output", A04);
                        className.putExtra("chat_jid", C15380n4.A03(A01));
                        className.putExtra("is_using_global_wallpaper", booleanExtra);
                        activity.startActivityForResult(className, i5);
                        this.A03.Ag4();
                    }
                } else {
                    AbstractC116685Wk r7 = this.A03;
                    r7.A7G();
                    int intExtra2 = intent.getIntExtra("selected_res_id", 0);
                    if (intExtra2 != 0) {
                        StringBuilder A0k = C12960it.A0k("conversation/wallpaper from pgk:");
                        A0k.append(intExtra2);
                        A0k.append(" [");
                        A0k.append(A00.x);
                        A0k.append(",");
                        A0k.append(A00.y);
                        Log.i(C12960it.A0d("]", A0k));
                        A00(null, A01, intExtra2, A00.x, A00.y, true);
                    } else if (intent.hasExtra("wallpaper_color_file")) {
                        int intExtra3 = intent.getIntExtra("wallpaper_color_file", 0);
                        boolean booleanExtra2 = intent.getBooleanExtra("wallpaper_doodle_overlay", false);
                        AbstractC15850o0 r10 = this.A06;
                        if (!(r10 instanceof C252218o)) {
                            C252118n r5 = (C252118n) r10;
                            r5.A00 = null;
                            try {
                                FileOutputStream openFileOutput = activity.openFileOutput("wallpaper.jpg", 0);
                                openFileOutput.write(4);
                                openFileOutput.write(intExtra3);
                                openFileOutput.flush();
                                openFileOutput.close();
                            } catch (IOException e3) {
                                Log.e(e3);
                            }
                            r5.A00 = r5.A03(r5.A0A(activity, false));
                            ((AbstractC15850o0) r5).A00 = true;
                        } else {
                            C252218o r9 = (C252218o) r10;
                            if (booleanExtra2) {
                                str2 = "COLOR_WITH_WA_OVERLAY";
                            } else {
                                str2 = "COLOR_ONLY";
                            }
                            r9.A0F(activity, A01, new C33191db(0, str2, String.valueOf(intExtra3)));
                        }
                        r7.AdA(r10.A03(r10.A06(activity, A01)));
                    } else {
                        if (intent.getBooleanExtra("is_reset", false)) {
                            AbstractC15850o0 r8 = this.A06;
                            if (!(r8 instanceof C252218o)) {
                                C252118n r82 = (C252118n) r8;
                                Log.i("wallpaper/reset");
                                r82.A00 = null;
                                try {
                                    FileOutputStream openFileOutput2 = activity.openFileOutput("wallpaper.jpg", 0);
                                    openFileOutput2.write(3);
                                    openFileOutput2.flush();
                                    openFileOutput2.close();
                                } catch (IOException e4) {
                                    Log.e(e4);
                                }
                                r82.A07.A6K();
                            } else {
                                ((C252218o) r8).A0F(activity, A01, new C33191db(0, "NONE", null));
                            }
                            r7.AdA(null);
                            str = "conversation/wallpaper/reset";
                        } else if (intent.getBooleanExtra("is_default", false)) {
                            AbstractC15850o0 r92 = this.A06;
                            if (!(r92 instanceof C252218o)) {
                                C252118n r52 = (C252118n) r92;
                                Log.i("wallpaper/default");
                                r52.A00 = null;
                                try {
                                    FileOutputStream openFileOutput3 = activity.openFileOutput("wallpaper.jpg", 0);
                                    openFileOutput3.write(2);
                                    openFileOutput3.flush();
                                    openFileOutput3.close();
                                } catch (IOException e5) {
                                    Log.e(e5);
                                }
                                r52.A00 = r52.A03(r52.A0A(activity, false));
                                r52.A07.A6K();
                            } else {
                                ((C252218o) r92).A0F(activity, A01, new C33191db(0, "DEFAULT", null));
                            }
                            r7.AdA(r92.A03(r92.A06(activity, A01)));
                            str = "conversation/wallpaper/default";
                        } else {
                            this.A02.A07(R.string.error_wallpaper_invalid_file, 0);
                            Log.e(C12960it.A0d(intent.toString(), C12960it.A0k("conversation/wallpaper/invalid_file:")));
                        }
                        Log.i(str);
                    }
                }
            }
            this.A03.Ag4();
            return true;
        }
        return false;
        A00(data, A01, i3, i4, 0, z);
        return true;
    }
}
