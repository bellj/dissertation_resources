package X;

import java.lang.ref.SoftReference;
import java.util.LinkedList;
import java.util.Queue;

/* renamed from: X.4XA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XA {
    public int A00;
    public final int A01;
    public final int A02;
    public final Queue A03;

    public AnonymousClass4XA(int i, int i2, int i3) {
        boolean z = true;
        AnonymousClass0RA.A01(C12960it.A1U(i));
        AnonymousClass0RA.A01(C12990iw.A1W(i2));
        AnonymousClass0RA.A01(i3 < 0 ? false : z);
        this.A01 = i;
        this.A02 = i2;
        this.A03 = new LinkedList();
        this.A00 = i3;
    }

    public Object A00() {
        if (!(this instanceof C75903kg)) {
            return this.A03.poll();
        }
        C75903kg r4 = (C75903kg) this;
        AnonymousClass0OW r3 = (AnonymousClass0OW) r4.A03.poll();
        Object A00 = r3.A00();
        SoftReference softReference = r3.A00;
        if (softReference != null) {
            softReference.clear();
            r3.A00 = null;
        }
        SoftReference softReference2 = r3.A01;
        if (softReference2 != null) {
            softReference2.clear();
            r3.A01 = null;
        }
        SoftReference softReference3 = r3.A02;
        if (softReference3 != null) {
            softReference3.clear();
            r3.A02 = null;
        }
        r4.A00.add(r3);
        return A00;
    }
}
