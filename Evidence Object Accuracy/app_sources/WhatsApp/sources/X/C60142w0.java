package X;

import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaRoundCornerImageView;
import com.whatsapp.calling.callgrid.view.VoiceParticipantAudioWave;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.2w0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60142w0 extends AbstractC55202hx {
    public int A00;
    public ValueAnimator A01;
    public CircularProgressBar A02;
    public AnonymousClass3CK A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final View A09;
    public final ViewGroup A0A;
    public final TextView A0B;
    public final TextView A0C;
    public final ConstraintLayout A0D;
    public final WaImageView A0E;
    public final WaImageView A0F;
    public final WaRoundCornerImageView A0G;
    public final VoiceParticipantAudioWave A0H;
    public final ThumbnailButton A0I;

    public C60142w0(View view, C18720su r5, C89374Js r6, CallGridViewModel callGridViewModel, AnonymousClass130 r8, C15610nY r9) {
        super(view, r5, r6, callGridViewModel, r8, r9);
        this.A0D = (ConstraintLayout) AnonymousClass028.A0D(view, R.id.audio_call_grid);
        this.A0B = C12960it.A0J(view, R.id.audio_call_participant_name);
        ThumbnailButton thumbnailButton = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.audio_call_participant_photo);
        this.A0I = thumbnailButton;
        this.A0H = (VoiceParticipantAudioWave) view.findViewById(R.id.audio_call_participant_wave);
        this.A0E = (WaImageView) view.findViewById(R.id.mute_icon);
        this.A0F = (WaImageView) view.findViewById(R.id.tile_background);
        this.A0G = (WaRoundCornerImageView) view.findViewById(R.id.call_grid_blur_background);
        this.A09 = view.findViewById(R.id.dark_overlay);
        ViewGroup A0P = C12980iv.A0P(view, R.id.status_container);
        this.A0A = A0P;
        this.A0C = A0P != null ? C12960it.A0J(A0P, R.id.status) : null;
        this.A05 = view.getResources().getDimensionPixelSize(R.dimen.voice_call_grid_2cols_avatar_max_height);
        this.A07 = view.getResources().getDimensionPixelSize(R.dimen.voice_call_grid_3cols_avatar_max_height);
        ((AbstractC55202hx) this).A00 = view.getResources().getDimensionPixelSize(R.dimen.call_grid_border_width_for_speaker);
        this.A06 = view.getResources().getDimensionPixelSize(R.dimen.voice_call_grid_2cols_font_max_size);
        this.A08 = view.getResources().getDimensionPixelSize(R.dimen.voice_call_grid_3cols_font_size);
        this.A04 = view.getResources().getDimensionPixelSize(R.dimen.voip_call_grid_margin);
        thumbnailButton.A02 = (((float) C12990iw.A0K(view).widthPixels) + 1.0f) / 2.0f;
    }

    @Override // X.AbstractC55202hx
    public void A0G(C64363Fg r8) {
        int dimensionPixelSize;
        VoiceParticipantAudioWave voiceParticipantAudioWave;
        int i;
        String str;
        Resources resources;
        int i2;
        TextView textView;
        int i3;
        C64363Fg r0 = ((AbstractC55202hx) this).A05;
        if (r0 == null || !r0.A0S.equals(r8.A0S)) {
            A0J(r8);
            A0K(r8);
            Resources resources2 = super.A0H.getResources();
            int i4 = r8.A01;
            if (i4 == -1) {
                i3 = resources2.getColor(R.color.white_alpha_80);
            } else {
                int[] intArray = resources2.getIntArray(R.array.voip_group_participant_name_colors);
                i3 = intArray[i4 % intArray.length];
            }
            this.A00 = i3;
            TextView textView2 = this.A0B;
            if (textView2 != null) {
                textView2.setTextColor(i3);
            }
            ((AbstractC55202hx) this).A06 = false;
            GradientDrawable gradientDrawable = (GradientDrawable) this.A0D.getBackground();
            int i5 = ((AbstractC55202hx) this).A00;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(i5, 0);
            }
            A0H();
            IDxObserverShape4S0100000_2_I1 iDxObserverShape4S0100000_2_I1 = new IDxObserverShape4S0100000_2_I1(this, 20);
            ((AbstractC55202hx) this).A03 = iDxObserverShape4S0100000_2_I1;
            CallGridViewModel callGridViewModel = ((AbstractC55202hx) this).A04;
            if (callGridViewModel != null) {
                AnonymousClass3CZ r4 = callGridViewModel.A0E;
                UserJid userJid = r8.A0S;
                Map map = r4.A01;
                if (!map.containsKey(userJid)) {
                    map.put(userJid, null);
                }
                r4.A00.put(userJid, iDxObserverShape4S0100000_2_I1);
            }
            A0L(r8, false);
        }
        if (r8.A02 == 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = super.A0H.getResources().getDimensionPixelSize(r8.A02);
        }
        View view = super.A0H;
        view.setPadding(0, 0, dimensionPixelSize, dimensionPixelSize);
        ViewGroup viewGroup = this.A0A;
        if (!(viewGroup == null || (textView = this.A0C) == null)) {
            if (!r8.A0H || r8.A05 == -1) {
                viewGroup.setVisibility(8);
                this.A0D.setVisibility(0);
            } else {
                viewGroup.setVisibility(0);
                this.A0D.setVisibility(8);
                textView.setText(r8.A05);
            }
            A0K(r8);
        }
        ConstraintLayout constraintLayout = this.A0D;
        if (constraintLayout.getVisibility() == 0 && r8.A0D) {
            if (this.A02 == null) {
                CircularProgressBar circularProgressBar = new CircularProgressBar(view.getContext());
                this.A02 = circularProgressBar;
                circularProgressBar.A0C = -1;
                int dimensionPixelSize2 = C12960it.A09(view).getDimensionPixelSize(R.dimen.call_grid_mute_other_spinner_size);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(dimensionPixelSize2, dimensionPixelSize2);
                layoutParams.gravity = 17;
                ((ViewGroup) view).addView(this.A02, layoutParams);
            }
            if (this.A02.getVisibility() != 0 || this.A01 == null) {
                this.A02.setVisibility(0);
                this.A09.setAlpha(1.0f);
                int[] A07 = C13000ix.A07();
                // fill-array-data instruction
                A07[0] = 0;
                A07[1] = 100;
                ValueAnimator ofInt = ValueAnimator.ofInt(A07);
                this.A01 = ofInt;
                ofInt.setDuration(750L);
                this.A01.setRepeatCount(-1);
                C12990iw.A0w(this.A01, this, 6);
                this.A01.start();
            }
        }
        if (constraintLayout.getVisibility() == 0) {
            VoiceParticipantAudioWave voiceParticipantAudioWave2 = this.A0H;
            if (voiceParticipantAudioWave2 != null) {
                voiceParticipantAudioWave2.setMuteIconVisibility(r8.A0N);
            } else {
                WaImageView waImageView = this.A0E;
                if (waImageView != null) {
                    waImageView.setVisibility(C12960it.A02(r8.A0N ? 1 : 0));
                }
            }
        }
        if (constraintLayout.getVisibility() == 0 && (voiceParticipantAudioWave = this.A0H) != null) {
            if (r8.A00 != -1) {
                i = voiceParticipantAudioWave.getResources().getColor(r8.A00);
            } else {
                i = this.A00;
            }
            voiceParticipantAudioWave.setColor(i);
            if (r8.A0J) {
                voiceParticipantAudioWave.A02(0.0f, false);
            }
            boolean z = r8.A0K;
            ValueAnimator valueAnimator = voiceParticipantAudioWave.A07;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            if (z) {
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.1f, 0.6f);
                voiceParticipantAudioWave.A07 = ofFloat;
                ofFloat.setDuration(1000L);
                voiceParticipantAudioWave.A07.setInterpolator(voiceParticipantAudioWave.A0H);
                voiceParticipantAudioWave.A07.setRepeatCount(-1);
                voiceParticipantAudioWave.A07.setRepeatMode(2);
                voiceParticipantAudioWave.A07.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Jd
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        VoiceParticipantAudioWave voiceParticipantAudioWave3 = VoiceParticipantAudioWave.this;
                        voiceParticipantAudioWave3.A08.setAlpha((int) (C12960it.A00(valueAnimator2) * 255.0f));
                        voiceParticipantAudioWave3.invalidate();
                    }
                });
                voiceParticipantAudioWave.A07.start();
            }
            if (r8.A05 != -1) {
                resources = voiceParticipantAudioWave.getResources();
                i2 = r8.A05;
            } else if (r8.A0N) {
                resources = voiceParticipantAudioWave.getResources();
                i2 = R.string.voip_pip_peer_muted;
            } else {
                str = "";
                voiceParticipantAudioWave.setContentDescription(str);
            }
            str = resources.getString(i2);
            voiceParticipantAudioWave.setContentDescription(str);
        }
        if (r8.A0L) {
            C12960it.A13(view, this, r8, 19);
            view.setOnLongClickListener(new View.OnLongClickListener(r8) { // from class: X.4n5
                public final /* synthetic */ C64363Fg A01;

                {
                    this.A01 = r2;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view2) {
                    C60142w0 r02 = C60142w0.this;
                    C64363Fg r1 = this.A01;
                    AnonymousClass3CK r03 = r02.A03;
                    if (r03 == null) {
                        return false;
                    }
                    r03.A00(r1);
                    return true;
                }
            });
            view.setOnTouchListener(new View.OnTouchListener() { // from class: X.4nK
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view2, MotionEvent motionEvent) {
                    C60142w0 r1 = C60142w0.this;
                    r1.A0C(motionEvent, r1.A09);
                    return false;
                }
            });
        }
        ((AbstractC55202hx) this).A05 = r8;
    }

    public final void A0H() {
        if (this.A02 != null) {
            ValueAnimator valueAnimator = this.A01;
            if (valueAnimator != null) {
                valueAnimator.end();
                this.A01 = null;
            }
            this.A02.setVisibility(8);
            this.A09.setAlpha(0.0f);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
        if (r1.A0N != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0I(int r6) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C60132vz
            if (r0 != 0) goto L_0x0059
            X.3Fg r1 = r5.A05
            if (r1 == 0) goto L_0x0059
            r4 = 1
            if (r6 <= 0) goto L_0x0010
            boolean r0 = r1.A0N
            r3 = 1
            if (r0 == 0) goto L_0x0011
        L_0x0010:
            r3 = 0
        L_0x0011:
            boolean r0 = r5.A06
            if (r0 == r3) goto L_0x002f
            boolean r0 = r1.A0I
            if (r0 == 0) goto L_0x005d
            if (r3 == 0) goto L_0x005d
        L_0x001b:
            androidx.constraintlayout.widget.ConstraintLayout r0 = r5.A0D
            android.graphics.drawable.Drawable r2 = r0.getBackground()
            android.graphics.drawable.GradientDrawable r2 = (android.graphics.drawable.GradientDrawable) r2
            int r1 = r5.A00
            int r0 = r5.A00
            if (r2 == 0) goto L_0x002f
            if (r4 != 0) goto L_0x002c
            r1 = 0
        L_0x002c:
            r2.setStroke(r0, r1)
        L_0x002f:
            X.3Fg r0 = r5.A05
            boolean r0 = r0.A0J
            if (r0 == 0) goto L_0x0036
            r6 = 0
        L_0x0036:
            com.whatsapp.calling.callgrid.view.VoiceParticipantAudioWave r2 = r5.A0H
            if (r2 == 0) goto L_0x003f
            float r1 = (float) r6
            r0 = 1
            r2.A02(r1, r0)
        L_0x003f:
            X.3Fg r0 = r5.A05
            boolean r0 = r0.A0J
            if (r0 != 0) goto L_0x0057
            if (r2 == 0) goto L_0x0057
            if (r3 == 0) goto L_0x005a
            android.content.res.Resources r1 = r2.getResources()
            r0 = 2131893019(0x7f121b1b, float:1.9420803E38)
            java.lang.String r0 = r1.getString(r0)
        L_0x0054:
            r2.setContentDescription(r0)
        L_0x0057:
            r5.A06 = r3
        L_0x0059:
            return
        L_0x005a:
            java.lang.String r0 = ""
            goto L_0x0054
        L_0x005d:
            r4 = 0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60142w0.A0I(int):void");
    }

    public void A0J(C64363Fg r4) {
        String A04;
        TextView textView = this.A0B;
        if (textView != null) {
            textView.setVisibility(C12960it.A02(r4.A0O ? 1 : 0));
            if (r4.A0G) {
                A04 = textView.getContext().getString(R.string.you);
            } else {
                A04 = ((AbstractC55202hx) this).A09.A04(r4.A0R);
            }
            textView.setText(A04);
        }
    }

    public final void A0K(C64363Fg r6) {
        WaImageView waImageView;
        boolean z;
        String A04;
        TextView textView;
        if (r6 != null && (waImageView = this.A0F) != null) {
            int i = 1;
            if (this.A0D.getVisibility() == 8 || (textView = this.A0B) == null || textView.getVisibility() == 8) {
                z = true;
                if (r6.A0G) {
                    A04 = waImageView.getContext().getString(R.string.you);
                } else {
                    A04 = ((AbstractC55202hx) this).A09.A04(r6.A0R);
                }
            } else {
                z = false;
                A04 = "";
            }
            waImageView.setContentDescription(A04);
            if (!z) {
                i = 2;
            }
            waImageView.setImportantForAccessibility(i);
        }
    }

    public void A0L(C64363Fg r4, boolean z) {
        if (this.A0D.getVisibility() == 0) {
            A0E(this.A0I, r4.A0R, false, false);
        }
        WaRoundCornerImageView waRoundCornerImageView = this.A0G;
        if (waRoundCornerImageView != null && r4.A0P) {
            waRoundCornerImageView.setVisibility(0);
            A0E(waRoundCornerImageView, r4.A0R, true, z);
        }
    }
}
