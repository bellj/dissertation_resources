package X;

/* renamed from: X.0nN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15510nN {
    public final C14820m6 A00;

    public C15510nN(C14820m6 r1) {
        this.A00 = r1;
    }

    public int A00() {
        return this.A00.A00.getInt("registration_state", 0);
    }

    public boolean A01() {
        return A00() == 3;
    }
}
