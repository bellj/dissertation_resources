package X;

import android.view.View;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;

/* renamed from: X.5cK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118755cK extends AnonymousClass03U {
    public PeerPaymentTransactionRow A00;
    public final /* synthetic */ C118645c9 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C118755cK(View view, C118645c9 r2) {
        super(view);
        this.A01 = r2;
        this.A00 = (PeerPaymentTransactionRow) view;
    }
}
