package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3JF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JF {
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (X.AnonymousClass3JU.A00(r3) != false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(int[] r4) {
        /*
            boolean r1 = X.AnonymousClass3JU.A03(r4)
            java.lang.String r0 = "must be skin tone"
            X.AnonymousClass009.A0A(r0, r1)
            int[] r3 = X.C38491oB.A04(r4)
            int r2 = r3.length
            r0 = 3
            if (r2 == r0) goto L_0x0014
            r0 = 4
            if (r2 != r0) goto L_0x004c
        L_0x0014:
            int r0 = r2 + -2
            r1 = r3[r0]
            r0 = 8205(0x200d, float:1.1498E-41)
            if (r1 != r0) goto L_0x004c
            int r0 = r2 + -1
            r3 = r3[r0]
            boolean r0 = X.AnonymousClass3JU.A00(r3)
            if (r0 == 0) goto L_0x004c
        L_0x0026:
            java.lang.StringBuilder r2 = X.C12960it.A0h()
            r0 = 0
            r0 = r4[r0]
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r2.append(r0)
            if (r3 != 0) goto L_0x003d
            java.lang.String r0 = ""
        L_0x0038:
            java.lang.String r0 = X.C12960it.A0d(r0, r2)
            return r0
        L_0x003d:
            java.lang.String r0 = "_"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = java.lang.Integer.toString(r3)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            goto L_0x0038
        L_0x004c:
            r3 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JF.A00(int[]):java.lang.String");
    }

    public static void A01(C16630pM r3, int[] iArr) {
        String join;
        int[] A06 = AnonymousClass3JU.A06(iArr);
        if (!AnonymousClass3JU.A02(A06)) {
            Log.e("EmojiSkinTonePreferenceManager/savePreferredMultiSkinTone/emoji is not a multi skin tone emoji");
            return;
        }
        int[] A07 = AnonymousClass3JU.A07(A06);
        String A0d = C12960it.A0d(new C37471mS(A07).toString(), C12960it.A0k("multi_skin_"));
        List A01 = new AnonymousClass3HQ(iArr).A01();
        if (A01.size() == 0) {
            join = "";
        } else {
            join = TextUtils.join("_", A01);
        }
        C12970iu.A1D(r3.A01("emoji_modifiers").edit(), A0d, join);
    }

    public static void A02(C16630pM r3, int[] iArr) {
        int i;
        String A00 = A00(iArr);
        if (AnonymousClass3JU.A01(iArr)) {
            i = iArr[1];
        } else {
            i = 0;
        }
        C12960it.A0u(r3.A01("emoji_modifiers"), A00, i);
    }

    public static int[] A03(C16630pM r4, int[] iArr) {
        String[] split;
        int length;
        try {
            String string = r4.A01("emoji_modifiers").getString(C12960it.A0d(new C37471mS(AnonymousClass3JU.A07(iArr)).toString(), C12960it.A0k("multi_skin_")), "");
            if (TextUtils.isEmpty(string) || (length = (split = TextUtils.split(string, "_")).length) < 2) {
                return iArr;
            }
            AnonymousClass3HQ r2 = new AnonymousClass3HQ(AnonymousClass3JU.A05(AnonymousClass3JU.A06(iArr)));
            for (int i = 1; i <= length; i++) {
                r2 = r2.A00(i, Integer.parseInt(split[i - 1]));
            }
            return r2.A02();
        } catch (ClassCastException e) {
            Log.e("EmojiSkinTonePreferenceManager/getPreferredMultiSkinTone", e);
            return iArr;
        }
    }
}
