package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AB extends Enum {
    public static final /* synthetic */ AnonymousClass4AB[] A00;
    public static final AnonymousClass4AB A01;
    public static final AnonymousClass4AB A02;
    public static final AnonymousClass4AB A03;

    static {
        AnonymousClass4AB r14 = new AnonymousClass4AB("ERROR_CORRECTION", 0);
        AnonymousClass4AB r0 = new AnonymousClass4AB("CHARACTER_SET", 1);
        A01 = r0;
        AnonymousClass4AB r12 = new AnonymousClass4AB("DATA_MATRIX_SHAPE", 2);
        AnonymousClass4AB r11 = new AnonymousClass4AB("MIN_SIZE", 3);
        AnonymousClass4AB r10 = new AnonymousClass4AB("MAX_SIZE", 4);
        AnonymousClass4AB r9 = new AnonymousClass4AB("MARGIN", 5);
        AnonymousClass4AB r8 = new AnonymousClass4AB("PDF417_COMPACT", 6);
        AnonymousClass4AB r7 = new AnonymousClass4AB("PDF417_COMPACTION", 7);
        AnonymousClass4AB r6 = new AnonymousClass4AB("PDF417_DIMENSIONS", 8);
        AnonymousClass4AB r5 = new AnonymousClass4AB("AZTEC_LAYERS", 9);
        AnonymousClass4AB r4 = new AnonymousClass4AB("QR_VERSION", 10);
        A03 = r4;
        AnonymousClass4AB r2 = new AnonymousClass4AB("GS1_FORMAT", 11);
        A02 = r2;
        AnonymousClass4AB[] r1 = new AnonymousClass4AB[12];
        r1[0] = r14;
        r1[1] = r0;
        C12980iv.A1P(r12, r11, r10, r1);
        C12970iu.A1R(r9, r8, r7, r6, r1);
        r1[9] = r5;
        r1[10] = r4;
        r1[11] = r2;
        A00 = r1;
    }

    public AnonymousClass4AB(String str, int i) {
    }

    public static AnonymousClass4AB valueOf(String str) {
        return (AnonymousClass4AB) Enum.valueOf(AnonymousClass4AB.class, str);
    }

    public static AnonymousClass4AB[] values() {
        return (AnonymousClass4AB[]) A00.clone();
    }
}
