package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.1OX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OX {
    public AnonymousClass1J1 A00;
    public AnonymousClass1AB A01;
    public List A02;
    public final AnonymousClass01F A03;
    public final C15450nH A04;
    public final C21270x9 A05;
    public final AnonymousClass1OW A06;
    public final AnonymousClass19D A07;
    public final AnonymousClass11P A08;
    public final C14850m9 A09;
    public final C239913u A0A;
    public final C39091pH A0B;

    public AnonymousClass1OX(AnonymousClass01F r2, C15450nH r3, C21270x9 r4, AnonymousClass19D r5, AnonymousClass11P r6, C14850m9 r7, C239913u r8, AnonymousClass1AB r9, AbstractC14440lR r10, C39091pH r11) {
        this.A09 = r7;
        this.A04 = r3;
        this.A05 = r4;
        this.A03 = r2;
        this.A0A = r8;
        this.A01 = r9;
        this.A07 = r5;
        this.A08 = r6;
        this.A06 = new AnonymousClass1OW(this, r7, r10);
        this.A0B = r11;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0148, code lost:
        if (r1 == 1) goto L_0x01f7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0240 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0250 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:230:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:231:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0027 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(X.AbstractC15340mz r15) {
        /*
        // Method dump skipped, instructions count: 736
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OX.A00(X.0mz):int");
    }

    public AnonymousClass1J1 A01(Context context) {
        AnonymousClass1J1 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass1J1 A04 = this.A05.A04(context, "conversation-row-inflater");
        this.A00 = A04;
        return A04;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public AnonymousClass1OY A02(Context context, AbstractC13890kV r17, AbstractC15340mz r18) {
        PollVoterViewModel pollVoterViewModel;
        byte b = r18.A0y;
        if (b != 0) {
            if (b == 1) {
                AnonymousClass1X7 r14 = (AnonymousClass1X7) r18;
                if (r18.A0y()) {
                    return new C60542yC(context, r17, r14);
                }
                return new C60782yd(context, r17, r14);
            } else if (b == 2) {
                C30421Xi r142 = (C30421Xi) r18;
                if (!this.A09.A07(1040) ? ((AbstractC15340mz) r142).A08 != 1 : !r142.A1C()) {
                    return new C60752yZ(context, r17, this.A07, this.A08, r142);
                }
                return new C60872ym(context, A01(context), r17, this.A07, this.A08, this.A0A, r142);
            } else if (b != 3) {
                if (b == 4) {
                    return new AnonymousClass2y0(context, A01(context), r17, r18, this.A0B);
                }
                if (b == 5) {
                    AnonymousClass1XO r143 = (AnonymousClass1XO) r18;
                    if (r18.A0y()) {
                        return new C60562yE(context, r17, r143);
                    }
                    return new C60792ye(context, r17, r143);
                } else if (b == 36) {
                    return new C60472xv(context, r17, (AnonymousClass1XK) r18);
                } else {
                    if (b != 49) {
                        if (b == 57) {
                            return new C60532yB(context, r17, (AnonymousClass1X6) r18);
                        }
                        if (b == 66) {
                            if (r17 != null) {
                                pollVoterViewModel = r17.AFm();
                            } else {
                                pollVoterViewModel = null;
                            }
                            return new C60802yf(context, r17, pollVoterViewModel, (C27671Iq) r18);
                        } else if (b != 51) {
                            if (!(b == 52 || b == 54 || b == 55)) {
                                switch (b) {
                                    case 9:
                                        if (C30041Vv.A0r(r18)) {
                                            int A00 = C47962Dl.A00(r18);
                                            AnonymousClass1J1 A01 = A01(context);
                                            C39091pH r0 = this.A0B;
                                            if (A00 == 1) {
                                                return new AnonymousClass2y0(context, A01, r17, r18, r0);
                                            }
                                            return new C60492xx(context, A01, r17, r18, r0);
                                        }
                                        C16440p1 r144 = (C16440p1) r18;
                                        if (r18.A0y()) {
                                            return new AnonymousClass2y8(context, r17, r144);
                                        }
                                        return new C60762yb(context, r17, r144);
                                    case 10:
                                        return new C60442xq(context, r17, (C30381Xe) r18);
                                    case 11:
                                        if (r18.A0L != null) {
                                            return new C42461vF(context, r17, r18);
                                        }
                                        return new AnonymousClass2xl(context, r17, (C30371Xd) r18);
                                    case 12:
                                        AnonymousClass1IR r02 = r18.A0L;
                                        if (r02 == null || r02.A03 == 5) {
                                            return new C60412xn(context, r17, r18);
                                        }
                                        return new C42461vF(context, r17, r18);
                                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                        C47412Ap r2 = new C47412Ap(context, r17, (AnonymousClass1XR) r18);
                                        List list = this.A02;
                                        if (list == null) {
                                            list = new ArrayList();
                                            this.A02 = list;
                                        }
                                        list.add(r2);
                                        return r2;
                                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                        return new C60492xx(context, A01(context), r17, r18, this.A0B);
                                    case 15:
                                        return new C60812yg(context, r17, (C30331Wz) r18);
                                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                        return new C42511vK(context, A01(context), r17, (C30341Xa) r18);
                                    default:
                                        switch (b) {
                                            case 19:
                                                return new C60452xr(context, r17, (AnonymousClass1XZ) r18);
                                            case C43951xu.A01 /* 20 */:
                                                if (r18.A0L != null) {
                                                    return new C42461vF(context, r17, r18);
                                                }
                                                return new C60822yh(context, r17, (C30061Vy) r18, this.A01);
                                            case 21:
                                            case 22:
                                                return new C60462xs(context, r17, r18);
                                            case 23:
                                                return new C60632yL(context, r17, (AnonymousClass1XV) r18);
                                            case 24:
                                                return new C60502xy(context, r17, (C28581Od) r18);
                                            case 25:
                                                return new C60772yc(context, r17, (AnonymousClass1XU) r18);
                                            case 26:
                                                return new AnonymousClass2y7(context, r17, (AnonymousClass1XT) r18);
                                            case 27:
                                                return new C60512xz(context, r17, (C28851Pg) r18);
                                            case 28:
                                                return new C60672yP(context, r17, (AnonymousClass1XS) r18);
                                            case 29:
                                                return new AnonymousClass2y9(context, r17, (AnonymousClass1XQ) r18);
                                            case C25991Bp.A0S:
                                                return new C60552yD(context, r17, (AnonymousClass1XN) r18);
                                            case 31:
                                                if (r18.A0L != null) {
                                                    return new C42461vF(context, r17, r18);
                                                }
                                                return new AnonymousClass2xm(context, r17, (AnonymousClass1XM) r18);
                                            case 32:
                                                break;
                                            default:
                                                switch (b) {
                                                    case 42:
                                                        AbstractC16130oV r145 = (AbstractC16130oV) r18;
                                                        if (r145.A0z.A02) {
                                                            return new C42651vb(context, r17, r145);
                                                        }
                                                        return new C60862yl(context, r17, r145);
                                                    case 43:
                                                        AbstractC16130oV r146 = (AbstractC16130oV) r18;
                                                        if (r146.A0z.A02) {
                                                            return new C42651vb(context, r17, r146);
                                                        }
                                                        return new C60862yl(context, r17, r146);
                                                    case 44:
                                                        return new AnonymousClass243(context, r17, (AnonymousClass1XF) r18);
                                                    case 45:
                                                        break;
                                                    case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                                                        break;
                                                    default:
                                                        switch (b) {
                                                            case 62:
                                                                return new C60662yO(context, r17, (AnonymousClass1X1) r18);
                                                            case 63:
                                                                return new AnonymousClass2y6(context, r17, (AnonymousClass1X0) r18);
                                                            case 64:
                                                                return new C60642yM(context, r17, (C30321Wy) r18);
                                                            default:
                                                                if (r18.A0C == -1 && b == -1) {
                                                                    return new C42441vD(context, r17, (AnonymousClass1XB) r18);
                                                                }
                                                                return new C60412xn(context, r17, r18);
                                                        }
                                                }
                                        }
                                }
                            }
                            return new C60422xo(context, r17, (C16380ov) r18);
                        } else {
                            AnonymousClass1XC r22 = (AnonymousClass1XC) r18;
                            if (r22.A00 == 3 || (this.A09.A07(544) && r22.A00 == 2)) {
                                return new C42491vI(context, r17, r18);
                            }
                            return new C60412xn(context, r17, r18);
                        }
                    }
                    return new C60842yj(context, r17, (C28861Ph) r18);
                }
            } else if (r18.A0y()) {
                return new C60682yQ(context, r17, (AnonymousClass1X2) r18);
            } else {
                AnonymousClass1X2 r1 = (AnonymousClass1X2) r18;
                if (C30041Vv.A0o(r1) || r18.A0z.A02) {
                    return new C60852yk(context, r17, r1);
                }
                return new C60622yK(context, r17, r1);
            }
        } else if (r18 instanceof AnonymousClass1XB) {
            AnonymousClass1XB r4 = (AnonymousClass1XB) r18;
            int i = r4.A00;
            if (!r18.A0z.A02 || i != 6) {
                if (i == 41 || i == 40 || i == 42 || i == 64 || i == 65 || i == 66) {
                    return new C42421vB(context, r17, r4);
                }
                if (i == 68) {
                    return new C60482xw(context, r17, (AnonymousClass1XA) r18);
                }
                if (i == 76) {
                    return new AnonymousClass2xt(context, r17, r4);
                }
                if (i == 80) {
                    return new AnonymousClass2xu(context, this.A03, r17, r4);
                }
                if (i == 67) {
                    C42441vD r3 = new C42441vD(context, r17, r4);
                    if (((AbstractC28551Oa) r3).A0L.A07(1071)) {
                        r3.A1P.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r3, 6, r4));
                    }
                    return r3;
                }
            } else if (((C30501Xq) r18).A00 != null) {
                return new AnonymousClass2y5(context, r17, r4);
            }
            return new C42441vD(context, r17, r4);
        } else if (r18.A0L != null) {
            return new C42461vF(context, r17, r18);
        } else {
            C28861Ph r147 = (C28861Ph) r18;
            if (r18.A0y()) {
                return new C60432xp(context, r17, r147);
            }
            return new C60842yj(context, r17, r147);
        }
    }
}
