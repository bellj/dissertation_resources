package X;

/* renamed from: X.24C  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass24C extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;

    public AnonymousClass24C() {
        super(3572, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamChatPsaAction {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psaMessageActionType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psaMessageId", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
