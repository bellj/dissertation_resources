package X;

import android.view.View;
import android.widget.PopupWindow;

/* renamed from: X.0Dc  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Dc extends AnonymousClass0Y9 {
    public final /* synthetic */ AnonymousClass0XC A00;

    public AnonymousClass0Dc(AnonymousClass0XC r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        LayoutInflater$Factory2C011505o r2 = this.A00.A01;
        r2.A0L.setVisibility(8);
        PopupWindow popupWindow = r2.A09;
        if (popupWindow != null) {
            popupWindow.dismiss();
        } else if (r2.A0L.getParent() instanceof View) {
            AnonymousClass028.A0R((View) r2.A0L.getParent());
        }
        r2.A0L.A03();
        r2.A0N.A09(null);
        r2.A0N = null;
        AnonymousClass028.A0R(r2.A07);
    }
}
