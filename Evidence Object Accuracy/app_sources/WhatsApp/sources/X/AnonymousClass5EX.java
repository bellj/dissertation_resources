package X;

import android.os.SystemClock;

/* renamed from: X.5EX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5EX implements AnonymousClass01N, AnonymousClass01H {
    public Object A00 = null;
    public AnonymousClass01N A01;

    public AnonymousClass5EX(AnonymousClass01N r2) {
        this.A01 = r2;
    }

    public static float A00(AnonymousClass5EX r0) {
        return ((Number) r0.get()).floatValue();
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        Object obj = this.A00;
        if (obj == null) {
            AnonymousClass01N r1 = this.A01;
            AnonymousClass009.A0F(true);
            SystemClock.uptimeMillis();
            try {
                obj = r1.get();
                AnonymousClass009.A05(obj);
                this.A00 = obj;
            } finally {
                SystemClock.uptimeMillis();
            }
        }
        return obj;
    }
}
