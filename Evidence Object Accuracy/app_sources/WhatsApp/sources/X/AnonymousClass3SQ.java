package X;

import com.facebook.msys.mci.DataTask;
import com.facebook.msys.mci.DataTaskListener;
import com.facebook.msys.mci.NetworkSession;
import com.facebook.msys.mci.NetworkUtils;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.whatsapp.util.Log;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.3SQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SQ implements DataTaskListener {
    public final /* synthetic */ C37561md A00;

    public AnonymousClass3SQ(C37561md r1) {
        this.A00 = r1;
    }

    @Override // com.facebook.msys.mci.DataTaskListener
    public void onCancelDataTask(String str, NetworkSession networkSession) {
        C64183Eo r3 = (C64183Eo) this.A00.A07.get(str);
        if (r3 != null) {
            r3.A01(NetworkUtils.newErrorURLResponse(r3.A04), C12990iw.A0i("Task cancelled."), null);
        }
    }

    @Override // com.facebook.msys.mci.DataTaskListener
    public void onNewTask(DataTask dataTask, NetworkSession networkSession) {
        try {
            this.A00.A05.Ab2(new RunnableBRunnable0Shape3S0300000_I1(this, dataTask, networkSession, 35));
        } catch (RejectedExecutionException e) {
            Log.e("wa-msys/NetworkSession: DataTask rejected for execution", e);
            throw e;
        }
    }

    @Override // com.facebook.msys.mci.DataTaskListener
    public void onUpdateStreamingDataTask(byte[] bArr, String str, NetworkSession networkSession) {
        this.A00.A05.Ab2(new RunnableBRunnable0Shape1S1200000_I1(bArr, this, str, 17));
    }
}
