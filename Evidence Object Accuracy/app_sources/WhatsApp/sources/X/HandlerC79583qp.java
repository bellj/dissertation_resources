package X;

import android.app.PendingIntent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

/* renamed from: X.3qp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class HandlerC79583qp extends HandlerC73383g9 {
    public final /* synthetic */ AbstractC95064d1 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC79583qp(Looper looper, AbstractC95064d1 r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        C56492ky r1;
        Object obj;
        C56492ky r12;
        AbstractC95064d1 r2;
        String str;
        IBinder iBinder;
        String interfaceDescriptor;
        AbstractC95064d1 r8;
        String A05;
        AbstractC95064d1 r22 = this.A00;
        if (r22.A0C.get() != message.arg1) {
            int i = message.what;
            if (!(i == 2 || i == 1 || i == 7)) {
                return;
            }
        } else {
            int i2 = message.what;
            if (!(i2 == 1 || i2 == 7 || i2 == 4 || i2 == 5) || r22.AJG()) {
                int i3 = message.what;
                PendingIntent pendingIntent = null;
                if (i3 == 4) {
                    r22.A07 = new C56492ky(message.arg2);
                    if (!r22.A0D) {
                        String A052 = r22.A05();
                        if (!TextUtils.isEmpty(A052) && !TextUtils.isEmpty(null)) {
                            try {
                                Class.forName(A052);
                                if (!r22.A0D) {
                                    r22.A09(null, 3);
                                    return;
                                }
                            } catch (ClassNotFoundException unused) {
                            }
                        }
                    }
                } else if (i3 != 5) {
                    if (i3 == 3) {
                        Object obj2 = message.obj;
                        if (obj2 instanceof PendingIntent) {
                            pendingIntent = (PendingIntent) obj2;
                        }
                        r1 = new C56492ky(message.arg2, pendingIntent);
                        r22.A08.AV1(r1);
                        r22.A01 = r1.A01;
                        r22.A05 = System.currentTimeMillis();
                        return;
                    } else if (i3 == 6) {
                        r22.A09(null, 5);
                        AbstractC115055Qa r0 = r22.A0J;
                        if (r0 != null) {
                            ((C108374yx) r0).A00.onConnectionSuspended(message.arg2);
                        }
                        r22.A00 = message.arg2;
                        r22.A03 = System.currentTimeMillis();
                        AbstractC95064d1.A01(null, r22, 5, 1);
                        return;
                    } else if (i3 != 2 || r22.isConnected()) {
                        int i4 = message.what;
                        if (i4 == 2 || i4 == 1 || i4 == 7) {
                            AnonymousClass4VW r3 = (AnonymousClass4VW) message.obj;
                            synchronized (r3) {
                                obj = r3.A00;
                                if (r3.A01) {
                                    String obj3 = r3.toString();
                                    StringBuilder A0t = C12980iv.A0t(obj3.length() + 47);
                                    A0t.append("Callback proxy ");
                                    A0t.append(obj3);
                                    Log.w("GmsClient", C12960it.A0d(" being reused. This is not safe.", A0t));
                                }
                            }
                            if (obj != null) {
                                try {
                                    AbstractC78733pS r23 = (AbstractC78733pS) r3;
                                    int i5 = r23.A00;
                                    PendingIntent pendingIntent2 = null;
                                    if (i5 != 0) {
                                        r23.A02.A09(null, 1);
                                        Bundle bundle = r23.A01;
                                        if (bundle != null) {
                                            pendingIntent2 = (PendingIntent) bundle.getParcelable("pendingIntent");
                                        }
                                        r12 = new C56492ky(i5, pendingIntent2);
                                    } else if (!(r23 instanceof C78713pQ)) {
                                        C78723pR r02 = (C78723pR) r23;
                                        try {
                                            iBinder = r02.A00;
                                            C13020j0.A01(iBinder);
                                            interfaceDescriptor = iBinder.getInterfaceDescriptor();
                                            r8 = r02.A01;
                                            A05 = r8.A05();
                                        } catch (RemoteException unused2) {
                                            str = "service probably died";
                                        }
                                        if (!A05.equals(interfaceDescriptor)) {
                                            StringBuilder A0t2 = C12980iv.A0t(C12970iu.A07(A05) + 34 + C12970iu.A07(interfaceDescriptor));
                                            A0t2.append("service descriptor mismatch: ");
                                            A0t2.append(A05);
                                            A0t2.append(" vs. ");
                                            str = C12960it.A0d(interfaceDescriptor, A0t2);
                                            Log.w("GmsClient", str);
                                            r23.A02.A09(null, 1);
                                            r12 = new C56492ky(8, null);
                                        } else {
                                            IInterface A04 = r8.A04(iBinder);
                                            if (A04 != null && (AbstractC95064d1.A01(A04, r8, 2, 4) || AbstractC95064d1.A01(A04, r8, 3, 4))) {
                                                r8.A07 = null;
                                                AbstractC115055Qa r03 = r8.A0J;
                                                if (r03 != null) {
                                                    ((C108374yx) r03).A00.onConnected(null);
                                                }
                                            }
                                            r23.A02.A09(null, 1);
                                            r12 = new C56492ky(8, null);
                                        }
                                    } else {
                                        ((C78713pQ) r23).A00.A08.AV1(C56492ky.A04);
                                    }
                                    if (!(r23 instanceof C78713pQ)) {
                                        r2 = ((C78723pR) r23).A01;
                                        AbstractC115065Qb r04 = r2.A0K;
                                        if (r04 != null) {
                                            ((C108384yy) r04).A00.onConnectionFailed(r12);
                                        }
                                    } else {
                                        r2 = ((C78713pQ) r23).A00;
                                        r2.A08.AV1(r12);
                                    }
                                    r2.A01 = r12.A01;
                                    r2.A05 = System.currentTimeMillis();
                                } catch (RuntimeException e) {
                                    throw e;
                                }
                            }
                            synchronized (r3) {
                                r3.A01 = true;
                            }
                            r3.A00();
                            return;
                        }
                        StringBuilder A0t3 = C12980iv.A0t(45);
                        A0t3.append("Don't know how to handle message: ");
                        A0t3.append(i4);
                        Log.wtf("GmsClient", A0t3.toString(), new Exception());
                        return;
                    }
                }
                r1 = r22.A07;
                if (r1 == null) {
                    r1 = new C56492ky(8);
                }
                r22.A08.AV1(r1);
                r22.A01 = r1.A01;
                r22.A05 = System.currentTimeMillis();
                return;
            }
        }
        ((AnonymousClass4VW) message.obj).A00();
    }
}
