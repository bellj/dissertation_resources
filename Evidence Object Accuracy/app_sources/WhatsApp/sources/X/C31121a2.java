package X;

import android.os.SystemClock;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.Enumeration;
import javax.crypto.SecretKey;

/* renamed from: X.1a2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31121a2 extends AbstractC31111a1 {
    public KeyStore A00;
    public final C14850m9 A01;
    public final String A02 = "aes_auth_key";

    public C31121a2(AbstractC15710nm r2, C15450nH r3, C14850m9 r4, C16630pM r5) {
        super(r2, r3, r5);
        this.A01 = r4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a5  */
    @Override // X.AbstractC31111a1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C31091Zz A00(byte[] r11) {
        /*
            r10 = this;
            java.lang.String r3 = "ged"
            java.security.KeyStore r1 = r10.A04()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            java.lang.String r4 = r10.A02     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            boolean r0 = r1.containsAlias(r4)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            if (r0 == 0) goto L_0x001b
            java.lang.Class<java.security.KeyStore$SecretKeyEntry> r0 = java.security.KeyStore.SecretKeyEntry.class
            boolean r0 = r1.entryInstanceOf(r4, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            if (r0 == 0) goto L_0x001b
            javax.crypto.SecretKey r2 = r10.A05()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            goto L_0x005b
        L_0x001b:
            java.lang.String r1 = "AES"
            java.lang.String r0 = "AndroidKeyStore"
            javax.crypto.KeyGenerator r5 = javax.crypto.KeyGenerator.getInstance(r1, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r0 = 3
            android.security.keystore.KeyGenParameterSpec$Builder r2 = new android.security.keystore.KeyGenParameterSpec$Builder     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r2.<init>(r4, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r6 = 1
            java.lang.String[] r1 = new java.lang.String[r6]     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            java.lang.String r0 = "GCM"
            r4 = 0
            r1[r4] = r0     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            android.security.keystore.KeyGenParameterSpec$Builder r2 = r2.setBlockModes(r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            java.lang.String[] r1 = new java.lang.String[r6]     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            java.lang.String r0 = "NoPadding"
            r1[r4] = r0     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            android.security.keystore.KeyGenParameterSpec$Builder r0 = r2.setEncryptionPaddings(r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            android.security.keystore.KeyGenParameterSpec$Builder r2 = r0.setUserAuthenticationRequired(r4)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r0 = 28
            if (r1 < r0) goto L_0x0050
            android.security.keystore.KeyGenParameterSpec$Builder r0 = r2.setUserConfirmationRequired(r4)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r0.setUserPresenceRequired(r4)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
        L_0x0050:
            android.security.keystore.KeyGenParameterSpec r0 = r2.build()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r5.init(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            javax.crypto.SecretKey r2 = r5.generateKey()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
        L_0x005b:
            if (r2 == 0) goto L_0x0078
            java.lang.String r0 = "AES/GCM/NoPadding"
            javax.crypto.Cipher r1 = javax.crypto.Cipher.getInstance(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r0 = 1
            r1.init(r0, r2)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            byte[] r7 = r1.getIV()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            byte[] r6 = r1.doFinal(r11)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r9 = 0
            r5 = 0
            r8 = r5
            X.1Zz r4 = new X.1Zz     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r4.<init>(r5, r6, r7, r8, r9)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            return r4
        L_0x0078:
            java.lang.String r1 = "could not get key store entry"
            java.security.KeyStoreException r0 = new java.security.KeyStoreException     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            r0.<init>(r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
            throw r0     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException | NullPointerException -> 0x008b, IOException | CertificateException | NoSuchProviderException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0080
        L_0x0080:
            r2 = move-exception
            java.lang.String r0 = "EncryptedKeyHelperAESKeyStore/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "key store issue on decryption"
            goto L_0x0095
        L_0x008b:
            r2 = move-exception
            java.lang.String r0 = "EncryptedKeyHelperAESKeyStore/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "crypto issue on encryption"
        L_0x0095:
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0, r2)
            java.lang.Throwable r0 = r2.getCause()
            if (r0 == 0) goto L_0x00a9
            java.lang.Throwable r2 = r2.getCause()
        L_0x00a9:
            r10.A02(r3, r2)
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31121a2.A00(byte[]):X.1Zz");
    }

    @Override // X.AbstractC31111a1
    public void A01() {
        try {
            A04().deleteEntry(this.A02);
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException unused) {
            StringBuilder sb = new StringBuilder("EncryptedKeyHelperAESKeyStore/");
            sb.append("failed to clear keyStore");
            Log.e(sb.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    @Override // X.AbstractC31111a1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] A03(X.EnumC31131a3 r9, X.C31091Zz r10) {
        /*
            r8 = this;
            java.lang.String r3 = "gd-"
            int r0 = r10.A00
            r7 = 0
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "EncryptedKeyHelperAESKeyStore/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "getDecrypted invalid type"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
            return r7
        L_0x001b:
            javax.crypto.SecretKey r6 = r8.A05()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            if (r6 == 0) goto L_0x003b
            java.lang.String r0 = "AES/GCM/NoPadding"
            javax.crypto.Cipher r5 = javax.crypto.Cipher.getInstance(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            r4 = 2
            r2 = 128(0x80, float:1.794E-43)
            byte[] r1 = r10.A03     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            javax.crypto.spec.GCMParameterSpec r0 = new javax.crypto.spec.GCMParameterSpec     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            r0.<init>(r2, r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            r5.init(r4, r6, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            byte[] r0 = r10.A02     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            byte[] r0 = r5.doFinal(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            return r0
        L_0x003b:
            java.lang.String r1 = "could not get key store entry"
            java.security.KeyStoreException r0 = new java.security.KeyStoreException     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            r0.<init>(r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
            throw r0     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | NullPointerException -> 0x004e, IOException | CertificateException | ProviderException | UnrecoverableEntryException | KeyStoreException -> 0x0043
        L_0x0043:
            r2 = move-exception
            java.lang.String r0 = "EncryptedKeyHelperAESKeyStore/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "key store issue on decryption"
            goto L_0x0058
        L_0x004e:
            r2 = move-exception
            java.lang.String r0 = "EncryptedKeyHelperAESKeyStore/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "crypto issue on decryption"
        L_0x0058:
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0, r2)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            int r0 = r9.ordinal()
            switch(r0) {
                case 0: goto L_0x0089;
                case 1: goto L_0x008c;
                default: goto L_0x0071;
            }
        L_0x0071:
            java.lang.String r0 = "unknown"
        L_0x0074:
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.Throwable r0 = r2.getCause()
            if (r0 == 0) goto L_0x0085
            java.lang.Throwable r2 = r2.getCause()
        L_0x0085:
            r8.A02(r1, r2)
            return r7
        L_0x0089:
            java.lang.String r0 = "selftest"
            goto L_0x0074
        L_0x008c:
            java.lang.String r0 = "active"
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31121a2.A03(X.1a3, X.1Zz):byte[]");
    }

    public final KeyStore A04() {
        if (this.A00 == null || !this.A01.A07(1862)) {
            KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
            this.A00 = instance;
            instance.load(null);
        }
        return this.A00;
    }

    public final SecretKey A05() {
        try {
            return A06(A04());
        } catch (UnrecoverableKeyException e) {
            e = e;
            StringBuilder sb = new StringBuilder("EncryptedKeyHelperAESKeyStore/");
            sb.append(" KeyStore error, will wait and retry with new keystore");
            Log.e(sb.toString());
            if (e.getCause() != null) {
                e = e.getCause();
            }
            A02("gd", e);
            SystemClock.sleep(50);
            return A06(A04());
        }
    }

    public final SecretKey A06(KeyStore keyStore) {
        String str = this.A02;
        KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry(str, null);
        if (secretKeyEntry != null) {
            return secretKeyEntry.getSecretKey();
        }
        Enumeration<String> aliases = keyStore.aliases();
        StringBuilder sb = new StringBuilder("Missing key alias ");
        sb.append(str);
        sb.append("; available aliases = ");
        sb.append(TextUtils.join(",", Collections.list(aliases)));
        throw new KeyStoreException(sb.toString());
    }
}
