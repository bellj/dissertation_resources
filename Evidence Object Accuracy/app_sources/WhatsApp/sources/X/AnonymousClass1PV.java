package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;

/* renamed from: X.1PV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PV implements AbstractC28771Oy {
    public final /* synthetic */ AnonymousClass1PX A00;
    public final /* synthetic */ C21430xP A01;
    public final /* synthetic */ AnonymousClass1PW A02;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
    }

    public AnonymousClass1PV(AnonymousClass1PX r1, C21430xP r2, AnonymousClass1PW r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r8, C28781Oz r9) {
        if (r8.A00 == 0) {
            this.A01.A0L.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, r9, this.A02, this.A00, 14));
            return;
        }
        this.A01.A0L.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this.A00, 24));
    }
}
