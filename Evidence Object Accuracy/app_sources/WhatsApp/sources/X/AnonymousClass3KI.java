package X;

import android.content.DialogInterface;
import com.whatsapp.twofactor.SetEmailFragment;
import com.whatsapp.util.Log;

/* renamed from: X.3KI  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3KI implements DialogInterface.OnClickListener {
    public final /* synthetic */ SetEmailFragment.ConfirmSkipEmailDialog A00;

    public /* synthetic */ AnonymousClass3KI(SetEmailFragment.ConfirmSkipEmailDialog confirmSkipEmailDialog) {
        this.A00 = confirmSkipEmailDialog;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        SetEmailFragment setEmailFragment = (SetEmailFragment) this.A00.A08();
        Log.i("setemailfragment/do-skip");
        C12990iw.A1G(setEmailFragment.A03);
        setEmailFragment.A06.A2e();
    }
}
