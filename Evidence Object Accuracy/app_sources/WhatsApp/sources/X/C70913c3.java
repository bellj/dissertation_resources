package X;

import com.whatsapp.quickcontact.QuickContactActivity;

/* renamed from: X.3c3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70913c3 implements AbstractC47972Dm {
    public final /* synthetic */ QuickContactActivity A00;

    @Override // X.AbstractC47972Dm
    public void ANV() {
    }

    public C70913c3(QuickContactActivity quickContactActivity) {
        this.A00 = quickContactActivity;
    }

    @Override // X.AbstractC47972Dm
    public void ANX(AnonymousClass1YT r4) {
        QuickContactActivity quickContactActivity = this.A00;
        C15580nU r1 = quickContactActivity.A0R;
        if (r1 != null && r1.equals(r4.A04)) {
            quickContactActivity.A0e = r4;
            if (!C29941Vi.A00(r4.A06, quickContactActivity.A0k)) {
                quickContactActivity.A0k = r4.A06;
                AbstractC51412Uo r0 = quickContactActivity.A0Y;
                r0.A00();
                r0.A03();
            }
        }
    }
}
