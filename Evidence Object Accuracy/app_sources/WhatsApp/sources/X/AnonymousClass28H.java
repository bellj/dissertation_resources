package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.28H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28H {
    public AnonymousClass28J A00;
    public Boolean A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final UserJid A05;
    public final String A06;
    public final String A07;

    public AnonymousClass28H(AnonymousClass28J r2, UserJid userJid, String str, String str2, int i, int i2, int i3) {
        this.A05 = userJid;
        this.A06 = str;
        this.A02 = i;
        this.A04 = i2;
        this.A03 = i3;
        this.A07 = str2;
        this.A01 = null;
        this.A00 = r2;
    }

    public AnonymousClass28H(UserJid userJid, Boolean bool, int i, int i2) {
        this.A05 = userJid;
        this.A06 = null;
        this.A02 = 6;
        this.A04 = i;
        this.A03 = i2;
        this.A07 = null;
        this.A01 = bool;
    }

    public AnonymousClass28H(UserJid userJid, String str, String str2, int i, int i2, int i3) {
        this.A05 = userJid;
        this.A06 = str;
        this.A02 = i;
        this.A04 = i2;
        this.A03 = i3;
        this.A07 = str2;
        this.A01 = null;
    }
}
