package X;

import com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet;

/* renamed from: X.4rE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103734rE implements AnonymousClass07L {
    public final /* synthetic */ GroupCallParticipantPickerSheet A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C103734rE(GroupCallParticipantPickerSheet groupCallParticipantPickerSheet) {
        this.A00 = groupCallParticipantPickerSheet;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        this.A00.A34(str);
        return false;
    }
}
