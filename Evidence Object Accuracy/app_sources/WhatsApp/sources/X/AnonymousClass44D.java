package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44D extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AnonymousClass34b A01;

    public AnonymousClass44D(SearchViewModel searchViewModel, AnonymousClass34b r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
