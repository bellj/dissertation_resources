package X;

import android.os.Bundle;
import android.os.Parcelable;

/* renamed from: X.05k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C011105k implements AnonymousClass05A {
    public final /* synthetic */ ActivityC000900k A00;

    public C011105k(ActivityC000900k r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05A
    public Bundle AbH() {
        Bundle bundle = new Bundle();
        ActivityC000900k r2 = this.A00;
        r2.A0X();
        r2.A04.A04(AnonymousClass074.ON_STOP);
        Parcelable A04 = r2.A03.A00.A03.A04();
        if (A04 != null) {
            bundle.putParcelable(ActivityC000900k.A05, A04);
        }
        return bundle;
    }
}
