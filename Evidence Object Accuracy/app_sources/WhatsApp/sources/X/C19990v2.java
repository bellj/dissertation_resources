package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0v2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19990v2 {
    public boolean A00;
    public final C14850m9 A01;
    public final ConcurrentHashMap A02 = new ConcurrentHashMap();
    public volatile C29551Tn A03;
    public volatile boolean A04;

    public C19990v2(C14850m9 r2) {
        this.A01 = r2;
    }

    public int A00(AbstractC14640lm r2) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r2);
        if (r0 == null) {
            return 0;
        }
        return r0.A06;
    }

    public int A01(AbstractC14640lm r2) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r2);
        if (r0 == null) {
            return 0;
        }
        return r0.A0Y.expiration;
    }

    public int A02(GroupJid groupJid) {
        if (!this.A01.A07(982)) {
            return 0;
        }
        return A03(groupJid);
    }

    public int A03(GroupJid groupJid) {
        AnonymousClass1PE r0;
        if (!C15380n4.A0J(groupJid) || (r0 = (AnonymousClass1PE) A0B().get(groupJid)) == null) {
            return 0;
        }
        return r0.A01;
    }

    public long A04(AbstractC14640lm r3) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r3);
        if (r0 == null) {
            return 1;
        }
        return r0.A0O;
    }

    public long A05(AbstractC14640lm r3) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r3);
        if (r0 == null) {
            return 0;
        }
        return r0.A0W;
    }

    public synchronized AnonymousClass1PE A06(AbstractC14640lm r2) {
        return r2 == null ? null : (AnonymousClass1PE) A0B().get(r2);
    }

    public C29541Tm A07(AbstractC14640lm r6) {
        C29541Tm r3;
        AnonymousClass1PE r4 = (AnonymousClass1PE) A0B().get(r6);
        if (r4 == null) {
            return new C29541Tm(0, 0, 0);
        }
        synchronized (r4) {
            r3 = new C29541Tm(r4.A06, r4.A07, r4.A08);
        }
        return r3;
    }

    public AnonymousClass1PG A08(UserJid userJid) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(userJid);
        if (r0 == null) {
            return null;
        }
        return r0.A0Y;
    }

    public String A09(AbstractC14640lm r2) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r2);
        if (r0 == null) {
            return null;
        }
        return r0.A0e;
    }

    public synchronized Set A0A() {
        return A0B().keySet();
    }

    public final ConcurrentHashMap A0B() {
        C20650w6 r6;
        C16490p7 r2;
        Map map;
        if (this.A03 != null || this.A04) {
            synchronized (this) {
                if (this.A03 != null) {
                    C29551Tn r1 = this.A03;
                    this.A04 = true;
                    this.A03 = null;
                    try {
                        r6 = r1.A00;
                        r2 = r6.A0F;
                        r2.A04();
                    } catch (C29571Tp unused) {
                    } catch (Throwable th) {
                        this.A04 = false;
                        throw th;
                    }
                    if (r2.A01) {
                        try {
                            map = r6.A08.A07();
                            r6.A0A.A01(map);
                        } catch (IllegalStateException e) {
                            Log.e("msgstore-manager/finish", e);
                            r2.A04();
                            r2.A05.close();
                            r6.A0I.A01();
                            map = r6.A08.A07();
                            r6.A0A.A01(map);
                        }
                        for (Map.Entry entry : map.entrySet()) {
                            this.A02.put((AbstractC14640lm) entry.getKey(), (AnonymousClass1PE) entry.getValue());
                        }
                        ArrayList arrayList = new ArrayList(this.A02.keySet());
                        r6.A03.A04(arrayList);
                        StringBuilder sb = new StringBuilder("msgstore-manager/initialize/chats ");
                        sb.append(arrayList.size());
                        Log.i(sb.toString());
                        this.A00 = true;
                        this.A04 = false;
                    } else {
                        Log.w("msgstore-manager/finish/db is not ready yet", new Throwable());
                        throw new C29571Tp();
                    }
                }
            }
        }
        return this.A02;
    }

    public synchronized void A0C(AnonymousClass1PE r2, AbstractC14640lm r3) {
        if (r3 != null) {
            A0B().put(r3, r2);
        }
    }

    public boolean A0D(AbstractC14640lm r3) {
        return A0B().containsKey(r3) && !A0F(r3);
    }

    public boolean A0E(AbstractC14640lm r3) {
        AnonymousClass1PE r0 = (AnonymousClass1PE) A0B().get(r3);
        return r0 != null && r0.A0f;
    }

    public boolean A0F(AbstractC14640lm r9) {
        AnonymousClass1PE r7 = (AnonymousClass1PE) A0B().get(r9);
        if (r7 == null) {
            return true;
        }
        long j = r7.A0N;
        if (j == 0 && r7.A0E == Long.MIN_VALUE) {
            return false;
        }
        long j2 = r7.A0E;
        return j2 == r7.A0F && j2 >= j;
    }
}
