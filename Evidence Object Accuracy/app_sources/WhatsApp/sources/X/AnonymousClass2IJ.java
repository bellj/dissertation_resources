package X;

/* renamed from: X.2IJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2IJ {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2IJ(C48302Fl r1) {
        this.A00 = r1;
    }

    public AnonymousClass3FT A00(ActivityC13810kN r15, C63633Ci r16, C15580nU r17) {
        AnonymousClass01J r1 = this.A00.A03;
        C14900mE r2 = (C14900mE) r1.A8X.get();
        C20710wC r10 = (C20710wC) r1.A8m.get();
        C21320xE r8 = (C21320xE) r1.A4Y.get();
        C15600nX r9 = (C15600nX) r1.A8x.get();
        return new AnonymousClass3FT(r15, r2, (C15570nT) r1.AAr.get(), r16, (C15550nR) r1.A45.get(), (C15610nY) r1.AMe.get(), (C18640sm) r1.A3u.get(), r8, r9, r10, r17, (C20660w7) r1.AIB.get(), (C14860mA) r1.ANU.get());
    }
}
