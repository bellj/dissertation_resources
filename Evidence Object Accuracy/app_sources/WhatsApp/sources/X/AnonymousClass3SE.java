package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.PlaceInfo;

/* renamed from: X.3SE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SE implements AbstractC12150hS {
    public final View A00;
    public final /* synthetic */ LocationPicker A01;

    public AnonymousClass3SE(LocationPicker locationPicker) {
        this.A01 = locationPicker;
        this.A00 = C12960it.A0F(locationPicker.getLayoutInflater(), null, R.layout.place_map_info_window);
    }

    @Override // X.AbstractC12150hS
    public View ADS(AnonymousClass03R r6) {
        View view = this.A00;
        TextView A0J = C12960it.A0J(view, R.id.place_name);
        TextView A0J2 = C12960it.A0J(view, R.id.place_address);
        Object obj = r6.A0L;
        if (obj instanceof PlaceInfo) {
            PlaceInfo placeInfo = (PlaceInfo) obj;
            A0J.setText(placeInfo.A06);
            A0J2.setText(placeInfo.A0B);
        }
        return view;
    }
}
