package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3rr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80193rr extends AnonymousClass5I0<Double> implements AnonymousClass5Z6<Double>, AbstractC115265Qv, RandomAccess {
    public static final C80193rr A02;
    public int A00;
    public double[] A01;

    static {
        C80193rr r0 = new C80193rr(new double[0], 0);
        A02 = r0;
        ((AnonymousClass5I0) r0).A00 = false;
    }

    public C80193rr() {
        this(new double[10], 0);
    }

    public C80193rr(double[] dArr, int i) {
        this.A01 = dArr;
        this.A00 = i;
    }

    public final void A03(double d) {
        A02();
        int i = this.A00;
        double[] dArr = this.A01;
        if (i == dArr.length) {
            double[] dArr2 = new double[((i * 3) >> 1) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.A01 = dArr2;
            dArr = dArr2;
        }
        int i2 = this.A00;
        this.A00 = i2 + 1;
        dArr[i2] = d;
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= this.A00) {
            return new C80193rr(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double A00 = C72453ed.A00(obj);
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        if (i2 < dArr.length) {
            C72463ee.A0T(dArr, i, i2);
        } else {
            double[] dArr2 = new double[((i2 * 3) >> 1) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.A01, i, dArr2, i + 1, this.A00 - i);
            this.A01 = dArr2;
        }
        this.A01[i] = A00;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* synthetic */ boolean add(Object obj) {
        A03(C72453ed.A00(obj));
        return true;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C80193rr)) {
            return super.addAll(collection);
        }
        C80193rr r7 = (C80193rr) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.A01;
            if (i3 > dArr.length) {
                dArr = Arrays.copyOf(dArr, i3);
                this.A01 = dArr;
            }
            System.arraycopy(r7.A01, 0, dArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean contains(Object obj) {
        return C12980iv.A1V(indexOf(obj), -1);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C80193rr)) {
                return super.equals(obj);
            }
            C80193rr r11 = (C80193rr) obj;
            int i = this.A00;
            if (i == r11.A00) {
                double[] dArr = r11.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (Double.doubleToLongBits(this.A01[i2]) == Double.doubleToLongBits(dArr[i2])) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return Double.valueOf(this.A01[i]);
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + C72453ed.A0B(Double.doubleToLongBits(this.A01[i2]));
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Double) {
            double A00 = C72453ed.A00(obj);
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.A01[i] == A00) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        double d = dArr[i];
        AnonymousClass5I0.A01(dArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            double[] dArr = this.A01;
            System.arraycopy(dArr, i2, dArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        double A00 = C72453ed.A00(obj);
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        double[] dArr = this.A01;
        double d = dArr[i];
        dArr[i] = A00;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }
}
