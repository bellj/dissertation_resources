package X;

import android.app.Activity;
import android.content.Intent;
import com.whatsapp.businessdirectory.view.activity.DirectoryBusinessChainingActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Dn  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Dn {
    public final C15570nT A00;
    public final C251118d A01;

    public AnonymousClass3Dn(C15570nT r1, C251118d r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(Activity activity, C15370n3 r6) {
        if (r6.A0D != null && A01(r6) && r6.A0D() != null) {
            Jid jid = r6.A0D;
            String A0D = r6.A0D();
            Intent A0D2 = C12990iw.A0D(activity, DirectoryBusinessChainingActivity.class);
            A0D2.putExtra("directory_biz_chaining_jid", jid);
            A0D2.putExtra("directory_biz_chaining_name", A0D);
            activity.startActivity(A0D2);
        }
    }

    public boolean A01(C15370n3 r3) {
        C14850m9 r1 = this.A01.A00;
        if (!r1.A07(450) || !r1.A07(1616) || !r3.A0J()) {
            return false;
        }
        if (r1.A07(450) && r1.A07(1616) && r1.A07(1764)) {
            return true;
        }
        Jid A0B = r3.A0B(UserJid.class);
        if (A0B == null || !A0B.getRawString().startsWith("5511")) {
            return false;
        }
        return true;
    }
}
