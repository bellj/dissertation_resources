package X;

import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.io.File;
import java.io.IOException;
import java.util.Collections;

/* renamed from: X.3XN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XN implements AbstractC39381po {
    public final /* synthetic */ AnonymousClass239 A00;

    public AnonymousClass3XN(AnonymousClass239 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC39381po
    public void AQA(Exception exc) {
        AnonymousClass239 r2 = this.A00;
        WebPagePreviewView webPagePreviewView = r2.A07;
        webPagePreviewView.setImageProgressBarVisibility(false);
        webPagePreviewView.setImageThumbVisibility(true);
        C89134Iu r1 = r2.A02;
        if (exc instanceof IOException) {
            ((ActivityC13810kN) r1.A00).A05.A08(R.string.generic_network_error_retry_later, 0);
        }
    }

    @Override // X.AbstractC39381po
    public void AQX(File file, String str, byte[] bArr) {
        AnonymousClass239 r2 = this.A00;
        WebPagePreviewView webPagePreviewView = r2.A07;
        webPagePreviewView.setImageProgressBarVisibility(false);
        webPagePreviewView.setImageThumbVisibility(true);
        if (file == null) {
            Log.e("ConversationShellWebPagePreviewController/onFileReceived/gif is null");
            return;
        }
        Conversation conversation = r2.A02.A00;
        conversation.A2E(AnonymousClass3AH.A00(conversation, conversation.A2w, conversation.A20.A07, file, Collections.singletonList(conversation.A2s)), 27);
    }
}
