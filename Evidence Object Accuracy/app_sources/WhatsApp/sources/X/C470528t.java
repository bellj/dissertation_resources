package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.28t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C470528t implements IInterface {
    public IBinder A00;

    public C470528t(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
