package X;

import android.util.Property;
import android.view.View;

/* renamed from: X.0Ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02250Ap extends Property {
    public C02250Ap(Class cls) {
        super(cls, "alpha");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return Float.valueOf(((View) obj).getAlpha());
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        ((View) obj).setAlpha(((Number) obj2).floatValue());
    }
}
