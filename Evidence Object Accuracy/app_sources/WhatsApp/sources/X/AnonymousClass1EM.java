package X;

import android.graphics.Bitmap;
import com.whatsapp.superpack.WhatsAppObiInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.1EM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EM {
    public static final AnonymousClass00E A02 = new AnonymousClass00E(1, 5, 650);
    public static final AnonymousClass00E A03 = new AnonymousClass00E(1, 50, 1000);
    public static final int[] A04 = {331, 756, 1563, 2546, 2984};
    public final C14850m9 A00;
    public final C16120oU A01;

    public AnonymousClass1EM(C14850m9 r1, C16120oU r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public Bitmap A00(InputStream inputStream, int i) {
        AnonymousClass00E r3;
        String str;
        int i2;
        try {
            WhatsAppObiInputStream openStream = WhatsAppObiInputStream.openStream(inputStream, inputStream.available());
            ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
            if (openStream.read(order.array()) == 8) {
                AnonymousClass4OV r0 = new AnonymousClass4OV(order.getInt(), order.getInt());
                int i3 = r0.A01;
                if (i3 <= 0 || (i2 = r0.A00) <= 0) {
                    throw new IOException("Metadata height/width is zero or less");
                } else if (i3 > 4096 || i2 > 4096) {
                    throw new IOException("Metadata height/width bigger than max value");
                } else {
                    int i4 = (i3 * i2) << 2;
                    if (i4 <= 16777216) {
                        ByteBuffer allocate = ByteBuffer.allocate(i4);
                        if (openStream.read(allocate.array(), 0, i4) == i4) {
                            Bitmap createBitmap = Bitmap.createBitmap(i3, i2, Bitmap.Config.ARGB_8888);
                            createBitmap.copyPixelsFromBuffer(allocate);
                            openStream.close();
                            return createBitmap;
                        }
                        throw new IOException("Bytes read from stream not equal metadata size in bytes");
                    }
                    throw new IOException("Metadata has more bytes than max allowed");
                }
            } else {
                throw new IOException("Dim bytes read not 8");
            }
        } catch (IOException e) {
            String obj = e.toString();
            if (i == 0) {
                r3 = A02;
                str = "doodle_emoji";
            } else if (i != 1) {
                return null;
            } else {
                r3 = A03;
                str = "regular_emoji";
            }
            AnonymousClass1Ti r2 = new AnonymousClass1Ti();
            r2.A01 = str;
            r2.A02 = obj;
            this.A01.A0B(r2, r3, false);
            return null;
        }
    }
}
