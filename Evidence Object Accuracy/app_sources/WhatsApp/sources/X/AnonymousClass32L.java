package X;

import android.content.res.Resources;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.group.GroupChatInfo;
import java.util.ArrayList;

/* renamed from: X.32L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32L extends AnonymousClass4V1 {
    public final WaTextView A00;
    public final /* synthetic */ GroupChatInfo A01;

    public AnonymousClass32L(View view, GroupChatInfo groupChatInfo) {
        this.A01 = groupChatInfo;
        this.A00 = C12960it.A0N(view, R.id.text);
    }

    @Override // X.AnonymousClass4V1
    public void A00(AbstractC36121jM r7, C92434Vw r8, ArrayList arrayList) {
        super.A00 = r7;
        if (r7 instanceof C36151jP) {
            int i = ((C36151jP) r7).A00;
            WaTextView waTextView = this.A00;
            Resources resources = this.A01.getResources();
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, i, 0);
            C12980iv.A15(resources, waTextView, A1b, R.plurals.view_all, i);
        } else if (r7 instanceof C36141jO) {
            C12970iu.A19(this.A01, this.A00, R.string.past_participants_view);
        }
    }
}
