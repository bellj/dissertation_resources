package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import com.whatsapp.TextData;

/* renamed from: X.2ZY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZY extends Drawable {
    public static final AbstractC116265Ut A08;
    public float A00;
    public int A01;
    public int A02;
    public Layout A03;
    public final Paint A04 = C12960it.A0A();
    public final Typeface A05;
    public final TextData A06;
    public final CharSequence A07;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            A08 = new C69953aV();
        } else {
            A08 = new C1114159h();
        }
    }

    public AnonymousClass2ZY(Context context, Typeface typeface, TextData textData, AnonymousClass01d r5, AnonymousClass19M r6, C16630pM r7, String str) {
        this.A07 = AnonymousClass1US.A01(C42971wC.A03(r5, r7, AbstractC36671kL.A05(context, r6, str)));
        this.A06 = textData;
        this.A05 = typeface;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int i;
        Rect bounds = getBounds();
        Paint paint = this.A04;
        TextData textData = this.A06;
        if (textData == null || (i = textData.backgroundColor) == 0) {
            i = 1711276032;
        }
        C12970iu.A16(i, paint);
        canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), ((float) (bounds.width() >> 1)) - this.A00, paint);
        if (this.A03 != null) {
            canvas.translate((float) ((bounds.width() - this.A03.getWidth()) >> 1), (float) ((bounds.height() - this.A03.getHeight()) >> 1));
            this.A03.draw(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
        if (r4.length() <= 100) goto L_0x0050;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0056  */
    @Override // android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setBounds(int r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            super.setBounds(r9, r10, r11, r12)
            int r11 = r11 - r9
            float r1 = (float) r11
            float r4 = r8.A00
            r0 = 1073741824(0x40000000, float:2.0)
            float r4 = r4 * r0
            float r1 = r1 - r4
            double r0 = (double) r1
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r2 = java.lang.Math.sqrt(r2)
            double r0 = r0 / r2
            int r5 = (int) r0
            int r12 = r12 - r10
            float r0 = (float) r12
            float r0 = r0 - r4
            double r0 = (double) r0
            double r0 = r0 / r2
            int r7 = (int) r0
            int r0 = r8.A01
            if (r0 != r7) goto L_0x0023
            int r0 = r8.A02
            if (r0 != r5) goto L_0x0023
        L_0x0022:
            return
        L_0x0023:
            r8.A02 = r5
            r8.A01 = r7
            r0 = 1
            android.text.TextPaint r6 = new android.text.TextPaint
            r6.<init>(r0)
            int r0 = r7 >> 1
            float r0 = (float) r0
            r6.setTextSize(r0)
            com.whatsapp.TextData r0 = r8.A06
            if (r0 == 0) goto L_0x008c
            int r0 = r0.textColor
            if (r0 == 0) goto L_0x008c
        L_0x003b:
            r6.setColor(r0)
            android.graphics.Typeface r0 = r8.A05
            r6.setTypeface(r0)
            java.lang.CharSequence r4 = r8.A07
            r3 = 0
            if (r4 == 0) goto L_0x0050
            int r1 = r4.length()
            r0 = 100
            if (r1 > r0) goto L_0x007c
        L_0x0050:
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 != 0) goto L_0x0022
            X.C64583Gc.A00(r6, r4)
            X.5Ut r0 = X.AnonymousClass2ZY.A08
            android.text.Layout r0 = r0.A8M(r6, r4, r5)
            r8.A03 = r0
            int r0 = r0.getHeight()
            if (r0 <= r7) goto L_0x0022
            float r2 = r6.getTextSize()
            int r0 = r7 >> 3
            r1 = 2
            int r0 = java.lang.Math.max(r1, r0)
            float r0 = (float) r0
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0081
            int r0 = r4.length()
            int r0 = r0 / r1
        L_0x007c:
            java.lang.CharSequence r4 = r4.subSequence(r3, r0)
            goto L_0x0050
        L_0x0081:
            float r1 = r6.getTextSize()
            r0 = 1065353216(0x3f800000, float:1.0)
            float r1 = r1 - r0
            r6.setTextSize(r1)
            goto L_0x0050
        L_0x008c:
            r0 = -1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2ZY.setBounds(int, int, int, int):void");
    }
}
