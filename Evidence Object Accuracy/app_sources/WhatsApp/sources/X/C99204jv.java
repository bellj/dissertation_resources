package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99204jv implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c != 3) {
                str3 = C95664e9.A09(parcel, str3, c, 4, readInt);
            } else {
                str2 = C95664e9.A08(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78653pG(str, str2, str3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78653pG[i];
    }
}
