package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.WaImageView;
import com.whatsapp.WaRoundCornerImageView;
import com.whatsapp.components.button.ThumbnailPickerButton;
import com.whatsapp.location.ContactLiveLocationThumbnail;
import com.whatsapp.search.views.MessageThumbView;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.1li  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37211li extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC37211li(Context context) {
        super(context, null);
        A00();
    }

    public AbstractC37211li(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC37211li(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        if (this instanceof StickerView) {
            StickerView stickerView = (StickerView) this;
            if (!stickerView.A02) {
                stickerView.A02 = true;
                ((WaImageView) stickerView).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) stickerView.generatedComponent())).A06.ANb.get();
            }
        } else if (this instanceof MessageThumbView) {
            MessageThumbView messageThumbView = (MessageThumbView) this;
            if (!messageThumbView.A03) {
                messageThumbView.A03 = true;
                AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) messageThumbView.generatedComponent())).A06;
                ((WaImageView) messageThumbView).A00 = (AnonymousClass018) r1.ANb.get();
                messageThumbView.A02 = (AnonymousClass19O) r1.ACO.get();
            }
        } else if (this instanceof AnonymousClass2UZ) {
            AnonymousClass2UZ r12 = (AnonymousClass2UZ) this;
            if (!r12.A0B) {
                r12.A0B = true;
                ((WaImageView) r12).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r12.generatedComponent())).A06.ANb.get();
            }
        } else if (this instanceof AbstractC51352Ua) {
            AbstractC51352Ua r13 = (AbstractC51352Ua) this;
            if (r13 instanceof AbstractC51362Uc) {
                AbstractC51362Uc r14 = (AbstractC51362Uc) r13;
                if (r14 instanceof AbstractC51382Uh) {
                    AbstractC51382Uh r15 = (AbstractC51382Uh) r14;
                    if (!r15.A00) {
                        r15.A00 = true;
                        ((WaImageView) r15).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r15.generatedComponent())).A06.ANb.get();
                    }
                } else if (!r14.A00) {
                    r14.A00 = true;
                    ((WaImageView) r14).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r14.generatedComponent())).A06.ANb.get();
                }
            } else if (r13 instanceof ContactLiveLocationThumbnail) {
                ContactLiveLocationThumbnail contactLiveLocationThumbnail = (ContactLiveLocationThumbnail) r13;
                if (!contactLiveLocationThumbnail.A04) {
                    contactLiveLocationThumbnail.A04 = true;
                    ((WaImageView) contactLiveLocationThumbnail).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) contactLiveLocationThumbnail.generatedComponent())).A06.ANb.get();
                }
            } else if (r13 instanceof ThumbnailPickerButton) {
                ThumbnailPickerButton thumbnailPickerButton = (ThumbnailPickerButton) r13;
                if (!thumbnailPickerButton.A01) {
                    thumbnailPickerButton.A01 = true;
                    ((WaImageView) thumbnailPickerButton).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) thumbnailPickerButton.generatedComponent())).A06.ANb.get();
                }
            } else if (r13 instanceof AbstractC51372Ug) {
                AbstractC51372Ug r16 = (AbstractC51372Ug) r13;
                if (!r16.A00) {
                    r16.A00 = true;
                    ((WaImageView) r16).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r16.generatedComponent())).A06.ANb.get();
                }
            } else if (!r13.A00) {
                r13.A00 = true;
                ((WaImageView) r13).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r13.generatedComponent())).A06.ANb.get();
            }
        } else if (this instanceof WaRoundCornerImageView) {
            WaRoundCornerImageView waRoundCornerImageView = (WaRoundCornerImageView) this;
            if (!waRoundCornerImageView.A03) {
                waRoundCornerImageView.A03 = true;
                ((WaImageView) waRoundCornerImageView).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) waRoundCornerImageView.generatedComponent())).A06.ANb.get();
            }
        } else if (this instanceof AbstractC37201lg) {
            AbstractC37201lg r17 = (AbstractC37201lg) this;
            if (!r17.A00) {
                r17.A00 = true;
                ((WaImageView) r17).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r17.generatedComponent())).A06.ANb.get();
            }
        } else if (!this.A01) {
            this.A01 = true;
            ((WaImageView) this).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ANb.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
