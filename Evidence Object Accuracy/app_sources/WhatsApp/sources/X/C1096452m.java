package X;

import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;

/* renamed from: X.52m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096452m implements AbstractC116455Vm {
    public final /* synthetic */ SharedTextPreviewDialogFragment A00;

    public C1096452m(SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment) {
        this.A00 = sharedTextPreviewDialogFragment;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0E);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A0E, iArr, 0);
    }
}
