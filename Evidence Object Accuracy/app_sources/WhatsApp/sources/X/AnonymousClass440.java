package X;

import android.database.Cursor;

/* renamed from: X.440  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass440 extends C35591iL {
    public final int A00;
    public final int A01;

    public AnonymousClass440(C15650ng r1, C15660nh r2, AbstractC14640lm r3, AnonymousClass19O r4, int i, int i2) {
        super(r1, r2, r3, r4);
        this.A00 = i2;
        this.A01 = i;
    }

    @Override // X.C35591iL
    public Cursor A00() {
        int i = this.A00;
        int i2 = this.A01;
        return C33471e8.A01(this.A03, this.A04, i, i2);
    }
}
