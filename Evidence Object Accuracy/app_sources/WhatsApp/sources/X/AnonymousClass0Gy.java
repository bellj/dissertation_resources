package X;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import java.util.List;

/* renamed from: X.0Gy  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gy extends AnonymousClass0H3 {
    public AnonymousClass0HJ A00;
    public final PathMeasure A01 = new PathMeasure();
    public final PointF A02 = new PointF();
    public final float[] A03 = new float[2];

    public AnonymousClass0Gy(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r8, float f) {
        AnonymousClass0HJ r6 = (AnonymousClass0HJ) r8;
        Path path = r6.A00;
        if (path == null) {
            return r8.A0F;
        }
        AnonymousClass0SF r3 = super.A03;
        if (r3 != null) {
            r6.A08.floatValue();
            Object obj = r6.A0F;
            Object obj2 = r6.A09;
            A02();
            AnonymousClass0NB r0 = r3.A02;
            r0.A01 = obj;
            r0.A00 = obj2;
            Object obj3 = r3.A01;
            if (obj3 != null) {
                return obj3;
            }
        }
        if (this.A00 != r6) {
            this.A01.setPath(path, false);
            this.A00 = r6;
        }
        PathMeasure pathMeasure = this.A01;
        float[] fArr = this.A03;
        pathMeasure.getPosTan(f * pathMeasure.getLength(), fArr, null);
        PointF pointF = this.A02;
        pointF.set(fArr[0], fArr[1]);
        return pointF;
    }
}
