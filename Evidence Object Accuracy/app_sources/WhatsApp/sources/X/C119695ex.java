package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119695ex extends AnonymousClass1ZO {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(12);
    public int A00 = 1;

    public C119695ex() {
    }

    public /* synthetic */ C119695ex(Parcel parcel) {
        super(parcel);
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("BrazilContactData toNetwork not supported");
    }

    @Override // X.AnonymousClass1ZO, X.AnonymousClass1ZP
    public String A03() {
        JSONObject A0a;
        try {
            String A03 = super.A03();
            if (A03 != null) {
                A0a = C13000ix.A05(A03);
            } else {
                A0a = C117295Zj.A0a();
            }
            A0a.put("v", this.A00);
            long j = this.A01;
            if (j != -1) {
                A0a.put("nextSyncTimeMillis", j);
            }
            if (!TextUtils.isEmpty(this.A06)) {
                A0a.put("dataHash", this.A06);
            }
            return A0a.toString();
        } catch (JSONException e) {
            Log.w("PAY: BrazilContactData toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AnonymousClass1ZO, X.AnonymousClass1ZP
    public void A04(String str) {
        super.A04(str);
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                int optInt = A05.optInt("v", 1);
                this.A00 = optInt;
                if (optInt == 1) {
                    this.A01 = A05.optLong("nextSyncTimeMillis", -1);
                }
                this.A06 = A05.optString("dataHash");
            } catch (JSONException e) {
                Log.w("PAY: BrazilContactData fromDBString threw: ", e);
            }
        }
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ ver: ");
        A0k.append(this.A00);
        A0k.append(" jid: ");
        A0k.append(this.A05);
        A0k.append(" isMerchant: ");
        A0k.append(this.A07);
        A0k.append(" defaultPaymentType: ");
        A0k.append(super.A00);
        return C12960it.A0d(" ]", A0k);
    }
}
