package X;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.4nO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnTouchListenerC101354nO implements View.OnTouchListener {
    public final /* synthetic */ AbstractC471028y A00;

    public View$OnTouchListenerC101354nO(AbstractC471028y r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        motionEvent.getX();
        motionEvent.getY();
        view.performClick();
        return false;
    }
}
