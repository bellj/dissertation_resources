package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54662h5 extends AbstractC018308n {
    public final float A00;
    public final float A01;
    public final float A02;
    public final int A03;

    public /* synthetic */ C54662h5(float f, float f2, float f3, int i) {
        this.A01 = f;
        this.A02 = f2;
        this.A00 = f3;
        this.A03 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r7, RecyclerView recyclerView) {
        int A00 = RecyclerView.A00(view);
        if (A00 == 0) {
            int i = this.A03;
            int i2 = (int) this.A01;
            if (i == 1) {
                rect.top = i2;
            } else {
                rect.left = i2;
            }
        }
        if (A00 < recyclerView.A0N.A0D() - 1) {
            int i3 = this.A03;
            int i4 = (int) this.A02;
            if (i3 == 1) {
                rect.bottom = i4;
            } else {
                rect.right = i4;
            }
        }
        if (A00 == recyclerView.A0N.A0D() - 1) {
            int i5 = this.A03;
            int i6 = (int) this.A00;
            if (i5 == 1) {
                rect.bottom = i6;
            } else {
                rect.right = i6;
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C54662h5)) {
            return false;
        }
        C54662h5 r4 = (C54662h5) obj;
        if (r4.A03 == this.A03 && r4.A00 == this.A00 && r4.A01 == this.A01 && r4.A02 == this.A02) {
            return true;
        }
        return false;
    }
}
