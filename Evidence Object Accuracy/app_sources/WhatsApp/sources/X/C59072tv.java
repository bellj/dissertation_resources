package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59072tv extends AbstractC59082tw {
    public final AbstractC15710nm A00;
    public final AbstractC116505Vs A01;
    public final AnonymousClass3HN A02;
    public final C18640sm A03;
    public final AnonymousClass28H A04;
    public final C15680nj A05;
    public final C19870uo A06;
    public final C17220qS A07;
    public final C19840ul A08;

    public C59072tv(AbstractC15710nm r2, C14650lo r3, AbstractC116505Vs r4, AnonymousClass3HN r5, C18640sm r6, AnonymousClass28H r7, C15680nj r8, C19870uo r9, C17220qS r10, C19840ul r11) {
        super(r3, r7.A05);
        this.A02 = r5;
        this.A00 = r2;
        this.A08 = r11;
        this.A07 = r10;
        this.A04 = r7;
        this.A05 = r8;
        this.A03 = r6;
        this.A01 = r4;
        this.A06 = r9;
    }

    public final void A06() {
        AnonymousClass1Q5 A00;
        if (this.A04.A06 == null && (A00 = C19840ul.A00(this.A08)) != null) {
            A00.A07("datasource_catalog");
        }
    }

    public boolean A07() {
        if (!this.A03.A0B()) {
            StringBuilder A0j = C12960it.A0j("app/sendGetBizProductCatalog jid=");
            A0j.append(this.A04.A05);
            Log.i(C12960it.A0d(" failed", A0j));
            return false;
        }
        if (super.A01.A08()) {
            A02();
        } else {
            A03();
        }
        StringBuilder A0j2 = C12960it.A0j("app/sendGetBizProductCatalog jid=");
        A0j2.append(this.A04.A05);
        Log.i(C12960it.A0d(" success", A0j2));
        return true;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A06();
        Log.e("sendGetBizProductCatalog/delivery-error");
        this.A01.AQ9(this.A04, -1);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        A06();
        AnonymousClass28H r4 = this.A04;
        UserJid userJid = r4.A05;
        AnonymousClass3HN r2 = this.A02;
        C44721zR A01 = r2.A01(r6);
        r2.A03(super.A01, userJid, r6);
        if (A01 != null) {
            this.A01.AXA(A01, r4);
            return;
        }
        this.A01.AQ9(r4, 0);
        this.A00.AaV("RequestBizProductCatalogProtocolHelper/get product catalog error", "error_code=0", true);
    }
}
