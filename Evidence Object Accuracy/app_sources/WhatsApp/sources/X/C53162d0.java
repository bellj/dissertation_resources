package X;

import android.content.Context;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2d0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53162d0 extends LinearLayout implements AnonymousClass004 {
    public TextEmojiLabel A00;
    public AnonymousClass1B8 A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public C53162d0(Context context) {
        super(context, null);
        if (!this.A03) {
            this.A03 = true;
            this.A01 = (AnonymousClass1B8) AnonymousClass2P6.A00(generatedComponent()).A2C.get();
        }
        LinearLayout.inflate(context, R.layout.beta_footer_row, this);
        this.A00 = C12970iu.A0T(this, R.id.beta_text);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public void setFAQLink(String str) {
        this.A01.A00(getContext(), this.A00, getContext().getString(R.string.biz_dir_beta_footer_text), "account-and-profile", str);
    }
}
