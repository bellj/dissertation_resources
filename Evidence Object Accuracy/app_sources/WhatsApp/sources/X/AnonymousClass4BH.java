package X;

import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BH  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BH extends Enum {
    public static final /* synthetic */ AnonymousClass4BH[] A00;
    public static final AnonymousClass4BH A01;
    public static final AnonymousClass4BH A02;
    public static final AnonymousClass4BH A03;
    public static final AnonymousClass4BH A04;
    public final int dimension;

    public static AnonymousClass4BH valueOf(String str) {
        return (AnonymousClass4BH) Enum.valueOf(AnonymousClass4BH.class, str);
    }

    public static AnonymousClass4BH[] values() {
        return (AnonymousClass4BH[]) A00.clone();
    }

    static {
        AnonymousClass4BH r7 = new AnonymousClass4BH(0, "EXTRA_SMALL", R.dimen.wds_profile_photo_extra_small);
        A01 = r7;
        AnonymousClass4BH r6 = new AnonymousClass4BH(1, "SMALL", R.dimen.wds_profile_photo_small);
        A04 = r6;
        AnonymousClass4BH r5 = new AnonymousClass4BH(2, "MEDIUM", R.dimen.wds_profile_photo_medium);
        A03 = r5;
        AnonymousClass4BH r4 = new AnonymousClass4BH(3, "LARGE", R.dimen.wds_profile_photo_large);
        A02 = r4;
        AnonymousClass4BH r1 = new AnonymousClass4BH(4, "EXTRA_LARGE", R.dimen.wds_profile_photo_extra_large);
        AnonymousClass4BH[] r0 = new AnonymousClass4BH[5];
        C72453ed.A1F(r7, r6, r5, r4, r0);
        r0[4] = r1;
        A00 = r0;
    }

    public AnonymousClass4BH(int i, String str, int i2) {
        this.dimension = i2;
    }
}
