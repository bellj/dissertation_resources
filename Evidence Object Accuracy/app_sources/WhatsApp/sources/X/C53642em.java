package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53642em extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new C98464ij();
    public float A00;
    public int A01;
    public boolean A02;

    public C53642em(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A01 = parcel.readInt();
        this.A00 = parcel.readFloat();
        this.A02 = C12960it.A1S(parcel.readByte());
    }

    public C53642em(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A01);
        parcel.writeFloat(this.A00);
        parcel.writeByte(this.A02 ? (byte) 1 : 0);
    }
}
