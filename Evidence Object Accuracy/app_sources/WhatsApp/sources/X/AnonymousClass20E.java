package X;

/* renamed from: X.20E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20E {
    public static final int A00;
    public static final int A01;
    public static final int A02;
    public static final int A03;
    public static final byte[] A04 = new byte[16];
    public static final byte[] A05;

    public static void A00(int[] iArr, int[] iArr2, int i) {
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        long j = 0;
        while (i > 0) {
            if (i4 < Math.min(30, i)) {
                i2++;
                j |= (((long) iArr[i2]) & 4294967295L) << i4;
                i4 += 32;
            }
            i3++;
            iArr2[i3] = ((int) j) & 1073741823;
            j >>>= 30;
            i4 -= 30;
            i -= 30;
        }
    }

    static {
        byte[] bytes = "expand 32-byte k".getBytes();
        A05 = bytes;
        A00 = AbstractC95434di.A00(bytes, 0);
        A02 = AbstractC95434di.A00(bytes, 4);
        A03 = AbstractC95434di.A00(bytes, 8);
        A01 = AbstractC95434di.A00(bytes, 12);
    }

    public static byte[] A01(byte[] bArr, byte[] bArr2) {
        int i;
        byte[] bArr3 = new byte[32];
        int[] iArr = new int[8];
        int i2 = 0;
        do {
            int i3 = (i2 << 2) + 0;
            int i4 = i3 + 1;
            int i5 = i4 + 1;
            iArr[i2] = (bArr2[i5 + 1] << 24) | (bArr2[i3] & 255) | ((bArr2[i4] & 255) << 8) | ((bArr2[i5] & 255) << 16);
            i2++;
        } while (i2 < 8);
        iArr[0] = iArr[0] & -8;
        int i6 = iArr[7] & Integer.MAX_VALUE;
        iArr[7] = i6;
        iArr[7] = i6 | 1073741824;
        int[] iArr2 = new int[10];
        AbstractC95594e2.A04(bArr, iArr2, 0, 0);
        AbstractC95594e2.A04(bArr, iArr2, 16, 5);
        iArr2[9] = iArr2[9] & 16777215;
        int[] iArr3 = new int[10];
        int i7 = 0;
        int i8 = 0;
        do {
            int i9 = 0 + i8;
            iArr3[i9] = iArr2[i9];
            i8++;
        } while (i8 < 10);
        int[] iArr4 = new int[10];
        iArr4[0] = 1;
        int[] iArr5 = new int[10];
        iArr5[0] = 1;
        int[] iArr6 = new int[10];
        int[] iArr7 = new int[10];
        int[] iArr8 = new int[10];
        int i10 = 254;
        int i11 = 1;
        while (true) {
            int i12 = 0;
            do {
                int i13 = iArr5[i12];
                int i14 = iArr6[i12];
                iArr7[i12] = i13 + i14;
                iArr5[i12] = i13 - i14;
                i12++;
            } while (i12 < 10);
            int i15 = 0;
            do {
                int i16 = iArr3[i15];
                int i17 = iArr4[i15];
                iArr6[i15] = i16 + i17;
                iArr3[i15] = i16 - i17;
                i15++;
            } while (i15 < 10);
            AbstractC95594e2.A09(iArr7, iArr3, iArr7);
            AbstractC95594e2.A09(iArr5, iArr6, iArr5);
            AbstractC95594e2.A08(iArr6, iArr6);
            AbstractC95594e2.A08(iArr3, iArr3);
            int i18 = 0;
            do {
                iArr8[i18] = iArr6[i18] - iArr3[i18];
                i18++;
            } while (i18 < 10);
            AbstractC95594e2.A07(iArr8, iArr4);
            int i19 = 0;
            do {
                iArr4[i19] = iArr4[i19] + iArr3[i19];
                i19++;
            } while (i19 < 10);
            AbstractC95594e2.A09(iArr4, iArr8, iArr4);
            AbstractC95594e2.A09(iArr3, iArr6, iArr3);
            int i20 = 0;
            do {
                int i21 = iArr7[i20];
                int i22 = iArr5[i20];
                iArr5[i20] = i21 + i22;
                iArr6[i20] = i21 - i22;
                i20++;
            } while (i20 < 10);
            AbstractC95594e2.A08(iArr5, iArr5);
            AbstractC95594e2.A08(iArr6, iArr6);
            AbstractC95594e2.A09(iArr6, iArr2, iArr6);
            i10--;
            int i23 = (iArr[i10 >>> 5] >>> (i10 & 31)) & 1;
            int i24 = i11 ^ i23;
            int i25 = 0;
            int i26 = 0 - i24;
            do {
                int i27 = iArr3[i25];
                int i28 = iArr5[i25];
                int i29 = (i27 ^ i28) & i26;
                iArr3[i25] = i27 ^ i29;
                iArr5[i25] = i28 ^ i29;
                i25++;
            } while (i25 < 10);
            int i30 = 0;
            int i31 = 0 - i24;
            do {
                int i32 = iArr4[i30];
                int i33 = iArr6[i30];
                int i34 = (i32 ^ i33) & i31;
                iArr4[i30] = i32 ^ i34;
                iArr6[i30] = i33 ^ i34;
                i30++;
            } while (i30 < 10);
            if (i10 >= 3) {
                i11 = i23;
            }
        }
        do {
            int[] iArr9 = new int[10];
            int[] iArr10 = new int[10];
            int i35 = 0;
            do {
                int i36 = iArr3[i35];
                int i37 = iArr4[i35];
                iArr9[i35] = i36 + i37;
                iArr10[i35] = i36 - i37;
                i35++;
            } while (i35 < 10);
            AbstractC95594e2.A08(iArr9, iArr9);
            AbstractC95594e2.A08(iArr10, iArr10);
            AbstractC95594e2.A09(iArr9, iArr10, iArr3);
            int i38 = 0;
            do {
                iArr9[i38] = iArr9[i38] - iArr10[i38];
                i38++;
            } while (i38 < 10);
            AbstractC95594e2.A07(iArr9, iArr4);
            int i39 = 0;
            do {
                iArr4[i39] = iArr4[i39] + iArr10[i39];
                i39++;
            } while (i39 < 10);
            AbstractC95594e2.A09(iArr4, iArr9, iArr4);
            i7++;
        } while (i7 < 3);
        int[] iArr11 = new int[10];
        int[] iArr12 = new int[8];
        int i40 = 0;
        do {
            int i41 = 0 + i40;
            iArr11[i41] = iArr4[i41];
            i40++;
        } while (i40 < 10);
        int i42 = (iArr11[9] >>> 23) & 1;
        AbstractC95594e2.A06(iArr11, i42);
        AbstractC95594e2.A06(iArr11, -i42);
        AbstractC95594e2.A02(0, 0, iArr11, iArr12);
        AbstractC95594e2.A02(5, 4, iArr11, iArr12);
        int[] iArr13 = AbstractC95594e2.A00;
        int length = iArr13.length;
        int numberOfLeadingZeros = (length << 5) - Integer.numberOfLeadingZeros(iArr13[length - 1]);
        int i43 = (numberOfLeadingZeros + 29) / 30;
        int[] iArr14 = new int[4];
        int[] iArr15 = new int[i43];
        int[] iArr16 = new int[i43];
        int[] iArr17 = new int[i43];
        int[] iArr18 = new int[i43];
        int[] iArr19 = new int[i43];
        iArr16[0] = 1;
        A00(iArr12, iArr18, numberOfLeadingZeros);
        A00(iArr13, iArr19, numberOfLeadingZeros);
        System.arraycopy(iArr19, 0, iArr17, 0, i43);
        int i44 = iArr19[0];
        int i45 = (2 - (i44 * i44)) * i44;
        int i46 = i45 * (2 - (i44 * i45));
        int i47 = i46 * (2 - (i44 * i46));
        int i48 = i47 * (2 - (i44 * i47));
        int i49 = numberOfLeadingZeros * 49;
        int i50 = 47;
        if (numberOfLeadingZeros < 46) {
            i50 = 80;
        }
        int i51 = (i49 + i50) / 17;
        int i52 = -1;
        for (int i53 = 0; i53 < i51; i53 += 30) {
            int i54 = iArr17[0];
            int i55 = iArr18[0];
            int i56 = 1;
            int i57 = 0;
            int i58 = 0;
            int i59 = 1;
            for (int i60 = 0; i60 < 30; i60++) {
                int i61 = i52 >> 31;
                int i62 = -(i55 & 1);
                int i63 = i55 + (((i54 ^ i61) - i61) & i62);
                i58 += ((i56 ^ i61) - i61) & i62;
                i59 += ((i57 ^ i61) - i61) & i62;
                int i64 = i61 & i62;
                i52 = (i52 ^ i64) - (i64 + 1);
                i54 += i63 & i64;
                i55 = i63 >> 1;
                i56 = (i56 + (i58 & i64)) << 1;
                i57 = (i57 + (i64 & i59)) << 1;
            }
            iArr14[0] = i56;
            iArr14[1] = i57;
            iArr14[2] = i58;
            iArr14[3] = i59;
            int i65 = iArr14[0];
            int i66 = iArr14[1];
            int i67 = iArr14[2];
            int i68 = i43 - 1;
            int i69 = iArr15[i68] >> 31;
            int i70 = iArr16[i68] >> 31;
            int i71 = (i65 & i69) + (i66 & i70);
            int i72 = (i69 & i67) + (i70 & i59);
            int i73 = iArr19[0];
            int i74 = iArr15[0];
            int i75 = iArr16[0];
            long j = (long) i65;
            long j2 = (long) i74;
            long j3 = j * j2;
            long j4 = (long) i66;
            long j5 = (long) i75;
            long j6 = j3 + (j4 * j5);
            long j7 = (long) i67;
            long j8 = j2 * j7;
            long j9 = (long) i59;
            long j10 = j8 + (j5 * j9);
            long j11 = (long) i73;
            long j12 = (long) (i71 - (((((int) j6) * i48) + i71) & 1073741823));
            long j13 = (long) (i72 - (((((int) j10) * i48) + i72) & 1073741823));
            long j14 = (j6 + (j11 * j12)) >> 30;
            long j15 = (j10 + (j11 * j13)) >> 30;
            for (int i76 = 1; i76 < i43; i76++) {
                int i77 = iArr19[i76];
                long j16 = (long) iArr15[i76];
                long j17 = (long) iArr16[i76];
                long j18 = (j * j16) + (j4 * j17);
                long j19 = (long) i77;
                long j20 = j14 + j18 + (j19 * j12);
                long j21 = j15 + (j16 * j7) + (j17 * j9) + (j19 * j13);
                int i78 = i76 - 1;
                iArr15[i78] = ((int) j20) & 1073741823;
                j14 = j20 >> 30;
                iArr16[i78] = ((int) j21) & 1073741823;
                j15 = j21 >> 30;
            }
            iArr15[i68] = (int) j14;
            iArr16[i68] = (int) j15;
            int i79 = iArr14[0];
            int i80 = iArr14[1];
            int i81 = iArr14[2];
            int i82 = iArr14[3];
            int i83 = iArr17[0];
            int i84 = iArr18[0];
            long j22 = (long) i79;
            long j23 = (long) i83;
            long j24 = j22 * j23;
            long j25 = (long) i80;
            long j26 = (long) i84;
            long j27 = (long) i81;
            long j28 = (long) i82;
            long j29 = (j24 + (j25 * j26)) >> 30;
            long j30 = ((j23 * j27) + (j26 * j28)) >> 30;
            for (int i85 = 1; i85 < i43; i85++) {
                long j31 = (long) iArr17[i85];
                long j32 = j22 * j31;
                long j33 = (long) iArr18[i85];
                long j34 = j29 + j32 + (j25 * j33);
                long j35 = j30 + (j31 * j27) + (j33 * j28);
                int i86 = i85 - 1;
                iArr17[i86] = ((int) j34) & 1073741823;
                j29 = j34 >> 30;
                iArr18[i86] = 1073741823 & ((int) j35);
                j30 = j35 >> 30;
            }
            int i87 = i43 - 1;
            iArr17[i87] = (int) j29;
            iArr18[i87] = (int) j30;
        }
        int i88 = i43 - 1;
        int i89 = iArr17[i88] >> 31;
        int i90 = 0;
        for (int i91 = 0; i91 < i88; i91++) {
            int i92 = i90 + ((iArr17[i91] ^ i89) - i89);
            iArr17[i91] = 1073741823 & i92;
            i90 = i92 >> 30;
        }
        iArr17[i88] = i90 + ((iArr17[i88] ^ i89) - i89);
        int i93 = iArr15[i88] >> 31;
        int i94 = 0;
        for (int i95 = 0; i95 < i88; i95++) {
            int i96 = i94 + (((iArr15[i95] + (iArr19[i95] & i93)) ^ i89) - i89);
            iArr15[i95] = 1073741823 & i96;
            i94 = i96 >> 30;
        }
        int i97 = i94 + (((iArr15[i88] + (i93 & iArr19[i88])) ^ i89) - i89);
        iArr15[i88] = i97;
        int i98 = i97 >> 31;
        int i99 = 0;
        for (int i100 = 0; i100 < i88; i100++) {
            int i101 = i99 + iArr15[i100] + (iArr19[i100] & i98);
            iArr15[i100] = i101 & 1073741823;
            i99 = i101 >> 30;
        }
        iArr15[i88] = i99 + iArr15[i88] + (i98 & iArr19[i88]);
        int i102 = 0;
        int i103 = 0;
        int i104 = 0;
        long j36 = 0;
        while (true) {
            i = 1;
            if (numberOfLeadingZeros > 0) {
                while (i104 < Math.min(32, numberOfLeadingZeros)) {
                    i102++;
                    j36 |= ((long) iArr15[i102]) << i104;
                    i104 += 30;
                }
                i103++;
                iArr12[i103] = (int) j36;
                j36 >>>= 32;
                i104 -= 32;
                numberOfLeadingZeros -= 32;
            }
        }
        do {
            i++;
        } while (i < i43);
        int i105 = 0;
        do {
            i105++;
        } while (i105 < i43);
        AbstractC95594e2.A01(0, 0, iArr12, iArr4);
        AbstractC95594e2.A01(4, 5, iArr12, iArr4);
        iArr4[9] = iArr4[9] & 16777215;
        AbstractC95594e2.A09(iArr3, iArr4, iArr3);
        int i106 = (iArr3[9] >>> 23) & 1;
        AbstractC95594e2.A06(iArr3, i106);
        AbstractC95594e2.A06(iArr3, -i106);
        AbstractC95594e2.A05(bArr3, iArr3, 0, 0);
        AbstractC95594e2.A05(bArr3, iArr3, 5, 16);
        byte[] bArr4 = new byte[32];
        byte[] bArr5 = A04;
        int[] iArr20 = new int[16];
        int A002 = AbstractC95434di.A00(bArr5, 0);
        int A003 = AbstractC95434di.A00(bArr5, 4);
        int A004 = AbstractC95434di.A00(bArr5, 8);
        int A005 = AbstractC95434di.A00(bArr5, 12);
        int i107 = A00;
        iArr20[0] = i107;
        iArr20[1] = AbstractC95434di.A00(bArr3, 0);
        iArr20[2] = AbstractC95434di.A00(bArr3, 4);
        iArr20[3] = AbstractC95434di.A00(bArr3, 8);
        iArr20[4] = AbstractC95434di.A00(bArr3, 12);
        int i108 = A02;
        iArr20[5] = i108;
        iArr20[6] = A002;
        iArr20[7] = A003;
        iArr20[8] = A004;
        iArr20[9] = A005;
        int i109 = A03;
        iArr20[10] = i109;
        iArr20[11] = AbstractC95434di.A00(bArr3, 16);
        iArr20[12] = AbstractC95434di.A00(bArr3, 20);
        iArr20[13] = AbstractC95434di.A00(bArr3, 24);
        iArr20[14] = AbstractC95434di.A00(bArr3, 28);
        int i110 = A01;
        iArr20[15] = i110;
        AnonymousClass20G.A00(iArr20, iArr20, 20);
        iArr20[0] = iArr20[0] - i107;
        iArr20[5] = iArr20[5] - i108;
        iArr20[10] = iArr20[10] - i109;
        iArr20[15] = iArr20[15] - i110;
        iArr20[6] = iArr20[6] - A002;
        iArr20[7] = iArr20[7] - A003;
        iArr20[8] = iArr20[8] - A004;
        int i111 = iArr20[9] - A005;
        iArr20[9] = i111;
        AbstractC95434di.A02(bArr4, iArr20[0], 0);
        AbstractC95434di.A02(bArr4, iArr20[5], 4);
        AbstractC95434di.A02(bArr4, iArr20[10], 8);
        AbstractC95434di.A02(bArr4, iArr20[15], 12);
        AbstractC95434di.A02(bArr4, iArr20[6], 16);
        AbstractC95434di.A02(bArr4, iArr20[7], 20);
        AbstractC95434di.A02(bArr4, iArr20[8], 24);
        AbstractC95434di.A02(bArr4, i111, 28);
        return bArr4;
    }
}
