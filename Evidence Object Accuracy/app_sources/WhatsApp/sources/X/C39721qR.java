package X;

import java.io.File;

/* renamed from: X.1qR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39721qR {
    public final int A00;
    public final C14510lY A01;
    public final C29171Rd A02;
    public final File A03;
    public final boolean A04;
    public final boolean A05;

    public C39721qR(C14510lY r1, C29171Rd r2, File file, int i, boolean z, boolean z2) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
        this.A04 = z;
        this.A05 = z2;
        this.A03 = file;
    }
}
