package X;

import android.os.Bundle;
import android.view.View;

/* renamed from: X.3hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74413hw extends AnonymousClass04v {
    public final /* synthetic */ AbstractC15160mf A00;

    public C74413hw(AbstractC15160mf r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        if (i != 1048576) {
            return super.A03(view, i, bundle);
        }
        this.A00.A04(3);
        return true;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        r4.A02.addAction(1048576);
        r4.A0K(true);
    }
}
