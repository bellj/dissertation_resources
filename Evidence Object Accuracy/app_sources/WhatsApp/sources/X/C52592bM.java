package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.whatsapp.emoji.EmojiPopupFooter;

/* renamed from: X.2bM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C52592bM extends Animation {
    public final int A00;
    public final int A01;
    public final /* synthetic */ C15270mq A02;

    public C52592bM(C15270mq r4, int i) {
        this.A02 = r4;
        this.A00 = i;
        EmojiPopupFooter emojiPopupFooter = r4.A08;
        int i2 = emojiPopupFooter.A00;
        this.A01 = i2;
        setDuration((long) ((C12980iv.A05(i2, i) * 300) / emojiPopupFooter.getHeight()));
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        EmojiPopupFooter emojiPopupFooter = this.A02.A08;
        int i = this.A01;
        emojiPopupFooter.setTopOffset((int) (((float) i) + (((float) (this.A00 - i)) * f)));
    }
}
