package X;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2d3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53182d3 extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public int A01;
    public int A02;
    public C15570nT A03;
    public AnonymousClass018 A04;
    public C14850m9 A05;
    public C40481rf A06;
    public AnonymousClass2P7 A07;
    public List A08;
    public List A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;

    public C53182d3(Context context) {
        super(context);
        if (!this.A0C) {
            this.A0C = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A05 = C12960it.A0S(A00);
            this.A03 = C12970iu.A0S(A00);
            this.A04 = C12960it.A0R(A00);
        }
        this.A0B = false;
        this.A0A = true;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        setGravity(17);
        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.reaction_bubble_height));
        setLayoutParams(layoutParams);
        setOrientation(0);
        setBackgroundResource(R.drawable.reaction_bubble_background);
        this.A02 = getResources().getDimensionPixelSize(R.dimen.space_tight);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.space_xTight);
        int i = this.A02;
        setPadding(i, dimensionPixelSize, i, dimensionPixelSize);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.reaction_bubble_count_size);
        setVisibility(8);
        setId(R.id.reactions_bubble_layout);
        setClipToPadding(false);
        setClipChildren(false);
        ViewParent parent = getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) parent;
            viewGroup.setClipToPadding(false);
            viewGroup.setClipChildren(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r5.A01() == 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.C40481rf r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 1096
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53182d3.A00(X.1rf, boolean):void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    public void setAreAnimationsEnabled(boolean z) {
        this.A0A = z;
    }
}
