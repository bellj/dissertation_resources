package X;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.net.ssl.SSLException;

/* renamed from: X.4WK  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4WK {
    public void A00(byte[] bArr, int i, int i2, byte b) {
        short s;
        if (!(this instanceof AnonymousClass46G)) {
            AnonymousClass46H r1 = (AnonymousClass46H) this;
            if (i2 <= 16384) {
                C94044bA r5 = new C94044bA(Arrays.copyOfRange(bArr, i, i2 + i), b);
                byte[] bArr2 = r5.A02;
                int i3 = r5.A01;
                ByteBuffer allocate = ByteBuffer.allocate(bArr2.length + 1 + i3);
                allocate.put(bArr2);
                allocate.put(r5.A00);
                allocate.put(new byte[i3]);
                byte[] array = allocate.array();
                ByteBuffer allocate2 = ByteBuffer.allocate(5);
                allocate2.put((byte) 23);
                allocate2.putShort(771);
                int length = array.length;
                AbstractC116735Wp r9 = r1.A01;
                AnonymousClass14N.A00();
                C72453ed.A1Q(allocate2, 16 + length);
                byte[] A9O = r9.A9O(allocate2.array(), array, 0, length, r1.A00);
                r1.A00++;
                try {
                    ByteBuffer allocate3 = ByteBuffer.allocate(A9O.length + 5);
                    allocate3.put(allocate2.array());
                    allocate3.put(A9O);
                    r1.A02.write(allocate3.array());
                } catch (SocketException | SocketTimeoutException e) {
                    throw new AnonymousClass1NR(new SSLException(e), (byte) 80, true);
                } catch (IOException e2) {
                    throw C72453ed.A0f(e2);
                }
            } else {
                StringBuilder A0k = C12960it.A0k("record size cannot exceed max length. ");
                A0k.append(i2);
                throw AnonymousClass1NR.A00(C12960it.A0e(" > ", A0k, 16384), (byte) 22);
            }
        } else {
            AnonymousClass46G r12 = (AnonymousClass46G) this;
            if (i2 <= 16384) {
                try {
                    ByteBuffer allocate4 = ByteBuffer.allocate(i2 + 5);
                    allocate4.put(b);
                    if (b != 22 || r12.A01) {
                        s = 771;
                    } else {
                        r12.A01 = true;
                        s = 769;
                    }
                    allocate4.putShort(s);
                    C72453ed.A1Q(allocate4, i2);
                    allocate4.put(bArr, i, i2);
                    r12.A00.write(allocate4.array());
                } catch (SocketException | SocketTimeoutException e3) {
                    throw new AnonymousClass1NR(new SSLException(e3), (byte) 80, true);
                } catch (IOException e4) {
                    throw C72453ed.A0f(e4);
                }
            } else {
                StringBuilder A0k2 = C12960it.A0k("record size cannot exceed max length. ");
                A0k2.append(i2);
                throw AnonymousClass1NR.A00(C12960it.A0e(" > ", A0k2, 16384), (byte) 22);
            }
        }
    }

    public synchronized void A01(byte[] bArr, int i, int i2, byte b) {
        if (bArr == null) {
            throw AnonymousClass1NR.A00("Data cannot be null", (byte) 80);
        } else if (AnonymousClass4GK.A00.contains(Byte.valueOf(b))) {
            while (i2 > 16384) {
                A00(bArr, i, 16384, b);
                i += 16384;
                i2 -= 16384;
            }
            if (i2 > 0) {
                A00(bArr, i, i2, b);
            }
        } else {
            throw AnonymousClass1NR.A00("Invalid content type", (byte) 80);
        }
    }
}
