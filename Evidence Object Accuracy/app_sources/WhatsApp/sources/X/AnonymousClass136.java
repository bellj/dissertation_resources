package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.136  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass136 {
    public final C28601Of A00;
    public final AnonymousClass1JO A01;

    public AnonymousClass136(AnonymousClass01H r15, AnonymousClass01H r16, AnonymousClass01H r17, AnonymousClass01H r18, AnonymousClass01H r19, AnonymousClass01H r20, AnonymousClass01H r21, AnonymousClass01H r22, AnonymousClass01H r23, AnonymousClass01H r24) {
        AnonymousClass1YJ r4 = new AnonymousClass1YJ();
        r4.A02(new C002601e(null, new AnonymousClass01N() { // from class: X.201
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new C35921j0((AnonymousClass137) AnonymousClass01H.this.get());
            }
        }));
        r4.A02(new C002601e(null, new AnonymousClass01N() { // from class: X.202
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new C36211jV(new Handler(Looper.getMainLooper()), (C16590pI) AnonymousClass01H.this.get());
            }
        }));
        AnonymousClass1YB r2 = new AnonymousClass1YB();
        r2.A01("com.facebook.stella", new C002601e(null, new AnonymousClass01N(r17, r18, r19, r20, r21, r22, r23) { // from class: X.203
            public final /* synthetic */ AnonymousClass01H A01;
            public final /* synthetic */ AnonymousClass01H A02;
            public final /* synthetic */ AnonymousClass01H A03;
            public final /* synthetic */ AnonymousClass01H A04;
            public final /* synthetic */ AnonymousClass01H A05;
            public final /* synthetic */ AnonymousClass01H A06;
            public final /* synthetic */ AnonymousClass01H A07;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
                this.A04 = r5;
                this.A05 = r6;
                this.A06 = r7;
                this.A07 = r8;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                AnonymousClass01H r7 = AnonymousClass01H.this;
                AnonymousClass01H r6 = this.A01;
                AnonymousClass01H r5 = this.A02;
                AnonymousClass01H r42 = this.A03;
                AnonymousClass01H r3 = this.A04;
                AnonymousClass01H r25 = this.A05;
                AnonymousClass01H r1 = this.A06;
                AnonymousClass01H r0 = this.A07;
                C16210od r62 = (C16210od) r42.get();
                AnonymousClass13G r12 = (AnonymousClass13G) r3.get();
                AnonymousClass13I r11 = (AnonymousClass13I) r25.get();
                AnonymousClass13J r10 = (AnonymousClass13J) r1.get();
                return new C37321m2(r62, (AnonymousClass13A) r7.get(), (AnonymousClass13K) r0.get(), (AnonymousClass13D) r6.get(), r10, r11, r12, (C15490nL) r5.get());
            }
        }));
        this.A01 = r4.A00();
        this.A00 = r2.A00();
    }
}
