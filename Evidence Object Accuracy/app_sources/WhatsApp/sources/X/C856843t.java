package X;

/* renamed from: X.43t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856843t extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;

    public C856843t() {
        super(2884, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(11, this.A00);
        r3.Abe(12, this.A01);
        r3.Abe(13, this.A02);
        r3.Abe(14, this.A03);
        r3.Abe(1, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(9, this.A06);
        r3.Abe(8, this.A07);
        r3.Abe(5, this.A08);
        r3.Abe(3, this.A09);
        r3.Abe(15, this.A0A);
        r3.Abe(2, this.A0B);
        r3.Abe(7, this.A0C);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerDailyAggregatedEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerAddToFavoriteCount", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPackDeleteCount", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPickerOpenedCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSearchOpenedCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountForward", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountIsAnimated", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountIsFirstParty", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountStickerPickerTabEmotion", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountStickerPickerTabFavorites", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountStickerPickerTabPack", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountStickerPickerTabRecents", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerSendCountStickerSearch", this.A0C);
        return C12960it.A0d("}", A0k);
    }
}
