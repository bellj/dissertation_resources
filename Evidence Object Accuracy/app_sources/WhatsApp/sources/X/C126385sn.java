package X;

/* renamed from: X.5sn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126385sn {
    public final AnonymousClass1V8 A00;

    public C126385sn(String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy.A01(A0M, "type", "set");
        A0M.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        C41141sy.A01(A0M, "xmlns", "w:pay");
        if (C117295Zj.A1Y(str, false)) {
            C41141sy.A01(A0M, "id", str);
        }
        this.A00 = A0M.A03();
    }
}
