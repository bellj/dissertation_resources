package X;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.DefaultWallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.GalleryWallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPreviewActivity;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.Serializable;

/* renamed from: X.35C  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass35C extends AnonymousClass473 implements AnonymousClass1SM {
    public Button A00;
    public C15550nR A01;
    public C15610nY A02;

    @Override // X.AnonymousClass2VP
    public int A2e() {
        if (this instanceof GalleryWallpaperPreview) {
            return R.layout.gallery_wallpaper_preview_v2;
        }
        if (!(this instanceof DefaultWallpaperPreview)) {
            return R.layout.wallpaper_preview_v2;
        }
        return R.layout.wallpaper_preview_default;
    }

    public String A2f() {
        int i;
        if (((AnonymousClass2VP) this).A00 == null) {
            boolean A08 = C41691tw.A08(this);
            i = R.string.wallpaper_set_light_wallpaper_bubble_message;
            if (A08) {
                i = R.string.wallpaper_set_dark_wallpaper_bubble_message;
            }
        } else {
            boolean z = ((AnonymousClass2VP) this).A01;
            i = R.string.wallpaper_set_with_custom_wallpaper_bubble_message;
            if (z) {
                i = R.string.wallpaper_set_without_custom_wallpaper_bubble_message;
            }
        }
        return getString(i);
    }

    /* JADX INFO: finally extract failed */
    public void A2g(AbstractC14640lm r14) {
        if (this instanceof DownloadableWallpaperPreviewActivity) {
            DownloadableWallpaperPreviewActivity downloadableWallpaperPreviewActivity = (DownloadableWallpaperPreviewActivity) this;
            Intent A0A = C12970iu.A0A();
            int currentItem = downloadableWallpaperPreviewActivity.A01.getCurrentItem();
            if (currentItem < downloadableWallpaperPreviewActivity.A05.size()) {
                C21990yJ r4 = downloadableWallpaperPreviewActivity.A02;
                String path = ((Uri) downloadableWallpaperPreviewActivity.A05.get(downloadableWallpaperPreviewActivity.A01.getCurrentItem())).getPath();
                AnonymousClass009.A05(path);
                File A01 = r4.A02.A01(new File(path).getName().split("\\.")[0]);
                AnonymousClass009.A05(A01);
                A0A.setData(Uri.fromFile(A01));
                A0A.putExtra("FROM_INTERNAL_DOWNLOADS_KEY", true);
            } else {
                A0A.putExtra("selected_res_id", (Serializable) downloadableWallpaperPreviewActivity.A06.get(currentItem - downloadableWallpaperPreviewActivity.A05.size()));
            }
            C15380n4.A0A(A0A, r14);
            C12960it.A0q(downloadableWallpaperPreviewActivity, A0A);
        } else if (this instanceof WallpaperPreview) {
            WallpaperPreview wallpaperPreview = (WallpaperPreview) this;
            Intent A0A2 = C12970iu.A0A();
            A0A2.putExtra("selected_res_id", (Serializable) wallpaperPreview.A0A.get(wallpaperPreview.A09.getCurrentItem()));
            C15380n4.A0A(A0A2, r14);
            wallpaperPreview.setResult(-1, A0A2);
            wallpaperPreview.finish();
        } else if (this instanceof SolidColorWallpaperPreview) {
            SolidColorWallpaperPreview solidColorWallpaperPreview = (SolidColorWallpaperPreview) this;
            Intent A0A3 = C12970iu.A0A();
            A0A3.putExtra("wallpaper_color_file", solidColorWallpaperPreview.A0E[solidColorWallpaperPreview.A09.getCurrentItem()]);
            A0A3.putExtra("wallpaper_doodle_overlay", solidColorWallpaperPreview.A08.isChecked());
            C15380n4.A0A(A0A3, r14);
            solidColorWallpaperPreview.setResult(-1, A0A3);
            solidColorWallpaperPreview.finish();
        } else if (!(this instanceof GalleryWallpaperPreview)) {
            Intent A0A4 = C12970iu.A0A();
            C15380n4.A0A(A0A4, r14);
            A0A4.putExtra("is_default", true);
            C12960it.A0q(this, A0A4);
        } else {
            GalleryWallpaperPreview galleryWallpaperPreview = (GalleryWallpaperPreview) this;
            Uri uri = galleryWallpaperPreview.A01;
            if (uri == null) {
                Log.i("gallerywallpaperpreview/no uri found to save to. generating our own");
                uri = galleryWallpaperPreview.A04.A04();
                galleryWallpaperPreview.A01 = uri;
            }
            File file = new File(uri.getPath());
            Bitmap fullViewCroppedBitmap = galleryWallpaperPreview.A02.getFullViewCroppedBitmap();
            AnonymousClass009.A05(fullViewCroppedBitmap);
            int i = 90;
            OutputStream outputStream = null;
            do {
                try {
                    try {
                        ContentResolver A0C = ((ActivityC13810kN) galleryWallpaperPreview).A08.A0C();
                        if (A0C == null) {
                            Log.w("gallerywallpaperpreview/save cr=null");
                        } else {
                            outputStream = A0C.openOutputStream(galleryWallpaperPreview.A01);
                        }
                    } catch (FileNotFoundException e) {
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("gallerywallpaperpreview/file not found at ");
                        Log.e(C12960it.A0d(galleryWallpaperPreview.A01.getPath(), A0h), e);
                        galleryWallpaperPreview.setResult(0, C12970iu.A0A().putExtra("io-error", true));
                    }
                    if (outputStream == null) {
                        StringBuilder A0h2 = C12960it.A0h();
                        A0h2.append("gallerywallpaperpreview/outputstream/failed to open output stream for ");
                        Log.i(C12960it.A0d(galleryWallpaperPreview.A01.getPath(), A0h2));
                        galleryWallpaperPreview.setResult(0, C12970iu.A0A().putExtra("io-error", true));
                        AnonymousClass1P1.A03(outputStream);
                        return;
                    }
                    fullViewCroppedBitmap.compress(Bitmap.CompressFormat.JPEG, i, outputStream);
                    i -= 10;
                    AnonymousClass1P1.A03(outputStream);
                    if (galleryWallpaperPreview.A00 <= 0 || i <= 0 || !file.exists()) {
                        break;
                    }
                } catch (Throwable th) {
                    AnonymousClass1P1.A03(outputStream);
                    throw th;
                }
            } while (file.length() > ((long) galleryWallpaperPreview.A00));
            if (file.length() == 0 && ((ActivityC13790kL) galleryWallpaperPreview).A06.A01() == 0) {
                Log.e("gallerywallpaperpreview/no space to save compressed image");
                galleryWallpaperPreview.setResult(0, C12970iu.A0A().putExtra("no-space", true));
                return;
            }
            Intent A0A5 = C12970iu.A0A();
            A0A5.setData(galleryWallpaperPreview.A01);
            C15380n4.A0A(A0A5, r14);
            C12960it.A0q(galleryWallpaperPreview, A0A5);
        }
    }

    @Override // X.AnonymousClass1SM
    public void AW7(int i, int i2) {
        if (i == 100) {
            A2g(i2 == 0 ? ((AnonymousClass2VP) this).A00 : null);
        }
    }

    @Override // X.AnonymousClass2VP, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.wallpaper_preview_header);
        Button button = (Button) AnonymousClass00T.A05(this, R.id.set_wallpaper_button);
        this.A00 = button;
        C12960it.A11(button, this, 15);
    }
}
