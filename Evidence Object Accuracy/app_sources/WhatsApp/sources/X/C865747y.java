package X;

import com.whatsapp.calling.VoipNotAllowedActivity;
import com.whatsapp.util.Log;

/* renamed from: X.47y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865747y extends C236712o {
    public final /* synthetic */ VoipNotAllowedActivity A00;

    public C865747y(VoipNotAllowedActivity voipNotAllowedActivity) {
        this.A00 = voipNotAllowedActivity;
    }

    @Override // X.C236712o
    public void A02(AnonymousClass1YT r2) {
        Log.i("voipnotallowedactivity/onCallStarted finish this activity");
        this.A00.finish();
    }
}
