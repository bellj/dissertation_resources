package X;

import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;
import java.util.Collections;

/* renamed from: X.3FR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FR {
    public float A00;
    public int A01 = -1;
    public View A02;
    public ImageView A03;
    public C53182d3 A04;
    public AbstractC16130oV A05;
    public final ImageView A06;
    public final LinearLayout A07;
    public final TextView A08;
    public final C64853Hd A09;
    public final StickerView A0A;
    public final /* synthetic */ C60832yi A0B;

    public AnonymousClass3FR(LinearLayout linearLayout, C60832yi r17) {
        this.A0B = r17;
        this.A0A = (StickerView) linearLayout.findViewById(R.id.sticker_image);
        this.A08 = C12960it.A0J(linearLayout, R.id.date);
        this.A06 = C12970iu.A0L(linearLayout, R.id.status);
        C14850m9 r10 = ((AbstractC28551Oa) r17).A0L;
        C239613r r6 = ((AnonymousClass1OY) r17).A0M;
        C16170oZ r7 = ((AnonymousClass1OY) r17).A0R;
        AnonymousClass018 r9 = ((AbstractC28551Oa) r17).A0K;
        AnonymousClass19O r14 = r17.A1O;
        this.A09 = new C64853Hd(linearLayout, r6, r7, r17.A02, r9, r10, r17.A03, r17.A04, r17.A05, r14);
        this.A07 = (LinearLayout) linearLayout.findViewById(R.id.date_wrapper);
        linearLayout.setClipChildren(false);
        linearLayout.setClipToPadding(false);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 8388613;
        C60832yi r3 = this.A0B;
        layoutParams.topMargin = -r3.getReactionsViewVerticalOverlap();
        boolean A01 = C28141Kv.A01(((AbstractC28551Oa) r3).A0K);
        int dimensionPixelOffset = r3.getResources().getDimensionPixelOffset(R.dimen.space_base);
        if (A01) {
            layoutParams.rightMargin = dimensionPixelOffset;
        } else {
            layoutParams.leftMargin = dimensionPixelOffset;
        }
        C53182d3 r1 = new C53182d3(r3.getContext());
        this.A04 = r1;
        AbstractView$OnClickListenerC34281fs.A01(r1, this, 16);
        linearLayout.addView(this.A04, layoutParams);
    }

    public final void A00() {
        View view = this.A02;
        if (view != null) {
            view.setVisibility(0);
            return;
        }
        C52482as r1 = new C52482as(this.A0B.getContext(), this);
        this.A02 = r1;
        r1.setClickable(true);
        C12960it.A0y(this.A02, this, 37);
        ((ViewGroup) this.A0A.getParent()).addView(this.A02, C12990iw.A0M());
    }

    public void A01(AbstractC16130oV r12, boolean z) {
        C53182d3 r6;
        C40481rf r3;
        ImageView imageView;
        int ABe;
        this.A05 = r12;
        C60832yi r5 = this.A0B;
        AbstractC13890kV r62 = ((AbstractC28551Oa) r5).A0a;
        if (r62 == null || !r62.AIM()) {
            C12970iu.A1G(this.A02);
        } else {
            A00();
            this.A02.setSelected(r62.AJm(r12));
        }
        ColorStateList colorStateList = null;
        if (r12 != null) {
            if (z) {
                boolean z2 = false;
                if (r62 != null) {
                    z2 = r62.AKC(r12);
                    StickerView stickerView = this.A09.A0H;
                    if (z2) {
                        stickerView.A01 = new C55322iD(this, r12);
                    } else {
                        stickerView.A01 = null;
                    }
                }
                this.A09.A02 = z2;
            }
            C64853Hd r2 = this.A09;
            r2.A03((C30061Vy) r12, z);
            AbstractC16130oV r32 = this.A05;
            boolean z3 = false;
            if (r32 == null || (!(r62 == null || (ABe = r62.ABe()) == 0 || ABe == 2) || !r5.A0l(r32))) {
                r6 = this.A04;
                r3 = new C40481rf(((AnonymousClass1OY) r5).A0L, Collections.emptyList());
            } else {
                r5.A1H.A02(this.A05, null, (byte) 56);
                r6 = this.A04;
                r3 = this.A05.A0V;
                int i = this.A01;
                if (i == 28 || i == -1) {
                    z3 = true;
                }
            }
            r6.A00(r3, z3);
            C16150oX A00 = AbstractC15340mz.A00(r12);
            this.A08.setText(AnonymousClass3JK.A00(((AbstractC28551Oa) r5).A0K, r5.A0k.A02(r12.A0I)));
            LinearLayout linearLayout = this.A07;
            if (linearLayout != null) {
                boolean z4 = r12.A0v;
                ImageView imageView2 = this.A03;
                if (z4) {
                    if (imageView2 == null) {
                        this.A03 = new ImageView(r5.getContext());
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams.gravity = 16;
                        this.A03.setLayoutParams(layoutParams);
                        C42941w9.A08(this.A03, ((AbstractC28551Oa) r5).A0K, 0, r5.getResources().getDimensionPixelSize(R.dimen.star_padding));
                        linearLayout.addView(this.A03, 0);
                        linearLayout.setClipChildren(false);
                    }
                    this.A03.setImageDrawable(r5.getStarDrawable());
                    this.A03.setVisibility(0);
                } else if (imageView2 != null) {
                    imageView2.setVisibility(8);
                }
            }
            AnonymousClass1IS r7 = r12.A0z;
            boolean z5 = r7.A02;
            if (z5 && (imageView = this.A06) != null) {
                int A0m = r5.A0m(((AbstractC15340mz) r12).A0C);
                int A0n = r5.A0n(((AbstractC15340mz) r12).A0C);
                if (A0n != 0) {
                    colorStateList = AnonymousClass00T.A03(r5.getContext(), A0n);
                }
                C016307r.A00(colorStateList, imageView);
                imageView.setImageResource(A0m);
            }
            if (A00.A0a && !A00.A0Y) {
                r2.A01();
            } else if ((!A00.A0P || (A00.A0X && !z5)) && (!r12.A0r || !z5 || C15380n4.A0F(r7.A00))) {
                r2.A00();
            } else {
                r2.A02();
            }
            this.A0A.setOnLongClickListener(new View.OnLongClickListener(r12) { // from class: X.3Mw
                public final /* synthetic */ AbstractC16130oV A01;

                {
                    this.A01 = r2;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    AnonymousClass3FR r1 = AnonymousClass3FR.this;
                    AbstractC16130oV r4 = this.A01;
                    C60832yi r33 = r1.A0B;
                    AbstractC13890kV r22 = ((AbstractC28551Oa) r33).A0a;
                    if (r22 == null) {
                        return true;
                    }
                    r22.AeE(r1.A05);
                    r1.A00();
                    r1.A02.setSelected(r22.AJm(r4));
                    r33.A1A(r4);
                    return true;
                }
            });
            return;
        }
        StickerView stickerView2 = this.A0A;
        stickerView2.setImageDrawable(null);
        stickerView2.setOnLongClickListener(null);
    }
}
