package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.3Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63373Bi {
    public final int A00;
    public final int A01;
    public final long A02;
    public final C15580nU A03;
    public final UserJid A04;
    public final AnonymousClass1PD A05;
    public final String A06;
    public final String A07;
    public final List A08;

    public C63373Bi(C15580nU r1, UserJid userJid, AnonymousClass1PD r3, String str, String str2, List list, int i, int i2, long j) {
        this.A03 = r1;
        this.A04 = userJid;
        this.A02 = j;
        this.A07 = str;
        this.A08 = list;
        this.A01 = i;
        this.A05 = r3;
        this.A00 = i2;
        this.A06 = str2;
    }
}
