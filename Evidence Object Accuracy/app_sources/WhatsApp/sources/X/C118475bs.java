package X;

/* renamed from: X.5bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118475bs extends AnonymousClass0Yo {
    public final /* synthetic */ C30861Zc A00;
    public final /* synthetic */ AnonymousClass1ZR A01;
    public final /* synthetic */ C120515gJ A02;
    public final /* synthetic */ C128355vy A03;

    public C118475bs(C30861Zc r1, AnonymousClass1ZR r2, C120515gJ r3, C128355vy r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117925az.class)) {
            C128355vy r0 = this.A03;
            return new C117925az(r0.A0A, r0.A0C, this.A00, this.A01, this.A02);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
