package X;

import android.app.RemoteInput;

/* renamed from: X.0KV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KV {
    public static void A00(RemoteInput.Builder builder, int i) {
        builder.setEditChoicesBeforeSending(i);
    }
}
