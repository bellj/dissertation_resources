package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker;
import com.whatsapp.calling.views.JoinableEducationDialogFragment;

/* renamed from: X.2oJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58202oJ extends AbstractC52172aN {
    public final /* synthetic */ GroupCallParticipantPicker A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58202oJ(Context context, GroupCallParticipantPicker groupCallParticipantPicker) {
        super(context, R.color.link_color);
        this.A00 = groupCallParticipantPicker;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        C004902f A0P = C12970iu.A0P(this.A00);
        A0P.A09(JoinableEducationDialogFragment.A00(), null);
        A0P.A02();
    }
}
