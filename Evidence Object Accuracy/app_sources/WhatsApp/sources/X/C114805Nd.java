package X;

/* renamed from: X.5Nd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114805Nd extends AnonymousClass5NV {
    public int A00 = -1;

    public C114805Nd() {
    }

    public C114805Nd(AnonymousClass1TN r2) {
        super(r2);
    }

    public C114805Nd(C94954co r2) {
        super(r2, false);
    }

    public C114805Nd(AnonymousClass1TN[] r2, boolean z) {
        super(r2, z);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int i = this.A00;
        if (i < 0) {
            AnonymousClass1TN[] r2 = this.A01;
            int length = r2.length;
            i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                i = C72453ed.A0S(r2, i2, i);
            }
            this.A00 = i;
        }
        return AnonymousClass1TQ.A00(i) + 1 + i;
    }
}
