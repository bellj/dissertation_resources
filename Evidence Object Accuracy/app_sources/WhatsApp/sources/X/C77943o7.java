package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.3o7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77943o7 extends AbstractC77963o9 {
    public final C108174yc A00;

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 203400000;
    }

    public C77943o7(Context context, Looper looper, AbstractC14990mN r10, AbstractC15010mP r11, AnonymousClass3BW r12, C108174yc r13) {
        super(context, looper, r10, r11, r12, 270);
        this.A00 = r13;
    }
}
