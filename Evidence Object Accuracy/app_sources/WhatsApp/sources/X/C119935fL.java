package X;

/* renamed from: X.5fL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119935fL extends AbstractC16110oT {
    public final String A00;
    public final C127585uj[] A01;

    public C119935fL(AnonymousClass00E r2, String str, C127585uj[] r4, int i, int i2) {
        super(i, r2, i2, -1);
        this.A00 = str;
        this.A01 = r4;
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r6) {
        C127585uj[] r4 = this.A01;
        for (C127585uj r0 : r4) {
            r6.Abe(r0.A00, r0.A02);
        }
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        sb.append(this.A00);
        sb.append(" {");
        C127585uj[] r4 = this.A01;
        for (C127585uj r1 : r4) {
            Object obj = r1.A01;
            if (obj != null) {
                AbstractC16110oT.appendFieldToStringBuilder(sb, r1.A03, String.valueOf(obj));
            }
        }
        return C12960it.A0d("}", sb);
    }
}
