package X;

/* renamed from: X.2TB  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass2TB {
    A01(0),
    /* Fake field, exist only in values array */
    EF15(1),
    /* Fake field, exist only in values array */
    EF23(2),
    /* Fake field, exist only in values array */
    EF31(3);
    
    public final int value;

    AnonymousClass2TB(int i) {
        this.value = i;
    }
}
