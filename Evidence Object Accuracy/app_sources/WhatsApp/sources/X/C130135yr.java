package X;

import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.text.TextUtils;
import android.util.Base64;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130135yr {
    public KeyStore A00;
    public JSONObject A01;
    public boolean A02;
    public final C16590pI A03;
    public final C18600si A04;
    public final C30931Zj A05 = C117305Zk.A0V("PaymentTrustedDeviceManager", "infra");
    public final C22120yY A06;

    public C130135yr(C16590pI r3, C18600si r4, C22120yY r5) {
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r5;
    }

    public PrivateKey A00(int i) {
        byte[] bArr;
        byte[] A06;
        PrivateKey privateKey;
        A02();
        byte[] bArr2 = null;
        String optString = this.A01.optString(String.valueOf(i), null);
        if (!TextUtils.isEmpty(optString)) {
            bArr2 = Base64.decode(optString, 3);
        }
        if (bArr2 == null) {
            A02();
            try {
                KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
                instance.initialize(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
                privateKey = instance.genKeyPair().getPrivate();
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                this.A05.A05(C12960it.A0d(e.toString(), C12960it.A0k("generate RSA key fails: ")));
            }
            if (Build.VERSION.SDK_INT >= 18) {
                byte[] A05 = A05(privateKey.getEncoded());
                if (A05 != null) {
                    A03(A05, i);
                    C18600si r4 = this.A04;
                    if (!r4.A01().getBoolean("payment_trusted_device_credential_use_keystore", false)) {
                        C12960it.A0t(C117295Zj.A05(r4), "payment_trusted_device_credential_use_keystore", true);
                    }
                    Arrays.fill(A05, (byte) 0);
                    return privateKey;
                }
                return null;
            }
            A03(privateKey.getEncoded(), i);
            return privateKey;
        }
        try {
            if (Build.VERSION.SDK_INT >= 18) {
                C18600si r7 = this.A04;
                if (!r7.A01().getBoolean("payment_trusted_device_credential_use_keystore", false)) {
                    byte[] A052 = A05(bArr2);
                    if (A052 != null) {
                        A03(bArr2, i);
                        C12960it.A0t(C117295Zj.A05(r7), "payment_trusted_device_credential_use_keystore", true);
                        Arrays.fill(A052, (byte) 0);
                    }
                } else {
                    try {
                        String string = r7.A01().getString("payment_trusted_device_credential_encrypted_aes", null);
                        if (TextUtils.isEmpty(string) || (bArr = Base64.decode(string, 3)) == null) {
                            bArr = A04();
                        }
                        if (!(bArr == null || (A06 = A06(bArr)) == null)) {
                            byte[] bArr3 = new byte[16];
                            System.arraycopy(bArr2, 0, bArr3, 0, 16);
                            int length = bArr2.length - 16;
                            byte[] bArr4 = new byte[length];
                            System.arraycopy(bArr2, 16, bArr4, 0, length);
                            SecretKeySpec secretKeySpec = new SecretKeySpec(A06, "AES");
                            Cipher instance2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
                            instance2.init(2, secretKeySpec, new IvParameterSpec(bArr3));
                            bArr2 = instance2.doFinal(bArr4);
                        }
                    } catch (Exception e2) {
                        this.A05.A05(C12960it.A0d(e2.toString(), C12960it.A0k("decrypt key fails: ")));
                    }
                    bArr2 = null;
                }
            }
            if (bArr2 == null) {
                return null;
            }
            PKCS8EncodedKeySpec pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(bArr2);
            KeyFactory instance3 = KeyFactory.getInstance("RSA");
            Arrays.fill(bArr2, (byte) 0);
            return instance3.generatePrivate(pKCS8EncodedKeySpec);
        } catch (Exception e3) {
            this.A05.A05(C12960it.A0d(e3.toString(), C12960it.A0k("loadRSAKey fails, ")));
            return null;
        }
    }

    public final void A01() {
        if (!C12980iv.A1W(this.A04.A01(), "payment_trusted_device_credential_use_keystore")) {
            try {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                instance2.add(1, 50);
                KeyPairGeneratorSpec build = new KeyPairGeneratorSpec.Builder(this.A03.A00).setAlias("payment_trusted_device_key_alias").setSubject(new X500Principal("CN=payment_trusted_device_key_alias")).setSerialNumber(BigInteger.TEN).setStartDate(instance.getTime()).setEndDate(instance2.getTime()).build();
                KeyPairGenerator instance3 = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                instance3.initialize(build);
                instance3.generateKeyPair();
            } catch (Exception e) {
                this.A05.A05(C12960it.A0d(e.toString(), C12960it.A0k("generate RSA key pairs fails: ")));
            }
            A04();
        }
    }

    public final synchronized void A02() {
        JSONObject A0a;
        byte[] decode;
        if (!this.A02) {
            if (this.A01 == null) {
                try {
                    String A0p = C12980iv.A0p(this.A04.A01(), "payments_trusted_device_credential_network_map");
                    if (A0p != null) {
                        A0a = C13000ix.A05(A0p);
                    } else {
                        A0a = C117295Zj.A0a();
                    }
                    this.A01 = A0a;
                } catch (JSONException e) {
                    this.A05.A05(C12960it.A0d(e.getMessage(), C12960it.A0k("JSONObject instantiation ")));
                    this.A01 = C117295Zj.A0a();
                }
            }
            if (Build.VERSION.SDK_INT >= 18) {
                try {
                    KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
                    this.A00 = instance;
                    instance.load(null);
                    if (!C12980iv.A1W(this.A04.A01(), "payment_trusted_device_credential_use_keystore")) {
                        A01();
                    }
                    this.A02 = true;
                } catch (Exception e2) {
                    C30931Zj r2 = this.A05;
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("keystore init fails: ");
                    r2.A05(C12960it.A0d(e2.toString(), A0h));
                }
            } else {
                this.A02 = true;
            }
            String A0p2 = C12980iv.A0p(this.A04.A01(), "payment_trusted_device_credential");
            if (!TextUtils.isEmpty(A0p2) && (decode = Base64.decode(A0p2, 3)) != null) {
                A03(decode, 1);
            }
        }
    }

    public synchronized void A03(byte[] bArr, int i) {
        try {
            this.A01.put(String.valueOf(i), Base64.encodeToString(bArr, 3));
            C18600si r1 = this.A04;
            C12970iu.A1D(C117295Zj.A05(r1), "payments_trusted_device_credential_network_map", this.A01.toString());
        } catch (JSONException unused) {
            this.A05.A05("setNetworkCredential failed");
        }
    }

    public final byte[] A04() {
        byte[] A1a = C117305Zk.A1a(16);
        byte[] bArr = null;
        try {
            bArr = C117295Zj.A0P("RSA/ECB/PKCS1Padding", (KeyStore.PrivateKeyEntry) this.A00.getEntry("payment_trusted_device_key_alias", null), A1a).toByteArray();
        } catch (Exception e) {
            this.A05.A05(C12960it.A0d(e.toString(), C12960it.A0k("RSA encrypt fails: ")));
        }
        if (bArr != null) {
            C18600si r1 = this.A04;
            C12970iu.A1D(C117295Zj.A05(r1), "payment_trusted_device_credential_encrypted_aes", Base64.encodeToString(bArr, 3));
        }
        Arrays.fill(A1a, (byte) 0);
        return bArr;
    }

    public final byte[] A05(byte[] bArr) {
        byte[] bArr2;
        byte[] A06;
        try {
            String string = this.A04.A01().getString("payment_trusted_device_credential_encrypted_aes", null);
            if (TextUtils.isEmpty(string) || (bArr2 = Base64.decode(string, 3)) == null) {
                bArr2 = A04();
            }
            if (bArr2 == null || (A06 = A06(bArr2)) == null) {
                return null;
            }
            byte[] A1a = C117305Zk.A1a(16);
            SecretKeySpec secretKeySpec = new SecretKeySpec(A06, "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, new IvParameterSpec(A1a));
            byte[] doFinal = instance.doFinal(bArr);
            int length = doFinal.length;
            byte[] bArr3 = new byte[16 + length];
            System.arraycopy(A1a, 0, bArr3, 0, 16);
            System.arraycopy(doFinal, 0, bArr3, 16, length);
            return bArr3;
        } catch (Exception e) {
            this.A05.A05(C12960it.A0d(e.toString(), C12960it.A0k("encrypt key fails: ")));
            return null;
        }
    }

    public final byte[] A06(byte[] bArr) {
        try {
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            instance.init(2, ((KeyStore.PrivateKeyEntry) this.A00.getEntry("payment_trusted_device_key_alias", null)).getPrivateKey());
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            CipherInputStream cipherInputStream = new CipherInputStream(byteArrayInputStream, instance);
            try {
                ArrayList A0l = C12960it.A0l();
                while (true) {
                    int read = cipherInputStream.read();
                    if (read == -1) {
                        break;
                    }
                    A0l.add(Byte.valueOf((byte) read));
                }
                int size = A0l.size();
                byte[] bArr2 = new byte[size];
                for (int i = 0; i < size; i++) {
                    bArr2[i] = ((Byte) A0l.get(i)).byteValue();
                }
                cipherInputStream.close();
                byteArrayInputStream.close();
                return bArr2;
            } catch (Throwable th) {
                try {
                    cipherInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception e) {
            this.A05.A05(C12960it.A0d(e.toString(), C12960it.A0k("RSA decrypt fails: ")));
            return null;
        }
    }
}
