package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.0rQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17810rQ implements AbstractC16940q0 {
    public final AbstractC16940q0 A00;

    public C17810rQ(AbstractC16940q0 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16940q0
    public /* bridge */ /* synthetic */ Object A7i(JSONObject jSONObject, long j) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2;
        Object A7i;
        try {
            HashSet hashSet = new HashSet();
            Collections.addAll(hashSet, "xwa_product_catalog_get_product_list");
            if (AnonymousClass3GH.A01(hashSet, jSONObject) && (optJSONObject = jSONObject.optJSONObject("xwa_product_catalog_get_product_list")) != null) {
                HashSet hashSet2 = new HashSet();
                Collections.addAll(hashSet2, "product_list");
                if (AnonymousClass3GH.A01(hashSet2, optJSONObject) && (optJSONObject2 = optJSONObject.optJSONObject("product_list")) != null) {
                    boolean z = true;
                    HashSet hashSet3 = new HashSet();
                    Collections.addAll(hashSet3, "products");
                    if (AnonymousClass3GH.A01(hashSet3, optJSONObject2)) {
                        JSONArray optJSONArray = optJSONObject2.optJSONArray("products");
                        if (optJSONArray == null) {
                            return new C90834Pk(4);
                        }
                        ArrayList arrayList = new ArrayList();
                        for (int i = 0; i < optJSONArray.length(); i++) {
                            JSONObject optJSONObject3 = optJSONArray.optJSONObject(i);
                            if (!(optJSONObject3 == null || (A7i = this.A00.A7i(optJSONObject3, j)) == null)) {
                                arrayList.add(A7i);
                            }
                        }
                        String A00 = AnonymousClass3GH.A00("cart_enabled", optJSONObject2);
                        if (TextUtils.isEmpty(A00) || !A00.equals("CARTENABLED_TRUE")) {
                            z = false;
                        }
                        if (arrayList.isEmpty()) {
                            return new C90834Pk(4);
                        }
                        C90834Pk r1 = new C90834Pk(1);
                        r1.A01 = arrayList;
                        r1.A02 = z;
                        return r1;
                    }
                }
            }
            return new C90834Pk(4);
        } catch (Exception e) {
            Log.e("CatalogPageGraphQLResponseConverter/convert/Could not create CatalogPage from GetProductList GraphQL response", e);
            return new C90834Pk(2);
        }
    }
}
