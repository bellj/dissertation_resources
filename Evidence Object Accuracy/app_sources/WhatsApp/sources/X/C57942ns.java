package X;

import android.util.Log;
import android.util.Pair;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57942ns extends AnonymousClass4WH {
    public final /* synthetic */ String A00;
    public final /* synthetic */ String A01;

    public C57942ns(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ void A00(Object obj) {
        String str;
        Pair A01 = C64943Hn.A01((AnonymousClass28D) obj, this.A01);
        List list = (List) A01.first;
        int A05 = C12960it.A05(A01.second);
        int A00 = C64943Hn.A00(this.A00, list);
        if (A05 == -1) {
            str = "removeChildren: The starting id doesn't exist. No children have been removed.";
        } else if (A00 == -1) {
            str = "removeChildren: The ending id doesn't exist. No children have been removed.";
        } else if (A05 > A00) {
            str = "removeChildren: The starting index is larger than the ending index. No children have been removed.";
        } else {
            Iterator it = list.iterator();
            int i = 0;
            while (it.hasNext()) {
                it.next();
                if (i > A05 && i < A00) {
                    it.remove();
                }
                i++;
            }
            return;
        }
        Log.w("ComponentTree", str);
    }
}
