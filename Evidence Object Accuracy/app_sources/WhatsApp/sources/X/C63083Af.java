package X;

import android.util.Base64;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/* renamed from: X.3Af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63083Af {
    public static PublicKey A00(String str) {
        if (!str.startsWith("-----BEGIN PUBLIC KEY-----\n") || !str.endsWith("\n-----END PUBLIC KEY-----\n")) {
            throw new InvalidKeySpecException("malformed string");
        }
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str.substring(26, str.length() - 26), 2)));
    }
}
