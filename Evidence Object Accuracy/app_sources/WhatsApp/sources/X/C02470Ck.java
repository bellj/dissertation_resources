package X;

import java.util.Map;

/* renamed from: X.0Ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02470Ck extends AbstractC008904n {
    public final /* synthetic */ AnonymousClass00N A00;

    public C02470Ck(AnonymousClass00N r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC008904n
    public int A01() {
        return ((AnonymousClass00O) this.A00).A00;
    }

    @Override // X.AbstractC008904n
    public int A02(Object obj) {
        return this.A00.A03(obj);
    }

    @Override // X.AbstractC008904n
    public Object A03(int i, int i2) {
        return this.A00.A02[(i << 1) + i2];
    }

    @Override // X.AbstractC008904n
    public Object A04(int i, Object obj) {
        int i2 = (i << 1) + 1;
        Object[] objArr = this.A00.A02;
        Object obj2 = objArr[i2];
        objArr[i2] = obj;
        return obj2;
    }

    @Override // X.AbstractC008904n
    public Map A05() {
        return this.A00;
    }

    @Override // X.AbstractC008904n
    public void A06() {
        this.A00.clear();
    }

    @Override // X.AbstractC008904n
    public void A07(int i) {
        this.A00.A06(i);
    }
}
