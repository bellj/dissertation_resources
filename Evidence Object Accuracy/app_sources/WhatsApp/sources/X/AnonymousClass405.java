package X;

/* renamed from: X.405  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass405 extends C37171lc {
    public final int A00;

    public AnonymousClass405(int i) {
        super(AnonymousClass39o.A0D);
        this.A00 = i;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || this.A00 != ((AnonymousClass405) obj).A00) {
            return false;
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A00;
    }
}
