package X;

import android.text.TextPaint;
import android.text.style.UnderlineSpan;

/* renamed from: X.3gR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73563gR extends UnderlineSpan {
    public final /* synthetic */ AnonymousClass3FC A00;

    public C73563gR(AnonymousClass3FC r1) {
        this.A00 = r1;
    }

    @Override // android.text.style.UnderlineSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
    }
}
