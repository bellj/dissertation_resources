package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug;
import android.view.ViewGroup;

/* renamed from: X.0Ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02390Ca extends AnonymousClass0Bl {
    @ViewDebug.ExportedProperty
    public int A00;
    @ViewDebug.ExportedProperty
    public int A01;
    @ViewDebug.ExportedProperty
    public boolean A02;
    public boolean A03;
    @ViewDebug.ExportedProperty
    public boolean A04;
    @ViewDebug.ExportedProperty
    public boolean A05;

    public C02390Ca() {
        super(-2);
        this.A04 = false;
    }

    public C02390Ca(C02390Ca r2) {
        super(r2);
        this.A04 = r2.A04;
    }

    public C02390Ca(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public C02390Ca(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
