package X;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3CJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CJ {
    public final AnonymousClass1B8 A00;

    public AnonymousClass3CJ(AnonymousClass1B8 r1) {
        this.A00 = r1;
    }

    public void A00(Context context, AbstractC116655Wh r12) {
        View A0N = C12980iv.A0N(context, R.layout.dialog_location_request);
        TextView A0I = C12960it.A0I(A0N, R.id.permission_message);
        ImageView A0K = C12970iu.A0K(A0N, R.id.permission_image);
        this.A00.A00(context, C12970iu.A0T(A0N, R.id.learn_more_view), context.getString(R.string.biz_dir_location_request_dialog_learn_more), "security-and-privacy", "how-to-select-a-location-when-looking-for-businesses-nearby");
        A0I.setText(R.string.permission_location_info_on_searching_businesses);
        A0K.setImageResource(R.drawable.permission_location);
        View A0D = AnonymousClass028.A0D(A0N, R.id.submit);
        View A0D2 = AnonymousClass028.A0D(A0N, R.id.cancel);
        C004802e A0S = C12980iv.A0S(context);
        A0S.setView(A0N);
        A0S.A0B(true);
        AnonymousClass04S create = A0S.create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: X.4hC
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                AbstractC116655Wh.this.ASE();
            }
        });
        if (create.getWindow() != null) {
            create.getWindow().setBackgroundDrawable(C12980iv.A0L(context, R.color.transparent));
        }
        C12960it.A13(A0D, r12, create, 10);
        C12960it.A13(A0D2, create, r12, 9);
        create.setCanceledOnTouchOutside(false);
        create.show();
    }
}
