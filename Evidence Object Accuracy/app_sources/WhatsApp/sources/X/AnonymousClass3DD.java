package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.mediacomposer.bottombar.caption.CaptionView;

/* renamed from: X.3DD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DD {
    public final Context A00;
    public final AnonymousClass01d A01;
    public final AnonymousClass018 A02;
    public final AnonymousClass19M A03;
    public final CaptionView A04;
    public final C16630pM A05;

    public AnonymousClass3DD(AnonymousClass01d r2, AnonymousClass018 r3, AnonymousClass19M r4, CaptionView captionView, C16630pM r6) {
        this.A04 = captionView;
        this.A00 = captionView.getContext();
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = r6;
        AnonymousClass23N.A02(captionView, R.string.add_caption);
        captionView.setLayoutTransition(new C72863fI());
    }

    public void A00(Integer num) {
        int intValue;
        View view;
        int i;
        CaptionView captionView;
        WaImageView waImageView;
        Context context;
        int i2;
        if (num != null && (intValue = num.intValue()) != -1) {
            if (intValue != 0) {
                if (intValue != 1) {
                    if (intValue == 2) {
                        captionView = this.A04;
                        captionView.A09.setEnabled(true);
                        waImageView = captionView.A0A;
                        waImageView.setVisibility(0);
                        waImageView.setActivated(false);
                        waImageView.setEnabled(true);
                        context = captionView.A03;
                        i2 = R.string.view_once_turn_on;
                    } else if (intValue == 3) {
                        captionView = this.A04;
                        captionView.A09.setEnabled(false);
                        waImageView = captionView.A0A;
                        waImageView.setVisibility(0);
                        waImageView.setActivated(true);
                        waImageView.setEnabled(true);
                        context = captionView.A03;
                        i2 = R.string.view_once_turn_off;
                    } else {
                        throw C12960it.A0U(C12960it.A0b("Unexpected value: ", num));
                    }
                    C12960it.A0r(context, waImageView, i2);
                    view = captionView.A06;
                } else {
                    CaptionView captionView2 = this.A04;
                    captionView2.A09.setEnabled(true);
                    WaImageView waImageView2 = captionView2.A0A;
                    waImageView2.setVisibility(0);
                    waImageView2.setActivated(false);
                    waImageView2.setEnabled(false);
                    view = captionView2.A06;
                }
                i = 8;
            } else {
                CaptionView captionView3 = this.A04;
                captionView3.A09.setEnabled(true);
                captionView3.A0A.setVisibility(8);
                view = captionView3.A06;
                i = 0;
            }
            view.setVisibility(i);
        }
    }
}
