package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.0tc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19130tc extends AbstractC18500sY implements AbstractC19010tQ {
    public final C18460sU A00;
    public final C20130vG A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19130tc(C18460sU r3, C20130vG r4, C18480sW r5) {
        super(r5, "message_mention", 1);
        this.A00 = r3;
        this.A01 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("mentioned_jids");
        C16310on A02 = this.A05.A02();
        int i = 0;
        long j = -1;
        int i2 = 0;
        while (cursor.moveToNext()) {
            try {
                j = cursor.getLong(columnIndexOrThrow);
                List<UserJid> A01 = AnonymousClass1Y6.A01(cursor.getString(columnIndexOrThrow2));
                if (A01 == null) {
                    i2++;
                } else {
                    for (UserJid userJid : A01) {
                        if (userJid != null) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("message_row_id", Long.valueOf(j));
                            contentValues.put("jid_row_id", Long.valueOf(this.A00.A01(userJid)));
                            A02.A03.A06(contentValues, "message_mentions", 4);
                        }
                    }
                    i++;
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(i, j, i2);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_mentions", null, null);
            C21390xL r1 = this.A06;
            r1.A03("mention_message_ready");
            r1.A03("migration_message_mention_index");
            r1.A03("migration_message_mention_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("MentionMessageStore/resetDatabaseMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
