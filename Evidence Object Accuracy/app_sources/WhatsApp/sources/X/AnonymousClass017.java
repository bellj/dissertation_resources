package X;

import androidx.lifecycle.LiveData$LifecycleBoundObserver;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.017  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass017 {
    public static final Object A0A = new Object();
    public int A00;
    public int A01;
    public AnonymousClass03E A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public final Object A06;
    public final Runnable A07;
    public volatile Object A08;
    public volatile Object A09;

    public void A02() {
    }

    public void A03() {
    }

    public AnonymousClass017() {
        this.A06 = new Object();
        this.A02 = new AnonymousClass03E();
        this.A00 = 0;
        Object obj = A0A;
        this.A09 = obj;
        this.A07 = new AnonymousClass03F(this);
        this.A08 = obj;
        this.A01 = -1;
    }

    public AnonymousClass017(Object obj) {
        this.A06 = new Object();
        this.A02 = new AnonymousClass03E();
        this.A00 = 0;
        this.A09 = A0A;
        this.A07 = new AnonymousClass03F(this);
        this.A08 = obj;
        this.A01 = 0;
    }

    public static void A00(String str) {
        if (!AnonymousClass05O.A00().A03()) {
            StringBuilder sb = new StringBuilder("Cannot invoke ");
            sb.append(str);
            sb.append(" on a background thread");
            throw new IllegalStateException(sb.toString());
        }
    }

    public Object A01() {
        Object obj = this.A08;
        if (obj == A0A) {
            return null;
        }
        return obj;
    }

    public void A04(AbstractC001200n r4) {
        A00("removeObservers");
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (((AnonymousClass06P) entry.getValue()).A03(r4)) {
                A09((AnonymousClass02B) entry.getKey());
            }
        }
    }

    public void A05(AbstractC001200n r3, AnonymousClass02B r4) {
        A00("observe");
        if (((C009804x) r3.ADr()).A02 != AnonymousClass05I.DESTROYED) {
            LiveData$LifecycleBoundObserver liveData$LifecycleBoundObserver = new LiveData$LifecycleBoundObserver(r3, this, r4);
            AnonymousClass06P r0 = (AnonymousClass06P) this.A02.A02(r4, liveData$LifecycleBoundObserver);
            if (r0 == null) {
                r3.ADr().A00(liveData$LifecycleBoundObserver);
            } else if (!r0.A03(r3)) {
                throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
            }
        }
    }

    public void A06(AnonymousClass06P r5) {
        if (this.A05) {
            this.A04 = true;
            return;
        }
        this.A05 = true;
        do {
            this.A04 = false;
            if (r5 == null) {
                AnonymousClass03E r0 = this.A02;
                AnonymousClass075 r2 = new AnonymousClass075(r0);
                r0.A03.put(r2, Boolean.FALSE);
                while (r2.hasNext()) {
                    A07((AnonymousClass06P) ((Map.Entry) r2.next()).getValue());
                    if (this.A04) {
                        break;
                    }
                }
            } else {
                A07(r5);
                r5 = null;
            }
        } while (this.A04);
        this.A05 = false;
    }

    public final void A07(AnonymousClass06P r3) {
        if (!r3.A01) {
            return;
        }
        if (!r3.A02()) {
            r3.A01(false);
            return;
        }
        int i = r3.A00;
        int i2 = this.A01;
        if (i < i2) {
            r3.A00 = i2;
            r3.A02.ANq(this.A08);
        }
    }

    public void A08(AnonymousClass02B r4) {
        A00("observeForever");
        AnonymousClass0EM r2 = new AnonymousClass0EM(this, r4);
        Object A02 = this.A02.A02(r4, r2);
        if (A02 instanceof LiveData$LifecycleBoundObserver) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        } else if (A02 == null) {
            r2.A01(true);
        }
    }

    public void A09(AnonymousClass02B r3) {
        A00("removeObserver");
        AnonymousClass06P r1 = (AnonymousClass06P) this.A02.A01(r3);
        if (r1 != null) {
            r1.A00();
            r1.A01(false);
        }
    }

    public void A0A(Object obj) {
        boolean z;
        synchronized (this.A06) {
            z = false;
            if (this.A09 == A0A) {
                z = true;
            }
            this.A09 = obj;
        }
        if (z) {
            AnonymousClass05O.A00().A02(this.A07);
        }
    }

    public void A0B(Object obj) {
        A00("setValue");
        this.A01++;
        this.A08 = obj;
        A06(null);
    }
}
