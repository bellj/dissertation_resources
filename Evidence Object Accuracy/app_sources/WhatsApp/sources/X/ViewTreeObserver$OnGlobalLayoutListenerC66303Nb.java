package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.conversation.ConversationListView;

/* renamed from: X.3Nb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66303Nb implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ConversationListView A01;
    public final /* synthetic */ ScaleGestureDetector$OnScaleGestureListenerC52942c4 A02;
    public final /* synthetic */ AbstractC35401hl A03;

    public ViewTreeObserver$OnGlobalLayoutListenerC66303Nb(View view, ConversationListView conversationListView, ScaleGestureDetector$OnScaleGestureListenerC52942c4 r3, AbstractC35401hl r4) {
        this.A01 = conversationListView;
        this.A00 = view;
        this.A03 = r4;
        this.A02 = r3;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r0;
        ConversationListView conversationListView = this.A01;
        if (C252718t.A00(this.A00)) {
            C12980iv.A1E(conversationListView, this);
            conversationListView.A05();
            AbstractC35401hl r02 = this.A03;
            if (r02 != null && r02.ADU() && (r0 = this.A02) != null) {
                r0.requestLayout();
            }
        }
    }
}
