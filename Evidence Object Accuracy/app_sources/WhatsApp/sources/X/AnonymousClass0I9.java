package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;

/* renamed from: X.0I9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I9 extends AnonymousClass03S {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public Bitmap A08;
    public final float A09;

    public AnonymousClass0I9(AnonymousClass04Q r16) {
        super(r16);
        float f = super.A05;
        float f2 = f * 12.0f;
        this.A05 = f2;
        this.A00 = 0.4f * f;
        float f3 = 16.0f * f;
        this.A03 = f3;
        this.A06 = f2;
        this.A07 = 4.8f * f;
        this.A04 = 1.6f * f;
        this.A09 = f * 44.0f;
        super.A03 = 5;
        super.A02 = 1.0f;
        int ceil = (int) Math.ceil((double) (f3 * 1.08f * 2.0f));
        Bitmap createBitmap = Bitmap.createBitmap(ceil, ceil, Bitmap.Config.ARGB_8888);
        this.A08 = createBitmap;
        Canvas canvas = new Canvas(createBitmap);
        float f4 = ((float) ceil) / 2.0f;
        float f5 = this.A04;
        float f6 = f4 - f5;
        float f7 = f5 + f4;
        RectF rectF = new RectF(f6, f6, f7, f7);
        float f8 = 1.08f * this.A03;
        RadialGradient radialGradient = new RadialGradient(f4, f4, f8, new int[]{570425344, 570425344, 0}, new float[]{0.9259259f, 0.9259259f, 1.0f}, Shader.TileMode.CLAMP);
        Path path = new Path();
        Paint paint = new Paint(1);
        Paint.Style style = Paint.Style.FILL;
        paint.setStyle(style);
        paint.setShader(radialGradient);
        canvas.drawCircle(f4, f4, f8, paint);
        paint.reset();
        paint.setFlags(1);
        paint.setStyle(style);
        paint.setColor(-2046820353);
        canvas.drawCircle(f4, f4, this.A03, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-6118750);
        paint.setStrokeWidth(this.A00);
        canvas.drawCircle(f4, f4, this.A03, paint);
        paint.setFlags(1);
        paint.setStyle(style);
        paint.setColor(-1365724);
        path.reset();
        path.moveTo(f4 - this.A07, f4);
        path.lineTo(f4 - this.A04, f4);
        path.addArc(rectF, 180.0f, 90.0f);
        path.lineTo(f4, f4 - this.A06);
        path.lineTo(f4 - this.A07, f4);
        path.close();
        canvas.drawPath(path, paint);
        paint.setStyle(style);
        paint.setColor(-2811114);
        path.reset();
        path.moveTo(this.A07 + f4, f4);
        path.lineTo(this.A04 + f4, f4);
        path.addArc(rectF, 0.0f, -90.0f);
        path.lineTo(f4, f4 - this.A06);
        path.lineTo(this.A07 + f4, f4);
        path.close();
        canvas.drawPath(path, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-4013374);
        path.reset();
        path.moveTo(f4 - this.A07, f4);
        path.lineTo(f4 - this.A04, f4);
        path.addArc(rectF, 180.0f, -90.0f);
        path.lineTo(f4, this.A06 + f4);
        path.lineTo(f4 - this.A07, f4);
        path.close();
        canvas.drawPath(path, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-2434342);
        path.reset();
        path.moveTo(this.A07 + f4, f4);
        path.lineTo(this.A04 + f4, f4);
        path.addArc(rectF, 0.0f, 90.0f);
        path.lineTo(f4, this.A06 + f4);
        path.lineTo(this.A07 + f4, f4);
        path.close();
        canvas.drawPath(path, paint);
    }

    @Override // X.AnonymousClass03S
    public int A00(float f, float f2) {
        float f3 = this.A01;
        float f4 = this.A03;
        if (f >= f3 - f4 && f <= f3 + f4) {
            float f5 = this.A02;
            if (f2 >= f5 - f4 && f2 <= f5 + f4) {
                return 2;
            }
        }
        float f6 = this.A09;
        if (f < f3 - f6 || f > f3 + f6) {
            return 0;
        }
        float f7 = this.A02;
        return (f2 < f7 - f6 || f2 > f7 + f6) ? 0 : 1;
    }

    @Override // X.AnonymousClass03S
    public boolean A05(float f, float f2) {
        AnonymousClass04Q r2 = super.A09;
        C05200Oq r0 = new C05200Oq();
        r0.A00 = 0.0f;
        r2.A09(r0);
        return true;
    }

    @Override // X.AnonymousClass03S
    public void A07() {
        float f = this.A05;
        float f2 = ((float) 0) + f;
        float f3 = f + ((float) super.A09.A06);
        float f4 = this.A03;
        this.A01 = f2 + f4;
        this.A02 = f3 + f4;
    }

    @Override // X.AnonymousClass03S
    public void A08(Canvas canvas) {
        canvas.save();
        float f = super.A09.A0R.A00.A0E.A0B;
        if (f < 0.0f) {
            f += 360.0f;
        }
        canvas.rotate(f, this.A01, this.A02);
        Bitmap bitmap = this.A08;
        float f2 = this.A01;
        float f3 = this.A03;
        canvas.drawBitmap(bitmap, f2 - f3, this.A02 - f3, (Paint) null);
        canvas.restore();
    }
}
