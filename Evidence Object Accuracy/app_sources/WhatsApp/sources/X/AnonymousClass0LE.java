package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;

/* renamed from: X.0LE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LE {
    public static void A00(Animator animator, AnimatorListenerAdapter animatorListenerAdapter) {
        if (Build.VERSION.SDK_INT >= 19) {
            animator.addPauseListener(animatorListenerAdapter);
        }
    }
}
