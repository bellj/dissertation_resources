package X;

/* renamed from: X.3qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79603qr extends AbstractC79623qt {
    public final char A00;

    public C79603qr(char c) {
        this.A00 = c;
    }

    public final String toString() {
        int i = this.A00;
        char[] cArr = {'\\', 'u', 0, 0, 0, 0};
        for (int i2 = 0; i2 < 4; i2++) {
            cArr[5 - i2] = "0123456789ABCDEF".charAt(i & 15);
            i >>= 4;
        }
        String copyValueOf = String.copyValueOf(cArr);
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(copyValueOf) + 18);
        A0t.append("CharMatcher.is('");
        A0t.append(copyValueOf);
        return C12960it.A0d("')", A0t);
    }
}
