package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3C9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3C9 {
    public final AnonymousClass3II A00(String str, Map map) {
        Set set;
        AnonymousClass3II r0;
        Object obj;
        Map map2;
        C16700pc.A0E(str, 0);
        ArrayList A0l = C12960it.A0l();
        if (map == null || (set = map.entrySet()) == null) {
            set = C16900pw.A00;
        }
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry A15 = C12970iu.A15(it);
            if (A15.getValue() == null || !(A15.getValue() instanceof Map)) {
                r0 = new AnonymousClass3II(C12990iw.A0r(A15));
            } else {
                String A0r = C12990iw.A0r(A15);
                if (map == null) {
                    obj = null;
                } else {
                    obj = map.get(A15.getKey());
                }
                if (obj instanceof Map) {
                    map2 = (Map) obj;
                } else {
                    map2 = null;
                }
                r0 = A00(A0r, map2);
            }
            A0l.add(r0);
        }
        Object[] array = A0l.toArray(new AnonymousClass3II[0]);
        if (array != null) {
            return new AnonymousClass3II(str, (AnonymousClass3II[]) array);
        }
        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }
}
