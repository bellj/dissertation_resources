package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

/* renamed from: X.0Wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class animation.Animation$AnimationListenerC07020Wj implements Animation.AnimationListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewGroup A01;
    public final /* synthetic */ AnonymousClass0EA A02;
    public final /* synthetic */ AnonymousClass0EJ A03;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public animation.Animation$AnimationListenerC07020Wj(View view, ViewGroup viewGroup, AnonymousClass0EA r3, AnonymousClass0EJ r4) {
        this.A03 = r4;
        this.A01 = viewGroup;
        this.A00 = view;
        this.A02 = r3;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A01.post(new RunnableC09210cX(this));
    }
}
