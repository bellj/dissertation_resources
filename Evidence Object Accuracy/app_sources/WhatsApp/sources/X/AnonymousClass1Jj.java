package X;

import java.text.ParseException;

/* renamed from: X.1Jj  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Jj {
    public static final int[] A05 = {32, 32, 32, 32, 32};
    public final byte[] A00;
    public final byte[] A01;
    public final byte[] A02;
    public final byte[] A03;
    public final byte[] A04;

    public AnonymousClass1Jj(C27821Ji r10) {
        int[] iArr = A05;
        int length = iArr.length;
        int i = 0;
        for (int i2 : iArr) {
            i += i2;
        }
        byte[] A00 = C32891cu.A00(r10.A02, "WhatsApp Mutation Keys".getBytes(AnonymousClass01V.A0A), i);
        try {
            if (A00 != null) {
                int i3 = 0;
                for (int i4 : iArr) {
                    if (i4 >= 0) {
                        i3 += i4;
                    } else {
                        StringBuilder sb = new StringBuilder("Invalid length: ");
                        sb.append(i4);
                        throw new ParseException(sb.toString(), 0);
                    }
                }
                if (A00.length >= i3) {
                    byte[][] bArr = new byte[length];
                    int i5 = 0;
                    for (int i6 = 0; i6 < length; i6++) {
                        int i7 = iArr[i6];
                        bArr[i6] = new byte[i7];
                        System.arraycopy(A00, i5, bArr[i6], 0, i7);
                        i5 += i7;
                    }
                    this.A00 = bArr[0];
                    this.A03 = bArr[1];
                    this.A04 = bArr[2];
                    this.A02 = bArr[3];
                    this.A01 = bArr[4];
                    return;
                }
                StringBuilder sb2 = new StringBuilder("Input too small: ");
                sb2.append(AnonymousClass4ZN.A00(A00));
                throw new ParseException(sb2.toString(), 0);
            }
            throw new ParseException("Null input", 0);
        } catch (ParseException e) {
            throw new C27981Jz(e);
        }
    }
}
