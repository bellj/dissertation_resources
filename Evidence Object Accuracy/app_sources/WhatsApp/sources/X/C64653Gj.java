package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64653Gj {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;

    public C64653Gj(String str, String str2, int i, int i2, int i3, int i4) {
        this.A05 = str;
        this.A01 = i;
        this.A03 = i3;
        this.A04 = str2;
        this.A00 = i2;
        this.A02 = i4;
    }

    public static C64653Gj A00(String str) {
        try {
            JSONObject A05 = C13000ix.A05(str);
            return new C64653Gj(A05.optString("media_codec_encoder", null), A05.optString("media_codec_decoder", null), A05.optInt("color_format_encoder", -1), A05.optInt("color_format_decoder", -1), A05.optInt("forced_frame_conv_id_encoder", -1), A05.optInt("forced_frame_conv_id_decoder", -1));
        } catch (JSONException unused) {
            return null;
        }
    }
}
