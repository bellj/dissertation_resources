package X;

import android.content.ContentValues;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.14z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243014z {
    public final C15570nT A00;
    public final C20870wS A01;
    public final C15550nR A02;
    public final C22700zV A03;
    public final C14830m7 A04;
    public final C15990oG A05;
    public final C18240s8 A06;
    public final C16490p7 A07;
    public final C20590w0 A08;
    public final C18680sq A09;
    public final C18470sV A0A;
    public final C18770sz A0B;

    public C243014z(C15570nT r1, C20870wS r2, C15550nR r3, C22700zV r4, C14830m7 r5, C15990oG r6, C18240s8 r7, C16490p7 r8, C20590w0 r9, C18680sq r10, C18470sV r11, C18770sz r12) {
        this.A04 = r5;
        this.A00 = r1;
        this.A0A = r11;
        this.A06 = r7;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = r6;
        this.A0B = r12;
        this.A03 = r4;
        this.A07 = r8;
        this.A09 = r10;
        this.A08 = r9;
    }

    public final Set A00(ArrayList arrayList, List list, List list2, int i) {
        HashSet hashSet;
        if (i == 2 || i == 0) {
            HashSet hashSet2 = new HashSet();
            if (i != 0) {
                Iterator it = list2.iterator();
                while (it.hasNext()) {
                    hashSet2.add(UserJid.of((Jid) it.next()));
                }
            }
            ArrayList arrayList2 = new ArrayList();
            this.A02.A0T(arrayList2);
            hashSet = new HashSet(arrayList2.size(), 1.0f);
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                C15370n3 r2 = (C15370n3) it2.next();
                UserJid userJid = (UserJid) r2.A0B(UserJid.class);
                if (userJid != null && !hashSet2.contains(userJid) && !this.A03.A02(userJid)) {
                    hashSet.add(userJid);
                    arrayList.add(r2);
                }
            }
        } else if (i == 1) {
            hashSet = new HashSet();
            Iterator it3 = list.iterator();
            while (it3.hasNext()) {
                AbstractC14640lm r1 = (AbstractC14640lm) it3.next();
                UserJid of = UserJid.of(r1);
                C15370n3 A0A = this.A02.A0A(r1);
                if (!(of == null || A0A == null || A0A.A0C == null || this.A03.A02(of))) {
                    hashSet.add(of);
                    arrayList.add(A0A);
                }
            }
        } else {
            throw new IllegalStateException("unknown status distribution mode");
        }
        return hashSet;
    }

    public final void A01(C16310on r7, String str, String str2, String str3, Set set, int i) {
        String join = TextUtils.join(",", set);
        ContentValues contentValues = new ContentValues(6);
        contentValues.put("timestamp", Long.valueOf(this.A04.A00() / 1000));
        contentValues.put("gjid", str2);
        contentValues.put("jid", join);
        contentValues.put("action", Integer.valueOf(i));
        contentValues.put("old_phash", str);
        contentValues.put("new_phash", str3);
        r7.A03.A02(contentValues, "group_participants_history");
    }

    public void A02(AbstractC15340mz r21, boolean z) {
        Jid jid;
        int i;
        List list;
        List list2;
        if (r21 != null) {
            StringBuilder sb = new StringBuilder("status-participant-user-manager/updateParticipantsTableForNewStatus/");
            AnonymousClass1IS r1 = r21.A0z;
            sb.append(r1);
            Log.i(sb.toString());
            jid = r1.A00;
        } else {
            Log.i("status-participant-user-manager/updateParticipantsTableForNewStatus");
            jid = AnonymousClass1VX.A00;
        }
        AnonymousClass009.A0E(C15380n4.A0N(jid));
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            if (!z || r21 == null) {
                C18470sV r3 = this.A0A;
                i = r3.A03.A00("status_distribution", 0);
                list = r3.A06();
                list2 = r3.A07();
            } else {
                this.A00.A08();
                C32731ce r0 = r21.A0K;
                AnonymousClass009.A05(r0);
                i = r0.A00;
                list = r0.A01;
                list2 = r0.A02;
            }
            C18680sq r7 = this.A09;
            AnonymousClass1VX r10 = AnonymousClass1VX.A00;
            Set A022 = r7.A02(r10);
            ArrayList arrayList = new ArrayList();
            Set A002 = A00(arrayList, list, list2, i);
            C14830m7 r13 = this.A04;
            long A003 = r13.A00() + 86400000;
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C15370n3 r4 = (C15370n3) it.next();
                if (r4.A09 < A003) {
                    r4.A09 = r13.A00() + 604800000;
                    arrayList2.add(r4);
                }
            }
            this.A02.A0W(arrayList2);
            HashSet hashSet = new HashSet(A022);
            hashSet.removeAll(A002);
            HashSet hashSet2 = new HashSet(A002);
            hashSet2.removeAll(A022);
            C15570nT r15 = this.A00;
            r15.A08();
            C27631Ih r11 = r15.A05;
            AnonymousClass009.A05(r11);
            hashSet.remove(r11);
            hashSet2.add(r11);
            AnonymousClass009.A05(jid);
            AbstractC15590nW r8 = (AbstractC15590nW) jid;
            HashSet hashSet3 = new HashSet(hashSet2.size());
            if (!hashSet2.isEmpty()) {
                Iterator it2 = hashSet2.iterator();
                while (it2.hasNext()) {
                    UserJid userJid = (UserJid) it2.next();
                    Set<DeviceJid> A0D = this.A0B.A0D(userJid);
                    AnonymousClass009.A0F(!A0D.isEmpty());
                    HashSet hashSet4 = new HashSet(A0D.size());
                    for (DeviceJid deviceJid : A0D) {
                        hashSet4.add(new AnonymousClass1YP(deviceJid, false));
                    }
                    int i2 = 0;
                    if (r15.A0F(userJid)) {
                        i2 = 2;
                    }
                    AnonymousClass1YO r02 = new AnonymousClass1YO(userJid, (Set) hashSet4, i2, false);
                    r7.A04(r02, r8);
                    hashSet3.add(r02);
                }
            }
            if (!hashSet.isEmpty()) {
                r7.A09(r8, new ArrayList(hashSet));
            }
            AnonymousClass1YM A004 = r7.A06.A00(r7.A05, r10);
            A004.A0F(hashSet3);
            A004.A0G(hashSet);
            if (!hashSet.isEmpty()) {
                this.A06.A00.submit(new RunnableBRunnable0Shape0S0300000_I0(this, A004, r10, 48));
            }
            if (r21 != null) {
                r21.A0l = AnonymousClass1YM.A00(r7.A02(r8));
                r21.A0A = A002.contains(r11) ? A002.size() - 1 : A002.size();
            }
            hashSet2.size();
            hashSet.size();
            A002.size();
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v4, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public void A03(AbstractC15340mz r30, boolean z) {
        Jid jid;
        int i;
        List list;
        List list2;
        int size;
        String rawString;
        if (r30 != null) {
            StringBuilder sb = new StringBuilder("status-participant-user-manager/updateOldParticipantsTableForNewStatus/");
            AnonymousClass1IS r1 = r30.A0z;
            sb.append(r1);
            Log.i(sb.toString());
            jid = r1.A00;
        } else {
            Log.i("status-participant-user-manager/updateOldParticipantsTableForNewStatus");
            jid = AnonymousClass1VX.A00;
        }
        AnonymousClass009.A0E(C15380n4.A0N(jid));
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            if (!z || r30 == null) {
                C18470sV r3 = this.A0A;
                i = r3.A03.A00("status_distribution", 0);
                list = r3.A06();
                list2 = r3.A07();
            } else {
                C32731ce r0 = r30.A0K;
                AnonymousClass009.A05(r0);
                i = r0.A00;
                list = r0.A01;
                list2 = r0.A02;
            }
            C20590w0 r9 = this.A08;
            AnonymousClass1VX r19 = AnonymousClass1VX.A00;
            Set A002 = r9.A00(r19);
            ArrayList arrayList = new ArrayList();
            Set A003 = A00(arrayList, list, list2, i);
            C14830m7 r7 = this.A04;
            long A004 = r7.A00() + 86400000;
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C15370n3 r5 = (C15370n3) it.next();
                if (r5.A09 < A004) {
                    r5.A09 = r7.A00() + 604800000;
                    arrayList2.add(r5);
                }
            }
            this.A02.A0W(arrayList2);
            String A005 = AnonymousClass1YM.A00(A002);
            HashSet hashSet = new HashSet(A002);
            hashSet.removeAll(A003);
            HashSet hashSet2 = new HashSet(A003);
            hashSet2.removeAll(A002);
            C15570nT r14 = this.A00;
            r14.A08();
            C27631Ih r4 = r14.A05;
            AnonymousClass009.A05(r4);
            hashSet.remove(r4);
            hashSet2.add(r4);
            AnonymousClass009.A05(jid);
            AbstractC15590nW r12 = (AbstractC15590nW) jid;
            String rawString2 = r12.getRawString();
            if (!hashSet2.isEmpty()) {
                HashSet hashSet3 = new HashSet();
                Iterator it2 = hashSet2.iterator();
                while (it2.hasNext()) {
                    Jid jid2 = (Jid) it2.next();
                    if (r14.A0F(jid2)) {
                        rawString = "";
                    } else {
                        rawString = jid2.getRawString();
                    }
                    ContentValues contentValues = new ContentValues(3);
                    contentValues.put("gjid", r19.getRawString());
                    contentValues.put("jid", rawString);
                    contentValues.put("pending", Boolean.FALSE);
                    String[] strArr = {rawString2, rawString};
                    C16330op r13 = A02.A03;
                    if (r13.A00("group_participants", contentValues, "gjid = ? AND jid = ?", strArr) == 0 && r13.A02(contentValues, "group_participants") >= 0) {
                        hashSet3.add(rawString);
                    }
                }
                String A006 = AnonymousClass1YM.A00(r9.A00(r12));
                if (!TextUtils.equals(A005, A006)) {
                    A01(A02, A005, rawString2, A006, hashSet3, 1);
                }
                A005 = A006;
            }
            if (!hashSet.isEmpty()) {
                HashSet hashSet4 = new HashSet();
                HashSet hashSet5 = new HashSet();
                C15380n4.A0D(hashSet, hashSet5);
                for (Object obj : hashSet5) {
                    if (A02.A03.A01("group_participants", "gjid = ? AND jid = ?", new String[]{rawString2, obj}) > 0) {
                        hashSet4.add(obj);
                    }
                }
                String A007 = AnonymousClass1YM.A00(r9.A00(r12));
                if (!TextUtils.equals(A005, A007)) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("sent_sender_key", Boolean.FALSE);
                    A02.A03.A00("group_participants", contentValues2, "gjid = ?", new String[]{rawString2});
                    A01(A02, A005, rawString2, A007, hashSet4, 2);
                }
                A005 = A007;
            }
            AnonymousClass1YM A008 = r9.A07.A00(r9.A06, r19);
            ArrayList arrayList3 = new ArrayList(hashSet2.size());
            Iterator it3 = hashSet2.iterator();
            while (it3.hasNext()) {
                UserJid userJid = (UserJid) it3.next();
                if (userJid != null) {
                    arrayList3.add(new AnonymousClass1YO(userJid, AnonymousClass1YM.A01(Collections.singleton(DeviceJid.of(userJid))), 0, false));
                }
            }
            A008.A0F(arrayList3);
            A008.A0G(hashSet);
            if (!hashSet.isEmpty()) {
                this.A06.A00.submit(new RunnableBRunnable0Shape0S0300000_I0(this, A008, r19, 49));
            }
            if (r30 != null) {
                r30.A0l = A005;
                if (A003.contains(r4)) {
                    size = A003.size() - 1;
                } else {
                    size = A003.size();
                }
                r30.A0A = size;
            }
            hashSet2.size();
            hashSet.size();
            A003.size();
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
