package X;

import android.content.SharedPreferences;
import android.view.View;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.3Tx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68053Tx implements AbstractC116635Wf {
    public static int A07 = 1;
    public static int A08 = 2;
    public static int A09 = 3;
    public static int A0A = -1;
    public static int A0B = -1;
    public View A00;
    public final C53042cM A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C15680nj A04;
    public final C22050yP A05;
    public final C16120oU A06;

    public C68053Tx(C53042cM r2, C14830m7 r3, C14820m6 r4, C15680nj r5, C14850m9 r6, C22050yP r7, C16120oU r8) {
        this.A02 = r3;
        this.A06 = r8;
        this.A05 = r7;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        A07 = r6.A02(354);
        A08 = r6.A02(351);
        A09 = r6.A02(350);
        A0B = r6.A02(352);
        A0A = r6.A02(353);
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        C14820m6 r5 = this.A03;
        SharedPreferences sharedPreferences = r5.A00;
        int A01 = C12970iu.A01(sharedPreferences, "create_group_tip_count");
        long A0F = C12980iv.A0F(sharedPreferences, "create_group_tip_time");
        C15680nj r6 = this.A04;
        if (A0B > 0 && sharedPreferences.getInt("groups_banner_total_day_count", 0) > A0B) {
            return false;
        }
        if (A0A > 0 && sharedPreferences.getInt("groups_banner_click_count", 0) >= A0A) {
            return false;
        }
        r6.A00.A0B();
        ArrayList arrayList = r6.A01;
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (C15380n4.A0J(((AnonymousClass1W2) it.next()).A01)) {
                    return false;
                }
            }
            if (r6.A01() < A09 || A01 >= A07 || A0F + 2592000000L >= this.A02.A00()) {
                return false;
            }
            if (sharedPreferences.getInt("education_banner_count", 0) < 3) {
                return true;
            }
            if (!r5.A1J("education_banner_timestamp", ((long) 7) * 86400000)) {
                return false;
            }
            r5.A0P(0);
            return true;
        }
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (this.A00 == null) {
            C53042cM r2 = this.A01;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.groups_banner);
            this.A00 = A0F;
            r2.addView(A0F);
        }
        View view = this.A00;
        if (view == null) {
            C53042cM r22 = this.A01;
            view = C12960it.A0F(C12960it.A0E(r22), r22, R.layout.groups_banner);
            this.A00 = view;
        }
        C53042cM r5 = this.A01;
        r5.setBackgroundResource(R.color.banner_info_bg);
        C12960it.A10(r5, this, 35);
        C12960it.A10(AnonymousClass028.A0D(view, R.id.dismiss_groups_banner_container), this, 34);
        r5.A00(1, 1);
        C14820m6 r6 = this.A03;
        if (r6.A1J("education_banner_timestamp", 86400000)) {
            SharedPreferences sharedPreferences = r6.A00;
            r6.A0P(sharedPreferences.getInt("education_banner_count", 0) + 1);
            C12960it.A0u(sharedPreferences, "groups_banner_total_day_count", sharedPreferences.getInt("groups_banner_total_day_count", 0) + 1);
            r6.A0k("education_banner_timestamp");
        }
        View view2 = this.A00;
        if (view2 == null) {
            view2 = C12960it.A0F(C12960it.A0E(r5), r5, R.layout.groups_banner);
            this.A00 = view2;
        }
        view2.setVisibility(0);
    }
}
