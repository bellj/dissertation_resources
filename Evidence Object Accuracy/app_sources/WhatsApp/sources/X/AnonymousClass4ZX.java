package X;

import java.util.regex.Pattern;

/* renamed from: X.4ZX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZX {
    public static final Pattern A00 = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
    public static final Pattern A01 = Pattern.compile("^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$");
    public static final Pattern A02 = Pattern.compile("^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$");

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:39:0x00b3 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v0, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r7v1, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r7v2, types: [java.util.AbstractMap, java.util.HashMap] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map A00(java.net.URI r9) {
        /*
            java.lang.String r8 = "UTF-8"
            java.util.Map r7 = java.util.Collections.emptyMap()
            java.lang.String r3 = r9.getRawQuery()
            if (r3 == 0) goto L_0x00b3
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x00b3
            java.util.HashMap r7 = X.C12970iu.A11()
            r0 = 61
            X.3qr r1 = new X.3qr
            r1.<init>(r0)
            X.4IV r0 = new X.4IV
            r0.<init>(r1)
            X.3qs r2 = X.C79613qs.A00
            r6 = 0
            X.4PP r5 = new X.4PP
            r5.<init>(r2, r0, r6)
            r1 = 38
            X.3qr r0 = new X.3qr
            r0.<init>(r1)
            X.4IV r1 = new X.4IV
            r1.<init>(r0)
            X.4PP r0 = new X.4PP
            r0.<init>(r2, r1, r6)
            X.4IV r2 = r0.A01
            X.4DS r0 = r0.A00
            r4 = 1
            X.4PP r1 = new X.4PP
            r1.<init>(r0, r2, r4)
            X.5Bg r0 = new X.5Bg
            r0.<init>(r1, r3)
            java.util.Iterator r9 = r0.iterator()
        L_0x004e:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x00b3
            java.lang.String r1 = X.C12970iu.A0x(r9)
            X.4IV r0 = r5.A01
            X.3qq r2 = new X.3qq
            r2.<init>(r0, r5, r1)
            java.util.ArrayList r1 = X.C12960it.A0l()
        L_0x0063:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0071
            java.lang.Object r0 = r2.next()
            r1.add(r0)
            goto L_0x0063
        L_0x0071:
            java.util.List r3 = java.util.Collections.unmodifiableList(r1)
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x00ac
            int r0 = r3.size()
            r2 = 2
            if (r0 > r2) goto L_0x00ac
            java.lang.String r0 = X.C12960it.A0g(r3, r6)
            java.lang.String r1 = java.net.URLDecoder.decode(r0, r8)     // Catch: UnsupportedEncodingException -> 0x00a5
            int r0 = r3.size()
            if (r0 != r2) goto L_0x0099
            java.lang.String r0 = X.C12960it.A0g(r3, r4)
            java.lang.String r0 = java.net.URLDecoder.decode(r0, r8)     // Catch: UnsupportedEncodingException -> 0x009e
            goto L_0x009a
        L_0x0099:
            r0 = 0
        L_0x009a:
            r7.put(r1, r0)
            goto L_0x004e
        L_0x009e:
            r1 = move-exception
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x00a5:
            r1 = move-exception
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x00ac:
            java.lang.String r0 = "bad parameter"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x00b3:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4ZX.A00(java.net.URI):java.util.Map");
    }
}
