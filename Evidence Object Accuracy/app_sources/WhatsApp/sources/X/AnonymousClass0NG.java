package X;

import android.content.Context;
import android.os.PowerManager;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.0NG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0NG {
    public static AnonymousClass0NG A02;
    public final PowerManager A00;
    public final ExecutorService A01 = Executors.newSingleThreadExecutor();

    public AnonymousClass0NG(Context context) {
        this.A00 = (PowerManager) context.getSystemService("power");
    }
}
