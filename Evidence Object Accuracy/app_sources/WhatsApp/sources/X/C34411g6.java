package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1g6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34411g6 extends AnonymousClass1JQ implements AbstractC34421g7 {
    public final long A00;
    public final AbstractC14640lm A01;
    public final AnonymousClass1IS A02;
    public final boolean A03;

    public C34411g6(AnonymousClass1JR r12, AbstractC14640lm r13, AnonymousClass1IS r14, String str, long j, long j2, boolean z, boolean z2) {
        super(C27791Jf.A03, r12, str, "regular_high", 3, j, z2);
        this.A02 = r14;
        this.A01 = r13;
        this.A03 = z;
        this.A00 = j2;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34401g5.A03.A0T();
        boolean z = this.A03;
        A0T.A03();
        C34401g5 r1 = (C34401g5) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = z;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(this.A00);
        A0T.A03();
        C34401g5 r12 = (C34401g5) A0T.A00;
        r12.A00 |= 2;
        r12.A01 = seconds;
        A01.A03();
        C27831Jk r13 = (C27831Jk) A01.A00;
        r13.A08 = (C34401g5) A0T.A02();
        r13.A00 |= DefaultCrypto.BUFFER_SIZE;
        return A01;
    }

    @Override // X.AbstractC34421g7, X.AbstractC34381g3
    public /* synthetic */ AbstractC14640lm ABH() {
        AbstractC14640lm r0 = this.A02.A00;
        AnonymousClass009.A05(r0);
        return r0;
    }

    @Override // X.AbstractC34421g7
    public AnonymousClass1IS AEI() {
        return this.A02;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("DeleteMessageForMeMutation{rowId=");
        sb.append(this.A07);
        sb.append(", key=");
        sb.append(this.A02);
        sb.append(", participant=");
        sb.append(this.A01);
        sb.append(", deleteMedia=");
        sb.append(this.A03);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", messageTimestamp=");
        sb.append(this.A00);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
