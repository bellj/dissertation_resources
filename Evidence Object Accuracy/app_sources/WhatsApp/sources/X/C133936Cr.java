package X;

/* renamed from: X.6Cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133936Cr implements AbstractC136356Mf {
    public final /* synthetic */ C123505nG A00;

    public C133936Cr(C123505nG r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136356Mf
    public void AOE() {
        C123505nG r3 = this.A00;
        C12960it.A0t(C117295Zj.A05(r3.A09.A01), "settingsQuickTipDismissedState", true);
        AbstractC118085bF.A00((AbstractC118085bF) r3);
    }

    @Override // X.AbstractC136356Mf
    public void AOm() {
        C123505nG r4 = this.A00;
        C126885tb r3 = r4.A09;
        C12960it.A0t(C117295Zj.A05(r3.A01), "settingsQuickTipDismissedState", true);
        r3.A00 = true;
        AbstractC118085bF.A00((AbstractC118085bF) r4);
    }
}
