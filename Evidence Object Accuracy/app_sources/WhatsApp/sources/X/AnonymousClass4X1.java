package X;

import java.security.SecureRandom;

/* renamed from: X.4X1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4X1 {
    public int A00;
    public SecureRandom A01;

    public byte[] A01() {
        byte[] bArr = new byte[this.A00];
        this.A01.nextBytes(bArr);
        return bArr;
    }

    public void A00(C90574Ok r2) {
        this.A01 = r2.A01;
        this.A00 = (r2.A00 + 7) >> 3;
    }
}
