package X;

import java.util.List;

/* renamed from: X.0bE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08460bE implements AbstractC12120hP {
    public List A00;

    public C08460bE(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC12120hP
    public boolean ALK(AnonymousClass0K2 r3, AnonymousClass0I1 r4) {
        for (AnonymousClass0PE r0 : this.A00) {
            if (AnonymousClass0UX.A02(r3, r0, r4)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("not(");
        sb.append(this.A00);
        sb.append(")");
        return sb.toString();
    }
}
