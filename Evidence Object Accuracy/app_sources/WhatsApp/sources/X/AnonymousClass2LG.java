package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;

/* renamed from: X.2LG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LG implements View.OnLayoutChangeListener {
    public final /* synthetic */ HomeActivity A00;

    public AnonymousClass2LG(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        HomeActivity homeActivity = this.A00;
        int A2e = homeActivity.A2e() + homeActivity.getResources().getDimensionPixelSize(R.dimen.tab_height);
        if (homeActivity.A06 == null) {
            int i9 = i4 - i2;
            Pair A03 = HomeActivity.A03(HomeActivity.A25, Math.abs(i3 - i), i9, i9);
            Animator A2g = homeActivity.A2g(i4 - A2e);
            int height = homeActivity.A08.getHeight();
            homeActivity.A08.clearAnimation();
            ValueAnimator ofInt = ValueAnimator.ofInt(height, i4);
            ofInt.addUpdateListener(new C49542Le(new FrameLayout.LayoutParams(-1, -2), new LinearLayout.LayoutParams(-1, 0, 1.0f), homeActivity, true));
            AnimatorSet animatorSet = new AnimatorSet();
            homeActivity.A06 = animatorSet;
            animatorSet.setStartDelay((long) ((Number) A03.second).intValue());
            homeActivity.A06.setDuration((long) ((Number) A03.first).intValue());
            homeActivity.A06.setInterpolator(homeActivity.A1t);
            homeActivity.A06.playTogether(ofInt, A2g);
            homeActivity.A06.addListener(new AnonymousClass2NB(this));
            homeActivity.A06.start();
        }
    }
}
