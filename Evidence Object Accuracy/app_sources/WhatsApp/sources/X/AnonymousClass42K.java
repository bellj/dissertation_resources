package X;

import android.graphics.RectF;
import android.util.Pair;

/* renamed from: X.42K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42K extends AbstractC65123If {
    @Override // X.AbstractC65123If
    public int A04() {
        return 72;
    }

    public AnonymousClass42K(int i) {
        super(i);
    }

    @Override // X.AbstractC65123If
    public RectF A05(int i, int i2) {
        throw C12970iu.A0z();
    }

    @Override // X.AbstractC65123If
    public Pair A07(int i, int i2) {
        return A08(i, i2, A03());
    }
}
