package X;

import java.util.Calendar;

/* renamed from: X.5xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129565xv {
    public final C125675rd A00;
    public final C30931Zj A01 = C117305Zk.A0V("PaymentsDobManager", "infra");
    public final C22650zQ A02;

    public C129565xv(C125675rd r3, C22650zQ r4) {
        this.A02 = r4;
        this.A00 = r3;
    }

    public AnonymousClass1V8 A00(int i, int i2, int i3) {
        String str;
        C125675rd r1 = this.A00;
        if (i2 < 0 || i2 > 11) {
            throw C12970iu.A0f(C12960it.A0W(i2, "Months are 0 indexed, invalid month: "));
        }
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2, i3);
        instance.setLenient(false);
        try {
            instance.getTime();
            Calendar instance2 = Calendar.getInstance();
            instance2.setTimeInMillis(r1.A00.A00());
            int i4 = instance2.get(1) - instance.get(1);
            int i5 = instance.get(2);
            int i6 = instance2.get(2);
            if (i5 > i6 || (i5 == i6 && instance.get(5) > instance2.get(5))) {
                i4--;
            }
            int i7 = 13;
            if (this.A02.A04()) {
                i7 = 16;
            }
            if (i4 < i7) {
                str = "2";
            } else if (i4 < 18) {
                str = "1";
            } else {
                str = "0";
            }
            if (str.equals("0")) {
                AnonymousClass1W9[] r3 = new AnonymousClass1W9[4];
                C12960it.A1M("state", "0", r3, 0);
                r3[1] = new AnonymousClass1W9("day", i3);
                r3[2] = new AnonymousClass1W9("month", i2 + 1);
                r3[3] = new AnonymousClass1W9("year", i);
                return new AnonymousClass1V8("dob", r3);
            }
            AnonymousClass1W9[] r0 = new AnonymousClass1W9[1];
            C12960it.A1M("state", str, r0, 0);
            return new AnonymousClass1V8("dob", r0);
        } catch (Exception unused) {
            StringBuilder A0k = C12960it.A0k("Date format invalid. Year: ");
            A0k.append(i);
            A0k.append(" Month: ");
            A0k.append(i2);
            A0k.append(" Day: ");
            throw C12970iu.A0f(C12960it.A0f(A0k, i3));
        }
    }
}
