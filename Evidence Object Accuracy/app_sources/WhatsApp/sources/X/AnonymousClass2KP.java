package X;

import android.util.Base64;
import com.whatsapp.util.Log;

/* renamed from: X.2KP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KP {
    public AnonymousClass1JS A00;
    public String A01;
    public String A02;
    public byte[] A03;
    public byte[] A04;

    public AnonymousClass2KP(AnonymousClass1JS r1, String str, String str2, byte[] bArr, byte[] bArr2) {
        this.A02 = str;
        this.A03 = bArr;
        this.A01 = str2;
        this.A00 = r1;
        this.A04 = bArr2;
    }

    public static AnonymousClass2KP A00(String str) {
        String str2;
        AnonymousClass1JS r9;
        byte[] decode;
        String[] split = str.split(",");
        int length = split.length;
        String str3 = null;
        if (length < 3) {
            str2 = "qrData/processQR/error/invalid_code parts";
        } else {
            boolean z = false;
            if (length >= 4) {
                z = true;
            }
            if (z) {
                try {
                    r9 = new AnonymousClass1JS(C15940oB.A01(C16050oM.A05(new byte[]{5}, Base64.decode(split[2], 0))));
                    decode = Base64.decode(split[3], 0);
                } catch (AnonymousClass1RY unused) {
                    str2 = "qrData/processQR/error/invalid identity key";
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder("qrData/processQR/error/");
                    sb.append(e);
                    str2 = sb.toString();
                }
            } else {
                r9 = null;
                decode = null;
            }
            try {
                byte[] decode2 = Base64.decode(split[1], 0);
                String str4 = split[0];
                if (!z) {
                    str3 = split[2];
                }
                return new AnonymousClass2KP(r9, str4, str3, decode2, decode);
            } catch (IllegalArgumentException unused2) {
                str2 = "qrData/processQR/error/invalid public key";
            }
        }
        Log.e(str2);
        return null;
    }
}
