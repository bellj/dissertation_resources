package X;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.whatsapp.R;

/* renamed from: X.04S  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04S extends AnonymousClass04T implements DialogInterface {
    public final AnonymousClass0U5 A00 = new AnonymousClass0U5(getContext(), getWindow(), this);

    public AnonymousClass04S(Context context, int i) {
        super(context, A00(context, i));
    }

    public static int A00(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void A03(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        AnonymousClass0U5 r2 = this.A00;
        Message obtainMessage = r2.A08.obtainMessage(i, onClickListener);
        if (i == -3) {
            r2.A0O = charSequence;
            r2.A0A = obtainMessage;
        } else if (i == -2) {
            r2.A0N = charSequence;
            r2.A09 = obtainMessage;
        } else if (i == -1) {
            r2.A0P = charSequence;
            r2.A0B = obtainMessage;
        } else {
            throw new IllegalArgumentException("Button does not exist");
        }
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00.A03();
    }

    @Override // android.app.Dialog, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.A00.A0M;
        if (nestedScrollView == null || !nestedScrollView.A0C(keyEvent)) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    @Override // android.app.Dialog, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        NestedScrollView nestedScrollView = this.A00.A0M;
        if (nestedScrollView == null || !nestedScrollView.A0C(keyEvent)) {
            return super.onKeyUp(i, keyEvent);
        }
        return true;
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        AnonymousClass0U5 r0 = this.A00;
        r0.A0R = charSequence;
        TextView textView = r0.A0L;
        if (textView != null) {
            textView.setText(charSequence);
        }
    }
}
