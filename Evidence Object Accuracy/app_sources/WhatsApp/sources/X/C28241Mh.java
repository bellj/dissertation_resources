package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28241Mh {
    public Object[] alternatingKeysAndValues;
    public boolean entriesUsed;
    public int size;

    public C28241Mh() {
        this(4);
    }

    public C28241Mh(int i) {
        this.alternatingKeysAndValues = new Object[i << 1];
        this.size = 0;
        this.entriesUsed = false;
    }

    public AbstractC17190qP build() {
        return buildOrThrow();
    }

    public AbstractC17190qP buildOrThrow() {
        this.entriesUsed = true;
        return C28261Mj.create(this.size, this.alternatingKeysAndValues);
    }

    private void ensureCapacity(int i) {
        int i2 = i << 1;
        Object[] objArr = this.alternatingKeysAndValues;
        int length = objArr.length;
        if (i2 > length) {
            this.alternatingKeysAndValues = Arrays.copyOf(objArr, AbstractC17980ri.expandedCapacity(length, i2));
            this.entriesUsed = false;
        }
    }

    public C28241Mh put(Object obj, Object obj2) {
        ensureCapacity(this.size + 1);
        C28251Mi.checkEntryNotNull(obj, obj2);
        Object[] objArr = this.alternatingKeysAndValues;
        int i = this.size;
        int i2 = i << 1;
        objArr[i2] = obj;
        objArr[i2 + 1] = obj2;
        this.size = i + 1;
        return this;
    }

    public C28241Mh put(Map.Entry entry) {
        put(entry.getKey(), entry.getValue());
        return this;
    }

    public C28241Mh putAll(Iterable iterable) {
        if (iterable instanceof Collection) {
            ensureCapacity(this.size + ((Collection) iterable).size());
        }
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            put((Map.Entry) it.next());
        }
        return this;
    }
}
