package X;

import android.os.SystemClock;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.fieldstats.privatestats.PrivateStatsWorker;
import com.whatsapp.util.Log;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.whispersystems.curve25519.NativeVOPRFExtension;

/* renamed from: X.0oP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16080oP {
    public NativeVOPRFExtension A00;
    public final C15450nH A01;
    public final C16240og A02;
    public final C14830m7 A03;
    public final C16100oS A04;
    public final C16090oQ A05;
    public final C21720xs A06;
    public final AbstractC14440lR A07;
    public final C21710xr A08;
    public volatile int A09 = 0;
    public volatile long A0A;
    public volatile long A0B;
    public volatile String A0C;
    public volatile boolean A0D;
    public volatile boolean A0E = false;
    public volatile byte[] A0F;

    public C16080oP(C15450nH r2, C16240og r3, C14830m7 r4, C16100oS r5, C16090oQ r6, C21720xs r7, AbstractC14440lR r8, C21710xr r9) {
        this.A03 = r4;
        this.A07 = r8;
        this.A0D = false;
        this.A01 = r2;
        this.A08 = r9;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
        r7.A00 = this;
    }

    public int A00() {
        C16090oQ r4 = this.A05;
        int i = r4.A00().getInt("first_token_stage", 0);
        if (i == 1) {
            synchronized (this) {
                if (this.A0D) {
                    return 15;
                }
                return 2;
            }
        } else if (i != 0) {
            return r4.A00().getInt("token_not_ready_reason", 0);
        } else {
            return 2;
        }
    }

    public synchronized void A01() {
        if (!A07() && !this.A0D) {
            C16090oQ r9 = this.A05;
            int i = r9.A00().getInt("redeem_count", -1);
            long A00 = (this.A03.A00() / 1000) - r9.A00().getLong("base_timestamp", 0);
            if (i < 0 || i >= 512 || A00 >= r9.A00().getLong("time_to_live", 0)) {
                this.A0D = true;
                A05(false);
            } else {
                C15450nH r3 = this.A01;
                int A02 = r3.A02(AbstractC15460nI.A0C);
                long A022 = (long) r3.A02(AbstractC15460nI.A0B);
                if (i >= A02 || A00 > r9.A00().getLong("time_to_live", 0) - A022) {
                    this.A0D = true;
                    A05(true);
                }
            }
        }
    }

    public final synchronized void A02() {
        this.A0C = null;
        this.A0F = null;
        this.A09 = 0;
        this.A0E = false;
        this.A0D = false;
        C16090oQ r3 = this.A05;
        r3.A05("original_token", null);
        r3.A05("next_original_token", null);
        r3.A04("base_timestamp", 0);
        r3.A04("time_to_live", 0);
        r3.A05("blinding_factor", null);
        r3.A02(-1);
        r3.A05("shared_secret", null);
        if (r3.A00().getInt("first_token_stage", 0) == 1) {
            r3.A01(0);
        }
    }

    public final void A03(int i) {
        if (this.A09 < 2) {
            this.A09++;
            this.A07.AbK(new RunnableBRunnable0Shape6S0100000_I0_6(this, 31), "PrivateStatstoken/retry", ((long) this.A09) * C26061Bw.A0L * ((long) this.A09));
            return;
        }
        C16090oQ r2 = this.A05;
        int i2 = 10;
        if (i == 5) {
            i2 = 9;
        }
        r2.A03(i2);
        A06(false, i);
    }

    public final synchronized void A04(int i) {
        if (!this.A0D) {
            this.A0D = true;
            StringBuilder sb = new StringBuilder();
            sb.append("PrivateStatsToken/doCreateFirstToken!!--->about to create 1st token: ");
            sb.append(i);
            Log.i(sb.toString());
            A05(false);
        }
    }

    public void A05(boolean z) {
        NativeVOPRFExtension nativeVOPRFExtension = this.A00;
        if (nativeVOPRFExtension == null) {
            nativeVOPRFExtension = new NativeVOPRFExtension();
            this.A00 = nativeVOPRFExtension;
        }
        byte[] bArr = new byte[32];
        AnonymousClass23U r3 = nativeVOPRFExtension.A00;
        r3.A00(bArr);
        byte[] bArr2 = null;
        int i = 0;
        while (i < 256) {
            bArr2 = new byte[32];
            r3.A00(bArr2);
            bArr2[31] = (byte) (bArr2[31] & 31);
            if (nativeVOPRFExtension.A00(bArr2)) {
                break;
            }
            i++;
        }
        if (i >= 256) {
            Log.w("PrivateStatsToken/generateCredentialToken cannot generate valid blindingFactor");
            this.A05.A03(5);
        } else {
            SystemClock.elapsedRealtime();
            byte[] A02 = nativeVOPRFExtension.A02(bArr, bArr2, 32);
            SystemClock.elapsedRealtime();
            if (A02 == null) {
                Log.e("PrivateStatsToken/generateCredentialToken failed to blind the token");
                this.A05.A03(7);
            } else {
                synchronized (this) {
                    this.A0E = z;
                    this.A0F = A02;
                    if (z) {
                        C16090oQ r2 = this.A05;
                        r2.A05("next_original_token", Base64.encodeToString(bArr, 10));
                        r2.A05("blinding_factor", Base64.encodeToString(bArr2, 10));
                    } else {
                        C16090oQ r5 = this.A05;
                        r5.A05("original_token", Base64.encodeToString(bArr, 10));
                        r5.A05("blinding_factor", Base64.encodeToString(bArr2, 10));
                        r5.A05("shared_secret", null);
                        r5.A02(-1);
                        r5.A04("base_timestamp", 0);
                        r5.A04("time_to_live", 0);
                    }
                    this.A09 = 0;
                    this.A0A = SystemClock.elapsedRealtime();
                    this.A0B = this.A0A;
                    if (this.A02.A04 == 2) {
                        this.A0C = this.A06.A00(this.A0F);
                    } else {
                        A03(5);
                    }
                }
                return;
            }
        }
        A06(true, 4);
    }

    public final synchronized void A06(boolean z, int i) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.A04.A01(i, elapsedRealtime - this.A0A, elapsedRealtime - this.A0B, (long) this.A09);
        this.A0D = false;
        C16090oQ r2 = this.A05;
        r2.A05("blinding_factor", null);
        if (this.A0E) {
            r2.A05("next_original_token", null);
        } else {
            r2.A05("original_token", null);
        }
        this.A0E = false;
        this.A0F = null;
        if (!z) {
            this.A0C = null;
            this.A09 = 0;
        }
        if (r2.A00().getInt("first_token_stage", 0) == 1) {
            r2.A01(0);
        }
    }

    public final boolean A07() {
        C16090oQ r7 = this.A05;
        int i = r7.A00().getInt("first_token_stage", 0);
        if (i != 2) {
            int A02 = this.A01.A02(AbstractC15460nI.A09);
            if (A02 <= 30) {
                r7.A01(2);
            } else {
                long j = r7.A00().getLong("first_token_delay_time", 0);
                if (i == 1) {
                    if (!this.A0D) {
                        C14830m7 r5 = this.A03;
                        if (r5.A00() / 1000 > j + r7.A00().getLong("first_token_request_timestamp", 0) + 300) {
                            ((AnonymousClass022) this.A08.get()).A08("name.com.whatsapp.privatestats.firsttoken");
                            Log.i("PrivateStatsToken/delayForFirstTokenIfNeeded cancelled the work");
                            A04(2);
                        } else {
                            r5.A00();
                            return true;
                        }
                    }
                    return true;
                } else if (i == 0) {
                    long nextInt = ((long) new Random().nextInt(A02 - 30)) + 30;
                    StringBuilder sb = new StringBuilder("PrivateStatsToken/delayForFirstToken-->delay timesec= ");
                    sb.append(nextInt);
                    Log.i(sb.toString());
                    r7.A04("first_token_delay_time", nextInt);
                    r7.A04("first_token_request_timestamp", this.A03.A00() / 1000);
                    r7.A01(1);
                    C004201x r1 = new C004201x(PrivateStatsWorker.class);
                    r1.A02(nextInt, TimeUnit.SECONDS);
                    ((AnonymousClass022) this.A08.get()).A05(AnonymousClass023.REPLACE, (AnonymousClass021) r1.A00(), "name.com.whatsapp.privatestats.firsttoken");
                    return true;
                }
            }
        }
        return false;
    }
}
