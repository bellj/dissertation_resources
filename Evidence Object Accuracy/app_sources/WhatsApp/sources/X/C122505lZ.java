package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5lZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122505lZ extends AbstractC118835cS {
    public View A00;

    public C122505lZ(View view) {
        super(view);
        this.A00 = view.findViewById(R.id.payment_retry_button);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        this.A00.setOnClickListener(((C123025mU) r3).A00);
    }
}
