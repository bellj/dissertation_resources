package X;

import android.util.Log;
import android.view.View;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

/* renamed from: X.03U  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass03U {
    public static final List A0I = Collections.emptyList();
    public int A00;
    public int A01 = 0;
    public int A02 = -1;
    public int A03 = -1;
    public int A04 = -1;
    public int A05 = -1;
    public int A06 = -1;
    public int A07 = 0;
    public long A08 = -1;
    public AnonymousClass0QS A09 = null;
    public AnonymousClass03U A0A = null;
    public AnonymousClass03U A0B = null;
    public RecyclerView A0C;
    public WeakReference A0D;
    public List A0E = null;
    public List A0F = null;
    public boolean A0G = false;
    public final View A0H;

    public AnonymousClass03U(View view) {
        if (view != null) {
            this.A0H = view;
            return;
        }
        throw new IllegalArgumentException("itemView may not be null");
    }

    public final int A00() {
        RecyclerView recyclerView = this.A0C;
        if (recyclerView == null) {
            return -1;
        }
        return recyclerView.A09(this);
    }

    public List A01() {
        List list;
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) != 0 || (list = this.A0E) == null || list.size() == 0) {
            return A0I;
        }
        return this.A0F;
    }

    public void A02() {
        this.A00 = 0;
        this.A05 = -1;
        this.A03 = -1;
        this.A08 = -1;
        this.A06 = -1;
        this.A01 = 0;
        this.A0A = null;
        this.A0B = null;
        List list = this.A0E;
        if (list != null) {
            list.clear();
        }
        this.A00 &= -1025;
        this.A07 = 0;
        this.A04 = -1;
        RecyclerView.A05(this);
    }

    public void A03(int i, int i2) {
        this.A00 = (i & i2) | (this.A00 & (i2 ^ -1));
    }

    public void A04(int i, boolean z) {
        if (this.A03 == -1) {
            this.A03 = this.A05;
        }
        int i2 = this.A06;
        if (i2 == -1) {
            i2 = this.A05;
            this.A06 = i2;
        }
        if (z) {
            this.A06 = i2 + i;
        }
        this.A05 += i;
        View view = this.A0H;
        if (view.getLayoutParams() != null) {
            ((AnonymousClass0B6) view.getLayoutParams()).A01 = true;
        }
    }

    public final void A05(boolean z) {
        int i;
        int i2;
        int i3 = this.A01;
        if (z) {
            i = i3 - 1;
        } else {
            i = i3 + 1;
        }
        this.A01 = i;
        if (i < 0) {
            this.A01 = 0;
            StringBuilder sb = new StringBuilder("isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for ");
            sb.append(this);
            Log.e("View", sb.toString());
            return;
        }
        if (!z) {
            if (i == 1) {
                i2 = this.A00 | 16;
            } else {
                return;
            }
        } else if (i == 0) {
            i2 = this.A00 & -17;
        } else {
            return;
        }
        this.A00 = i2;
    }

    public boolean A06() {
        return (this.A00 & 128) != 0;
    }

    public boolean A07() {
        return (this.A00 & 1) != 0;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("ViewHolder{");
        sb.append(Integer.toHexString(hashCode()));
        sb.append(" position=");
        sb.append(this.A05);
        sb.append(" id=");
        sb.append(this.A08);
        sb.append(", oldPos=");
        sb.append(this.A03);
        sb.append(", pLpos:");
        sb.append(this.A06);
        StringBuilder sb2 = new StringBuilder(sb.toString());
        if (this.A09 != null) {
            sb2.append(" scrap ");
            if (this.A0G) {
                str = "[changeScrap]";
            } else {
                str = "[attachedScrap]";
            }
            sb2.append(str);
        }
        int i = this.A00;
        if ((i & 4) != 0) {
            sb2.append(" invalid");
        }
        if (!A07()) {
            sb2.append(" unbound");
        }
        if ((i & 2) != 0) {
            sb2.append(" update");
        }
        if ((this.A00 & 8) != 0) {
            sb2.append(" removed");
        }
        if (A06()) {
            sb2.append(" ignored");
        }
        if ((i & 256) != 0) {
            sb2.append(" tmpDetached");
        }
        if ((i & 16) != 0 || this.A0H.hasTransientState()) {
            StringBuilder sb3 = new StringBuilder(" not recyclable(");
            sb3.append(this.A01);
            sb3.append(")");
            sb2.append(sb3.toString());
        }
        int i2 = this.A00;
        if (!((i2 & 512) == 0 && (i2 & 4) == 0)) {
            sb2.append(" undefined adapter position");
        }
        if (this.A0H.getParent() == null) {
            sb2.append(" no parent");
        }
        sb2.append("}");
        return sb2.toString();
    }
}
