package X;

import android.util.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.5z1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130235z1 {
    static {
        new C129845yO(1280, 720);
        new C129845yO(1920, 1080);
    }

    public static List A00(Size[] sizeArr) {
        int length;
        if (sizeArr == null || (length = sizeArr.length) == 0) {
            return Collections.emptyList();
        }
        ArrayList A0w = C12980iv.A0w(length);
        int i = 0;
        do {
            A0w.add(new C129845yO(sizeArr[i].getWidth(), sizeArr[i].getHeight()));
            i++;
        } while (i < length);
        return Collections.unmodifiableList(A0w);
    }
}
