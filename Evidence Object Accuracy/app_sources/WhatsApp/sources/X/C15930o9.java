package X;

/* renamed from: X.0o9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15930o9 {
    public final int A00;
    public final int A01;
    public final byte[] A02;

    public C15930o9(byte[] bArr, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A02 = bArr;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("EncryptedMessage{ciphertextVersion=");
        sb.append(this.A01);
        sb.append(", ciphertextType=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
