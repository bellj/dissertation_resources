package X;

import android.os.Handler;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Set;

/* renamed from: X.1Gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27231Gn implements AbstractC21730xt {
    public long A00 = 0;
    public final C14830m7 A01;
    public final C17220qS A02;
    public final C22600zL A03;

    public C27231Gn(C14830m7 r3, C17220qS r4, C22600zL r5) {
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        synchronized (this) {
            this.A00 = 0;
        }
        Log.w("routeselector/on delivery failure");
        StringBuilder sb = new StringBuilder("routeselector/onmediaroutingrequesterror/code ");
        sb.append(0);
        Log.w(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r11, String str) {
        synchronized (this) {
            this.A00 = 0;
        }
        for (AnonymousClass1V8 r3 : r11.A0J("error")) {
            if (r3 != null) {
                try {
                    int A05 = r3.A05("code", 0);
                    if (A05 != 0) {
                        Pair pair = new Pair(Integer.valueOf(A05), Integer.valueOf(r3.A05("backoff", 0)));
                        C22600zL r4 = this.A03;
                        int intValue = ((Number) pair.first).intValue();
                        int intValue2 = ((Number) pair.second).intValue();
                        StringBuilder sb = new StringBuilder("routeselector/onmediaroutingrequesterror/code ");
                        sb.append(intValue);
                        Log.w(sb.toString());
                        if (503 == intValue) {
                            AnonymousClass156 r2 = r4.A0B;
                            synchronized (r2) {
                                r2.A00 = 0;
                                Log.i("ChatdMediaThrottleManager/resetThrottle");
                            }
                            Handler handler = r4.A04;
                            C37671mo r42 = r4.A0D;
                            long A01 = r42.A00.A01();
                            long j = 0;
                            if (A01 != 0) {
                                long j2 = A01 * 1000;
                                j = ((3 * j2) / 4) + Math.abs(r42.A01.nextLong() % (j2 / 2));
                                StringBuilder sb2 = new StringBuilder("fibonaccibackoffhandler/sleep/");
                                sb2.append(j);
                                sb2.append(" milliseconds");
                                Log.i(sb2.toString());
                            }
                            handler.sendEmptyMessageDelayed(0, j);
                            return;
                        } else if (507 == intValue && intValue2 > 0) {
                            AnonymousClass156 r6 = r4.A0B;
                            long j3 = (long) intValue2;
                            synchronized (r6) {
                                long A00 = r6.A01.A00() + (Math.min(j3, 10800L) * 1000);
                                r6.A00 = A00;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("ChatdMediaThrottleManager/setThrottle until ");
                                sb3.append(A00);
                                Log.i(sb3.toString());
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        continue;
                    }
                } catch (AnonymousClass1V9 e) {
                    Log.e("MediaConnFactory/getErrorCodeAndBackoffFromIqResponse CorruptStreamException ", e);
                }
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r45, String str) {
        long j;
        synchronized (this) {
            j = this.A00;
            this.A00 = 0;
        }
        C22600zL r2 = this.A03;
        AnonymousClass1V8 A0F = r45.A0F("media_conn");
        String A0I = A0F.A0I("id", null);
        String A0H = A0F.A0H("auth");
        long A09 = A0F.A09(A0F.A0H("ttl"), "ttl");
        long A092 = A0F.A09(A0F.A0H("auth_ttl"), "auth_ttl");
        long A08 = A0F.A08("max_buckets", 0);
        int A05 = A0F.A05("is_new", 1);
        int A052 = A0F.A05("max_auto_download_retry", 3);
        int A053 = A0F.A05("max_manual_retry", 3);
        AnonymousClass1V8[] r6 = A0F.A03;
        ArrayList arrayList = new ArrayList();
        if (r6 != null) {
            for (AnonymousClass1V8 r0 : r6) {
                if ("host".equals(r0.A00)) {
                    String A0H2 = r0.A0H("hostname");
                    String A0I2 = r0.A0I("ip4", null);
                    String A0I3 = r0.A0I("ip6", null);
                    String A0I4 = r0.A0I("class", null);
                    String A0I5 = r0.A0I("fallback_hostname", null);
                    String A0I6 = r0.A0I("fallback_ip4", null);
                    String A0I7 = r0.A0I("fallback_ip6", null);
                    String A0I8 = r0.A0I("fallback_class", null);
                    AnonymousClass1V8 A0E = r0.A0E("upload");
                    Set set = AnonymousClass27v.A00;
                    arrayList.add(new AnonymousClass1n2(A0H2, A0I2, A0I3, A0I4, A0I5, A0I6, A0I7, A0I8, r0.A0I("type", null), AnonymousClass27v.A00(A0E, set), AnonymousClass27v.A00(r0.A0E("download"), set), AnonymousClass27v.A00(r0.A0E("download_buckets"), null), "true".equals(r0.A0I("force_ip", null))));
                }
            }
        }
        boolean z = false;
        if (A05 == 1) {
            z = true;
        }
        C37691mq r02 = new C37691mq(A0H, A0I, arrayList, A052, A053, A09, A092, A08, j, z);
        AnonymousClass156 r3 = r2.A0B;
        synchronized (r3) {
            r3.A00 = 0;
            Log.i("ChatdMediaThrottleManager/resetThrottle");
        }
        r2.A0D(r02);
        if (r2.A09.A07(149)) {
            r2.A0C.A01("route_selector_prefs").edit().putString("media_conn", C468327w.A00(r2.A08, r2.A08())).apply();
        }
    }
}
