package X;

import java.util.concurrent.Executor;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class EnumC03940Jt extends Enum implements Executor {
    public static final /* synthetic */ EnumC03940Jt[] A00;
    public static final EnumC03940Jt A01;

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return "DirectExecutor";
    }

    static {
        EnumC03940Jt r1 = new EnumC03940Jt();
        A01 = r1;
        A00 = new EnumC03940Jt[]{r1};
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        runnable.run();
    }

    public static EnumC03940Jt valueOf(String str) {
        return (EnumC03940Jt) Enum.valueOf(EnumC03940Jt.class, str);
    }

    public static EnumC03940Jt[] values() {
        return (EnumC03940Jt[]) A00.clone();
    }
}
