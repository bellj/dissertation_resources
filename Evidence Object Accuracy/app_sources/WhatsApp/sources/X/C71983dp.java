package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71983dp extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71983dp(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        UserJid userJid = this.this$0.A0P;
        if (userJid != null) {
            return AnonymousClass3AT.A00(AnonymousClass4A0.A02, userJid, "catalog_category_dummy_root_id");
        }
        throw C16700pc.A06("bizJid");
    }
}
