package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.127  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass127 {
    public final C16590pI A00;

    public AnonymousClass127(C16590pI r1) {
        this.A00 = r1;
    }

    public static File[] A00(C16590pI r7) {
        File[] fileArr;
        File file = new File(r7.A00.getCacheDir(), "traces");
        if (!file.exists() || (fileArr = file.listFiles(new FilenameFilter() { // from class: X.1SO
            @Override // java.io.FilenameFilter
            public final boolean accept(File file2, String str) {
                return str.endsWith(".stacktrace");
            }
        })) == null) {
            fileArr = new File[0];
        }
        if (fileArr.length > 0) {
            return fileArr;
        }
        StringBuilder sb = new StringBuilder("traces_");
        sb.append("com.whatsapp");
        sb.append(".txt");
        String[] strArr = {"traces.txt", sb.toString()};
        int i = 0;
        do {
            File file2 = new File("/data/anr/", strArr[i]);
            if (file2.exists()) {
                return new File[]{file2};
            }
            i++;
        } while (i < 2);
        return new File[0];
    }
}
