package X;

/* renamed from: X.0Rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05950Rp {
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;

    public C05950Rp(int i, String str, int i2) {
        this.A03 = str;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = -1;
    }

    public C05950Rp(String str, int i, int i2, int i3) {
        this.A03 = str;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
    }
}
