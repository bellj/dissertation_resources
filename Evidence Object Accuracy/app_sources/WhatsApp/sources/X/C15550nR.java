package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/* renamed from: X.0nR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15550nR {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C15570nT A01;
    public final C237913a A02;
    public final AnonymousClass116 A03;
    public final C29871Vb A04;
    public final C29881Vc A05 = new C29881Vc();
    public final C21580xe A06;
    public final AnonymousClass10S A07;
    public final AnonymousClass10T A08;
    public final AnonymousClass16P A09;
    public final C14830m7 A0A;
    public final C14820m6 A0B;
    public final AnonymousClass018 A0C;
    public final C22300yr A0D;
    public final C22430z4 A0E;

    public C15550nR(C15570nT r3, C237913a r4, AnonymousClass116 r5, C21580xe r6, AnonymousClass10S r7, AnonymousClass10T r8, AnonymousClass16P r9, C14830m7 r10, C14820m6 r11, AnonymousClass018 r12, C22300yr r13, C22430z4 r14) {
        this.A0A = r10;
        this.A02 = r4;
        this.A01 = r3;
        this.A0C = r12;
        this.A07 = r7;
        this.A0D = r13;
        this.A08 = r8;
        this.A03 = r5;
        this.A0B = r11;
        this.A09 = r9;
        this.A06 = r6;
        this.A04 = new C29871Vb(r12);
        this.A0E = r14;
    }

    public static C15370n3 A00(C15550nR r0, AbstractC14640lm r1) {
        AnonymousClass009.A05(r1);
        return r0.A0B(r1);
    }

    public static String A01(String str) {
        String stripSeparators = PhoneNumberUtils.stripSeparators(str.trim());
        return (stripSeparators.length() <= 0 || Character.isDigit(stripSeparators.charAt(0))) ? stripSeparators : stripSeparators.substring(1);
    }

    public static ArrayList A02(Collection collection) {
        ArrayList arrayList = new ArrayList();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C15370n3 r1 = (C15370n3) it.next();
            if (r1 != null && !(r1.A0D instanceof C29891Vd) && !r1.A0K()) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public static void A03(String str, Collection collection) {
        int size = collection.size();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("/count ");
        sb.append(size);
        Log.i(sb.toString());
    }

    public int A04() {
        int i;
        C21580xe r3 = this.A06;
        synchronized (r3.A07) {
            i = -1;
            if (r3.A00 == null) {
                C15570nT r0 = r3.A01;
                r0.A08();
                C27631Ih r4 = r0.A05;
                if (r4 != null) {
                    C28181Ma r6 = new C28181Ma(true);
                    r6.A03();
                    String[] strArr = {r4.getRawString()};
                    C16310on A01 = ((AbstractC21570xd) r3).A00.get();
                    try {
                        Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "is_whatsapp_user = 1 AND raw_contact_id NOT NULL AND raw_contact_id != -1 AND wa_contacts.jid != ?", null, "initIndividualContactCount", C21580xe.A09, strArr);
                        if (A03 == null) {
                            AnonymousClass009.A07("contact-mgr-db/unable to get individual contact count");
                            r3.A00 = 0;
                        } else {
                            if (A03.moveToNext()) {
                                int i2 = A03.getInt(0);
                                r6.A00();
                                r3.A00 = Integer.valueOf(i2);
                            } else {
                                Log.w("contact-mgr-db/individual contact count missing cursor");
                                r3.A00 = null;
                            }
                            A03.close();
                        }
                        A01.close();
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            Integer num = r3.A00;
            if (num != null) {
                i = num.intValue();
            }
        }
        StringBuilder sb = new StringBuilder("indivcount/count ");
        sb.append(i);
        Log.i(sb.toString());
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x002d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri A05(android.content.ContentResolver r7, X.C15370n3 r8) {
        /*
            r6 = this;
            r5 = 0
            if (r8 == 0) goto L_0x004b
            X.116 r0 = r6.A03
            boolean r0 = r0.A00()
            if (r0 == 0) goto L_0x004b
            X.0nT r0 = r6.A01
            r0.A08()
            X.1Pc r0 = r8.A0C
            if (r0 == 0) goto L_0x002b
            long r1 = r0.A00
            r3 = -2
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x002b
            r3 = 0
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x002b
            android.net.Uri r0 = android.provider.ContactsContract.RawContacts.CONTENT_URI
            android.net.Uri r0 = android.content.ContentUris.withAppendedId(r0, r1)
        L_0x0028:
            if (r0 == 0) goto L_0x004b
            goto L_0x002d
        L_0x002b:
            r0 = 0
            goto L_0x0028
        L_0x002d:
            android.net.Uri r0 = android.provider.ContactsContract.RawContacts.getContactLookupUri(r7, r0)     // Catch: SecurityException -> 0x0039, NullPointerException -> 0x0032
            return r0
        L_0x0032:
            r1 = move-exception
            java.lang.String r0 = "contactmanager/NPE"
            com.whatsapp.util.Log.w(r0, r1)
            return r5
        L_0x0039:
            r2 = move-exception
            java.lang.String r1 = "contactmanager/permission problem:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.w(r0)
        L_0x004b:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15550nR.A05(android.content.ContentResolver, X.0n3):android.net.Uri");
    }

    public C15370n3 A06(long j) {
        C29871Vb r3 = this.A04;
        C29861Va r4 = r3.A00;
        if (j != -2) {
            Map map = r3.A01;
            synchronized (map) {
                for (C15370n3 r42 : map.values()) {
                    if (j == r42.A08()) {
                        return r42;
                    }
                }
            }
        } else if (r4 != null) {
            return r4;
        }
        C21580xe r2 = this.A06;
        C28181Ma r1 = new C28181Ma(true);
        r1.A03();
        C16310on A01 = ((AbstractC21570xd) r2).A00.get();
        Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "wa_contacts._id = ?", null, "CONTACT", C21580xe.A08, new String[]{String.valueOf(j)});
        C15370n3 r43 = null;
        try {
            if (A03 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("contact-mgr-db/unable to get contact by id ");
                sb.append(j);
                AnonymousClass009.A07(sb.toString());
                A01.close();
                return null;
            }
            if (A03.moveToNext()) {
                r43 = AnonymousClass1VW.A00(A03);
            }
            A03.getCount();
            A03.close();
            A01.close();
            if (r43 != null) {
                r2.A0N(r43, AnonymousClass018.A00(r2.A05.A00));
            }
            r1.A00();
            return r43;
        } catch (Throwable th) {
            if (A03 != null) {
                try {
                    A03.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }

    public C15370n3 A07(C29901Ve r15, String str, long j) {
        C15370n3 r1 = new C15370n3(r15);
        A0O(r1, null, AnonymousClass1PD.A04, str, 0, j, false, false, false, false, false, false);
        return r1;
    }

    public C15370n3 A08(AbstractC14640lm r3) {
        C15570nT r1 = this.A01;
        if (r1.A0F(r3)) {
            r1.A08();
            return r1.A01;
        }
        boolean A0M = C15380n4.A0M(r3);
        C29871Vb r0 = this.A04;
        if (A0M) {
            return r0.A00;
        }
        return (C15370n3) r0.A01.get(r3);
    }

    public C15370n3 A09(AbstractC14640lm r5) {
        C15370n3 r0;
        C29871Vb r2 = this.A04;
        if (C15380n4.A0M(r5)) {
            r0 = r2.A00;
        } else {
            r0 = (C15370n3) r2.A01.get(r5);
        }
        if (r0 == null) {
            r0 = this.A06.A0A(r5);
            A0N(r0, r5);
            if (!(r0 == null || r0.A0B(AbstractC14640lm.class) == null)) {
                Map map = r2.A01;
                Jid A0B = r0.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(A0B);
                map.put(A0B, r0);
            }
        }
        return r0;
    }

    public C15370n3 A0A(AbstractC14640lm r3) {
        C15570nT r1 = this.A01;
        if (r1.A0F(r3)) {
            r1.A08();
            return r1.A01;
        } else if (C15380n4.A0M(r3)) {
            return this.A04.A00;
        } else {
            return A09(r3);
        }
    }

    public C15370n3 A0B(AbstractC14640lm r3) {
        C15370n3 A0A = A0A(r3);
        if (A0A != null) {
            return A0A;
        }
        C15370n3 r1 = new C15370n3(r3);
        this.A06.A0J(r1);
        return r1;
    }

    public C15370n3 A0C(String str, boolean z) {
        String obj;
        List<C15370n3> list;
        if (str != null) {
            String A01 = A01(str);
            if (!A01.isEmpty()) {
                C21580xe r4 = this.A06;
                C28181Ma r2 = new C28181Ma(true);
                r2.A03();
                int length = A01.length();
                if (length < 5) {
                    obj = A01;
                } else {
                    StringBuilder sb = new StringBuilder("%");
                    String str2 = A01;
                    if (length > 5) {
                        str2 = A01.substring(Math.min(length - 5, 3));
                    }
                    sb.append(str2);
                    obj = sb.toString();
                }
                C16310on A012 = ((AbstractC21570xd) r4).A00.get();
                try {
                    String[] strArr = C21580xe.A08;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(obj);
                    sb2.append("@");
                    sb2.append("s.whatsapp.net");
                    Cursor A03 = AbstractC21570xd.A03(A012, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "wa_contacts.jid LIKE ?", null, "CONTACTS", strArr, new String[]{sb2.toString()});
                    if (A03 == null) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("contact-mgr-db/unable to get contacts by phone number ");
                        sb3.append(A01);
                        Log.e(sb3.toString());
                        List emptyList = Collections.emptyList();
                        A012.close();
                        list = emptyList;
                    } else {
                        ArrayList arrayList = new ArrayList(A03.getCount());
                        while (A03.moveToNext()) {
                            arrayList.add(AnonymousClass1VW.A00(A03));
                        }
                        A03.close();
                        A012.close();
                        r4.A0Q(arrayList);
                        arrayList.size();
                        r2.A00();
                        list = arrayList;
                    }
                    int i = 0;
                    C15370n3 r42 = null;
                    for (C15370n3 r22 : list) {
                        Jid A0B = r22.A0B(UserJid.class);
                        if (A0B != null && (r22.A0f || !z)) {
                            if (A01.equals(A0B.user)) {
                                return r22;
                            }
                            i++;
                            r42 = r22;
                        }
                    }
                    if (i == 1) {
                        return r42;
                    }
                } catch (Throwable th) {
                    try {
                        A012.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        return null;
    }

    public UserJid A0D(GroupJid groupJid) {
        UserJid userJid;
        String str;
        int indexOf;
        String substring;
        C15370n3 A0A;
        if (groupJid == null || (A0A = A0A(groupJid)) == null || (userJid = A0A.A0E) == null) {
            userJid = null;
            if (!(groupJid == null || (str = groupJid.user) == null || (indexOf = str.indexOf("-")) == -1 || (substring = str.substring(0, indexOf)) == null)) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(substring);
                    sb.append("@");
                    sb.append("s.whatsapp.net");
                    userJid = UserJid.get(sb.toString());
                    return userJid;
                } catch (AnonymousClass1MW unused) {
                    StringBuilder sb2 = new StringBuilder("jids/failed to get group creator jid from group jid: ");
                    sb2.append(groupJid.getRawString());
                    Log.w(sb2.toString());
                    return userJid;
                }
            }
        }
        return userJid;
    }

    public ArrayList A0E() {
        C21580xe r4 = this.A06;
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        ArrayList arrayList = new ArrayList();
        C15570nT r0 = r4.A01;
        r0.A08();
        String A03 = C15380n4.A03(r0.A05);
        String[] strArr = new String[5];
        strArr[0] = "broadcast";
        strArr[1] = "%@broadcast";
        strArr[2] = "%@g.us";
        strArr[3] = "%@temp";
        if (A03 == null) {
            A03 = AnonymousClass1VY.A00.getRawString();
        }
        strArr[4] = A03;
        C16310on A01 = ((AbstractC21570xd) r4).A00.get();
        try {
            Cursor A032 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "is_whatsapp_user = 1 AND wa_contacts.jid != ? AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid NOT LIKE ? AND wa_contacts.jid != ?", null, "CONTACT", C21580xe.A08, strArr);
            if (A032 == null) {
                AnonymousClass009.A07("contact-mgr-db/unable to get all individual chats");
                A01.close();
                return arrayList;
            }
            A032.getCount();
            while (A032.moveToNext()) {
                arrayList.add(AnonymousClass1VW.A00(A032));
            }
            A032.close();
            A01.close();
            r4.A0Q(arrayList);
            arrayList.size();
            r3.A00();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Collection A0F(Set set) {
        long currentTimeMillis = System.currentTimeMillis();
        Collection<C15370n3> A0D = this.A06.A0D(false);
        ArrayList arrayList = new ArrayList();
        for (C15370n3 r1 : A0D) {
            if (r1.A0J() || set.contains(r1.A0D)) {
                arrayList.add(r1);
            }
        }
        StringBuilder sb = new StringBuilder("returned ");
        sb.append(arrayList.size());
        sb.append(" sidelist sync pending contacts | time: ");
        sb.append(System.currentTimeMillis() - currentTimeMillis);
        Log.i(sb.toString());
        return arrayList;
    }

    public List A0G() {
        C21580xe r1 = this.A06;
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        ArrayList arrayList = new ArrayList();
        C16310on A01 = ((AbstractC21570xd) r1).A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", null, null, "CONTACTS", C21580xe.A08, null);
            if (A03 == null) {
                AnonymousClass009.A07("contact-mgr-db/unable to get all db contacts for sync");
                A01.close();
                return arrayList;
            }
            int count = A03.getCount();
            while (A03.moveToNext()) {
                try {
                    C15370n3 A00 = AnonymousClass1VW.A00(A03);
                    if (C21580xe.A08(A00)) {
                        arrayList.add(A00);
                    }
                } catch (IllegalStateException e) {
                    if (e.getMessage() == null || !e.getMessage().contains("Make sure the Cursor is initialized correctly before accessing data from it")) {
                        throw e;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("contactmanagerdb/getAllDBContactsForSync/illegal-state-exception/cursor count=");
                    sb.append(count);
                    sb.append("; partial map size=");
                    sb.append(arrayList.size());
                    AnonymousClass009.A08(sb.toString(), e);
                }
            }
            A03.close();
            A01.close();
            arrayList.size();
            r3.A00();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Map A0H(Set set) {
        Object obj;
        HashMap hashMap = new HashMap(set.size(), 1.0f);
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Jid jid = (Jid) it.next();
            C15570nT r1 = this.A01;
            if (r1.A0F(jid)) {
                r1.A08();
                hashMap.put(jid, r1.A01);
            }
            if (C15380n4.A0M(jid)) {
                hashMap.put(jid, this.A04.A00);
            }
        }
        set.removeAll(hashMap.keySet());
        HashMap hashMap2 = new HashMap(set.size(), 1.0f);
        HashSet hashSet = new HashSet(set.size(), 1.0f);
        Iterator it2 = set.iterator();
        while (it2.hasNext()) {
            Jid jid2 = (Jid) it2.next();
            C29871Vb r12 = this.A04;
            if (C15380n4.A0M(jid2)) {
                obj = r12.A00;
            } else {
                obj = r12.A01.get(jid2);
            }
            if (obj != null) {
                hashMap2.put(jid2, obj);
            } else {
                hashSet.add(jid2);
            }
        }
        C21580xe r8 = this.A06;
        int min = Math.min(975, 975);
        C28181Ma r7 = new C28181Ma(true);
        r7.A03();
        HashMap hashMap3 = new HashMap(hashSet.size(), 1.0f);
        HashSet hashSet2 = new HashSet();
        C29841Uw r2 = new C29841Uw(C15380n4.A0Q(hashSet), min);
        C16310on A01 = ((AbstractC21570xd) r8).A00.get();
        Iterator it3 = r2.iterator();
        while (it3.hasNext()) {
            String[] strArr = (String[]) it3.next();
            int length = strArr.length;
            boolean z = false;
            if (length <= 975) {
                z = true;
            }
            AnonymousClass009.A0B("SQL param length exceeded", z);
            String[] strArr2 = C21580xe.A08;
            StringBuilder sb = new StringBuilder("wa_contacts.jid IN ");
            sb.append(AnonymousClass1Ux.A00(length));
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", sb.toString(), "wa_contacts.jid", "CONTACTS_BULK", strArr2, strArr);
            if (A03 == null) {
                AnonymousClass009.A07("contact-mgr-db/unable to get contacts by jids");
            } else {
                while (A03.moveToNext()) {
                    C15370n3 A00 = AnonymousClass1VW.A00(A03);
                    C15370n3 A06 = C21580xe.A06(A00, (C15370n3) hashMap3.get((AbstractC14640lm) A00.A0D));
                    hashMap3.put((AbstractC14640lm) A06.A0D, A06);
                    if (A00.A0C == null) {
                        hashSet2.add(A00);
                    }
                }
                A03.close();
            }
        }
        Collection values = hashMap3.values();
        if (hashSet2.size() != 0) {
            AnonymousClass1Lx A002 = A01.A00();
            Iterator it4 = hashSet2.iterator();
            while (it4.hasNext()) {
                C15370n3 r13 = (C15370n3) it4.next();
                if (!values.contains(r13)) {
                    r8.A0L(r13);
                }
            }
            A002.A00();
            A002.close();
        }
        r8.A0Q(values);
        A01.close();
        r7.A01();
        for (Map.Entry entry : hashMap3.entrySet()) {
            A0N((C15370n3) entry.getValue(), (AbstractC14640lm) entry.getKey());
            C29871Vb r14 = this.A04;
            C15370n3 r3 = (C15370n3) entry.getValue();
            if (!(r3 == null || r3.A0B(AbstractC14640lm.class) == null)) {
                Map map = r14.A01;
                Jid A0B = r3.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(A0B);
                map.put(A0B, r3);
            }
            hashMap2.put(entry.getKey(), entry.getValue());
        }
        hashMap.putAll(hashMap2);
        return hashMap;
    }

    public void A0I() {
        byte[] bArr = new byte[12];
        new Random().nextBytes(bArr);
        this.A0B.A00.edit().putString("web_contact_checksum", Base64.encodeToString(bArr, 8)).apply();
    }

    public void A0J(C15370n3 r6) {
        C21580xe r2 = this.A06;
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("status_autodownload_disabled", Integer.valueOf(r6.A0j ? 1 : 0));
        r2.A0E(contentValues, r6.A0D);
        StringBuilder sb = new StringBuilder("updated contact status autodownload jid=");
        sb.append(r6.A0D);
        sb.append(' ');
        sb.append(contentValues);
        sb.append(" | time: ");
        sb.append(r4.A00());
        Log.i(sb.toString());
    }

    public void A0K(C15370n3 r6) {
        C21580xe r2 = this.A06;
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("wa_name", r6.A0U);
        r2.A0E(contentValues, r6.A0D);
        StringBuilder sb = new StringBuilder("updated whatsapp name for contact jid=");
        sb.append(r6.A0D);
        sb.append(' ');
        sb.append(contentValues);
        sb.append(" | time: ");
        sb.append(r4.A00());
        Log.i(sb.toString());
        this.A04.A00(r6);
        A0I();
        this.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(this, 21, r6));
    }

    public void A0L(C15370n3 r4) {
        this.A06.A0K(r4);
        this.A04.A00(r4);
        this.A00.post(new RunnableBRunnable0Shape4S0100000_I0_4(this, 27));
    }

    public void A0M(C15370n3 r6) {
        C21580xe r2 = this.A06;
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        ContentValues contentValues = new ContentValues(3);
        contentValues.put("photo_ts", Integer.valueOf(r6.A04));
        contentValues.put("thumb_ts", Integer.valueOf(r6.A05));
        contentValues.put("photo_id_timestamp", Long.valueOf(r6.A0A));
        r2.A0E(contentValues, r6.A0D);
        StringBuilder sb = new StringBuilder("updated photo id for contact jid=");
        sb.append(r6.A0D);
        sb.append(' ');
        sb.append(contentValues);
        sb.append(" | time: ");
        sb.append(r3.A00());
        Log.i(sb.toString());
        this.A04.A00(r6);
    }

    public final void A0N(C15370n3 r12, AbstractC14640lm r13) {
        String str;
        if (r12 != null && (r13 instanceof AnonymousClass1MU)) {
            AnonymousClass1MU r132 = (AnonymousClass1MU) r13;
            C27631Ih A00 = this.A0D.A00(r132);
            C15370n3 A0A = this.A06.A0A(A00);
            if (A00 == null || A0A == null) {
                C22430z4 r1 = this.A0E;
                C16700pc.A0E(r132, 0);
                long A01 = r1.A00.A01(r132);
                AnonymousClass11E r0 = r1.A01;
                Map map = r0.A01;
                Long valueOf = Long.valueOf(A01);
                String str2 = (String) map.get(valueOf);
                if (str2 == null) {
                    C16310on A012 = r0.A00.get();
                    Cursor A09 = A012.A03.A09("SELECT display_name FROM lid_display_name WHERE lid_row_id = ?", new String[]{String.valueOf(A01)});
                    int columnIndex = A09.getColumnIndex("display_name");
                    if (columnIndex >= 0 && A09.moveToFirst()) {
                        str2 = A09.getString(columnIndex);
                        map.put(valueOf, str2);
                    }
                    A09.close();
                    A012.close();
                    if (str2 == null) {
                        if (!AnonymousClass1US.A0C(r12.A0U)) {
                            r12.A0K = r12.A0U;
                            return;
                        } else {
                            r12.A0K = "???";
                            return;
                        }
                    }
                }
                r12.A0K = str2;
                return;
            }
            if (AnonymousClass1US.A0C(r12.A0K)) {
                str = A00.user;
            } else {
                str = A0A.A0K;
            }
            r12.A0K = str;
        }
    }

    public void A0O(C15370n3 r7, UserJid userJid, AnonymousClass1PD r9, String str, int i, long j, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        Log.i("addGroupChatContact");
        r7.A0K = str;
        r7.A0P = Long.toString(j);
        r7.A0Y = z;
        r7.A0i = z2;
        r7.A0W = z3;
        r7.A0g = z4;
        r7.A01 = i;
        r7.A0E = userJid;
        r7.A0a = z5;
        if (r9.A02 != null) {
            r7.A0G = r9;
        }
        r7.A0Z = z6;
        C21580xe r4 = this.A06;
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        Jid jid = r7.A0D;
        if (jid == null) {
            Log.w("contact-mgr-db/unable to add group chat with null jid");
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("jid", jid.getRawString());
        contentValues.put("is_whatsapp_user", Boolean.TRUE);
        contentValues.put("status", r7.A0R);
        contentValues.put("status_timestamp", Long.valueOf(r7.A0B));
        contentValues.put("display_name", r7.A0K);
        contentValues.put("phone_label", r7.A0P);
        contentValues.put("history_sync_initial_phash", r7.A0N);
        try {
            C16310on A02 = ((AbstractC21570xd) r4).A00.A02();
            r7.A0F(AbstractC21570xd.A00(contentValues, A02, "wa_contacts"));
            r4.A0M(r7, (C15580nU) r7.A0B(C15580nU.class));
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("contact-mgr-db/unable to add group chat ");
            sb.append(r7);
            AnonymousClass009.A08(sb.toString(), e);
        }
        r3.A00();
    }

    public void A0P(GroupJid groupJid, boolean z) {
        C15370n3 A0B = A0B(groupJid);
        if (A0B.A0a != z) {
            A0B.A0a = z;
            this.A06.A0K(A0B);
            this.A04.A00(A0B);
        }
    }

    public void A0Q(C15580nU r3, int i) {
        C15370n3 A0B = A0B(r3);
        if (A0B.A01 != i) {
            A0B.A01 = i;
            this.A06.A0K(A0B);
            this.A04.A00(A0B);
        }
    }

    public void A0R(UserJid userJid, int i, long j) {
        C21580xe r5 = this.A06;
        long j2 = (long) i;
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("disappearing_mode_duration", Long.valueOf(j2));
        contentValues.put("disappearing_mode_timestamp", Long.valueOf(j));
        try {
            C16310on A02 = ((AbstractC21570xd) r5).A00.A02();
            String A03 = C15380n4.A03(userJid);
            AnonymousClass009.A05(A03);
            AbstractC21570xd.A01(contentValues, A02, "wa_contacts", "jid = ?", new String[]{A03});
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("contact-mgr-db/unable to update disappearing_mode_duration state  ");
            sb.append(userJid);
            sb.append(", ");
            sb.append(j2);
            AnonymousClass009.A08(sb.toString(), e);
        }
        this.A04.A01.remove(userJid);
        A0I();
    }

    public void A0S(UserJid userJid, String str, long j) {
        this.A06.A0O(userJid, str, j);
        this.A04.A01.remove(userJid);
        this.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(this, 23, userJid));
    }

    public void A0T(ArrayList arrayList) {
        this.A06.A0S(arrayList, 1, false, false);
    }

    public void A0U(Collection collection) {
        C21580xe r5 = this.A06;
        if (collection.isEmpty()) {
            Log.i("contact-mgr-db/add contacts called without any contacts");
            return;
        }
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        ContentValues contentValues = new ContentValues();
        try {
            C16310on A02 = ((AbstractC21570xd) r5).A00.A02();
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                C15370n3 r8 = (C15370n3) it.next();
                if (r8.A0D != null) {
                    AnonymousClass009.A05(r8.A0C);
                    Iterator it2 = r5.A0C((AbstractC14640lm) r8.A0B(AbstractC14640lm.class)).iterator();
                    while (it2.hasNext()) {
                        C15370n3 r7 = (C15370n3) it2.next();
                        if (C21580xe.A08(r7)) {
                            C28811Pc r0 = r7.A0C;
                            AnonymousClass009.A05(r0);
                            if (r0.A01.equals(r8.A0C.A01)) {
                            }
                        }
                        AnonymousClass009.A0F(A00.A01());
                        C28181Ma r02 = new C28181Ma(true);
                        r02.A03();
                        r5.A0F(A02, A00, r7);
                        r02.A00();
                    }
                }
            }
            Iterator it3 = collection.iterator();
            while (it3.hasNext()) {
                C15370n3 r72 = (C15370n3) it3.next();
                Jid jid = r72.A0D;
                if (jid == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("contact-mgr-db/skipped adding contact due to empty jid: ");
                    sb.append(r72);
                    Log.i(sb.toString());
                } else {
                    contentValues.put("jid", jid.getRawString());
                    contentValues.put("is_whatsapp_user", Boolean.valueOf(r72.A0f));
                    contentValues.put("status", r72.A0R);
                    contentValues.put("status_timestamp", Long.valueOf(r72.A0B));
                    contentValues.put("number", r72.A0C.A01);
                    contentValues.put("raw_contact_id", Long.valueOf(r72.A0C.A00));
                    contentValues.put("display_name", r72.A0K);
                    contentValues.put("phone_type", r72.A0H);
                    contentValues.put("phone_label", r72.A0P);
                    contentValues.put("given_name", r72.A0M);
                    contentValues.put("family_name", r72.A0L);
                    contentValues.put("sort_name", r72.A0Q);
                    contentValues.put("nickname", r72.A0O);
                    contentValues.put("company", r72.A0J);
                    contentValues.put("title", r72.A0S);
                    contentValues.put("is_spam_reported", Boolean.valueOf(r72.A0c));
                    AbstractC21570xd.A00(contentValues, A02, "wa_contacts");
                    if (r72.A0D instanceof C15580nU) {
                        r5.A0G(A02, A00, (C15580nU) r72.A0B(C15580nU.class), r72.A0G);
                    }
                }
            }
            A00.A00();
            A02.A03(new RunnableBRunnable0Shape2S0200000_I0_2(r5, 28, collection));
            A00.close();
            A02.close();
            collection.size();
            r4.A00();
        } catch (IllegalArgumentException e) {
            StringBuilder sb2 = new StringBuilder("contact-mgr-db/unable to add ");
            sb2.append(collection.size());
            sb2.append(" contacts ");
            AnonymousClass009.A08(sb2.toString(), e);
        }
    }

    public void A0V(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C15370n3 r4 = (C15370n3) it.next();
            C21580xe r3 = this.A06;
            Jid A0B = r4.A0B(UserJid.class);
            boolean z = r4.A0f;
            ContentValues contentValues = new ContentValues(1);
            contentValues.put("is_whatsapp_user", Boolean.valueOf(z));
            try {
                C16310on A02 = ((AbstractC21570xd) r3).A00.A02();
                String A03 = C15380n4.A03(A0B);
                AnonymousClass009.A05(A03);
                AbstractC21570xd.A01(contentValues, A02, "wa_contacts", "jid = ?", new String[]{A03});
                A02.close();
            } catch (IllegalArgumentException e) {
                StringBuilder sb = new StringBuilder("contact-mgr-db/unable to update is_whatsapp_user state  ");
                sb.append(A0B);
                sb.append(", ");
                sb.append(z);
                AnonymousClass009.A08(sb.toString(), e);
            }
            this.A04.A00(r4);
            this.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(this, 22, r4));
        }
    }

    public void A0W(Collection collection) {
        C15370n3 r2;
        C16310on A02;
        AnonymousClass1Lx A00;
        C21580xe r1 = this.A06;
        if (!collection.isEmpty()) {
            C28181Ma r5 = new C28181Ma(true);
            r5.A03();
            ContentValues contentValues = new ContentValues(1);
            try {
                A02 = ((AbstractC21570xd) r1).A00.A02();
                A00 = A02.A00();
            } catch (IllegalArgumentException e) {
                AnonymousClass009.A08("contact-mgr-db/unable to update keep timestamp ", e);
            }
            try {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    C15370n3 r9 = (C15370n3) it.next();
                    Jid jid = r9.A0D;
                    if (jid == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("contact-mgr-db/update contact skipped for jid=");
                        sb.append(jid);
                        Log.i(sb.toString());
                    } else {
                        contentValues.put("keep_timestamp", Long.valueOf(r9.A09));
                        AbstractC21570xd.A01(contentValues, A02, "wa_contacts", "_id = ?", new String[]{String.valueOf(r9.A08())});
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
                collection.size();
                r5.A00();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        C29871Vb r52 = this.A04;
        Iterator it2 = collection.iterator();
        while (it2.hasNext()) {
            C15370n3 r3 = (C15370n3) it2.next();
            Jid A0B = r3.A0B(AbstractC14640lm.class);
            if (!(A0B == null || (r2 = (C15370n3) r52.A01.get(A0B)) == null)) {
                r2.A09 = r3.A09;
            }
        }
    }

    public void A0X(Collection collection) {
        String str;
        C21580xe r9 = this.A06;
        C28181Ma r8 = new C28181Ma(true);
        r8.A03();
        ArrayList arrayList = new ArrayList();
        ContentValues contentValues = new ContentValues();
        try {
            C16310on A02 = ((AbstractC21570xd) r9).A00.A02();
            AnonymousClass1Lx A01 = A02.A01();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                C15370n3 r10 = (C15370n3) it.next();
                Jid jid = r10.A0D;
                if (jid == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("contact-mgr-db/update or add contact skipped for jid=");
                    sb.append(jid);
                    Log.i(sb.toString());
                } else {
                    String rawString = jid.getRawString();
                    arrayList.add(r10);
                    contentValues.clear();
                    long A08 = r10.A08();
                    if (A08 > 0) {
                        contentValues.put("_id", Long.valueOf(A08));
                    }
                    contentValues.put("jid", rawString);
                    contentValues.put("is_whatsapp_user", Boolean.valueOf(r10.A0f));
                    contentValues.put("status", r10.A0R);
                    contentValues.put("status_timestamp", Long.valueOf(r10.A0B));
                    C28811Pc r0 = r10.A0C;
                    Long l = null;
                    if (r0 != null) {
                        str = r0.A01;
                    } else {
                        str = null;
                    }
                    contentValues.put("number", str);
                    C28811Pc r02 = r10.A0C;
                    if (r02 != null) {
                        l = Long.valueOf(r02.A00);
                    }
                    contentValues.put("raw_contact_id", l);
                    contentValues.put("display_name", r10.A0K);
                    contentValues.put("phone_type", r10.A0H);
                    contentValues.put("phone_label", r10.A0P);
                    contentValues.put("given_name", r10.A0M);
                    contentValues.put("family_name", r10.A0L);
                    contentValues.put("sort_name", r10.A0Q);
                    contentValues.put("photo_ts", Integer.valueOf(r10.A04));
                    contentValues.put("thumb_ts", Integer.valueOf(r10.A05));
                    contentValues.put("photo_id_timestamp", Long.valueOf(r10.A0A));
                    contentValues.put("history_sync_initial_phash", r10.A0N);
                    contentValues.put("wa_name", r10.A0U);
                    contentValues.put("nickname", r10.A0O);
                    contentValues.put("company", r10.A0J);
                    contentValues.put("title", r10.A0S);
                    contentValues.put("is_spam_reported", Boolean.valueOf(r10.A0c));
                    AbstractC21570xd.A04(contentValues, A02, "wa_contacts");
                    if (jid instanceof C15580nU) {
                        r9.A0G(A02, A01, (C15580nU) jid, r10.A0G);
                    }
                }
            }
            A01.A00();
            A01.close();
            A02.close();
            r9.A02.A05(arrayList);
            collection.size();
            r8.A00();
        } catch (IllegalArgumentException e) {
            AnonymousClass009.A08("contact-mgr-db/unable to update or add contacts ", e);
        }
        Iterator it2 = collection.iterator();
        while (it2.hasNext()) {
            this.A04.A00((C15370n3) it2.next());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0097, code lost:
        if (r1 != null) goto L_0x0099;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Y(java.util.List r17) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15550nR.A0Y(java.util.List):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:121:0x01ca A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x001c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Z(java.util.Map r30) {
        /*
        // Method dump skipped, instructions count: 577
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15550nR.A0Z(java.util.Map):void");
    }

    public boolean A0a(UserJid userJid) {
        C28811Pc r0;
        C15370n3 A0A = A0A(userJid);
        return (A0A == null || (r0 = A0A.A0C) == null || TextUtils.isEmpty(r0.A01)) ? false : true;
    }
}
