package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.shareinvitelink.ShareInviteLinkActivity;
import com.whatsapp.util.Log;
import java.util.Map;

@Deprecated
/* renamed from: X.1vx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC42831vx extends Handler {
    public final C14900mE A00;
    public final C15450nH A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final C15650ng A06;
    public final C15600nX A07;
    public final C42821vw A08;
    public final AnonymousClass11A A09;

    public /* synthetic */ HandlerC42831vx(C14900mE r2, C15450nH r3, C15550nR r4, C15610nY r5, C16590pI r6, AnonymousClass018 r7, C15650ng r8, C15600nX r9, C42821vw r10, AnonymousClass11A r11) {
        super(Looper.getMainLooper());
        this.A04 = r6;
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = r7;
        this.A06 = r8;
        this.A09 = r11;
        this.A07 = r9;
        this.A08 = r10;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AnonymousClass11A r1;
        AbstractC14640lm r0;
        AbstractC15340mz r2;
        C14900mE r12;
        int i;
        C14900mE r13;
        int i2;
        C14900mE r8;
        String str;
        Context context;
        int i3;
        String str2;
        StringBuilder sb;
        Context context2;
        int i4;
        Context context3;
        int i5;
        int i6;
        int i7 = message.what;
        if (i7 == 3) {
            Log.i("groupmgr/conversations/leave group");
            r2 = (AbstractC15340mz) message.obj;
        } else if (i7 == 5) {
            r0 = (AbstractC14640lm) message.obj;
            r1 = this.A09;
            r1.A00(r0);
        } else if (i7 != 6) {
            switch (i7) {
                case 1001:
                case 1004:
                    C15370n3 A09 = this.A02.A09((AbstractC14640lm) message.obj);
                    if (A09 != null) {
                        this.A00.A0E(this.A04.A00.getString(R.string.failed_to_leave_x_group, A09.A0K), 0);
                        return;
                    }
                    return;
                case 1002:
                    this.A09.A00((AbstractC14640lm) message.obj);
                    r12 = this.A00;
                    i = R.string.group_error_add_participants;
                    break;
                case 1003:
                    this.A09.A00((AbstractC14640lm) message.obj);
                    r12 = this.A00;
                    i = R.string.group_error_remove_participants;
                    break;
                case 1005:
                case 1006:
                    this.A09.A00((AbstractC14640lm) message.obj);
                    r12 = this.A00;
                    i = R.string.group_error_change_admins;
                    break;
                case 1007:
                    this.A09.A00((AbstractC14640lm) message.obj);
                    r12 = this.A00;
                    i = R.string.group_error_subject;
                    break;
                case 1008:
                    this.A09.A00((AbstractC14640lm) message.obj);
                    r12 = this.A00;
                    i = R.string.group_error_change_membership_approval_mode;
                    break;
                default:
                    switch (i7) {
                        case 2001:
                            r8 = this.A00;
                            context = this.A04.A00;
                            i3 = R.string.group_error_create;
                            str = context.getString(i3, message.obj);
                            r8.A0L(str, 0);
                            return;
                        case 2002:
                            r8 = this.A00;
                            context = this.A04.A00;
                            i3 = R.string.group_error_create_too_many_groups;
                            str = context.getString(i3, message.obj);
                            r8.A0L(str, 0);
                            return;
                        case 2003:
                            r8 = this.A00;
                            context = this.A04.A00;
                            i3 = R.string.group_error_create_subject_too_long;
                            str = context.getString(i3, message.obj);
                            r8.A0L(str, 0);
                            return;
                        case 2004:
                            r8 = this.A00;
                            context = this.A04.A00;
                            i3 = R.string.group_error_create_too_many_requests;
                            str = context.getString(i3, message.obj);
                            r8.A0L(str, 0);
                            return;
                        case 2005:
                            r13 = this.A00;
                            i2 = R.string.group_error_subject;
                            r13.A05(i2, 0);
                            return;
                        case 2006:
                            r13 = this.A00;
                            i2 = R.string.group_error_subject_not_authorized;
                            r13.A05(i2, 0);
                            return;
                        case 2007:
                            r13 = this.A00;
                            i2 = R.string.group_error_subject_not_in_group;
                            r13.A05(i2, 0);
                            return;
                        case 2008:
                            r13 = this.A00;
                            i2 = R.string.group_error_subject_no_such_group;
                            r13.A05(i2, 0);
                            return;
                        case 2009:
                            r8 = this.A00;
                            context = this.A04.A00;
                            i3 = R.string.group_error_subject_too_long;
                            str = context.getString(i3, message.obj);
                            r8.A0L(str, 0);
                            return;
                        case 2010:
                            r13 = this.A00;
                            i2 = R.string.group_error_add_participants;
                            r13.A05(i2, 0);
                            return;
                        case 2011:
                            r13 = this.A00;
                            i2 = R.string.group_error_add_participants_not_authorized;
                            r13.A05(i2, 0);
                            return;
                        case 2012:
                            r13 = this.A00;
                            i2 = R.string.group_error_add_participants_not_in_group;
                            r13.A05(i2, 0);
                            return;
                        case 2013:
                            r13 = this.A00;
                            i2 = R.string.group_error_add_participants_no_such_group;
                            r13.A05(i2, 0);
                            return;
                        case 2014:
                            r13 = this.A00;
                            i2 = R.string.group_error_remove_participants;
                            r13.A05(i2, 0);
                            return;
                        case 2015:
                            r13 = this.A00;
                            i2 = R.string.group_error_remove_participants_not_authorized;
                            r13.A05(i2, 0);
                            return;
                        case 2016:
                            r13 = this.A00;
                            i2 = R.string.group_error_remove_participants_not_in_group;
                            r13.A05(i2, 0);
                            return;
                        case 2017:
                            r13 = this.A00;
                            i2 = R.string.group_error_remove_participants_no_such_group;
                            r13.A05(i2, 0);
                            return;
                        case 2018:
                            r13 = this.A00;
                            i2 = R.string.group_error_change_admins;
                            r13.A05(i2, 0);
                            return;
                        case 2019:
                            r13 = this.A00;
                            i2 = R.string.group_error_change_admins_not_authorized;
                            r13.A05(i2, 0);
                            return;
                        case 2020:
                            r13 = this.A00;
                            i2 = R.string.group_error_change_admins_not_in_group;
                            r13.A05(i2, 0);
                            return;
                        case 2021:
                            r13 = this.A00;
                            i2 = R.string.group_error_change_admins_no_such_group;
                            r13.A05(i2, 0);
                            return;
                        case 2022:
                            r13 = this.A00;
                            i2 = R.string.group_error_leave;
                            r13.A05(i2, 0);
                            return;
                        case 2023:
                            r13 = this.A00;
                            i2 = R.string.group_error_leave_not_in_group;
                            r13.A05(i2, 0);
                            return;
                        case 2024:
                            r13 = this.A00;
                            i2 = R.string.group_error_leave_no_such_group;
                            r13.A05(i2, 0);
                            return;
                        case 2025:
                            r13 = this.A00;
                            i2 = R.string.group_error_end;
                            r13.A05(i2, 0);
                            return;
                        case 2026:
                            r13 = this.A00;
                            i2 = R.string.group_error_change_membership_approval_mode;
                            r13.A05(i2, 0);
                            return;
                        default:
                            switch (i7) {
                                case 3001:
                                    sb = new StringBuilder();
                                    String str3 = null;
                                    int i8 = 0;
                                    int i9 = 0;
                                    for (Map.Entry entry : ((Map) message.obj).entrySet()) {
                                        AbstractC14640lm r9 = (AbstractC14640lm) entry.getKey();
                                        StringBuilder sb2 = new StringBuilder("groupmgr/add-participant/error/");
                                        sb2.append(r9);
                                        sb2.append("/");
                                        sb2.append(entry.getValue());
                                        Log.e(sb2.toString());
                                        int intValue = ((Number) entry.getValue()).intValue();
                                        C15370n3 A0B = this.A02.A0B(r9);
                                        if (intValue == 401) {
                                            i9++;
                                            context2 = this.A04.A00;
                                            i4 = R.string.error_adding_participant_401;
                                        } else if (intValue != 403) {
                                            if (intValue == 406) {
                                                i9++;
                                                context2 = this.A04.A00;
                                                i4 = R.string.error_adding_participant_406;
                                            } else if (intValue == 500) {
                                                i9++;
                                                context2 = this.A04.A00;
                                                i4 = R.string.error_adding_participant_500;
                                            } else if (intValue == 408) {
                                                i8++;
                                                if (str3 == null) {
                                                    str3 = this.A03.A04(A0B);
                                                }
                                            } else if (intValue != 409) {
                                                i9++;
                                                context2 = this.A04.A00;
                                                i4 = R.string.error_adding_participant;
                                            }
                                        }
                                        sb.append(context2.getString(i4, this.A03.A04(A0B)));
                                        sb.append("\n");
                                    }
                                    if (i8 > 0) {
                                        if (i8 == 1) {
                                            str = this.A04.A00.getString(R.string.error_adding_participant_408_single, str3);
                                        } else {
                                            str = this.A05.A0I(new Object[]{Integer.valueOf(i8)}, R.plurals.error_adding_participant_408_multi, (long) i8);
                                        }
                                        r8 = this.A00;
                                        r8.A0L(str, 0);
                                        return;
                                    }
                                    if (i9 <= 0) {
                                        return;
                                    }
                                    this.A00.A0L(sb.toString(), 0);
                                    return;
                                case 3002:
                                    sb = new StringBuilder();
                                    for (Map.Entry entry2 : ((Map) message.obj).entrySet()) {
                                        AbstractC14640lm r82 = (AbstractC14640lm) entry2.getKey();
                                        StringBuilder sb3 = new StringBuilder("groupmgr/remove-participant/error/");
                                        sb3.append(r82);
                                        sb3.append("/");
                                        sb3.append(entry2.getValue());
                                        Log.e(sb3.toString());
                                        int intValue2 = ((Number) entry2.getValue()).intValue();
                                        C15370n3 A0B2 = this.A02.A0B(r82);
                                        if (intValue2 != 404) {
                                            Context context4 = this.A04.A00;
                                            int i10 = R.string.error_removing_participant_406;
                                            if (intValue2 != 406) {
                                                i10 = R.string.error_removing_participant;
                                            }
                                            sb.append(context4.getString(i10, this.A03.A04(A0B2)));
                                            sb.append("\n");
                                        }
                                    }
                                    if (TextUtils.isEmpty(sb)) {
                                        return;
                                    }
                                    this.A00.A0L(sb.toString(), 0);
                                    return;
                                case 3003:
                                    sb = new StringBuilder();
                                    for (Map.Entry entry3 : ((Map) message.obj).entrySet()) {
                                        AbstractC14640lm r5 = (AbstractC14640lm) entry3.getKey();
                                        StringBuilder sb4 = new StringBuilder("groupmgr/add-admins/error/");
                                        sb4.append(r5);
                                        sb4.append("/");
                                        sb4.append(entry3.getValue());
                                        Log.e(sb4.toString());
                                        int intValue3 = ((Number) entry3.getValue()).intValue();
                                        C15370n3 A0B3 = this.A02.A0B(r5);
                                        if (intValue3 != 404) {
                                            context3 = this.A04.A00;
                                            i5 = R.string.failed_announcement_group_add_admin;
                                            if (intValue3 == 419) {
                                                sb.append(context3.getString(i5, this.A03.A04(A0B3)));
                                                sb.append("\n");
                                            }
                                        } else {
                                            context3 = this.A04.A00;
                                        }
                                        i5 = R.string.error_adding_participant;
                                        sb.append(context3.getString(i5, this.A03.A04(A0B3)));
                                        sb.append("\n");
                                    }
                                    this.A00.A0L(sb.toString(), 0);
                                    return;
                                case 3004:
                                    sb = new StringBuilder();
                                    for (Map.Entry entry4 : ((Map) message.obj).entrySet()) {
                                        AbstractC14640lm r83 = (AbstractC14640lm) entry4.getKey();
                                        StringBuilder sb5 = new StringBuilder("groupmgr/remove-admins/error/");
                                        sb5.append(r83);
                                        sb5.append("/");
                                        sb5.append(entry4.getValue());
                                        Log.e(sb5.toString());
                                        int intValue4 = ((Number) entry4.getValue()).intValue();
                                        C15370n3 A0B4 = this.A02.A0B(r83);
                                        Context context5 = this.A04.A00;
                                        if (intValue4 != 404) {
                                            i6 = R.string.error_removing_admin_406;
                                            if (intValue4 == 406) {
                                                sb.append(context5.getString(i6, this.A03.A04(A0B4)));
                                                sb.append("\n");
                                            }
                                        }
                                        i6 = R.string.error_removing_participant;
                                        sb.append(context5.getString(i6, this.A03.A04(A0B4)));
                                        sb.append("\n");
                                    }
                                    this.A00.A0L(sb.toString(), 0);
                                    return;
                                case 3005:
                                    r13 = this.A00;
                                    i2 = R.string.group_error_add_participants_too_many_requests;
                                    r13.A05(i2, 0);
                                    return;
                                case 3006:
                                    str2 = "groupmgr/handle groupchat description change";
                                    Log.i(str2);
                                    r2 = (AbstractC15340mz) message.obj;
                                    this.A06.A0S(r2);
                                    break;
                                case 3007:
                                    this.A00.A05(R.string.group_error_add_participant_repeated_add_blocked, 0);
                                    r13 = this.A00;
                                    i2 = R.string.group_error_add_participants_no_such_group;
                                    r13.A05(i2, 0);
                                    return;
                                case 3008:
                                    str2 = "groupmgr/handle groupchat restrict mode change";
                                    Log.i(str2);
                                    r2 = (AbstractC15340mz) message.obj;
                                    this.A06.A0S(r2);
                                    break;
                                default:
                                    switch (i7) {
                                        case 3010:
                                            r13 = this.A00;
                                            i2 = R.string.failed_update_group_info_not_admin;
                                            break;
                                        case 3011:
                                            r13 = this.A00;
                                            i2 = R.string.failed_update_group_info_not_participant;
                                            break;
                                        case 3012:
                                            r13 = this.A00;
                                            i2 = R.string.failed_update_group_info;
                                            break;
                                        default:
                                            switch (i7) {
                                                case 3014:
                                                    int A02 = this.A01.A02(AbstractC15460nI.A1G);
                                                    r8 = this.A00;
                                                    str = this.A05.A0I(new Object[]{Integer.valueOf(A02)}, R.plurals.failed_announcement_group_toggle_time, (long) A02);
                                                    r8.A0L(str, 0);
                                                    return;
                                                case 3015:
                                                    Log.i("groupmgr/handle groupchat description updated");
                                                    r2 = (AbstractC15340mz) message.obj;
                                                    this.A06.A0W(r2);
                                                    break;
                                                case 3016:
                                                    str2 = "groupmgr/handle groupchat no frequently forwarded change";
                                                    Log.i(str2);
                                                    r2 = (AbstractC15340mz) message.obj;
                                                    this.A06.A0S(r2);
                                                    break;
                                                case 3017:
                                                    str2 = "groupmgr/handle groupchat ephemeral setting changed";
                                                    Log.i(str2);
                                                    r2 = (AbstractC15340mz) message.obj;
                                                    this.A06.A0S(r2);
                                                    break;
                                                default:
                                                    switch (i7) {
                                                        case 3019:
                                                            Object obj = message.obj;
                                                            AnonymousClass009.A05(obj);
                                                            int intValue5 = ((Number) obj).intValue();
                                                            r8 = this.A00;
                                                            str = this.A04.A00.getResources().getQuantityString(R.plurals.failed_community_add_admin_limit_reached, intValue5, Integer.valueOf(intValue5));
                                                            r8.A0L(str, 0);
                                                            return;
                                                        case 3020:
                                                            str2 = "groupmgr/handle groupchat membership approval request";
                                                            break;
                                                        case 3021:
                                                            str2 = "groupmgr/handle groupchat membership approval mode change";
                                                            break;
                                                        case 3022:
                                                            str2 = "groupmgr/handle group member add mode change";
                                                            break;
                                                        default:
                                                            return;
                                                    }
                                                    Log.i(str2);
                                                    r2 = (AbstractC15340mz) message.obj;
                                                    this.A06.A0S(r2);
                                                    break;
                                            }
                                    }
                                    r13.A05(i2, 0);
                                    return;
                            }
                    }
            }
            r12.A07(i, 0);
            return;
        } else {
            C42821vw r02 = this.A08;
            C42871w1 r4 = (C42871w1) message.obj;
            for (C42881w2 r03 : r02.A01()) {
                ShareInviteLinkActivity shareInviteLinkActivity = r03.A00;
                C15580nU r14 = shareInviteLinkActivity.A09;
                if (r14 != null && r14.equals(r4.A00)) {
                    shareInviteLinkActivity.A2k(r4.A01);
                }
            }
            return;
        }
        r1 = this.A09;
        r0 = r2.A0z.A00;
        r1.A00(r0);
    }
}
