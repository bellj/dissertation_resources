package X;

/* renamed from: X.4CC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CC extends Exception {
    public AnonymousClass4CC(String str) {
        super(str);
    }

    public AnonymousClass4CC(Throwable th) {
        super("Json exception", th);
    }
}
