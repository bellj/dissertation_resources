package X;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.4bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94454bq {
    public static final AnonymousClass4VA A04 = new AnonymousClass4VA("REMOVE_FROZEN");
    public static final /* synthetic */ AtomicLongFieldUpdater A05 = AtomicLongFieldUpdater.newUpdater(C94454bq.class, "_state");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A06 = AtomicReferenceFieldUpdater.newUpdater(C94454bq.class, Object.class, "_next");
    public final int A00;
    public final int A01;
    public final boolean A02;
    public /* synthetic */ AtomicReferenceArray A03;
    public volatile /* synthetic */ Object _next = null;
    public volatile /* synthetic */ long _state = 0;

    public C94454bq(int i, boolean z) {
        this.A00 = i;
        this.A02 = z;
        int i2 = i - 1;
        this.A01 = i2;
        this.A03 = new AtomicReferenceArray(i);
        if (!(i2 <= 1073741823)) {
            throw C12960it.A0U("Check failed.");
        } else if ((i & i2) != 0) {
            throw C12960it.A0U("Check failed.");
        }
    }

    public final int A00(Object obj) {
        while (true) {
            C94454bq r11 = this;
            long j = r11._state;
            if ((3458764513820540928L & j) == 0) {
                int i = (int) ((1073741823 & j) >> 0);
                int i2 = (int) ((1152921503533105152L & j) >> 30);
                int i3 = r11.A01;
                if (((i2 + 2) & i3) != (i & i3)) {
                    if (!r11.A02 && r11.A03.get(i2 & i3) != null) {
                        int i4 = r11.A00;
                        if (i4 < 1024 || ((i2 - i) & 1073741823) > (i4 >> 1)) {
                            break;
                        }
                    } else if (A05.compareAndSet(r11, j, (j & -1152921503533105153L) | (((long) ((i2 + 1) & 1073741823)) << 30))) {
                        r11.A03.set(i2 & i3, obj);
                        while ((r11._state & 1152921504606846976L) != 0) {
                            r11 = r11.A02();
                            AtomicReferenceArray atomicReferenceArray = r11.A03;
                            int i5 = r11.A01 & i2;
                            Object obj2 = atomicReferenceArray.get(i5);
                            if (!(obj2 instanceof AnonymousClass4LZ) || ((AnonymousClass4LZ) obj2).A00 != i2) {
                                break;
                            }
                            atomicReferenceArray.set(i5, obj);
                        }
                        return 0;
                    }
                } else {
                    break;
                }
            } else {
                return (j & 2305843009213693952L) != 0 ? 2 : 1;
            }
        }
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
        r13 = r12._state;
        r6 = (int) ((1073741823 & r13) >> 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006a, code lost:
        if ((1152921504606846976L & r13) == 0) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006c, code lost:
        r12 = r12.A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007b, code lost:
        if (r11.compareAndSet(r12, r13, (r13 & -1073741824) | r0) == false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007d, code lost:
        r12.A03.set(r12.A01 & r6, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0085, code lost:
        return r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A01() {
        /*
            r17 = this;
        L_0x0000:
            r12 = r17
            long r13 = r12._state
            r3 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r3 = r3 & r13
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0010
            X.4VA r0 = X.C94454bq.A04
            return r0
        L_0x0010:
            r1 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r1 = r1 & r13
            r0 = 0
            long r1 = r1 >> r0
            int r7 = (int) r1
            r2 = 1152921503533105152(0xfffffffc0000000, double:1.2882296003504729E-231)
            long r2 = r2 & r13
            r0 = 30
            long r2 = r2 >> r0
            int r1 = (int) r2
            int r0 = r12.A01
            r1 = r1 & r0
            r6 = r7 & r0
            r5 = 0
            if (r1 == r6) goto L_0x0035
            java.util.concurrent.atomic.AtomicReferenceArray r3 = r12.A03
            java.lang.Object r4 = r3.get(r6)
            if (r4 != 0) goto L_0x0036
            boolean r0 = r12.A02
            if (r0 == 0) goto L_0x0000
        L_0x0035:
            return r5
        L_0x0036:
            boolean r0 = r4 instanceof X.AnonymousClass4LZ
            if (r0 != 0) goto L_0x0035
            int r2 = r7 + 1
            r0 = 1073741823(0x3fffffff, float:1.9999999)
            r2 = r2 & r0
            java.util.concurrent.atomic.AtomicLongFieldUpdater r11 = X.C94454bq.A05
            r0 = -1073741824(0xffffffffc0000000, double:NaN)
            long r15 = r13 & r0
            long r0 = (long) r2
            r2 = 0
            long r0 = r0 << r2
            long r15 = r15 | r0
            boolean r2 = r11.compareAndSet(r12, r13, r15)
            if (r2 == 0) goto L_0x0055
            r3.set(r6, r5)
            return r4
        L_0x0055:
            boolean r2 = r12.A02
            if (r2 != 0) goto L_0x005a
            goto L_0x0000
        L_0x005a:
            long r13 = r12._state
            r2 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r2 = r2 & r13
            r6 = 0
            long r2 = r2 >> r6
            int r6 = (int) r2
            r9 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r9 = r9 & r13
            r7 = 0
            int r2 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x0071
            X.4bq r12 = r12.A02()
            goto L_0x005a
        L_0x0071:
            r2 = -1073741824(0xffffffffc0000000, double:NaN)
            long r15 = r13 & r2
            long r15 = r15 | r0
            boolean r2 = r11.compareAndSet(r12, r13, r15)
            if (r2 == 0) goto L_0x005a
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r12.A03
            int r0 = r12.A01
            r0 = r0 & r6
            r1.set(r0, r5)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94454bq.A01():java.lang.Object");
    }

    public final C94454bq A02() {
        long j;
        while (true) {
            j = this._state;
            if ((j & 1152921504606846976L) != 0) {
                break;
            }
            long j2 = j | 1152921504606846976L;
            if (A05.compareAndSet(this, j, j2)) {
                j = j2;
                break;
            }
        }
        while (true) {
            C94454bq r0 = (C94454bq) this._next;
            if (r0 != null) {
                return r0;
            }
            AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = A06;
            C94454bq r4 = new C94454bq(this.A00 << 1, this.A02);
            int i = (int) ((1073741823 & j) >> 0);
            int i2 = (int) ((1152921503533105152L & j) >> 30);
            while (true) {
                int i3 = this.A01;
                if ((i & i3) != (i2 & i3)) {
                    Object obj = this.A03.get(i3 & i);
                    if (obj == null) {
                        obj = new AnonymousClass4LZ(i);
                    }
                    r4.A03.set(r4.A01 & i, obj);
                    i++;
                }
            }
            r4._state = j & -1152921504606846977L;
            AnonymousClass0KN.A00(this, null, r4, atomicReferenceFieldUpdater);
        }
    }

    public final boolean A03() {
        long j;
        do {
            j = this._state;
            if ((j & 2305843009213693952L) != 0) {
                break;
            } else if ((1152921504606846976L & j) != 0) {
                return false;
            }
        } while (!A05.compareAndSet(this, j, j | 2305843009213693952L));
        return true;
    }

    public final boolean A04() {
        long j = this._state;
        return ((int) ((1073741823 & j) >> 0)) == ((int) ((j & 1152921503533105152L) >> 30));
    }
}
