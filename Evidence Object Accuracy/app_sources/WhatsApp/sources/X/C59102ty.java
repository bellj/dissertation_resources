package X;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.components.Button;

/* renamed from: X.2ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59102ty extends AbstractC75653kC {
    public final WaTextView A00;
    public final WaTextView A01;
    public final Button A02;

    public C59102ty(View view, CartFragment cartFragment) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.total_quantity_textview);
        this.A00 = C12960it.A0N(view, R.id.estimated_value_textview);
        Button button = (Button) AnonymousClass028.A0D(view, R.id.add_more_btn);
        this.A02 = button;
        if (cartFragment != null) {
            AbstractView$OnClickListenerC34281fs.A02(button, this, cartFragment, 14);
        }
    }

    @Override // X.AbstractC75653kC
    public void A08(AnonymousClass4JV r8) {
        C84513zQ r82 = (C84513zQ) r8;
        WaTextView waTextView = this.A01;
        Resources A09 = C12960it.A09(this.A0H);
        int i = r82.A00;
        Object[] A1b = C12970iu.A1b();
        A1b[0] = Integer.valueOf(i);
        C12980iv.A15(A09, waTextView, A1b, R.plurals.products_total_quantity, i);
        boolean isEmpty = TextUtils.isEmpty(r82.A01);
        WaTextView waTextView2 = this.A00;
        if (isEmpty) {
            waTextView2.setVisibility(8);
        } else {
            waTextView2.setVisibility(0);
            waTextView2.setText(r82.A01);
        }
        boolean z = r82.A02;
        Button button = this.A02;
        if (z) {
            button.setVisibility(0);
        } else {
            button.setVisibility(8);
        }
    }
}
