package X;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.0ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13550ju extends ClassLoader {
    @Override // java.lang.ClassLoader
    public final Class loadClass(String str, boolean z) {
        if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
            return super.loadClass(str, z);
        }
        if (!FirebaseInstanceId.A03()) {
            return C13560jv.class;
        }
        Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
        return C13560jv.class;
    }
}
