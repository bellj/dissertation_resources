package X;

import java.util.Set;

/* renamed from: X.0Hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC03290Hf extends AnonymousClass0I0 implements AbstractC12720iO {
    public String A00 = null;
    public Set A01 = null;
    public Set A02 = null;
    public Set A03 = null;
    public Set A04 = null;

    @Override // X.AbstractC12720iO
    public String AGA() {
        return this.A00;
    }

    @Override // X.AbstractC12720iO
    public Set AGB() {
        return this.A01;
    }

    @Override // X.AbstractC12720iO
    public Set AGC() {
        return this.A02;
    }

    @Override // X.AbstractC12720iO
    public Set AGD() {
        return this.A03;
    }

    @Override // X.AbstractC12720iO
    public Set AH3() {
        return this.A04;
    }

    @Override // X.AbstractC12720iO
    public void Acj(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC12720iO
    public void Ack(Set set) {
        this.A01 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acl(Set set) {
        this.A02 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acm(Set set) {
        this.A03 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acy(Set set) {
        this.A04 = set;
    }
}
