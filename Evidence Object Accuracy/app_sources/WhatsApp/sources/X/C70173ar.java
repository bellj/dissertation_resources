package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70173ar implements AbstractC41521tf {
    public final /* synthetic */ C60762yb A00;

    public C70173ar(C60762yb r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.getResources().getDimensionPixelSize(R.dimen.conversation_row_message_thumb_size);
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r7) {
        C60762yb r3;
        ImageView imageView;
        int i;
        ImageView.ScaleType scaleType;
        if (bitmap != null) {
            String str = ((AbstractC16130oV) r7).A06;
            if (str == null || (!C26511Dt.A07(str) && !"video/mp4".equals(str) && !"video/x.looping_mp4".equals(str))) {
                r3 = this.A00;
                imageView = r3.A08;
                scaleType = ImageView.ScaleType.MATRIX;
            } else {
                r3 = this.A00;
                imageView = r3.A08;
                scaleType = ImageView.ScaleType.CENTER_CROP;
            }
            imageView.setScaleType(scaleType);
            imageView.setImageBitmap(bitmap);
            i = 0;
        } else {
            r3 = this.A00;
            imageView = r3.A08;
            imageView.setTag(null);
            i = 8;
        }
        imageView.setVisibility(i);
        r3.A04.setVisibility(i);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C60762yb r2 = this.A00;
        ImageView imageView = r2.A08;
        C12990iw.A1D(imageView);
        imageView.setVisibility(0);
        r2.A04.setVisibility(0);
    }
}
