package X;

import android.view.View;

/* renamed from: X.4ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C105764ua implements AbstractC11920h5 {
    @Override // X.AbstractC11920h5
    public final void Af5(View view, float f) {
        if (f >= -1.0f && f <= 1.0f) {
            view.setPivotX(((float) view.getWidth()) * 0.5f);
            view.setPivotY((float) view.getHeight());
            view.setRotation(f * 18.75f);
        }
    }
}
