package X;

import com.whatsapp.framework.alerts.ui.AlertCardListFragment;

/* renamed from: X.4uF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C105554uF implements AbstractC009404s {
    public final /* synthetic */ AlertCardListFragment A00;

    public C105554uF(AlertCardListFragment alertCardListFragment) {
        this.A00 = alertCardListFragment;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AlertCardListFragment alertCardListFragment = this.A00;
        alertCardListFragment.A1L();
        return new C74463i4(alertCardListFragment.A1K());
    }
}
