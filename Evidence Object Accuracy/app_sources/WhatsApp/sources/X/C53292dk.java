package X;

import android.content.Context;
import android.text.Spanned;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.components.Button;

/* renamed from: X.2dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53292dk extends ScrollView {
    public final ViewStub A00;
    public final RadioButton A01 = ((RadioButton) AnonymousClass028.A0D(this, R.id.only_share_with_button));
    public final RadioButton A02 = ((RadioButton) AnonymousClass028.A0D(this, R.id.my_contacts_except_button));
    public final RadioButton A03 = ((RadioButton) AnonymousClass028.A0D(this, R.id.my_contacts_button));
    public final WaTextView A04;
    public final WaTextView A05;
    public final WaTextView A06;
    public final WaTextView A07;
    public final Button A08;

    public C53292dk(Context context) {
        super(context, null);
        ScrollView.inflate(getContext(), R.layout.privacy_settings_bottom_sheet, this);
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2, 80));
        setPadding(0, getResources().getDimensionPixelSize(R.dimen.space_xxLoose), 0, getResources().getDimensionPixelSize(R.dimen.space_loose));
        WaTextView A0N = C12960it.A0N(this, R.id.status_privacy_bottom_sheet_title);
        this.A07 = A0N;
        WaTextView A0N2 = C12960it.A0N(this, R.id.excluded);
        this.A04 = A0N2;
        WaTextView A0N3 = C12960it.A0N(this, R.id.included);
        this.A06 = A0N3;
        this.A00 = (ViewStub) AnonymousClass028.A0D(this, R.id.status_privacy_stub);
        this.A05 = C12960it.A0N(this, R.id.footer_text);
        this.A08 = (Button) AnonymousClass028.A0D(this, R.id.done_btn);
        C27531Hw.A06(A0N);
        C27531Hw.A06(A0N2);
        C27531Hw.A06(A0N3);
    }

    public void setFooterText(Spanned spanned) {
        this.A05.setText(spanned);
    }
}
