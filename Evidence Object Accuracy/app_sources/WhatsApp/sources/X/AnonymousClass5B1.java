package X;

import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.VideoPort;
import java.nio.FloatBuffer;
import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;

/* renamed from: X.5B1  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5B1 implements VideoPort {
    public int A00;
    public int A01;
    public AnonymousClass5X3 A02;
    public AbstractC94504bw A03;
    public boolean A04;
    public boolean A05;
    public final Handler A06;
    public final HandlerThread A07;
    public final GlVideoRenderer A08 = new GlVideoRenderer();
    public final String A09;
    public final boolean A0A;
    public final boolean A0B;

    public AnonymousClass5B1(String str, boolean z, boolean z2) {
        AnonymousClass009.A01();
        this.A09 = str;
        this.A0B = z;
        this.A0A = z2;
        HandlerThread handlerThread = new HandlerThread(C12960it.A0f(C12960it.A0k("VideoPort_"), hashCode()));
        this.A07 = handlerThread;
        handlerThread.start();
        this.A06 = new Handler(handlerThread.getLooper());
    }

    public static Object A00(AnonymousClass5B1 r1, Callable callable) {
        return r1.A03(callable, -100);
    }

    public final int A01() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(16384);
        AbstractC94504bw r0 = this.A03;
        AnonymousClass009.A05(r0);
        return r0.A0A() ? 0 : -3;
    }

    public Object A02() {
        if (this instanceof C849240i) {
            return ((C849240i) this).A01.getSurfaceTexture();
        }
        Surface surface = ((C849340j) this).A01.getHolder().getSurface();
        if (surface == null || !surface.isValid()) {
            return null;
        }
        return surface;
    }

    public final Object A03(Callable callable, Object obj) {
        if (Thread.currentThread() == this.A07) {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            Exchanger exchanger = new Exchanger();
            if (!this.A06.post(new RunnableBRunnable0Shape3S0300000_I1(this, exchanger, callable, 15))) {
                return obj;
            }
            try {
                return exchanger.exchange(null);
            } catch (InterruptedException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public void A04() {
        AnonymousClass009.A01();
        this.A05 = false;
        if (this.A04) {
            AnonymousClass5X3 r0 = this.A02;
            if (r0 != null) {
                r0.APE(this);
            }
            A03(new Callable() { // from class: X.5Db
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    AnonymousClass5B1.this.A06();
                    return C12980iv.A0i();
                }
            }, -100);
            this.A04 = false;
        }
    }

    public void A05() {
        AnonymousClass009.A01();
        String str = this.A09;
        if (!this.A04) {
            Object A02 = A02();
            if (A02 == null) {
                Log.w(C12960it.A0d("/openPort no Surface/SurfaceTexture", C12960it.A0j(str)));
                return;
            }
            this.A05 = false;
            this.A04 = true;
            ((Number) A00(this, new Callable(A02) { // from class: X.5Dj
                public final /* synthetic */ Object A01;

                {
                    this.A01 = r2;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    int i;
                    AbstractC94504bw r0;
                    AnonymousClass5B1 r4 = AnonymousClass5B1.this;
                    Object obj = this.A01;
                    if (r4.A03 == null) {
                        if ((obj instanceof Surface) || (obj instanceof SurfaceTexture)) {
                            try {
                                if (r4.A0B) {
                                    int[] iArr = AbstractC94504bw.A00;
                                    if (AnonymousClass5Pf.A00()) {
                                        r0 = new AnonymousClass5Pf(null, iArr);
                                    } else {
                                        r0 = new AnonymousClass5Pe(iArr);
                                    }
                                } else {
                                    int[] iArr2 = AbstractC94504bw.A01;
                                    if (AnonymousClass5Pf.A00()) {
                                        r0 = new AnonymousClass5Pf(null, iArr2);
                                    } else {
                                        r0 = new AnonymousClass5Pe(iArr2);
                                    }
                                }
                                AnonymousClass009.A05(r0);
                                r4.A03 = r0;
                                if (!r4.A07(obj)) {
                                    i = -5;
                                } else {
                                    GlVideoRenderer glVideoRenderer = r4.A08;
                                    if (!glVideoRenderer.init(29, 0)) {
                                        r4.A06();
                                        i = -2;
                                    } else {
                                        glVideoRenderer.setWindow(0, 0, r4.A03.A02(), r4.A03.A01());
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(e);
                                r4.A06();
                                i = -5;
                            }
                            return Integer.valueOf(i);
                        }
                        throw C12960it.A0U("Input must be either a Surface or SurfaceTexture");
                    }
                    i = 0;
                    return Integer.valueOf(i);
                }
            })).intValue();
            AnonymousClass5X3 r0 = this.A02;
            if (r0 != null) {
                r0.AOY(this);
            }
        }
    }

    public final void A06() {
        if (this.A03 != null) {
            this.A08.release();
            try {
                this.A03.A03();
                this.A03.A06();
                this.A03.A05();
            } catch (Exception e) {
                Log.e(e);
            }
            this.A03 = null;
        }
    }

    public final boolean A07(Object obj) {
        AnonymousClass009.A0F(C12970iu.A1Z(this.A07.getLooper(), Looper.myLooper()));
        AbstractC94504bw r1 = this.A03;
        AnonymousClass009.A05(r1);
        try {
            if (r1.A09()) {
                r1.A03();
                this.A03.A06();
            }
            if (obj instanceof Surface) {
                this.A03.A08((Surface) obj);
            } else {
                this.A03.A07((SurfaceTexture) obj);
            }
            this.A03.A04();
            return true;
        } catch (RuntimeException e) {
            Log.e(e);
            A06();
            return false;
        }
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public C92444Vx createSurfaceTexture() {
        return (C92444Vx) A03(new Callable() { // from class: X.5DT
            @Override // java.util.concurrent.Callable
            public final Object call() {
                try {
                    return new C92444Vx();
                } catch (RuntimeException e) {
                    Log.e(e);
                    return null;
                }
            }
        }, null);
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    @Deprecated
    public SurfaceHolder getSurfaceHolder() {
        if (!(this instanceof C849340j)) {
            return null;
        }
        return ((C849340j) this).A01.getHolder();
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public Point getWindowSize() {
        return (Point) A03(new Callable() { // from class: X.5Dc
            @Override // java.util.concurrent.Callable
            public final Object call() {
                AnonymousClass5B1 r2 = AnonymousClass5B1.this;
                AbstractC94504bw r1 = r2.A03;
                if (r1 == null || !r1.A09()) {
                    return new Point(0, 0);
                }
                return new Point(r1.A02(), r2.A03.A01());
            }
        }, new Point(0, 0));
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public void release() {
        AnonymousClass009.A01();
        A04();
        this.A07.quit();
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public void releaseSurfaceTexture(C92444Vx r3) {
        A03(new Callable(r3) { // from class: X.5Dm
            public final /* synthetic */ C92444Vx A01;

            {
                this.A01 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i;
                AnonymousClass5B1 r0 = AnonymousClass5B1.this;
                C92444Vx r4 = this.A01;
                AbstractC94504bw r02 = r0.A03;
                if (r02 == null || !r02.A09()) {
                    i = -6;
                } else {
                    SurfaceTexture surfaceTexture = r4.A01;
                    Log.i(C12960it.A0b("voip/video/SurfaceTextureHolder/deleteSurfaceTexture surfaceTexture = ", surfaceTexture));
                    surfaceTexture.release();
                    GLES20.glDeleteTextures(1, new int[]{r4.A00}, 0);
                    r4.A00 = 0;
                    i = 0;
                }
                return Integer.valueOf(i);
            }
        }, -100);
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public int renderNativeFrame(long j, int i, int i2, int i3, int i4, int i5) {
        AnonymousClass5X3 r1 = this.A02;
        if (r1 != null && !this.A05) {
            this.A05 = true;
            r1.AUu(this);
        }
        return C12960it.A05(A00(this, new Callable(i, i2, i3, i4, i5, j) { // from class: X.5Dx
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ int A02;
            public final /* synthetic */ int A03;
            public final /* synthetic */ int A04;
            public final /* synthetic */ long A05;

            {
                this.A05 = r7;
                this.A00 = r2;
                this.A01 = r3;
                this.A02 = r4;
                this.A03 = r5;
                this.A04 = r6;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i6;
                AnonymousClass5B1 r2 = AnonymousClass5B1.this;
                long j2 = this.A05;
                int i7 = this.A00;
                int i8 = this.A01;
                int i9 = this.A02;
                int i10 = this.A03;
                int i11 = this.A04;
                AbstractC94504bw r0 = r2.A03;
                if (r0 == null || !r0.A09()) {
                    i6 = -6;
                } else {
                    Object A02 = r2.A02();
                    if (!r2.A0A || ((r2.A03.A02() == r2.A01 && r2.A03.A01() == r2.A00) || (A02 != null && r2.A07(A02)))) {
                        r2.A08.renderNativeFrame(j2, i7, i8, i9, i10, i11);
                        AbstractC94504bw r02 = r2.A03;
                        AnonymousClass009.A05(r02);
                        i6 = -3;
                        if (r02.A0A()) {
                            i6 = 0;
                        }
                    } else {
                        i6 = -5;
                    }
                }
                return Integer.valueOf(i6);
            }
        }));
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public int renderTexture(C92444Vx r3, int i, int i2) {
        AnonymousClass5X3 r1 = this.A02;
        if (r1 != null && !this.A05) {
            this.A05 = true;
            r1.AUu(this);
        }
        return C12960it.A05(A00(this, new Callable(r3, i, i2) { // from class: X.5Dw
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ C92444Vx A03;

            {
                this.A03 = r2;
                this.A00 = r3;
                this.A01 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i3;
                AnonymousClass5B1 r5 = AnonymousClass5B1.this;
                C92444Vx r6 = this.A03;
                int i4 = this.A00;
                int i5 = this.A01;
                AbstractC94504bw r0 = r5.A03;
                if (r0 == null || !r0.A09()) {
                    i3 = -6;
                } else {
                    Object A02 = r5.A02();
                    if (!r5.A0A || ((r5.A03.A02() == r5.A01 && r5.A03.A01() == r5.A00) || (A02 != null && r5.A07(A02)))) {
                        GlVideoRenderer glVideoRenderer = r5.A08;
                        if (r6.A00 == 0) {
                            Log.i("voip/SurfaceTextureHolder/render ignore rendering after texture is released");
                            i3 = -7;
                        } else {
                            SurfaceTexture surfaceTexture = r6.A01;
                            surfaceTexture.updateTexImage();
                            float[] fArr = r6.A03;
                            surfaceTexture.getTransformMatrix(fArr);
                            if (r6.A04 != 0) {
                                Matrix.rotateM(fArr, 0, (float) (r6.A04 * 90), 0.0f, 0.0f, 1.0f);
                            }
                            if (r6.A04 == 1) {
                                Matrix.translateM(fArr, 0, 0.0f, -1.0f, 0.0f);
                            } else if (r6.A04 == 2) {
                                Matrix.translateM(fArr, 0, -1.0f, -1.0f, 0.0f);
                            } else if (r6.A04 == 3) {
                                Matrix.translateM(fArr, 0, -1.0f, 0.0f, 0.0f);
                            }
                            FloatBuffer asFloatBuffer = r6.A02.asFloatBuffer();
                            asFloatBuffer.rewind();
                            asFloatBuffer.put(fArr);
                            glVideoRenderer.renderOesTexture(r6.A00, i4, i5, asFloatBuffer);
                            AbstractC94504bw r02 = r5.A03;
                            AnonymousClass009.A05(r02);
                            i3 = -3;
                            if (r02.A0A()) {
                                i3 = 0;
                            }
                        }
                    } else {
                        i3 = -5;
                    }
                }
                return Integer.valueOf(i3);
            }
        }));
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public int resetBlackScreen() {
        return C12960it.A05(A00(this, new Callable() { // from class: X.5Da
            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i;
                AnonymousClass5B1 r1 = AnonymousClass5B1.this;
                AbstractC94504bw r0 = r1.A03;
                if (r0 == null || !r0.A09()) {
                    i = -6;
                } else {
                    i = r1.A01();
                }
                return Integer.valueOf(i);
            }
        }));
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public int setCornerRadius(float f) {
        return C12960it.A05(A00(this, new Callable(f) { // from class: X.5Dk
            public final /* synthetic */ float A00;

            {
                this.A00 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i;
                AnonymousClass5B1 r2 = AnonymousClass5B1.this;
                float f2 = this.A00;
                AbstractC94504bw r0 = r2.A03;
                if (r0 == null || !r0.A09()) {
                    i = -6;
                } else {
                    r2.A08.setCornerRadius(f2);
                    i = 0;
                }
                return Integer.valueOf(i);
            }
        }));
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public void setListener(AnonymousClass5X3 r3) {
        AnonymousClass009.A01();
        AnonymousClass5X3 r1 = this.A02;
        if (r3 != r1) {
            if (this.A04 && r1 != null) {
                r1.APE(this);
            }
            this.A02 = r3;
            if (this.A04 && r3 != null) {
                r3.AOY(this);
            }
        }
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public int setScaleType(int i) {
        return C12960it.A05(A00(this, new Callable(i) { // from class: X.5Dl
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i2;
                AnonymousClass5B1 r2 = AnonymousClass5B1.this;
                int i3 = this.A00;
                AbstractC94504bw r0 = r2.A03;
                if (r0 == null || !r0.A09()) {
                    i2 = -6;
                } else {
                    r2.A08.setScaleType(i3);
                    i2 = 0;
                }
                return Integer.valueOf(i2);
            }
        }));
    }
}
