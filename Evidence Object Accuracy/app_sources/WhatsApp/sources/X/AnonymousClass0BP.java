package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* renamed from: X.0BP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BP extends BaseAdapter {
    public int A00 = -1;
    public final /* synthetic */ AnonymousClass0XO A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public AnonymousClass0BP(AnonymousClass0XO r2) {
        this.A01 = r2;
        A01();
    }

    /* renamed from: A00 */
    public C07340Xp getItem(int i) {
        AnonymousClass07H r0 = this.A01.A05;
        r0.A05();
        ArrayList arrayList = r0.A08;
        int i2 = i + 0;
        int i3 = this.A00;
        if (i3 >= 0 && i2 >= i3) {
            i2++;
        }
        return (C07340Xp) arrayList.get(i2);
    }

    public void A01() {
        AnonymousClass07H r0 = this.A01.A05;
        C07340Xp r4 = r0.A04;
        if (r4 != null) {
            r0.A05();
            ArrayList arrayList = r0.A08;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (arrayList.get(i) == r4) {
                    this.A00 = i;
                    return;
                }
            }
        }
        this.A00 = -1;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        AnonymousClass07H r0 = this.A01.A05;
        r0.A05();
        int size = r0.A08.size() - 0;
        return this.A00 >= 0 ? size - 1 : size;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            AnonymousClass0XO r0 = this.A01;
            view = r0.A02.inflate(r0.A00, viewGroup, false);
        }
        ((AbstractC12290hg) view).AIt(getItem(i), 0);
        return view;
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        A01();
        super.notifyDataSetChanged();
    }
}
