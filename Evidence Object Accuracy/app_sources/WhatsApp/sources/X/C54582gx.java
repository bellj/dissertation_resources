package X;

import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2gx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54582gx extends AbstractC018308n {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public C54582gx(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC018308n
    public void A00(Canvas canvas, C05480Ps r11, RecyclerView recyclerView) {
        int paddingLeft = recyclerView.getPaddingLeft();
        AbstractView$OnCreateContextMenuListenerC35851ir r6 = this.A00;
        int i = paddingLeft + ((int) r6.A01);
        int width = (recyclerView.getWidth() - ((int) r6.A02)) - recyclerView.getPaddingRight();
        for (int i2 = 0; i2 < recyclerView.getChildCount(); i2++) {
            View childAt = recyclerView.getChildAt(i2);
            AnonymousClass0B6 r1 = (AnonymousClass0B6) childAt.getLayoutParams();
            int bottom = childAt.getBottom() + ((ViewGroup.MarginLayoutParams) r1).bottomMargin;
            int intrinsicHeight = r6.A0I.getIntrinsicHeight() + bottom;
            if (r1.A00() == 0 && r6.A0n == null) {
                r6.A0I.setBounds(0, bottom, recyclerView.getWidth(), intrinsicHeight);
            } else {
                r6.A0I.setBounds(i, bottom, width, intrinsicHeight);
            }
            r6.A0I.draw(canvas);
        }
    }
}
