package X;

/* renamed from: X.5FC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FC implements AnonymousClass5VN {
    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        appendable.append('[');
        Object[] objArr = (Object[]) obj;
        boolean z = false;
        for (Object obj2 : objArr) {
            z = C72453ed.A1U(appendable, z);
            AnonymousClass4ZZ.A00(appendable, obj2, r7);
        }
        appendable.append(']');
    }
}
