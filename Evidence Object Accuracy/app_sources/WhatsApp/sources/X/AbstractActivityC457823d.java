package X;

import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.23d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC457823d extends ActivityC13830kP {
    public boolean A00 = false;

    public AbstractActivityC457823d() {
        A0R(new C102734pc(this));
    }

    @Override // X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            RequestPermissionActivity requestPermissionActivity = (RequestPermissionActivity) this;
            AnonymousClass01J r2 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) requestPermissionActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            requestPermissionActivity.A05 = (C25661Ag) r2.A8G.get();
            requestPermissionActivity.A04 = (C21200x2) r2.A2q.get();
            requestPermissionActivity.A01 = (C20730wE) r2.A4J.get();
            requestPermissionActivity.A02 = (C15890o4) r2.AN1.get();
            requestPermissionActivity.A03 = (C14820m6) r2.AN3.get();
            requestPermissionActivity.A00 = (C249417m) r2.A0S.get();
        }
    }
}
