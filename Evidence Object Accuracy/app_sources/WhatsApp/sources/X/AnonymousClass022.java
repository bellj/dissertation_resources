package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.WorkDatabase_Impl;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.022  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass022 {
    public static AnonymousClass022 A0A;
    public static AnonymousClass022 A0B;
    public static final Object A0C = new Object();
    public BroadcastReceiver.PendingResult A00;
    public Context A01;
    public C05180Oo A02;
    public C07650Zp A03;
    public WorkDatabase A04;
    public AnonymousClass0MZ A05;
    public AbstractC11500gO A06;
    public List A07;
    public boolean A08;
    public volatile AbstractC04000Jz A09;

    static {
        C06390Tk.A01("WorkManagerImpl");
    }

    public AnonymousClass022(Context context, C05180Oo r29, AbstractC11500gO r30) {
        C05260Ow r4;
        String obj;
        boolean z = context.getResources().getBoolean(R.bool.workmanager_test_configuration);
        Context applicationContext = context.getApplicationContext();
        ExecutorC10610eu r2 = ((C07760a2) r30).A01;
        if (z) {
            r4 = new C05260Ow(applicationContext, null);
            r4.A07 = true;
        } else if ("androidx.work.workdb".trim().length() != 0) {
            r4 = new C05260Ow(applicationContext, "androidx.work.workdb");
            r4.A01 = new AnonymousClass0ZG(applicationContext);
        } else {
            throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder");
        }
        r4.A04 = r2;
        AnonymousClass0LN r1 = new AnonymousClass0LN();
        ArrayList arrayList = r4.A02;
        if (arrayList == null) {
            arrayList = new ArrayList();
            r4.A02 = arrayList;
        }
        arrayList.add(r1);
        r4.A00(AnonymousClass0MM.A01);
        r4.A00(new C02960Fn(applicationContext, 2, 3));
        r4.A00(AnonymousClass0MM.A02);
        r4.A00(AnonymousClass0MM.A03);
        r4.A00(new C02960Fn(applicationContext, 5, 6));
        r4.A00(AnonymousClass0MM.A04);
        r4.A00(AnonymousClass0MM.A05);
        r4.A00(AnonymousClass0MM.A06);
        r4.A00(new C02970Fo(applicationContext));
        r4.A00(new C02960Fn(applicationContext, 10, 11));
        r4.A00(AnonymousClass0MM.A00);
        r4.A08 = false;
        r4.A06 = true;
        Context context2 = r4.A09;
        if (context2 != null) {
            Class cls = r4.A0B;
            if (cls != null) {
                Executor executor = r4.A04;
                Executor executor2 = r4.A05;
                if (executor == null) {
                    if (executor2 == null) {
                        executor2 = AnonymousClass05O.A02;
                        r4.A05 = executor2;
                    }
                    r4.A04 = executor2;
                } else if (executor2 == null) {
                    r4.A05 = executor;
                }
                AbstractC11890h2 r14 = r4.A01;
                if (r14 == null) {
                    r14 = new AnonymousClass0ZF();
                    r4.A01 = r14;
                }
                AnonymousClass0O7 r10 = new AnonymousClass0O7(context2, r4.A00.A00(context2), r4.A0A, r14, r4.A0C, r4.A02, r4.A04, r4.A05, r4.A07, r4.A08, r4.A06);
                String name = cls.getPackage().getName();
                String canonicalName = cls.getCanonicalName();
                boolean isEmpty = name.isEmpty();
                canonicalName = !isEmpty ? canonicalName.substring(name.length() + 1) : canonicalName;
                StringBuilder sb = new StringBuilder();
                sb.append(canonicalName.replace('.', '_'));
                sb.append("_Impl");
                String obj2 = sb.toString();
                if (isEmpty) {
                    obj = obj2;
                } else {
                    try {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(name);
                        sb2.append(".");
                        sb2.append(obj2);
                        obj = sb2.toString();
                    } catch (ClassNotFoundException unused) {
                        StringBuilder sb3 = new StringBuilder("cannot find implementation for ");
                        sb3.append(cls.getCanonicalName());
                        sb3.append(". ");
                        sb3.append(obj2);
                        sb3.append(" does not exist");
                        throw new RuntimeException(sb3.toString());
                    } catch (IllegalAccessException unused2) {
                        StringBuilder sb4 = new StringBuilder("Cannot access the constructor");
                        sb4.append(cls.getCanonicalName());
                        throw new RuntimeException(sb4.toString());
                    } catch (InstantiationException unused3) {
                        StringBuilder sb5 = new StringBuilder("Failed to create an instance of ");
                        sb5.append(cls.getCanonicalName());
                        throw new RuntimeException(sb5.toString());
                    }
                }
                AnonymousClass0QN r6 = (AnonymousClass0QN) Class.forName(obj).newInstance();
                AnonymousClass0SX r22 = new AnonymousClass0SX(r10, new AnonymousClass0PA((WorkDatabase_Impl) r6));
                C05040Oa r12 = new C05040Oa(r10.A00);
                r12.A02 = r10.A04;
                r12.A01 = r22;
                AbstractC12910il A7s = r10.A03.A7s(r12.A00());
                r6.A00 = A7s;
                boolean z2 = r10.A01 == EnumC03830Jh.WRITE_AHEAD_LOGGING;
                AnonymousClass0ZH r3 = (AnonymousClass0ZH) A7s;
                synchronized (r3.A04) {
                    C020309q r0 = r3.A00;
                    if (r0 != null) {
                        r0.setWriteAheadLoggingEnabled(z2);
                    }
                    r3.A01 = z2;
                }
                r6.A01 = r10.A05;
                r6.A02 = r10.A06;
                r6.A03 = new ExecutorC10600et(r10.A07);
                r6.A04 = r10.A09;
                r6.A05 = z2;
                WorkDatabase workDatabase = (WorkDatabase) r6;
                Context applicationContext2 = context.getApplicationContext();
                C06390Tk r02 = new C06390Tk(r29.A00);
                synchronized (C06390Tk.class) {
                    C06390Tk.A01 = r02;
                }
                List asList = Arrays.asList(AnonymousClass0TI.A00(applicationContext2, this), new C07680Zs(applicationContext2, r29, this, r30));
                C07650Zp r03 = new C07650Zp(context, r29, workDatabase, r30, asList);
                Context applicationContext3 = context.getApplicationContext();
                this.A01 = applicationContext3;
                this.A02 = r29;
                this.A06 = r30;
                this.A04 = workDatabase;
                this.A07 = asList;
                this.A03 = r03;
                this.A05 = new AnonymousClass0MZ(workDatabase);
                this.A08 = false;
                if (Build.VERSION.SDK_INT < 24 || !applicationContext3.isDeviceProtectedStorage()) {
                    ((C07760a2) this.A06).A01.execute(new RunnableC10270eJ(applicationContext3, this));
                    return;
                }
                throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
            }
            throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
        }
        throw new IllegalArgumentException("Cannot provide null context for the database.");
    }

    public static AnonymousClass022 A00(Context context) {
        AnonymousClass022 r0;
        synchronized (A0C) {
            r0 = A0B;
            if (r0 == null && (r0 = A0A) == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof AnonymousClass003) {
                    C05180Oo workManagerConfiguration = ((AnonymousClass003) applicationContext).getWorkManagerConfiguration();
                    if (A0B == null) {
                        Context applicationContext2 = applicationContext.getApplicationContext();
                        AnonymousClass022 r1 = A0A;
                        if (r1 == null) {
                            r1 = new AnonymousClass022(applicationContext2, workManagerConfiguration, new C07760a2(workManagerConfiguration.A06));
                            A0A = r1;
                        }
                        A0B = r1;
                    } else if (A0A != null) {
                        throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
                    }
                    r0 = A00(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return r0;
    }

    public final AnonymousClass03v A01(AnonymousClass023 r7, AnonymousClass021 r8, String str) {
        List singletonList = Collections.singletonList(r8);
        if (!singletonList.isEmpty()) {
            return new AnonymousClass03v(r7, this, str, singletonList, null);
        }
        throw new IllegalArgumentException("beginUniqueWork needs at least one OneTimeWorkRequest.");
    }

    public AbstractFutureC44231yX A02() {
        RunnableC10140e6 r1 = new RunnableC10140e6(this);
        ((C07760a2) this.A06).A01.execute(r1);
        return r1.A00();
    }

    public void A03() {
        synchronized (A0C) {
            this.A08 = true;
            BroadcastReceiver.PendingResult pendingResult = this.A00;
            if (pendingResult != null) {
                pendingResult.finish();
                this.A00 = null;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void A04() {
        if (Build.VERSION.SDK_INT >= 23) {
            C07670Zr.A03(this.A01);
        }
        WorkDatabase workDatabase = this.A04;
        C07740a0 r0 = (C07740a0) workDatabase.A0B();
        AnonymousClass0QN r3 = r0.A01;
        r3.A02();
        AbstractC05330Pd r2 = r0.A06;
        AbstractC12830ic A00 = r2.A00();
        r3.A03();
        try {
            ((C02980Fp) A00).A00.executeUpdateDelete();
            r3.A05();
            r3.A04();
            r2.A02(A00);
            AnonymousClass0TI.A01(this.A02, workDatabase, this.A07);
        } catch (Throwable th) {
            r3.A04();
            r2.A02(A00);
            throw th;
        }
    }

    public void A05(AnonymousClass023 r7, AnonymousClass021 r8, String str) {
        new AnonymousClass03v(r7, this, str, Collections.singletonList(r8), null).A03();
    }

    public final void A06(AnonymousClass020 r7) {
        List singletonList = Collections.singletonList(r7);
        if (!singletonList.isEmpty()) {
            new AnonymousClass03v(AnonymousClass023.KEEP, this, null, singletonList, null).A03();
            return;
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }

    public void A07(String str) {
        ((C07760a2) this.A06).A01.execute(new AnonymousClass0Gm(this, str));
    }

    public void A08(String str) {
        ((C07760a2) this.A06).A01.execute(new C03180Gn(this, str, true));
    }

    public void A09(String str) {
        AbstractC11500gO r2 = this.A06;
        ((C07760a2) r2).A01.execute(new RunnableC10210eD(this, str, false));
    }
}
