package X;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import com.whatsapp.util.Log;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.1Ha  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27361Ha {
    public final Application A00;
    public final AbstractC15710nm A01;

    public C27361Ha(Application application, AbstractC15710nm r2) {
        this.A01 = r2;
        this.A00 = application;
    }

    public void A00() {
        Throwable e;
        if (Build.VERSION.SDK_INT == 21) {
            try {
                Class.forName("android.media.session.MediaSessionLegacyHelper").getDeclaredMethod("getHelper", Context.class).invoke(null, this.A00);
            } catch (InvocationTargetException e2) {
                e = e2.getTargetException();
                Log.e("MediaSessionLegacyHelperLeakFix/applyFix", e);
                this.A01.A02("MediaSessionLegacyHelperLeakFix", e.getMessage(), e);
            } catch (Exception e3) {
                e = e3;
                Log.e("MediaSessionLegacyHelperLeakFix/applyFix", e);
                this.A01.A02("MediaSessionLegacyHelperLeakFix", e.getMessage(), e);
            }
        }
    }
}
