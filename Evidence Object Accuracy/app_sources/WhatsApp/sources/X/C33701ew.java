package X;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1ew  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33701ew {
    public final AnonymousClass1V2 A00;
    public final List A01;
    public final List A02;
    public final List A03;
    public final Map A04;
    public final Map A05;

    public C33701ew() {
        this.A00 = null;
        this.A02 = Collections.emptyList();
        this.A03 = Collections.emptyList();
        this.A01 = Collections.emptyList();
        this.A05 = Collections.emptyMap();
        this.A04 = Collections.emptyMap();
    }

    public C33701ew(AnonymousClass1V2 r3, List list, List list2, List list3, Map map, Map map2) {
        AnonymousClass009.A05(list);
        AnonymousClass009.A05(list2);
        AnonymousClass009.A05(list3);
        AnonymousClass009.A05(map);
        AnonymousClass009.A05(map2);
        Collections.sort(list, new Comparator(true) { // from class: X.5Cl
            public final /* synthetic */ boolean A00;

            {
                this.A00 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                boolean z = this.A00;
                AnonymousClass1V2 r6 = (AnonymousClass1V2) obj;
                AnonymousClass1V2 r7 = (AnonymousClass1V2) obj2;
                if (r6.A0C()) {
                    return -1;
                }
                if (r7.A0C()) {
                    return 1;
                }
                if (z) {
                    if (C15380n4.A0M(r6.A0A)) {
                        return -1;
                    }
                    if (C15380n4.A0M(r7.A0A)) {
                        return 1;
                    }
                }
                return -(r6.A04() > r7.A04() ? 1 : (r6.A04() == r7.A04() ? 0 : -1));
            }
        });
        Collections.sort(list2, new Comparator(true) { // from class: X.5Cl
            public final /* synthetic */ boolean A00;

            {
                this.A00 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                boolean z = this.A00;
                AnonymousClass1V2 r6 = (AnonymousClass1V2) obj;
                AnonymousClass1V2 r7 = (AnonymousClass1V2) obj2;
                if (r6.A0C()) {
                    return -1;
                }
                if (r7.A0C()) {
                    return 1;
                }
                if (z) {
                    if (C15380n4.A0M(r6.A0A)) {
                        return -1;
                    }
                    if (C15380n4.A0M(r7.A0A)) {
                        return 1;
                    }
                }
                return -(r6.A04() > r7.A04() ? 1 : (r6.A04() == r7.A04() ? 0 : -1));
            }
        });
        Collections.sort(list3, new Comparator(false) { // from class: X.5Cl
            public final /* synthetic */ boolean A00;

            {
                this.A00 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                boolean z = this.A00;
                AnonymousClass1V2 r6 = (AnonymousClass1V2) obj;
                AnonymousClass1V2 r7 = (AnonymousClass1V2) obj2;
                if (r6.A0C()) {
                    return -1;
                }
                if (r7.A0C()) {
                    return 1;
                }
                if (z) {
                    if (C15380n4.A0M(r6.A0A)) {
                        return -1;
                    }
                    if (C15380n4.A0M(r7.A0A)) {
                        return 1;
                    }
                }
                return -(r6.A04() > r7.A04() ? 1 : (r6.A04() == r7.A04() ? 0 : -1));
            }
        });
        this.A00 = r3;
        this.A02 = Collections.unmodifiableList(list);
        this.A03 = Collections.unmodifiableList(list2);
        this.A01 = Collections.unmodifiableList(list3);
        this.A05 = Collections.unmodifiableMap(map);
        this.A04 = Collections.unmodifiableMap(map2);
    }

    public List A00() {
        return this.A01;
    }

    public List A01() {
        return this.A02;
    }

    public List A02() {
        return this.A03;
    }

    public Map A03() {
        return this.A05;
    }

    public boolean A04() {
        return this.A00 == null && this.A02.isEmpty() && this.A03.isEmpty() && this.A01.isEmpty() && this.A05.isEmpty() && this.A04.isEmpty();
    }
}
