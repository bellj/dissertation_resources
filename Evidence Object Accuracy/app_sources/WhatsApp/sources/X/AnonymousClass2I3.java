package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.facebook.redex.ViewOnClickCListenerShape11S0100000_I1_5;
import com.facebook.redex.ViewOnClickCListenerShape12S0100000_I1_6;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.Conversation;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.YoutubePlayerTouchOverlay;

/* renamed from: X.2I3 */
/* loaded from: classes2.dex */
public class AnonymousClass2I3 implements AbstractC35401hl {
    public double A00;
    public int A01 = 0;
    public int A02 = 3;
    public int A03 = 0;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public FrameLayout A08;
    public C89114Is A09;
    public AnonymousClass4NS A0A;
    public AnonymousClass1IS A0B;
    public ScaleGestureDetector$OnScaleGestureListenerC52942c4 A0C;
    public AnonymousClass2G9 A0D;
    public AnonymousClass21T A0E;
    public String A0F;
    public boolean A0G;
    public boolean A0H;
    public final Context A0I;
    public final Rect A0J = new Rect();
    public final AnonymousClass12P A0K;
    public final AbstractC15710nm A0L;
    public final C14900mE A0M;
    public final Mp4Ops A0N;
    public final C18790t3 A0O;
    public final AnonymousClass01d A0P;
    public final C16590pI A0Q;
    public final AnonymousClass018 A0R;
    public final C14850m9 A0S;
    public final AbstractC14440lR A0T;
    public final C91824Tg A0U;

    public AnonymousClass2I3(Context context, AnonymousClass12P r3, AbstractC15710nm r4, C14900mE r5, Mp4Ops mp4Ops, C18790t3 r7, AnonymousClass01d r8, C16590pI r9, AnonymousClass018 r10, C14850m9 r11, C16120oU r12, AbstractC14440lR r13) {
        this.A0Q = r9;
        this.A0I = context;
        this.A0N = mp4Ops;
        this.A0S = r11;
        this.A0M = r5;
        this.A0L = r4;
        this.A0T = r13;
        this.A0O = r7;
        this.A0K = r3;
        this.A0P = r8;
        this.A0R = r10;
        this.A0U = new C91824Tg(r12);
    }

    public static /* synthetic */ void A00(AnonymousClass2I3 r2) {
        boolean A05 = r2.A0D.A05();
        AnonymousClass2G9 r0 = r2.A0D;
        if (A05) {
            r0.A0C();
        } else {
            r0.A01();
        }
    }

    public static /* synthetic */ void A02(AnonymousClass2I3 r1) {
        if (r1.A0H) {
            r1.A9m(false);
        } else {
            r1.A9X();
        }
    }

    public static /* synthetic */ boolean A03(MotionEvent motionEvent, AnonymousClass2I3 r2) {
        if (!AnonymousClass23N.A05(r2.A0P.A0P()) || motionEvent.getActionMasked() != 10) {
            return false;
        }
        r2.A08.requestFocus();
        r2.A08.performClick();
        return true;
    }

    public String A04(C14320lF r5) {
        String str = r5.A0C;
        String str2 = r5.A0D;
        if (str == null || str2 == null || !this.A0S.A07(1912)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" - ");
        sb.append(str2);
        return sb.toString();
    }

    public final void A05() {
        String str = this.A0F;
        if (str != null) {
            this.A0K.Ab9(this.A0I, Uri.parse(str));
        }
        this.A0U.A00 = true;
        A7N();
    }

    public final void A06(Rect rect, Rect rect2, View view) {
        float f;
        float width;
        view.setPivotX(0.0f);
        view.setPivotY(0.0f);
        float f2 = 1.0f;
        if (this.A0H) {
            f = this.A0C.A00;
        } else {
            f = 1.0f;
        }
        if (Settings.Global.getFloat(this.A0I.getContentResolver(), "animator_duration_scale", 0.0f) == 0.0f) {
            view.setX((float) rect.left);
            view.setY((float) rect.top);
            view.setScaleX(f);
            view.setScaleY(f);
            view.requestLayout();
            return;
        }
        boolean z = this.A0H;
        float width2 = ((float) rect2.width()) / ((float) rect2.height());
        float width3 = ((float) rect.width()) / ((float) rect.height());
        if (z ? width2 >= width3 : width2 <= width3) {
            width = ((float) rect.width()) / ((float) rect2.width());
            float height = ((((float) rect2.height()) * width) - ((float) rect.height())) / 2.0f;
            rect.top = (int) (((float) rect.top) - height);
            rect.bottom = (int) (((float) rect.bottom) + height);
        } else {
            width = ((float) rect.height()) / ((float) rect2.height());
            float width4 = ((((float) rect2.width()) * width) - ((float) rect.width())) / 2.0f;
            rect.left = (int) (((float) rect.left) - width4);
            rect.right = (int) (((float) rect.right) + width4);
        }
        if (Float.isNaN(width) || Float.isInfinite(width)) {
            width = 1.0f;
        }
        if (!Float.isNaN(f) && !Float.isInfinite(f)) {
            f2 = f;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(view, View.X, (float) rect.left, (float) rect2.left)).with(ObjectAnimator.ofFloat(view, View.Y, (float) rect.top, (float) rect2.top)).with(ObjectAnimator.ofFloat(view, View.SCALE_X, width, f2)).with(ObjectAnimator.ofFloat(view, View.SCALE_Y, width, f2));
        animatorSet.setDuration(250L);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.start();
    }

    public void A07(AnonymousClass1IS r21, AnonymousClass3JH r22, String str, Bitmap[] bitmapArr, int i) {
        double d;
        FrameLayout r2;
        FrameLayout frameLayout;
        AnonymousClass21T r10;
        Bitmap createBitmap;
        int i2;
        int i3;
        if (this.A0E != null || r21 != this.A0B) {
            return;
        }
        if (r22 == null) {
            Log.i("InlineVideoPlaybackImplHandler/startInlinePlayback - loadPage returned null.  Opening video externally");
            String str2 = this.A0F;
            if (str2 != null) {
                C49132Jk.A00.remove(str2);
            }
            A05();
            return;
        }
        C91824Tg r7 = this.A0U;
        r7.A04.A02();
        r7.A05.A02();
        AnonymousClass4NS r23 = this.A0A;
        if (r23 != null) {
            if (r21.equals(r23.A01.A0z)) {
                r23.A00.A0A.A06(0.0f, 0.0f, 1.0f, 0.67f);
            }
            this.A02 = 2;
        }
        int i4 = r22.A00;
        if (i4 == -1 || (i3 = r22.A01) == -1) {
            d = (i != 4 || !r22.A02.contains("/shorts/")) ? 1.7777777777777777d : 0.5620608899297423d;
        } else {
            d = ((double) i3) / ((double) i4);
        }
        int sqrt = (int) Math.sqrt(this.A00 / d);
        this.A05 = sqrt;
        this.A07 = (int) (((double) sqrt) * d);
        this.A04 = sqrt;
        Context context = this.A0I;
        this.A04 += context.getResources().getDimensionPixelSize(R.dimen.inline_video_bottom_bar_height);
        int dimension = (int) context.getResources().getDimension(R.dimen.inline_video_container_radius);
        if (Build.VERSION.SDK_INT < 21) {
            r2 = new FrameLayout(context);
        } else {
            r2 = new C865247s(context, dimension);
        }
        this.A08 = r2;
        if (r2 instanceof C865247s) {
            ((C865247s) r2).setIsFullscreen(this.A0H);
        }
        r2.setContentDescription(context.getString(R.string.inline_video_pip));
        this.A08.setFocusable(true);
        this.A08.setImportantForAccessibility(1);
        this.A08.setFocusableInTouchMode(true);
        this.A0C.A0G = new AnonymousClass5AR(this);
        this.A0G = true;
        AnonymousClass028.A0V(this.A08, 6.0f);
        FrameLayout frameLayout2 = new FrameLayout(context);
        this.A08.addView(frameLayout2);
        this.A0D = new AnonymousClass39B(context, r7, i);
        if (bitmapArr[0] != null && !this.A0S.A07(1052)) {
            ((ImageView) AnonymousClass028.A0D(this.A0D, R.id.background)).setImageBitmap(bitmapArr[0]);
        }
        ((AnonymousClass39B) this.A0D).setVideoAttribution(str);
        this.A0D.setCloseButtonListener(new AnonymousClass5V0() { // from class: X.5AX
            @Override // X.AnonymousClass5V0
            public final void AO1() {
                AnonymousClass2I3.this.A7N();
            }
        });
        AnonymousClass39B r9 = (AnonymousClass39B) this.A0D;
        r9.A04 = new AnonymousClass5V0() { // from class: X.5AY
            @Override // X.AnonymousClass5V0
            public final void AO1() {
                AnonymousClass2I3.this.A05();
            }
        };
        if (!r9.A0K()) {
            r9.A0a.setVisibility(8);
            r9.A0W.setVisibility(8);
        } else if (r9.A0l) {
            ImageButton imageButton = r9.A0X;
            switch (i) {
                case 1:
                    i2 = R.drawable.ic_pip_streamable;
                    break;
                case 2:
                    i2 = R.drawable.ic_pip_facebook_color;
                    break;
                case 3:
                    i2 = R.drawable.ic_pip_instagram_color;
                    break;
                case 4:
                    i2 = R.drawable.ic_pip_youtube;
                    break;
                case 5:
                    i2 = R.drawable.ic_pip_fb_watch;
                    break;
                case 6:
                    i2 = R.drawable.ic_pip_lasso;
                    break;
                case 7:
                    i2 = R.drawable.ic_pip_netflix;
                    break;
                case 8:
                    i2 = R.drawable.ic_pip_sharechat_color;
                    break;
                default:
                    i2 = -1;
                    break;
            }
            imageButton.setImageResource(i2);
            LinearLayout linearLayout = r9.A0a;
            linearLayout.setOnClickListener(new ViewOnClickCListenerShape12S0100000_I1_6(r9, 0));
            linearLayout.setVisibility(0);
        } else {
            ImageButton imageButton2 = r9.A0W;
            imageButton2.setImageResource(AnonymousClass3JH.A00(i));
            imageButton2.setOnClickListener(new ViewOnClickCListenerShape11S0100000_I1_5(r9, 44));
            imageButton2.setVisibility(0);
        }
        this.A0D.setFullscreenButtonClickListener(new AnonymousClass5V0() { // from class: X.5AZ
            @Override // X.AnonymousClass5V0
            public final void AO1() {
                AnonymousClass2I3.A02(AnonymousClass2I3.this);
            }
        });
        frameLayout2.addView(this.A0D);
        this.A08.setOnHoverListener(new View.OnHoverListener() { // from class: X.4mp
            @Override // android.view.View.OnHoverListener
            public final boolean onHover(View view, MotionEvent motionEvent) {
                return AnonymousClass2I3.A03(motionEvent, AnonymousClass2I3.this);
            }
        });
        this.A08.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(this, 20));
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r14 = this.A0C;
        FrameLayout frameLayout3 = this.A08;
        AnonymousClass4NS r8 = this.A0A;
        if (r21.equals(r8.A01.A0z)) {
            frameLayout = r8.A00.A0A.A09;
        } else {
            frameLayout = null;
        }
        int i5 = this.A07;
        int i6 = this.A04;
        if (r14.A0P) {
            r14.A04 = r14.A07;
            r14.A05 = r14.A08;
            r14.A0P = false;
        }
        r14.A00 = 1.0f;
        r14.A03 = i5;
        r14.A02 = i6;
        r14.A04 = r14.A04(i5);
        r14.A05 = r14.A05(i6);
        if (frameLayout == null) {
            frameLayout3.setScaleX(0.0f);
            frameLayout3.setScaleY(0.0f);
            frameLayout3.setAlpha(0.0f);
        } else {
            int[] iArr = new int[2];
            frameLayout.getLocationInWindow(iArr);
            frameLayout3.setTranslationX((float) (iArr[0] - r14.A04));
            frameLayout3.setTranslationY((float) (iArr[1] - r14.A05));
            frameLayout3.setPivotY(0.0f);
            frameLayout3.setPivotX(0.0f);
            frameLayout3.setScaleX(((float) frameLayout.getWidth()) / ((float) i5));
            frameLayout3.setScaleY(((float) frameLayout.getHeight()) / ((float) i6));
        }
        r14.A0K = true;
        r14.addView(frameLayout3, i5, i6);
        String str3 = r22.A02;
        if (i == 4) {
            Bitmap bitmap = bitmapArr[0];
            if (bitmap != null) {
                createBitmap = Bitmap.createScaledBitmap(bitmap, this.A07, this.A05, false);
            } else {
                createBitmap = Bitmap.createBitmap(this.A07, this.A05, Bitmap.Config.ARGB_8888);
                new Canvas(createBitmap).drawColor(AnonymousClass00T.A00(context, R.color.primary_surface));
            }
            r10 = new AnonymousClass39H(context, createBitmap, this.A0K, this.A0M, (AnonymousClass39B) this.A0D, str3, this.A05);
        } else {
            r10 = new AnonymousClass21S(AnonymousClass12P.A00(context), Uri.parse(str3), this.A0M, this.A0P, this.A0R, this.A0T, new C865147r(this.A0L, this.A0N, this.A0Q, AnonymousClass3JZ.A09(context, context.getString(R.string.app_name))), null);
        }
        this.A0E = r10;
        frameLayout2.addView(r10.A04(), 0);
        frameLayout2.setClipChildren(false);
        this.A0D.setClipChildren(false);
        View A04 = this.A0E.A04();
        ViewGroup.LayoutParams layoutParams = A04.getLayoutParams();
        layoutParams.height = this.A05;
        layoutParams.width = -1;
        A04.setLayoutParams(layoutParams);
        frameLayout2.setBackgroundColor(context.getResources().getColor(R.color.wds_black));
        this.A0E.A04().setBackgroundColor(context.getResources().getColor(R.color.black));
        AnonymousClass21T r24 = this.A0E;
        r24.A02 = new AnonymousClass5V3() { // from class: X.5Ad
            @Override // X.AnonymousClass5V3
            public final void APt(String str4, boolean z) {
                AnonymousClass2I3.this.A08(str4, z);
            }
        };
        r24.A03 = new C70813bt(r21, this);
        this.A0D.setPlayer(r24);
        AnonymousClass39B r25 = (AnonymousClass39B) this.A0D;
        r25.A0Y.setVisibility(4);
        r25.A0Z.setVisibility(4);
        this.A0C.setControlView(this.A0D);
        this.A0E.A07();
        this.A0C.A0H = new AnonymousClass5AS(this);
    }

    public void A08(String str, boolean z) {
        StringBuilder sb = new StringBuilder("InlineVideoPlaybackImplHandler/onPlaybackError=");
        sb.append(str);
        sb.append(" isTransient=");
        sb.append(z);
        Log.e(sb.toString());
        A05();
    }

    @Override // X.AbstractC35401hl
    public void A7N() {
        long j;
        AnonymousClass1IS r1;
        long j2;
        int i;
        Integer valueOf;
        if (this.A0G) {
            C91824Tg r8 = this.A0U;
            int i2 = this.A06;
            AnonymousClass21T r0 = this.A0E;
            if (r0 != null) {
                j = (long) r0.A02();
            } else {
                j = 0;
            }
            AnonymousClass4Xy r7 = r8.A06;
            if (r7.A02) {
                r7.A00();
            }
            AnonymousClass4Xy r9 = r8.A04;
            r9.A00();
            C615730w r4 = new C615730w();
            if (!r8.A00) {
                boolean z = r8.A01;
                long j3 = 0;
                if (z) {
                    j2 = 0;
                } else {
                    j2 = r9.A00;
                }
                r4.A02 = Long.valueOf(j2);
                r4.A03 = Long.valueOf(Math.round(((double) j) / 10000.0d) * 10000);
                if (z) {
                    j3 = r8.A05.A00;
                }
                r4.A04 = Long.valueOf(j3);
                r4.A00 = Boolean.valueOf(z);
                r4.A05 = Long.valueOf(r8.A03.A00);
                r4.A06 = Long.valueOf(Math.round(((double) r7.A00) / 10000.0d) * 10000);
                switch (i2) {
                    case 1:
                        i = 5;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 2:
                        i = 1;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 3:
                        i = 3;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 4:
                        i = 4;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 5:
                        i = 2;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 6:
                        i = 7;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 7:
                        i = 6;
                        valueOf = Integer.valueOf(i);
                        break;
                    case 8:
                        i = 8;
                        valueOf = Integer.valueOf(i);
                        break;
                    default:
                        valueOf = null;
                        break;
                }
                r4.A01 = valueOf;
                r8.A02.A07(r4);
            }
            r8.A00 = false;
            r8.A01 = false;
            r8.A05.A01();
            r9.A01();
            r7.A01();
            r8.A03.A01();
            this.A02 = 3;
            AnonymousClass4NS r2 = this.A0A;
            if (!(r2 == null || (r1 = this.A0B) == null)) {
                if (r1.equals(r2.A01.A0z)) {
                    r2.A00.A0A.A06(0.0f, 1.0f, 0.0f, 0.0f);
                }
                this.A0A = null;
            }
            AnonymousClass2G9 r02 = this.A0D;
            if (r02 != null) {
                r02.A02();
            }
            AnonymousClass21T r03 = this.A0E;
            if (r03 != null) {
                r03.A08();
                this.A0E = null;
            }
            this.A0C.setSystemUiVisibility(0);
            ScaleGestureDetector$OnScaleGestureListenerC52942c4 r12 = this.A0C;
            r12.A0Q = false;
            r12.A0N = false;
            r12.A0L = true;
            r12.A09 = 0;
            r12.A0A = 0;
            r12.removeAllViews();
            this.A0G = false;
            this.A0H = false;
            this.A0B = null;
            this.A0F = null;
        }
    }

    @Override // X.AbstractC35401hl
    public void A9X() {
        Context context = this.A0I;
        if (!AnonymousClass12P.A00(context).isFinishing()) {
            AnonymousClass21T r0 = this.A0E;
            if (r0 != null) {
                View A04 = r0.A04();
                ViewGroup.LayoutParams layoutParams = A04.getLayoutParams();
                layoutParams.height = -1;
                layoutParams.width = -1;
                A04.setLayoutParams(layoutParams);
                if (this.A0E instanceof AnonymousClass39H) {
                    int i = context.getResources().getConfiguration().orientation;
                    YoutubePlayerTouchOverlay youtubePlayerTouchOverlay = ((AnonymousClass39H) this.A0E).A0E;
                    if (i == 2) {
                        youtubePlayerTouchOverlay.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                    } else {
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, youtubePlayerTouchOverlay.A00);
                        layoutParams2.addRule(13);
                        youtubePlayerTouchOverlay.setLayoutParams(layoutParams2);
                    }
                    youtubePlayerTouchOverlay.requestLayout();
                }
            }
            this.A08.setContentDescription(context.getString(R.string.inline_video_fullscreen));
            ScaleGestureDetector$OnScaleGestureListenerC52942c4 r1 = this.A0C;
            r1.A0L = false;
            r1.A0Q = false;
            r1.A0N = true;
            r1.A0M = false;
            r1.A09(1.0f);
            ScaleGestureDetector$OnScaleGestureListenerC52942c4 r3 = this.A0C;
            FrameLayout frameLayout = this.A08;
            if (!(frameLayout.getX() == 0.0f || frameLayout.getY() == 0.0f)) {
                r3.A09 = r3.A04(r3.A03);
                r3.A0A = r3.A05(r3.A02);
            }
            AnonymousClass028.A0R(AnonymousClass12P.A00(context).getWindow().getDecorView());
            this.A08.setScaleX(1.0f);
            this.A08.setScaleY(1.0f);
            Conversation conversation = this.A09.A00;
            if (C252718t.A00(conversation.A09)) {
                conversation.A2k();
            } else {
                conversation.A3N();
            }
            FrameLayout frameLayout2 = this.A08;
            ScaleGestureDetector$OnScaleGestureListenerC52942c4 r02 = this.A0C;
            Rect rect = new Rect();
            Rect rect2 = new Rect();
            Point point = new Point();
            Point point2 = new Point();
            frameLayout2.getGlobalVisibleRect(rect, point2);
            r02.getGlobalVisibleRect(rect2, point);
            rect.offset(point2.x - rect.left, point2.y - rect.top);
            rect2.offset(-point.x, -point.y);
            this.A0J.set(rect);
            frameLayout2.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            A06(rect, rect2, frameLayout2);
            this.A0H = true;
            AnonymousClass39B r32 = (AnonymousClass39B) this.A0D;
            if (r32.A06 != null) {
                r32.A0L.setVisibility(8);
                r32.A0G();
                r32.A0N.setVisibility(0);
            }
            r32.A0c.setVisibility(8);
            r32.A0B = true;
            r32.A0H();
            if (r32.A0K()) {
                if (r32.A0l) {
                    r32.A0a.setVisibility(0);
                } else {
                    r32.A0W.setVisibility(0);
                }
                r32.A0F();
            }
            this.A0C.requestLayout();
            FrameLayout frameLayout3 = this.A08;
            if (frameLayout3 instanceof C865247s) {
                ((C865247s) frameLayout3).setIsFullscreen(this.A0H);
            }
        }
    }

    @Override // X.AbstractC35401hl
    public void A9m(boolean z) {
        AnonymousClass21T r0 = this.A0E;
        if (r0 != null) {
            View A04 = r0.A04();
            ViewGroup.LayoutParams layoutParams = A04.getLayoutParams();
            layoutParams.height = this.A05;
            layoutParams.width = -1;
            A04.setLayoutParams(layoutParams);
            AnonymousClass21T r1 = this.A0E;
            if (r1 instanceof AnonymousClass39H) {
                YoutubePlayerTouchOverlay youtubePlayerTouchOverlay = ((AnonymousClass39H) r1).A0E;
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, youtubePlayerTouchOverlay.A00);
                layoutParams2.addRule(13);
                youtubePlayerTouchOverlay.setLayoutParams(layoutParams2);
                youtubePlayerTouchOverlay.requestLayout();
            }
        }
        FrameLayout frameLayout = this.A08;
        Context context = this.A0I;
        frameLayout.setContentDescription(context.getString(R.string.inline_video_pip));
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r12 = this.A0C;
        boolean z2 = true;
        r12.A0L = true;
        r12.A0Q = false;
        r12.A09(r12.A00);
        if (z || this.A03 != this.A01) {
            this.A08.setLayoutParams(new FrameLayout.LayoutParams(this.A07, this.A04));
        } else {
            FrameLayout frameLayout2 = this.A08;
            ScaleGestureDetector$OnScaleGestureListenerC52942c4 r02 = this.A0C;
            Rect rect = new Rect();
            Rect rect2 = new Rect();
            Point point = new Point();
            r02.getGlobalVisibleRect(rect, point);
            rect.offset(-point.x, -point.y);
            rect2.set(this.A0J);
            frameLayout2.setLayoutParams(new FrameLayout.LayoutParams(this.A07, this.A04));
            A06(rect, rect2, frameLayout2);
        }
        this.A0H = false;
        AnonymousClass39B r2 = (AnonymousClass39B) this.A0D;
        r2.A0N.setVisibility(8);
        r2.A0L.setVisibility(0);
        r2.A0c.setVisibility(0);
        r2.A0B = false;
        r2.A0H();
        if (r2.A0K()) {
            r2.A0F();
            if (r2.A0l) {
                r2.A0a.setVisibility(0);
            } else {
                r2.A0W.setVisibility(0);
            }
        }
        this.A0D.setSystemUiVisibility(0);
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r22 = this.A0C;
        r22.A0M = true;
        if (this.A03 != this.A01) {
            z2 = false;
        }
        r22.A0A(z2);
        this.A0C.A0N = false;
        AnonymousClass028.A0R(AnonymousClass12P.A00(context).getWindow().getDecorView());
        this.A03 = this.A01;
        FrameLayout frameLayout3 = this.A08;
        if (frameLayout3 instanceof C865247s) {
            ((C865247s) frameLayout3).setIsFullscreen(this.A0H);
        }
    }

    @Override // X.AbstractC35401hl
    public void A9r(AnonymousClass4NS r20, AnonymousClass1IS r21, String str, String str2, Bitmap[] bitmapArr, int i) {
        C14320lF r0;
        if (this.A0B != r21) {
            A7N();
            this.A0B = r21;
            this.A0F = str2;
            this.A0A = r20;
            this.A06 = i;
        }
        if (i != 3) {
            str = str2;
        }
        String obj = Uri.parse(str).buildUpon().appendQueryParameter("wa_logging_event", "video_play_open").build().toString();
        C14900mE r13 = this.A0M;
        AbstractC14440lR r4 = this.A0T;
        C18790t3 r14 = this.A0O;
        AnonymousClass018 r3 = this.A0R;
        if (i == 4) {
            if (str2 != null && r21 != null && bitmapArr != null) {
                A07(r21, new AnonymousClass3JH(-1, str2, -1), null, bitmapArr, 4);
            }
        } else if (obj == null || (r0 = (C14320lF) C49132Jk.A00.get(obj)) == null) {
            try {
                AnonymousClass4NS r1 = this.A0A;
                if (r1 != null) {
                    if (r21.equals(r1.A01.A0z)) {
                        r1.A00.A0A.A06(1.0f, 0.0f, 0.0f, 0.0f);
                    }
                    this.A02 = 1;
                }
                C49132Jk.A00(r13, r14, new AnonymousClass536(r21, this, bitmapArr), r3, r4, obj);
            } catch (Exception unused) {
                A08("InlineVideoPlaybackImplHandler/fetchPageInfo - loadPage failed", true);
            }
        } else if (r21 != null && bitmapArr != null) {
            A07(r21, r0.A07, A04(r0), bitmapArr, i);
        }
    }

    @Override // X.AbstractC35401hl
    public int AC4() {
        return this.A02;
    }

    @Override // X.AbstractC35401hl
    public AnonymousClass1IS AC5() {
        return this.A0B;
    }

    @Override // X.AbstractC35401hl
    public boolean ADU() {
        return this.A0G;
    }

    @Override // X.AbstractC35401hl
    public boolean ADc() {
        return this.A0H;
    }

    @Override // X.AbstractC35401hl
    public void AYz() {
        AnonymousClass21T r0 = this.A0E;
        if (r0 != null && r0.A0B()) {
            this.A0D.A00();
        }
    }

    @Override // X.AbstractC35401hl
    public void Ac0(int i) {
        this.A01 = i;
    }

    @Override // X.AbstractC35401hl
    public void AcB(AnonymousClass4NS r1) {
        this.A0A = r1;
    }

    @Override // X.AbstractC35401hl
    public void AcP(int i) {
        this.A03 = i;
    }

    @Override // X.AbstractC35401hl
    public void AeJ(C89114Is r6, ScaleGestureDetector$OnScaleGestureListenerC52942c4 r7) {
        int height;
        this.A0C = r7;
        this.A09 = r6;
        Context context = this.A0I;
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.inline_video_player_padding);
        if (context.getResources().getConfiguration().orientation == 1) {
            height = r7.getWidth();
        } else {
            height = r7.getHeight();
        }
        int i = height - (dimensionPixelSize << 1);
        this.A00 = (double) (((i * i) * 9) >> 4);
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r3 = this.A0C;
        int[] viewIdsToIgnoreScaling = AnonymousClass2G9.getViewIdsToIgnoreScaling();
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(R.dimen.inline_controls_padding);
        r3.A0R = viewIdsToIgnoreScaling;
        r3.A06 = dimensionPixelSize2;
    }
}
