package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: X.3hn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74323hn extends AnonymousClass04v {
    public final /* synthetic */ C53042cM A00;

    public C74323hn(C53042cM r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A02(View view, AccessibilityEvent accessibilityEvent) {
        view.setLongClickable(false);
    }
}
