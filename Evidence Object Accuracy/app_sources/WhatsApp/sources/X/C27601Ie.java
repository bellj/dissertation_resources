package X;

/* renamed from: X.1Ie  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27601Ie extends AbstractC16110oT {
    public String A00;

    public C27601Ie() {
        super(1942, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAdvertisingId {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidAdvertisingId", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
