package X;

import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1It  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27691It extends AnonymousClass016 {
    public final AtomicBoolean A00 = new AtomicBoolean(false);

    @Override // X.AnonymousClass017
    public void A05(AbstractC001200n r3, AnonymousClass02B r4) {
        if (super.A00 > 0) {
            throw new IllegalStateException("Can not register multiple observers for a *Single*LiveEvent");
        }
        super.A05(r3, new AnonymousClass02B(r4, this) { // from class: X.2Vx
            public final /* synthetic */ AnonymousClass02B A00;
            public final /* synthetic */ C27691It A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C27691It r0 = this.A01;
                AnonymousClass02B r32 = this.A00;
                if (r0.A00.compareAndSet(true, false)) {
                    r32.ANq(obj);
                }
            }
        });
    }

    @Override // X.AnonymousClass017
    public void A0B(Object obj) {
        this.A00.set(true);
        super.A0B(obj);
    }
}
