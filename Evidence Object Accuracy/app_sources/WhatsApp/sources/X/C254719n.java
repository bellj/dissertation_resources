package X;

import android.text.TextUtils;

/* renamed from: X.19n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254719n {
    public final AnonymousClass19R A00;
    public final C14850m9 A01;

    public C254719n(AnonymousClass19R r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean A00(C30141Wg r4) {
        if (this.A01.A07(355) && r4 != null) {
            String str = r4.A0B;
            if (!TextUtils.isEmpty(str)) {
                return this.A00.A01(str);
            }
        }
        return false;
    }

    public boolean A01(C30141Wg r3) {
        C14850m9 r1 = this.A01;
        return r1.A07(355) && r1.A07(636) && A00(r3);
    }
}
