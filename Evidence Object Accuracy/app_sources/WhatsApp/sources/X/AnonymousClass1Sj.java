package X;

import java.io.File;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1Sj  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Sj implements AbstractC29401Sk {
    public final CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();

    @Override // X.AbstractC29401Sk
    public boolean A72(C29441Sr r3, File file) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            if (!((AbstractC29401Sk) it.next()).A72(r3, file)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AbstractC29401Sk
    public void AUP(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AUP(r3);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AUQ(C29441Sr r3, int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AUQ(r3, i);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AXc(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AXc(r3);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AXd(int i, int i2, int i3, int i4) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AXd(i, i2, i3, i4);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AXe(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AXe(r3);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AXf(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AXf(r3);
        }
    }

    @Override // X.AbstractC29401Sk
    public void AXg(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29401Sk) it.next()).AXg(r3);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXh(C29441Sr r3, int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29391Sf) it.next()).AXh(r3, i);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXi(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29391Sf) it.next()).AXi(r3);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXj(C29441Sr r3, Throwable th) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29391Sf) it.next()).AXj(r3, th);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXk(C29441Sr r3) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AbstractC29391Sf) it.next()).AXk(r3);
        }
    }
}
