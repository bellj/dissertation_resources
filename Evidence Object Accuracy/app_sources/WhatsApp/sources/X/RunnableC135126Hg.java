package X;

/* renamed from: X.6Hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135126Hg implements Runnable {
    public final /* synthetic */ C119025cl A00;
    public final /* synthetic */ Exception A01;

    public RunnableC135126Hg(C119025cl r1, Exception exc) {
        this.A00 = r1;
        this.A01 = exc;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A00.A00(this.A01);
    }
}
