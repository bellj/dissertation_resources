package X;

import java.util.List;

/* renamed from: X.3Hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64973Hq {
    public static int A00(List list, int i) {
        if (list == null || list.isEmpty()) {
            return i;
        }
        int i2 = 0;
        for (Object obj : list) {
            String obj2 = obj.toString();
            switch (obj2.hashCode()) {
                case -1699597560:
                    if (obj2.equals("bottom_right")) {
                        i2 |= 4;
                        break;
                    } else {
                        throw new AnonymousClass491(C12960it.A0b("Can't parse corner: ", obj));
                    }
                case -966253391:
                    if (obj2.equals("top_left")) {
                        i2 |= 1;
                        break;
                    } else {
                        throw new AnonymousClass491(C12960it.A0b("Can't parse corner: ", obj));
                    }
                case -609197669:
                    if (obj2.equals("bottom_left")) {
                        i2 |= 8;
                        break;
                    } else {
                        throw new AnonymousClass491(C12960it.A0b("Can't parse corner: ", obj));
                    }
                case 116576946:
                    if (obj2.equals("top_right")) {
                        i2 |= 2;
                        break;
                    } else {
                        throw new AnonymousClass491(C12960it.A0b("Can't parse corner: ", obj));
                    }
                default:
                    throw new AnonymousClass491(C12960it.A0b("Can't parse corner: ", obj));
            }
        }
        return i2;
    }

    public static void A01(float[] fArr, float f, int i) {
        float f2 = 0.0f;
        if ((1 & i) != 0) {
            f2 = f;
        }
        float f3 = 0.0f;
        if ((i & 2) != 0) {
            f3 = f;
        }
        float f4 = 0.0f;
        if ((i & 4) != 0) {
            f4 = f;
        }
        if ((i & 8) == 0) {
            f = 0.0f;
        }
        fArr[1] = f2;
        fArr[0] = f2;
        fArr[3] = f3;
        fArr[2] = f3;
        fArr[5] = f4;
        fArr[4] = f4;
        fArr[7] = f;
        fArr[6] = f;
    }

    public static boolean A02(int i) {
        if (i != 0) {
            return ((1 & i) == 0 || (i & 2) == 0 || (i & 4) == 0 || (i & 8) == 0) ? false : true;
        }
        return true;
    }
}
