package X;

import android.view.ViewTreeObserver;
import com.whatsapp.PagerSlidingTabStrip;

/* renamed from: X.2QT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QT implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ PagerSlidingTabStrip A00;

    public AnonymousClass2QT(PagerSlidingTabStrip pagerSlidingTabStrip) {
        this.A00 = pagerSlidingTabStrip;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        PagerSlidingTabStrip pagerSlidingTabStrip = this.A00;
        pagerSlidingTabStrip.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        int currentItem = pagerSlidingTabStrip.A0N.getCurrentItem();
        pagerSlidingTabStrip.A01 = currentItem;
        PagerSlidingTabStrip.A00(pagerSlidingTabStrip, currentItem, 0);
    }
}
