package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.biz.catalog.view.PostcodeChangeBottomSheet;

/* renamed from: X.3MB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MB implements TextWatcher {
    public final /* synthetic */ PostcodeChangeBottomSheet A00;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass3MB(PostcodeChangeBottomSheet postcodeChangeBottomSheet) {
        this.A00 = postcodeChangeBottomSheet;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        int length = editable.toString().length();
        PostcodeChangeBottomSheet postcodeChangeBottomSheet = this.A00;
        if (length == 6) {
            postcodeChangeBottomSheet.A03.setVisibility(0);
            postcodeChangeBottomSheet.A05.setVisibility(8);
            postcodeChangeBottomSheet.A04.getBackground().clearColorFilter();
            return;
        }
        postcodeChangeBottomSheet.A1N();
    }
}
