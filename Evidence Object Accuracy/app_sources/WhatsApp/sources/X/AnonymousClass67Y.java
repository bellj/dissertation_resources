package X;

import java.io.InputStream;

/* renamed from: X.67Y  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass67Y implements AbstractC14500lX {
    public final /* synthetic */ C1309560q A00;
    public final /* synthetic */ C127675us A01;
    public final /* synthetic */ C120025fU A02;
    public final /* synthetic */ byte[] A03;

    public AnonymousClass67Y(C1309560q r1, C127675us r2, C120025fU r3, byte[] bArr) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = bArr;
    }

    @Override // X.AbstractC14500lX
    public AbstractC37941nG ACi(byte[] bArr) {
        return new AbstractC37941nG(ADk().A8r(bArr), this.A01, this.A02, this.A03) { // from class: X.67X
            public final /* synthetic */ C37891nB A00;
            public final /* synthetic */ C127675us A01;
            public final /* synthetic */ C120025fU A02;
            public final /* synthetic */ byte[] A03;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
                this.A03 = r4;
            }

            @Override // X.AbstractC37941nG
            public final InputStream A9M(InputStream inputStream) {
                C127675us r2 = this.A01;
                C120025fU r1 = this.A02;
                C37891nB r0 = this.A00;
                return new C124475pb(inputStream, r2.A03, r1.A00, r0.A00, r0.A01, this.A03);
            }
        };
    }

    @Override // X.AbstractC14500lX
    public AnonymousClass2QI ADk() {
        return new AnonymousClass67W(this.A02.A00);
    }
}
