package X;

import java.util.concurrent.Executor;

/* renamed from: X.1qM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class ExecutorC39691qM implements Executor {
    public final /* synthetic */ C14900mE A00;

    public /* synthetic */ ExecutorC39691qM(C14900mE r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.A00.A0I(runnable);
    }
}
