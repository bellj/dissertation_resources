package X;

import java.util.ArrayList;
import java.util.Collection;

/* renamed from: X.0pi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16760pi extends C16770pj {
    public static final int A0D(Iterable iterable) {
        C16700pc.A0E(iterable, 0);
        if (iterable instanceof Collection) {
            return ((Collection) iterable).size();
        }
        return 10;
    }

    public static ArrayList A0E(Iterable iterable) {
        return new ArrayList(A0D(iterable));
    }
}
