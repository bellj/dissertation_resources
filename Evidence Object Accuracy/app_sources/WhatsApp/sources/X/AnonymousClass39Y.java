package X;

import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.39Y  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass39Y extends AbstractC120015fT {
    public final C18160s0 A00;

    @Override // X.AbstractC120015fT
    public String A03() {
        return "version";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass39Y(C18790t3 r17, C14820m6 r18, C14850m9 r19, C18160s0 r20, AnonymousClass18L r21, AnonymousClass01H r22, String str, String str2, String str3, Map map, AnonymousClass01N r27, AnonymousClass01N r28, long j) {
        super(r17, r18, r19, r21, r22, str3, str, str2, map, r27, r28, j);
        C16700pc.A0E(r19, 1);
        C16700pc.A0J(r17, r21, r18, r22, r27);
        C16700pc.A0E(r28, 7);
        C16700pc.A0E(str, 9);
        C16700pc.A0E(r20, 13);
        this.A00 = r20;
    }

    @Override // X.AnonymousClass19V
    public String A00() {
        return this.A00.A01();
    }

    @Override // X.AbstractC120015fT
    public void A04(JSONObject jSONObject) {
        super.A04(jSONObject);
        jSONObject.put("tos_version", "1");
    }
}
