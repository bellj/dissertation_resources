package X;

import android.graphics.Point;
import android.os.Build;
import android.view.View;
import android.widget.ListView;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;

/* renamed from: X.3FL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FL {
    public int A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass1OY A04;
    public final /* synthetic */ MediaAlbumActivity A05;

    public /* synthetic */ AnonymousClass3FL(MediaAlbumActivity mediaAlbumActivity) {
        this.A05 = mediaAlbumActivity;
    }

    public int A00(int i, int i2, boolean z) {
        int i3;
        int identifier;
        MediaAlbumActivity mediaAlbumActivity = this.A05;
        int i4 = -mediaAlbumActivity.getResources().getDimensionPixelSize(R.dimen.album_item_padding);
        if (Build.VERSION.SDK_INT < 21 || (identifier = mediaAlbumActivity.getResources().getIdentifier("status_bar_height", "dimen", "android")) <= 0) {
            i3 = 0;
        } else {
            i3 = mediaAlbumActivity.getResources().getDimensionPixelSize(identifier);
        }
        int max = Math.max(i4, (((i3 + i) + mediaAlbumActivity.getResources().getDimensionPixelSize(R.dimen.actionbar_height)) - i2) >> 1);
        return z ? Math.max(max, i - i2) : max;
    }

    public void A01() {
        int top;
        AnonymousClass1OY r2 = this.A04;
        if (r2 != null && this.A01 < this.A02) {
            if (!(this.A03 == 0 || r2.getTop() == this.A03)) {
                int A05 = this.A01 + (C12980iv.A05(this.A04.getTop(), this.A03) << 1);
                this.A01 = A05;
                AnonymousClass1OY r0 = this.A04;
                r0.A01 = A05;
                r0.requestLayout();
                if (this.A00 != 0) {
                    Point point = new Point();
                    MediaAlbumActivity mediaAlbumActivity = this.A05;
                    C12970iu.A17(mediaAlbumActivity, point);
                    int i = point.y;
                    int i2 = this.A01;
                    boolean z = true;
                    if (this.A00 != mediaAlbumActivity.A08.getCount() - 1) {
                        z = false;
                    }
                    top = A00(i, i2, z);
                    ListView A2e = mediaAlbumActivity.A2e();
                    AnonymousClass009.A03(A2e);
                    A2e.setSelectionFromTop(this.A00 + A2e.getHeaderViewsCount(), top);
                    this.A03 = top;
                }
            }
            top = this.A04.getTop();
            this.A03 = top;
        }
    }

    public void A02(C35421hp r8, int i) {
        Point point = new Point();
        MediaAlbumActivity mediaAlbumActivity = this.A05;
        C12970iu.A17(mediaAlbumActivity, point);
        int i2 = point.y;
        int i3 = point.x;
        ListView A2e = mediaAlbumActivity.A2e();
        AnonymousClass009.A03(A2e);
        int i4 = 0;
        boolean z = false;
        if (i2 >= i3) {
            View view = r8.getView(i, null, A2e);
            view.measure(View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), 0);
            this.A01 = (i2 << 2) / 5;
            int measuredHeight = view.getMeasuredHeight();
            this.A02 = measuredHeight;
            int i5 = this.A01;
            if (i5 < measuredHeight) {
                this.A00 = i;
            } else {
                this.A00 = -1;
            }
            if (i != 0) {
                int min = Math.min(measuredHeight, i5);
                if (i == r8.getCount() - 1) {
                    z = true;
                }
                this.A03 = A00(i2, min, z);
                A2e.setSelectionFromTop(i + A2e.getHeaderViewsCount(), this.A03);
                return;
            }
            this.A03 = 0;
            return;
        }
        int headerViewsCount = i + A2e.getHeaderViewsCount();
        if (Build.VERSION.SDK_INT >= 21) {
            int identifier = mediaAlbumActivity.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (identifier > 0) {
                i4 = mediaAlbumActivity.getResources().getDimensionPixelSize(identifier);
            } else {
                i4 = 0;
            }
        }
        A2e.setSelectionFromTop(headerViewsCount, i4 + mediaAlbumActivity.getResources().getDimensionPixelSize(R.dimen.actionbar_height));
    }
}
