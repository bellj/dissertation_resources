package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;

/* renamed from: X.3EJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EJ {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EW A01;
    public final AnonymousClass3EL A02;

    public AnonymousClass3EJ(AbstractC15710nm r6, AnonymousClass1V8 r7, AnonymousClass3BH r8) {
        AnonymousClass1V8.A01(r7, "iq");
        AnonymousClass1V8 r4 = r8.A00;
        IDxNFunctionShape17S0100000_2_I1 iDxNFunctionShape17S0100000_2_I1 = new IDxNFunctionShape17S0100000_2_I1(r6, 21);
        String[] A08 = C13000ix.A08();
        A08[0] = "error";
        this.A01 = (AnonymousClass3EW) AnonymousClass3JT.A05(r7, iDxNFunctionShape17S0100000_2_I1, A08);
        this.A02 = (AnonymousClass3EL) AnonymousClass3JT.A05(r7, new AbstractC116095Uc(r6, r4) { // from class: X.58v
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r42) {
                return new AnonymousClass3EL(this.A00, r42, this.A01);
            }
        }, new String[0]);
        this.A00 = r7;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EJ.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EJ r5 = (AnonymousClass3EJ) obj;
            if (!this.A01.equals(r5.A01) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A02, A1a);
    }
}
