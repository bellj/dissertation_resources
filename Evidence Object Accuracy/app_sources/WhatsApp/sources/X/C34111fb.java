package X;

import java.util.Collection;

/* renamed from: X.1fb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34111fb extends AbstractC34011fR {
    public final int A00;
    public final AbstractC14640lm A01;
    public final C22230yk A02;
    public final Collection A03;

    public C34111fb(AbstractC14640lm r1, C22230yk r2, Collection collection, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = collection;
        this.A00 = i;
    }
}
