package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1sy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41141sy {
    public String A00;
    public byte[] A01;
    public final List A02;
    public final List A03;

    public C41141sy(AnonymousClass1V8 r3) {
        this(r3.A00);
        AnonymousClass1V8[] r0 = r3.A03;
        if (r0 != null) {
            this.A03.addAll(Arrays.asList(r0));
        }
        AnonymousClass1W9[] A0L = r3.A0L();
        if (A0L != null) {
            this.A02.addAll(Arrays.asList(A0L));
        }
        this.A01 = r3.A01;
    }

    public C41141sy(String str) {
        this.A03 = new ArrayList();
        this.A02 = new ArrayList();
        this.A00 = str;
    }

    public static C41141sy A00() {
        C41141sy r3 = new C41141sy("iq");
        r3.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        return r3;
    }

    public static void A01(C41141sy r1, String str, String str2) {
        r1.A04(new AnonymousClass1W9(str, str2));
    }

    public static void A02(C41141sy r8, String str, String str2) {
        r8.A04(new AnonymousClass1W9("type", str));
        if (AnonymousClass3JT.A0E(str2, 0, 9007199254740991L, false)) {
            r8.A04(new AnonymousClass1W9("id", str2));
        }
    }

    public AnonymousClass1V8 A03() {
        AnonymousClass1V8[] r2;
        List list = this.A02;
        AnonymousClass1W9[] r3 = null;
        if (!list.isEmpty()) {
            r3 = (AnonymousClass1W9[]) list.toArray(new AnonymousClass1W9[0]);
        }
        List list2 = this.A03;
        if (list2.isEmpty() || (r2 = (AnonymousClass1V8[]) list2.toArray(new AnonymousClass1V8[0])) == null) {
            return new AnonymousClass1V8(this.A00, this.A01, r3);
        }
        return new AnonymousClass1V8(this.A00, r3, r2);
    }

    public void A04(AnonymousClass1W9 r2) {
        this.A02.add(r2);
    }

    public void A05(AnonymousClass1V8 r2) {
        if (r2 != null) {
            this.A03.add(r2);
        }
    }

    public final void A06(AnonymousClass1V8 r5) {
        String str = this.A00;
        if ("smax:any".equals(str)) {
            str = r5.A00;
            this.A00 = str;
        }
        String str2 = r5.A00;
        if (!str.equals(str2) && !"smax:any".equals(str2)) {
            AnonymousClass009.A07(String.format("Error merging <%s/> with <%s/>: conflicting tags", str, str2));
        }
    }

    public void A07(AnonymousClass1V8 r12, List list) {
        int length;
        A06(r12);
        AnonymousClass1W9[] A0L = r12.A0L();
        if (A0L != null) {
            for (AnonymousClass1W9 r7 : A0L) {
                String str = r7.A02;
                List list2 = this.A02;
                Iterator it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        list2.add(r7);
                        break;
                    }
                    AnonymousClass1W9 r1 = (AnonymousClass1W9) it.next();
                    if (r1.A02.equals(str)) {
                        if (!r7.equals(r1)) {
                            AnonymousClass009.A07(String.format("Error merging attribute '%s' in <%s/>: conflicting values", r7.A02, this.A00));
                        }
                    }
                }
            }
        }
        AnonymousClass1V8[] r2 = r12.A03;
        if (!(r2 == null || (length = r2.length) == 0)) {
            int i = 0;
            if (this.A01 != null) {
                AnonymousClass009.A07(String.format("Error merging children into <%s/>: element already has data", this.A00));
            }
            List<AnonymousClass1V8> list3 = this.A03;
            if (!list3.isEmpty()) {
                C103894rU r13 = C103894rU.A00;
                Map A00 = C37871n9.A00(r13, list3);
                Map A002 = C37871n9.A00(r13, Arrays.asList(r2));
                A0B(list, A00, A002);
                ArrayList arrayList = new ArrayList();
                for (AnonymousClass1V8 r9 : list3) {
                    String str2 = r9.A00;
                    if (A002.containsKey(str2) && !list.contains(Arrays.asList(str2))) {
                        C41141sy r14 = new C41141sy(r9);
                        r14.A07((AnonymousClass1V8) ((List) A002.get(str2)).remove(0), r9.A0K(list));
                        r9 = r14.A03();
                    }
                    arrayList.add(r9);
                }
                for (Map.Entry entry : A002.entrySet()) {
                    if (!A00.containsKey(entry.getKey()) && !list.contains(entry.getKey())) {
                        arrayList.addAll((Collection) entry.getValue());
                    }
                }
                list3.clear();
                list3.addAll(arrayList);
            } else {
                do {
                    AnonymousClass1V8 r15 = r2[i];
                    if (!list.contains(r15.A00)) {
                        list3.add(r15);
                    }
                    i++;
                } while (i < length);
            }
        }
        byte[] bArr = r12.A01;
        if (bArr != null) {
            if (!this.A03.isEmpty()) {
                AnonymousClass009.A07(String.format("Error merging data into <%s/>: element already has children", this.A00));
            }
            byte[] bArr2 = this.A01;
            if (bArr2 != null && !Arrays.equals(bArr2, bArr)) {
                AnonymousClass009.A07(String.format("Error merging data into <%s/>: conflicting values", this.A00));
            }
            this.A01 = bArr;
        }
    }

    public void A08(AnonymousClass1V8 r11, List list) {
        C41141sy r2;
        A06(r11);
        AnonymousClass1V8[] r3 = r11.A03;
        if (!(r3 == null || r3.length == 0)) {
            if (this.A01 != null) {
                AnonymousClass009.A07(String.format("Error merging children into <%s/>: element already has data", this.A00));
            }
            List<AnonymousClass1V8> list2 = this.A03;
            C103894rU r22 = C103894rU.A00;
            Map A00 = C37871n9.A00(r22, list2);
            Map A002 = C37871n9.A00(r22, Arrays.asList(r3));
            A0B(list, A00, A002);
            ArrayList arrayList = new ArrayList();
            for (AnonymousClass1V8 r8 : list2) {
                String str = r8.A00;
                if (A002.containsKey(str)) {
                    boolean contains = list.contains(Arrays.asList(str));
                    List list3 = (List) A002.get(str);
                    if (contains) {
                        List emptyList = Collections.emptyList();
                        r2 = new C41141sy(r8);
                        r2.A07((AnonymousClass1V8) list3.get(0), r8.A0K(emptyList));
                    } else {
                        r2 = new C41141sy(r8);
                        r2.A08((AnonymousClass1V8) list3.remove(0), r8.A0K(list));
                    }
                    r8 = r2.A03();
                }
                arrayList.add(r8);
            }
            list2.clear();
            list2.addAll(arrayList);
        }
    }

    public void A09(AnonymousClass1V8 r6, List list, List list2) {
        if (list.size() == 0) {
            A08(r6, list2);
            return;
        }
        int i = 0;
        Object remove = list.remove(0);
        ArrayList arrayList = new ArrayList();
        while (true) {
            List list3 = this.A03;
            if (i < list3.size()) {
                AnonymousClass1V8 r1 = (AnonymousClass1V8) list3.get(i);
                if (r1.A00.equals(remove)) {
                    C41141sy r0 = new C41141sy(r1);
                    r0.A09(r6, list, list2);
                    r1 = r0.A03();
                }
                arrayList.add(r1);
                i++;
            } else {
                return;
            }
        }
    }

    public void A0A(String str, String str2, List list) {
        if (!list.contains(str)) {
            AnonymousClass009.A07(String.format("String was expected to be one of '%s'.", TextUtils.join(", ", list)));
        }
        A04(new AnonymousClass1W9(str2, str));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public final void A0B(List list, Map map, Map map2) {
        for (Map.Entry entry : map.entrySet()) {
            if (map2.containsKey(entry.getKey()) && ((List) entry.getValue()).size() != ((List) map2.get(entry.getKey())).size() && !list.contains(Arrays.asList(entry.getKey()))) {
                AnonymousClass009.A07(String.format("Error merging children into <%s/>: conflicting child count for <%s/>", this.A00, entry.getKey()));
            }
        }
    }
}
