package X;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.SparseArray;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.bloks.components.BkCdsBottomSheetFragment;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1AI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AI implements AbstractC17450qp {
    public final AbstractC17450qp A00 = new C08800bs(new C67973Tp());
    public final AnonymousClass1AH A01;

    public AnonymousClass1AI(AnonymousClass1AH r3) {
        this.A01 = r3;
    }

    public static Activity A00(C14230l4 r1) {
        return (Activity) r1.A00.A02.A00().get(R.id.bloks_host_activity);
    }

    public static DialogFragment A01(String str, List list) {
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                return null;
            }
            AnonymousClass01E r3 = (AnonymousClass01E) list.get(size);
            if (!(r3 instanceof AbstractC12880ii) || !(r3 instanceof DialogFragment)) {
                DialogFragment A01 = A01(str, r3.A0E().A0U.A02());
                if (A01 != null) {
                    return A01;
                }
            } else {
                DialogFragment dialogFragment = (DialogFragment) r3;
                for (AnonymousClass0OS r0 : ((BkCdsBottomSheetFragment) ((AbstractC12880ii) dialogFragment)).A1I().A0B) {
                    if (str.equals(r0.A02)) {
                        return dialogFragment;
                    }
                }
                continue;
            }
        }
    }

    public static C14230l4 A02(Object obj) {
        if (obj instanceof C14230l4) {
            return (C14230l4) obj;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(obj.getClass().getSimpleName());
        sb.append("is not an instance of ");
        sb.append("BloksInterpreterEnvironment");
        throw new IllegalStateException(sb.toString());
    }

    public static Object A03(List list) {
        Object A03;
        int size = list.size();
        do {
            size--;
            if (size < 0) {
                return null;
            }
            AnonymousClass01E r1 = (AnonymousClass01E) list.get(size);
            if (AbstractC12880ii.class.isInstance(r1)) {
                return AbstractC12880ii.class.cast(r1);
            }
            A03 = A03(r1.A0E().A0U.A02());
        } while (A03 == null);
        return A03;
    }

    public static String A04(AnonymousClass28D r5) {
        List<AnonymousClass28D> emptyList;
        int i = r5.A01;
        boolean z = false;
        if (i == 13647) {
            z = true;
        }
        if (!z) {
            if (i == 13784) {
                Object obj = r5.A02.get(42);
                if (obj instanceof List) {
                    emptyList = (List) obj;
                } else {
                    emptyList = Collections.emptyList();
                }
                for (AnonymousClass28D r1 : emptyList) {
                    if (r1.A01 == 15855) {
                        return r1.A0I(40);
                    }
                }
            } else {
                throw new IllegalArgumentException("screen should be an instance of BloksScreenData or BloksScreenV2Data");
            }
        }
        return r5.A0I(35);
    }

    public static HashMap A05(Map map) {
        String str;
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            boolean z = entry.getValue() instanceof Number;
            Object key = entry.getKey();
            if (!z) {
                str = null;
                if (entry.getValue() == null) {
                    hashMap.put(key, str);
                }
            }
            str = entry.getValue().toString();
            hashMap.put(key, str);
        }
        return hashMap;
    }

    public static HashMap A06(Map map) {
        String obj;
        String obj2;
        String obj3;
        if (map == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            Object key = entry.getKey();
            boolean z = key instanceof Number;
            if (value == null) {
                if (z || key != null) {
                    obj = key.toString();
                } else {
                    obj = null;
                }
                hashMap.put(obj, null);
            } else {
                if (z || key != null) {
                    obj2 = key.toString();
                } else {
                    obj2 = null;
                }
                Object value2 = entry.getValue();
                if ((value2 instanceof Number) || value2 != null) {
                    obj3 = value2.toString();
                } else {
                    obj3 = null;
                }
                hashMap.put(obj2, obj3);
            }
        }
        return hashMap;
    }

    public static final void A07(C14260l7 r3, C14230l4 r4, AnonymousClass28D r5) {
        AbstractC14200l1 A0G = r5.A0G(35);
        if (A0G != null) {
            C14210l2 r1 = new C14210l2();
            r1.A05(r5, 0);
            r1.A05(r3, 1);
            C14250l6.A00(r4, new C14220l3(r1.A00), A0G);
        }
    }

    public static void A08(C64173En r5, AbstractC28681Oo r6) {
        AbstractC14200l1 AAN = r6.AAN();
        C14250l6.A00(C14230l4.A00(AnonymousClass3JV.A01(C65093Ic.A00().A00, new SparseArray(), null, r5, null), null), C14220l3.A01, AAN);
    }

    public final AlertDialog.Builder A09(C14260l7 r7, C14230l4 r8, AnonymousClass28D r9) {
        AlertDialog.Builder message = new AlertDialog.Builder(A00(r8)).setTitle(r9.A0I(40)).setMessage(r9.A0I(35));
        AnonymousClass28D A0F = r9.A0F(36);
        if (A0F != null) {
            message.setPositiveButton(A0F.A0J(36, ""), new DialogInterface.OnClickListener(r7, r8, A0F, this) { // from class: X.4h4
                public final /* synthetic */ C14260l7 A00;
                public final /* synthetic */ C14230l4 A01;
                public final /* synthetic */ AnonymousClass28D A02;
                public final /* synthetic */ AnonymousClass1AI A03;

                {
                    this.A03 = r4;
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    AnonymousClass1AI.A07(this.A00, this.A01, this.A02);
                }
            });
        }
        AnonymousClass28D A0F2 = r9.A0F(38);
        if (A0F2 != null) {
            message.setNegativeButton(A0F2.A0J(36, ""), new DialogInterface.OnClickListener(r7, r8, A0F2, this) { // from class: X.4h2
                public final /* synthetic */ C14260l7 A00;
                public final /* synthetic */ C14230l4 A01;
                public final /* synthetic */ AnonymousClass28D A02;
                public final /* synthetic */ AnonymousClass1AI A03;

                {
                    this.A03 = r4;
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    AnonymousClass1AI.A07(this.A00, this.A01, this.A02);
                }
            });
        }
        AnonymousClass28D A0F3 = r9.A0F(44);
        if (A0F3 != null) {
            message.setNeutralButton(A0F3.A0J(36, ""), new DialogInterface.OnClickListener(r7, r8, A0F3, this) { // from class: X.4h3
                public final /* synthetic */ C14260l7 A00;
                public final /* synthetic */ C14230l4 A01;
                public final /* synthetic */ AnonymousClass28D A02;
                public final /* synthetic */ AnonymousClass1AI A03;

                {
                    this.A03 = r4;
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    AnonymousClass1AI.A07(this.A00, this.A01, this.A02);
                }
            });
        }
        return message;
    }

    public final Object A0A(C14230l4 r10, C14220l3 r11, boolean z) {
        List list = r11.A00;
        String str = (String) list.get(0);
        C1093751l r1 = ((C94724cR) list.get(2)).A00;
        C1093751l r0 = ((C94724cR) list.get(3)).A00;
        AnonymousClass1AH r3 = this.A01;
        HashMap A05 = A05((Map) list.get(1));
        if (z) {
            r3.A6C(new AbstractC28681Oo() { // from class: X.54I
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AbstractC14200l1.this;
                }
            }, new AbstractC28681Oo() { // from class: X.54H
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AbstractC14200l1.this;
                }
            }, r10, str, A05);
            return null;
        }
        r3.A6B(new AbstractC28681Oo() { // from class: X.54F
            @Override // X.AbstractC28681Oo
            public final AbstractC14200l1 AAN() {
                return AbstractC14200l1.this;
            }
        }, new AbstractC28681Oo() { // from class: X.54J
            @Override // X.AbstractC28681Oo
            public final AbstractC14200l1 AAN() {
                return AbstractC14200l1.this;
            }
        }, r10, str, A05);
        return null;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:590:0x0e41 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v21, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v134, resolved type: java.lang.String[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v37, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v13, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v23 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x079e, code lost:
        if (r6.equals("a") == false) goto L_0x078d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x07a5, code lost:
        if (r6.equals("e") == false) goto L_0x078d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x07ae, code lost:
        if (r6.equals("i") == false) goto L_0x078d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x07c2, code lost:
        if (r6.equals("w") == false) goto L_0x078d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x081a, code lost:
        if ("e".equals("a") == false) goto L_0x07dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0821, code lost:
        if ("e".equals("e") == false) goto L_0x07dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x082a, code lost:
        if ("e".equals("i") == false) goto L_0x07dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x0834, code lost:
        if ("e".equals("w") == false) goto L_0x07dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x0837, code lost:
        com.whatsapp.util.Log.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x083c, code lost:
        com.whatsapp.util.Log.i(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x0841, code lost:
        com.whatsapp.util.Log.w(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x0ac5, code lost:
        if (r1 == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:444:0x0d5e, code lost:
        if ("e".equals("e") == false) goto L_0x0d32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:450:0x0d74, code lost:
        if ("e".equals(r6) != false) goto L_0x0d46;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:512:0x0eeb, code lost:
        if ("e".equals("e") == false) goto L_0x0e5f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:518:0x0f03, code lost:
        if ("e".equals(r6) != false) goto L_0x0e73;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:652:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:653:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:654:?, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x064a  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x078d  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0798  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x07a1  */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x07a8  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x07b1  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x07b4 A[PHI: r7 
      PHI: (r7v14 java.lang.String) = (r7v0 java.lang.String), (r7v15 java.lang.String) binds: [B:246:0x078a, B:254:0x07b1] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x07bb  */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0c52  */
    /* JADX WARNING: Removed duplicated region for block: B:413:0x0c57  */
    /* JADX WARNING: Removed duplicated region for block: B:615:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:647:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:651:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC17450qp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A9j(X.C14220l3 r22, X.C1093651k r23, X.C14240l5 r24) {
        /*
        // Method dump skipped, instructions count: 4692
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AI.A9j(X.0l3, X.51k, X.0l5):java.lang.Object");
    }
}
