package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.3IO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IO {
    public final C15570nT A00;
    public final C22830zi A01;
    public final C22130yZ A02;
    public final C22180yf A03;

    public AnonymousClass3IO(C15570nT r1, C22830zi r2, C22130yZ r3, C22180yf r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static C27081Fy A00(C27081Fy r5, int i) {
        AnonymousClass1G4 A0T = C27081Fy.A0i.A0T();
        A0T.A04(r5);
        AnonymousClass2n6 r0 = ((C27081Fy) A0T.A00).A0g;
        if (r0 == null) {
            r0 = AnonymousClass2n6.A05;
        }
        AnonymousClass1G4 A0T2 = r0.A0T();
        byte[] bArr = new byte[i];
        C002901h.A00().nextBytes(bArr);
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, i);
        AnonymousClass2n6 r1 = (AnonymousClass2n6) AnonymousClass1G4.A00(A0T2);
        r1.A00 |= 8;
        r1.A03 = A01;
        C27081Fy r2 = (C27081Fy) AnonymousClass1G4.A00(A0T);
        r2.A0g = (AnonymousClass2n6) A0T2.A02();
        r2.A00 |= 67108864;
        return (C27081Fy) A0T.A02();
    }

    public static C27081Fy A01(C27081Fy r5, String str, String str2) {
        AnonymousClass1G4 A0T = C57312mp.A04.A0T();
        if (r5 != null) {
            C57312mp r1 = (C57312mp) AnonymousClass1G4.A00(A0T);
            r1.A01 = r5;
            r1.A00 |= 2;
        }
        if (str != null) {
            C57312mp r12 = (C57312mp) AnonymousClass1G4.A00(A0T);
            r12.A00 |= 1;
            r12.A02 = str;
        }
        if (str2 != null) {
            C57312mp r13 = (C57312mp) AnonymousClass1G4.A00(A0T);
            r13.A00 |= 4;
            r13.A03 = str2;
        }
        AnonymousClass1G3 A00 = C27081Fy.A00();
        C27081Fy r2 = (C27081Fy) AnonymousClass1G4.A00(A00);
        r2.A0B = (C57312mp) A0T.A02();
        r2.A00 |= 33554432;
        return (C27081Fy) A00.A02();
    }

    public C27081Fy A02(DeviceJid deviceJid, C27081Fy r7, AnonymousClass1IS r8, String str) {
        String str2;
        C15570nT r4 = this.A00;
        if (!r4.A0F(deviceJid.getUserJid())) {
            return r7;
        }
        if (C15380n4.A0G(r8.A00)) {
            Set<DeviceJid> A00 = this.A01.A00(r8);
            HashSet A12 = C12970iu.A12();
            for (DeviceJid deviceJid2 : A00) {
                UserJid userJid = deviceJid2.getUserJid();
                if (!r4.A0F(userJid)) {
                    A12.add(userJid.getPrimaryDevice());
                }
            }
            str2 = AnonymousClass1YK.A00(A12);
        } else {
            str2 = null;
        }
        return A01(r7, str, str2);
    }
}
