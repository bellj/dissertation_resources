package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.5zI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130365zI {
    public static final List A00 = Collections.emptyList();

    public static List A00(List list) {
        if (list == null || list.isEmpty()) {
            return A00;
        }
        return C117305Zk.A0r(list);
    }
}
