package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.util.Log;

/* renamed from: X.2aU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52242aU extends ClickableSpan {
    public final /* synthetic */ CountDownTimerC51922Zr A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public C52242aU(CountDownTimerC51922Zr r1, String str, String str2) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = str2;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        RegisterPhone registerPhone = this.A00.A00;
        C22680zT r1 = registerPhone.A07;
        String str = this.A01;
        String str2 = this.A02;
        ((AbstractActivityC452520u) registerPhone).A09.A03.setText(AnonymousClass23M.A0B(r1, str, str2).substring(str.length() + 2));
        EditText editText = ((AbstractActivityC452520u) registerPhone).A09.A03;
        editText.setSelection(editText.getText().length());
        registerPhone.A2h();
        ((ActivityC13810kN) registerPhone).A05.A07(R.string.register_number_mistyped_toast, 1);
        Log.i(C12960it.A0d(str2, C12960it.A0k("register/phone/suggested/tapped ")));
        registerPhone.A0Y = true;
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setUnderlineText(false);
    }
}
