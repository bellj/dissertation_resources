package X;

import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: X.0of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16230of {
    public final AnonymousClass1I2 A00;

    public AbstractC16230of() {
        this(new C002601e(Collections.emptySet(), null));
    }

    public AbstractC16230of(AnonymousClass01H r2) {
        this.A00 = new AnonymousClass1I2(r2);
    }

    public static Iterator A00(AbstractC16230of r0) {
        return r0.A01().iterator();
    }

    public Iterable A01() {
        A02();
        AnonymousClass1I2 r1 = this.A00;
        synchronized (r1) {
        }
        return r1;
    }

    public void A02() {
        if (this instanceof AnonymousClass1I3) {
            AnonymousClass009.A00();
        } else if (this instanceof AbstractC16220oe) {
            AnonymousClass009.A01();
        }
    }

    public void A03(Object obj) {
        AnonymousClass009.A05(obj);
        A02();
        AnonymousClass1I2 r3 = this.A00;
        synchronized (r3) {
            AnonymousClass009.A05(obj);
            ConcurrentLinkedQueue concurrentLinkedQueue = r3.A01;
            if (concurrentLinkedQueue.contains(obj)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Observer ");
                sb.append(obj);
                sb.append(" is already registered.");
                String obj2 = sb.toString();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(getClass().getSimpleName());
                sb2.append(":");
                sb2.append(obj2);
                Log.e(sb2.toString());
            } else {
                AnonymousClass009.A05(obj);
                concurrentLinkedQueue.add(obj);
            }
        }
    }

    public void A04(Object obj) {
        AnonymousClass009.A05(obj);
        A02();
        AnonymousClass1I2 r2 = this.A00;
        synchronized (r2) {
            AnonymousClass009.A05(obj);
            if (!r2.A01.remove(obj)) {
                StringBuilder sb = new StringBuilder();
                sb.append(getClass().getSimpleName());
                sb.append(":Observer ");
                sb.append(obj);
                sb.append(" was not registered.");
                Log.e(sb.toString());
            }
        }
    }
}
