package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44C extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AnonymousClass34c A01;

    public AnonymousClass44C(SearchViewModel searchViewModel, AnonymousClass34c r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
