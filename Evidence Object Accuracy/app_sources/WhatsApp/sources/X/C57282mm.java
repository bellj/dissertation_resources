package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57282mm extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57282mm A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public long A01;
    public AbstractC27881Jp A02 = AbstractC27881Jp.A01;
    public C34311fw A03;

    static {
        C57282mm r0 = new C57282mm();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        C81723uT r1;
        switch (r12.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                C57282mm r14 = (C57282mm) obj2;
                this.A02 = r4.Afm(this.A02, r14.A02, C12960it.A1R(this.A00), C12960it.A1R(r14.A00));
                this.A03 = (C34311fw) r4.Aft(this.A03, r14.A03);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 4, 4);
                long j = this.A01;
                int i2 = r14.A00;
                this.A01 = r4.Afs(j, r14.A01, A1V, C12960it.A1V(i2 & 4, 4));
                if (r4 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                AnonymousClass254 r142 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r42.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A02 = r42.A08();
                            } else if (A03 == 18) {
                                if ((this.A00 & 2) == 2) {
                                    r1 = (C81723uT) this.A03.A0T();
                                } else {
                                    r1 = null;
                                }
                                C34311fw r0 = (C34311fw) AbstractC27091Fz.A0H(r42, r142, C34311fw.A05);
                                this.A03 = r0;
                                if (r1 != null) {
                                    this.A03 = (C34311fw) AbstractC27091Fz.A0C(r1, r0);
                                }
                                this.A00 |= 2;
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A01 = r42.A06();
                            } else if (!A0a(r42, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57282mm();
            case 5:
                return new C81713uS();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57282mm.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A05(this.A02, 1, 0);
        }
        if ((i3 & 2) == 2) {
            C34311fw r0 = this.A03;
            if (r0 == null) {
                r0 = C34311fw.A05;
            }
            i2 = AbstractC27091Fz.A08(r0, 2, i2);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A05(3, this.A01);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            C34311fw r0 = this.A03;
            if (r0 == null) {
                r0 = C34311fw.A05;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
