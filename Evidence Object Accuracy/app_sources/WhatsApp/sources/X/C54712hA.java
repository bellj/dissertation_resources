package X;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.biz.catalog.view.activity.ProductListActivity;

/* renamed from: X.2hA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54712hA extends AbstractC05270Ox {
    public final /* synthetic */ ProductListActivity A00;

    public C54712hA(ProductListActivity productListActivity) {
        this.A00 = productListActivity;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        LinearLayoutManager linearLayoutManager;
        ProductListActivity productListActivity = this.A00;
        productListActivity.A2e();
        if (!productListActivity.A0C.A0E() && (linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager()) != null && C12980iv.A0A(linearLayoutManager) <= 4) {
            productListActivity.A0D.A04();
        }
    }
}
