package X;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

/* renamed from: X.0FE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FE extends AbstractC05520Pw {
    public int A00 = 0;
    public int A01 = 0;
    public PointF A02;
    public final float A03;
    public final DecelerateInterpolator A04 = new DecelerateInterpolator();
    public final LinearInterpolator A05 = new LinearInterpolator();

    public AnonymousClass0FE(Context context) {
        this.A03 = A04(context.getResources().getDisplayMetrics());
    }

    @Override // X.AbstractC05520Pw
    public void A03(View view, C05160Om r15, C05480Ps r16) {
        int i;
        int i2;
        int A05 = A05();
        AnonymousClass02H r1 = super.A02;
        if (r1 == null || !r1.A14()) {
            i = 0;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            i = A08((view.getLeft() - ((AnonymousClass0B6) view.getLayoutParams()).A03.left) - marginLayoutParams.leftMargin, view.getRight() + ((AnonymousClass0B6) view.getLayoutParams()).A03.right + marginLayoutParams.rightMargin, r1.A09(), r1.A03 - r1.A0A(), A05);
        }
        int A06 = A06();
        AnonymousClass02H r2 = super.A02;
        if (r2 == null || !r2.A15()) {
            i2 = 0;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            i2 = A08((view.getTop() - ((AnonymousClass0B6) view.getLayoutParams()).A03.top) - marginLayoutParams2.topMargin, view.getBottom() + ((AnonymousClass0B6) view.getLayoutParams()).A03.bottom + marginLayoutParams2.bottomMargin, r2.A0B(), r2.A00 - r2.A08(), A06);
        }
        int ceil = (int) Math.ceil(((double) A07((int) Math.sqrt((double) ((i * i) + (i2 * i2))))) / 0.3356d);
        if (ceil > 0) {
            DecelerateInterpolator decelerateInterpolator = this.A04;
            r15.A02 = -i;
            r15.A03 = -i2;
            r15.A01 = ceil;
            r15.A05 = decelerateInterpolator;
            r15.A06 = true;
        }
    }

    public float A04(DisplayMetrics displayMetrics) {
        return 25.0f / ((float) displayMetrics.densityDpi);
    }

    public int A05() {
        PointF pointF = this.A02;
        if (pointF == null) {
            return 0;
        }
        float f = pointF.x;
        if (f != 0.0f) {
            return f > 0.0f ? 1 : -1;
        }
        return 0;
    }

    public int A06() {
        PointF pointF = this.A02;
        if (pointF == null) {
            return 0;
        }
        float f = pointF.y;
        if (f != 0.0f) {
            return f > 0.0f ? 1 : -1;
        }
        return 0;
    }

    public int A07(int i) {
        return (int) Math.ceil((double) (((float) Math.abs(i)) * this.A03));
    }

    public int A08(int i, int i2, int i3, int i4, int i5) {
        if (i5 == -1) {
            return i3 - i;
        }
        if (i5 == 0) {
            int i6 = i3 - i;
            if (i6 > 0) {
                return i6;
            }
            int i7 = i4 - i2;
            if (i7 >= 0) {
                return 0;
            }
            return i7;
        } else if (i5 == 1) {
            return i4 - i2;
        } else {
            throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }
}
