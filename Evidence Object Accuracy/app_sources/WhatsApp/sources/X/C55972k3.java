package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.2k3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55972k3 extends AnonymousClass2k6 {
    public AnonymousClass3DV A00 = new AnonymousClass3DV(this);

    public C55972k3(Context context) {
        super(context, null);
    }

    @Override // X.AnonymousClass2k6, android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        AnonymousClass3DV r4 = this.A00;
        if (r4.A04) {
            Path path = r4.A08;
            if (path.isEmpty()) {
                RectF rectF = r4.A09;
                float f = r4.A00;
                RectF rectF2 = r4.A0A;
                rectF.set(f, f, rectF2.right - f, rectF2.bottom - f);
                path.addRect(rectF2, Path.Direction.CW);
                int i = r4.A03;
                boolean A02 = C64973Hq.A02(i);
                float f2 = r4.A02;
                if (A02) {
                    path.addRoundRect(rectF2, f2, f2, Path.Direction.CCW);
                } else {
                    Float valueOf = Float.valueOf(f2);
                    float[] fArr = r4.A0C;
                    C64973Hq.A01(fArr, valueOf.floatValue(), i);
                    path.addRoundRect(rectF2, fArr, Path.Direction.CW);
                    Path path2 = r4.A07;
                    path2.reset();
                    Float valueOf2 = Float.valueOf(r4.A01);
                    C64973Hq.A01(fArr, valueOf2.floatValue(), r4.A03);
                    path2.addRoundRect(rectF, fArr, Path.Direction.CW);
                }
            }
            canvas.drawPath(path, r4.A06);
            if (C64973Hq.A02(r4.A03)) {
                RectF rectF3 = r4.A09;
                float f3 = r4.A01;
                canvas.drawRoundRect(rectF3, f3, f3, r4.A05);
                return;
            }
            canvas.drawPath(r4.A07, r4.A05);
        }
    }

    public AnonymousClass3DV getDecorationHelper() {
        return this.A00;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        AnonymousClass3DV r5 = this.A00;
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        RectF rectF = r5.A0A;
        float f = (float) measuredWidth;
        if (rectF.right != f || rectF.bottom != ((float) measuredHeight)) {
            rectF.set(0.0f, 0.0f, f, (float) measuredHeight);
            r5.A08.reset();
        }
    }
}
