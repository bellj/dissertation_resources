package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1gm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34831gm extends AbstractC30271Wt {
    public final Set A00 = new HashSet();

    public C34831gm(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 39, j);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r7) {
        AnonymousClass1G4 A0T = AnonymousClass2m9.A01.A0T();
        for (AnonymousClass1JR r0 : this.A00) {
            C34851go A02 = r0.A02();
            A0T.A03();
            AnonymousClass2m9 r2 = (AnonymousClass2m9) A0T.A00;
            AnonymousClass1K6 r1 = r2.A00;
            if (!((AnonymousClass1K7) r1).A00) {
                r1 = AbstractC27091Fz.A0G(r1);
                r2.A00 = r1;
            }
            r1.add(A02);
        }
        C56882m6 r22 = (C56882m6) C57692nT.A0D.A0T();
        r22.A05(EnumC87324Bb.A02);
        r22.A03();
        C57692nT r12 = (C57692nT) r22.A00;
        r12.A06 = (AnonymousClass2m9) A0T.A02();
        r12.A00 |= 64;
        r7.A03.A09((C57692nT) r22.A02());
    }
}
