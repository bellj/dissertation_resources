package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.io.File;

/* renamed from: X.0uG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19530uG {
    public final C19540uH A00;

    public C19530uG(C19540uH r1) {
        this.A00 = r1;
    }

    public static AnonymousClass2FQ A00(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("local_path");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("exported_path");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("required");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("file_size");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("encryption_iv");
        long j = cursor.getLong(columnIndexOrThrow);
        String string = cursor.getString(columnIndexOrThrow2);
        String string2 = cursor.getString(columnIndexOrThrow3);
        cursor.getInt(columnIndexOrThrow4);
        long j2 = cursor.getLong(columnIndexOrThrow5);
        String string3 = cursor.getString(columnIndexOrThrow6);
        AnonymousClass009.A05(string);
        AnonymousClass009.A05(string2);
        AnonymousClass009.A05(string3);
        return new AnonymousClass2FQ(new File(string), string2, string3, j, j2);
    }

    public long A01(String str, String str2, String str3, long j, boolean z) {
        AnonymousClass113 r0;
        ContentValues contentValues = new ContentValues();
        contentValues.put("local_path", str);
        contentValues.put("exported_path", str2);
        contentValues.put("required", Boolean.valueOf(z));
        contentValues.put("file_size", Long.valueOf(j));
        contentValues.put("encryption_iv", str3);
        C19540uH r1 = this.A00;
        synchronized (r1) {
            r0 = r1.A00;
            if (r0 == null) {
                r0 = (AnonymousClass113) r1.A02.get();
                r1.A00 = r0;
            }
        }
        C16310on A02 = r0.A02();
        try {
            long A022 = A02.A03.A02(contentValues, "exported_files_metadata");
            A02.close();
            return A022;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
