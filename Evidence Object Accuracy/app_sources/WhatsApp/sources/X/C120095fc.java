package X;

import com.whatsapp.R;

/* renamed from: X.5fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120095fc extends AnonymousClass4UZ {
    public final /* synthetic */ AbstractActivityC121655j9 A00;

    public C120095fc(AbstractActivityC121655j9 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass4UZ
    public void A00() {
        AbstractActivityC121655j9 r4 = this.A00;
        r4.A3O(r4.A08, "ConfirmPaymentFragment");
        RunnableC134686Fo r3 = new Runnable() { // from class: X.6Fo
            @Override // java.lang.Runnable
            public final void run() {
                AbstractActivityC121655j9 r1 = C120095fc.this.A00;
                r1.A3L(r1.A08);
            }
        };
        r4.A2C(R.string.register_wait_message);
        ((ActivityC13830kP) r4).A05.Ab2(new AnonymousClass6JW(r4, r3, true));
    }
}
