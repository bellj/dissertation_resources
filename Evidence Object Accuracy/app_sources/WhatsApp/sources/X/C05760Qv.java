package X;

import android.view.accessibility.AccessibilityEvent;

/* renamed from: X.0Qv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05760Qv {
    public static int A00(AccessibilityEvent accessibilityEvent) {
        return accessibilityEvent.getContentChangeTypes();
    }

    public static void A01(AccessibilityEvent accessibilityEvent, int i) {
        accessibilityEvent.setContentChangeTypes(i);
    }
}
