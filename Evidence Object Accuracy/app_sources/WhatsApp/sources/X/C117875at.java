package X;

import android.view.View;
import com.whatsapp.PagerSlidingTabStrip;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiMyQrFragment;
import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;
import com.whatsapp.payments.ui.IndiaUpiScanQrCodeFragment;

/* renamed from: X.5at  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117875at extends AnonymousClass019 implements AbstractC14800m4 {
    public AnonymousClass4OE[] A00;
    public final /* synthetic */ IndiaUpiQrTabActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C117875at(AnonymousClass01F r2, IndiaUpiQrTabActivity indiaUpiQrTabActivity, int i) {
        super(r2, 0);
        this.A01 = indiaUpiQrTabActivity;
        this.A00 = new AnonymousClass4OE[i];
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.length;
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        if (i == 0) {
            return "Scan code";
        }
        if (i == 1) {
            return "My code";
        }
        throw C12970iu.A0f("The item position is not defined");
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        if (i == 0) {
            return new IndiaUpiScanQrCodeFragment();
        }
        if (i == 1) {
            return IndiaUpiMyQrFragment.A00(IndiaUpiQrTabActivity.A0F);
        }
        throw C12970iu.A0f("The item position is not defined");
    }

    @Override // X.AbstractC14800m4
    public View AEv(int i) {
        AnonymousClass4OE[] r3 = this.A00;
        if (r3[i] == null) {
            PagerSlidingTabStrip pagerSlidingTabStrip = this.A01.A01;
            AnonymousClass4OE r2 = new AnonymousClass4OE(C12960it.A0F(C12960it.A0E(pagerSlidingTabStrip), pagerSlidingTabStrip, R.layout.qr_tab));
            CharSequence A04 = A04(i);
            AnonymousClass009.A05(A04);
            r2.A01.setText(A04);
            r3[i] = r2;
        }
        return r3[i].A00;
    }
}
