package X;

import android.content.Context;
import com.whatsapp.status.StatusPrivacyActivity;

/* renamed from: X.4qu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103534qu implements AbstractC009204q {
    public final /* synthetic */ StatusPrivacyActivity A00;

    public C103534qu(StatusPrivacyActivity statusPrivacyActivity) {
        this.A00 = statusPrivacyActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
