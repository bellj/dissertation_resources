package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* renamed from: X.2eZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53512eZ extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass367 A00;

    public C53512eZ(AnonymousClass367 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        AnonymousClass367 r0;
        Runnable runnable;
        super.A01(view, accessibilityEvent);
        if (accessibilityEvent.getEventType() == 65536 && (runnable = (r0 = this.A00).A01) != null) {
            r0.A05.removeCallbacks(runnable);
        }
    }
}
