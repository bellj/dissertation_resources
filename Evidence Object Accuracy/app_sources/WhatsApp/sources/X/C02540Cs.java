package X;

import java.util.ArrayList;

/* renamed from: X.0Cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02540Cs extends AnonymousClass0QV {
    public float A00 = -1.0f;
    public int A01;
    public int A02 = -1;
    public int A03 = -1;
    public AnonymousClass0Q7 A04;

    @Override // X.AnonymousClass0QV
    public boolean A0D() {
        return true;
    }

    public C02540Cs() {
        AnonymousClass0Q7 r3 = this.A0Y;
        this.A04 = r3;
        this.A01 = 0;
        ArrayList arrayList = this.A0g;
        arrayList.clear();
        arrayList.add(r3);
        AnonymousClass0Q7[] r1 = this.A0n;
        int length = r1.length;
        for (int i = 0; i < length; i++) {
            r1[i] = r3;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        return r2.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r2.A01 == 0) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r2.A01 == 1) goto L_0x001b;
     */
    @Override // X.AnonymousClass0QV
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0Q7 A04(X.AnonymousClass0JZ r3) {
        /*
            r2 = this;
            int r0 = r3.ordinal()
            switch(r0) {
                case 1: goto L_0x0016;
                case 2: goto L_0x0011;
                case 3: goto L_0x0016;
                case 4: goto L_0x0011;
                case 5: goto L_0x001e;
                case 6: goto L_0x001e;
                case 7: goto L_0x001e;
                case 8: goto L_0x001e;
                default: goto L_0x0007;
            }
        L_0x0007:
            java.lang.String r1 = r3.name()
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        L_0x0011:
            int r0 = r2.A01
            if (r0 != 0) goto L_0x0007
            goto L_0x001b
        L_0x0016:
            int r1 = r2.A01
            r0 = 1
            if (r1 != r0) goto L_0x0007
        L_0x001b:
            X.0Q7 r0 = r2.A04
            return r0
        L_0x001e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02540Cs.A04(X.0JZ):X.0Q7");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001d, code lost:
        if (r0.A0o[0] != X.AnonymousClass0JR.A04) goto L_0x001f;
     */
    @Override // X.AnonymousClass0QV
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.C06240Ss r10) {
        /*
            r9 = this;
            X.0QV r3 = r9.A0Z
            if (r3 == 0) goto L_0x005d
            X.0JZ r0 = X.AnonymousClass0JZ.LEFT
            X.0Q7 r7 = r3.A04(r0)
            X.0JZ r0 = X.AnonymousClass0JZ.RIGHT
            X.0Q7 r6 = r3.A04(r0)
            X.0QV r0 = r9.A0Z
            r2 = 1
            r4 = 0
            if (r0 == 0) goto L_0x001f
            X.0JR[] r0 = r0.A0o
            r1 = r0[r4]
            X.0JR r0 = X.AnonymousClass0JR.WRAP_CONTENT
            r8 = 1
            if (r1 == r0) goto L_0x0020
        L_0x001f:
            r8 = 0
        L_0x0020:
            int r0 = r9.A01
            if (r0 != 0) goto L_0x003d
            X.0JZ r0 = X.AnonymousClass0JZ.TOP
            X.0Q7 r7 = r3.A04(r0)
            X.0JZ r0 = X.AnonymousClass0JZ.BOTTOM
            X.0Q7 r6 = r3.A04(r0)
            X.0QV r0 = r9.A0Z
            if (r0 == 0) goto L_0x00a7
            X.0JR[] r0 = r0.A0o
            r1 = r0[r2]
            X.0JR r0 = X.AnonymousClass0JR.WRAP_CONTENT
            if (r1 != r0) goto L_0x00a7
        L_0x003c:
            r8 = r2
        L_0x003d:
            int r0 = r9.A02
            r5 = 8
            r1 = -1
            r3 = 5
            if (r0 == r1) goto L_0x005e
            X.0Q7 r0 = r9.A04
            X.0QC r2 = r10.A05(r0)
            X.0QC r1 = r10.A05(r7)
            int r0 = r9.A02
            r10.A0D(r2, r1, r0, r5)
            if (r8 == 0) goto L_0x005d
            X.0QC r0 = r10.A05(r6)
            r10.A0E(r0, r2, r4, r3)
        L_0x005d:
            return
        L_0x005e:
            int r0 = r9.A03
            if (r0 == r1) goto L_0x007f
            X.0Q7 r0 = r9.A04
            X.0QC r2 = r10.A05(r0)
            X.0QC r1 = r10.A05(r6)
            int r0 = r9.A03
            int r0 = -r0
            r10.A0D(r2, r1, r0, r5)
            if (r8 == 0) goto L_0x005d
            X.0QC r0 = r10.A05(r7)
            r10.A0E(r2, r0, r4, r3)
            r10.A0E(r1, r2, r4, r3)
            return
        L_0x007f:
            float r1 = r9.A00
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x005d
            X.0Q7 r0 = r9.A04
            X.0QC r5 = r10.A05(r0)
            X.0QC r4 = r10.A05(r6)
            float r3 = r9.A00
            X.0Xf r2 = r10.A01()
            X.0iP r1 = r2.A01
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            r1.AZg(r5, r0)
            X.0iP r0 = r2.A01
            r0.AZg(r4, r3)
            r10.A09(r2)
            return
        L_0x00a7:
            r2 = 0
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02540Cs.A08(X.0Ss):void");
    }

    @Override // X.AnonymousClass0QV
    public void A0A(C06240Ss r6) {
        AnonymousClass0QV r4 = this.A0Z;
        if (r4 != null) {
            int A00 = C06240Ss.A00(this.A04);
            if (this.A01 == 1) {
                this.A0P = A00;
                this.A0Q = 0;
                A05(r4.A00());
                A06(0);
                return;
            }
            this.A0P = 0;
            this.A0Q = A00;
            A06(r4.A01());
            A05(0);
        }
    }

    public void A0I(int i) {
        AnonymousClass0Q7 r3;
        if (this.A01 != i) {
            this.A01 = i;
            ArrayList arrayList = this.A0g;
            arrayList.clear();
            if (i == 1) {
                r3 = this.A0W;
            } else {
                r3 = this.A0Y;
            }
            this.A04 = r3;
            arrayList.add(r3);
            AnonymousClass0Q7[] r2 = this.A0n;
            int length = r2.length;
            for (int i2 = 0; i2 < length; i2++) {
                r2[i2] = r3;
            }
        }
    }
}
