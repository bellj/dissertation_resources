package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.52A  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52A implements AnonymousClass5T8 {
    @Override // X.AnonymousClass5T8
    public Object AJ9(AbstractC111885Be r3, C94394bk r4, Object obj, String str, List list) {
        AbstractC117035Xw r1 = r4.A01.A00;
        if (obj instanceof Map) {
            return r1.AFy(obj);
        }
        return null;
    }
}
