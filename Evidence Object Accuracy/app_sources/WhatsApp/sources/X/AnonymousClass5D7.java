package X;

import java.util.Iterator;

/* renamed from: X.5D7  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5D7 implements Iterator {
    public Iterator A00;
    public final /* synthetic */ C113545Hz A01;

    public AnonymousClass5D7(C113545Hz r2) {
        this.A01 = r2;
        this.A00 = r2.A00.iterator();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.A00.hasNext();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        return this.A00.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }
}
