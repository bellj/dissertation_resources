package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import androidx.appcompat.widget.ActionBarOverlayLayout;

/* renamed from: X.099  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass099 extends AnimatorListenerAdapter {
    public final /* synthetic */ ActionBarOverlayLayout A00;

    public AnonymousClass099(ActionBarOverlayLayout actionBarOverlayLayout) {
        this.A00 = actionBarOverlayLayout;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        ActionBarOverlayLayout actionBarOverlayLayout = this.A00;
        actionBarOverlayLayout.A05 = null;
        actionBarOverlayLayout.A0F = false;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        ActionBarOverlayLayout actionBarOverlayLayout = this.A00;
        actionBarOverlayLayout.A05 = null;
        actionBarOverlayLayout.A0F = false;
    }
}
