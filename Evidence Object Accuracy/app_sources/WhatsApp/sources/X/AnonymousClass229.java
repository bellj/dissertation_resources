package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.229  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass229 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass229 A0A;
    public static volatile AnonymousClass255 A0B;
    public int A00;
    public AnonymousClass1K6 A01;
    public AnonymousClass1K6 A02;
    public C49692Lu A03;
    public String A04;
    public String A05;
    public String A06 = "";
    public String A07;
    public String A08;
    public String A09 = "";

    static {
        AnonymousClass229 r0 = new AnonymousClass229();
        A0A = r0;
        r0.A0W();
    }

    public AnonymousClass229() {
        AnonymousClass277 r0 = AnonymousClass277.A01;
        this.A02 = r0;
        this.A08 = "";
        this.A07 = "";
        this.A01 = r0;
        this.A05 = "";
        this.A04 = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        AnonymousClass2Lw r1;
        switch (r8.ordinal()) {
            case 0:
                return A0A;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass229 r10 = (AnonymousClass229) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A09;
                int i2 = r10.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A09 = r9.Afy(str, r10.A09, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A06;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A06 = r9.Afy(str2, r10.A06, z3, z4);
                this.A02 = r9.Afr(this.A02, r10.A02);
                int i3 = this.A00;
                boolean z5 = false;
                if ((i3 & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A08;
                int i4 = r10.A00;
                boolean z6 = false;
                if ((i4 & 4) == 4) {
                    z6 = true;
                }
                this.A08 = r9.Afy(str3, r10.A08, z5, z6);
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                String str4 = this.A07;
                boolean z8 = false;
                if ((i4 & 8) == 8) {
                    z8 = true;
                }
                this.A07 = r9.Afy(str4, r10.A07, z7, z8);
                this.A01 = r9.Afr(this.A01, r10.A01);
                int i5 = this.A00;
                boolean z9 = false;
                if ((i5 & 16) == 16) {
                    z9 = true;
                }
                String str5 = this.A05;
                int i6 = r10.A00;
                boolean z10 = false;
                if ((i6 & 16) == 16) {
                    z10 = true;
                }
                this.A05 = r9.Afy(str5, r10.A05, z9, z10);
                boolean z11 = false;
                if ((i5 & 32) == 32) {
                    z11 = true;
                }
                String str6 = this.A04;
                boolean z12 = false;
                if ((i6 & 32) == 32) {
                    z12 = true;
                }
                this.A04 = r9.Afy(str6, r10.A04, z11, z12);
                this.A03 = (C49692Lu) r9.Aft(this.A03, r10.A03);
                if (r9 == C463025i.A00) {
                    this.A00 |= r10.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A2 = r92.A0A();
                                this.A00 = 1 | this.A00;
                                this.A09 = A0A2;
                            } else if (A03 == 18) {
                                String A0A3 = r92.A0A();
                                this.A00 |= 2;
                                this.A06 = A0A3;
                            } else if (A03 == 26) {
                                String A0A4 = r92.A0A();
                                AnonymousClass1K6 r12 = this.A02;
                                if (!((AnonymousClass1K7) r12).A00) {
                                    r12 = AbstractC27091Fz.A0G(r12);
                                    this.A02 = r12;
                                }
                                r12.add(A0A4);
                            } else if (A03 == 34) {
                                String A0A5 = r92.A0A();
                                this.A00 |= 4;
                                this.A08 = A0A5;
                            } else if (A03 == 42) {
                                String A0A6 = r92.A0A();
                                this.A00 |= 8;
                                this.A07 = A0A6;
                            } else if (A03 == 50) {
                                AnonymousClass1K6 r13 = this.A01;
                                if (!((AnonymousClass1K7) r13).A00) {
                                    r13 = AbstractC27091Fz.A0G(r13);
                                    this.A01 = r13;
                                }
                                r13.add((C57322mq) r92.A09(r102, C57322mq.A04.A0U()));
                            } else if (A03 == 58) {
                                String A0A7 = r92.A0A();
                                this.A00 |= 16;
                                this.A05 = A0A7;
                            } else if (A03 == 66) {
                                String A0A8 = r92.A0A();
                                this.A00 |= 32;
                                this.A04 = A0A8;
                            } else if (A03 == 74) {
                                if ((this.A00 & 64) == 64) {
                                    r1 = (AnonymousClass2Lw) this.A03.A0T();
                                } else {
                                    r1 = null;
                                }
                                C49692Lu r0 = (C49692Lu) r92.A09(r102, C49692Lu.A05.A0U());
                                this.A03 = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A03 = (C49692Lu) r1.A01();
                                }
                                this.A00 |= 64;
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r14 = new C28971Pt(e2.getMessage());
                        r14.unfinishedMessage = this;
                        throw new RuntimeException(r14);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A02).A00 = false;
                ((AnonymousClass1K7) this.A01).A00 = false;
                return null;
            case 4:
                return new AnonymousClass229();
            case 5:
                return new C467226y();
            case 6:
                break;
            case 7:
                if (A0B == null) {
                    synchronized (AnonymousClass229.class) {
                        if (A0B == null) {
                            A0B = new AnonymousClass255(A0A);
                        }
                    }
                }
                return A0B;
            default:
                throw new UnsupportedOperationException();
        }
        return A0A;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A09) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A06);
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.A02.size(); i4++) {
            i3 += CodedOutputStream.A0B((String) this.A02.get(i4));
        }
        int size = i + i3 + this.A02.size();
        if ((this.A00 & 4) == 4) {
            size += CodedOutputStream.A07(4, this.A08);
        }
        if ((this.A00 & 8) == 8) {
            size += CodedOutputStream.A07(5, this.A07);
        }
        for (int i5 = 0; i5 < this.A01.size(); i5++) {
            size += CodedOutputStream.A0A((AnonymousClass1G1) this.A01.get(i5), 6);
        }
        if ((this.A00 & 16) == 16) {
            size += CodedOutputStream.A07(7, this.A05);
        }
        if ((this.A00 & 32) == 32) {
            size += CodedOutputStream.A07(8, this.A04);
        }
        if ((this.A00 & 64) == 64) {
            C49692Lu r0 = this.A03;
            if (r0 == null) {
                r0 = C49692Lu.A05;
            }
            size += CodedOutputStream.A0A(r0, 9);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A09);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A06);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0I(3, (String) this.A02.get(i));
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(4, this.A08);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(5, this.A07);
        }
        for (int i2 = 0; i2 < this.A01.size(); i2++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A01.get(i2), 6);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(7, this.A05);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(8, this.A04);
        }
        if ((this.A00 & 64) == 64) {
            C49692Lu r0 = this.A03;
            if (r0 == null) {
                r0 = C49692Lu.A05;
            }
            codedOutputStream.A0L(r0, 9);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
