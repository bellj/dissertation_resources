package X;

import java.lang.reflect.Method;

/* renamed from: X.4En  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88164En {
    public static final void A00(Throwable th, Throwable th2) {
        Method method;
        C16700pc.A0E(th2, 1);
        if (th != th2 && !(AnonymousClass4GX.A00 instanceof C113715Ir) && (method = AnonymousClass4HF.A00) != null) {
            method.invoke(th, th2);
        }
    }
}
