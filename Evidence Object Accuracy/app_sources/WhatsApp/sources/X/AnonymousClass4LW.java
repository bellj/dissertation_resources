package X;

/* renamed from: X.4LW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4LW {
    public final AnonymousClass1V8 A00;

    public AnonymousClass4LW(String str) {
        C41141sy r2 = new C41141sy("context");
        if (AnonymousClass3JT.A0E(str, 0, 9007199254740991L, false)) {
            r2.A04(new AnonymousClass1W9("params", str));
        }
        this.A00 = r2.A03();
    }
}
