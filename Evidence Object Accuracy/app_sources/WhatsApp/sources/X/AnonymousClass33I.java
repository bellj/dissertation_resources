package X;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import org.json.JSONObject;

/* renamed from: X.33I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33I extends AbstractC454821u {
    public final Paint A00;
    public final Path A01;
    public final PointF A02;
    public final PointF A03;
    public final RectF A04;
    public final RectF A05;
    public final PointF[] A06;

    public AnonymousClass33I() {
        this.A01 = new Path();
        Paint A0G = C12990iw.A0G(1);
        this.A00 = A0G;
        this.A04 = C12980iv.A0K();
        this.A06 = new PointF[]{new PointF(), new PointF(), new PointF(), new PointF(), new PointF(), new PointF(), new PointF()};
        this.A05 = C12980iv.A0K();
        this.A02 = new PointF();
        this.A03 = new PointF();
        super.A00 = 110.0f;
        A0G.setStyle(Paint.Style.FILL);
        A0G.setColor(-31);
        C12990iw.A13(super.A01);
    }

    public AnonymousClass33I(JSONObject jSONObject) {
        this();
        super.A0A(jSONObject);
    }

    public static PointF A02(PointF pointF, RectF rectF, PointF[] pointFArr, int i) {
        pointF.y = rectF.top + rectF.height();
        return pointFArr[i];
    }

    public static PointF A03(PointF pointF, RectF rectF, PointF[] pointFArr, int i, int i2) {
        float f = rectF.left;
        pointF.x = f;
        pointF.y = rectF.top;
        pointFArr[i].x = f + rectF.width();
        pointFArr[i].y = rectF.top;
        pointFArr[i2].x = rectF.left + rectF.width();
        return pointFArr[i2];
    }

    public static PointF A04(RectF rectF, PointF[] pointFArr, int i, int i2, int i3) {
        pointFArr[i].x = rectF.left + rectF.width();
        pointFArr[i].y = rectF.top + rectF.height();
        PointF pointF = pointFArr[i2];
        pointF.x = rectF.left;
        pointF.y = rectF.top + rectF.height();
        return pointFArr[i3];
    }

    public static void A05(PointF pointF, RectF rectF, float f) {
        pointF.y = rectF.top + ((rectF.height() * f) / 6.0f);
    }

    public static void A06(RectF rectF, PointF[] pointFArr, int i, int i2) {
        PointF pointF = pointFArr[i];
        float f = rectF.left;
        pointF.x = f;
        pointF.y = rectF.top;
        pointFArr[i2].x = f + rectF.width();
        pointFArr[i2].y = rectF.top;
    }

    @Override // X.AbstractC454821u
    public void A0O(float f) {
        super.A0O((f * 2.0f) / 3.0f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01ff  */
    @Override // X.AbstractC454821u
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P(android.graphics.Canvas r23) {
        /*
        // Method dump skipped, instructions count: 892
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass33I.A0P(android.graphics.Canvas):void");
    }

    @Override // X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        float f5 = (f2 + f4) / 2.0f;
        float f6 = (((f3 - f) * 2.0f) / 3.0f) / 2.0f;
        super.A0Q(rectF, f, f5 - f6, f3, f5 + f6);
    }
}
