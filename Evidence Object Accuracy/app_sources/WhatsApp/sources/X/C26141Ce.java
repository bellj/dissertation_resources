package X;

/* renamed from: X.1Ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26141Ce extends AbstractC26131Cd {
    public static int A04 = 7;
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C17900ra A02;
    public final C17070qD A03;

    public C26141Ce(C14830m7 r1, C14820m6 r2, C14850m9 r3, C21860y6 r4, C17900ra r5, C22710zW r6, C17070qD r7) {
        super(r3, r4, r6);
        this.A00 = r1;
        this.A03 = r7;
        this.A01 = r2;
        this.A02 = r5;
    }

    public final void A03(int i) {
        this.A01.A00.edit().putInt("payments_onboarding_banner_total_days", i).apply();
    }
}
