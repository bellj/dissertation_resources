package X;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import java.util.List;

/* renamed from: X.3Hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64963Hp {
    public static int A00(C14260l7 r1, AnonymousClass28D r2) {
        AnonymousClass28D A0F = r2.A0F(38);
        if (A0F != null) {
            return AnonymousClass4Di.A00(r1, A0F);
        }
        try {
            String A0I = r2.A0I(36);
            if (A0I == null) {
                return -16777216;
            }
            return AnonymousClass3JW.A05(A0I);
        } catch (AnonymousClass491 e) {
            C28691Op.A01("ColorDrawableUtils", "Error parsing border color in BoxDecoration", e);
            return 0;
        }
    }

    public static AnonymousClass2ZU A01(C14260l7 r10, AnonymousClass28D r11, int i) {
        float A00;
        int A002;
        int A003;
        float A004;
        float[] A02;
        float A01;
        Paint paint;
        AnonymousClass2ZU r2 = new AnonymousClass2ZU();
        if (r11 == null) {
            A00 = 0.0f;
            A002 = 0;
            A003 = 0;
            A004 = 0.0f;
            A02 = null;
        } else {
            A00 = AnonymousClass28D.A00(r11, 46);
            A002 = C64973Hq.A00(r11.A0M(56), 0);
            A003 = A00(r10, r11);
            A004 = AnonymousClass28D.A00(r11, 40);
            A02 = A02(r11.A0M(62));
            String A0I = r11.A0I(63);
            if (A0I != null) {
                A01 = AnonymousClass3JW.A01(A0I);
                r2.A05.setColor(i);
                paint = r2.A04;
                paint.setColor(A003);
                paint.setStrokeWidth(A004);
                if (A004 > 0.0f && A02 != null) {
                    paint.setPathEffect(new DashPathEffect(A02, A01));
                }
                r2.A02 = A00;
                r2.A03 = A002;
                float f = A004 / 2.0f;
                r2.A00 = f;
                r2.A01 = A00 - f;
                return r2;
            }
        }
        A01 = 0.0f;
        r2.A05.setColor(i);
        paint = r2.A04;
        paint.setColor(A003);
        paint.setStrokeWidth(A004);
        if (A004 > 0.0f) {
            paint.setPathEffect(new DashPathEffect(A02, A01));
        }
        r2.A02 = A00;
        r2.A03 = A002;
        float f = A004 / 2.0f;
        r2.A00 = f;
        r2.A01 = A00 - f;
        return r2;
    }

    public static float[] A02(List list) {
        float A01;
        if (list == null || list.size() == 0) {
            return null;
        }
        float[] fArr = new float[list.size()];
        for (int i = 0; i < list.size(); i++) {
            String A0g = C12960it.A0g(list, i);
            if (A0g == null) {
                A01 = 0.0f;
            } else {
                A01 = AnonymousClass3JW.A01(A0g);
            }
            fArr[i] = A01;
        }
        return fArr;
    }
}
