package X;

import android.view.View;
import androidx.core.widget.NestedScrollView;

/* renamed from: X.4Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88124Ej {
    public static void A00(NestedScrollView nestedScrollView) {
        int childCount = nestedScrollView.getChildCount();
        if (childCount != 0) {
            View childAt = nestedScrollView.getChildAt(childCount - 1);
            nestedScrollView.A06(0 - nestedScrollView.getScrollX(), ((childAt.getScrollY() + childAt.getHeight()) + nestedScrollView.getPaddingBottom()) - nestedScrollView.getScrollY(), false);
        }
    }
}
