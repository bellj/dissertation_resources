package X;

import java.lang.ref.WeakReference;

/* renamed from: X.4OD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4OD {
    public final C14820m6 A00;
    public final WeakReference A01;

    public AnonymousClass4OD(C14820m6 r2, AnonymousClass34O r3) {
        this.A00 = r2;
        this.A01 = C12970iu.A10(r3);
    }
}
