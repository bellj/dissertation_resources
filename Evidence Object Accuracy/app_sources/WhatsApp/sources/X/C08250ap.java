package X;

import android.graphics.Path;

/* renamed from: X.0ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08250ap implements AbstractC12040hH {
    public final Path.FillType A00;
    public final AnonymousClass0H4 A01;
    public final AnonymousClass0HA A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;

    public C08250ap(Path.FillType fillType, AnonymousClass0H4 r2, AnonymousClass0HA r3, String str, boolean z, boolean z2) {
        this.A03 = str;
        this.A04 = z;
        this.A00 = fillType;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = z2;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C08050aV(r2, this, r3);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ShapeFill{color=, fillEnabled=");
        sb.append(this.A04);
        sb.append('}');
        return sb.toString();
    }
}
