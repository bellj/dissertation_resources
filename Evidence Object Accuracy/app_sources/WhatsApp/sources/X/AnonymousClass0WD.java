package X;

import android.os.Message;
import android.view.View;

/* renamed from: X.0WD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WD implements View.OnClickListener {
    public final /* synthetic */ AnonymousClass0U5 A00;

    public AnonymousClass0WD(AnonymousClass0U5 r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        Message message;
        Message obtain;
        AnonymousClass0U5 r3 = this.A00;
        if (((view == r3.A0G && (message = r3.A0B) != null) || ((view == r3.A0E && (message = r3.A09) != null) || (view == r3.A0F && (message = r3.A0A) != null))) && (obtain = Message.obtain(message)) != null) {
            obtain.sendToTarget();
        }
        r3.A08.obtainMessage(1, r3.A0W).sendToTarget();
    }
}
