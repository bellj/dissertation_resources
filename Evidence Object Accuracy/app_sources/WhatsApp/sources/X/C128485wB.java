package X;

import android.os.Environment;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.5wB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128485wB {
    public final C14330lG A00;

    public C128485wB(C14330lG r1) {
        this.A00 = r1;
    }

    public File A00(String str) {
        File A0M;
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            Log.e("BloksFileManager/getCapturedMediaDirectory external storage is not writable");
            A0M = null;
        } else {
            A0M = this.A00.A0M("bloks_captured_media");
            if (!A0M.exists() && !A0M.mkdirs()) {
                Log.e("BloksFileManager/getCapturedMediaFile: failed to create media directory");
                return null;
            }
        }
        return new File(A0M.getPath(), str);
    }
}
