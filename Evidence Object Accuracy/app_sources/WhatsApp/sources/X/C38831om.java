package X;

/* renamed from: X.1om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38831om extends AbstractC16110oT {
    public Boolean A00;

    public C38831om() {
        super(1842, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamStickerAddToFavorite {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsAvatar", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
