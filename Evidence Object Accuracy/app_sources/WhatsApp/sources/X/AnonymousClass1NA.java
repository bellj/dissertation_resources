package X;

import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/* renamed from: X.1NA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1NA {
    public int A00 = 0;
    public int A01 = 0;
    public final int A02;
    public final C28211Md A03 = new C28211Md();
    public final AnonymousClass02V A04;
    public final AnonymousClass23W A05;
    public final AnonymousClass16F A06;

    public AnonymousClass1NA(AnonymousClass23W r5, AnonymousClass16F r6, RandomAccessFile randomAccessFile, int i, int i2) {
        AnonymousClass02V r2;
        int i3 = 65536;
        int i4 = (i * 65536) + 65536;
        if (i2 != 0) {
            if (i2 == 1) {
                i4 = (i << 14) + 65536;
                i3 = 16384;
            } else {
                r2 = new AnonymousClass02V(r6, randomAccessFile, (i * 32768) + 65536, 32768);
                this.A04 = r2;
                this.A02 = i;
                this.A05 = r5;
                this.A06 = r6;
            }
        }
        r2 = new AnonymousClass02V(r6, randomAccessFile, i4, i3);
        this.A04 = r2;
        this.A02 = i;
        this.A05 = r5;
        this.A06 = r6;
    }

    public void A00() {
        AnonymousClass02V r4 = this.A04;
        r4.A02();
        AnonymousClass23X r3 = this.A05.A05[this.A02];
        r3.A01 = r4.A01;
        r3.A04 = 0;
        r3.A02 = 1;
        r3.A00 = 0;
        r3.A03 = r4.A00();
        this.A01 = 0;
        this.A00 = 0;
        this.A03.A00.clear();
    }

    public void A01() {
        try {
            AnonymousClass02V r3 = this.A04;
            r3.A03();
            AnonymousClass23X r2 = this.A05.A05[this.A02];
            r2.A01 = r3.A01;
            r2.A03 = r3.A00();
        } catch (IOException e) {
            AnonymousClass16F r1 = this.A06;
            r1.A0F = Boolean.TRUE;
            r1.A05();
            Log.e("eventbuffer/flushEventBuffers: cannot write event buffer", e);
            throw e;
        }
    }

    public void A02() {
        try {
            AnonymousClass02V r6 = this.A04;
            AnonymousClass23X[] r7 = this.A05.A05;
            int i = this.A02;
            r6.A04(r7[i].A01);
            boolean z = true;
            boolean z2 = false;
            if (r6.A05.position() == r7[i].A01) {
                z2 = true;
            }
            Log.a(z2);
            if (r6.A01 != r7[i].A01) {
                z = false;
            }
            Log.a(z);
            int i2 = (r6.A00() > r7[i].A03 ? 1 : (r6.A00() == r7[i].A03 ? 0 : -1));
            boolean A04 = A04();
            if (i2 != 0) {
                AnonymousClass16F r1 = this.A06;
                Boolean bool = Boolean.TRUE;
                if (A04) {
                    r1.A01 = bool;
                } else {
                    r1.A06 = bool;
                }
                throw new AnonymousClass2CB("Invalid checksum");
            } else if (A04) {
                this.A01 = 0;
                this.A00 = 0;
                C28211Md r5 = this.A03;
                r5.A00.clear();
                ByteBuffer A01 = r6.A01();
                if (A01.limit() != 0) {
                    byte[][] bArr = C47592Bp.A00;
                    int length = bArr.length;
                    int i3 = length - 1;
                    byte[] bArr2 = new byte[C47592Bp.A00(i3).length];
                    try {
                        A01.get(bArr2);
                        for (int i4 = 0; i4 < length; i4++) {
                            if (Arrays.equals(bArr2, bArr[i4])) {
                                if (i4 < 0 || i4 > i3) {
                                    throw new AnonymousClass2CB("Invalid event buffer header");
                                } else {
                                    if (i4 >= 1) {
                                        A01.get(A06(i4));
                                    }
                                    while (A01.position() < A01.limit()) {
                                        try {
                                            try {
                                                AnonymousClass04W A05 = AnonymousClass02X.A05(A01);
                                                this.A01++;
                                                int i5 = A05.A01;
                                                if (i5 == 1) {
                                                    this.A00++;
                                                } else if (i5 == 0) {
                                                    r5.A00(A05.A00, A05.A02);
                                                }
                                            } catch (BufferUnderflowException unused) {
                                                throw new AnonymousClass2CC("Incomplete buffer");
                                            }
                                        } catch (AnonymousClass2CC e) {
                                            throw new AnonymousClass2CB(e.toString());
                                        }
                                    }
                                    return;
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder("Invalid value: ");
                        sb.append(bArr2);
                        throw new RuntimeException(sb.toString());
                    } catch (BufferUnderflowException unused2) {
                        throw new AnonymousClass2CB("Event buffer does not have a header");
                    }
                }
            }
        } catch (IOException e2) {
            throw new AnonymousClass2CB(e2.toString());
        }
    }

    public final void A03(byte[] bArr, int i) {
        if (A05()) {
            AnonymousClass02V r1 = this.A04;
            byte[] A00 = C47592Bp.A00(2);
            ByteBuffer byteBuffer = r1.A05;
            byteBuffer.put(A00);
            AnonymousClass23W r5 = this.A05;
            AnonymousClass23X r3 = r5.A05[this.A02];
            int i2 = r5.A00 + 1;
            r5.A00 = i2;
            if (i2 > 65535) {
                r5.A00 = 1;
                i2 = 1;
            }
            r3.A00 = i2;
            byteBuffer.put(A06(2));
        }
        int min = Math.min(bArr.length - 0, i);
        ByteBuffer byteBuffer2 = this.A04.A05;
        if (min <= byteBuffer2.remaining()) {
            byteBuffer2.put(bArr, 0, min);
            return;
        }
        StringBuilder sb = new StringBuilder("Not enough space in the buffer lenToWrite = ");
        sb.append(min);
        sb.append(" remaining = ");
        sb.append(byteBuffer2.remaining());
        throw new IndexOutOfBoundsException(sb.toString());
    }

    public boolean A04() {
        return this.A02 == this.A05.A01;
    }

    public final boolean A05() {
        return this.A04.A05.position() == 0;
    }

    public byte[] A06(int i) {
        if (i == 1) {
            AnonymousClass23X r1 = this.A05.A05[this.A02];
            int i2 = r1.A00;
            return new byte[]{(byte) r1.A02, (byte) i2, (byte) (i2 >> 8)};
        }
        AnonymousClass23X r2 = this.A05.A05[this.A02];
        int i3 = r2.A00;
        return new byte[]{(byte) r2.A02, (byte) i3, (byte) (i3 >> 8), (byte) r2.A05};
    }
}
