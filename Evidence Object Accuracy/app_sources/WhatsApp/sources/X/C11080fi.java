package X;

import androidx.window.sidecar.SidecarDisplayFeature;

/* renamed from: X.0fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11080fi extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C11080fi() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        SidecarDisplayFeature sidecarDisplayFeature = (SidecarDisplayFeature) obj;
        C16700pc.A0E(sidecarDisplayFeature, 0);
        boolean z = true;
        if (!(sidecarDisplayFeature.getType() != 1 || sidecarDisplayFeature.getRect().width() == 0 || sidecarDisplayFeature.getRect().height() == 0)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
