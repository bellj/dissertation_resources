package X;

import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;

/* renamed from: X.08K  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass08K {
    public abstract void A01(int i);

    public abstract void A02(Typeface typeface);

    public final void A00(int i) {
        new Handler(Looper.getMainLooper()).post(new Runnable(i) { // from class: X.0d1
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass08K.this.A01(this.A00);
            }
        });
    }
}
