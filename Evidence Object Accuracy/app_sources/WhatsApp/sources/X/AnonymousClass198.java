package X;

import java.util.UUID;

/* renamed from: X.198  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass198 {
    public Integer A00;
    public String A01;
    public final C15570nT A02;
    public final AnonymousClass01d A03;
    public final C15890o4 A04;
    public final C16120oU A05;

    public AnonymousClass198(C15570nT r1, AnonymousClass01d r2, C15890o4 r3, C16120oU r4) {
        this.A02 = r1;
        this.A05 = r4;
        this.A03 = r2;
        this.A04 = r3;
    }

    public void A00() {
        this.A02.A08();
        if (this.A01 != null) {
            AnonymousClass30W r1 = new AnonymousClass30W();
            A01(r1);
            r1.A00 = 3;
            this.A05.A07(r1);
            this.A01 = null;
            this.A00 = null;
        }
    }

    public final void A01(AnonymousClass30W r3) {
        r3.A03 = this.A01;
        r3.A01 = this.A00;
        Integer A00 = AnonymousClass3AY.A00(this.A03, this.A04);
        if (A00 != null) {
            r3.A02 = Long.valueOf(A00.longValue());
        }
    }

    public void A02(boolean z, int i) {
        this.A02.A08();
        this.A01 = UUID.randomUUID().toString();
        this.A00 = Integer.valueOf(i);
        AnonymousClass30W r1 = new AnonymousClass30W();
        A01(r1);
        int i2 = 2;
        if (z) {
            i2 = 1;
        }
        r1.A00 = Integer.valueOf(i2);
        this.A05.A07(r1);
    }
}
