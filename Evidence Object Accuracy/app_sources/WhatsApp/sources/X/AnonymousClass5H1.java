package X;

/* renamed from: X.5H1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5H1 extends RuntimeException {
    public static final long serialVersionUID = 4419305302960432348L;

    @Override // java.lang.Throwable
    public Throwable fillInStackTrace() {
        return this;
    }
}
