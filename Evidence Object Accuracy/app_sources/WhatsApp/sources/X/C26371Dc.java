package X;

import android.os.Handler;

/* renamed from: X.1Dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26371Dc extends AnonymousClass1DY {
    public final Handler A00;
    public final C16210od A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final C15890o4 A05;
    public final C15620nZ A06;
    public final AnonymousClass13C A07;
    public final AnonymousClass137 A08;
    public final AnonymousClass19Z A09;

    public C26371Dc(Handler handler, C16210od r2, C15570nT r3, C14830m7 r4, C16590pI r5, C15890o4 r6, C15620nZ r7, AnonymousClass13C r8, AnonymousClass137 r9, AnonymousClass19Z r10) {
        this.A00 = handler;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
        this.A09 = r10;
        this.A05 = r6;
        this.A01 = r2;
        this.A08 = r9;
        this.A07 = r8;
        this.A06 = r7;
    }
}
