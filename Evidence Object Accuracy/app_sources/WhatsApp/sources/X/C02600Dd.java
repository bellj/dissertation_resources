package X;

import android.view.View;
import androidx.appcompat.widget.ActionBarOverlayLayout;

/* renamed from: X.0Dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02600Dd extends AnonymousClass0Y9 {
    public final /* synthetic */ AnonymousClass0C4 A00;

    public C02600Dd(AnonymousClass0C4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        View view2;
        AnonymousClass0C4 r3 = this.A00;
        if (r3.A0F && (view2 = r3.A04) != null) {
            view2.setTranslationY(0.0f);
            r3.A09.setTranslationY(0.0f);
        }
        r3.A09.setVisibility(8);
        r3.A09.setTransitioning(false);
        r3.A08 = null;
        AnonymousClass02Q r1 = r3.A06;
        if (r1 != null) {
            r1.AP3(r3.A07);
            r3.A07 = null;
            r3.A06 = null;
        }
        ActionBarOverlayLayout actionBarOverlayLayout = r3.A0B;
        if (actionBarOverlayLayout != null) {
            AnonymousClass028.A0R(actionBarOverlayLayout);
        }
    }
}
