package X;

import android.view.animation.Animation;
import com.whatsapp.Conversation;
import com.whatsapp.util.Log;

/* renamed from: X.2nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57992nx extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ ViewTreeObserver$OnGlobalLayoutListenerC66323Nd A00;

    public C57992nx(ViewTreeObserver$OnGlobalLayoutListenerC66323Nd r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        Log.i("conversation/showinputextension/end");
        ViewTreeObserver$OnGlobalLayoutListenerC66323Nd r3 = this.A00;
        Conversation conversation = r3.A03;
        conversation.A10.setClipChildren(true);
        conversation.A02 = 0;
        conversation.A1g.setTranscriptMode(r3.A00);
    }
}
