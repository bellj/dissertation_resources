package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.69C  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69C implements AnonymousClass5UU {
    public C16590pI A00;
    public C18600si A01;
    public C30931Zj A02 = C117305Zk.A0V("PaymentCommonDeviceIdManager", "infra");

    public AnonymousClass69C(C16590pI r3, C18600si r4) {
        this.A00 = r3;
        this.A01 = r4;
    }

    public String A00() {
        Pair pair;
        C30931Zj r2 = this.A02;
        r2.A04("PaymentDeviceId: getid_v2()");
        Context context = this.A00.A00;
        if (Build.VERSION.SDK_INT >= 26) {
            r2.A04("PaymentDeviceId: still fallback to v1");
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        r2.A04("PaymentDeviceId: generate id for v2");
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (string == null) {
            string = "";
        }
        try {
            String charsString = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toCharsString();
            if (!TextUtils.isEmpty(charsString)) {
                StringBuilder A0j = C12960it.A0j(string);
                A0j.append("-");
                A0j.append(charsString);
                string = A0j.toString();
            }
            pair = new Pair(string, MessageDigest.getInstance("SHA-1").digest(C117315Zl.A0a(string)));
        } catch (PackageManager.NameNotFoundException | UnsupportedEncodingException | NullPointerException | NoSuchAlgorithmException unused) {
            pair = new Pair(string, null);
        }
        String str = (String) pair.first;
        byte[] bArr = (byte[]) pair.second;
        if (bArr == null) {
            return str;
        }
        StringBuilder A0h = C12960it.A0h();
        for (byte b : bArr) {
            Object[] A1b = C12970iu.A1b();
            A1b[0] = Byte.valueOf(b);
            A0h.append(String.format("%02X", A1b));
        }
        return A0h.toString();
    }

    @Override // X.AnonymousClass5UU
    public String getId() {
        C30931Zj r2;
        StringBuilder A0h;
        String str;
        C18600si r22 = this.A01;
        String A0p = C12980iv.A0p(r22.A01(), "payments_device_id");
        if (!TextUtils.isEmpty(A0p)) {
            r2 = this.A02;
            A0h = C12960it.A0h();
            str = "PaymentDeviceId: from cache: ";
        } else {
            A0p = A00();
            C12970iu.A1D(C117295Zj.A05(r22), "payments_device_id", A0p);
            r2 = this.A02;
            A0h = C12960it.A0h();
            str = "PaymentDeviceId: generated: ";
        }
        A0h.append(str);
        r2.A04(C12960it.A0d(A0p, A0h));
        return A0p;
    }
}
