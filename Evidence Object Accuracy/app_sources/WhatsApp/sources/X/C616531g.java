package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.whatsapp.R;
import java.util.HashSet;

/* renamed from: X.31g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616531g extends C616631i {
    public int A00;
    public Drawable A01;
    public C39341ph A02;
    public AnonymousClass2BB A03;
    public C75173jQ A04;
    public HashSet A05;
    public boolean A06;
    public final Matrix A07 = C13000ix.A01();
    public final Paint A08;

    public C616531g(Context context, AnonymousClass2BB r5, HashSet hashSet, int i, boolean z) {
        super(context);
        A00();
        Paint A0F = C12990iw.A0F();
        this.A08 = A0F;
        this.A03 = r5;
        this.A05 = hashSet;
        this.A00 = i;
        C12980iv.A12(context, A0F, z ? R.color.wds_emerald_500 : R.color.thumbnail_selected_stroke);
        A0F.setStrokeWidth((float) context.getResources().getDimensionPixelSize(R.dimen.gallery_picker_preview_selection_border));
        C12990iw.A13(A0F);
        A0F.setAntiAlias(true);
        setId(R.id.thumb);
        C12990iw.A1E(this);
    }

    @Override // X.AnonymousClass2T3
    public Uri getUri() {
        return this.A02.A0G;
    }

    public C75173jQ getViewHolder() {
        return this.A04;
    }

    @Override // X.AnonymousClass2T3, android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.A03.A06 != this && !this.A05.contains(this.A02)) {
            canvas.save();
            if (this.A02.A01() != 0) {
                int A01 = this.A02.A01();
                Matrix matrix = this.A07;
                matrix.setRotate((float) A01, (float) (getWidth() >> 1), (float) C13000ix.A00(this));
                canvas.concat(matrix);
            }
            super.onDraw(canvas);
            canvas.restore();
            Drawable drawable = this.A01;
            if (drawable != null) {
                int intrinsicHeight = drawable.getIntrinsicHeight() >> 2;
                this.A01.setBounds(intrinsicHeight, (getHeight() - this.A01.getIntrinsicHeight()) - intrinsicHeight, this.A01.getIntrinsicWidth() + intrinsicHeight, getHeight() - intrinsicHeight);
                this.A01.draw(canvas);
            }
            if (isPressed() || isSelected()) {
                canvas.drawRect(0.0f, 0.0f, C12990iw.A02(this), C12990iw.A03(this), this.A08);
            }
        }
    }

    @Override // X.AnonymousClass2T3, android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3 = this.A00;
        setMeasuredDimension(i3, i3);
    }

    public void setIcon(Drawable drawable) {
        this.A01 = drawable;
    }

    public void setItem(C39341ph r1) {
        this.A02 = r1;
    }

    public void setViewHolder(C75173jQ r1) {
        this.A04 = r1;
    }
}
