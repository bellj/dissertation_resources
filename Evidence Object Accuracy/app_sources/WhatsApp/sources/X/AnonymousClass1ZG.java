package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZG implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100284lf();
    public final String A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZG(Parcel parcel) {
        this.A00 = parcel.readString();
    }

    public AnonymousClass1ZG(String str) {
        this.A00 = str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
    }
}
