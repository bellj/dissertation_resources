package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import java.lang.ref.WeakReference;
import java.util.Arrays;

/* renamed from: X.2Zd  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Zd extends Drawable implements Drawable.Callback, AbstractC013806l {
    public static final int[] A0w = {16842910};
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public float A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public int A0J;
    public int A0K;
    public ColorStateList A0L;
    public ColorStateList A0M;
    public ColorStateList A0N;
    public ColorStateList A0O;
    public ColorStateList A0P;
    public ColorStateList A0Q;
    public ColorStateList A0R;
    public ColorFilter A0S;
    public PorterDuff.Mode A0T;
    public PorterDuffColorFilter A0U;
    public Drawable A0V;
    public Drawable A0W;
    public Drawable A0X;
    public TextUtils.TruncateAt A0Y;
    public C50612Qf A0Z;
    public C50612Qf A0a;
    public C64353Ff A0b;
    public CharSequence A0c;
    public CharSequence A0d;
    public CharSequence A0e;
    public WeakReference A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public boolean A0l;
    public boolean A0m;
    public boolean A0n;
    public int[] A0o;
    public final Context A0p;
    public final Paint.FontMetrics A0q;
    public final Paint A0r;
    public final PointF A0s;
    public final RectF A0t;
    public final TextPaint A0u;
    public final AnonymousClass08K A0v = new C74283hj(this);

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public AnonymousClass2Zd(Context context) {
        TextPaint textPaint = new TextPaint(1);
        this.A0u = textPaint;
        this.A0r = C12990iw.A0G(1);
        this.A0q = new Paint.FontMetrics();
        this.A0t = C12980iv.A0K();
        this.A0s = new PointF();
        this.A0E = 255;
        this.A0T = PorterDuff.Mode.SRC_IN;
        this.A0f = C12970iu.A10(null);
        this.A0m = true;
        this.A0p = context;
        this.A0d = "";
        textPaint.density = C12960it.A01(context);
        int[] iArr = A0w;
        setState(iArr);
        if (!Arrays.equals(this.A0o, iArr)) {
            this.A0o = iArr;
            if (A0S()) {
                A0T(getState(), iArr);
            }
        }
        this.A0l = true;
    }

    public static float A00(AnonymousClass2Zd r0, int i) {
        return r0.A0p.getResources().getDimension(i);
    }

    public static void A01(AnonymousClass2Zd r1) {
        r1.onStateChange(r1.getState());
    }

    public float A02() {
        if (A0R() || A0Q()) {
            return this.A0A + this.A02 + this.A09;
        }
        return 0.0f;
    }

    public final float A03() {
        if (A0S()) {
            return this.A08 + this.A07 + this.A06;
        }
        return 0.0f;
    }

    public Drawable A04() {
        Drawable drawable = this.A0X;
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof AbstractC016707w) {
            return ((C016607v) ((AbstractC016707w) drawable)).A02;
        }
        return drawable;
    }

    public void A05() {
        AbstractC115795Sy r0 = (AbstractC115795Sy) this.A0f.get();
        if (r0 != null) {
            r0.ANx();
        }
    }

    public void A06(float f) {
        if (this.A02 != f) {
            float A02 = A02();
            this.A02 = f;
            float A022 = A02();
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A07(float f) {
        if (this.A06 != f) {
            this.A06 = f;
            invalidateSelf();
            if (A0S()) {
                A05();
            }
        }
    }

    public void A08(float f) {
        if (this.A07 != f) {
            this.A07 = f;
            invalidateSelf();
            if (A0S()) {
                A05();
            }
        }
    }

    public void A09(float f) {
        if (this.A08 != f) {
            this.A08 = f;
            invalidateSelf();
            if (A0S()) {
                A05();
            }
        }
    }

    public void A0A(float f) {
        if (this.A09 != f) {
            float A02 = A02();
            this.A09 = f;
            float A022 = A02();
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A0B(float f) {
        if (this.A0A != f) {
            float A02 = A02();
            this.A0A = f;
            float A022 = A02();
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A0C(ColorStateList colorStateList) {
        if (this.A0M != colorStateList) {
            this.A0M = colorStateList;
            if (A0R()) {
                C015607k.A04(colorStateList, this.A0W);
            }
            A01(this);
        }
    }

    public void A0D(ColorStateList colorStateList) {
        if (this.A0O != colorStateList) {
            this.A0O = colorStateList;
            if (A0S()) {
                C015607k.A04(colorStateList, this.A0X);
            }
            A01(this);
        }
    }

    public void A0E(ColorStateList colorStateList) {
        ColorStateList colorStateList2;
        if (this.A0Q != colorStateList) {
            this.A0Q = colorStateList;
            if (this.A0n) {
                colorStateList2 = AnonymousClass2RB.A02(colorStateList);
            } else {
                colorStateList2 = null;
            }
            this.A0P = colorStateList2;
            A01(this);
        }
    }

    public final void A0F(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (A0R() || A0Q()) {
            float f = this.A04 + this.A0A;
            if (C015607k.A01(this) == 0) {
                float f2 = ((float) rect.left) + f;
                rectF.left = f2;
                rectF.right = f2 + this.A02;
            } else {
                float f3 = ((float) rect.right) - f;
                rectF.right = f3;
                rectF.left = f3 - this.A02;
            }
            float exactCenterY = rect.exactCenterY();
            float f4 = this.A02;
            float f5 = exactCenterY - (f4 / 2.0f);
            rectF.top = f5;
            rectF.bottom = f5 + f4;
        }
    }

    public void A0G(Drawable drawable) {
        if (this.A0V != drawable) {
            float A02 = A02();
            this.A0V = drawable;
            float A022 = A02();
            C12990iw.A16(drawable);
            A0J(this.A0V);
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A0H(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.A0W;
        if (drawable3 == null) {
            drawable3 = null;
        } else if (drawable3 instanceof AbstractC016707w) {
            drawable3 = ((C016607v) ((AbstractC016707w) drawable3)).A02;
        }
        if (drawable3 != drawable) {
            float A02 = A02();
            if (drawable != null) {
                drawable2 = C015607k.A03(drawable).mutate();
            } else {
                drawable2 = null;
            }
            this.A0W = drawable2;
            float A022 = A02();
            C12990iw.A16(drawable3);
            if (A0R()) {
                A0J(this.A0W);
            }
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A0I(Drawable drawable) {
        Drawable drawable2;
        Drawable A04 = A04();
        if (A04 != drawable) {
            float A03 = A03();
            if (drawable != null) {
                drawable2 = C015607k.A03(drawable).mutate();
            } else {
                drawable2 = null;
            }
            this.A0X = drawable2;
            float A032 = A03();
            C12990iw.A16(A04);
            if (A0S()) {
                A0J(this.A0X);
            }
            invalidateSelf();
            if (A03 != A032) {
                A05();
            }
        }
    }

    public final void A0J(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            C015607k.A0D(C015607k.A01(this), drawable);
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            Drawable drawable2 = this.A0X;
            boolean isStateful = drawable.isStateful();
            if (drawable == drawable2) {
                if (isStateful) {
                    drawable.setState(this.A0o);
                }
                C015607k.A04(this.A0O, drawable);
            } else if (isStateful) {
                drawable.setState(getState());
            }
        }
    }

    public void A0K(C64353Ff r4) {
        if (this.A0b != r4) {
            this.A0b = r4;
            if (r4 != null) {
                r4.A02(this.A0p, this.A0u, this.A0v);
                this.A0m = true;
            }
            A01(this);
            A05();
        }
    }

    public void A0L(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (this.A0d != charSequence) {
            this.A0d = charSequence;
            AnonymousClass02S A00 = new AnonymousClass06A().A00();
            this.A0e = A00.A02(A00.A01, charSequence);
            this.A0m = true;
            invalidateSelf();
            A05();
        }
    }

    public void A0M(boolean z) {
        if (this.A0g != z) {
            this.A0g = z;
            float A02 = A02();
            if (!z && this.A0k) {
                this.A0k = false;
            }
            float A022 = A02();
            invalidateSelf();
            if (A02 != A022) {
                A05();
            }
        }
    }

    public void A0N(boolean z) {
        if (this.A0h != z) {
            boolean A0Q = A0Q();
            this.A0h = z;
            boolean A0Q2 = A0Q();
            if (A0Q != A0Q2) {
                Drawable drawable = this.A0V;
                if (A0Q2) {
                    A0J(drawable);
                } else {
                    C12990iw.A16(drawable);
                }
                invalidateSelf();
                A05();
            }
        }
    }

    public void A0O(boolean z) {
        if (this.A0i != z) {
            boolean A0R = A0R();
            this.A0i = z;
            boolean A0R2 = A0R();
            if (A0R != A0R2) {
                Drawable drawable = this.A0W;
                if (A0R2) {
                    A0J(drawable);
                } else {
                    C12990iw.A16(drawable);
                }
                invalidateSelf();
                A05();
            }
        }
    }

    public void A0P(boolean z) {
        if (this.A0j != z) {
            boolean A0S = A0S();
            this.A0j = z;
            boolean A0S2 = A0S();
            if (A0S != A0S2) {
                Drawable drawable = this.A0X;
                if (A0S2) {
                    A0J(drawable);
                } else {
                    C12990iw.A16(drawable);
                }
                invalidateSelf();
                A05();
            }
        }
    }

    public final boolean A0Q() {
        return this.A0h && this.A0V != null && this.A0k;
    }

    public final boolean A0R() {
        return this.A0i && this.A0W != null;
    }

    public final boolean A0S() {
        return this.A0j && this.A0X != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0066, code lost:
        if (r8.A0g == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x007f, code lost:
        if (r1 == A02()) goto L_0x0081;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0T(int[] r9, int[] r10) {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Zd.A0T(int[], int[]):boolean");
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int i;
        float f;
        float f2;
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && (i = this.A0E) != 0) {
            int i2 = 0;
            if (i < 255) {
                float f3 = (float) bounds.left;
                float f4 = (float) bounds.top;
                float f5 = (float) bounds.right;
                float f6 = (float) bounds.bottom;
                if (Build.VERSION.SDK_INT > 21) {
                    i2 = canvas.saveLayerAlpha(f3, f4, f5, f6, i);
                } else {
                    i2 = canvas.saveLayerAlpha(f3, f4, f5, f6, i, 31);
                }
            }
            Paint paint = this.A0r;
            C12970iu.A16(this.A0F, paint);
            ColorFilter colorFilter = this.A0S;
            if (colorFilter == null) {
                colorFilter = this.A0U;
            }
            paint.setColorFilter(colorFilter);
            RectF rectF = this.A0t;
            rectF.set(bounds);
            float f7 = this.A00;
            canvas.drawRoundRect(rectF, f7, f7, paint);
            if (this.A05 > 0.0f) {
                paint.setColor(this.A0G);
                C12990iw.A13(paint);
                ColorFilter colorFilter2 = this.A0S;
                if (colorFilter2 == null) {
                    colorFilter2 = this.A0U;
                }
                paint.setColorFilter(colorFilter2);
                float f8 = this.A05 / 2.0f;
                rectF.set(((float) bounds.left) + f8, ((float) bounds.top) + f8, ((float) bounds.right) - f8, ((float) bounds.bottom) - f8);
                float f9 = this.A00 - (this.A05 / 2.0f);
                canvas.drawRoundRect(rectF, f9, f9, paint);
            }
            C12970iu.A16(this.A0H, paint);
            rectF.set(bounds);
            float f10 = this.A00;
            canvas.drawRoundRect(rectF, f10, f10, paint);
            if (A0R()) {
                A0F(bounds, rectF);
                float f11 = rectF.left;
                float f12 = rectF.top;
                canvas.translate(f11, f12);
                this.A0W.setBounds(0, 0, (int) rectF.width(), (int) rectF.height());
                this.A0W.draw(canvas);
                canvas.translate(-f11, -f12);
            }
            if (A0Q()) {
                A0F(bounds, rectF);
                float f13 = rectF.left;
                float f14 = rectF.top;
                canvas.translate(f13, f14);
                this.A0V.setBounds(0, 0, (int) rectF.width(), (int) rectF.height());
                this.A0V.draw(canvas);
                canvas.translate(-f13, -f14);
            }
            if (this.A0l && this.A0e != null) {
                PointF pointF = this.A0s;
                pointF.set(0.0f, 0.0f);
                Paint.Align align = Paint.Align.LEFT;
                if (this.A0e != null) {
                    float A02 = this.A04 + A02() + this.A0C;
                    if (C015607k.A01(this) == 0) {
                        pointF.x = ((float) bounds.left) + A02;
                    } else {
                        pointF.x = ((float) bounds.right) - A02;
                        align = Paint.Align.RIGHT;
                    }
                    float centerY = (float) bounds.centerY();
                    TextPaint textPaint = this.A0u;
                    Paint.FontMetrics fontMetrics = this.A0q;
                    textPaint.getFontMetrics(fontMetrics);
                    pointF.y = centerY - ((fontMetrics.descent + fontMetrics.ascent) / 2.0f);
                }
                rectF.setEmpty();
                if (this.A0e != null) {
                    float A022 = this.A04 + A02() + this.A0C;
                    float A03 = this.A01 + A03() + this.A0B;
                    int A01 = C015607k.A01(this);
                    float f15 = (float) bounds.left;
                    if (A01 == 0) {
                        rectF.left = f15 + A022;
                        f2 = ((float) bounds.right) - A03;
                    } else {
                        rectF.left = f15 + A03;
                        f2 = ((float) bounds.right) - A022;
                    }
                    rectF.right = f2;
                    rectF.top = (float) bounds.top;
                    rectF.bottom = (float) bounds.bottom;
                }
                if (this.A0b != null) {
                    TextPaint textPaint2 = this.A0u;
                    textPaint2.drawableState = getState();
                    this.A0b.A01(this.A0p, textPaint2, this.A0v);
                }
                TextPaint textPaint3 = this.A0u;
                textPaint3.setTextAlign(align);
                if (!this.A0m) {
                    f = this.A0D;
                } else {
                    CharSequence charSequence = this.A0e;
                    f = 0.0f;
                    if (charSequence != null) {
                        f = textPaint3.measureText(charSequence, 0, charSequence.length());
                    }
                    this.A0D = f;
                    this.A0m = false;
                }
                int i3 = 0;
                boolean z = false;
                if (Math.round(f) > Math.round(rectF.width())) {
                    z = true;
                    i3 = canvas.save();
                    canvas.clipRect(rectF);
                }
                CharSequence charSequence2 = this.A0e;
                if (z && this.A0Y != null) {
                    charSequence2 = TextUtils.ellipsize(charSequence2, textPaint3, rectF.width(), this.A0Y);
                }
                canvas.drawText(charSequence2, 0, charSequence2.length(), pointF.x, pointF.y, textPaint3);
                if (z) {
                    canvas.restoreToCount(i3);
                }
            }
            if (A0S()) {
                rectF.setEmpty();
                if (A0S()) {
                    float f16 = this.A01 + this.A06;
                    if (C015607k.A01(this) == 0) {
                        float f17 = ((float) bounds.right) - f16;
                        rectF.right = f17;
                        rectF.left = f17 - this.A07;
                    } else {
                        float f18 = ((float) bounds.left) + f16;
                        rectF.left = f18;
                        rectF.right = f18 + this.A07;
                    }
                    float exactCenterY = bounds.exactCenterY();
                    float f19 = this.A07;
                    float f20 = exactCenterY - (f19 / 2.0f);
                    rectF.top = f20;
                    rectF.bottom = f20 + f19;
                }
                float f21 = rectF.left;
                float f22 = rectF.top;
                canvas.translate(f21, f22);
                this.A0X.setBounds(0, 0, (int) rectF.width(), (int) rectF.height());
                this.A0X.draw(canvas);
                canvas.translate(-f21, -f22);
            }
            if (this.A0E < 255) {
                canvas.restoreToCount(i2);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A0E;
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        return this.A0S;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return (int) this.A03;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        float measureText;
        float A02 = this.A04 + A02() + this.A0C;
        if (!this.A0m) {
            measureText = this.A0D;
        } else {
            CharSequence charSequence = this.A0e;
            if (charSequence == null) {
                measureText = 0.0f;
            } else {
                measureText = this.A0u.measureText(charSequence, 0, charSequence.length());
            }
            this.A0D = measureText;
            this.A0m = false;
        }
        return Math.min(Math.round(A02 + measureText + this.A0B + A03() + this.A01), this.A0K);
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.A00);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), (int) this.A03, this.A00);
        }
        outline.setAlpha(((float) this.A0E) / 255.0f);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3 = this.A0L;
        if (colorStateList3 != null && colorStateList3.isStateful()) {
            return true;
        }
        ColorStateList colorStateList4 = this.A0N;
        if (colorStateList4 != null && colorStateList4.isStateful()) {
            return true;
        }
        if (this.A0n && (colorStateList2 = this.A0P) != null && colorStateList2.isStateful()) {
            return true;
        }
        C64353Ff r0 = this.A0b;
        if (r0 != null && (colorStateList = r0.A0A) != null && colorStateList.isStateful()) {
            return true;
        }
        if (this.A0h && this.A0V != null && this.A0g) {
            return true;
        }
        Drawable drawable = this.A0W;
        if (drawable != null && drawable.isStateful()) {
            return true;
        }
        Drawable drawable2 = this.A0V;
        if (drawable2 != null && drawable2.isStateful()) {
            return true;
        }
        ColorStateList colorStateList5 = this.A0R;
        if (colorStateList5 == null || !colorStateList5.isStateful()) {
            return false;
        }
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i);
        if (A0R()) {
            onLayoutDirectionChanged |= this.A0W.setLayoutDirection(i);
        }
        if (A0Q()) {
            onLayoutDirectionChanged |= this.A0V.setLayoutDirection(i);
        }
        if (A0S()) {
            onLayoutDirectionChanged |= this.A0X.setLayoutDirection(i);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        boolean onLevelChange = super.onLevelChange(i);
        if (A0R()) {
            onLevelChange |= this.A0W.setLevel(i);
        }
        if (A0Q()) {
            onLevelChange |= this.A0V.setLevel(i);
        }
        if (A0S()) {
            onLevelChange |= this.A0X.setLevel(i);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        return A0T(iArr, this.A0o);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (this.A0E != i) {
            this.A0E = i;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.A0S != colorFilter) {
            this.A0S = colorFilter;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintList(ColorStateList colorStateList) {
        if (this.A0R != colorStateList) {
            this.A0R = colorStateList;
            A01(this);
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintMode(PorterDuff.Mode mode) {
        PorterDuffColorFilter porterDuffColorFilter;
        if (this.A0T != mode) {
            this.A0T = mode;
            ColorStateList colorStateList = this.A0R;
            if (colorStateList == null || mode == null) {
                porterDuffColorFilter = null;
            } else {
                porterDuffColorFilter = new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
            }
            this.A0U = porterDuffColorFilter;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (A0R()) {
            visible |= this.A0W.setVisible(z, z2);
        }
        if (A0Q()) {
            visible |= this.A0V.setVisible(z, z2);
        }
        if (A0S()) {
            visible |= this.A0X.setVisible(z, z2);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }
}
