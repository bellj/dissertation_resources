package X;

import android.content.Intent;
import com.whatsapp.payments.ui.NoviAmountEntryActivity;
import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;

/* renamed from: X.6EY  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6EY implements AbstractC14590lg {
    public final /* synthetic */ C1316163l A00;
    public final /* synthetic */ NoviWithdrawCashStoreLocatorActivity A01;

    public /* synthetic */ AnonymousClass6EY(C1316163l r1, NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity) {
        this.A01 = noviWithdrawCashStoreLocatorActivity;
        this.A00 = r1;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        C1315463e A02;
        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = this.A01;
        C1316163l r4 = this.A00;
        AbstractC28901Pl r7 = (AbstractC28901Pl) obj;
        if (r7 != null && (A02 = AnonymousClass61F.A02(r7)) != null) {
            Intent A0D = C12990iw.A0D(noviWithdrawCashStoreLocatorActivity, NoviAmountEntryActivity.class);
            A0D.putExtra("withdrawal_type", 1);
            A0D.putExtra("account_info", A02);
            A0D.putExtra("withdraw_store_info", r4);
            A0D.putExtra("amount_entry_type", "withdraw");
            noviWithdrawCashStoreLocatorActivity.startActivityForResult(A0D, 1);
        }
    }
}
