package X;

import com.whatsapp.R;

/* renamed from: X.48N  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass48N extends AbstractC93424a9 {
    public AnonymousClass48N() {
        super(new C92794Xl(R.dimen.wds_badge_icon_dimen_12, R.dimen.wds_badge_icon_dimen_14, R.dimen.wds_badge_icon_dimen_16, R.dimen.wds_badge_icon_dimen_24), new AnonymousClass48J());
    }
}
