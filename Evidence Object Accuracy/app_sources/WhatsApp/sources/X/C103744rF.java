package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;

/* renamed from: X.4rF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C103744rF implements AnonymousClass07L {
    public final /* synthetic */ CatalogSearchFragment A00;

    public C103744rF(CatalogSearchFragment catalogSearchFragment) {
        this.A00 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        C16700pc.A0E(str, 0);
        ((CatalogSearchViewModel) this.A00.A0a.getValue()).A05(str);
        return true;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        C16700pc.A0E(str, 0);
        this.A00.A1D(str);
        return true;
    }
}
