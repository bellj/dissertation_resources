package X;

import java.util.Arrays;

/* renamed from: X.5NP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NP extends AnonymousClass1TL implements AnonymousClass5VP {
    public final byte[] A00;

    public AnonymousClass5NP(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 12, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public String toString() {
        return AGy();
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NP)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NP) r3).A00);
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        byte[] bArr = this.A00;
        int length = bArr.length;
        char[] cArr = new char[length];
        int i = 0;
        int i2 = 0;
        loop0: while (true) {
            if (i < length) {
                int i3 = i + 1;
                byte b = bArr[i];
                if (b >= 0) {
                    if (i2 >= length) {
                        break;
                    }
                    i2++;
                    cArr[i2] = (char) b;
                    i = i3;
                } else {
                    short s = AnonymousClass4HJ.A01[b & Byte.MAX_VALUE];
                    int i4 = s >>> 8;
                    byte b2 = (byte) s;
                    while (true) {
                        if (b2 < 0) {
                            if (b2 != -2) {
                                if (i4 > 65535) {
                                    if (i2 >= length - 1) {
                                        break;
                                    }
                                    int i5 = i2 + 1;
                                    cArr[i2] = (char) ((i4 >>> 10) + 55232);
                                    i2 = i5 + 1;
                                    cArr[i5] = (char) (56320 | (i4 & 1023));
                                    i = i3;
                                } else if (i2 >= length) {
                                    break;
                                } else {
                                    i2++;
                                    cArr[i2] = (char) i4;
                                    i = i3;
                                }
                            } else {
                                break;
                            }
                        } else if (i3 >= length) {
                            break loop0;
                        } else {
                            i3++;
                            byte b3 = bArr[i3];
                            i4 = (i4 << 6) | (b3 & 63);
                            b2 = AnonymousClass4HJ.A00[b2 + ((b3 & 255) >>> 4)];
                        }
                    }
                }
            } else if (i2 >= 0) {
                return new String(cArr, 0, i2);
            }
        }
        throw C12970iu.A0f("Invalid UTF-8 input");
    }
}
