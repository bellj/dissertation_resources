package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.RandomAccess;

/* renamed from: X.3rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80243rw<E> extends AnonymousClass5I0<E> implements RandomAccess {
    public static final C80243rw A02;
    public int A00;
    public Object[] A01;

    static {
        C80243rw r0 = new C80243rw(new Object[0], 0);
        A02 = r0;
        ((AnonymousClass5I0) r0).A00 = false;
    }

    public C80243rw() {
        this(new Object[10], 0);
    }

    public C80243rw(Object[] objArr, int i) {
        this.A01 = objArr;
        this.A00 = i;
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= this.A00) {
            return new C80243rw(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final void add(int i, Object obj) {
        int i2;
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        Object[] objArr = this.A01;
        if (i2 < objArr.length) {
            C72463ee.A0T(objArr, i, i2);
        } else {
            Object[] objArr2 = new Object[((i2 * 3) >> 1) + 1];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.A01, i, objArr2, i + 1, this.A00 - i);
            this.A01 = objArr2;
        }
        this.A01[i] = obj;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean add(Object obj) {
        A02();
        int i = this.A00;
        Object[] objArr = this.A01;
        if (i == objArr.length) {
            objArr = Arrays.copyOf(objArr, ((i * 3) >> 1) + 1);
            this.A01 = objArr;
        }
        int i2 = this.A00;
        this.A00 = i2 + 1;
        objArr[i2] = obj;
        ((AbstractList) this).modCount++;
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return this.A01[i];
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        Object[] objArr = this.A01;
        Object obj = objArr[i];
        AnonymousClass5I0.A01(objArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return obj;
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object set(int i, Object obj) {
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        Object[] objArr = this.A01;
        Object obj2 = objArr[i];
        objArr[i] = obj;
        ((AbstractList) this).modCount++;
        return obj2;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }
}
