package X;

/* renamed from: X.1b3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31751b3 {
    public final C31641as A00;
    public final C31741b2 A01;
    public final C31491ad A02;
    public final C31731b1 A03;
    public final C31731b1 A04;
    public final AbstractC31681aw A05;

    public C31751b3(C31641as r3, C31741b2 r4, C31491ad r5, C31731b1 r6, C31731b1 r7, AbstractC31681aw r8) {
        this.A01 = r4;
        this.A04 = r6;
        this.A03 = r7;
        this.A05 = r8;
        this.A00 = r3;
        this.A02 = r5;
        if (r3 == null) {
            throw new IllegalArgumentException("Null value!");
        }
    }
}
