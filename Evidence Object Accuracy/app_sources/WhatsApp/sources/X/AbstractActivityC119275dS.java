package X;

/* renamed from: X.5dS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119275dS extends ActivityC13790kL {
    public AbstractActivityC119275dS() {
        C117295Zj.A0p(this, 104);
    }

    public static void A02(AnonymousClass01J r1, C249317l r2, AbstractView$OnClickListenerC121765jx r3) {
        ((ActivityC13790kL) r3).A08 = r2;
        r3.A05 = (C14900mE) r1.A8X.get();
        r3.A0H = (AbstractC14440lR) r1.ANe.get();
        r3.A07 = (C18790t3) r1.AJw.get();
        r3.A06 = (AnonymousClass19Y) r1.AI6.get();
        r3.A08 = (AnonymousClass018) r1.ANb.get();
        r3.A0D = (C17070qD) r1.AFC.get();
        r3.A0A = (C21860y6) r1.AE6.get();
        r3.A0C = (C17900ra) r1.AF5.get();
        r3.A0B = (C25861Bc) r1.AEh.get();
    }

    public static void A03(AnonymousClass01J r1, AbstractActivityC121755jw r2) {
        r2.A0C = (C18590sh) r1.AES.get();
        r2.A01 = (C15650ng) r1.A4m.get();
        r2.A05 = (C18600si) r1.AEo.get();
        r2.A06 = (C18610sj) r1.AF0.get();
        r2.A09 = (C129095xA) r1.ACy.get();
        r2.A02 = (C129925yW) r1.AEW.get();
        r2.A04 = (C20390vg) r1.AEi.get();
        r2.A08 = (C18620sk) r1.AFB.get();
        r2.A03 = (C18650sn) r1.AEe.get();
        r2.A07 = (AnonymousClass61M) r1.AF1.get();
    }
}
