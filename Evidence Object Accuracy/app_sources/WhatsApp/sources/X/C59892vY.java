package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59892vY extends AbstractC37191le {
    public final WaTextView A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final WaTextView A03;
    public final C251118d A04;

    public C59892vY(View view, C251118d r3) {
        super(view);
        this.A04 = r3;
        this.A01 = C12960it.A0N(view, R.id.error_message);
        this.A02 = C12960it.A0N(view, R.id.retry_button);
        this.A03 = C12960it.A0N(view, R.id.settings_btn);
        this.A00 = C12960it.A0N(view, R.id.fallback_action_button);
    }
}
