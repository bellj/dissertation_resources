package X;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Looper;
import com.google.android.gms.location.LocationRequest;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.15p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244615p {
    public LocationManager A00;
    public AnonymousClass1U8 A01;
    public Map A02;
    public final C16210od A03;
    public final AbstractC15710nm A04;
    public final AnonymousClass01d A05;
    public final C16590pI A06;
    public final C15890o4 A07;
    public volatile boolean A08;

    public C244615p(C16210od r1, AbstractC15710nm r2, AnonymousClass01d r3, C16590pI r4, C15890o4 r5) {
        this.A06 = r4;
        this.A04 = r2;
        this.A07 = r5;
        this.A05 = r3;
        this.A03 = r1;
    }

    public static LocationRequest A00(AnonymousClass1U2 r7) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.A08 = true;
        int i = r7.A01;
        int i2 = 100;
        if ((i & 1) == 0) {
            i2 = 105;
            if ((i & 2) != 0) {
                i2 = 102;
            }
        }
        locationRequest.A01 = i2;
        long j = r7.A03;
        LocationRequest.A00(j);
        locationRequest.A03 = j;
        if (!locationRequest.A07) {
            locationRequest.A04 = (long) (((double) j) / 6.0d);
        }
        long j2 = r7.A02;
        LocationRequest.A00(j2);
        locationRequest.A07 = true;
        locationRequest.A04 = j2;
        float f = r7.A00;
        if (f >= 0.0f) {
            locationRequest.A00 = f;
            return locationRequest;
        }
        StringBuilder sb = new StringBuilder(37);
        sb.append("invalid displacement: ");
        sb.append(f);
        throw new IllegalArgumentException(sb.toString());
    }

    public Location A01(String str) {
        A03();
        Location A02 = A02(str, 1);
        Location A022 = A02(str, 2);
        if (A02 == null || (A022 != null && A02.getTime() <= A022.getTime() - 20000)) {
            A02 = A022;
            if (A022 == null) {
                return A02;
            }
        }
        if (A02.getTime() + 7200000 < System.currentTimeMillis()) {
            return null;
        }
        return A02;
    }

    public Location A02(String str, int i) {
        LocationManager locationManager;
        String str2;
        C15890o4 r3 = this.A07;
        if (r3.A03()) {
            StringBuilder sb = new StringBuilder("FusedLocationManager/getLocation:");
            sb.append(str);
            Log.i(sb.toString());
            A03();
            A06(str);
            AnonymousClass1U8 r0 = this.A01;
            if (r0 != null && r0.A0A()) {
                return AnonymousClass1U9.A03.ADn(this.A01);
            }
            if (this.A00 != null) {
                if (i == 1) {
                    if (r3.A02("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        locationManager = this.A00;
                        str2 = "gps";
                        return locationManager.getLastKnownLocation(str2);
                    }
                } else if (r3.A02("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    locationManager = this.A00;
                    str2 = "network";
                    return locationManager.getLastKnownLocation(str2);
                }
            }
            return null;
        }
        StringBuilder sb2 = new StringBuilder("FusedLocationManager/getLastKnownLocation/do not have location permissions context:");
        sb2.append(str);
        Log.w(sb2.toString());
        return null;
    }

    public synchronized void A03() {
        AnonymousClass1U8 r0;
        if (this.A00 == null) {
            Context context = this.A06.A00;
            if (AnonymousClass1UB.A00(context) == 0) {
                AnonymousClass1UC r2 = new AnonymousClass1UC(this);
                this.A02 = new HashMap();
                AnonymousClass1UD r1 = new AnonymousClass1UD(context);
                r1.A01(AnonymousClass1U9.A02);
                r1.A06.add(r2);
                r1.A07.add(r2);
                r0 = r1.A00();
            } else {
                r0 = null;
                this.A02 = null;
            }
            this.A01 = r0;
            this.A00 = this.A05.A0F();
        }
    }

    public void A04(LocationListener locationListener) {
        A03();
        if (this.A01 != null) {
            AnonymousClass1U2 r2 = (AnonymousClass1U2) this.A02.remove(locationListener);
            if (r2 != null) {
                if (this.A01.A0A()) {
                    AnonymousClass1U8 r1 = this.A01;
                    r1.A06(new AnonymousClass1UF(r1, r2));
                }
                if (this.A02.isEmpty()) {
                    this.A01.A09();
                }
            }
        } else if (this.A00 != null && this.A07.A03()) {
            this.A00.removeUpdates(locationListener);
        }
    }

    public void A05(LocationListener locationListener, String str, float f, int i, long j, long j2) {
        C15890o4 r3 = this.A07;
        if (r3.A03()) {
            A03();
            A06(str);
            if (this.A01 != null) {
                if (this.A02.isEmpty()) {
                    this.A01.A08();
                }
                AnonymousClass1U2 r4 = new AnonymousClass1U2(locationListener, f, i, j, j2);
                this.A02.put(locationListener, r4);
                if (this.A01.A0A()) {
                    LocationRequest A00 = A00(r4);
                    AnonymousClass1U8 r2 = this.A01;
                    C13020j0.A02(Looper.myLooper(), "Calling thread must be a prepared Looper thread.");
                    r2.A06(new AnonymousClass1UM(r2, r4, A00));
                    return;
                }
                return;
            }
            if ((i & 1) != 0) {
                try {
                    if (this.A00 == null || r3.A02("android.permission.ACCESS_FINE_LOCATION") != 0) {
                        Log.w("FusedLocationManager/requestLocationUpdates/do not have fine location permission");
                    } else {
                        this.A00.requestLocationUpdates("gps", j, f, locationListener);
                    }
                } catch (RuntimeException e) {
                    Log.e("FusedLocationManager/requestLocationUpdates", e);
                }
            }
            if ((i & 2) != 0) {
                try {
                    if (this.A00 == null || r3.A02("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                        Log.w("FusedLocationManager/requestLocationUpdates/do not have coarse location permission");
                    } else {
                        this.A00.requestLocationUpdates("network", j, f, locationListener);
                    }
                } catch (RuntimeException e2) {
                    Log.e("FusedLocationManager/requestLocationUpdates", e2);
                }
            }
        }
    }

    public final void A06(String str) {
        if (Build.VERSION.SDK_INT == 29 && !this.A03.A00 && !this.A08 && !"group-chat-live-location-ui-oncreate".equals(str)) {
            this.A04.AaV("FusedLocationManager/logIfLocationAccessedInBackground", "background-location", true);
        }
    }

    public boolean A07() {
        A03();
        LocationManager locationManager = this.A00;
        if (locationManager != null) {
            return locationManager.isProviderEnabled("gps") || this.A00.isProviderEnabled("network");
        }
        return false;
    }
}
