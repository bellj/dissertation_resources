package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import java.util.List;

/* renamed from: X.2V6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V6 extends AnonymousClass02M {
    public final List A00;
    public final /* synthetic */ ViewSharedContactArrayActivity A01;

    public AnonymousClass2V6(ViewSharedContactArrayActivity viewSharedContactArrayActivity, List list) {
        this.A01 = viewSharedContactArrayActivity;
        this.A00 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02ee, code lost:
        if (r23.A01.A0M == false) goto L_0x02f0;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r24, int r25) {
        /*
        // Method dump skipped, instructions count: 930
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2V6.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            return new C75583k5(from.inflate(R.layout.review_individual_contact, (ViewGroup) null, true));
        }
        if (i == 1) {
            return new C75623k9(from.inflate(R.layout.contact_info_data, (ViewGroup) null, true));
        }
        if (i == 2) {
            return new C75493jw(from.inflate(R.layout.contact_card_divider, (ViewGroup) null, true));
        }
        if (i == 3) {
            return new C75503jx(from.inflate(R.layout.contact_info_sent_by, (ViewGroup) null, true));
        }
        throw new IllegalStateException();
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        Object obj = this.A00.get(i);
        if (obj instanceof AnonymousClass4LN) {
            return 0;
        }
        if (obj instanceof AnonymousClass4S9) {
            return 1;
        }
        if (obj instanceof AnonymousClass4LM) {
            return 2;
        }
        return obj instanceof AnonymousClass2V5 ? 3 : -1;
    }
}
