package X;

/* renamed from: X.3eR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C72343eR extends AnonymousClass1TM {
    public static final AnonymousClass1TK A01;
    public static final C72343eR A02 = A00("0", C114715Mu.A0E);
    public static final C72343eR A03;
    public static final C72343eR A04;
    public static final C72343eR A05;
    public static final C72343eR A06;
    public static final C72343eR A07;
    public static final C72343eR A08;
    public static final C72343eR A09;
    public static final C72343eR A0A;
    public static final C72343eR A0B;
    public static final C72343eR A0C;
    public static final C72343eR A0D;
    public static final C72343eR A0E;
    public static final C72343eR A0F;
    public static final C72343eR A0G = new C72343eR(new AnonymousClass1TK("1.3.6.1.1.1.1.22"));
    public static final C72343eR A0H = new C72343eR(new AnonymousClass1TK("1.3.6.1.4.1.311.10.3.3"));
    public static final C72343eR A0I = new C72343eR(new AnonymousClass1TK("2.16.840.1.113730.4.1"));
    public static final C72343eR A0J;
    public static final C72343eR A0K;
    public static final C72343eR A0L;
    public static final C72343eR A0M;
    public static final C72343eR A0N;
    public static final C72343eR A0O = new C72343eR(new AnonymousClass1TK("1.3.6.1.4.1.311.20.2.2"));
    public static final C72343eR A0P;
    public AnonymousClass1TK A00;

    public C72343eR(AnonymousClass1TK r1) {
        this.A00 = r1;
    }

    public String toString() {
        return this.A00.toString();
    }

    static {
        AnonymousClass1TK r1 = new AnonymousClass1TK("1.3.6.1.5.5.7.3");
        A01 = r1;
        A0N = A00("1", r1);
        A06 = A00("2", r1);
        A07 = A00("3", r1);
        A0B = A00("4", r1);
        A0C = A00("5", r1);
        A0E = A00("6", r1);
        A0F = A00("7", r1);
        A0P = A00("8", r1);
        A03 = A00("9", r1);
        A08 = A00("10", r1);
        A0J = A00("11", r1);
        A0M = A00("12", r1);
        A0A = A00("13", r1);
        A09 = A00("14", r1);
        A0L = A00("15", r1);
        A0K = A00("16", r1);
        A0D = A00("17", r1);
        A04 = A00("18", r1);
        A05 = A00("19", r1);
    }

    public static C72343eR A00(String str, AnonymousClass1TK r3) {
        return new C72343eR(new AnonymousClass1TK(str, r3));
    }
}
