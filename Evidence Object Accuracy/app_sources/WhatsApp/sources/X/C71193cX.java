package X;

import java.io.Serializable;
import java.text.DateFormat;

/* renamed from: X.3cX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71193cX implements Comparable, Serializable {
    public static final long serialVersionUID = 8139806907588338737L;
    public final long ntpTime;
    public DateFormat simpleFormatter;
    public DateFormat utcFormatter;

    public C71193cX(long j) {
        this.ntpTime = j;
    }

    public static long A00(long j) {
        long j2 = (j >>> 32) & 4294967295L;
        return (j2 * 1000) + ((2147483648L & j2) == 0 ? 2085978496000L : -2208988800000L) + Math.round((((double) (j & 4294967295L)) * 1000.0d) / 4.294967296E9d);
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        long j = this.ntpTime;
        long j2 = ((C71193cX) obj).ntpTime;
        if (j < j2) {
            return -1;
        }
        return j == j2 ? 0 : 1;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C71193cX) || this.ntpTime != ((C71193cX) obj).ntpTime) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.ntpTime;
        return (int) (j ^ (j >>> 32));
    }

    @Override // java.lang.Object
    public String toString() {
        long j = this.ntpTime;
        StringBuilder A0h = C12960it.A0h();
        String hexString = Long.toHexString((j >>> 32) & 4294967295L);
        for (int length = hexString.length(); length < 8; length++) {
            A0h.append('0');
        }
        A0h.append(hexString);
        A0h.append('.');
        String hexString2 = Long.toHexString(j & 4294967295L);
        for (int length2 = hexString2.length(); length2 < 8; length2++) {
            A0h.append('0');
        }
        return C12960it.A0d(hexString2, A0h);
    }
}
