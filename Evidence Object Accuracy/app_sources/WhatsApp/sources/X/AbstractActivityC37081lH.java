package X;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.List;
import java.util.Set;

/* renamed from: X.1lH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC37081lH extends AbstractActivityC37091lI {
    public RecyclerView A00;
    public C48882Ih A01;
    public C48892Ii A02;
    public AnonymousClass10A A03;
    public C25791Av A04;
    public C21770xx A05;
    public C53852fO A06;
    public C25821Ay A07;
    public AnonymousClass1FZ A08;
    public C19850um A09;
    public AnonymousClass19Q A0A;
    public AnonymousClass19T A0B;
    public C252918v A0C;
    public C37071lG A0D;
    public C37101lJ A0E;
    public C53842fM A0F;
    public C27131Gd A0G = new C36971ky(this);
    public AnonymousClass10S A0H;
    public C26311Cv A0I;
    public UserJid A0J;
    public C19840ul A0K;
    public String A0L;
    public boolean A0M;
    public boolean A0N;
    public final AnonymousClass2Cu A0O = new C58982tl(this);
    public final AnonymousClass4UV A0P = new C84423zH(this);
    public final AbstractC13960kc A0Q = new C68273Ut(this);
    public final AnonymousClass2SH A0R = new C84533zS(this);

    public final void A2e() {
        this.A0A.A03(this.A0J, 50, null, 32);
        Adm(CartFragment.A00(this.A0F.A0M, null, 0));
    }

    public void A2f(List list) {
        this.A0L = this.A06.A04(((ActivityC13830kP) this).A01, list);
        Set<String> A00 = C53852fO.A00(((AnonymousClass1lK) this.A0E).A05, list);
        List list2 = ((AnonymousClass1lK) this.A0E).A05;
        list2.clear();
        list2.addAll(list);
        for (String str : A00) {
            this.A08.A05(str);
        }
        invalidateOptionsMenu();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 3000) {
            super.onActivityResult(i, i2, intent);
        } else if (intent != null && intent.getIntExtra("get_collection_error_code", -1) == 404) {
            this.A0F.A04(this.A0J);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2) {
            C37101lJ r3 = this.A0E;
            List list = ((AnonymousClass1lL) r3).A00;
            if (list.size() > 0 && (list.get(0) instanceof C84623zc)) {
                list.remove(0);
                r3.A05(0);
                return;
            }
            return;
        }
        this.A0E.A0K();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0230  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0249  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r39) {
        /*
        // Method dump skipped, instructions count: 676
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC37081lH.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem findItem = menu.findItem(R.id.menu_cart);
        findItem.setActionView(R.layout.menu_item_cart);
        AnonymousClass23N.A01(findItem.getActionView());
        findItem.getActionView().setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 17));
        TextView textView = (TextView) findItem.getActionView().findViewById(R.id.cart_total_quantity);
        String str = this.A0L;
        if (str != null) {
            textView.setText(str);
        }
        this.A06.A00.A05(this, new AnonymousClass02B(findItem, this) { // from class: X.3RC
            public final /* synthetic */ MenuItem A00;
            public final /* synthetic */ AbstractActivityC37081lH A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
                if (r3.A0L == null) goto L_0x000f;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r14) {
                /*
                    r13 = this;
                    X.1lH r3 = r13.A01
                    android.view.MenuItem r2 = r13.A00
                    boolean r0 = X.C12970iu.A1Y(r14)
                    if (r0 == 0) goto L_0x000f
                    java.lang.String r1 = r3.A0L
                    r0 = 1
                    if (r1 != 0) goto L_0x0010
                L_0x000f:
                    r0 = 0
                L_0x0010:
                    r2.setVisible(r0)
                    boolean r0 = r3.A0N
                    if (r0 != 0) goto L_0x0045
                    r0 = 1
                    r3.A0N = r0
                    android.content.Intent r1 = r3.getIntent()
                    java.lang.String r0 = "source"
                    java.io.Serializable r6 = r1.getSerializableExtra(r0)
                    java.lang.Integer r6 = (java.lang.Integer) r6
                    X.19Q r1 = r3.A0A
                    r0 = 23
                    java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
                    com.whatsapp.jid.UserJid r2 = r3.A0J
                    X.2fO r0 = r3.A06
                    X.016 r0 = r0.A00
                    java.lang.Object r4 = r0.A01()
                    java.lang.Boolean r4 = (java.lang.Boolean) r4
                    r12 = 4
                    r3 = 0
                    r8 = r3
                    r9 = r3
                    r10 = r3
                    r11 = r3
                    r7 = r3
                    r1.A01(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                L_0x0045:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3RC.ANq(java.lang.Object):void");
            }
        });
        this.A06.A05();
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A04.A04(this.A0P);
        this.A07.A04(this.A0Q);
        this.A08.A04(this.A0R);
        this.A0H.A04(this.A0G);
        this.A03.A04(this.A0O);
        this.A0D.A00();
        this.A0K.A06("catalog_collections_view_tag", false);
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (16908332 == itemId) {
            onBackPressed();
            return true;
        } else if (R.id.menu_share == itemId) {
            UserJid userJid = this.A0J;
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.ShareCatalogLinkActivity");
            intent.setAction("android.intent.action.VIEW");
            intent.putExtra("jid", userJid.getRawString());
            startActivity(intent);
            return true;
        } else if (R.id.menu_cart != itemId) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            A2e();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0E.A0L();
        this.A0F.A0H.A00();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("catalog_loaded", this.A0M);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        this.A0N = false;
    }
}
