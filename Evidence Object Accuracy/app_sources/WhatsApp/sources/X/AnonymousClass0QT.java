package X;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0QT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QT {
    public final AnonymousClass0QO A00 = new AnonymousClass0QO();
    public final AbstractC11850gy A01;
    public final List A02 = new ArrayList();

    public AnonymousClass0QT(AbstractC11850gy r2) {
        this.A01 = r2;
    }

    public int A00() {
        return ((AnonymousClass0Z0) this.A01).A00.getChildCount() - this.A02.size();
    }

    public int A01() {
        return ((AnonymousClass0Z0) this.A01).A00.getChildCount();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
        if (r1.A06(r2) == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A02(int r6) {
        /*
            r5 = this;
            r4 = -1
            if (r6 < 0) goto L_0x0028
            X.0gy r0 = r5.A01
            X.0Z0 r0 = (X.AnonymousClass0Z0) r0
            androidx.recyclerview.widget.RecyclerView r0 = r0.A00
            int r3 = r0.getChildCount()
            r2 = r6
        L_0x000e:
            if (r2 >= r3) goto L_0x0028
            X.0QO r1 = r5.A00
            int r0 = r1.A00(r2)
            int r0 = r2 - r0
            int r0 = r6 - r0
            if (r0 == 0) goto L_0x001e
            int r2 = r2 + r0
            goto L_0x000e
        L_0x001e:
            boolean r0 = r1.A06(r2)
            if (r0 == 0) goto L_0x0027
            int r2 = r2 + 1
            goto L_0x001e
        L_0x0027:
            return r2
        L_0x0028:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QT.A02(int):int");
    }

    public View A03(int i) {
        return ((AnonymousClass0Z0) this.A01).A00.getChildAt(A02(i));
    }

    public View A04(int i) {
        return ((AnonymousClass0Z0) this.A01).A00.getChildAt(i);
    }

    public void A05(int i) {
        AnonymousClass03U A01;
        int A02 = A02(i);
        this.A00.A07(A02);
        RecyclerView recyclerView = ((AnonymousClass0Z0) this.A01).A00;
        View childAt = recyclerView.getChildAt(A02);
        if (!(childAt == null || (A01 = RecyclerView.A01(childAt)) == null)) {
            int i2 = A01.A00;
            if ((i2 & 256) == 0 || A01.A06()) {
                A01.A00 = 256 | i2;
            } else {
                StringBuilder sb = new StringBuilder("called detach on an already detached child ");
                sb.append(A01);
                sb.append(recyclerView.A0F());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        RecyclerView.A06(recyclerView, A02);
    }

    public void A06(int i) {
        int A02 = A02(i);
        RecyclerView recyclerView = ((AnonymousClass0Z0) this.A01).A00;
        View childAt = recyclerView.getChildAt(A02);
        if (childAt != null) {
            if (this.A00.A07(A02)) {
                A08(childAt);
            }
            View childAt2 = recyclerView.getChildAt(A02);
            if (childAt2 != null) {
                recyclerView.A0h(childAt2);
                childAt2.clearAnimation();
            }
            recyclerView.removeViewAt(A02);
        }
    }

    public final void A07(View view) {
        this.A02.add(view);
        AnonymousClass0Z0 r0 = (AnonymousClass0Z0) this.A01;
        AnonymousClass03U A01 = RecyclerView.A01(view);
        if (A01 != null) {
            RecyclerView recyclerView = r0.A00;
            int i = A01.A04;
            if (i == -1) {
                i = A01.A0H.getImportantForAccessibility();
            }
            A01.A07 = i;
            if (recyclerView.A09 > 0) {
                A01.A04 = 4;
                recyclerView.A15.add(A01);
                return;
            }
            AnonymousClass028.A0a(A01.A0H, 4);
        }
    }

    public final void A08(View view) {
        if (this.A02.remove(view)) {
            this.A01.ARl(view);
        }
    }

    public void A09(View view, int i, boolean z) {
        int A02;
        if (i < 0) {
            A02 = ((AnonymousClass0Z0) this.A01).A00.getChildCount();
        } else {
            A02 = A02(i);
        }
        this.A00.A05(A02, z);
        if (z) {
            A07(view);
        }
        RecyclerView recyclerView = ((AnonymousClass0Z0) this.A01).A00;
        recyclerView.addView(view, A02);
        AnonymousClass03U A01 = RecyclerView.A01(view);
        AnonymousClass02M r0 = recyclerView.A0N;
        if (!(r0 == null || A01 == null)) {
            r0.A08(A01);
        }
        List list = recyclerView.A0a;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    ((AnonymousClass045) recyclerView.A0a.get(size)).ANv(view);
                } else {
                    return;
                }
            }
        }
    }

    public void A0A(View view, ViewGroup.LayoutParams layoutParams, int i, boolean z) {
        int A02;
        if (i < 0) {
            A02 = ((AnonymousClass0Z0) this.A01).A00.getChildCount();
        } else {
            A02 = A02(i);
        }
        this.A00.A05(A02, z);
        if (z) {
            A07(view);
        }
        AnonymousClass0Z0 r3 = (AnonymousClass0Z0) this.A01;
        AnonymousClass03U A01 = RecyclerView.A01(view);
        if (A01 != null) {
            int i2 = A01.A00;
            if ((i2 & 256) != 0 || A01.A06()) {
                A01.A00 = i2 & -257;
            } else {
                StringBuilder sb = new StringBuilder("Called attach on a child which is not detached: ");
                sb.append(A01);
                sb.append(r3.A00.A0F());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        RecyclerView.A04(view, layoutParams, r3.A00, A02);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00.toString());
        sb.append(", hidden list:");
        sb.append(this.A02.size());
        return sb.toString();
    }
}
