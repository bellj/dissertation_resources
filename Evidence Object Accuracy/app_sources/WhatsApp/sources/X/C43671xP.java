package X;

import android.content.ContentValues;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1xP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43671xP extends AbstractC21570xd {
    public C43671xP(C232010t r1) {
        super(r1);
    }

    public void A00(Set set) {
        C28181Ma r6 = new C28181Ma(true);
        r6.A03();
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AbstractC21570xd.A02(A02, "wa_block_list", null, null);
            ContentValues contentValues = new ContentValues(1);
            Iterator it = set.iterator();
            while (it.hasNext()) {
                contentValues.put("jid", ((UserJid) it.next()).getRawString());
                AbstractC21570xd.A00(contentValues, A02, "wa_block_list");
            }
            A00.A00();
            A00.close();
            A02.close();
            r6.A00();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
