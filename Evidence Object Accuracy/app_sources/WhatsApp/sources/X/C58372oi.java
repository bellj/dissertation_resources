package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: X.2oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58372oi extends AbstractC75803kU {
    public HashMap A00 = C12970iu.A11();
    public AnonymousClass5WC[] A01;
    public final AnonymousClass018 A02;
    public final HashMap A03 = C12970iu.A11();
    public final HashMap A04 = C12970iu.A11();
    public final HashSet A05 = C12970iu.A12();

    public C58372oi(AnonymousClass018 r2, AnonymousClass5WC[] r3) {
        this.A02 = r2;
        this.A01 = r3;
        A0L(r3);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A01.length;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ int A0F(Object obj) {
        Object tag = ((View) obj).getTag();
        HashMap hashMap = this.A04;
        if (!hashMap.containsKey(tag)) {
            this.A03.remove(tag);
            return -2;
        }
        int A05 = C12960it.A05(hashMap.get(tag));
        HashMap hashMap2 = this.A03;
        if (hashMap2.containsKey(tag) && A05 == C12960it.A05(hashMap2.get(tag))) {
            return -1;
        }
        C12960it.A1K(tag, hashMap2, A05);
        return A05;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ Object A0G(ViewGroup viewGroup, int i) {
        int i2;
        int A0K = A0K(i);
        AnonymousClass5WC r6 = this.A01[A0K];
        AnonymousClass009.A05(r6);
        AbstractC69213Yj r62 = (AbstractC69213Yj) r6;
        LayoutInflater layoutInflater = r62.A09;
        if (!(r62 instanceof C622335s)) {
            if ((r62 instanceof AnonymousClass35q) || (r62 instanceof C622235r) || !(r62 instanceof AnonymousClass35p)) {
                i2 = R.layout.fixed_sticker_page;
            } else {
                i2 = R.layout.reaction_sticker_page;
            }
        } else if (!(((C622335s) r62) instanceof AnonymousClass35n)) {
            i2 = R.layout.sticker_pack_page;
        } else {
            i2 = R.layout.third_party_pack_page;
        }
        View A0O = C12980iv.A0O(layoutInflater, i2);
        r62.A04 = C12990iw.A0R(A0O, R.id.sticker_grid);
        int i3 = r62.A00;
        if (i3 <= 0) {
            i3 = 1;
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(i3);
        r62.A03 = gridLayoutManager;
        r62.A04.setLayoutManager(gridLayoutManager);
        r62.A04.A0l(new C54622h1(r62.A0B, r62.A02));
        RecyclerView recyclerView = r62.A04;
        C54392ge A00 = r62.A00();
        recyclerView.setLayoutFrozen(false);
        recyclerView.A0j(A00, true, false);
        recyclerView.A0r(true);
        recyclerView.requestLayout();
        RecyclerView recyclerView2 = r62.A04;
        recyclerView2.A0n(new C54812hK(recyclerView2.getResources(), r62.A03, r62.A0A));
        r62.A03(A0O);
        r62.A01();
        Iterator it = this.A05.iterator();
        while (it.hasNext()) {
            AbstractC05270Ox r1 = (AbstractC05270Ox) it.next();
            RecyclerView recyclerView3 = ((AbstractC69213Yj) this.A01[A0K]).A04;
            if (recyclerView3 != null) {
                recyclerView3.A0n(r1);
            }
        }
        String id = this.A01[A0K].getId();
        A0O.setTag(id);
        this.A00.put(id, this.A01[A0K]);
        viewGroup.addView(A0O, 0);
        return A0O;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ void A0I(ViewGroup viewGroup, Object obj, int i) {
        View view = (View) obj;
        int A0K = A0K(i);
        Object tag = view.getTag();
        viewGroup.removeView(view);
        AnonymousClass5WC r3 = (AnonymousClass5WC) this.A00.remove(tag);
        if (r3 != null) {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                AbstractC05270Ox r1 = (AbstractC05270Ox) it.next();
                RecyclerView recyclerView = ((AbstractC69213Yj) r3).A04;
                if (recyclerView != null) {
                    recyclerView.A0o(r1);
                }
            }
            r3.AP2(view, viewGroup, A0K);
        }
    }

    @Override // X.AbstractC75803kU
    public boolean A0J(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }

    public final int A0K(int i) {
        int length;
        AnonymousClass018 r6 = this.A02;
        if (C28141Kv.A01(r6)) {
            length = i;
        } else {
            length = (this.A01.length - 1) - i;
        }
        if (length < 0) {
            Locale locale = Locale.US;
            Object[] objArr = new Object[3];
            objArr[0] = Boolean.valueOf(C28141Kv.A01(r6));
            C12960it.A1P(objArr, this.A01.length, 1);
            C12960it.A1P(objArr, i, 2);
            C12990iw.A1R("ContentPagerAdapter/getAbsolutePosition/absolutePosition < 0, isLtr: %s, pages.length: %d, position: %d", locale, objArr);
        }
        return length;
    }

    public final void A0L(AnonymousClass5WC[] r5) {
        this.A01 = r5;
        HashMap hashMap = this.A04;
        hashMap.clear();
        for (int i = 0; i < r5.length; i++) {
            C12960it.A1K(r5[i].getId(), hashMap, A0K(i));
        }
    }
}
