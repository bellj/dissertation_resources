package X;

import java.io.StringWriter;
import org.chromium.net.UrlRequest;

/* renamed from: X.3wU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82973wU extends AbstractC94534c0 {
    public boolean A00 = true;
    public final String A01;

    public C82973wU(CharSequence charSequence, boolean z) {
        String charSequence2;
        if (!z || charSequence.length() <= 1) {
            charSequence2 = charSequence.toString();
        } else {
            char charAt = charSequence.charAt(0);
            char charAt2 = charSequence.charAt(charSequence.length() - 1);
            if (charAt == '\'') {
                if (charAt2 == '\'') {
                    charSequence = charSequence.subSequence(1, charSequence.length() - 1);
                }
            } else if (charAt == '\"' && charAt2 == '\"') {
                charSequence = charSequence.subSequence(1, charSequence.length() - 1);
                this.A00 = false;
            }
            charSequence2 = C95094d8.A01(charSequence.toString());
        }
        this.A01 = charSequence2;
    }

    public static int A00(AbstractC94534c0 r1, AbstractC94534c0 r2) {
        return r1.A06().A01.compareTo(r2.A06().A01);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C82973wU) && !(obj instanceof C83013wY)) {
                return false;
            }
            C82973wU A06 = ((AbstractC94534c0) obj).A06();
            String str = this.A01;
            String str2 = A06.A01;
            if (str != null) {
                if (!str.equals(str2)) {
                    return false;
                }
            } else if (str2 != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String str;
        String obj;
        String str2;
        StringBuilder A0h;
        if (this.A00) {
            str = "'";
        } else {
            str = "\"";
        }
        StringBuilder A0j = C12960it.A0j(str);
        String str3 = this.A01;
        if (str3 == null) {
            obj = null;
        } else {
            int length = str3.length();
            StringWriter stringWriter = new StringWriter(length << 1);
            for (int i = 0; i < length; i++) {
                char charAt = str3.charAt(i);
                if (charAt > 4095) {
                    A0h = C12960it.A0h();
                    str2 = "\\u";
                } else if (charAt > 255) {
                    A0h = C12960it.A0h();
                    str2 = "\\u0";
                } else {
                    str2 = "\\u00";
                    if (charAt <= 127) {
                        if (charAt < ' ') {
                            switch (charAt) {
                                case '\b':
                                    stringWriter.write(92);
                                    charAt = 'b';
                                    break;
                                case '\t':
                                    stringWriter.write(92);
                                    charAt = 't';
                                    break;
                                case '\n':
                                    stringWriter.write(92);
                                    charAt = 'n';
                                    break;
                                case 11:
                                default:
                                    if (charAt <= 15) {
                                        A0h = C12960it.A0h();
                                        str2 = "\\u000";
                                        break;
                                    }
                                    break;
                                case '\f':
                                    stringWriter.write(92);
                                    charAt = 'f';
                                    break;
                                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                    stringWriter.write(92);
                                    charAt = 'r';
                                    break;
                            }
                        } else {
                            int i2 = 34;
                            if (charAt != '\"') {
                                i2 = 39;
                                if (charAt != '\'') {
                                    i2 = 47;
                                    if (charAt != '/') {
                                        if (charAt == '\\') {
                                            stringWriter.write(92);
                                            stringWriter.write(92);
                                        }
                                    }
                                }
                            }
                            stringWriter.write(92);
                            stringWriter.write(i2);
                        }
                        stringWriter.write(charAt);
                    }
                    A0h = C12960it.A0h();
                }
                A0h.append(str2);
                stringWriter.write(C12960it.A0d(Integer.toHexString(charAt).toUpperCase(), A0h));
            }
            obj = stringWriter.toString();
        }
        A0j.append(obj);
        return C12960it.A0d(str, A0j);
    }
}
