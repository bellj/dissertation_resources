package X;

import android.view.ViewTreeObserver;
import com.whatsapp.account.delete.DeleteAccountConfirmation;

/* renamed from: X.4o9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101824o9 implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ DeleteAccountConfirmation A00;

    public ViewTreeObserver$OnPreDrawListenerC101824o9(DeleteAccountConfirmation deleteAccountConfirmation) {
        this.A00 = deleteAccountConfirmation;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        DeleteAccountConfirmation deleteAccountConfirmation = this.A00;
        C12980iv.A1G(deleteAccountConfirmation.A03, this);
        deleteAccountConfirmation.A2e();
        return false;
    }
}
