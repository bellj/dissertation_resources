package X;

/* renamed from: X.27B  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass27B {
    A02(0),
    A03(1),
    A06(2),
    A01(3),
    A05(4),
    A04(5);
    
    public final int value;

    AnonymousClass27B(int i) {
        this.value = i;
    }

    public static AnonymousClass27B A00(int i) {
        if (i == 0) {
            return A02;
        }
        if (i == 1) {
            return A03;
        }
        if (i == 2) {
            return A06;
        }
        if (i == 3) {
            return A01;
        }
        if (i == 4) {
            return A05;
        }
        if (i != 5) {
            return null;
        }
        return A04;
    }
}
