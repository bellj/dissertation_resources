package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124655pt extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124655pt[] A00;
    public static final EnumC124655pt A01;
    public static final EnumC124655pt A02;
    public static final EnumC124655pt A03;
    public static final EnumC124655pt A04;
    public static final EnumC124655pt A05;
    public static final EnumC124655pt A06;
    public static final EnumC124655pt A07;
    public static final EnumC124655pt A08;
    public final String fieldName;

    public static EnumC124655pt valueOf(String str) {
        return (EnumC124655pt) Enum.valueOf(EnumC124655pt.class, str);
    }

    public static EnumC124655pt[] values() {
        return (EnumC124655pt[]) A00.clone();
    }

    static {
        EnumC124655pt r11 = new EnumC124655pt("ACCOUNT_HOLDER_NAME", "account_holder_name", 0);
        A01 = r11;
        EnumC124655pt r10 = new EnumC124655pt("ACCOUNT_PROVIDER", "account_provider", 1);
        A02 = r10;
        EnumC124655pt r9 = new EnumC124655pt("ACCOUNT_TYPE", "account_type", 2);
        A03 = r9;
        EnumC124655pt r8 = new EnumC124655pt("BANK_IMAGE_URL", "bank_image_url", 3);
        A04 = r8;
        EnumC124655pt r7 = new EnumC124655pt("IS_MPIN_SET", "is_mpin_set", 4);
        A05 = r7;
        EnumC124655pt r6 = new EnumC124655pt("VPA_HANDLE", "vpa_handle", 5);
        A07 = r6;
        EnumC124655pt r5 = new EnumC124655pt("VPA_ID", "vpa_id", 6);
        A08 = r5;
        EnumC124655pt r4 = new EnumC124655pt("PROVIDER_ICON_BLOB", "provider_icon_blob", 7);
        A06 = r4;
        EnumC124655pt r2 = new EnumC124655pt("BANK_INFO", "bank_info", 8);
        EnumC124655pt[] r1 = new EnumC124655pt[9];
        C72453ed.A1F(r11, r10, r9, r8, r1);
        C117305Zk.A1M(r7, r6, r1);
        r1[6] = r5;
        r1[7] = r4;
        r1[8] = r2;
        A00 = r1;
    }

    public EnumC124655pt(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
