package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.SystemClock;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21600xg {
    public final C15550nR A00;
    public final C21580xe A01;
    public final C14820m6 A02;
    public final C16510p9 A03;
    public final C19990v2 A04;
    public final C20120vF A05;
    public final C21590xf A06;
    public final C33461e7 A07;
    public final CopyOnWriteArrayList A08 = new CopyOnWriteArrayList();

    public C21600xg(C15550nR r2, C21580xe r3, C14830m7 r4, C14820m6 r5, C16510p9 r6, C19990v2 r7, C20120vF r8, C21590xf r9, C21560xc r10) {
        this.A03 = r6;
        this.A04 = r7;
        this.A00 = r2;
        this.A02 = r5;
        this.A05 = r8;
        this.A01 = r3;
        this.A06 = r9;
        this.A07 = new C33461e7(r4, r10);
    }

    public static final boolean A00(C28021Kd r5) {
        C28011Kc r52 = r5.A00;
        return r52.A0G == 0 && r52.A06 == 0;
    }

    public final List A01(Set set) {
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r2 = (AbstractC14640lm) it.next();
            if (this.A00.A0A(r2) != null) {
                arrayList.add(new C28021Kd(this.A05.A04(r2), r2));
            }
        }
        return arrayList;
    }

    public void A02(AtomicBoolean atomicBoolean) {
        AbstractC14640lm r9;
        Cursor A09;
        SystemClock.elapsedRealtime();
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet(this.A04.A0A());
        C21580xe r6 = this.A01;
        Cursor A092 = r6.A09();
        try {
            if (A092.getCount() != 0) {
                HashSet hashSet3 = new HashSet(hashSet2);
                while (A092.moveToNext()) {
                    AbstractC14640lm A01 = AbstractC14640lm.A01(A092.getString(0));
                    if (A01 != null) {
                        hashSet3.remove(A01);
                    }
                }
                ArrayList arrayList2 = new ArrayList(hashSet3);
                arrayList2.size();
                if (!arrayList2.isEmpty()) {
                    r6.A0R(arrayList2);
                }
                A092.close();
                A09 = r6.A09();
            } else {
                A092.close();
                C21590xf r3 = this.A06;
                if (r3.A03.A0A()) {
                    long A02 = r3.A00.A02(AnonymousClass1VX.A00);
                    C16490p7 r0 = r3.A05;
                    r0.A04();
                    A09 = r0.A05.AG6().A09("SELECT message.chat_row_id AS chat_row_id, sum(file_size) AS media_sum FROM message_media message_media JOIN message_view message ON message_media.message_row_id = message._id WHERE message.message_type IN ('0','1','2','3','4','5','9','13','14') AND message.chat_row_id != ? GROUP BY message.chat_row_id ORDER BY media_sum DESC", new String[]{Long.toString(A02)});
                } else {
                    C16490p7 r02 = r3.A05;
                    r02.A04();
                    A09 = r02.A05.AG6().A09(AnonymousClass1Y7.A00, null);
                }
            }
            try {
                A092.getCount();
                if (A092.moveToFirst()) {
                    boolean z = false;
                    while (!atomicBoolean.get()) {
                        if (arrayList.size() > 16 && !z) {
                            SystemClock.elapsedRealtime();
                            z = true;
                        }
                        int columnIndex = A092.getColumnIndex("chat_row_id");
                        if (columnIndex > -1) {
                            r9 = this.A03.A05(A092.getLong(columnIndex));
                        } else {
                            r9 = AbstractC14640lm.A01(A092.getString(0));
                        }
                        if (r9 != null) {
                            Iterator it = arrayList.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (((C28021Kd) it.next()).A01().equals(r9)) {
                                        break;
                                    }
                                } else {
                                    Iterator it2 = hashSet.iterator();
                                    while (true) {
                                        if (it2.hasNext()) {
                                            if (((Jid) it2.next()).equals(r9)) {
                                                break;
                                            }
                                        } else {
                                            hashSet.add(r9);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if ((arrayList.size() < 16 && hashSet.size() >= 2) || (arrayList.size() >= 16 && hashSet.size() >= 5)) {
                            List A012 = A01(hashSet);
                            hashSet.clear();
                            if (!A012.isEmpty()) {
                                arrayList.addAll(A012);
                                arrayList.size();
                                C89454Ka r92 = new C89454Ka(A012);
                                Iterator it3 = this.A08.iterator();
                                while (it3.hasNext()) {
                                    ((AbstractC28051Kk) it3.next()).ANt(r92);
                                }
                                for (int i = 0; i < A012.size(); i++) {
                                    r6.A0I((C28021Kd) A012.get(i));
                                }
                            }
                        }
                        if (!A092.moveToNext()) {
                            if (hashSet.size() > 0) {
                                arrayList.addAll(A01(hashSet));
                            }
                            Collections.sort(arrayList);
                        }
                    }
                    A092.close();
                    return;
                }
                A092.close();
                try {
                    C232010t r7 = ((AbstractC21570xd) r6).A00;
                    C16310on A022 = r7.A02();
                    AnonymousClass1Lx A00 = A022.A00();
                    try {
                        Cursor A093 = r6.A09();
                        HashSet hashSet4 = new HashSet(hashSet2);
                        while (A093.moveToNext()) {
                            AbstractC14640lm A013 = AbstractC14640lm.A01(A093.getString(0));
                            if (A013 != null) {
                                hashSet4.remove(A013);
                            }
                        }
                        ArrayList arrayList3 = new ArrayList(hashSet4);
                        r6.A0R(arrayList3);
                        A093.close();
                        if (arrayList3.size() == 0) {
                            C16310on A014 = r7.get();
                            Cursor A03 = AbstractC21570xd.A03(A014, "wa_contact_storage_usage", null, null, "CONTACT_STORAGE_USAGES", new String[]{"jid"}, null);
                            int count = A03.getCount();
                            A03.close();
                            A014.close();
                            if (count != hashSet2.size()) {
                                HashSet hashSet5 = new HashSet();
                                HashSet hashSet6 = new HashSet(hashSet2);
                                C16310on A023 = r7.A02();
                                AnonymousClass1Lx A002 = A023.A00();
                                Cursor A032 = AbstractC21570xd.A03(A023, "wa_contact_storage_usage", null, null, "CONTACT_STORAGE_USAGES", new String[]{"jid"}, null);
                                while (A032.moveToNext()) {
                                    AbstractC14640lm A015 = AbstractC14640lm.A01(A032.getString(0));
                                    if (A015 == null) {
                                        Log.w("contact-manager-database/remove-copies-and-not-wa-contacts/jid is null or invalid!");
                                    } else if (hashSet6.contains(A015)) {
                                        hashSet6.remove(A015);
                                    } else {
                                        hashSet5.add(A015);
                                    }
                                }
                                A032.close();
                                Iterator it4 = hashSet5.iterator();
                                while (it4.hasNext()) {
                                    AbstractC21570xd.A02(A023, "wa_contact_storage_usage", "jid = ? ", new String[]{((AbstractC14640lm) it4.next()).getRawString()});
                                }
                                A002.A00();
                                A002.close();
                                A023.close();
                            }
                        }
                        ContentValues contentValues = new ContentValues();
                        Iterator it5 = arrayList.iterator();
                        while (it5.hasNext()) {
                            C28021Kd r03 = (C28021Kd) it5.next();
                            AbstractC14640lm A016 = r03.A01();
                            C28011Kc r32 = r03.A00;
                            long j = r32.A0G;
                            int i2 = r32.A06;
                            contentValues.clear();
                            contentValues.put("conversation_size", Long.valueOf(j));
                            contentValues.put("conversation_message_count", Integer.valueOf(i2));
                            AbstractC21570xd.A01(contentValues, A022, "wa_contact_storage_usage", "jid = ?", new String[]{A016.getRawString()});
                        }
                        A00.A00();
                        A00.close();
                        A022.close();
                    } catch (Throwable th) {
                        try {
                            A00.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IllegalArgumentException e) {
                    AnonymousClass009.A08("contact-mgr-db/unable to update batch on storage usage table", e);
                }
                ArrayList arrayList4 = new ArrayList();
                Iterator it6 = arrayList.iterator();
                while (it6.hasNext()) {
                    C28021Kd r1 = (C28021Kd) it6.next();
                    if (r1 != null && !A00(r1)) {
                        arrayList4.add(r1);
                    }
                }
                if (!atomicBoolean.get()) {
                    this.A07.A01("STORAGE_USAGE_CHAT_LIST_CACHE_TIME");
                    AnonymousClass4KZ r2 = new AnonymousClass4KZ(arrayList4);
                    Iterator it7 = this.A08.iterator();
                    while (it7.hasNext()) {
                        ((AbstractC28051Kk) it7.next()).ANs(r2);
                    }
                    SystemClock.elapsedRealtime();
                }
            } finally {
            }
        } finally {
        }
    }
}
