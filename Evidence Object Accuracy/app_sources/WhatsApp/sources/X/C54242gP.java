package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;
import java.util.List;

/* renamed from: X.2gP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54242gP extends AnonymousClass02M {
    public List A00 = C12960it.A0l();
    public final FilterBottomSheetDialogFragment A01;
    public final boolean A02;

    public C54242gP(FilterBottomSheetDialogFragment filterBottomSheetDialogFragment, boolean z) {
        this.A02 = z;
        this.A01 = filterBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC37191le) r2).A09(this.A00.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater A0E = C12960it.A0E(viewGroup);
        if (this.A02) {
            return new C59782vN((AppCompatCheckBox) C12980iv.A0O(A0E, R.layout.filter_bottom_sheet_dialog_category_item_checkbox), this.A01);
        }
        return new C59842vT((AppCompatRadioButton) C12980iv.A0O(A0E, R.layout.filter_bottom_sheet_dialog_category_item_radiobutton), this.A01);
    }
}
