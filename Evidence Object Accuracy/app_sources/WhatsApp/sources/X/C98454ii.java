package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4ii  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98454ii implements IInterface, AnonymousClass5Y9 {
    public final IBinder A00;
    public final String A01 = "com.google.android.gms.clearcut.internal.IClearcutLoggerService";

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }

    public C98454ii(IBinder iBinder) {
        this.A00 = iBinder;
    }
}
