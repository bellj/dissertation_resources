package X;

/* renamed from: X.25h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC462925h {
    boolean Afl(boolean z, boolean z2, boolean z3, boolean z4);

    AbstractC27881Jp Afm(AbstractC27881Jp v, AbstractC27881Jp v2, boolean z, boolean z2);

    double Afn(double d, double d2, boolean z, boolean z2);

    float Afo(float f, float f2, boolean z, boolean z2);

    int Afp(int i, int i2, boolean z, boolean z2);

    AbstractC41941uP Afq(AbstractC41941uP v, AbstractC41941uP v2);

    AnonymousClass1K6 Afr(AnonymousClass1K6 v, AnonymousClass1K6 v2);

    long Afs(long j, long j2, boolean z, boolean z2);

    AnonymousClass1G1 Aft(AnonymousClass1G1 v, AnonymousClass1G1 v2);

    Object Afu(Object obj, Object obj2, boolean z);

    Object Afv(Object obj, Object obj2, boolean z);

    void Afw(boolean z);

    Object Afx(Object obj, Object obj2, boolean z);

    String Afy(String str, String str2, boolean z, boolean z2);

    AnonymousClass256 Afz(AnonymousClass256 v, AnonymousClass256 v2);
}
