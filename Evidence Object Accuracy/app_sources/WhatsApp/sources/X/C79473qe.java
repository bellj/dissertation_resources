package X;

import java.util.Arrays;

/* renamed from: X.3qe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79473qe extends C79503qh implements Cloneable {
    public byte[] A00 = C88764Ha.A00;
    public byte[][] A01 = C88764Ha.A04;

    public C79473qe() {
        ((C79503qh) this).A00 = null;
        ((AbstractC94974cq) this).A00 = -1;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final void A05(C95484do r4) {
        byte[] bArr = this.A00;
        if (!Arrays.equals(bArr, C88764Ha.A00)) {
            r4.A08(1, bArr);
        }
        byte[][] bArr2 = this.A01;
        if (bArr2 != null && bArr2.length > 0) {
            int i = 0;
            while (true) {
                byte[][] bArr3 = this.A01;
                if (i >= bArr3.length) {
                    break;
                }
                byte[] bArr4 = bArr3[i];
                if (bArr4 != null) {
                    r4.A08(2, bArr4);
                }
                i++;
            }
        }
        if (!"".equals("")) {
            r4.A07(4, "");
        }
        super.A05(r4);
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final int A03() {
        int length;
        int A03 = super.A03();
        byte[] bArr = this.A00;
        if (!Arrays.equals(bArr, C88764Ha.A00)) {
            A03 = C79503qh.A00(bArr.length, 1, A03);
        }
        byte[][] bArr2 = this.A01;
        if (bArr2 != null && (length = bArr2.length) > 0) {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            do {
                byte[] bArr3 = bArr2[i];
                if (bArr3 != null) {
                    i3++;
                    int length2 = bArr3.length;
                    i2 += C72453ed.A07(length2) + length2;
                }
                i++;
            } while (i < length);
            A03 = A03 + i2 + i3;
        }
        return !"".equals("") ? A03 + C95484do.A00(4) : A03;
    }

    @Override // X.AbstractC94974cq, java.lang.Object
    public final /* synthetic */ Object clone() {
        try {
            C79473qe r2 = (C79473qe) super.A06();
            byte[][] bArr = this.A01;
            if (bArr != null && bArr.length > 0) {
                r2.A01 = (byte[][]) bArr.clone();
            }
            return r2;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        int length;
        int length2;
        if (obj != this) {
            if (obj instanceof C79473qe) {
                C79473qe r11 = (C79473qe) obj;
                if (Arrays.equals(this.A00, r11.A00) && "".equals("")) {
                    byte[][] bArr = this.A01;
                    byte[][] bArr2 = r11.A01;
                    if (bArr == null) {
                        length = 0;
                    } else {
                        length = bArr.length;
                    }
                    if (bArr2 == null) {
                        length2 = 0;
                    } else {
                        length2 = bArr2.length;
                    }
                    int i = 0;
                    int i2 = 0;
                    while (true) {
                        if (i >= length || bArr[i] != null) {
                            while (i2 < length2 && bArr2[i2] == null) {
                                i2++;
                            }
                            boolean A1X = C12990iw.A1X(i, length);
                            boolean A1X2 = C12990iw.A1X(i2, length2);
                            if (!A1X) {
                                if (A1X != A1X2 || !Arrays.equals(bArr[i], bArr2[i2])) {
                                    break;
                                }
                                i++;
                                i2++;
                            } else if (A1X2) {
                                AnonymousClass5BY r1 = ((C79503qh) this).A00;
                                if (r1 != null && r1.A00 != 0) {
                                    return r1.equals(((C79503qh) r11).A00);
                                }
                                AnonymousClass5BY r0 = ((C79503qh) r11).A00;
                                if (r0 == null || r0.A00 == 0) {
                                    return true;
                                }
                            }
                        } else {
                            i++;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = 0;
        int hashCode = (((((C79473qe.class.getName().hashCode() + 527) * 31) + Arrays.hashCode(this.A00)) * 31) + "".hashCode()) * 31;
        byte[][] bArr = this.A01;
        int length = bArr == null ? 0 : bArr.length;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            byte[] bArr2 = bArr[i3];
            if (bArr2 != null) {
                i2 = (i2 * 31) + Arrays.hashCode(bArr2);
            }
        }
        int i4 = (((hashCode + i2) * 31) + 1237) * 31;
        AnonymousClass5BY r1 = ((C79503qh) this).A00;
        if (!(r1 == null || r1.A00 == 0)) {
            i = r1.hashCode();
        }
        return i4 + i;
    }
}
