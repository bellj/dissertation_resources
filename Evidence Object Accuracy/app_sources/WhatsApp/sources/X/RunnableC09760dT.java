package X;

import android.content.Intent;
import android.content.IntentSender;

/* renamed from: X.0dT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09760dT implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ IntentSender.SendIntentException A01;
    public final /* synthetic */ AnonymousClass052 A02;

    public RunnableC09760dT(IntentSender.SendIntentException sendIntentException, AnonymousClass052 r2, int i) {
        this.A02 = r2;
        this.A00 = i;
        this.A01 = sendIntentException;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A02.A06(new Intent().setAction("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION", this.A01), this.A00, 0);
    }
}
