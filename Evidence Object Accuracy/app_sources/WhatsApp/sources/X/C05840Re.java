package X;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0Re  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05840Re {
    public final Map A00 = new HashMap();
    public final Map A01;

    public C05840Re(Map map) {
        this.A01 = map;
        for (Map.Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            List list = (List) this.A00.get(value);
            if (list == null) {
                list = new ArrayList();
                this.A00.put(value, list);
            }
            list.add(entry.getKey());
        }
    }

    public static void A00(AnonymousClass074 r6, AbstractC001200n r7, Object obj, List list) {
        Method method;
        Object[] objArr;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    AnonymousClass0P7 r4 = (AnonymousClass0P7) list.get(size);
                    try {
                        int i = r4.A00;
                        if (i == 0) {
                            method = r4.A01;
                            objArr = new Object[0];
                        } else if (i == 1) {
                            method = r4.A01;
                            objArr = new Object[]{r7};
                        } else if (i == 2) {
                            method = r4.A01;
                            objArr = new Object[]{r7, r6};
                        }
                        method.invoke(obj, objArr);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (InvocationTargetException e2) {
                        throw new RuntimeException("Failed to call observer method", e2.getCause());
                    }
                } else {
                    return;
                }
            }
        }
    }
}
