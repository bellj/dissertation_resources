package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.TextData;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

/* renamed from: X.0tV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19060tV extends AbstractC18500sY implements AbstractC19010tQ {
    public final C16510p9 A00;
    public final C20490vq A01;
    public final C20950wa A02;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19060tV(C16510p9 r3, C20490vq r4, C20950wa r5, C18480sW r6) {
        super(r6, "message_text", 1);
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        boolean z;
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("media_name");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("media_caption");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("media_url");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("thumb_image");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("preview_type");
        int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("status");
        int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("media_duration");
        C16310on A02 = this.A05.A02();
        long j = -1;
        int i = 0;
        int i2 = 0;
        while (cursor.moveToNext()) {
            try {
                j = cursor.getLong(columnIndexOrThrow);
                if (cursor.getInt(columnIndexOrThrow7) == 6) {
                    i2++;
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(j));
                    String string = cursor.getString(columnIndexOrThrow2);
                    if (!TextUtils.isEmpty(string)) {
                        contentValues.put("description", string);
                        z = true;
                    } else {
                        contentValues.putNull("description");
                        z = false;
                    }
                    String string2 = cursor.getString(columnIndexOrThrow3);
                    if (!TextUtils.isEmpty(string2)) {
                        contentValues.put("page_title", string2);
                        z = true;
                    } else {
                        contentValues.putNull("page_title");
                    }
                    String string3 = cursor.getString(columnIndexOrThrow4);
                    if (!TextUtils.isEmpty(string3)) {
                        contentValues.put("url", string3);
                        z = true;
                    } else {
                        contentValues.putNull("url");
                    }
                    byte[] blob = cursor.getBlob(columnIndexOrThrow5);
                    if (blob == null) {
                        contentValues.putNull("font_style");
                        contentValues.putNull("text_color");
                        contentValues.putNull("background_color");
                    } else {
                        try {
                            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(blob));
                            try {
                                Object readObject = objectInputStream.readObject();
                                if (readObject instanceof TextData) {
                                    TextData textData = (TextData) readObject;
                                    contentValues.put("font_style", Integer.valueOf(textData.fontStyle));
                                    contentValues.put("text_color", Integer.valueOf(textData.textColor));
                                    contentValues.put("background_color", Integer.valueOf(textData.backgroundColor));
                                    byte[] bArr = textData.thumbnail;
                                    if (bArr != null) {
                                        this.A02.A05(A0W(cursor), bArr, j);
                                    }
                                    z = true;
                                } else if (readObject instanceof byte[]) {
                                    this.A02.A05(A0W(cursor), (byte[]) readObject, j);
                                }
                                objectInputStream.close();
                            } catch (Throwable th) {
                                try {
                                    objectInputStream.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                                break;
                            }
                        } catch (Exception unused2) {
                        }
                    }
                    int i3 = (int) cursor.getLong(columnIndexOrThrow6);
                    contentValues.put("preview_type", Integer.valueOf(i3));
                    contentValues.put("invite_link_group_type", Integer.valueOf(cursor.getInt(columnIndexOrThrow8)));
                    if (z || i3 != 0) {
                        A02.A03.A03(contentValues, "message_text");
                    }
                    i++;
                }
            } catch (Throwable th2) {
                try {
                    A02.close();
                } catch (Throwable unused3) {
                }
                throw th2;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(i, j, i2);
    }

    public final AnonymousClass1IS A0W(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("key_from_me");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("key_id");
        AbstractC14640lm A06 = this.A00.A06(cursor);
        int i = cursor.getInt(columnIndexOrThrow);
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        return new AnonymousClass1IS(A06, cursor.getString(columnIndexOrThrow2), z);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_text", null, null);
            C21390xL r1 = this.A06;
            r1.A03("text_ready");
            r1.A03("migration_message_text_index");
            r1.A03("migration_message_text_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("TextMessageStore/TextMessageDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
