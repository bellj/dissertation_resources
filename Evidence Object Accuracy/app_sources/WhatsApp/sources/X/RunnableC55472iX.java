package X;

import android.net.ConnectivityManager;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.2iX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55472iX extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AnonymousClass2CS A00;
    public final /* synthetic */ boolean A01;

    public RunnableC55472iX(AnonymousClass2CS r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass2CS r2 = this.A00;
        ConnectivityManager.NetworkCallback networkCallback = r2.A00;
        if (networkCallback == null) {
            Log.i("voip/weak-wifi/onUnavailable: network callback is already unregistered");
            return;
        }
        r2.A04.unregisterNetworkCallback(networkCallback);
        r2.A00 = null;
        r2.A01 = null;
        Voip.notifyFailureToCreateAlternativeSocket(this.A01);
    }
}
