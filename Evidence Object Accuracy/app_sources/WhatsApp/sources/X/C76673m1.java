package X;

import java.nio.ByteBuffer;

/* renamed from: X.3m1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76673m1 extends AbstractC106504vo {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public boolean A05;
    public boolean A06;
    public byte[] A07;
    public byte[] A08;
    public final long A09 = 150000;
    public final long A0A = 20000;
    public final short A0B = 1024;

    public C76673m1() {
        byte[] bArr = AnonymousClass3JZ.A0A;
        this.A07 = bArr;
        this.A08 = bArr;
    }

    public final void A01(ByteBuffer byteBuffer, byte[] bArr, int i) {
        int remaining = byteBuffer.remaining();
        int i2 = this.A02;
        int min = Math.min(remaining, i2);
        int i3 = i2 - min;
        System.arraycopy(bArr, i - i3, this.A08, 0, i3);
        byteBuffer.position(byteBuffer.limit() - min);
        byteBuffer.get(this.A08, i3, min);
    }

    public final void A02(byte[] bArr, int i) {
        ByteBuffer A00 = A00(i);
        A00.put(bArr, 0, i);
        A00.flip();
        if (i > 0) {
            this.A06 = true;
        }
    }

    @Override // X.AnonymousClass5Xx
    public void AZk(ByteBuffer byteBuffer) {
        int limit;
        int limit2;
        long j;
        int i;
        int position;
        while (byteBuffer.hasRemaining() && !super.A05.hasRemaining()) {
            int i2 = this.A03;
            if (i2 == 0) {
                int limit3 = byteBuffer.limit();
                byteBuffer.limit(Math.min(limit3, byteBuffer.position() + this.A07.length));
                int limit4 = byteBuffer.limit();
                while (true) {
                    limit4 -= 2;
                    if (limit4 >= byteBuffer.position()) {
                        if (Math.abs((int) byteBuffer.getShort(limit4)) > this.A0B) {
                            int i3 = this.A00;
                            position = ((limit4 / i3) * i3) + i3;
                            break;
                        }
                    } else {
                        position = byteBuffer.position();
                        break;
                    }
                }
                if (position == byteBuffer.position()) {
                    this.A03 = 1;
                } else {
                    byteBuffer.limit(position);
                    int remaining = byteBuffer.remaining();
                    ByteBuffer A00 = A00(remaining);
                    A00.put(byteBuffer);
                    A00.flip();
                    if (remaining > 0) {
                        this.A06 = true;
                    }
                }
                byteBuffer.limit(limit3);
            } else if (i2 == 1) {
                int limit5 = byteBuffer.limit();
                int position2 = byteBuffer.position();
                while (true) {
                    if (position2 >= byteBuffer.limit()) {
                        limit2 = byteBuffer.limit();
                        break;
                    } else if (Math.abs((int) byteBuffer.getShort(position2)) > this.A0B) {
                        int i4 = this.A00;
                        limit2 = i4 * (position2 / i4);
                        break;
                    } else {
                        position2 += 2;
                    }
                }
                int position3 = limit2 - byteBuffer.position();
                byte[] bArr = this.A07;
                int length = bArr.length;
                int i5 = this.A01;
                int i6 = length - i5;
                if (limit2 >= limit5 || position3 >= i6) {
                    int min = Math.min(position3, i6);
                    byteBuffer.limit(byteBuffer.position() + min);
                    byteBuffer.get(this.A07, this.A01, min);
                    int i7 = this.A01 + min;
                    this.A01 = i7;
                    byte[] bArr2 = this.A07;
                    if (i7 == bArr2.length) {
                        if (this.A06) {
                            A02(bArr2, this.A02);
                            j = this.A04;
                            i7 = this.A01;
                            i = this.A02 << 1;
                        } else {
                            j = this.A04;
                            i = this.A02;
                        }
                        this.A04 = j + ((long) ((i7 - i) / this.A00));
                        A01(byteBuffer, this.A07, i7);
                        this.A01 = 0;
                        this.A03 = 2;
                    }
                    byteBuffer.limit(limit5);
                } else {
                    A02(bArr, i5);
                    this.A01 = 0;
                    this.A03 = 0;
                }
            } else if (i2 == 2) {
                int limit6 = byteBuffer.limit();
                int position4 = byteBuffer.position();
                while (true) {
                    if (position4 >= byteBuffer.limit()) {
                        limit = byteBuffer.limit();
                        break;
                    } else if (Math.abs((int) byteBuffer.getShort(position4)) > this.A0B) {
                        int i8 = this.A00;
                        limit = i8 * (position4 / i8);
                        break;
                    } else {
                        position4 += 2;
                    }
                }
                byteBuffer.limit(limit);
                this.A04 += (long) (byteBuffer.remaining() / this.A00);
                A01(byteBuffer, this.A08, this.A02);
                if (limit < limit6) {
                    A02(this.A08, this.A02);
                    this.A03 = 0;
                    byteBuffer.limit(limit6);
                }
            } else {
                throw C72463ee.A0D();
            }
        }
    }
}
