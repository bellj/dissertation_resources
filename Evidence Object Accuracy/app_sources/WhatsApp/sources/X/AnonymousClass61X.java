package X;

import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.61X  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61X {
    public static final Rect A00 = new Rect(-1000, -1000, 1000, 1000);
    public static final boolean A01;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 17) {
            z = true;
        }
        A01 = z;
    }

    public static C127805v5 A00(Camera.Parameters parameters, String str, String str2) {
        String str3 = parameters.get(str);
        ArrayList A02 = A02(str2);
        SparseArray sparseArray = new SparseArray();
        ArrayList A0w = C12980iv.A0w(A02.size());
        Iterator it = A02.iterator();
        int i = -1;
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            try {
                String str4 = A0x;
                if (A0x.startsWith("ISO")) {
                    str4 = A0x.substring(3);
                }
                Integer valueOf = Integer.valueOf(Integer.parseInt(str4));
                int intValue = valueOf.intValue();
                sparseArray.put(intValue, A0x);
                A0w.add(valueOf);
                if (A0x.equals(str3)) {
                    i = intValue;
                }
            } catch (NumberFormatException unused) {
            }
        }
        return new C127805v5(sparseArray, str, str3, A0w, i);
    }

    public static String A01(List list) {
        if (list.isEmpty()) {
            return "()";
        }
        StringBuilder A0k = C12960it.A0k("(");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Camera.Area area = (Camera.Area) list.get(i);
            A0k.append('[');
            A0k.append(area.rect.flattenToString());
            A0k.append(' ');
            A0k.append(area.weight);
            A0k.append("] ");
        }
        return C12970iu.A0u(A0k);
    }

    public static ArrayList A02(String str) {
        if (TextUtils.isEmpty(str)) {
            return C12960it.A0l();
        }
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
        simpleStringSplitter.setString(str);
        ArrayList A0l = C12960it.A0l();
        Iterator<String> it = simpleStringSplitter.iterator();
        while (it.hasNext()) {
            A0l.add(it.next());
        }
        return A0l;
    }

    public static ArrayList A03(String str) {
        if (str == null || str.isEmpty() || str.charAt(0) != '(' || str.charAt(str.length() - 1) != ')') {
            Log.e("ParametersHelper", C12960it.A0d(str, C12960it.A0k("Invalid area string=")));
        } else if (!str.equals("(0,0,0,0,0)") && !str.equals("(0, 0, 0, 0, 0)")) {
            ArrayList A0l = C12960it.A0l();
            int i = 0;
            do {
                int indexOf = str.indexOf(41, i);
                String substring = str.substring(i, indexOf + 1);
                Camera.Area area = null;
                if (substring == null || substring.isEmpty() || substring.charAt(0) != '(' || substring.charAt(substring.length() - 1) != ')') {
                    Log.e("ParametersHelper", C12960it.A0d(substring, C12960it.A0j("Invalid area string=")));
                } else {
                    Rect rect = new Rect();
                    try {
                        int indexOf2 = substring.indexOf(44);
                        rect.left = C117305Zk.A02(substring, 1, indexOf2);
                        int i2 = indexOf2 + 1;
                        int indexOf3 = substring.indexOf(44, i2);
                        rect.top = C117305Zk.A02(substring, i2, indexOf3);
                        int i3 = indexOf3 + 1;
                        int indexOf4 = substring.indexOf(44, i3);
                        rect.right = C117305Zk.A02(substring, i3, indexOf4);
                        int i4 = indexOf4 + 1;
                        int indexOf5 = substring.indexOf(44, i4);
                        rect.bottom = C117305Zk.A02(substring, i4, indexOf5);
                        int i5 = indexOf5 + 1;
                        int indexOf6 = substring.indexOf(44, i5);
                        if (indexOf6 == -1) {
                            indexOf6 = substring.indexOf(41, i5);
                        }
                        area = new Camera.Area(rect, Integer.parseInt(substring.substring(i5, indexOf6).trim()));
                    } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
                        Log.e("ParametersHelper", C12960it.A0d(substring, C12960it.A0j("Invalid area string=")), e);
                    }
                }
                if (area != null) {
                    A0l.add(area);
                }
                i = str.indexOf(40, indexOf);
            } while (i != -1);
            if (!A0l.isEmpty()) {
                if (A0l.size() == 1) {
                    Camera.Area area2 = (Camera.Area) A0l.get(0);
                    Rect rect2 = area2.rect;
                    if (rect2.left == 0 && rect2.top == 0 && rect2.right == 0 && rect2.bottom == 0 && area2.weight == 0) {
                        return null;
                    }
                }
                return A0l;
            }
        }
        return null;
    }

    public static List A04(Rect rect) {
        int i;
        Rect rect2 = A00;
        if (!rect.intersect(rect2) && !rect2.contains(rect)) {
            int i2 = rect.right;
            int i3 = rect2.left;
            int i4 = 0;
            if (i2 <= i3) {
                i = i3 - rect.left;
            } else {
                int i5 = rect.left;
                int i6 = rect2.right;
                i = i6 - i2;
                if (i5 < i6) {
                    i = 0;
                }
            }
            int i7 = rect.bottom;
            int i8 = rect2.top;
            if (i7 <= i8) {
                i4 = i8 - rect.top;
            } else {
                int i9 = rect.top;
                int i10 = rect2.bottom;
                if (i9 >= i10) {
                    i4 = i10 - i7;
                }
            }
            rect.offset(i, i4);
            rect.intersect(rect2);
        }
        ArrayList A0l = C12960it.A0l();
        A0l.add(new Camera.Area(rect, 1000));
        return A0l;
    }
}
