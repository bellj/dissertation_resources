package X;

import com.whatsapp.wabloks.ui.WaBloksActivity;
import java.util.Map;

/* renamed from: X.61W  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass61W {
    public static void A00(C48912Ik r0, WaBloksActivity waBloksActivity) {
        waBloksActivity.A01 = r0;
    }

    public static void A01(C18580sg r0, WaBloksActivity waBloksActivity) {
        waBloksActivity.A03 = r0;
    }

    public static void A02(C17120qI r0, WaBloksActivity waBloksActivity) {
        waBloksActivity.A04 = r0;
    }

    public static void A03(C18840t8 r0, WaBloksActivity waBloksActivity) {
        waBloksActivity.A05 = r0;
    }

    public static void A04(WaBloksActivity waBloksActivity, AnonymousClass01H r1) {
        waBloksActivity.A08 = r1;
    }

    public static void A05(WaBloksActivity waBloksActivity, Map map) {
        waBloksActivity.A0D = map;
    }

    public static void A06(WaBloksActivity waBloksActivity, Map map) {
        waBloksActivity.A0E = map;
    }
}
