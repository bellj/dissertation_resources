package X;

import android.app.Notification;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0Xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07310Xm implements AbstractC11240fy {
    public int A00;
    public RemoteViews A01;
    public final Notification.Builder A02;
    public final Context A03;
    public final Bundle A04 = new Bundle();
    public final C005602s A05;
    public final List A06 = new ArrayList();

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v6, types: [java.util.List, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r0v84, types: [X.01b, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r9v3, types: [java.util.AbstractCollection, java.util.Collection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r9v4 */
    /* JADX WARN: Type inference failed for: r0v92, types: [X.01b, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r9v5, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0297, code lost:
        if (r3 != null) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01f2, code lost:
        if (r9 != null) goto L_0x01f4;
     */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C07310Xm(X.C005602s r15) {
        /*
        // Method dump skipped, instructions count: 925
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07310Xm.<init>(X.02s):void");
    }

    public static List A00(List list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C007303s r1 = (C007303s) it.next();
            String str = r1.A03;
            if (str == null) {
                CharSequence charSequence = r1.A01;
                if (charSequence != null) {
                    StringBuilder sb = new StringBuilder("name:");
                    sb.append((Object) charSequence);
                    str = sb.toString();
                } else {
                    str = "";
                }
            }
            arrayList.add(str);
        }
        return arrayList;
    }

    public Notification A01() {
        RemoteViews remoteViews;
        Notification build;
        C005602s r8 = this.A05;
        AbstractC006703e r7 = r8.A0F;
        if (r7 != null) {
            r7.A08(this);
            remoteViews = r7.A04(this);
        } else {
            remoteViews = null;
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            build = this.A02.build();
        } else {
            if (i >= 24) {
                build = this.A02.build();
            } else if (i >= 21 || i >= 20) {
                Notification.Builder builder = this.A02;
                builder.setExtras(this.A04);
                build = builder.build();
                RemoteViews remoteViews2 = this.A01;
                if (remoteViews2 != null) {
                    build.contentView = remoteViews2;
                }
            } else {
                if (i >= 19) {
                    List list = this.A06;
                    int size = list.size();
                    SparseArray<? extends Parcelable> sparseArray = null;
                    for (int i2 = 0; i2 < size; i2++) {
                        Object obj = list.get(i2);
                        if (obj != 0) {
                            if (sparseArray == null) {
                                sparseArray = new SparseArray<>();
                            }
                            sparseArray.put(i2, obj);
                        }
                    }
                    if (sparseArray != null) {
                        this.A04.putSparseParcelableArray("android.support.actionExtras", sparseArray);
                    }
                    Notification.Builder builder2 = this.A02;
                    builder2.setExtras(this.A04);
                    build = builder2.build();
                } else {
                    build = this.A02.build();
                    Bundle A00 = C005902v.A00(build);
                    Bundle bundle = this.A04;
                    Bundle bundle2 = new Bundle(bundle);
                    for (String str : bundle.keySet()) {
                        if (A00.containsKey(str)) {
                            bundle2.remove(str);
                        }
                    }
                    A00.putAll(bundle2);
                    List list2 = this.A06;
                    int size2 = list2.size();
                    SparseArray<? extends Parcelable> sparseArray2 = null;
                    for (int i3 = 0; i3 < size2; i3++) {
                        Object obj2 = list2.get(i3);
                        if (obj2 != 0) {
                            if (sparseArray2 == null) {
                                sparseArray2 = new SparseArray<>();
                            }
                            sparseArray2.put(i3, obj2);
                        }
                    }
                    if (sparseArray2 != null) {
                        C005902v.A00(build).putSparseParcelableArray("android.support.actionExtras", sparseArray2);
                    }
                }
                RemoteViews remoteViews3 = this.A01;
                if (remoteViews3 != null) {
                    build.contentView = remoteViews3;
                }
            }
            int i4 = this.A00;
            if (i4 != 0) {
                if (!(build.getGroup() == null || (build.flags & 512) == 0 || i4 != 2)) {
                    build.sound = null;
                    build.vibrate = null;
                    int i5 = build.defaults & -2;
                    build.defaults = i5;
                    build.defaults = i5 & -3;
                }
                if (build.getGroup() != null && (build.flags & 512) == 0 && i4 == 1) {
                    build.sound = null;
                    build.vibrate = null;
                    int i6 = build.defaults & -2;
                    build.defaults = i6;
                    build.defaults = i6 & -3;
                }
            }
        }
        if (!(remoteViews == null && (remoteViews = r8.A0E) == null)) {
            build.contentView = remoteViews;
        }
        if (r7 != null) {
            RemoteViews A03 = r7.A03(this);
            if (A03 != null) {
                build.bigContentView = A03;
            }
            Bundle A002 = C005902v.A00(build);
            if (A002 != null) {
                r7.A06(A002);
            }
        }
        return build;
    }
}
