package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.3ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71263ce implements Comparator {
    public final Collator A00;

    public C71263ce(AnonymousClass018 r3) {
        Collator instance = Collator.getInstance(C12970iu.A14(r3));
        this.A00 = instance;
        instance.setDecomposition(1);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return this.A00.compare(((C30721Yo) obj).A08(), ((C30721Yo) obj2).A08());
    }
}
