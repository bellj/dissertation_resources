package X;

import androidx.work.impl.WorkDatabase;
import java.util.Map;

/* renamed from: X.0eD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10210eD implements Runnable {
    public static final String A03 = C06390Tk.A01("StopWorkRunnable");
    public final AnonymousClass022 A00;
    public final String A01;
    public final boolean A02;

    public RunnableC10210eD(AnonymousClass022 r1, String str, boolean z) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = z;
    }

    @Override // java.lang.Runnable
    public void run() {
        Map map;
        boolean containsKey;
        boolean z;
        AnonymousClass022 r0 = this.A00;
        WorkDatabase workDatabase = r0.A04;
        C07650Zp r9 = r0.A03;
        AbstractC12700iM A0B = workDatabase.A0B();
        workDatabase.A03();
        try {
            String str = this.A01;
            Object obj = r9.A09;
            synchronized (obj) {
                map = r9.A07;
                containsKey = map.containsKey(str);
            }
            if (this.A02) {
                synchronized (obj) {
                    C06390Tk.A00().A02(C07650Zp.A0B, String.format("Processor stopping foreground work %s", str), new Throwable[0]);
                    z = C07650Zp.A00((RunnableC10250eH) map.remove(str), str);
                }
            } else {
                if (!containsKey && A0B.AGv(str) == EnumC03840Ji.RUNNING) {
                    A0B.Act(EnumC03840Ji.ENQUEUED, str);
                }
                synchronized (obj) {
                    C06390Tk.A00().A02(C07650Zp.A0B, String.format("Processor stopping background work %s", str), new Throwable[0]);
                    z = C07650Zp.A00((RunnableC10250eH) r9.A06.remove(str), str);
                }
            }
            C06390Tk.A00().A02(A03, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", str, Boolean.valueOf(z)), new Throwable[0]);
            workDatabase.A05();
        } finally {
            workDatabase.A04();
        }
    }
}
