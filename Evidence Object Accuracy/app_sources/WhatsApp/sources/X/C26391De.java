package X;

import android.os.Bundle;
import android.os.Message;

/* renamed from: X.1De  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26391De {
    public final Bundle A00;
    public final Message A01;
    public final String A02;

    public C26391De(Message message, String str) {
        AnonymousClass009.A0E(true);
        this.A02 = str;
        this.A00 = null;
        this.A01 = message;
    }

    public C26391De(String str) {
        AnonymousClass009.A0E(true);
        this.A02 = str;
        this.A00 = null;
        this.A01 = null;
    }

    public C26391De(String str, Bundle bundle) {
        AnonymousClass009.A0E(true);
        this.A02 = str;
        this.A00 = bundle;
        this.A01 = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("action=");
        sb.append(this.A02);
        sb.append(", args=");
        sb.append(this.A00);
        sb.append(", message=");
        sb.append(this.A01);
        return sb.toString();
    }
}
