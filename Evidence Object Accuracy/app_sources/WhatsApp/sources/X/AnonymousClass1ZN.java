package X;

/* renamed from: X.1ZN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZN {
    public boolean A00;
    public final int A01;
    public final AnonymousClass1Z8 A02;
    public final String A03;
    public final String A04;

    public AnonymousClass1ZN(AnonymousClass1Z8 r1, String str, String str2, int i, boolean z) {
        this.A04 = str;
        this.A03 = str2;
        this.A00 = z;
        this.A01 = i;
        this.A02 = r1;
    }

    public /* bridge */ /* synthetic */ Object clone() {
        String str = this.A04;
        String str2 = this.A03;
        boolean z = this.A00;
        return new AnonymousClass1ZN(this.A02, str, str2, this.A01, z);
    }
}
