package X;

import java.util.List;

/* renamed from: X.21C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21C {
    public final int A00;
    public final String A01;
    public final List A02;

    public AnonymousClass21C(List list, int i, String str) {
        this.A01 = str;
        this.A00 = i;
        this.A02 = list;
    }
}
