package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2he  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55012he extends AnonymousClass03U {
    public View A00;
    public ImageView A01;
    public TextView A02;
    public TextEmojiLabel A03;
    public TextEmojiLabel A04;
    public C28801Pb A05;

    public C55012he(View view, C15610nY r4, AnonymousClass12F r5) {
        super(view);
        this.A05 = new C28801Pb(view, r4, r5, (int) R.id.name);
        this.A04 = C12970iu.A0U(view, R.id.status);
        this.A01 = C12970iu.A0L(view, R.id.avatar);
        this.A00 = view.findViewById(R.id.divider);
        this.A02 = C12960it.A0J(view, R.id.invite);
        this.A03 = C12970iu.A0U(view, R.id.push_name);
        AnonymousClass028.A0a(this.A01, 2);
        view.setBackgroundResource(R.drawable.selector_orange_gradient);
        view.setFocusable(true);
        view.setClickable(true);
    }
}
