package X;

import com.whatsapp.contact.picker.PhoneContactsSelector;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.37W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37W extends AbstractC16350or {
    public final AnonymousClass018 A00;
    public final WeakReference A01;
    public final ArrayList A02;
    public final List A03;

    public AnonymousClass37W(PhoneContactsSelector phoneContactsSelector, AnonymousClass018 r3, List list, List list2) {
        ArrayList arrayList;
        this.A00 = r3;
        this.A01 = C12970iu.A10(phoneContactsSelector);
        if (list != null) {
            arrayList = C12980iv.A0x(list);
        } else {
            arrayList = null;
        }
        this.A02 = arrayList;
        this.A03 = C12980iv.A0x(list2);
    }
}
