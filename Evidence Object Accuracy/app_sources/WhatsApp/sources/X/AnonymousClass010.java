package X;

import java.lang.ref.WeakReference;

/* renamed from: X.010  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass010 {
    public final /* synthetic */ C08800bs A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ AbstractC14200l1 A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ WeakReference A04;

    public AnonymousClass010(C08800bs r1, C14230l4 r2, AbstractC14200l1 r3, String str, WeakReference weakReference) {
        this.A00 = r1;
        this.A04 = weakReference;
        this.A03 = str;
        this.A02 = r3;
        this.A01 = r2;
    }
}
