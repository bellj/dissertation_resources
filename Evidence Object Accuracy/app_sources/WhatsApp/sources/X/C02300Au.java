package X;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* renamed from: X.0Au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02300Au extends GestureDetector.SimpleOnGestureListener {
    public final AnonymousClass0BZ A00;

    public C02300Au(AnonymousClass0BZ r1) {
        this.A00 = r1;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        AnonymousClass0BZ r3 = this.A00;
        if (r3.getContext() == null) {
            return false;
        }
        float translationY = r3.getTranslationY();
        if (f2 > 0.0f) {
            r3.A01((int) Math.abs(((((float) r3.getHeight()) - translationY) / f2) * 1000.0f));
        } else {
            r3.A02(null, (int) Math.abs((translationY / (-f2)) * 1000.0f));
        }
        r3.A0A = true;
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        AnonymousClass0BZ r3 = this.A00;
        if (r3.getTranslationY() <= 0.0f && f2 > 0.0f) {
            return false;
        }
        r3.A0A = false;
        return true;
    }
}
