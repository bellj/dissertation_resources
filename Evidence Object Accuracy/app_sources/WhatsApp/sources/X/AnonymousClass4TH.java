package X;

import android.net.Uri;
import java.io.File;

/* renamed from: X.4TH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TH {
    public final double A00;
    public final int A01;
    public final Uri A02;
    public final File A03;
    public final String A04;
    public final String A05;

    public AnonymousClass4TH(Uri uri, File file, String str, String str2, double d, int i) {
        this.A00 = d;
        this.A01 = i;
        this.A04 = str;
        this.A02 = uri;
        this.A03 = file;
        this.A05 = str2;
    }
}
