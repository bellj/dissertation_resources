package X;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.util.List;

/* renamed from: X.3Ho  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64953Ho {
    public static void A00(View view, C14260l7 r3, Object obj) {
        if (Build.VERSION.SDK_INT >= 17) {
            new Handler(Looper.myLooper()).post(new RunnableC55492id(view, r3, obj));
        }
    }

    public static void A01(AnonymousClass28D r3, List list) {
        String valueOf;
        C89054Im r0;
        View view;
        if (!(Build.VERSION.SDK_INT < 22 || list == null || list.isEmpty())) {
            for (Object obj : list) {
                if (obj == null) {
                    valueOf = null;
                } else if (obj instanceof String) {
                    valueOf = (String) obj;
                } else {
                    valueOf = String.valueOf(obj);
                }
                AnonymousClass28D A00 = C87854Dg.A00(r3, new C1093351h(valueOf));
                if (A00 != null && (r0 = A00.A03) != null && (view = r0.A00) != null) {
                    view.setAccessibilityTraversalAfter(-1);
                } else {
                    return;
                }
            }
        }
    }

    public static void A02(AnonymousClass28D r2, List list) {
        if (Build.VERSION.SDK_INT >= 22 && list != null && !list.isEmpty()) {
            new Handler(Looper.myLooper()).post(new RunnableC55462iW(r2, list));
        }
    }
}
