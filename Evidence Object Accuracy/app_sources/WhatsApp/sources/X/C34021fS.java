package X;

import com.whatsapp.util.Log;

/* renamed from: X.1fS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34021fS implements AbstractC32491cF, AbstractC34031fT {
    public final AbstractC34011fR A00;
    public final C14860mA A01;

    public C34021fS(AbstractC34011fR r1, C14860mA r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r0 == false) goto L_0x001b;
     */
    @Override // X.AbstractC32491cF
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Aaw(int r5) {
        /*
            r4 = this;
            r0 = 500(0x1f4, float:7.0E-43)
            if (r5 < r0) goto L_0x0050
            X.0mA r3 = r4.A01
            X.1fR r2 = r4.A00
            boolean r0 = r2 instanceof X.AnonymousClass2CW
            if (r0 != 0) goto L_0x004a
            boolean r0 = r2 instanceof X.C34001fQ
            if (r0 != 0) goto L_0x0044
            boolean r0 = r2 instanceof X.AnonymousClass2CX
            if (r0 == 0) goto L_0x001b
            r0 = r2
            X.2CX r0 = (X.AnonymousClass2CX) r0
            boolean r0 = r0.A06
        L_0x0019:
            if (r0 != 0) goto L_0x002d
        L_0x001b:
            java.lang.String r1 = r2.A00
            if (r1 == 0) goto L_0x0075
            X.0mD r0 = r3.A0N
            X.1UP r0 = r0.A00()
            java.lang.String r0 = r0.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0075
        L_0x002d:
            java.lang.String r1 = "qr_error 500 queueing: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            java.util.List r0 = r3.A0Q
            r0.add(r2)
            return
        L_0x0044:
            r0 = r2
            X.1fQ r0 = (X.C34001fQ) r0
            boolean r0 = r0.A02
            goto L_0x0019
        L_0x004a:
            r0 = r2
            X.2CW r0 = (X.AnonymousClass2CW) r0
            boolean r0 = r0.A05
            goto L_0x0019
        L_0x0050:
            r0 = 400(0x190, float:5.6E-43)
            if (r5 < r0) goto L_0x005b
            X.0mA r1 = r4.A01
            r0 = 0
            r1.A0L(r0)
            return
        L_0x005b:
            java.lang.String r0 = "unexpected return code: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r0 = " op: "
            r1.append(r0)
            X.1fR r0 = r4.A00
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x0083
        L_0x0075:
            java.lang.String r1 = "qr_error 500 op invalid dropping: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0083:
            com.whatsapp.util.Log.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34021fS.Aaw(int):void");
    }

    @Override // X.AbstractC34031fT
    public void Ab0(Exception exc) {
        StringBuilder sb = new StringBuilder("qr_exception: ");
        sb.append(this.A00);
        Log.e(sb.toString(), exc);
    }
}
