package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59232uD extends AnonymousClass32K {
    public boolean A00;
    public final C90104Mp A01;
    public final C92404Vt A02;
    public final C19830uk A03;
    public final C18640sm A04;

    public C59232uD(C90104Mp r8, C92404Vt r9, C19810ui r10, C17560r0 r11, AnonymousClass4RQ r12, C17570r1 r13, C19830uk r14, C18640sm r15, AbstractC14440lR r16) {
        super(r10, r11, r12, r13, r16, 6);
        this.A03 = r14;
        this.A04 = r15;
        this.A01 = r8;
        this.A02 = r9;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r4, JSONObject jSONObject, int i) {
        try {
            if (!A03(this.A02.A02, r4.A00, true)) {
                A04(i);
            }
        } catch (Exception unused) {
            A04(i);
        }
    }

    public final void A04(int i) {
        C90104Mp r0 = this.A01;
        r0.A00.AQJ(this.A02, i);
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        Log.e("GetCategoriesGraphQLService/onDeliveryFailure", iOException);
        if (this.A00) {
            try {
                if (!A03(this.A02.A02, -1, false)) {
                    A04(-1);
                }
            } catch (Exception unused) {
                A04(0);
            }
        } else {
            this.A00 = true;
            A02();
        }
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        A04(422);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        Log.e("GetCategoriesGraphQLService/onError", exc);
        try {
            if (!A03(this.A02.A02, 0, false)) {
                A04(0);
            }
        } catch (Exception unused) {
            A04(0);
        }
    }
}
