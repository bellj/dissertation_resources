package X;

/* renamed from: X.1bF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31871bF extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public /* synthetic */ C31871bF() {
        super(C31861bE.A03);
    }

    public void A05(int i) {
        A03();
        C31861bE r1 = (C31861bE) this.A00;
        r1.A00 |= 1;
        r1.A01 = i;
    }

    public void A06(AbstractC27881Jp r3) {
        A03();
        C31861bE r1 = (C31861bE) this.A00;
        r1.A00 |= 2;
        r1.A02 = r3;
    }
}
