package X;

import android.os.Message;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import com.whatsapp.util.Log;
import java.util.Arrays;

/* renamed from: X.10X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10X implements AbstractC15920o8 {
    public final C22690zU A00;
    public final C22490zA A01;
    public final AbstractC15710nm A02;
    public final C22260yn A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final C22330yu A06;
    public final C18850tA A07;
    public final C15550nR A08;
    public final AnonymousClass10S A09;
    public final AnonymousClass10W A0A;
    public final AnonymousClass10T A0B;
    public final AnonymousClass10V A0C;
    public final C20730wE A0D;
    public final C20840wP A0E;
    public final C16590pI A0F;
    public final C22320yt A0G;
    public final AnonymousClass10U A0H;
    public final C17220qS A0I;
    public final C20660w7 A0J;
    public final C17230qT A0K;
    public final C14910mF A0L;
    public final C22280yp A0M;
    public final C20750wG A0N;
    public final C20700wB A0O;
    public final AbstractC14440lR A0P;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{25};
    }

    public AnonymousClass10X(C22690zU r2, C22490zA r3, AbstractC15710nm r4, C22260yn r5, C14900mE r6, C15570nT r7, C22330yu r8, C18850tA r9, C15550nR r10, AnonymousClass10S r11, AnonymousClass10W r12, AnonymousClass10T r13, AnonymousClass10V r14, C20730wE r15, C20840wP r16, C16590pI r17, C22320yt r18, AnonymousClass10U r19, C17220qS r20, C20660w7 r21, C17230qT r22, C14910mF r23, C22280yp r24, C20750wG r25, C20700wB r26, AbstractC14440lR r27) {
        this.A0F = r17;
        this.A04 = r6;
        this.A02 = r4;
        this.A05 = r7;
        this.A0P = r27;
        this.A0J = r21;
        this.A07 = r9;
        this.A0I = r20;
        this.A08 = r10;
        this.A03 = r5;
        this.A0L = r23;
        this.A0M = r24;
        this.A09 = r11;
        this.A0O = r26;
        this.A0G = r18;
        this.A06 = r8;
        this.A0B = r13;
        this.A0D = r15;
        this.A0H = r19;
        this.A00 = r2;
        this.A0C = r14;
        this.A0K = r22;
        this.A0N = r25;
        this.A0A = r12;
        this.A0E = r16;
        this.A01 = r3;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        boolean z;
        C42251uu A01;
        String str;
        boolean z2 = false;
        if (i != 25) {
            return false;
        }
        Parcelable parcelable = message.getData().getParcelable("stanzaKey");
        AnonymousClass009.A06(parcelable, "stanzaKey is null");
        AnonymousClass1OT r4 = (AnonymousClass1OT) parcelable;
        AnonymousClass1V8 r7 = (AnonymousClass1V8) message.obj;
        AnonymousClass1V8 A0D = r7.A0D(0);
        AnonymousClass2QR r3 = (AnonymousClass2QR) this.A0K.A00(2, r4.A00);
        if (r3 != null) {
            if (A0D != null) {
                str = A0D.A00;
            } else {
                str = null;
            }
            r3.A00 = str;
            r3.A02(3);
        }
        if (A0D != null) {
            if (AnonymousClass1V8.A02(A0D, "add")) {
                this.A05.A08();
                z = true;
                byte[] bArr = A0D.A01;
                C42271uw r32 = new C42271uw(AnonymousClass1JA.A0E);
                r32.A02 = true;
                r32.A00 = AnonymousClass1JB.A09;
                r32.A02(bArr);
                A01 = r32.A01();
            } else if (AnonymousClass1V8.A02(A0D, "remove")) {
                this.A05.A08();
                Jid A0A = A0D.A0A(this.A02, UserJid.class, "jid");
                this.A0G.A01(new RunnableBRunnable0Shape6S0200000_I0_6(this, 49, A0A), 45);
                this.A0P.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(this, A0A, r4, 32));
                z2 = true;
            } else {
                if (AnonymousClass1V8.A02(A0D, "update")) {
                    String A0I = A0D.A0I("hash", null);
                    z = true;
                    if (A0I == null) {
                        AbstractC15710nm r72 = this.A02;
                        UserJid userJid = (UserJid) A0D.A0A(r72, UserJid.class, "jid");
                        C15550nR r12 = this.A08;
                        C15370n3 A0A2 = r12.A0A(userJid);
                        if (A0A2 != null) {
                            this.A03.A01(new RunnableBRunnable0Shape1S0300000_I0_1(this, A0A2, userJid, 30));
                            C22280yp r8 = this.A0M;
                            if (!r8.A06(userJid)) {
                                r8.A01(userJid, 2, 0);
                                this.A04.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(this, 0, userJid));
                            }
                            if (this.A01.A00().A01(userJid) && this.A0L.A00 != 3) {
                                r8.A04(userJid);
                            }
                            new AnonymousClass1VQ(r72, this.A0F, this.A0I, new C47872Db(this.A04, this.A07, r12, this.A09, this.A0A, this.A0P)).A00(userJid, A0A2.A0B);
                        }
                    } else if (!TextUtils.isEmpty(A0I)) {
                        byte[] decode = Base64.decode(A0I.getBytes(), 0);
                        Arrays.toString(decode);
                        C42271uw r2 = new C42271uw(AnonymousClass1JA.A0G);
                        r2.A02 = true;
                        r2.A00 = new AnonymousClass1JB(false, false, true, true, false, false, false, false, false);
                        r2.A02(decode);
                        A01 = r2.A01();
                    }
                } else if (AnonymousClass1V8.A02(A0D, "sync")) {
                    long j = 0;
                    long A012 = C28421Nd.A01(A0D.A0I("after", null), 0) * 1000;
                    long A013 = C28421Nd.A01(r7.A0I("t", null), 0) * 1000;
                    C14900mE r73 = this.A04;
                    RunnableBRunnable0Shape9S0100000_I0_9 runnableBRunnable0Shape9S0100000_I0_9 = new RunnableBRunnable0Shape9S0100000_I0_9(this, 40);
                    if (A012 > A013 && A013 > 0) {
                        j = A012 - A013;
                    }
                    r73.A0J(runnableBRunnable0Shape9S0100000_I0_9, j);
                } else if (AnonymousClass1V8.A02(A0D, "modify")) {
                    long A014 = C28421Nd.A01(r7.A0I("t", null), 0) * 1000;
                    AbstractC15710nm r33 = this.A02;
                    UserJid userJid2 = (UserJid) A0D.A0A(r33, UserJid.class, "old");
                    UserJid userJid3 = (UserJid) A0D.A0A(r33, UserJid.class, "new");
                    StringBuilder sb = new StringBuilder("contactupdatenotificationhandler/handleContactModify oldUserJid=");
                    sb.append(userJid2);
                    sb.append(" newUserJid=");
                    sb.append(userJid3);
                    Log.i(sb.toString());
                    C22690zU r5 = this.A00;
                    StringBuilder sb2 = new StringBuilder("ChangeNumberManager/onContactNumberChanged/oldJid=");
                    sb2.append(userJid2);
                    sb2.append("; newJid=");
                    sb2.append(userJid3);
                    Log.i(sb2.toString());
                    C22140ya r34 = r5.A07;
                    AnonymousClass15O r9 = r34.A03;
                    AnonymousClass1IS A02 = r9.A02(userJid2, true);
                    AbstractC15710nm r35 = r34.A00;
                    C30511Xs r15 = new C30511Xs(r35, A02, 28, A014);
                    r15.A01 = userJid2;
                    r15.A00 = userJid3;
                    C30511Xs r92 = new C30511Xs(r35, r9.A02(userJid3, true), 28, A014);
                    r92.A01 = userJid2;
                    r92.A00 = userJid3;
                    C15650ng r36 = r5.A04;
                    r36.A0S(r15);
                    r36.A0S(r92);
                    AnonymousClass15L r122 = r5.A03;
                    C19990v2 r11 = r122.A03;
                    AnonymousClass1PE A06 = r11.A06(userJid2);
                    if (A06 != null) {
                        r122.A00.A01(new RunnableBRunnable0Shape0S0300000_I0(r122, A06, r15, 41), 18);
                    }
                    AnonymousClass1PE A062 = r11.A06(userJid3);
                    if (A062 != null) {
                        r122.A00.A01(new RunnableBRunnable0Shape0S0300000_I0(r122, A062, r92, 41), 18);
                    }
                    C22230yk r112 = r5.A06;
                    C14890mD r10 = r112.A0G;
                    if (r10.A02() && userJid3 != null) {
                        String A022 = r112.A0H.A02();
                        r112.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 174, 0, new C49712Mf(userJid3, null, userJid2, A022)), A022, r10.A00().A03));
                    }
                    if (r10.A02() && userJid2 != null) {
                        String A023 = r112.A0H.A02();
                        r112.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 174, 0, new C49712Mf(userJid2, userJid3, null, A023)), A023, r10.A00().A03));
                    }
                }
                this.A0J.A0E(r4);
                z2 = true;
            }
            this.A0D.A03(A01, z);
            this.A0J.A0E(r4);
            z2 = true;
        }
        if (!z2) {
            this.A0J.A0E(r4);
            StringBuilder sb3 = new StringBuilder("ContactUpdateNotificationHandler/handleXmppMessage/handled-issue/operation was not handled: ");
            sb3.append(A0D);
            Log.w(sb3.toString());
            this.A02.AaV("ContactUpdateNotificationHandler/handleXmppMessage", "handled-issue", true);
        }
        return true;
    }
}
