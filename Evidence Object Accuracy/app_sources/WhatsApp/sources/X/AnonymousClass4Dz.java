package X;

import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.4Dz  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Dz {
    public static void A00(ImageView imageView) {
        imageView.setImageResource(R.drawable.catalog_product_placeholder_background);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
    }
}
