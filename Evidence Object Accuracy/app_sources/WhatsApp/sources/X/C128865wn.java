package X;

import com.whatsapp.payments.ui.PaymentsUnavailableDialogFragment;
import java.util.HashMap;

/* renamed from: X.5wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128865wn {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ C128135vc A01;
    public final /* synthetic */ C126955ti A02;

    public C128865wn(ActivityC13810kN r1, C128135vc r2, C126955ti r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public void A00(C452120p r5) {
        this.A01.A07.A06(C12960it.A0b("performNameCheck onError: ", r5));
        int i = r5.A00;
        if (i == 10755) {
            this.A00.Adm(PaymentsUnavailableDialogFragment.A00());
            return;
        }
        C126955ti r3 = this.A02;
        HashMap A11 = C12970iu.A11();
        A11.put("error_code", String.valueOf(i));
        r3.A00.A01("on_exception", A11);
    }
}
