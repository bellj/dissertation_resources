package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.40c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C848640c extends AbstractC848840e {
    public C848640c(View view) {
        super(view);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        super.A0A((AnonymousClass403) obj);
        ((AbstractC848840e) this).A01.setImageDrawable(AnonymousClass2GE.A00(this.A0H.getContext(), R.drawable.ic_location));
        ((AbstractC848840e) this).A03.setText(R.string.biz_dir_pick_location_on_map);
    }
}
