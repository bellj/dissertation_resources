package X;

import android.content.Context;
import android.util.Log;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54022fy extends AnonymousClass0ER implements AnonymousClass5QY {
    public final Set A00;
    public final Semaphore A01 = new Semaphore(0);

    public C54022fy(Context context, Set set) {
        super(context);
        this.A00 = set;
    }

    @Override // X.AnonymousClass0QL
    public final void A03() {
        this.A01.drainPermits();
        A09();
    }

    @Override // X.AnonymousClass0ER
    public final /* bridge */ /* synthetic */ Object A06() {
        int i = 0;
        for (AnonymousClass1U8 r0 : this.A00) {
            if (r0.A0B(this)) {
                i++;
            }
        }
        try {
            this.A01.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }
}
