package X;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3H7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3H7 {
    public final AnonymousClass4Yz A00;
    public final C94244bU A01;
    public final AnonymousClass3CV A02;
    public final AnonymousClass4PX A03;
    public final AnonymousClass4PX A04;
    public final AbstractC116425Vj A05;
    public final C90764Pd A06;
    public final AbstractC17450qp A07;
    public final String A08;
    public final List A09 = C12960it.A0l();
    public final Map A0A = C12970iu.A11();
    public final Map A0B;
    public final Set A0C;

    public AnonymousClass3H7(AnonymousClass4Yz r5, AnonymousClass4TT r6, C94244bU r7, AnonymousClass3CV r8, AbstractC116425Vj r9, C90764Pd r10, AbstractC17450qp r11, String str) {
        this.A06 = r10;
        this.A07 = r11;
        this.A08 = str;
        this.A00 = r5;
        this.A01 = r7;
        this.A02 = r8;
        this.A05 = r9;
        if (r6 != null) {
            AnonymousClass4PX r1 = r6.A00;
            this.A04 = new AnonymousClass4PX(r1);
            this.A03 = r1;
            this.A0B = r6.A05;
            this.A0C = C12970iu.A12();
            Map map = r6.A04;
            if (map != null) {
                Iterator A0n = C12960it.A0n(map);
                while (A0n.hasNext()) {
                    Map.Entry A15 = C12970iu.A15(A0n);
                    Object key = A15.getKey();
                    if (!C87834De.A00(A15.getValue(), r7.A03.get(key))) {
                        this.A0C.add(key);
                    }
                }
                return;
            }
            return;
        }
        this.A04 = new AnonymousClass4PX(null);
        this.A03 = new AnonymousClass4PX(null);
        this.A0B = Collections.emptyMap();
    }

    public static AnonymousClass28D A00(AnonymousClass28D r2, AnonymousClass28D r3, Object obj, int i) {
        if (r2 != r3 || !C87834De.A00(r3.A02.get(i), obj)) {
            if (r2 == r3) {
                r2 = new AnonymousClass28D(r3, r3, r3.A06, r3.A00);
            }
            r2.A02.put(i, obj);
        }
        return r2;
    }
}
