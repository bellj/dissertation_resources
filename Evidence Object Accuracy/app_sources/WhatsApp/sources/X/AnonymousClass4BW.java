package X;

/* renamed from: X.4BW  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BW {
    A04(0),
    A02(1),
    A01(2),
    A05(3),
    A03(4);
    
    public final int value;

    AnonymousClass4BW(int i) {
        this.value = i;
    }

    public static AnonymousClass4BW A00(int i) {
        if (i == 0) {
            return A04;
        }
        if (i == 1) {
            return A02;
        }
        if (i == 2) {
            return A01;
        }
        if (i == 3) {
            return A05;
        }
        if (i != 4) {
            return null;
        }
        return A03;
    }
}
