package X;

/* renamed from: X.4CV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CV extends IndexOutOfBoundsException {
    public static final long serialVersionUID = 160715609518896765L;
    public final String className;
    public final int constantPoolCount;

    public AnonymousClass4CV(String str, int i) {
        super(C12960it.A0d(str, C12960it.A0k("Class too large: ")));
        this.className = str;
        this.constantPoolCount = i;
    }
}
