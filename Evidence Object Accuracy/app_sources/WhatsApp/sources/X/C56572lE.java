package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.2lE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56572lE extends AbstractC15040mS {
    public final C15030mR A00;

    public C56572lE(C14160kx r2) {
        super(r2);
        this.A00 = new C15030mR(r2);
    }

    public final void A0H(AbstractC115635Si r4) {
        A0G();
        C14170ky r2 = ((C15050mT) this).A00.A03;
        C13020j0.A01(r2);
        r2.A03.submit(new RunnableBRunnable0Shape10S0200000_I1(r4, 16, this));
    }
}
