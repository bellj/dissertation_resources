package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

/* renamed from: X.5hD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121075hD extends AbstractC129495xo {
    public C121075hD() {
        super(0);
    }

    public static void A00(AnonymousClass6BF r4, Object obj, Map map) {
        C121075hD r3 = new C121075hD();
        r3.A01((AbstractC16830pp) ((AnonymousClass01N) map.get(obj)).get(), new HashSet(Collections.singleton("USDP")));
        r4.A01.add(r3);
    }
}
