package X;

/* renamed from: X.57X  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57X implements AnonymousClass2SN {
    @Override // X.AnonymousClass2SN
    public String A64(String str, Object obj) {
        return C12960it.A0Y(obj);
    }

    public boolean equals(Object obj) {
        return obj != null && getClass() == obj.getClass();
    }

    public int hashCode() {
        return getClass().hashCode();
    }
}
