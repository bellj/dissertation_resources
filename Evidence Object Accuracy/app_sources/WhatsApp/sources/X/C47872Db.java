package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2Db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47872Db implements AnonymousClass1JI {
    public final C14900mE A00;
    public final C18850tA A01;
    public final C15550nR A02;
    public final AnonymousClass10S A03;
    public final AnonymousClass10W A04;
    public final AbstractC14440lR A05;

    public /* synthetic */ C47872Db(C14900mE r1, C18850tA r2, C15550nR r3, AnonymousClass10S r4, AnonymousClass10W r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
    }

    @Override // X.AnonymousClass1JI
    public void AOv(UserJid userJid) {
        StringBuilder sb = new StringBuilder("getstatus/delete jid=");
        sb.append(userJid);
        Log.i(sb.toString());
        C15370n3 A0A = this.A02.A0A(userJid);
        if (A0A != null) {
            A0A.A0R = null;
            A0A.A0B = 0;
            this.A05.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(this, 1, A0A));
        }
    }

    @Override // X.AnonymousClass1JI
    public void APn(UserJid userJid, int i) {
        StringBuilder sb = new StringBuilder("getstatus/failed jid=");
        sb.append(userJid);
        sb.append(" code=");
        sb.append(i);
        Log.w(sb.toString());
    }

    @Override // X.AnonymousClass1JI
    public void AT4(UserJid userJid) {
        StringBuilder sb = new StringBuilder("getstatus/nochange jid=");
        sb.append(userJid);
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass1JI
    public void AWV(UserJid userJid, String str, long j) {
        C15370n3 A0A = this.A02.A0A(userJid);
        if (A0A != null) {
            A0A.A0R = str;
            A0A.A0B = j;
            StringBuilder sb = new StringBuilder("getstatus/received  jid=");
            sb.append(userJid);
            sb.append(" status=");
            sb.append(A0A.A0R);
            sb.append(" timestamp=");
            sb.append(A0A.A0B);
            Log.i(sb.toString());
            this.A05.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(this, 1, A0A));
        }
    }
}
