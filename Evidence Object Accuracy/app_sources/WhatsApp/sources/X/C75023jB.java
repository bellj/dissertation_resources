package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.3jB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75023jB extends AbstractC05270Ox {
    public final /* synthetic */ GifSearchContainer A00;

    public C75023jB(GifSearchContainer gifSearchContainer) {
        this.A00 = gifSearchContainer;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i2 != 0) {
            this.A00.A07.A03();
        }
    }
}
