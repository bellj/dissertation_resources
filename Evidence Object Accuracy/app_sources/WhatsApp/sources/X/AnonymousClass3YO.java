package X;

import android.os.SystemClock;
import com.whatsapp.mediaview.MediaViewFragment;

/* renamed from: X.3YO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YO implements AnonymousClass5XC {
    public final /* synthetic */ MediaViewFragment A00;

    public /* synthetic */ AnonymousClass3YO(MediaViewFragment mediaViewFragment) {
        this.A00 = mediaViewFragment;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:56:0x032d */
    /* JADX WARN: Type inference failed for: r3v1, types: [android.view.View, android.view.ViewGroup] */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v4, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r3v5, types: [android.view.View] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass5XC
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass01T A8c(int r44) {
        /*
        // Method dump skipped, instructions count: 1299
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3YO.A8c(int):X.01T");
    }

    @Override // X.AnonymousClass5XC
    public void A8t(int i) {
        AnonymousClass21T r0;
        MediaViewFragment mediaViewFragment = this.A00;
        AbstractC16130oV A1Q = mediaViewFragment.A1Q(i);
        if (A1Q != null && C30041Vv.A0F(A1Q.A0y)) {
            AnonymousClass21T r02 = (AnonymousClass21T) mediaViewFragment.A1n.remove(A1Q.A0z);
            if (r02 != null) {
                r02.A08();
            }
        } else if (MediaViewFragment.A1p && A1Q != null && (r0 = (AnonymousClass21T) mediaViewFragment.A1o.remove(A1Q.A0z)) != null) {
            r0.A08();
            r0.A06();
        }
    }

    @Override // X.AnonymousClass5XC
    public /* bridge */ /* synthetic */ int AFp(Object obj) {
        AnonymousClass1IS r2 = (AnonymousClass1IS) obj;
        AbstractC35521iA r0 = this.A00.A18;
        if (r0 == null) {
            return -2;
        }
        return r0.AFq(r2);
    }

    @Override // X.AnonymousClass5XC
    public void AQb() {
        MediaViewFragment mediaViewFragment = this.A00;
        mediaViewFragment.A1g = true;
        MediaViewFragment.A04(mediaViewFragment);
        if (!mediaViewFragment.A1W && !mediaViewFragment.A1h) {
            long j = mediaViewFragment.A05;
            if (j != 0) {
                mediaViewFragment.A0x.A04(4, SystemClock.uptimeMillis() - j);
                mediaViewFragment.A1h = true;
            }
        }
    }

    @Override // X.AnonymousClass5XC
    public int getCount() {
        AbstractC35521iA r0 = this.A00.A18;
        if (r0 == null) {
            return 0;
        }
        return r0.getCount();
    }
}
