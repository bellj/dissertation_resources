package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Kf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135896Kf implements Callable {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ C130185yw A01;
    public final /* synthetic */ AnonymousClass66I A02;

    public CallableC135896Kf(CaptureRequest.Builder builder, C130185yw r2, AnonymousClass66I r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = builder;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C130185yw r3 = this.A01;
        r3.A0A.A00("Cannot schedule reset focus task, not prepared");
        if (r3.A03.A00.isConnected() && !r3.A0E && r3.A0D) {
            r3.A0C = false;
            r3.A02();
            r3.A09(EnumC124565pk.CANCELLED, null);
            AnonymousClass66I r1 = this.A02;
            if (r1 != null) {
                r1.A06 = null;
                r1.A04 = null;
            }
            try {
                r3.A05(this.A00, r1);
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
