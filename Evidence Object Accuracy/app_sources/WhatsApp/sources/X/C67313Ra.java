package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.3Ra  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67313Ra implements AbstractC009404s {
    public final /* synthetic */ AnonymousClass2IT A00;
    public final /* synthetic */ C15370n3 A01;

    public C67313Ra(AnonymousClass2IT r1, C15370n3 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AbstractC14640lm r1;
        AnonymousClass2IT r0 = this.A00;
        C15370n3 r4 = this.A01;
        AnonymousClass01J r3 = r0.A00.A03;
        C36801kb r32 = new C36801kb(C12990iw.A0Z(r3), (C20830wO) r3.A4W.get(), r4, (AnonymousClass1E5) r3.AKs.get());
        C20830wO r2 = r32.A03;
        Jid jid = r32.A00.A0D;
        if (jid instanceof AbstractC14640lm) {
            r1 = (AbstractC14640lm) jid;
        } else {
            r1 = null;
        }
        C15370n3 A01 = r2.A01(r1);
        r32.A00 = A01;
        r32.A05.A0A(Boolean.valueOf(r32.A04.A00(A01)));
        return r32;
    }
}
