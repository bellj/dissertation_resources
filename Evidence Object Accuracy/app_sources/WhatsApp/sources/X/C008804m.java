package X;

import java.util.Map;

/* renamed from: X.04m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008804m extends AbstractC008904n {
    public final /* synthetic */ AnonymousClass01b A00;

    public C008804m(AnonymousClass01b r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC008904n
    public int A01() {
        return this.A00.A00;
    }

    @Override // X.AbstractC008904n
    public int A02(Object obj) {
        AnonymousClass01b r1 = this.A00;
        if (obj == null) {
            return r1.A01();
        }
        return r1.A02(obj, obj.hashCode());
    }

    @Override // X.AbstractC008904n
    public Object A03(int i, int i2) {
        return this.A00.A03[i];
    }

    @Override // X.AbstractC008904n
    public Object A04(int i, Object obj) {
        throw new UnsupportedOperationException("not a map");
    }

    @Override // X.AbstractC008904n
    public Map A05() {
        throw new UnsupportedOperationException("not a map");
    }

    @Override // X.AbstractC008904n
    public void A06() {
        this.A00.clear();
    }

    @Override // X.AbstractC008904n
    public void A07(int i) {
        this.A00.A03(i);
    }
}
