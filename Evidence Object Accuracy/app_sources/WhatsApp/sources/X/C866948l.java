package X;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/* renamed from: X.48l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C866948l extends ByteArrayOutputStream {
    public byte[] A01() {
        return ((ByteArrayOutputStream) this).buf;
    }

    public void A00() {
        Arrays.fill(((ByteArrayOutputStream) this).buf, (byte) 0);
        reset();
    }
}
