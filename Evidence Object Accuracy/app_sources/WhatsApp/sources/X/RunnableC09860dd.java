package X;

/* renamed from: X.0dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09860dd implements Runnable {
    public boolean A00 = false;
    public final AnonymousClass074 A01;
    public final C009804x A02;

    public RunnableC09860dd(AnonymousClass074 r2, C009804x r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (!this.A00) {
            this.A02.A04(this.A01);
            this.A00 = true;
        }
    }
}
