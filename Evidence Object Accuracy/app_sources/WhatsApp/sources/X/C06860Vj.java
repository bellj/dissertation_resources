package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06860Vj implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(30);
    public final float A00;
    public final float A01;
    public final float A02;
    public final AnonymousClass03T A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06860Vj(AnonymousClass03T r1, float f, float f2, float f3) {
        this.A03 = r1;
        this.A02 = f;
        this.A01 = f2;
        this.A00 = f3;
    }

    public C06860Vj(Parcel parcel) {
        this.A03 = (AnonymousClass03T) parcel.readParcelable(AnonymousClass03T.class.getClassLoader());
        this.A02 = parcel.readFloat();
        this.A01 = parcel.readFloat();
        this.A00 = parcel.readFloat();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C06860Vj)) {
            return false;
        }
        C06860Vj r4 = (C06860Vj) obj;
        if (this.A00 == r4.A00) {
            AnonymousClass03T r1 = this.A03;
            AnonymousClass03T r0 = r4.A03;
            if (r1 == null) {
                if (r0 == null) {
                    return true;
                }
            } else if (r1.equals(r0) && this.A01 == r4.A01 && this.A02 == r4.A02) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        AnonymousClass03T r0 = this.A03;
        float f = 17.0f;
        if (r0 != null) {
            f = 527.0f + ((float) r0.hashCode());
        }
        return (int) ((((((f * 31.0f) + this.A02) * 31.0f) + this.A01) * 31.0f) + this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("CameraPosition");
        sb.append("{target=");
        sb.append(this.A03);
        sb.append(", zoom=");
        sb.append(this.A02);
        sb.append(", tilt=");
        sb.append(this.A01);
        sb.append(", bearing=");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A03, i);
        parcel.writeFloat(this.A02);
        parcel.writeFloat(this.A01);
        parcel.writeFloat(this.A00);
    }
}
