package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.3fM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72903fM extends BroadcastReceiver {
    public final /* synthetic */ C29631Ua A00;

    public C72903fM(C29631Ua r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            this.A00.A0N();
            Log.i("Screen is being turned off");
        }
    }
}
