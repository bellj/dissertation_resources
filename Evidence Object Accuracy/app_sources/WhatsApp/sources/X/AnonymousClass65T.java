package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.fragment.NoviWithdrawCashReviewSheet;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.65T  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65T implements AbstractC11780gr {
    public final /* synthetic */ AnonymousClass01F A00;
    public final /* synthetic */ C123475nD A01;

    public AnonymousClass65T(AnonymousClass01F r1, C123475nD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC11780gr
    public void onBackStackChanged() {
        AnonymousClass01F r5 = this.A00;
        Iterator it = r5.A0U.A02().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AnonymousClass01E r4 = (AnonymousClass01E) it.next();
            if (r4 instanceof NoviWithdrawCashReviewSheet) {
                NoviWithdrawCashReviewSheet noviWithdrawCashReviewSheet = (NoviWithdrawCashReviewSheet) r4;
                C1315663g A06 = this.A01.A06();
                C134136Dl r2 = new C134136Dl(C117305Zk.A0A(noviWithdrawCashReviewSheet, 124), noviWithdrawCashReviewSheet.A05.A04);
                View view = noviWithdrawCashReviewSheet.A0A;
                if (view != null) {
                    r2.AYL(AnonymousClass028.A0D(view, R.id.novi_withdraw_review_method_inflated));
                    r2.A6Q(new AnonymousClass4OZ(2, A06));
                }
            }
        }
        ArrayList arrayList = r5.A0F;
        if (arrayList != null) {
            arrayList.remove(this);
        }
    }
}
