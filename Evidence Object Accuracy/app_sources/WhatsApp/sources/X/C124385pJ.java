package X;

import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.5pJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124385pJ extends AbstractC120015fT {
    public C124385pJ(C18790t3 r1, C14820m6 r2, C14850m9 r3, AnonymousClass18L r4, AnonymousClass01H r5, String str, String str2, String str3, Map map, AnonymousClass01N r10, AnonymousClass01N r11, long j) {
        super(r1, r2, r3, r4, r5, str, str2, str3, map, r10, r11, j);
    }

    @Override // X.AbstractC120015fT
    public void A04(JSONObject jSONObject) {
        super.A04(jSONObject);
        jSONObject.put("tos_version", this.A03.A00.getInt("shops_privacy_notice", -1));
    }
}
