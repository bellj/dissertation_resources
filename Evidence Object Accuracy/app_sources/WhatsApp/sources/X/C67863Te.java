package X;

import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: X.3Te  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67863Te implements AnonymousClass5T3 {
    public AbstractC14200l1 A00;
    public final String A01;

    public /* synthetic */ C67863Te(String str) {
        this.A01 = str;
    }

    @Override // X.AnonymousClass5T3
    public boolean Afk(AnonymousClass28D r5) {
        LinkedList linkedList = r5.A05;
        if (linkedList == null) {
            return false;
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            AnonymousClass4RF r2 = (AnonymousClass4RF) it.next();
            String str = this.A01;
            String str2 = r2.A03;
            if (str2 != null && str2.equals(str)) {
                this.A00 = r2.A02;
                return true;
            }
        }
        return false;
    }
}
