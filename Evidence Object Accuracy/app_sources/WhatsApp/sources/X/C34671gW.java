package X;

/* renamed from: X.1gW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34671gW extends AnonymousClass1JQ implements AbstractC34381g3, AbstractC34391g4 {
    public final C34361g1 A00;
    public final AbstractC14640lm A01;
    public final boolean A02;
    public final boolean A03;

    public C34671gW(AnonymousClass1JR r11, C34361g1 r12, AbstractC14640lm r13, String str, long j, boolean z, boolean z2, boolean z3) {
        super(C27791Jf.A03, r11, str, "regular_high", 6, j, z3);
        this.A01 = r13;
        this.A03 = z;
        this.A02 = z2;
        this.A00 = r12;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34661gV.A02.A0T();
        C34351g0 A04 = this.A00.A04();
        A0T.A03();
        C34661gV r1 = (C34661gV) A0T.A00;
        r1.A01 = A04;
        r1.A00 |= 1;
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r2 = (C27831Jk) A01.A00;
        r2.A05 = (C34661gV) A0T.A02();
        r2.A00 |= 65536;
        return A01;
    }

    @Override // X.AbstractC34381g3
    public AbstractC14640lm ABH() {
        return this.A01;
    }

    @Override // X.AbstractC34391g4
    public C34361g1 AEJ() {
        return this.A00;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("ClearChatMutation{rowId=");
        sb.append(this.A07);
        sb.append(", chatJid=");
        sb.append(this.A01);
        sb.append(", deleteStarredMessages=");
        sb.append(this.A03);
        sb.append(", deleteMediaFiles=");
        sb.append(this.A02);
        sb.append(", messageRange=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
