package X;

/* renamed from: X.5b7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118005b7 extends AnonymousClass015 {
    public int A00;
    public AbstractC001200n A01;
    public AnonymousClass016 A02 = C12980iv.A0T();
    public C14830m7 A03;
    public C16590pI A04;
    public AbstractC28901Pl A05;
    public AnonymousClass1IR A06;
    public AnonymousClass1IR A07;
    public C120565gO A08;
    public C27691It A09 = C13000ix.A03();
    public String A0A;
    public final C14900mE A0B;
    public final C241414j A0C;
    public final C243515e A0D;
    public final C17070qD A0E;
    public final C120555gN A0F;
    public final C120475gF A0G;
    public final C120525gK A0H;
    public final C30931Zj A0I = C117295Zj.A0G("IndiaUpiMandatePaymentViewModel");
    public final AbstractC14440lR A0J;

    public C118005b7(AbstractC001200n r2, C14900mE r3, C14830m7 r4, C16590pI r5, C241414j r6, AnonymousClass1IR r7, C243515e r8, C17070qD r9, C120555gN r10, C120475gF r11, C120565gO r12, C120525gK r13, AbstractC14440lR r14, String str, int i) {
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A0B = r3;
        this.A0J = r14;
        this.A0C = r6;
        this.A0E = r9;
        this.A0D = r8;
        this.A07 = r7;
        this.A0H = r13;
        this.A08 = r12;
        this.A0F = r10;
        this.A0G = r11;
        this.A00 = i;
        this.A0A = str;
    }

    public static void A00(C452120p r2, C118005b7 r3) {
        C128285vr r1 = new C128285vr(2);
        r1.A06 = r2;
        r3.A09.A0B(r1);
    }

    public final void A04() {
        this.A09.A0A(new C128285vr(1));
    }
}
