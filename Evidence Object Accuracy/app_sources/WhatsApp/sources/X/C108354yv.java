package X;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yv */
/* loaded from: classes3.dex */
public final class C108354yv implements AnonymousClass5XO {
    public int A00 = 0;
    public Bundle A01;
    public C56492ky A02 = null;
    public C56492ky A03 = null;
    public boolean A04 = false;
    public final Context A05;
    public final Looper A06;
    public final AbstractC72443eb A07;
    public final C77733nl A08;
    public final C108364yw A09;
    public final C108364yw A0A;
    public final Map A0B;
    public final Set A0C = Collections.newSetFromMap(new WeakHashMap());
    public final Lock A0D;

    public C108354yv(Context context, Looper looper, C471929k r29, AbstractC77683ng r30, AbstractC72443eb r31, C77733nl r32, AnonymousClass3BW r33, ArrayList arrayList, ArrayList arrayList2, Map map, Map map2, Map map3, Map map4, Lock lock) {
        this.A05 = context;
        this.A08 = r32;
        this.A0D = lock;
        this.A06 = looper;
        this.A07 = r31;
        this.A09 = new C108364yw(context, looper, r29, null, r32, new C108334yt(this), null, arrayList2, map2, map4, lock);
        this.A0A = new C108364yw(context, looper, r29, r30, r32, new C108344yu(this), r33, arrayList, map, map3, lock);
        AnonymousClass00N r4 = new AnonymousClass00N();
        Iterator A10 = C72453ed.A10(map2);
        while (A10.hasNext()) {
            r4.put(A10.next(), this.A09);
        }
        Iterator A102 = C72453ed.A10(map);
        while (A102.hasNext()) {
            r4.put(A102.next(), this.A0A);
        }
        this.A0B = Collections.unmodifiableMap(r4);
    }

    public static /* bridge */ /* synthetic */ void A00(C108354yv r4) {
        C56492ky r2 = r4.A02;
        if (r2 != null) {
            int i = r2.A01;
            C56492ky r3 = r4.A03;
            if (i == 0) {
                if (r3 != null) {
                    int i2 = r3.A01;
                    if (C12960it.A1T(i2) || i2 == 4) {
                        int i3 = r4.A00;
                        if (i3 != 1) {
                            if (i3 != 2) {
                                Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                                r4.A00 = 0;
                            }
                            C77733nl r1 = r4.A08;
                            C13020j0.A01(r1);
                            r1.AgP(r4.A01);
                        }
                        r4.A01();
                        r4.A00 = 0;
                    } else if (r4.A00 == 1) {
                        r4.A01();
                    } else {
                        r4.A02(r3);
                        r4.A09.Age();
                    }
                }
            } else if (r3 != null) {
                int i4 = r3.A01;
                C108364yw r0 = r4.A0A;
                if (i4 == 0) {
                    r0.Age();
                    r2 = r4.A02;
                    C13020j0.A01(r2);
                } else if (r0.A00 < r4.A09.A00) {
                    r2 = r3;
                }
                r4.A02(r2);
            }
        }
    }

    public final void A01() {
        Set<AnonymousClass5QY> set = this.A0C;
        for (AnonymousClass5QY r0 : set) {
            ((C54022fy) r0).A01.release();
        }
        set.clear();
    }

    public final void A02(C56492ky r4) {
        int i = this.A00;
        if (i != 1) {
            if (i != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.A00 = 0;
            }
            this.A08.AgN(r4);
        }
        A01();
        this.A00 = 0;
    }

    @Override // X.AnonymousClass5XO
    public final AnonymousClass1UI AgV(AnonymousClass1UI r10) {
        PendingIntent activity;
        Object obj = this.A0B.get(r10.A00);
        C13020j0.A02(obj, "GoogleApiClient is not configured to use the API required for this call.");
        C108364yw r2 = this.A0A;
        if (obj.equals(r2)) {
            C56492ky r0 = this.A03;
            if (r0 != null && r0.A01 == 4) {
                AbstractC72443eb r1 = this.A07;
                if (r1 == null) {
                    activity = null;
                } else {
                    activity = PendingIntent.getActivity(this.A05, System.identityHashCode(this.A08), r1.AGl(), C88344Fh.A00 | 134217728);
                }
                r10.A09(new Status(activity, null, null, 1, 4));
                return r10;
            }
        } else {
            r2 = this.A09;
        }
        r2.AgV(r10);
        return r10;
    }

    @Override // X.AnonymousClass5XO
    public final AnonymousClass1UI AgY(AnonymousClass1UI r10) {
        PendingIntent activity;
        Object obj = this.A0B.get(r10.A00);
        C13020j0.A02(obj, "GoogleApiClient is not configured to use the API required for this call.");
        C108364yw r2 = this.A0A;
        if (obj.equals(r2)) {
            C56492ky r0 = this.A03;
            if (r0 != null && r0.A01 == 4) {
                AbstractC72443eb r1 = this.A07;
                if (r1 == null) {
                    activity = null;
                } else {
                    activity = PendingIntent.getActivity(this.A05, System.identityHashCode(this.A08), r1.AGl(), C88344Fh.A00 | 134217728);
                }
                r10.A09(new Status(activity, null, null, 1, 4));
                return r10;
            }
        } else {
            r2 = this.A09;
        }
        return r2.AgY(r10);
    }

    @Override // X.AnonymousClass5XO
    public final void Agd() {
        this.A00 = 2;
        this.A04 = false;
        this.A03 = null;
        this.A02 = null;
        this.A09.Agd();
        this.A0A.Agd();
    }

    @Override // X.AnonymousClass5XO
    public final void Age() {
        this.A03 = null;
        this.A02 = null;
        this.A00 = 0;
        this.A09.Age();
        this.A0A.Age();
        A01();
    }

    @Override // X.AnonymousClass5XO
    public final void Agf(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append("").append("authClient").println(":");
        this.A0A.Agf(String.valueOf("").concat("  "), null, printWriter, null);
        printWriter.append("").append("anonClient").println(":");
        this.A09.Agf(String.valueOf("").concat("  "), null, printWriter, null);
    }

    @Override // X.AnonymousClass5XO
    public final void Agg() {
        Lock lock = this.A0D;
        lock.lock();
        try {
            lock.lock();
            boolean A1V = C12960it.A1V(this.A00, 2);
            lock.unlock();
            this.A0A.Age();
            this.A03 = new C56492ky(4);
            if (A1V) {
                new HandlerC472529t(this.A06).post(new RunnableBRunnable0Shape14S0100000_I1(this, 8));
            } else {
                A01();
            }
        } finally {
            lock.unlock();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r5.A00 == 1) goto L_0x0025;
     */
    @Override // X.AnonymousClass5XO
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean Agh() {
        /*
            r5 = this;
            java.util.concurrent.locks.Lock r4 = r5.A0D
            r4.lock()
            X.4yw r0 = r5.A09     // Catch: all -> 0x002a
            X.5XN r0 = r0.A0E     // Catch: all -> 0x002a
            boolean r0 = r0 instanceof X.C108304yq     // Catch: all -> 0x002a
            r3 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0026
            X.4yw r0 = r5.A0A     // Catch: all -> 0x002a
            X.5XN r0 = r0.A0E     // Catch: all -> 0x002a
            boolean r0 = r0 instanceof X.C108304yq     // Catch: all -> 0x002a
            if (r0 != 0) goto L_0x0025
            X.2ky r0 = r5.A03     // Catch: all -> 0x002a
            if (r0 == 0) goto L_0x0021
            int r1 = r0.A01     // Catch: all -> 0x002a
            r0 = 4
            if (r1 != r0) goto L_0x0021
            goto L_0x0025
        L_0x0021:
            int r0 = r5.A00     // Catch: all -> 0x002a
            if (r0 != r2) goto L_0x0026
        L_0x0025:
            r3 = 1
        L_0x0026:
            r4.unlock()
            return r3
        L_0x002a:
            r0 = move-exception
            r4.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108354yv.Agh():boolean");
    }

    @Override // X.AnonymousClass5XO
    public final boolean Agi(AnonymousClass5QY r5) {
        Lock lock = this.A0D;
        lock.lock();
        try {
            lock.lock();
            boolean A1V = C12960it.A1V(this.A00, 2);
            lock.unlock();
            if (A1V || Agh()) {
                C108364yw r2 = this.A0A;
                if (!(r2.A0E instanceof C108304yq)) {
                    this.A0C.add(r5);
                    if (this.A00 == 0) {
                        this.A00 = 1;
                    }
                    this.A03 = null;
                    r2.Agd();
                    return true;
                }
            }
            lock.unlock();
            return false;
        } finally {
            lock.unlock();
        }
    }
}
