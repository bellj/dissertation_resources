package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VP implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass03T(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass03T[i];
    }
}
