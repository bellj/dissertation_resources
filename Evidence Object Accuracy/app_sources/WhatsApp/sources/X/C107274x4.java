package X;

/* renamed from: X.4x4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107274x4 implements AbstractC116595Wb {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public AnonymousClass4YB A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public final AnonymousClass5X7 A09;
    public final C95054d0 A0A = new C95054d0(new byte[10], 10);

    public C107274x4(AnonymousClass5X7 r4) {
        this.A09 = r4;
    }

    public final boolean A00(C95304dT r4, byte[] bArr, int i) {
        int A00 = C95304dT.A00(r4);
        int i2 = this.A00;
        int A02 = C72463ee.A02(i, i2, A00);
        if (A02 <= 0) {
            return true;
        }
        if (bArr == null) {
            r4.A0T(A02);
        } else {
            r4.A0V(bArr, i2, A02);
        }
        int i3 = this.A00 + A02;
        this.A00 = i3;
        if (i3 != i) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0085, code lost:
        r7.A07(0);
        r0 = -9223372036854775807L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x008f, code lost:
        if (r12.A07 == false) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0091, code lost:
        r7.A08(1);
        r8 = X.C95054d0.A02(r7, ((long) X.C95054d0.A01(r7, 4, 3)) << 30);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a3, code lost:
        if (r12.A08 != false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a7, code lost:
        if (r12.A06 == false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a9, code lost:
        r7.A08(1);
        r12.A04.A03(X.C95054d0.A02(r7, ((long) X.C95054d0.A01(r7, 4, 3)) << 30));
        r12.A08 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00bd, code lost:
        r0 = r12.A04.A03(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c5, code lost:
        if (r12.A05 == false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c7, code lost:
        r2 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c8, code lost:
        r14 = r14 | r2;
        r12.A09.AYp(r0, r14);
        r12.A03 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00de, code lost:
        r8.A07(0);
        r1 = r8.A04(24);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e9, code lost:
        if (r1 == 1) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00eb, code lost:
        android.util.Log.w("PesReader", X.C12960it.A0W(r1, "Unexpected start code prefix: "));
        r12.A02 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        r12.A03 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00fa, code lost:
        r8.A08(8);
        r2 = r8.A04(16);
        r8.A08(5);
        r12.A05 = r8.A0C();
        r8.A08(2);
        r12.A07 = r8.A0C();
        r12.A06 = r8.A0C();
        r0 = X.C95054d0.A01(r8, 6, 8);
        r12.A01 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0125, code lost:
        if (r2 == 0) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0127, code lost:
        r1 = ((r2 + 6) - 9) - r0;
        r12.A02 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x012d, code lost:
        if (r1 >= 0) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x012f, code lost:
        android.util.Log.w("PesReader", X.C12960it.A0W(r1, "Found negative packet payload size: "));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0138, code lost:
        r12.A02 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x013a, code lost:
        r2 = 2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x014d A[SYNTHETIC] */
    @Override // X.AbstractC116595Wb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A7b(X.C95304dT r13, int r14) {
        /*
        // Method dump skipped, instructions count: 339
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107274x4.A7b(X.4dT, int):void");
    }

    @Override // X.AbstractC116595Wb
    public void AIe(AbstractC14070ko r2, C92824Xo r3, AnonymousClass4YB r4) {
        this.A04 = r4;
        this.A09.A8b(r2, r3);
    }

    @Override // X.AbstractC116595Wb
    public final void AbP() {
        this.A03 = 0;
        this.A00 = 0;
        this.A08 = false;
        this.A09.AbP();
    }
}
