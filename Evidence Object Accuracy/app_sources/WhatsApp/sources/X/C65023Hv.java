package X;

import android.content.Context;
import com.whatsapp.R;
import java.io.File;

/* renamed from: X.3Hv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65023Hv {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r3 == 8) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable A00(android.content.Context r4, X.C30421Xi r5) {
        /*
            X.1IS r0 = r5.A0z
            boolean r0 = r0.A02
            if (r0 == 0) goto L_0x001a
            int r3 = r5.A0C
            r2 = 2131232446(0x7f0806be, float:1.8081001E38)
            r1 = 8
            r0 = 2131100454(0x7f060326, float:1.781329E38)
            if (r3 != r1) goto L_0x0015
        L_0x0012:
            r0 = 2131100453(0x7f060325, float:1.7813288E38)
        L_0x0015:
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A01(r4, r2, r0)
            return r0
        L_0x001a:
            int r1 = r5.A0C
            r0 = 9
            if (r1 == r0) goto L_0x002b
            r0 = 10
            if (r1 == r0) goto L_0x002b
            r2 = 2131232446(0x7f0806be, float:1.8081001E38)
            r0 = 2131100455(0x7f060327, float:1.7813292E38)
            goto L_0x0015
        L_0x002b:
            r2 = 2131232446(0x7f0806be, float:1.8081001E38)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65023Hv.A00(android.content.Context, X.1Xi):android.graphics.drawable.Drawable");
    }

    public static String A01(Context context, C15550nR r11, C15610nY r12, C14830m7 r13, AnonymousClass018 r14, C30421Xi r15) {
        int i;
        Object[] objArr;
        int i2;
        String A06 = C38131nZ.A06(r14, (long) Math.max(0, ((AbstractC16130oV) r15).A00 * 1000));
        String A00 = AnonymousClass3JK.A00(r14, r13.A02(r15.A0I));
        boolean A1V = C12960it.A1V(((AbstractC15340mz) r15).A08, 1);
        if (r15.A0z.A02) {
            int i3 = ((AbstractC15340mz) r15).A0C;
            if (i3 == 1) {
                i = R.string.audio_message_description_sent_pending;
                if (A1V) {
                    i = R.string.voice_message_description_sent_pending;
                }
            } else if (i3 == 5) {
                i = R.string.audio_message_description_sent_delivered;
                if (A1V) {
                    i = R.string.voice_message_description_sent_delivered;
                }
            } else if (i3 == 13) {
                i = R.string.audio_message_description_sent_read;
                if (A1V) {
                    i = R.string.voice_message_description_sent_read;
                }
            } else if (i3 == 8) {
                i = R.string.voice_message_description_sent_played;
            } else {
                i = R.string.audio_message_description_sent;
                if (A1V) {
                    i = R.string.voice_message_description_sent;
                }
            }
            objArr = new Object[]{A06, A00};
        } else {
            String A08 = r12.A08(C15550nR.A00(r11, r15.A0C()));
            int i4 = ((AbstractC15340mz) r15).A0C;
            if (i4 == 9 || i4 == 10) {
                i = R.string.audio_message_description_played;
                if (A1V) {
                    i = R.string.voice_message_description_played;
                }
                objArr = new Object[3];
            } else {
                File file = ((AbstractC16130oV) r15).A02.A0F;
                if (A1V) {
                    if (file == null) {
                        i2 = R.string.voice_message_download_description;
                        Object[] objArr2 = new Object[4];
                        objArr2[0] = A08;
                        objArr2[1] = A06;
                        objArr2[2] = A00;
                        return C12960it.A0X(context, C30041Vv.A0B(r14, ((AbstractC16130oV) r15).A01), objArr2, 3, i2);
                    }
                    i = R.string.voice_message_description;
                    objArr = new Object[3];
                } else if (file == null) {
                    i2 = R.string.audio_message_download_description;
                    Object[] objArr2 = new Object[4];
                    objArr2[0] = A08;
                    objArr2[1] = A06;
                    objArr2[2] = A00;
                    return C12960it.A0X(context, C30041Vv.A0B(r14, ((AbstractC16130oV) r15).A01), objArr2, 3, i2);
                } else {
                    i = R.string.audio_message_description;
                    objArr = new Object[3];
                }
            }
            objArr[0] = A08;
            objArr[1] = A06;
            objArr[2] = A00;
        }
        return context.getString(i, objArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r2 == 10) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.C30421Xi r6, com.whatsapp.search.views.itemviews.AudioPlayerView r7, com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView r8) {
        /*
            android.content.Context r5 = r8.getContext()
            X.1IS r4 = r6.A0z
            boolean r3 = r4.A02
            int r2 = r6.A0C
            if (r3 == 0) goto L_0x003f
            r0 = 8
            if (r2 == r0) goto L_0x004a
            r1 = 2131101056(0x7f060580, float:1.781451E38)
            r0 = 2131099740(0x7f06005c, float:1.7811842E38)
        L_0x0016:
            r8.setMicColorTint(r1)
            int r0 = X.AnonymousClass00T.A00(r5, r0)
            r7.setSeekbarColor(r0)
            X.0oX r1 = X.AbstractC15340mz.A00(r6)
            boolean r0 = r1.A0a
            if (r0 != 0) goto L_0x003e
            boolean r0 = r1.A0P
            if (r0 != 0) goto L_0x003e
            boolean r0 = r6.A0r
            if (r0 == 0) goto L_0x003a
            if (r3 == 0) goto L_0x003a
            X.0lm r0 = r4.A00
            boolean r0 = X.C15380n4.A0F(r0)
            if (r0 == 0) goto L_0x003e
        L_0x003a:
            r0 = 0
            r7.setSeekbarColor(r0)
        L_0x003e:
            return
        L_0x003f:
            r0 = 9
            if (r2 == r0) goto L_0x004a
            r0 = 10
            r1 = 2131101054(0x7f06057e, float:1.7814507E38)
            if (r2 != r0) goto L_0x004d
        L_0x004a:
            r1 = 2131101055(0x7f06057f, float:1.7814509E38)
        L_0x004d:
            r0 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65023Hv.A02(X.1Xi, com.whatsapp.search.views.itemviews.AudioPlayerView, com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView):void");
    }
}
