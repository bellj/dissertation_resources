package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

/* renamed from: X.29s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC472429s extends HandlerC472529t {
    public final Context A00;
    public final /* synthetic */ C471729i A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HandlerC472429s(android.content.Context r2, X.C471729i r3) {
        /*
            r1 = this;
            r1.A01 = r3
            android.os.Looper r0 = android.os.Looper.myLooper()
            if (r0 != 0) goto L_0x0016
            android.os.Looper r0 = android.os.Looper.getMainLooper()
        L_0x000c:
            r1.<init>(r0)
            android.content.Context r0 = r2.getApplicationContext()
            r1.A00 = r0
            return
        L_0x0016:
            android.os.Looper r0 = android.os.Looper.myLooper()
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC472429s.<init>(android.content.Context, X.29i):void");
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        PendingIntent activity;
        int i = message.what;
        if (i != 1) {
            StringBuilder sb = new StringBuilder(50);
            sb.append("Don't know how to handle this message: ");
            sb.append(i);
            Log.w("GoogleApiAvailability", sb.toString());
            return;
        }
        C471729i r6 = this.A01;
        Context context = this.A00;
        int A00 = r6.A00(context, 12451000);
        if (A00 == 1 || A00 == 2 || A00 == 3 || A00 == 9) {
            Intent A01 = r6.A01(context, "n", A00);
            if (A01 == null) {
                activity = null;
            } else {
                activity = PendingIntent.getActivity(context, 0, A01, C88384Fl.A00 | 134217728);
            }
            r6.A03(activity, context, A00);
        }
    }
}
