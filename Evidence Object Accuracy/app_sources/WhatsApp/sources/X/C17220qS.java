package X;

import android.os.Message;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0qS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17220qS {
    public AnonymousClass1VM A00;
    public AnonymousClass1VL A01;
    public final C16240og A02;
    public final C14850m9 A03;
    public final C22910zq A04;
    public final C230610f A05;
    public final C230510e A06;
    public final C230710g A07;
    public final C235111y A08;
    public final C17230qT A09;
    public final Set A0A = new HashSet();
    public final AtomicInteger A0B = new AtomicInteger();

    public C17220qS(C16240og r2, C14850m9 r3, C22910zq r4, C230610f r5, C230510e r6, C230710g r7, C235111y r8, C17230qT r9) {
        this.A03 = r3;
        this.A06 = r6;
        this.A04 = r4;
        this.A02 = r2;
        this.A09 = r9;
        this.A08 = r8;
        this.A05 = r5;
        this.A07 = r7;
    }

    public static Message A00(AnonymousClass1V8 r3, String str, int i, boolean z, boolean z2) {
        Message obtain = Message.obtain(null, 0, 233, i, r3);
        obtain.getData().putString("messageClient:iqId", str);
        obtain.getData().putBoolean("messageClient:dropIfOffline", z);
        obtain.getData().putBoolean("messageClient:checkCallback", z2);
        return obtain;
    }

    public String A01() {
        String obj;
        C230710g r6 = this.A07;
        synchronized (r6.A0B) {
            StringBuilder sb = new StringBuilder();
            sb.append("0");
            int i = r6.A00;
            r6.A00 = i + 1;
            sb.append(Integer.toHexString(i));
            obj = sb.toString();
            boolean z = false;
            if (r6.A0E.put(obj, r6.A0A) == null) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            if (r6.A00 == 65536) {
                r6.A05.AaV("iqId too large", null, false);
                r6.A00 = 0;
            }
        }
        A0C(new AbstractC14590lg(obj) { // from class: X.1VG
            public final /* synthetic */ String A00;

            {
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj2) {
                String str = this.A00;
                C91774Tb r2 = new C91774Tb();
                r2.A02 = System.currentTimeMillis();
                ((AnonymousClass1EV) obj2).A03.put(str, r2);
            }
        });
        return obj;
    }

    public String A02() {
        StringBuilder sb = new StringBuilder("n");
        sb.append(Integer.toHexString(this.A0B.getAndIncrement()));
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r2 == null) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.concurrent.Future A03(android.os.Message r7, X.AnonymousClass1OT r8) {
        /*
            r6 = this;
            java.lang.String r0 = "MessageClient/sendAckableMessage: stanzaKey is null"
            X.AnonymousClass009.A06(r8, r0)
            com.whatsapp.jid.Jid r1 = r8.A01
            boolean r0 = r1 instanceof com.whatsapp.jid.DeviceJid
            if (r0 == 0) goto L_0x0047
            int r0 = r1.getDevice()
            if (r0 != 0) goto L_0x0047
            X.1VH r2 = r8.A01()
            com.whatsapp.jid.DeviceJid r1 = (com.whatsapp.jid.DeviceJid) r1
            com.whatsapp.jid.UserJid r0 = r1.getUserJid()
            r2.A01 = r0
        L_0x001d:
            com.whatsapp.jid.Jid r0 = r8.A02
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r0)
            if (r1 == 0) goto L_0x0044
            byte r0 = r1.device
            if (r0 != 0) goto L_0x0044
            if (r2 != 0) goto L_0x002f
            X.1VH r2 = r8.A01()
        L_0x002f:
            com.whatsapp.jid.UserJid r0 = r1.getUserJid()
            r2.A02 = r0
        L_0x0035:
            X.1OT r8 = r2.A00()
        L_0x0039:
            X.1VC r5 = new X.1VC
            r5.<init>()
            X.10f r2 = r6.A05
            java.util.Map r4 = r2.A02
            monitor-enter(r4)
            goto L_0x0049
        L_0x0044:
            if (r2 != 0) goto L_0x0035
            goto L_0x0039
        L_0x0047:
            r2 = 0
            goto L_0x001d
        L_0x0049:
            boolean r0 = r4.containsKey(r8)     // Catch: all -> 0x0076
            if (r0 == 0) goto L_0x006d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0076
            r1.<init>()     // Catch: all -> 0x0076
            java.lang.String r0 = "added duplicate ackable stanza: "
            r1.append(r0)     // Catch: all -> 0x0076
            r1.append(r8)     // Catch: all -> 0x0076
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0076
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x0076
            X.0nm r3 = r2.A01     // Catch: all -> 0x0076
            java.lang.String r2 = "MessageCallbacksManager/addAckCallback"
            java.lang.String r1 = "duplicate_ackable_stanza"
            r0 = 1
            r3.AaV(r2, r1, r0)     // Catch: all -> 0x0076
        L_0x006d:
            r4.put(r8, r5)     // Catch: all -> 0x0076
            monitor-exit(r4)     // Catch: all -> 0x0076
            r0 = 0
            r6.A08(r7, r0)
            return r5
        L_0x0076:
            r0 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x0076
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17220qS.A03(android.os.Message, X.1OT):java.util.concurrent.Future");
    }

    public Future A04(Message message, String str, boolean z) {
        boolean containsKey;
        AnonymousClass009.A06(str, "MessageClient/sendIq: id is null");
        if (z) {
            Map map = this.A05.A03;
            synchronized (map) {
                containsKey = map.containsKey(str);
            }
            if (containsKey) {
                this.A07.A03(str);
                StringBuilder sb = new StringBuilder("MessageClient/sendIq: duplicate id: ");
                sb.append(str);
                throw new AnonymousClass1VI(sb.toString());
            }
        }
        AnonymousClass1VC r2 = new AnonymousClass1VC();
        Map map2 = this.A05.A03;
        synchronized (map2) {
            map2.put(str, r2);
        }
        A0C(new AnonymousClass1VK(str, AnonymousClass1VJ.A00(message)));
        A08(message, false);
        this.A07.A03(str);
        return r2;
    }

    public void A05(long j) {
        AnonymousClass009.A00();
        C16240og r3 = this.A02;
        if (r3.A04 != 2) {
            if (this.A00 != null) {
                Log.i("app/msghandler-not-connected/connecting-now");
                C19890uq r2 = this.A00.A00;
                r2.A06();
                r2.A0n.A02();
                r2.A0J(true, false, false);
            } else {
                Log.i("app/msghandler-not-connected/too-early-to-connect");
            }
            Log.i("app/waiting-for-msghandler-to-be-connected");
            AnonymousClass009.A00();
            if (!r3.A03.block(j)) {
                Log.i("gdrive-service/backup-map/timeout-while-waiting-for-msghandler-to-be-connected/abort");
                throw new AnonymousClass1VO();
            }
        }
        Log.i("app/msghandler-connected/true");
    }

    public void A06(Message message) {
        if (this.A02.A06) {
            A08(message, false);
        }
    }

    public final void A07(Message message, String str, boolean z) {
        int A00 = AnonymousClass1VJ.A00(message);
        if (this.A02.A06) {
            if (z) {
                C230510e r2 = this.A06;
                AnonymousClass009.A05(str);
                StringBuilder sb = new StringBuilder("Ackable message with null id not allowed:");
                sb.append(message);
                AnonymousClass009.A06(str, sb.toString());
                LinkedHashMap linkedHashMap = r2.A00;
                synchronized (linkedHashMap) {
                    int i = 1;
                    if (linkedHashMap.containsKey(str)) {
                        i = 1 + ((Integer) ((Pair) linkedHashMap.get(str)).second).intValue();
                    }
                    linkedHashMap.put(str, Pair.create(message, Integer.valueOf(i)));
                }
            }
            A08(message, false);
            return;
        }
        StringBuilder sb2 = new StringBuilder("MessageClient/sendMessageWhenReady/add-to-pending type: ");
        sb2.append(A00);
        sb2.append(" id: ");
        sb2.append(str);
        Log.i(sb2.toString());
        List list = this.A04.A00;
        synchronized (list) {
            list.add(new AnonymousClass1V7(message, str, z));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0024, code lost:
        if (r5 != false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.os.Message r4, boolean r5) {
        /*
            r3 = this;
            X.1VL r1 = r3.A01
            java.lang.String r0 = "sendXmpp called before sending channel is ready"
            X.AnonymousClass009.A06(r1, r0)
            java.lang.String r0 = "MessageClient/sendXmpp; type="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            int r0 = X.AnonymousClass1VJ.A00(r4)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            int r0 = X.AnonymousClass1VJ.A00(r4)
            switch(r0) {
                case 4: goto L_0x0026;
                case 8: goto L_0x0026;
                case 12: goto L_0x0026;
                case 15: goto L_0x0026;
                case 16: goto L_0x0026;
                case 17: goto L_0x0026;
                case 20: goto L_0x0026;
                case 22: goto L_0x0026;
                case 25: goto L_0x0026;
                case 27: goto L_0x0026;
                case 30: goto L_0x0026;
                case 35: goto L_0x0026;
                case 38: goto L_0x0026;
                case 43: goto L_0x0026;
                case 59: goto L_0x0026;
                case 60: goto L_0x0026;
                case 71: goto L_0x0026;
                case 72: goto L_0x0026;
                case 73: goto L_0x0026;
                case 77: goto L_0x0026;
                case 81: goto L_0x0026;
                case 82: goto L_0x0026;
                case 83: goto L_0x0026;
                case 84: goto L_0x0026;
                case 89: goto L_0x0026;
                case 91: goto L_0x0026;
                case 92: goto L_0x0026;
                case 98: goto L_0x0026;
                case 107: goto L_0x0026;
                case 108: goto L_0x0026;
                case 112: goto L_0x0026;
                case 115: goto L_0x0026;
                case 118: goto L_0x0026;
                case 119: goto L_0x0026;
                case 120: goto L_0x0026;
                case 121: goto L_0x0026;
                case 123: goto L_0x0026;
                case 125: goto L_0x0026;
                case 126: goto L_0x0026;
                case 134: goto L_0x0026;
                case 153: goto L_0x0026;
                case 157: goto L_0x0026;
                case 158: goto L_0x0026;
                case 166: goto L_0x0026;
                case 167: goto L_0x0026;
                case 168: goto L_0x0026;
                case 169: goto L_0x0026;
                case 188: goto L_0x0026;
                case 191: goto L_0x0026;
                case 194: goto L_0x0026;
                case 206: goto L_0x0026;
                case 208: goto L_0x0026;
                case 209: goto L_0x0026;
                case 220: goto L_0x0026;
                case 255: goto L_0x0026;
                case 273: goto L_0x0026;
                case 327: goto L_0x0026;
                default: goto L_0x0024;
            }
        L_0x0024:
            if (r5 == 0) goto L_0x003a
        L_0x0026:
            X.1VM r0 = r3.A00
            X.AnonymousClass009.A05(r0)
            X.0uq r2 = r0.A00
            r2.A06()
            X.1VN r0 = r2.A0n
            r0.A02()
            r1 = 1
            r0 = 0
            r2.A0J(r1, r0, r0)
        L_0x003a:
            X.1VL r2 = r3.A01
            android.os.Message r1 = android.os.Message.obtain(r4)
            android.os.Handler r2 = (android.os.Handler) r2
            r0 = 4
            r1.what = r0
            r2.sendMessage(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17220qS.A08(android.os.Message, boolean):void");
    }

    public void A09(AbstractC21730xt r10, AnonymousClass1V8 r11, String str, int i, long j) {
        C16240og r2 = this.A02;
        if (!r2.A06 || r2.A04 != 2) {
            this.A07.A03(str);
            StringBuilder sb = new StringBuilder("MessageClient/sendIqWithCallback ready:");
            sb.append(r2.A06);
            sb.append(" connected:");
            boolean z = false;
            if (r2.A04 == 2) {
                z = true;
            }
            sb.append(z);
            sb.append(" iqId=");
            sb.append(str);
            Log.i(sb.toString());
            r10.AP1(str);
            return;
        }
        this.A07.A01(r10, str, j, false);
        A0C(new AnonymousClass1VK(str, i));
        boolean z2 = false;
        if (j > 0) {
            z2 = true;
        }
        A08(A00(r11, str, i, true, z2), false);
    }

    public void A0A(AbstractC21730xt r19, AnonymousClass1V8 r20, String str, int i, long j) {
        if (!A0D(r19, r20, str, i, j)) {
            StringBuilder sb = new StringBuilder("MessageClient/sendIqWithCallback/add-to-pending type: ");
            sb.append(i);
            sb.append(" id: ");
            sb.append(str);
            Log.i(sb.toString());
            this.A07.A01(r19, str, j, true);
            A0C(new AnonymousClass1VK(str, i));
            C22910zq r6 = this.A04;
            boolean z = false;
            if (j > 0) {
                z = true;
            }
            Message A00 = A00(r20, str, i, false, z);
            List list = r6.A00;
            synchronized (list) {
                list.add(new AnonymousClass1V7(A00, str, false));
            }
        }
    }

    public void A0B(AnonymousClass1OT r4) {
        this.A09.A03(r4.A00);
        A08(Message.obtain(null, 0, 76, 0, r4), false);
    }

    public final void A0C(AbstractC14590lg r4) {
        if (this.A03.A07(1740)) {
            Set<AnonymousClass1EV> set = this.A0A;
            synchronized (set) {
                for (AnonymousClass1EV r0 : set) {
                    r4.accept(r0);
                }
            }
        }
    }

    public boolean A0D(AbstractC21730xt r10, AnonymousClass1V8 r11, String str, int i, long j) {
        if (this.A02.A06) {
            this.A07.A01(r10, str, j, false);
            A0C(new AnonymousClass1VK(str, i));
            boolean z = false;
            if (j > 0) {
                z = true;
            }
            A08(A00(r11, str, i, false, z), true);
            return true;
        }
        this.A07.A03(str);
        StringBuilder sb = new StringBuilder("MessageClient/sendIqWithCallback not ready, iqId=");
        sb.append(str);
        Log.i(sb.toString());
        return false;
    }
}
