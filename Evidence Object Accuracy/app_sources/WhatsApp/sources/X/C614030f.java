package X;

/* renamed from: X.30f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614030f extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Long A02;
    public String A03;
    public String A04;

    public C614030f() {
        super(3466, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCtwaUserJourney {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "adId", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessJid", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaUserJourneyOperation", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "icebreakersShown", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
