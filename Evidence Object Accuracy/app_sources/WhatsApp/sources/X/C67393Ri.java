package X;

/* renamed from: X.3Ri  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67393Ri implements AbstractC009404s {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass4JD A01;
    public final /* synthetic */ C15580nU A02;
    public final /* synthetic */ C15580nU A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ boolean A05;

    public C67393Ri(AnonymousClass4JD r1, C15580nU r2, C15580nU r3, String str, int i, boolean z) {
        this.A01 = r1;
        this.A00 = i;
        this.A03 = r2;
        this.A02 = r3;
        this.A04 = str;
        this.A05 = z;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass4JD r0 = this.A01;
        int i = this.A00;
        C15580nU r4 = this.A03;
        C15580nU r3 = this.A02;
        String str = this.A04;
        boolean z = this.A05;
        C71573d9 r5 = r0.A00;
        C51112Sw r02 = r5.A03;
        AnonymousClass01J r52 = r5.A04;
        C14830m7 A0b = C12980iv.A0b(r52);
        C20660w7 A0d = C12990iw.A0d(r52);
        C15550nR A0O = C12960it.A0O(r52);
        C15610nY A0P = C12960it.A0P(r52);
        AnonymousClass018 A0R = C12960it.A0R(r52);
        C20710wC A0e = C12980iv.A0e(r52);
        AnonymousClass1CS r9 = (AnonymousClass1CS) r52.AKi.get();
        C15600nX A0d2 = C12980iv.A0d(r52);
        C468027s r7 = new C468027s((C22640zP) r52.A3Z.get(), r9, A0O, A0P, (C18640sm) r52.A3u.get(), A0b, A0R, (C21320xE) r52.A4Y.get(), A0d2, A0e, r4, r3, A0d, str, i, z);
        AnonymousClass01J r1 = r02.A0Y;
        r7.A0A = C12980iv.A0b(r1);
        r7.A03 = C12970iu.A0S(r1);
        r7.A0M = C12960it.A0T(r1);
        r7.A02 = C12970iu.A0Q(r1);
        r7.A0D = C12980iv.A0c(r1);
        r7.A0H = C12970iu.A0b(r1);
        r7.A0L = C12990iw.A0d(r1);
        r7.A0G = (AnonymousClass15K) r1.AKk.get();
        r7.A08 = C12970iu.A0W(r1);
        r7.A0K = C12990iw.A0c(r1);
        r7.A06 = C12960it.A0O(r1);
        r7.A07 = C12960it.A0P(r1);
        r7.A0C = C12960it.A0R(r1);
        r7.A0I = C12980iv.A0e(r1);
        r7.A0B = C12970iu.A0Z(r1);
        r7.A04 = (C22640zP) r1.A3Z.get();
        r7.A0E = (C21320xE) r1.A4Y.get();
        r7.A05 = (AnonymousClass1CS) r1.AKi.get();
        r7.A0F = C12980iv.A0d(r1);
        r7.A09 = (C18640sm) r1.A3u.get();
        return r7;
    }
}
