package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.0sI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18340sI {
    public final AnonymousClass10E A00;
    public final AnonymousClass10F A01;
    public final C18640sm A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final AnonymousClass10D A05;
    public final AbstractC14440lR A06;

    public C18340sI(AnonymousClass10E r1, AnonymousClass10F r2, C18640sm r3, C16590pI r4, C14820m6 r5, AnonymousClass10D r6, AbstractC14440lR r7) {
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x000b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.AnonymousClass23Z r4, X.AnonymousClass23a r5) {
        /*
            r3 = this;
            X.12X r2 = r4.A03
            if (r2 != 0) goto L_0x000f
            r0 = 2
        L_0x0005:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x0009:
            if (r5 == 0) goto L_0x000e
            r5.AQB(r0)
        L_0x000e:
            return
        L_0x000f:
            r1 = 0
            java.util.Map r0 = r2.A00
            if (r0 == 0) goto L_0x0026
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0026
            java.util.Map r0 = r2.A00
            java.lang.Object r0 = r2.A00(r0, r1)
        L_0x0020:
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 != 0) goto L_0x0009
            r0 = -1
            goto L_0x0005
        L_0x0026:
            r0 = 0
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18340sI.A00(X.23Z, X.23a):void");
    }

    public final void A01(C457723b r5) {
        String str = r5.A00;
        SharedPreferences sharedPreferences = this.A04.A00;
        sharedPreferences.edit().putString("support_ban_appeal_state", str).apply();
        if ("UNBANNED".equals(str)) {
            String str2 = r5.A01;
            StringBuilder sb = new StringBuilder("BanAppealRepository/storeUnbanReason ");
            sb.append(str2);
            Log.i(sb.toString());
            sharedPreferences.edit().putString("support_ban_appeal_unban_reason", str2).apply();
            String str3 = r5.A02;
            StringBuilder sb2 = new StringBuilder("BanAppealRepository/storeUnbanReasonUrl ");
            sb2.append(str3);
            Log.i(sb2.toString());
            sharedPreferences.edit().putString("support_ban_appeal_unban_reason_url", str3).apply();
        }
    }
}
