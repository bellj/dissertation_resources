package X;

/* renamed from: X.1gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34801gj extends AbstractC16110oT {
    public Integer A00;

    public C34801gj() {
        super(3016, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamMdSyncdDogfoodingFeatureUsage {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSyncdDogfoodingFeature", obj);
        sb.append("}");
        return sb.toString();
    }
}
