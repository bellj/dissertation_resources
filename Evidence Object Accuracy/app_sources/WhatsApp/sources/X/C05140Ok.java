package X;

/* renamed from: X.0Ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05140Ok {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass03U A04;
    public AnonymousClass03U A05;

    public C05140Ok(AnonymousClass03U r1, AnonymousClass03U r2, int i, int i2, int i3, int i4) {
        this.A05 = r1;
        this.A04 = r2;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A03 = i4;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ChangeInfo{oldHolder=");
        sb.append(this.A05);
        sb.append(", newHolder=");
        sb.append(this.A04);
        sb.append(", fromX=");
        sb.append(this.A00);
        sb.append(", fromY=");
        sb.append(this.A01);
        sb.append(", toX=");
        sb.append(this.A02);
        sb.append(", toY=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
