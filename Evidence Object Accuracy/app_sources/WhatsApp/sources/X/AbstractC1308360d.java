package X;

import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.60d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC1308360d {
    public int A00;
    public C32631cT A01;
    public List A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final ActivityC13790kL A05;
    public final C18640sm A06;
    public final C14830m7 A07;
    public final C18050rp A08;
    public final C21860y6 A09;
    public final C20400vh A0A;
    public final C18650sn A0B;
    public final C18600si A0C;
    public final C18610sj A0D;
    public final AnonymousClass61M A0E;
    public final C129135xE A0F;
    public final AnonymousClass60T A0G;
    public final AnonymousClass61E A0H;
    public final C130015yf A0I;
    public final AnonymousClass6M6 A0J;

    public AbstractC1308360d(C14900mE r2, C15570nT r3, ActivityC13790kL r4, C18640sm r5, C14830m7 r6, C18050rp r7, C21860y6 r8, C20400vh r9, C18650sn r10, C18600si r11, C18610sj r12, AnonymousClass61M r13, C129135xE r14, AnonymousClass60T r15, AnonymousClass61E r16, C130015yf r17, AnonymousClass6M6 r18) {
        this.A07 = r6;
        this.A03 = r2;
        this.A04 = r3;
        this.A08 = r7;
        this.A0F = r14;
        this.A0C = r11;
        this.A09 = r8;
        this.A0I = r17;
        this.A0D = r12;
        this.A0E = r13;
        this.A06 = r5;
        this.A0B = r10;
        this.A0H = r16;
        this.A0G = r15;
        this.A0A = r9;
        this.A05 = r4;
        this.A0J = r18;
    }

    public void A00() {
        this.A0J.Abh(false);
        this.A0C.A08();
        if (!this.A02.isEmpty()) {
            this.A08.A05(this.A02);
            Iterator it = this.A02.iterator();
            while (it.hasNext()) {
                this.A0A.A02(C12970iu.A0x(it));
            }
        }
    }

    public void A01() {
        ActivityC13790kL r2 = this.A05;
        AnonymousClass61M.A02(r2, null, r2.getString(R.string.payments_generic_error)).show();
    }

    public void A02() {
        AnonymousClass6M6 r1;
        boolean z;
        if (this.A0C.A02() != null || this.A08.A02().size() > 0) {
            r1 = this.A0J;
            z = true;
        } else {
            r1 = this.A0J;
            z = false;
        }
        r1.Abh(z);
    }

    public final void A03() {
        FingerprintBottomSheet A0D = C117305Zk.A0D();
        C14830m7 r3 = this.A07;
        C129155xG r4 = new C129155xG(this.A04, r3, this.A0D, this.A0H, "AUTH");
        C130015yf r6 = this.A0I;
        ActivityC13790kL r1 = this.A05;
        A0D.A05 = new C119455e0(r1, A0D, r3, r4, new C133736Bx(A0D, this), r6);
        r1.Adm(A0D);
    }

    public void A04(ActivityC13790kL r5, String str) {
        C32631cT r0;
        AbstractC460224d r2;
        C460124c A01 = this.A08.A01(str);
        if (A01 != null) {
            r0 = A01.A03;
        } else {
            r0 = null;
        }
        this.A01 = r0;
        if (r0 != null && (r2 = r0.A00) != null && r2.A00.equals("WEBVIEW")) {
            if (((C460324e) r2).A00) {
                if (Build.VERSION.SDK_INT >= 23) {
                    AnonymousClass61E r1 = this.A0H;
                    if (r1.A07() && r1.A02() == 1) {
                        A03();
                        return;
                    }
                }
                PinBottomSheetDialogFragment A00 = C125035qZ.A00();
                A00.A0B = new AnonymousClass6CF(A00, this);
                this.A05.Adm(A00);
                return;
            }
            A05(null, null);
        }
    }

    public void A05(PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AnonymousClass1V8 r10) {
        new C129145xF(this.A05, this.A03, this.A06, this.A0B, this.A0D).A00(new AnonymousClass6B0(pinBottomSheetDialogFragment, this), this.A01, r10);
    }

    public void A06(String str, String str2) {
        C460124c A01;
        this.A00 = 1;
        A02();
        if (!TextUtils.isEmpty(str) && str.equals("STEP_UP")) {
            C12960it.A0t(C117295Zj.A05(this.A0C), "payment_step_up_update_ack", true);
            if (!(str2 == null || (A01 = this.A08.A01(str2)) == null)) {
                A01.A00 = false;
            }
            this.A0A.A02(str2);
        }
        this.A02 = C12960it.A0l();
    }
}
