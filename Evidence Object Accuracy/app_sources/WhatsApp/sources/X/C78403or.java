package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78403or extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99184jt();
    public final float A00;
    public final float A01;
    public final int A02;
    public final int A03;

    public C78403or(float f, float f2, int i, int i2) {
        this.A03 = i;
        this.A00 = f;
        this.A01 = f2;
        this.A02 = i2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A03);
        C95654e8.A05(parcel, this.A00, 2);
        C95654e8.A05(parcel, this.A01, 3);
        C95654e8.A07(parcel, 4, this.A02);
        C95654e8.A06(parcel, A00);
    }
}
