package X;

import android.app.IntentService;
import com.whatsapp.backup.google.GoogleBackupService;
import com.whatsapp.notification.AndroidWear;
import com.whatsapp.notification.DirectReplyService;
import java.util.Random;

/* renamed from: X.1Il  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractIntentServiceC27651Il extends IntentService implements AnonymousClass004 {
    public final Object A00 = new Object();
    public volatile C71083cM A01;

    public AbstractIntentServiceC27651Il(String str) {
        super(str);
    }

    public void A00() {
        if (this instanceof DirectReplyService) {
            DirectReplyService directReplyService = (DirectReplyService) this;
            if (!directReplyService.A09) {
                directReplyService.A09 = true;
                AnonymousClass01J r2 = ((C58182oH) ((AnonymousClass5B7) directReplyService.generatedComponent())).A01;
                directReplyService.A00 = (C14900mE) r2.A8X.get();
                directReplyService.A01 = (C16170oZ) r2.AM4.get();
                directReplyService.A02 = (C15550nR) r2.A45.get();
                directReplyService.A04 = (AnonymousClass01d) r2.ALI.get();
                directReplyService.A05 = (AnonymousClass12H) r2.AC5.get();
                directReplyService.A06 = (C20220vP) r2.AC3.get();
                directReplyService.A07 = (C20230vQ) r2.ACe.get();
                directReplyService.A03 = (C250417w) r2.A4b.get();
                directReplyService.A08 = (C16630pM) r2.AIc.get();
            }
        } else if (!(this instanceof AndroidWear)) {
            GoogleBackupService googleBackupService = (GoogleBackupService) this;
            if (!googleBackupService.A0Y) {
                googleBackupService.A0Y = true;
                googleBackupService.A0X = new Random();
                AnonymousClass01J r22 = ((C58182oH) ((AnonymousClass5B7) googleBackupService.generatedComponent())).A01;
                googleBackupService.A0H = (C14830m7) r22.ALb.get();
                googleBackupService.A0P = (C14850m9) r22.A04.get();
                googleBackupService.A03 = (C14900mE) r22.A8X.get();
                googleBackupService.A01 = (AbstractC15710nm) r22.A4o.get();
                googleBackupService.A04 = (C15570nT) r22.AAr.get();
                googleBackupService.A0I = (C16590pI) r22.AMg.get();
                googleBackupService.A02 = (C14330lG) r22.A7B.get();
                googleBackupService.A0Q = (C16120oU) r22.ANE.get();
                googleBackupService.A0F = (C15810nw) r22.A73.get();
                googleBackupService.A0S = (C22660zR) r22.AAk.get();
                googleBackupService.A0R = (C17220qS) r22.ABt.get();
                googleBackupService.A0E = (C19380u1) r22.A1N.get();
                googleBackupService.A0V = (C21710xr) r22.ANg.get();
                googleBackupService.A05 = (C15820nx) r22.A6U.get();
                googleBackupService.A0U = (AbstractC15850o0) r22.ANA.get();
                googleBackupService.A0G = (C17050qB) r22.ABL.get();
                googleBackupService.A0O = (C21630xj) r22.ACc.get();
                googleBackupService.A0D = (AnonymousClass1D6) r22.A1G.get();
                googleBackupService.A0T = (C26981Fo) r22.ABG.get();
                googleBackupService.A0L = (C15880o3) r22.ACF.get();
                googleBackupService.A06 = (C25721Am) r22.A1B.get();
                googleBackupService.A08 = (C26551Dx) r22.A8Z.get();
                googleBackupService.A0N = (C16490p7) r22.ACJ.get();
                googleBackupService.A0C = (C27051Fv) r22.AHd.get();
                googleBackupService.A0J = (C15890o4) r22.AN1.get();
                googleBackupService.A0K = (C14820m6) r22.AN3.get();
                googleBackupService.A0M = (C20850wQ) r22.ACI.get();
                googleBackupService.A07 = (C22730zY) r22.A8Y.get();
                googleBackupService.A09 = (AnonymousClass10N) r22.A8a.get();
                googleBackupService.A0B = (AnonymousClass10K) r22.A8c.get();
                googleBackupService.A0A = (AnonymousClass10J) r22.A8b.get();
            }
        } else {
            AndroidWear androidWear = (AndroidWear) this;
            if (!androidWear.A07) {
                androidWear.A07 = true;
                AnonymousClass01J r23 = ((C58182oH) ((AnonymousClass5B7) androidWear.generatedComponent())).A01;
                androidWear.A00 = (C14900mE) r23.A8X.get();
                androidWear.A01 = (C16170oZ) r23.AM4.get();
                androidWear.A02 = (C15550nR) r23.A45.get();
                androidWear.A04 = (AnonymousClass01d) r23.ALI.get();
                androidWear.A05 = (C20220vP) r23.AC3.get();
                androidWear.A03 = (C250417w) r23.A4b.get();
                androidWear.A06 = (C16630pM) r23.AIc.get();
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A01 == null) {
            synchronized (this.A00) {
                if (this.A01 == null) {
                    this.A01 = new C71083cM(this);
                }
            }
        }
        return this.A01.generatedComponent();
    }

    @Override // android.app.IntentService, android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
    }
}
