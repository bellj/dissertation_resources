package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.1RP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RP {
    public final C14830m7 A00;
    public final AnonymousClass1RO A01;

    public AnonymousClass1RP(C14830m7 r1, AnonymousClass1RO r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(int i, byte[] bArr) {
        C16310on A02 = this.A01.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("prekey_id", Integer.valueOf(i));
            contentValues.put("timestamp", Long.valueOf(this.A00.A00() / 1000));
            contentValues.put("record", bArr);
            A02.A03.A05(contentValues, "signed_prekeys");
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl stored signed pre key with id ");
            sb.append(i);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public byte[] A01(int i) {
        C16310on A01 = this.A01.get();
        try {
            Cursor A08 = A01.A03.A08("signed_prekeys", "prekey_id = ?", null, null, new String[]{"record"}, new String[]{String.valueOf(i)});
            if (!A08.moveToNext()) {
                StringBuilder sb = new StringBuilder();
                sb.append("no signed prekey available with id ");
                sb.append(i);
                Log.e(sb.toString());
                A08.close();
                A01.close();
                return null;
            }
            byte[] blob = A08.getBlob(0);
            A08.close();
            A01.close();
            return blob;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
