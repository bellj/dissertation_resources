package X;

import com.whatsapp.settings.SettingsPrivacy;

/* renamed from: X.37M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37M extends AbstractC16350or {
    public final /* synthetic */ AnonymousClass1BO A00;
    public final /* synthetic */ SettingsPrivacy A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass37M(AbstractC001200n r1, AnonymousClass1BO r2, SettingsPrivacy settingsPrivacy, String str) {
        super(r1);
        this.A01 = settingsPrivacy;
        this.A00 = r2;
        this.A02 = str;
    }
}
