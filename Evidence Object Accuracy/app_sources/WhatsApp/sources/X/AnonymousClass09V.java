package X;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* renamed from: X.09V  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass09V extends Dialog {
    public static final AbstractC12080hL A0H = new C08370b1();
    public static final AbstractC12080hL A0I = new AnonymousClass0b2();
    public float A00 = 1.0f;
    public float A01 = 1.0f;
    public int A02 = -16777216;
    public Context A03;
    public View A04;
    public FrameLayout A05;
    public AnonymousClass0NR A06;
    public AbstractC12080hL A07 = new C08390b4(this);
    public AbstractC12080hL A08 = A0I;
    public AnonymousClass0BC A09;
    public boolean A0A = true;
    public boolean A0B = true;
    public boolean A0C = true;
    public boolean A0D = false;
    public boolean A0E = true;
    public final Handler A0F = new Handler(Looper.getMainLooper());
    public final AnonymousClass0P3 A0G = new AnonymousClass0P3(this);

    public AnonymousClass09V(Context context) {
        super(context, R.style.Bloks_BottomSheetDialog);
        Context context2 = getContext();
        this.A03 = context2;
        AnonymousClass0BC r3 = new AnonymousClass0BC(context2);
        this.A09 = r3;
        r3.A04 = this.A0G;
        r3.A00 = -1;
        r3.A02(new AbstractC12080hL[]{A0H, this.A08, this.A07}, true);
        AnonymousClass0BC r2 = this.A09;
        r2.A05 = new C04560Me(this);
        if (Build.VERSION.SDK_INT >= 19) {
            r2.setFitsSystemWindows(true);
        }
        this.A09.A07.A03();
        FrameLayout frameLayout = new FrameLayout(this.A03);
        this.A05 = frameLayout;
        frameLayout.addView(this.A09);
        super.setContentView(this.A05);
        AnonymousClass028.A0g(this.A09, new AnonymousClass0DT(this));
    }

    public void A00() {
        InputMethodManager inputMethodManager;
        View currentFocus = getCurrentFocus();
        if (!(currentFocus == null || (inputMethodManager = (InputMethodManager) currentFocus.getContext().getSystemService("input_method")) == null)) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
        super.dismiss();
    }

    public final void A01() {
        InputMethodManager inputMethodManager;
        Window window = getWindow();
        AnonymousClass0BC r2 = this.A09;
        if (!r2.hasWindowFocus()) {
            A00();
        }
        if (window != null) {
            window.setFlags(8, 8);
        }
        this.A0D = true;
        if (!this.A0A && this.A01 != 0.0f) {
            this.A01 = 0.0f;
            A02(this.A00);
        }
        r2.A07.A03();
        r2.A01(A0H, -1);
        r2.setInteractable(false);
        View currentFocus = getCurrentFocus();
        if (currentFocus != null && (inputMethodManager = (InputMethodManager) currentFocus.getContext().getSystemService("input_method")) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    public final void A02(float f) {
        ColorDrawable colorDrawable;
        float f2 = this.A01 * f;
        Window window = getWindow();
        if (window != null) {
            ViewGroup viewGroup = (ViewGroup) window.getDecorView();
            View childAt = viewGroup.getChildAt(0);
            View view = viewGroup;
            if (childAt != null) {
                view = childAt;
            }
            int A06 = C016907y.A06(this.A02, (int) (Math.min(1.0f, Math.max(0.0f, f2)) * 255.0f));
            Drawable background = view.getBackground();
            if (background instanceof ColorDrawable) {
                colorDrawable = (ColorDrawable) background;
            } else {
                colorDrawable = new ColorDrawable();
                view.setBackground(colorDrawable);
            }
            colorDrawable.setColor(A06);
        }
    }

    public void A03(EnumC03710Iv r5) {
        AnonymousClass0NR r0 = this.A06;
        if (r0 != null) {
            AnonymousClass0QB r3 = r0.A01;
            Context context = r0.A00;
            if (r5 == EnumC03710Iv.BACK_BUTTON) {
                r3.A08.peek();
                r3.A09.peek();
                if (r3.A0B.size() > 1) {
                    r3.A02(context);
                    return;
                }
                AnonymousClass09V r02 = r3.A05;
                if (r02 != null) {
                    r02.dismiss();
                    return;
                }
                return;
            }
        }
        super.cancel();
    }

    @Override // android.app.Dialog, android.content.DialogInterface
    public void cancel() {
        A03(EnumC03710Iv.OTHER);
    }

    @Override // android.app.Dialog, android.content.DialogInterface
    public void dismiss() {
        Looper myLooper = Looper.myLooper();
        Handler handler = this.A0F;
        if (myLooper == handler.getLooper()) {
            A01();
        } else {
            handler.post(new RunnableC09400cq(this));
        }
    }

    @Override // android.app.Dialog, android.view.Window.Callback
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            return true;
        }
        return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    @Override // android.app.Dialog
    public void onBackPressed() {
        if (this.A0B) {
            A03(EnumC03710Iv.BACK_BUTTON);
        }
    }

    @Override // android.app.Dialog
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        this.A0B = z;
    }

    @Override // android.app.Dialog
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        this.A0C = z;
    }

    @Override // android.app.Dialog
    public void setContentView(int i) {
        setContentView(LayoutInflater.from(getContext()).inflate(i, (ViewGroup) this.A09, false), null);
    }

    @Override // android.app.Dialog
    public void setContentView(View view) {
        setContentView(view, null);
    }

    @Override // android.app.Dialog
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        View view2 = this.A04;
        if (view2 != null) {
            this.A09.removeView(view2);
        }
        this.A04 = view;
        if (layoutParams == null) {
            this.A09.addView(view);
        } else {
            this.A09.addView(view, layoutParams);
        }
    }

    @Override // android.app.Dialog
    public void show() {
        AbstractC12080hL r0;
        AccessibilityManager accessibilityManager;
        this.A0D = false;
        AnonymousClass0BC r3 = this.A09;
        r3.A07.A03();
        r3.A0C = true;
        super.show();
        Context context = this.A03;
        if ((!Boolean.getBoolean("is_accessibility_enabled") && (context == null || (accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility")) == null || !accessibilityManager.isTouchExplorationEnabled())) || (r0 = this.A07) == null) {
            r0 = this.A08;
        }
        r3.A01(r0, -1);
    }
}
