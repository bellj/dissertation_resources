package X;

/* renamed from: X.1Au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25781Au {
    public final AnonymousClass19Q A00;
    public final C251118d A01;
    public final C16430p0 A02;
    public final C14850m9 A03;
    public final C16120oU A04;

    public C25781Au(AnonymousClass19Q r1, C251118d r2, C16430p0 r3, C14850m9 r4, C16120oU r5) {
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static final Integer A00(Integer num) {
        int intValue;
        int i;
        if (num == null || (intValue = num.intValue()) < 0) {
            return null;
        }
        int i2 = 1;
        if (intValue != 0) {
            if (intValue == 1) {
                i = 2;
            } else {
                i2 = 10;
                if (intValue <= 10) {
                    i = 3;
                } else if (intValue <= 50) {
                    i = 4;
                } else if (intValue <= 100) {
                    i = 5;
                } else if (intValue <= 500) {
                    i = 6;
                } else if (intValue <= 1000) {
                    i = 7;
                } else if (intValue <= 10000) {
                    i = 8;
                } else if (intValue <= 100000) {
                    i = 9;
                } else if (intValue > 1000000) {
                    i = 11;
                }
            }
            return Integer.valueOf(i);
        }
        return Integer.valueOf(i2);
    }

    public void A01(AnonymousClass2VB r13, int i) {
        Integer valueOf;
        C14850m9 r2 = this.A01.A00;
        if (r2.A07(450) && r2.A07(1295) && r13 != null) {
            C614730m r1 = new C614730m();
            r1.A03 = r13.A05;
            r1.A04 = r13.A08;
            r1.A01 = r13.A04;
            r1.A02 = Integer.valueOf(i);
            this.A04.A07(r1);
        }
        if (r2.A07(450) && r2.A07(1951) && r13 != null) {
            C16430p0 r22 = this.A02;
            int i2 = r13.A03;
            double d = r13.A01;
            double d2 = r13.A00;
            String str = r13.A07;
            String str2 = r13.A06;
            int i3 = 2;
            if (i != 1) {
                if (i == 2 || i == 3) {
                    i3 = 4;
                } else {
                    int i4 = 6;
                    if (i != 6) {
                        i4 = 7;
                        i3 = 5;
                        if (i != 7) {
                            i3 = 8;
                            if (i != 8) {
                                if (i != 10) {
                                    valueOf = null;
                                    r22.A09(valueOf, str, str2, d, d2, i2, r13.A02);
                                }
                            }
                        }
                    }
                    valueOf = Integer.valueOf(i4);
                    r22.A09(valueOf, str, str2, d, d2, i2, r13.A02);
                }
            }
            valueOf = Integer.valueOf(i3);
            r22.A09(valueOf, str, str2, d, d2, i2, r13.A02);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r0.intValue() != 0) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.AnonymousClass2VB r5, int r6, boolean r7) {
        /*
            r4 = this;
            X.30m r3 = new X.30m
            r3.<init>()
            if (r5 == 0) goto L_0x0019
            java.lang.String r0 = r5.A05
            r3.A03 = r0
            java.lang.String r0 = r5.A08
            r3.A04 = r0
            java.lang.Integer r0 = r5.A04
            r3.A01 = r0
            int r0 = r0.intValue()
            if (r0 == 0) goto L_0x003b
        L_0x0019:
            r0 = 17
            if (r6 == r0) goto L_0x0021
            r0 = 15
            if (r6 != r0) goto L_0x003b
        L_0x0021:
            X.0p0 r2 = r4.A02
            r0 = 5
            r2.A00 = r0
            java.util.Random r0 = r2.A02
            if (r0 != 0) goto L_0x0031
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            r2.A02 = r0
        L_0x0031:
            long r0 = r0.nextLong()
            java.lang.String r0 = java.lang.Long.toHexString(r0)
            r2.A01 = r0
        L_0x003b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)
            r3.A00 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r3.A02 = r0
            X.0oU r0 = r4.A04
            r0.A07(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25781Au.A02(X.2VB, int, boolean):void");
    }

    public void A03(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, String str, int i, boolean z, boolean z2) {
        C14850m9 r3 = this.A03;
        if (r3.A07(904)) {
            C856743s r4 = new C856743s();
            AnonymousClass19Q r5 = this.A00;
            r4.A09 = Long.valueOf((long) r5.A08.getAndIncrement());
            r4.A06 = Integer.valueOf(i);
            r4.A0B = r5.A00;
            r4.A0A = str;
            r4.A07 = num;
            r4.A01 = Boolean.valueOf(z);
            r4.A02 = num2;
            r4.A03 = num3;
            r4.A04 = num5;
            r4.A00 = Boolean.valueOf(z2);
            if (r3.A07(1255)) {
                r4.A05 = num4;
            }
            this.A04.A07(r4);
            return;
        }
        C856543q r1 = new C856543q();
        r1.A06 = Integer.valueOf(i);
        r1.A09 = this.A00.A00;
        r1.A07 = num;
        r1.A01 = Boolean.valueOf(z);
        r1.A02 = num2;
        r1.A03 = num3;
        r1.A04 = num5;
        r1.A00 = Boolean.valueOf(z2);
        if (r3.A07(1255)) {
            r1.A05 = num4;
        }
        this.A04.A07(r1);
    }

    public void A04(Integer num, Integer num2, String str, int i, int i2, boolean z) {
        Integer num3;
        Integer num4;
        if (i2 == 0) {
            num3 = A00(num2);
        } else {
            num3 = null;
        }
        int i3 = 1;
        if (i2 == 1) {
            num4 = A00(num2);
        } else {
            num4 = null;
            if (i2 == 0) {
                i3 = 0;
            }
        }
        A03(null, num3, num4, num, Integer.valueOf(i3), str, i, true, z);
    }

    public void A05(Integer num, Integer num2, String str, int i, boolean z, boolean z2) {
        A03(num, null, null, num2, null, str, i, z, z2);
    }
}
