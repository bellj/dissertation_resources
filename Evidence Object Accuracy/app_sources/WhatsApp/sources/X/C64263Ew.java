package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* renamed from: X.3Ew  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64263Ew {
    public final AbstractC27101Ga A00 = new AnonymousClass3UK(this);
    public final C22820zh A01;
    public final C22920zr A02;
    public final AnonymousClass101 A03;
    public final AnonymousClass3CL A04;
    public final C20720wD A05;
    public final AbstractC22250ym A06 = new AbstractC22250ym() { // from class: X.3WW
        @Override // X.AbstractC22250ym
        public final void AMm(List list) {
            C64263Ew r3 = C64263Ew.this;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                DeviceJid deviceJid = (DeviceJid) it.next();
                if (deviceJid != null) {
                    r3.A0K.remove(deviceJid);
                    r3.A04.A00(deviceJid);
                }
            }
        }
    };
    public final C237112s A07;
    public final C14830m7 A08;
    public final C15990oG A09;
    public final C18240s8 A0A;
    public final C21490xV A0B;
    public final C21540xa A0C;
    public final C17650rA A0D;
    public final AbstractC35861it A0E = new C68713Wl(this);
    public final C237312u A0F;
    public final C22130yZ A0G;
    public final C14850m9 A0H;
    public final C16120oU A0I;
    public final C21510xX A0J;
    public final Set A0K = Collections.newSetFromMap(new ConcurrentHashMap());
    public volatile Future A0L;

    public C64263Ew(C22820zh r2, C22920zr r3, AnonymousClass101 r4, AnonymousClass3CL r5, C20720wD r6, C237112s r7, C14830m7 r8, C15990oG r9, C18240s8 r10, C21490xV r11, C21540xa r12, C17650rA r13, C237312u r14, C22130yZ r15, C14850m9 r16, C16120oU r17, C21510xX r18) {
        this.A08 = r8;
        this.A0H = r16;
        this.A0I = r17;
        this.A0A = r10;
        this.A02 = r3;
        this.A05 = r6;
        this.A01 = r2;
        this.A09 = r9;
        this.A0G = r15;
        this.A03 = r4;
        this.A0J = r18;
        this.A0D = r13;
        this.A07 = r7;
        this.A0F = r14;
        this.A0B = r11;
        this.A0C = r12;
        this.A04 = r5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:150:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x033b  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x037a  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0398  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x03a7  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x03c9  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x03f2  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x036a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C93744af A00(X.AnonymousClass2O7 r42) {
        /*
        // Method dump skipped, instructions count: 1100
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64263Ew.A00(X.2O7):X.4af");
    }

    public boolean A01(DeviceJid deviceJid, String str, byte[] bArr, int i) {
        if (i < 0 || i > 4) {
            Log.i(C12960it.A0W(i, "voip/receive_message/onPeerE2EDecryptionFailed do nothing for retry count: "));
        } else if (bArr == null || bArr.length != 4) {
            Log.i(C12960it.A0d(Arrays.toString(bArr), C12960it.A0k("voip/receive_message/onPeerE2EDecryptionFailed e2e decryption failure; invalid remote remoteRegBytes id; remoteRegistrationId=")));
            return false;
        } else {
            int A00 = C16050oM.A00(bArr);
            StringBuilder A0k = C12960it.A0k("voip/receive_message/onPeerE2EDecryptionFailed peer e2e decryption failure; remoteRegistrationId=");
            A0k.append(A00);
            A0k.append(" retryCount: ");
            A0k.append(i);
            A0k.append(" from: ");
            A0k.append(deviceJid);
            C12960it.A1F(A0k);
            try {
                C18240s8 r0 = this.A0A;
                DeviceJid deviceJid2 = (DeviceJid) r0.A00.submit(new Callable(deviceJid, this, str, A00, i) { // from class: X.3d1
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ int A01;
                    public final /* synthetic */ DeviceJid A02;
                    public final /* synthetic */ C64263Ew A03;
                    public final /* synthetic */ String A04;

                    {
                        this.A03 = r2;
                        this.A02 = r1;
                        this.A04 = r3;
                        this.A00 = r4;
                        this.A01 = r5;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        StringBuilder A0k2;
                        String str2;
                        C64263Ew r2 = this.A03;
                        DeviceJid deviceJid3 = this.A02;
                        String str3 = this.A04;
                        int i2 = this.A00;
                        int i3 = this.A01;
                        AnonymousClass1IS r3 = new AnonymousClass1IS(deviceJid3.getUserJid(), str3, true);
                        C15950oC A02 = C15940oB.A02(deviceJid3);
                        C15990oG r7 = r2.A09;
                        AnonymousClass1RV A0G = r7.A0G(A02);
                        C31311aL r10 = A0G.A01;
                        byte[] A04 = r10.A00.A05.A04();
                        if (A0G.A00 || r10.A00.A03 != i2) {
                            A0k2 = C12960it.A0k("voip/receive_message/onPeerE2EDecryptionFailed registration id is not equal. stored= ");
                            A0k2.append(r10.A00.A03);
                            A0k2.append(", incoming=");
                            A0k2.append(i2);
                            str2 = ". Fetching new prekey for: ";
                        } else if (i3 <= 2 || !r7.A0i(A02, r3)) {
                            if (i3 == 2) {
                                Log.i(C12960it.A0b("voip/receive_message/onPeerE2EDecryptionFailed recording base key. ", r3));
                                r7.A0Z(A02, r3, A04);
                            }
                            return deviceJid3;
                        } else {
                            A0k2 = C12960it.A0h();
                            str2 = "voip/receive_message/onPeerE2EDecryptionFailed reg id is equal and has same basekey. Fetching new prekey for: ";
                        }
                        A0k2.append(str2);
                        A0k2.append(r3);
                        C12960it.A1F(A0k2);
                        if (r2.A0K.add(deviceJid3)) {
                            Log.i(C12960it.A0b("voip/encryption/startGetPreKeyJob for ", deviceJid3));
                            r2.A03.A04(new DeviceJid[]{deviceJid3}, 7, false);
                            return null;
                        }
                        Log.i(C12960it.A0b("voip/encryption/startGetPreKeyJob do nothing, PreKey already sent for ", deviceJid3));
                        return null;
                    }
                }).get();
                if (deviceJid2 != null) {
                    this.A04.A00(deviceJid2);
                    return true;
                }
            } catch (InterruptedException | ExecutionException e) {
                Log.i("voip/receive_message/onPeerE2EDecryptionFailed session retry threw: ", e);
                return false;
            }
        }
        return true;
    }
}
