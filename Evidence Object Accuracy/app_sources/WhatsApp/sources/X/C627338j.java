package X;

import android.util.Pair;
import java.util.List;

/* renamed from: X.38j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627338j extends AbstractC16350or {
    public long A00;
    public String A01;
    public final Pair A02;
    public final AnonymousClass10G A03;
    public final AbstractC32851cq A04 = new C68613Wb(this);
    public final C17050qB A05;
    public final C14950mJ A06;
    public final AnonymousClass1MJ A07;
    public final C91694Ss A08;
    public final AnonymousClass10D A09;
    public final String A0A;
    public final String A0B;
    public final List A0C;
    public final boolean A0D;

    public /* synthetic */ C627338j(Pair pair, AnonymousClass10G r3, C17050qB r4, C14950mJ r5, AnonymousClass1MJ r6, C91694Ss r7, AnonymousClass10D r8, String str, String str2, List list, boolean z) {
        this.A06 = r5;
        this.A03 = r3;
        this.A05 = r4;
        this.A09 = r8;
        this.A08 = r7;
        this.A0A = str;
        this.A0B = str2;
        this.A02 = pair;
        this.A0C = list;
        this.A0D = z;
        this.A07 = r6;
    }
}
