package X;

import android.os.SystemClock;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0eL  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0eL implements Runnable, Delayed {
    public long A00;
    public long A01;
    public String A02;

    public void A00() {
    }

    @Override // java.lang.Runnable
    public abstract void run();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        if (r1 < 0) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        if (r1 == 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r1 > 0) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        return -1;
     */
    @Override // java.lang.Comparable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ int compareTo(java.lang.Object r9) {
        /*
            r8 = this;
            java.util.concurrent.Delayed r9 = (java.util.concurrent.Delayed) r9
            boolean r0 = r9 instanceof X.AnonymousClass0eL
            if (r0 == 0) goto L_0x002a
            X.0eL r9 = (X.AnonymousClass0eL) r9
            long r1 = r8.A01
            long r3 = r9.A01
            r7 = 0
            r5 = 0
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x001e
            long r1 = r8.A00
            long r3 = r9.A00
            long r1 = r1 - r3
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0024
        L_0x001c:
            r7 = -1
        L_0x001d:
            return r7
        L_0x001e:
            long r1 = r1 - r3
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0024
            goto L_0x001c
        L_0x0024:
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x001d
            r7 = 1
            return r7
        L_0x002a:
            java.lang.String r1 = "Comparing a Dispatchable to a non-Dispatchable."
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0eL.compareTo(java.lang.Object):int");
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass0eL)) {
            return false;
        }
        AnonymousClass0eL r7 = (AnonymousClass0eL) obj;
        if (this.A00 == r7.A00) {
            String str = this.A02;
            String str2 = r7.A02;
            if (str != null ? str.equals(str2) : str2 == null) {
                if (this.A01 == r7.A01) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.util.concurrent.Delayed
    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(this.A01 - SystemClock.uptimeMillis(), TimeUnit.MILLISECONDS);
    }
}
