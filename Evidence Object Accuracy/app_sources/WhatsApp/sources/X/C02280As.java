package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.AbsSavedState;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0As  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02280As extends AbsSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(21);

    public C02280As(Parcel parcel) {
        super(parcel);
    }

    public C02280As(Parcelable parcelable) {
        super(parcelable);
    }
}
