package X;

/* renamed from: X.4Aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87144Aj {
    /* Fake field, exist only in values array */
    EF3(0),
    A01(1);
    
    public final int mIntValue;

    EnumC87144Aj(int i) {
        this.mIntValue = i;
    }
}
