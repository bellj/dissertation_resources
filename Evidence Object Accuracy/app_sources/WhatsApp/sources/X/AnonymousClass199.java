package X;

import android.media.AudioManager;
import com.whatsapp.R;

/* renamed from: X.199  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass199 {
    public long A00;
    public AudioManager.OnAudioFocusChangeListener A01;
    public final C14900mE A02;
    public final AnonymousClass01d A03;

    public AnonymousClass199(C14900mE r1, AnonymousClass01d r2) {
        this.A02 = r1;
        this.A03 = r2;
    }

    public void A00() {
        AudioManager A0G = this.A03.A0G();
        if (A0G != null) {
            AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = this.A01;
            if (onAudioFocusChangeListener == null) {
                onAudioFocusChangeListener = new AnonymousClass2P2();
                this.A01 = onAudioFocusChangeListener;
            }
            A0G.abandonAudioFocus(onAudioFocusChangeListener);
        }
    }

    public void A01() {
        AudioManager A0G = this.A03.A0G();
        if (A0G != null) {
            AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = this.A01;
            if (onAudioFocusChangeListener == null) {
                onAudioFocusChangeListener = new AnonymousClass2P2();
                this.A01 = onAudioFocusChangeListener;
            }
            A0G.requestAudioFocus(onAudioFocusChangeListener, 3, 2);
        }
    }

    public boolean A02() {
        AudioManager A0G = this.A03.A0G();
        if (A0G == null || A0G.getStreamVolume(3) != 0) {
            return true;
        }
        if (System.currentTimeMillis() - this.A00 > 2000) {
            this.A00 = System.currentTimeMillis();
            this.A02.A07(R.string.please_turn_volume_up, 0);
        }
        return false;
    }
}
