package X;

import android.os.Bundle;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3AU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3AU {
    public static final CatalogSearchFragment A00(UserJid userJid, int i) {
        C16700pc.A0E(userJid, 0);
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("category_biz_id", userJid);
        A0D.putInt("search_entry_point", i);
        CatalogSearchFragment catalogSearchFragment = new CatalogSearchFragment();
        catalogSearchFragment.A0U(A0D);
        return catalogSearchFragment;
    }
}
