package X;

/* renamed from: X.19y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255819y {
    public final C16120oU A00;
    public final boolean A01;

    public C255819y(C14850m9 r2, C16120oU r3) {
        this.A00 = r3;
        this.A01 = r2.A07(790);
    }

    public final void A00(int i) {
        if (this.A01) {
            AnonymousClass43H r1 = new AnonymousClass43H();
            r1.A01 = 1;
            r1.A00 = Integer.valueOf(i);
            this.A00.A07(r1);
        }
    }
}
