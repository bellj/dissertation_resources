package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.1Px  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class RunnableC29011Px extends EmptyBaseRunnable0 implements Runnable {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A01;
    public final C15570nT A02;
    public final C20870wS A03;
    public final C22920zr A04;
    public final C18790t3 A05;
    public final C20670w8 A06;
    public final C18200s4 A07;
    public final C18850tA A08;
    public final AnonymousClass10W A09;
    public final AnonymousClass1EC A0A;
    public final C15990oG A0B;
    public final C22320yt A0C;
    public final C20830wO A0D;
    public final C15650ng A0E;
    public final C238113c A0F;
    public final C15600nX A0G;
    public final AnonymousClass12H A0H;
    public final C18470sV A0I;
    public final C22180yf A0J;
    public final C16120oU A0K;
    public final C20710wC A0L;
    public final C18810t5 A0M;
    public final C238613h A0N;
    public final C19460u9 A0O;
    public final C22170ye A0P;
    public final C20660w7 A0Q;
    public final C17230qT A0R;
    public final C28941Pp A0S;
    public final AbstractC14440lR A0T;

    public RunnableC29011Px(AbstractC15710nm r3, C15570nT r4, C20870wS r5, C22920zr r6, C18790t3 r7, C20670w8 r8, C18200s4 r9, C18850tA r10, AnonymousClass10W r11, AnonymousClass1EC r12, C15990oG r13, C22320yt r14, C20830wO r15, C15650ng r16, C238113c r17, C15600nX r18, AnonymousClass12H r19, C18470sV r20, C22180yf r21, C16120oU r22, C20710wC r23, C18810t5 r24, C238613h r25, C19460u9 r26, C22170ye r27, C20660w7 r28, C17230qT r29, C28941Pp r30, AbstractC14440lR r31) {
        this.A01 = r3;
        this.A02 = r4;
        this.A0T = r31;
        this.A05 = r7;
        this.A0K = r22;
        this.A0Q = r28;
        this.A0I = r20;
        this.A08 = r10;
        this.A0P = r27;
        this.A06 = r8;
        this.A0J = r21;
        this.A03 = r5;
        this.A0E = r16;
        this.A0F = r17;
        this.A0L = r23;
        this.A0S = r30;
        this.A0H = r19;
        this.A04 = r6;
        this.A0B = r13;
        this.A0C = r14;
        this.A0R = r29;
        this.A0M = r24;
        this.A0A = r12;
        this.A0N = r25;
        this.A09 = r11;
        this.A0G = r18;
        this.A0O = r26;
        this.A07 = r9;
        this.A0D = r15;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        if (r5 == -1001) goto L_0x0045;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A00(X.C16020oJ r7, X.AnonymousClass31D r8, X.AnonymousClass2LM r9, boolean r10) {
        /*
            r6 = this;
            r2 = 0
            if (r7 == 0) goto L_0x008a
            int r5 = r7.A00
            if (r5 == 0) goto L_0x008a
            java.lang.String r1 = "decryptmessagerunnable/handleDecryptionResult axolotl status="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.w(r0)
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r8.A00 = r0
            if (r9 == 0) goto L_0x0020
            r9.A02 = r2
        L_0x0020:
            r1 = -1001(0xfffffffffffffc17, float:NaN)
            if (r5 == r1) goto L_0x0045
            java.lang.Integer r0 = X.C20870wS.A07(r5)
            r8.A04 = r0
            r0 = -1008(0xfffffffffffffc10, float:NaN)
            r4 = 1
            if (r5 == r0) goto L_0x0064
            r0 = -1002(0xfffffffffffffc16, float:NaN)
            if (r5 == r0) goto L_0x0064
            r0 = -1003(0xfffffffffffffc15, float:NaN)
            if (r5 == r0) goto L_0x0064
            r0 = -1005(0xfffffffffffffc13, float:NaN)
            if (r5 == r0) goto L_0x0064
            r0 = -1006(0xfffffffffffffc12, float:NaN)
            if (r5 == r0) goto L_0x0080
            r0 = -1007(0xfffffffffffffc11, float:NaN)
            if (r5 == r0) goto L_0x0080
            if (r5 != r1) goto L_0x008a
        L_0x0045:
            if (r10 != 0) goto L_0x008a
            X.1Pp r3 = r6.A0S
            boolean r0 = r3.A0b
            if (r0 != 0) goto L_0x008a
            X.0o9 r0 = r3.A09
            if (r0 == 0) goto L_0x005d
            X.0lR r2 = r6.A0T
            r1 = 15
            com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7 r0 = new com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7
            r0.<init>(r6, r1, r3)
            r2.Ab2(r0)
        L_0x005d:
            X.0ye r0 = r6.A0P
            r0.A08(r3)
            r0 = 2
            return r0
        L_0x0064:
            X.1Pp r3 = r6.A0S
            r3.A0b = r4
            r0 = -1008(0xfffffffffffffc10, float:NaN)
            r1 = 1
            if (r5 == r0) goto L_0x007d
            r0 = -1005(0xfffffffffffffc13, float:NaN)
            r1 = 4
            if (r5 == r0) goto L_0x007d
            r0 = -1003(0xfffffffffffffc15, float:NaN)
            r1 = 3
            if (r5 == r0) goto L_0x007d
            r0 = -1002(0xfffffffffffffc16, float:NaN)
            r1 = 2
            if (r5 == r0) goto L_0x007d
            r1 = 0
        L_0x007d:
            r3.A04 = r1
            goto L_0x0087
        L_0x0080:
            X.0ye r1 = r6.A0P
            X.1Pp r0 = r6.A0S
            r1.A08(r0)
        L_0x0087:
            if (r10 == 0) goto L_0x008a
            return r4
        L_0x008a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC29011Px.A00(X.0oJ, X.31D, X.2LM, boolean):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0379, code lost:
        if (X.C30041Vv.A0I(r10.A0y) == false) goto L_0x037b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03bc, code lost:
        if (r10.A0E() == null) goto L_0x03be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04f2, code lost:
        if (r0 != 0) goto L_0x04f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0137, code lost:
        if (r1 == 0) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0249, code lost:
        if (r0.booleanValue() == false) goto L_0x024b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0580  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 1712
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC29011Px.run():void");
    }
}
