package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2hR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54882hR extends AnonymousClass03U {
    public C54882hR(View view, AnonymousClass12P r13, C14900mE r14, AnonymousClass01d r15, C252018m r16) {
        super(view);
        Context context = view.getContext();
        C42971wC.A08(context, r16.A04("download-and-installation", "about-linked-devices"), r13, r14, C12970iu.A0T(view, R.id.place_holder_text), r15, C12960it.A0X(context, "learn-more", C12970iu.A1b(), 0, R.string.device_linking_no_devices_place_holder), "learn-more");
    }
}
