package X;

import java.util.Map;

/* renamed from: X.46S  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass46S extends AnonymousClass4VP {
    public final String A00;
    public final String A01;
    public final String A02;
    public final Map A03;

    public AnonymousClass46S(String str, String str2, String str3, String str4, String str5, Map map) {
        super(str, str5);
        this.A00 = str2;
        this.A03 = map;
        this.A02 = str3;
        this.A01 = str4;
    }
}
