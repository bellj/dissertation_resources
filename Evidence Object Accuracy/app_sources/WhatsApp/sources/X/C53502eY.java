package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.2eY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53502eY extends AnonymousClass04v {
    public final /* synthetic */ MessageReplyActivity A00;

    public C53502eY(MessageReplyActivity messageReplyActivity) {
        this.A00 = messageReplyActivity;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        C12970iu.A1O(r4, view.getResources().getText(R.string.send));
    }
}
