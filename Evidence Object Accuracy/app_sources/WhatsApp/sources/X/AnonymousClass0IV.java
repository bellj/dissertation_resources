package X;

/* renamed from: X.0IV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IV extends AnonymousClass032 {
    public final AnonymousClass00O mMetricsMap = new AnonymousClass00O();
    public final AnonymousClass00O mMetricsValid = new AnonymousClass00O();

    public static boolean A00(AnonymousClass00O r7, AnonymousClass00O r8) {
        boolean equals;
        if (r7 != r8) {
            int size = r7.size();
            if (size == r8.size()) {
                for (int i = 0; i < size; i++) {
                    Object[] objArr = r7.A02;
                    int i2 = i << 1;
                    Object obj = objArr[i2];
                    Object obj2 = objArr[i2 + 1];
                    Object obj3 = r8.get(obj);
                    if (obj2 != null) {
                        equals = obj2.equals(obj3);
                    } else if (obj3 == null) {
                        equals = r8.containsKey(obj);
                    }
                    if (equals) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A01(AnonymousClass032 r1) {
        A04((AnonymousClass0IV) r1);
        return this;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A02(AnonymousClass032 r8, AnonymousClass032 r9) {
        boolean z;
        Boolean bool;
        AnonymousClass0IV r82 = (AnonymousClass0IV) r8;
        AnonymousClass0IV r92 = (AnonymousClass0IV) r9;
        if (r92 != null) {
            if (r82 == null) {
                r92.A04(this);
            } else {
                int size = this.mMetricsMap.size();
                for (int i = 0; i < size; i++) {
                    Class cls = (Class) this.mMetricsMap.A02[i << 1];
                    if (!A05(cls) || !r82.A05(cls)) {
                        z = false;
                    } else {
                        z = true;
                        AnonymousClass032 A03 = r92.A03(cls);
                        if (A03 != null) {
                            A03(cls).A02(r82.A03(cls), A03);
                        }
                    }
                    AnonymousClass00O r1 = r92.mMetricsValid;
                    if (z) {
                        bool = Boolean.TRUE;
                    } else {
                        bool = Boolean.FALSE;
                    }
                    r1.put(cls, bool);
                }
            }
            return r92;
        }
        throw new IllegalArgumentException("CompositeMetrics doesn't support nullable results");
    }

    public AnonymousClass032 A03(Class cls) {
        return (AnonymousClass032) cls.cast(this.mMetricsMap.get(cls));
    }

    public void A04(AnonymousClass0IV r6) {
        AnonymousClass00O r1;
        Boolean bool;
        int size = this.mMetricsMap.size();
        for (int i = 0; i < size; i++) {
            Class cls = (Class) this.mMetricsMap.A02[i << 1];
            AnonymousClass032 A03 = r6.A03(cls);
            if (A03 != null) {
                A03(cls).A01(A03);
                boolean A05 = r6.A05(cls);
                r1 = this.mMetricsValid;
                if (A05) {
                    bool = Boolean.TRUE;
                    r1.put(cls, bool);
                }
            } else {
                r1 = this.mMetricsValid;
            }
            bool = Boolean.FALSE;
            r1.put(cls, bool);
        }
    }

    public boolean A05(Class cls) {
        Boolean bool = (Boolean) this.mMetricsValid.get(cls);
        return bool != null && bool.booleanValue();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0IV r5 = (AnonymousClass0IV) obj;
            if (!A00(this.mMetricsValid, r5.mMetricsValid) || !A00(this.mMetricsMap, r5.mMetricsMap)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.mMetricsMap.hashCode() * 31) + this.mMetricsValid.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("Composite Metrics{\n");
        int size = this.mMetricsMap.size();
        for (int i = 0; i < size; i++) {
            int i2 = i << 1;
            sb.append(this.mMetricsMap.A02[i2 + 1]);
            if (A05((Class) this.mMetricsMap.A02[i2])) {
                str = " [valid]";
            } else {
                str = " [invalid]";
            }
            sb.append(str);
            sb.append('\n');
        }
        sb.append("}");
        return sb.toString();
    }
}
