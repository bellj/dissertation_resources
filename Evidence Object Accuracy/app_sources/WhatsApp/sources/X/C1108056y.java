package X;

import android.view.View;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;

/* renamed from: X.56y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1108056y implements AnonymousClass5X1 {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ Toolbar A02;
    public final /* synthetic */ MediaAlbumActivity A03;

    @Override // X.AnonymousClass5X1
    public void APV(int i) {
    }

    @Override // X.AnonymousClass5X1
    public void AVv(View view) {
    }

    public C1108056y(View view, View view2, Toolbar toolbar, MediaAlbumActivity mediaAlbumActivity) {
        this.A03 = mediaAlbumActivity;
        this.A00 = view;
        this.A01 = view2;
        this.A02 = toolbar;
    }

    @Override // X.AnonymousClass5X1
    public void APG(View view) {
        this.A03.onBackPressed();
    }

    @Override // X.AnonymousClass5X1
    public void AWA(View view, float f) {
        float f2;
        float f3 = 1.0f - f;
        if (f3 < 0.8f) {
            f2 = 0.0f;
        } else {
            f2 = (f3 - 0.8f) / 0.19999999f;
        }
        this.A00.setAlpha(f2);
        this.A01.setAlpha(f2);
        this.A02.setAlpha(f2);
    }
}
