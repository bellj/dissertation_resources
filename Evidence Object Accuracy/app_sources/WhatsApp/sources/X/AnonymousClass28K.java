package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.28K  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28K {
    public final UserJid A00;
    public final String A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass28K) {
                AnonymousClass28K r5 = (AnonymousClass28K) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A01.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CatalogSearchPageRequest(bizJid=");
        sb.append(this.A00);
        sb.append(", searchQuery=");
        sb.append(this.A01);
        sb.append(')');
        return sb.toString();
    }

    public AnonymousClass28K(UserJid userJid, String str) {
        this.A00 = userJid;
        this.A01 = str;
    }
}
