package X;

import android.content.Context;
import android.view.OrientationEventListener;

/* renamed from: X.5a9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117555a9 extends OrientationEventListener {
    public final /* synthetic */ AnonymousClass643 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C117555a9(Context context, AnonymousClass643 r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // android.view.OrientationEventListener
    public void onOrientationChanged(int i) {
        int i2 = ((((i + 45) + 360) / 90) % 4) * 90;
        AnonymousClass643 r2 = this.A00;
        int A04 = r2.A04();
        if (r2.A03 != i2 || r2.A04 != A04) {
            r2.A03 = i2;
            r2.A0N.ATI(i2);
            r2.A0H(r2.A08);
        }
    }
}
