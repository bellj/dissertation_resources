package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.5MC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MC extends AnonymousClass5NC {
    public AnonymousClass5MC(AnonymousClass5NG r1, AnonymousClass1TK r2, AnonymousClass1TL r3, AnonymousClass1TL r4, int i) {
        super(r1, r2, r3, r4, i);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return A01().length;
    }

    @Override // X.AnonymousClass5NC, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r7, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        AnonymousClass5NC.A01(byteArrayOutputStream, "DER", this.A02);
        AnonymousClass5NC.A01(byteArrayOutputStream, "DER", this.A01);
        AnonymousClass5NC.A01(byteArrayOutputStream, "DER", this.A03);
        byteArrayOutputStream.write(new C114835Ng(this.A04, this.A00, true).A02("DER"));
        r7.A05(byteArrayOutputStream.toByteArray(), 32, 8, z);
    }
}
