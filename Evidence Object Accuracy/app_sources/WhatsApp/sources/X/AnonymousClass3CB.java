package X;

/* renamed from: X.3CB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CB {
    public AnonymousClass4EZ A00;

    public AnonymousClass3CB(AnonymousClass4EZ r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C93614aS A00(android.os.Bundle r9) {
        /*
            r8 = this;
            java.lang.String r0 = "error_code"
            r2 = 1
            int r7 = r9.getInt(r0, r2)
            java.lang.String r1 = "exception"
            android.os.Parcelable r0 = r9.getParcelable(r1)
            android.os.Bundle r0 = (android.os.Bundle) r0
            r6 = 0
            if (r0 == 0) goto L_0x0024
            java.io.Serializable r0 = r0.getSerializable(r1)     // Catch: all -> 0x0027
            java.lang.Throwable r0 = (java.lang.Throwable) r0     // Catch: all -> 0x0027
            if (r0 == 0) goto L_0x001d
            X.4AN r1 = X.AnonymousClass4AN.Deserialized     // Catch: all -> 0x0021
            goto L_0x001f
        L_0x001d:
            X.4AN r1 = X.AnonymousClass4AN.NotIncluded     // Catch: all -> 0x0021
        L_0x001f:
            r6 = r0
            goto L_0x002f
        L_0x0021:
            r1 = move-exception
            r6 = r0
            goto L_0x0028
        L_0x0024:
            X.4AN r1 = X.AnonymousClass4AN.NotIncluded
            goto L_0x002f
        L_0x0027:
            r1 = move-exception
        L_0x0028:
            java.lang.String r0 = "AutoconfManagerConsumerImpl/preload/feo2/soft_error"
            com.whatsapp.util.Log.e(r0, r1)
            X.4AN r1 = X.AnonymousClass4AN.DeserializationFailed
        L_0x002f:
            X.4AN r0 = X.AnonymousClass4AN.NotIncluded
            if (r1 != r0) goto L_0x0039
            java.lang.String r0 = "serialization_result"
            r9.getBoolean(r0, r2)
        L_0x0039:
            java.lang.String r0 = "stringified_exception"
            java.lang.String r5 = r9.getString(r0)
            java.lang.String r0 = "exception_hierarchies"
            java.util.ArrayList r0 = r9.getStringArrayList(r0)
            X.29A r4 = X.AnonymousClass1Mr.builder()
            if (r0 == 0) goto L_0x0076
            X.29A r3 = X.AnonymousClass1Mr.builder()
            java.util.Iterator r2 = r0.iterator()
        L_0x0054:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = X.C12970iu.A0x(r2)
            java.lang.String r0 = "--"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0072
            X.1Mr r0 = r3.build()
            r4.add(r0)
            X.29A r3 = X.AnonymousClass1Mr.builder()
            goto L_0x0054
        L_0x0072:
            r3.add(r1)
            goto L_0x0054
        L_0x0076:
            X.1Mr r1 = r4.build()
            X.4aS r0 = new X.4aS
            r0.<init>(r1, r5, r6, r7)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3CB.A00(android.os.Bundle):X.4aS");
    }
}
