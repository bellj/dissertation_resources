package X;

import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import org.json.JSONObject;

/* renamed from: X.33H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33H extends AbstractC454821u {
    public final Matrix A00;
    public final Paint A01;
    public final Path A02;
    public final Path A03;

    public AnonymousClass33H() {
        Path path = new Path();
        this.A02 = path;
        this.A01 = C12960it.A0A();
        this.A00 = C13000ix.A01();
        this.A03 = new Path();
        double radians = Math.toRadians(35.0d);
        float cos = (float) (Math.cos(radians) * 1000.0d);
        float sin = (float) (Math.sin(radians) * 1000.0d);
        double radians2 = Math.toRadians(55.0d);
        path.addArc(new RectF(-1000.0f, -1000.0f, 1000.0f, 1000.0f), 55.0f, 340.0f);
        path.moveTo(cos, sin);
        path.lineTo(1200.0f, 1200.0f);
        path.lineTo((float) (Math.cos(radians2) * 1000.0d), (float) (Math.sin(radians2) * 1000.0d));
    }

    public AnonymousClass33H(JSONObject jSONObject) {
        this();
        super.A0A(jSONObject);
    }

    @Override // X.AbstractC454821u
    public void A0O(float f) {
        super.A0O((f * 2.0f) / 3.0f);
    }

    @Override // X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        float f5 = (f2 + f4) / 2.0f;
        float f6 = (((f3 - f) * 2.0f) / 3.0f) / 2.0f;
        super.A0Q(rectF, f, f5 - f6, f3, f5 + f6);
    }
}
