package X;

import java.io.FileOutputStream;

/* renamed from: X.01o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C003601o {
    public static volatile C19350ty A00;

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Try blocks wrapping queue limit reached! Please report as an issue!
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.connectExcHandlers(BlockExceptionHandler.java:88)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.process(BlockExceptionHandler.java:58)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.independentBlockTreeMod(BlockProcessor.java:452)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:51)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public static void A00(android.content.Context r2, java.lang.Throwable r3) {
        /*
            java.io.File r2 = r2.getFilesDir()     // Catch: Exception -> 0x001f, all -> 0x002f
            java.lang.String r1 = "crash_sentinel"
            java.io.File r0 = new java.io.File     // Catch: Exception -> 0x001f, all -> 0x002f
            r0.<init>(r2, r1)     // Catch: Exception -> 0x001f, all -> 0x002f
            r0.createNewFile()     // Catch: Exception -> 0x001f, all -> 0x002f
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch: Exception -> 0x001f, all -> 0x002f
            r1.<init>(r0)     // Catch: Exception -> 0x001f, all -> 0x002f
            A02(r1, r3)     // Catch: all -> 0x001a
            r1.close()     // Catch: Exception -> 0x001f, all -> 0x002f
            goto L_0x0025
        L_0x001a:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x001e
        L_0x001e:
            throw r0     // Catch: Exception -> 0x001f, all -> 0x002f
        L_0x001f:
            r1 = move-exception
            java.lang.String r0 = "Unable to create crash sentinel file"
            com.whatsapp.util.Log.e(r0, r1)     // Catch: all -> 0x002f
        L_0x0025:
            X.0ty r0 = X.C003601o.A00
            if (r0 == 0) goto L_0x002e
            X.0ty r0 = X.C003601o.A00
            r0.A02()
        L_0x002e:
            return
        L_0x002f:
            r1 = move-exception
            X.0ty r0 = X.C003601o.A00
            if (r0 == 0) goto L_0x0039
            X.0ty r0 = X.C003601o.A00
            r0.A02()
        L_0x0039:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C003601o.A00(android.content.Context, java.lang.Throwable):void");
    }

    public static void A01(C19350ty r0) {
        A00 = r0;
    }

    public static void A02(FileOutputStream fileOutputStream, Throwable th) {
        fileOutputStream.write(AnonymousClass1OA.A00(th).A01().getBytes());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        r2.A05(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(java.lang.Thread.UncaughtExceptionHandler r3, java.lang.Thread r4, java.lang.Throwable r5) {
        /*
            java.lang.String r0 = "UNCAUGHT EXCEPTION"
            com.whatsapp.util.Log.e(r0, r5)     // Catch: Exception -> 0x0018, all -> 0x001f
            X.0ty r2 = X.C003601o.A00     // Catch: Exception -> 0x0018, all -> 0x001f
            if (r2 == 0) goto L_0x0018
            r1 = r5
        L_0x000a:
            boolean r0 = r1 instanceof java.lang.OutOfMemoryError     // Catch: Exception -> 0x0018, all -> 0x001f
            if (r0 == 0) goto L_0x0012
            r2.A05(r5)     // Catch: Exception -> 0x0018, all -> 0x001f
            goto L_0x0018
        L_0x0012:
            java.lang.Throwable r1 = r1.getCause()     // Catch: Exception -> 0x0018, all -> 0x001f
            if (r1 != 0) goto L_0x000a
        L_0x0018:
            com.whatsapp.util.Log.flush()
            r3.uncaughtException(r4, r5)
            return
        L_0x001f:
            r0 = move-exception
            com.whatsapp.util.Log.flush()
            r3.uncaughtException(r4, r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C003601o.A03(java.lang.Thread$UncaughtExceptionHandler, java.lang.Thread, java.lang.Throwable):void");
    }
}
