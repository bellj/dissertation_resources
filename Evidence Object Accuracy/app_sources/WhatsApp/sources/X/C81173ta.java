package X;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/* renamed from: X.3ta  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81173ta extends AnonymousClass4XX {
    public final /* synthetic */ Comparator val$comparator;

    public C81173ta(Comparator comparator) {
        this.val$comparator = comparator;
    }

    @Override // X.AnonymousClass4XX
    public Map createMap() {
        return new TreeMap(this.val$comparator);
    }
}
