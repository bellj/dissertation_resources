package X;

import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.6Kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135866Kc implements Callable {
    public final /* synthetic */ C129455xk A00;
    public final /* synthetic */ AnonymousClass661 A01;
    public final /* synthetic */ AnonymousClass60B A02;

    public CallableC135866Kc(C129455xk r1, AnonymousClass661 r2, AnonymousClass60B r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        Camera.ShutterCallback shutterCallback;
        AnonymousClass63C r1;
        AnonymousClass661 r4 = this.A01;
        C117305Zk.A1C(r4.A05());
        int A03 = r4.A0J.A03(r4.A00, r4.A0X);
        C129885yS r5 = r4.A0R;
        C119105ct A00 = r5.A00(r4.A00);
        AbstractC125485rK.A02(AbstractC130685zo.A0c, A00, A03);
        A00.A02();
        Rect rect = (Rect) r5.A02(r4.A00).A03(AbstractC130685zo.A0l);
        int A05 = C12960it.A05(r5.A02(r4.A00).A03(AbstractC130685zo.A0o));
        r4.A0T.A05(new AnonymousClass6FH(this), r4.A0S.A03);
        AnonymousClass60B r7 = this.A02;
        if (C12970iu.A1Y(r7.A00(AnonymousClass60B.A06))) {
            shutterCallback = null;
        } else {
            shutterCallback = AnonymousClass661.A0g;
        }
        C129755yF r11 = new C129755yF((Rect) r5.A02(r4.A00).A03(AbstractC130685zo.A0f), rect, A03, r4.A00);
        if (C12970iu.A1Y(r7.A00(AnonymousClass60B.A07))) {
            r1 = new AnonymousClass63C(this, r11);
        } else {
            ((CountDownLatch) r4.A0N.A00.get()).countDown();
            r1 = null;
        }
        r4.A0Y.takePicture(shutterCallback, null, r1, new AnonymousClass63E(rect, this, r11, A03, A05));
        C129465xl r2 = r4.A0L.A00;
        ReentrantLock reentrantLock = r2.A01;
        reentrantLock.lock();
        try {
            r2.A00 = 0;
            reentrantLock.unlock();
            AnonymousClass61K.A01("Some how photo taking call is happening on the UI Thread!!");
            try {
                ((CountDownLatch) r4.A0N.A00.get()).await(10000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Log.e("Camera1Device", "Interrupted while waiting on Camera.takePicture", e);
            }
            C128435w6 r8 = r4.A0N;
            if (((CountDownLatch) r8.A00.get()).getCount() <= 0) {
                boolean A1Y = C12970iu.A1Y(r7.A00(AnonymousClass60B.A08));
                AnonymousClass61K.A01("Performing post photo capture on UI thread");
                if (r4.isConnected()) {
                    if (A1Y) {
                        r4.A07();
                    }
                    r8.A01(0);
                }
                return null;
            }
            r4.A0W.set(true);
            throw new C136086Ky();
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }
}
