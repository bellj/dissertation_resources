package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC30851Zb extends AnonymousClass1ZY {
    public long A00;
    public AnonymousClass1ZR A01;
    public AnonymousClass1ZR A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public byte[] A09;

    public String A0B() {
        Object obj;
        AnonymousClass1ZR r0 = this.A01;
        if (r0 == null) {
            obj = null;
        } else {
            obj = r0.A00;
        }
        return (String) obj;
    }

    public JSONObject A0C() {
        JSONObject jSONObject = new JSONObject();
        try {
            String str = this.A03;
            if (str != null) {
                jSONObject.put("bankImageURL", str);
            }
            String str2 = this.A04;
            if (str2 != null) {
                jSONObject.put("bankPhoneNumber", str2);
                return jSONObject;
            }
        } catch (JSONException e) {
            Log.w("PAY: PaymentMethodBankAccountCountryData toJSONObject threw: ", e);
        }
        return jSONObject;
    }

    public void A0D(JSONObject jSONObject) {
        this.A03 = jSONObject.optString("bankImageURL", null);
        this.A04 = jSONObject.optString("bankPhoneNumber", null);
    }
}
