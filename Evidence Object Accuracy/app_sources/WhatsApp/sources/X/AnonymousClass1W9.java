package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.Jid;

/* renamed from: X.1W9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1W9 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100364ln();
    public final byte A00;
    public final Jid A01;
    public final String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1W9(Parcel parcel) {
        String readString;
        Jid jid;
        this.A02 = parcel.readString();
        byte readByte = parcel.readByte();
        this.A00 = readByte;
        if (readByte == 1) {
            jid = (Jid) parcel.readParcelable(Jid.class.getClassLoader());
            readString = jid.getRawString();
        } else {
            readString = parcel.readString();
            jid = null;
        }
        this.A03 = readString;
        this.A01 = jid;
    }

    public AnonymousClass1W9(Jid jid, String str) {
        this(jid, str, jid.getRawString(), (byte) 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r4.getType() == 8) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1W9(com.whatsapp.jid.Jid r4, java.lang.String r5, java.lang.String r6, byte r7) {
        /*
            r3 = this;
            r3.<init>()
            if (r4 == 0) goto L_0x002f
            boolean r0 = r4.isProtocolCompliant()
            if (r0 != 0) goto L_0x0014
            int r1 = r4.getType()
            r0 = 8
            r2 = 0
            if (r1 != r0) goto L_0x0015
        L_0x0014:
            r2 = 1
        L_0x0015:
            java.lang.String r0 = "Jid: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r4.getRawString()
            r1.append(r0)
            java.lang.String r0 = " is not protocol compliant"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            X.AnonymousClass009.A0B(r0, r2)
        L_0x002f:
            X.AnonymousClass009.A05(r5)
            r3.A02 = r5
            X.AnonymousClass009.A05(r6)
            r3.A03 = r6
            r3.A01 = r4
            r3.A00 = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1W9.<init>(com.whatsapp.jid.Jid, java.lang.String, java.lang.String, byte):void");
    }

    public AnonymousClass1W9(String str, int i) {
        this(str, String.valueOf(i));
    }

    public AnonymousClass1W9(String str, long j) {
        this(str, String.valueOf(j));
    }

    public AnonymousClass1W9(String str, String str2) {
        this(null, str, str2, (byte) 0);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass1W9.class != obj.getClass()) {
                return false;
            }
            AnonymousClass1W9 r5 = (AnonymousClass1W9) obj;
            if (!this.A02.equals(r5.A02) || !this.A03.equals(r5.A03)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((this.A02.hashCode() + 31) * 31) + this.A03.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("KeyValue{key='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", value='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", type='");
        sb.append((int) this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        byte b = this.A00;
        parcel.writeByte(b);
        if (b == 1) {
            parcel.writeParcelable(this.A01, i);
        } else {
            parcel.writeString(this.A03);
        }
    }
}
