package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3ZV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZV implements AbstractC116755Wr {
    public final /* synthetic */ AnonymousClass2A9 A00;

    public AnonymousClass3ZV(AnonymousClass2A9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116755Wr
    public void APl(int i) {
        AnonymousClass2A9 r1 = this.A00;
        AnonymousClass2A9.A00(r1, r1.A0H.decrementAndGet());
    }

    @Override // X.AbstractC116755Wr
    public void ARp(Set set) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            if (-1 == C12960it.A05(pair.second)) {
                Object obj = pair.first;
                AnonymousClass009.A05(obj);
                A0l.add(obj);
            }
        }
        Iterator it2 = A0l.iterator();
        while (it2.hasNext()) {
            this.A00.A0G.remove(it2.next());
        }
        AnonymousClass2A9 r1 = this.A00;
        AnonymousClass2A9.A00(r1, r1.A0H.decrementAndGet());
    }

    @Override // X.AbstractC116755Wr
    public void AXX() {
        AnonymousClass2A9 r1 = this.A00;
        AnonymousClass2A9.A00(r1, r1.A0H.decrementAndGet());
    }
}
