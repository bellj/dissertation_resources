package X;

import android.util.JsonReader;
import java.io.IOException;
import java.io.StringReader;

/* renamed from: X.3Gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64573Gb {
    public C89084Ip A00;
    public String A01;

    public static C63433Bo A00(String str) {
        C89074Io r0;
        try {
            C65093Ic.A00();
            C67883Tg r02 = new C67883Tg(new JsonReader(new StringReader(str)));
            r02.ALh();
            C89084Ip A00 = AnonymousClass3AF.A00(r02);
            if (A00 != null && (r0 = A00.A00) != null) {
                return r0.A00;
            }
            throw C12970iu.A0f("Encountered empty BloksResponse. Could not parse embedded payload");
        } catch (IOException unused) {
            throw C12970iu.A0f("Could not parse embedded payload");
        }
    }
}
