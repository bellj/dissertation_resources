package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34321fx {
    public final int A00;
    public final int A01;
    public final Set A02;

    public C34321fx(Set set, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A02 = Collections.unmodifiableSet(set);
    }

    public static C34321fx A00(C34311fw r4) {
        int i = r4.A00;
        if ((i & 1) != 1 || (i & 2) != 2 || r4.A04.size() == 0) {
            return null;
        }
        return new C34321fx(new HashSet(r4.A04), r4.A03, r4.A01);
    }

    public C34311fw A01() {
        AnonymousClass1G4 A0T = C34311fw.A05.A0T();
        int i = this.A01;
        A0T.A03();
        C34311fw r1 = (C34311fw) A0T.A00;
        r1.A00 |= 1;
        r1.A03 = i;
        int i2 = this.A00;
        A0T.A03();
        C34311fw r12 = (C34311fw) A0T.A00;
        r12.A00 |= 2;
        r12.A01 = i2;
        Set set = this.A02;
        A0T.A03();
        C34311fw r2 = (C34311fw) A0T.A00;
        AbstractC41941uP r13 = r2.A04;
        if (!((AnonymousClass1K7) r13).A00) {
            r13 = AbstractC27091Fz.A0F(r13);
            r2.A04 = r13;
        }
        AnonymousClass1G5.A01(set, r13);
        return (C34311fw) A0T.A02();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C34321fx)) {
            return false;
        }
        C34321fx r4 = (C34321fx) obj;
        if (this.A01 == r4.A01 && this.A00 == r4.A00 && this.A02.equals(r4.A02)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A01), Integer.valueOf(this.A00), this.A02});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdKeyFingerprint{");
        sb.append("rawId=");
        sb.append(this.A01);
        sb.append(", currentIndex=");
        sb.append(this.A00);
        sb.append(", deviceIndexes=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
