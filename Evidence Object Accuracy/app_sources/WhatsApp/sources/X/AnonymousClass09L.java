package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09L  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09L extends AnimatorListenerAdapter {
    public final /* synthetic */ C08790br A00;
    public final /* synthetic */ C14260l7 A01;
    public final /* synthetic */ String A02;

    public AnonymousClass09L(C08790br r1, C14260l7 r2, String str) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = str;
    }

    public final void A00() {
        AnonymousClass3JV.A08(this.A01, this.A02);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        A00();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        A00();
    }
}
