package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* renamed from: X.4Qf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91044Qf {
    public SensorEventListener A00;
    public final Sensor A01;
    public final SensorManager A02;

    public C91044Qf(AnonymousClass01d r3) {
        SensorManager A0D = r3.A0D();
        this.A02 = A0D;
        this.A01 = A0D.getDefaultSensor(8);
    }
}
