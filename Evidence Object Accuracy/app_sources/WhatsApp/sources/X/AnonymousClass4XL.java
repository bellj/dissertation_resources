package X;

/* renamed from: X.4XL  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XL {
    public final AnonymousClass4WP A00;
    public final AnonymousClass4XK A01;
    public final AnonymousClass4XB A02;
    public final AnonymousClass4WF A03;
    public final String A04;

    public /* synthetic */ AnonymousClass4XL(AnonymousClass4WP r1, AnonymousClass4XK r2, AnonymousClass4XB r3, AnonymousClass4WF r4, String str) {
        this.A04 = str;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass4XL)) {
            return false;
        }
        AnonymousClass4XL r4 = (AnonymousClass4XL) obj;
        if (!AnonymousClass3JZ.A0H(this.A04, r4.A04) || !this.A00.equals(r4.A00) || !AnonymousClass3JZ.A0H(this.A02, r4.A02) || !AnonymousClass3JZ.A0H(this.A01, r4.A01) || !AnonymousClass3JZ.A0H(this.A03, r4.A03)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int hashCode = this.A04.hashCode() * 31;
        AnonymousClass4XB r0 = this.A02;
        if (r0 != null) {
            i = r0.hashCode();
        } else {
            i = 0;
        }
        return C12990iw.A08(this.A03, C12990iw.A08(this.A00, C12990iw.A08(this.A01, (hashCode + i) * 31) * 31) * 31);
    }
}
