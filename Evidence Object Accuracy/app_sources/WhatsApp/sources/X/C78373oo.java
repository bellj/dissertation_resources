package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78373oo extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98564it();
    public long A00;
    public boolean A01;
    public final int A02;
    public final boolean A03;

    public C78373oo(int i, long j, boolean z, boolean z2) {
        this.A02 = i;
        this.A01 = z;
        this.A00 = j;
        this.A03 = z2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A02);
        C95654e8.A09(parcel, 2, this.A01);
        C95654e8.A08(parcel, 3, this.A00);
        C95654e8.A09(parcel, 4, this.A03);
        C95654e8.A06(parcel, A00);
    }
}
