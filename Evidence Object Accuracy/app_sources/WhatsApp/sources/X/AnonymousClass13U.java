package X;

import android.os.Handler;
import android.os.Looper;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;

/* renamed from: X.13U  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass13U implements AbstractC15920o8 {
    public AnonymousClass1MO A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final C22920zr A02;
    public final AnonymousClass101 A03;
    public final AnonymousClass13T A04;
    public final C237112s A05;
    public final C14820m6 A06;
    public final C15990oG A07;
    public final C18240s8 A08;
    public final C22130yZ A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass13U(C22920zr r3, AnonymousClass101 r4, AnonymousClass13T r5, C237112s r6, C14820m6 r7, C15990oG r8, C18240s8 r9, C22130yZ r10, AbstractC14440lR r11) {
        this.A0A = r11;
        this.A08 = r9;
        this.A02 = r3;
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = r7;
        this.A09 = r10;
        this.A03 = r4;
        this.A05 = r6;
    }

    public final void A00(DeviceJid deviceJid) {
        StringBuilder sb = new StringBuilder("Prekey request returned none or signature invalid; jid=");
        sb.append(deviceJid);
        Log.i(sb.toString());
        this.A07.A0U(null, C15940oB.A02(deviceJid));
        AnonymousClass101 r2 = this.A03;
        synchronized (r2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("prekeysmanager/onGetPreKeyNone:");
            sb2.append(deviceJid);
            Log.i(sb2.toString());
            r2.A08.remove(deviceJid);
        }
        if (deviceJid.device != 0) {
            this.A09.A09(deviceJid, true);
        }
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{74, 75, 76, 77, 78, 196, 82, 83, 84};
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:97:0x0035 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v0, types: [byte[][]] */
    /* JADX WARN: Type inference failed for: r11v1, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r11v2 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC15920o8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AI8(android.os.Message r15, int r16) {
        /*
        // Method dump skipped, instructions count: 608
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13U.AI8(android.os.Message, int):boolean");
    }
}
