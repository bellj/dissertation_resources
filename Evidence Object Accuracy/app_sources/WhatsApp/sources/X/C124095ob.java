package X;

import android.util.Pair;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124095ob extends AbstractC16350or {
    public WeakReference A00;
    public final C20370ve A01;

    public /* synthetic */ C124095ob(C20370ve r2, C117935b0 r3) {
        this.A01 = r2;
        this.A00 = C12970iu.A10(r3);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        Integer[] numArr = new Integer[1];
        C12960it.A1P(numArr, 300, 0);
        return this.A01.A0b(new Integer[0], numArr, 0);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        List list = (List) obj;
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null) {
            C117935b0 r5 = (C117935b0) weakReference.get();
            C12990iw.A1J(r5.A00, false);
            C12990iw.A1J(r5.A01, true);
            C129795yJ r3 = r5.A07;
            ArrayList A0l = C12960it.A0l();
            Iterator it = list.iterator();
            C122245l9 r7 = null;
            while (it.hasNext()) {
                C122245l9 A00 = r3.A00(C117315Zl.A09(it).A06);
                if (r7 != null) {
                    if (r7.get(2) == A00.get(2) && r7.get(1) == A00.get(1)) {
                        r7.count++;
                    } else {
                        A0l.add(r7);
                    }
                }
                A00.count = 0;
                r7 = A00;
                r7.count++;
            }
            if (r7 != null) {
                A0l.add(r7);
            }
            ArrayList A0l2 = C12960it.A0l();
            for (int i = 0; i < list.size(); i++) {
                AnonymousClass1IR r9 = (AnonymousClass1IR) list.get(i);
                C123175mj r72 = new C123175mj();
                r72.A01 = AnonymousClass1MY.A04(r5.A05, r5.A04.A02(r9.A06));
                r72.A00 = r5.A08.A0I(r9);
                boolean z = true;
                if (i < list.size() - 1) {
                    C122245l9 A002 = r3.A00(r9.A06);
                    C122245l9 A003 = r3.A00(((AnonymousClass1IR) list.get(i + 1)).A06);
                    if (A002.get(2) != A003.get(2) || A002.get(1) != A003.get(1)) {
                        z = false;
                    }
                }
                r72.A02 = z;
                A0l2.add(r72);
            }
            r5.A02.A0B(Pair.create(A0l2, A0l));
        }
    }
}
