package X;

import android.app.Activity;
import com.whatsapp.emoji.search.EmojiSearchContainer;

/* renamed from: X.4Tq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C91894Tq {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass018 A01;
    public final /* synthetic */ AnonymousClass19M A02;
    public final /* synthetic */ C15270mq A03;
    public final /* synthetic */ C231510o A04;
    public final /* synthetic */ EmojiSearchContainer A05;
    public final /* synthetic */ C15330mx A06;
    public final /* synthetic */ C16630pM A07;

    public /* synthetic */ C91894Tq(Activity activity, AnonymousClass018 r2, AnonymousClass19M r3, C15270mq r4, C231510o r5, EmojiSearchContainer emojiSearchContainer, C15330mx r7, C16630pM r8) {
        this.A06 = r7;
        this.A03 = r4;
        this.A05 = emojiSearchContainer;
        this.A00 = activity;
        this.A02 = r3;
        this.A04 = r5;
        this.A01 = r2;
        this.A07 = r8;
    }
}
