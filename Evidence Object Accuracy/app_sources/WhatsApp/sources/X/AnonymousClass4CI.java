package X;

/* renamed from: X.4CI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CI extends Exception {
    public Throwable cause;

    public AnonymousClass4CI() {
    }

    public AnonymousClass4CI(String str) {
        super(str);
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
