package X;

import java.util.Arrays;

/* renamed from: X.07i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C015507i {
    public static int A00(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static boolean A01(Object obj, Object obj2) {
        return AnonymousClass08r.A00(obj, obj2);
    }
}
