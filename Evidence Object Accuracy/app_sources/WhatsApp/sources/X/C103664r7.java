package X;

import android.content.Context;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;

/* renamed from: X.4r7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103664r7 implements AbstractC009204q {
    public final /* synthetic */ ViewSharedContactArrayActivity A00;

    public C103664r7(ViewSharedContactArrayActivity viewSharedContactArrayActivity) {
        this.A00 = viewSharedContactArrayActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
