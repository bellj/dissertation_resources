package X;

import com.whatsapp.util.Log;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68933Xh implements AbstractC28461Nh {
    public final /* synthetic */ C63333Be A00;
    public final /* synthetic */ C63383Bj A01;

    public C68933Xh(C63333Be r1, C63383Bj r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
        this.A01.A04.A0E.A04(str);
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
        this.A01.A04.A09(j);
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("mediaupload/error = ")));
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        C63383Bj r2 = this.A01;
        r2.A00 = new AnonymousClass4TF();
        if (!this.A00.A07) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                r2.A00.A05 = A05.optString("url");
                r2.A00.A02 = A05.optString("handle");
                r2.A00.A00 = A05.optString("direct_path");
                r2.A00.A03 = A05.optString("meta_hmac");
                r2.A00.A01 = A05.optString("fbid");
                r2.A00.A04 = A05.optString("ts");
            } catch (JSONException e) {
                Log.e("mediaupload/jsonexception", e);
            }
        }
        r2.A01 = true;
    }
}
