package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.2kD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56032kD extends AbstractC56052kF {
    public C67683Sl A00;
    public C56072kH A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass4YJ A05;
    public final C94404bl A06;
    public final AnonymousClass2CD A07;
    public final boolean A08;

    public C56032kD(AnonymousClass2CD r6, boolean z) {
        boolean z2;
        Timeline r2;
        this.A07 = r6;
        if (!z || (r6 instanceof C56022kC)) {
            z2 = false;
        } else {
            z2 = true;
        }
        this.A08 = z2;
        this.A06 = new C94404bl();
        this.A05 = new AnonymousClass4YJ();
        if (!(r6 instanceof C56022kC)) {
            this.A01 = new C56072kH(new C76603lu(r6.AED()), C94404bl.A0F, C56072kH.A02);
            return;
        }
        C56022kC r62 = (C56022kC) r6;
        int i = r62.A00;
        C56072kH r0 = r62.A01.A01;
        if (i != Integer.MAX_VALUE) {
            r2 = new C76493lj(r0, i);
        } else {
            r2 = new C77163mq(r0);
        }
        this.A01 = new C56072kH(r2, null, null);
        this.A02 = true;
    }

    @Override // X.AbstractC56052kF, X.AbstractC67703Sn
    public void A00() {
        this.A04 = false;
        this.A03 = false;
        super.A00();
    }

    @Override // X.AbstractC56052kF, X.AbstractC67703Sn
    public void A02(AnonymousClass5QP r2) {
        super.A02(r2);
        if (!this.A08) {
            this.A03 = true;
            A04(this.A07);
        }
    }

    /* renamed from: A05 */
    public C67683Sl A8S(C28741Ov r4, AnonymousClass5VZ r5, long j) {
        C67683Sl r2 = new C67683Sl(r4, r5, j);
        AnonymousClass2CD r1 = this.A07;
        C95314dV.A04(C12980iv.A1X(r2.A03));
        r2.A03 = r1;
        if (this.A04) {
            Object obj = r4.A04;
            if (this.A01.A00 != null && obj.equals(C56072kH.A02)) {
                obj = this.A01.A00;
            }
            r2.A00(r4.A01(obj));
        } else {
            this.A00 = r2;
            if (!this.A03) {
                this.A03 = true;
                A04(r1);
                return r2;
            }
        }
        return r2;
    }

    public final void A06(long j) {
        C67683Sl r6 = this.A00;
        int A04 = this.A01.A04(r6.A05.A04);
        if (A04 != -1) {
            long j2 = this.A01.A09(this.A05, A04, false).A01;
            if (j2 != -9223372036854775807L && j >= j2) {
                j = Math.max(0L, j2 - 1);
            }
            r6.A00 = j;
        }
    }

    @Override // X.AnonymousClass2CD
    public AnonymousClass4XL AED() {
        return this.A07.AED();
    }

    @Override // X.AnonymousClass2CD
    public void Aa9(AbstractC14080kp r3) {
        C67683Sl r0 = (C67683Sl) r3;
        AbstractC14080kp r1 = r0.A02;
        if (r1 != null) {
            r0.A03.Aa9(r1);
        }
        if (r3 == this.A00) {
            this.A00 = null;
        }
    }
}
