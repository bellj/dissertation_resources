package X;

import java.lang.reflect.InvocationTargetException;

/* renamed from: X.05f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C010605f {
    public static final AnonymousClass00O A01 = new AnonymousClass00O();
    public final /* synthetic */ AnonymousClass01F A00;

    public C010605f(AnonymousClass01F r1) {
        this.A00 = r1;
    }

    public AnonymousClass01E A00(String str) {
        try {
            ClassLoader classLoader = this.A00.A07.A01.getClassLoader();
            try {
                AnonymousClass00O r0 = A01;
                AnonymousClass00O r2 = (AnonymousClass00O) r0.get(classLoader);
                if (r2 == null) {
                    r2 = new AnonymousClass00O();
                    r0.put(classLoader, r2);
                }
                Class<?> cls = (Class) r2.get(str);
                if (cls == null) {
                    cls = Class.forName(str, false, classLoader);
                    r2.put(str, cls);
                }
                return (AnonymousClass01E) cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassCastException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to instantiate fragment ");
                sb.append(str);
                sb.append(": make sure class is a valid subclass of Fragment");
                throw new AnonymousClass0f8(sb.toString(), e);
            } catch (ClassNotFoundException e2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to instantiate fragment ");
                sb2.append(str);
                sb2.append(": make sure class name exists");
                throw new AnonymousClass0f8(sb2.toString(), e2);
            }
        } catch (IllegalAccessException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to instantiate fragment ");
            sb3.append(str);
            sb3.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new AnonymousClass0f8(sb3.toString(), e3);
        } catch (InstantiationException e4) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Unable to instantiate fragment ");
            sb4.append(str);
            sb4.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new AnonymousClass0f8(sb4.toString(), e4);
        } catch (NoSuchMethodException e5) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Unable to instantiate fragment ");
            sb5.append(str);
            sb5.append(": could not find Fragment constructor");
            throw new AnonymousClass0f8(sb5.toString(), e5);
        } catch (InvocationTargetException e6) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Unable to instantiate fragment ");
            sb6.append(str);
            sb6.append(": calling Fragment constructor caused an exception");
            throw new AnonymousClass0f8(sb6.toString(), e6);
        }
    }
}
