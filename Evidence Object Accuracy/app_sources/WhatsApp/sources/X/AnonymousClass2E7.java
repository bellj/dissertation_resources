package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.2E7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2E7 {
    public long A00;
    public String[] A01;
    public final AbstractC14640lm A02;
    public final DeviceJid A03;
    public final AnonymousClass1IS A04;
    public final String A05;

    public AnonymousClass2E7(AbstractC14640lm r1, DeviceJid deviceJid, AnonymousClass1IS r3, String str) {
        this.A04 = r3;
        this.A02 = r1;
        this.A03 = deviceJid;
        this.A05 = str;
    }
}
