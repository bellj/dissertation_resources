package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2RJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2RJ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2RJ A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public long A01;
    public AnonymousClass1G8 A02;
    public String A03 = "";
    public String A04 = "";

    static {
        AnonymousClass2RJ r0 = new AnonymousClass2RJ();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        AnonymousClass1G9 r1;
        switch (r16.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass2RJ r2 = (AnonymousClass2RJ) obj2;
                this.A02 = (AnonymousClass1G8) r8.Aft(this.A02, r2.A02);
                int i = this.A00;
                boolean z = false;
                if ((i & 2) == 2) {
                    z = true;
                }
                String str = this.A04;
                int i2 = r2.A00;
                boolean z2 = false;
                if ((i2 & 2) == 2) {
                    z2 = true;
                }
                this.A04 = r8.Afy(str, r2.A04, z, z2);
                boolean z3 = false;
                if ((i & 4) == 4) {
                    z3 = true;
                }
                String str2 = this.A03;
                boolean z4 = false;
                if ((i2 & 4) == 4) {
                    z4 = true;
                }
                this.A03 = r8.Afy(str2, r2.A03, z3, z4);
                boolean z5 = false;
                if ((i & 8) == 8) {
                    z5 = true;
                }
                long j = this.A01;
                boolean z6 = false;
                if ((i2 & 8) == 8) {
                    z6 = true;
                }
                this.A01 = r8.Afs(j, r2.A01, z5, z6);
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            if ((this.A00 & 1) == 1) {
                                r1 = (AnonymousClass1G9) this.A02.A0T();
                            } else {
                                r1 = null;
                            }
                            AnonymousClass1G8 r0 = (AnonymousClass1G8) r82.A09(r22, AnonymousClass1G8.A05.A0U());
                            this.A02 = r0;
                            if (r1 != null) {
                                r1.A04(r0);
                                this.A02 = (AnonymousClass1G8) r1.A01();
                            }
                            this.A00 |= 1;
                        } else if (A03 == 18) {
                            String A0A = r82.A0A();
                            this.A00 |= 2;
                            this.A04 = A0A;
                        } else if (A03 == 26) {
                            String A0A2 = r82.A0A();
                            this.A00 |= 4;
                            this.A03 = A0A2;
                        } else if (A03 == 32) {
                            this.A00 |= 8;
                            this.A01 = r82.A06();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2RJ();
            case 5:
                return new C82283vN();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (AnonymousClass2RJ.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A03);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A05(4, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A03);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
