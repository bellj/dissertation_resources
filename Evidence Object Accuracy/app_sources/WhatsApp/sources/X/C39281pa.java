package X;

import java.io.OutputStream;

/* renamed from: X.1pa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39281pa extends OutputStream {
    public long A00;
    public final int A01;
    public final OutputStream A02;

    public C39281pa(OutputStream outputStream, int i) {
        this.A02 = outputStream;
        this.A01 = i;
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        if (this.A00 < ((long) this.A01)) {
            this.A02.write(i);
            this.A00++;
        }
    }
}
