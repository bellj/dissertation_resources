package X;

import android.view.View;
import com.whatsapp.conversation.ConversationAttachmentContentView;

/* renamed from: X.4n7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnLongClickListenerC101184n7 implements View.OnLongClickListener {
    public final /* synthetic */ ConversationAttachmentContentView A00;
    public final /* synthetic */ String A01;

    public View$OnLongClickListenerC101184n7(ConversationAttachmentContentView conversationAttachmentContentView, String str) {
        this.A00 = conversationAttachmentContentView;
        this.A01 = str;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        this.A00.A0D.A0C(this.A01, true);
        return true;
    }
}
