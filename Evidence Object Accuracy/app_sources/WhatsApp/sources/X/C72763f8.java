package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.3f8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72763f8 extends AnimatorListenerAdapter {
    public final /* synthetic */ float A00;
    public final /* synthetic */ View A01;

    public C72763f8(View view, float f) {
        this.A01 = view;
        this.A00 = f;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A01.setX(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setX(this.A00);
    }
}
