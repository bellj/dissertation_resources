package X;

import android.content.Context;
import android.view.WindowManager;
import java.lang.ref.WeakReference;

/* renamed from: X.0b7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0b7 implements AbstractC12100hN {
    public final /* synthetic */ AnonymousClass0BZ A00;
    public final /* synthetic */ C06060Sa A01;

    public AnonymousClass0b7(AnonymousClass0BZ r1, C06060Sa r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC12100hN
    public void API() {
        WindowManager windowManager;
        C06060Sa r3 = this.A01;
        Context A00 = r3.A07.A00();
        AnonymousClass0BZ r2 = this.A00;
        r2.setVisibility(8);
        try {
            windowManager = (WindowManager) A00.getSystemService("window");
        } catch (IllegalArgumentException unused) {
        }
        if (windowManager != null) {
            windowManager.removeView(r2);
            WeakReference weakReference = C04470Lv.A00;
            if (weakReference.get() == r2) {
                weakReference.clear();
            }
            AbstractC12100hN r0 = r3.A05;
            if (r0 != null) {
                r0.API();
                return;
            }
            return;
        }
        throw new IllegalStateException("Window manager required but not found.");
    }
}
