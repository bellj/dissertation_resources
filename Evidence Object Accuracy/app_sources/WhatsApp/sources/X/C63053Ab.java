package X;

import java.io.File;

/* renamed from: X.3Ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63053Ab {
    public static String A00(AnonymousClass018 r2, AnonymousClass1X2 r3) {
        File file;
        C16150oX r0 = ((AbstractC16130oV) r3).A02;
        if (r0 == null || (file = r0.A0F) == null) {
            return "";
        }
        int i = ((AbstractC16130oV) r3).A00;
        if (i == 0) {
            i = C22200yh.A07(file);
            ((AbstractC16130oV) r3).A00 = i;
            if (i == 0) {
                return C44891zj.A03(r2, ((AbstractC16130oV) r3).A01);
            }
        }
        return C38131nZ.A04(r2, (long) i);
    }
}
