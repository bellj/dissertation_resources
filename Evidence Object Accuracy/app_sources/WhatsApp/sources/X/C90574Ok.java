package X;

import java.security.SecureRandom;

/* renamed from: X.4Ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90574Ok {
    public int A00;
    public SecureRandom A01;

    public C90574Ok(int i, SecureRandom secureRandom) {
        this.A01 = secureRandom == null ? C95234dM.A00() : secureRandom;
        this.A00 = i;
    }
}
