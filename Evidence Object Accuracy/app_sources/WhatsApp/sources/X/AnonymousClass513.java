package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.513  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass513 implements AbstractC13670k8 {
    public final AbstractC115765Sv A00;
    public final C13600jz A01;
    public final Executor A02;

    public AnonymousClass513(AbstractC115765Sv r1, C13600jz r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        this.A02.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 20, this));
    }
}
