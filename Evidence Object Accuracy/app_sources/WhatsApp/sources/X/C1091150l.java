package X;

import com.google.android.gms.common.api.Status;

/* renamed from: X.50l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1091150l implements AbstractC117095Yk {
    public final Status A00;
    public final C77973oA A01;

    public C1091150l(Status status, C77973oA r2) {
        this.A00 = status;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        return this.A00;
    }
}
