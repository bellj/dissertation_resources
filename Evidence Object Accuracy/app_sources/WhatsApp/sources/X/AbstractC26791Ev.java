package X;

import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.1Ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC26791Ev extends AbstractC14550lc {
    public AbstractC26791Ev(C39401pq r4) {
        super(new C002601e(null, new AnonymousClass01N() { // from class: X.1pr
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                C39401pq r0 = C39401pq.this;
                return r0.A00.A8a(r0.A01, new LinkedBlockingQueue(), 0, 1, 1, 60);
            }
        }));
    }
}
