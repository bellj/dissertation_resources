package X;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: X.5Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC111925Bi implements Iterable, Serializable {
    public static final AbstractC111925Bi A00 = new C80273rz(C93104Zc.A04);
    public static final AbstractC115645Sj A01;
    public int zzc = 0;

    @Override // java.lang.Object
    public abstract boolean equals(Object obj);

    static {
        AbstractC115645Sj r0;
        if (AnonymousClass4ZV.A00()) {
            r0 = new AnonymousClass50I();
        } else {
            r0 = new AnonymousClass50H();
        }
        A01 = r0;
    }

    public static int A01(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder A0t = C12980iv.A0t(32);
            A0t.append("Beginning index: ");
            A0t.append(i);
            throw new IndexOutOfBoundsException(C12960it.A0d(" < 0", A0t));
        } else if (i2 < i) {
            StringBuilder A0t2 = C12980iv.A0t(66);
            A0t2.append("Beginning index larger than ending index: ");
            A0t2.append(i);
            throw new IndexOutOfBoundsException(C12960it.A0e(", ", A0t2, i2));
        } else {
            StringBuilder A0t3 = C12980iv.A0t(37);
            A0t3.append("End index: ");
            A0t3.append(i2);
            throw new IndexOutOfBoundsException(C12960it.A0e(" >= ", A0t3, i3));
        }
    }

    public int A02() {
        C80273rz r1 = (C80273rz) this;
        if (!(r1 instanceof C80263ry)) {
            return r1.zzb.length;
        }
        return ((C80263ry) r1).zzd;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = this.zzc;
        if (i == 0) {
            int A02 = A02();
            C80273rz r0 = (C80273rz) this;
            byte[] bArr = r0.zzb;
            int A03 = r0.A03();
            i = A02;
            for (int i2 = A03; i2 < A03 + A02; i2++) {
                i = (i * 31) + bArr[i2];
            }
            if (i == 0) {
                i = 1;
            }
            this.zzc = i;
        }
        return i;
    }

    @Override // java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return new AnonymousClass5DD(this);
    }

    @Override // java.lang.Object
    public final String toString() {
        String concat;
        Locale locale = Locale.ROOT;
        Object[] objArr = new Object[3];
        objArr[0] = C72453ed.A0o(this);
        int A02 = A02();
        C12980iv.A1T(objArr, A02);
        if (A02 <= 50) {
            concat = AnonymousClass4DW.A00(this);
        } else {
            concat = String.valueOf(AnonymousClass4DW.A00(C80273rz.A00((C80273rz) this, 47))).concat("...");
        }
        objArr[2] = concat;
        return String.format(locale, "<ByteString@%s size=%d contents=\"%s\">", objArr);
    }
}
