package X;

/* renamed from: X.4EW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4EW {
    public static final String A00(int i) {
        switch (i) {
            case 1:
                return "REQUEST_FAILED";
            case 2:
                return "INVALID_TOS_VERSION";
            case 3:
                return "NULL_LAYOUT";
            case 4:
                return "UNEXPECTED_ERROR";
            case 5:
                return "SUCCESS";
            case 6:
            default:
                return "UNKNOWN";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "EXPIRED_TOKEN";
            case 9:
                return "PING_NEEDED";
            case 10:
                return "RETRY_WITH_BACKOFF";
        }
    }
}
