package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZU extends Drawable {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public final Paint A04;
    public final Paint A05 = C12990iw.A0G(5);
    public final Path A06;
    public final Path A07;
    public final Rect A08;
    public final RectF A09;
    public final RectF A0A;
    public final float[] A0B;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZU() {
        Paint A0A = C12960it.A0A();
        this.A04 = A0A;
        this.A0A = C12980iv.A0K();
        this.A08 = C12980iv.A0J();
        this.A09 = C12980iv.A0K();
        this.A07 = new Path();
        this.A06 = new Path();
        this.A0B = new float[8];
        C12990iw.A13(A0A);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (C64973Hq.A02(this.A03)) {
            RectF rectF = this.A0A;
            float f = this.A02;
            canvas.drawRoundRect(rectF, f, f, this.A05);
            Paint paint = this.A04;
            if (paint.getStrokeWidth() != 0.0f) {
                RectF rectF2 = this.A09;
                float f2 = this.A01;
                canvas.drawRoundRect(rectF2, f2, f2, paint);
                return;
            }
            return;
        }
        canvas.drawPath(this.A07, this.A05);
        Paint paint2 = this.A04;
        if (paint2.getStrokeWidth() != 0.0f) {
            canvas.drawPath(this.A06, paint2);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        outline.setRoundRect(this.A08, this.A02);
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        RectF rectF = this.A0A;
        rectF.set(rect);
        this.A08.set(rect);
        RectF rectF2 = this.A09;
        float f = this.A00;
        rectF2.set(((float) rect.left) + f, ((float) rect.top) + f, ((float) rect.right) - f, ((float) rect.bottom) - f);
        int i = this.A03;
        if (!C64973Hq.A02(i)) {
            Path path = this.A07;
            Float valueOf = Float.valueOf(this.A02);
            float[] fArr = this.A0B;
            C64973Hq.A01(fArr, valueOf.floatValue(), i);
            path.reset();
            path.addRoundRect(rectF, fArr, Path.Direction.CW);
            if (this.A04.getStrokeWidth() != 0.0f) {
                Path path2 = this.A06;
                Float valueOf2 = Float.valueOf(this.A01);
                C64973Hq.A01(fArr, valueOf2.floatValue(), this.A03);
                path2.reset();
                path2.addRoundRect(rectF2, fArr, Path.Direction.CW);
            }
        }
    }
}
