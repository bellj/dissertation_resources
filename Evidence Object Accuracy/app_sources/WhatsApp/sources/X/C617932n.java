package X;

import com.whatsapp.invites.ViewGroupInviteActivity;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;

/* renamed from: X.32n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617932n extends AbstractC628338t {
    public WeakReference A00;
    public final C14900mE A01;

    public C617932n(C14900mE r2, ViewGroupInviteActivity viewGroupInviteActivity, C15580nU r4, UserJid userJid, C20660w7 r6) {
        super(r4, userJid, r6);
        this.A01 = r2;
        this.A00 = C12970iu.A10(viewGroupInviteActivity);
    }
}
