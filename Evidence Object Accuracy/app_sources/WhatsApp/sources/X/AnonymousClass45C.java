package X;

import java.io.File;

/* renamed from: X.45C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45C extends AbstractC91424Rr {
    public int A00;
    public boolean A01 = true;

    public AnonymousClass45F A00() {
        File file = super.A00;
        byte[] bArr = this.A03;
        boolean z = this.A02;
        return new AnonymousClass45F(file, super.A01, bArr, this.A00, z, this.A01);
    }
}
