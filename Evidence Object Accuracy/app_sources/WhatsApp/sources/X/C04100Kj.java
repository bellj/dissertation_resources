package X;

import android.content.res.Configuration;
import android.os.Build;

/* renamed from: X.0Kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C04100Kj {
    public static AnonymousClass06C A00(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 24) {
            return AnonymousClass06C.A00(C04090Ki.A00(configuration));
        }
        return AnonymousClass06C.A02(configuration.locale);
    }
}
