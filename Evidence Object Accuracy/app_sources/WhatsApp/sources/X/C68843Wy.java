package X;

import android.database.ContentObserver;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.3Wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68843Wy implements AbstractC35581iK {
    public final AbstractC35581iK A00;
    public final List A01;

    public C68843Wy(AbstractC35581iK r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        return this.A00.AAz();
    }

    @Override // X.AbstractC35581iK
    public AbstractC35611iN AEC(int i) {
        List list = this.A01;
        if (i < list.size()) {
            return (AbstractC35611iN) list.get(i);
        }
        return this.A00.AEC(i - list.size());
    }

    @Override // X.AbstractC35581iK
    public void AaX() {
        this.A00.AaX();
    }

    @Override // X.AbstractC35581iK
    public void close() {
        this.A00.close();
    }

    @Override // X.AbstractC35581iK
    public int getCount() {
        return this.A00.getCount() + this.A01.size();
    }

    @Override // X.AbstractC35581iK
    public boolean isEmpty() {
        return this.A00.isEmpty() && this.A01.isEmpty();
    }

    @Override // X.AbstractC35581iK
    public void registerContentObserver(ContentObserver contentObserver) {
        this.A00.registerContentObserver(contentObserver);
    }

    @Override // X.AbstractC35581iK
    public void unregisterContentObserver(ContentObserver contentObserver) {
        this.A00.unregisterContentObserver(contentObserver);
    }
}
