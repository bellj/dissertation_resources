package X;

import android.media.MediaCodec;
import android.media.MediaFormat;
import com.whatsapp.util.Log;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/* renamed from: X.1p5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1p5 implements AbstractC39001p6 {
    public static final int[] A09 = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000};
    public int A00;
    public AnonymousClass5UN A01;
    public ByteBuffer[] A02;
    public final int A03;
    public final long A04;
    public final long A05;
    public final File A06;
    public final File A07;
    public volatile boolean A08;

    @Override // X.AbstractC39001p6
    public boolean AII() {
        return true;
    }

    public /* synthetic */ AnonymousClass1p5(C91614Sk r3) {
        this.A06 = r3.A03;
        this.A04 = r3.A01;
        this.A05 = r3.A02;
        this.A07 = r3.A04;
        this.A03 = r3.A00;
    }

    public static boolean A00(File file) {
        if (file != null && file.exists()) {
            try {
                C38941ox r2 = new C38941ox();
                r2.setDataSource(file.getAbsolutePath());
                boolean z = false;
                if (r2.extractMetadata(16) != null) {
                    z = true;
                }
                r2.close();
                return z;
            } catch (Exception e) {
                Log.e("audiotranscoder/cantranscode", e);
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:124:0x03ea, code lost:
        if (r50.A01.AUN(r6) != false) goto L_0x03ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0263, code lost:
        if (r2.getInteger("bit-width") != 24) goto L_0x0265;
     */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03ff  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x044e  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x042a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x023b A[Catch: all -> 0x048b, TryCatch #4 {all -> 0x04dc, blocks: (B:10:0x008d, B:148:0x0465, B:11:0x0091, B:15:0x00b2, B:17:0x00bc, B:18:0x00c2, B:21:0x00c7, B:24:0x00d2, B:27:0x00d9, B:29:0x00de, B:30:0x00ed, B:31:0x00ee, B:33:0x0123, B:35:0x012b, B:37:0x0131, B:38:0x0149, B:39:0x0150, B:41:0x0156, B:42:0x015a, B:44:0x017c, B:45:0x0180, B:47:0x0187, B:49:0x01c4, B:50:0x01ea, B:143:0x0452, B:145:0x045a, B:146:0x045d, B:151:0x049e, B:152:0x04b8, B:154:0x04ba, B:155:0x04d4, B:51:0x01f6, B:53:0x01fa, B:55:0x0202, B:57:0x020b, B:58:0x0220, B:60:0x0232, B:62:0x023b, B:65:0x0245, B:67:0x0251, B:69:0x0259, B:72:0x0267, B:74:0x027b, B:75:0x0281, B:78:0x02ab, B:79:0x02b0, B:80:0x02e0, B:82:0x02e5, B:84:0x02e9, B:85:0x02ec, B:86:0x0303, B:88:0x033b, B:90:0x0342, B:92:0x034e, B:95:0x0365, B:97:0x036b, B:99:0x0373, B:100:0x0376, B:101:0x0379, B:102:0x037f, B:103:0x0385, B:105:0x03ad, B:110:0x03bd, B:118:0x03ce, B:119:0x03d0, B:121:0x03df, B:123:0x03e3, B:126:0x03ed, B:127:0x03f2, B:128:0x03f5, B:129:0x03fa, B:132:0x0402, B:135:0x040c, B:136:0x0421, B:137:0x0424, B:139:0x042a, B:140:0x042f), top: B:162:0x008d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
        // Method dump skipped, instructions count: 1258
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1p5.A01():void");
    }

    public final void A02(MediaCodec.BufferInfo bufferInfo, MediaCodec mediaCodec, WritableByteChannel writableByteChannel, byte[] bArr) {
        String str;
        while (true) {
            int dequeueOutputBuffer = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);
            if (dequeueOutputBuffer == -1) {
                return;
            }
            if (dequeueOutputBuffer >= 0) {
                ByteBuffer byteBuffer = this.A02[dequeueOutputBuffer];
                byteBuffer.position(bufferInfo.offset);
                byteBuffer.limit(bufferInfo.offset + bufferInfo.size);
                if ((bufferInfo.flags & 2) == 0) {
                    this.A00++;
                    int i = (bufferInfo.size - bufferInfo.offset) + 7;
                    bArr[3] = (byte) (((i >> 11) & 3) | (bArr[3] & 252));
                    bArr[4] = (byte) ((i >> 3) & 255);
                    bArr[5] = (byte) (((i & 7) << 5) | 31);
                    writableByteChannel.write(ByteBuffer.wrap(bArr));
                    writableByteChannel.write(byteBuffer);
                }
                byteBuffer.clear();
                mediaCodec.releaseOutputBuffer(dequeueOutputBuffer, false);
            } else {
                if (dequeueOutputBuffer == -3) {
                    this.A02 = mediaCodec.getOutputBuffers();
                    str = "audiotranscoder/encoder output buffers have changed";
                } else if (dequeueOutputBuffer == -2) {
                    MediaFormat outputFormat = mediaCodec.getOutputFormat();
                    StringBuilder sb = new StringBuilder("audiotranscoder/encoder output format has changed to ");
                    sb.append(outputFormat);
                    str = sb.toString();
                }
                Log.i(str);
            }
        }
    }

    @Override // X.AbstractC39001p6
    public void cancel() {
        this.A08 = true;
    }
}
