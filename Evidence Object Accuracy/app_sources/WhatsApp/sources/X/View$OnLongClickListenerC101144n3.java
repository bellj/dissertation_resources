package X;

import android.view.View;

/* renamed from: X.4n3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnLongClickListenerC101144n3 implements View.OnLongClickListener {
    public final /* synthetic */ C55082hl A00;

    public View$OnLongClickListenerC101144n3(C55082hl r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        View.OnLongClickListener onLongClickListener = this.A00.A00;
        if (onLongClickListener != null) {
            return onLongClickListener.onLongClick(view);
        }
        return false;
    }
}
