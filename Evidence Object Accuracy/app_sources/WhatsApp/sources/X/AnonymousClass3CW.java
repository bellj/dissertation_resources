package X;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3CW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CW {
    public final TextView A00;
    public final AnonymousClass018 A01;

    public AnonymousClass3CW(View view, AnonymousClass018 r3) {
        this.A01 = r3;
        this.A00 = C12960it.A0I(view, R.id.update_postcode_tip);
    }

    public void A00() {
        TextView textView = this.A00;
        textView.setVisibility(8);
        AlphaAnimation A0R = C12980iv.A0R();
        A0R.setDuration(320);
        textView.startAnimation(A0R);
    }
}
