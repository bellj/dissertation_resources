package X;

import java.security.InvalidParameterException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertPathBuilderSpi;
import java.security.cert.CertPathChecker;
import java.security.cert.CertPathParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* renamed from: X.5Hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113365Hf extends CertPathBuilderSpi {
    public Exception A00;
    public final AnonymousClass5S2 A01;
    public final boolean A02;

    public C113365Hf() {
        this(false);
    }

    public C113365Hf(boolean z) {
        this.A01 = new AnonymousClass5GT();
        this.A02 = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.security.cert.CertPathBuilderResult A00(java.security.cert.X509Certificate r9, java.util.List r10, X.C112075By r11) {
        /*
            r8 = this;
            boolean r0 = r10.contains(r9)
            r2 = 0
            if (r0 != 0) goto L_0x001c
            java.util.Set r0 = r11.A01
            boolean r0 = r0.contains(r9)
            if (r0 != 0) goto L_0x001c
            int r1 = r11.A00
            r0 = -1
            if (r1 == r0) goto L_0x001d
            int r0 = r10.size()
            int r0 = r0 + -1
            if (r0 <= r1) goto L_0x001d
        L_0x001c:
            return r2
        L_0x001d:
            r10.add(r9)
            org.spongycastle.jcajce.provider.asymmetric.x509.CertificateFactory r5 = new org.spongycastle.jcajce.provider.asymmetric.x509.CertificateFactory     // Catch: Exception -> 0x00e3
            r5.<init>()     // Catch: Exception -> 0x00e3
            boolean r0 = r8.A02     // Catch: Exception -> 0x00e3
            X.5Hj r6 = new X.5Hj     // Catch: Exception -> 0x00e3
            r6.<init>(r0)     // Catch: Exception -> 0x00e3
            X.5Bz r4 = r11.A02     // Catch: 4C6 -> 0x00ea
            java.util.Set r3 = r4.A08     // Catch: 4C6 -> 0x00ea
            java.security.cert.PKIXParameters r7 = r4.A01     // Catch: 4C6 -> 0x00ea
            java.lang.String r0 = r7.getSigProvider()     // Catch: 4C6 -> 0x00ea
            r1 = 0
            java.security.cert.TrustAnchor r0 = X.C95644e7.A01(r0, r9, r3)     // Catch: Exception -> 0x003e, 4C6 -> 0x00ea
            if (r0 == 0) goto L_0x003e
            r1 = 1
        L_0x003e:
            if (r1 == 0) goto L_0x006d
            java.security.cert.CertPath r5 = r5.engineGenerateCertPath(r10)     // Catch: Exception -> 0x0065
            java.security.cert.CertPathValidatorResult r0 = r6.engineValidate(r5, r11)     // Catch: Exception -> 0x005d
            java.security.cert.PKIXCertPathValidatorResult r0 = (java.security.cert.PKIXCertPathValidatorResult) r0     // Catch: Exception -> 0x005d
            java.security.cert.TrustAnchor r4 = r0.getTrustAnchor()     // Catch: 4C6 -> 0x00ea
            java.security.cert.PolicyNode r3 = r0.getPolicyTree()     // Catch: 4C6 -> 0x00ea
            java.security.PublicKey r1 = r0.getPublicKey()     // Catch: 4C6 -> 0x00ea
            java.security.cert.PKIXCertPathBuilderResult r0 = new java.security.cert.PKIXCertPathBuilderResult     // Catch: 4C6 -> 0x00ea
            r0.<init>(r5, r4, r3, r1)     // Catch: 4C6 -> 0x00ea
            goto L_0x00f3
        L_0x005d:
            r1 = move-exception
            java.lang.String r0 = "Certification path could not be validated."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x0065:
            r1 = move-exception
            java.lang.String r0 = "Certification path could not be constructed from certificate list."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x006d:
            java.util.ArrayList r6 = X.C12960it.A0l()     // Catch: 4C6 -> 0x00ea
            java.util.List r0 = r4.A05     // Catch: 4C6 -> 0x00ea
            r6.addAll(r0)     // Catch: 4C6 -> 0x00ea
            X.1TK r0 = X.C114715Mu.A0J     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            java.lang.String r0 = r0.A01     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            byte[] r0 = r9.getExtensionValue(r0)     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            java.util.Map r5 = r4.A07     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            if (r0 != 0) goto L_0x0088
            java.util.List r4 = java.util.Collections.EMPTY_LIST     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
        L_0x0084:
            r6.addAll(r4)     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            goto L_0x00a0
        L_0x0088:
            byte[] r0 = X.AnonymousClass5NH.A05(r0)     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            X.5N1[] r3 = X.C114705Mt.A01(r0)     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            java.util.ArrayList r4 = X.C12960it.A0l()     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            r1 = 0
        L_0x0095:
            int r0 = r3.length     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            if (r1 == r0) goto L_0x0084
            r0 = r3[r1]     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            r5.get(r0)     // Catch: CertificateParsingException -> 0x00db, 4C6 -> 0x00ea
            int r1 = r1 + 1
            goto L_0x0095
        L_0x00a0:
            java.util.HashSet r1 = X.C12970iu.A12()     // Catch: 4C6 -> 0x00ea
            java.util.List r0 = r7.getCertStores()     // Catch: 4C6 -> 0x00d3
            java.util.Collection r0 = X.C95644e7.A02(r9, r0, r6)     // Catch: 4C6 -> 0x00d3
            r1.addAll(r0)     // Catch: 4C6 -> 0x00d3
            boolean r0 = r1.isEmpty()     // Catch: 4C6 -> 0x00ea
            if (r0 != 0) goto L_0x00cc
            java.util.Iterator r1 = r1.iterator()     // Catch: 4C6 -> 0x00ea
        L_0x00b9:
            boolean r0 = r1.hasNext()     // Catch: 4C6 -> 0x00ea
            if (r0 == 0) goto L_0x00ed
            if (r2 != 0) goto L_0x00ed
            java.lang.Object r0 = r1.next()     // Catch: 4C6 -> 0x00ea
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch: 4C6 -> 0x00ea
            java.security.cert.CertPathBuilderResult r2 = r8.A00(r0, r10, r11)     // Catch: 4C6 -> 0x00ea
            goto L_0x00b9
        L_0x00cc:
            java.lang.String r0 = "No issuer certificate for certificate in certification path found."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r2)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x00d3:
            r1 = move-exception
            java.lang.String r0 = "Cannot find issuer certificate for certificate in certification path."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x00db:
            r1 = move-exception
            java.lang.String r0 = "No additional X.509 stores can be added from certificate locations."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x00e3:
            java.lang.String r0 = "Exception creating support classes."
            java.lang.RuntimeException r0 = X.C12990iw.A0m(r0)     // Catch: 4C6 -> 0x00ea
            throw r0     // Catch: 4C6 -> 0x00ea
        L_0x00ea:
            r0 = move-exception
            r8.A00 = r0
        L_0x00ed:
            if (r2 != 0) goto L_0x001c
            r10.remove(r9)
            return r2
        L_0x00f3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113365Hf.A00(java.security.cert.X509Certificate, java.util.List, X.5By):java.security.cert.CertPathBuilderResult");
    }

    @Override // java.security.cert.CertPathBuilderSpi
    public CertPathBuilderResult engineBuild(CertPathParameters certPathParameters) {
        C112075By r5;
        C93504aH r2;
        if (certPathParameters instanceof PKIXBuilderParameters) {
            PKIXBuilderParameters pKIXBuilderParameters = (PKIXBuilderParameters) certPathParameters;
            C93814am r3 = new C93814am(pKIXBuilderParameters);
            if (certPathParameters instanceof C113415Hm) {
                AnonymousClass5Pc r52 = (AnonymousClass5Pc) certPathParameters;
                Iterator it = Collections.unmodifiableList(((C113415Hm) r52).A01).iterator();
                while (it.hasNext()) {
                    it.next();
                    r3.A02.add(null);
                }
                r2 = new C93504aH(new C112085Bz(r3));
                r2.A01.addAll(Collections.unmodifiableSet(r52.A01));
                int i = r52.A00;
                if (i >= -1) {
                    r2.A00 = i;
                } else {
                    throw new InvalidParameterException("The maximum path length parameter can not be less than -1.");
                }
            } else {
                r2 = new C93504aH(pKIXBuilderParameters);
            }
            r5 = new C112075By(r2);
        } else if (certPathParameters instanceof C112075By) {
            r5 = (C112075By) certPathParameters;
        } else {
            StringBuilder A0k = C12960it.A0k("Parameters must be an instance of ");
            A0k.append(PKIXBuilderParameters.class.getName());
            A0k.append(" or ");
            A0k.append(C112075By.class.getName());
            throw C72463ee.A0J(C12960it.A0d(".", A0k));
        }
        ArrayList A0l = C12960it.A0l();
        CertPathBuilderResult certPathBuilderResult = null;
        Iterator it2 = C95644e7.A03(r5).iterator();
        while (true) {
            if (it2.hasNext()) {
                if (certPathBuilderResult != null) {
                    break;
                }
                certPathBuilderResult = A00((X509Certificate) it2.next(), A0l, r5);
            } else if (certPathBuilderResult == null) {
                Exception exc = this.A00;
                if (exc == null) {
                    throw new CertPathBuilderException("Unable to find certificate chain.");
                } else if (exc instanceof AnonymousClass4C6) {
                    throw new CertPathBuilderException(exc.getMessage(), this.A00.getCause());
                } else {
                    throw new CertPathBuilderException("Possible certificate chain could not be validated.", exc);
                }
            }
        }
        return certPathBuilderResult;
    }

    @Override // java.security.cert.CertPathBuilderSpi
    public /* bridge */ /* synthetic */ CertPathChecker engineGetRevocationChecker() {
        return new C113425Hn(this.A01);
    }
}
