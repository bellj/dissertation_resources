package X;

import android.graphics.Bitmap;
import com.whatsapp.mediacomposer.ImageComposerFragment;

/* renamed from: X.56w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1107856w implements AnonymousClass23E {
    public final /* synthetic */ ImageComposerFragment A00;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void A6L() {
    }

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public C1107856w(ImageComposerFragment imageComposerFragment) {
        this.A00 = imageComposerFragment;
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        this.A00.A00 = bitmap;
    }
}
