package X;

import android.graphics.Rect;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.whatsapp.calling.callgrid.view.FocusViewContainer;

/* renamed from: X.3Mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnLayoutChangeListenerC66123Mj implements View.OnLayoutChangeListener {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ FocusViewContainer A01;

    public View$OnLayoutChangeListenerC66123Mj(Rect rect, FocusViewContainer focusViewContainer) {
        this.A01 = focusViewContainer;
        this.A00 = rect;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        FocusViewContainer focusViewContainer = this.A01;
        Rect rect = focusViewContainer.A00;
        view.getGlobalVisibleRect(rect);
        Rect rect2 = this.A00;
        int i9 = rect2.top - rect.top;
        int i10 = rect2.left - rect.left;
        float width = ((float) rect2.width()) / ((float) rect.width());
        float height = ((float) rect2.height()) / ((float) rect.height());
        focusViewContainer.A03.setAlpha(0.0f);
        focusViewContainer.A02.setAlpha(0.0f);
        FrameLayout frameLayout = focusViewContainer.A01;
        frameLayout.setPivotX(0.0f);
        frameLayout.setPivotY(0.0f);
        frameLayout.setScaleX(width);
        frameLayout.setScaleY(height);
        frameLayout.setTranslationX((float) i10);
        frameLayout.setTranslationY((float) i9);
        frameLayout.animate().setDuration(250).scaleX(1.0f).scaleY(1.0f).translationX(0.0f).translationY(0.0f).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 4)).setInterpolator(new DecelerateInterpolator(1.5f));
        frameLayout.removeOnLayoutChangeListener(this);
    }
}
