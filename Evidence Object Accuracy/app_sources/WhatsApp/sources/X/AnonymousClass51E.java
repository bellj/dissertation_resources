package X;

import com.whatsapp.picker.search.StickerSearchDialogFragment;

/* renamed from: X.51E  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51E implements AbstractC467627l {
    public final /* synthetic */ StickerSearchDialogFragment A00;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public AnonymousClass51E(StickerSearchDialogFragment stickerSearchDialogFragment) {
        this.A00 = stickerSearchDialogFragment;
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r3) {
        StickerSearchDialogFragment stickerSearchDialogFragment = this.A00;
        stickerSearchDialogFragment.A05.A03();
        stickerSearchDialogFragment.A03.setCurrentItem(r3.A00);
    }
}
