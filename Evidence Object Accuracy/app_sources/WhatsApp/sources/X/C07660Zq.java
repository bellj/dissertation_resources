package X;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.SystemAlarmService;

/* renamed from: X.0Zq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07660Zq implements AbstractC12570i8 {
    public static final String A01 = C06390Tk.A01("SystemAlarmScheduler");
    public final Context A00;

    @Override // X.AbstractC12570i8
    public boolean AIE() {
        return true;
    }

    public C07660Zq(Context context) {
        this.A00 = context.getApplicationContext();
    }

    @Override // X.AbstractC12570i8
    public void A73(String str) {
        Context context = this.A00;
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        context.startService(intent);
    }

    @Override // X.AbstractC12570i8
    public void AbJ(C004401z... r9) {
        for (C004401z r7 : r9) {
            C06390Tk.A00().A02(A01, String.format("Scheduling work with workSpecId %s", r7.A0E), new Throwable[0]);
            Context context = this.A00;
            String str = r7.A0E;
            Intent intent = new Intent(context, SystemAlarmService.class);
            intent.setAction("ACTION_SCHEDULE_WORK");
            intent.putExtra("KEY_WORKSPEC_ID", str);
            context.startService(intent);
        }
    }
}
