package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1ZW extends AbstractC28901Pl {
    public static final Parcelable.Creator CREATOR = new C99834kw();
    public String A00;
    public final String A01;
    public final boolean A02;
    public final boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZW(C17930rd r1, AnonymousClass1ZY r2, String str, String str2, String str3, String str4, boolean z, boolean z2) {
        this.A00 = str3;
        this.A02 = z;
        this.A03 = z2;
        this.A01 = str2;
        AnonymousClass009.A05(r1);
        this.A07 = r1;
        this.A0A = str;
        this.A08 = r2;
        A0A(str4);
    }

    public /* synthetic */ AnonymousClass1ZW(Parcel parcel) {
        A09(parcel);
        this.A00 = parcel.readString();
        boolean z = false;
        this.A02 = parcel.readByte() == 1;
        this.A03 = parcel.readByte() == 1 ? true : z;
        this.A01 = parcel.readString();
    }

    @Override // X.AbstractC28901Pl, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("[ MERCHANT: ");
        sb.append(super.toString());
        sb.append(" merchantId: ");
        sb.append(this.A00);
        sb.append(" p2mEligible: ");
        sb.append(this.A02);
        sb.append(" p2pEligible: ");
        sb.append(this.A03);
        sb.append(" logoUri: ");
        sb.append(this.A01);
        sb.append(" ]");
        return sb.toString();
    }

    @Override // X.AbstractC28901Pl, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
        parcel.writeByte(this.A02 ? (byte) 1 : 0);
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
        parcel.writeString(this.A01);
    }
}
