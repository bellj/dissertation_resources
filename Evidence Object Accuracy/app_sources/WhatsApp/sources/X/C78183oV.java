package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.wearable.ConnectionConfiguration;

/* renamed from: X.3oV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78183oV extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99374kC();
    public final int A00;
    public final ConnectionConfiguration[] A01;

    public C78183oV(ConnectionConfiguration[] connectionConfigurationArr, int i) {
        this.A00 = i;
        this.A01 = connectionConfigurationArr;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0I(parcel, this.A01, 3, i);
        C95654e8.A06(parcel, A00);
    }
}
