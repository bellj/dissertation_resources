package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC869649r extends Enum {
    public static final /* synthetic */ EnumC869649r[] A00;
    public static final EnumC869649r A01;

    public static EnumC869649r valueOf(String str) {
        return (EnumC869649r) Enum.valueOf(EnumC869649r.class, str);
    }

    public static EnumC869649r[] values() {
        return (EnumC869649r[]) A00.clone();
    }

    static {
        EnumC869649r r3 = new EnumC869649r("NORMAL", 0);
        A01 = r3;
        EnumC869649r[] r0 = new EnumC869649r[2];
        C72453ed.A1J(r3, new EnumC869649r("SMALL", 1), r0);
        A00 = r0;
    }

    public EnumC869649r(String str, int i) {
    }
}
