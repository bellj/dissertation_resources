package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.4je  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99034je implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        CameraPosition cameraPosition = null;
        Float f = null;
        Float f2 = null;
        LatLngBounds latLngBounds = null;
        byte b = -1;
        byte b2 = -1;
        int i = 0;
        byte b3 = -1;
        byte b4 = -1;
        byte b5 = -1;
        byte b6 = -1;
        byte b7 = -1;
        byte b8 = -1;
        byte b9 = -1;
        byte b10 = -1;
        byte b11 = -1;
        byte b12 = -1;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    b = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    b2 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case 4:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 5:
                    cameraPosition = (CameraPosition) C95664e9.A07(parcel, CameraPosition.CREATOR, readInt);
                    break;
                case 6:
                    b3 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case 7:
                    b4 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case '\b':
                    b5 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case '\t':
                    b6 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case '\n':
                    b7 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case 11:
                    b8 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case '\f':
                    b9 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    b10 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case 15:
                    b11 = (byte) C95664e9.A02(parcel, readInt);
                    break;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    int A03 = C95664e9.A03(parcel, readInt);
                    if (A03 != 0) {
                        C95664e9.A0E(parcel, A03, 4);
                        f = Float.valueOf(parcel.readFloat());
                        break;
                    } else {
                        f = null;
                        break;
                    }
                case 17:
                    int A032 = C95664e9.A03(parcel, readInt);
                    if (A032 != 0) {
                        C95664e9.A0E(parcel, A032, 4);
                        f2 = Float.valueOf(parcel.readFloat());
                        break;
                    } else {
                        f2 = null;
                        break;
                    }
                case 18:
                    latLngBounds = (LatLngBounds) C95664e9.A07(parcel, LatLngBounds.CREATOR, readInt);
                    break;
                case 19:
                    b12 = (byte) C95664e9.A02(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new GoogleMapOptions(cameraPosition, latLngBounds, f, f2, b, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
