package X;

import android.content.Context;

/* renamed from: X.0xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21710xr extends C002601e {
    public C21710xr(Context context) {
        super(null, new AnonymousClass01N(context) { // from class: X.1OC
            public final /* synthetic */ Context A00;

            {
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return AnonymousClass022.A00(this.A00);
            }
        });
    }
}
