package X;

import android.content.ClipData;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputContentInfo;
import androidx.core.view.inputmethod.InputConnectionCompat;
import androidx.core.view.inputmethod.InputContentInfoCompat;

/* renamed from: X.0YC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0YC implements InputConnectionCompat.OnCommitContentListener {
    public final /* synthetic */ View A00;

    public AnonymousClass0YC(View view) {
        this.A00 = view;
    }

    @Override // androidx.core.view.inputmethod.InputConnectionCompat.OnCommitContentListener
    public boolean onCommitContent(InputContentInfoCompat inputContentInfoCompat, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 25 && (i & 1) != 0) {
            try {
                inputContentInfoCompat.requestPermission();
                InputContentInfo inputContentInfo = (InputContentInfo) inputContentInfoCompat.mImpl.getInputContentInfo();
                if (bundle == null) {
                    bundle = new Bundle();
                } else {
                    bundle = new Bundle(bundle);
                }
                bundle.putParcelable(InputConnectionCompat.EXTRA_INPUT_CONTENT_INFO, inputContentInfo);
            } catch (Exception e) {
                Log.w("ReceiveContent", "Can't insert content from IME; requestPermission() failed", e);
                return false;
            }
        }
        AnonymousClass0MT r2 = new AnonymousClass0MT(new ClipData(inputContentInfoCompat.mImpl.getDescription(), new ClipData.Item(inputContentInfoCompat.mImpl.getContentUri())), 2);
        Uri linkUri = inputContentInfoCompat.mImpl.getLinkUri();
        AbstractC12620iD r0 = r2.A00;
        r0.AcH(linkUri);
        r0.setExtras(bundle);
        if (AnonymousClass028.A0E(this.A00, r0.A6j()) == null) {
            return true;
        }
        return false;
    }
}
