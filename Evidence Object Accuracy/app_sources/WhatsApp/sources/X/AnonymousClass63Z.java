package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;

/* renamed from: X.63Z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63Z implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(34);
    public final UserJid A00;
    public final C1316363n A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass63Z(UserJid userJid, C1316363n r2, String str) {
        this.A00 = userJid;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A01, i);
    }
}
