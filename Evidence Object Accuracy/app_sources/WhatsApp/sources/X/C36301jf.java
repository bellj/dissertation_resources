package X;

import android.location.Location;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.1jf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36301jf extends AbstractView$OnCreateContextMenuListenerC35851ir {
    public final /* synthetic */ GroupChatLiveLocationsActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36301jf(AnonymousClass12P r24, C244615p r25, C14900mE r26, C15570nT r27, C16240og r28, C22330yu r29, AnonymousClass130 r30, C15550nR r31, AnonymousClass10S r32, C15610nY r33, C21270x9 r34, AnonymousClass131 r35, C14830m7 r36, C15890o4 r37, AnonymousClass018 r38, AnonymousClass12H r39, C244215l r40, GroupChatLiveLocationsActivity groupChatLiveLocationsActivity, C16030oK r42, AnonymousClass13O r43, C244415n r44, AnonymousClass19Z r45) {
        super(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r42, r43, r44, r45);
        this.A00 = groupChatLiveLocationsActivity;
    }

    @Override // X.AbstractView$OnCreateContextMenuListenerC35851ir
    public void A0C() {
        super.A0C();
        this.A00.A2f();
    }

    @Override // X.AbstractView$OnCreateContextMenuListenerC35851ir, android.location.LocationListener
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = this.A00;
        if (groupChatLiveLocationsActivity.A0L.A0s && location != null) {
            AnonymousClass009.A05(groupChatLiveLocationsActivity.A05);
            AnonymousClass03T r2 = new AnonymousClass03T(location.getLatitude(), location.getLongitude());
            AnonymousClass04Q r1 = groupChatLiveLocationsActivity.A05;
            C05200Oq r0 = new C05200Oq();
            r0.A06 = r2;
            r1.A09(r0);
        }
    }
}
