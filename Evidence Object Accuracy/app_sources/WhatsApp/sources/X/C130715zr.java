package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.math.BigInteger;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.Locale;
import javax.crypto.Cipher;

/* renamed from: X.5zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130715zr {
    public final JniBridge A00;

    public C130715zr(JniBridge jniBridge) {
        this.A00 = jniBridge;
    }

    public static String A00(BigInteger bigInteger) {
        if (bigInteger != null) {
            try {
                byte[] byteArray = bigInteger.toByteArray();
                int length = byteArray.length;
                if (length >= 4) {
                    StringBuilder A0h = C12960it.A0h();
                    for (int i = length - 1; i >= length - 4; i--) {
                        A0h.append(String.format("%02X", Byte.valueOf(byteArray[i])));
                    }
                    return A0h.toString();
                }
            } catch (Exception e) {
                Log.e(C12960it.A0b("PAY: JweCompactSerializer/getCertID: ", e));
                return null;
            }
        }
        return null;
    }

    public String A01(AnonymousClass6B7 r17, String str, boolean z) {
        BigInteger bigInteger;
        try {
            try {
                bigInteger = ((RSAPublicKey) C117305Zk.A0p(r17.A06)).getModulus();
            } catch (Exception e) {
                Log.w("PAY: JweCompactSerializer/getModulusFromProviderKey failed: ", e);
                bigInteger = null;
            }
            String A00 = A00(bigInteger);
            if (A00 != null && z) {
                A00 = A00.toLowerCase(Locale.US);
            }
            String obj = C117295Zj.A0a().put("alg", "RSA-OAEP-256").put("enc", "A256GCM").put("typ", "JOSE").put("kid", A00).toString();
            String str2 = AnonymousClass01V.A08;
            String encodeToString = Base64.encodeToString(obj.getBytes(str2), 11);
            byte[] bArr = new byte[32];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(bArr);
            try {
                PublicKey A0p = C117305Zk.A0p(r17.A06);
                Cipher instance = Cipher.getInstance("RSA/ECB/OAEPwithSHA-256andMGF1Padding");
                instance.init(1, A0p);
                String encodeToString2 = Base64.encodeToString(instance.doFinal(bArr), 11);
                byte[] bArr2 = new byte[12];
                secureRandom.nextBytes(bArr2);
                String encodeToString3 = Base64.encodeToString(bArr2, 11);
                byte[] bytes = str.getBytes(str2);
                byte[] bArr3 = (byte[]) JniBridge.jvidispatchOIOOOO(4, (long) 16, bArr, bArr2, bytes, encodeToString.getBytes("US-ASCII"));
                if (bArr3 == null) {
                    return null;
                }
                int length = bytes.length;
                byte[] copyOfRange = Arrays.copyOfRange(bArr3, 0, length);
                byte[] copyOfRange2 = Arrays.copyOfRange(bArr3, length, bArr3.length);
                String encodeToString4 = Base64.encodeToString(copyOfRange, 11);
                String encodeToString5 = Base64.encodeToString(copyOfRange2, 11);
                String[] strArr = new String[5];
                C12990iw.A1P(encodeToString, encodeToString2, strArr);
                strArr[2] = encodeToString3;
                strArr[3] = encodeToString4;
                strArr[4] = encodeToString5;
                return TextUtils.join(".", strArr);
            } catch (Exception e2) {
                Log.w(C12960it.A0d(e2.toString(), C12960it.A0k("PAY: JweCompactSerializer/encryptCek")));
                throw C117315Zl.A0J(e2);
            }
        } catch (Exception e3) {
            Log.w("PAY: JweCompactSerializer/generateStepUpJweToken failed: ", e3);
            return null;
        }
    }
}
