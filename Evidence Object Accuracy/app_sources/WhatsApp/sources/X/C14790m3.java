package X;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CameraHomeFragment;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.community.CommunityFragment;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.status.StatusesFragment;

/* renamed from: X.0m3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14790m3 extends AnonymousClass019 implements AbstractC14800m4 {
    public final int A00;
    public final C14810m5[] A01;
    public final /* synthetic */ HomeActivity A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C14790m3(AnonymousClass01F r2, HomeActivity homeActivity) {
        super(r2, 0);
        this.A02 = homeActivity;
        int size = HomeActivity.A29.size();
        this.A00 = size;
        this.A01 = new C14810m5[size];
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00;
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        HomeActivity homeActivity = this.A02;
        int A2f = homeActivity.A2f(i);
        if (A2f == 100) {
            return "";
        }
        int i2 = R.string.chats;
        if (A2f != 200) {
            i2 = R.string.statuses;
            if (A2f != 300) {
                i2 = R.string.calls;
                if (A2f != 400) {
                    if (A2f == 500 || A2f == 600) {
                        return "";
                    }
                    StringBuilder sb = new StringBuilder("The item position should be less or equal to:");
                    sb.append(this.A00);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        return homeActivity.getString(i2);
    }

    @Override // X.AnonymousClass019, X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        AnonymousClass01E r3 = (AnonymousClass01E) super.A05(viewGroup, i);
        HomeActivity homeActivity = this.A02;
        if (homeActivity.A2f(i) == 100) {
            homeActivity.A0L = (CameraHomeFragment) r3;
        }
        return r3;
    }

    @Override // X.AnonymousClass019
    public long A0F(int i) {
        return (long) this.A02.A2f(i);
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        HomeActivity homeActivity = this.A02;
        int A2f = homeActivity.A2f(i);
        if (A2f == 100) {
            return new CameraHomeFragment();
        }
        if (A2f == 200) {
            return new ConversationsFragment();
        }
        if (A2f == 300) {
            return new StatusesFragment();
        }
        if (A2f == 400) {
            AnonymousClass01E r1 = (AnonymousClass01E) ((C18980tN) homeActivity.A1i.get()).A00(CallsHistoryFragment.class);
            AnonymousClass009.A0E(r1 instanceof AnonymousClass01E);
            return r1;
        } else if (A2f == 500) {
            throw new IllegalStateException("Invalid tab id: 500");
        } else if (A2f != 600) {
            StringBuilder sb = new StringBuilder("The item position should be less or equal to:");
            sb.append(this.A00);
            throw new IllegalArgumentException(sb.toString());
        } else if (homeActivity.A16.A0V()) {
            return new CommunityFragment();
        } else {
            throw new IllegalStateException("Invalid tab id: 600");
        }
    }

    public final C14810m5 A0H(int i) {
        ImageView imageView;
        int i2;
        C14810m5[] r6 = this.A01;
        if (r6[i] == null) {
            C14810m5 r5 = new C14810m5();
            HomeActivity homeActivity = this.A02;
            View inflate = homeActivity.getLayoutInflater().inflate(R.layout.home_tab, (ViewGroup) null, false);
            r5.A01 = inflate;
            r5.A06 = (TextView) AnonymousClass028.A0D(inflate, R.id.tab);
            r5.A05 = (TextView) AnonymousClass028.A0D(r5.A01, R.id.badge);
            r5.A04 = (ImageView) AnonymousClass028.A0D(r5.A01, R.id.icon);
            r5.A03 = (ImageView) AnonymousClass028.A0D(r5.A01, R.id.icon_badge);
            r5.A02 = (ViewGroup) AnonymousClass028.A0D(r5.A01, R.id.tab_container);
            r5.A06.setText(A04(i));
            if (i != HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 400)) {
                C016307r.A00(AnonymousClass00T.A03(homeActivity, R.color.selector_home_tab_color), r5.A04);
            }
            if (i == HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 100)) {
                r5.A04.setVisibility(0);
                r5.A04.setImageDrawable(AnonymousClass00T.A04(homeActivity, R.drawable.ic_home_camera).mutate());
                imageView = r5.A04;
                i2 = R.string.camera_button_description;
            } else {
                if (i == HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 300)) {
                    r5.A04.setImageDrawable(AnonymousClass00T.A04(homeActivity, R.drawable.new_status_indicator).mutate());
                    C42941w9.A08(r5.A04, ((ActivityC13830kP) homeActivity).A01, homeActivity.getResources().getDimensionPixelSize(R.dimen.tab_statuses_icon_padding), 0);
                } else if (i == HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 400)) {
                    C42941w9.A08(r5.A04, ((ActivityC13830kP) homeActivity).A01, homeActivity.getResources().getDimensionPixelSize(R.dimen.tab_calls_icon_padding), 0);
                    if (Build.VERSION.SDK_INT > 21) {
                        AnonymousClass07M A04 = AnonymousClass07M.A04(homeActivity, R.drawable.ic_calls_tab_joinable_badge_flash);
                        homeActivity.A0K = A04;
                        r5.A04.setImageDrawable(A04);
                    } else {
                        r5.A04.setImageResource(R.drawable.ic_calls_tab_joinable_badge);
                    }
                } else if (homeActivity.A16.A0V() && i == HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 600)) {
                    C013606j A01 = C013606j.A01(null, homeActivity.getResources(), R.drawable.ic_community_tab);
                    C42941w9.A08(r5.A04, ((ActivityC13830kP) homeActivity).A01, 0, 0);
                    C42631vX.A01(r5.A02, homeActivity.getResources().getDimensionPixelSize(R.dimen.tab_width), homeActivity.getResources().getDimensionPixelSize(R.dimen.tab_height));
                    r5.A06.setVisibility(8);
                    r5.A04.setVisibility(0);
                    r5.A04.setImageDrawable(A01);
                    imageView = r5.A04;
                    i2 = R.string.community_button_description;
                }
                r6[i] = r5;
            }
            imageView.setContentDescription(homeActivity.getString(i2));
            r6[i] = r5;
        }
        return r6[i];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (r2.A2f(r5) == 600) goto L_0x001d;
     */
    @Override // X.AbstractC14800m4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View AEv(int r5) {
        /*
            r4 = this;
            com.whatsapp.HomeActivity r2 = r4.A02
            com.whatsapp.PagerSlidingTabStrip r3 = r2.A0P
            int r1 = r2.A2f(r5)
            r0 = 100
            if (r1 == r0) goto L_0x001d
            int r1 = r2.A2f(r5)
            r0 = 500(0x1f4, float:7.0E-43)
            if (r1 == r0) goto L_0x001d
            int r2 = r2.A2f(r5)
            r1 = 600(0x258, float:8.41E-43)
            r0 = 1
            if (r2 != r1) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            r3.setShouldExpand(r0)
            X.0m5 r0 = r4.A0H(r5)
            android.view.View r0 = r0.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14790m3.AEv(int):android.view.View");
    }
}
