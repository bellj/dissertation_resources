package X;

/* renamed from: X.0ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19190ti extends C19180th implements AbstractC19010tQ {
    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19190ti(C16510p9 r7, C20120vF r8, C18480sW r9) {
        super(r7, r8, r9, "media_migration_fixer", 3);
    }

    @Override // X.C19180th, X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        C21390xL r2 = this.A06;
        r2.A04("media_message_fixer_ready", 3);
        r2.A04("media_message_ready", 2);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        this.A06.A03("media_message_fixer_ready");
    }
}
