package X;

import android.app.Application;

/* renamed from: X.0cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09490cz implements Runnable {
    public final /* synthetic */ Application A00;
    public final /* synthetic */ C06770Uz A01;

    public RunnableC09490cz(Application application, C06770Uz r2) {
        this.A00 = application;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.unregisterActivityLifecycleCallbacks(this.A01);
    }
}
