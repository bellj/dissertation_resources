package X;

import android.content.Context;
import android.view.TextureView;

/* renamed from: X.5qB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C124825qB {
    public static AnonymousClass643 A00(Context context, TextureView textureView) {
        AnonymousClass643 r0 = new AnonymousClass643(context, textureView, new AnonymousClass66P(), C125345r6.A00(context));
        r0.A0C = false;
        return r0;
    }
}
