package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

/* renamed from: X.2Qd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50592Qd {
    public static ColorStateList A00(Context context, TypedArray typedArray, int i) {
        int resourceId;
        ColorStateList A00;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (A00 = C015007d.A00(context, resourceId)) == null) {
            return typedArray.getColorStateList(i);
        }
        return A00;
    }

    public static Drawable A01(Context context, TypedArray typedArray, int i) {
        int resourceId;
        Drawable A01;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (A01 = C015007d.A01(context, resourceId)) == null) {
            return typedArray.getDrawable(i);
        }
        return A01;
    }
}
