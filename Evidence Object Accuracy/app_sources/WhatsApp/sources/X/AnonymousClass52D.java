package X;

import java.util.List;

/* renamed from: X.52D  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass52D implements AnonymousClass5T8 {
    public void A00(Number number) {
        if (this instanceof C83053wc) {
            C83053wc r4 = (C83053wc) this;
            r4.A00 = Double.valueOf(r4.A00.doubleValue() + number.doubleValue());
        } else if (this instanceof C83073we) {
            C83073we r6 = (C83073we) this;
            r6.A01 = Double.valueOf(r6.A01.doubleValue() + number.doubleValue());
            r6.A02 = Double.valueOf(r6.A02.doubleValue() + (number.doubleValue() * number.doubleValue()));
            r6.A00 = Double.valueOf(r6.A00.doubleValue() + 1.0d);
        } else if (this instanceof C83043wb) {
            C83043wb r5 = (C83043wb) this;
            if (r5.A00.doubleValue() > number.doubleValue()) {
                r5.A00 = Double.valueOf(number.doubleValue());
            }
        } else if (!(this instanceof C83033wa)) {
            C83063wd r42 = (C83063wd) this;
            r42.A00 = Double.valueOf(r42.A00.doubleValue() + 1.0d);
            r42.A01 = Double.valueOf(r42.A01.doubleValue() + number.doubleValue());
        } else {
            C83033wa r52 = (C83033wa) this;
            if (r52.A00.doubleValue() < number.doubleValue()) {
                r52.A00 = Double.valueOf(number.doubleValue());
            }
        }
    }

    @Override // X.AnonymousClass5T8
    public Object AJ9(AbstractC111885Be r8, C94394bk r9, Object obj, String str, List list) {
        double sqrt;
        AbstractC117035Xw r1 = r9.A01.A00;
        int i = 0;
        if (obj instanceof List) {
            for (Object obj2 : r1.Aet(obj)) {
                if (obj2 instanceof Number) {
                    i++;
                    A00((Number) obj2);
                }
            }
        }
        if (list != null) {
            for (Number number : C95264dP.A00(r9, Number.class, list)) {
                i++;
                A00(number);
            }
        }
        if (i == 0) {
            throw new AnonymousClass5H8("Aggregation function attempted to calculate value using empty array");
        } else if (this instanceof C83053wc) {
            return ((C83053wc) this).A00;
        } else {
            if (this instanceof C83073we) {
                C83073we r6 = (C83073we) this;
                double doubleValue = r6.A02.doubleValue();
                double doubleValue2 = r6.A00.doubleValue();
                double doubleValue3 = r6.A01.doubleValue();
                sqrt = Math.sqrt((doubleValue / doubleValue2) - (((doubleValue3 * doubleValue3) / doubleValue2) / doubleValue2));
            } else if (this instanceof C83043wb) {
                return ((C83043wb) this).A00;
            } else {
                if (this instanceof C83033wa) {
                    return ((C83033wa) this).A00;
                }
                C83063wd r62 = (C83063wd) this;
                double doubleValue4 = r62.A00.doubleValue();
                if (doubleValue4 == 0.0d) {
                    return Double.valueOf(0.0d);
                }
                sqrt = r62.A01.doubleValue() / doubleValue4;
            }
            return Double.valueOf(sqrt);
        }
    }
}
