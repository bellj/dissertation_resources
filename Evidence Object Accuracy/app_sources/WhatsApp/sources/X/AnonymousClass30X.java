package X;

/* renamed from: X.30X  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30X extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;

    public AnonymousClass30X() {
        super(2582, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(4, this.A00);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCallStanzaReceive {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaDuration", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaOfflineCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaStage", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaType", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
