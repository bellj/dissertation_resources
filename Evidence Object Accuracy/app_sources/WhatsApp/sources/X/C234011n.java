package X;

import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.11n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C234011n {
    public final C233411h A00;
    public final C239313o A01;
    public final C234111o A02;
    public final C233511i A03;
    public final AnonymousClass16R A04;
    public final Map A05 = new HashMap();

    public C234011n(C233411h r2, C239313o r3, C234111o r4, C233511i r5, AnonymousClass16R r6) {
        this.A00 = r2;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r6;
        this.A01 = r3;
    }

    public static final C34891gs A00(C27811Jh r7) {
        String str = r7.A04;
        Integer valueOf = Integer.valueOf(r7.A00);
        byte[] bArr = r7.A05;
        AnonymousClass009.A05(bArr);
        return new C34891gs(r7.A01, r7.A02, r7.A03, valueOf, str, bArr, 6);
    }

    public final AbstractC250017s A01(AnonymousClass1JQ r3) {
        AbstractC250017s r0;
        String A03 = r3.A03();
        synchronized (this) {
            r0 = (AbstractC250017s) A03(A03);
        }
        return r0;
    }

    public AnonymousClass1JQ A02(C27811Jh r5) {
        AbstractC250017s r0;
        String str = r5.A06[0];
        synchronized (this) {
            r0 = (AbstractC250017s) A03(str);
        }
        if (r0 != null) {
            try {
                AnonymousClass1JQ A00 = this.A02.A00(r5, null, false);
                if (A00 != null) {
                    A00.A02 = r5.A05;
                    return A00;
                }
            } catch (C34331fy e) {
                this.A00.A07(e.errorCode, null);
            }
            StringBuilder sb = new StringBuilder("mutation-handlers/handleMutation the handler couldn't create a valid mutation for ");
            sb.append(str);
            Log.e(sb.toString());
            throw A00(r5);
        }
        StringBuilder sb2 = new StringBuilder("mutation-handlers/handleMutation no mutation handlers found to handle mutation: ");
        sb2.append(str);
        Log.e(sb2.toString());
        throw A00(r5);
    }

    public Object A03(String str) {
        Object obj;
        if (!this.A02.A02(str)) {
            return null;
        }
        synchronized (this) {
            obj = this.A05.get(str);
        }
        return obj;
    }

    public Set A04() {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet();
            for (String str : this.A05.keySet()) {
                if (this.A02.A02(str)) {
                    hashSet.add(str);
                }
            }
        }
        return hashSet;
    }

    public synchronized void A05(AbstractC250017s r3, String str) {
        Map map = this.A05;
        if (!map.containsKey(str)) {
            map.put(str, r3);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("mutation-handlers/add-handler handler exists with key: ");
            sb.append(str);
            Log.w(sb.toString());
        }
    }

    public void A06(AnonymousClass1JQ r4) {
        if (r4.A05() && (r4 instanceof AbstractC34381g3)) {
            String A03 = r4.A03();
            if ("clearChat".equals(A03) || "deleteChat".equals(A03) || "deleteMessageForMe".equals(A03)) {
                C239313o r2 = this.A01;
                AbstractC14640lm ABH = ((AbstractC34381g3) r4).ABH();
                synchronized (r2) {
                    Set set = r2.A00;
                    if (set != null) {
                        set.add(ABH);
                    }
                }
            }
        }
    }

    public void A07(Collection collection) {
        synchronized (this) {
            this.A03.A0F(collection);
        }
    }
}
