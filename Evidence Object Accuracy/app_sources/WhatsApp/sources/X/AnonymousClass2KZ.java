package X;

import java.lang.reflect.Array;

/* renamed from: X.2KZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KZ {
    public C71183cW A00;
    public final AnonymousClass2KY A01;

    public AnonymousClass2KZ(AnonymousClass2KY r1) {
        this.A01 = r1;
    }

    public C71183cW A00() {
        int[] iArr;
        int i;
        C71183cW r0 = this.A00;
        C71183cW r22 = r0;
        if (r0 == null) {
            AnonymousClass2KX r02 = (AnonymousClass2KX) this.A01;
            C71183cW r03 = r02.A00;
            r22 = r03;
            if (r03 == null) {
                AnonymousClass2KW r8 = ((AnonymousClass2KY) r02).A00;
                int i2 = r8.A01;
                int i3 = r8.A00;
                if (i2 < 40 || i3 < 40) {
                    r22 = new C71183cW(i2, i3);
                    if (r02.A01.length < i2) {
                        r02.A01 = new byte[i2];
                    }
                    int i4 = 0;
                    do {
                        iArr = r02.A02;
                        iArr[i4] = 0;
                        i4++;
                    } while (i4 < 32);
                    for (int i5 = 1; i5 < 5; i5++) {
                        byte[] A01 = r8.A01(r02.A01, (i3 * i5) / 5);
                        int i6 = (i2 << 2) / 5;
                        for (int i7 = i2 / 5; i7 < i6; i7++) {
                            int i8 = (A01[i7] & 255) >> 3;
                            iArr[i8] = iArr[i8] + 1;
                        }
                    }
                    int length = iArr.length;
                    int i9 = 0;
                    int i10 = 0;
                    int i11 = 0;
                    for (int i12 = 0; i12 < length; i12++) {
                        if (iArr[i12] > i9) {
                            i9 = iArr[i12];
                            i11 = i12;
                        }
                        if (iArr[i12] > i10) {
                            i10 = iArr[i12];
                        }
                    }
                    int i13 = 0;
                    int i14 = 0;
                    for (int i15 = 0; i15 < length; i15++) {
                        int i16 = i15 - i11;
                        int i17 = iArr[i15] * i16 * i16;
                        if (i17 > i14) {
                            i13 = i15;
                            i14 = i17;
                        }
                    }
                    if (i11 <= i13) {
                        i11 = i13;
                        i13 = i11;
                    }
                    if (i11 - i13 > (length >> 4)) {
                        int i18 = i11 - 1;
                        int i19 = i18;
                        int i20 = -1;
                        while (i18 > i13) {
                            int i21 = i18 - i13;
                            int i22 = i21 * i21 * (i11 - i18) * (i10 - iArr[i18]);
                            if (i22 > i20) {
                                i19 = i18;
                                i20 = i22;
                            }
                            i18--;
                        }
                        int i23 = i19 << 3;
                        byte[] A00 = r8.A00();
                        for (int i24 = 0; i24 < i3; i24++) {
                            int i25 = i24 * i2;
                            for (int i26 = 0; i26 < i2; i26++) {
                                if ((A00[i25 + i26] & 255) < i23) {
                                    r22.A01(i26, i24);
                                }
                            }
                        }
                    } else {
                        throw C82563vp.A00;
                    }
                } else {
                    byte[] A002 = r8.A00();
                    int i27 = i2 >> 3;
                    if ((i2 & 7) != 0) {
                        i27++;
                    }
                    int i28 = i3 >> 3;
                    if ((i3 & 7) != 0) {
                        i28++;
                    }
                    int i29 = i3 - 8;
                    int i30 = i2 - 8;
                    int[][] iArr2 = (int[][]) Array.newInstance(int.class, i28, i27);
                    for (int i31 = 0; i31 < i28; i31++) {
                        int i32 = i31 << 3;
                        if (i32 > i29) {
                            i32 = i29;
                        }
                        for (int i33 = 0; i33 < i27; i33++) {
                            int i34 = i33 << 3;
                            if (i34 > i30) {
                                i34 = i30;
                            }
                            int i35 = (i32 * i2) + i34;
                            int i36 = 255;
                            int i37 = 0;
                            int i38 = 0;
                            int i39 = 0;
                            do {
                                int i40 = 0;
                                do {
                                    int i41 = A002[i35 + i40] & 255;
                                    i38 += i41;
                                    if (i41 < i36) {
                                        i36 = i41;
                                    }
                                    if (i41 > i39) {
                                        i39 = i41;
                                    }
                                    i40++;
                                } while (i40 < 8);
                                i = i39 - i36;
                                if (i > 24) {
                                    while (true) {
                                        i37++;
                                        i35 += i2;
                                        if (i37 >= 8) {
                                            break;
                                        }
                                        int i42 = 0;
                                        do {
                                            i38 += A002[i35 + i42] & 255;
                                            i42++;
                                        } while (i42 < 8);
                                    }
                                }
                                i37++;
                                i35 += i2;
                            } while (i37 < 8);
                            int i43 = i38 >> 6;
                            if (i <= 24) {
                                i43 = i36 >> 1;
                                if (i31 > 0 && i33 > 0) {
                                    int[] iArr3 = iArr2[i31 - 1];
                                    int i44 = i33 - 1;
                                    int i45 = ((iArr3[i33] + (iArr2[i31][i44] << 1)) + iArr3[i44]) >> 2;
                                    if (i36 < i45) {
                                        i43 = i45;
                                    }
                                }
                            }
                            iArr2[i31][i33] = i43;
                        }
                    }
                    r22 = new C71183cW(i2, i3);
                    for (int i46 = 0; i46 < i28; i46++) {
                        int i47 = i46 << 3;
                        if (i47 > i29) {
                            i47 = i29;
                        }
                        int i48 = i28 - 3;
                        if (i46 < 2) {
                            i48 = 2;
                        } else if (i46 <= i48) {
                            i48 = i46;
                        }
                        for (int i49 = 0; i49 < i27; i49++) {
                            int i50 = i49 << 3;
                            if (i50 > i30) {
                                i50 = i30;
                            }
                            int i51 = i27 - 3;
                            if (i49 < 2) {
                                i51 = 2;
                            } else if (i49 <= i51) {
                                i51 = i49;
                            }
                            int i52 = -2;
                            int i53 = 0;
                            do {
                                int[] iArr4 = iArr2[i48 + i52];
                                i53 += iArr4[i51 - 2] + iArr4[i51 - 1] + iArr4[i51] + iArr4[i51 + 1] + iArr4[i51 + 2];
                                i52++;
                            } while (i52 <= 2);
                            int i54 = i53 / 25;
                            int i55 = (i47 * i2) + i50;
                            int i56 = 0;
                            while (i56 < 8) {
                                int i57 = 0;
                                do {
                                    if ((A002[i55 + i57] & 255) <= i54) {
                                        r22.A01(i50 + i57, i47 + i56);
                                    }
                                    i57++;
                                } while (i57 < 8);
                                i56++;
                                i55 += i2;
                            }
                        }
                    }
                }
                r02.A00 = r22;
            }
            this.A00 = r22;
        }
        return r22;
    }

    public String toString() {
        try {
            return A00().toString();
        } catch (C82563vp unused) {
            return "";
        }
    }
}
