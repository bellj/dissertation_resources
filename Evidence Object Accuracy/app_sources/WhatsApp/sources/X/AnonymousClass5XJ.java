package X;

/* renamed from: X.5XJ  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XJ {
    byte[] AAH();

    byte[] AG4(int i);

    byte[] calculateAgreement(byte[] bArr, byte[] bArr2);

    byte[] calculateSignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    byte[] generatePublicKey(byte[] bArr);

    boolean verifySignature(byte[] bArr, byte[] bArr2, byte[] bArr3);
}
