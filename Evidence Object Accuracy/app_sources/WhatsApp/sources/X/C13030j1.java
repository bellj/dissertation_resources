package X;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.google.firebase.iid.Registrar;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0j1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13030j1 {
    public static final Object A09 = new Object();
    public static final Map A0A = new AnonymousClass00N();
    public static final Executor A0B = new ExecutorC13040j2();
    public final Context A00;
    public final C13050j3 A01;
    public final C13160jF A02;
    public final C13200jJ A03;
    public final String A04;
    public final List A05 = new CopyOnWriteArrayList();
    public final List A06 = new CopyOnWriteArrayList();
    public final AtomicBoolean A07 = new AtomicBoolean(false);
    public final AtomicBoolean A08 = new AtomicBoolean();

    public C13030j1(Context context, C13050j3 r13, String str) {
        Throwable e;
        String str2;
        C13020j0.A01(context);
        this.A00 = context;
        C13020j0.A05(str);
        this.A04 = str;
        this.A01 = r13;
        C13070j5 r0 = new C13070j5(new C13060j4(), context);
        List<String> A00 = r0.A00.A00(r0.A01);
        ArrayList arrayList = new ArrayList();
        for (String str3 : A00) {
            try {
                Class<?> cls = Class.forName(str3);
                if (!Registrar.class.isAssignableFrom(cls)) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", str3, "com.google.firebase.components.ComponentRegistrar"));
                } else {
                    arrayList.add(cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                }
            } catch (ClassNotFoundException e2) {
                Log.w("ComponentDiscovery", String.format("Class %s is not an found.", str3), e2);
            } catch (IllegalAccessException | InstantiationException e3) {
                e = e3;
                str2 = String.format("Could not instantiate %s.", str3);
                Log.w("ComponentDiscovery", str2, e);
            } catch (NoSuchMethodException | InvocationTargetException e4) {
                e = e4;
                str2 = String.format("Could not instantiate %s", str3);
                Log.w("ComponentDiscovery", str2, e);
            }
        }
        Executor executor = A0B;
        C13090j8 r1 = new C13090j8(Context.class, new Class[0]);
        r1.A02 = new C13100j9(context);
        C13090j8 r12 = new C13090j8(C13030j1.class, new Class[0]);
        r12.A02 = new C13100j9(this);
        C13090j8 r14 = new C13090j8(C13050j3.class, new Class[0]);
        r14.A02 = new C13100j9(r13);
        C13090j8 r3 = new C13090j8(C13130jC.class, new Class[0]);
        r3.A01(new C13140jD(AnonymousClass00P.class, 2));
        C13150jE r15 = C13150jE.A00;
        C13020j0.A02(r15, "Null factory");
        r3.A02 = r15;
        this.A02 = new C13160jF(arrayList, executor, r1.A00(), r12.A00(), r14.A00(), C13120jB.A00("fire-android", ""), C13120jB.A00("fire-core", "19.0.0"), r3.A00());
        this.A03 = new C13200jJ(new AbstractC13190jI(context, this) { // from class: X.0jH
            public final Context A00;
            public final C13030j1 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC13190jI
            public Object get() {
                C13030j1 r4 = this.A01;
                Context context2 = this.A00;
                StringBuilder sb = new StringBuilder();
                r4.A02();
                byte[] bytes = r4.A04.getBytes(Charset.defaultCharset());
                String str4 = null;
                if (bytes != null) {
                    str4 = Base64.encodeToString(bytes, 11);
                }
                sb.append(str4);
                sb.append("+");
                r4.A02();
                byte[] bytes2 = r4.A01.A01.getBytes(Charset.defaultCharset());
                String str5 = null;
                if (bytes2 != null) {
                    str5 = Base64.encodeToString(bytes2, 11);
                }
                sb.append(str5);
                return new C15700nl(context2, (AbstractC13280jR) r4.A02.A02(AbstractC13280jR.class), sb.toString());
            }
        });
    }

    public static C13030j1 A00() {
        C13030j1 r0;
        synchronized (A09) {
            r0 = (C13030j1) A0A.get("[DEFAULT]");
            if (r0 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Default FirebaseApp is not initialized in this process ");
                sb.append(C13210jK.A00());
                sb.append(". Make sure to call FirebaseApp.initializeApp(Context) first.");
                throw new IllegalStateException(sb.toString());
            }
        }
        return r0;
    }

    public static C13030j1 A01(Context context) {
        C13030j1 r0;
        synchronized (A09) {
            Map map = A0A;
            if (map.containsKey("[DEFAULT]")) {
                r0 = A00();
            } else {
                C13220jL r1 = new C13220jL(context);
                String A00 = r1.A00("google_app_id");
                if (TextUtils.isEmpty(A00)) {
                    Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                    r0 = null;
                } else {
                    C13050j3 r7 = new C13050j3(A00, r1.A00("google_api_key"), r1.A00("firebase_database_url"), r1.A00("ga_trackingId"), r1.A00("gcm_defaultSenderId"), r1.A00("google_storage_bucket"), r1.A00("project_id"));
                    if (context.getApplicationContext() instanceof Application) {
                        Application application = (Application) context.getApplicationContext();
                        AtomicReference atomicReference = C13230jM.A00;
                        if (atomicReference.get() == null) {
                            C13230jM r2 = new C13230jM();
                            if (atomicReference.compareAndSet(null, r2)) {
                                ComponentCallbacks2C13250jO.A00(application);
                                ComponentCallbacks2C13250jO r12 = ComponentCallbacks2C13250jO.A04;
                                synchronized (r12) {
                                    r12.A01.add(r2);
                                }
                            }
                        }
                    }
                    String trim = "[DEFAULT]".trim();
                    if (context.getApplicationContext() != null) {
                        context = context.getApplicationContext();
                    }
                    boolean z = false;
                    if (!map.containsKey(trim)) {
                        z = true;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("FirebaseApp name ");
                    sb.append(trim);
                    sb.append(" already exists!");
                    C13020j0.A04(sb.toString(), z);
                    C13020j0.A02(context, "Application context cannot be null.");
                    r0 = new C13030j1(context, r7, trim);
                    map.put(trim, r0);
                    r0.A03();
                }
            }
        }
        return r0;
    }

    public final void A02() {
        C13020j0.A04("FirebaseApp was deleted", !this.A08.get());
    }

    public final void A03() {
        Queue queue;
        Context context = this.A00;
        if (Build.VERSION.SDK_INT < 24 || !(!AnonymousClass00Q.A00(context))) {
            C13160jF r5 = this.A02;
            A02();
            boolean equals = "[DEFAULT]".equals(this.A04);
            for (Map.Entry entry : r5.A01.entrySet()) {
                C13200jJ r2 = (C13200jJ) entry.getValue();
                int i = ((C13080j7) entry.getKey()).A00;
                if (i == 1 || (i == 2 && equals)) {
                    r2.get();
                }
            }
            C13270jQ r3 = r5.A00;
            synchronized (r3) {
                queue = r3.A00;
                if (queue != null) {
                    r3.A00 = null;
                } else {
                    queue = null;
                }
            }
            if (queue != null) {
                Iterator it = queue.iterator();
                while (it.hasNext()) {
                    it.next();
                    C13020j0.A01(null);
                    synchronized (r3) {
                        Queue queue2 = r3.A00;
                        if (queue2 != null) {
                            queue2.add(null);
                        }
                    }
                    synchronized (r3) {
                        throw new NullPointerException("getType");
                    }
                }
                return;
            }
            return;
        }
        C13260jP.A00(context);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C13030j1)) {
            return false;
        }
        String str = this.A04;
        C13030j1 r3 = (C13030j1) obj;
        r3.A02();
        return str.equals(r3.A04);
    }

    public int hashCode() {
        return this.A04.hashCode();
    }

    public String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(this.A04, "name");
        r2.A00(this.A01, "options");
        return r2.toString();
    }
}
