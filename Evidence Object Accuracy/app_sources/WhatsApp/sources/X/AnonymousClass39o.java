package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39o  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass39o extends Enum {
    public static final /* synthetic */ AnonymousClass39o[] A00;
    public static final AnonymousClass39o A01;
    public static final AnonymousClass39o A02;
    public static final AnonymousClass39o A03;
    public static final AnonymousClass39o A04;
    public static final AnonymousClass39o A05;
    public static final AnonymousClass39o A06;
    public static final AnonymousClass39o A07;
    public static final AnonymousClass39o A08;
    public static final AnonymousClass39o A09;
    public static final AnonymousClass39o A0A;
    public static final AnonymousClass39o A0B;
    public static final AnonymousClass39o A0C;
    public static final AnonymousClass39o A0D;
    public static final AnonymousClass39o A0E;
    public static final AnonymousClass39o A0F;
    public static final AnonymousClass39o A0G;
    public static final AnonymousClass39o A0H;
    public static final AnonymousClass39o A0I;
    public static final AnonymousClass39o A0J;
    public static final AnonymousClass39o A0K;
    public static final AnonymousClass39o A0L;
    public static final AnonymousClass39o A0M;
    public static final AnonymousClass39o A0N;
    public static final AnonymousClass39o A0O;
    public static final AnonymousClass39o A0P;
    public static final AnonymousClass39o A0Q;
    public static final AnonymousClass39o A0R;
    public static final AnonymousClass39o A0S;
    public static final AnonymousClass39o A0T;
    public static final AnonymousClass39o A0U;
    public static final AnonymousClass39o A0V;
    public static final AnonymousClass39o A0W;
    public static final AnonymousClass39o A0X;
    public static final AnonymousClass39o A0Y;
    public static final AnonymousClass39o A0Z;
    public static final AnonymousClass39o A0a;
    public static final AnonymousClass39o A0b;
    public static final AnonymousClass39o A0c;
    public static final AnonymousClass39o A0d;
    public static final AnonymousClass39o A0e;
    public static final AnonymousClass39o A0f;

    static {
        AnonymousClass39o A002 = A00("LOADING_INDICATOR", 0);
        A0D = A002;
        AnonymousClass39o A003 = A00("CATEGORY_SHIMMER", 1);
        A07 = A003;
        AnonymousClass39o A004 = A00("BUSINESS_PROFILE_SHIMMER", 2);
        A05 = A004;
        AnonymousClass39o A005 = A00("SEARCH_LOCATION", 3);
        A0Y = A005;
        AnonymousClass39o A006 = A00("DIVIDER", 4);
        A09 = A006;
        AnonymousClass39o A007 = A00("ROOT_CATEGORY", 5);
        A0W = A007;
        AnonymousClass39o A008 = A00("SUB_CATEGORY", 6);
        AnonymousClass39o A009 = A00("SECURITY_FOOTER", 7);
        A0c = A009;
        AnonymousClass39o A0010 = A00("BETA_FOOTER", 8);
        A01 = A0010;
        AnonymousClass39o A0011 = A00("FILTER_BAR", 9);
        A0A = A0011;
        AnonymousClass39o A0012 = A00("BUSINESS_PROFILE", 10);
        A02 = A0012;
        AnonymousClass39o A0013 = A00("BUSINESS_PROFILE_DIVIDER", 11);
        A03 = A0013;
        AnonymousClass39o A0014 = A00("LOAD_MORE_BUSINESS", 12);
        A0H = A0014;
        AnonymousClass39o A0015 = A00("LOAD_ALL_BUSINESS", 13);
        A0E = A0015;
        AnonymousClass39o A0016 = A00("NO_MORE_RESULTS", 14);
        A0R = A0016;
        AnonymousClass39o A0017 = A00("REFINE_RESULTS", 15);
        A0V = A0017;
        AnonymousClass39o A0018 = A00("LOAD_ALL_SUB_CATEGORIES", 16);
        A0F = A0018;
        AnonymousClass39o A0019 = A00("LOAD_ERROR", 17);
        A0G = A0019;
        AnonymousClass39o A0020 = A00("RETRY", 18);
        AnonymousClass39o A0021 = A00("DEFAULT_LOCATION", 19);
        A08 = A0021;
        AnonymousClass39o A0022 = A00("FINDING_LOCATION", 20);
        A0B = A0022;
        AnonymousClass39o A0023 = A00("FIND_LOCATION_ERROR", 21);
        A0C = A0023;
        AnonymousClass39o A0024 = A00("USE_CURRENT_LOCATION", 22);
        A0d = A0024;
        AnonymousClass39o A0025 = A00("NEIGHBOURHOODS_TITLE", 23);
        A0P = A0025;
        AnonymousClass39o A0026 = A00("NEIGHBOURHOOD_ITEM", 24);
        A0Q = A0026;
        AnonymousClass39o A0027 = A00("SET_LOCATION_FOOTER", 25);
        AnonymousClass39o A0028 = A00("LOCATION_NOT_SUPPORTED", 26);
        A0I = A0028;
        AnonymousClass39o A0029 = A00("SET_DIFFERENT_LOCATION", 27);
        AnonymousClass39o A0030 = A00("INTERACTIVE_MAP_ENTRYPOINT", 28);
        AnonymousClass39o A0031 = A00("HEADER", 29);
        AnonymousClass39o A0032 = A00("SEARCH_QUERY_CATEGORY", 30);
        A0Z = A0032;
        AnonymousClass39o A0033 = A00("SEARCH_QUERY_ERROR", 31);
        A0a = A0033;
        AnonymousClass39o A0034 = A00("RECENT_SEARCH", 32);
        A0U = A0034;
        AnonymousClass39o A0035 = A00("RECENT_SEARCH_CLEAR_ALL", 33);
        AnonymousClass39o A0036 = A00("SECOND_LEVEL_CATEGORY", 34);
        A0b = A0036;
        AnonymousClass39o A0037 = A00("BUSINESS_PROFILE_RECENT", 35);
        A04 = A0037;
        AnonymousClass39o A0038 = A00("LOCATION_SEARCH_NO_RESULT", 36);
        A0J = A0038;
        AnonymousClass39o A0039 = A00("SEARCH_HISTORY_BAR", 37);
        A0X = A0039;
        AnonymousClass39o A0040 = A00("POPULAR_CATEGORIES", 38);
        A0S = A0040;
        AnonymousClass39o A0041 = A00("POPULAR_CATEGORIES_SHIMMER", 39);
        A0T = A0041;
        AnonymousClass39o A0042 = A00("CATEGORY_BREADCRUMBS", 40);
        A06 = A0042;
        AnonymousClass39o A0043 = A00("USE_MANUAL_NEIGHBORHOOD", 41);
        A0e = A0043;
        AnonymousClass39o A0044 = A00("USE_MAP_TO_SET_LOCATION", 42);
        A0f = A0044;
        AnonymousClass39o A0045 = A00("NEARBY_BUSINESSES_HEADER", 43);
        A0L = A0045;
        AnonymousClass39o A0046 = A00("NEARBY_BUSINESSES_SHIMMER", 44);
        A0O = A0046;
        AnonymousClass39o A0047 = A00("NEARBY_BUSINESSES_EMPTY_LIST", 45);
        A0K = A0047;
        AnonymousClass39o A0048 = A00("NEARBY_BUSINESSES_LIST_WIDGET", 46);
        A0M = A0048;
        AnonymousClass39o A0049 = A00("NEARBY_BUSINESSES_LOCATION_REQUEST_WIDGET", 47);
        A0N = A0049;
        AnonymousClass39o[] r3 = new AnonymousClass39o[48];
        C12970iu.A1U(A002, A003, r3);
        C12980iv.A1P(A004, A005, A006, r3);
        C12970iu.A1R(A007, A008, A009, A0010, r3);
        r3[9] = A0011;
        r3[10] = A0012;
        r3[11] = A0013;
        r3[12] = A0014;
        r3[13] = A0015;
        r3[14] = A0016;
        r3[15] = A0017;
        r3[16] = A0018;
        r3[17] = A0019;
        r3[18] = A0020;
        r3[19] = A0021;
        C12960it.A1G(A0022, A0023, A0024, A0025, r3);
        r3[24] = A0026;
        C12960it.A1H(A0027, A0028, A0029, A0030, r3);
        C12970iu.A1S(A0031, A0032, A0033, r3);
        C12960it.A1I(A0034, A0035, A0036, A0037, r3);
        C12980iv.A1O(A0038, A0039, A0040, r3);
        r3[39] = A0041;
        r3[40] = A0042;
        r3[41] = A0043;
        r3[42] = A0044;
        r3[43] = A0045;
        r3[44] = A0046;
        r3[45] = A0047;
        r3[46] = A0048;
        r3[47] = A0049;
        A00 = r3;
    }

    public AnonymousClass39o(String str, int i) {
    }

    public static AnonymousClass39o A00(String str, int i) {
        return new AnonymousClass39o(str, i);
    }

    public static AnonymousClass39o valueOf(String str) {
        return (AnonymousClass39o) Enum.valueOf(AnonymousClass39o.class, str);
    }

    public static AnonymousClass39o[] values() {
        return (AnonymousClass39o[]) A00.clone();
    }
}
