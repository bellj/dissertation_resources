package X;

import java.io.File;

/* renamed from: X.1pJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC39111pJ {
    public AbstractC39001p6 A00;
    public final C39741qT A01;
    public final AbstractC14470lU A02;
    public final C39781qX A03;
    public final C39771qW A04;
    public final AbstractC39761qV A05;
    public final File A06;

    public AbstractC39111pJ(C39741qT r1, AbstractC14470lU r2, C39781qX r3, C39771qW r4, AbstractC39761qV r5, File file) {
        this.A02 = r2;
        this.A01 = r1;
        this.A05 = r5;
        this.A04 = r4;
        this.A06 = file;
        this.A03 = r3;
    }

    public void A00(int i) {
        this.A03.A00.A01.A04(Integer.valueOf(i));
    }

    public synchronized void A01(AbstractC39001p6 r2) {
        this.A00 = r2;
    }
}
