package X;

/* renamed from: X.4Wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92624Wr {
    public final int A00;
    public final Object A01;

    public C92624Wr(int i, Object obj) {
        this.A00 = i;
        this.A01 = obj;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92624Wr r5 = (C92624Wr) obj;
            if (this.A00 != r5.A00 || !AnonymousClass28V.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, this.A00);
        return C12960it.A06(this.A01, A1a);
    }
}
