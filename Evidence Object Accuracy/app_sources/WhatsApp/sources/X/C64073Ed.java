package X;

import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64073Ed {
    public boolean A00;
    public final Handler A01;
    public final Vibrator A02;
    public final View A03;
    public final AnonymousClass2ZR A04;
    public final Runnable A05;
    public final Runnable A06;

    public C64073Ed(Handler handler, View view, AnonymousClass01d r7, AnonymousClass018 r8, AnonymousClass2ZR r9) {
        this.A03 = view;
        this.A04 = r9;
        this.A01 = handler;
        view.setBackground(new AnonymousClass2GF(r9, r8));
        this.A06 = new RunnableC55552im(view, 0.0f, 1.0f, 0);
        this.A05 = new RunnableC55552im(view, 1.0f, 0.0f, 4);
        this.A02 = r7.A0K();
        A00();
    }

    public final void A00() {
        AnonymousClass2ZR r2 = this.A04;
        r2.A00 = C12960it.A09(this.A03).getColor(R.color.trashNormalBackground);
        r2.invalidateSelf();
    }

    public final boolean A01(float f, float f2) {
        View view = this.A03;
        return f >= ((float) view.getLeft()) && f <= ((float) view.getRight()) && f2 >= ((float) view.getTop()) && f2 <= ((float) view.getBottom());
    }
}
