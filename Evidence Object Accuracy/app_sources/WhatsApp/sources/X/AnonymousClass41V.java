package X;

import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;

/* renamed from: X.41V  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41V extends AnonymousClass2Dn {
    public final /* synthetic */ MediaAlbumActivity A00;

    public AnonymousClass41V(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        this.A00.A08.notifyDataSetChanged();
    }
}
