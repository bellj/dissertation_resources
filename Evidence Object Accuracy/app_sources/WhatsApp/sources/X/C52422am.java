package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.2am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52422am extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(50);
    public final String A00;
    public final String A01;

    public /* synthetic */ C52422am(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public /* synthetic */ C52422am(Parcelable parcelable, String str, String str2) {
        super(parcelable);
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
