package X;

/* renamed from: X.1by  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32321by {
    @Deprecated
    public static final String A00;
    @Deprecated
    public static final String A01;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        String str = C16500p8.A00;
        sb.append(str);
        sb.append(", ");
        sb.append("links.link_index AS link_index");
        sb.append(" FROM ");
        sb.append("available_message_view AS message, ");
        sb.append("(SELECT message_row_id, link_index FROM messages_links AS links WHERE links.key_remote_jid = ?) links");
        sb.append(" WHERE ");
        sb.append("message._id = links.message_row_id");
        sb.append(" ORDER BY _id DESC");
        A00 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT ");
        sb2.append(str);
        sb2.append(", ");
        sb2.append("links.link_index AS link_index");
        sb2.append(" FROM ");
        sb2.append(" (SELECT message_row_id, link_index FROM messages_links AS links JOIN jid AS jid ON jid.raw_string = links.key_remote_jid JOIN chat AS chat ON jid._id = chat.jid_row_id WHERE chat._id = ?) links");
        sb2.append(" JOIN ");
        sb2.append("available_message_view AS message");
        sb2.append(" ON ");
        sb2.append("message._id = links.message_row_id");
        A01 = sb2.toString();
    }
}
