package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3Lr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65943Lr implements Parcelable {
    public static final C65903Ln CREATOR = new C65903Ln();
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C65943Lr) {
                C65943Lr r5 = (C65943Lr) obj;
                if (!C16700pc.A0O(this.A06, r5.A06) || !C16700pc.A0O(this.A03, r5.A03) || this.A00 != r5.A00 || !C16700pc.A0O(this.A05, r5.A05) || this.A01 != r5.A01 || this.A02 != r5.A02 || !C16700pc.A0O(this.A04, r5.A04)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((((((((((this.A06.hashCode() * 31) + this.A03.hashCode()) * 31) + this.A00) * 31) + this.A05.hashCode()) * 31) + this.A01) * 31) + this.A02) * 31) + this.A04.hashCode();
    }

    public C65943Lr(String str, String str2, String str3, String str4, int i, int i2, int i3) {
        this.A06 = str;
        this.A03 = str2;
        this.A00 = i;
        this.A05 = str3;
        this.A01 = i2;
        this.A02 = i3;
        this.A04 = str4;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarSticker(url=");
        A0k.append(this.A06);
        A0k.append(", emojis=");
        A0k.append(this.A03);
        A0k.append(", fileSize=");
        A0k.append(this.A00);
        A0k.append(", mimeType=");
        A0k.append(this.A05);
        A0k.append(", height=");
        A0k.append(this.A01);
        A0k.append(", width=");
        A0k.append(this.A02);
        A0k.append(", fileHash=");
        A0k.append(this.A04);
        return C12970iu.A0u(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeString(this.A06);
        parcel.writeString(this.A03);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A05);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A02);
        parcel.writeString(this.A04);
    }
}
