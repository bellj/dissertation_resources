package X;

/* renamed from: X.5bV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118245bV extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;

    public C118245bV(C128375w0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123355n1.class)) {
            C128375w0 r0 = this.A00;
            C16590pI r5 = r0.A0B;
            C14830m7 r4 = r0.A0A;
            C14850m9 r7 = r0.A0J;
            C15570nT r3 = r0.A03;
            AnonymousClass12P r2 = r0.A01;
            C20920wX r1 = r0.A00;
            C130155yt r9 = r0.A0W;
            AnonymousClass61F r11 = r0.A0d;
            return new C123355n1(r1, r2, r3, r4, r5, r0.A0H, r7, r0.A0S, r9, r0.A0a, r11, r0.A0o);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviPayHubPersonalInfoViewModel");
    }
}
