package X;

import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.components.Button;

/* renamed from: X.6Dn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134156Dn implements AnonymousClass5Wu {
    public View A00;
    public ProgressBar A01;
    public Button A02;

    public final void A00(C127445uV r4) {
        float f;
        if (r4 != null) {
            this.A02.setText(r4.A01);
            Button button = this.A02;
            boolean z = r4.A02;
            button.setEnabled(z);
            if (C28391Mz.A02()) {
                View view = this.A00;
                if (z) {
                    f = view.getResources().getDimension(R.dimen.novi_pay_button_elevation);
                } else {
                    f = 0.0f;
                }
                view.setElevation(f);
            }
            this.A02.setOnClickListener(r4.A00);
        }
    }

    /* renamed from: A01 */
    public void A6Q(AnonymousClass4OZ r5) {
        if (r5 != null) {
            int i = r5.A00;
            if (!(i == -2 || i == -1)) {
                if (i == 0) {
                    this.A00.setVisibility(8);
                    return;
                } else if (i == 1) {
                    this.A00.setVisibility(0);
                    A00((C127445uV) r5.A01);
                    this.A02.setText("");
                    this.A02.setOnClickListener(null);
                    this.A01.setVisibility(0);
                    return;
                } else if (i != 2) {
                    return;
                }
            }
            this.A00.setVisibility(0);
            this.A01.setVisibility(8);
            A00((C127445uV) r5.A01);
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        if (!(this instanceof C121735jr)) {
            return R.layout.novi_bottom_button_view;
        }
        return R.layout.novi_send_button_view;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A02 = (Button) AnonymousClass028.A0D(view, R.id.bottom_button);
        this.A01 = (ProgressBar) AnonymousClass028.A0D(view, R.id.bottom_button_progressbar);
        this.A01.getIndeterminateDrawable().setColorFilter(AnonymousClass00T.A00(view.getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
    }
}
