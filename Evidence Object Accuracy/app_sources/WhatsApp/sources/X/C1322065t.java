package X;

/* renamed from: X.65t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1322065t implements AbstractC009404s {
    public final /* synthetic */ AbstractC16870pt A00;
    public final /* synthetic */ C128355vy A01;

    public C1322065t(AbstractC16870pt r1, C128355vy r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C128355vy r0 = this.A01;
        C14830m7 r1 = r0.A09;
        C14850m9 r5 = r0.A0I;
        AbstractC14440lR r14 = r0.A0k;
        AnonymousClass018 r2 = r0.A0C;
        C17070qD r9 = r0.A0W;
        C18600si r8 = r0.A0R;
        C21860y6 r7 = r0.A0P;
        AbstractC16870pt r11 = this.A00;
        AnonymousClass102 r4 = r0.A0G;
        C126885tb r12 = r0.A0b;
        return new C123505nG(r1, r2, r0.A0F, r4, r5, r0.A0N, r7, r8, r9, r0.A0X, r11, r12, r0.A0d, r14);
    }
}
