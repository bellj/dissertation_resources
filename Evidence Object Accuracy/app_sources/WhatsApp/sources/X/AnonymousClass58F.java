package X;

/* renamed from: X.58F  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58F implements AnonymousClass5UW {
    public static final AnonymousClass58S A01 = new AnonymousClass58S();
    public final AnonymousClass5UW A00;

    public AnonymousClass58F(AnonymousClass5UW r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r2) {
        C16700pc.A0E(r2, 0);
        return !this.A00.A9g(r2);
    }
}
