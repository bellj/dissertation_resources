package X;

/* renamed from: X.299  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass299 extends AnonymousClass28Y {
    public final AnonymousClass1Mr list;

    public AnonymousClass299(AnonymousClass1Mr r2, int i) {
        super(r2.size(), i);
        this.list = r2;
    }

    @Override // X.AnonymousClass28Y
    public Object get(int i) {
        return this.list.get(i);
    }
}
