package X;

import java.io.File;

/* renamed from: X.45F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45F extends AbstractC39731qS {
    public final int A00;
    public final boolean A01;

    public AnonymousClass45F(File file, String str, byte[] bArr, int i, boolean z, boolean z2) {
        super(file, str, bArr, z);
        this.A00 = i;
        this.A01 = z2;
    }
}
