package X;

import android.content.ClipData;
import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/* renamed from: X.0Y8  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Y8 implements AbstractC11750gn {
    @Override // X.AbstractC11750gn
    public AnonymousClass0SR AUe(View view, AnonymousClass0SR r13) {
        CharSequence coerceToStyledText;
        if (Log.isLoggable("ReceiveContent", 3)) {
            StringBuilder sb = new StringBuilder("onReceive: ");
            sb.append(r13);
            Log.d("ReceiveContent", sb.toString());
        }
        AbstractC12630iE r2 = r13.A00;
        if (r2.AGp() == 2) {
            return r13;
        }
        ClipData ABQ = r2.ABQ();
        int AD4 = r2.AD4();
        TextView textView = (TextView) view;
        Editable editable = (Editable) textView.getText();
        Context context = textView.getContext();
        boolean z = false;
        for (int i = 0; i < ABQ.getItemCount(); i++) {
            ClipData.Item itemAt = ABQ.getItemAt(i);
            if ((AD4 & 1) != 0) {
                coerceToStyledText = itemAt.coerceToText(context);
                if (coerceToStyledText instanceof Spanned) {
                    coerceToStyledText = coerceToStyledText.toString();
                }
            } else {
                coerceToStyledText = itemAt.coerceToStyledText(context);
            }
            if (coerceToStyledText != null) {
                if (!z) {
                    int selectionStart = Selection.getSelectionStart(editable);
                    int selectionEnd = Selection.getSelectionEnd(editable);
                    int max = Math.max(0, Math.min(selectionStart, selectionEnd));
                    int max2 = Math.max(0, Math.max(selectionStart, selectionEnd));
                    Selection.setSelection(editable, max2);
                    editable.replace(max, max2, coerceToStyledText);
                    z = true;
                } else {
                    editable.insert(Selection.getSelectionEnd(editable), "\n");
                    editable.insert(Selection.getSelectionEnd(editable), coerceToStyledText);
                }
            }
        }
        return null;
    }
}
