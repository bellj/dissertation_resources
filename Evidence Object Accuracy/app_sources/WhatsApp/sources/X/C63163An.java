package X;

import java.util.Map;

/* renamed from: X.3An  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63163An {
    public static final AnonymousClass3FA A00(AnonymousClass3EW r6) {
        long j = r6.A00;
        String str = r6.A03;
        C16700pc.A0B(str);
        AnonymousClass3FA r5 = new AnonymousClass3FA(str, null, j);
        String str2 = r6.A02;
        if (str2 == null) {
            return r5;
        }
        long j2 = r5.A00;
        String str3 = r5.A01;
        C16700pc.A0E(str3, 1);
        return new AnonymousClass3FA(str3, (Map) C94734cS.A00(str2).A01("$", new AnonymousClass5T6[0]), j2);
    }
}
