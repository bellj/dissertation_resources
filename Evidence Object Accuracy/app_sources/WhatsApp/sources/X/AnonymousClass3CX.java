package X;

import android.util.Log;
import java.util.LinkedList;
import java.util.Queue;

/* renamed from: X.3CX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CX {
    public boolean A00 = true;
    public final Queue A01 = new LinkedList();

    public void A00(Runnable runnable) {
        StringBuilder A0k = C12960it.A0k("BloksCallbackQueue/run/active=");
        A0k.append(this.A00);
        Log.d("Whatsapp", A0k.toString());
        if (this.A00) {
            runnable.run();
        } else {
            this.A01.add(runnable);
        }
    }
}
