package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1vW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ServiceConnectionC42621vW implements ServiceConnection {
    public final /* synthetic */ ConversationsFragment A00;

    public ServiceConnectionC42621vW(ConversationsFragment conversationsFragment) {
        this.A00 = conversationsFragment;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ConversationsFragment conversationsFragment = this.A00;
        conversationsFragment.A0z = new C42591vT(conversationsFragment);
        conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this, 28));
        Log.i("conversations/gdrive-service-connected");
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        ConversationsFragment conversationsFragment = this.A00;
        AnonymousClass10J r0 = conversationsFragment.A0T;
        r0.A01.A04(conversationsFragment.A0z);
        Log.i("conversations/gdrive-service-disconnected");
    }
}
