package X;

import android.util.Log;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;

/* renamed from: X.0Iq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03660Iq extends InputStream implements DataInput {
    public static final ByteOrder A04 = ByteOrder.BIG_ENDIAN;
    public static final ByteOrder A05 = ByteOrder.LITTLE_ENDIAN;
    public int A00;
    public ByteOrder A01;
    public byte[] A02;
    public final DataInputStream A03;

    public C03660Iq(InputStream inputStream, ByteOrder byteOrder) {
        this.A01 = ByteOrder.BIG_ENDIAN;
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        this.A03 = dataInputStream;
        dataInputStream.mark(0);
        this.A00 = 0;
        this.A01 = byteOrder;
    }

    public C03660Iq(byte[] bArr) {
        this(new ByteArrayInputStream(bArr), ByteOrder.BIG_ENDIAN);
    }

    public void A00(int i) {
        int i2 = 0;
        while (i2 < i) {
            DataInputStream dataInputStream = this.A03;
            int i3 = i - i2;
            int skip = (int) dataInputStream.skip((long) i3);
            if (skip <= 0) {
                byte[] bArr = this.A02;
                if (bArr == null) {
                    bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                    this.A02 = bArr;
                }
                skip = dataInputStream.read(bArr, 0, Math.min((int) DefaultCrypto.BUFFER_SIZE, i3));
                if (skip == -1) {
                    StringBuilder sb = new StringBuilder("Reached EOF while skipping ");
                    sb.append(i);
                    sb.append(" bytes.");
                    throw new EOFException(sb.toString());
                }
            }
            i2 += skip;
        }
        this.A00 += i2;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.A03.available();
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        throw new UnsupportedOperationException("Mark is currently unsupported");
    }

    @Override // java.io.InputStream
    public int read() {
        this.A00++;
        return this.A03.read();
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int read = this.A03.read(bArr, i, i2);
        this.A00 += read;
        return read;
    }

    @Override // java.io.DataInput
    public boolean readBoolean() {
        this.A00++;
        return this.A03.readBoolean();
    }

    @Override // java.io.DataInput
    public byte readByte() {
        this.A00++;
        int read = this.A03.read();
        if (read >= 0) {
            return (byte) read;
        }
        throw new EOFException();
    }

    @Override // java.io.DataInput
    public char readChar() {
        this.A00 += 2;
        return this.A03.readChar();
    }

    @Override // java.io.DataInput
    public double readDouble() {
        return Double.longBitsToDouble(readLong());
    }

    @Override // java.io.DataInput
    public float readFloat() {
        return Float.intBitsToFloat(readInt());
    }

    @Override // java.io.DataInput
    public void readFully(byte[] bArr) {
        this.A00 += bArr.length;
        this.A03.readFully(bArr);
    }

    @Override // java.io.DataInput
    public void readFully(byte[] bArr, int i, int i2) {
        this.A00 += i2;
        this.A03.readFully(bArr, i, i2);
    }

    @Override // java.io.DataInput
    public int readInt() {
        this.A00 += 4;
        DataInputStream dataInputStream = this.A03;
        int read = dataInputStream.read();
        int read2 = dataInputStream.read();
        int read3 = dataInputStream.read();
        int read4 = dataInputStream.read();
        if ((read | read2 | read3 | read4) >= 0) {
            ByteOrder byteOrder = this.A01;
            if (byteOrder == A05) {
                return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
            }
            if (byteOrder == A04) {
                return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
            }
            StringBuilder sb = new StringBuilder("Invalid byte order: ");
            sb.append(byteOrder);
            throw new IOException(sb.toString());
        }
        throw new EOFException();
    }

    @Override // java.io.DataInput
    public String readLine() {
        Log.d("ExifInterface", "Currently unsupported");
        return null;
    }

    @Override // java.io.DataInput
    public long readLong() {
        long j;
        long j2;
        this.A00 += 8;
        DataInputStream dataInputStream = this.A03;
        int read = dataInputStream.read();
        int read2 = dataInputStream.read();
        int read3 = dataInputStream.read();
        int read4 = dataInputStream.read();
        int read5 = dataInputStream.read();
        int read6 = dataInputStream.read();
        int read7 = dataInputStream.read();
        int read8 = dataInputStream.read();
        if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
            ByteOrder byteOrder = this.A01;
            if (byteOrder == A05) {
                j = (((long) read8) << 56) + (((long) read7) << 48) + (((long) read6) << 40) + (((long) read5) << 32) + (((long) read4) << 24) + (((long) read3) << 16) + (((long) read2) << 8);
                j2 = (long) read;
            } else if (byteOrder == A04) {
                j = (((long) read) << 56) + (((long) read2) << 48) + (((long) read3) << 40) + (((long) read4) << 32) + (((long) read5) << 24) + (((long) read6) << 16) + (((long) read7) << 8);
                j2 = (long) read8;
            } else {
                StringBuilder sb = new StringBuilder("Invalid byte order: ");
                sb.append(byteOrder);
                throw new IOException(sb.toString());
            }
            return j + j2;
        }
        throw new EOFException();
    }

    @Override // java.io.DataInput
    public short readShort() {
        int i;
        this.A00 += 2;
        DataInputStream dataInputStream = this.A03;
        int read = dataInputStream.read();
        int read2 = dataInputStream.read();
        if ((read | read2) >= 0) {
            ByteOrder byteOrder = this.A01;
            if (byteOrder == A05) {
                i = (read2 << 8) + read;
            } else if (byteOrder == A04) {
                i = (read << 8) + read2;
            } else {
                StringBuilder sb = new StringBuilder("Invalid byte order: ");
                sb.append(byteOrder);
                throw new IOException(sb.toString());
            }
            return (short) i;
        }
        throw new EOFException();
    }

    @Override // java.io.DataInput
    public String readUTF() {
        this.A00 += 2;
        return this.A03.readUTF();
    }

    @Override // java.io.DataInput
    public int readUnsignedByte() {
        this.A00++;
        return this.A03.readUnsignedByte();
    }

    @Override // java.io.DataInput
    public int readUnsignedShort() {
        this.A00 += 2;
        DataInputStream dataInputStream = this.A03;
        int read = dataInputStream.read();
        int read2 = dataInputStream.read();
        if ((read | read2) >= 0) {
            ByteOrder byteOrder = this.A01;
            if (byteOrder == A05) {
                return (read2 << 8) + read;
            }
            if (byteOrder == A04) {
                return (read << 8) + read2;
            }
            StringBuilder sb = new StringBuilder("Invalid byte order: ");
            sb.append(byteOrder);
            throw new IOException(sb.toString());
        }
        throw new EOFException();
    }

    @Override // java.io.InputStream
    public void reset() {
        throw new UnsupportedOperationException("Reset is currently unsupported");
    }

    @Override // java.io.DataInput
    public int skipBytes(int i) {
        throw new UnsupportedOperationException("skipBytes is currently unsupported");
    }
}
