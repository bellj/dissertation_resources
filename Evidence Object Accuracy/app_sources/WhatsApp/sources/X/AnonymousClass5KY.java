package X;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/* renamed from: X.5KY  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KY extends AbstractC114055Ka {
    @Override // X.AbstractC114055Ka
    public Random A01() {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        C16700pc.A0B(current);
        return current;
    }
}
