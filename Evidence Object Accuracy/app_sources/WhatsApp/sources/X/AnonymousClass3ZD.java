package X;

import java.util.Set;

/* renamed from: X.3ZD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZD implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ AnonymousClass1BO A01;
    public final /* synthetic */ Set A02;

    public AnonymousClass3ZD(AnonymousClass016 r1, AnonymousClass1BO r2, Set set) {
        this.A01 = r2;
        this.A02 = set;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A00.A0A(Boolean.FALSE);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A00.A0A(Boolean.FALSE);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        AnonymousClass016 r1;
        Boolean bool;
        String A0I = r5.A0F("privacy").A0F("category").A0I("dhash", null);
        if (A0I != null) {
            this.A01.A04(A0I, this.A02, true);
            r1 = this.A00;
            bool = Boolean.TRUE;
        } else {
            this.A01.A00();
            r1 = this.A00;
            bool = Boolean.FALSE;
        }
        r1.A0A(bool);
    }
}
