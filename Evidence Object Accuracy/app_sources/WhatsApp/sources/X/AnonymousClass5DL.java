package X;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DL  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5DL implements Iterator {
    public int currentIndex;
    public int expectedMetadata;
    public int indexToRemove;
    public final /* synthetic */ AnonymousClass5I4 this$0;

    public abstract Object getOutput(int i);

    public AnonymousClass5DL(AnonymousClass5I4 r2) {
        this.this$0 = r2;
        this.expectedMetadata = r2.metadata;
        this.currentIndex = r2.firstEntryIndex();
        this.indexToRemove = -1;
    }

    public /* synthetic */ AnonymousClass5DL(AnonymousClass5I4 r2, C80963tF r3) {
        this(r2);
    }

    private void checkForConcurrentModification() {
        if (this.this$0.metadata != this.expectedMetadata) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1W(this.currentIndex);
    }

    public void incrementExpectedModCount() {
        this.expectedMetadata += 32;
    }

    @Override // java.util.Iterator
    public Object next() {
        checkForConcurrentModification();
        if (hasNext()) {
            int i = this.currentIndex;
            this.indexToRemove = i;
            Object output = getOutput(i);
            this.currentIndex = this.this$0.getSuccessor(i);
            return output;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        checkForConcurrentModification();
        C28251Mi.checkRemove(C12990iw.A1W(this.indexToRemove));
        incrementExpectedModCount();
        AnonymousClass5I4 r1 = this.this$0;
        r1.remove(r1.key(this.indexToRemove));
        this.currentIndex = this.this$0.adjustAfterRemove(this.currentIndex, this.indexToRemove);
        this.indexToRemove = -1;
    }
}
