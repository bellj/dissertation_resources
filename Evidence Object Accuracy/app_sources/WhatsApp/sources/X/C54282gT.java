package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54282gT extends AnonymousClass02M {
    public final List A00;

    public C54282gT(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC37191le r22 = (AbstractC37191le) r2;
        r22.A08();
        r22.A09(this.A00.get(i));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (AnonymousClass39o.values()[i].ordinal()) {
            case 22:
                return new C848540b(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.row_location_picker_option));
            case 41:
                return new C848740d(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.row_location_picker_option));
            case 42:
                return new C848640c(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.row_location_picker_option));
            default:
                throw C12960it.A0U(C12960it.A0W(i, "LocationOptionPickerAdapter/onCreateViewHolder type not handled: "));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((C37171lc) this.A00.get(i)).A00.ordinal();
    }
}
