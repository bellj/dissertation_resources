package X;

import android.app.Activity;
import android.graphics.Rect;

/* renamed from: X.0LJ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LJ {
    public static final Rect A00(Activity activity) {
        Rect bounds = activity.getWindowManager().getCurrentWindowMetrics().getBounds();
        C16700pc.A0B(bounds);
        return bounds;
    }
}
