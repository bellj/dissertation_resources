package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;

/* renamed from: X.3sy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80793sy extends AbstractC80933tC<K, V>.SortedAsMap implements NavigableMap<K, Collection<V>> {
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80793sy(AbstractC80933tC r1, NavigableMap navigableMap) {
        super(r1, navigableMap);
        this.this$0 = r1;
    }

    @Override // java.util.NavigableMap
    public Map.Entry ceilingEntry(Object obj) {
        Map.Entry ceilingEntry = sortedMap().ceilingEntry(obj);
        if (ceilingEntry == null) {
            return null;
        }
        return wrapEntry(ceilingEntry);
    }

    @Override // java.util.NavigableMap
    public Object ceilingKey(Object obj) {
        return sortedMap().ceilingKey(obj);
    }

    public NavigableSet createKeySet() {
        return new C80813t0(this.this$0, sortedMap());
    }

    @Override // java.util.NavigableMap
    public NavigableSet descendingKeySet() {
        return descendingMap().navigableKeySet();
    }

    @Override // java.util.NavigableMap
    public NavigableMap descendingMap() {
        return new C80793sy(this.this$0, sortedMap().descendingMap());
    }

    @Override // java.util.NavigableMap
    public Map.Entry firstEntry() {
        Map.Entry firstEntry = sortedMap().firstEntry();
        if (firstEntry == null) {
            return null;
        }
        return wrapEntry(firstEntry);
    }

    @Override // java.util.NavigableMap
    public Map.Entry floorEntry(Object obj) {
        Map.Entry floorEntry = sortedMap().floorEntry(obj);
        if (floorEntry == null) {
            return null;
        }
        return wrapEntry(floorEntry);
    }

    @Override // java.util.NavigableMap
    public Object floorKey(Object obj) {
        return sortedMap().floorKey(obj);
    }

    @Override // java.util.NavigableMap, java.util.SortedMap
    public NavigableMap headMap(Object obj) {
        return headMap(obj, false);
    }

    @Override // java.util.NavigableMap
    public NavigableMap headMap(Object obj, boolean z) {
        return new C80793sy(this.this$0, sortedMap().headMap(obj, z));
    }

    @Override // java.util.NavigableMap
    public Map.Entry higherEntry(Object obj) {
        Map.Entry higherEntry = sortedMap().higherEntry(obj);
        if (higherEntry == null) {
            return null;
        }
        return wrapEntry(higherEntry);
    }

    @Override // java.util.NavigableMap
    public Object higherKey(Object obj) {
        return sortedMap().higherKey(obj);
    }

    @Override // java.util.Map, java.util.SortedMap
    public NavigableSet keySet() {
        return (NavigableSet) super.keySet();
    }

    @Override // java.util.NavigableMap
    public Map.Entry lastEntry() {
        Map.Entry lastEntry = sortedMap().lastEntry();
        if (lastEntry == null) {
            return null;
        }
        return wrapEntry(lastEntry);
    }

    @Override // java.util.NavigableMap
    public Map.Entry lowerEntry(Object obj) {
        Map.Entry lowerEntry = sortedMap().lowerEntry(obj);
        if (lowerEntry == null) {
            return null;
        }
        return wrapEntry(lowerEntry);
    }

    @Override // java.util.NavigableMap
    public Object lowerKey(Object obj) {
        return sortedMap().lowerKey(obj);
    }

    @Override // java.util.NavigableMap
    public NavigableSet navigableKeySet() {
        return keySet();
    }

    public Map.Entry pollAsMapEntry(Iterator it) {
        if (!it.hasNext()) {
            return null;
        }
        Map.Entry A15 = C12970iu.A15(it);
        Collection createCollection = this.this$0.createCollection();
        createCollection.addAll((Collection) A15.getValue());
        it.remove();
        return C28271Mk.immutableEntry(A15.getKey(), this.this$0.unmodifiableCollectionSubclass(createCollection));
    }

    @Override // java.util.NavigableMap
    public Map.Entry pollFirstEntry() {
        return pollAsMapEntry(C12990iw.A0s(this));
    }

    @Override // java.util.NavigableMap
    public Map.Entry pollLastEntry() {
        return pollAsMapEntry(descendingMap().entrySet().iterator());
    }

    public NavigableMap sortedMap() {
        return (NavigableMap) super.sortedMap();
    }

    @Override // java.util.NavigableMap, java.util.SortedMap
    public NavigableMap subMap(Object obj, Object obj2) {
        return subMap(obj, true, obj2, false);
    }

    @Override // java.util.NavigableMap
    public NavigableMap subMap(Object obj, boolean z, Object obj2, boolean z2) {
        return new C80793sy(this.this$0, sortedMap().subMap(obj, z, obj2, z2));
    }

    @Override // java.util.NavigableMap, java.util.SortedMap
    public NavigableMap tailMap(Object obj) {
        return tailMap(obj, true);
    }

    @Override // java.util.NavigableMap
    public NavigableMap tailMap(Object obj, boolean z) {
        return new C80793sy(this.this$0, sortedMap().tailMap(obj, z));
    }
}
