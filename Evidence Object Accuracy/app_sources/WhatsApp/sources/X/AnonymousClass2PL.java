package X;

import com.whatsapp.jid.UserJid;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2PL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PL extends AnonymousClass1JW {
    public List A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final UserJid A04;
    public final String A05;
    public final boolean A06;
    public final boolean A07;

    public AnonymousClass2PL(AbstractC15710nm r1, C15450nH r2, UserJid userJid, String str, List list, int i, int i2, int i3, boolean z, boolean z2) {
        super(r1, r2);
        this.A05 = str;
        this.A07 = z;
        this.A01 = i;
        this.A06 = z2;
        this.A03 = i2;
        this.A04 = userJid;
        this.A02 = i3;
        this.A00 = list == null ? Collections.emptyList() : list;
    }
}
