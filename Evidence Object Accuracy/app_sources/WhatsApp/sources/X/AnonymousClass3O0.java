package X;

import android.view.animation.Animation;
import android.widget.AbsListView;
import com.whatsapp.emoji.EmojiPopupFooter;

/* renamed from: X.3O0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3O0 implements AbsListView.OnScrollListener {
    public final /* synthetic */ C15270mq A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public AnonymousClass3O0(C15270mq r1) {
        this.A00 = r1;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        C15270mq r3 = this.A00;
        int height = r3.A08.getHeight();
        int i4 = r3.A02;
        if (i4 >= i) {
            height = -1;
            if (i4 > i) {
                height = 0;
            }
        }
        r3.A02 = i;
        if (height >= 0) {
            EmojiPopupFooter emojiPopupFooter = r3.A08;
            if (height != emojiPopupFooter.A00) {
                Animation animation = emojiPopupFooter.getAnimation();
                if (!(animation instanceof C52592bM) || ((C52592bM) animation).A00 != height) {
                    if (animation != null) {
                        animation.cancel();
                    }
                    r3.A08.startAnimation(new C52592bM(r3, height));
                }
            }
        }
    }
}
