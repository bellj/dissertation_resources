package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayDeque;
import java.util.Deque;

/* renamed from: X.0QB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QB {
    public AnonymousClass0BX A00;
    public AnonymousClass0BY A01;
    public C05150Ol A02;
    public AnonymousClass0PL A03;
    public EnumC03910Jp A04;
    public AnonymousClass09V A05;
    public Integer A06;
    public boolean A07 = false;
    public final Deque A08 = new ArrayDeque();
    public final Deque A09 = new ArrayDeque();
    public final Deque A0A = new ArrayDeque();
    public final Deque A0B = new ArrayDeque();

    public final int A00() {
        Number number = (Number) this.A0A.peek();
        if (number != null) {
            return number.intValue();
        }
        C28691Op.A00("CDSBloksBottomSheetDelegate", "Attempting to check the current keyboard soft input mode but found null.");
        return 32;
    }

    public final void A01(int i) {
        C14260l7 r0 = (C14260l7) this.A09.peek();
        if (r0 != null) {
            Context A00 = r0.A00();
            if (A00 instanceof Activity) {
                Activity activity = (Activity) A00;
                if (activity.getWindow() != null) {
                    activity.getWindow().setSoftInputMode(i);
                }
            }
        }
    }

    public final void A02(Context context) {
        AnonymousClass0OS r2 = (AnonymousClass0OS) this.A0B.pop();
        this.A0A.pop();
        A01(A00());
        Deque deque = this.A09;
        if (!deque.isEmpty()) {
            deque.pop();
            this.A08.pop();
        }
        AnonymousClass0BX r0 = this.A00;
        if (r0 != null) {
            View primaryChild = r0.A01.getPrimaryChild();
            if (primaryChild != null) {
                r2.A00.A06();
                primaryChild.addOnAttachStateChangeListener(new AnonymousClass0W7(r2));
                A03(context);
                return;
            }
            throw new IllegalStateException("Bottom sheet layout pager must have a non-null view.");
        }
        throw new IllegalStateException("Cannot pop Screen content without initializing the CDS bottom sheet. Please call onCreateDialog() and onCreateView().");
    }

    public final void A03(Context context) {
        AnonymousClass0OS r3 = (AnonymousClass0OS) this.A0B.peek();
        if (r3 == null) {
            throw new IllegalStateException("Cannot pop Screen content with an empty CDS bottom sheet or full screen.");
        } else if (this.A00 != null) {
            this.A00.A01.A03((View) r3.A00.A02(context).A00, null, false);
            C55982k5 A00 = r3.A00();
            AnonymousClass0BX r0 = this.A00;
            if (r0 != null) {
                ViewGroup viewGroup = r0.A00;
                viewGroup.removeAllViews();
                viewGroup.addView(A00);
            }
        } else {
            throw new IllegalStateException("Cannot pop Screen content without initializing the CDS bottom sheet. Please call onCreateDialog() and onCreateView().");
        }
    }

    public final void A04(Context context, AnonymousClass0OS r5, EnumC03850Jj r6, C14260l7 r7, int i) {
        if (this.A00 != null) {
            this.A00.A01.A03((View) r5.A00.A02(context).A00, r6, true);
            C55982k5 A00 = r5.A00();
            AnonymousClass0BX r0 = this.A00;
            if (r0 != null) {
                ViewGroup viewGroup = r0.A00;
                viewGroup.removeAllViews();
                viewGroup.addView(A00);
            }
            this.A0B.push(r5);
            this.A0A.push(Integer.valueOf(i));
            if (r7 != null) {
                this.A09.push(r7);
                this.A08.push(new AnonymousClass0LR());
            }
            A01(A00());
            return;
        }
        throw new IllegalStateException("Cannot push Screen content without initializing the CDS bottom sheet. Please call onCreateDialog() and onCreateView().");
    }
}
