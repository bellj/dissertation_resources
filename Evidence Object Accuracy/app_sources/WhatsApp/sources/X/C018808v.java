package X;

/* renamed from: X.08v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C018808v extends AnonymousClass016 {
    public AnonymousClass07E A00;
    public String A01;

    public C018808v(AnonymousClass07E r1, Object obj, String str) {
        super(obj);
        this.A01 = str;
        this.A00 = r1;
    }

    public C018808v(AnonymousClass07E r1, String str) {
        this.A01 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass017
    public void A0B(Object obj) {
        AnonymousClass07E r0 = this.A00;
        if (r0 != null) {
            r0.A02.put(this.A01, obj);
        }
        super.A0B(obj);
    }
}
