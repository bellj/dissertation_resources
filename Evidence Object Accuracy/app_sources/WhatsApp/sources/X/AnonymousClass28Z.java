package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.28Z  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass28Z implements Iterator {
    INSTANCE;

    @Override // java.util.Iterator
    public boolean hasNext() {
        return false;
    }

    @Override // java.util.Iterator
    public Object next() {
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        C28251Mi.checkRemove(false);
    }
}
