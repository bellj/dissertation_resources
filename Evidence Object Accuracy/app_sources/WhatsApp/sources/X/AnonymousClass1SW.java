package X;

import android.os.Process;
import com.whatsapp.util.Log;
import java.lang.reflect.Method;

/* renamed from: X.1SW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SW {
    public static Method A00;
    public static final int[] A01 = {4096};

    static {
        try {
            A00 = Process.class.getMethod("readProcFile", String.class, int[].class, String[].class, long[].class, float[].class);
        } catch (Exception unused) {
            A00 = null;
            Log.e("procreader/native API inaccessible");
        }
    }

    public static void A00() {
        Method method = A00;
        if (method != null) {
            try {
                int myPid = Process.myPid();
                StringBuilder sb = new StringBuilder();
                sb.append("/proc/");
                sb.append(myPid);
                sb.append("/cgroup");
                try {
                    method.invoke(null, sb.toString(), A01, new String[]{null}, null, null);
                } catch (Exception unused) {
                    Log.e("procreader/native API invoke error");
                }
            } catch (RuntimeException e) {
                Log.e("procreader/Runtime Exception", e);
            }
        }
    }
}
