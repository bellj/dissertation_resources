package X;

import java.util.ArrayList;

/* renamed from: X.0OA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OA {
    public float A00 = 0.0f;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public AnonymousClass0QV A07;
    public AnonymousClass0QV A08;
    public AnonymousClass0QV A09;
    public AnonymousClass0QV A0A;
    public AnonymousClass0QV A0B;
    public AnonymousClass0QV A0C;
    public AnonymousClass0QV A0D;
    public ArrayList A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;

    public AnonymousClass0OA(AnonymousClass0QV r2, int i, boolean z) {
        this.A07 = r2;
        this.A01 = i;
        this.A0J = z;
    }
}
