package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0SQ  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0SQ {
    public abstract boolean A01();

    public abstract boolean A02(long[] jArr);

    public static AnonymousClass0SQ A00(Context context) {
        if (Build.VERSION.SDK_INT >= 28) {
            return new AnonymousClass0IW(context);
        }
        AnonymousClass0IX r1 = new AnonymousClass0IX();
        if (r1.A02(new long[8])) {
            return r1;
        }
        return new AnonymousClass0IY(context);
    }
}
