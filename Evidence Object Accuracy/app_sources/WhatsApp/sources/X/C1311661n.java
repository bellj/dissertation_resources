package X;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.util.Range;
import android.util.Size;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.61n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1311661n {
    public static final Range A00;
    public static final boolean A01;
    public static final int[] A02 = new int[0];

    public static int A00(int i) {
        switch (i) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            case 12:
                return 12;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return 13;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return 14;
            case 15:
                return 15;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return 16;
            case 17:
                return 18;
            case 18:
                return 17;
            default:
                return -1;
        }
    }

    static {
        boolean z = false;
        Float valueOf = Float.valueOf(0.0f);
        A00 = Range.create(valueOf, valueOf);
        if (Build.VERSION.SDK_INT >= 30) {
            z = true;
        }
        A01 = z;
    }

    public static int A01(CameraCharacteristics.Key key, CameraCharacteristics cameraCharacteristics) {
        Number number = (Number) cameraCharacteristics.get(key);
        if (number != null) {
            return number.intValue();
        }
        return 0;
    }

    public static Range A02(CameraCharacteristics cameraCharacteristics) {
        Float valueOf;
        Float valueOf2;
        if (!A01) {
            List A0F = A0F(cameraCharacteristics);
            valueOf = (Float) C12980iv.A0o(A0F);
            valueOf2 = (Float) A0F.get(A0F.size() - 1);
        } else {
            Range range = (Range) cameraCharacteristics.get(CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE);
            if (range != null) {
                Number number = (Number) range.getLower();
                Number number2 = (Number) range.getUpper();
                if (!(number == null || number2 == null)) {
                    valueOf = Float.valueOf(number.floatValue() * 100.0f);
                    valueOf2 = Float.valueOf(number2.floatValue() * 100.0f);
                }
            }
            return A00;
        }
        return new Range(valueOf, valueOf2);
    }

    public static List A03(float f) {
        if (f <= 0.0f) {
            return null;
        }
        double d = (double) f;
        int log = (int) ((Math.log(1.0E-11d + d) * 20.0d) / Math.log(2.0d));
        double d2 = 1.0d;
        double pow = Math.pow(d, 1.0d / ((double) log));
        ArrayList A0l = C12960it.A0l();
        A0l.add(Float.valueOf(100.0f));
        for (int i = 0; i < log - 1; i++) {
            d2 *= pow;
            A0l.add(Float.valueOf((float) (100.0d * d2)));
        }
        A0l.add(Float.valueOf(f * 100.0f));
        return A0l;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0026, code lost:
        if (r1 != 5) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A04(android.hardware.camera2.CameraCharacteristics r6) {
        /*
            android.hardware.camera2.CameraCharacteristics$Key r0 = android.hardware.camera2.CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES
            java.lang.Object r6 = r6.get(r0)
            int[] r6 = (int[]) r6
            if (r6 != 0) goto L_0x000c
            int[] r6 = X.C1311661n.A02
        L_0x000c:
            int r5 = r6.length
            if (r5 <= 0) goto L_0x003e
            java.util.ArrayList r4 = X.C12960it.A0l()
            r3 = 0
            r2 = 0
        L_0x0015:
            r1 = r6[r2]
            if (r1 == 0) goto L_0x0031
            r0 = 1
            if (r1 == r0) goto L_0x0036
            r0 = 2
            if (r1 == r0) goto L_0x0036
            r0 = 3
            if (r1 == r0) goto L_0x0036
            r0 = 4
            if (r1 == r0) goto L_0x0036
            r0 = 5
            if (r1 == r0) goto L_0x0036
        L_0x0028:
            int r2 = r2 + 1
            if (r2 < r5) goto L_0x0015
            java.util.List r0 = X.C117305Zk.A0r(r4)
            return r0
        L_0x0031:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            goto L_0x003a
        L_0x0036:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x003a:
            r4.add(r0)
            goto L_0x0028
        L_0x003e:
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1311661n.A04(android.hardware.camera2.CameraCharacteristics):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        if (r1 != 3) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A05(android.hardware.camera2.CameraCharacteristics r6) {
        /*
            android.hardware.camera2.CameraCharacteristics$Key r0 = android.hardware.camera2.CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES
            java.lang.Object r6 = r6.get(r0)
            int[] r6 = (int[]) r6
            if (r6 != 0) goto L_0x000c
            int[] r6 = X.C1311661n.A02
        L_0x000c:
            int r5 = r6.length
            if (r5 != 0) goto L_0x0014
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x0014:
            java.util.ArrayList r4 = X.C12960it.A0l()
            r3 = 0
            r2 = 0
        L_0x001a:
            r1 = r6[r2]
            if (r1 == 0) goto L_0x0030
            r0 = 1
            if (r1 == r0) goto L_0x0035
            r0 = 2
            if (r1 == r0) goto L_0x0035
            r0 = 3
            if (r1 == r0) goto L_0x0035
        L_0x0027:
            int r2 = r2 + 1
            if (r2 < r5) goto L_0x001a
            java.util.List r0 = X.C117305Zk.A0r(r4)
            return r0
        L_0x0030:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            goto L_0x0039
        L_0x0035:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x0039:
            r4.add(r0)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1311661n.A05(android.hardware.camera2.CameraCharacteristics):java.util.List");
    }

    public static List A06(CameraCharacteristics cameraCharacteristics) {
        float[] fArr = (float[]) cameraCharacteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_APERTURES);
        if (fArr == null) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        for (float f : fArr) {
            A0l.add(Float.valueOf(f));
        }
        return C117305Zk.A0r(A0l);
    }

    public static List A07(CameraCharacteristics cameraCharacteristics) {
        int i;
        int[] iArr = (int[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS);
        if (iArr == null) {
            iArr = A02;
        }
        int length = iArr.length;
        if (length == 0) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        int i2 = 0;
        do {
            switch (iArr[i2]) {
                case 0:
                    i = 0;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 1:
                    i = 1;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 2:
                    i = 2;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 3:
                    i = 3;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 4:
                    i = 4;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 5:
                    i = 5;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 6:
                    i = 6;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 7:
                    i = 7;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 8:
                    i = 8;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                default:
                    i2++;
                    break;
            }
        } while (i2 < length);
        return C117305Zk.A0r(A0l);
    }

    public static List A08(CameraCharacteristics cameraCharacteristics) {
        Range range = (Range) cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE);
        if (range == null) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        A0l.add(range.getLower());
        A0l.add(range.getUpper());
        return C117305Zk.A0r(A0l);
    }

    public static List A09(CameraCharacteristics cameraCharacteristics) {
        ArrayList A0l = C12960it.A0l();
        C12980iv.A1R(A0l, 0);
        if (A0M(CameraCharacteristics.FLASH_INFO_AVAILABLE, cameraCharacteristics)) {
            C12980iv.A1R(A0l, 3);
            int[] iArr = (int[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES);
            if (iArr == null) {
                iArr = A02;
            }
            for (int i = 0; i < iArr.length; i++) {
                int i2 = 2;
                if (iArr[i] != 2) {
                    if (iArr[i] == 3) {
                        i2 = 1;
                    }
                }
                C12980iv.A1R(A0l, i2);
            }
        }
        return C117305Zk.A0r(A0l);
    }

    public static List A0A(CameraCharacteristics cameraCharacteristics) {
        Range range = (Range) cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE);
        if (range == null) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        A0l.add(range.getLower());
        A0l.add(range.getUpper());
        return C117305Zk.A0r(A0l);
    }

    public static List A0B(CameraCharacteristics cameraCharacteristics) {
        return C130235z1.A00((Size[]) cameraCharacteristics.get(CameraCharacteristics.JPEG_AVAILABLE_THUMBNAIL_SIZES));
    }

    public static List A0C(CameraCharacteristics cameraCharacteristics) {
        int[] iArr = (int[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES);
        if (iArr == null) {
            iArr = A02;
        }
        int length = iArr.length;
        if (length == 0) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        do {
            C117295Zj.A1Q(A0l, A00(iArr[i]));
            i++;
        } while (i < length);
        return C117305Zk.A0r(A0l);
    }

    public static List A0D(CameraCharacteristics cameraCharacteristics) {
        int i;
        int[] iArr = (int[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES);
        if (iArr == null) {
            iArr = A02;
        }
        int length = iArr.length;
        if (length == 0) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        int i2 = 0;
        do {
            switch (iArr[i2]) {
                case 0:
                    i = 0;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 1:
                    i = 1;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 2:
                    i = 2;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 3:
                    i = 3;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 4:
                    i = 4;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 5:
                    i = 5;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 6:
                    i = 6;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 7:
                    i = 7;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                case 8:
                    i = 8;
                    C12980iv.A1R(A0l, i);
                    i2++;
                    break;
                default:
                    i2++;
                    break;
            }
        } while (i2 < length);
        return C117305Zk.A0r(A0l);
    }

    public static List A0E(CameraCharacteristics cameraCharacteristics) {
        ArrayList arrayList;
        List<Object> A03;
        float A022 = C72453ed.A02(A02(cameraCharacteristics).getLower());
        Float valueOf = Float.valueOf(100.0f);
        if (A022 <= 0.0f || A022 >= 100.0f || (A03 = A03(100.0f / A022)) == null || A03.isEmpty()) {
            List singletonList = Collections.singletonList(valueOf);
            if (singletonList == null) {
                return Collections.emptyList();
            }
            arrayList = C12980iv.A0x(singletonList);
        } else {
            float f = -1.0f;
            ArrayList A0l = C12960it.A0l();
            for (Object obj : A03) {
                float A023 = (100.0f / C72453ed.A02(obj)) * 100.0f;
                if (A023 != f) {
                    A0l.add(Float.valueOf(A023));
                    f = A023;
                }
            }
            arrayList = C12980iv.A0x(A0l);
        }
        return Collections.unmodifiableList(arrayList);
    }

    public static List A0F(CameraCharacteristics cameraCharacteristics) {
        float f;
        Number number = (Number) cameraCharacteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
        if (number != null) {
            f = number.floatValue();
        } else {
            f = 0.0f;
        }
        List A03 = A03(f);
        if (A03 == null) {
            return Collections.emptyList();
        }
        return C117305Zk.A0r(A03);
    }

    public static List A0G(CameraCharacteristics cameraCharacteristics, boolean z) {
        int length;
        int A05;
        Range[] rangeArr = (Range[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        if (rangeArr == null || (length = rangeArr.length) == 0) {
            return Collections.emptyList();
        }
        ArrayList A0w = C12980iv.A0w(length);
        int i = 0;
        do {
            Range range = rangeArr[i];
            int[] iArr = new int[2];
            int A052 = C12960it.A05(range.getLower());
            if (z) {
                iArr[0] = A052 * 1000;
                A05 = C12960it.A05(range.getUpper()) * 1000;
            } else {
                iArr[0] = A052;
                A05 = C12960it.A05(range.getUpper());
            }
            iArr[1] = A05;
            A0w.add(iArr);
            i++;
        } while (i < length);
        return C117305Zk.A0r(A0w);
    }

    public static List A0H(CameraCharacteristics cameraCharacteristics, boolean z) {
        int length;
        Range[] rangeArr = (Range[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        if (rangeArr == null || (length = rangeArr.length) == 0) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        do {
            Range range = rangeArr[i];
            if (range.getLower() == range.getUpper()) {
                Number number = (Number) range.getUpper();
                if (z) {
                    number = Integer.valueOf(number.intValue() * 1000);
                }
                A0l.add(number);
            }
            i++;
        } while (i < length);
        return Collections.unmodifiableList(A0l);
    }

    public static List A0I(StreamConfigurationMap streamConfigurationMap) {
        ArrayList A0l = C12960it.A0l();
        if (streamConfigurationMap == null) {
            return Collections.emptyList();
        }
        for (int i : streamConfigurationMap.getOutputFormats()) {
            C12980iv.A1R(A0l, i);
        }
        return Collections.unmodifiableList(A0l);
    }

    public static List A0J(StreamConfigurationMap streamConfigurationMap) {
        if (streamConfigurationMap == null) {
            return Collections.emptyList();
        }
        ArrayList A0l = C12960it.A0l();
        int[] outputFormats = streamConfigurationMap.getOutputFormats();
        if (outputFormats != null) {
            for (int i = 0; i < outputFormats.length; i++) {
                if (outputFormats[i] == 35) {
                    C12980iv.A1R(A0l, outputFormats[i]);
                }
            }
        }
        return Collections.unmodifiableList(A0l);
    }

    public static List A0K(StreamConfigurationMap streamConfigurationMap, int i) {
        Size[] sizeArr;
        int length;
        if (streamConfigurationMap == null) {
            sizeArr = null;
        } else if (Build.VERSION.SDK_INT >= 23) {
            Size[] highResolutionOutputSizes = streamConfigurationMap.getHighResolutionOutputSizes(i);
            sizeArr = streamConfigurationMap.getOutputSizes(i);
            if (!(highResolutionOutputSizes == null || (length = highResolutionOutputSizes.length) == 0)) {
                int length2 = sizeArr.length;
                Size[] sizeArr2 = new Size[length + length2];
                System.arraycopy(highResolutionOutputSizes, 0, sizeArr2, 0, length);
                System.arraycopy(sizeArr, 0, sizeArr2, length, length2);
                sizeArr = sizeArr2;
            }
        } else {
            sizeArr = streamConfigurationMap.getOutputSizes(i);
        }
        return C130235z1.A00(sizeArr);
    }

    public static List A0L(StreamConfigurationMap streamConfigurationMap, Class cls) {
        Size[] sizeArr;
        if (streamConfigurationMap != null) {
            sizeArr = streamConfigurationMap.getOutputSizes(cls);
        } else {
            sizeArr = null;
        }
        return C130235z1.A00(sizeArr);
    }

    public static boolean A0M(CameraCharacteristics.Key key, CameraCharacteristics cameraCharacteristics) {
        Boolean bool = (Boolean) cameraCharacteristics.get(key);
        if (bool != null) {
            return bool.booleanValue();
        }
        return false;
    }

    public static boolean A0N(CameraCharacteristics.Key key, CameraCharacteristics cameraCharacteristics, int i) {
        int[] iArr = (int[]) cameraCharacteristics.get(key);
        if (iArr == null) {
            iArr = A02;
        }
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean A0O(CameraCharacteristics cameraCharacteristics) {
        if (!A01 || cameraCharacteristics.get(CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE) == null || !A0R(cameraCharacteristics, 0) || !A0P(cameraCharacteristics)) {
            return false;
        }
        return true;
    }

    public static boolean A0P(CameraCharacteristics cameraCharacteristics) {
        Number number = (Number) cameraCharacteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
        return number != null && number.floatValue() > 0.0f;
    }

    public static boolean A0Q(CameraCharacteristics cameraCharacteristics) {
        Range[] rangeArr = (Range[]) cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        if (rangeArr == null || rangeArr.length == 0) {
            return false;
        }
        Range range = rangeArr[0];
        if (C12960it.A05(range.getLower()) >= 1000 || C12960it.A05(range.getUpper()) >= 1000) {
            return false;
        }
        return true;
    }

    public static boolean A0R(CameraCharacteristics cameraCharacteristics, int i) {
        int A05 = C12960it.A05(cameraCharacteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL));
        return A05 == 2 ? i == A05 : A05 >= i;
    }
}
