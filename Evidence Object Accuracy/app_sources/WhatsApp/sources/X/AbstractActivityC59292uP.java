package X;

import com.whatsapp.biz.product.view.activity.ProductDetailActivity;

/* renamed from: X.2uP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC59292uP extends AnonymousClass283 {
    public boolean A00 = false;

    public AbstractActivityC59292uP() {
        ActivityC13830kP.A1P(this, 20);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ProductDetailActivity productDetailActivity = (ProductDetailActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, productDetailActivity);
            ActivityC13810kN.A10(A1M, productDetailActivity);
            ((ActivityC13790kL) productDetailActivity).A08 = ActivityC13790kL.A0S(r3, A1M, productDetailActivity, ActivityC13790kL.A0Y(A1M, productDetailActivity));
            ((AnonymousClass283) productDetailActivity).A0F = (C16170oZ) A1M.AM4.get();
            ((AnonymousClass283) productDetailActivity).A0N = (AnonymousClass19N) A1M.A32.get();
            productDetailActivity.A0i = (C19840ul) A1M.A1Q.get();
            ((AnonymousClass283) productDetailActivity).A0L = (C21770xx) A1M.A2s.get();
            ((AnonymousClass283) productDetailActivity).A0K = (C25791Av) A1M.A2t.get();
            productDetailActivity.A0S = (AnonymousClass19T) A1M.A2y.get();
            productDetailActivity.A0f = (C20020v5) A1M.A3B.get();
            productDetailActivity.A0c = C12960it.A0O(A1M);
            ((AnonymousClass283) productDetailActivity).A0O = (AnonymousClass1FZ) A1M.AGJ.get();
            productDetailActivity.A0T = C12990iw.A0V(A1M);
            ((AnonymousClass283) productDetailActivity).A0J = (C25831Az) A1M.A30.get();
            productDetailActivity.A0e = C12980iv.A0a(A1M);
            productDetailActivity.A0P = (C19850um) A1M.A2v.get();
            ((AnonymousClass283) productDetailActivity).A0I = C12980iv.A0Y(A1M);
            ((AnonymousClass283) productDetailActivity).A0C = (C48882Ih) r3.A0Q.get();
            productDetailActivity.A0R = C12980iv.A0Z(A1M);
            productDetailActivity.A0d = (C26311Cv) A1M.AAQ.get();
            productDetailActivity.A0g = C12990iw.A0a(A1M);
            productDetailActivity.A04 = C12970iu.A0W(A1M);
            productDetailActivity.A02 = C12960it.A0P(A1M);
            productDetailActivity.A01 = (AnonymousClass10A) A1M.A2W.get();
        }
    }
}
