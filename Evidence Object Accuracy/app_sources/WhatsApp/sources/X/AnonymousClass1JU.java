package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.jid.DeviceJid;
import java.util.Arrays;
import org.chromium.net.UrlRequest;

/* renamed from: X.1JU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JU {
    public long A00;
    public long A01;
    public String A02;
    public final int A03;
    public final long A04;
    public final DeviceJid A05;
    public final AnonymousClass1YI A06;
    public final String A07;
    public final boolean A08;

    public AnonymousClass1JU(DeviceJid deviceJid, AnonymousClass1YI r2, String str, String str2, int i, long j, long j2, long j3, boolean z) {
        AnonymousClass009.A05(deviceJid);
        this.A05 = deviceJid;
        this.A06 = r2;
        this.A07 = str;
        this.A00 = j;
        this.A04 = j2;
        this.A01 = j3;
        this.A03 = i;
        this.A08 = z;
        this.A02 = str2;
    }

    public static String A00(Context context, AnonymousClass1JU r5) {
        int i;
        int i2;
        switch (r5.A06.ordinal()) {
            case 1:
                i = R.string.linked_device_name_platform_chrome;
                return context.getString(i, r5.A07);
            case 2:
                i = R.string.linked_device_name_platform_firefox;
                return context.getString(i, r5.A07);
            case 3:
                i = R.string.linked_device_name_platform_ie;
                return context.getString(i, r5.A07);
            case 4:
                i = R.string.linked_device_name_platform_opera;
                return context.getString(i, r5.A07);
            case 5:
                i = R.string.linked_device_name_platform_safari;
                return context.getString(i, r5.A07);
            case 6:
                i = R.string.linked_device_name_platform_edge;
                return context.getString(i, r5.A07);
            case 7:
            case 8:
            case 9:
            default:
                return r5.A07;
            case 10:
                i2 = R.string.linked_device_name_platform_ohana;
                return context.getString(i2);
            case 11:
                i2 = R.string.linked_device_name_platform_aloha;
                return context.getString(i2);
            case 12:
                i2 = R.string.linked_device_name_platform_catalina;
                return context.getString(i2);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i2 = R.string.linked_device_name_platform_portal_tcl_tv;
                return context.getString(i2);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        AnonymousClass1JU r7 = (AnonymousClass1JU) obj;
        if (!this.A05.equals(r7.A05) || !this.A06.equals(r7.A06) || !AnonymousClass1US.A0D(this.A07, r7.A07) || this.A04 != r7.A04 || this.A03 != r7.A03 || this.A08 != r7.A08) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        DeviceJid deviceJid = this.A05;
        return Arrays.hashCode(new Object[]{deviceJid, this.A06, deviceJid, Long.valueOf(this.A04), Integer.valueOf(this.A03), Boolean.valueOf(this.A08)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Device jid: ");
        sb.append(this.A05);
        sb.append(", Platform type: ");
        sb.append(this.A06.toString());
        sb.append(", Device OS: ");
        sb.append(this.A07);
        sb.append(", Last active: ");
        sb.append(this.A00);
        sb.append(", Login time: ");
        sb.append(this.A04);
        sb.append(", Logout time: ");
        sb.append(this.A01);
        sb.append(", ADV Key Index: ");
        sb.append(this.A03);
        sb.append(", full sync required: ");
        sb.append(this.A08);
        sb.append(", Place Name: ");
        sb.append(this.A02);
        return sb.toString();
    }
}
