package X;

import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/* renamed from: X.13i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238713i {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final C20670w8 A03;
    public final C233411h A04;
    public final C15550nR A05;
    public final C18780t0 A06;
    public final C20020v5 A07;
    public final C14830m7 A08;
    public final C15990oG A09;
    public final C19990v2 A0A;
    public final C15650ng A0B;
    public final C15600nX A0C;
    public final AnonymousClass11B A0D;
    public final C20140vH A0E;
    public final AnonymousClass134 A0F;
    public final AnonymousClass10U A0G;
    public final C18470sV A0H;
    public final AnonymousClass15K A0I;
    public final C234211p A0J;
    public final C22090yV A0K;
    public final C14850m9 A0L;
    public final C16120oU A0M;
    public final C21780xy A0N;
    public final C14300lD A0O;
    public final C20160vJ A0P;
    public final C20320vZ A0Q;
    public final C22210yi A0R;
    public final Random A0S = new Random();

    public C238713i(AbstractC15710nm r2, C15570nT r3, C15450nH r4, C20670w8 r5, C233411h r6, C15550nR r7, C18780t0 r8, C20020v5 r9, C14830m7 r10, C15990oG r11, C19990v2 r12, C15650ng r13, C15600nX r14, AnonymousClass11B r15, C20140vH r16, AnonymousClass134 r17, AnonymousClass10U r18, C18470sV r19, AnonymousClass15K r20, C234211p r21, C22090yV r22, C14850m9 r23, C16120oU r24, C21780xy r25, C14300lD r26, C20160vJ r27, C20320vZ r28, C22210yi r29) {
        this.A08 = r10;
        this.A0L = r23;
        this.A00 = r2;
        this.A01 = r3;
        this.A0A = r12;
        this.A0E = r16;
        this.A0M = r24;
        this.A0F = r17;
        this.A02 = r4;
        this.A0H = r19;
        this.A03 = r5;
        this.A0I = r20;
        this.A05 = r7;
        this.A0R = r29;
        this.A0Q = r28;
        this.A0O = r26;
        this.A0B = r13;
        this.A0P = r27;
        this.A04 = r6;
        this.A07 = r9;
        this.A0K = r22;
        this.A09 = r11;
        this.A0G = r18;
        this.A06 = r8;
        this.A0C = r14;
        this.A0D = r15;
        this.A0J = r21;
        this.A0N = r25;
    }

    public static final AnonymousClass1P9 A00(int i) {
        if (i == 0) {
            return AnonymousClass1P9.A02;
        }
        if (i == 1) {
            return AnonymousClass1P9.A03;
        }
        if (i == 2) {
            return AnonymousClass1P9.A06;
        }
        if (i == 3) {
            return AnonymousClass1P9.A01;
        }
        if (i == 4) {
            return AnonymousClass1P9.A05;
        }
        StringBuilder sb = new StringBuilder("Unexpected type (");
        sb.append(i);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }

    public final long A01(AnonymousClass1JT r30, DeviceJid deviceJid, AnonymousClass1P8 r32, String str, int i, int i2, int i3, int i4, long j, long j2, long j3, long j4, long j5) {
        try {
            File A00 = this.A0N.A00.A00("");
            A00.getAbsolutePath();
            AbstractC27091Fz A02 = r32.A02();
            Deflater deflater = new Deflater(1, false);
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(new FileOutputStream(A00), deflater);
            int AGd = A02.AGd();
            if (AGd > 4096) {
                AGd = 4096;
            }
            C40921sY r1 = new C40921sY(deflaterOutputStream, AGd);
            A02.AgI(r1);
            if (r1.A00 > 0) {
                r1.A0O();
            }
            deflaterOutputStream.close();
            deflater.end();
            AnonymousClass1K9 A002 = AnonymousClass1K9.A00(Uri.fromFile(A00), null, null, new C14480lV(false, false, true), C14370lK.A0K, null, null, 0, false, false, true, false);
            long length = A00.length();
            this.A04.A0C(r30, i2, (long) i3, true);
            C14300lD r2 = this.A0O;
            AnonymousClass1KC A04 = r2.A04(A002, false);
            A04.A0U = "mms";
            r2.A0D(A04, null);
            A04.A03(new AbstractC14590lg(r30, deviceJid, A04, this, r32, A00, str, i2, i3, i, i4, j4, length, j, j2, j3, j5) { // from class: X.1sb
                public final /* synthetic */ int A00;
                public final /* synthetic */ int A01;
                public final /* synthetic */ int A02;
                public final /* synthetic */ int A03;
                public final /* synthetic */ long A04;
                public final /* synthetic */ long A05;
                public final /* synthetic */ long A06;
                public final /* synthetic */ long A07;
                public final /* synthetic */ long A08;
                public final /* synthetic */ long A09;
                public final /* synthetic */ AnonymousClass1JT A0A;
                public final /* synthetic */ DeviceJid A0B;
                public final /* synthetic */ AnonymousClass1KC A0C;
                public final /* synthetic */ C238713i A0D;
                public final /* synthetic */ AnonymousClass1P8 A0E;
                public final /* synthetic */ File A0F;
                public final /* synthetic */ String A0G;

                {
                    this.A0D = r6;
                    this.A0F = r8;
                    this.A0C = r5;
                    this.A02 = r10;
                    this.A0A = r3;
                    this.A03 = r11;
                    this.A0E = r7;
                    this.A08 = r14;
                    this.A09 = r16;
                    this.A0B = r4;
                    this.A00 = r12;
                    this.A01 = r13;
                    this.A04 = r18;
                    this.A05 = r20;
                    this.A06 = r22;
                    this.A0G = r9;
                    this.A07 = r24;
                }

                /* JADX WARNING: Removed duplicated region for block: B:24:0x00c6  */
                @Override // X.AbstractC14590lg
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void accept(java.lang.Object r35) {
                    /*
                    // Method dump skipped, instructions count: 396
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C40941sb.accept(java.lang.Object):void");
                }
            }, null);
            return length;
        } catch (IOException e) {
            Log.e("history-sync-send-methods/save-to-file: failed", e);
            this.A04.A0C(r30, i2, (long) i3, false);
            return 0;
        }
    }

    public void A02(AnonymousClass1JT r28, DeviceJid deviceJid, String str, int i) {
        int A02 = this.A0L.A02(1181);
        try {
            ArrayList A0E = this.A05.A0E();
            AnonymousClass1P8 r11 = (AnonymousClass1P8) AnonymousClass1P7.A0D.A0T();
            r11.A06(AnonymousClass1P9.A05);
            Iterator it = A0E.iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C15370n3 r6 = (C15370n3) it.next();
                UserJid userJid = (UserJid) r6.A0B(UserJid.class);
                if (userJid != null && !TextUtils.isEmpty(r6.A0U)) {
                    C40911sX r3 = (C40911sX) C40901sW.A03.A0T();
                    String rawString = userJid.getRawString();
                    r3.A03();
                    C40901sW r1 = (C40901sW) r3.A00;
                    r1.A00 |= 1;
                    r1.A01 = rawString;
                    String str2 = r6.A0U;
                    r3.A03();
                    C40901sW r12 = (C40901sW) r3.A00;
                    r12.A00 |= 2;
                    r12.A02 = str2;
                    C40901sW r32 = (C40901sW) r3.A02();
                    r11.A03();
                    AnonymousClass1P7 r2 = (AnonymousClass1P7) r11.A00;
                    AnonymousClass1K6 r13 = r2.A09;
                    if (!((AnonymousClass1K7) r13).A00) {
                        r13 = AbstractC27091Fz.A0G(r13);
                        r2.A09 = r13;
                    }
                    r13.add(r32);
                    i2++;
                    if (A02 > 0 && i2 >= A02) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("history-sync-send-methods/send-push-names reach limit=");
                        sb.append(A02);
                        Log.i(sb.toString());
                        break;
                    }
                }
            }
            A01(r28, deviceJid, r11, str, 1, 4, 100, i, -1, -1, -1, 0, 0);
        } catch (Exception e) {
            Log.e("history-sync-send-methods/send-push-names: error", e);
        }
    }

    public final void A03(AbstractC14640lm r6, AnonymousClass1PC r7, long j, long j2) {
        AnonymousClass1PK r2;
        long A05 = this.A0F.A05(r6);
        if (A05 != Long.MIN_VALUE) {
            if (A05 >= j) {
                r2 = AnonymousClass1PK.A01;
            } else if (A05 < j2) {
                C20140vH r4 = this.A0E;
                int i = 0;
                String[] strArr = {String.valueOf(r4.A02.A02(r6)), Long.toString(j2), Long.toString(j)};
                C16310on A01 = r4.A06.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND _id > ? AND _id <= ?", strArr);
                    if (A09.moveToNext()) {
                        i = A09.getInt(0);
                        StringBuilder sb = new StringBuilder();
                        sb.append("msgstore/getmessagesatid/pos:");
                        sb.append(i);
                        Log.i(sb.toString());
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("msgstore/getmessagesatid/db no message for ");
                        sb2.append(r6);
                        Log.i(sb2.toString());
                    }
                    A09.close();
                    A01.close();
                    if (i == 0) {
                        r2 = AnonymousClass1PK.A02;
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                return;
            }
            r7.A03();
            AnonymousClass1PB r1 = (AnonymousClass1PB) r7.A00;
            r1.A01 |= 512;
            r1.A03 = r2.value;
        }
    }
}
