package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101714ny implements ViewTreeObserver.OnGlobalLayoutListener {
    public boolean A00 = false;
    public final /* synthetic */ AbstractC36001jA A01;

    public ViewTreeObserver$OnGlobalLayoutListenerC101714ny(AbstractC36001jA r2) {
        this.A01 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AbstractC36001jA r2 = this.A01;
        boolean A00 = C252718t.A00(r2.A0Q);
        if (A00 != this.A00) {
            this.A00 = A00;
            r2.A0O(null, false);
        }
    }
}
