package X;

import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import java.util.ArrayList;

/* renamed from: X.65N  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65N implements AnonymousClass07L {
    public final /* synthetic */ IndiaUpiBankPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public AnonymousClass65N(IndiaUpiBankPickerActivity indiaUpiBankPickerActivity) {
        this.A00 = indiaUpiBankPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        IndiaUpiBankPickerActivity indiaUpiBankPickerActivity = this.A00;
        indiaUpiBankPickerActivity.A0F = str;
        ArrayList A02 = C32751cg.A02(((ActivityC13830kP) indiaUpiBankPickerActivity).A01, str);
        indiaUpiBankPickerActivity.A0G = A02;
        if (A02.isEmpty()) {
            indiaUpiBankPickerActivity.A0G = null;
        }
        C124255or r1 = indiaUpiBankPickerActivity.A0D;
        if (r1 != null) {
            r1.A03(true);
            indiaUpiBankPickerActivity.A0D = null;
        }
        C124255or r12 = new C124255or(indiaUpiBankPickerActivity, indiaUpiBankPickerActivity.A0F, indiaUpiBankPickerActivity.A0G);
        indiaUpiBankPickerActivity.A0D = r12;
        C12960it.A1E(r12, ((ActivityC13830kP) indiaUpiBankPickerActivity).A05);
        return false;
    }
}
