package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3Lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65893Lm implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        C16700pc.A0E(parcel, 0);
        return new C65933Lq(parcel.readString(), parcel.createTypedArrayList(C65943Lr.CREATOR));
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C65933Lq[i];
    }
}
