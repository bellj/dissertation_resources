package X;

import java.util.HashMap;
import java.util.Locale;

/* renamed from: X.0qE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17080qE {
    public final C15450nH A00;
    public final C18790t3 A01;
    public final AnonymousClass10V A02;
    public final C16590pI A03;
    public final C14950mJ A04;
    public final C14850m9 A05;
    public final C22050yP A06;
    public final C20110vE A07;
    public final C22600zL A08;
    public final AbstractC14440lR A09;
    public final HashMap A0A = new HashMap();

    public C17080qE(C15450nH r2, C18790t3 r3, AnonymousClass10V r4, C16590pI r5, C14950mJ r6, C14850m9 r7, C22050yP r8, C20110vE r9, C22600zL r10, AbstractC14440lR r11) {
        this.A05 = r7;
        this.A03 = r5;
        this.A09 = r11;
        this.A01 = r3;
        this.A00 = r2;
        this.A04 = r6;
        this.A08 = r10;
        this.A06 = r8;
        this.A02 = r4;
        this.A07 = r9;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0003, code lost:
        if (r3 != null) goto L_0x0005;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File A00(android.content.Context r2, java.lang.String r3, java.net.URL r4) {
        /*
            if (r4 != 0) goto L_0x0005
            r0 = 0
            if (r3 == 0) goto L_0x0006
        L_0x0005:
            r0 = 1
        L_0x0006:
            X.AnonymousClass009.A0F(r0)
            if (r4 == 0) goto L_0x0028
            java.lang.String r1 = r4.toString()
        L_0x000f:
            r0 = 0
            java.lang.String r3 = android.webkit.URLUtil.guessFileName(r1, r0, r0)
            java.io.File r2 = r2.getCacheDir()
            java.lang.String r0 = "ProfilePictureTemp"
            java.io.File r1 = new java.io.File
            r1.<init>(r2, r0)
            r1.mkdirs()
            java.io.File r0 = new java.io.File
            r0.<init>(r1, r3)
            return r0
        L_0x0028:
            java.lang.String r1 = "https://pps.whatsapp.net"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r1 = r0.toString()
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17080qE.A00(android.content.Context, java.lang.String, java.net.URL):java.io.File");
    }

    public void A01(C41831uE r19, long j) {
        AbstractC14640lm r8 = r19.A03;
        int i = r19.A02;
        Locale locale = Locale.US;
        Integer valueOf = Integer.valueOf(i);
        String format = String.format(locale, "%s.%d", r8.getRawString(), valueOf);
        HashMap hashMap = this.A0A;
        synchronized (hashMap) {
            C41841uF r4 = (C41841uF) hashMap.get(format);
            if (r4 != null) {
                if (!r4.A04.A04.equals(r19.A04)) {
                    r4.A03();
                    hashMap.remove(format);
                }
            }
            String format2 = String.format(Locale.US, "%s.%d", r8.getRawString(), valueOf);
            C41851uG r14 = new AbstractC14590lg(r19) { // from class: X.1uG
                public final /* synthetic */ C41831uE A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    HashMap hashMap2 = C17080qE.this.A0A;
                    synchronized (hashMap2) {
                        hashMap2.remove(obj);
                    }
                }
            };
            C14850m9 r9 = this.A05;
            C16590pI r7 = this.A03;
            C18790t3 r5 = this.A01;
            C41841uF r3 = new C41841uF(this.A00, r5, this.A02, r7, this.A04, r9, this.A06, this.A07, r19, this.A08, r14, format2, j);
            hashMap.put(format2, r3);
            this.A09.Ab2(r3);
        }
    }
}
