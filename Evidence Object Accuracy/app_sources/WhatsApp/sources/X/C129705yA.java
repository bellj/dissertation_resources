package X;

import com.whatsapp.util.Log;

/* renamed from: X.5yA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129705yA {
    public final C006202y A00 = new C006202y(10);
    public final C15570nT A01;
    public final C14830m7 A02;
    public final AnonymousClass102 A03;
    public final C17070qD A04;
    public final C130155yt A05;
    public final C130125yq A06;
    public final C22980zx A07;

    public C129705yA(C15570nT r3, C14830m7 r4, AnonymousClass102 r5, C17070qD r6, C130155yt r7, C130125yq r8, C22980zx r9) {
        this.A02 = r4;
        this.A01 = r3;
        this.A04 = r6;
        this.A05 = r7;
        this.A07 = r9;
        this.A06 = r8;
        this.A03 = r5;
    }

    public AnonymousClass017 A00(AnonymousClass6F2 r7) {
        AnonymousClass016 A0T = C12980iv.A0T();
        C006202y r2 = this.A00;
        C1309460p r1 = (C1309460p) r2.A04(r7);
        if (r1 != null) {
            if (r1.A02.A01()) {
                C130785zy.A01(A0T, null, r1);
                return A0T;
            }
            r2.A07(r7);
        }
        C130155yt r4 = this.A05;
        C1310460z A01 = AnonymousClass61S.A01("novi-get-deposit-quote");
        A01.A02.add(C130325zE.A02(r7, "amount"));
        r4.A0B(new AbstractC136196Lo(A0T, this) { // from class: X.69t
            public final /* synthetic */ AnonymousClass016 A00;
            public final /* synthetic */ C129705yA A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r72) {
                Object obj;
                C129705yA r5 = this.A01;
                AnonymousClass016 r42 = this.A00;
                if (r72.A06() && (obj = r72.A02) != null) {
                    try {
                        C1309460p A00 = C1309460p.A00(r5.A03, ((AnonymousClass1V8) obj).A0F("deposit_amount"));
                        C130785zy.A01(r42, null, A00);
                        r5.A00.A08(A00.A00, A00);
                        return;
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: NoviDepositRepository/handleDepositQuoteResponse can't parse result");
                    }
                }
                C130785zy.A01(r42, r72.A00, null);
            }
        }, A01, "get", 5);
        return A0T;
    }
}
