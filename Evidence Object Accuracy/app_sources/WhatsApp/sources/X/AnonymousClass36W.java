package X;

import android.content.Intent;
import android.view.View;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.36W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36W extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AcceptInviteLinkActivity A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public AnonymousClass36W(AcceptInviteLinkActivity acceptInviteLinkActivity, String str, String str2) {
        this.A00 = acceptInviteLinkActivity;
        this.A02 = str;
        this.A01 = str2;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AcceptInviteLinkActivity acceptInviteLinkActivity = this.A00;
        Intent A0D = C12990iw.A0D(acceptInviteLinkActivity, GroupAdminPickerActivity.class);
        A0D.putExtra("gid", C15380n4.A03(acceptInviteLinkActivity.A0G));
        A0D.putExtra("subgroup_subject", this.A02);
        A0D.putExtra("parent_group_jid", this.A01);
        acceptInviteLinkActivity.startActivity(A0D);
        acceptInviteLinkActivity.overridePendingTransition(0, 0);
        acceptInviteLinkActivity.finish();
    }
}
