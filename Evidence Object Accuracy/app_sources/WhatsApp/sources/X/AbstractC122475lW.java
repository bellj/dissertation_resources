package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5lW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC122475lW extends AbstractC118825cR {
    public View.OnClickListener A00;
    public ViewGroup A01;
    public FrameLayout A02;
    public ImageView A03;
    public LinearLayout A04;
    public TextView A05;
    public TextView A06;
    public CharSequence A07;
    public CharSequence A08;
    public final List A09 = C12960it.A0l();

    public AbstractC122475lW(View view) {
        super(view);
        View view2 = this.A0H;
        this.A06 = C12960it.A0I(view2, R.id.header);
        this.A01 = C117315Zl.A04(view2, R.id.see_more_container);
        this.A03 = C12970iu.A0K(view2, R.id.see_more_icon);
        this.A05 = C12960it.A0I(view2, R.id.see_more_text);
        this.A02 = (FrameLayout) AnonymousClass028.A0D(view2, R.id.custom_empty_view_container);
        this.A04 = C117305Zk.A07(view2, R.id.list_item_container);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:64:0x01ec */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v0, types: [android.widget.LinearLayout, android.view.View, android.view.ViewGroup] */
    /* JADX WARN: Type inference failed for: r6v0, types: [X.5aS] */
    /* JADX WARN: Type inference failed for: r6v1, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r1v7, types: [android.view.LayoutInflater] */
    /* JADX WARN: Type inference failed for: r6v3, types: [com.whatsapp.payments.ui.widget.PaymentInteropShimmerRow, android.view.View] */
    /* JADX WARN: Type inference failed for: r4v2, types: [android.widget.LinearLayout, android.view.ViewGroup] */
    /* JADX WARNING: Unknown variable types count: 3 */
    @Override // X.AbstractC118825cR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AbstractC125975s7 r28, int r29) {
        /*
        // Method dump skipped, instructions count: 560
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC122475lW.A08(X.5s7, int):void");
    }

    public void A09() {
        ViewGroup viewGroup;
        View.OnClickListener onClickListener;
        if (!(this instanceof C122275lC)) {
            List list = this.A09;
            int i = 0;
            if (list.size() > 2) {
                viewGroup = this.A01;
                viewGroup.setVisibility(0);
                this.A05.setText(this.A08);
                onClickListener = this.A00;
            } else {
                if (!list.isEmpty()) {
                    i = 8;
                    this.A01.setVisibility(8);
                } else if (this.A02.getChildCount() <= 0) {
                    viewGroup = this.A01;
                    viewGroup.setVisibility(0);
                    this.A05.setText(this.A07);
                    onClickListener = null;
                }
                this.A02.setVisibility(i);
                return;
            }
            viewGroup.setOnClickListener(onClickListener);
            return;
        }
        List list2 = this.A09;
        if (!list2.isEmpty()) {
            C122905mI r2 = (C122905mI) list2.get(0);
            List list3 = r2.A03;
            if (list3.isEmpty()) {
                this.A01.setVisibility(8);
                FrameLayout frameLayout = this.A02;
                C12960it.A0E(frameLayout).inflate(R.layout.novi_pay_hub_no_transactions, (ViewGroup) frameLayout, true);
                frameLayout.setVisibility(0);
                return;
            }
            this.A02.setVisibility(8);
            if (list3.size() > 3) {
                this.A05.setText(R.string.payments_settings_view_payment_history);
                ViewGroup viewGroup2 = this.A01;
                viewGroup2.setVisibility(0);
                viewGroup2.setOnClickListener(r2.A01);
                return;
            }
        }
        this.A01.setVisibility(8);
    }
}
