package X;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.24n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C461124n {
    public Integer A00;
    public boolean A01 = false;
    public boolean A02 = true;
    public final AnonymousClass04F A03;
    public final C16120oU A04;
    public final AbstractC21180x0 A05;
    public final Map A06 = new HashMap();

    public C461124n(Context context, C003301l r4, C16120oU r5, AbstractC21180x0 r6) {
        this.A05 = r6;
        this.A04 = r5;
        this.A03 = new AnonymousClass04F(context, new C461224o(this), r4);
    }
}
