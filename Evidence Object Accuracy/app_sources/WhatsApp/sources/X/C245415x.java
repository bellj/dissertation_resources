package X;

import android.database.Cursor;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;

/* renamed from: X.15x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245415x extends AbstractC20610w2 {
    public final C16510p9 A00;

    public C245415x(AbstractC15710nm r7, C16510p9 r8, C18460sU r9, C20850wQ r10, C16490p7 r11, C22840zj r12) {
        super(r7, r9, r10, r11, r12);
        this.A00 = r8;
    }

    public final C32611cR A05(AnonymousClass1IS r11) {
        String str;
        C32611cR r7 = new C32611cR();
        AbstractC14640lm r1 = r11.A00;
        AnonymousClass009.A05(r1);
        String[] strArr = new String[3];
        strArr[0] = String.valueOf(this.A00.A02(r1));
        if (r11.A02) {
            str = "1";
        } else {
            str = "0";
        }
        strArr[1] = str;
        strArr[2] = r11.A01;
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT receipt_device_jid_row_id, receipt_device_timestamp FROM message_add_on JOIN message_add_on_receipt_device ON message_add_on._id = message_add_on_receipt_device.message_add_on_row_id WHERE chat_row_id = ? AND from_me = ? AND key_id = ?", strArr);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("receipt_device_jid_row_id");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("receipt_device_timestamp");
            while (A09.moveToNext()) {
                long j = A09.getLong(columnIndexOrThrow);
                C18460sU r9 = this.A02;
                DeviceJid deviceJid = (DeviceJid) r9.A07(DeviceJid.class, j);
                if (deviceJid != null) {
                    r7.A00.put(deviceJid, new C32621cS(A09.getLong(columnIndexOrThrow2)));
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("MessageAddOnReceiptDeviceStore//getmessagedevicereceipts: got a null deviceJid for ");
                    sb.append(r11);
                    sb.append(", deviceJidRowId=");
                    sb.append(j);
                    sb.append(", jid=");
                    sb.append(r9.A03(j));
                    Log.e(sb.toString());
                }
            }
            A09.close();
            A01.close();
            return r7;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
