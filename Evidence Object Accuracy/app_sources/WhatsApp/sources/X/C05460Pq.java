package X;

/* renamed from: X.0Pq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05460Pq {
    public float A00 = 0.0f;
    public float A01 = 0.0f;
    public float A02;
    public float A03;
    public boolean A04 = false;
    public final /* synthetic */ C06540Ua A05;

    public C05460Pq(C06540Ua r6, float f, float f2, float f3, float f4) {
        this.A05 = r6;
        this.A02 = f;
        this.A03 = f2;
        double sqrt = Math.sqrt((double) ((f3 * f3) + (f4 * f4)));
        if (sqrt != 0.0d) {
            this.A00 = (float) (((double) f3) / sqrt);
            this.A01 = (float) (((double) f4) / sqrt);
        }
    }

    public void A00(float f, float f2) {
        float f3 = f - this.A02;
        float f4 = f2 - this.A03;
        double sqrt = Math.sqrt((double) ((f3 * f3) + (f4 * f4)));
        if (sqrt != 0.0d) {
            f3 = (float) (((double) f3) / sqrt);
            f4 = (float) (((double) f4) / sqrt);
        }
        float f5 = this.A00;
        if (f3 == (-f5) && f4 == (-this.A01)) {
            this.A04 = true;
            this.A00 = -f4;
        } else {
            this.A00 = f5 + f3;
            f3 = this.A01 + f4;
        }
        this.A01 = f3;
    }

    public void A01(C05460Pq r5) {
        float f = r5.A00;
        float f2 = this.A00;
        if (f == (-f2)) {
            float f3 = r5.A01;
            if (f3 == (-this.A01)) {
                this.A04 = true;
                this.A00 = -f3;
                this.A01 = r5.A00;
                return;
            }
        }
        this.A00 = f2 + f;
        this.A01 += r5.A01;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        sb.append(this.A02);
        sb.append(",");
        sb.append(this.A03);
        sb.append(" ");
        sb.append(this.A00);
        sb.append(",");
        sb.append(this.A01);
        sb.append(")");
        return sb.toString();
    }
}
