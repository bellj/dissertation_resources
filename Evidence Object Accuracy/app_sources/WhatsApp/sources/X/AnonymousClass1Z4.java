package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Z4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z4 {
    public static Typeface A00;
    public static final Map A01 = new AnonymousClass5IC();

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: android.text.SpannableStringBuilder */
    /* JADX WARN: Multi-variable type inference failed */
    public static CharSequence A00(Context context, String str) {
        Map map = A01;
        for (String str2 : map.keySet()) {
            if (map.get(str2) != null) {
                String str3 = (String) map.get(str2);
                if (A00 == null) {
                    try {
                        A00 = AnonymousClass00X.A02(context);
                    } catch (Resources.NotFoundException unused) {
                        Log.e("PAY: PaymentsTypeface/loadTypefaceSync could not load font R.font.payment_icons_regular");
                    }
                }
                if (A00 != null) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
                    ArrayList arrayList = new ArrayList(5);
                    String charSequence = str.toString();
                    for (int indexOf = charSequence.indexOf(str2); indexOf >= 0; indexOf = charSequence.indexOf(str2, indexOf + 1)) {
                        arrayList.add(new AnonymousClass01T(Integer.valueOf(indexOf), Integer.valueOf(str2.length() + indexOf)));
                    }
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AnonymousClass01T r1 = (AnonymousClass01T) it.next();
                        int intValue = ((Number) r1.A00).intValue();
                        spannableStringBuilder.replace(intValue, ((Number) r1.A01).intValue(), (CharSequence) str3);
                        spannableStringBuilder.setSpan(new AnonymousClass1Z3(A00), intValue, intValue + 1, 0);
                    }
                    str = spannableStringBuilder;
                } else {
                    Log.e("PAY: PaymentsTypeface/format Could not load payment_icons_regular typeface, call loadTypeface() before applying font.");
                }
            }
        }
        return str;
    }
}
