package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.5CX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5CX implements Comparator {
    public final /* synthetic */ AnonymousClass1Sh A00;

    public AnonymousClass5CX(AnonymousClass1Sh r1) {
        this.A00 = r1;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((File) obj).getName().compareTo(((File) obj2).getName());
    }
}
