package X;

import android.content.Context;

/* renamed from: X.5v7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127825v7 {
    public final Context A00;
    public final C14900mE A01;
    public final C17220qS A02;
    public final C18650sn A03;
    public final C18610sj A04;

    public C127825v7(Context context, C14900mE r2, C17220qS r3, C18650sn r4, C18610sj r5) {
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
    }
}
