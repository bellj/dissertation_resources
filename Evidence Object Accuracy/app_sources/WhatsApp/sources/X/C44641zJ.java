package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1zJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44641zJ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C44641zJ A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public C44631zI A02;
    public C56912mA A03;
    public C57482n8 A04;

    static {
        C44641zJ r0 = new C44641zJ();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r6, Object obj, Object obj2) {
        C44621zH r1;
        C81463u3 r12;
        C81473u4 r13;
        switch (r6.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C44641zJ r8 = (C44641zJ) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                int i = this.A01;
                boolean z2 = true;
                if ((r8.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r7.Afp(i, r8.A01, z, z2);
                this.A04 = (C57482n8) r7.Aft(this.A04, r8.A04);
                this.A03 = (C56912mA) r7.Aft(this.A03, r8.A03);
                this.A02 = (C44631zI) r7.Aft(this.A02, r8.A02);
                if (r7 == C463025i.A00) {
                    this.A00 |= r8.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r82 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                int A02 = r72.A02();
                                if (A02 == 0 || A02 == 1) {
                                    this.A00 = 1 | this.A00;
                                    this.A01 = A02;
                                } else {
                                    super.A0X(1, A02);
                                }
                            } else if (A03 == 18) {
                                if ((this.A00 & 2) == 2) {
                                    r13 = (C81473u4) this.A04.A0T();
                                } else {
                                    r13 = null;
                                }
                                C57482n8 r0 = (C57482n8) r72.A09(r82, C57482n8.A06.A0U());
                                this.A04 = r0;
                                if (r13 != null) {
                                    r13.A04(r0);
                                    this.A04 = (C57482n8) r13.A01();
                                }
                                this.A00 |= 2;
                            } else if (A03 == 26) {
                                if ((this.A00 & 4) == 4) {
                                    r12 = (C81463u3) this.A03.A0T();
                                } else {
                                    r12 = null;
                                }
                                C56912mA r02 = (C56912mA) r72.A09(r82, C56912mA.A02.A0U());
                                this.A03 = r02;
                                if (r12 != null) {
                                    r12.A04(r02);
                                    this.A03 = (C56912mA) r12.A01();
                                }
                                this.A00 |= 4;
                            } else if (A03 == 34) {
                                if ((this.A00 & 8) == 8) {
                                    r1 = (C44621zH) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                C44631zI r03 = (C44631zI) r72.A09(r82, C44631zI.A0e.A0U());
                                this.A02 = r03;
                                if (r1 != null) {
                                    r1.A04(r03);
                                    this.A02 = (C44631zI) r1.A01();
                                }
                                this.A00 |= 8;
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r14 = new C28971Pt(e2.getMessage());
                        r14.unfinishedMessage = this;
                        throw new RuntimeException(r14);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C44641zJ();
            case 5:
                return new C82503vj();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C44641zJ.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            C57482n8 r0 = this.A04;
            if (r0 == null) {
                r0 = C57482n8.A06;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C56912mA r02 = this.A03;
            if (r02 == null) {
                r02 = C56912mA.A02;
            }
            i2 += CodedOutputStream.A0A(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C44631zI r03 = this.A02;
            if (r03 == null) {
                r03 = C44631zI.A0e;
            }
            i2 += CodedOutputStream.A0A(r03, 4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            C57482n8 r0 = this.A04;
            if (r0 == null) {
                r0 = C57482n8.A06;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C56912mA r02 = this.A03;
            if (r02 == null) {
                r02 = C56912mA.A02;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C44631zI r03 = this.A02;
            if (r03 == null) {
                r03 = C44631zI.A0e;
            }
            codedOutputStream.A0L(r03, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
