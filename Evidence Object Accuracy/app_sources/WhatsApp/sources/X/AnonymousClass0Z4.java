package X;

import java.util.Comparator;

/* renamed from: X.0Z4  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Z4 implements AbstractC12640iF, Comparator {
    public Object A00(Object obj, Object obj2) {
        return null;
    }

    public abstract boolean A01(Object obj, Object obj2);

    public abstract boolean A02(Object obj, Object obj2);

    @Override // X.AbstractC12640iF
    public abstract void ANr(Object obj, int i, int i2);

    @Override // java.util.Comparator
    public abstract int compare(Object obj, Object obj2);
}
