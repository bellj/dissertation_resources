package X;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.os.Build;
import android.view.Choreographer;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.09T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09T extends ValueAnimator implements Choreographer.FrameCallback {
    public float A00 = 0.0f;
    public float A01 = 2.14748365E9f;
    public float A02 = -2.14748365E9f;
    public float A03 = 1.0f;
    public int A04 = 0;
    public long A05 = 0;
    public C05540Py A06;
    public boolean A07 = false;
    public boolean A08 = false;
    public final Set A09 = new CopyOnWriteArraySet();
    public final Set A0A = new CopyOnWriteArraySet();

    public float A00() {
        C05540Py r2 = this.A06;
        if (r2 == null) {
            return 0.0f;
        }
        float f = this.A01;
        if (f == 2.14748365E9f) {
            return r2.A00;
        }
        return f;
    }

    public float A01() {
        C05540Py r2 = this.A06;
        if (r2 == null) {
            return 0.0f;
        }
        float f = this.A02;
        if (f == -2.14748365E9f) {
            return r2.A02;
        }
        return f;
    }

    public void A02() {
        Choreographer.getInstance().removeFrameCallback(this);
        this.A07 = false;
        boolean z = false;
        if (this.A03 < 0.0f) {
            z = true;
        }
        A08(z);
    }

    public void A03() {
        for (ValueAnimator.AnimatorUpdateListener animatorUpdateListener : this.A0A) {
            animatorUpdateListener.onAnimationUpdate(this);
        }
    }

    public void A04() {
        float A01;
        this.A07 = true;
        boolean z = false;
        if (this.A03 < 0.0f) {
            z = true;
        }
        for (Animator.AnimatorListener animatorListener : this.A09) {
            if (Build.VERSION.SDK_INT >= 26) {
                animatorListener.onAnimationStart(this, z);
            } else {
                animatorListener.onAnimationStart(this);
            }
        }
        if (this.A03 < 0.0f) {
            A01 = A00();
        } else {
            A01 = A01();
        }
        A06((float) ((int) A01));
        this.A05 = 0;
        this.A04 = 0;
        A05();
    }

    public void A05() {
        if (this.A07) {
            Choreographer.getInstance().removeFrameCallback(this);
            Choreographer.getInstance().postFrameCallback(this);
        }
    }

    public void A06(float f) {
        if (this.A00 != f) {
            this.A00 = Math.max(A01(), Math.min(A00(), f));
            this.A05 = 0;
            A03();
        }
    }

    public void A07(float f, float f2) {
        float f3;
        float f4;
        if (f <= f2) {
            C05540Py r0 = this.A06;
            if (r0 == null) {
                f3 = -3.4028235E38f;
                f4 = Float.MAX_VALUE;
            } else {
                f3 = r0.A02;
                f4 = r0.A00;
            }
            this.A02 = Math.max(f3, Math.min(f4, f));
            this.A01 = Math.max(f3, Math.min(f4, f2));
            A06((float) ((int) Math.max(f, Math.min(f2, this.A00))));
            return;
        }
        throw new IllegalArgumentException(String.format("minFrame (%s) must be <= maxFrame (%s)", Float.valueOf(f), Float.valueOf(f2)));
    }

    public void A08(boolean z) {
        for (Animator.AnimatorListener animatorListener : this.A09) {
            if (Build.VERSION.SDK_INT >= 26) {
                animatorListener.onAnimationEnd(this, z);
            } else {
                animatorListener.onAnimationEnd(this);
            }
        }
    }

    @Override // android.animation.Animator
    public void addListener(Animator.AnimatorListener animatorListener) {
        this.A09.add(animatorListener);
    }

    @Override // android.animation.ValueAnimator
    public void addUpdateListener(ValueAnimator.AnimatorUpdateListener animatorUpdateListener) {
        this.A0A.add(animatorUpdateListener);
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public void cancel() {
        for (Animator.AnimatorListener animatorListener : this.A09) {
            animatorListener.onAnimationCancel(this);
        }
        Choreographer.getInstance().removeFrameCallback(this);
        this.A07 = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (r4 > r2) goto L_0x0040;
     */
    @Override // android.view.Choreographer.FrameCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doFrame(long r8) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass09T.doFrame(long):void");
    }

    @Override // android.animation.ValueAnimator
    public float getAnimatedFraction() {
        float A01;
        float f;
        float A00;
        if (this.A06 == null) {
            return 0.0f;
        }
        if (this.A03 < 0.0f) {
            A00 = A00();
            f = A00 - this.A00;
            A01 = A01();
        } else {
            float f2 = this.A00;
            A01 = A01();
            f = f2 - A01;
            A00 = A00();
        }
        return f / (A00 - A01);
    }

    @Override // android.animation.ValueAnimator
    public Object getAnimatedValue() {
        float f;
        C05540Py r0 = this.A06;
        if (r0 == null) {
            f = 0.0f;
        } else {
            float f2 = this.A00;
            float f3 = r0.A02;
            f = (f2 - f3) / (r0.A00 - f3);
        }
        return Float.valueOf(f);
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public long getDuration() {
        C05540Py r2 = this.A06;
        if (r2 == null) {
            return 0;
        }
        return (long) ((float) ((long) (((r2.A00 - r2.A02) / r2.A01) * 1000.0f)));
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public long getStartDelay() {
        throw new UnsupportedOperationException("LottieAnimator does not support getStartDelay.");
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public boolean isRunning() {
        return this.A07;
    }

    @Override // android.animation.Animator
    public void removeAllListeners() {
        this.A09.clear();
    }

    @Override // android.animation.ValueAnimator
    public void removeAllUpdateListeners() {
        this.A0A.clear();
    }

    @Override // android.animation.Animator
    public void removeListener(Animator.AnimatorListener animatorListener) {
        this.A09.remove(animatorListener);
    }

    @Override // android.animation.ValueAnimator
    public void removeUpdateListener(ValueAnimator.AnimatorUpdateListener animatorUpdateListener) {
        this.A0A.remove(animatorUpdateListener);
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public ValueAnimator setDuration(long j) {
        throw new UnsupportedOperationException("LottieAnimator does not support setDuration.");
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        throw new UnsupportedOperationException("LottieAnimator does not support setInterpolator.");
    }

    @Override // android.animation.ValueAnimator
    public void setRepeatMode(int i) {
        super.setRepeatMode(i);
        if (i != 2 && this.A08) {
            this.A08 = false;
            this.A03 = -this.A03;
        }
    }

    @Override // android.animation.ValueAnimator, android.animation.Animator
    public void setStartDelay(long j) {
        throw new UnsupportedOperationException("LottieAnimator does not support setStartDelay.");
    }
}
