package X;

import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextWatcher;
import android.view.View;

/* renamed from: X.2na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57762na extends AnonymousClass2k7 {
    public C57762na(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0189  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:94:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass2k7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A06(android.view.View r10, X.C14260l7 r11, X.AnonymousClass28D r12, java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 598
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57762na.A06(android.view.View, X.0l7, X.28D, java.lang.Object):void");
    }

    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r8, AnonymousClass28D r9, Object obj) {
        C15190mi r7 = (C15190mi) view;
        AnonymousClass3C4 r5 = (AnonymousClass3C4) AnonymousClass3JV.A04(r8, r9);
        r5.A0E = r7.getText();
        r5.A0D = r7.onSaveInstanceState();
        r7.removeTextChangedListener(r5.A0M);
        TextWatcher textWatcher = r5.A0H;
        if (textWatcher != null) {
            r7.removeTextChangedListener(textWatcher);
        }
        r7.setEnabled(true);
        r7.setFocusable(true);
        r7.setKeyListener(r5.A0I);
        r7.A00 = null;
        r7.setFilters(C15200mj.A00);
        r7.setOnFocusChangeListener(null);
        r7.setOnEditorActionListener(null);
        r7.setText("");
        r7.setGravity(8388659);
        r7.setTypeface(Typeface.DEFAULT);
        r7.setHint("");
        r7.setMaxLines(Integer.MAX_VALUE);
        r7.setImeOptions(r5.A02);
        r7.setTextColor(r5.A09);
        r7.setHintTextColor(r5.A08);
        r7.setBackground(r5.A0B);
        if (Build.VERSION.SDK_INT >= 29) {
            C15200mj.A05(r7, r5);
        }
        C15200mj.A07(r7, r5, false);
        C15200mj.A06(r7, r5, r5.A03);
        r7.setSingleLine(false);
        r7.setTextSize(0, r5.A00);
        Rect rect = r5.A0A;
        r7.setPadding(rect.left, rect.top, rect.right, rect.bottom);
        r5.A0K = null;
        r7.setEllipsize(r5.A0F);
    }
}
