package X;

import android.os.SystemClock;
import java.lang.ref.WeakReference;

/* renamed from: X.37r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625537r extends AbstractC16350or {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C16170oZ A01;
    public final WeakReference A02;
    public final boolean A03;
    public final boolean A04;

    public C625537r(AbstractC13860kS r3, C16170oZ r4, boolean z, boolean z2) {
        this.A02 = C12970iu.A10(r3);
        this.A01 = r4;
        this.A04 = z;
        this.A03 = z2;
    }
}
