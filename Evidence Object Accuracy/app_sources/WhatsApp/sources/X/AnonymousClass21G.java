package X;

import android.util.SparseArray;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.21G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21G {
    public final SparseArray A00 = new SparseArray();
    public final List A01;

    public AnonymousClass21G(List list, List list2) {
        this.A01 = list;
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            AnonymousClass21D r0 = (AnonymousClass21D) it.next();
            SparseArray sparseArray = this.A00;
            int i = r0.A00;
            Object obj = r0.A01;
            if (obj instanceof Boolean) {
                obj = Integer.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
            }
            sparseArray.append(i, obj.toString());
        }
    }
}
