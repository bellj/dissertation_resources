package X;

import android.content.Context;

/* renamed from: X.1Ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractIntentServiceC27641Ik extends AbstractIntentServiceC27651Il {
    public AbstractC18390sN A00;
    public AnonymousClass018 A01;

    public AbstractIntentServiceC27641Ik(String str) {
        super(str);
    }

    @Override // android.app.Service, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = r1.Ag8();
        C18380sM A1v = r1.A1v();
        this.A00 = A1v;
        super.attachBaseContext(new C48502Gn(context, A1v, this.A01));
    }
}
