package X;

import android.widget.BaseAdapter;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.2bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52782bg extends BaseAdapter {
    public final /* synthetic */ MessageDetailsActivity A00;

    @Override // android.widget.Adapter
    public int getCount() {
        return 1;
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return null;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 1;
    }

    public /* synthetic */ C52782bg(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x010a, code lost:
        if (r9 != null) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0110, code lost:
        if (r9 != null) goto L_0x00ea;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r14, android.view.View r15, android.view.ViewGroup r16) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52782bg.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
