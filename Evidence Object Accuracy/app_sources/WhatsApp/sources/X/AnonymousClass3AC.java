package X;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import java.util.Arrays;

/* renamed from: X.3AC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AC {
    public static Drawable A00(C14260l7 r6, AnonymousClass28D r7, AnonymousClass28D r8) {
        Drawable A01;
        float f;
        AnonymousClass28D A0F = r7.A0F(35);
        if (A0F == null) {
            C28691Op.A00(AnonymousClass3B0.A00, "Client received a RippleDrawable with null content");
        }
        if (A0F == null) {
            A01 = new ColorDrawable();
        } else {
            A01 = C65093Ic.A00().A06.A01(r6, A0F, r8);
        }
        ShapeDrawable shapeDrawable = null;
        if (r8 != null) {
            float[] fArr = new float[8];
            try {
                String A0I = r8.A0I(46);
                if (A0I == null) {
                    f = 0.0f;
                } else {
                    f = AnonymousClass3JW.A01(A0I);
                }
                Arrays.fill(fArr, f);
            } catch (AnonymousClass491 unused) {
                C28691Op.A00(AnonymousClass3B0.A00, "Error parsing Corner radius for Box decoration");
                Arrays.fill(fArr, 0.0f);
            }
            shapeDrawable = new ShapeDrawable(new RoundRectShape(fArr, null, null));
        }
        return new RippleDrawable(ColorStateList.valueOf(Color.parseColor(r7.A0I(38))), A01, shapeDrawable);
    }
}
