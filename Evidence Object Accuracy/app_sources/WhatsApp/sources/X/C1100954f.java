package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.54f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1100954f implements AnonymousClass5W1 {
    public UserJid A00;

    @Override // X.AnonymousClass5W1
    public int ADd() {
        return 1;
    }

    public C1100954f(UserJid userJid) {
        this.A00 = userJid;
    }

    @Override // X.AnonymousClass5W1
    public UserJid ADh() {
        return this.A00;
    }
}
