package X;

import android.os.ConditionVariable;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0410000_I0;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0z3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22420z3 {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final AnonymousClass018 A03;
    public final C15680nj A04;
    public final AnonymousClass10Y A05;
    public final AnonymousClass10U A06;
    public final C18470sV A07;
    public final C22290yq A08;
    public final AnonymousClass16O A09;
    public final C22170ye A0A;
    public final C20660w7 A0B;
    public final C22410z2 A0C;
    public final C22470z8 A0D;
    public final C244715q A0E;
    public final C244515o A0F;
    public final C22230yk A0G;
    public final C239913u A0H;
    public final C15860o1 A0I;
    public final AbstractC14440lR A0J;
    public final AnonymousClass12L A0K;
    public final C14890mD A0L;
    public final C14860mA A0M;

    public C22420z3(AbstractC15710nm r2, C15570nT r3, C15450nH r4, AnonymousClass018 r5, C15680nj r6, AnonymousClass10Y r7, AnonymousClass10U r8, C18470sV r9, C22290yq r10, AnonymousClass16O r11, C22170ye r12, C20660w7 r13, C22410z2 r14, C22470z8 r15, C244715q r16, C244515o r17, C22230yk r18, C239913u r19, C15860o1 r20, AbstractC14440lR r21, AnonymousClass12L r22, C14890mD r23, C14860mA r24) {
        this.A00 = r2;
        this.A01 = r3;
        this.A0J = r21;
        this.A0L = r23;
        this.A0M = r24;
        this.A0B = r13;
        this.A02 = r4;
        this.A07 = r9;
        this.A0A = r12;
        this.A0G = r18;
        this.A03 = r5;
        this.A08 = r10;
        this.A0E = r16;
        this.A09 = r11;
        this.A05 = r7;
        this.A0I = r20;
        this.A0H = r19;
        this.A0F = r17;
        this.A06 = r8;
        this.A0K = r22;
        this.A04 = r6;
        this.A0C = r14;
        this.A0D = r15;
    }

    public void A00(ConditionVariable conditionVariable, ConditionVariable conditionVariable2, AnonymousClass1IS r16, List list, int i, boolean z, boolean z2) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A0H.A00((AbstractC15340mz) it.next());
        }
        C22470z8 r2 = this.A0D;
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            if (!C30051Vw.A17((AbstractC15340mz) it2.next())) {
                throw new IllegalArgumentException("message thumb not loaded");
            }
        }
        if (r2.A03()) {
            r2.A00(conditionVariable, conditionVariable2, r16, null, list, null, i, 4, z, z2);
        }
    }

    public void A01(String str) {
        int i = 0;
        boolean z = false;
        if (str == null) {
            z = true;
        }
        ConditionVariable conditionVariable = new ConditionVariable();
        ConditionVariable conditionVariable2 = new ConditionVariable();
        HashMap hashMap = new HashMap();
        C15680nj r2 = this.A04;
        C15860o1 r1 = this.A0I;
        r2.A00.A0B();
        List<Jid> A08 = r2.A08(r1);
        ArrayList arrayList = new ArrayList();
        for (Jid jid : A08) {
            if (!C15380n4.A0O(jid)) {
                arrayList.add(jid);
            }
        }
        StringBuilder sb = new StringBuilder("web-message-send-methods/send-web-response-conversations: conversation list size is ");
        sb.append(arrayList.size());
        Log.i(sb.toString());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r22 = (AbstractC14640lm) it.next();
            AbstractC15340mz r12 = null;
            if (i < 1000 || C15380n4.A0F(r22) || !z) {
                r12 = this.A05.A00(r22);
                i++;
            }
            hashMap.put(r22, r12);
        }
        try {
            this.A0J.Ab2(new RunnableBRunnable0Shape0S0410000_I0(this, hashMap, conditionVariable, conditionVariable2, 2, z));
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("app/xmpp/send/qr_send_conv preempt:");
            sb2.append(z);
            sb2.append(" recents dispatch error ");
            Log.e(sb2.toString(), e);
            conditionVariable2.open();
        }
        try {
            this.A0J.Ab2(new Runnable(conditionVariable, conditionVariable2, this, str, hashMap, z) { // from class: X.22N
                public final /* synthetic */ ConditionVariable A00;
                public final /* synthetic */ ConditionVariable A01;
                public final /* synthetic */ C22420z3 A02;
                public final /* synthetic */ String A03;
                public final /* synthetic */ HashMap A04;
                public final /* synthetic */ boolean A05;

                {
                    this.A02 = r3;
                    this.A04 = r5;
                    this.A03 = r4;
                    this.A05 = r6;
                    this.A00 = r1;
                    this.A01 = r2;
                }

                /* JADX INFO: finally extract failed */
                @Override // java.lang.Runnable
                public final void run() {
                    int i2;
                    List list;
                    AnonymousClass1IS r0;
                    C22420z3 r4 = this.A02;
                    HashMap hashMap2 = this.A04;
                    String str2 = this.A03;
                    boolean z2 = this.A05;
                    ConditionVariable conditionVariable3 = this.A00;
                    ConditionVariable conditionVariable4 = this.A01;
                    ArrayList arrayList2 = new ArrayList();
                    long currentTimeMillis = System.currentTimeMillis();
                    ArrayList arrayList3 = new ArrayList();
                    int i3 = 0;
                    try {
                        for (Map.Entry entry : hashMap2.entrySet()) {
                            AbstractC14640lm r11 = (AbstractC14640lm) entry.getKey();
                            AbstractC15340mz r122 = (AbstractC15340mz) entry.getValue();
                            AnonymousClass1JW A01 = r4.A0E.A01(r11);
                            arrayList2.add(A01);
                            if (r122 != null && C15380n4.A0F(r11) && r122.A0C == 6) {
                                r4.A0G.A03((C29901Ve) r11, true);
                            }
                            if (currentTimeMillis - 86400000 < A01.A0A || A01.A09 > 0) {
                                if (r122 != null) {
                                    arrayList3.add(Pair.create(r122.A0z, Integer.valueOf(A01.A01)));
                                    A01.A0R = true;
                                }
                            }
                        }
                        C244515o r02 = r4.A0F;
                        if (z2) {
                            i3 = 7;
                        }
                        r02.A01(str2, null, arrayList2, i3, z2);
                        conditionVariable3.open();
                        Iterator it2 = arrayList3.iterator();
                        while (it2.hasNext()) {
                            Pair pair = (Pair) it2.next();
                            if (((Number) pair.second).intValue() < r4.A02.A02(C15450nH.A00)) {
                                list = r4.A08.A03((AnonymousClass1IS) pair.first, null, ((Number) pair.second).intValue() + 20);
                                r0 = null;
                                i2 = 1;
                            } else {
                                i2 = 5;
                                C244715q r13 = r4.A0E;
                                AbstractC14640lm r03 = ((AnonymousClass1IS) pair.first).A00;
                                AnonymousClass009.A05(r03);
                                Pair A00 = r13.A00(r03);
                                list = (List) A00.second;
                                r0 = (AnonymousClass1IS) A00.first;
                            }
                            if (list != null) {
                                r4.A00(null, conditionVariable4, r0, list, i2, false, z2);
                            }
                        }
                    } catch (Throwable th) {
                        C244515o r9 = r4.A0F;
                        if (z2) {
                            i3 = 7;
                        }
                        r9.A01(str2, null, arrayList2, i3, z2);
                        conditionVariable3.open();
                        throw th;
                    }
                }
            });
        } catch (Exception e2) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("app/xmpp/send/qr_send_conv preempt:");
            sb3.append(z);
            sb3.append(" chats/before dispatch error ");
            Log.e(sb3.toString(), e2);
            conditionVariable.open();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0069, code lost:
        if (r1 == Integer.MIN_VALUE) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.lang.String r22, java.lang.String r23, java.lang.String r24, java.lang.String r25, int r26) {
        /*
            r21 = this;
            r2 = r21
            X.0mD r0 = r2.A0L
            boolean r0 = r0.A02()
            r8 = r23
            r3 = r26
            if (r0 != 0) goto L_0x008d
            r7 = r22
            if (r22 == 0) goto L_0x008d
            X.018 r0 = r2.A03
            java.lang.String r12 = r0.A05()
            java.lang.String r13 = r0.A06()
            X.0mA r4 = r2.A0M
            r0 = 32
            byte[] r1 = new byte[r0]
            java.security.SecureRandom r0 = r4.A03()
            r0.nextBytes(r1)
            r0 = 2
            java.lang.String r11 = android.util.Base64.encodeToString(r1, r0)
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            if (r1 < r0) goto L_0x00c1
            android.os.LocaleList r0 = android.os.LocaleList.getDefault()
            java.lang.String r14 = r0.toLanguageTags()
        L_0x003c:
            X.12L r0 = r2.A0K
            byte[] r15 = r0.A00()
            X.0w7 r5 = r2.A0B
            X.0og r0 = r5.A01
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x008d
            X.0pI r0 = r5.A05
            android.content.Context r0 = r0.A00
            boolean r20 = android.text.format.DateFormat.is24HourFormat(r0)
            X.0sC r0 = r5.A02
            X.1cA r6 = r0.A00
            double r0 = r6.A00()
            boolean r4 = java.lang.Double.isNaN(r0)
            if (r4 == 0) goto L_0x00bf
            r4 = 0
        L_0x0061:
            int r1 = r6.A02
            if (r1 == 0) goto L_0x006b
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r18 = 1
            if (r1 != r0) goto L_0x006d
        L_0x006b:
            r18 = 0
        L_0x006d:
            X.143 r0 = r5.A04
            boolean r19 = r0.A06()
            X.0qS r1 = r5.A06
            r0 = 0
            r10 = r24
            r9 = r25
            r16 = r3
            r17 = r4
            X.22O r6 = new X.22O
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            r5 = 0
            r4 = 43
            android.os.Message r4 = android.os.Message.obtain(r5, r0, r4, r0, r6)
            r1.A08(r4, r0)
        L_0x008d:
            X.0mA r7 = r2.A0M
            X.0mD r1 = r7.A0N
            r0 = 0
            r1.A01(r8, r0)
            if (r26 == 0) goto L_0x009a
            r0 = 1
            if (r3 != r0) goto L_0x00ac
        L_0x009a:
            r6 = 0
            r2.A01(r6)
            X.0z2 r5 = r2.A0C
            r4 = 1
            X.0lR r3 = r5.A0C
            r1 = 4
            com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0
            r0.<init>(r5, r6, r1, r4)
            r3.Ab2(r0)
        L_0x00ac:
            X.0yk r4 = r2.A0G
            java.lang.String r3 = r7.A02()
            X.0lR r2 = r4.A0F
            r1 = 28
            com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0
            r0.<init>(r1, r3, r4)
            r2.Ab2(r0)
            return
        L_0x00bf:
            int r4 = (int) r0
            goto L_0x0061
        L_0x00c1:
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r14 = X.AbstractC27291Gt.A05(r0)
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22420z3.A02(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int):void");
    }

    public void A03(String str, HashMap hashMap, List list, int i) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A0H.A00((AbstractC15340mz) it.next());
        }
        C22470z8 r2 = this.A0D;
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            if (!C30051Vw.A17((AbstractC15340mz) it2.next())) {
                throw new IllegalArgumentException("message thumb not loaded");
            }
        }
        r2.A00(null, null, null, str, list, hashMap, -1, i, true, false);
    }
}
