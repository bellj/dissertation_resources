package X;

import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;

/* renamed from: X.5xV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129305xV {
    public int A00;
    public Camera A01;
    public AbstractC136156Lf A02;
    public C125395rB A03;
    public boolean A04;
    public final C129885yS A05;
    public final C1308560f A06;
    public volatile boolean A07;
    public volatile boolean A08;
    public volatile boolean A09;
    public volatile boolean A0A;

    public C129305xV(C129885yS r1, C1308560f r2) {
        this.A06 = r2;
        this.A05 = r1;
    }

    public final void A00(Point point, EnumC124565pk r7, AbstractC136156Lf r8) {
        if (r8 != null) {
            C125395rB r1 = this.A03;
            if (!(point == null || r1 == null)) {
                float[] fArr = {(float) point.x, (float) point.y};
                Matrix matrix = r1.A00;
                Matrix matrix2 = new Matrix();
                matrix.invert(matrix2);
                matrix2.mapPoints(fArr);
                point.set((int) fArr[0], (int) fArr[1]);
            }
            AnonymousClass61K.A00(new RunnableC135726Jo(point, r7, r8, this));
        }
    }
}
