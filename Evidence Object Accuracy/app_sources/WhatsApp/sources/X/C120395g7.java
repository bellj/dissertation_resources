package X;

import android.content.Context;

/* renamed from: X.5g7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120395g7 extends C126705tJ {
    public final Context A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C17220qS A03;
    public final C18650sn A04;

    public C120395g7(Context context, AbstractC15710nm r3, C14900mE r4, C17220qS r5, C1308460e r6, C18650sn r7, C18610sj r8) {
        super(r6.A04, r8);
        this.A00 = context;
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r5;
        this.A04 = r7;
    }
}
