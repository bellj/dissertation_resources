package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import com.whatsapp.R;

/* renamed from: X.2of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58342of extends AbstractC75803kU {
    public final AnonymousClass018 A00;
    public final /* synthetic */ AnonymousClass3IZ A01;

    public C58342of(AnonymousClass3IZ r1, AnonymousClass018 r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A01.A0S.length;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ Object A0G(ViewGroup viewGroup, int i) {
        AnonymousClass3IZ r4 = this.A01;
        View A0O = C12980iv.A0O(r4.A0C, R.layout.emoji_list);
        AbsListView absListView = (AbsListView) A0O.findViewById(16908298);
        AnonymousClass018 r6 = this.A00;
        if (!C28141Kv.A01(r6)) {
            i = (r4.A0S.length - 1) - i;
        }
        AnonymousClass2bf[] r5 = r4.A0S;
        if (r5[i] == null) {
            r5[i] = new AnonymousClass2bf(r4.A0A, r4, r6, i);
        }
        absListView.setAdapter((ListAdapter) r5[i]);
        absListView.setEmptyView(A0O.findViewById(16908292));
        absListView.setTag(Integer.valueOf(i));
        if (i == r4.A00) {
            absListView.setOnScrollListener(r4.A0J);
        }
        viewGroup.addView(A0O, 0);
        return A0O;
    }

    @Override // X.AbstractC75803kU
    public void A0I(ViewGroup viewGroup, Object obj, int i) {
        View view = (View) obj;
        viewGroup.removeView(view);
        ((AbsListView) view.findViewById(16908298)).setOnScrollListener(null);
    }

    @Override // X.AbstractC75803kU
    public boolean A0J(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }
}
