package X;

import android.os.Bundle;

/* renamed from: X.2Gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48572Gu {
    public static AbstractC009404s A00(ActivityC001000l r5, AbstractC009404s r6) {
        Bundle bundle;
        AbstractC009404s r3 = r6;
        AnonymousClass2IG A00 = ((AnonymousClass2FJ) AnonymousClass027.A00(AnonymousClass2FJ.class, r5)).A00();
        if (r5.getIntent() != null) {
            bundle = r5.getIntent().getExtras();
        } else {
            bundle = null;
        }
        if (r6 == null) {
            r3 = new AnonymousClass05E(A00.A00, bundle, r5);
        }
        return new C67363Rf(bundle, r3, r5, A00.A01, A00.A02);
    }

    public static AbstractC009404s A01(AnonymousClass01E r5, AbstractC009404s r6) {
        AbstractC009404s r3 = r6;
        AnonymousClass2IG A00 = ((C51112Sw) ((AbstractC51092Su) AnonymousClass027.A00(AbstractC51092Su.class, r5))).A0V.A00();
        Bundle bundle = r5.A05;
        if (r6 == null) {
            r3 = new AnonymousClass05E(A00.A00, bundle, r5);
        }
        return new C67363Rf(bundle, r3, r5, A00.A01, A00.A02);
    }
}
