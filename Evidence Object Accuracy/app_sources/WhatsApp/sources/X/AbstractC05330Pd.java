package X;

import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0Pd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC05330Pd {
    public final AnonymousClass0QN A00;
    public final AtomicBoolean A01 = new AtomicBoolean(false);
    public volatile AbstractC12830ic A02;

    public abstract String A01();

    public AbstractC05330Pd(AnonymousClass0QN r3) {
        this.A00 = r3;
    }

    public AbstractC12830ic A00() {
        AnonymousClass0QN r3 = this.A00;
        r3.A01();
        if (!this.A01.compareAndSet(false, true)) {
            return r3.A00(A01());
        }
        if (this.A02 == null) {
            this.A02 = r3.A00(A01());
        }
        return this.A02;
    }

    public void A02(AbstractC12830ic r3) {
        if (r3 == this.A02) {
            this.A01.set(false);
        }
    }
}
