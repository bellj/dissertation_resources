package X;

import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95364da {
    public static ConcurrentHashMap A00 = new ConcurrentHashMap();

    public AbstractC95364da() {
        throw null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:141:0x0415  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(java.lang.Class r41) {
        /*
        // Method dump skipped, instructions count: 1123
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC95364da.A00(java.lang.Class):void");
    }

    public static void A01(AnonymousClass4YT r6, C95574dz r7) {
        boolean z;
        int i;
        String str;
        String str2;
        String str3;
        int i2 = r7.A00;
        if (i2 != 12) {
            switch (i2) {
                case 1:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Boolean";
                    str2 = "valueOf";
                    str = "(Z)Ljava/lang/Boolean;";
                    break;
                case 2:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Character";
                    str2 = "valueOf";
                    str = "(C)Ljava/lang/Character;";
                    break;
                case 3:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Byte";
                    str2 = "valueOf";
                    str = "(B)Ljava/lang/Byte;";
                    break;
                case 4:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Short";
                    str2 = "valueOf";
                    str = "(S)Ljava/lang/Short;";
                    break;
                case 5:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Integer";
                    str2 = "valueOf";
                    str = "(I)Ljava/lang/Integer;";
                    break;
                case 6:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Float";
                    str2 = "valueOf";
                    str = "(F)Ljava/lang/Float;";
                    break;
                case 7:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Long";
                    str2 = "valueOf";
                    str = "(J)Ljava/lang/Long;";
                    break;
                case 8:
                    i = 184;
                    z = false;
                    str3 = "java/lang/Double";
                    str2 = "valueOf";
                    str = "(D)Ljava/lang/Double;";
                    break;
                default:
                    return;
            }
            r6.A07(str3, str2, str, i, z);
        }
    }
}
