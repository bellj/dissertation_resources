package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.media.transcode.MediaTranscodeService;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.1Gi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27181Gi extends AbstractC18860tB {
    public boolean A00 = false;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ C16210od A02;

    public C27181Gi(Context context, C16210od r3) {
        this.A01 = context;
        this.A02 = r3;
    }

    @Override // X.AbstractC18860tB
    public void A02(AbstractC15340mz r8, int i) {
        HashMap hashMap = MediaTranscodeService.A0B;
        synchronized (hashMap) {
            byte b = r8.A0y;
            if (b == 3 || b == 13 || b == 9 || (b == 2 && r8.A08 != 1)) {
                if (r8.A0C == 1) {
                    AnonymousClass1IS r4 = r8.A0z;
                    if (!hashMap.containsKey(r4)) {
                        hashMap.put(r4, (AbstractC16130oV) r8);
                        Context context = this.A01;
                        Intent action = new Intent(context, MediaTranscodeService.class).setAction("com.whatsapp.media.transcode.MediaTranscodeService.START");
                        if (!this.A02.A00) {
                            this.A00 = true;
                            AnonymousClass1Tv.A00(context, action);
                            StringBuilder sb = new StringBuilder();
                            sb.append("MediaTranscodeService/start-service-foreground for message:");
                            sb.append(r4);
                            Log.i(sb.toString());
                        } else {
                            try {
                                context.startService(action);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("MediaTranscodeService/start-service for message:");
                                sb2.append(r4);
                                Log.i(sb2.toString());
                            } catch (IllegalStateException unused) {
                                this.A00 = true;
                                AnonymousClass1Tv.A00(context, action);
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("MediaTranscodeService/start-service-foreground for message:");
                                sb3.append(r4);
                                Log.i(sb3.toString());
                            }
                        }
                    }
                } else {
                    AbstractC16130oV r2 = (AbstractC16130oV) hashMap.remove(r8.A0z);
                    if (r2 != null && hashMap.isEmpty()) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("MediaTranscodeService/stop-service for message:");
                        sb4.append(r2.A0z);
                        Log.i(sb4.toString());
                        if (Build.VERSION.SDK_INT < 26 || !this.A00) {
                            Context context2 = this.A01;
                            context2.stopService(new Intent(context2, MediaTranscodeService.class));
                        } else {
                            Context context3 = this.A01;
                            AnonymousClass1Tv.A00(context3, new Intent(context3, MediaTranscodeService.class).setAction("com.whatsapp.media.transcode.MediaTranscodeService.STOP"));
                        }
                        this.A00 = false;
                    }
                }
            }
        }
    }
}
