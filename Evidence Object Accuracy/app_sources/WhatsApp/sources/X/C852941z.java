package X;

import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.41z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852941z extends C27131Gd {
    public final /* synthetic */ MediaAlbumActivity A00;

    public C852941z(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        this.A00.A08.notifyDataSetChanged();
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        this.A00.A08.notifyDataSetChanged();
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A08.notifyDataSetChanged();
    }
}
