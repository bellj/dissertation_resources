package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.05K  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass05K {
    public abstract Intent A00(Context context, Object obj);

    public AnonymousClass0MP A01(Context context, Object obj) {
        return null;
    }

    public abstract Object A02(Intent intent, int i);
}
