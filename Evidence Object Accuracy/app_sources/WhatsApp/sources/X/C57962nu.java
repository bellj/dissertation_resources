package X;

import android.util.Log;
import android.util.Pair;
import java.util.List;

/* renamed from: X.2nu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57962nu extends AnonymousClass4WH {
    public final /* synthetic */ String A00;
    public final /* synthetic */ List A01;

    public C57962nu(String str, List list) {
        this.A00 = str;
        this.A01 = list;
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ void A00(Object obj) {
        Pair A01 = C64943Hn.A01((AnonymousClass28D) obj, this.A00);
        int A05 = C12960it.A05(A01.second);
        if (A05 < 0) {
            Log.w("ComponentTree", "replaceChild: No existing child found with specified ID in parent. New children have not been added to parent.");
            return;
        }
        List list = (List) A01.first;
        list.remove(A05);
        list.addAll(A05, C64943Hn.A02(this.A01));
    }
}
