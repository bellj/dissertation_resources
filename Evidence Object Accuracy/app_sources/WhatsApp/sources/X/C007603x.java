package X;

import android.app.Person;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.os.Build;
import android.os.PersistableBundle;
import android.os.UserHandle;
import android.text.TextUtils;
import androidx.core.graphics.drawable.IconCompat;
import java.util.Set;

/* renamed from: X.03x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C007603x {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public ComponentName A04;
    public Context A05;
    public PersistableBundle A06;
    public UserHandle A07;
    public C06140Si A08;
    public IconCompat A09;
    public CharSequence A0A;
    public CharSequence A0B;
    public CharSequence A0C;
    public String A0D;
    public String A0E;
    public Set A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L = true;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public Intent[] A0P;
    public C007303s[] A0Q;

    public static C06140Si A00(ShortcutInfo shortcutInfo) {
        String string;
        if (Build.VERSION.SDK_INT < 29) {
            PersistableBundle extras = shortcutInfo.getExtras();
            if (extras == null || (string = extras.getString("extraLocusId")) == null) {
                return null;
            }
            return new C06140Si(string);
        } else if (shortcutInfo.getLocusId() == null) {
            return null;
        } else {
            return C06140Si.A00(shortcutInfo.getLocusId());
        }
    }

    public static C007303s[] A01(PersistableBundle persistableBundle) {
        if (persistableBundle == null || !persistableBundle.containsKey("extraPersonCount")) {
            return null;
        }
        int i = persistableBundle.getInt("extraPersonCount");
        C007303s[] r3 = new C007303s[i];
        int i2 = 0;
        while (i2 < i) {
            StringBuilder sb = new StringBuilder("extraPerson_");
            int i3 = i2 + 1;
            sb.append(i3);
            r3[i2] = C007303s.A00(persistableBundle.getPersistableBundle(sb.toString()));
            i2 = i3;
        }
        return r3;
    }

    public ShortcutInfo A02() {
        int length;
        int length2;
        ShortcutInfo.Builder intents = new ShortcutInfo.Builder(this.A05, this.A0D).setShortLabel(this.A0B).setIntents(this.A0P);
        IconCompat iconCompat = this.A09;
        if (iconCompat != null) {
            intents.setIcon(iconCompat.A08(this.A05));
        }
        if (!TextUtils.isEmpty(this.A0C)) {
            intents.setLongLabel(this.A0C);
        }
        if (!TextUtils.isEmpty(this.A0A)) {
            intents.setDisabledMessage(this.A0A);
        }
        ComponentName componentName = this.A04;
        if (componentName != null) {
            intents.setActivity(componentName);
        }
        Set<String> set = this.A0F;
        if (set != null) {
            intents.setCategories(set);
        }
        intents.setRank(this.A02);
        PersistableBundle persistableBundle = this.A06;
        if (persistableBundle != null) {
            intents.setExtras(persistableBundle);
        }
        if (Build.VERSION.SDK_INT >= 29) {
            C007303s[] r0 = this.A0Q;
            if (r0 != null && (length2 = r0.length) > 0) {
                Person[] personArr = new Person[length2];
                int i = 0;
                do {
                    personArr[i] = this.A0Q[i].A01();
                    i++;
                } while (i < length2);
                intents.setPersons(personArr);
            }
            C06140Si r02 = this.A08;
            if (r02 != null) {
                intents.setLocusId(r02.A01());
            }
            intents.setLongLived(this.A0N);
        } else {
            PersistableBundle persistableBundle2 = this.A06;
            if (persistableBundle2 == null) {
                persistableBundle2 = new PersistableBundle();
                this.A06 = persistableBundle2;
            }
            C007303s[] r03 = this.A0Q;
            if (r03 != null && (length = r03.length) > 0) {
                persistableBundle2.putInt("extraPersonCount", length);
                int i2 = 0;
                while (true) {
                    C007303s[] r5 = this.A0Q;
                    if (i2 >= r5.length) {
                        break;
                    }
                    PersistableBundle persistableBundle3 = this.A06;
                    StringBuilder sb = new StringBuilder("extraPerson_");
                    int i3 = i2 + 1;
                    sb.append(i3);
                    persistableBundle3.putPersistableBundle(sb.toString(), r5[i2].A03());
                    i2 = i3;
                }
            }
            C06140Si r04 = this.A08;
            if (r04 != null) {
                this.A06.putString("extraLocusId", r04.A01);
            }
            this.A06.putBoolean("extraLongLived", this.A0N);
            intents.setExtras(this.A06);
        }
        return intents.build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        if (r4 != null) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002f, code lost:
        if (r4 != null) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(android.content.Intent r8) {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C007603x.A03(android.content.Intent):void");
    }
}
