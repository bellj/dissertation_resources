package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.3g9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerC73383g9 extends Handler {
    public HandlerC73383g9() {
    }

    public HandlerC73383g9(Looper looper) {
        super(looper);
    }

    public HandlerC73383g9(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
