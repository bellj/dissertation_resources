package X;

import java.nio.ByteBuffer;

/* renamed from: X.65z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65z implements AnonymousClass6MI {
    public int A00;
    public ByteBuffer A01;

    public AnonymousClass65z(ByteBuffer byteBuffer, int i) {
        this.A01 = byteBuffer;
        this.A00 = i;
    }

    @Override // X.AnonymousClass6MI
    public ByteBuffer ACN() {
        return this.A01;
    }

    @Override // X.AnonymousClass6MI
    public int AGK() {
        return this.A00;
    }
}
