package X;

import android.app.Application;
import android.text.TextUtils;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.2fK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53832fK extends AnonymousClass014 {
    public final AnonymousClass02P A00;
    public final AnonymousClass02P A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass1FB A04;
    public final AnonymousClass3EQ A05;
    public final AnonymousClass3EX A06;
    public final C18640sm A07;
    public final UserJid A08;

    public C53832fK(Application application, AnonymousClass1FB r11, AnonymousClass3EX r12, C18640sm r13, UserJid userJid, AnonymousClass1ZC r15) {
        super(application);
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A02 = A0T;
        AnonymousClass02P r4 = new AnonymousClass02P();
        this.A01 = r4;
        AnonymousClass02P r5 = new AnonymousClass02P();
        this.A00 = r5;
        AnonymousClass016 A0T2 = C12980iv.A0T();
        this.A03 = A0T2;
        this.A08 = userJid;
        this.A04 = r11;
        this.A06 = r12;
        this.A07 = r13;
        String valueOf = String.valueOf((int) ((AnonymousClass014) this).A00.getResources().getDimension(R.dimen.medium_thumbnail_size));
        ArrayList A0l = C12960it.A0l();
        for (AnonymousClass1ZF r0 : r15.A02) {
            for (AnonymousClass1ZG r02 : r0.A01) {
                A0l.add(r02.A00);
            }
        }
        AnonymousClass3EQ r2 = new AnonymousClass3EQ(userJid, valueOf, valueOf, A0l);
        this.A05 = r2;
        r12.A00 = A0T;
        r5.A0D(A0T2, new IDxObserverShape3S0100000_1_I1(this, 17));
        r4.A0D(A0T2, new AnonymousClass02B(r11, this, r15) { // from class: X.3RT
            public final /* synthetic */ AnonymousClass1FB A00;
            public final /* synthetic */ C53832fK A01;
            public final /* synthetic */ AnonymousClass1ZC A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                int i;
                C53832fK r03 = this.A01;
                AnonymousClass1ZC r42 = this.A02;
                AnonymousClass1FB r1 = this.A00;
                C90834Pk r122 = (C90834Pk) obj;
                AnonymousClass02P r22 = r03.A01;
                if (r122.A00 == 0) {
                    C90844Pl r14 = r1.A05;
                    float f = (float) C16590pI.A00(r14.A01).getDisplayMetrics().heightPixels;
                    float f2 = r14.A00;
                    if (f < f2) {
                        i = 1;
                    } else {
                        i = (int) Math.ceil((double) (f / f2));
                    }
                } else {
                    i = 0;
                }
                AnonymousClass3BR r52 = new AnonymousClass3BR(r42, i);
                List<C44691zO> list = r122.A01;
                ArrayList A0l2 = C12960it.A0l();
                if (list != null) {
                    for (C44691zO r6 : list) {
                        Map map = r52.A04;
                        String str = r6.A0D;
                        if (!map.containsKey(str)) {
                            map.put(str, r6);
                        }
                    }
                    int i2 = 0;
                    while (true) {
                        List list2 = r52.A03;
                        if (i2 >= list2.size()) {
                            break;
                        }
                        List<Object> A10 = C12980iv.A10(r52.A02, i2);
                        ArrayList A0l3 = C12960it.A0l();
                        for (Object obj2 : A10) {
                            C44691zO r16 = (C44691zO) r52.A04.get(obj2);
                            if (r16 != null) {
                                A0l3.add(new C1097953b(r16));
                            }
                        }
                        if (!A0l3.isEmpty()) {
                            String A0g = C12960it.A0g(list2, i2);
                            if (!TextUtils.isEmpty(A0g)) {
                                A0l2.add(new C1097853a(A0g));
                            }
                            A0l2.addAll(A0l3);
                        }
                        i2++;
                    }
                    int size = r52.A01 - list.size();
                    int i3 = r52.A00;
                    if (size > i3) {
                        size = i3;
                    }
                    for (int i4 = 0; i4 < size; i4++) {
                        A0l2.add(new AnonymousClass53Z());
                    }
                }
                r22.A0A(A0l2);
            }
        });
        r11.A00(A0T2, r2);
    }

    public void A04() {
        boolean A0B = this.A07.A0B();
        AnonymousClass1FB r0 = this.A04;
        AnonymousClass3EQ r1 = this.A05;
        if (A0B) {
            r0.A01(r1);
            return;
        }
        AnonymousClass2EK r02 = (AnonymousClass2EK) r0.A0C.get(r1);
        if (r02 != null && r02.A00 < r02.A07.size()) {
            C12960it.A1A(this.A00, 5);
        }
    }

    public void A05() {
        boolean A0B = this.A07.A0B();
        AnonymousClass1FB r4 = this.A04;
        if (A0B) {
            r4.A04.A0E(this.A08, 0);
            AnonymousClass3EQ r3 = this.A05;
            r4.A0C.remove(r3);
            AnonymousClass016 r2 = this.A03;
            r2.A0A(new C90834Pk(4));
            r4.A00(r2, r3);
            r4.A01(r3);
            return;
        }
        AnonymousClass2EK r0 = (AnonymousClass2EK) r4.A0C.get(this.A05);
        if (r0 != null && r0.A00 < r0.A07.size()) {
            C12960it.A1A(this.A00, 5);
        }
    }
}
