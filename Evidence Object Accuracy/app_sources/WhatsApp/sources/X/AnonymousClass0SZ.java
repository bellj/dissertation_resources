package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0SZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SZ {
    public final int A00;
    public final int A01;
    public final AnonymousClass0Q0 A02;
    public final List A03;
    public final boolean A04 = true;
    public final int[] A05;
    public final int[] A06;

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b6, code lost:
        r11 = r14.A02;
        r15 = r15 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fa, code lost:
        r2 = r14.A01;
        r15 = r15 - 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0SZ(X.AnonymousClass0Q0 r19, java.util.List r20, int[] r21, int[] r22) {
        /*
        // Method dump skipped, instructions count: 304
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SZ.<init>(X.0Q0, java.util.List, int[], int[]):void");
    }

    public static AnonymousClass0NJ A00(List list, int i, boolean z) {
        int size = list.size() - 1;
        while (size >= 0) {
            AnonymousClass0NJ r3 = (AnonymousClass0NJ) list.get(size);
            if (r3.A01 == i && r3.A02 == z) {
                list.remove(size);
                while (size < list.size()) {
                    AnonymousClass0NJ r2 = (AnonymousClass0NJ) list.get(size);
                    int i2 = r2.A00;
                    int i3 = -1;
                    if (z) {
                        i3 = 1;
                    }
                    r2.A00 = i2 + i3;
                    size++;
                }
                return r3;
            }
            size--;
        }
        return null;
    }

    public void A01(AbstractC12640iF r19) {
        AnonymousClass0Z3 r10;
        if (r19 instanceof AnonymousClass0Z3) {
            r10 = (AnonymousClass0Z3) r19;
        } else {
            r10 = new AnonymousClass0Z3(r19);
        }
        ArrayList arrayList = new ArrayList();
        int i = this.A01;
        int i2 = this.A00;
        List list = this.A03;
        for (int size = list.size() - 1; size >= 0; size--) {
            C04940Nq r13 = (C04940Nq) list.get(size);
            int i3 = r13.A00;
            int i4 = r13.A01 + i3;
            int i5 = r13.A02 + i3;
            if (i4 < i) {
                int i6 = i - i4;
                if (!this.A04) {
                    r10.AUs(i4, i6);
                } else {
                    for (int i7 = i6 - 1; i7 >= 0; i7--) {
                        int[] iArr = this.A06;
                        int i8 = i4 + i7;
                        int i9 = iArr[i8] & 31;
                        if (i9 == 0) {
                            r10.AUs(i4 + i7, 1);
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                ((AnonymousClass0NJ) it.next()).A00--;
                            }
                        } else if (i9 == 4 || i9 == 8) {
                            int i10 = iArr[i8] >> 5;
                            AnonymousClass0NJ A00 = A00(arrayList, i10, false);
                            r10.ASr(i4 + i7, A00.A00 - 1);
                            if (i9 == 4) {
                                r10.ANr(this.A02.A02(i8, i10), A00.A00 - 1, 1);
                            }
                        } else if (i9 == 16) {
                            arrayList.add(new AnonymousClass0NJ(i8, i4 + i7, true));
                        } else {
                            StringBuilder sb = new StringBuilder("unknown flag for pos ");
                            sb.append(i8);
                            sb.append(" ");
                            sb.append(Long.toBinaryString((long) i9));
                            throw new IllegalStateException(sb.toString());
                        }
                    }
                }
            }
            if (i5 < i2) {
                int i11 = i2 - i5;
                if (!this.A04) {
                    r10.ARP(i4, i11);
                } else {
                    for (int i12 = i11 - 1; i12 >= 0; i12--) {
                        int[] iArr2 = this.A05;
                        int i13 = i5 + i12;
                        int i14 = iArr2[i13] & 31;
                        if (i14 == 0) {
                            r10.ARP(i4, 1);
                            Iterator it2 = arrayList.iterator();
                            while (it2.hasNext()) {
                                ((AnonymousClass0NJ) it2.next()).A00++;
                            }
                        } else if (i14 == 4 || i14 == 8) {
                            int i15 = iArr2[i13] >> 5;
                            r10.ASr(A00(arrayList, i15, true).A00, i4);
                            if (i14 == 4) {
                                r10.ANr(this.A02.A02(i15, i13), i4, 1);
                            }
                        } else if (i14 == 16) {
                            arrayList.add(new AnonymousClass0NJ(i13, i4, false));
                        } else {
                            StringBuilder sb2 = new StringBuilder("unknown flag for pos ");
                            sb2.append(i13);
                            sb2.append(" ");
                            sb2.append(Long.toBinaryString((long) i14));
                            throw new IllegalStateException(sb2.toString());
                        }
                    }
                }
            }
            while (true) {
                i3--;
                if (i3 >= 0) {
                    int[] iArr3 = this.A06;
                    int i16 = r13.A01;
                    if ((iArr3[i16 + i3] & 31) == 2) {
                        int i17 = i16 + i3;
                        r10.ANr(this.A02.A02(i17, r13.A02 + i3), i17, 1);
                    }
                }
            }
            i = r13.A01;
            i2 = r13.A02;
        }
        r10.A00();
    }

    public void A02(AnonymousClass02M r2) {
        A01(new AnonymousClass0Z2(r2));
    }
}
