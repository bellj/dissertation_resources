package X;

/* renamed from: X.2iA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55302iA extends AnonymousClass0FK {
    public final C19990v2 A00;

    public C55302iA(AnonymousClass02M r1, C19990v2 r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0Z4
    public boolean A01(Object obj, Object obj2) {
        return C29941Vi.A00(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A02(Object obj, Object obj2) {
        return C29941Vi.A00(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        AnonymousClass1OU r4 = (AnonymousClass1OU) obj;
        AnonymousClass1OU r5 = (AnonymousClass1OU) obj2;
        C19990v2 r1 = this.A00;
        int A02 = r1.A02(r4.A02);
        if (A02 != r1.A02(r5.A02)) {
            return A02 == 3 ? -1 : 1;
        }
        return r4.A03.compareTo(r5.A03);
    }
}
