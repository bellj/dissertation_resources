package X;

/* renamed from: X.69V  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69V implements AbstractC35651iS {
    public final /* synthetic */ C123575nN A00;

    public AnonymousClass69V(C123575nN r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35651iS
    public void ATe(AnonymousClass1IR r4) {
        C123575nN r2 = this.A00;
        r2.A0d.A06("IN- HANDLE_SEND_AGAIN IndiaUpiPaymentTransactionDetailsViewModel#getPaymentTransactionObserver() trying to load the added txn");
        r2.A0C(r4);
    }

    @Override // X.AbstractC35651iS
    public void ATf(AnonymousClass1IR r5) {
        String str;
        String str2;
        C123575nN r3 = this.A00;
        C30931Zj r1 = r3.A0d;
        r1.A06("IN- HANDLE_SEND_AGAIN IndiaUpiPaymentTransactionDetailsViewModel#getPaymentTransactionObserver() txn update event is called");
        if (!r3.A01 || r5 == null || !r5.A0A()) {
            r1.A06("IN- HANDLE_SEND_AGAIN IndiaUpiPaymentTransactionDetailsViewModel#getPaymentTransactionObserver() trying to reload the updated txn");
            r3.A0C(r5);
            return;
        }
        C123535nJ r2 = new C123535nJ(101);
        r2.A02 = r5.A0L;
        r2.A05 = ((C118185bP) r3).A0C;
        if (r5.A0H()) {
            str = "SUCCESS";
        } else {
            str = "FAILURE";
        }
        r2.A04 = str;
        if (C28421Nd.A00(r5.A0J, 0) != 0) {
            str2 = "U13";
        } else {
            str2 = "00";
        }
        r2.A03 = str2;
        C118185bP.A01(r3, r2);
    }
}
