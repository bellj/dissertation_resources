package X;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117635aK extends FrameLayout implements AnonymousClass004 {
    public ImageView A00;
    public ImageView A01;
    public TextView A02;
    public TextView A03;
    public AnonymousClass2P7 A04;
    public boolean A05;

    public C117635aK(Context context) {
        super(context);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        LayoutInflater.from(context).inflate(R.layout.payment_card_view, (ViewGroup) this, true);
        setBottomDividerSpaceVisibility(0);
        this.A02 = C12960it.A0J(this, R.id.card_name);
        this.A03 = C12960it.A0J(this, R.id.card_number);
        this.A01 = C12970iu.A0L(this, R.id.card_network_icon);
        this.A00 = C12970iu.A0L(this, R.id.card_view_background);
        this.A00.setImageDrawable(new C117405Zu(context));
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00.setClipToOutline(true);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(((int) (((float) (View.MeasureSpec.getSize(i) - (getPaddingLeft() + getPaddingRight()))) * 0.62f)) + getPaddingTop() + getPaddingBottom(), 1073741824));
    }

    public void setBottomDividerSpaceVisibility(int i) {
        float f = 0.0f;
        if (i == 0) {
            f = 24.0f;
        }
        setPadding(AnonymousClass3G9.A01(getContext(), 24.0f), 0, AnonymousClass3G9.A01(getContext(), 24.0f), AnonymousClass3G9.A01(getContext(), f));
    }

    public void setCard(C30881Ze r8) {
        int A00;
        TextView textView = this.A02;
        Context context = getContext();
        Object[] A1a = C12980iv.A1a();
        A1a[0] = C30881Ze.A08(r8.A01);
        textView.setText(C12960it.A0X(context, AbstractC28901Pl.A02(r8.A00), A1a, 1, R.string.fb_pay_card_name));
        this.A03.setText(C12960it.A0X(getContext(), C117295Zj.A0R(r8.A09), new Object[1], 0, R.string.fb_pay_card_number));
        ImageView imageView = this.A01;
        int i = r8.A01;
        if (i == 0) {
            A00 = R.drawable.card_default;
        } else {
            A00 = C1311161i.A00(i);
        }
        imageView.setImageResource(A00);
    }

    public void setCardNameTextViewVisibility(int i) {
        this.A02.setVisibility(i);
    }

    public void setCardNetworkIconVisibility(int i) {
        this.A01.setVisibility(i);
    }

    public void setCardNumberTextColor(int i) {
        this.A03.setTextColor(i);
    }
}
