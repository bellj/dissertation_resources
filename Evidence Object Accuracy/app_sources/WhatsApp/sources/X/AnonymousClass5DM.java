package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DM implements Iterator, AbstractC16910px {
    public int A00;
    public final Object[] A01;

    public AnonymousClass5DM(Object[] objArr) {
        this.A01 = objArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A01.length);
    }

    @Override // java.util.Iterator
    public Object next() {
        try {
            Object[] objArr = this.A01;
            int i = this.A00;
            this.A00 = i + 1;
            return objArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.A00--;
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }
}
