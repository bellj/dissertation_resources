package X;

import android.view.ContentInfo;

/* renamed from: X.0SR  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0SR {
    public final AbstractC12630iE A00;

    public AnonymousClass0SR(AbstractC12630iE r1) {
        this.A00 = r1;
    }

    public static AnonymousClass0SR A00(ContentInfo contentInfo) {
        return new AnonymousClass0SR(new AnonymousClass0Y1(contentInfo));
    }

    public ContentInfo A01() {
        return this.A00.AHo();
    }

    public String toString() {
        return this.A00.toString();
    }
}
