package X;

import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;

/* renamed from: X.13Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13Y implements AbstractC15920o8 {
    public final C18800t4 A00;
    public final AbstractC14440lR A01;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{101};
    }

    public AnonymousClass13Y(C18800t4 r1, AbstractC14440lR r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (i != 101) {
            return false;
        }
        this.A01.Ab2(new RunnableBRunnable0Shape0S1100000_I0(26, (String) message.obj, this));
        return true;
    }
}
