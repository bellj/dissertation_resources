package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.ParcelFileDescriptor;
import com.whatsapp.util.Log;
import java.io.FileDescriptor;

/* renamed from: X.3Il  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Il {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r8 == -1) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(int r6, int r7, int r8, long r9) {
        /*
            r4 = r8
            r0 = -1
            r3 = 2147483647(0x7fffffff, float:NaN)
            if (r8 == r0) goto L_0x000a
            r3 = r8
            if (r8 != r0) goto L_0x000d
        L_0x000a:
            r4 = 2147483647(0x7fffffff, float:NaN)
        L_0x000d:
            r1 = -1
            int r0 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0020
            r2 = 0
        L_0x0014:
            r5 = 1
            r1 = 0
            X.1tm r0 = new X.1tm
            r0.<init>(r1, r2, r3, r4, r5)
            int r0 = X.C37501mV.A01(r0, r6, r7)
            return r0
        L_0x0020:
            java.lang.Long r2 = java.lang.Long.valueOf(r9)
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3Il.A00(int, int, int, long):int");
    }

    public static Bitmap A01(Bitmap bitmap, int i) {
        if (i != 0) {
            Matrix A01 = C13000ix.A01();
            A01.setRotate((float) i, ((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
            try {
                Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), A01, true);
                if (bitmap != createBitmap) {
                    bitmap.recycle();
                    return createBitmap;
                }
            } catch (OutOfMemoryError unused) {
            }
        }
        return bitmap;
    }

    public static Bitmap A02(ParcelFileDescriptor parcelFileDescriptor, int i, long j) {
        int i2;
        int i3;
        try {
            if (parcelFileDescriptor != null) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                    options.inJustDecodeBounds = true;
                    AnonymousClass3HO.A00().A02(options, fileDescriptor);
                    if (!(options.mCancel || (i2 = options.outWidth) == -1 || (i3 = options.outHeight) == -1)) {
                        options.inSampleSize = A00(i2, i3, i, j);
                        options.inJustDecodeBounds = false;
                        options.inDither = false;
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        return AnonymousClass3HO.A00().A02(options, fileDescriptor);
                    }
                } catch (OutOfMemoryError e) {
                    Log.e("GalleryPickerUtil/Got oom exception ", e);
                }
            }
            if (parcelFileDescriptor != null) {
                try {
                    parcelFileDescriptor.close();
                } catch (Throwable unused) {
                }
            }
            return null;
        } finally {
            try {
                parcelFileDescriptor.close();
            } catch (Throwable unused2) {
            }
        }
    }
}
