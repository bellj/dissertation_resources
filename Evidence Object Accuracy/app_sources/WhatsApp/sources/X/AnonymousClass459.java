package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.459  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass459 extends AnonymousClass57Y {
    public final AnonymousClass018 A00;
    public final AbstractC39801qZ A01;
    public final AnonymousClass1AB A02;

    public AnonymousClass459(Context context, C14330lG r8, AnonymousClass018 r9, AnonymousClass19M r10, AbstractC39801qZ r11, AbstractC14470lU r12, AnonymousClass1AB r13, String str) {
        super(context, r8, r10, r12, str);
        this.A00 = r9;
        this.A01 = r11;
        this.A02 = r13;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass3JD r1;
        File A0I = C22200yh.A0I(super.A01, this.A04);
        if (A0I.exists()) {
            r1 = AnonymousClass3JD.A02(super.A00, this.A00, super.A02, this.A02, A0I);
        } else {
            r1 = null;
        }
        this.A01.AS1(r1);
    }
}
