package X;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14180kz extends ThreadPoolExecutor {
    public final /* synthetic */ C14170ky A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C14180kz(C14170ky r10) {
        super(1, 1, 1, TimeUnit.MINUTES, new LinkedBlockingQueue());
        this.A00 = r10;
        setThreadFactory(new ThreadFactoryC71553d7(null));
        allowCoreThreadTimeOut(true);
    }

    @Override // java.util.concurrent.AbstractExecutorService
    public final RunnableFuture newTaskFor(Runnable runnable, Object obj) {
        return new C71773dU(this, obj, runnable);
    }
}
