package X;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.5Z5  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Z5<E> extends List<E>, RandomAccess {
    AnonymousClass5Z5 AhN(int i);
}
