package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.16Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16Q extends AbstractC17250qV implements AbstractC18870tC {
    public final C18850tA A00;
    public final C233411h A01;
    public final Map A02 = new HashMap();

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    public AnonymousClass16Q(AbstractC15710nm r9, C18850tA r10, C233411h r11, C17220qS r12, C17230qT r13, AbstractC14440lR r14) {
        super(r9, r12, r13, r14, new int[]{210}, true);
        this.A00 = r10;
        this.A01 = r11;
    }

    @Override // X.AbstractC18870tC
    public void ARC() {
        ExecutorC27271Gr r2 = this.A03;
        AnonymousClass009.A05(r2);
        r2.execute(new RunnableBRunnable0Shape8S0100000_I0_8(this, 41));
    }
}
