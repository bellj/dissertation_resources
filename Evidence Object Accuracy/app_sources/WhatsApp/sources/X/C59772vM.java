package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59772vM extends AbstractC37191le {
    public final WaTextView A00;

    public C59772vM(View view) {
        super(view);
        this.A00 = C12960it.A0N(view, R.id.neighbourhood);
    }
}
