package X;

import javax.net.ssl.X509TrustManager;

/* renamed from: X.46D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass46D extends AbstractC113555Ia {
    public final AnonymousClass14H A00;
    public final AnonymousClass14N A01;
    public final AnonymousClass14M A02;
    public final AnonymousClass14L A03;
    public final AnonymousClass14F A04;
    public final AnonymousClass14G A05;
    public final AnonymousClass14K A06;
    public final AnonymousClass14O A07;
    public final X509TrustManager A08;
    public final boolean A09;

    public AnonymousClass46D(AnonymousClass14H r1, AnonymousClass14N r2, AnonymousClass14M r3, AnonymousClass14L r4, AnonymousClass14F r5, AnonymousClass14G r6, AnonymousClass14K r7, AnonymousClass14O r8, X509TrustManager x509TrustManager, boolean z) {
        this.A09 = z;
        this.A08 = x509TrustManager;
        this.A02 = r3;
        this.A06 = r7;
        this.A03 = r4;
        this.A00 = r1;
        this.A05 = r6;
        this.A01 = r2;
        this.A04 = r5;
        this.A07 = r8;
    }
}
