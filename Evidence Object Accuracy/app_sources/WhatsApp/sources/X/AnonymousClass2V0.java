package X;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.ConversationListRowHeaderView;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2V0  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2V0 extends AnonymousClass2V1 {
    public AnonymousClass01T A00;
    public TextEmojiLabel A01;
    public C64303Fa A02;
    public C61252zf A03;
    public C61232zd A04;
    public CharSequence A05;
    public final int A06;
    public final int A07;
    public final C15570nT A08;
    public final C15550nR A09;
    public final C15610nY A0A;
    public final C63563Cb A0B;
    public final C63543Bz A0C;
    public final AnonymousClass01d A0D;
    public final C14830m7 A0E;
    public final AnonymousClass018 A0F;
    public final AnonymousClass19M A0G;
    public final C16630pM A0H;
    public final AnonymousClass12F A0I;

    public abstract CharSequence A02(C15370n3 v, AbstractC15340mz v2);

    public AnonymousClass2V0(Context context, C15570nT r3, C15550nR r4, C15610nY r5, C63563Cb r6, C63543Bz r7, AnonymousClass01d r8, C14830m7 r9, AnonymousClass018 r10, AnonymousClass19M r11, C16630pM r12, AnonymousClass12F r13) {
        super(context);
        this.A0E = r9;
        this.A08 = r3;
        this.A0G = r11;
        this.A0C = r7;
        this.A09 = r4;
        this.A0D = r8;
        this.A0A = r5;
        this.A0F = r10;
        this.A0I = r13;
        this.A0B = r6;
        this.A0H = r12;
        this.A06 = AnonymousClass00T.A00(context, R.color.list_item_sub_title);
        this.A07 = AnonymousClass00T.A00(context, R.color.list_item_title);
        if (!(this instanceof AnonymousClass34e) && !(this instanceof AnonymousClass34c)) {
            A01();
        }
    }

    public CharSequence A03(AbstractC15340mz r10, List list) {
        String A14;
        String str;
        if (this instanceof C621134m) {
            C28861Ph r102 = (C28861Ph) r10;
            if (!(((C621134m) this) instanceof C621434s)) {
                String str2 = "";
                if (r102.A0F().A00 != null) {
                    C30441Xk r1 = r102.A0F().A00;
                    if (!TextUtils.isEmpty(r1.A00)) {
                        str2 = r1.A00;
                    }
                    if (TextUtils.isEmpty(r102.A0I())) {
                        return str2;
                    }
                    StringBuilder A0k = C12960it.A0k("*");
                    A0k.append(r102.A0I());
                    A0k.append("*\n\n");
                    return C12960it.A0d(str2, A0k);
                } else if (r102.A14() != null) {
                    return r102.A14();
                } else {
                    return str2;
                }
            } else {
                if (r102.A14() == null) {
                    A14 = "";
                } else {
                    A14 = r102.A14();
                }
                if (list == null || "".equals(A14)) {
                    return A14;
                }
                String str3 = AnonymousClass3IJ.A00(r102).A03;
                if (str3 == null) {
                    str3 = "";
                }
                if (str3.isEmpty() || !A14.contains(str3)) {
                    return A14;
                }
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (str3.contains(C12970iu.A0x(it))) {
                        int indexOf = A14.indexOf(str3);
                        if (indexOf != 0 && indexOf != A14.length() - str3.length()) {
                            return A14;
                        }
                        String str4 = r102.A05;
                        String str5 = r102.A03;
                        String A00 = C65043Hx.A00(str3);
                        String[] A1b = C12990iw.A1b(str4, str5, 3);
                        A1b[2] = A00;
                        Iterator it2 = list.iterator();
                        while (true) {
                            int i = 0;
                            if (!it2.hasNext()) {
                                return A14;
                            }
                            String A0x = C12970iu.A0x(it2);
                            do {
                                String str6 = A1b[i];
                                if (str6 != null && str6.contains(A0x)) {
                                    return A14.replace(str3, "");
                                }
                                i++;
                            } while (i < 3);
                        }
                    }
                }
                return A14;
            }
        } else if (!(this instanceof AnonymousClass34r)) {
            if (((AbstractC51472Uz) this) instanceof AnonymousClass34e) {
                AnonymousClass1XP r103 = (AnonymousClass1XP) r10;
                if ((r103 instanceof C30341Xa) && (str = ((C30341Xa) r103).A03) != null) {
                    return str;
                }
            }
            return "";
        } else {
            AnonymousClass34r r2 = (AnonymousClass34r) this;
            AbstractC16130oV r104 = (AbstractC16130oV) r10;
            String A01 = C35011h5.A01(r104);
            if (!TextUtils.isEmpty(A01) || ((A01 = r104.A15()) != null && !C35011h5.A04(r104))) {
                return A01;
            }
            return r2.getDefaultMessageText();
        }
    }

    public void A04(C15370n3 r7, C15370n3 r8, AbstractC15340mz r9, List list) {
        C64303Fa r0;
        int i;
        this.A00 = new AnonymousClass01T(Boolean.TRUE, r8);
        this.A02.A03(r7, AnonymousClass3J9.A02, list);
        this.A02.A02(r7);
        setTitleColorBasedOnQuery(list);
        if (!(this instanceof AnonymousClass34r)) {
            C14830m7 r5 = this.A0E;
            AnonymousClass018 r4 = this.A0F;
            AnonymousClass01T r1 = new AnonymousClass01T(C38131nZ.A0A(r4, r5.A02(r9.A0I), false), C38131nZ.A0A(r4, r5.A02(r9.A0I), true));
            ConversationListRowHeaderView conversationListRowHeaderView = this.A02.A00;
            conversationListRowHeaderView.A01.setText((CharSequence) r1.A00);
            conversationListRowHeaderView.A01.setContentDescription((CharSequence) r1.A01);
            r0 = this.A02;
            i = 0;
        } else {
            r0 = this.A02;
            i = 8;
        }
        r0.A00.A01.setVisibility(i);
        A06(r9, list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r0 == null) goto L_0x0030;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AbstractC15340mz r12, java.util.List r13) {
        /*
            r11 = this;
            X.2zd r0 = r11.A04
            if (r0 == 0) goto L_0x0007
            r0.A00()
        L_0x0007:
            X.2zf r0 = r11.A03
            if (r0 == 0) goto L_0x000e
            r0.A00()
        L_0x000e:
            r8 = r13
            r11.setTitleColorBasedOnQuery(r13)
            X.0nR r3 = r11.A09
            X.1IS r0 = r12.A0z
            X.0lm r0 = r0.A00
            X.AnonymousClass009.A05(r0)
            X.0n3 r1 = r3.A08(r0)
            r2 = 0
            if (r1 == 0) goto L_0x0030
            X.0nT r0 = r11.A08
            X.0lm r0 = X.AnonymousClass3J0.A00(r0, r1, r12)
            if (r0 == 0) goto L_0x00a6
            X.0n3 r0 = r3.A08(r0)
            if (r0 != 0) goto L_0x00a7
        L_0x0030:
            X.3Fa r0 = r11.A02
            r0.A01()
            java.lang.Boolean r1 = java.lang.Boolean.FALSE
            X.01T r0 = new X.01T
            r0.<init>(r1, r2)
            r11.A00 = r0
            X.0nT r0 = r11.A08
            X.2zd r2 = new X.2zd
            r2.<init>(r0, r3, r12)
            r11.A04 = r2
            X.3Cb r1 = r11.A0B
            X.55W r0 = new X.55W
            r0.<init>(r12, r11, r13)
            r1.A00(r0, r2)
            java.lang.CharSequence r7 = r11.A03(r12, r13)
        L_0x0055:
            android.content.Context r1 = r11.getContext()
            com.whatsapp.TextEmojiLabel r0 = r11.A01
            android.text.TextPaint r2 = r0.getPaint()
            com.whatsapp.TextEmojiLabel r0 = r11.A01
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
            int r9 = r0.getMeasuredWidth()
            X.19M r5 = r11.A0G
            X.01d r3 = r11.A0D
            X.018 r4 = r11.A0F
            X.0pM r6 = r11.A0H
            X.2zf r0 = new X.2zf
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            r11.A03 = r0
            X.55X r5 = new X.55X
            r5.<init>(r12, r11, r13)
            int r1 = r7.length()
            r0 = 768(0x300, float:1.076E-42)
            if (r1 > r0) goto L_0x00b9
            X.1cY r10 = new X.1cY
            r10.<init>(r7)
            int r9 = r7.length()
            r8 = 0
            r7 = 0
        L_0x0092:
            if (r7 >= r9) goto L_0x00af
            r10.A00 = r7
            long r3 = com.whatsapp.emoji.EmojiDescriptor.A00(r10, r8)
            int r6 = r10.A01(r7, r3)
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x00b9
            int r7 = r7 + r6
            goto L_0x0092
        L_0x00a6:
            r0 = r2
        L_0x00a7:
            java.lang.CharSequence r7 = r11.A03(r12, r13)
            r11.A04(r1, r0, r12, r13)
            goto L_0x0055
        L_0x00af:
            X.2zf r0 = r11.A03
            java.lang.Object r0 = r0.call()     // Catch: 04U -> 0x00c7
            r5.AOO(r0)     // Catch: 04U -> 0x00c7
            return
        L_0x00b9:
            com.whatsapp.TextEmojiLabel r1 = r11.A01
            r0 = 80
            r1.setPlaceholder(r0)
            X.3Cb r1 = r11.A0B
            X.2zf r0 = r11.A03
            r1.A00(r5, r0)
        L_0x00c7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2V0.A05(X.0mz, java.util.List):void");
    }

    public final void A06(AbstractC15340mz r4, List list) {
        AnonymousClass01T r0 = this.A00;
        if (r0 != null && Boolean.TRUE.equals(r0.A00) && this.A05 != null) {
            CharSequence A02 = AnonymousClass3J0.A02(AnonymousClass3J9.A01(getContext(), this.A0F, A02((C15370n3) this.A00.A01, r4), list), this.A05);
            this.A01.setPlaceholder(0);
            if (TextUtils.isEmpty(A02)) {
                this.A01.setVisibility(8);
                return;
            }
            this.A01.setVisibility(0);
            this.A01.A0G(null, A02);
        }
    }

    public Paint getMessageViewPaint() {
        return this.A01.getPaint();
    }

    private void setTitleColorBasedOnQuery(List list) {
        C64303Fa r0;
        int i;
        if (list == null || list.isEmpty()) {
            r0 = this.A02;
            i = this.A07;
        } else {
            r0 = this.A02;
            i = this.A06;
        }
        r0.A00.A00.setTextColor(i);
    }
}
