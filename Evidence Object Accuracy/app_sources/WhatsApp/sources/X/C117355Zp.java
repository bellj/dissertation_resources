package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.5Zp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117355Zp extends BroadcastReceiver {
    public final /* synthetic */ AbstractActivityC121515iQ A00;

    public C117355Zp(AbstractActivityC121515iQ r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        AbstractActivityC121515iQ r1 = this.A00;
        C30861Zc r0 = r1.A00;
        if (r0 != null) {
            r1.A01.A01((C119755f3) r0.A08, null);
        } else {
            r1.A07.A06("onLibraryResult got resend otp but bankaccount is null");
        }
    }
}
