package X;

import android.content.Context;
import android.util.Base64;
import com.whatsapp.net.tls13.WtCachedPsk;
import com.whatsapp.util.Log;
import com.whatsapp.watls13.WtPersistentSession;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.14J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14J {
    public File A00;
    public final C16590pI A01;

    public AnonymousClass14J(C16590pI r1) {
        this.A01 = r1;
    }

    public final WtPersistentSession A00(File file) {
        LinkedHashSet linkedHashSet;
        if (file != null && file.exists()) {
            try {
                JSONObject jSONObject = new JSONObject(new String(AnonymousClass1NM.A00(file)));
                String string = jSONObject.getString("sni");
                int i = jSONObject.getInt("port");
                String string2 = jSONObject.getString("cipher");
                JSONArray optJSONArray = jSONObject.optJSONArray("psks");
                if (optJSONArray != null) {
                    linkedHashSet = new LinkedHashSet();
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64.decode(optJSONArray.getString(i2), 0));
                        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                        try {
                            WtCachedPsk wtCachedPsk = (WtCachedPsk) objectInputStream.readObject();
                            objectInputStream.close();
                            byteArrayInputStream.close();
                            linkedHashSet.add(wtCachedPsk);
                        } catch (Throwable th) {
                            try {
                                objectInputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                } else {
                    linkedHashSet = null;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("certs");
                HashMap hashMap = new HashMap();
                HashMap hashMap2 = new HashMap();
                Iterator<String> keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    Byte valueOf = Byte.valueOf(next);
                    JSONArray jSONArray = jSONObject2.getJSONArray(next);
                    Certificate[] certificateArr = new Certificate[jSONArray.length()];
                    for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i3);
                        String string3 = jSONObject3.getString("type");
                        byte[] decode = Base64.decode(jSONObject3.getString("data"), 0);
                        CertificateFactory certificateFactory = (CertificateFactory) hashMap.get(string3);
                        if (certificateFactory == null) {
                            certificateFactory = CertificateFactory.getInstance(string3);
                            hashMap.put(string3, certificateFactory);
                        }
                        ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(decode);
                        certificateArr[i3] = certificateFactory.generateCertificate(byteArrayInputStream2);
                        byteArrayInputStream2.close();
                    }
                    hashMap2.put(valueOf, certificateArr);
                }
                return new WtPersistentSession(string, string2, linkedHashSet, hashMap2, i);
            } catch (Exception e) {
                Log.w("WtPersistentSessionCacheImpl/readSession: unable to deserialize persisted session", e);
            }
        }
        return null;
    }

    public final File A01() {
        synchronized (this) {
            File file = this.A00;
            if (file == null) {
                Context context = this.A01.A00;
                if (context != null) {
                    File file2 = new File(context.getCacheDir(), "watls-sessions");
                    this.A00 = file2;
                    if (file2.exists() || this.A00.mkdir()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("WtPersistentSessionCacheImpl/getCacheDir: using external persistent cache directory ");
                        sb.append(this.A00.getAbsolutePath());
                        Log.i(sb.toString());
                        file = this.A00;
                    } else {
                        this.A00 = null;
                    }
                }
                return null;
            }
            return file;
        }
    }

    public synchronized void A02(Object obj, byte[] bArr) {
        JSONArray jSONArray;
        if (A01() != null) {
            File file = new File(A01(), Base64.encodeToString(bArr, 10));
            try {
                WtPersistentSession wtPersistentSession = (WtPersistentSession) obj;
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("sni", wtPersistentSession.A02);
                    jSONObject.put("port", wtPersistentSession.A00);
                    jSONObject.put("cipher", wtPersistentSession.A01);
                    LinkedHashSet linkedHashSet = wtPersistentSession.A03;
                    if (linkedHashSet != null) {
                        jSONArray = new JSONArray();
                        Iterator it = linkedHashSet.iterator();
                        while (it.hasNext()) {
                            WtCachedPsk wtCachedPsk = (WtCachedPsk) it.next();
                            try {
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                try {
                                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                                    objectOutputStream.writeObject(wtCachedPsk);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                    objectOutputStream.close();
                                    byteArrayOutputStream.close();
                                    jSONArray.put(Base64.encodeToString(byteArray, 2));
                                } finally {
                                }
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    } else {
                        jSONArray = null;
                    }
                    jSONObject.put("psks", jSONArray);
                    Map map = wtPersistentSession.A04;
                    JSONObject jSONObject2 = new JSONObject();
                    for (Map.Entry entry : map.entrySet()) {
                        JSONArray jSONArray2 = new JSONArray();
                        Certificate[] certificateArr = (Certificate[]) entry.getValue();
                        for (Certificate certificate : certificateArr) {
                            JSONObject jSONObject3 = new JSONObject();
                            jSONObject3.put("type", certificate.getType());
                            try {
                                jSONObject3.put("data", Base64.encodeToString(certificate.getEncoded(), 2));
                                jSONArray2.put(jSONObject3);
                            } catch (CertificateEncodingException e2) {
                                throw new RuntimeException(e2);
                            }
                        }
                        jSONObject2.put(String.valueOf(entry.getKey()), jSONArray2);
                    }
                    jSONObject.put("certs", jSONObject2);
                    C14350lI.A0F(file, jSONObject.toString().getBytes());
                } catch (JSONException e3) {
                    throw new RuntimeException(e3);
                }
            } catch (Exception e4) {
                StringBuilder sb = new StringBuilder();
                sb.append("WtPersistentSessionCacheImpl/putSession: Error during put session ");
                sb.append(file.getAbsolutePath());
                sb.append(" : ");
                sb.append(e4);
                Log.e(sb.toString());
            }
        }
    }

    public synchronized void A03(byte[] bArr) {
        if (A01() != null) {
            File file = new File(A01(), Base64.encodeToString(bArr, 10));
            if (!file.delete()) {
                StringBuilder sb = new StringBuilder();
                sb.append("WtPersistentSessionCacheImpl/removeSession: Error during remove session ");
                sb.append(file.getAbsolutePath());
                Log.e(sb.toString());
            }
        }
    }
}
