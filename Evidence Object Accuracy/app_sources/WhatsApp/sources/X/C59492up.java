package X;

import android.view.View;

/* renamed from: X.2up  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59492up extends C37171lc {
    public View.OnClickListener A00;

    public C59492up(View.OnClickListener onClickListener) {
        super(AnonymousClass39o.A0c);
        this.A00 = onClickListener;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((C59492up) obj).A00);
    }

    @Override // X.C37171lc
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.hashCode());
        return C12960it.A06(this.A00, A1a);
    }
}
