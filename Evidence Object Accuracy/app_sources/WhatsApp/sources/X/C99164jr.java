package X;

import android.os.Parcel;
import android.os.Parcelable;
import org.chromium.net.UrlRequest;

/* renamed from: X.4jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99164jr implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C78403or[] r4 = null;
        C78083oL[] r5 = null;
        int i = 0;
        int i2 = 0;
        float f = 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        float f5 = Float.MAX_VALUE;
        float f6 = Float.MAX_VALUE;
        float f7 = Float.MAX_VALUE;
        float f8 = 0.0f;
        float f9 = 0.0f;
        float f10 = 0.0f;
        float f11 = -1.0f;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    f = C95664e9.A00(parcel, readInt);
                    break;
                case 4:
                    f2 = C95664e9.A00(parcel, readInt);
                    break;
                case 5:
                    f3 = C95664e9.A00(parcel, readInt);
                    break;
                case 6:
                    f4 = C95664e9.A00(parcel, readInt);
                    break;
                case 7:
                    f5 = C95664e9.A00(parcel, readInt);
                    break;
                case '\b':
                    f6 = C95664e9.A00(parcel, readInt);
                    break;
                case '\t':
                    r4 = (C78403or[]) C95664e9.A0K(parcel, C78403or.CREATOR, readInt);
                    break;
                case '\n':
                    f8 = C95664e9.A00(parcel, readInt);
                    break;
                case 11:
                    f9 = C95664e9.A00(parcel, readInt);
                    break;
                case '\f':
                    f10 = C95664e9.A00(parcel, readInt);
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    r5 = (C78083oL[]) C95664e9.A0K(parcel, C78083oL.CREATOR, readInt);
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    f7 = C95664e9.A00(parcel, readInt);
                    break;
                case 15:
                    f11 = C95664e9.A00(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78433ou(r4, r5, f, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, i, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78433ou[i];
    }
}
