package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39v  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass39v extends Enum {
    public static final /* synthetic */ AnonymousClass39v[] A00;
    public static final AnonymousClass39v A01;
    public static final AnonymousClass39v A02;
    public static final AnonymousClass39v A03;
    public static final AnonymousClass39v A04;
    public static final AnonymousClass39v A05;
    public static final AnonymousClass39v A06;
    public static final AnonymousClass39v A07;
    public static final AnonymousClass39v A08;
    public static final AnonymousClass39v A09;
    public static final AnonymousClass39v A0A;
    public final int bits;
    public final int[] characterCountBitsForVersions;

    static {
        AnonymousClass39v r13 = new AnonymousClass39v("TERMINATOR", new int[]{0, 0, 0}, 0, 0);
        A0A = r13;
        AnonymousClass39v r11 = new AnonymousClass39v("NUMERIC", new int[]{10, 12, 14}, 1, 1);
        A08 = r11;
        AnonymousClass39v r10 = new AnonymousClass39v("ALPHANUMERIC", new int[]{9, 11, 13}, 2, 2);
        A01 = r10;
        AnonymousClass39v r9 = new AnonymousClass39v("STRUCTURED_APPEND", new int[]{0, 0, 0}, 3, 3);
        A09 = r9;
        AnonymousClass39v r8 = new AnonymousClass39v("BYTE", new int[]{8, 16, 16}, 4, 4);
        A02 = r8;
        AnonymousClass39v r7 = new AnonymousClass39v("ECI", new int[]{0, 0, 0}, 5, 7);
        A03 = r7;
        AnonymousClass39v r6 = new AnonymousClass39v("KANJI", new int[]{8, 10, 12}, 6, 8);
        A07 = r6;
        AnonymousClass39v r5 = new AnonymousClass39v("FNC1_FIRST_POSITION", new int[]{0, 0, 0}, 7, 5);
        A04 = r5;
        AnonymousClass39v r3 = new AnonymousClass39v("FNC1_SECOND_POSITION", new int[]{0, 0, 0}, 8, 9);
        A05 = r3;
        AnonymousClass39v r2 = new AnonymousClass39v("HANZI", new int[]{8, 10, 12}, 9, 13);
        A06 = r2;
        AnonymousClass39v[] r1 = new AnonymousClass39v[10];
        r1[0] = r13;
        r1[1] = r11;
        C12980iv.A1P(r10, r9, r8, r1);
        C12970iu.A1R(r7, r6, r5, r3, r1);
        r1[9] = r2;
        A00 = r1;
    }

    public AnonymousClass39v(String str, int[] iArr, int i, int i2) {
        this.characterCountBitsForVersions = iArr;
        this.bits = i2;
    }

    public int A00(C65243It r4) {
        char c;
        int i = r4.A01;
        if (i <= 9) {
            c = 0;
        } else {
            c = 2;
            if (i <= 26) {
                c = 1;
            }
        }
        return this.characterCountBitsForVersions[c];
    }

    public static AnonymousClass39v valueOf(String str) {
        return (AnonymousClass39v) Enum.valueOf(AnonymousClass39v.class, str);
    }

    public static AnonymousClass39v[] values() {
        return (AnonymousClass39v[]) A00.clone();
    }
}
