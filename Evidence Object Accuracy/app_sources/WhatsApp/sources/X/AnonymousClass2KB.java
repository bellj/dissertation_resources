package X;

/* renamed from: X.2KB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KB extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;

    public AnonymousClass2KB() {
        super(3182, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(7, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        StringBuilder sb = new StringBuilder("WamBackupQuotaUserNotice {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "bannerDismissed", obj);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "bannerShown", obj2);
        Integer num3 = this.A03;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "bannerTapLearnMore", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "gdriveSettingsClickedLearnMore", this.A00);
        Integer num4 = this.A04;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "notifClicked", obj4);
        Integer num5 = this.A05;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "notifDismissed", obj5);
        Integer num6 = this.A06;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "notifSent", obj6);
        sb.append("}");
        return sb.toString();
    }
}
