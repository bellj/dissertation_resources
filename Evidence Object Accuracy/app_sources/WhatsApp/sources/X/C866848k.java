package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.48k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C866848k extends ByteArrayOutputStream {
    public final /* synthetic */ AnonymousClass5G8 A00;

    public C866848k(AnonymousClass5G8 r1) {
        this.A00 = r1;
    }

    public byte[] A00() {
        return ((ByteArrayOutputStream) this).buf;
    }
}
