package X;

import java.util.concurrent.Callable;

/* renamed from: X.6Kd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135876Kd implements Callable {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass661 A01;
    public final /* synthetic */ String A02;

    public CallableC135876Kd(AnonymousClass661 r1, String str, long j) {
        this.A01 = r1;
        this.A02 = str;
        this.A00 = j;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011b, code lost:
        if (X.C117305Zk.A1Z(r1, 6) != false) goto L_0x011d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0131 A[Catch: all -> 0x0158, TryCatch #0 {all -> 0x0158, blocks: (B:38:0x012d, B:40:0x0131, B:41:0x013a), top: B:47:0x012d }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x013a A[Catch: all -> 0x0158, TRY_LEAVE, TryCatch #0 {all -> 0x0158, blocks: (B:38:0x012d, B:40:0x0131, B:41:0x013a), top: B:47:0x012d }] */
    @Override // java.util.concurrent.Callable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object call() {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: X.CallableC135876Kd.call():java.lang.Object");
    }
}
