package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68263Us implements AbstractC13940ka {
    public AnonymousClass016 A00;
    public AnonymousClass016 A01;
    public AnonymousClass016 A02;
    public AnonymousClass016 A03;
    public AnonymousClass016 A04;
    public final C14650lo A05;
    public final C30101Wc A06;
    public final C21770xx A07;
    public final AnonymousClass19Q A08;
    public final UserJid A09;
    public final AbstractC14440lR A0A;

    @Override // X.AbstractC13940ka
    public void AR0() {
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
    }

    public C68263Us(C14650lo r1, C30101Wc r2, C21770xx r3, AnonymousClass19Q r4, UserJid userJid, AbstractC14440lR r6) {
        this.A0A = r6;
        this.A09 = userJid;
        this.A07 = r3;
        this.A05 = r1;
        this.A08 = r4;
        this.A06 = r2;
    }
}
