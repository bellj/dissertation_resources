package X;

/* renamed from: X.5l7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122225l7 extends AbstractC1308360d {
    public final C17070qD A00;
    public final AbstractC14440lR A01;

    public C122225l7(C14900mE r20, C15570nT r21, ActivityC13790kL r22, C18640sm r23, C14830m7 r24, C18050rp r25, C21860y6 r26, C20400vh r27, C18650sn r28, C18600si r29, C18610sj r30, AnonymousClass61M r31, C17070qD r32, C129135xE r33, AnonymousClass60T r34, AnonymousClass61E r35, C130015yf r36, AnonymousClass6M6 r37, AbstractC14440lR r38) {
        super(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r33, r34, r35, r36, r37);
        this.A01 = r38;
        this.A00 = r32;
    }

    @Override // X.AbstractC1308360d
    public void A04(ActivityC13790kL r6, String str) {
        C32631cT r0;
        AbstractC460224d r2;
        if (str != null) {
            this.A02.add(str);
        }
        C460124c A01 = this.A08.A01(str);
        if (A01 != null && (r0 = A01.A03) != null && (r2 = r0.A00) != null && r2.A00.equals("WEBVIEW")) {
            if (((C460324e) r2).A00) {
                this.A01.Aaz(new AnonymousClass5oW(this.A05, this.A00, new C126945th(this, str)), new AbstractC001200n[0]);
            } else {
                A05(null, null);
            }
        }
    }
}
