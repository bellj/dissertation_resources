package X;

import android.util.SparseIntArray;

/* renamed from: X.2g6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54052g6 extends AnonymousClass0Q0 {
    public final C44091yC A00;
    public final C44091yC A01;
    public final String A02;
    public final String A03;

    public C54052g6(C44091yC r1, C44091yC r2, String str, String str2) {
        this.A01 = r1;
        this.A00 = r2;
        this.A03 = str;
        this.A02 = str2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        AnonymousClass2Vu r4 = this.A01.get(i);
        AnonymousClass2Vu r3 = this.A00.get(i2);
        int i3 = r3.A00;
        if (i3 == 1 || i3 == 12 || i3 == 4 || i3 == 6 || i3 == 3 || i3 == 2 || C36911kq.A00(i3)) {
            int i4 = r4.A00;
            if (C36911kq.A00(i4) || C36911kq.A00(i3)) {
                return C29941Vi.A00(Integer.valueOf(i4), Integer.valueOf(i3));
            }
            Object obj = r4.A01;
            if (i4 != 4) {
                return C29941Vi.A00(obj, r3.A01);
            }
            SparseIntArray sparseIntArray = (SparseIntArray) obj;
            SparseIntArray sparseIntArray2 = (SparseIntArray) r3.A01;
            if (sparseIntArray.size() != sparseIntArray2.size()) {
                return false;
            }
            for (int i5 = 0; i5 < sparseIntArray.size(); i5++) {
                if (!(sparseIntArray.keyAt(i5) == sparseIntArray2.keyAt(i5) && sparseIntArray.valueAt(i5) == sparseIntArray2.valueAt(i5))) {
                    return false;
                }
            }
            return true;
        } else if (!C29941Vi.A00(Integer.valueOf(r4.A00), Integer.valueOf(i3)) || !C29941Vi.A00(this.A03, this.A02)) {
            return false;
        } else {
            return true;
        }
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        AnonymousClass2Vu r5 = this.A01.get(i);
        AnonymousClass2Vu r4 = this.A00.get(i2);
        int i3 = r5.A00;
        if (!C36911kq.A00(i3)) {
            int i4 = r4.A00;
            if (!C36911kq.A00(i4)) {
                if (i3 == 4) {
                    return C12960it.A1V(i4, 4);
                }
                if (!this.A03.equals(this.A02)) {
                    return C29941Vi.A00(Integer.valueOf(i3), Integer.valueOf(i4));
                }
            }
        }
        return C29941Vi.A00(r5.A01, r4.A01);
    }
}
