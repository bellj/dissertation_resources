package X;

import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;

/* renamed from: X.39L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39L extends AbstractC14670lq {
    public final /* synthetic */ Conversation A00;
    public final /* synthetic */ C14310lE A01;
    public final /* synthetic */ C15260mp A02;
    public final /* synthetic */ AnonymousClass2IC A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass39L(View view, ActivityC000900k r40, Conversation conversation, AbstractC15710nm r42, AbstractC13860kS r43, C14330lG r44, C14900mE r45, C15450nH r46, C16170oZ r47, AudioRecordFactory audioRecordFactory, OpusRecorderFactory opusRecorderFactory, C18280sC r50, AnonymousClass11P r51, AnonymousClass2IA r52, C14310lE r53, AnonymousClass01d r54, C14830m7 r55, C14820m6 r56, AnonymousClass018 r57, C21310xD r58, C14850m9 r59, C22050yP r60, C15260mp r61, C14410lO r62, AnonymousClass109 r63, C16630pM r64, C20320vZ r65, AnonymousClass1AL r66, AnonymousClass199 r67, C255819y r68, AbstractC14440lR r69, C17020q8 r70, C254419k r71, C14680lr r72, AnonymousClass19A r73, C63683Cn r74, AnonymousClass2IC r75, C21260x8 r76) {
        super(view, r40, r42, r43, r44, r45, r46, r47, audioRecordFactory, opusRecorderFactory, r50, r51, r52, r54, r55, r56, r57, r58, r59, r60, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r76, true, true);
        this.A03 = r75;
        this.A00 = conversation;
        this.A01 = r53;
        this.A02 = r61;
    }
}
