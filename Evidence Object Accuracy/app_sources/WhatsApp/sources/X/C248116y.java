package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.16y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C248116y extends AbstractC18860tB {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C247916w A01;
    public final C248016x A02;
    public final Map A03 = new LinkedHashMap();
    public final Executor A04;

    public C248116y(C247916w r4, C248016x r5, AbstractC14440lR r6) {
        ExecutorC27271Gr r2 = new ExecutorC27271Gr(r6, false);
        this.A01 = r4;
        this.A02 = r5;
        this.A04 = r2;
    }

    public final void A03() {
        boolean z;
        C247916w r1 = this.A01;
        ArrayList arrayList = new ArrayList(this.A03.values());
        for (AnonymousClass4LB r0 : r1.A01()) {
            C64153El r2 = r0.A00;
            if (arrayList.isEmpty()) {
                z = false;
            } else if (!r2.A04.A00) {
                z = true;
            }
            AnonymousClass4ON r02 = r2.A04;
            r02.A00 = z;
            r02.A01 = z;
            r2.A00();
        }
    }

    public final void A04(AbstractC15340mz r4, int i) {
        C16150oX r1;
        AnonymousClass1IS r12 = r4.A0z;
        if (C15380n4.A0N(r12.A00) && r12.A02) {
            if ((r4 instanceof AbstractC16130oV) && (i != 1 || (r1 = ((AbstractC16130oV) r4).A02) == null || !r1.A0O || r1.A0a || !r1.A0P)) {
                return;
            }
            if (!(r4 instanceof C30331Wz) || ((C30331Wz) r4).A01 != null) {
                this.A04.execute(new RunnableBRunnable0Shape7S0200000_I0_7(this, 46, r4));
            }
        }
    }
}
