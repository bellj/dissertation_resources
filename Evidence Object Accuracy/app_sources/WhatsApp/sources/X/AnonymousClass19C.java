package X;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.widget.RemoteViews;
import com.whatsapp.R;
import com.whatsapp.service.BackgroundMediaControlService;

/* renamed from: X.19C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19C {
    public long A00;
    public C005602s A01;
    public AbstractC15340mz A02;
    public String A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public final C15570nT A07;
    public final AnonymousClass130 A08;
    public final C15550nR A09;
    public final C15610nY A0A;
    public final C21270x9 A0B;
    public final AnonymousClass11P A0C;
    public final AnonymousClass01d A0D;
    public final C16590pI A0E;
    public final C18360sK A0F;

    public AnonymousClass19C(C15570nT r1, AnonymousClass130 r2, C15550nR r3, C15610nY r4, C21270x9 r5, AnonymousClass11P r6, AnonymousClass01d r7, C16590pI r8, C18360sK r9) {
        this.A0E = r8;
        this.A07 = r1;
        this.A0B = r5;
        this.A08 = r2;
        this.A09 = r3;
        this.A0D = r7;
        this.A0A = r4;
        this.A0F = r9;
        this.A0C = r6;
    }

    public final void A00(RemoteViews remoteViews, boolean z) {
        int i;
        int i2;
        String str = this.A03;
        if (str != null) {
            remoteViews.setTextViewText(R.id.ongoing_media_text, str);
        }
        Context context = this.A0E.A00;
        Intent intent = new Intent(context, BackgroundMediaControlService.class);
        if (z) {
            intent.setAction("com.whatsapp.service.BackgroundMediaControlService.STOP");
            i = R.id.ongoing_media_control_btn;
            remoteViews.setImageViewResource(R.id.ongoing_media_control_btn, R.drawable.inline_audio_pause);
            i2 = R.string.pause;
        } else {
            intent.setAction("com.whatsapp.service.BackgroundMediaControlService.START");
            i = R.id.ongoing_media_control_btn;
            remoteViews.setImageViewResource(R.id.ongoing_media_control_btn, R.drawable.inline_audio_play);
            i2 = R.string.play;
        }
        remoteViews.setContentDescription(i, context.getString(i2));
        this.A01.A0E(z);
        this.A05 = z;
        remoteViews.setOnClickPendingIntent(i, AnonymousClass1UY.A03(context, intent, 134217728));
        C005602s r0 = this.A01;
        r0.A0E = remoteViews;
        this.A0F.A03(14, r0.A01());
    }

    public void A01(C35191hP r8) {
        boolean A0I = r8.A0I();
        if (!this.A04) {
            RemoteViews remoteViews = new RemoteViews(this.A0E.A00.getPackageName(), (int) R.layout.ongoing_media_notification);
            int A02 = r8.A02();
            remoteViews.setProgressBar(R.id.ongoing_media_audio_seekbar, r8.A03, A02, false);
            remoteViews.setTextViewText(R.id.ongoing_media_timeLeft, DateUtils.formatElapsedTime((long) (A02 / 1000)));
            A00(remoteViews, A0I);
            return;
        }
        boolean z = this.A05;
        if (!A0I ? !z : z) {
            if (!this.A06) {
                return;
            }
        }
        A00(new RemoteViews(this.A0E.A00.getPackageName(), (int) R.layout.ongoing_media_notification_talkback), A0I);
        this.A06 = false;
    }
}
