package X;

/* renamed from: X.43s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856743s extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public Long A09;
    public String A0A;
    public String A0B;

    public C856743s() {
        super(3008, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A02);
        r3.Abe(9, this.A03);
        r3.Abe(2, this.A0A);
        r3.Abe(6, this.A0B);
        r3.Abe(13, this.A00);
        r3.Abe(10, this.A01);
        r3.Abe(5, this.A04);
        r3.Abe(12, this.A05);
        r3.Abe(4, this.A08);
        r3.Abe(7, this.A09);
        r3.Abe(1, this.A06);
        r3.Abe(3, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamBizProfileView {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizFbSize", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizIgSize", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessProfileJid", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasCoverPhoto", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isProfileLinked", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "linkedAccount", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profileEntryPoint", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "scrollDepth", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "viewBusinessProfileAction", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "websiteSource", C12960it.A0Y(this.A07));
        return C12960it.A0d("}", A0k);
    }
}
