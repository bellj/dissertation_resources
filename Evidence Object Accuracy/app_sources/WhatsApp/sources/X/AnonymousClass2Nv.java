package X;

/* renamed from: X.2Nv  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Nv extends AnonymousClass1JW {
    public final int A00;
    public final int A01;
    public final long A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;

    public AnonymousClass2Nv(AbstractC15710nm r2, C15450nH r3, String str, String str2, String str3, String str4, String str5, byte[] bArr, int i, int i2, int i3, long j) {
        super(r2, r3);
        super.A05 = 5;
        ((AnonymousClass1JX) this).A00 = i;
        this.A07 = str;
        this.A0U = bArr;
        this.A05 = str2;
        this.A03 = str3;
        this.A04 = str4;
        this.A06 = str5;
        this.A01 = i2;
        this.A00 = i3;
        this.A02 = j;
    }

    public static AnonymousClass2Nv A03(AbstractC15710nm r14, C15450nH r15, int i) {
        return new AnonymousClass2Nv(r14, r15, null, null, null, null, null, null, i, 0, 0, 0);
    }

    @Override // X.AnonymousClass1JX
    public String toString() {
        StringBuilder sb = new StringBuilder("NonMessageMediaUploadResponse{directpath='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", encFilehash='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", mimeType='");
        sb.append(this.A06);
        sb.append('\'');
        sb.append(", mediaId='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.A01);
        sb.append(", height=");
        sb.append(this.A00);
        sb.append(", size=");
        sb.append(this.A02);
        sb.append(", code=");
        sb.append(((AnonymousClass1JX) this).A00);
        sb.append('}');
        return sb.toString();
    }
}
