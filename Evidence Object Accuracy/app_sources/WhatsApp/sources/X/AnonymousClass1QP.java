package X;

import android.content.Context;
import android.util.Log;
import java.io.File;

/* renamed from: X.1QP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QP extends AnonymousClass1QG {
    public int A00;
    public Context A01;
    public AnonymousClass1QF A02;

    public AnonymousClass1QP(Context context, int i) {
        Context applicationContext = context.getApplicationContext();
        this.A01 = applicationContext;
        if (applicationContext == null) {
            Log.w("SoLoader", "context.getApplicationContext returned null, holding reference to original context.");
            this.A01 = context;
            applicationContext = context;
        }
        this.A00 = i;
        this.A02 = new AnonymousClass1QF(new File(applicationContext.getApplicationInfo().nativeLibraryDir), i);
    }

    public String toString() {
        return this.A02.toString();
    }
}
