package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.2m0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56822m0 extends AnonymousClass1K7<Integer> implements AbstractC41941uP, RandomAccess {
    public static final C56822m0 A02;
    public int A00;
    public int[] A01;

    static {
        C56822m0 r0 = new C56822m0(new int[10], 0);
        A02 = r0;
        ((AnonymousClass1K7) r0).A00 = false;
    }

    public C56822m0(int[] iArr, int i) {
        this.A01 = iArr;
        this.A00 = i;
    }

    public final void A01(int i) {
        if (i < 0 || i >= this.A00) {
            StringBuilder A0k = C12960it.A0k("Index:");
            A0k.append(i);
            A0k.append(", Size:");
            throw new IndexOutOfBoundsException(C12960it.A0f(A0k, this.A00));
        }
    }

    public final void A02(int i, int i2) {
        int i3;
        A00();
        if (i < 0 || i > (i3 = this.A00)) {
            StringBuilder A0k = C12960it.A0k("Index:");
            A0k.append(i);
            A0k.append(", Size:");
            throw new IndexOutOfBoundsException(C12960it.A0f(A0k, this.A00));
        }
        int[] iArr = this.A01;
        if (i3 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i3 - i);
        } else {
            int[] iArr2 = new int[((i3 * 3) >> 1) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.A01, i, iArr2, i + 1, this.A00 - i);
            this.A01 = iArr2;
        }
        this.A01[i] = i2;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AbstractC41941uP
    /* renamed from: ALY */
    public AbstractC41941uP ALZ(int i) {
        if (i >= this.A00) {
            return new C56822m0(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ void add(int i, Object obj) {
        A02(i, C12960it.A05(obj));
    }

    @Override // X.AnonymousClass1K7, java.util.AbstractCollection, java.util.List, java.util.Collection
    public boolean addAll(Collection collection) {
        A00();
        if (!(collection instanceof C56822m0)) {
            return super.addAll(collection);
        }
        C56822m0 r7 = (C56822m0) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.A01;
            if (i3 > iArr.length) {
                iArr = Arrays.copyOf(iArr, i3);
                this.A01 = iArr;
            }
            System.arraycopy(r7.A01, 0, iArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AnonymousClass1K7, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C56822m0)) {
                return super.equals(obj);
            }
            C56822m0 r8 = (C56822m0) obj;
            int i = this.A00;
            if (i == r8.A00) {
                int[] iArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == iArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object get(int i) {
        A01(i);
        return Integer.valueOf(this.A01[i]);
    }

    @Override // X.AnonymousClass1K7, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + this.A01[i2];
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object remove(int i) {
        A00();
        A01(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        System.arraycopy(iArr, i + 1, iArr, i, this.A00 - i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i2);
    }

    @Override // X.AnonymousClass1K7, java.util.AbstractCollection, java.util.List, java.util.Collection
    public boolean remove(Object obj) {
        A00();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Integer.valueOf(this.A01[i]))) {
                int[] iArr = this.A01;
                System.arraycopy(iArr, i + 1, iArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        int A05 = C12960it.A05(obj);
        A00();
        A01(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        iArr[i] = A05;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public int size() {
        return this.A00;
    }
}
