package X;

/* renamed from: X.0aY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08080aY implements AbstractC12660iI {
    @Override // X.AbstractC12660iI
    public float ACj() {
        return 1.0f;
    }

    @Override // X.AbstractC12660iI
    public float AGu() {
        return 0.0f;
    }

    @Override // X.AbstractC12660iI
    public boolean AKI(float f) {
        return false;
    }

    @Override // X.AbstractC12660iI
    public boolean isEmpty() {
        return true;
    }

    @Override // X.AbstractC12660iI
    public AnonymousClass0U8 AC6() {
        throw new IllegalStateException("not implemented");
    }

    @Override // X.AbstractC12660iI
    public boolean AJF(float f) {
        throw new IllegalStateException("not implemented");
    }
}
