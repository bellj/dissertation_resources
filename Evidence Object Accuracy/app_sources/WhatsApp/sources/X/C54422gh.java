package X;

import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.2gh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54422gh extends AnonymousClass02M {
    public ArrayList A00;
    public AbstractC69213Yj[] A01;
    public final /* synthetic */ C69233Yl A02;

    public C54422gh(C69233Yl r2, ArrayList arrayList, AbstractC69213Yj[] r4) {
        this.A02 = r2;
        this.A00 = arrayList;
        this.A01 = r4;
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        if (!super.A00) {
            return -1;
        }
        String str = ((C93964b2) this.A00.get(i)).A01;
        C69233Yl r5 = this.A02;
        HashMap hashMap = r5.A0C;
        Number number = (Number) hashMap.get(str);
        if (number == null) {
            long j = r5.A03;
            r5.A03 = 1 + j;
            number = Long.valueOf(j);
            hashMap.put(str, number);
        }
        return number.longValue();
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r2) {
        if (r2 instanceof C55152hs) {
            ((C55152hs) r2).A08();
        }
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01ae, code lost:
        if (r3 != 3) goto L_0x01b0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01df  */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r21, int r22) {
        /*
        // Method dump skipped, instructions count: 483
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54422gh.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C69233Yl r0 = this.A02;
        if (i == 1) {
            return new C55152hs(r0.A07, viewGroup, r0.A0B);
        }
        return new C54962hZ(r0.A07, viewGroup);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return this.A00.get(i) instanceof AnonymousClass47G ? 1 : 0;
    }
}
