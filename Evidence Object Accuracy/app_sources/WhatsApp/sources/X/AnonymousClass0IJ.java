package X;

/* renamed from: X.0IJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IJ extends AnonymousClass0eL {
    public final /* synthetic */ AnonymousClass0OD A00;

    public AnonymousClass0IJ(AnonymousClass0OD r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        AnonymousClass0OD r1 = this.A00;
        r1.A0S = false;
        ((AnonymousClass04L) r1.A0M).A06();
    }
}
