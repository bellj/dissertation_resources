package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C66003Lx implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(55);
    public final int A00;
    public final int A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C66003Lx(int i, String str, int i2) {
        this.A02 = str;
        this.A01 = i;
        this.A00 = i2;
    }

    public /* synthetic */ C66003Lx(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C66003Lx.class.getName());
        A0h.append("{url='");
        A0h.append(this.A02);
        A0h.append('\'');
        A0h.append(", width=");
        A0h.append(this.A01);
        A0h.append(", height=");
        A0h.append(this.A00);
        return C12970iu.A0v(A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
    }
}
