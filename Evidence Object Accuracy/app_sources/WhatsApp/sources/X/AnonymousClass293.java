package X;

/* renamed from: X.293  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass293 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Double A02;
    public Integer A03;

    public AnonymousClass293() {
        super(1734, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(5, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamMessageMediaDownload {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaSize", this.A02);
        Integer num = this.A03;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsAvatar", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsFirstParty", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
