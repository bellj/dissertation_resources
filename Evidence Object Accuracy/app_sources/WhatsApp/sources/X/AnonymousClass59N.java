package X;

import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackFragment;
import com.whatsapp.status.playback.widget.AudioVolumeView;

/* renamed from: X.59N  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass59N implements AnonymousClass5WI {
    public final /* synthetic */ StatusPlaybackBaseFragment A00;

    @Override // X.AnonymousClass5WI
    public void AMW(boolean z) {
    }

    public AnonymousClass59N(StatusPlaybackBaseFragment statusPlaybackBaseFragment) {
        this.A00 = statusPlaybackBaseFragment;
    }

    @Override // X.AnonymousClass5WI
    public void AMa(int i, int i2, int i3) {
        StatusPlaybackBaseFragment statusPlaybackBaseFragment = this.A00;
        if (((StatusPlaybackFragment) statusPlaybackBaseFragment).A00 && i3 != 0) {
            int i4 = i2 + 1;
            if (i2 == 0) {
                i4 = 0;
            }
            AudioVolumeView audioVolumeView = statusPlaybackBaseFragment.A1F().A0D;
            audioVolumeView.setVolume((((float) i4) * 1.0f) / ((float) (i3 + 1)));
            audioVolumeView.setVisibility(0);
            Runnable runnable = statusPlaybackBaseFragment.A06;
            audioVolumeView.removeCallbacks(runnable);
            audioVolumeView.postDelayed(runnable, 1500);
        }
    }
}
