package X;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentContactOmbudsmanActivity;
import com.whatsapp.payments.ui.BrazilPaymentContactSupportActivity;
import com.whatsapp.payments.ui.BrazilPaymentContactSupportP2pActivity;
import com.whatsapp.payments.ui.BrazilPaymentDPOActivity;
import com.whatsapp.payments.ui.BrazilPaymentIntegrityAppealActivity;
import com.whatsapp.payments.ui.BrazilPaymentReportPaymentActivity;

/* renamed from: X.5dX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119325dX extends ActivityC13790kL {
    public View A00;
    public Button A01;
    public EditText A02;
    public TextView A03;
    public TextView A04;
    public TextView A05;
    public TextView A06;
    public AbstractC118075bE A07;

    public void A2e() {
        int i;
        Spanned fromHtml;
        int i2;
        int i3;
        this.A00 = findViewById(R.id.transaction_header);
        this.A06 = C12970iu.A0M(this, R.id.describe_problem_instructions);
        this.A02 = (EditText) findViewById(R.id.describe_problem_field);
        this.A04 = C12970iu.A0M(this, R.id.describe_problem_field_error);
        this.A01 = (Button) findViewById(R.id.contact_support_send_button);
        this.A05 = C12970iu.A0M(this, R.id.help_center_link);
        this.A03 = C12970iu.A0M(this, R.id.describe_contact_info);
        this.A00.setVisibility(8);
        int i4 = Build.VERSION.SDK_INT;
        Resources resources = getResources();
        boolean z = this instanceof BrazilPaymentReportPaymentActivity;
        if (z) {
            i = R.string.report_payment_desc;
        } else if (this instanceof BrazilPaymentDPOActivity) {
            i = R.string.restore_payments_desc;
        } else if ((this instanceof BrazilPaymentContactSupportP2pActivity) || (this instanceof BrazilPaymentContactSupportActivity)) {
            i = R.string.contact_support_desc;
        } else {
            i = R.string.contact_ombudsman_desc;
        }
        String string = resources.getString(i);
        if (i4 >= 24) {
            fromHtml = Html.fromHtml(string, 63);
        } else {
            fromHtml = Html.fromHtml(string);
        }
        this.A06.setText(fromHtml);
        EditText editText = this.A02;
        if (z) {
            i2 = R.string.report_payment_problem_description_hint;
        } else if (!(this instanceof BrazilPaymentDPOActivity)) {
            i2 = R.string.contact_us_problem_description_hint;
        } else {
            i2 = R.string.restore_payments_problem_description_hint;
        }
        editText.setHint(i2);
        this.A02.addTextChangedListener(new C123795nx(this));
        this.A02.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.64h
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z2) {
                AbstractActivityC119325dX r0 = AbstractActivityC119325dX.this;
                if (z2) {
                    AbstractC118075bE r1 = r0.A07;
                    AbstractC16870pt r2 = r1.A06;
                    Integer A0V = C12960it.A0V();
                    String A05 = r1.A05();
                    AnonymousClass3FW A0S = C117305Zk.A0S();
                    C117325Zm.A07(A0S);
                    r2.AKi(A0S, A0V, 116, A05, null);
                }
            }
        });
        Button button = this.A01;
        if (z || (this instanceof BrazilPaymentDPOActivity)) {
            i3 = R.string.submit;
        } else {
            i3 = R.string.send;
        }
        button.setText(i3);
        C117295Zj.A0n(this.A01, this, 7);
        C117295Zj.A0n(this.A05, this, 8);
    }

    public void A2f() {
        AbstractC118075bE r0;
        if (this instanceof BrazilPaymentReportPaymentActivity) {
            r0 = ((BrazilPaymentReportPaymentActivity) this).A00;
        } else if (this instanceof BrazilPaymentDPOActivity) {
            r0 = ((BrazilPaymentDPOActivity) this).A00;
        } else if (this instanceof BrazilPaymentContactSupportP2pActivity) {
            r0 = ((BrazilPaymentContactSupportP2pActivity) this).A00;
        } else if (!(this instanceof BrazilPaymentContactSupportActivity)) {
            r0 = ((BrazilPaymentContactOmbudsmanActivity) this).A00;
        } else {
            BrazilPaymentContactSupportActivity brazilPaymentContactSupportActivity = (BrazilPaymentContactSupportActivity) this;
            if (!(brazilPaymentContactSupportActivity instanceof BrazilPaymentIntegrityAppealActivity)) {
                r0 = brazilPaymentContactSupportActivity.A00;
            } else {
                r0 = ((BrazilPaymentIntegrityAppealActivity) brazilPaymentContactSupportActivity).A00;
            }
        }
        this.A07 = r0;
        AnonymousClass009.A05(r0.A01.A01());
        C117295Zj.A0r(this, this.A07.A01, 21);
        C117295Zj.A0r(this, this.A07.A08, 22);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        setContentView(R.layout.contact_support);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            if (this instanceof BrazilPaymentReportPaymentActivity) {
                i = R.string.report_payment;
            } else if (this instanceof BrazilPaymentDPOActivity) {
                i = R.string.restore_payments;
            } else if ((this instanceof BrazilPaymentContactSupportP2pActivity) || (this instanceof BrazilPaymentContactSupportActivity)) {
                i = R.string.contact_support;
            } else {
                i = R.string.contact_ombudsman;
            }
            A1U.A0A(i);
        }
        A2f();
        A2e();
        if (getIntent() != null) {
            this.A07.A08(getIntent().getStringExtra("extra_transaction_id"));
        }
        AbstractC118075bE r1 = this.A07;
        AnonymousClass3FW A0S = C117305Zk.A0S();
        C117325Zm.A07(A0S);
        A0S.A00(r1.A05);
        r1.A06.AKi(A0S, C12980iv.A0i(), null, r1.A05(), null);
    }
}
