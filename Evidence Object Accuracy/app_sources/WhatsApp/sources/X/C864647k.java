package X;

/* renamed from: X.47k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864647k extends AbstractC16350or {
    public final int A00 = 15;
    public final int A01;
    public final long A02;
    public final long A03;
    public final C17070qD A04;
    public final Runnable A05;
    public final String A06;

    public C864647k(C17070qD r2, Runnable runnable, String str, int i, long j, long j2) {
        this.A06 = str;
        this.A01 = i;
        this.A02 = j;
        this.A03 = j2;
        this.A04 = r2;
        this.A05 = runnable;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        C17070qD r0 = this.A04;
        r0.A03();
        r0.A08.A0e(this.A06, this.A01, this.A00, this.A02, this.A03);
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        this.A05.run();
    }
}
