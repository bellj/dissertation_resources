package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.util.Log;

/* renamed from: X.3WX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WX implements AbstractC32851cq {
    public final /* synthetic */ SettingsGoogleDrive A00;

    public AnonymousClass3WX(SettingsGoogleDrive settingsGoogleDrive) {
        this.A00 = settingsGoogleDrive;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        Log.i("settings-gdrive/readonly-external-storage-readonly");
        SettingsGoogleDrive settingsGoogleDrive = this.A00;
        boolean A00 = C14950mJ.A00();
        int i = R.string.read_only_media_message_shared_storage;
        if (A00) {
            i = R.string.read_only_media_message;
        }
        settingsGoogleDrive.Adr(new Object[0], R.string.msg_store_backup_skipped, i);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        Log.i("settings-gdrive/readonly-external-storage-readonly-permission");
        SettingsGoogleDrive settingsGoogleDrive = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_backup_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_backup;
        }
        RequestPermissionActivity.A0K(settingsGoogleDrive, R.string.permission_storage_need_write_access_on_backup_request, i2);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        Log.i("settings-gdrive/external-storage-unavailable");
        C36021jC.A01(this.A00, 602);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        Log.i("settings-gdrive/external-storage-unavailable-permission");
        SettingsGoogleDrive settingsGoogleDrive = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_backup_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_backup;
        }
        RequestPermissionActivity.A0K(settingsGoogleDrive, R.string.permission_storage_need_write_access_on_backup_request, i2);
    }
}
