package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.63r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316763r implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(35);
    public final int A00;
    public final int A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316763r(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }

    public static int A00(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("CLAIM")) {
                return 1;
            }
            if (str.equals("ASYNC_NOVI_INITIATED")) {
                return 2;
            }
        }
        return 0;
    }

    public static C1316763r A01(AnonymousClass1V8 r3) {
        if (r3 == null) {
            return null;
        }
        int A00 = A00(r3.A0H("reason"));
        try {
            int parseInt = Integer.parseInt(C117295Zj.A0W(r3, "completed_timestamp_seconds"));
            if (A00 == 0 || parseInt <= 0) {
                return null;
            }
            return new C1316763r(A00, parseInt);
        } catch (NumberFormatException unused) {
            Log.e("PAY: RefundTransaction/parseFromNode unable to fetch claim completed timestamp");
            return null;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2 = this.A01;
        parcel.writeString(i2 != 1 ? i2 != 2 ? null : "ASYNC_NOVI_INITIATED" : "CLAIM");
        parcel.writeInt(this.A00);
    }
}
