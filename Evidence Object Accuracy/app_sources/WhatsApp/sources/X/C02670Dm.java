package X;

import android.view.View;
import android.view.WindowInsets;

/* renamed from: X.0Dm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02670Dm extends C02680Dn {
    public static final C018408o A00 = C018408o.A02(WindowInsets.CONSUMED);

    @Override // X.C02710Dq, X.C06250St
    public final void A0B(View view) {
    }

    public C02670Dm(C018408o r1, WindowInsets windowInsets) {
        super(r1, windowInsets);
    }

    @Override // X.C02710Dq, X.C06250St
    public AnonymousClass0U7 A05(int i) {
        WindowInsets windowInsets = this.A03;
        int i2 = 0;
        int i3 = 1;
        while (true) {
            int statusBars = WindowInsets.Type.statusBars();
            while (true) {
                i2 |= statusBars;
                while (true) {
                    i3 <<= 1;
                    if (i3 <= 256) {
                        if ((7 & i3) != 0) {
                            if (i3 != 1) {
                                if (i3 != 2) {
                                    if (i3 != 4) {
                                        if (i3 != 8) {
                                            if (i3 != 16) {
                                                if (i3 != 32) {
                                                    if (i3 == 64) {
                                                        statusBars = WindowInsets.Type.tappableElement();
                                                        break;
                                                    } else if (i3 == 128) {
                                                        statusBars = WindowInsets.Type.displayCutout();
                                                        break;
                                                    }
                                                } else {
                                                    statusBars = WindowInsets.Type.mandatorySystemGestures();
                                                    break;
                                                }
                                            } else {
                                                statusBars = WindowInsets.Type.systemGestures();
                                                break;
                                            }
                                        } else {
                                            statusBars = WindowInsets.Type.ime();
                                            break;
                                        }
                                    } else {
                                        statusBars = WindowInsets.Type.captionBar();
                                        break;
                                    }
                                } else {
                                    statusBars = WindowInsets.Type.navigationBars();
                                    break;
                                }
                            }
                        }
                    } else {
                        return AnonymousClass0U7.A01(windowInsets.getInsets(i2));
                    }
                }
            }
        }
    }
}
