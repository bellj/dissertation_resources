package X;

import android.graphics.RectF;

/* renamed from: X.4Rx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91484Rx {
    public final float A00;
    public final float A01;
    public final int A02;
    public final RectF A03;

    public C91484Rx(RectF rectF, float f, float f2, int i) {
        RectF rectF2 = new RectF();
        this.A03 = rectF2;
        this.A00 = f;
        rectF2.set(rectF);
        this.A02 = i;
        this.A01 = f2;
    }
}
