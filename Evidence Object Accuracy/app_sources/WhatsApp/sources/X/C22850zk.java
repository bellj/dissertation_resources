package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GenerateTcTokenJob;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22850zk {
    public Set A00;
    public final C20670w8 A01;
    public final C18780t0 A02;
    public final C14830m7 A03;
    public final C14850m9 A04;

    public C22850zk(C20670w8 r1, C18780t0 r2, C14830m7 r3, C14850m9 r4) {
        this.A03 = r3;
        this.A04 = r4;
        this.A01 = r1;
        this.A02 = r2;
    }

    public void A00(UserJid userJid) {
        C14850m9 r1 = this.A04;
        if (r1.A07(995) && A02(userJid)) {
            Long A03 = this.A02.A03(userJid);
            if (A03 != null) {
                long longValue = A03.longValue();
                long A02 = (long) r1.A02(996);
                if (longValue / A02 >= (this.A03.A01() / 1000) / A02) {
                    A01(userJid);
                    return;
                }
            }
            this.A01.A00(new GenerateTcTokenJob(userJid, Long.valueOf(this.A03.A01() / 1000)));
        }
    }

    public void A01(UserJid userJid) {
        Set set;
        synchronized (this) {
            set = this.A00;
            if (set == null) {
                set = Collections.synchronizedSet(new HashSet());
                this.A00 = set;
            }
        }
        synchronized (set) {
            set.remove(userJid);
        }
    }

    public boolean A02(UserJid userJid) {
        Set set;
        synchronized (this) {
            set = this.A00;
            if (set == null) {
                set = Collections.synchronizedSet(new HashSet());
                this.A00 = set;
            }
        }
        synchronized (set) {
            if (set.contains(userJid)) {
                return false;
            }
            set.add(userJid);
            return true;
        }
    }
}
