package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3qC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79193qC extends AbstractC113535Hy<Long> implements AnonymousClass5Z5<Long>, RandomAccess {
    public static final C79193qC A02;
    public int A00;
    public long[] A01;

    public C79193qC(long[] jArr, int i) {
        this.A01 = jArr;
        this.A00 = i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C79193qC)) {
            return super.addAll(collection);
        }
        C79193qC r7 = (C79193qC) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.A01;
            if (i3 > jArr.length) {
                jArr = Arrays.copyOf(jArr, i3);
                this.A01 = jArr;
            }
            System.arraycopy(r7.A01, 0, jArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C79193qC)) {
                return super.equals(obj);
            }
            C79193qC r11 = (C79193qC) obj;
            int i = this.A00;
            if (i == r11.A00) {
                long[] jArr = r11.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == jArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean remove(Object obj) {
        A02();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Long.valueOf(this.A01[i]))) {
                long[] jArr = this.A01;
                System.arraycopy(jArr, i + 1, jArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            long[] jArr = this.A01;
            System.arraycopy(jArr, i2, jArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }

    static {
        C79193qC r0 = new C79193qC(new long[10], 0);
        A02 = r0;
        ((AbstractC113535Hy) r0).A00 = false;
    }

    public final void A03(int i) {
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
    }

    public final void A04(int i, long j) {
        int i2;
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        long[] jArr = this.A01;
        if (i2 < jArr.length) {
            C72463ee.A0T(jArr, i, i2);
        } else {
            long[] jArr2 = new long[((i2 * 3) >> 1) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.A01, i, jArr2, i + 1, this.A00 - i);
            this.A01 = jArr2;
        }
        this.A01[i] = j;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= this.A00) {
            return new C79193qC(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A04(i, C12980iv.A0G(obj));
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        A03(i);
        return Long.valueOf(this.A01[i]);
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + C72453ed.A0B(this.A01[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        A03(i);
        long[] jArr = this.A01;
        long j = jArr[i];
        AbstractC113535Hy.A01(jArr, this.A00, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        long A0G = C12980iv.A0G(obj);
        A02();
        A03(i);
        long[] jArr = this.A01;
        long j = jArr[i];
        jArr[i] = A0G;
        return Long.valueOf(j);
    }
}
