package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0202000_I0;
import com.whatsapp.util.Log;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.15Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15Y {
    public final C28211Md A00 = new C28211Md();
    public final C28211Md A01 = new C28211Md();
    public final AnonymousClass16E A02;
    public final CountDownLatch A03 = new CountDownLatch(1);

    public AnonymousClass15Y(AnonymousClass16E r3) {
        this.A02 = r3;
    }

    public void A00(int i) {
        ExecutorC27271Gr r0;
        AnonymousClass16E r02 = this.A02;
        if (i != 2) {
            r0 = r02.A01;
        } else {
            r0 = r02.A02;
        }
        long j = r0.A05;
        Thread currentThread = Thread.currentThread();
        boolean z = false;
        if (j == currentThread.getId()) {
            z = true;
        }
        AnonymousClass009.A0A("Not running on this SerialExecutor", z);
        try {
            this.A03.await();
        } catch (InterruptedException e) {
            StringBuilder sb = new StringBuilder("wamruntime: unexpected thread interrupt (");
            sb.append(e);
            sb.append(")");
            Log.a(sb.toString());
            currentThread.interrupt();
        }
    }

    public void A01(Object obj, int i, int i2) {
        C28211Md r0;
        if (i2 == 1 || i2 == 0) {
            r0 = this.A00;
        } else {
            r0 = this.A01;
        }
        r0.A00(i, obj);
    }

    public void A02(Object obj, int i, int i2) {
        AnonymousClass16E r6 = this.A02;
        long j = r6.A00.A05;
        Thread currentThread = Thread.currentThread();
        if (j == currentThread.getId()) {
            A01(obj, i, i2);
        } else if (i2 != 2) {
            r6.A01.execute(new RunnableBRunnable0Shape0S0202000_I0(this, obj, i2, i, 1));
        } else {
            ExecutorC27271Gr r5 = r6.A02;
            if (r5.A05 == currentThread.getId()) {
                A00(i2);
                A01(obj, i, i2);
                return;
            }
            r5.execute(new RunnableBRunnable0Shape0S0202000_I0(this, obj, 2, i, 0));
        }
    }
}
