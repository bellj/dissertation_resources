package X;

import com.whatsapp.util.Log;

/* renamed from: X.69G  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69G implements AnonymousClass2S7 {
    public final /* synthetic */ C118025b9 A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass69G(C118025b9 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AnonymousClass2S7
    public void APk() {
        Log.e("PAY: PaymentIncentiveViewModel/syncIncentiveData/on-error");
        if (this.A01) {
            C118025b9 r0 = this.A00;
            r0.A01.A0A(AnonymousClass617.A02(r0.A04.A00(), C12990iw.A0m("Failed syncing incentive")));
        }
    }

    @Override // X.AnonymousClass2S7
    public void AWz(AnonymousClass2S0 r3) {
        if (this.A01) {
            this.A00.A01.A0A(AnonymousClass617.A01(r3));
        }
    }
}
