package X;

/* renamed from: X.0Pi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05380Pi {
    public C08740bm A00;
    public C02510Co A01 = C02510Co.A00();
    public Object A02;
    public boolean A03;

    public void A00() {
        this.A02 = null;
        this.A00 = null;
        this.A01.A07(null);
    }

    public void A01(Object obj) {
        this.A03 = true;
        C08740bm r0 = this.A00;
        if (r0 != null && r0.A01(obj)) {
            this.A02 = null;
            this.A00 = null;
            this.A01 = null;
        }
    }

    public void finalize() {
        C02510Co r1;
        C08740bm r2 = this.A00;
        if (r2 != null && !r2.isDone()) {
            StringBuilder sb = new StringBuilder("The completer object was garbage collected - this future would otherwise never complete. The tag was: ");
            sb.append(this.A02);
            r2.A00(new C10860fK(sb.toString()));
        }
        if (!this.A03 && (r1 = this.A01) != null) {
            r1.A07(null);
        }
    }
}
