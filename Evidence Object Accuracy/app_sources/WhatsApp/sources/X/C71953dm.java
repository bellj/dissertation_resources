package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3dm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71953dm extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71953dm(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogSearchFragment catalogSearchFragment = this.this$0;
        UserJid userJid = catalogSearchFragment.A0P;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        C48882Ih r0 = catalogSearchFragment.A0D;
        if (r0 != null) {
            return AnonymousClass3RX.A00(catalogSearchFragment, r0, userJid);
        }
        throw C16700pc.A06("cartMenuViewModelFactory");
    }
}
