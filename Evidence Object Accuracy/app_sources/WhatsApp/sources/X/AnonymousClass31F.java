package X;

/* renamed from: X.31F  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31F extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public String A08;
    public String A09;

    public AnonymousClass31F() {
        super(2978, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A00);
        r3.Abe(10, this.A01);
        r3.Abe(8, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(7, this.A08);
        r3.Abe(4, this.A09);
        r3.Abe(5, this.A04);
        r3.Abe(3, this.A05);
        r3.Abe(1, this.A06);
        r3.Abe(2, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusMute {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isPosterBiz", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isPosterInAddressBook", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "muteAction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "muteOrigin", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaignId", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaignIds", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaignItemIndex", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemIndex", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusSessionId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusViewerSessionId", this.A07);
        return C12960it.A0d("}", A0k);
    }
}
