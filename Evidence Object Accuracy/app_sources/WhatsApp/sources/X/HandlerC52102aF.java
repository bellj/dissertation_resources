package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import java.io.File;

/* renamed from: X.2aF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC52102aF extends Handler {
    public long A00;
    public final long A01;
    public final C28151Kw A02;
    public final C16170oZ A03;
    public final C14680lr A04;
    public final AnonymousClass5V6 A05;
    public final AnonymousClass5V6 A06;
    public final AnonymousClass5V6 A07;
    public final AnonymousClass5V6 A08;
    public final C28641Ok A09;
    public final AbstractC28661Om A0A;
    public final AbstractC28661Om A0B;

    public HandlerC52102aF(C28151Kw r2, C16170oZ r3, C14680lr r4, AnonymousClass5V6 r5, AnonymousClass5V6 r6, AnonymousClass5V6 r7, AnonymousClass5V6 r8, C28641Ok r9, AbstractC28661Om r10, AbstractC28661Om r11, long j) {
        super(Looper.getMainLooper());
        this.A03 = r3;
        this.A09 = r9;
        this.A04 = r4;
        this.A02 = r2;
        this.A0A = r10;
        this.A0B = r11;
        this.A07 = r5;
        this.A08 = r6;
        this.A06 = r7;
        this.A05 = r8;
        this.A01 = j;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AbstractC14640lm r2;
        File file;
        C14690ls r0;
        File file2;
        AbstractC14670lq r9 = this.A09.A00;
        if (r9.A0P != null) {
            long elapsedRealtime = (SystemClock.elapsedRealtime() - r9.A0B) + r9.A0A;
            this.A0B.A6q(Integer.valueOf((int) C12980iv.A0D(elapsedRealtime)));
            if (r9.A06 % 2 == 0) {
                double longBitsToDouble = Double.longBitsToDouble(this.A02.A00.getAndSet(Double.doubleToRawLongBits(Double.MIN_VALUE)));
                if (longBitsToDouble == -1.0d) {
                    this.A06.A6p();
                } else if (longBitsToDouble != Double.MIN_VALUE) {
                    float f = (float) longBitsToDouble;
                    VoiceVisualizer voiceVisualizer = this.A04.A0C;
                    AnonymousClass009.A0E(voiceVisualizer.A0H.isEmpty());
                    long elapsedRealtime2 = SystemClock.elapsedRealtime();
                    long j = voiceVisualizer.A02;
                    if (j != 0) {
                        voiceVisualizer.A03 = Math.max(elapsedRealtime2 - j, 0L);
                    }
                    voiceVisualizer.A02 = elapsedRealtime2;
                    voiceVisualizer.A0F.add(Float.valueOf(f));
                    if (!voiceVisualizer.A06) {
                        voiceVisualizer.A06 = true;
                        voiceVisualizer.invalidate();
                    }
                }
            }
            this.A05.A6p();
            C12990iw.A17(this);
            if (r9.A0K == null && elapsedRealtime > 1000 && (r0 = r9.A0P) != null && (file2 = r0.A0A) != null && file2.length() > 0) {
                this.A08.A6p();
                this.A0A.A6q(r9.A0P.A0A);
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (this.A00 + 1250 < currentTimeMillis && (r2 = r9.A0J) != null) {
                this.A00 = currentTimeMillis;
                this.A03.A0B(r2, 1);
                C14690ls r02 = r9.A0P;
                if (r02 != null && (file = r02.A0A) != null && file.length() > this.A01) {
                    StringBuilder A0k = C12960it.A0k("voicenote/filelimit ");
                    A0k.append(r9.A0P.A0A.length());
                    C12960it.A1F(A0k);
                    this.A07.A6p();
                }
            }
        }
    }
}
