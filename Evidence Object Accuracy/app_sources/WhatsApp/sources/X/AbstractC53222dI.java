package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.BoundedLinearLayout;
import com.whatsapp.components.MaxHeightLinearLayout;

/* renamed from: X.2dI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53222dI extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53222dI(Context context) {
        super(context);
        A00();
    }

    public AbstractC53222dI(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC53222dI(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public AbstractC53222dI(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A00();
    }

    public void A00() {
        if (this instanceof MaxHeightLinearLayout) {
            MaxHeightLinearLayout maxHeightLinearLayout = (MaxHeightLinearLayout) this;
            if (!maxHeightLinearLayout.A01) {
                maxHeightLinearLayout.A01 = true;
                maxHeightLinearLayout.generatedComponent();
            }
        } else if (this instanceof BoundedLinearLayout) {
            BoundedLinearLayout boundedLinearLayout = (BoundedLinearLayout) this;
            if (!boundedLinearLayout.A02) {
                boundedLinearLayout.A02 = true;
                boundedLinearLayout.generatedComponent();
            }
        } else if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
