package X;

import java.util.Enumeration;

/* renamed from: X.5MY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MY extends AnonymousClass1TM {
    public C114535Mc[] A00;
    public C114535Mc[] A01;

    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: X.5Mc[] */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: X.5Mc[] */
    /* JADX WARN: Multi-variable type inference failed */
    public AnonymousClass5MY(AbstractC114775Na r8) {
        Enumeration A0C = r8.A0C();
        while (A0C.hasMoreElements()) {
            AnonymousClass5NU A01 = AnonymousClass5NU.A01(A0C.nextElement());
            int i = A01.A00;
            if (i == 0) {
                AbstractC114775Na A05 = AbstractC114775Na.A05(A01, false);
                int A0B = A05.A0B();
                C114535Mc[] r3 = new C114535Mc[A0B];
                for (int i2 = 0; i2 != A0B; i2++) {
                    AnonymousClass1TN A0D = A05.A0D(i2);
                    if (A0D == null) {
                        A0D = null;
                    } else if (!(A0D instanceof C114535Mc)) {
                        A0D = new C114535Mc(AbstractC114775Na.A04(A0D));
                    }
                    r3[i2] = A0D;
                }
                this.A01 = r3;
            } else if (i == 1) {
                AbstractC114775Na A052 = AbstractC114775Na.A05(A01, false);
                int A0B2 = A052.A0B();
                C114535Mc[] r32 = new C114535Mc[A0B2];
                for (int i3 = 0; i3 != A0B2; i3++) {
                    AnonymousClass1TN A0D2 = A052.A0D(i3);
                    if (A0D2 == null) {
                        A0D2 = null;
                    } else if (!(A0D2 instanceof C114535Mc)) {
                        A0D2 = new C114535Mc(AbstractC114775Na.A04(A0D2));
                    }
                    r32[i3] = A0D2;
                }
                this.A00 = r32;
            } else {
                throw C12970iu.A0f(C12960it.A0W(i, "Unknown tag encountered: "));
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        C114535Mc[] r1 = this.A01;
        if (r1 != null) {
            C94954co.A03(new AnonymousClass5NZ(r1), A00, false);
        }
        C114535Mc[] r2 = this.A00;
        if (r2 != null) {
            C94954co.A02(new AnonymousClass5NZ(r2), A00, 1, false);
        }
        return new AnonymousClass5NZ(A00);
    }
}
