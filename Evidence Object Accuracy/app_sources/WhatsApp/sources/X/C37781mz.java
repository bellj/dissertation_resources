package X;

import android.net.Uri;
import android.text.TextUtils;

/* renamed from: X.1mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37781mz extends AbstractC37791n0 {
    public String A00;
    public final String A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0006, code lost:
        if (r5 != null) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C37781mz(java.lang.String r2, java.lang.String r3, java.lang.String r4, java.lang.String r5, java.lang.String r6) {
        /*
            r1 = this;
            r1.<init>(r2, r3, r4)
            if (r2 != 0) goto L_0x0008
            r0 = 0
            if (r5 == 0) goto L_0x0009
        L_0x0008:
            r0 = 1
        L_0x0009:
            X.AnonymousClass009.A0F(r0)
            r1.A00 = r5
            r1.A01 = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37781mz.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    @Override // X.AbstractC37701mr
    public String AAJ(C15450nH r4, C28481Nj r5, boolean z) {
        Uri.Builder A01;
        String str = this.A00;
        if (!TextUtils.isEmpty(str)) {
            Uri.Builder buildUpon = Uri.parse(new Uri.Builder().scheme("https").encodedAuthority(r5.A02).encodedPath(str).build().toString()).buildUpon();
            int i = 1;
            if (r5.A00 == 0) {
                i = 0;
            }
            A01 = buildUpon.appendQueryParameter("direct_ip", String.valueOf(i));
            if (z) {
                A01.appendQueryParameter("auth", r5.A01);
            }
            String str2 = this.A03;
            if (!TextUtils.isEmpty(str2)) {
                A01.appendQueryParameter("hash", str2);
            }
        } else {
            A01 = A01(r5);
        }
        String str3 = r5.A03;
        if (str3 != null) {
            AbstractC37791n0.A00(A01, "_nc_cat", str3);
        }
        String str4 = this.A01;
        if (str4 != null) {
            A01.appendQueryParameter("mode", str4);
        }
        return A01.build().toString();
    }
}
