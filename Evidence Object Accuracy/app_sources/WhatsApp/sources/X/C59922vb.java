package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59922vb extends AbstractC37191le {
    public final RecyclerView A00;
    public final C54322gX A01;

    public C59922vb(View view, C54322gX r4) {
        super(view);
        RecyclerView A0R = C12990iw.A0R(view, R.id.search_history_recycler_view);
        this.A00 = A0R;
        A0R.setLayoutManager(C12990iw.A0Q(view));
        this.A01 = r4;
    }
}
