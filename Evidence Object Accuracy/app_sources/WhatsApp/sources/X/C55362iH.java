package X;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.greenalert.GreenAlertActivity;
import java.util.TreeMap;

/* renamed from: X.2iH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55362iH extends AnonymousClass01A {
    public final AbstractC11770gq A00;
    public final AnonymousClass12P A01;
    public final C14900mE A02;
    public final C252818u A03;
    public final AnonymousClass01d A04;
    public final C17170qN A05;
    public final AnonymousClass018 A06;
    public final C252018m A07;
    public final C22650zQ A08;

    @Override // X.AnonymousClass01A
    public int A01() {
        return 2;
    }

    public /* synthetic */ C55362iH(AbstractC11770gq r1, AnonymousClass12P r2, C14900mE r3, C252818u r4, AnonymousClass01d r5, C17170qN r6, AnonymousClass018 r7, C252018m r8, C22650zQ r9) {
        this.A02 = r3;
        this.A08 = r9;
        this.A01 = r2;
        this.A03 = r4;
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A00 = r1;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r12v0, types: [android.view.View, android.view.ViewGroup] */
    /* JADX WARN: Type inference failed for: r0v71 */
    /* JADX WARN: Type inference failed for: r0v79 */
    /* JADX WARN: Type inference failed for: r0v85 */
    /* JADX WARN: Type inference failed for: r0v86 */
    /* JADX WARN: Type inference failed for: r0v91 */
    /* JADX WARN: Type inference failed for: r0v92 */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // X.AnonymousClass01A
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A05(android.view.ViewGroup r12, int r13) {
        /*
        // Method dump skipped, instructions count: 603
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55362iH.A05(android.view.ViewGroup, int):java.lang.Object");
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        viewGroup.removeView((View) obj);
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }

    public final String A0F(Context context, int[] iArr) {
        char c;
        C22650zQ r1 = this.A08;
        if (C93114Zd.A00(r1) || (r1.A05("BR") && iArr == GreenAlertActivity.A0N)) {
            c = 1;
        } else {
            c = 0;
        }
        return context.getString(iArr[c]);
    }

    public final String A0G(Context context, int[] iArr, Object... objArr) {
        char c;
        C22650zQ r1 = this.A08;
        if (C93114Zd.A00(r1) || (r1.A05("BR") && iArr == GreenAlertActivity.A0N)) {
            c = 1;
        } else {
            c = 0;
        }
        return context.getString(iArr[c], objArr);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A0H(java.lang.String[] r4) {
        /*
            r3 = this;
            X.18m r2 = r3.A07
            X.0zQ r1 = r3.A08
            boolean r0 = X.C93114Zd.A00(r1)
            if (r0 == 0) goto L_0x0018
            r0 = 2
        L_0x000b:
            r1 = r4[r0]
            java.lang.String r0 = "security-and-privacy"
            android.net.Uri r0 = r2.A04(r0, r1)
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0018:
            java.lang.String r0 = "BR"
            boolean r0 = r1.A05(r0)
            boolean r0 = X.C12960it.A1S(r0)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55362iH.A0H(java.lang.String[]):java.lang.String");
    }

    public final void A0I(View view, String str, String[] strArr, int i) {
        C12970iu.A0K(view, R.id.green_alert_tos_bullet_image).setImageResource(i);
        A0J(C12970iu.A0T(view, R.id.green_alert_tos_bullet_text), str, strArr);
    }

    public final void A0J(TextEmojiLabel textEmojiLabel, String str, String... strArr) {
        int length = strArr.length;
        Object[] objArr = new Object[length];
        TreeMap treeMap = new TreeMap();
        for (int i = 0; i < length; i++) {
            String valueOf = String.valueOf(i);
            objArr[i] = valueOf;
            treeMap.put(valueOf, Uri.parse(strArr[i]));
        }
        C42971wC.A09(textEmojiLabel.getContext(), this.A01, this.A02, textEmojiLabel, this.A04, String.format(C12970iu.A14(this.A06), str, objArr), treeMap);
    }
}
