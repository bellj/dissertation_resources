package X;

import java.util.Arrays;

/* renamed from: X.5NK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NK extends AnonymousClass1TL implements AnonymousClass5VP {
    public final char[] A00;

    public AnonymousClass5NK(char[] cArr) {
        this.A00 = cArr;
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = this.A00.length << 1;
        return AnonymousClass1TQ.A00(length) + 1 + length;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        return new String(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r14, boolean z) {
        char[] cArr = this.A00;
        int length = cArr.length;
        if (z) {
            r14.A00.write(30);
        }
        r14.A02(length << 1);
        byte[] bArr = new byte[8];
        int i = length & -4;
        int i2 = 0;
        while (i2 < i) {
            char c = cArr[i2];
            char c2 = cArr[i2 + 1];
            char c3 = cArr[i2 + 2];
            char c4 = cArr[i2 + 3];
            i2 += 4;
            bArr[0] = (byte) (c >> '\b');
            bArr[1] = (byte) c;
            bArr[2] = (byte) (c2 >> '\b');
            bArr[3] = (byte) c2;
            bArr[4] = (byte) (c3 >> '\b');
            bArr[5] = (byte) c3;
            bArr[6] = (byte) (c4 >> '\b');
            bArr[7] = (byte) c4;
            r14.A00.write(bArr, 0, 8);
        }
        if (i2 < length) {
            int i3 = 0;
            do {
                char c5 = cArr[i2];
                i2++;
                int i4 = i3 + 1;
                bArr[i3] = (byte) (c5 >> '\b');
                i3 = i4 + 1;
                bArr[i4] = (byte) c5;
            } while (i2 < length);
            r14.A00.write(bArr, 0, i3);
        }
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NK)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NK) r3).A00);
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        char[] cArr = this.A00;
        if (cArr == null) {
            return 0;
        }
        int length = cArr.length;
        int i = length + 1;
        while (true) {
            length--;
            if (length < 0) {
                return i;
            }
            i = (i * 257) ^ cArr[length];
        }
    }

    public String toString() {
        return new String(this.A00);
    }
}
