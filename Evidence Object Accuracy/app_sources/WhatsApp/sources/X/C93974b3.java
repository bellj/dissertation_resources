package X;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.4b3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93974b3 {
    public static final Pattern A02 = Pattern.compile("^ [0-9a-fA-F]{8} ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8})");
    public int A00 = -1;
    public int A01 = -1;

    public void A00(C100624mD r7) {
        String str;
        int i = 0;
        while (true) {
            AnonymousClass5YX[] r1 = r7.A00;
            if (i < r1.length) {
                AnonymousClass5YX r5 = r1[i];
                if (r5 instanceof C77053mf) {
                    C77053mf r52 = (C77053mf) r5;
                    if ("iTunSMPB".equals(r52.A00)) {
                        str = r52.A02;
                    } else {
                        continue;
                        i++;
                    }
                } else {
                    if (r5 instanceof C77063mg) {
                        C77063mg r53 = (C77063mg) r5;
                        if ("com.apple.iTunes".equals(r53.A01) && "iTunSMPB".equals(r53.A00)) {
                            str = r53.A02;
                        }
                    } else {
                        continue;
                    }
                    i++;
                }
                Matcher matcher = A02.matcher(str);
                if (matcher.find()) {
                    try {
                        int parseInt = Integer.parseInt(matcher.group(1), 16);
                        int parseInt2 = Integer.parseInt(matcher.group(2), 16);
                        if (parseInt > 0 || parseInt2 > 0) {
                            this.A00 = parseInt;
                            this.A01 = parseInt2;
                            return;
                        }
                    } catch (NumberFormatException unused) {
                        continue;
                    }
                } else {
                    continue;
                }
                i++;
            } else {
                return;
            }
        }
    }
}
