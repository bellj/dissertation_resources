package X;

import android.media.MediaRecorder;
import android.util.Pair;

/* renamed from: X.668  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass668 implements AbstractC136406Mk {
    public final /* synthetic */ AnonymousClass661 A00;

    @Override // X.AbstractC136406Mk
    public void AWK(MediaRecorder mediaRecorder) {
    }

    public AnonymousClass668(AnonymousClass661 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136406Mk
    public void AST(MediaRecorder mediaRecorder, int i, int i2, boolean z) {
        new Pair(Integer.valueOf(i), Integer.valueOf(i2));
        AnonymousClass616.A00();
    }

    @Override // X.AbstractC136406Mk
    public void AVw(MediaRecorder mediaRecorder) {
        try {
            this.A00.A0B(mediaRecorder);
        } catch (Exception e) {
            AnonymousClass616.A01("Camera1Device.setVideoRecordingSource", e.getMessage());
        }
    }
}
