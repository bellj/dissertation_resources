package X;

import android.content.Context;

/* renamed from: X.1C0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1C0 {
    public AnonymousClass1C3 A00;
    public final Context A01;
    public final AnonymousClass01N A02;

    public AnonymousClass1C0(Context context, AnonymousClass01N r2) {
        this.A01 = context;
        this.A02 = r2;
    }

    public C16310on A00() {
        AnonymousClass1C3 r0;
        synchronized (this) {
            r0 = this.A00;
            if (r0 == null) {
                r0 = (AnonymousClass1C3) this.A02.get();
                this.A00 = r0;
            }
        }
        return r0.get();
    }

    public C16310on A01() {
        AnonymousClass1C3 r0;
        synchronized (this) {
            r0 = this.A00;
            if (r0 == null) {
                r0 = (AnonymousClass1C3) this.A02.get();
                this.A00 = r0;
            }
        }
        return r0.A02();
    }
}
