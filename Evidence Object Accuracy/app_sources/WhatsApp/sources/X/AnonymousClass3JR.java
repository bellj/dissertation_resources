package X;

import android.content.Context;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3JR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3JR {
    public static final AnonymousClass3JR A00 = new AnonymousClass3JR();

    public static final int A00(AnonymousClass28D r3) {
        AnonymousClass28D A05 = AnonymousClass28D.A05(r3);
        if (A05 == null || A05.A01 != 16372) {
            return 1;
        }
        return A05.A0B(35, 1);
    }

    public static final Rect A01(Rect rect, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        boolean z;
        boolean z2;
        boolean z3;
        int i9;
        int i10;
        int i11;
        int i12;
        boolean z4 = false;
        if (i != 1 ? i8 != 0 : i7 != 0) {
            z = false;
        } else {
            z = true;
        }
        if (i != 1 ? i7 != 0 : i8 != 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        int i13 = (i8 + i6) - 1;
        if (i != 1 ? i7 != i4 - 1 : i13 != i5 - 1) {
            z3 = false;
        } else {
            z3 = true;
        }
        if (i != 1 ? i13 == i5 - 1 : i7 == i4 - 1) {
            z4 = true;
        }
        if (z2) {
            i9 = rect.left;
        } else {
            i9 = i2 >> 1;
        }
        if (z) {
            i10 = rect.top;
        } else {
            i10 = i3 >> 1;
        }
        if (z3) {
            i11 = rect.right;
        } else {
            i11 = i2 >> 1;
        }
        if (z4) {
            i12 = rect.bottom;
        } else {
            i12 = i3 >> 1;
        }
        return new Rect(i9, i10, i11, i12);
    }

    public static final Rect A02(AnonymousClass28D r3, boolean z) {
        AnonymousClass28D A0F = r3.A0F(40);
        if (A0F == null) {
            return C12980iv.A0J();
        }
        Rect A01 = AnonymousClass3G6.A01(A0F);
        if (!z) {
            return A01;
        }
        return new Rect(A01.right, A01.top, A01.left, A01.bottom);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01f5, code lost:
        if (r42 == 1) goto L_0x01f7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AbstractC72393eW A03(X.C92304Vj r37, X.AbstractC65073Ia r38, X.AnonymousClass28D r39, int r40, int r41, int r42) {
        /*
        // Method dump skipped, instructions count: 712
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JR.A03(X.4Vj, X.3Ia, X.28D, int, int, int):X.3eW");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:52:0x00bd */
    /* JADX WARN: Type inference failed for: r11v1, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v2 */
    /* JADX WARN: Type inference failed for: r11v3, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.AnonymousClass28D A04(X.AnonymousClass28D r14) {
        /*
            r0 = 94
            X.28D r2 = r14.A0F(r0)
            if (r2 == 0) goto L_0x000f
            int r1 = r2.A01
            r0 = 16373(0x3ff5, float:2.2943E-41)
            if (r1 != r0) goto L_0x000f
            return r2
        L_0x000f:
            java.lang.String r0 = "GridCollectionMeasureHelper: Illegal grid layout config: received "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r0 = 0
            if (r2 == 0) goto L_0x001e
            int r0 = r2.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x001e:
            r1.append(r0)
            java.lang.String r0 = "\n            |, but should be 16373\n            |"
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            java.lang.String r10 = "|"
            r0 = 0
            X.C16700pc.A0E(r1, r0)
            java.lang.String r8 = ""
            boolean r0 = X.AnonymousClass03C.A0J(r10)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x00dc
            X.1WO r0 = X.AnonymousClass03B.A0B(r1)
            java.util.List r2 = X.C11100fk.A02(r0)
            int r14 = r1.length()
            int r1 = r8.length()
            int r0 = r2.size()
            int r0 = r0 * r1
            int r14 = r14 + r0
            if (r1 != 0) goto L_0x00c1
            X.5K1 r9 = new X.5K1
            r9.<init>()
        L_0x0055:
            int r7 = X.C12980iv.A0C(r2)
            java.util.ArrayList r6 = X.C12960it.A0l()
            java.util.Iterator r13 = r2.iterator()
            r1 = 0
        L_0x0062:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x00c7
            java.lang.Object r11 = r13.next()
            int r12 = r1 + 1
            if (r1 >= 0) goto L_0x0078
            java.lang.String r1 = "Index overflow has happened."
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            r0.<init>(r1)
            throw r0
        L_0x0078:
            java.lang.String r11 = (java.lang.String) r11
            if (r1 == 0) goto L_0x007e
            if (r1 != r7) goto L_0x0086
        L_0x007e:
            boolean r0 = X.AnonymousClass03C.A0J(r11)
            if (r0 == 0) goto L_0x0086
        L_0x0084:
            r1 = r12
            goto L_0x0062
        L_0x0086:
            int r5 = r11.length()
            r4 = 0
        L_0x008b:
            r3 = -1
            if (r4 >= r5) goto L_0x00bd
            int r2 = r4 + 1
            char r1 = r11.charAt(r4)
            boolean r0 = java.lang.Character.isWhitespace(r1)
            if (r0 != 0) goto L_0x00bb
            boolean r0 = java.lang.Character.isSpaceChar(r1)
            if (r0 != 0) goto L_0x00bb
            if (r4 == r3) goto L_0x00bd
            boolean r0 = r11.startsWith(r10, r4)
            if (r0 == 0) goto L_0x00bd
            int r0 = r10.length()
            int r4 = r4 + r0
            java.lang.String r0 = r11.substring(r4)
            X.C16700pc.A0B(r0)
            java.lang.Object r11 = r9.AJ4(r0)
            if (r11 != 0) goto L_0x00bd
            goto L_0x0084
        L_0x00bb:
            r4 = r2
            goto L_0x008b
        L_0x00bd:
            r6.add(r11)
            goto L_0x0084
        L_0x00c1:
            X.5KH r9 = new X.5KH
            r9.<init>()
            goto L_0x0055
        L_0x00c7:
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r14)
            java.lang.String r0 = "\n"
            X.AnonymousClass01Y.A09(r1, r0, r8, r8, r6)
            java.lang.String r0 = r1.toString()
            X.C16700pc.A0B(r0)
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x00dc:
            java.lang.String r0 = "marginPrefix must be non-blank string."
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JR.A04(X.28D):X.28D");
    }

    public static final AnonymousClass0F4 A05(Context context, AnonymousClass28D r17, int i) {
        C16700pc.A0E(context, 0);
        AnonymousClass3JR r3 = A00;
        AnonymousClass28D A04 = A04(r17);
        int A0B = A04.A0B(41, -1);
        if (A0B != -1) {
            C17520qw A06 = A06(A04, i);
            int A05 = C12960it.A05(A06.first);
            int A052 = C12960it.A05(A06.second);
            boolean z = true;
            if ((context.getApplicationContext().getApplicationInfo().flags & 4194304) == 0 || C12980iv.A0H(context).getLayoutDirection() != 1) {
                z = false;
            }
            Rect A02 = A02(A04, z);
            if (A05 == 0 && A052 == 0 && A02.left == 0 && A02.top == 0 && A02.right == 0 && A02.bottom == 0) {
                return null;
            }
            List A0K = r17.A0K();
            C16700pc.A0B(A0K);
            List A07 = r3.A07(A0K, A0B);
            ArrayList A0l = C12960it.A0l();
            int size = A07.size();
            int i2 = 0;
            while (i2 < size) {
                int i3 = i2 + 1;
                List A10 = C12980iv.A10(A07, i2);
                int size2 = A10.size();
                for (int i4 = 0; i4 < size2; i4++) {
                    A0l.add(A01(A02, i, A05, A052, A07.size(), A0B, A00(C12980iv.A0U(A10, i4)), i2, i4));
                }
                i2 = i3;
            }
            return new AnonymousClass0F4(A0l);
        }
        throw C12970iu.A0f("GridCollectionMeasureHelper: span-count is required for grid-collection");
    }

    public static final C17520qw A06(AnonymousClass28D r4, int i) {
        float f;
        float f2;
        try {
            String A0I = r4.A0I(38);
            if (A0I == null) {
                f = 0.0f;
            } else {
                f = AnonymousClass3JW.A01(A0I);
            }
            int i2 = (int) f;
            String A0I2 = r4.A0I(36);
            if (A0I2 == null) {
                f2 = 0.0f;
            } else {
                f2 = AnonymousClass3JW.A01(A0I2);
            }
            int i3 = (int) f2;
            if (i == 1) {
                return new C17520qw(Integer.valueOf(i3), Integer.valueOf(i2));
            }
            return new C17520qw(Integer.valueOf(i2), Integer.valueOf(i3));
        } catch (AnonymousClass491 unused) {
            throw C12990iw.A0m("Invalid pixel format for grid vertical spacing");
        }
    }

    public final List A07(List list, int i) {
        int A002;
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        int size = list.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            int i4 = i2 + 1;
            AnonymousClass28D A0U = C12980iv.A0U(list, i2);
            AnonymousClass28D A05 = AnonymousClass28D.A05(A0U);
            if (A05 == null || A05.A01 != 16372 || !A05.A0O(36, false)) {
                A002 = A00(A0U);
            } else {
                A002 = i;
            }
            i3 += A002;
            if (i3 > i) {
                A0l.add(AnonymousClass01Y.A06(A0l2));
                A0l2.clear();
                A0l2.add(A0U);
                i3 = A002;
            } else {
                A0l2.add(A0U);
            }
            if (i2 == C12980iv.A0C(list)) {
                A0l.add(AnonymousClass01Y.A06(A0l2));
            }
            i2 = i4;
        }
        return A0l;
    }
}
