package X;

/* renamed from: X.30m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614730m extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public String A03;
    public String A04;

    public C614730m() {
        super(3246, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A04);
        r3.Abe(8, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamUserActionsOnBusinessProfile {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessProfileDirectorySessionId", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessProfileViewEntryPoint", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessProfileViewEventType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessRootCategory", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isAddedInContacts", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
