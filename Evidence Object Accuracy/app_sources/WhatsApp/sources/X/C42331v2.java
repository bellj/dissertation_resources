package X;

/* renamed from: X.1v2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42331v2 {
    public static final String[] A08 = {"com.google", "com.microsoft.office.outlook.USER_ACCOUNT"};
    public static final String[] A09 = {"com.google.android.apps.tachyon"};
    public final C20840wP A00;
    public final C42221ur A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final C15890o4 A04;
    public final C14820m6 A05;
    public final AnonymousClass018 A06;
    public final C14850m9 A07;

    public C42331v2(C20840wP r1, C42221ur r2, AnonymousClass01d r3, C16590pI r4, C15890o4 r5, C14820m6 r6, AnonymousClass018 r7, C14850m9 r8) {
        this.A07 = r8;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A04 = r5;
        this.A05 = r6;
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.C42361v5 r4, X.C42371v6 r5, X.C15370n3 r6) {
        /*
        // Method dump skipped, instructions count: 228
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42331v2.A00(X.1v5, X.1v6, X.0n3):boolean");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public X.C42381v7 A01(java.lang.String r30, java.util.List r31, java.util.List r32) {
        /*
        // Method dump skipped, instructions count: 1549
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42331v2.A01(java.lang.String, java.util.List, java.util.List):X.1v7");
    }
}
