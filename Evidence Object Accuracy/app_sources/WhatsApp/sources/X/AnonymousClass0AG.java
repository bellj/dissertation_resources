package X;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

/* renamed from: X.0AG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AG extends ConnectivityManager.NetworkCallback {
    public final /* synthetic */ C03160Gk A00;

    public AnonymousClass0AG(C03160Gk r1) {
        this.A00 = r1;
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
        C06390Tk.A00().A02(C03160Gk.A03, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
        C03160Gk r1 = this.A00;
        r1.A04(r1.A00());
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onLost(Network network) {
        C06390Tk.A00().A02(C03160Gk.A03, "Network connection lost", new Throwable[0]);
        C03160Gk r1 = this.A00;
        r1.A04(r1.A00());
    }
}
