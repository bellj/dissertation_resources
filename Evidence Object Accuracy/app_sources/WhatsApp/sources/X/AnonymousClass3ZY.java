package X;

import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.community.ManageGroupsInCommunityActivity;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3ZY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZY implements AbstractC116765Ws {
    public final /* synthetic */ ManageGroupsInCommunityActivity A00;
    public final /* synthetic */ AnonymousClass1OU A01;

    public AnonymousClass3ZY(ManageGroupsInCommunityActivity manageGroupsInCommunityActivity, AnonymousClass1OU r2) {
        this.A00 = manageGroupsInCommunityActivity;
        this.A01 = r2;
    }

    @Override // X.AbstractC116765Ws
    public void APl(int i) {
        Log.e(C12960it.A0W(i, "ManageGroupsInCommunityActivityUnlinkSubgroupsProtocolHelper/error = "));
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A00;
        manageGroupsInCommunityActivity.AaN();
        manageGroupsInCommunityActivity.A2K(
        /*  JADX ERROR: Method code generation error
            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0010: INVOKE  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.52i : 0x000f: CONSTRUCTOR  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.1OU : 0x000e: IGET  (r0v2 X.1OU A[REMOVE]) = (r2v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR)
              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_title int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_text int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.group_community_management_try_again_label int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.cancel int)
             type: VIRTUAL call: X.0kN.A2K(X.2GV, int, int, int, int):void in method: X.3ZY.APl(int):void, file: classes2.dex
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
            	at jadx.core.dex.regions.Region.generate(Region.java:35)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
            	at java.util.ArrayList.forEach(ArrayList.java:1259)
            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000f: CONSTRUCTOR  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.1OU : 0x000e: IGET  (r0v2 X.1OU A[REMOVE]) = (r2v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR in method: X.3ZY.APl(int):void, file: classes2.dex
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
            	... 15 more
            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.52i, state: NOT_LOADED
            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
            	... 21 more
            */
        /*
            this = this;
            java.lang.String r0 = "ManageGroupsInCommunityActivityUnlinkSubgroupsProtocolHelper/error = "
            java.lang.String r0 = X.C12960it.A0W(r3, r0)
            com.whatsapp.util.Log.e(r0)
            com.whatsapp.community.ManageGroupsInCommunityActivity r1 = r2.A00
            r1.AaN()
            X.1OU r0 = r2.A01
            com.whatsapp.community.ManageGroupsInCommunityActivity.A02(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3ZY.APl(int):void");
    }

    @Override // X.AbstractC116765Ws
    public void AXX() {
        Log.e("ManageGroupsInCommunityActivityUnlinkSubgroupsProtocolHelper/timeout");
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A00;
        manageGroupsInCommunityActivity.AaN();
        manageGroupsInCommunityActivity.A2K(
        /*  JADX ERROR: Method code generation error
            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000c: INVOKE  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.52i : 0x000f: CONSTRUCTOR  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.1OU : 0x000a: IGET  (r0v1 X.1OU A[REMOVE]) = (r2v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR)
              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_title int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_text int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.group_community_management_try_again_label int)
              (wrap: int : ?: SGET   com.whatsapp.R.string.cancel int)
             type: VIRTUAL call: X.0kN.A2K(X.2GV, int, int, int, int):void in method: X.3ZY.AXX():void, file: classes2.dex
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
            	at jadx.core.dex.regions.Region.generate(Region.java:35)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
            	at java.util.ArrayList.forEach(ArrayList.java:1259)
            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000f: CONSTRUCTOR  
              (r1v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
              (wrap: X.1OU : 0x000a: IGET  (r0v1 X.1OU A[REMOVE]) = (r2v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR in method: X.3ZY.AXX():void, file: classes2.dex
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
            	... 15 more
            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.52i, state: NOT_LOADED
            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
            	... 21 more
            */
        /*
            this = this;
            java.lang.String r0 = "ManageGroupsInCommunityActivityUnlinkSubgroupsProtocolHelper/timeout"
            com.whatsapp.util.Log.e(r0)
            com.whatsapp.community.ManageGroupsInCommunityActivity r1 = r2.A00
            r1.AaN()
            X.1OU r0 = r2.A01
            com.whatsapp.community.ManageGroupsInCommunityActivity.A02(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3ZY.AXX():void");
    }

    @Override // X.AbstractC116765Ws
    public void AY0(Set set) {
        ManageGroupsInCommunityActivity manageGroupsInCommunityActivity = this.A00;
        manageGroupsInCommunityActivity.AaN();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            int A05 = C12960it.A05(((Pair) it.next()).second);
            if (A05 != -1) {
                int i = R.string.unlink_error_group_already_removed_from_community;
                if (A05 != 400) {
                    if (A05 != 404) {
                        manageGroupsInCommunityActivity.A2K(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002b: INVOKE  
                              (r5v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
                              (wrap: X.52i : 0x000f: CONSTRUCTOR  
                              (r5v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
                              (wrap: X.1OU : 0x0029: IGET  (r0v9 X.1OU A[REMOVE]) = (r7v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
                             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR)
                              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_title int)
                              (wrap: int : ?: SGET   com.whatsapp.R.string.unlink_error_text int)
                              (wrap: int : ?: SGET   com.whatsapp.R.string.group_community_management_try_again_label int)
                              (wrap: int : ?: SGET   com.whatsapp.R.string.cancel int)
                             type: VIRTUAL call: X.0kN.A2K(X.2GV, int, int, int, int):void in method: X.3ZY.AY0(java.util.Set):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:240)
                            	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000f: CONSTRUCTOR  
                              (r5v0 'manageGroupsInCommunityActivity' com.whatsapp.community.ManageGroupsInCommunityActivity)
                              (wrap: X.1OU : 0x0029: IGET  (r0v9 X.1OU A[REMOVE]) = (r7v0 'this' X.3ZY A[IMMUTABLE_TYPE, THIS]) X.3ZY.A01 X.1OU)
                             call: X.52i.<init>(com.whatsapp.community.ManageGroupsInCommunityActivity, X.1OU):void type: CONSTRUCTOR in method: X.3ZY.AY0(java.util.Set):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 39 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.52i, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 45 more
                            */
                        /*
                            this = this;
                            com.whatsapp.community.ManageGroupsInCommunityActivity r5 = r7.A00
                            r5.AaN()
                            java.util.Iterator r6 = r8.iterator()
                        L_0x0009:
                            boolean r0 = r6.hasNext()
                            if (r0 == 0) goto L_0x0046
                            java.lang.Object r0 = r6.next()
                            android.util.Pair r0 = (android.util.Pair) r0
                            java.lang.Object r0 = r0.second
                            int r2 = X.C12960it.A05(r0)
                            r0 = -1
                            if (r2 == r0) goto L_0x0035
                            r1 = 400(0x190, float:5.6E-43)
                            r0 = 2131892453(0x7f1218e5, float:1.9419655E38)
                            if (r2 == r1) goto L_0x0032
                            r0 = 404(0x194, float:5.66E-43)
                            if (r2 == r0) goto L_0x002f
                            X.1OU r0 = r7.A01
                            com.whatsapp.community.ManageGroupsInCommunityActivity.A02(r5, r0)
                            goto L_0x0009
                        L_0x002f:
                            r0 = 2131892454(0x7f1218e6, float:1.9419657E38)
                        L_0x0032:
                            r5.Ado(r0)
                        L_0x0035:
                            X.1lv r4 = r5.A06
                            X.1OU r3 = r7.A01
                            X.1Gr r2 = r4.A0U
                            r1 = 10
                            com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2 r0 = new com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2
                            r0.<init>(r4, r1, r3)
                            r2.execute(r0)
                            goto L_0x0009
                        L_0x0046:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3ZY.AY0(java.util.Set):void");
                    }
                }
