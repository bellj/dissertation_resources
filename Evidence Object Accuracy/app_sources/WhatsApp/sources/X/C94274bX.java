package X;

import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.Matrix;
import java.nio.Buffer;
import java.nio.FloatBuffer;

/* renamed from: X.4bX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94274bX {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = -12345;
    public int A04;
    public int A05;
    public FloatBuffer A06;
    public float[] A07 = new float[16];
    public float[] A08 = new float[16];
    public final float[] A09;

    public C94274bX() {
        float[] fArr = {-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f};
        this.A09 = fArr;
        FloatBuffer asFloatBuffer = C72453ed.A0x(80).asFloatBuffer();
        this.A06 = asFloatBuffer;
        asFloatBuffer.put(fArr);
        asFloatBuffer.position(0);
        Matrix.setIdentityM(this.A08, 0);
    }

    public static void A00(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            throw C12990iw.A0m(C12960it.A0e(": glError ", C12960it.A0j(str), glGetError));
        }
    }

    public final int A01(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        A00(C12960it.A0W(i, "glCreateShader type="));
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        int[] iArr = new int[1];
        GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
        if (iArr[0] != 0) {
            return glCreateShader;
        }
        GLES20.glDeleteShader(glCreateShader);
        return 0;
    }

    public void A02(SurfaceTexture surfaceTexture) {
        A00("onDrawFrame start");
        float[] fArr = this.A08;
        surfaceTexture.getTransformMatrix(fArr);
        GLES20.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glClear(16640);
        GLES20.glUseProgram(this.A02);
        A00("glUseProgram");
        GLES20.glActiveTexture(33984);
        GLES20.glBindTexture(36197, this.A03);
        FloatBuffer floatBuffer = this.A06;
        floatBuffer.position(0);
        GLES20.glVertexAttribPointer(this.A00, 3, 5126, false, 20, (Buffer) floatBuffer);
        A00("glVertexAttribPointer maPosition");
        GLES20.glEnableVertexAttribArray(this.A00);
        A00("glEnableVertexAttribArray aPositionHandle");
        floatBuffer.position(3);
        GLES20.glVertexAttribPointer(this.A01, 2, 5126, false, 20, (Buffer) floatBuffer);
        A00("glVertexAttribPointer aTextureHandle");
        GLES20.glEnableVertexAttribArray(this.A01);
        A00("glEnableVertexAttribArray aTextureHandle");
        float[] fArr2 = this.A07;
        Matrix.setIdentityM(fArr2, 0);
        GLES20.glUniformMatrix4fv(this.A04, 1, false, fArr2, 0);
        GLES20.glUniformMatrix4fv(this.A05, 1, false, fArr, 0);
        GLES20.glDrawArrays(5, 0, 4);
        A00("glDrawArrays");
        GLES20.glFinish();
    }
}
