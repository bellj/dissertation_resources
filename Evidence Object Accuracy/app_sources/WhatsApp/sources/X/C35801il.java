package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1il  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35801il {
    public final long A00;
    public final AbstractC14640lm A01;
    public final UserJid A02;
    public final AnonymousClass1IS A03;

    public C35801il(AbstractC14640lm r1, UserJid userJid, AnonymousClass1IS r3, long j) {
        this.A01 = r1;
        this.A02 = userJid;
        this.A00 = j;
        this.A03 = r3;
    }

    public C35801il(Cursor cursor, AbstractC14640lm r5, UserJid userJid) {
        this.A01 = r5;
        this.A02 = userJid;
        this.A00 = cursor.getLong(3);
        this.A03 = new AnonymousClass1IS(r5, cursor.getString(4), cursor.getInt(1) != 1 ? false : true);
    }
}
