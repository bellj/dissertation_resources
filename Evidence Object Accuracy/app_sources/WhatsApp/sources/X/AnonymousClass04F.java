package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.04F  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04F {
    public static final long A07 = TimeUnit.MINUTES.toNanos(1);
    public double A00;
    public double A01;
    public long A02;
    public boolean A03 = false;
    public final double A04;
    public final AnonymousClass04G A05;
    public final AnonymousClass04H A06;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (r3 > 80.0d) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass04F(android.content.Context r8, X.AnonymousClass04H r9, X.C003301l r10) {
        /*
            r7 = this;
            r7.<init>()
            r0 = 0
            r7.A03 = r0
            X.AnonymousClass009.A05(r9)
            r7.A06 = r9
            X.0Mp r2 = new X.0Mp
            r2.<init>(r7)
            android.view.Choreographer r1 = android.view.Choreographer.getInstance()
            X.04G r0 = new X.04G
            r0.<init>(r1, r2)
            r7.A05 = r0
            long r3 = r10.A00
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0045
            android.view.WindowManager r0 = X.AnonymousClass01d.A02(r8)
            X.AnonymousClass009.A05(r0)
            android.view.Display r0 = r0.getDefaultDisplay()
            float r0 = r0.getRefreshRate()
            double r3 = (double) r0
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0053
            r3 = 4633641066610819072(0x404e000000000000, double:60.0)
        L_0x003b:
            long r5 = X.C003301l.A01
            double r0 = (double) r5
            double r0 = r0 / r3
            long r3 = java.lang.Math.round(r0)
            r10.A00 = r3
        L_0x0045:
            double r0 = (double) r3
            r7.A04 = r0
            r0 = 0
            r7.A01 = r0
            r7.A00 = r0
            r0 = 0
            r7.A02 = r0
            return
        L_0x0053:
            r1 = 4629137466983448576(0x403e000000000000, double:30.0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x005f
            r1 = 4635329916471083008(0x4054000000000000, double:80.0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x003b
        L_0x005f:
            r3 = r1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04F.<init>(android.content.Context, X.04H, X.01l):void");
    }
}
