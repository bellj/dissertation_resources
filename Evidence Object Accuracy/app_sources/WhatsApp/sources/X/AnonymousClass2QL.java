package X;

import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.HomePagerSlidingTabStrip;

/* renamed from: X.2QL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QL implements LayoutTransition.TransitionListener {
    public final /* synthetic */ HomePagerSlidingTabStrip A00;

    @Override // android.animation.LayoutTransition.TransitionListener
    public void startTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {
    }

    public AnonymousClass2QL(HomePagerSlidingTabStrip homePagerSlidingTabStrip) {
        this.A00 = homePagerSlidingTabStrip;
    }

    @Override // android.animation.LayoutTransition.TransitionListener
    public void endTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {
        this.A00.invalidate();
    }
}
