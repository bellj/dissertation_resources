package X;

import java.util.List;

/* renamed from: X.5ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119605ei extends AbstractC17440qo {
    public final C125935s3 A00;

    public C119605ei(C125935s3 r4) {
        super("bk.action.novi.data.CreateGraphQLInput");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r7, C1093651k r8, C14240l5 r9) {
        String str;
        Long l;
        String str2 = null;
        if (!r8.A00.equals("bk.action.novi.data.CreateGraphQLInput")) {
            return null;
        }
        List list = r7.A00;
        if (list.get(0) != null) {
            str = C12960it.A0g(list, 0);
        } else {
            str = null;
        }
        if (C117315Zl.A0I(list) != null) {
            l = (Long) C117315Zl.A0I(list);
        } else {
            l = null;
        }
        if (list.get(2) != null) {
            str2 = C12960it.A0g(list, 2);
        }
        return this.A00.A00.A01(new C127595uk(l, str, str2, C12960it.A0g(list, 3)));
    }
}
