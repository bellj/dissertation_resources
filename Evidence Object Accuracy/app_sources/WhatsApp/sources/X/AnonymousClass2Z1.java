package X;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioDeviceInfo;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.2Z1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Z1 extends BroadcastReceiver {
    public final Object A00 = C12970iu.A0l();
    public volatile boolean A01 = false;
    public final /* synthetic */ AnonymousClass2Nu A02;

    public static final String A00(int i) {
        return i != -1 ? i != 0 ? i != 1 ? i != 2 ? "UNKNOWN BLUETOOTH SCO STATE" : "CONNECTING" : "CONNECTED" : "DISCONNECTED" : "ERROR";
    }

    public AnonymousClass2Z1(AnonymousClass2Nu r2) {
        this.A02 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A01) {
            synchronized (this.A00) {
                if (!this.A01) {
                    AnonymousClass22D.A00(context);
                    this.A01 = true;
                }
            }
        }
        AnonymousClass009.A01();
        if ("android.media.ACTION_SCO_AUDIO_STATE_UPDATED".equals(intent.getAction())) {
            AnonymousClass2Nu r3 = this.A02;
            int i = r3.A01;
            int intExtra = intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", -1);
            r3.A01 = intExtra;
            StringBuilder A0k = C12960it.A0k("voip/audio_route/bluetoothScoReceiver/ACTION_SCO_AUDIO_STATE_UPDATED [");
            A0k.append(A00(i));
            A0k.append(" -> ");
            A0k.append(A00(intExtra));
            Log.i(C12960it.A0d("]", A0k));
            CallInfo callInfo = Voip.getCallInfo();
            int i2 = r3.A01;
            if (i2 == 0) {
                if (i == 2 || i == 1) {
                    r3.A09(callInfo, false);
                    r3.A07(callInfo, null);
                }
            } else if (i2 == 1) {
                if (C28391Mz.A05()) {
                    for (AudioDeviceInfo audioDeviceInfo : C236912q.A02(r3.A0C.A0G())) {
                        StringBuilder A0j = C12960it.A0j("voip/audio_route/bluetoothScoReceiver device name: ");
                        A0j.append((Object) audioDeviceInfo.getProductName());
                        A0j.append(", type: ");
                        A0j.append(audioDeviceInfo.getType());
                        A0j.append(", address: ");
                        Log.i(C12960it.A0d(audioDeviceInfo.getAddress(), A0j));
                    }
                } else {
                    BluetoothHeadset bluetoothHeadset = r3.A0A.A02;
                    if (bluetoothHeadset != null) {
                        for (BluetoothDevice bluetoothDevice : bluetoothHeadset.getConnectedDevices()) {
                            if (bluetoothHeadset.isAudioConnected(bluetoothDevice)) {
                                BluetoothClass bluetoothClass = bluetoothDevice.getBluetoothClass();
                                StringBuilder A0j2 = C12960it.A0j("voip/audio_route/bluetoothScoReceiver device name: ");
                                A0j2.append(bluetoothDevice.getName());
                                A0j2.append(", device class:");
                                A0j2.append(bluetoothClass.getDeviceClass());
                                A0j2.append(", major class: ");
                                A0j2.append(bluetoothClass.getMajorDeviceClass());
                                A0j2.append(", supports AUDIO: ");
                                A0j2.append(bluetoothClass.hasService(2097152));
                                A0j2.append(", supports TELEPHONY: ");
                                A0j2.append(bluetoothClass.hasService(4194304));
                                A0j2.append(", address: ");
                                Log.i(C12960it.A0d(bluetoothDevice.getAddress(), A0j2));
                            }
                        }
                    }
                }
            }
            r3.A08(callInfo, null);
        }
    }
}
