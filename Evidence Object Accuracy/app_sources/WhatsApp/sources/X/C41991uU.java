package X;

/* renamed from: X.1uU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41991uU extends AbstractC16110oT {
    public Integer A00;

    public C41991uU() {
        super(3028, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamAdvIdentitySignatureInvalid {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "advSignatureType", obj);
        sb.append("}");
        return sb.toString();
    }
}
