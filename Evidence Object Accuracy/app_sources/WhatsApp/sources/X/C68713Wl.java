package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3Wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68713Wl implements AbstractC35861it {
    public final /* synthetic */ C64263Ew A00;

    public C68713Wl(C64263Ew r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35861it
    public void AY9(UserJid userJid, Set set, Set set2) {
        Iterator it = set2.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            AnonymousClass3CL r2 = this.A00.A04;
            Log.i(C12960it.A0b("VoiceService/notifyDeviceRemoved ", deviceJid));
            boolean z = false;
            if (deviceJid.device == 0) {
                z = true;
            }
            AnonymousClass009.A0A("primary device should never be removed", !z);
            C29631Ua.A03(r2.A00, deviceJid, true);
        }
    }
}
