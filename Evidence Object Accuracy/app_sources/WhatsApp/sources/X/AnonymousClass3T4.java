package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.3T4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3T4 implements AbstractC116355Vc {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C64163Em A01;

    public AnonymousClass3T4(Bundle bundle, C64163Em r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AbstractC116355Vc
    public final int AgL() {
        return 1;
    }

    @Override // X.AbstractC116355Vc
    public final void AgQ(AbstractC115105Qf r7) {
        AbstractC115105Qf r5 = this.A01.A01;
        Bundle bundle = this.A00;
        AnonymousClass3T2 r52 = (AnonymousClass3T2) r5;
        try {
            Bundle A0D = C12970iu.A0D();
            C65153Ii.A01(bundle, A0D);
            C65873Li r2 = (C65873Li) r52.A02;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, A0D);
            r2.A03(2, A01);
            C65153Ii.A01(A0D, bundle);
            r52.A00 = (View) BinderC56502l7.A00(C65873Li.A00(r2.A01(), r2, 8));
            ViewGroup viewGroup = r52.A01;
            viewGroup.removeAllViews();
            viewGroup.addView(r52.A00);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
