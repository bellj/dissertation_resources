package X;

import android.graphics.Matrix;
import android.view.View;
import org.json.JSONObject;

/* renamed from: X.0ix  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13000ix {
    public static int A00(View view) {
        return view.getHeight() >> 1;
    }

    public static Matrix A01() {
        return new Matrix();
    }

    public static AnonymousClass02A A02(AbstractC001400p r1) {
        return new AnonymousClass02A(r1);
    }

    public static C27691It A03() {
        return new C27691It();
    }

    public static String A04(AnonymousClass1V8 r1) {
        if (r1 != null) {
            return r1.A0G();
        }
        return null;
    }

    public static JSONObject A05(String str) {
        return new JSONObject(str);
    }

    public static void A06(C28531Ny r1, int i) {
        r1.A06 = Integer.valueOf(i);
    }

    public static int[] A07() {
        return new int[2];
    }

    public static String[] A08() {
        return new String[1];
    }
}
