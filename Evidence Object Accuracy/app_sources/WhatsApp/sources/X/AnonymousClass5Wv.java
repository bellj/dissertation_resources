package X;

import java.security.SecureRandom;

/* renamed from: X.5Wv  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Wv {
    int A5m(byte[] bArr, int i);

    void AIb(SecureRandom secureRandom);

    int AYq(byte[] bArr);
}
