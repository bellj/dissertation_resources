package X;

import android.os.Handler;
import android.widget.AbsListView;
import android.widget.PopupWindow;

/* renamed from: X.0Wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07090Wq implements AbsListView.OnScrollListener {
    public final /* synthetic */ AnonymousClass0XR A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public C07090Wq(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1) {
            AnonymousClass0XR r3 = this.A00;
            PopupWindow popupWindow = r3.A0D;
            if (popupWindow.getInputMethodMode() != 2 && popupWindow.getContentView() != null) {
                Handler handler = r3.A0L;
                RunnableC09080cK r0 = r3.A0P;
                handler.removeCallbacks(r0);
                r0.run();
            }
        }
    }
}
