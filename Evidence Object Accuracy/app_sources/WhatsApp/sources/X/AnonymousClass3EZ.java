package X;

import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3EZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EZ {
    public C63633Ci A00;
    public AnonymousClass4KE A01 = new AnonymousClass4KE(this);
    public final C15570nT A02;
    public final ActivityC13790kL A03;
    public final AnonymousClass3F3 A04;
    public final AnonymousClass3FT A05;

    public AnonymousClass3EZ(AnonymousClass2II r3, AnonymousClass2IJ r4, C15570nT r5, ActivityC13790kL r6, C15580nU r7, int i) {
        C63633Ci r1 = new C63633Ci(this);
        this.A00 = r1;
        this.A03 = r6;
        this.A02 = r5;
        this.A05 = r4.A00(r6, r1, r7);
        this.A04 = new AnonymousClass3F3(C12970iu.A0b(r3.A00.A03), i);
    }

    public void A00(UserJid userJid) {
        if (!this.A05.A02()) {
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("dialog_id", 3);
            ActivityC13790kL r3 = this.A03;
            C15570nT r2 = this.A02;
            boolean A0F = r2.A0F(userJid);
            int i = R.string.demote_cadmin_title;
            if (A0F) {
                i = R.string.demote_self_cadmin_title;
            }
            A0D.putString("title", r3.getString(i));
            boolean A0F2 = r2.A0F(userJid);
            int i2 = R.string.demote_cadmin_description;
            if (A0F2) {
                i2 = R.string.demote_self_cadmin_description;
            }
            A0D.putCharSequence("message", r3.getString(i2));
            A0D.putString("user_jid", userJid.getRawString());
            AnonymousClass4KE r22 = this.A01;
            A0D.putString("positive_button", r3.getString(R.string.ok));
            A0D.putString("negative_button", r3.getString(R.string.cancel));
            ActivityC13810kN.A0y(A0D, r3, r22);
        }
    }

    public void A01(UserJid userJid) {
        if (!this.A05.A02()) {
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("dialog_id", 1);
            ActivityC13790kL r3 = this.A03;
            A0D.putString("title", r3.getString(R.string.make_community_admin_title));
            A0D.putCharSequence("message", r3.getString(R.string.make_community_admin_details));
            A0D.putString("user_jid", userJid.getRawString());
            AnonymousClass4KE r2 = this.A01;
            A0D.putString("positive_button", r3.getString(R.string.ok));
            A0D.putString("negative_button", r3.getString(R.string.cancel));
            ActivityC13810kN.A0y(A0D, r3, r2);
        }
    }
}
