package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120875gt extends C120895gv {
    public final /* synthetic */ AnonymousClass6MQ A00;
    public final /* synthetic */ C120495gH A01;
    public final /* synthetic */ C130485zU A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120875gt(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, AnonymousClass6MQ r11, C120495gH r12, C130485zU r13) {
        super(context, r8, r9, r10, "upi-get-vpa");
        this.A01 = r12;
        this.A02 = r13;
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r1) {
        super.A02(r1);
        A05(r1);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r1) {
        super.A03(r1);
        A05(r1);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r8) {
        try {
            AnonymousClass60G r6 = new AnonymousClass60G(r8, this.A02);
            C119705ey r4 = new C119705ey();
            ((AnonymousClass1ZO) r4).A05 = r6.A00;
            r4.A02 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r6.A07, "upiHandle");
            r4.A03 = r6.A08;
            r4.A01 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r6.A06, "accountHolderName");
            boolean z = true;
            boolean A1Z = C12970iu.A1Z(r6.A04, "1");
            r4.A04 = A1Z;
            r4.A05 = C12970iu.A1Z(r6.A03, "1");
            if (r6.A05 != "1") {
                z = false;
            }
            r4.A06 = z;
            if (A1Z) {
                r4.A02 = null;
                r4.A03 = null;
            }
            C120495gH r3 = this.A01;
            C38051nR A00 = r3.A09.A00();
            C12960it.A1E(new C61322zq(A00, r4), A00.A03);
            r3.A0B.A06("in_upi_get_vpa_tag", 2);
            AnonymousClass6MQ r0 = this.A00;
            if (r0 != null) {
                r0.AOb(r4);
            }
        } catch (AnonymousClass1V9 unused) {
            Log.w("PAY: IndiaUpiContactActions : invalid node");
            A05(C117305Zk.A0L());
        }
    }

    public final void A05(C452120p r3) {
        this.A01.A0B.A05(r3, "in_upi_get_vpa_tag");
        AnonymousClass6MQ r0 = this.A00;
        if (r0 != null) {
            r0.APo(r3);
        }
    }
}
