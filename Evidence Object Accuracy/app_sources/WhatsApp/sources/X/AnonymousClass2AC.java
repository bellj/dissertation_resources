package X;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.MessageDialogFragment;

/* renamed from: X.2AC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AC {
    public int A00 = 0;
    public int A01 = 0;
    public int A02;
    public int A03 = 0;
    public int A04 = 0;
    public int A05 = 0;
    public DialogInterface.OnClickListener A06;
    public DialogInterface.OnClickListener A07;
    public String A08;
    public String A09;
    public Object[] A0A;
    public Object[] A0B;

    public /* synthetic */ AnonymousClass2AC() {
    }

    @Deprecated
    public AnonymousClass2AC(String str) {
        this.A08 = str;
    }

    @Deprecated
    public AnonymousClass2AC(Object[] objArr, int i) {
        this.A01 = i;
        this.A0A = objArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if ((r2 instanceof java.lang.Byte) != false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A00(android.os.Bundle r7, java.lang.String r8, java.lang.String r9, java.lang.Object[] r10) {
        /*
            if (r10 == 0) goto L_0x003c
            int r6 = r10.length
            if (r6 == 0) goto L_0x003c
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r6)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>(r6)
            r3 = 0
        L_0x0010:
            r2 = r10[r3]
            boolean r0 = r2 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0023
            boolean r0 = r2 instanceof java.lang.Long
            if (r0 != 0) goto L_0x0023
            boolean r0 = r2 instanceof java.lang.Short
            if (r0 != 0) goto L_0x0023
            boolean r1 = r2 instanceof java.lang.Byte
            r0 = 2
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.add(r0)
            java.lang.String r0 = r2.toString()
            r5.add(r0)
            int r3 = r3 + 1
            if (r3 < r6) goto L_0x0010
            r7.putStringArrayList(r8, r5)
            r7.putIntegerArrayList(r9, r4)
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2AC.A00(android.os.Bundle, java.lang.String, java.lang.String, java.lang.Object[]):void");
    }

    public DialogFragment A01() {
        MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
        Bundle bundle = new Bundle();
        int i = this.A00;
        if (i != 0) {
            bundle.putInt("id", i);
        }
        int i2 = this.A05;
        if (i2 != 0) {
            bundle.putInt("title_id", i2);
            A00(bundle, "title_params_values", "title_params_types", this.A0B);
        }
        int i3 = this.A01;
        if (i3 != 0) {
            bundle.putInt("message_id", i3);
            A00(bundle, "message_params_values", "message_params_types", this.A0A);
        }
        String str = this.A09;
        if (str != null) {
            bundle.putString("title", str);
        }
        String str2 = this.A08;
        if (str2 != null) {
            bundle.putString("message", str2);
        }
        int i4 = this.A02;
        if (i4 != 0) {
            bundle.putInt("message_view_id", i4);
        }
        int i5 = this.A03;
        if (!(i5 == 0 || this.A06 == null)) {
            bundle.putInt("primary_action_text_id", i5);
            messageDialogFragment.A00 = this.A06;
        }
        int i6 = this.A04;
        if (!(i6 == 0 || this.A07 == null)) {
            bundle.putInt("secondary_action_text_id", i6);
            messageDialogFragment.A01 = this.A07;
        }
        messageDialogFragment.A0U(bundle);
        return messageDialogFragment;
    }

    public void A02(DialogInterface.OnClickListener onClickListener, int i) {
        this.A03 = i;
        this.A06 = onClickListener;
    }
}
