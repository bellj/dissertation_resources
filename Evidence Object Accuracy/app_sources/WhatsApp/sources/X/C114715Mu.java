package X;

import java.io.IOException;

/* renamed from: X.5Mu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114715Mu extends AnonymousClass1TM {
    public static final AnonymousClass1TK A03 = C72453ed.A13("1.3.6.1.5.5.7.1.4");
    public static final AnonymousClass1TK A04 = C72453ed.A13("1.3.6.1.5.5.7.1.1");
    public static final AnonymousClass1TK A05 = C72453ed.A13("2.5.29.35");
    public static final AnonymousClass1TK A06 = C72453ed.A13("2.5.29.19");
    public static final AnonymousClass1TK A07 = C72453ed.A13("1.3.6.1.5.5.7.1.2");
    public static final AnonymousClass1TK A08 = C72453ed.A13("2.5.29.31");
    public static final AnonymousClass1TK A09 = C72453ed.A13("2.5.29.20");
    public static final AnonymousClass1TK A0A = C72453ed.A13("2.5.29.29");
    public static final AnonymousClass1TK A0B = C72453ed.A13("2.5.29.32");
    public static final AnonymousClass1TK A0C = C72453ed.A13("2.5.29.27");
    public static final AnonymousClass1TK A0D = C72453ed.A13("2.5.29.60");
    public static final AnonymousClass1TK A0E = C72453ed.A13("2.5.29.37");
    public static final AnonymousClass1TK A0F = C72453ed.A13("2.5.29.46");
    public static final AnonymousClass1TK A0G = C72453ed.A13("2.5.29.54");
    public static final AnonymousClass1TK A0H = C72453ed.A13("2.5.29.23");
    public static final AnonymousClass1TK A0I = C72453ed.A13("2.5.29.24");
    public static final AnonymousClass1TK A0J = C72453ed.A13("2.5.29.18");
    public static final AnonymousClass1TK A0K = C72453ed.A13("2.5.29.28");
    public static final AnonymousClass1TK A0L = C72453ed.A13("2.5.29.15");
    public static final AnonymousClass1TK A0M = C72453ed.A13("1.3.6.1.5.5.7.1.12");
    public static final AnonymousClass1TK A0N = C72453ed.A13("2.5.29.30");
    public static final AnonymousClass1TK A0O = C72453ed.A13("2.5.29.56");
    public static final AnonymousClass1TK A0P = C72453ed.A13("2.5.29.36");
    public static final AnonymousClass1TK A0Q = C72453ed.A13("2.5.29.33");
    public static final AnonymousClass1TK A0R = C72453ed.A13("2.5.29.16");
    public static final AnonymousClass1TK A0S = C72453ed.A13("1.3.6.1.5.5.7.1.3");
    public static final AnonymousClass1TK A0T = C72453ed.A13("2.5.29.21");
    public static final AnonymousClass1TK A0U = C72453ed.A13("2.5.29.17");
    public static final AnonymousClass1TK A0V = C72453ed.A13("2.5.29.9");
    public static final AnonymousClass1TK A0W = C72453ed.A13("1.3.6.1.5.5.7.1.11");
    public static final AnonymousClass1TK A0X = C72453ed.A13("2.5.29.14");
    public static final AnonymousClass1TK A0Y = C72453ed.A13("2.5.29.55");
    public AnonymousClass1TK A00;
    public AnonymousClass5NH A01;
    public boolean A02;

    public C114715Mu(AnonymousClass1TK r2, byte[] bArr, boolean z) {
        AnonymousClass5N5 r0 = new AnonymousClass5N5(bArr);
        this.A00 = r2;
        this.A02 = z;
        this.A01 = r0;
    }

    public C114715Mu(AbstractC114775Na r6) {
        AnonymousClass1TN A0D2;
        if (r6.A0B() == 2) {
            this.A00 = AnonymousClass1TK.A00(r6.A0D(0));
            this.A02 = false;
            A0D2 = r6.A0D(1);
        } else if (r6.A0B() == 3) {
            this.A00 = AnonymousClass1TK.A00(r6.A0D(0));
            this.A02 = C12960it.A1S(AnonymousClass5NF.A00(r6.A0D(1)).A00);
            A0D2 = r6.A0D(2);
        } else {
            throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r6.A0B()));
        }
        this.A01 = AnonymousClass5NH.A01(A0D2);
    }

    public AnonymousClass1TN A03() {
        try {
            return AnonymousClass1TL.A03(this.A01.A00);
        } catch (IOException e) {
            throw C12970iu.A0f(C12960it.A0b("can't convert extension: ", e));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r1 = new C94954co(3);
        r1.A06(this.A00);
        if (this.A02) {
            r1.A06(AnonymousClass5NF.A02);
        }
        return C94954co.A01(this.A01, r1);
    }

    @Override // X.AnonymousClass1TM
    public boolean equals(Object obj) {
        if (!(obj instanceof C114715Mu)) {
            return false;
        }
        C114715Mu r4 = (C114715Mu) obj;
        return r4.A00.A04(this.A00) && r4.A01.A04(this.A01) && r4.A02 == this.A02;
    }

    @Override // X.AnonymousClass1TM
    public int hashCode() {
        boolean z = this.A02;
        int hashCode = this.A01.hashCode() ^ this.A00.hashCode();
        return !z ? hashCode ^ -1 : hashCode;
    }
}
