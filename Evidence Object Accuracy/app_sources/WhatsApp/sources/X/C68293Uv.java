package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Uv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68293Uv implements AnonymousClass5Vo {
    public final /* synthetic */ C53842fM A00;
    public final /* synthetic */ UserJid A01;

    @Override // X.AnonymousClass5Vo
    public void AQJ(C92404Vt r1, int i) {
    }

    public C68293Uv(C53842fM r1, UserJid userJid) {
        this.A00 = r1;
        this.A01 = userJid;
    }

    @Override // X.AnonymousClass5Vo
    public void AQK(C92404Vt r5, AnonymousClass3CI r6) {
        C53842fM r3 = this.A00;
        C25801Aw r2 = r3.A0I;
        UserJid userJid = this.A01;
        r2.A03(r6, userJid, true);
        r3.A08.A0A(r2.A01(userJid, "catalog_category_dummy_root_id"));
    }
}
