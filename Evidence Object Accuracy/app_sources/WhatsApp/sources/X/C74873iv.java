package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;

/* renamed from: X.3iv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74873iv extends AbstractC018308n {
    public final int A00;
    public final /* synthetic */ CatalogCarouselDetailImageView A01;

    public C74873iv(CatalogCarouselDetailImageView catalogCarouselDetailImageView, int i) {
        this.A01 = catalogCarouselDetailImageView;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        int A00 = RecyclerView.A00(view);
        int i = 0;
        rect.top = 0;
        rect.bottom = 0;
        if (A00 != 0) {
            i = this.A00;
        }
        rect.left = i;
    }
}
