package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3zE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84393zE extends AnonymousClass2Cu {
    public final /* synthetic */ ConversationsFragment A00;

    public C84393zE(ConversationsFragment conversationsFragment) {
        this.A00 = conversationsFragment;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        ConversationsFragment.A04(this.A00, userJid);
    }
}
