package X;

import android.view.animation.Animation;
import com.whatsapp.QrImageView;

/* renamed from: X.4oh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class animation.Animation$AnimationListenerC102164oh implements Animation.AnimationListener {
    public final /* synthetic */ AbstractC49172Jp A00;
    public final /* synthetic */ QrImageView A01;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public animation.Animation$AnimationListenerC102164oh(AbstractC49172Jp r1, QrImageView qrImageView) {
        this.A01 = qrImageView;
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.AOS(this.A01);
    }
}
