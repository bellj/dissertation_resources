package X;

/* renamed from: X.3qL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C79283qL extends AbstractC79223qF {
    public int A00;
    public final int A01;
    public final int A02;
    public final byte[] A03;

    public C79283qL(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            int length = bArr.length;
            int i3 = i + i2;
            if ((i | i2 | (length - i3)) >= 0) {
                this.A03 = bArr;
                this.A02 = i;
                this.A00 = i;
                this.A01 = i3;
                return;
            }
            Object[] objArr = new Object[3];
            C12960it.A1P(objArr, length, 0);
            C12960it.A1P(objArr, i, 1);
            C12960it.A1P(objArr, i2, 2);
            throw C12970iu.A0f(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", objArr));
        }
        throw C12980iv.A0n("buffer");
    }
}
