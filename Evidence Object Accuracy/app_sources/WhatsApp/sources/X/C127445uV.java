package X;

import android.view.View;

/* renamed from: X.5uV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127445uV {
    public final View.OnClickListener A00;
    public final String A01;
    public final boolean A02;

    public C127445uV(View.OnClickListener onClickListener, String str, boolean z) {
        this.A02 = z;
        this.A01 = str;
        this.A00 = onClickListener;
    }
}
