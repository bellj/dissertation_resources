package X;

import android.content.pm.PackageManager;
import android.net.Uri;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.133  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass133 {
    public final AnonymousClass136 A00;
    public final C15520nO A01;
    public final C15490nL A02;
    public final AnonymousClass135 A03;

    public AnonymousClass133(AnonymousClass136 r1, C15520nO r2, C15490nL r3, AnonymousClass135 r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
    }

    public final Iterable A00() {
        boolean z;
        C15520nO r6 = this.A01;
        Set<String> A02 = r6.A02();
        if (A02.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        AnonymousClass136 r2 = this.A00;
        Iterator it = r2.A01.iterator();
        while (it.hasNext()) {
            arrayList.add(((AnonymousClass01H) it.next()).get());
        }
        C28601Of r4 = r2.A00;
        for (String str : A02) {
            try {
                z = this.A02.A01(str).A03;
            } catch (PackageManager.NameNotFoundException unused) {
                z = false;
            }
            if (!z) {
                StringBuilder sb = new StringBuilder("InstrumentationChangeDispatcher/verification failed, dropping event for package - ");
                sb.append(str);
                Log.w(sb.toString());
                r6.A03(str);
            } else {
                AnonymousClass01H r0 = (AnonymousClass01H) r4.A00.get(str);
                if (r0 != null) {
                    arrayList.add(r0.get());
                }
            }
        }
        return arrayList;
    }

    public void A01() {
        for (AbstractC35911iz r0 : A00()) {
            r0.A00();
        }
        C15520nO r2 = this.A01;
        for (String str : r2.A02()) {
            r2.A03(str);
        }
        AnonymousClass135 r1 = this.A03;
        r1.A00.A00.revokeUriPermission(Uri.parse("content://com.whatsapp.provider.instrumentation"), 3);
    }
}
