package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;

/* renamed from: X.2CE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CE extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AbstractC14670lq A00;

    public AnonymousClass2CE(AbstractC14670lq r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass2CF r3;
        AbstractC14670lq r1 = this.A00;
        if (r1.A0S) {
            r1.A0Z.postDelayed(this, 1000);
        } else if (r1.A0P != null && (r3 = r1.A0O) != null) {
            r3.A07(new RunnableBRunnable0Shape13S0100000_I0_13(this, 30), 300);
        }
    }
}
