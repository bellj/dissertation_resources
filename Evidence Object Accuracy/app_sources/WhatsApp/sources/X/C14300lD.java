package X;

import android.graphics.Bitmap;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.UUID;
import java.util.concurrent.Executor;

/* renamed from: X.0lD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14300lD {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C002701f A03;
    public final C18640sm A04;
    public final C14850m9 A05;
    public final C16120oU A06;
    public final C21780xy A07;
    public final C14410lO A08;
    public final C14420lP A09;
    public final AnonymousClass12J A0A;
    public final C26771Et A0B;
    public final C26471Dp A0C;
    public final C239713s A0D;
    public final C14540lb A0E;
    public final C22590zK A0F;
    public final C26511Dt A0G;
    public final C15690nk A0H;
    public final C26521Du A0I;
    public final AbstractC14440lR A0J;
    public final Executor A0K;

    public C14300lD(AbstractC15710nm r2, C14330lG r3, C14900mE r4, C002701f r5, C18640sm r6, C14850m9 r7, C16120oU r8, C21780xy r9, C14410lO r10, C14420lP r11, AnonymousClass12J r12, C26771Et r13, C26471Dp r14, C239713s r15, C14540lb r16, C22590zK r17, C26511Dt r18, C15690nk r19, C26521Du r20, AbstractC14440lR r21) {
        this.A05 = r7;
        this.A02 = r4;
        this.A00 = r2;
        this.A0J = r21;
        this.A01 = r3;
        this.A06 = r8;
        this.A0C = r14;
        this.A08 = r10;
        this.A0G = r18;
        this.A0D = r15;
        this.A0I = r20;
        this.A0F = r17;
        this.A0E = r16;
        this.A09 = r11;
        this.A0A = r12;
        this.A0B = r13;
        this.A0H = r19;
        this.A04 = r6;
        this.A03 = r5;
        this.A07 = r9;
        this.A0K = new ExecutorC39691qM(r4);
    }

    public static void A00(File file) {
        if (file != null) {
            StringBuilder sb = new StringBuilder("size=");
            sb.append(file.length());
            sb.append(" exists=");
            sb.append(file.exists());
            sb.toString();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:131:0x022b A[Catch: all -> 0x035d, TryCatch #3 {, blocks: (B:34:0x0066, B:36:0x0070, B:59:0x00ad, B:60:0x00b1, B:71:0x00f7, B:72:0x00fe, B:75:0x0102, B:85:0x0124, B:86:0x012b, B:87:0x012c, B:88:0x0133, B:89:0x0137, B:91:0x0148, B:92:0x014d, B:94:0x0171, B:95:0x018e, B:97:0x0192, B:98:0x019e, B:100:0x01a2, B:102:0x01a6, B:105:0x01ad, B:107:0x01b2, B:108:0x01b7, B:110:0x01c3, B:114:0x01ce, B:115:0x01d2, B:117:0x01d8, B:118:0x01e0, B:120:0x01ea, B:122:0x01f4, B:124:0x0200, B:126:0x0204, B:128:0x0210, B:129:0x0215, B:131:0x022b, B:132:0x0233, B:138:0x0249, B:140:0x0254, B:141:0x0259, B:143:0x0261, B:145:0x026b, B:149:0x027f, B:152:0x0287, B:153:0x028b, B:155:0x028f, B:156:0x02ab, B:158:0x02af, B:160:0x02b5, B:162:0x02bb, B:164:0x02bf, B:165:0x02c9, B:167:0x02cf, B:168:0x02d3, B:170:0x02d7, B:171:0x02f3, B:173:0x02f9, B:175:0x0309, B:176:0x0310, B:178:0x032c, B:180:0x0332, B:181:0x0336), top: B:195:0x0066, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x028f A[Catch: all -> 0x035d, TryCatch #3 {, blocks: (B:34:0x0066, B:36:0x0070, B:59:0x00ad, B:60:0x00b1, B:71:0x00f7, B:72:0x00fe, B:75:0x0102, B:85:0x0124, B:86:0x012b, B:87:0x012c, B:88:0x0133, B:89:0x0137, B:91:0x0148, B:92:0x014d, B:94:0x0171, B:95:0x018e, B:97:0x0192, B:98:0x019e, B:100:0x01a2, B:102:0x01a6, B:105:0x01ad, B:107:0x01b2, B:108:0x01b7, B:110:0x01c3, B:114:0x01ce, B:115:0x01d2, B:117:0x01d8, B:118:0x01e0, B:120:0x01ea, B:122:0x01f4, B:124:0x0200, B:126:0x0204, B:128:0x0210, B:129:0x0215, B:131:0x022b, B:132:0x0233, B:138:0x0249, B:140:0x0254, B:141:0x0259, B:143:0x0261, B:145:0x026b, B:149:0x027f, B:152:0x0287, B:153:0x028b, B:155:0x028f, B:156:0x02ab, B:158:0x02af, B:160:0x02b5, B:162:0x02bb, B:164:0x02bf, B:165:0x02c9, B:167:0x02cf, B:168:0x02d3, B:170:0x02d7, B:171:0x02f3, B:173:0x02f9, B:175:0x0309, B:176:0x0310, B:178:0x032c, B:180:0x0332, B:181:0x0336), top: B:195:0x0066, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02af A[Catch: all -> 0x035d, TryCatch #3 {, blocks: (B:34:0x0066, B:36:0x0070, B:59:0x00ad, B:60:0x00b1, B:71:0x00f7, B:72:0x00fe, B:75:0x0102, B:85:0x0124, B:86:0x012b, B:87:0x012c, B:88:0x0133, B:89:0x0137, B:91:0x0148, B:92:0x014d, B:94:0x0171, B:95:0x018e, B:97:0x0192, B:98:0x019e, B:100:0x01a2, B:102:0x01a6, B:105:0x01ad, B:107:0x01b2, B:108:0x01b7, B:110:0x01c3, B:114:0x01ce, B:115:0x01d2, B:117:0x01d8, B:118:0x01e0, B:120:0x01ea, B:122:0x01f4, B:124:0x0200, B:126:0x0204, B:128:0x0210, B:129:0x0215, B:131:0x022b, B:132:0x0233, B:138:0x0249, B:140:0x0254, B:141:0x0259, B:143:0x0261, B:145:0x026b, B:149:0x027f, B:152:0x0287, B:153:0x028b, B:155:0x028f, B:156:0x02ab, B:158:0x02af, B:160:0x02b5, B:162:0x02bb, B:164:0x02bf, B:165:0x02c9, B:167:0x02cf, B:168:0x02d3, B:170:0x02d7, B:171:0x02f3, B:173:0x02f9, B:175:0x0309, B:176:0x0310, B:178:0x032c, B:180:0x0332, B:181:0x0336), top: B:195:0x0066, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x02f9 A[Catch: all -> 0x035d, TryCatch #3 {, blocks: (B:34:0x0066, B:36:0x0070, B:59:0x00ad, B:60:0x00b1, B:71:0x00f7, B:72:0x00fe, B:75:0x0102, B:85:0x0124, B:86:0x012b, B:87:0x012c, B:88:0x0133, B:89:0x0137, B:91:0x0148, B:92:0x014d, B:94:0x0171, B:95:0x018e, B:97:0x0192, B:98:0x019e, B:100:0x01a2, B:102:0x01a6, B:105:0x01ad, B:107:0x01b2, B:108:0x01b7, B:110:0x01c3, B:114:0x01ce, B:115:0x01d2, B:117:0x01d8, B:118:0x01e0, B:120:0x01ea, B:122:0x01f4, B:124:0x0200, B:126:0x0204, B:128:0x0210, B:129:0x0215, B:131:0x022b, B:132:0x0233, B:138:0x0249, B:140:0x0254, B:141:0x0259, B:143:0x0261, B:145:0x026b, B:149:0x027f, B:152:0x0287, B:153:0x028b, B:155:0x028f, B:156:0x02ab, B:158:0x02af, B:160:0x02b5, B:162:0x02bb, B:164:0x02bf, B:165:0x02c9, B:167:0x02cf, B:168:0x02d3, B:170:0x02d7, B:171:0x02f3, B:173:0x02f9, B:175:0x0309, B:176:0x0310, B:178:0x032c, B:180:0x0332, B:181:0x0336), top: B:195:0x0066, inners: #1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C39841qd A01(X.C14430lQ r15, X.C14450lS r16, X.C39721qR r17, int r18, int r19, int r20) {
        /*
        // Method dump skipped, instructions count: 924
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14300lD.A01(X.0lQ, X.0lS, X.1qR, int, int, int):X.1qd");
    }

    public C39841qd A02(AnonymousClass1KC r9, int i, int i2) {
        int A00;
        C14430lQ r2 = r9.A0J;
        AnonymousClass009.A05(r2);
        C14450lS r3 = r9.A0K;
        AnonymousClass009.A05(r3);
        C39721qR r4 = (C39721qR) r9.A0H.A00();
        if (r9.A0U == "express") {
            A00 = 4;
        } else {
            AnonymousClass009.A05(r3);
            A00 = r3.A00();
        }
        return A01(r2, r3, r4, i, i2, A00);
    }

    public AnonymousClass1KC A03(AbstractC14500lX r6, AnonymousClass1K9 r7, boolean z) {
        C14410lO r4 = this.A08;
        C14420lP r2 = r4.A0K;
        String str = r7.A03;
        if (str == null) {
            str = UUID.randomUUID().toString();
        }
        C14430lQ A01 = r2.A01(str, 0);
        AnonymousClass1KC r22 = new AnonymousClass1KC(r6, A01, new C14450lS(A01, r2, r4.A0R, z), r7);
        C14430lQ r0 = r22.A0J;
        AnonymousClass009.A05(r0);
        r22.A09.A04(r0.A0D);
        C14450lS r02 = r22.A0K;
        AnonymousClass009.A05(r02);
        r02.A03();
        return r22;
    }

    public AnonymousClass1KC A04(AnonymousClass1K9 r3, boolean z) {
        AbstractC14500lX r0;
        C14380lL r02 = r3.A00;
        C14370lK r1 = r02.A05;
        if (r02.A0A) {
            r0 = new C14490lW(r1);
        } else {
            r0 = new C38431o5(r1);
        }
        return A03(r0, r3, z);
    }

    public AnonymousClass1KC A05(AnonymousClass1K9 r4, boolean z) {
        AnonymousClass1KC A02 = this.A08.A02(r4, z);
        if (A02 == null) {
            return A04(r4, z);
        }
        if (z) {
            C14430lQ r1 = A02.A0J;
            AnonymousClass009.A05(r1);
            r1.A03++;
        }
        C14420lP r12 = this.A09;
        C14430lQ r0 = A02.A0J;
        AnonymousClass009.A05(r0);
        r12.A03(r0);
        AnonymousClass009.A05(r0);
        A02.A09.A04(r0.A0D);
        C14450lS r02 = A02.A0K;
        AnonymousClass009.A05(r02);
        r02.A03();
        return A02;
    }

    public void A06(AnonymousClass1KC r4) {
        r4.A02 = true;
        C26771Et r2 = this.A0B;
        r2.A04.A00(r4.A01().A05).A05(r4);
        r2.A02.A05(r4);
        r2.A03.A05(r4);
        C14540lb r1 = this.A0E;
        r1.A05(r4);
        C14460lT r0 = r4.A01;
        if (r0 != null) {
            r1.A05(r0);
        }
    }

    public void A07(AnonymousClass1KC r2) {
        C39721qR r0 = (C39721qR) r2.A0H.A00();
        if (r0 != null) {
            A0C(r2, r0);
            return;
        }
        AbstractC39731qS r02 = (AbstractC39731qS) r2.A0D.A00();
        if (r02 != null) {
            A0B(r2, r02);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r4.A0H.A00() != null) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08(X.AnonymousClass1KC r4, int r5) {
        /*
            r3 = this;
            X.0lj r0 = r4.A0D
            java.lang.Object r0 = r0.A00()
            r2 = 1
            if (r0 != 0) goto L_0x0012
            X.0lj r0 = r4.A0H
            java.lang.Object r1 = r0.A00()
            r0 = 0
            if (r1 == 0) goto L_0x0013
        L_0x0012:
            r0 = 1
        L_0x0013:
            X.AnonymousClass009.A0F(r0)
            if (r5 != r2) goto L_0x0035
            X.0lj r0 = r4.A08
            java.lang.Object r0 = r0.A00()
            X.1qg r0 = (X.C39871qg) r0
            if (r0 == 0) goto L_0x0035
            java.io.File r1 = r0.A01
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.A02
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0035
            boolean r0 = X.C14350lI.A0M(r1)
            if (r0 == 0) goto L_0x0035
            r1.getAbsolutePath()
        L_0x0035:
            X.0lj r1 = r4.A0A
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r1.A04(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14300lD.A08(X.1KC, int):void");
    }

    public void A09(AnonymousClass1KC r7, C14510lY r8) {
        C14540lb r2 = this.A0E;
        StringBuilder sb = new StringBuilder("mediauploadqueue/enqueue ");
        sb.append(r8);
        Log.i(sb.toString());
        C14560ld r4 = (C14560ld) r2.A01(r7, r8);
        C14450lS r1 = r7.A0K;
        AnonymousClass009.A05(r1);
        r1.A07(r4.A0V.A04);
        int A00 = r8.A00();
        boolean z = true;
        if (!(A00 == 3 || A00 == 1)) {
            z = false;
            r4.A0D.A03(r7.A0P, this.A0K);
        }
        C39821qb r12 = new AbstractC14590lg(r7, r4) { // from class: X.1qb
            public final /* synthetic */ AnonymousClass1KC A01;
            public final /* synthetic */ C14560ld A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C14300lD r5 = C14300lD.this;
                AnonymousClass1KC r42 = this.A01;
                C14560ld r3 = this.A02;
                C14450lS r13 = r42.A0K;
                AnonymousClass009.A05(r13);
                synchronized (r13) {
                    r13.A0C = true;
                }
                r5.A0J.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r5, r42, obj, 7));
                r3.A04();
            }
        };
        Executor executor = this.A0K;
        r4.A0H.A03(r12, executor);
        r4.A0F.A03(new AbstractC14590lg(r7) { // from class: X.1qc
            public final /* synthetic */ AnonymousClass1KC A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                boolean z2;
                C41601tn A04;
                Bitmap bitmap;
                Bitmap A0B;
                byte[] A002;
                C14300lD r13 = C14300lD.this;
                AnonymousClass1KC r0 = this.A01;
                C14400lN r15 = (C14400lN) obj;
                C14370lK r5 = r0.A01().A05;
                C14480lV r42 = r0.A0L.A02;
                if (!r13.A0E(r5, r0.A00().A0E, r42.A01)) {
                    r0.A03 = null;
                    return;
                }
                byte[] bArr = r0.A03;
                if (bArr != null) {
                    try {
                        File A003 = r13.A07.A00.A00("");
                        C14350lI.A0F(A003, bArr);
                        A003.getAbsolutePath();
                        C14380lL r132 = new C14380lL(null, r15, C14370lK.A00(r0.A01().A05.A00), A003, null, null, "mms", null, 0, 0, 0, true, true, false, false);
                        C14410lO r6 = r13.A08;
                        byte b = r0.A01().A05.A00;
                        int i = r0.A01().A01;
                        C14450lS r52 = r0.A0K;
                        AnonymousClass009.A05(r52);
                        synchronized (r52) {
                            z2 = r52.A0I;
                        }
                        C14430lQ r53 = r0.A0J;
                        AnonymousClass009.A05(r53);
                        String str = r53.A0D;
                        C14420lP r82 = r6.A0K;
                        C14430lQ A02 = r82.A02(str, 2);
                        if (A02 == null) {
                            A02 = r82.A01(str, 2);
                        } else if (z2) {
                            A02.A03++;
                        }
                        C14450lS r62 = new C14450lS(A02, r82, r6.A0R, z2);
                        r62.A04(b, i, false);
                        C14460lT r54 = new C14460lT(A02, r62);
                        C14370lK r63 = r132.A05;
                        C39881qh r83 = new C39881qh(r63, A003, null, false);
                        if (!(r83.A00 != C14370lK.A09 || (bitmap = (A04 = C37501mV.A04(new C41591tm(null, null, 96, 96, true), r83.A01)).A02) == null || (A002 = AnonymousClass3GQ.A00((A0B = C22200yh.A0B(bitmap, null, 96, 96)), 48, false)) == null)) {
                            Pair pair = new Pair(Integer.valueOf(A04.A01), Integer.valueOf(A04.A00));
                            A0B.getWidth();
                            A0B.getHeight();
                            r0.A04.A04(new C39901qj(pair, null, A002, null));
                        }
                        C14510lY r64 = new C14510lY(new C14490lW(r63), r54.A00, r132, r42);
                        r54.A01.A07(r64.A04);
                        r0.A01 = r54;
                        C14540lb r43 = r13.A0E;
                        StringBuilder sb2 = new StringBuilder("mediauploadqueue/enqueue ");
                        sb2.append(r64);
                        Log.i(sb2.toString());
                        ((C14560ld) r43.A01(r54, r64)).A0H.A03(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x012c: INVOKE  
                              (wrap: X.0lj : 0x012a: IGET  (r1v2 X.0lj A[REMOVE]) = 
                              (wrap: X.0ld : 0x0121: CHECK_CAST (r4v3 X.0ld A[REMOVE]) = (X.0ld) (wrap: java.lang.Runnable : 0x011d: INVOKE  (r4v2 java.lang.Runnable A[REMOVE]) = (r4v1 'r43' X.0lb), (r5v11 'r54' X.0lT), (r6v3 'r64' X.0lY) type: VIRTUAL call: X.0lc.A01(java.lang.Object, java.lang.Object):java.lang.Runnable))
                             X.0ld.A0H X.0lj)
                              (wrap: X.5A8 : 0x0125: CONSTRUCTOR  (r3v7 X.5A8 A[REMOVE]) = (r0v1 'r0' X.1KC) call: X.5A8.<init>(X.1KC):void type: CONSTRUCTOR)
                              (wrap: java.util.concurrent.Executor : 0x0128: IGET  (r2v16 java.util.concurrent.Executor A[REMOVE]) = (r1v0 'r13' X.0lD) X.0lD.A0K java.util.concurrent.Executor)
                             type: VIRTUAL call: X.0lj.A03(X.0lg, java.util.concurrent.Executor):void in method: X.1qc.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:314)
                            	at jadx.core.dex.regions.TryCatchRegion.generate(TryCatchRegion.java:85)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0125: CONSTRUCTOR  (r3v7 X.5A8 A[REMOVE]) = (r0v1 'r0' X.1KC) call: X.5A8.<init>(X.1KC):void type: CONSTRUCTOR in method: X.1qc.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 31 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.5A8, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 37 more
                            */
                        /*
                        // Method dump skipped, instructions count: 327
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C39831qc.accept(java.lang.Object):void");
                    }
                }, null);
                if (!z) {
                    r4.A0G.A03(r7.A0Q, executor);
                }
                r4.A0E.A03(r7.A0M, executor);
            }

            public final void A0A(AnonymousClass1KC r4, AnonymousClass1qN r5) {
                r5.A02.A03(r4.A0N, null);
                r5.A03.A03(r4.A0O, null);
                r5.A04.A03(new AbstractC14590lg(r4, r5) { // from class: X.1qO
                    public final /* synthetic */ AnonymousClass1KC A01;
                    public final /* synthetic */ AnonymousClass1qN A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                    }

                    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
                        if (r6 == X.C14370lK.A0S) goto L_0x003b;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
                        r4 = false;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0039, code lost:
                        if ((X.C38441o6.A00(r7.A05, r7.A03.A05(false)) & 1) != 0) goto L_0x003b;
                     */
                    @Override // X.AbstractC14590lg
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final void accept(java.lang.Object r9) {
                        /*
                            r8 = this;
                            X.0lD r5 = X.C14300lD.this
                            X.1KC r3 = r8.A01
                            X.1qN r2 = r8.A02
                            X.1qS r9 = (X.AbstractC39731qS) r9
                            java.io.File r4 = r9.A01
                            if (r4 == 0) goto L_0x0017
                            r0 = 0
                            X.1qg r1 = new X.1qg
                            r1.<init>(r4, r0)
                            X.0lj r0 = r3.A08
                            r0.A04(r1)
                        L_0x0017:
                            X.0lj r0 = r3.A0D
                            r0.A04(r9)
                            X.12J r7 = r5.A0A
                            X.1qQ r0 = r3.A01()
                            X.0lK r6 = r0.A05
                            boolean r0 = X.AnonymousClass14P.A01(r6)
                            r4 = 1
                            r1 = 0
                            if (r0 == 0) goto L_0x0043
                            X.0sm r0 = r7.A03
                            int r1 = r0.A05(r1)
                            X.0m6 r0 = r7.A05
                            int r0 = X.C38441o6.A00(r0, r1)
                            r0 = r0 & r4
                            if (r0 == 0) goto L_0x0048
                        L_0x003b:
                            X.0lS r1 = r3.A0K
                            X.AnonymousClass009.A05(r1)
                            monitor-enter(r1)
                            r0 = 1
                            goto L_0x004a
                        L_0x0043:
                            X.0lK r0 = X.C14370lK.A0S
                            if (r6 != r0) goto L_0x0048
                            goto L_0x003b
                        L_0x0048:
                            r4 = 0
                            goto L_0x003b
                        L_0x004a:
                            r1.A0D = r0     // Catch: all -> 0x0086
                            monitor-exit(r1)
                            java.lang.String r0 = r3.A0U
                            if (r0 != 0) goto L_0x0082
                            boolean r0 = r3.A02
                            if (r0 != 0) goto L_0x0082
                            if (r4 != 0) goto L_0x0082
                            X.AnonymousClass009.A05(r1)
                            X.0lZ r0 = new X.0lZ
                            r0.<init>()
                            r1.A07(r0)
                            r0 = 14
                            X.0lj r1 = r3.A0A
                            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                            r1.A04(r0)
                        L_0x006d:
                            X.0lj r0 = r2.A02
                            r0.A01()
                            X.0lj r0 = r2.A03
                            r0.A01()
                            X.0lj r0 = r2.A04
                            r0.A01()
                            X.0lj r0 = r2.A01
                            r0.A01()
                            return
                        L_0x0082:
                            r5.A0B(r3, r9)
                            goto L_0x006d
                        L_0x0086:
                            r0 = move-exception
                            monitor-exit(r1)
                            throw r0
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C39701qO.accept(java.lang.Object):void");
                    }
                }, null);
                r5.A01.A03(new AbstractC14590lg(r4) { // from class: X.1qP
                    public final /* synthetic */ AnonymousClass1KC A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        C14300lD r1 = C14300lD.this;
                        Number number = (Number) obj;
                        if (this.A01.A0U != null) {
                            r1.A02.A04(number.intValue());
                        }
                    }
                }, null);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:13:0x003a, code lost:
                if (r4 != null) goto L_0x003c;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void A0B(X.AnonymousClass1KC r6, X.AbstractC39731qS r7) {
                /*
                    r5 = this;
                    java.util.concurrent.atomic.AtomicBoolean r2 = r6.A0S
                    r1 = 0
                    r0 = 1
                    boolean r0 = r2.compareAndSet(r1, r0)
                    if (r0 == 0) goto L_0x0012
                    boolean r0 = r6.A02
                    if (r0 == 0) goto L_0x0013
                    r0 = 1
                L_0x000f:
                    r5.A08(r6, r0)
                L_0x0012:
                    return
                L_0x0013:
                    boolean r0 = r7.A02
                    if (r0 == 0) goto L_0x0048
                    byte[] r4 = r7.A03
                    X.0lL r0 = r6.A00()
                    X.0lY r3 = new X.0lY
                    r3.<init>(r6, r0)
                    X.1qQ r0 = r6.A01()
                    X.0lK r2 = r0.A05
                    X.1K9 r0 = r6.A0L
                    X.0lV r0 = r0.A02
                    boolean r1 = r0.A01
                    X.0lL r0 = r6.A00()
                    int[] r0 = r0.A0E
                    boolean r0 = r5.A0E(r2, r0, r1)
                    if (r0 == 0) goto L_0x0046
                    if (r4 == 0) goto L_0x003e
                L_0x003c:
                    r6.A03 = r4
                L_0x003e:
                    boolean r0 = r6.A02
                    if (r0 != 0) goto L_0x004b
                    r5.A09(r6, r3)
                    return
                L_0x0046:
                    r4 = 0
                    goto L_0x003c
                L_0x0048:
                    r0 = 20
                    goto L_0x000f
                L_0x004b:
                    java.lang.String r1 = "mediatranscodequeue/success/all-cancelled "
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>(r1)
                    r0.append(r6)
                    java.lang.String r0 = r0.toString()
                    com.whatsapp.util.Log.i(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C14300lD.A0B(X.1KC, X.1qS):void");
            }

            /* JADX WARNING: Code restructure failed: missing block: B:26:0x006c, code lost:
                if (r7 == 0) goto L_0x006e;
             */
            /* JADX WARNING: Removed duplicated region for block: B:43:0x00ba A[Catch: all -> 0x01ff, TryCatch #6 {, blocks: (B:23:0x0058, B:30:0x0073, B:32:0x0086, B:34:0x0095, B:35:0x009a, B:37:0x00a5, B:39:0x00ab, B:40:0x00b0, B:41:0x00b4, B:43:0x00ba, B:44:0x00bc, B:46:0x00c9, B:49:0x00e1, B:51:0x00e9, B:53:0x00ef, B:54:0x00f9, B:56:0x00ff, B:58:0x010c, B:59:0x0112, B:60:0x0117, B:62:0x0122, B:64:0x012d, B:65:0x0134, B:67:0x013c, B:68:0x0142, B:70:0x014e, B:72:0x0158, B:75:0x0162, B:78:0x0171, B:79:0x0174), top: B:118:0x0058, inners: #5, #7 }] */
            /* JADX WARNING: Removed duplicated region for block: B:46:0x00c9 A[Catch: all -> 0x01ff, TryCatch #6 {, blocks: (B:23:0x0058, B:30:0x0073, B:32:0x0086, B:34:0x0095, B:35:0x009a, B:37:0x00a5, B:39:0x00ab, B:40:0x00b0, B:41:0x00b4, B:43:0x00ba, B:44:0x00bc, B:46:0x00c9, B:49:0x00e1, B:51:0x00e9, B:53:0x00ef, B:54:0x00f9, B:56:0x00ff, B:58:0x010c, B:59:0x0112, B:60:0x0117, B:62:0x0122, B:64:0x012d, B:65:0x0134, B:67:0x013c, B:68:0x0142, B:70:0x014e, B:72:0x0158, B:75:0x0162, B:78:0x0171, B:79:0x0174), top: B:118:0x0058, inners: #5, #7 }] */
            /* JADX WARNING: Removed duplicated region for block: B:47:0x00de  */
            /* JADX WARNING: Removed duplicated region for block: B:72:0x0158 A[Catch: all -> 0x01ff, TryCatch #6 {, blocks: (B:23:0x0058, B:30:0x0073, B:32:0x0086, B:34:0x0095, B:35:0x009a, B:37:0x00a5, B:39:0x00ab, B:40:0x00b0, B:41:0x00b4, B:43:0x00ba, B:44:0x00bc, B:46:0x00c9, B:49:0x00e1, B:51:0x00e9, B:53:0x00ef, B:54:0x00f9, B:56:0x00ff, B:58:0x010c, B:59:0x0112, B:60:0x0117, B:62:0x0122, B:64:0x012d, B:65:0x0134, B:67:0x013c, B:68:0x0142, B:70:0x014e, B:72:0x0158, B:75:0x0162, B:78:0x0171, B:79:0x0174), top: B:118:0x0058, inners: #5, #7 }] */
            /* JADX WARNING: Removed duplicated region for block: B:94:0x01c9  */
            /* JADX WARNING: Removed duplicated region for block: B:97:0x01df  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void A0C(X.AnonymousClass1KC r18, X.C39721qR r19) {
                /*
                // Method dump skipped, instructions count: 514
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C14300lD.A0C(X.1KC, X.1qR):void");
            }

            /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b5, code lost:
                if (r7 != false) goto L_0x00b7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b7, code lost:
                r0 = r20.A01();
                r2 = r19.A0B;
                r9 = new X.C39741qT(r2.A01);
                r1 = r20.A0J;
                X.AnonymousClass009.A05(r1);
                r1 = r1.A0D;
                r9.A03.A0D = java.lang.Long.valueOf(java.util.UUID.fromString(r1).getMostSignificantBits() & Long.MAX_VALUE);
                r1 = new X.AnonymousClass1qN(r9, r0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e8, code lost:
                if (r0.A0C != false) goto L_0x0134;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ec, code lost:
                if (r0.A07 != null) goto L_0x0134;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ee, code lost:
                r13 = r1.A07;
                r12 = r1.A06;
                r11 = r1.A05;
                r15 = r0.A0B;
                X.AnonymousClass009.A05(r15);
                r6 = r0.A00();
                r5 = new java.lang.StringBuilder();
                r5.append(java.util.UUID.randomUUID().toString());
                r5.append(".tmp");
                r2.A04.A01(new X.C39241pW(r9, r20, r11, r12, r13, r2.A00(r6, r5.toString()), r15), r0.A05);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:41:0x0126, code lost:
                r2 = r20.A0K;
                X.AnonymousClass009.A05(r2);
                r2.A06(r1.A00);
                A0A(r20, r1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:42:0x0133, code lost:
                return;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:43:0x0134, code lost:
                r6 = r0.A05;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:44:0x0138, code lost:
                if (r6 != X.C14370lK.A04) goto L_0x014e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:45:0x013a, code lost:
                r2.A02.A06(r2.A00.A00, new X.C39791qY(r9, r20, r1, r2, r0), r20, r0.A08);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:47:0x0150, code lost:
                if (r6 == X.C14370lK.A0X) goto L_0x023c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:49:0x0154, code lost:
                if (r6 == X.C14370lK.A0a) goto L_0x023c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:50:0x0156, code lost:
                r4 = X.C14370lK.A05;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:51:0x0158, code lost:
                if (r6 == r4) goto L_0x0204;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:53:0x015c, code lost:
                if (r6 == X.C14370lK.A0I) goto L_0x0204;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:54:0x015e, code lost:
                r5 = X.C14370lK.A0B;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:55:0x0160, code lost:
                if (r6 == r5) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:57:0x0164, code lost:
                if (r6 == X.C14370lK.A0Z) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:59:0x0168, code lost:
                if (r6 == X.C14370lK.A0G) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:61:0x016c, code lost:
                if (r6 == X.C14370lK.A0R) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:63:0x0170, code lost:
                if (r6 == X.C14370lK.A06) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:65:0x0174, code lost:
                if (r6 == X.C14370lK.A0L) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:67:0x0178, code lost:
                if (r6 == X.C14370lK.A07) goto L_0x01bd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:68:0x017a, code lost:
                r5 = X.C14370lK.A0S;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:69:0x017c, code lost:
                if (r6 != r5) goto L_0x0126;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:70:0x017e, code lost:
                r15 = r0.A07;
                r6 = r0.A0B;
                r4 = r0.A09;
                r14 = r0.A06;
                r13 = r1.A07;
                r12 = r1.A06;
                r11 = r1.A05;
                r7 = r0.A00();
                r8 = new java.lang.StringBuilder();
                r8.append(java.util.UUID.randomUUID().toString());
                r8.append(".webp");
                r2.A04.A01(new X.C39221pU(r9, r20, r11, r12, r13, r14, r15, r2.A00(r7, r8.toString()), r6, r4), r5);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:71:0x01bd, code lost:
                r6 = r0.A0B;
                X.AnonymousClass009.A05(r6);
                r14 = r1.A07;
                r12 = r1.A06;
                r11 = r1.A05;
                r7 = r0.A00();
                r8 = new java.lang.StringBuilder();
                r8.append(java.util.UUID.randomUUID().toString());
                r8.append(".jpg");
                r15 = r2.A00(r7, r8.toString());
                r13 = r0.A04;
                X.AnonymousClass009.A05(r13);
                r2.A04.A01(new X.C39201pS(r9, r20, r11, r12, r13, r14, r15, r6, r0.A0E, r0.A0D), r5);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:72:0x0204, code lost:
                r13 = r1.A07;
                r12 = r1.A06;
                r11 = r1.A05;
                r14 = r0.A07;
                X.AnonymousClass009.A05(r14);
                r5 = r0.A0G;
                r6 = new java.lang.StringBuilder();
                r6.append(java.util.UUID.randomUUID().toString());
                r6.append(".aac");
                r2.A04.A01(new X.C39181pQ(r9, r20, r11, r12, r13, r14, r2.A00(r14, r6.toString()), r5), r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:73:0x023c, code lost:
                r2.A02.A06(r2.A00.A00, new X.C39811qa(r9, r20, r1, r2, r0), r20, r0.A08);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:74:0x0251, code lost:
                if (r1 == null) goto L_0x00b7;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void A0D(X.AnonymousClass1KC r20, java.lang.String r21) {
                /*
                // Method dump skipped, instructions count: 610
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C14300lD.A0D(X.1KC, java.lang.String):void");
            }

            public boolean A0E(C14370lK r3, int[] iArr, boolean z) {
                C14850m9 r1 = this.A05;
                if (r1.A07(247) && r3 == C14370lK.A08) {
                    return true;
                }
                if (r1.A07(246)) {
                    return ((r3 == C14370lK.A0B && (iArr == null || iArr.length < 2)) || r3 == C14370lK.A0X || r3 == C14370lK.A04) && z;
                }
                return false;
            }
        }
