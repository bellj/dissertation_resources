package X;

import android.content.Intent;

/* renamed from: X.66k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1323166k implements AbstractC42541vN {
    public final /* synthetic */ int A00 = 30;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ AbstractC115825Tb A02;
    public final /* synthetic */ AnonymousClass67N A03;

    public C1323166k(ActivityC13790kL r2, AbstractC115825Tb r3, AnonymousClass67N r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        this.A01.A2Z(this);
        boolean z = false;
        if (i != this.A00) {
            return false;
        }
        AbstractC115825Tb r2 = this.A02;
        if (i2 == -1) {
            z = true;
        }
        r2.AVM(z);
        return true;
    }
}
