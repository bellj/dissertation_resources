package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.whatsapp.Conversation;
import com.whatsapp.util.Log;

/* renamed from: X.2o8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58092o8 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ ViewGroup A02;
    public final /* synthetic */ Conversation A03;
    public final /* synthetic */ boolean A04;

    public C58092o8(View view, ViewGroup viewGroup, Conversation conversation, int i, boolean z) {
        this.A03 = conversation;
        this.A02 = viewGroup;
        this.A01 = view;
        this.A04 = z;
        this.A00 = i;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        Log.i("conversation/hideinputextension/end");
        this.A02.setVisibility(8);
        Conversation conversation = this.A03;
        C12980iv.A1H(conversation.A0A.getViewTreeObserver(), this, 0);
        conversation.A10.setClipChildren(true);
        conversation.A1g.setTranscriptMode(this.A00);
    }
}
