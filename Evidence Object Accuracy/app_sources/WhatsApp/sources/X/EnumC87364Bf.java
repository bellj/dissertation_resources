package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87364Bf extends Enum implements AbstractC115135Qi {
    public static final AbstractC115605Sf A00 = new C108554zH();
    public static final /* synthetic */ EnumC87364Bf[] A01;
    public static final EnumC87364Bf A02;
    public static final EnumC87364Bf A03;
    public static final EnumC87364Bf A04;
    public static final EnumC87364Bf A05;
    public static final EnumC87364Bf A06;
    public static final EnumC87364Bf A07;
    public static final EnumC87364Bf A08;
    public static final EnumC87364Bf A09;
    public static final EnumC87364Bf A0A;
    public static final EnumC87364Bf A0B;
    public static final EnumC87364Bf A0C;
    public static final EnumC87364Bf A0D;
    public static final EnumC87364Bf A0E;
    public static final EnumC87364Bf A0F;
    public static final EnumC87364Bf A0G;
    public static final EnumC87364Bf A0H;
    public static final EnumC87364Bf A0I;
    public static final EnumC87364Bf A0J;
    public static final EnumC87364Bf A0K;
    public static final EnumC87364Bf A0L;
    public static final EnumC87364Bf A0M;
    public final int value;

    public EnumC87364Bf(int i, String str, int i2) {
        this.value = i2;
    }

    public static EnumC87364Bf[] values() {
        return (EnumC87364Bf[]) A01.clone();
    }

    static {
        EnumC87364Bf r15 = new EnumC87364Bf(0, "UNKNOWN_MOBILE_SUBTYPE", 0);
        A02 = r15;
        EnumC87364Bf r25 = new EnumC87364Bf(1, "GPRS", 1);
        A03 = r25;
        EnumC87364Bf r24 = new EnumC87364Bf(2, "EDGE", 2);
        A04 = r24;
        EnumC87364Bf r23 = new EnumC87364Bf(3, "UMTS", 3);
        A05 = r23;
        EnumC87364Bf r22 = new EnumC87364Bf(4, "CDMA", 4);
        A06 = r22;
        EnumC87364Bf r21 = new EnumC87364Bf(5, "EVDO_0", 5);
        A07 = r21;
        EnumC87364Bf r20 = new EnumC87364Bf(6, "EVDO_A", 6);
        A08 = r20;
        EnumC87364Bf r19 = new EnumC87364Bf(7, "RTT", 7);
        A09 = r19;
        EnumC87364Bf r18 = new EnumC87364Bf(8, "HSDPA", 8);
        A0A = r18;
        EnumC87364Bf r17 = new EnumC87364Bf(9, "HSUPA", 9);
        A0B = r17;
        EnumC87364Bf r14 = new EnumC87364Bf(10, "HSPA", 10);
        A0C = r14;
        EnumC87364Bf r13 = new EnumC87364Bf(11, "IDEN", 11);
        A0D = r13;
        EnumC87364Bf r12 = new EnumC87364Bf(12, "EVDO_B", 12);
        A0E = r12;
        EnumC87364Bf r11 = new EnumC87364Bf(13, "LTE", 13);
        A0F = r11;
        EnumC87364Bf r10 = new EnumC87364Bf(14, "EHRPD", 14);
        A0G = r10;
        EnumC87364Bf r9 = new EnumC87364Bf(15, "HSPAP", 15);
        A0H = r9;
        EnumC87364Bf r8 = new EnumC87364Bf(16, "GSM", 16);
        A0I = r8;
        EnumC87364Bf r7 = new EnumC87364Bf(17, "TD_SCDMA", 17);
        A0J = r7;
        EnumC87364Bf r6 = new EnumC87364Bf(18, "IWLAN", 18);
        A0K = r6;
        EnumC87364Bf r5 = new EnumC87364Bf(19, "LTE_CA", 19);
        A0L = r5;
        EnumC87364Bf r4 = new EnumC87364Bf(20, "COMBINED", 100);
        A0M = r4;
        EnumC87364Bf[] r3 = new EnumC87364Bf[21];
        C72453ed.A1F(r15, r25, r24, r23, r3);
        r3[4] = r22;
        C12970iu.A1R(r21, r20, r19, r18, r3);
        C72453ed.A1G(r17, r14, r13, r12, r3);
        C72453ed.A1H(r11, r10, r9, r8, r3);
        C72453ed.A1I(r7, r6, r5, r3);
        r3[20] = r4;
        A01 = r3;
    }
}
