package X;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;

/* renamed from: X.0Gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03160Gk extends AbstractC06170Sl {
    public static final String A03 = C06390Tk.A01("NetworkStateTracker");
    public C019509e A00;
    public AnonymousClass0AG A01;
    public final ConnectivityManager A02 = ((ConnectivityManager) super.A01.getSystemService("connectivity"));

    public C03160Gk(Context context, AbstractC11500gO r4) {
        super(context, r4);
        if (Build.VERSION.SDK_INT >= 24) {
            this.A01 = new AnonymousClass0AG(this);
        } else {
            this.A00 = new C019509e(this);
        }
    }

    @Override // X.AbstractC06170Sl
    public void A01() {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 24) {
            z = true;
        }
        if (z) {
            try {
                C06390Tk.A00().A02(A03, "Registering network callback", new Throwable[0]);
                this.A02.registerDefaultNetworkCallback(this.A01);
            } catch (IllegalArgumentException | SecurityException e) {
                C06390Tk.A00().A03(A03, "Received exception while registering network callback", e);
            }
        } else {
            C06390Tk.A00().A02(A03, "Registering broadcast receiver", new Throwable[0]);
            super.A01.registerReceiver(this.A00, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @Override // X.AbstractC06170Sl
    public void A02() {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 24) {
            z = true;
        }
        if (z) {
            try {
                C06390Tk.A00().A02(A03, "Unregistering network callback", new Throwable[0]);
                this.A02.unregisterNetworkCallback(this.A01);
            } catch (IllegalArgumentException | SecurityException e) {
                C06390Tk.A00().A03(A03, "Received exception while unregistering network callback", e);
            }
        } else {
            C06390Tk.A00().A02(A03, "Unregistering broadcast receiver", new Throwable[0]);
            super.A01.unregisterReceiver(this.A00);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r8.isConnected() == false) goto L_0x0010;
     */
    /* renamed from: A05 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C05410Pl A00() {
        /*
            r9 = this;
            android.net.ConnectivityManager r7 = r9.A02
            android.net.NetworkInfo r8 = r7.getActiveNetworkInfo()
            r6 = 1
            if (r8 == 0) goto L_0x0010
            boolean r0 = r8.isConnected()
            r5 = 1
            if (r0 != 0) goto L_0x0011
        L_0x0010:
            r5 = 0
        L_0x0011:
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 0
            r0 = 23
            if (r1 < r0) goto L_0x003d
            android.net.Network r0 = r7.getActiveNetwork()     // Catch: SecurityException -> 0x002b
            android.net.NetworkCapabilities r1 = r7.getNetworkCapabilities(r0)     // Catch: SecurityException -> 0x002b
            if (r1 == 0) goto L_0x003d
            r0 = 16
            boolean r0 = r1.hasCapability(r0)     // Catch: SecurityException -> 0x002b
            if (r0 == 0) goto L_0x003d
            goto L_0x003c
        L_0x002b:
            r0 = move-exception
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = X.C03160Gk.A03
            java.lang.Throwable[] r1 = new java.lang.Throwable[r6]
            r1[r4] = r0
            java.lang.String r0 = "Unable to validate active network"
            r3.A03(r2, r0, r1)
            goto L_0x003d
        L_0x003c:
            r4 = 1
        L_0x003d:
            boolean r1 = r7.isActiveNetworkMetered()
            if (r8 == 0) goto L_0x004f
            boolean r0 = r8.isRoaming()
            if (r0 != 0) goto L_0x004f
        L_0x0049:
            X.0Pl r0 = new X.0Pl
            r0.<init>(r5, r4, r1, r6)
            return r0
        L_0x004f:
            r6 = 0
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03160Gk.A00():X.0Pl");
    }
}
