package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.3Ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63203Ar {
    public static Dialog A00(Activity activity, C14900mE r13, C16170oZ r14, AnonymousClass19M r15, AbstractC116195Um r16, Set set) {
        if (set == null || set.isEmpty()) {
            Log.e("dialog/delete no statuses");
            return null;
        }
        Resources resources = activity.getResources();
        int size = set.size();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1P(A1b, set.size(), 0);
        String quantityString = resources.getQuantityString(R.plurals.delete_status_confirmation, size, A1b);
        Resources resources2 = activity.getResources();
        C004802e A0S = C12980iv.A0S(activity);
        A0S.A0B(true);
        A0S.A0A(AbstractC36671kL.A05(activity, r15, quantityString));
        A0S.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener(activity, resources2, r13, r14, r16, set) { // from class: X.3L1
            public final /* synthetic */ int A00 = 13;
            public final /* synthetic */ Activity A01;
            public final /* synthetic */ Resources A02;
            public final /* synthetic */ C14900mE A03;
            public final /* synthetic */ C16170oZ A04;
            public final /* synthetic */ AbstractC116195Um A05;
            public final /* synthetic */ Set A06;

            {
                this.A01 = r2;
                this.A04 = r5;
                this.A06 = r7;
                this.A03 = r4;
                this.A02 = r3;
                this.A05 = r6;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                Activity activity2 = this.A01;
                int i2 = this.A00;
                C16170oZ r0 = this.A04;
                Set set2 = this.A06;
                C14900mE r7 = this.A03;
                Resources resources3 = this.A02;
                AbstractC116195Um r5 = this.A05;
                C36021jC.A00(activity2, i2);
                r0.A0V(set2, true);
                if (set2.size() == 1) {
                    r7.A07(R.string.status_deleted, 0);
                } else {
                    int size2 = set2.size();
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, set2.size(), 0);
                    r7.A0E(resources3.getQuantityString(R.plurals.statuses_deleted, size2, objArr), 0);
                }
                r5.AOu();
            }
        });
        A0S.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(activity) { // from class: X.4gv
            public final /* synthetic */ int A00 = 13;
            public final /* synthetic */ Activity A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C36021jC.A00(this.A01, this.A00);
            }
        });
        A0S.A08(new DialogInterface.OnCancelListener(activity) { // from class: X.4fC
            public final /* synthetic */ int A00 = 13;
            public final /* synthetic */ Activity A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                C36021jC.A00(this.A01, this.A00);
            }
        });
        return A0S.create();
    }
}
