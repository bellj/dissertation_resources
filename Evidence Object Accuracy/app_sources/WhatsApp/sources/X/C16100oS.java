package X;

/* renamed from: X.0oS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16100oS {
    public final C16210od A00;
    public final C16240og A01;
    public final C16120oU A02;

    public C16100oS(C16210od r1, C16240og r2, C16120oU r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public final void A00(int i, int i2, int i3, int i4, long j, boolean z, boolean z2) {
        AnonymousClass21M r2 = new AnonymousClass21M();
        r2.A07 = Long.valueOf(j);
        r2.A03 = Integer.valueOf(i);
        r2.A06 = Long.valueOf((long) i3);
        r2.A04 = Integer.valueOf(i2);
        if (this.A00.A00) {
            r2.A02 = 1;
        } else {
            r2.A02 = 2;
        }
        boolean z3 = false;
        if (this.A01.A04 == 2) {
            z3 = true;
        }
        r2.A01 = Boolean.valueOf(z3);
        r2.A00 = Boolean.valueOf(z);
        r2.A08 = Long.valueOf((long) i4);
        if (z2) {
            r2.A05 = 0;
        } else {
            r2.A05 = 1;
        }
        this.A02.A07(r2);
    }

    public void A01(int i, long j, long j2, long j3) {
        C16200oc r4 = new C16200oc();
        r4.A03 = Integer.valueOf(i);
        r4.A04 = Long.valueOf(j);
        r4.A06 = Long.valueOf(j2);
        r4.A05 = Long.valueOf(j3);
        if (this.A00.A00) {
            r4.A01 = 1;
        } else {
            r4.A01 = 2;
        }
        boolean z = false;
        if (this.A01.A04 == 2) {
            z = true;
        }
        r4.A00 = Boolean.valueOf(z);
        r4.A02 = 1;
        this.A02.A07(r4);
    }
}
