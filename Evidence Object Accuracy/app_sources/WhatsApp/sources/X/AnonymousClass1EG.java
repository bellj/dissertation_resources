package X;

/* renamed from: X.1EG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EG {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15650ng A02;
    public final C18460sU A03;
    public final C16490p7 A04;
    public final AnonymousClass1EF A05;
    public final AnonymousClass1ED A06;

    public AnonymousClass1EG(AbstractC15710nm r1, C15570nT r2, C15650ng r3, C18460sU r4, C16490p7 r5, AnonymousClass1EF r6, AnonymousClass1ED r7) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A04 = r5;
        this.A05 = r6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        if (r1.equals(r11.A0C()) == false) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.AbstractC15340mz r10, X.AnonymousClass1X9 r11, X.AnonymousClass1X9 r12) {
        /*
            r9 = this;
            X.1rf r3 = r10.A0V
            if (r3 != 0) goto L_0x002e
            r3 = 1
            int r0 = r10.A07
            r0 = r0 & r3
            if (r0 == r3) goto L_0x0025
            X.0nT r2 = r9.A01
            java.util.List r0 = java.util.Collections.singletonList(r12)
            X.1rf r1 = new X.1rf
            r1.<init>(r2, r0)
            X.1rf r0 = r10.A0V
            if (r0 != 0) goto L_0x0026
            r10.A0V = r1
            int r0 = r10.A07
            r3 = r3 | r0
            r10.A07 = r3
            X.0ng r0 = r9.A02
            r0.A0V(r10)
        L_0x0025:
            return
        L_0x0026:
            java.lang.String r1 = "FMessage/setMessageReactions re-assigning messageReactions"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x002e:
            if (r11 == 0) goto L_0x00d0
            monitor-enter(r3)
            com.whatsapp.jid.UserJid r1 = r12.A0C()     // Catch: all -> 0x00cd
            if (r1 != 0) goto L_0x003e
            com.whatsapp.jid.UserJid r0 = r11.A0C()     // Catch: all -> 0x00cd
            if (r0 != 0) goto L_0x0048
            goto L_0x004e
        L_0x003e:
            com.whatsapp.jid.UserJid r0 = r11.A0C()     // Catch: all -> 0x00cd
            boolean r0 = r1.equals(r0)     // Catch: all -> 0x00cd
            if (r0 != 0) goto L_0x004e
        L_0x0048:
            java.lang.String r0 = "Wrong message add on passed into MessageReactionsImpl"
            X.AnonymousClass009.A07(r0)     // Catch: all -> 0x00cd
            goto L_0x00c9
        L_0x004e:
            X.0nT r0 = r3.A00     // Catch: all -> 0x00cb
            r0.A08()     // Catch: all -> 0x00cb
            X.1Ih r7 = r0.A05     // Catch: all -> 0x00cb
            if (r7 != 0) goto L_0x005d
            java.lang.String r0 = "myUserJid is null. User logged out?"
            X.AnonymousClass009.A07(r0)     // Catch: all -> 0x00cb
            goto L_0x00c6
        L_0x005d:
            java.lang.String r0 = r11.A01     // Catch: all -> 0x00cb
            java.lang.String r6 = X.C40481rf.A00(r0)     // Catch: all -> 0x00cb
            java.util.Map r5 = r3.A01     // Catch: all -> 0x00cb
            java.lang.Object r4 = r5.get(r6)     // Catch: all -> 0x00cb
            X.1rg r4 = (X.C40491rg) r4     // Catch: all -> 0x00cb
            if (r4 == 0) goto L_0x00c6
            java.util.TreeSet r2 = r3.A03     // Catch: all -> 0x00cb
            r2.remove(r4)     // Catch: all -> 0x00cb
            X.1IS r0 = r11.A0z     // Catch: all -> 0x00cb
            boolean r0 = r0.A02     // Catch: all -> 0x00cb
            if (r0 != 0) goto L_0x007f
            com.whatsapp.jid.UserJid r7 = r11.A0C()     // Catch: all -> 0x00cb
            X.AnonymousClass009.A05(r7)     // Catch: all -> 0x00cb
        L_0x007f:
            long r0 = r11.A00     // Catch: all -> 0x00cb
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: all -> 0x00cb
            android.util.Pair r8 = new android.util.Pair     // Catch: all -> 0x00cb
            r8.<init>(r7, r0)     // Catch: all -> 0x00cb
            java.util.HashMap r0 = r4.A03     // Catch: all -> 0x00cb
            java.lang.Object r1 = r0.get(r8)     // Catch: all -> 0x00cb
            if (r1 == 0) goto L_0x00a8
            r0.remove(r8)     // Catch: all -> 0x00cb
            java.util.TreeSet r0 = r4.A04     // Catch: all -> 0x00cb
            r0.remove(r1)     // Catch: all -> 0x00cb
            X.0nT r0 = r4.A01     // Catch: all -> 0x00cb
            boolean r0 = r0.A0F(r7)     // Catch: all -> 0x00cb
            if (r0 == 0) goto L_0x00a8
            int r0 = r4.A00     // Catch: all -> 0x00cb
            int r0 = r0 + -1
            r4.A00 = r0     // Catch: all -> 0x00cb
        L_0x00a8:
            java.util.TreeSet r0 = r4.A04     // Catch: all -> 0x00cb
            int r0 = r0.size()     // Catch: all -> 0x00cb
            if (r0 != 0) goto L_0x00c2
            r2.remove(r4)     // Catch: all -> 0x00cb
            r5.remove(r6)     // Catch: all -> 0x00cb
        L_0x00b6:
            java.util.Map r2 = r3.A02     // Catch: all -> 0x00cb
            long r0 = r11.A11     // Catch: all -> 0x00cb
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: all -> 0x00cb
            r2.remove(r0)     // Catch: all -> 0x00cb
            goto L_0x00c6
        L_0x00c2:
            r2.add(r4)     // Catch: all -> 0x00cb
            goto L_0x00b6
        L_0x00c6:
            r3.A04(r12)     // Catch: all -> 0x00cd
        L_0x00c9:
            monitor-exit(r3)
            goto L_0x00d3
        L_0x00cb:
            r0 = move-exception
            throw r0     // Catch: all -> 0x00cd
        L_0x00cd:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x00d0:
            r3.A04(r12)
        L_0x00d3:
            X.0ng r0 = r9.A02
            X.0xi r0 = r0.A0n
            r0.A02(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1EG.A00(X.0mz, X.1X9, X.1X9):void");
    }
}
