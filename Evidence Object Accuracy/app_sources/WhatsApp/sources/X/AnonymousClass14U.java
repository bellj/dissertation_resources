package X;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.14U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14U {
    public final AnonymousClass14T A00;
    public final Map A01 = new ConcurrentHashMap();

    public AnonymousClass14U(AnonymousClass14T r2) {
        this.A00 = r2;
    }
}
