package X;

import android.hardware.Camera;
import java.util.concurrent.Callable;

/* renamed from: X.6KP  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KP implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KP(AnonymousClass661 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass616.A00();
        Camera open = Camera.open(this.A00);
        AnonymousClass616.A00();
        return open;
    }
}
