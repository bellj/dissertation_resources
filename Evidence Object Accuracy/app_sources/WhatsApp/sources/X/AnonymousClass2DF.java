package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.2DF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DF extends AbstractC28131Kt {
    public final int A00;
    public final List A01;
    public final Map A02;

    public AnonymousClass2DF(C34021fS r1, String str, List list, Map map, int i) {
        super(r1, str);
        this.A00 = i;
        this.A01 = list;
        this.A02 = map;
    }
}
