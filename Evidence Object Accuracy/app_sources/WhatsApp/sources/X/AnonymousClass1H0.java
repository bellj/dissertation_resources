package X;

import com.whatsapp.Conversation;
import com.whatsapp.util.Log;

/* renamed from: X.1H0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H0 extends AbstractC250017s {
    public final C250317v A00;
    public final C250217u A01;
    public final C233411h A02;
    public final C14830m7 A03;
    public final C19990v2 A04;

    public AnonymousClass1H0(C250317v r1, C250217u r2, C233411h r3, C14830m7 r4, C19990v2 r5, C233511i r6) {
        super(r6);
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public final void A08(C34371g2 r8) {
        C19990v2 r0 = this.A04;
        AbstractC14640lm r6 = r8.A01;
        if (r0.A06(r6) != null) {
            Log.i("delete-chat-handler/deleteChat deleteMessagesForRange");
            C250317v r5 = this.A00;
            boolean z = r8.A02;
            C34361g1 r3 = r8.A00;
            int A00 = C34361g1.A00(r5.A02.A04(r6, true), r3);
            if (A00 == 2 || A00 == 1) {
                r5.A01.A0J(r6, z, false);
                Conversation.A0K(r5.A00, r6);
                return;
            }
            r5.A01(r3, r6, z, true);
        }
    }
}
