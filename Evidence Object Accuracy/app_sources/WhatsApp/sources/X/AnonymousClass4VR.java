package X;

import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;

/* renamed from: X.4VR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VR {
    public final PackageManager A00;
    public final C15500nM A01;

    public AnonymousClass4VR(C16590pI r2, C15500nM r3) {
        this.A00 = r2.A00.getPackageManager();
        this.A01 = r3;
    }

    public boolean A00(String str) {
        boolean z;
        try {
            z = this.A01.A01(str).A03;
        } catch (PackageManager.NameNotFoundException unused) {
            z = false;
        }
        if (z) {
            PackageManager packageManager = this.A00;
            if (packageManager.checkPermission("com.apple.movetoios.ACCESS", str) == 0) {
                try {
                    PermissionInfo permissionInfo = packageManager.getPermissionInfo("com.apple.movetoios.ACCESS", 0);
                    if ((permissionInfo.protectionLevel & 15) == 2) {
                        return C45051zz.A00(packageManager, permissionInfo.packageName).equals(C45051zz.A00(packageManager, str));
                    }
                } catch (PackageManager.NameNotFoundException unused2) {
                    return false;
                }
            }
        }
        return false;
    }
}
