package X;

/* renamed from: X.3iS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74653iS extends AnonymousClass02K {
    public static final C74653iS A00 = new C74653iS();

    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        C16700pc.A0F(obj, obj2);
        return obj.equals(obj2);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        AnonymousClass4K7 r3 = (AnonymousClass4K7) obj;
        AnonymousClass4K7 r4 = (AnonymousClass4K7) obj2;
        C16700pc.A0F(r3, r4);
        return C12960it.A1V(r3.A00, r4.A00);
    }
}
