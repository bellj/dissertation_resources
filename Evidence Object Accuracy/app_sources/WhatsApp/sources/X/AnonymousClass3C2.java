package X;

/* renamed from: X.3C2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C2 {
    public AnonymousClass3VO A00;
    public AnonymousClass3VN A01;
    public final AnonymousClass12P A02;
    public final C14900mE A03;
    public final AnonymousClass18U A04;
    public final C15570nT A05;
    public final C254719n A06;
    public final C14650lo A07;
    public final C25821Ay A08;
    public final AnonymousClass19S A09;
    public final C19850um A0A;
    public final AnonymousClass19Q A0B;
    public final AnonymousClass19T A0C;
    public final AnonymousClass19X A0D;
    public final C253619c A0E;
    public final C16120oU A0F;
    public final AnonymousClass17R A0G;
    public final AbstractC14440lR A0H;
    public final AnonymousClass18X A0I;

    public AnonymousClass3C2(AnonymousClass12P r2, C14900mE r3, AnonymousClass18U r4, C15570nT r5, C254719n r6, C14650lo r7, C25821Ay r8, AnonymousClass19S r9, C19850um r10, AnonymousClass19Q r11, AnonymousClass19T r12, AnonymousClass19X r13, C253619c r14, C16120oU r15, AnonymousClass17R r16, AbstractC14440lR r17, AnonymousClass18X r18) {
        this.A05 = r5;
        this.A0F = r15;
        this.A0G = r16;
        this.A04 = r4;
        this.A0E = r14;
        this.A0I = r18;
        this.A02 = r2;
        this.A0B = r11;
        this.A06 = r6;
        this.A0D = r13;
        this.A09 = r9;
        this.A0C = r12;
        this.A0A = r10;
        this.A0H = r17;
        this.A03 = r3;
        this.A07 = r7;
        this.A08 = r8;
    }
}
