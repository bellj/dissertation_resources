package X;

import java.lang.ref.WeakReference;

/* renamed from: X.2GQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GQ extends AbstractC16350or {
    public final C14900mE A00;
    public final AbstractC37361mF A01;
    public final WeakReference A02;

    public AnonymousClass2GQ(ActivityC13810kN r2, C14900mE r3, AbstractC37361mF r4) {
        super(r2);
        this.A02 = new WeakReference(r2);
        this.A00 = r3;
        this.A01 = r4;
    }
}
