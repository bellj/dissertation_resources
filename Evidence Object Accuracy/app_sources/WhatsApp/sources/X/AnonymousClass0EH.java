package X;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* renamed from: X.0EH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EH extends AbstractC06480Tu {
    @Override // X.AbstractC06480Tu
    public Object A02(Object obj) {
        if (obj != null) {
            return ((AnonymousClass072) obj).clone();
        }
        return null;
    }

    @Override // X.AbstractC06480Tu
    public Object A03(Object obj) {
        if (obj == null) {
            return null;
        }
        C03060Fy r0 = new C03060Fy();
        r0.A0W((AnonymousClass072) obj);
        return r0;
    }

    @Override // X.AbstractC06480Tu
    public Object A04(Object obj, Object obj2, Object obj3) {
        AnonymousClass072 r3 = (AnonymousClass072) obj;
        AnonymousClass072 r4 = (AnonymousClass072) obj2;
        AnonymousClass072 r5 = (AnonymousClass072) obj3;
        if (r3 == null) {
            r3 = null;
            if (r4 != null) {
                r3 = r4;
            }
        } else if (r4 != null) {
            C03060Fy r1 = new C03060Fy();
            r1.A0W(r3);
            r3 = r1;
            r1.A0W(r4);
            r1.A03 = false;
        }
        if (r5 == null) {
            return r3;
        }
        C03060Fy r0 = new C03060Fy();
        if (r3 != null) {
            r0.A0W(r3);
        }
        r0.A0W(r5);
        return r0;
    }

    @Override // X.AbstractC06480Tu
    public Object A05(Object obj, Object obj2, Object obj3) {
        C03060Fy r0 = new C03060Fy();
        if (obj != null) {
            r0.A0W((AnonymousClass072) obj);
        }
        r0.A0W((AnonymousClass072) obj2);
        return r0;
    }

    @Override // X.AbstractC06480Tu
    public void A06(Rect rect, Object obj) {
        ((AnonymousClass072) obj).A0M(new C03050Fx(rect, this));
    }

    @Override // X.AbstractC06480Tu
    public void A07(View view, Object obj) {
        ((AnonymousClass072) obj).A06(view);
    }

    @Override // X.AbstractC06480Tu
    public void A08(View view, Object obj) {
        if (view != null) {
            Rect rect = new Rect();
            AbstractC06480Tu.A00(view, rect);
            ((AnonymousClass072) obj).A0M(new C03040Fw(rect, this));
        }
    }

    @Override // X.AbstractC06480Tu
    public void A09(View view, Object obj, ArrayList arrayList) {
        ((AnonymousClass072) obj).A08(new AnonymousClass0ZN(view, this, arrayList));
    }

    @Override // X.AbstractC06480Tu
    public void A0A(View view, Object obj, ArrayList arrayList) {
        AnonymousClass072 r6 = (AnonymousClass072) obj;
        ArrayList arrayList2 = r6.A0H;
        arrayList2.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            AbstractC06480Tu.A01((View) arrayList.get(i), arrayList2);
        }
        arrayList2.add(view);
        arrayList.add(view);
        A0E(r6, arrayList);
    }

    @Override // X.AbstractC06480Tu
    public void A0B(ViewGroup viewGroup, Object obj) {
        AnonymousClass073.A01(viewGroup, (AnonymousClass072) obj);
    }

    @Override // X.AbstractC06480Tu
    public void A0D(Object obj, Object obj2, Object obj3, Object obj4, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        ((AnonymousClass072) obj).A08(new AnonymousClass0ZO(this, obj2, obj4, arrayList, arrayList3));
    }

    @Override // X.AbstractC06480Tu
    public void A0E(Object obj, ArrayList arrayList) {
        Object obj2;
        AnonymousClass072 r5 = (AnonymousClass072) obj;
        if (r5 != null) {
            int i = 0;
            if (r5 instanceof C03060Fy) {
                C03060Fy r52 = (C03060Fy) r5;
                int size = r52.A02.size();
                while (i < size) {
                    if (i >= 0) {
                        ArrayList arrayList2 = r52.A02;
                        if (i < arrayList2.size()) {
                            obj2 = arrayList2.get(i);
                            A0E(obj2, arrayList);
                            i++;
                        }
                    }
                    obj2 = null;
                    A0E(obj2, arrayList);
                    i++;
                }
                return;
            }
            ArrayList arrayList3 = r5.A0G;
            if (arrayList3 == null || arrayList3.isEmpty()) {
                ArrayList arrayList4 = r5.A0H;
                if (arrayList4 == null || arrayList4.isEmpty()) {
                    int size2 = arrayList.size();
                    while (i < size2) {
                        r5.A06((View) arrayList.get(i));
                        i++;
                    }
                }
            }
        }
    }

    @Override // X.AbstractC06480Tu
    public void A0F(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        AnonymousClass072 r2 = (AnonymousClass072) obj;
        if (r2 != null) {
            ArrayList arrayList3 = r2.A0H;
            arrayList3.clear();
            arrayList3.addAll(arrayList2);
            A0H(r2, arrayList, arrayList2);
        }
    }

    @Override // X.AbstractC06480Tu
    public boolean A0G(Object obj) {
        return obj instanceof AnonymousClass072;
    }

    public void A0H(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        Object obj2;
        AnonymousClass072 r5 = (AnonymousClass072) obj;
        int i = 0;
        if (r5 instanceof C03060Fy) {
            C03060Fy r52 = (C03060Fy) r5;
            int size = r52.A02.size();
            while (i < size) {
                if (i >= 0) {
                    ArrayList arrayList3 = r52.A02;
                    if (i < arrayList3.size()) {
                        obj2 = arrayList3.get(i);
                        A0H(obj2, arrayList, arrayList2);
                        i++;
                    }
                }
                obj2 = null;
                A0H(obj2, arrayList, arrayList2);
                i++;
            }
            return;
        }
        ArrayList arrayList4 = r5.A0G;
        if (arrayList4 == null || arrayList4.isEmpty()) {
            ArrayList arrayList5 = r5.A0H;
            if (arrayList5.size() == arrayList.size() && arrayList5.containsAll(arrayList)) {
                if (arrayList2 != null) {
                    int size2 = arrayList2.size();
                    while (i < size2) {
                        r5.A06((View) arrayList2.get(i));
                        i++;
                    }
                }
                int size3 = arrayList.size();
                while (true) {
                    size3--;
                    if (size3 >= 0) {
                        r5.A07((View) arrayList.get(size3));
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
