package X;

import android.content.Context;

/* renamed from: X.34m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C621134m extends AnonymousClass34n {
    public C621134m(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
    }

    @Override // X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ CharSequence A02(C15370n3 r8, AbstractC15340mz r9) {
        return AnonymousClass3J0.A01(getContext(), this.A08, this.A0A, this.A0F, r8, r9.A0z.A02);
    }
}
