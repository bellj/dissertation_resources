package X;

import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.1nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38241nl {
    public static void A00(View view, Window window, AnonymousClass01d r5) {
        if (Build.MANUFACTURER.equals("OnePlus")) {
            String str = Build.MODEL;
            if ((str.equals("ONEPLUS A6000") || str.equals("ONEPLUS A6003")) && (window.getAttributes().flags & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) != 0 && r5.A0S("com.oneplus.screen.cameranotch")) {
                view.setPadding(0, (int) TypedValue.applyDimension(5, 5.0f, view.getContext().getResources().getDisplayMetrics()), 0, 0);
            }
        }
    }

    public static boolean A01() {
        int i = Build.VERSION.SDK_INT;
        if ((i != 26 && i != 27) || !Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
            return false;
        }
        String str = Build.MODEL;
        return str.startsWith("SM-G570") || str.startsWith("SM-J260") || str.startsWith("SM-G935") || str.startsWith("SM-G930") || str.startsWith("SM-A520") || str.startsWith("SM-A720") || str.startsWith("SM-A260") || str.startsWith("SM-J400") || str.startsWith("SM-J600") || str.startsWith("SM-G950");
    }

    public static boolean A02() {
        String str = Build.MANUFACTURER;
        if (str.equalsIgnoreCase("vestel") && Build.MODEL.equalsIgnoreCase("vsp250s")) {
            return true;
        }
        if (!str.equalsIgnoreCase("asus")) {
            return false;
        }
        String str2 = Build.MODEL;
        return str2.equalsIgnoreCase("ASUS_Z00AD") || str2.equalsIgnoreCase("asus_x00ada") || str2.equalsIgnoreCase("asus_x00adc") || str2.equalsIgnoreCase("asus_t00j") || str2.equalsIgnoreCase("asus_x00ad") || str2.equalsIgnoreCase("asus_x014d") || str2.equalsIgnoreCase("asus_z008d") || str2.equalsIgnoreCase("asus_z00ldd") || str2.equalsIgnoreCase("zb500kl");
    }

    public static boolean A03() {
        return Build.VERSION.SDK_INT == 15;
    }

    public static boolean A04() {
        return Build.VERSION.SDK_INT >= 28;
    }

    public static boolean A05() {
        return Build.VERSION.SDK_INT == 23;
    }

    public static boolean A06() {
        String str;
        String str2;
        if (Build.VERSION.SDK_INT <= 23) {
            return true;
        }
        String str3 = Build.MANUFACTURER;
        if (str3.equalsIgnoreCase("oppo")) {
            str = Build.MODEL;
            str2 = "A53";
        } else if (!str3.equalsIgnoreCase("GiONEE")) {
            return false;
        } else {
            str = Build.MODEL;
            str2 = "GN5001S";
        }
        return str.equalsIgnoreCase(str2);
    }

    public static boolean A07() {
        return Build.MODEL.equals("Nokia 3.1 Plus") && Build.VERSION.SDK_INT == 28;
    }

    public static boolean A08() {
        String str = Build.MANUFACTURER;
        if (str.equals("Huawei") && Build.MODEL.equals("Nexus 6P")) {
            return true;
        }
        if (str.equals("Google")) {
            String str2 = Build.MODEL;
            if (str2.equals("Pixel 2") || str2.equals("Pixel 2 XL")) {
                return true;
            }
        }
        return str.equals("Xiaomi") && Build.VERSION.SDK_INT == 27;
    }

    public static boolean A09() {
        return Build.VERSION.SDK_INT < 20;
    }

    public static boolean A0A() {
        return Build.VERSION.SDK_INT == 22 && Build.MANUFACTURER.equalsIgnoreCase("LAVA");
    }

    public static boolean A0B(String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass009.A07("permission issue with UPDATE_APP_OPS_STATS should only occur in Android 4.3 or earlier");
        }
        return str.contains("android.permission.UPDATE_APP_OPS_STATS");
    }
}
