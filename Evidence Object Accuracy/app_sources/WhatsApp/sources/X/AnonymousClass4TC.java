package X;

/* renamed from: X.4TC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TC {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AbstractC35611iN A03;
    public final String A04;
    public final String A05;

    public AnonymousClass4TC(AbstractC35611iN r1, String str, String str2, int i, int i2, int i3) {
        this.A02 = i;
        this.A01 = i2;
        this.A04 = str;
        this.A05 = str2;
        this.A03 = r1;
        this.A00 = i3;
    }
}
