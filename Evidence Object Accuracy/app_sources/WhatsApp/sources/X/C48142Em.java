package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2Em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48142Em implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99624kb();
    public long A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C48142Em(long j) {
        this.A00 = j;
    }

    public C48142Em(Parcel parcel) {
        this.A00 = parcel.readLong();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
    }
}
