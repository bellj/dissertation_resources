package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.03P  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03P extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.LOCALE_CHANGED".equals(intent.getAction())) {
            AnonymousClass03N.A01();
        }
    }
}
