package X;

import com.whatsapp.util.Log;

/* renamed from: X.68W  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68W implements AbstractC136436Mn {
    @Override // X.AbstractC136436Mn
    public void AQw() {
        C117305Zk.A1N("IndiaUpiDeviceBindActivity", "onGetChallengeFailure");
    }

    @Override // X.AbstractC136436Mn
    public void AR3(C452120p r3, boolean z) {
        StringBuilder A0k = C12960it.A0k("onToken success: ");
        A0k.append(z);
        Log.i(C30931Zj.A01("IndiaUpiDeviceBindActivity", C12960it.A0Z(r3, " error: ", A0k)));
    }

    @Override // X.AbstractC136436Mn
    public void AUn(boolean z) {
        StringBuilder A0k = C12960it.A0k("/onRegisterApp registered: ");
        A0k.append(z);
        Log.i(C30931Zj.A01("IndiaUpiDeviceBindActivity", A0k.toString()));
    }
}
