package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.4XG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XG {
    public final int A00 = 50;
    public final C26211Cl A01;
    public final Comparator A02 = new IDxComparatorShape3S0000000_2_I1(12);
    public volatile List A03;

    public AnonymousClass4XG(C26211Cl r4) {
        this.A01 = r4;
    }

    public List A00() {
        List list;
        synchronized (this) {
            if (this.A03 == null) {
                A01();
            }
            list = this.A03;
        }
        return list;
    }

    public void A01() {
        synchronized (this) {
            if (this.A03 == null) {
                this.A03 = this.A01.A00();
            }
        }
    }
}
