package X;

import android.app.Activity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1;
import com.whatsapp.R;
import java.text.DateFormat;
import java.util.Date;

/* renamed from: X.2oc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC58312oc extends AnonymousClass27U {
    public final /* synthetic */ AnonymousClass12P A00;
    public final /* synthetic */ C20640w5 A01;
    public final /* synthetic */ C21740xu A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DialogC58312oc(Activity activity, AnonymousClass12P r8, C20640w5 r9, C21740xu r10, AnonymousClass01d r11, C14830m7 r12, AnonymousClass018 r13) {
        super(activity, r11, r12, r13, R.layout.software_too_old);
        this.A01 = r9;
        this.A00 = r8;
        this.A02 = r10;
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        int i;
        Object[] objArr;
        super.onCreate(bundle);
        DateFormat dateInstance = DateFormat.getDateInstance(2, C12970iu.A14(this.A04));
        Activity activity = super.A01;
        Date A01 = this.A01.A01();
        if (C38241nl.A03()) {
            i = R.string.software_deprecated_with_date;
            objArr = new Object[]{activity.getString(R.string.localized_app_name), dateInstance.format(A01)};
        } else {
            i = R.string.software_expired_with_date;
            objArr = new Object[]{activity.getString(R.string.localized_app_name), dateInstance.format(A01), activity.getString(R.string.button_download)};
        }
        Spanned A00 = AnonymousClass1US.A00(activity, objArr, i);
        if (C38241nl.A03()) {
            ((TextView) findViewById(R.id.software_too_old_title)).setText(R.string.software_deprecated_title);
        }
        ((TextView) findViewById(R.id.software_too_old)).setText(A00);
        SpannableString valueOf = SpannableString.valueOf(AnonymousClass1US.A00(activity, new Object[]{dateInstance.format(new Date()), activity.getString(R.string.localized_app_name)}, R.string.software_expired_current_date));
        URLSpan[] uRLSpanArr = (URLSpan[]) valueOf.getSpans(0, valueOf.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if ("date-settings".equals(uRLSpan.getURL())) {
                    int spanStart = valueOf.getSpanStart(uRLSpan);
                    int spanEnd = valueOf.getSpanEnd(uRLSpan);
                    int spanFlags = valueOf.getSpanFlags(uRLSpan);
                    valueOf.removeSpan(uRLSpan);
                    valueOf.setSpan(new C52182aO(this), spanStart, spanEnd, spanFlags);
                }
            }
        }
        TextView textView = (TextView) findViewById(R.id.current_date);
        textView.setText(valueOf);
        textView.setMovementMethod(new LinkMovementMethod());
        if (C38241nl.A03()) {
            ((TextView) findViewById(R.id.download)).setText(R.string.learn_more);
            ((ImageView) findViewById(R.id.update_whatsapp)).setImageResource(R.drawable.splash_logo);
        }
        ViewOnClickCListenerShape2S0200000_I1 viewOnClickCListenerShape2S0200000_I1 = new ViewOnClickCListenerShape2S0200000_I1(this, 1, this.A02);
        findViewById(R.id.download).setOnClickListener(viewOnClickCListenerShape2S0200000_I1);
        findViewById(R.id.update_whatsapp).setOnClickListener(viewOnClickCListenerShape2S0200000_I1);
    }
}
