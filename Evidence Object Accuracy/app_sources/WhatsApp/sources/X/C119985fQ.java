package X;

/* renamed from: X.5fQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119985fQ extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;

    public C119985fQ() {
        super(3398, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(7, this.A05);
        r3.Abe(8, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDisappearingMessageChatPicker {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatsSelected", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dmChatPickerEntryPoint", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dmChatPickerEventName", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ephemeralityDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupChatsSelected", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "newlyEphemeralChats", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalChatsInChatPicker", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
