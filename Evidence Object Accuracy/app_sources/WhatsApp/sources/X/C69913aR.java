package X;

/* renamed from: X.3aR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69913aR implements AnonymousClass2BZ {
    public final /* synthetic */ C28921Pn A00;

    @Override // X.AnonymousClass2BZ
    public void AQT(AnonymousClass2Ba r1) {
    }

    public C69913aR(C28921Pn r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2BZ
    public void ANU(AnonymousClass2Ba r5, long j) {
        long j2;
        C28921Pn r3 = this.A00;
        r3.A0h.A05(1);
        synchronized (r5) {
            j2 = r5.A06;
        }
        r3.A09(j2);
        r3.A0Z.A0B(j2);
        r3.A03.A09(j2, j);
        C14430lQ r0 = r3.A01;
        if (r0 != null) {
            r0.A0A = j2;
        }
    }

    @Override // X.AnonymousClass2BZ
    public void APR(int i) {
        if (i == 1) {
            this.A00.A03.A04();
        } else if (i == 2) {
            this.A00.A03.A07();
        } else if (i == 3) {
            this.A00.A03.A08();
        }
    }

    @Override // X.AnonymousClass2BZ
    public void APS(AnonymousClass2Ba r3) {
        if (r3.A00() == 4) {
            C43161wW r1 = this.A00.A03;
            if (r1.A0A == null) {
                r1.A04();
            }
            if (r1.A0G == null) {
                r1.A08();
            }
        }
    }

    @Override // X.AnonymousClass2BZ
    public void AV6() {
        this.A00.A08();
    }
}
