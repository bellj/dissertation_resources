package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71903dh extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AbstractActivityC60272wQ this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71903dh(AbstractActivityC60272wQ r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AbstractActivityC60272wQ r6 = this.this$0;
        UserJid A2e = r6.A2e();
        UserJid A2e2 = this.this$0.A2e();
        AbstractActivityC60272wQ r0 = this.this$0;
        AbstractC14440lR r3 = ((ActivityC13830kP) r0).A05;
        C21770xx r2 = r0.A01;
        if (r2 != null) {
            AnonymousClass19Q r02 = r0.A02;
            if (r02 != null) {
                return new AnonymousClass02A(new C105594uJ(new AnonymousClass3EX(r2, r02, A2e2, r3), A2e), r6).A00(C53872fS.class);
            }
            throw C16700pc.A06("catalogAnalyticManager");
        }
        throw C16700pc.A06("cartItemStore");
    }
}
