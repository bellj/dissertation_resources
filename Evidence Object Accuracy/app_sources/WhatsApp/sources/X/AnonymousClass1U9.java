package X;

/* renamed from: X.1U9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1U9 {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    public static final AnonymousClass1UE A02;
    @Deprecated
    public static final AnonymousClass1UA A03 = new AnonymousClass3T5();

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77633nb r2 = new C77633nb();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "LocationServices.API");
    }
}
