package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.components.Button;
import com.whatsapp.wds.components.button.WDSButton;

/* renamed from: X.2dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53332dt extends AnonymousClass0BS implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53332dt(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v6, types: [com.whatsapp.wds.components.button.WDSButton] */
    public void A00() {
        Button button;
        AnonymousClass01J A00;
        AbstractC629139c r2;
        if (this instanceof AbstractC629139c) {
            AbstractC629139c r22 = (AbstractC629139c) this;
            if (r22 instanceof Button) {
                Button button2 = (Button) r22;
                if (!button2.A05) {
                    button2.A05 = true;
                    button = button2;
                } else {
                    return;
                }
            } else if (!r22.A00) {
                r22.A00 = true;
                button = r22;
            } else {
                return;
            }
            A00 = AnonymousClass2P6.A00(button.generatedComponent());
            r2 = button;
        } else if (!this.A01) {
            this.A01 = true;
            A00 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            r2 = (WDSButton) this;
        } else {
            return;
        }
        r2.A04 = C12960it.A0S(A00);
        r2.A03 = C12960it.A0R(A00);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
