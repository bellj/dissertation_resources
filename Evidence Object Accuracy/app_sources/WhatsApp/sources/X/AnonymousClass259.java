package X;

/* renamed from: X.259  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass259 {
    public static final Class A00;

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            cls = null;
        }
        A00 = cls;
    }

    public static AnonymousClass254 A00() {
        Class cls = A00;
        if (cls != null) {
            try {
                return (AnonymousClass254) cls.getMethod("getEmptyRegistry", new Class[0]).invoke(null, new Object[0]);
            } catch (Exception unused) {
            }
        }
        return AnonymousClass254.A01;
    }
}
