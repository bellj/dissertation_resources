package X;

import android.os.StrictMode;
import java.util.Map;

/* renamed from: X.1QG  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1QG {
    public int A00(StrictMode.ThreadPolicy threadPolicy, String str, int i) {
        Object obj;
        int A05;
        if (!(this instanceof AnonymousClass1QF)) {
            return ((AnonymousClass1QP) this).A02.A00(threadPolicy, str, i);
        }
        AnonymousClass1QF r3 = (AnonymousClass1QF) this;
        if (!(r3 instanceof AnonymousClass1QJ)) {
            return r3.A05(threadPolicy, r3.A01, str, i);
        }
        AnonymousClass1QJ r32 = (AnonymousClass1QJ) r3;
        Map map = r32.A03;
        synchronized (map) {
            obj = map.get(str);
            if (obj == null) {
                obj = new Object();
                map.put(str, obj);
            }
        }
        synchronized (obj) {
            A05 = r32.A05(threadPolicy, ((AnonymousClass1QF) r32).A01, str, i);
        }
        return A05;
    }

    public void A04(int i) {
        if (this instanceof AnonymousClass1QP) {
            ((AnonymousClass1QP) this).A02.A04(i);
        }
    }
}
