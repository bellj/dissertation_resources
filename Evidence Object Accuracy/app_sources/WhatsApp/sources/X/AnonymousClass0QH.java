package X;

import android.opengl.EGL14;
import android.opengl.EGLSurface;
import android.util.Log;
import android.view.Surface;

/* renamed from: X.0QH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QH {
    public int A00 = -1;
    public int A01 = -1;
    public EGLSurface A02 = EGL14.EGL_NO_SURFACE;
    public Surface A03;
    public C06450Tr A04;
    public boolean A05;

    public AnonymousClass0QH(C06450Tr r2) {
        this.A04 = r2;
    }

    public int A00() {
        int i = this.A00;
        return i < 0 ? this.A04.A01(this.A02, 12374) : i;
    }

    public int A01() {
        int i = this.A01;
        return i < 0 ? this.A04.A01(this.A02, 12375) : i;
    }

    public void A02() {
        this.A04.A04(this.A02);
    }

    public void A03() {
        this.A04.A05(this.A02);
        this.A02 = EGL14.EGL_NO_SURFACE;
        this.A00 = -1;
        this.A01 = -1;
    }

    public void A04() {
        if (!this.A04.A06(this.A02)) {
            Log.d("Grafika", "WARNING: swapBuffers() failed");
        }
    }

    public void A05(Object obj) {
        if (this.A02 == EGL14.EGL_NO_SURFACE) {
            this.A02 = this.A04.A02(obj);
            return;
        }
        throw new IllegalStateException("surface already created");
    }
}
