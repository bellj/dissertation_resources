package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.0QK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QK {
    public List A00;
    public int[] A01;

    public C06840Vh A00(int i) {
        C06840Vh r1;
        List list = this.A00;
        if (list != null) {
            int size = list.size();
            do {
                size--;
                if (size >= 0) {
                    r1 = (C06840Vh) this.A00.get(size);
                }
            } while (r1.A01 != i);
            return r1;
        }
        return null;
    }

    public C06840Vh A01(int i, int i2, int i3) {
        List list = this.A00;
        if (list != null) {
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                C06840Vh r1 = (C06840Vh) this.A00.get(i4);
                int i5 = r1.A01;
                if (i5 >= i2) {
                    break;
                } else if (i5 >= i && (i3 == 0 || r1.A00 == i3 || r1.A02)) {
                    return r1;
                }
            }
        }
        return null;
    }

    public void A02(int i) {
        int[] iArr = this.A01;
        if (iArr == null) {
            int[] iArr2 = new int[Math.max(i, 10) + 1];
            this.A01 = iArr2;
            Arrays.fill(iArr2, -1);
            return;
        }
        int length = iArr.length;
        if (i >= length) {
            while (length <= i) {
                length <<= 1;
            }
            int[] iArr3 = new int[length];
            this.A01 = iArr3;
            int length2 = iArr.length;
            System.arraycopy(iArr, 0, iArr3, 0, length2);
            int[] iArr4 = this.A01;
            Arrays.fill(iArr4, length2, iArr4.length, -1);
        }
    }

    public void A03(int i) {
        List list = this.A00;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                } else if (((C06840Vh) this.A00.get(size)).A01 >= i) {
                    this.A00.remove(size);
                }
            }
        }
        A04(i);
    }

    public void A04(int i) {
        int[] iArr;
        int length;
        int[] iArr2 = this.A01;
        if (iArr2 != null && i < iArr2.length) {
            if (this.A00 != null) {
                C06840Vh A00 = A00(i);
                if (A00 != null) {
                    this.A00.remove(A00);
                }
                int size = this.A00.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    } else if (((C06840Vh) this.A00.get(i2)).A01 < i) {
                        i2++;
                    } else if (i2 != -1) {
                        this.A00.remove(i2);
                        int i3 = ((C06840Vh) this.A00.get(i2)).A01;
                        if (i3 != -1) {
                            iArr = this.A01;
                            length = i3 + 1;
                        }
                    }
                }
            }
            iArr = this.A01;
            length = iArr.length;
            Arrays.fill(iArr, i, length, -1);
        }
    }

    public void A05(int i, int i2) {
        int[] iArr = this.A01;
        if (iArr != null && i < iArr.length) {
            int i3 = i + i2;
            A02(i3);
            int[] iArr2 = this.A01;
            System.arraycopy(iArr2, i, iArr2, i3, (iArr2.length - i) - i2);
            Arrays.fill(this.A01, i, i3, -1);
            List list = this.A00;
            if (list != null) {
                int size = list.size();
                while (true) {
                    size--;
                    if (size >= 0) {
                        C06840Vh r1 = (C06840Vh) this.A00.get(size);
                        int i4 = r1.A01;
                        if (i4 >= i) {
                            r1.A01 = i4 + i2;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void A06(int i, int i2) {
        int[] iArr = this.A01;
        if (iArr != null && i < iArr.length) {
            int i3 = i + i2;
            A02(i3);
            int[] iArr2 = this.A01;
            System.arraycopy(iArr2, i3, iArr2, i, (iArr2.length - i) - i2);
            int[] iArr3 = this.A01;
            int length = iArr3.length;
            Arrays.fill(iArr3, length - i2, length, -1);
            List list = this.A00;
            if (list != null) {
                int size = list.size();
                while (true) {
                    size--;
                    if (size >= 0) {
                        C06840Vh r1 = (C06840Vh) this.A00.get(size);
                        int i4 = r1.A01;
                        if (i4 >= i) {
                            if (i4 < i3) {
                                this.A00.remove(size);
                            } else {
                                r1.A01 = i4 - i2;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
