package X;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/* renamed from: X.1TM  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1TM implements AnonymousClass1TN {
    public byte[] A02(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        A00(byteArrayOutputStream, str);
        return byteArrayOutputStream.toByteArray();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1TN)) {
            return false;
        }
        return Aer().A04(((AnonymousClass1TN) obj).Aer());
    }

    public int hashCode() {
        return Aer().hashCode();
    }

    public void A00(OutputStream outputStream, String str) {
        if (!(this instanceof AnonymousClass1TL)) {
            (str.equals("DER") ? new AnonymousClass5N8(outputStream) : str.equals("DL") ? new AnonymousClass5N7(outputStream) : new AnonymousClass1TP(outputStream)).A04(Aer(), true);
            return;
        }
        (str.equals("DER") ? new AnonymousClass5N8(outputStream) : str.equals("DL") ? new AnonymousClass5N7(outputStream) : new AnonymousClass1TP(outputStream)).A04((AnonymousClass1TL) this, true);
    }

    public byte[] A01() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (!(this instanceof AnonymousClass1TL)) {
            new AnonymousClass1TP(byteArrayOutputStream).A04(Aer(), true);
        } else {
            new AnonymousClass1TP(byteArrayOutputStream).A04((AnonymousClass1TL) this, true);
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return !(this instanceof C72343eR) ? (AnonymousClass1TL) this : ((C72343eR) this).A00;
    }
}
