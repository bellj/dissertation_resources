package X;

import android.hardware.Camera;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.63C  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63C implements Camera.PictureCallback {
    public final /* synthetic */ CallableC135866Kc A00;
    public final /* synthetic */ C129755yF A01;

    public AnonymousClass63C(CallableC135866Kc r1, C129755yF r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.hardware.Camera.PictureCallback
    public void onPictureTaken(byte[] bArr, Camera camera) {
        C129755yF r4 = this.A01;
        r4.A01(AnonymousClass60J.A0d, bArr);
        CallableC135866Kc r0 = this.A00;
        AnonymousClass661 r3 = r0.A01;
        AnonymousClass661.A01(r0.A00, r3, r0.A02, r4, null);
        ((CountDownLatch) r3.A0N.A00.get()).countDown();
    }
}
