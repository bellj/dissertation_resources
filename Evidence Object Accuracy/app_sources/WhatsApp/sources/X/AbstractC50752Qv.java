package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

/* renamed from: X.2Qv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC50752Qv extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
    public float A00;
    public float A01;
    public boolean A02;
    public final /* synthetic */ C50632Qh A03;

    public /* synthetic */ AbstractC50752Qv(C50632Qh r1) {
        this.A03 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2R6 r2 = this.A03.A0H;
        r2.A00(this.A00, r2.A01);
        this.A02 = false;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float f;
        float f2;
        float f3;
        if (!this.A02) {
            this.A01 = this.A03.A0H.A02;
            if (!(this instanceof C50772Qx)) {
                if (this instanceof AnonymousClass2Qu) {
                    C50632Qh r0 = ((AnonymousClass2Qu) this).A00;
                    f2 = r0.A00;
                    f3 = r0.A03;
                } else if (!(this instanceof C50762Qw)) {
                    f = 0.0f;
                } else {
                    C50632Qh r02 = ((C50762Qw) this).A00;
                    f2 = r02.A00;
                    f3 = r02.A01;
                }
                f = f2 + f3;
            } else {
                f = ((C50772Qx) this).A00.A00;
            }
            this.A00 = f;
            this.A02 = true;
        }
        AnonymousClass2R6 r3 = this.A03.A0H;
        float f4 = this.A01;
        r3.A00(f4 + ((this.A00 - f4) * valueAnimator.getAnimatedFraction()), r3.A01);
    }
}
