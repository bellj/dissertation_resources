package X;

/* renamed from: X.0Pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05430Pn {
    public final AnonymousClass03T A00;
    public final AnonymousClass03T A01;
    public final AnonymousClass03T A02;
    public final AnonymousClass03T A03;
    public final C06870Vk A04;

    public C05430Pn(AnonymousClass03T r1, AnonymousClass03T r2, AnonymousClass03T r3, AnonymousClass03T r4, C06870Vk r5) {
        this.A02 = r1;
        this.A03 = r2;
        this.A00 = r3;
        this.A01 = r4;
        this.A04 = r5;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C05430Pn)) {
            return false;
        }
        C05430Pn r4 = (C05430Pn) obj;
        if (!this.A02.equals(r4.A02) || !this.A03.equals(r4.A03) || !this.A00.equals(r4.A00) || !this.A01.equals(r4.A01) || !this.A04.equals(r4.A04)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((((((527 + this.A02.hashCode()) * 31) + this.A03.hashCode()) * 31) + this.A00.hashCode()) * 31) + this.A01.hashCode()) * 31) + this.A04.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VisibleRegion");
        sb.append("{nearLeft=");
        sb.append(this.A02);
        sb.append(", nearRight=");
        sb.append(this.A03);
        sb.append(", farLeft=");
        sb.append(this.A00);
        sb.append(", farRight=");
        sb.append(this.A01);
        sb.append(", latLngBounds=");
        sb.append(this.A04);
        sb.append("}");
        return sb.toString();
    }
}
