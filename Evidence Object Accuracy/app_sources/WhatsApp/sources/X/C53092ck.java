package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar$Tab;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.R;

/* renamed from: X.2ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53092ck extends LinearLayout {
    public int A00 = 2;
    public Drawable A01;
    public View A02;
    public ImageView A03;
    public ImageView A04;
    public TextView A05;
    public TextView A06;
    public AnonymousClass3FN A07;
    public final /* synthetic */ TabLayout A08;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53092ck(Context context, TabLayout tabLayout) {
        super(context);
        this.A08 = tabLayout;
        A01(context);
        AnonymousClass028.A0e(this, tabLayout.A0A, tabLayout.A0B, tabLayout.A09, tabLayout.A08);
        setGravity(17);
        setOrientation(!tabLayout.A0Q ? 1 : 0);
        setClickable(true);
        AnonymousClass028.A0i(this, AnonymousClass0S7.A00(getContext()));
    }

    public final void A00() {
        View view;
        AnonymousClass3FN r3 = this.A07;
        ImageView imageView = null;
        if (r3 == null || (view = r3.A01) == null) {
            View view2 = this.A02;
            if (view2 != null) {
                removeView(view2);
                this.A02 = null;
            }
            this.A05 = null;
        } else {
            ViewParent parent = view.getParent();
            if (parent != this) {
                if (parent != null) {
                    ((ViewGroup) parent).removeView(view);
                }
                addView(view);
            }
            this.A02 = view;
            TextView textView = this.A06;
            if (textView != null) {
                textView.setVisibility(8);
            }
            ImageView imageView2 = this.A04;
            if (imageView2 != null) {
                imageView2.setVisibility(8);
                this.A04.setImageDrawable(null);
            }
            TextView A0J = C12960it.A0J(view, 16908308);
            this.A05 = A0J;
            if (A0J != null) {
                this.A00 = A0J.getMaxLines();
            }
            imageView = C12970iu.A0L(view, 16908294);
        }
        this.A03 = imageView;
        boolean z = false;
        if (this.A02 == null) {
            if (this.A04 == null) {
                ImageView imageView3 = (ImageView) C12960it.A0E(this).inflate(R.layout.design_layout_tab_icon, (ViewGroup) this, false);
                addView(imageView3, 0);
                this.A04 = imageView3;
            }
            if (this.A06 == null) {
                TextView textView2 = (TextView) C12960it.A0E(this).inflate(R.layout.design_layout_tab_text, (ViewGroup) this, false);
                addView(textView2);
                this.A06 = textView2;
                this.A00 = textView2.getMaxLines();
            }
            TextView textView3 = this.A06;
            TabLayout tabLayout = this.A08;
            AnonymousClass04D.A08(textView3, tabLayout.A0C);
            ColorStateList colorStateList = tabLayout.A0G;
            if (colorStateList != null) {
                this.A06.setTextColor(colorStateList);
            }
            A02(this.A04, this.A06);
        } else {
            TextView textView4 = this.A05;
            if (!(textView4 == null && imageView == null)) {
                A02(imageView, textView4);
            }
        }
        if (r3 != null) {
            if (!TextUtils.isEmpty(r3.A04)) {
                setContentDescription(r3.A04);
            }
            TabLayout tabLayout2 = r3.A03;
            if (tabLayout2 == null) {
                throw C12970iu.A0f("Tab not attached to a TabLayout");
            } else if (tabLayout2.getSelectedTabPosition() == r3.A00) {
                z = true;
            }
        }
        setSelected(z);
    }

    public final void A01(Context context) {
        GradientDrawable gradientDrawable;
        TabLayout tabLayout = this.A08;
        int i = tabLayout.A0X;
        GradientDrawable gradientDrawable2 = null;
        if (i != 0) {
            Drawable A01 = C015007d.A01(context, i);
            this.A01 = A01;
            if (A01 != null && A01.isStateful()) {
                this.A01.setState(getDrawableState());
            }
        } else {
            this.A01 = null;
        }
        GradientDrawable gradientDrawable3 = new GradientDrawable();
        gradientDrawable3.setColor(0);
        RippleDrawable rippleDrawable = gradientDrawable3;
        if (tabLayout.A0F != null) {
            GradientDrawable gradientDrawable4 = new GradientDrawable();
            gradientDrawable4.setCornerRadius(1.0E-5f);
            gradientDrawable4.setColor(-1);
            ColorStateList A02 = AnonymousClass2RB.A02(tabLayout.A0F);
            if (Build.VERSION.SDK_INT >= 21) {
                if (tabLayout.A0T) {
                    gradientDrawable = null;
                } else {
                    gradientDrawable2 = gradientDrawable4;
                    gradientDrawable = gradientDrawable3;
                }
                rippleDrawable = new RippleDrawable(A02, gradientDrawable, gradientDrawable2);
            } else {
                Drawable A03 = C015607k.A03(gradientDrawable4);
                C015607k.A04(A02, A03);
                Drawable[] drawableArr = new Drawable[2];
                C12990iw.A1P(gradientDrawable3, A03, drawableArr);
                rippleDrawable = new LayerDrawable(drawableArr);
            }
        }
        setBackground(rippleDrawable);
        tabLayout.invalidate();
    }

    public final void A02(ImageView imageView, TextView textView) {
        CharSequence charSequence;
        CharSequence charSequence2;
        int i;
        AnonymousClass3FN r0 = this.A07;
        CharSequence charSequence3 = null;
        if (r0 != null) {
            charSequence = r0.A05;
        } else {
            charSequence = null;
        }
        if (imageView != null) {
            imageView.setVisibility(8);
            imageView.setImageDrawable(null);
        }
        boolean z = !TextUtils.isEmpty(charSequence);
        if (textView != null) {
            if (z) {
                textView.setText(charSequence);
                textView.setVisibility(0);
                setVisibility(0);
            } else {
                textView.setVisibility(8);
                textView.setText((CharSequence) null);
            }
        }
        if (imageView != null) {
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(imageView);
            if (!z || imageView.getVisibility() != 0) {
                i = 0;
            } else {
                i = this.A08.A01(8);
            }
            if (this.A08.A0Q) {
                if (i != C05670Qm.A00(A0H)) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        AnonymousClass0T4.A02(A0H, i);
                    } else {
                        A0H.rightMargin = i;
                    }
                    A0H.bottomMargin = 0;
                    imageView.setLayoutParams(A0H);
                    imageView.requestLayout();
                }
            } else if (i != A0H.bottomMargin) {
                A0H.bottomMargin = i;
                if (Build.VERSION.SDK_INT >= 17) {
                    AnonymousClass0T4.A02(A0H, 0);
                } else {
                    A0H.rightMargin = 0;
                }
                imageView.setLayoutParams(A0H);
                imageView.requestLayout();
            }
        }
        AnonymousClass3FN r02 = this.A07;
        if (r02 != null) {
            charSequence2 = r02.A04;
        } else {
            charSequence2 = null;
        }
        if (!z) {
            charSequence3 = charSequence2;
        }
        AnonymousClass0KD.A00(this, charSequence3);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.A01;
        if (drawable != null && drawable.isStateful() && (false || this.A01.setState(drawableState))) {
            invalidate();
            this.A08.invalidate();
        }
    }

    public final int getContentWidth() {
        int i = 0;
        View[] viewArr = {this.A06, this.A04, this.A02};
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        do {
            View view = viewArr[i];
            if (view != null && view.getVisibility() == 0) {
                if (z) {
                    i3 = Math.min(i3, view.getLeft());
                    i2 = Math.max(i2, view.getRight());
                } else {
                    i3 = view.getLeft();
                    i2 = view.getRight();
                }
                z = true;
            }
            i++;
        } while (i < 3);
        return i2 - i3;
    }

    public AnonymousClass3FN getTab() {
        return this.A07;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(ActionBar$Tab.class.getName());
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(ActionBar$Tab.class.getName());
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        Layout layout;
        int size = View.MeasureSpec.getSize(i);
        int mode = View.MeasureSpec.getMode(i);
        TabLayout tabLayout = this.A08;
        int i3 = tabLayout.A07;
        if (i3 > 0 && (mode == 0 || size > i3)) {
            i = View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE);
        }
        super.onMeasure(i, i2);
        if (this.A06 != null) {
            float f = tabLayout.A01;
            int i4 = this.A00;
            ImageView imageView = this.A04;
            if (imageView == null || imageView.getVisibility() != 0) {
                TextView textView = this.A06;
                if (textView != null && textView.getLineCount() > 1) {
                    f = tabLayout.A00;
                }
            } else {
                i4 = 1;
            }
            float textSize = this.A06.getTextSize();
            int lineCount = this.A06.getLineCount();
            int maxLines = this.A06.getMaxLines();
            if (f == textSize && (maxLines < 0 || i4 == maxLines)) {
                return;
            }
            if (tabLayout.A03 != 1 || f <= textSize || lineCount != 1 || ((layout = this.A06.getLayout()) != null && layout.getLineWidth(0) * (f / layout.getPaint().getTextSize()) <= ((float) C12960it.A04(this, getMeasuredWidth())))) {
                this.A06.setTextSize(0, f);
                this.A06.setMaxLines(i4);
                super.onMeasure(i, i2);
            }
        }
    }

    @Override // android.view.View
    public boolean performClick() {
        boolean performClick = super.performClick();
        if (this.A07 == null) {
            return performClick;
        }
        if (!performClick) {
            playSoundEffect(0);
        }
        this.A07.A00();
        return true;
    }

    @Override // android.view.View
    public void setSelected(boolean z) {
        isSelected();
        super.setSelected(z);
        TextView textView = this.A06;
        if (textView != null) {
            textView.setSelected(z);
        }
        ImageView imageView = this.A04;
        if (imageView != null) {
            imageView.setSelected(z);
        }
        View view = this.A02;
        if (view != null) {
            view.setSelected(z);
        }
    }

    public void setTab(AnonymousClass3FN r2) {
        if (r2 != this.A07) {
            this.A07 = r2;
            A00();
        }
    }
}
