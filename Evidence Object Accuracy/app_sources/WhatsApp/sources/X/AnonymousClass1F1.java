package X;

import android.util.JsonWriter;
import com.whatsapp.EmojiPicker$EmojiWeight;
import com.whatsapp.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1F1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1F1 implements AnonymousClass1BW {
    public final AbstractC15710nm A00;
    public final C16590pI A01;

    public AnonymousClass1F1(AbstractC15710nm r1, C16590pI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1BW
    public AbstractC38871oq A8G(Object obj, float f) {
        return new EmojiPicker$EmojiWeight((int[]) obj, f);
    }

    @Override // X.AnonymousClass1BW
    public Object ADE(String str) {
        int length = str.length();
        int i = 0;
        int[] iArr = new int[str.codePointCount(0, length)];
        int i2 = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            iArr[i2] = codePointAt;
            i += Character.charCount(codePointAt);
            i2++;
        }
        return iArr;
    }

    @Override // X.AnonymousClass1BW
    public String ADj(Object obj) {
        int[] iArr = (int[]) obj;
        return new String(iArr, 0, iArr.length);
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass1BW
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AIX() {
        /*
            r7 = this;
            X.0pI r0 = r7.A01
            android.content.Context r0 = r0.A00
            java.io.File r1 = r0.getFilesDir()
            java.lang.String r0 = "emoji"
            java.io.File r4 = new java.io.File
            r4.<init>(r1, r0)
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x00a4
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch: IOException -> 0x006f, Exception -> 0x008f
            r5.<init>()     // Catch: IOException -> 0x006f, Exception -> 0x008f
            java.io.FileReader r0 = new java.io.FileReader     // Catch: IOException -> 0x006f, Exception -> 0x008f
            r0.<init>(r4)     // Catch: IOException -> 0x006f, Exception -> 0x008f
            android.util.JsonReader r6 = new android.util.JsonReader     // Catch: IOException -> 0x006f, Exception -> 0x008f
            r6.<init>(r0)     // Catch: IOException -> 0x006f, Exception -> 0x008f
            r6.beginObject()     // Catch: all -> 0x006a
        L_0x0027:
            boolean r0 = r6.hasNext()     // Catch: all -> 0x006a
            if (r0 == 0) goto L_0x0063
            java.lang.String r1 = "weights"
            java.lang.String r0 = r6.nextName()     // Catch: all -> 0x006a
            boolean r0 = r1.equals(r0)     // Catch: all -> 0x006a
            if (r0 == 0) goto L_0x005f
            r6.beginObject()     // Catch: all -> 0x006a
        L_0x003d:
            boolean r0 = r6.hasNext()     // Catch: all -> 0x006a
            if (r0 == 0) goto L_0x005b
            java.lang.String r0 = r6.nextName()     // Catch: all -> 0x006a
            X.1mS r3 = X.C38491oB.A00(r0)     // Catch: all -> 0x006a
            double r0 = r6.nextDouble()     // Catch: all -> 0x006a
            float r2 = (float) r0     // Catch: all -> 0x006a
            int[] r1 = r3.A00     // Catch: all -> 0x006a
            com.whatsapp.EmojiPicker$EmojiWeight r0 = new com.whatsapp.EmojiPicker$EmojiWeight     // Catch: all -> 0x006a
            r0.<init>(r1, r2)     // Catch: all -> 0x006a
            r5.add(r0)     // Catch: all -> 0x006a
            goto L_0x003d
        L_0x005b:
            r6.endObject()     // Catch: all -> 0x006a
            goto L_0x0027
        L_0x005f:
            r6.skipValue()     // Catch: all -> 0x006a
            goto L_0x0027
        L_0x0063:
            r6.endObject()     // Catch: all -> 0x006a
            r6.close()     // Catch: IOException -> 0x006f, Exception -> 0x008f
            goto L_0x00a5
        L_0x006a:
            r0 = move-exception
            r6.close()     // Catch: all -> 0x006e
        L_0x006e:
            throw r0     // Catch: IOException -> 0x006f, Exception -> 0x008f
        L_0x006f:
            r2 = move-exception
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch: IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
            r0.<init>(r4)     // Catch: IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch: IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
            r1.<init>(r0)     // Catch: IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
            java.lang.Object r0 = r1.readObject()     // Catch: all -> 0x0089
            java.util.List r0 = (java.util.List) r0     // Catch: all -> 0x0089
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch: all -> 0x0089
            r5.<init>(r0)     // Catch: all -> 0x0089
            r1.close()     // Catch: IOException | ClassNotFoundException -> 0x008e, IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
            goto L_0x00a5
        L_0x0089:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x008d
        L_0x008d:
            throw r0     // Catch: IOException | ClassNotFoundException -> 0x008e, IOException | ClassNotFoundException -> 0x008e, Exception -> 0x008f
        L_0x008e:
            throw r2     // Catch: Exception -> 0x008f
        L_0x008f:
            r1 = move-exception
            java.lang.String r0 = "recentemoji/readrecent "
            com.whatsapp.util.Log.e(r0, r1)
            X.0nm r3 = r7.A00
            java.lang.String r2 = r1.toString()
            r1 = 0
            java.lang.String r0 = "recentemoji/load-error"
            r3.AaV(r0, r2, r1)
            r4.delete()
        L_0x00a4:
            r5 = 0
        L_0x00a5:
            if (r5 != 0) goto L_0x00ac
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
        L_0x00ac:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1F1.AIX():java.util.List");
    }

    @Override // X.AnonymousClass1BW
    public void AZH(List list) {
        try {
            File file = new File(this.A01.A00.getFilesDir(), "emoji");
            AbstractC15710nm r5 = this.A00;
            JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(new FileWriter(file)));
            jsonWriter.beginObject();
            jsonWriter.name("weights");
            jsonWriter.beginObject();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                EmojiPicker$EmojiWeight emojiPicker$EmojiWeight = (EmojiPicker$EmojiWeight) it.next();
                int[] iArr = emojiPicker$EmojiWeight.emoji;
                if (iArr == null) {
                    r5.AaV("RecentEmojiHelper/persistListJson/emoji is null", null, true);
                } else {
                    jsonWriter.name(C37471mS.A00(iArr));
                    jsonWriter.value((double) emojiPicker$EmojiWeight.weight);
                }
            }
            jsonWriter.endObject();
            jsonWriter.endObject();
            jsonWriter.close();
        } catch (IOException e) {
            Log.e(e);
            this.A00.AaV("recentemoji/save-error", e.toString(), false);
        }
    }
}
