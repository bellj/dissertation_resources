package X;

import android.graphics.Bitmap;
import android.widget.ImageView;

/* renamed from: X.5KC  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KC extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ C850340w this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KC(C850340w r2) {
        super(1);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        C16700pc.A0E(bitmap, 0);
        ((ImageView) this.this$0.A01.getValue()).setImageBitmap(bitmap);
        return AnonymousClass1WZ.A00;
    }
}
