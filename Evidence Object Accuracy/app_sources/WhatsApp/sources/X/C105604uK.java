package X;

/* renamed from: X.4uK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C105604uK implements AbstractC009404s {
    public final AnonymousClass1CR A00;
    public final AnonymousClass3EX A01;

    public C105604uK(AnonymousClass1CR r2, AnonymousClass3EX r3) {
        C16700pc.A0E(r3, 1);
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new AnonymousClass2fT(this.A00, this.A01);
    }
}
