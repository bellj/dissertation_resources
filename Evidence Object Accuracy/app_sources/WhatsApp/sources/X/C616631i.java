package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import com.whatsapp.R;
import java.io.File;

/* renamed from: X.31i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616631i extends AbstractC616731m {
    public int A00 = 1;
    public Drawable A01;
    public Drawable A02;
    public Drawable A03;
    public Drawable A04;
    public Drawable A05;
    public TextPaint A06;
    public C253218y A07;
    public boolean A08;

    public C616631i(Context context) {
        super(context);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r1 != 1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        if (r1 == 3) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        if (r4 == false) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        if (r3 != false) goto L_0x0145;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002a, code lost:
        if (r4 != false) goto L_0x0157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002c, code lost:
        if (r5 == false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0030, code lost:
        if (r1 == 0) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0034, code lost:
        if (r12.A06 != null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0036, code lost:
        r12.A06 = A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003c, code lost:
        r13.drawText(X.C38131nZ.A04(r12.A04, r1), ((float) r0) + (r12.A06.getTextSize() / 3.0f), X.C12990iw.A03(r12) - (r12.A06.getTextSize() / 3.0f), r12.A06);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0145, code lost:
        r6 = r12.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0147, code lost:
        if (r6 != null) goto L_0x016a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0149, code lost:
        r6 = X.AnonymousClass00T.A04(getContext(), com.whatsapp.R.drawable.mark_video);
        r12.A05 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0157, code lost:
        r6 = r12.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0159, code lost:
        if (r6 != null) goto L_0x016a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x015b, code lost:
        r6 = X.AnonymousClass00T.A04(getContext(), com.whatsapp.R.drawable.mark_gif);
        r12.A01 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0168, code lost:
        if (r6 == null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x016a, code lost:
        r9 = r12.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x016c, code lost:
        if (r9 != null) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016e, code lost:
        r9 = X.AnonymousClass00T.A04(getContext(), com.whatsapp.R.drawable.gallery_album_overlay);
        r12.A04 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x017b, code lost:
        r9.setBounds(0, getHeight() - (r6.getIntrinsicHeight() << 1), getWidth(), getHeight());
        r12.A04.draw(r13);
        r5 = r6.getIntrinsicHeight() >> 2;
        r6.setBounds(r5, (getHeight() - r6.getIntrinsicHeight()) - r5, r6.getIntrinsicWidth() + r5, getHeight() - r5);
        r6.draw(r13);
        r0 = r6.getIntrinsicWidth() + r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01bd, code lost:
        r1 = r2.ACb();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01c3, code lost:
        r5 = false;
     */
    @Override // X.AnonymousClass2T3
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.graphics.Canvas r13) {
        /*
        // Method dump skipped, instructions count: 462
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C616631i.A01(android.graphics.Canvas):void");
    }

    public final TextPaint A02() {
        TextPaint textPaint = new TextPaint(1);
        textPaint.setColor(-1);
        textPaint.setTextSize((float) C12960it.A09(this).getDimensionPixelSize(R.dimen.media_gallery_item_text_size));
        return textPaint;
    }

    public final void A03(Canvas canvas, AbstractC35601iM r9, float f) {
        String str;
        File file = r9.A04;
        if (file != null) {
            str = file.getName();
        } else {
            str = null;
        }
        if (r9.getType() == 4 && !TextUtils.isEmpty(str)) {
            if (this.A06 == null) {
                this.A06 = A02();
            }
            Drawable drawable = this.A04;
            if (drawable == null) {
                drawable = C12970iu.A0C(getContext(), R.drawable.gallery_album_overlay);
                this.A04 = drawable;
            }
            drawable.setBounds(0, getHeight() - ((int) (this.A06.getTextSize() * 2.0f)), getWidth(), getHeight());
            this.A04.draw(canvas);
            TextPaint textPaint = this.A06;
            canvas.drawText(TextUtils.ellipsize(str, textPaint, f - ((textPaint.getTextSize() / 3.0f) * 2.0f), TextUtils.TruncateAt.END).toString(), this.A06.getTextSize() / 3.0f, C12990iw.A03(this) - (this.A06.getTextSize() / 3.0f), this.A06);
        }
    }

    public void setDetailsLevel(int i) {
        this.A00 = i;
    }

    @Override // X.AnonymousClass2T3
    public void setMediaItem(AbstractC35611iN r3) {
        super.setMediaItem(r3);
        AbstractC35611iN r1 = super.A05;
        if (r1 instanceof AbstractC35601iM) {
            AnonymousClass028.A0k(this, AbstractC28561Ob.A0W(((AbstractC35601iM) r1).A03.A0z));
        }
    }

    public void setShowForwardScore(boolean z) {
        this.A08 = z;
    }
}
