package X;

import android.database.Cursor;

/* renamed from: X.24x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C461924x {
    public final long A00;
    public final long A01;
    public final AnonymousClass1IS A02;
    public final boolean A03;

    public /* synthetic */ C461924x(Cursor cursor, AbstractC14640lm r7) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow("key_id"));
        boolean z = false;
        boolean z2 = cursor.getInt(cursor.getColumnIndexOrThrow("from_me")) == 1;
        this.A02 = new AnonymousClass1IS(r7, string, z2);
        cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        this.A00 = cursor.getLong(cursor.getColumnIndexOrThrow("sort_id"));
        this.A03 = cursor.getInt(cursor.getColumnIndexOrThrow("starred")) == 1 ? true : z;
        this.A01 = C250217u.A01(cursor, z2);
    }
}
