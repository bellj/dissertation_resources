package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.SparseArray;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.167  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass167 {
    public final Handler A00;
    public final HandlerThread A01;
    public final SparseArray A02 = new SparseArray();
    public final AbstractC14440lR A03;

    public AnonymousClass167(AbstractC14440lR r4) {
        this.A03 = r4;
        HandlerThread handlerThread = new HandlerThread("light-prefs-save-scheduler", -2);
        this.A01 = handlerThread;
        handlerThread.start();
        this.A00 = new Handler(handlerThread.getLooper());
    }

    public void A00(Runnable runnable, int i, boolean z) {
        ExecutorC27271Gr r4;
        synchronized (this) {
            SparseArray sparseArray = this.A02;
            r4 = (ExecutorC27271Gr) sparseArray.get(i);
            if (r4 == null) {
                r4 = new ExecutorC27271Gr(this.A03, true);
                sparseArray.put(i, r4);
            }
        }
        if (z) {
            this.A00.postDelayed(new RunnableBRunnable0Shape6S0200000_I0_6(r4, 48, runnable), 100);
        } else {
            r4.execute(runnable);
        }
    }
}
