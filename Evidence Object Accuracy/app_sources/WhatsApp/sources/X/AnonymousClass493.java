package X;

import java.io.IOException;

/* renamed from: X.493  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass493 extends IOException {
    public AnonymousClass493() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }

    public AnonymousClass493(String str, Throwable th) {
        super(C72453ed.A0r("CodedOutputStream was writing to a flat byte array and ran out of space.: ", str), th);
    }

    public AnonymousClass493(Throwable th) {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
    }
}
