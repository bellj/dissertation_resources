package X;

import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/* renamed from: X.0ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10430ec implements Callable {
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A00;

    public CallableC10430ec(ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl) {
        this.A00 = shortcutInfoCompatSaverImpl;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass0NL r0 : this.A00.A04.values()) {
            arrayList.add(new AnonymousClass03w(r0.A00).A00());
        }
        return arrayList;
    }
}
