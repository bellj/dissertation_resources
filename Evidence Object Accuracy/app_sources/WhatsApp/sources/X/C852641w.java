package X;

import com.whatsapp.settings.SettingsPrivacy;
import java.util.Collection;

/* renamed from: X.41w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852641w extends C27131Gd {
    public final /* synthetic */ SettingsPrivacy A00;

    public C852641w(SettingsPrivacy settingsPrivacy) {
        this.A00 = settingsPrivacy;
    }

    @Override // X.C27131Gd
    public void A04(Collection collection) {
        this.A00.A2h();
    }
}
