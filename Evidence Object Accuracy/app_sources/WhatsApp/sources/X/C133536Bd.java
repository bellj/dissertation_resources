package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.service.NoviVideoSelfieFgService;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.6Bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133536Bd implements AnonymousClass6MX {
    public final /* synthetic */ NoviVideoSelfieFgService A00;
    public final /* synthetic */ File A01;
    public final /* synthetic */ String A02;

    public C133536Bd(NoviVideoSelfieFgService noviVideoSelfieFgService, File file, String str) {
        this.A00 = noviVideoSelfieFgService;
        this.A01 = file;
        this.A02 = str;
    }

    @Override // X.AnonymousClass6MX
    public void AY5(int i) {
        NoviVideoSelfieFgService noviVideoSelfieFgService = this.A00;
        noviVideoSelfieFgService.A03 = (noviVideoSelfieFgService.A02 * ((long) i)) / 100;
        NoviVideoSelfieFgService.A00(noviVideoSelfieFgService);
    }

    @Override // X.AnonymousClass6MX
    public void AY6(C452120p r7, String str, String str2, boolean z) {
        StringBuilder A0k = C12960it.A0k("VideoSelfie image upload success? ");
        A0k.append(z);
        A0k.append(" imageHandle: ");
        A0k.append(str);
        A0k.append(" imageMimetype: ");
        Log.i(C12960it.A0d(str2, A0k));
        if (!z || str == null) {
            C128365vz r2 = new AnonymousClass610("SELFI_VIDEO_UPLOAD_STATUS", "SELFIE_UPLOAD", "MODAL").A00;
            r2.A0Q = "FAILED";
            r2.A0g = "STEP_UP_SELFIE";
            NoviVideoSelfieFgService noviVideoSelfieFgService = this.A00;
            C1316663q r1 = noviVideoSelfieFgService.A0C;
            if (r1 != null) {
                r2.A0E = r1.A02;
                r2.A0K = noviVideoSelfieFgService.A0E;
                r2.A0f = r1.A03;
            }
            if (r7 != null) {
                r2.A0N = Integer.toString(r7.A00);
                r2.A0P = r7.A08;
                r2.A0O = r7.A07;
            }
            if (noviVideoSelfieFgService.A01 != 10) {
                noviVideoSelfieFgService.A0A.A05(r2);
            }
            Intent intent = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
            intent.putExtra("extra_event_type", "extra_event_upload_failed");
            C117305Zk.A0y(noviVideoSelfieFgService, intent);
            noviVideoSelfieFgService.A04(noviVideoSelfieFgService.getString(R.string.novi_selfie_upload_media_failed));
            return;
        }
        C129695y9 r5 = this.A00.A0B;
        File file = this.A01;
        String str3 = this.A02;
        r5.A05.A0B(new AbstractC136286Ly(new C133506Ba(this, str), r5, file, str3) { // from class: X.6BV
            public final /* synthetic */ AnonymousClass6MX A00;
            public final /* synthetic */ C129695y9 A01;
            public final /* synthetic */ File A02;
            public final /* synthetic */ String A03 = "video/mp4";
            public final /* synthetic */ String A04 = "SELFIE_VIDEO";
            public final /* synthetic */ String A05;

            {
                this.A01 = r4;
                this.A02 = r5;
                this.A05 = r6;
                this.A00 = r3;
            }

            @Override // X.AbstractC136286Ly
            public final void AT6(C1315463e r10) {
                String str4;
                C129695y9 r0 = this.A01;
                File file2 = this.A02;
                String str5 = this.A03;
                String str6 = this.A04;
                String str7 = this.A05;
                AnonymousClass6MX r22 = this.A00;
                C14370lK r3 = C14370lK.A0N;
                if (r10 != null) {
                    str4 = r10.A02;
                } else {
                    str4 = null;
                }
                r0.A00(null, r22, r3, file2, str5, str6, str7, str4);
            }
        });
    }
}
