package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

/* renamed from: X.3Wo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68743Wo implements AnonymousClass2DH {
    public final /* synthetic */ C65963Lt A00;
    public final /* synthetic */ C16680pa A01;
    public final /* synthetic */ C129625y2 A02;
    public final /* synthetic */ String A03;

    public C68743Wo(C65963Lt r1, C16680pa r2, C129625y2 r3, String str) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = str;
    }

    public static /* synthetic */ void A00(C65963Lt r8, C16680pa r9, C129625y2 r10, String str) {
        String str2;
        File[] listFiles;
        C16700pc.A0F(r9, r10);
        C16700pc.A0E(str, 3);
        C18820t6 r2 = r9.A07;
        File A00 = r2.A00(C16700pc.A08("commerce_flow_", r2.A00));
        if (!(A00 == null || (listFiles = A00.listFiles()) == null)) {
            int i = 0;
            int length = listFiles.length;
            while (i < length) {
                File file = listFiles[i];
                i++;
                if (C16700pc.A0O(C14350lI.A07(file.getAbsolutePath()), "json")) {
                    try {
                        byte[] A002 = AnonymousClass1NM.A00(file);
                        C16700pc.A0B(A002);
                        String str3 = new String(A002, C88844Hi.A05);
                        r10.A01(str3);
                        if (r8 != null) {
                            r9.A05.A03(str3, r8.A01, r9.A02(str), Long.valueOf(r8.A00).longValue(), r8.A02);
                            return;
                        }
                        return;
                    } catch (IOException e) {
                        Log.e("CommerceBloksAssetDownloader/readFile/ioerror", e);
                    }
                }
            }
            throw new NoSuchElementException("Array contains no element matching the predicate.");
        }
        if (!(r9 instanceof C16670pZ)) {
            str2 = "Commerce bloks layout was not loaded";
        } else {
            str2 = "Extensions bloks layout was not loaded";
        }
        r10.A00(C12960it.A0U(str2));
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        String str;
        C129625y2 r1 = this.A02;
        if (!(this.A01 instanceof C16670pZ)) {
            str = "Commerce bloks layout was not loaded";
        } else {
            str = "Extensions bloks layout was not loaded";
        }
        r1.A00(C12960it.A0U(str));
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        String str;
        C129625y2 r1 = this.A02;
        if (!(this.A01 instanceof C16670pZ)) {
            str = "Commerce bloks layout was not loaded";
        } else {
            str = "Extensions bloks layout was not loaded";
        }
        r1.A00(C12960it.A0U(str));
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        C16680pa r5 = this.A01;
        r5.A04.Ab2(new Runnable(this.A00, r5, this.A02, this.A03) { // from class: X.3lH
            public final /* synthetic */ C65963Lt A00;
            public final /* synthetic */ C16680pa A01;
            public final /* synthetic */ C129625y2 A02;
            public final /* synthetic */ String A03;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
                this.A03 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C68743Wo.A00(this.A00, this.A01, this.A02, this.A03);
            }
        });
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        String str;
        C129625y2 r1 = this.A02;
        if (!(this.A01 instanceof C16670pZ)) {
            str = "Commerce bloks layout was not loaded";
        } else {
            str = "Extensions bloks layout was not loaded";
        }
        r1.A00(C12960it.A0U(str));
    }
}
