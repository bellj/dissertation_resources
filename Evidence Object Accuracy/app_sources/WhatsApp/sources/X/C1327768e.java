package X;

import android.text.TextUtils;
import java.util.List;

/* renamed from: X.68e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327768e implements AnonymousClass1FK {
    public final /* synthetic */ C123555nL A00;

    public C1327768e(C123555nL r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A00.A08.A0B(C128315vu.A00(24));
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A00.A08.A0B(C128315vu.A00(24));
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        AnonymousClass46P r42;
        List list;
        if (!(r4 instanceof AnonymousClass46P) || (list = (r42 = (AnonymousClass46P) r4).A01) == null || list.size() <= 0) {
            this.A00.A08.A0B(C128315vu.A00(24));
            return;
        }
        C123555nL r2 = this.A00;
        r2.A0d.A04("BrazilConsumer/getTransactions/onResponseSuccess");
        AnonymousClass20A r1 = r42.A00;
        if (r1 == null) {
            r2.A0O(false);
            r2.A0P(false);
        } else if (!r1.A02 && !TextUtils.isEmpty(r1.A00)) {
            r2.A0b.A00(this, null, null, r42.A00.A00);
        }
    }
}
