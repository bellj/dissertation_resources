package X;

import java.util.List;

/* renamed from: X.51c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1092851c implements AbstractC116415Vi {
    public final /* synthetic */ AnonymousClass3H7 A00;
    public final /* synthetic */ List A01;

    @Override // X.AbstractC116415Vi
    public void AY4(AnonymousClass28D r1) {
    }

    public C1092851c(AnonymousClass3H7 r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // X.AbstractC116415Vi
    public AnonymousClass28D A62(AnonymousClass28D r5) {
        AnonymousClass3CV r0 = this.A00.A02;
        List list = this.A01;
        return new AnonymousClass28D(r5, null, list, r0.A00(r5, list));
    }
}
