package X;

import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.6My  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC136546My {
    boolean AA5();

    void Aez(Drawable drawable, View.OnClickListener onClickListener, String str, int i, boolean z, boolean z2);

    boolean Af0();

    Object getFormDataTag();

    String getInputValue();
}
