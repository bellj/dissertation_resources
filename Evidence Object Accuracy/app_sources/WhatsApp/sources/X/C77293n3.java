package X;

import java.util.List;

/* renamed from: X.3n3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77293n3 extends AbstractC107784xw {
    public int A00;
    public int A01 = -1;
    public C95004ct A02;
    public AnonymousClass4R2 A03;
    public List A04;
    public List A05;
    public final int A06;
    public final C95054d0 A07 = new C95054d0();
    public final C95304dT A08 = new C95304dT();
    public final C95004ct[] A09;

    public C77293n3(List list, int i) {
        this.A06 = i == -1 ? 1 : i;
        if (list != null && list.size() == 1 && C72463ee.A0b(list, 0).length == 1) {
            list.get(0);
        }
        this.A09 = new C95004ct[8];
        int i2 = 0;
        do {
            this.A09[i2] = new C95004ct();
            i2++;
        } while (i2 < 8);
        this.A02 = this.A09[0];
    }

    public static int A00(C95054d0 r4) {
        return C95004ct.A00(r4.A04(2), r4.A04(2), r4.A04(2), r4.A04(2));
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A02() {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77293n3.A02():java.util.List");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x0406  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
        // Method dump skipped, instructions count: 1200
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77293n3.A03():void");
    }

    public final void A04() {
        int i = 0;
        do {
            this.A09[i].A02();
            i++;
        } while (i < 8);
    }

    @Override // X.AbstractC107784xw, X.AnonymousClass5XF
    public void flush() {
        super.flush();
        this.A04 = null;
        this.A05 = null;
        this.A00 = 0;
        this.A02 = this.A09[0];
        A04();
        this.A03 = null;
    }
}
