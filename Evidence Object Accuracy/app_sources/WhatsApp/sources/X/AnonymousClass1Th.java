package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.charset.Charset;
import java.util.Arrays;

/* renamed from: X.1Th  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Th {
    public static final AnonymousClass01b A00 = new AnonymousClass01b(Arrays.asList("AO", "CV", "GQ", "FR", "GW", "LU", "MO", "MZ", "PT", "ST", "CH", "TL"));
    public static final Charset A01 = Charset.forName("US-ASCII");
    public static final Charset[] A02 = {Charset.forName(DefaultCrypto.UTF_8), Charset.forName("UTF-16BE")};
}
