package X;

import com.whatsapp.util.Log;

/* renamed from: X.0m8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14840m8 {
    public Boolean A00;
    public final AbstractC15710nm A01;
    public final C15450nH A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C16490p7 A05;
    public final C21390xL A06;
    public final C14850m9 A07;
    public final C16120oU A08;
    public final C14860mA A09;

    public C14840m8(AbstractC15710nm r1, C15450nH r2, C14830m7 r3, C14820m6 r4, C16490p7 r5, C21390xL r6, C14850m9 r7, C16120oU r8, C14860mA r9) {
        this.A03 = r3;
        this.A07 = r7;
        this.A01 = r1;
        this.A08 = r8;
        this.A09 = r9;
        this.A02 = r2;
        this.A06 = r6;
        this.A05 = r5;
        this.A04 = r4;
    }

    public void A00() {
        this.A04.A00.edit().remove("md_opt_in_awareness_period_deadline").apply();
    }

    public synchronized void A01(boolean z) {
        this.A00 = Boolean.valueOf(z);
        this.A04.A00.edit().putBoolean("md_messaging_enabled", z).apply();
    }

    public boolean A02() {
        if (!A05() || this.A07.A02(861) != 2) {
            return false;
        }
        return true;
    }

    public boolean A03() {
        if (!A05()) {
            return false;
        }
        return this.A04.A00.getBoolean("companion_reg_opt_in_enabled", false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        if (r5.A04.A00.getInt("md_seamless_status", 0) != 1) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04() {
        /*
            r5 = this;
            boolean r0 = r5.A05()
            r2 = 0
            if (r0 == 0) goto L_0x0043
            X.0m9 r4 = r5.A07
            r0 = 861(0x35d, float:1.207E-42)
            int r1 = r4.A02(r0)
            r3 = 1
            if (r1 != r3) goto L_0x0044
            r0 = 489(0x1e9, float:6.85E-43)
            int r1 = r4.A02(r0)
            r0 = 2
            if (r1 == r0) goto L_0x001e
            r0 = 3
            if (r1 != r0) goto L_0x002b
        L_0x001e:
            X.0m6 r0 = r5.A04
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "md_seamless_status"
            int r0 = r1.getInt(r0, r2)
            r1 = 1
            if (r0 == r3) goto L_0x002c
        L_0x002b:
            r1 = 0
        L_0x002c:
            X.0m6 r0 = r5.A04
            android.content.SharedPreferences r4 = r0.A00
            java.lang.String r3 = "seamless_migration_in_progress"
            boolean r0 = r4.getBoolean(r3, r2)
            if (r0 != 0) goto L_0x006b
            java.lang.String r0 = "companion_reg_opt_in_enabled"
            boolean r0 = r4.getBoolean(r0, r2)
            if (r0 == 0) goto L_0x0042
            if (r1 != 0) goto L_0x006b
        L_0x0042:
            r2 = 1
        L_0x0043:
            return r2
        L_0x0044:
            r0 = 2
            if (r1 != r0) goto L_0x005c
            X.0m6 r0 = r5.A04
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "companion_reg_opt_in_enabled"
            boolean r0 = r1.getBoolean(r0, r2)
            if (r0 != 0) goto L_0x0043
            X.0mA r0 = r5.A09
            boolean r0 = r0.A0M()
            if (r0 != 0) goto L_0x0043
            goto L_0x0042
        L_0x005c:
            if (r1 != 0) goto L_0x0043
            X.0m6 r0 = r5.A04
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "companion_reg_opt_in_enabled"
            boolean r0 = r1.getBoolean(r0, r2)
            if (r0 == 0) goto L_0x0043
            goto L_0x0042
        L_0x006b:
            java.lang.String r0 = "MultiDeviceConfig/not showing Opt In for manual. Is Seamless in progress: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            boolean r0 = r4.getBoolean(r3, r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14840m8.A04():boolean");
    }

    public synchronized boolean A05() {
        if (this.A00 == null) {
            boolean z = false;
            if (this.A04.A00.getBoolean("md_messaging_enabled", false)) {
                z = true;
            } else {
                C16490p7 r0 = this.A05;
                r0.A04();
                if (!r0.A01) {
                    Log.w("MultiDeviceConfig/isMultiDeviceMessagingEnabled/message store isn't ready yet");
                    return false;
                }
                C21390xL r4 = this.A06;
                if (r4.A00("participant_user_ready", 0) == 2) {
                    z = true;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("MultiDeviceConfig/isMultiDeviceMessagingEnabled/M-D enabled: ");
                sb.append(z);
                Log.i(sb.toString());
                if (!z) {
                    AnonymousClass1Y8 r2 = new AnonymousClass1Y8();
                    r2.A00 = Long.valueOf((long) r4.A00("participant_user_ready", 0));
                    this.A08.A07(r2);
                }
            }
            A01(z);
        }
        return this.A00.booleanValue();
    }
}
