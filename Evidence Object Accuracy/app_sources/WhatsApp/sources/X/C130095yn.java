package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.5yn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130095yn {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C15650ng A03;
    public final C20370ve A04;
    public final C243515e A05;
    public final C17070qD A06;
    public final C130155yt A07;
    public final C130125yq A08;
    public final C22980zx A09;
    public final AbstractC14440lR A0A;
    public final ConcurrentHashMap A0B = new ConcurrentHashMap();
    public final ConcurrentHashMap A0C = new ConcurrentHashMap();

    public C130095yn(C14900mE r2, C15570nT r3, C14830m7 r4, C15650ng r5, C20370ve r6, C243515e r7, C17070qD r8, C130155yt r9, C130125yq r10, C22980zx r11, AbstractC14440lR r12) {
        this.A02 = r4;
        this.A00 = r2;
        this.A0A = r12;
        this.A01 = r3;
        this.A06 = r8;
        this.A03 = r5;
        this.A07 = r9;
        this.A09 = r11;
        this.A08 = r10;
        this.A04 = r6;
        this.A05 = r7;
    }

    public final AnonymousClass46P A00(AnonymousClass1V8 r9) {
        ArrayList arrayList;
        AnonymousClass1V8[] r0;
        int length;
        AnonymousClass46P r5 = new AnonymousClass46P();
        if (r9 == null || (r0 = r9.A03) == null || (length = r0.length) <= 0) {
            arrayList = null;
        } else {
            arrayList = C12960it.A0l();
            int i = 0;
            do {
                AnonymousClass1V8 A0D = r9.A0D(i);
                AnonymousClass009.A05(A0D);
                AnonymousClass1IR A00 = new C128715wY(this.A01, this.A09).A00(A0D);
                if (A00 != null) {
                    arrayList.add(A00);
                }
                i++;
            } while (i < length);
        }
        r5.A01 = arrayList;
        r5.A00 = C22980zx.A00(r9, "before");
        if (r9 != null) {
            r5.A00.A01 = "true".equalsIgnoreCase(C117295Zj.A0W(r9, "has_previous"));
        }
        return r5;
    }

    public final void A01(AnonymousClass016 r4, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A05.A05(C117315Zl.A09(it));
        }
        if (r4 != null) {
            C130785zy.A02(r4, null, list);
        }
    }

    public final void A02(AnonymousClass20A r5, List list) {
        for (int i = 0; i < list.size(); i++) {
            AnonymousClass1IR r2 = (AnonymousClass1IR) list.get(i);
            if (!TextUtils.isEmpty(r2.A0K)) {
                this.A0C.put(r2.A0K, r2);
                if (i == list.size() - 1 && r5 != null) {
                    this.A0B.put(r2.A0K, r5);
                }
            }
        }
    }
}
