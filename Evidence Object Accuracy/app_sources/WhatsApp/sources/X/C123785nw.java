package X;

import org.json.JSONObject;

/* renamed from: X.5nw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123785nw extends C124385pJ {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C123785nw(X.C18790t3 r20, X.C14820m6 r21, X.C14850m9 r22, X.AnonymousClass18L r23, X.AnonymousClass01H r24, java.lang.String r25, java.lang.String r26, java.util.Map r27, X.AnonymousClass01N r28, X.AnonymousClass01N r29, long r30) {
        /*
            r19 = this;
            r7 = r21
            java.lang.String r0 = r7.A0B()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r1 = r0.intValue()
            android.util.SparseArray r0 = X.AnonymousClass1NE.A01
            java.lang.Object r6 = r0.get(r1)
            if (r6 == 0) goto L_0x0058
            org.json.JSONObject r5 = X.C117295Zj.A0a()     // Catch: JSONException -> 0x0052
            java.lang.String r4 = "params"
            org.json.JSONObject r3 = X.C117295Zj.A0a()     // Catch: JSONException -> 0x0052
            java.lang.String r2 = "server_params"
            org.json.JSONObject r1 = X.C117295Zj.A0a()     // Catch: JSONException -> 0x0052
            java.lang.String r0 = "country_iso_graphql"
            org.json.JSONObject r0 = r1.put(r0, r6)     // Catch: JSONException -> 0x0052
            org.json.JSONObject r0 = r3.put(r2, r0)     // Catch: JSONException -> 0x0052
            org.json.JSONObject r0 = r5.put(r4, r0)     // Catch: JSONException -> 0x0052
            java.lang.String r13 = r0.toString()     // Catch: JSONException -> 0x0052
            r5 = r19
            r10 = r24
            r9 = r23
            r8 = r22
            r6 = r20
            r11 = r25
            r16 = r29
            r17 = r30
            r15 = r28
            r14 = r27
            r12 = r26
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            return
        L_0x0052:
            r0 = move-exception
            java.lang.RuntimeException r0 = X.C117315Zl.A0J(r0)
            throw r0
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123785nw.<init>(X.0t3, X.0m6, X.0m9, X.18L, X.01H, java.lang.String, java.lang.String, java.util.Map, X.01N, X.01N, long):void");
    }

    @Override // X.C124385pJ, X.AbstractC120015fT
    public void A04(JSONObject jSONObject) {
        super.A04(jSONObject);
        jSONObject.put("country_iso_graphql", AnonymousClass1NE.A01.get(Integer.valueOf(this.A03.A0B()).intValue()));
    }
}
