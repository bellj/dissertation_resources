package X;

/* renamed from: X.5Xn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116945Xn {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L;
    public static final AnonymousClass1TK A0M;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("0.4.0.127.0.7");
        A01 = A12;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("1.1", A12);
        A0M = A022;
        AnonymousClass1TK A023 = AnonymousClass1TL.A02("4.1", A022);
        A08 = A023;
        A03 = AnonymousClass1TL.A02("1", A023);
        A04 = AnonymousClass1TL.A02("2", A023);
        A05 = AnonymousClass1TL.A02("3", A023);
        A06 = AnonymousClass1TL.A02("4", A023);
        A07 = AnonymousClass1TL.A02("5", A023);
        A02 = AnonymousClass1TL.A02("6", A023);
        A00 = AnonymousClass1TL.A02("1", A12);
        AnonymousClass1TK A024 = AnonymousClass1TL.A02("5.1", A022);
        A09 = A024;
        AnonymousClass1TK A025 = AnonymousClass1TL.A02("1", A024);
        A0F = A025;
        A0H = AnonymousClass1TL.A02("1", A025);
        A0I = AnonymousClass1TL.A02("2", A025);
        A0J = AnonymousClass1TL.A02("3", A025);
        A0K = AnonymousClass1TL.A02("4", A025);
        A0L = AnonymousClass1TL.A02("5", A025);
        A0G = AnonymousClass1TL.A02("6", A025);
        AnonymousClass1TK A026 = AnonymousClass1TL.A02("2", A024);
        A0A = A026;
        A0B = AnonymousClass1TL.A02("1", A026);
        A0C = AnonymousClass1TL.A02("2", A026);
        A0D = AnonymousClass1TL.A02("3", A026);
        A0E = AnonymousClass1TL.A02("4", A026);
    }
}
