package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1E9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E9 {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C22320yt A02;
    public final AnonymousClass10U A03;
    public final C18470sV A04;
    public final C22170ye A05;
    public final C22410z2 A06;
    public final C22230yk A07;
    public final C15860o1 A08;

    public AnonymousClass1E9(C14830m7 r1, C14820m6 r2, C22320yt r3, AnonymousClass10U r4, C18470sV r5, C22170ye r6, C22410z2 r7, C22230yk r8, C15860o1 r9) {
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
        this.A07 = r8;
        this.A08 = r9;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A06 = r7;
    }

    public void A00(UserJid userJid, boolean z) {
        boolean z2;
        C15860o1 r2 = this.A08;
        C33181da A08 = r2.A08(userJid.getRawString());
        if (!A08.A0H) {
            A08.A0H = true;
            r2.A0M(A08);
            for (AbstractC41661tt r0 : r2.A0S.A01()) {
                r0.AWW(userJid);
            }
            z2 = true;
        } else {
            z2 = false;
        }
        StringBuilder sb = new StringBuilder("statusmanager/mute-status-user returned ");
        sb.append(z2);
        sb.append(" for ");
        sb.append(userJid);
        Log.i(sb.toString());
        if (z2 && z) {
            this.A06.A03(userJid);
        }
    }

    public void A01(UserJid userJid, boolean z) {
        C15860o1 r3 = this.A08;
        C33181da A08 = r3.A08(userJid.getRawString());
        boolean z2 = false;
        if (A08.A0H) {
            A08.A0H = false;
            r3.A0M(A08);
            for (AbstractC41661tt r0 : r3.A0S.A01()) {
                r0.AWW(userJid);
            }
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("statusmanager/unmute-status-user returned ");
        sb.append(z2);
        sb.append(" for ");
        sb.append(userJid);
        Log.i(sb.toString());
        if (z2 && z) {
            this.A06.A03(userJid);
        }
    }

    public void A02(AbstractC15340mz r9) {
        AnonymousClass10U r1 = this.A03;
        AbstractC14640lm A0B = r9.A0B();
        AnonymousClass009.A05(A0B);
        this.A02.A01(new RunnableBRunnable0Shape0S0310000_I0(this, r1.A01((UserJid) A0B), r9, 10, false), 51);
    }
}
