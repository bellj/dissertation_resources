package X;

import java.util.Comparator;

/* renamed from: X.5Cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC112285Cu implements Comparator {
    @Override // java.util.Comparator
    public abstract int compare(Object obj, Object obj2);

    public static AbstractC95284dR A00(AbstractC95284dR r2, Object obj, int i) {
        return r2.compare(obj, Integer.valueOf(i), natural().reverse());
    }

    public static AbstractC112285Cu from(Comparator comparator) {
        if (comparator instanceof AbstractC112285Cu) {
            return (AbstractC112285Cu) comparator;
        }
        return new C81203td(comparator);
    }

    public static AbstractC112285Cu natural() {
        return C81223tf.INSTANCE;
    }

    public AbstractC112285Cu reverse() {
        return new C81213te(this);
    }
}
