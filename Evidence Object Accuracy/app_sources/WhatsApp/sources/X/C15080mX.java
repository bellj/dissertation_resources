package X;

import android.content.Context;

/* renamed from: X.0mX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15080mX {
    public static C15080mX A01 = new C15080mX();
    public C15090mY A00 = null;

    public static C15090mY A00(Context context) {
        C15090mY r0;
        C15080mX r1 = A01;
        synchronized (r1) {
            r0 = r1.A00;
            if (r0 == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                r0 = new C15090mY(context);
                r1.A00 = r0;
            }
        }
        return r0;
    }
}
