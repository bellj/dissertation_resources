package X;

import java.nio.ByteBuffer;

/* renamed from: X.4Vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92344Vn {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass3IR A03;

    public C92344Vn(AnonymousClass3IR r1, int i, int i2, int i3) {
        this.A03 = r1;
        this.A02 = i;
        this.A00 = i2;
        this.A01 = i3;
    }

    public ByteBuffer A00() {
        ByteBuffer byteBuffer = this.A03.A01;
        ByteBuffer duplicate = byteBuffer.duplicate();
        duplicate.order(byteBuffer.order());
        int i = this.A02;
        duplicate.position(i);
        duplicate.limit(i + this.A00);
        return duplicate;
    }
}
