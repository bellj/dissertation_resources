package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.20I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20I implements AnonymousClass20J {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public final AnonymousClass5XE A0J;
    public final byte[] A0K;
    public final byte[] A0L;

    public AnonymousClass20I() {
        this.A0L = new byte[1];
        this.A0K = new byte[16];
        this.A00 = 0;
        this.A0J = null;
    }

    public AnonymousClass20I(AnonymousClass5XE r3) {
        this.A0L = new byte[1];
        this.A0K = new byte[16];
        this.A00 = 0;
        if (r3.AAt() == 16) {
            this.A0J = r3;
            return;
        }
        throw new IllegalArgumentException("Poly1305 requires a 128 bit block cipher.");
    }

    public static final long A00(int i, int i2) {
        return (((long) i) & 4294967295L) * ((long) i2);
    }

    public final void A01() {
        int i = this.A00;
        if (i < 16) {
            byte[] bArr = this.A0K;
            bArr[i] = 1;
            for (int i2 = 1 + i; i2 < 16; i2++) {
                bArr[i2] = 0;
            }
        }
        byte[] bArr2 = this.A0K;
        long A00 = ((long) AbstractC95434di.A00(bArr2, 0)) & 4294967295L;
        long A002 = ((long) AbstractC95434di.A00(bArr2, 4)) & 4294967295L;
        long A003 = ((long) AbstractC95434di.A00(bArr2, 8)) & 4294967295L;
        long A004 = 4294967295L & ((long) AbstractC95434di.A00(bArr2, 12));
        int i3 = (int) (((long) this.A01) + (A00 & 67108863));
        this.A01 = i3;
        int i4 = (int) (((long) this.A02) + ((((A002 << 32) | A00) >>> 26) & 67108863));
        this.A02 = i4;
        int i5 = (int) (((long) this.A03) + (((A002 | (A003 << 32)) >>> 20) & 67108863));
        this.A03 = i5;
        int i6 = (int) (((long) this.A04) + ((((A004 << 32) | A003) >>> 14) & 67108863));
        this.A04 = i6;
        int i7 = (int) (((long) this.A05) + (A004 >>> 8));
        this.A05 = i7;
        if (i == 16) {
            i7 += EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
            this.A05 = i7;
        }
        int i8 = this.A0A;
        long A005 = A00(i3, i8);
        int i9 = this.A0I;
        int i10 = this.A0H;
        long A006 = A005 + A00(i4, i9) + A00(i5, i10);
        int i11 = this.A0G;
        long A007 = A006 + A00(i6, i11) + A00(i7, this.A0F);
        int i12 = this.A0B;
        long A008 = A00(i3, i12) + A00(i4, i8) + A00(i5, i9) + A00(i6, i10) + A00(i7, i11);
        int i13 = this.A0C;
        long A009 = A00(i3, i13) + A00(i4, i12) + A00(i5, i8) + A00(i6, i9) + A00(i7, i10);
        int i14 = this.A0D;
        long A0010 = A00(i3, i14) + A00(i4, i13) + A00(i5, i12) + A00(i6, i8) + A00(i7, i9);
        long A0011 = A00(i3, this.A0E) + A00(i4, i14) + A00(i5, i13) + A00(i6, i12) + A00(i7, i8);
        int i15 = ((int) A007) & 67108863;
        this.A01 = i15;
        long j = A008 + (A007 >>> 26);
        int i16 = ((int) j) & 67108863;
        this.A02 = i16;
        long j2 = A009 + (j >>> 26);
        this.A03 = ((int) j2) & 67108863;
        long j3 = A0010 + (j2 >>> 26);
        this.A04 = ((int) j3) & 67108863;
        long j4 = A0011 + (j3 >>> 26);
        this.A05 = ((int) j4) & 67108863;
        int i17 = i15 + (((int) (j4 >>> 26)) * 5);
        this.A01 = i17;
        this.A02 = i16 + (i17 >>> 26);
        this.A01 = i17 & 67108863;
    }

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        if (i + 16 <= bArr.length) {
            if (this.A00 > 0) {
                A01();
            }
            int i2 = this.A02;
            int i3 = this.A01;
            int i4 = i2 + (i3 >>> 26);
            this.A02 = i4;
            int i5 = i3 & 67108863;
            this.A01 = i5;
            int i6 = this.A03 + (i4 >>> 26);
            this.A03 = i6;
            int i7 = i4 & 67108863;
            this.A02 = i7;
            int i8 = this.A04 + (i6 >>> 26);
            this.A04 = i8;
            int i9 = i6 & 67108863;
            this.A03 = i9;
            int i10 = this.A05 + (i8 >>> 26);
            this.A05 = i10;
            int i11 = i8 & 67108863;
            this.A04 = i11;
            int i12 = i5 + ((i10 >>> 26) * 5);
            this.A01 = i12;
            int i13 = i10 & 67108863;
            this.A05 = i13;
            int i14 = i7 + (i12 >>> 26);
            this.A02 = i14;
            int i15 = i12 & 67108863;
            this.A01 = i15;
            int i16 = i15 + 5;
            int i17 = (i16 >>> 26) + i14;
            int i18 = (i17 >>> 26) + i9;
            int i19 = (i18 >>> 26) + i11;
            int i20 = 67108863 & i19;
            int i21 = ((i19 >>> 26) + i13) - 67108864;
            int i22 = (i21 >>> 31) - 1;
            int i23 = i22 ^ -1;
            int i24 = (i15 & i23) | (i16 & 67108863 & i22);
            this.A01 = i24;
            int i25 = (i14 & i23) | (i17 & 67108863 & i22);
            this.A02 = i25;
            int i26 = (i9 & i23) | (i18 & 67108863 & i22);
            this.A03 = i26;
            int i27 = (i20 & i22) | (i11 & i23);
            this.A04 = i27;
            int i28 = (i13 & i23) | (i21 & i22);
            this.A05 = i28;
            long j = (((long) (i24 | (i25 << 26))) & 4294967295L) + (((long) this.A06) & 4294967295L);
            long j2 = (((long) ((i25 >>> 6) | (i26 << 20))) & 4294967295L) + (((long) this.A07) & 4294967295L);
            long j3 = (((long) ((i26 >>> 12) | (i27 << 14))) & 4294967295L) + (((long) this.A08) & 4294967295L);
            AbstractC95434di.A02(bArr, (int) j, i);
            long j4 = j2 + (j >>> 32);
            AbstractC95434di.A02(bArr, (int) j4, i + 4);
            long j5 = j3 + (j4 >>> 32);
            AbstractC95434di.A02(bArr, (int) j5, i + 8);
            AbstractC95434di.A02(bArr, (int) ((((long) ((i27 >>> 18) | (i28 << 8))) & 4294967295L) + (4294967295L & ((long) this.A09)) + (j5 >>> 32)), i + 12);
            reset();
            return 16;
        }
        throw new C114975Nu("Output buffer is too short.");
    }

    @Override // X.AnonymousClass20J
    public int AE2() {
        return 16;
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        byte[] bArr = this.A0L;
        bArr[0] = b;
        update(bArr, 0, 1);
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        this.A00 = 0;
        this.A05 = 0;
        this.A04 = 0;
        this.A03 = 0;
        this.A02 = 0;
        this.A01 = 0;
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i2 > i3) {
            int i4 = this.A00;
            if (i4 == 16) {
                A01();
                this.A00 = 0;
                i4 = 0;
            }
            int min = Math.min(i2 - i3, 16 - i4);
            System.arraycopy(bArr, i3 + i, this.A0K, i4, min);
            i3 += min;
            this.A00 += min;
        }
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r7) {
        if (r7 instanceof AnonymousClass20K) {
            byte[] bArr = ((AnonymousClass20K) r7).A00;
            if (bArr.length == 32) {
                int A00 = AbstractC95434di.A00(bArr, 0);
                int A002 = AbstractC95434di.A00(bArr, 4);
                int A003 = AbstractC95434di.A00(bArr, 8);
                int A004 = AbstractC95434di.A00(bArr, 12);
                this.A0A = 67108863 & A00;
                int i = ((A00 >>> 26) | (A002 << 6)) & 67108611;
                this.A0B = i;
                int i2 = ((A002 >>> 20) | (A003 << 12)) & 67092735;
                this.A0C = i2;
                int i3 = ((A003 >>> 14) | (A004 << 18)) & 66076671;
                this.A0D = i3;
                int i4 = (A004 >>> 8) & 1048575;
                this.A0E = i4;
                this.A0F = i * 5;
                this.A0G = i2 * 5;
                this.A0H = i3 * 5;
                this.A0I = i4 * 5;
                this.A06 = AbstractC95434di.A00(bArr, 16);
                this.A07 = AbstractC95434di.A00(bArr, 20);
                this.A08 = AbstractC95434di.A00(bArr, 24);
                this.A09 = AbstractC95434di.A00(bArr, 28);
                reset();
                return;
            }
            throw new IllegalArgumentException("Poly1305 key must be 256 bits.");
        }
        throw new IllegalArgumentException("Poly1305 requires a key.");
    }
}
