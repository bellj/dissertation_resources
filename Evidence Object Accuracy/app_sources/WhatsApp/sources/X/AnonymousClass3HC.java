package X;

/* renamed from: X.3HC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HC {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;

    public static String A00(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int length = str.length();
            if (i >= length) {
                return stringBuffer.toString();
            }
            char charAt = str.charAt(i);
            if (charAt != '\\' || i >= length - 3) {
                stringBuffer.append(charAt);
            } else if (str.charAt(i + 1) == 'r' && str.charAt(i + 2) == '\\') {
                int i2 = i + 3;
                if (str.charAt(i2) == 'n') {
                    stringBuffer.append('\n');
                    i = i2;
                }
            }
            i++;
        }
    }

    public String A01() {
        StringBuilder A0h = C12960it.A0h();
        String str = this.A03;
        if (str != null) {
            A0h.append(str);
        }
        A0h.append(";");
        String str2 = this.A00;
        if (str2 != null) {
            A0h.append(str2);
        }
        A0h.append(";");
        String str3 = this.A02;
        if (str3 != null) {
            A0h.append(str3);
        }
        A0h.append(";");
        String str4 = this.A04;
        if (str4 != null) {
            A0h.append(str4);
        }
        A0h.append(";");
        String str5 = this.A01;
        if (str5 != null) {
            A0h.append(str5);
        }
        return A0h.toString();
    }

    public String toString() {
        String A00;
        StringBuilder A0h = C12960it.A0h();
        String str = this.A03;
        if (str == null) {
            A00 = null;
        } else {
            A00 = A00(str);
        }
        A0h.append(A00);
        A0h.append(" ");
        A0h.append(this.A00);
        A0h.append(" ");
        A0h.append(this.A02);
        A0h.append(" ");
        A0h.append(this.A04);
        A0h.append(" ");
        return C12960it.A0d(this.A01, A0h);
    }
}
