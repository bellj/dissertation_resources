package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.catalogsearch.view.adapter.CatalogSearchResultsListAdapter;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2tx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC59092tx extends AnonymousClass1lK implements AnonymousClass1lN {
    public final C14900mE A00;
    public final AnonymousClass19Q A01;
    public final C15550nR A02;
    public final C22700zV A03;
    public final C15610nY A04;
    public final AnonymousClass018 A05;
    public final String A06;

    public AbstractC59092tx(AnonymousClass12P r8, C14900mE r9, C15570nT r10, AnonymousClass19Q r11, AnonymousClass19T r12, C37071lG r13, C15550nR r14, C22700zV r15, C15610nY r16, AnonymousClass018 r17, UserJid userJid, String str) {
        super(r8, r10, r12, r13, userJid);
        this.A06 = str;
        this.A00 = r9;
        this.A02 = r14;
        this.A04 = r16;
        this.A05 = r17;
        this.A03 = r15;
        this.A01 = r11;
    }

    @Override // X.AnonymousClass1lK
    public AbstractC75723kJ A0F(ViewGroup viewGroup, int i) {
        if (i != 2) {
            return super.A0F(viewGroup, i);
        }
        UserJid userJid = super.A04;
        C15570nT r3 = ((AnonymousClass1lK) this).A01;
        AnonymousClass12P r2 = ((AnonymousClass1lK) this).A00;
        C15550nR r4 = this.A02;
        C15610nY r6 = this.A04;
        return new C59372ua(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.business_product_catalog_list_footer), r2, r3, r4, this.A03, r6, userJid);
    }

    public void A0J(C44671zM r8, List list) {
        AnonymousClass2SK r2;
        int i;
        List list2 = ((AnonymousClass1lL) this).A00;
        list2.clear();
        if (r8 != null && ((i = (r2 = r8.A00).A00) == 1 || i == 2)) {
            list2.add(new C84683zi(r2, r8.A03));
        }
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C44691zO r4 = (C44691zO) it.next();
                if (this instanceof CatalogSearchResultsListAdapter) {
                    C16700pc.A0E(r4, 0);
                }
                if (r4.A01()) {
                    list2.add(new C84693zj(r4, 5, A0E(r4.A0D)));
                }
            }
        }
        list2.add(new C84663zg());
        A04(C12980iv.A0C(list2));
        A02();
    }

    public void A0K(C44691zO r5) {
        boolean z = this instanceof CatalogSearchResultsListAdapter;
        if (r5.A01()) {
            int i = 0;
            while (true) {
                List list = ((AnonymousClass1lL) this).A00;
                if (i < list.size()) {
                    AbstractC89244Jf r2 = (AbstractC89244Jf) list.get(i);
                    if (r2 instanceof C84693zj) {
                        C84693zj r22 = (C84693zj) r2;
                        String str = r22.A01.A0D;
                        String str2 = r5.A0D;
                        if (str.equals(str2)) {
                            r22.A01 = r5;
                            r22.A00 = A0E(str2);
                            A03(i);
                            return;
                        }
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void A0L(Boolean bool) {
        int A0C;
        if (bool == null || !bool.booleanValue()) {
            A0G();
        } else {
            A0H();
        }
        List list = ((AnonymousClass1lL) this).A00;
        if (list.size() > 0 && (list.get(C12980iv.A0C(list)) instanceof C84663zg) && (A0C = C12980iv.A0C(list)) != -1) {
            ((C84663zg) list.get(A0C)).A00 = 5;
        }
    }

    @Override // X.AnonymousClass1lN
    public C90174Mw ABV(int i) {
        boolean z = C12980iv.A0o(((AnonymousClass1lL) this).A00) instanceof C84683zi;
        String str = this.A06;
        if (z) {
            i--;
        }
        return new C90174Mw(str, String.valueOf(i));
    }
}
