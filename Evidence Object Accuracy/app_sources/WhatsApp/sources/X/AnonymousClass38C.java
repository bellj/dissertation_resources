package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.38C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38C extends AbstractC16350or {
    public int A00;
    public AnonymousClass1IR A01;
    public UserJid A02;
    public Runnable A03;
    public final C14830m7 A04;
    public final C15650ng A05;
    public final C21380xK A06;
    public final C18610sj A07;
    public final C17070qD A08;
    public final C20320vZ A09;

    public AnonymousClass38C(C14830m7 r1, C15650ng r2, C21380xK r3, AnonymousClass1IR r4, UserJid userJid, C18610sj r6, C17070qD r7, C20320vZ r8, Runnable runnable, int i) {
        this.A04 = r1;
        this.A06 = r3;
        this.A09 = r8;
        this.A08 = r7;
        this.A05 = r2;
        this.A07 = r6;
        this.A01 = r4;
        this.A02 = userJid;
        this.A00 = i;
        this.A03 = runnable;
    }
}
