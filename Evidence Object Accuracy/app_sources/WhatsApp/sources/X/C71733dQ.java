package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3dQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71733dQ extends ArrayList<AnonymousClass1KZ> {
    public C71733dQ(List list, List list2) {
        super(list.size() + list2.size());
        C38681oX r0 = new C38681oX();
        addAll(list);
        addAll(list2);
        Collections.sort(this, r0);
    }
}
