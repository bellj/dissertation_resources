package X;

/* renamed from: X.4Zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93154Zi {
    public final String A00;

    public C93154Zi(String str) {
        this.A00 = str;
    }

    public static C93154Zi A00(C95304dT r5) {
        String str;
        int A01 = C95304dT.A01(r5, 2);
        int i = A01 >> 1;
        int A0C = ((r5.A0C() >> 3) & 31) | ((A01 & 1) << 5);
        if (i == 4 || i == 5 || i == 7) {
            str = "dvhe";
        } else if (i == 8) {
            str = "hev1";
        } else if (i != 9) {
            return null;
        } else {
            str = "avc3";
        }
        StringBuilder A0j = C12960it.A0j(str);
        String str2 = ".0";
        A0j.append(str2);
        A0j.append(i);
        if (A0C >= 10) {
            str2 = ".";
        }
        return new C93154Zi(C12960it.A0e(str2, A0j, A0C));
    }
}
