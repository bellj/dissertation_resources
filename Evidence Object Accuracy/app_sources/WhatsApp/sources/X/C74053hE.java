package X;

import android.graphics.Bitmap;
import android.webkit.WebChromeClient;

/* renamed from: X.3hE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74053hE extends WebChromeClient {
    public final /* synthetic */ AnonymousClass39H A00;

    public C74053hE(AnonymousClass39H r1) {
        this.A00 = r1;
    }

    @Override // android.webkit.WebChromeClient
    public Bitmap getDefaultVideoPoster() {
        return this.A00.A08[0];
    }
}
