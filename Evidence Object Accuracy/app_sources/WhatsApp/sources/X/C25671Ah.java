package X;

import android.content.SharedPreferences;

/* renamed from: X.1Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25671Ah {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C25671Ah(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("google_migrate_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
