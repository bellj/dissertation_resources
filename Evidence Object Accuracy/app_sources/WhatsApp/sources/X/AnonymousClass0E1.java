package X;

import android.app.SearchableInfo;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.SearchView;
import com.whatsapp.R;
import java.util.WeakHashMap;

/* renamed from: X.0E1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E1 extends AnonymousClass0E2 implements View.OnClickListener {
    public int A00 = -1;
    public int A01 = -1;
    public int A02 = -1;
    public int A03 = 1;
    public int A04 = -1;
    public int A05 = -1;
    public int A06 = -1;
    public ColorStateList A07;
    public final int A08;
    public final SearchableInfo A09;
    public final Context A0A;
    public final SearchView A0B;
    public final WeakHashMap A0C;

    @Override // X.AnonymousClass0BR, android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return false;
    }

    public AnonymousClass0E1(SearchableInfo searchableInfo, Context context, SearchView searchView, WeakHashMap weakHashMap) {
        super(context, searchView.A0T);
        this.A0B = searchView;
        this.A09 = searchableInfo;
        this.A08 = searchView.A0S;
        this.A0A = context;
        this.A0C = weakHashMap;
    }

    public static String A00(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    @Override // X.AnonymousClass0E2, X.AnonymousClass0BR
    public View A03(Context context, Cursor cursor, ViewGroup viewGroup) {
        View A03 = super.A03(context, cursor, viewGroup);
        A03.setTag(new C04890Nl(A03));
        ((ImageView) A03.findViewById(R.id.edit_query)).setImageResource(this.A08);
        return A03;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r11v3, resolved type: android.text.SpannableString */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0127, code lost:
        if (r13 != null) goto L_0x00a4;
     */
    @Override // X.AnonymousClass0BR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(android.view.View r25, android.content.Context r26, android.database.Cursor r27) {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0E1.A04(android.view.View, android.content.Context, android.database.Cursor):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x01df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable A05(java.lang.String r12) {
        /*
        // Method dump skipped, instructions count: 487
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0E1.A05(java.lang.String):android.graphics.drawable.Drawable");
    }

    @Override // X.AnonymousClass0BR, X.AbstractC12540i5
    public void A77(Cursor cursor) {
        try {
            super.A77(cursor);
            if (cursor != null) {
                this.A04 = cursor.getColumnIndex("suggest_text_1");
                this.A05 = cursor.getColumnIndex("suggest_text_2");
                this.A06 = cursor.getColumnIndex("suggest_text_2_url");
                this.A01 = cursor.getColumnIndex("suggest_icon_1");
                this.A02 = cursor.getColumnIndex("suggest_icon_2");
                this.A00 = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    @Override // X.AnonymousClass0BR, X.AbstractC12540i5
    public CharSequence A7k(Cursor cursor) {
        String A00;
        if (cursor != null) {
            String A002 = A00(cursor, cursor.getColumnIndex("suggest_intent_query"));
            if (A002 == null) {
                SearchableInfo searchableInfo = this.A09;
                if (!searchableInfo.shouldRewriteQueryFromData() || (A002 = A00(cursor, cursor.getColumnIndex("suggest_intent_data"))) == null) {
                    if (searchableInfo.shouldRewriteQueryFromText() && (A00 = A00(cursor, cursor.getColumnIndex("suggest_text_1"))) != null) {
                        return A00;
                    }
                }
            }
            return A002;
        }
        return null;
    }

    @Override // X.AnonymousClass0BR, X.AbstractC12540i5
    public Cursor Ab7(CharSequence charSequence) {
        String charSequence2;
        Cursor cursor;
        String suggestAuthority;
        if (charSequence == null) {
            charSequence2 = "";
        } else {
            charSequence2 = charSequence.toString();
        }
        SearchView searchView = this.A0B;
        if (searchView.getVisibility() == 0 && searchView.getWindowVisibility() == 0) {
            try {
                SearchableInfo searchableInfo = this.A09;
                String[] strArr = null;
                if (searchableInfo == null || (suggestAuthority = searchableInfo.getSuggestAuthority()) == null) {
                    cursor = null;
                } else {
                    Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
                    String suggestPath = searchableInfo.getSuggestPath();
                    if (suggestPath != null) {
                        fragment.appendEncodedPath(suggestPath);
                    }
                    fragment.appendPath("search_suggest_query");
                    String suggestSelection = searchableInfo.getSuggestSelection();
                    if (suggestSelection != null) {
                        strArr = new String[]{charSequence2};
                    } else {
                        fragment.appendPath(charSequence2);
                    }
                    fragment.appendQueryParameter("limit", String.valueOf(50));
                    cursor = this.A0A.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
                }
                if (cursor != null) {
                    cursor.getCount();
                    return cursor;
                }
            } catch (RuntimeException e) {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
            }
        }
        return null;
    }

    @Override // X.AnonymousClass0BR, android.widget.BaseAdapter, android.widget.SpinnerAdapter
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View A01 = A01(this.A0A, ((AnonymousClass0BR) this).A02, viewGroup);
            if (A01 != null) {
                ((C04890Nl) A01.getTag()).A03.setText(e.toString());
            }
            return A01;
        }
    }

    @Override // X.AnonymousClass0BR, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View A03 = A03(this.A0A, ((AnonymousClass0BR) this).A02, viewGroup);
            ((C04890Nl) A03.getTag()).A03.setText(e.toString());
            return A03;
        }
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        Bundle extras;
        super.notifyDataSetChanged();
        Cursor cursor = ((AnonymousClass0BR) this).A02;
        if (cursor != null && (extras = cursor.getExtras()) != null) {
            extras.getBoolean("in_progress");
        }
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetInvalidated() {
        Bundle extras;
        super.notifyDataSetInvalidated();
        Cursor cursor = ((AnonymousClass0BR) this).A02;
        if (cursor != null && (extras = cursor.getExtras()) != null) {
            extras.getBoolean("in_progress");
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.A0B.A0E((CharSequence) tag);
        }
    }
}
