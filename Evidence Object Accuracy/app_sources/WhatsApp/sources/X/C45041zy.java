package X;

import java.util.Comparator;

/* renamed from: X.1zy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C45041zy implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return (((AbstractC15340mz) obj).A0I > ((AbstractC15340mz) obj2).A0I ? 1 : (((AbstractC15340mz) obj).A0I == ((AbstractC15340mz) obj2).A0I ? 0 : -1));
    }
}
