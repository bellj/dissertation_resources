package X;

import java.io.EOFException;
import java.io.InputStream;

/* renamed from: X.3eS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C72353eS extends AnonymousClass49A {
    public static final byte[] A02 = new byte[0];
    public int A00;
    public final int A01;

    public C72353eS(InputStream inputStream, int i, int i2) {
        super(inputStream, i2);
        this.A01 = i;
        this.A00 = i;
        if (i == 0) {
            A00();
        }
    }

    public byte[] A01() {
        int i = this.A00;
        if (i == 0) {
            return A02;
        }
        int i2 = super.A00;
        if (i < i2) {
            byte[] bArr = new byte[i];
            InputStream inputStream = super.A01;
            int i3 = 0;
            while (i3 < i) {
                int read = inputStream.read(bArr, 0 + i3, i - i3);
                if (read < 0) {
                    break;
                }
                i3 += read;
            }
            int i4 = i - i3;
            this.A00 = i4;
            if (i4 == 0) {
                A00();
                return bArr;
            }
            StringBuilder A0k = C12960it.A0k("DEF length ");
            A0k.append(this.A01);
            throw new EOFException(C12960it.A0e(" object truncated by ", A0k, i4));
        }
        StringBuilder A0k2 = C12960it.A0k("corrupted stream - out of bounds length found: ");
        A0k2.append(i);
        throw C12990iw.A0i(C12960it.A0e(" >= ", A0k2, i2));
    }

    @Override // java.io.InputStream
    public int read() {
        if (this.A00 == 0) {
            return -1;
        }
        int read = super.A01.read();
        if (read >= 0) {
            int i = this.A00 - 1;
            this.A00 = i;
            if (i != 0) {
                return read;
            }
            A00();
            return read;
        }
        StringBuilder A0k = C12960it.A0k("DEF length ");
        A0k.append(this.A01);
        A0k.append(" object truncated by ");
        throw new EOFException(C12960it.A0f(A0k, this.A00));
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int i3 = this.A00;
        if (i3 == 0) {
            return -1;
        }
        int read = super.A01.read(bArr, i, Math.min(i2, i3));
        if (read >= 0) {
            int i4 = this.A00 - read;
            this.A00 = i4;
            if (i4 != 0) {
                return read;
            }
            A00();
            return read;
        }
        StringBuilder A0k = C12960it.A0k("DEF length ");
        A0k.append(this.A01);
        A0k.append(" object truncated by ");
        throw new EOFException(C12960it.A0f(A0k, this.A00));
    }
}
