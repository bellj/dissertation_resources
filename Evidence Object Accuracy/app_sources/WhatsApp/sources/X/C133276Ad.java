package X;

import java.util.HashMap;

/* renamed from: X.6Ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133276Ad implements AnonymousClass6MT {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ AbstractActivityC121705jc A01;

    public C133276Ad(AnonymousClass3FE r1, AbstractActivityC121705jc r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MT
    public void ARg(C452120p r4) {
        AbstractActivityC121705jc.A0l(this.A00, null, r4.A00);
    }

    @Override // X.AnonymousClass6MT
    public void ARh(C460524g r4) {
        HashMap A11 = C12970iu.A11();
        A11.put("kyc_status", r4.A02);
        this.A00.A01("on_success", A11);
    }
}
