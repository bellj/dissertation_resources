package X;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.2fo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53922fo extends AnonymousClass015 implements AnonymousClass1KY {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();
    public HashSet A02;
    public final AnonymousClass193 A03;

    public C53922fo(AnonymousClass193 r4) {
        this.A03 = r4;
        this.A00.A0B(C12980iv.A0w(0));
        this.A01.A0B(C12980iv.A0w(0));
    }

    public void A04() {
        ArrayList arrayList;
        C37471mS[] r3;
        AnonymousClass016 r8 = this.A01;
        HashSet hashSet = this.A02;
        List<AnonymousClass1KS> A0z = C12980iv.A0z(this.A00);
        if (hashSet == null || A0z == null) {
            arrayList = C12980iv.A0w(0);
        } else {
            arrayList = C12960it.A0l();
            for (AnonymousClass1KS r4 : A0z) {
                AnonymousClass1KB r0 = r4.A04;
                if (r0 != null && (r3 = r0.A08) != null) {
                    int length = r3.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            break;
                        } else if (hashSet.contains(r3[i])) {
                            arrayList.add(r4);
                            break;
                        } else {
                            i++;
                        }
                    }
                }
            }
        }
        r8.A0B(arrayList);
    }

    @Override // X.AnonymousClass1KY
    public void AVQ(AnonymousClass4VM r5) {
        List list = r5.A01;
        HashSet hashSet = new HashSet(list.size());
        for (int i = 0; i < list.size(); i++) {
            hashSet.add(list.get(i));
        }
        this.A02 = hashSet;
        A04();
    }
}
