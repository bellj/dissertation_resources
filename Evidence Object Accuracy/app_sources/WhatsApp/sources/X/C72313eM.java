package X;

import java.util.List;

/* renamed from: X.3eM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72313eM extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public final /* synthetic */ List $delimitersList;
    public final /* synthetic */ boolean $ignoreCase = false;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72313eM(List list) {
        super(2);
        this.$delimitersList = list;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0081, code lost:
        if (r8 == r7) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return null;
     */
    @Override // X.AnonymousClass5ZQ
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object AJ5(java.lang.Object r12, java.lang.Object r13) {
        /*
            r11 = this;
            java.lang.CharSequence r12 = (java.lang.CharSequence) r12
            int r3 = X.C12960it.A05(r13)
            r2 = 0
            X.C16700pc.A0E(r12, r2)
            java.util.List r10 = r11.$delimitersList
            boolean r9 = r11.$ignoreCase
            if (r9 != 0) goto L_0x0040
            int r1 = r10.size()
            r0 = 1
            if (r1 != r0) goto L_0x0040
            java.lang.Object r1 = X.AnonymousClass01Y.A01(r10)
            java.lang.String r1 = (java.lang.String) r1
            int r0 = X.AnonymousClass03B.A03(r12, r1, r3)
            if (r0 < 0) goto L_0x005a
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            X.0qw r2 = new X.0qw
            r2.<init>(r0, r1)
        L_0x002c:
            java.lang.Object r1 = r2.first
            java.lang.Object r0 = r2.second
            java.lang.String r0 = (java.lang.String) r0
            int r0 = r0.length()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            X.0qw r2 = new X.0qw
            r2.<init>(r1, r0)
            return r2
        L_0x0040:
            if (r3 >= r2) goto L_0x0043
            r3 = 0
        L_0x0043:
            int r0 = r12.length()
            X.5Kc r1 = new X.5Kc
            r1.<init>(r3, r0)
            boolean r0 = r12 instanceof java.lang.String
            int r8 = r1.A00
            int r7 = r1.A01
            int r6 = r1.A02
            if (r0 == 0) goto L_0x005c
            if (r6 <= 0) goto L_0x005a
            if (r8 <= r7) goto L_0x0085
        L_0x005a:
            r2 = 0
            return r2
        L_0x005c:
            if (r6 <= 0) goto L_0x005a
            if (r8 <= r7) goto L_0x0061
            goto L_0x005a
        L_0x0061:
            int r3 = r8 + r6
            java.util.Iterator r2 = r10.iterator()
        L_0x0067:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0081
            java.lang.Object r5 = r2.next()
            r1 = r5
            java.lang.String r1 = (java.lang.String) r1
            int r0 = r1.length()
            boolean r0 = X.AnonymousClass03B.A0H(r1, r12, r8, r0, r9)
            if (r0 == 0) goto L_0x0067
            if (r5 == 0) goto L_0x0081
            goto L_0x00a7
        L_0x0081:
            if (r8 == r7) goto L_0x005a
            r8 = r3
            goto L_0x0061
        L_0x0085:
            int r4 = r8 + r6
            java.util.Iterator r3 = r10.iterator()
        L_0x008b:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00b2
            java.lang.Object r5 = r3.next()
            r2 = r5
            java.lang.String r2 = (java.lang.String) r2
            r1 = r12
            java.lang.String r1 = (java.lang.String) r1
            int r0 = r2.length()
            boolean r0 = X.AnonymousClass03C.A0M(r2, r1, r8, r0, r9)
            if (r0 == 0) goto L_0x008b
            if (r5 == 0) goto L_0x00b2
        L_0x00a7:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            X.0qw r2 = new X.0qw
            r2.<init>(r0, r5)
            goto L_0x002c
        L_0x00b2:
            if (r8 == r7) goto L_0x005a
            r8 = r4
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C72313eM.AJ5(java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
