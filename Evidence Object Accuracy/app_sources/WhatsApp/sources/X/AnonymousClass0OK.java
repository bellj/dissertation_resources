package X;

import android.util.SparseArray;

/* renamed from: X.0OK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OK {
    public int A00 = 0;
    public SparseArray A01 = new SparseArray();

    public final C04810Nd A00(int i) {
        SparseArray sparseArray = this.A01;
        C04810Nd r0 = (C04810Nd) sparseArray.get(i);
        if (r0 != null) {
            return r0;
        }
        C04810Nd r02 = new C04810Nd();
        sparseArray.put(i, r02);
        return r02;
    }
}
