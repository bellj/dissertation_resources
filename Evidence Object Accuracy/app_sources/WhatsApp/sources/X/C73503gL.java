package X;

import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.util.Log;

/* renamed from: X.3gL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73503gL extends ClickableSpan {
    public final Intent A00;

    public C73503gL(Intent intent) {
        this.A00 = intent;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        StringBuilder A0k = C12960it.A0k("activity-intent-span/go intent=");
        Intent intent = this.A00;
        Log.i(C12970iu.A0s(intent, A0k));
        view.getContext().startActivity(intent);
    }
}
