package X;

/* renamed from: X.21N  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass21N extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public AnonymousClass21N() {
        super(1502, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(5, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(6, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamUserActivitySessionSummary {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userActivityDuration", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userActivityForeground", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userActivitySessionsLength", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userActivityStartTime", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userActivityTimeChange", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "userSessionSummarySequence", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
