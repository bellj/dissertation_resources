package X;

/* renamed from: X.1qp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC39961qp {
    A0A(0),
    A06(1),
    A09(2),
    A05(3),
    A02(4),
    A03(5),
    A07(6),
    A04(7),
    A08(8),
    A01(9),
    A0C(10),
    A0B(11);
    
    public final int value;

    EnumC39961qp(int i) {
        this.value = i;
    }
}
