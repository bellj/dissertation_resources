package X;

import android.content.Context;
import android.widget.ImageView;
import java.util.List;

/* renamed from: X.5kW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121935kW extends C118645c9 {
    public C121935kW(Context context, AnonymousClass018 r2, C15650ng r3, AnonymousClass1In r4, C30931Zj r5, AnonymousClass6LY r6, C129795yJ r7, AnonymousClass14X r8, List list, int i) {
        super(context, r2, r3, r4, r5, r6, r7, r8, list, i);
    }

    @Override // X.C118645c9
    public void A0F(C118735cI r3, int i) {
        super.ANF(r3, i);
        ImageView imageView = ((C121915kU) r3).A00;
        int i2 = 8;
        if (i == 0) {
            i2 = 0;
        }
        imageView.setVisibility(i2);
    }
}
