package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.5f0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119725f0 extends AbstractC28901Pl {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(3);

    @Override // X.AbstractC28901Pl
    public int A04() {
        return 0;
    }

    @Override // X.AbstractC28901Pl
    public void A06(int i) {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C119725f0(Parcel parcel) {
        A09(parcel);
    }

    public C119725f0(String str) {
        A0A(str);
    }
}
