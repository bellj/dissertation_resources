package X;

/* renamed from: X.4C6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C6 extends Exception {
    public Throwable _underlyingException;

    public AnonymousClass4C6(String str, Throwable th) {
        super(str);
        this._underlyingException = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this._underlyingException;
    }

    public static AnonymousClass4C6 A00(String str, Throwable th) {
        return new AnonymousClass4C6(str, th);
    }
}
