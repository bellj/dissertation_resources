package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Rm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67433Rm implements AbstractC009404s {
    public final C16170oZ A00;
    public final C20670w8 A01;
    public final C14650lo A02;
    public final C25831Az A03;
    public final C48202Ev A04;
    public final AnonymousClass2EM A05;
    public final AnonymousClass19Q A06;
    public final AnonymousClass2E4 A07;
    public final C22700zV A08;
    public final C15610nY A09;
    public final C14830m7 A0A;
    public final UserJid A0B;
    public final C19840ul A0C;
    public final AbstractC14440lR A0D;

    public C67433Rm(C16170oZ r1, C20670w8 r2, C14650lo r3, C25831Az r4, C48202Ev r5, AnonymousClass2EM r6, AnonymousClass19Q r7, AnonymousClass2E4 r8, C22700zV r9, C15610nY r10, C14830m7 r11, UserJid userJid, C19840ul r13, AbstractC14440lR r14) {
        this.A0A = r11;
        this.A0B = userJid;
        this.A00 = r1;
        this.A01 = r2;
        this.A0C = r13;
        this.A05 = r6;
        this.A09 = r10;
        this.A07 = r8;
        this.A03 = r4;
        this.A08 = r9;
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A0D = r14;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C14830m7 r11 = this.A0A;
        UserJid userJid = this.A0B;
        C16170oZ r1 = this.A00;
        AnonymousClass2EM r6 = this.A05;
        C20670w8 r2 = this.A01;
        C19840ul r13 = this.A0C;
        AnonymousClass2E4 r8 = this.A07;
        C15610nY r10 = this.A09;
        C25831Az r4 = this.A03;
        C22700zV r9 = this.A08;
        return new C37041lD(r1, r2, this.A02, r4, this.A04, r6, this.A06, r8, r9, r10, r11, userJid, r13, this.A0D);
    }
}
