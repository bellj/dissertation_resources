package X;

/* renamed from: X.4wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107134wq implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public int A02 = 0;
    public long A03;
    public long A04;
    public C100614mC A05;
    public AnonymousClass5X6 A06;
    public String A07;
    public boolean A08;
    public final C95054d0 A09;
    public final C95304dT A0A;
    public final String A0B;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107134wq(String str) {
        C95054d0 r0 = new C95054d0(new byte[128], 128);
        this.A09 = r0;
        this.A0A = new C95304dT(r0.A03);
        this.A0B = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:138:0x029a  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x02aa  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r24) {
        /*
        // Method dump skipped, instructions count: 734
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107134wq.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r2, C92824Xo r3) {
        r3.A03();
        this.A07 = r3.A02();
        this.A06 = C92824Xo.A00(r2, r3);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A04 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A02 = 0;
        this.A00 = 0;
        this.A08 = false;
    }
}
