package X;

import com.whatsapp.SimpleExternalStorageStateCallback$PermissionDeniedDialogFragment;
import com.whatsapp.SimpleExternalStorageStateCallback$SDCardUnavailableDialogFragment;

/* renamed from: X.55v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1105155v implements AbstractC32851cq {
    public final ActivityC000900k A00;

    public C1105155v(ActivityC000900k r1) {
        this.A00 = r1;
    }

    public static void A00(ActivityC000900k r2) {
        C004902f r22 = new C004902f(r2.A0V());
        r22.A09(new SimpleExternalStorageStateCallback$SDCardUnavailableDialogFragment(), null);
        r22.A02();
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        A00(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        C004902f r2 = new C004902f(this.A00.A0V());
        r2.A09(new SimpleExternalStorageStateCallback$PermissionDeniedDialogFragment(), null);
        r2.A02();
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        A00(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        C004902f r2 = new C004902f(this.A00.A0V());
        r2.A09(new SimpleExternalStorageStateCallback$PermissionDeniedDialogFragment(), null);
        r2.A02();
    }
}
