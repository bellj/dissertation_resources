package X;

import android.content.Intent;

/* renamed from: X.1ib  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35741ib {
    public static void A00(Intent intent, String str) {
        if (!intent.hasExtra("perf_start_time_ns")) {
            intent.putExtra("perf_start_time_ns", System.nanoTime());
        }
        if (!intent.hasExtra("perf_origin")) {
            intent.putExtra("perf_origin", str);
        }
    }

    public static void A01(Intent intent, String str) {
        if (!intent.hasExtra("perf_origin")) {
            intent.putExtra("perf_origin", str);
        }
    }
}
