package X;

import android.util.Pair;

/* renamed from: X.4vK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106204vK implements AnonymousClass5SD {
    public final /* synthetic */ AnonymousClass28D A00;

    public C106204vK(AnonymousClass28D r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5SD
    public Pair Aal() {
        return new Pair(this.A00, null);
    }
}
