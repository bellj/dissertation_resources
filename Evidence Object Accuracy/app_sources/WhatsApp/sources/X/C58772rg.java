package X;

import java.util.Map;

/* renamed from: X.2rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58772rg extends AnonymousClass29N {
    public Map A00;
    public final C65113Ie A01;
    public final AnonymousClass3C0 A02;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C58772rg(X.C65113Ie r11, X.AnonymousClass3C0 r12) {
        /*
            r10 = this;
            X.1zY r0 = r11.A08
            java.lang.String r2 = r0.A0E
            long r3 = r11.A05
            org.json.JSONObject r1 = r11.A0B
            r7 = 0
            if (r1 != 0) goto L_0x0018
            r9 = 0
        L_0x000c:
            r5 = -1
            r1 = r10
            r8 = 0
            r1.<init>(r2, r3, r5, r7, r8, r9)
            r10.A02 = r12
            r10.A01 = r11
            return
        L_0x0018:
            java.lang.String r0 = "encryptedBackupEnabled"
            boolean r9 = r1.optBoolean(r0, r7)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C58772rg.<init>(X.3Ie, X.3C0):void");
    }
}
