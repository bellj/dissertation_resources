package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4uO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105644uO implements AbstractC009404s {
    public final int A00;
    public final C15450nH A01;
    public final AnonymousClass19T A02;
    public final C68263Us A03;
    public final AnonymousClass4E0 A04;
    public final C18640sm A05;
    public final C14820m6 A06;
    public final UserJid A07;

    public C105644uO(C15450nH r1, AnonymousClass19T r2, C68263Us r3, AnonymousClass4E0 r4, C18640sm r5, C14820m6 r6, UserJid userJid, int i) {
        this.A04 = r4;
        this.A07 = userJid;
        this.A01 = r1;
        this.A03 = r3;
        this.A05 = r5;
        this.A00 = i;
        this.A02 = r2;
        this.A06 = r6;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass4E0 r4 = this.A04;
        UserJid userJid = this.A07;
        return new C53912fi(this.A01, this.A02, this.A03, r4, this.A05, this.A06, userJid, this.A00);
    }
}
