package X;

/* renamed from: X.3Fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64323Fc {
    public int A00 = -1;
    public String A01;
    public boolean A02;
    public final ActivityC13810kN A03;
    public final C14820m6 A04;

    public C64323Fc(ActivityC13810kN r2, C14820m6 r3) {
        this.A03 = r2;
        this.A04 = r3;
    }

    public void A00() {
        this.A02 = false;
        AnonymousClass23M.A00 = this.A04.A00.getString("registration_failure_reason", "");
        String str = this.A01;
        if (str != null) {
            this.A03.Adp(str);
        }
        int i = this.A00;
        if (i != -1) {
            C36021jC.A01(this.A03, i);
        }
        this.A01 = null;
        this.A00 = -1;
    }

    public void A01(int i) {
        if (!this.A02) {
            C36021jC.A01(this.A03, i);
        } else {
            this.A00 = i;
        }
    }

    public void A02(int i) {
        if (!this.A02) {
            ActivityC13810kN r1 = this.A03;
            if (!r1.isFinishing()) {
                r1.Ado(i);
                return;
            }
        }
        ActivityC13810kN r0 = this.A03;
        r0.getString(i);
        this.A01 = r0.getString(i);
    }

    public void A03(String str) {
        if (!this.A02) {
            ActivityC13810kN r1 = this.A03;
            if (!r1.isFinishing()) {
                r1.Adp(str);
                return;
            }
        }
        this.A01 = str;
    }
}
