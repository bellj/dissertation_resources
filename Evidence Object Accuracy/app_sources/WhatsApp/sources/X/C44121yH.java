package X;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.util.Log;

/* renamed from: X.1yH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44121yH extends AnonymousClass015 {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final AnonymousClass017 A01;
    public final AnonymousClass02P A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass016 A04;
    public final C14820m6 A05;
    public final C16490p7 A06;
    public final C26991Fp A07;
    public final C44411ys A08;
    public final C44131yI A09;
    public final C27011Fr A0A;
    public final C27001Fq A0B;
    public final Runnable A0C = new RunnableBRunnable0Shape11S0100000_I0_11(this, 3);

    public C44121yH(C14900mE r28, C15570nT r29, C17050qB r30, C14820m6 r31, C14950mJ r32, C19490uC r33, C15880o3 r34, C20850wQ r35, C16490p7 r36, C27021Fs r37, C19890uq r38, C20740wF r39, C25651Af r40, C18350sJ r41, C26991Fp r42, C27011Fr r43, C27001Fq r44, AnonymousClass15E r45, C15860o1 r46, AbstractC15850o0 r47, C15830ny r48, AbstractC14440lR r49) {
        AnonymousClass02P r4 = new AnonymousClass02P();
        this.A02 = r4;
        C44411ys r3 = new C44411ys(this);
        this.A08 = r3;
        AnonymousClass016 r2 = new AnonymousClass016();
        this.A03 = r2;
        this.A06 = r36;
        this.A05 = r31;
        this.A07 = r42;
        this.A0A = r43;
        this.A0B = r44;
        C44131yI r7 = new C44131yI(r28, r29, r30, r32, r33, r34, r35, r37, r38, r39, r40, r41, r42, this, r45, r46, r47, r48, r49);
        this.A09 = r7;
        AnonymousClass016 r1 = ((AbstractC44141yJ) r7).A00;
        this.A01 = r1;
        r4.A0D(r1, new AnonymousClass02B() { // from class: X.4tS
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C44121yH r12 = C44121yH.this;
                if (C12960it.A05(obj) == 0) {
                    r12.A02.A0B(C12980iv.A0i());
                }
            }
        });
        r4.A0D(r2, new AnonymousClass02B() { // from class: X.4tR
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass02P.this.A0B(obj);
            }
        });
        this.A04 = new AnonymousClass016();
        r44.A03(r3);
        A04();
    }

    @Override // X.AnonymousClass015
    public void A03() {
        Log.i("DirectTransferBackgroundTaskViewModel/removeAllListener");
        this.A00.removeCallbacks(this.A0C);
        AnonymousClass02P r1 = this.A02;
        r1.A0C(this.A03);
        r1.A0C(this.A01);
        this.A0B.A04(this.A08);
    }

    public final void A04() {
        C16490p7 r0 = this.A06;
        r0.A04();
        if (r0.A01) {
            Log.i("DirectTransferBackgroundTaskViewModel/msg-store-is-already-ready");
            A07(this.A05.A00.getInt("migrate_from_other_app_attempt_count", 0), 2);
            A06(6);
            return;
        }
        SharedPreferences sharedPreferences = this.A05.A00;
        sharedPreferences.edit().putInt("migrate_from_other_app_attempt_count", sharedPreferences.getInt("migrate_from_other_app_attempt_count", 0) + 1).apply();
        A05();
        Integer num = 1;
        if (!num.equals(this.A03.A01())) {
            Log.i("DirectTransferBackgroundTaskViewModel/startBackgroundTaskAfterOtherAppIsLoggedOut/start-background-task");
            A06(1);
            C44131yI r1 = this.A09;
            if (!r1.A0C.A0F()) {
                ((AbstractC44141yJ) r1).A00.A0A(0);
            } else {
                r1.A00();
            }
        }
    }

    public final void A05() {
        int i = this.A05.A00.getInt("direct_db_migration_timeout_in_secs", 180);
        StringBuilder sb = new StringBuilder("DirectTransferBackgroundTaskViewModel/setupTimeout/timeout =");
        long j = ((long) i) * 1000;
        sb.append(j);
        Log.i(sb.toString());
        if (i > 0) {
            this.A00.postDelayed(this.A0C, j);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void A06(int i) {
        switch (i) {
            case 1:
            case 2:
            case 7:
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                if (this.A05.A00.getInt("migrate_from_other_app_attempt_count", 0) >= 3) {
                    A06(7);
                    return;
                }
                break;
            case 8:
                return;
            default:
                StringBuilder sb = new StringBuilder("state is not supported, state  = ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
        }
        this.A03.A0A(Integer.valueOf(i));
    }

    public final void A07(int i, int i2) {
        if (i == 1) {
            this.A07.A01.A04 = Integer.valueOf(i2);
        } else if (i == 2) {
            this.A07.A01.A07 = Integer.valueOf(i2);
        } else if (i == 3) {
            this.A07.A01.A08 = Integer.valueOf(i2);
        }
    }
}
