package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59272uH extends AnonymousClass32K {
    public final AbstractC15710nm A00;
    public final C14650lo A01;
    public final AnonymousClass2EK A02;
    public final C19830uk A03;
    public final AnonymousClass3EQ A04;
    public final C18640sm A05;
    public final C15680nj A06;
    public final C19870uo A07;
    public final C17220qS A08;
    public final C19840ul A09;
    public final C19860un A0A;

    public C59272uH(AbstractC15710nm r9, C14650lo r10, AnonymousClass2EK r11, C19810ui r12, C17560r0 r13, AnonymousClass4RQ r14, C17570r1 r15, C19830uk r16, AnonymousClass3EQ r17, C18640sm r18, C15680nj r19, C19870uo r20, C17220qS r21, C19840ul r22, C19860un r23, AbstractC14440lR r24) {
        super(r12, r13, r14, r15, r24, 3);
        this.A02 = r11;
        this.A09 = r22;
        this.A01 = r10;
        this.A00 = r9;
        this.A04 = r17;
        this.A03 = r16;
        this.A06 = r19;
        this.A08 = r21;
        this.A0A = r23;
        this.A05 = r18;
        this.A07 = r20;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r8, JSONObject jSONObject, int i) {
        A04(new C90834Pk(2), null, "/onErrorResponse", i, r8.A00, true);
    }

    public final void A04(C90834Pk r5, Exception exc, String str, int i, int i2, boolean z) {
        Log.e("GetProductListGraphQLService/onError/response-error");
        this.A09.A02("plm_details_view_tag");
        if (!A03(this.A04.A00, i2, z)) {
            String A0d = C12960it.A0d(str, C12960it.A0j("GetProductListGraphQLService"));
            if (exc != null) {
                Log.e(A0d, exc);
            } else {
                Log.e(A0d);
            }
            this.A02.A02(r5);
            this.A00.AaV(C12960it.A0d(str, C12960it.A0j("GetProductListGraphQLService")), C12960it.A0W(i, "error_code="), true);
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        A04(new C90834Pk(3), iOException, "/onDeliveryFailure", -1, -1, false);
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("GetProductListGraphQLService/onDirectConnectionError/jid=")));
        this.A09.A02("plm_details_view_tag");
        AnonymousClass2EK.A00(this.A02, 2);
        this.A00.AaV("GetProductListGraphQLService/get product list error - direct connection failed", C12960it.A0f(C12960it.A0k("error_code="), 422), true);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        A04(new C90834Pk(2), exc, "/onError", 0, 0, false);
    }
}
