package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZE implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100314li();
    public int A00;
    public int A01;
    public String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZE(int i, String str, int i2) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = i2;
    }

    public AnonymousClass1ZE(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
    }
}
