package X;

import android.text.Editable;

/* renamed from: X.363  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass363 extends C469928m {
    public final /* synthetic */ AbstractC471028y A00;
    public final /* synthetic */ AnonymousClass290 A01;

    public AnonymousClass363(AbstractC471028y r1, AnonymousClass290 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        AbstractC471028y r4 = this.A00;
        AbstractC36671kL.A07(r4.getContext(), r4.A08.getPaint(), editable, r4.A05, 1.3f);
        this.A01.AA0(r4.A08.getPaint(), editable, r4.A08.getWidth());
    }
}
