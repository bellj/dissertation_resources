package X;

/* renamed from: X.40A  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass40A extends AnonymousClass4UW {
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof AnonymousClass40A);
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("MoreChip(isSelected=");
        A0k.append(false);
        return C12970iu.A0u(A0k);
    }
}
