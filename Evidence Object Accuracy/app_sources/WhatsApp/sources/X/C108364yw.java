package X;

import android.content.Context;
import android.os.Bundle;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.IGmsServiceBroker;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108364yw implements AnonymousClass5XO, AbstractC15020mQ {
    public int A00;
    public C56492ky A01 = null;
    public final Context A02;
    public final C471929k A03;
    public final AbstractC77683ng A04;
    public final C77733nl A05;
    public final HandlerC79083q1 A06;
    public final AbstractC116625We A07;
    public final AnonymousClass3BW A08;
    public final Map A09;
    public final Map A0A = C12970iu.A11();
    public final Map A0B;
    public final Condition A0C;
    public final Lock A0D;
    public volatile AnonymousClass5XN A0E;

    @Override // X.AnonymousClass5XO
    public final void Agg() {
    }

    @Override // X.AnonymousClass5XO
    public final boolean Agi(AnonymousClass5QY r2) {
        return false;
    }

    public C108364yw(Context context, Looper looper, C471929k r6, AbstractC77683ng r7, C77733nl r8, AbstractC116625We r9, AnonymousClass3BW r10, ArrayList arrayList, Map map, Map map2, Lock lock) {
        this.A02 = context;
        this.A0D = lock;
        this.A03 = r6;
        this.A09 = map;
        this.A08 = r10;
        this.A0B = map2;
        this.A04 = r7;
        this.A05 = r8;
        this.A07 = r9;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((C108214yh) arrayList.get(i)).A00 = this;
        }
        this.A06 = new HandlerC79083q1(looper, this);
        this.A0C = lock.newCondition();
        this.A0E = new C108314yr(this);
    }

    public final void A00(C56492ky r3) {
        Lock lock = this.A0D;
        lock.lock();
        try {
            this.A01 = r3;
            this.A0E = new C108314yr(this);
            this.A0E.AgT();
            this.A0C.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AnonymousClass5XO
    public final AnonymousClass1UI AgV(AnonymousClass1UI r2) {
        r2.A04();
        this.A0E.AgM(r2);
        return r2;
    }

    @Override // X.AnonymousClass5XO
    public final AnonymousClass1UI AgY(AnonymousClass1UI r2) {
        r2.A04();
        return this.A0E.AgO(r2);
    }

    @Override // X.AnonymousClass5XO
    public final void Agd() {
        this.A0E.AgW();
    }

    @Override // X.AnonymousClass5XO
    public final void Age() {
        this.A0E.Agc();
        this.A0A.clear();
    }

    @Override // X.AnonymousClass5XO
    public final void Agf(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        IInterface iInterface;
        IGmsServiceBroker iGmsServiceBroker;
        String str2;
        String str3;
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.A0E);
        Iterator A10 = C72453ed.A10(this.A0B);
        while (A10.hasNext()) {
            AnonymousClass1UE r2 = (AnonymousClass1UE) A10.next();
            printWriter.append((CharSequence) str).append((CharSequence) r2.A02).println(":");
            Object obj = this.A09.get(r2.A01);
            C13020j0.A01(obj);
            AbstractC95064d1 r7 = (AbstractC95064d1) ((AbstractC72443eb) obj);
            synchronized (r7.A0M) {
                i = r7.A02;
                iInterface = r7.A06;
            }
            synchronized (r7.A0N) {
                iGmsServiceBroker = r7.A09;
            }
            printWriter.append((CharSequence) concat).append("mConnectState=");
            if (i == 1) {
                str2 = "DISCONNECTED";
            } else if (i == 2) {
                str2 = "REMOTE_CONNECTING";
            } else if (i == 3) {
                str2 = "LOCAL_CONNECTING";
            } else if (i == 4) {
                str2 = "CONNECTED";
            } else if (i != 5) {
                str2 = "UNKNOWN";
            } else {
                str2 = "DISCONNECTING";
            }
            printWriter.print(str2);
            printWriter.append(" mService=");
            if (iInterface == null) {
                printWriter.append("null");
            } else {
                printWriter.append((CharSequence) r7.A05()).append("@").append((CharSequence) C72453ed.A0o(iInterface.asBinder()));
            }
            printWriter.append(" mServiceBroker=");
            if (iGmsServiceBroker == null) {
                printWriter.println("null");
            } else {
                printWriter.append("IGmsServiceBroker@").println(C72453ed.A0o(iGmsServiceBroker.asBinder()));
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
            if (r7.A04 > 0) {
                PrintWriter append = printWriter.append((CharSequence) concat).append("lastConnectedTime=");
                long j = r7.A04;
                String format = simpleDateFormat.format(new Date(j));
                StringBuilder A0t = C12980iv.A0t(C12970iu.A07(format) + 21);
                A0t.append(j);
                A0t.append(" ");
                append.println(C12960it.A0d(format, A0t));
            }
            if (r7.A03 > 0) {
                printWriter.append((CharSequence) concat).append("lastSuspendedCause=");
                int i2 = r7.A00;
                if (i2 == 1) {
                    str3 = "CAUSE_SERVICE_DISCONNECTED";
                } else if (i2 == 2) {
                    str3 = "CAUSE_NETWORK_LOST";
                } else if (i2 != 3) {
                    str3 = String.valueOf(i2);
                } else {
                    str3 = "CAUSE_DEAD_OBJECT_EXCEPTION";
                }
                printWriter.append((CharSequence) str3);
                PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
                long j2 = r7.A03;
                String format2 = simpleDateFormat.format(new Date(j2));
                StringBuilder A0t2 = C12980iv.A0t(C12970iu.A07(format2) + 21);
                A0t2.append(j2);
                A0t2.append(" ");
                append2.println(C12960it.A0d(format2, A0t2));
            }
            if (r7.A05 > 0) {
                printWriter.append((CharSequence) concat).append("lastFailedStatus=").append((CharSequence) AnonymousClass3A7.A00(r7.A01));
                PrintWriter append3 = printWriter.append(" lastFailedTime=");
                long j3 = r7.A05;
                String format3 = simpleDateFormat.format(new Date(j3));
                StringBuilder A0t3 = C12980iv.A0t(C12970iu.A07(format3) + 21);
                A0t3.append(j3);
                A0t3.append(" ");
                append3.println(C12960it.A0d(format3, A0t3));
            }
        }
    }

    @Override // X.AnonymousClass5XO
    public final boolean Agh() {
        return this.A0E instanceof C108304yq;
    }

    @Override // X.AbstractC14990mN
    public final void onConnected(Bundle bundle) {
        Lock lock = this.A0D;
        lock.lock();
        try {
            this.A0E.AgZ(bundle);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC14990mN
    public final void onConnectionSuspended(int i) {
        Lock lock = this.A0D;
        lock.lock();
        try {
            this.A0E.Agb(i);
        } finally {
            lock.unlock();
        }
    }
}
