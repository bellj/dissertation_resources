package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.search.IteratingPlayer;

/* renamed from: X.3jE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75053jE extends AbstractC05270Ox {
    public final /* synthetic */ IteratingPlayer A00;

    public C75053jE(IteratingPlayer iteratingPlayer) {
        this.A00 = iteratingPlayer;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        IteratingPlayer iteratingPlayer = this.A00;
        if (i == 0) {
            iteratingPlayer.A02();
            iteratingPlayer.A03(iteratingPlayer.A01);
            if (!iteratingPlayer.A03) {
                iteratingPlayer.A03 = true;
                iteratingPlayer.A00();
                return;
            }
            return;
        }
        iteratingPlayer.A01();
    }
}
