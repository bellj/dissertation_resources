package X;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.whatsapp.WaInAppBrowsingActivity;

/* renamed from: X.3hD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74043hD extends WebChromeClient {
    public final /* synthetic */ WaInAppBrowsingActivity A00;

    public /* synthetic */ C74043hD(WaInAppBrowsingActivity waInAppBrowsingActivity) {
        this.A00 = waInAppBrowsingActivity;
    }

    @Override // android.webkit.WebChromeClient
    public void onProgressChanged(WebView webView, int i) {
        WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
        ProgressBar progressBar = waInAppBrowsingActivity.A01;
        int i2 = 0;
        if (i == 100) {
            i2 = 8;
        }
        progressBar.setVisibility(i2);
        waInAppBrowsingActivity.A01.setProgress(i);
    }
}
