package X;

import android.view.inputmethod.InputMethodManager;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0cN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09110cN implements Runnable {
    public final /* synthetic */ SearchView.SearchAutoComplete A00;

    public RunnableC09110cN(SearchView.SearchAutoComplete searchAutoComplete) {
        this.A00 = searchAutoComplete;
    }

    @Override // java.lang.Runnable
    public void run() {
        SearchView.SearchAutoComplete searchAutoComplete = this.A00;
        if (searchAutoComplete.A02) {
            ((InputMethodManager) searchAutoComplete.getContext().getSystemService("input_method")).showSoftInput(searchAutoComplete, 0);
            searchAutoComplete.A02 = false;
        }
    }
}
