package X;

import android.content.SharedPreferences;
import java.util.Random;

/* renamed from: X.1Fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26991Fp {
    public final C16120oU A00;
    public final C44021y2 A01;
    public final C27011Fr A02;
    public final Random A03;

    public C26991Fp(C14820m6 r7, C16120oU r8, C27011Fr r9) {
        Random random = new Random();
        this.A03 = random;
        this.A00 = r8;
        this.A02 = r9;
        C44021y2 r4 = new C44021y2();
        this.A01 = r4;
        r4.A04 = 0;
        r4.A07 = 0;
        r4.A08 = 0;
        r4.A06 = 0;
        r4.A05 = 3;
        SharedPreferences sharedPreferences = r7.A00;
        String string = sharedPreferences.getString("direct_migration_session_id", null);
        if (string == null) {
            string = Long.toHexString(random.nextLong());
            sharedPreferences.edit().putString("direct_migration_session_id", string).apply();
        }
        r4.A0B = string;
    }
}
