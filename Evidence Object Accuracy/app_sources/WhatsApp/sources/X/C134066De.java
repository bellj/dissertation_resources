package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.6De  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134066De implements AnonymousClass5Wu {
    public View.OnClickListener A00;
    public ImageView A01;

    public void A00(Integer num) {
        ImageView imageView;
        int i;
        if (!(this instanceof C123715nk)) {
            if (num == null || 2 != num.intValue()) {
                imageView = this.A01;
                i = R.drawable.ic_action_arrow_next;
            } else {
                imageView = this.A01;
                i = R.drawable.input_send;
            }
            imageView.setImageResource(i);
        }
    }

    @Override // X.AnonymousClass5Wu
    public /* bridge */ /* synthetic */ void A6Q(Object obj) {
        if (!(this instanceof C123715nk)) {
            A00((Integer) obj);
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        if (!(this instanceof C123715nk)) {
            return R.layout.shared_payment_entry_action;
        }
        if (!(((C123715nk) this) instanceof C123705nj)) {
            return R.layout.payment_note_entry_next_action;
        }
        return R.layout.payment_entry_next_action;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        if (!(this instanceof C123715nk)) {
            ImageView A0K = C12970iu.A0K(view, R.id.send_payment_send);
            this.A01 = A0K;
            C117295Zj.A0n(A0K, this, 189);
            return;
        }
        C123715nk r2 = (C123715nk) this;
        r2.A00 = view;
        C117295Zj.A0n(view, r2, 190);
        r2.A00.setEnabled(false);
        r2.A00.setClickable(false);
    }
}
