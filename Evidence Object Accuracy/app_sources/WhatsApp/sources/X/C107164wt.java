package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.4wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107164wt implements AnonymousClass5X7 {
    public long A00;
    public long A01;
    public AnonymousClass5X6 A02;
    public AnonymousClass4U7 A03;
    public String A04;
    public boolean A05;
    public boolean A06;
    public final C92814Xn A07 = new C92814Xn(8);
    public final C92814Xn A08 = new C92814Xn(6);
    public final C92814Xn A09 = new C92814Xn(7);
    public final AnonymousClass4VF A0A;
    public final C95304dT A0B = new C95304dT();
    public final boolean[] A0C = new boolean[3];

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107164wt(AnonymousClass4VF r3) {
        this.A0A = r3;
    }

    @Override // X.AnonymousClass5X7
    public void A7a(C95304dT r26) {
        AnonymousClass5X6 r6 = this.A02;
        C95314dV.A01(r6);
        int i = r26.A01;
        int i2 = r26.A00;
        byte[] bArr = r26.A02;
        int i3 = i2 - i;
        this.A01 += (long) i3;
        r6.AbC(r26, i3);
        while (true) {
            int A01 = C95464dl.A01(bArr, this.A0C, i, i2);
            if (A01 == i2) {
                break;
            }
            int i4 = A01 + 3;
            int i5 = bArr[i4] & 31;
            int i6 = A01 - i;
            if (i6 > 0) {
                if (!this.A05) {
                    this.A09.A01(bArr, i, A01);
                    this.A07.A01(bArr, i, A01);
                }
                this.A08.A01(bArr, i, A01);
            }
            int i7 = i2 - A01;
            long j = this.A01 - ((long) i7);
            int i8 = -i6;
            if (i6 >= 0) {
                i8 = 0;
            }
            long j2 = this.A00;
            if (!this.A05) {
                C92814Xn r10 = this.A09;
                r10.A02(i8);
                C92814Xn r62 = this.A07;
                r62.A02(i8);
                if (r10.A01 && r62.A01) {
                    ArrayList A0l = C12960it.A0l();
                    A0l.add(Arrays.copyOf(r10.A03, r10.A00));
                    A0l.add(Arrays.copyOf(r62.A03, r62.A00));
                    AnonymousClass4TQ A02 = C95464dl.A02(r10.A03, 3, r10.A00);
                    AnonymousClass4YP r2 = new AnonymousClass4YP(r62.A03, 3, r62.A00);
                    r2.A05(8);
                    int A012 = r2.A01();
                    r2.A01();
                    r2.A03();
                    r2.A03();
                    AnonymousClass4IM r12 = new AnonymousClass4IM(A012);
                    int i9 = A02.A04;
                    int i10 = A02.A01;
                    int i11 = A02.A03;
                    Object[] objArr = new Object[3];
                    C12960it.A1O(objArr, i9);
                    C12980iv.A1T(objArr, i10);
                    C12990iw.A1V(objArr, i11);
                    String format = String.format("avc1.%02X%02X%02X", objArr);
                    AnonymousClass5X6 r22 = this.A02;
                    C93844ap A00 = C93844ap.A00();
                    A00.A0O = this.A04;
                    A00.A0R = "video/avc";
                    A00.A0M = format;
                    A00.A0G = A02.A06;
                    A00.A07 = A02.A02;
                    A00.A01 = A02.A00;
                    A00.A0S = A0l;
                    C72453ed.A17(A00, r22);
                    this.A05 = true;
                    AnonymousClass4U7 r23 = this.A03;
                    r23.A09.append(A02.A05, A02);
                    r23.A08.append(r12.A00, r12);
                    r10.A02 = false;
                    r10.A01 = false;
                    r62.A02 = false;
                    r62.A01 = false;
                }
            }
            C92814Xn r63 = this.A08;
            if (r63.A02(i8)) {
                int A002 = C95464dl.A00(r63.A03, r63.A00);
                C95304dT r3 = this.A0B;
                r3.A0U(r63.A03, A002);
                r3.A0S(4);
                C92934Yb.A00(r3, this.A0A.A01, j2);
            }
            AnonymousClass4U7 r102 = this.A03;
            boolean z = this.A05;
            boolean z2 = this.A06;
            boolean z3 = false;
            if (r102.A00 == 9) {
                if (z && r102.A05) {
                    long j3 = r102.A01;
                    boolean z4 = r102.A06;
                    int i12 = (int) (j3 - r102.A03);
                    r102.A0A.AbG(null, z4 ? 1 : 0, i12, i7 + ((int) (j - j3)), r102.A04);
                }
                r102.A03 = r102.A01;
                r102.A04 = r102.A02;
                r102.A06 = false;
                r102.A05 = true;
            }
            boolean z5 = r102.A06;
            int i13 = r102.A00;
            if (i13 == 5 || (z2 && i13 == 1)) {
                z3 = true;
            }
            boolean z6 = z5 | z3;
            r102.A06 = z6;
            if (z6) {
                this.A06 = false;
            }
            long j4 = this.A00;
            if (!this.A05) {
                this.A09.A00(i5);
                this.A07.A00(i5);
            }
            r63.A00(i5);
            AnonymousClass4U7 r24 = this.A03;
            r24.A00 = i5;
            r24.A02 = j4;
            r24.A01 = j;
            i = i4;
        }
        if (!this.A05) {
            this.A09.A01(bArr, i, i2);
            this.A07.A01(bArr, i, i2);
        }
        this.A08.A01(bArr, i, i2);
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r3, C92824Xo r4) {
        r4.A03();
        this.A04 = r4.A02();
        AnonymousClass5X6 Af4 = r3.Af4(r4.A01(), 2);
        this.A02 = Af4;
        this.A03 = new AnonymousClass4U7(Af4);
        this.A0A.A00(r3, r4);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A00 = j;
        this.A06 |= C12960it.A1S(i & 2);
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A01 = 0;
        this.A06 = false;
        boolean A1W = C72453ed.A1W(this.A0C);
        C92814Xn r0 = this.A09;
        r0.A02 = A1W;
        r0.A01 = A1W;
        C92814Xn r02 = this.A07;
        r02.A02 = A1W;
        r02.A01 = A1W;
        C92814Xn r03 = this.A08;
        r03.A02 = A1W;
        r03.A01 = A1W;
        AnonymousClass4U7 r04 = this.A03;
        if (r04 != null) {
            r04.A05 = A1W;
        }
    }
}
