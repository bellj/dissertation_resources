package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.33Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33Z extends AbstractC454421p {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public Drawable A05;
    public final MediaViewBaseFragment A06;

    public AnonymousClass33Z(MediaViewBaseFragment mediaViewBaseFragment) {
        this.A06 = mediaViewBaseFragment;
    }
}
