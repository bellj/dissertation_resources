package X;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.3tS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81093tS<E> extends AbstractC81103tT<E> {
    public static final C81093tS NATURAL_EMPTY_SET = new C81093tS(AnonymousClass1Mr.of(), AbstractC112285Cu.natural());
    public final transient AnonymousClass1Mr elements;

    public C81093tS(AnonymousClass1Mr r1, Comparator comparator) {
        super(comparator);
        this.elements = r1;
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf
    public AnonymousClass1Mr asList() {
        return this.elements;
    }

    @Override // X.AbstractC81103tT, java.util.NavigableSet
    public Object ceiling(Object obj) {
        int tailIndex = tailIndex(obj, true);
        if (tailIndex == size()) {
            return null;
        }
        return this.elements.get(tailIndex);
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return unsafeBinarySearch(obj) >= 0;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: X.3tS<E> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection collection) {
        if (collection instanceof AnonymousClass5Z2) {
            collection = ((AnonymousClass5Z2) collection).elementSet();
        }
        if (!AnonymousClass4Yg.hasSameComparator(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        AnonymousClass1I5 it = iterator();
        Iterator<E> it2 = collection.iterator();
        if (it.hasNext()) {
            E next = it2.next();
            Object next2 = it.next();
            while (true) {
                try {
                    int unsafeCompare = unsafeCompare(next2, next);
                    if (unsafeCompare >= 0) {
                        if (unsafeCompare != 0) {
                            break;
                        } else if (!it2.hasNext()) {
                            return true;
                        } else {
                            next = it2.next();
                        }
                    } else if (!it.hasNext()) {
                        break;
                    } else {
                        next2 = it.next();
                    }
                } catch (ClassCastException | NullPointerException unused) {
                }
            }
        }
        return false;
        return false;
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        return this.elements.copyIntoArray(objArr, i);
    }

    @Override // X.AbstractC81103tT
    public AbstractC81103tT createDescendingSet() {
        Comparator reverseOrder = Collections.reverseOrder(this.comparator);
        if (isEmpty()) {
            return AbstractC81103tT.emptySet(reverseOrder);
        }
        return new C81093tS(this.elements.reverse(), reverseOrder);
    }

    @Override // X.AbstractC81103tT, java.util.NavigableSet
    public AnonymousClass1I5 descendingIterator() {
        return this.elements.reverse().iterator();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030 A[Catch: ClassCastException | NoSuchElementException -> 0x0041, TryCatch #0 {ClassCastException | NoSuchElementException -> 0x0041, blocks: (B:13:0x0026, B:14:0x002a, B:16:0x0030, B:18:0x003a), top: B:26:0x0026 }] */
    @Override // X.AbstractC17940re, java.util.Collection, java.lang.Object, java.util.Set
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 1
            if (r7 == r6) goto L_0x0048
            boolean r0 = r7 instanceof java.util.Set
            r4 = 0
            if (r0 == 0) goto L_0x0047
            java.util.Set r7 = (java.util.Set) r7
            int r1 = r6.size()
            int r0 = r7.size()
            if (r1 != r0) goto L_0x0047
            boolean r0 = r6.isEmpty()
            if (r0 != 0) goto L_0x0048
            java.util.Comparator r0 = r6.comparator
            boolean r0 = X.AnonymousClass4Yg.hasSameComparator(r0, r7)
            if (r0 == 0) goto L_0x0042
            java.util.Iterator r3 = r7.iterator()
            X.1I5 r2 = r6.iterator()     // Catch: ClassCastException | NoSuchElementException -> 0x0041
        L_0x002a:
            boolean r0 = r2.hasNext()     // Catch: ClassCastException | NoSuchElementException -> 0x0041
            if (r0 == 0) goto L_0x0048
            java.lang.Object r1 = r2.next()     // Catch: ClassCastException | NoSuchElementException -> 0x0041
            java.lang.Object r0 = r3.next()     // Catch: ClassCastException | NoSuchElementException -> 0x0041
            if (r0 == 0) goto L_0x0047
            int r0 = r6.unsafeCompare(r1, r0)     // Catch: ClassCastException | NoSuchElementException -> 0x0041
            if (r0 == 0) goto L_0x002a
            return r4
        L_0x0041:
            return r4
        L_0x0042:
            boolean r0 = r6.containsAll(r7)
            return r0
        L_0x0047:
            return r4
        L_0x0048:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C81093tS.equals(java.lang.Object):boolean");
    }

    @Override // X.AbstractC81103tT, java.util.SortedSet
    public Object first() {
        if (!isEmpty()) {
            return this.elements.get(0);
        }
        throw new NoSuchElementException();
    }

    @Override // X.AbstractC81103tT, java.util.NavigableSet
    public Object floor(Object obj) {
        int headIndex = headIndex(obj, true) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.elements.get(headIndex);
    }

    public C81093tS getSubSet(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i < i2) {
            return new C81093tS(this.elements.subList(i, i2), this.comparator);
        }
        return AbstractC81103tT.emptySet(this.comparator);
    }

    public int headIndex(Object obj, boolean z) {
        int binarySearch = Collections.binarySearch(this.elements, obj, comparator());
        if (binarySearch >= 0) {
            return z ? binarySearch + 1 : binarySearch;
        }
        return binarySearch ^ -1;
    }

    @Override // X.AbstractC81103tT
    public AbstractC81103tT headSetImpl(Object obj, boolean z) {
        return getSubSet(0, headIndex(obj, z));
    }

    @Override // X.AbstractC81103tT, java.util.NavigableSet
    public Object higher(Object obj) {
        int tailIndex = tailIndex(obj, false);
        if (tailIndex == size()) {
            return null;
        }
        return this.elements.get(tailIndex);
    }

    @Override // X.AbstractC17950rf
    public Object[] internalArray() {
        return this.elements.internalArray();
    }

    @Override // X.AbstractC17950rf
    public int internalArrayEnd() {
        return this.elements.internalArrayEnd();
    }

    @Override // X.AbstractC17950rf
    public int internalArrayStart() {
        return this.elements.internalArrayStart();
    }

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return this.elements.isPartialView();
    }

    @Override // X.AbstractC81103tT, X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return this.elements.iterator();
    }

    @Override // X.AbstractC81103tT, java.util.SortedSet
    public Object last() {
        if (!isEmpty()) {
            return this.elements.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @Override // X.AbstractC81103tT, java.util.NavigableSet
    public Object lower(Object obj) {
        int headIndex = headIndex(obj, false) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.elements.get(headIndex);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.elements.size();
    }

    @Override // X.AbstractC81103tT
    public AbstractC81103tT subSetImpl(Object obj, boolean z, Object obj2, boolean z2) {
        return tailSetImpl(obj, z).headSetImpl(obj2, z2);
    }

    public int tailIndex(Object obj, boolean z) {
        int binarySearch = Collections.binarySearch(this.elements, obj, comparator());
        if (binarySearch >= 0) {
            return !z ? binarySearch + 1 : binarySearch;
        }
        return binarySearch ^ -1;
    }

    @Override // X.AbstractC81103tT
    public AbstractC81103tT tailSetImpl(Object obj, boolean z) {
        return getSubSet(tailIndex(obj, z), size());
    }

    private int unsafeBinarySearch(Object obj) {
        return Collections.binarySearch(this.elements, obj, unsafeComparator());
    }

    public Comparator unsafeComparator() {
        return this.comparator;
    }
}
