package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1EH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EH {
    public final C16490p7 A00;

    public AnonymousClass1EH(C16490p7 r1) {
        this.A00 = r1;
    }

    public List A00(long j) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id, message_poll_option_id FROM message_add_on_poll_vote_selected_option WHERE message_add_on_row_id = ?", new String[]{Long.toString(j)});
            ArrayList arrayList = new ArrayList();
            if (A09.moveToFirst()) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("message_poll_option_id");
                do {
                    arrayList.add(Long.valueOf(A09.getLong(columnIndexOrThrow)));
                } while (A09.moveToNext());
                A09.close();
                A01.close();
                return arrayList;
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(C16310on r13, C27711Iw r14, long j) {
        AnonymousClass1Lx A00 = r13.A00();
        try {
            ContentValues contentValues = new ContentValues(3);
            Long valueOf = Long.valueOf(j);
            contentValues.put("message_add_on_row_id", valueOf);
            contentValues.put("sender_timestamp", Long.valueOf(r14.A00));
            C16330op r6 = r13.A03;
            r6.A03(contentValues, "message_add_on_poll_vote");
            AnonymousClass1Lx A002 = r13.A00();
            r6.A01("message_add_on_poll_vote_selected_option", "message_add_on_row_id = ?", new String[]{String.valueOf(j)});
            List<Long> list = r14.A05;
            if (!list.isEmpty()) {
                for (Long l : list) {
                    long longValue = l.longValue();
                    ContentValues contentValues2 = new ContentValues(2);
                    contentValues2.put("message_add_on_row_id", valueOf);
                    contentValues2.put("message_poll_option_id", Long.valueOf(longValue));
                    r6.A03(contentValues2, "message_add_on_poll_vote_selected_option");
                }
            }
            A002.A00();
            A002.close();
            A00.A00();
            A00.close();
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
