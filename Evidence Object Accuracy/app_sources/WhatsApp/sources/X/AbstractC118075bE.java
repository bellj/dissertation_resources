package X;

import com.whatsapp.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.5bE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118075bE extends AnonymousClass015 {
    public String A00;
    public final AnonymousClass016 A01;
    public final C15450nH A02;
    public final C14830m7 A03;
    public final C15650ng A04;
    public final AnonymousClass3FW A05 = C117305Zk.A0S();
    public final AbstractC16870pt A06;
    public final C20320vZ A07;
    public final C27691It A08 = C13000ix.A03();

    public AbstractC118075bE(C15450nH r4, C14830m7 r5, C15650ng r6, AbstractC16870pt r7, C20320vZ r8) {
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A01 = A0T;
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
        this.A07 = r8;
        this.A06 = r7;
        A0T.A0B(new C127065tt(1));
    }

    public String A04() {
        if (this instanceof C123455nB) {
            return "report_this_payment_submitted";
        }
        if (this instanceof C123425n8) {
            return "contact_support_integrity_dpo_submitted";
        }
        if (this instanceof C123415n7) {
            return "appeal_request_ack";
        }
        if (!(this instanceof C123405n6)) {
            return !(this instanceof C123445nA) ? "contact_ombudsman_submitted" : "contact_support_submitted_p2p";
        }
        return "contact_support_submitted";
    }

    public String A05() {
        if (this instanceof C123455nB) {
            return "report_this_payment";
        }
        if (this instanceof C123425n8) {
            return "contact_support_integrity_dpo";
        }
        if (this instanceof C123415n7) {
            return "restore_payment";
        }
        if (!(this instanceof C123405n6)) {
            return !(this instanceof C123445nA) ? "contact_ombudsman" : "contact_support_p2p";
        }
        return "contact_support";
    }

    public void A06(String str) {
        AnonymousClass3FW A0S = C117305Zk.A0S();
        C117325Zm.A07(A0S);
        A0S.A00(this.A05);
        A0S.A01("status", str);
        this.A06.AKi(A0S, C12960it.A0V(), 114, A05(), null);
    }

    public void A07(String str) {
        String A03;
        StringBuilder A0k;
        String str2;
        String str3 = str;
        if (str.getBytes().length >= 10) {
            Matcher matcher = Pattern.compile("[a-zA-Z\\u0080-\\u00ff]+").matcher(str);
            int i = 0;
            while (matcher.find()) {
                i++;
                if (i >= 3) {
                    A06("sent");
                    this.A01.A0B(new C127065tt(4));
                    if (!(this instanceof C123435n9)) {
                        A03 = this.A02.A03(AbstractC15460nI.A2K);
                    } else {
                        A03 = ((C123435n9) this).A00.A03(1925);
                    }
                    AnonymousClass009.A05(A03);
                    try {
                        C20320vZ r3 = this.A07;
                        AbstractC14640lm A00 = AbstractC14640lm.A00(A03);
                        String str4 = this.A00;
                        if (this instanceof C123455nB) {
                            AnonymousClass009.A05(str4);
                            A0k = C12960it.A0k("### ");
                        } else if (!(this instanceof C123425n8)) {
                            if (!(this instanceof C123415n7)) {
                                if (this instanceof C123405n6) {
                                    A0k = C12960it.A0h();
                                    if (!AnonymousClass1US.A0C(str4)) {
                                        str2 = "## ";
                                        A0k.append(str2);
                                    }
                                } else if (this instanceof C123445nA) {
                                    A0k = C12960it.A0h();
                                    if (!AnonymousClass1US.A0C(str4)) {
                                        str2 = "###### ";
                                        A0k.append(str2);
                                    }
                                }
                                str3 = C12960it.A0d(str, A0k);
                            } else {
                                str3 = C12960it.A0d(str, C12960it.A0k("#### \n"));
                            }
                            this.A04.A0S(r3.A03(null, A00, null, str3, null, this.A03.A00(), false));
                            return;
                        } else {
                            A0k = C12960it.A0k("##### ");
                            A0k.append('\n');
                            str3 = C12960it.A0d(str, A0k);
                            this.A04.A0S(r3.A03(null, A00, null, str3, null, this.A03.A00(), false));
                            return;
                        }
                        A0k.append(str4);
                        A0k.append('\n');
                        str3 = C12960it.A0d(str, A0k);
                        this.A04.A0S(r3.A03(null, A00, null, str3, null, this.A03.A00(), false));
                        return;
                    } catch (AnonymousClass1MW unused) {
                        Log.e("PAY: BrazilPaymentCareBaseViewModel - failed to send message to Payment Support Brazil JID");
                        return;
                    }
                }
            }
        }
        A06("failed");
        this.A01.A0B(new C127065tt(2));
    }

    public void A08(String str) {
        this.A00 = str;
        this.A05.A01("transaction_id", str);
    }
}
