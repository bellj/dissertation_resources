package X;

import android.graphics.Rect;
import android.transition.Transition;

/* renamed from: X.0Ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02130Ad extends Transition.EpicenterCallback {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass0EI A01;

    public C02130Ad(Rect rect, AnonymousClass0EI r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // android.transition.Transition.EpicenterCallback
    public Rect onGetEpicenter(Transition transition) {
        return this.A00;
    }
}
