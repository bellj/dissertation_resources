package X;

/* renamed from: X.1xC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43541xC extends Exception {
    public static final long serialVersionUID = 1;
    public String banMessage;
    public int code;
    public long expiration_time;
    public int expire_time_out;
    public String faqUrl;
    public final int serverErrorCode;
    public final int type;
    public int violationType;

    public C43541xC(int i, int i2) {
        this.type = i;
        this.serverErrorCode = i2;
    }
}
