package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;

/* renamed from: X.1IT  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1IT extends AnonymousClass1IU implements TextWatcher {
    public float A00;
    public int A01;
    public int A02;
    public Handler A03 = new Handler(Looper.getMainLooper());
    public Runnable A04 = new RunnableBRunnable0Shape9S0100000_I0_9(this, 9);
    public boolean A05 = false;
    public boolean A06;
    public boolean A07;
    public final Paint A08 = new Paint(1);
    public final RectF A09 = new RectF();

    public abstract float A05(String str);

    public abstract void A07(boolean z);

    @Override // android.text.TextWatcher
    public abstract void afterTextChanged(Editable editable);

    @Override // android.text.TextWatcher
    public abstract void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3);

    public abstract int getCursorColor();

    public abstract int getCursorVerticalPadding();

    public abstract int getCursorWidth();

    @Override // android.widget.TextView, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass1IT(Context context) {
        super(context);
        A06();
    }

    public AnonymousClass1IT(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A06();
    }

    public AnonymousClass1IT(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A06();
    }

    public final void A06() {
        this.A02 = getCursorWidth();
        this.A01 = getCursorVerticalPadding();
        Paint paint = this.A08;
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getCursorColor());
    }

    @Override // android.view.View
    public boolean hasFocus() {
        return this.A06;
    }

    @Override // com.whatsapp.WaEditText, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.A05 && this.A07) {
            RectF rectF = this.A09;
            float f = this.A00;
            rectF.set(f, (float) this.A01, ((float) this.A02) + f, (float) (getHeight() - this.A01));
            canvas.drawRect(rectF, this.A08);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        if (getText() == null || TextUtils.isEmpty(getText().toString())) {
            this.A00 = 0.0f;
        } else {
            float A05 = A05(getText().toString());
            this.A00 = A05;
            if (this.A05) {
                i3 = this.A02;
            } else {
                i3 = 0;
            }
            i = View.MeasureSpec.makeMeasureSpec((int) (A05 + ((float) i3) + ((float) getPaddingLeft()) + ((float) getPaddingRight())), 1073741824);
        }
        super.onMeasure(i, i2);
    }

    public void setCustomCursorEnabled(boolean z) {
        if (this.A05 != z) {
            this.A05 = z;
            Handler handler = this.A03;
            Runnable runnable = this.A04;
            handler.removeCallbacks(runnable);
            handler.post(runnable);
            boolean z2 = !z;
            setFocusable(z2);
            setCursorVisible(z2);
            if (z) {
                addTextChangedListener(this);
            } else {
                removeTextChangedListener(this);
            }
        }
    }

    public void setHasFocus(boolean z) {
        if (z != this.A06) {
            this.A06 = z;
            Handler handler = this.A03;
            Runnable runnable = this.A04;
            handler.removeCallbacks(runnable);
            handler.post(runnable);
            A07(z);
        }
    }
}
