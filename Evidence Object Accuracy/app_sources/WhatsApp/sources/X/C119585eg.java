package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;

/* renamed from: X.5eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119585eg extends AbstractC17440qo {
    public final C125135ql A00;

    public C119585eg(C125135ql r4) {
        super("wa.action.shops.TOSaccept");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r15, C1093651k r16, C14240l5 r17) {
        C14230l4 r2 = (C14230l4) r17;
        if (!r16.A00.equals("wa.action.shops.TOSaccept")) {
            return null;
        }
        int A05 = C12960it.A05(r15.A00.get(0));
        PrivacyNoticeDialogFragment privacyNoticeDialogFragment = (PrivacyNoticeDialogFragment) ((AnonymousClass01F) r2.A00.A02.A00().get(R.id.bloks_host_fragment_manager)).A0A("TOSFragmentOuter");
        if (privacyNoticeDialogFragment != null) {
            C127205u7 r4 = (C127205u7) privacyNoticeDialogFragment.A03.get();
            C128655wS r3 = new C128655wS(privacyNoticeDialogFragment);
            AnonymousClass01H r1 = r4.A00;
            String A01 = ((C17220qS) r1.get()).A01();
            C41141sy A0M = C117295Zj.A0M();
            C41141sy.A01(A0M, "id", A01);
            C41141sy.A01(A0M, "xmlns", "urn:xmpp:whatsapp:account");
            C41141sy.A01(A0M, "type", "set");
            A0M.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
            C41141sy r22 = new C41141sy("shops_notice");
            r22.A04(new AnonymousClass1W9("tos_version", A05));
            C117295Zj.A1H(r22, A0M);
            ((C17220qS) r1.get()).A0D(new AnonymousClass6DM(r4, r3), A0M.A03(), A01, 249, 32000);
            return null;
        }
        Log.e("Bloks: Invalid fragment tag");
        return null;
    }
}
