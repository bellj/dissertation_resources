package X;

import com.facebook.imagepipeline.memory.AshmemMemoryChunkPool;
import com.facebook.imagepipeline.memory.BufferMemoryChunkPool;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.4XR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XR {
    public AbstractC12890ij A00;
    public AbstractC11590gX A01;
    public C04660Mo A02;
    public AnonymousClass5YZ A03;
    public C89774Li A04;
    public AbstractC75883ke A05;
    public AbstractC75883ke A06;
    public AbstractC75883ke A07;
    public final C91954Tw A08;

    public AnonymousClass4XR(C91954Tw r1) {
        this.A08 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005e, code lost:
        if (r1.equals("dummy") != false) goto L_0x0060;
     */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass5YZ A00() {
        /*
            r4 = this;
            X.5YZ r3 = r4.A03
            if (r3 != 0) goto L_0x0022
            X.4Tw r3 = r4.A08
            java.lang.String r1 = r3.A09
            int r0 = r1.hashCode()
            switch(r0) {
                case -1868884870: goto L_0x0023;
                case -404562712: goto L_0x0032;
                case -402149703: goto L_0x0048;
                case 95945896: goto L_0x0058;
                default: goto L_0x000f;
            }
        L_0x000f:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0060
            X.0gW r2 = r3.A01
            X.4aR r1 = r3.A02
        L_0x0019:
            X.5Pl r0 = r3.A06
            X.3kf r3 = new X.3kf
            r3.<init>(r2, r1, r0)
            r4.A03 = r3
        L_0x0022:
            return r3
        L_0x0023:
            java.lang.String r0 = "legacy_default_params"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000f
            X.0gW r2 = r3.A01
            X.4aR r1 = X.C93064Yw.A00()
            goto L_0x0019
        L_0x0032:
            java.lang.String r0 = "experimental"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000f
            int r1 = r3.A00
            X.4v9 r0 = X.C106114v9.A00()
            X.4v8 r3 = new X.4v8
            r3.<init>(r0, r1)
            r4.A03 = r3
            return r3
        L_0x0048:
            java.lang.String r0 = "dummy_with_tracking"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000f
            X.3SO r3 = new X.3SO
            r3.<init>()
            r4.A03 = r3
            return r3
        L_0x0058:
            java.lang.String r0 = "dummy"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000f
        L_0x0060:
            X.4v7 r3 = new X.4v7
            r3.<init>()
            r4.A03 = r3
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XR.A00():X.5YZ");
    }

    public final AbstractC75883ke A01(int i) {
        AbstractC75883ke r7;
        if (i == 0) {
            r7 = this.A07;
            if (r7 == null) {
                try {
                    Constructor<?> constructor = Class.forName("com.facebook.imagepipeline.memory.NativeMemoryChunkPool").getConstructor(AbstractC11580gW.class, C93604aR.class, AbstractC115035Pl.class);
                    C91954Tw r1 = this.A08;
                    AbstractC75883ke r0 = (AbstractC75883ke) constructor.newInstance(r1.A01, r1.A04, r1.A07);
                    this.A07 = r0;
                    return r0;
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                    AbstractC12710iN r12 = AnonymousClass0UN.A00;
                    if (r12.AJi(6)) {
                        r12.A9H("PoolFactory", "", e);
                    }
                    this.A07 = null;
                    return null;
                }
            }
        } else if (i == 1) {
            r7 = this.A06;
            if (r7 == null) {
                r7 = null;
                try {
                    Constructor constructor2 = BufferMemoryChunkPool.class.getConstructor(AbstractC11580gW.class, C93604aR.class, AbstractC115035Pl.class);
                    C91954Tw r13 = this.A08;
                    AbstractC75883ke r02 = (AbstractC75883ke) constructor2.newInstance(r13.A01, r13.A04, r13.A07);
                    this.A06 = r02;
                    return r02;
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused) {
                    this.A06 = null;
                }
            }
        } else if (i == 2) {
            r7 = this.A05;
            if (r7 == null) {
                try {
                    Constructor constructor3 = AshmemMemoryChunkPool.class.getConstructor(AbstractC11580gW.class, C93604aR.class, AbstractC115035Pl.class);
                    C91954Tw r14 = this.A08;
                    AbstractC75883ke r03 = (AbstractC75883ke) constructor3.newInstance(r14.A01, r14.A04, r14.A07);
                    this.A05 = r03;
                    return r03;
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused2) {
                    this.A05 = null;
                    return null;
                }
            }
        } else {
            throw C12970iu.A0f("Invalid MemoryChunkType");
        }
        return r7;
    }
}
