package X;

import com.whatsapp.spamwarning.SpamWarningActivity;

/* renamed from: X.539  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass539 implements AbstractC18870tC {
    public boolean A00;
    public final /* synthetic */ SpamWarningActivity A01;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARC() {
    }

    public AnonymousClass539(SpamWarningActivity spamWarningActivity) {
        this.A01 = spamWarningActivity;
    }

    @Override // X.AbstractC18870tC
    public void ARA() {
        if (!this.A00) {
            SpamWarningActivity spamWarningActivity = this.A01;
            spamWarningActivity.startActivity(C14960mK.A02(spamWarningActivity));
            spamWarningActivity.finish();
        }
        this.A00 = true;
    }
}
