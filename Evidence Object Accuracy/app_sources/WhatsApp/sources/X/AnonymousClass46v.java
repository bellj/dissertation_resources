package X;

import android.content.Context;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.46v  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass46v extends AbstractC863446w {
    public AnonymousClass46v(Context context) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.search_attachment_height_compact));
    }
}
