package X;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.4V3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4V3 {
    public final AnonymousClass4QU A00;

    public AnonymousClass4V3(AnonymousClass584 r11) {
        AnonymousClass4QV r3 = new AnonymousClass4QV();
        try {
            C91504Rz[] r9 = AnonymousClass4Hp.A0C;
            for (C91504Rz r5 : r9) {
                HashMap hashMap = r3.A01;
                if (hashMap.get(r5) == null) {
                    boolean z = false;
                    if (r5.A02 == AnonymousClass4AH.START) {
                        z = true;
                        if (r3.A00 != null) {
                            throw new AnonymousClass4CD(C12960it.A0d(r5.A03, C12960it.A0k("Start state already exists, new state invalid: ")));
                        }
                    }
                    hashMap.put(r5, new AnonymousClass4WT());
                    if (z) {
                        r3.A00 = r5;
                    }
                } else {
                    throw new AnonymousClass4CD(C12960it.A0d(r5.A03, C12960it.A0k("State already added: ")));
                }
            }
            C91654So[] r6 = C88894Hv.A0V;
            for (C91654So r4 : r6) {
                HashMap hashMap2 = r3.A01;
                AnonymousClass4WT r1 = (AnonymousClass4WT) hashMap2.get(r4.A02);
                if (r1 == null) {
                    throw new AnonymousClass4CD(C12960it.A0d(r4.A04, C12960it.A0k("Cannot find input state for transition ")));
                } else if (hashMap2.get(r4.A01) != null) {
                    r1.A00.add(r4);
                } else {
                    throw new AnonymousClass4CD(C12960it.A0d(r4.A04, C12960it.A0k("Cannot find output state for transition ")));
                }
            }
            if (r3.A00 != null) {
                HashMap hashMap3 = r3.A01;
                Iterator A0L = C72463ee.A0L(hashMap3);
                while (A0L.hasNext()) {
                    AnonymousClass4AH r0 = ((C91504Rz) A0L.next()).A02;
                    AnonymousClass4AH r7 = AnonymousClass4AH.END;
                    if (r0 == r7) {
                        HashSet A12 = C12970iu.A12();
                        Iterator A0s = C12990iw.A0s(hashMap3);
                        while (A0s.hasNext()) {
                            Map.Entry A15 = C12970iu.A15(A0s);
                            if (((AnonymousClass4WT) A15.getValue()).A00.size() != 0 || ((C91504Rz) A15.getKey()).A02 == r7) {
                                HashSet A122 = C12970iu.A12();
                                Iterator it = ((AnonymousClass4WT) A15.getValue()).A00.iterator();
                                while (it.hasNext()) {
                                    A122.add(((C91654So) it.next()).A01);
                                }
                                A12.addAll(A122);
                            } else {
                                throw new AnonymousClass4CD(C12960it.A0d(((C91504Rz) A15.getKey()).A03, C12960it.A0k("Non-end state with no outbound transitions: ")));
                            }
                        }
                        if (hashMap3.size() - A12.size() > 1) {
                            throw new AnonymousClass4CD("Non-start state(s) with no incoming transitions exist(s)");
                        } else if (hashMap3.size() - A12.size() != 1 || !A12.contains(r3.A00)) {
                            r3.A02 = true;
                            this.A00 = new AnonymousClass4QU(r11, r3);
                            return;
                        } else {
                            throw new AnonymousClass4CD("Non-start state(s) with no incoming transitions exist(s)");
                        }
                    }
                }
                throw new AnonymousClass4CD("State machine must have an end state");
            }
            throw new AnonymousClass4CD("State machine must have a start state");
        } catch (AnonymousClass4CD e) {
            throw AnonymousClass1NR.A01("Failed to init finite state machine.", C72453ed.A0w(e), (byte) 80);
        }
    }

    public synchronized void A00(C89634Ks r7) {
        AnonymousClass4UY r1;
        AnonymousClass4UY r12;
        try {
            AnonymousClass4QU r5 = this.A00;
            AnonymousClass4QV r13 = r5.A02;
            C91504Rz r2 = r5.A00;
            AbstractC115365Rg r4 = r5.A01;
            if (r13.A02) {
                Iterator it = ((AnonymousClass4WT) r13.A01.get(r2)).A00.iterator();
                while (it.hasNext()) {
                    C91654So r3 = (C91654So) it.next();
                    if (r3.A03.isInstance(r7)) {
                        C91504Rz r22 = r3.A01;
                        C91504Rz r0 = r5.A00;
                        if (!(r22 == r0 || (r12 = r0.A01) == null)) {
                            r12.A04(r4, r7, r3, 1);
                        }
                        AnonymousClass4UY r14 = r3.A00;
                        if (r14 != null) {
                            r14.A04(r4, r7, r3, 2);
                        }
                        if (!(r22 == r5.A00 || (r1 = r22.A00) == null)) {
                            r1.A04(r4, r7, r3, 3);
                        }
                        r5.A00 = r22;
                    }
                }
                throw new AnonymousClass4CD(C12960it.A0d(r2.A03, C12960it.A0k("No valid transition from state: ")));
            }
            throw new AnonymousClass4CD("State machine map is not initialized - call build()");
        } catch (AnonymousClass4CD e) {
            if (!(e.getCause() instanceof AnonymousClass1NR)) {
                throw AnonymousClass1NR.A01("Internal Error", C72453ed.A0w(e), (byte) 80);
            }
            throw ((AnonymousClass1NR) e.getCause());
        }
    }
}
