package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2eV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53472eV extends AnonymousClass04v {
    public final /* synthetic */ C36591kA A00;

    public C53472eV(C36591kA r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        super.A06(view, r5);
        AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
        accessibilityNodeInfo.setClickable(false);
        r5.A0A(C007804a.A05);
        accessibilityNodeInfo.setLongClickable(false);
        r5.A0A(C007804a.A0F);
    }
}
