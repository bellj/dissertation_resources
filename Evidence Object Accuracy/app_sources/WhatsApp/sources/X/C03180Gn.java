package X;

import android.database.Cursor;
import androidx.work.impl.WorkDatabase;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0Gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03180Gn extends AbstractRunnableC10150e7 {
    public final /* synthetic */ AnonymousClass022 A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ boolean A02;

    public C03180Gn(AnonymousClass022 r1, String str, boolean z) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = z;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractRunnableC10150e7
    public void A00() {
        AnonymousClass022 r6 = this.A00;
        WorkDatabase workDatabase = r6.A04;
        workDatabase.A03();
        try {
            AbstractC12700iM A0B = workDatabase.A0B();
            String str = this.A01;
            C07740a0 r3 = (C07740a0) A0B;
            AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
            if (str == null) {
                A00.A6T(1);
            } else {
                A00.A6U(1, str);
            }
            AnonymousClass0QN r0 = r3.A01;
            r0.A02();
            Cursor A002 = AnonymousClass0LC.A00(r0, A00, false);
            ArrayList arrayList = new ArrayList(A002.getCount());
            while (A002.moveToNext()) {
                arrayList.add(A002.getString(0));
            }
            A002.close();
            A00.A01();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                A01(r6, (String) it.next());
            }
            workDatabase.A05();
            workDatabase.A04();
            if (this.A02) {
                AnonymousClass0TI.A01(r6.A02, workDatabase, r6.A07);
            }
        } catch (Throwable th) {
            workDatabase.A04();
            throw th;
        }
    }
}
