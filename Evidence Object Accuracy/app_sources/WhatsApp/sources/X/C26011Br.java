package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService;
import java.io.IOException;

/* renamed from: X.1Br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26011Br {
    public int A00 = 0;
    public AnonymousClass3LK A01 = null;
    public String A02;
    public final Context A03;
    public final Context A04;
    public final AbstractC15710nm A05;
    public final AnonymousClass1C4 A06;
    public final AnonymousClass01H A07;
    public final String A08 = "GoogleMigrateClient";

    public C26011Br(Context context, AbstractC15710nm r3, AnonymousClass1C4 r4, AnonymousClass01H r5) {
        this.A04 = context;
        this.A05 = r3;
        this.A03 = context;
        this.A07 = r5;
        this.A06 = r4;
    }

    public ParcelFileDescriptor A00(String str) {
        try {
            AnonymousClass351 A01 = A01();
            C67603Sd r1 = (C67603Sd) ((IAppDataReaderService) A01.A00());
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService");
                obtain.writeString(str);
                r1.A00.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                ParcelFileDescriptor parcelFileDescriptor = obtain2.readInt() != 0 ? (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(obtain2) : null;
                A01.close();
                return parcelFileDescriptor;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        } catch (Exception e) {
            throw new IOException(str, e);
        }
    }

    public AnonymousClass351 A01() {
        AnonymousClass351 r0;
        synchronized (this) {
            this.A00++;
            r0 = new AnonymousClass351(this);
        }
        return r0;
    }

    public C71103cO A02() {
        C71103cO r0;
        synchronized (this) {
            this.A00++;
            r0 = new C71103cO(this);
        }
        return r0;
    }

    public void A03() {
        try {
            AnonymousClass351 A01 = A01();
            C67603Sd r1 = (C67603Sd) ((IAppDataReaderService) A01.A00());
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService");
            r1.A00.transact(4, obtain, obtain2, 0);
            obtain2.readException();
            obtain2.recycle();
            obtain.recycle();
            A01.close();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r3.getFileDescriptor() == null) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04() {
        /*
            r6 = this;
            r5 = 0
            X.351 r4 = r6.A01()     // Catch: SecurityException | 4CE -> 0x0049, all -> 0x0042
            android.os.IInterface r0 = r4.A00()     // Catch: all -> 0x003d
            com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService r0 = (com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService) r0     // Catch: all -> 0x003d
            android.os.ParcelFileDescriptor r3 = r0.AD1()     // Catch: all -> 0x003d
            if (r3 == 0) goto L_0x0018
            java.io.FileDescriptor r0 = r3.getFileDescriptor()     // Catch: all -> 0x0036
            r2 = 1
            if (r0 != 0) goto L_0x0019
        L_0x0018:
            r2 = 0
        L_0x0019:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0036
            r1.<init>()     // Catch: all -> 0x0036
            java.lang.String r0 = "GoogleMigrateClient/hasWhatsAppData; hasFileDescriptor = "
            r1.append(r0)     // Catch: all -> 0x0036
            r1.append(r2)     // Catch: all -> 0x0036
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0036
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x0036
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch: all -> 0x003d
        L_0x0032:
            r4.close()     // Catch: SecurityException | 4CE -> 0x0049, SecurityException | 4CE -> 0x0049, all -> 0x0042
            return r2
        L_0x0036:
            r0 = move-exception
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch: all -> 0x003c
        L_0x003c:
            throw r0     // Catch: all -> 0x003d
        L_0x003d:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0041
        L_0x0041:
            throw r0     // Catch: SecurityException | 4CE -> 0x0049, SecurityException | 4CE -> 0x0049, all -> 0x0042
        L_0x0042:
            r1 = move-exception
            java.lang.String r0 = "GoogleMigrateClient/hasWhatsAppData()"
            com.whatsapp.util.Log.e(r0, r1)
            return r5
        L_0x0049:
            r2 = move-exception
            java.lang.String r1 = "GoogleMigrateClient/hasWhatsAppData(): "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26011Br.A04():boolean");
    }

    public boolean A05(String str) {
        try {
            ApplicationInfo applicationInfo = this.A03.getPackageManager().getPackageInfo(str, 0).applicationInfo;
            if (!applicationInfo.enabled) {
                return false;
            }
            int i = applicationInfo.flags;
            boolean z = false;
            if ((i & 1) != 0) {
                z = true;
            }
            boolean z2 = false;
            if ((i & 128) != 0) {
                z2 = true;
            }
            if (z || z2) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
