package X;

import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.2bI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52552bI extends ViewOutlineProvider {
    public final /* synthetic */ AnonymousClass28D A00;

    public C52552bI(AnonymousClass28D r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        float f;
        Drawable background = view.getBackground();
        if (background != null) {
            background.getOutline(outline);
            f = this.A00.A09(65, 1.0f);
        } else {
            outline.setRect(0, 0, view.getWidth(), view.getHeight());
            f = 0.0f;
        }
        outline.setAlpha(f);
    }
}
