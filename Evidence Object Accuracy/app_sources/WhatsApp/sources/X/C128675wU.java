package X;

/* renamed from: X.5wU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C128675wU {
    public final AnonymousClass1V8 A00;

    public C128675wU(String str, String str2, String str3) {
        C41141sy r1 = new C41141sy("smax:any");
        C41141sy r0 = new C41141sy("money");
        if (AnonymousClass3JT.A0E(str, 1, 100, false)) {
            C41141sy.A01(r0, "value", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 100, false)) {
            C41141sy.A01(r0, "offset", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 100, false)) {
            C41141sy.A01(r0, "currency", str3);
        }
        C117295Zj.A1H(r0, r1);
        this.A00 = r1.A03();
    }
}
