package X;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0wF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20740wF {
    public final AbstractC15710nm A00;
    public final C22930zs A01;
    public final C15570nT A02;
    public final C20870wS A03;
    public final C20670w8 A04;
    public final C16240og A05;
    public final C14830m7 A06;
    public final C15990oG A07;
    public final C18240s8 A08;
    public final C21410xN A09;
    public final C15600nX A0A;
    public final C26731Ep A0B;
    public final C22830zi A0C;
    public final C22100yW A0D;
    public final C14850m9 A0E;
    public final C22910zq A0F;
    public final C17220qS A0G;
    public final C19890uq A0H;
    public final C14840m8 A0I;
    public final C239913u A0J;
    public final C20780wJ A0K;
    public final ExecutorC27271Gr A0L;
    public final JniBridge A0M;

    public C20740wF(AbstractC15710nm r4, C22930zs r5, C15570nT r6, C20870wS r7, C20670w8 r8, C16240og r9, C14830m7 r10, C15990oG r11, C18240s8 r12, C21410xN r13, C15600nX r14, C26731Ep r15, C22830zi r16, C22100yW r17, C14850m9 r18, C22910zq r19, C17220qS r20, C19890uq r21, C14840m8 r22, C239913u r23, C20780wJ r24, AbstractC14440lR r25, JniBridge jniBridge) {
        ExecutorC27271Gr r0 = new ExecutorC27271Gr(r25, true);
        this.A06 = r10;
        this.A0E = r18;
        this.A00 = r4;
        this.A02 = r6;
        this.A0M = jniBridge;
        this.A04 = r8;
        this.A0G = r20;
        this.A03 = r7;
        this.A0H = r21;
        this.A08 = r12;
        this.A0F = r19;
        this.A05 = r9;
        this.A01 = r5;
        this.A0J = r23;
        this.A0I = r22;
        this.A07 = r11;
        this.A0C = r16;
        this.A09 = r13;
        this.A0D = r17;
        this.A0K = r24;
        this.A0A = r14;
        this.A0B = r15;
        this.A0L = r0;
    }

    public final void A00(C44481z2 r50) {
        boolean z;
        Set set;
        DeviceJid deviceJid;
        Set set2;
        StringBuilder sb = new StringBuilder("sending message: ");
        sb.append(r50);
        Log.i(sb.toString());
        Handler handler = this.A01.A00;
        handler.removeMessages(0);
        handler.removeMessages(1);
        handler.removeMessages(2);
        AbstractC15340mz r7 = r50.A05;
        Jid jid = r50.A03;
        Jid jid2 = jid;
        if (r7.A11()) {
            C16240og r5 = this.A05;
            if (r5.A06) {
                C22910zq r0 = this.A0F;
                AnonymousClass1IS r1 = r7.A0z;
                Map map = r0.A01;
                synchronized (map) {
                    Pair pair = (Pair) map.get(r1);
                    z = false;
                    if (!(pair == null || (set2 = (Set) pair.first) == null)) {
                        if (jid == null) {
                            AbstractC14640lm r2 = r1.A00;
                            if (r2 instanceof UserJid) {
                                jid2 = DeviceJid.of(r2);
                                AnonymousClass009.A05(jid2);
                            } else {
                                jid2 = null;
                            }
                        }
                        if (set2.contains(jid2)) {
                            z = true;
                        }
                    }
                }
                if (!z && !r7.A1B) {
                    synchronized (map) {
                        Pair pair2 = (Pair) map.get(r1);
                        if (pair2 == null) {
                            set = new HashSet();
                            map.put(r1, new Pair(set, r7));
                        } else {
                            set = (Set) pair2.first;
                        }
                        if (jid != null) {
                            deviceJid = jid;
                        } else {
                            AbstractC14640lm r22 = r1.A00;
                            if (r22 instanceof UserJid) {
                                deviceJid = DeviceJid.of(r22);
                                AnonymousClass009.A05(deviceJid);
                            } else {
                                deviceJid = null;
                            }
                        }
                        if (!set.add(deviceJid)) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("in-flight-messages/duplicate in flight message: ");
                            sb2.append(r1);
                            sb2.append(" : ");
                            sb2.append(jid);
                            Log.w(sb2.toString());
                        }
                    }
                    if (r5.A04 != 2 && !r50.A0A) {
                        this.A0H.A0C(1, true, false, false, false);
                    }
                    if (r7.A16 == 0) {
                        r7.A16 = SystemClock.uptimeMillis();
                    }
                    boolean z2 = r50.A09;
                    if (z2 || r50.A0A) {
                        C20870wS r23 = this.A03;
                        int i = r7.A1F;
                        if (r23.A0M) {
                            r23.A0E.A05(r1.A01.hashCode(), 5, i, r7.A0y);
                        }
                    }
                    ExecutorC27271Gr r10 = this.A0L;
                    C14830m7 r02 = this.A06;
                    C14850m9 r03 = this.A0E;
                    AbstractC15710nm r04 = this.A00;
                    C15570nT r05 = this.A02;
                    JniBridge jniBridge = this.A0M;
                    C20670w8 r06 = this.A04;
                    C20870wS r07 = this.A03;
                    C18240s8 r08 = this.A08;
                    C239913u r09 = this.A0J;
                    C14840m8 r010 = this.A0I;
                    C15990oG r011 = this.A07;
                    C22830zi r012 = this.A0C;
                    C21410xN r013 = this.A09;
                    C22100yW r014 = this.A0D;
                    C20780wJ r015 = this.A0K;
                    C15600nX r016 = this.A0A;
                    C26731Ep r017 = this.A0B;
                    Jid jid3 = jid;
                    if (jid == null) {
                        Jid jid4 = r1.A00;
                        AnonymousClass009.A05(jid4);
                        jid3 = jid4;
                    }
                    UserJid userJid = r50.A04;
                    HashSet hashSet = new HashSet(r50.A08);
                    boolean z3 = r50.A0A;
                    r10.execute(new RunnableC44491z3(r04, r05, r07, r06, r02, r011, r08, r013, r016, r017, r012, r014, r03, jid3, userJid, r010, r7, r09, r015, r50.A06, jniBridge, new RunnableBRunnable0Shape6S0200000_I0_6(this, 20, r50), hashSet, r50.A00, r50.A01, r50.A02, z3, z2));
                    return;
                }
            }
        }
        StringBuilder sb3 = new StringBuilder("Dropping send message: ");
        sb3.append(r50);
        Log.i(sb3.toString());
        AnonymousClass1VC r12 = r50.A06;
        if (r12 != null) {
            r12.A01(null);
        }
    }

    public void A01(AbstractC15340mz r19) {
        long j;
        C14830m7 r1 = this.A06;
        HashSet hashSet = new HashSet();
        if (r19 instanceof C30331Wz) {
            j = r1.A00();
        } else {
            j = r19.A0I;
        }
        A00(new C44481z2(null, null, r19, null, null, hashSet, 0, 0, j, false, false));
    }

    public void A02(AbstractC15340mz r19, boolean z) {
        long j;
        C14830m7 r1 = this.A06;
        HashSet hashSet = new HashSet();
        if (r19 instanceof C30331Wz) {
            j = r1.A00();
        } else {
            j = r19.A0I;
        }
        A00(new C44481z2(null, null, r19, null, null, hashSet, 0, 0, j, z, true));
    }

    public void A03(boolean z) {
        this.A0H.A0w = z;
        this.A0G.A07(Message.obtain(null, 0, 37, 0, Boolean.valueOf(z)), null, false);
    }
}
