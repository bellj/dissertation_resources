package X;

/* renamed from: X.036  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass036 implements Cloneable {
    public static final Object A04 = new Object();
    public int A00;
    public boolean A01;
    public long[] A02;
    public Object[] A03;

    public AnonymousClass036() {
        this(10);
    }

    public AnonymousClass036(int i) {
        Object[] objArr;
        this.A01 = false;
        if (i == 0) {
            this.A02 = AnonymousClass00R.A01;
            objArr = AnonymousClass00R.A02;
        } else {
            int i2 = i << 3;
            int i3 = 4;
            while (true) {
                int i4 = (1 << i3) - 12;
                if (i2 > i4) {
                    i3++;
                    if (i3 >= 32) {
                        break;
                    }
                } else {
                    i2 = i4;
                    break;
                }
            }
            int i5 = i2 >> 3;
            this.A02 = new long[i5];
            objArr = new Object[i5];
        }
        this.A03 = objArr;
    }

    public int A00() {
        if (this.A01) {
            A06();
        }
        return this.A00;
    }

    public long A01(int i) {
        if (this.A01) {
            A06();
        }
        return this.A02[i];
    }

    /* renamed from: A02 */
    public AnonymousClass036 clone() {
        try {
            AnonymousClass036 r1 = (AnonymousClass036) super.clone();
            r1.A02 = (long[]) this.A02.clone();
            r1.A03 = (Object[]) this.A03.clone();
            return r1;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public Object A03(int i) {
        if (this.A01) {
            A06();
        }
        return this.A03[i];
    }

    public Object A04(long j, Object obj) {
        Object obj2;
        int A01 = AnonymousClass00R.A01(this.A02, this.A00, j);
        return (A01 < 0 || (obj2 = this.A03[A01]) == A04) ? obj : obj2;
    }

    public void A05() {
        int i = this.A00;
        Object[] objArr = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.A00 = 0;
        this.A01 = false;
    }

    public final void A06() {
        int i = this.A00;
        long[] jArr = this.A02;
        Object[] objArr = this.A03;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != A04) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.A01 = false;
        this.A00 = i2;
    }

    public void A07(long j) {
        Object[] objArr;
        Object obj;
        int A01 = AnonymousClass00R.A01(this.A02, this.A00, j);
        if (A01 >= 0 && (objArr = this.A03)[A01] != (obj = A04)) {
            objArr[A01] = obj;
            this.A01 = true;
        }
    }

    public void A08(long j, Object obj) {
        int i = this.A00;
        if (i == 0 || j > this.A02[i - 1]) {
            if (this.A01 && i >= this.A02.length) {
                A06();
            }
            int i2 = this.A00;
            long[] jArr = this.A02;
            long[] jArr2 = jArr;
            int length = jArr.length;
            if (i2 >= length) {
                int i3 = (i2 + 1) << 3;
                int i4 = 4;
                while (true) {
                    int i5 = (1 << i4) - 12;
                    if (i3 > i5) {
                        i4++;
                        if (i4 >= 32) {
                            break;
                        }
                    } else {
                        i3 = i5;
                        break;
                    }
                }
                int i6 = i3 >> 3;
                jArr2 = new long[i6];
                Object[] objArr = new Object[i6];
                System.arraycopy(jArr, 0, jArr2, 0, length);
                Object[] objArr2 = this.A03;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.A02 = jArr2;
                this.A03 = objArr;
            }
            jArr2[i2] = j;
            this.A03[i2] = obj;
            this.A00 = i2 + 1;
            return;
        }
        A09(j, obj);
    }

    public void A09(long j, Object obj) {
        long[] jArr = this.A02;
        int i = this.A00;
        int A01 = AnonymousClass00R.A01(jArr, i, j);
        if (A01 >= 0) {
            this.A03[A01] = obj;
            return;
        }
        int i2 = A01 ^ -1;
        if (i2 < i) {
            Object[] objArr = this.A03;
            if (objArr[i2] == A04) {
                jArr[i2] = j;
                objArr[i2] = obj;
                return;
            }
        }
        if (this.A01 && i >= jArr.length) {
            A06();
            i = this.A00;
            i2 = AnonymousClass00R.A01(jArr, i, j) ^ -1;
        }
        int length = jArr.length;
        if (i >= length) {
            int i3 = (i + 1) << 3;
            int i4 = 4;
            while (true) {
                int i5 = (1 << i4) - 12;
                if (i3 > i5) {
                    i4++;
                    if (i4 >= 32) {
                        break;
                    }
                } else {
                    i3 = i5;
                    break;
                }
            }
            int i6 = i3 >> 3;
            long[] jArr2 = new long[i6];
            Object[] objArr2 = new Object[i6];
            System.arraycopy(jArr, 0, jArr2, 0, length);
            Object[] objArr3 = this.A03;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.A02 = jArr2;
            jArr = jArr2;
            this.A03 = objArr2;
        }
        int i7 = this.A00 - i2;
        if (i7 != 0) {
            int i8 = i2 + 1;
            System.arraycopy(jArr, i2, jArr, i8, i7);
            Object[] objArr4 = this.A03;
            System.arraycopy(objArr4, i2, objArr4, i8, this.A00 - i2);
        }
        this.A02[i2] = j;
        this.A03[i2] = obj;
        this.A00++;
    }

    @Override // java.lang.Object
    public String toString() {
        if (A00() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 28);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(A01(i));
            sb.append('=');
            Object A03 = A03(i);
            if (A03 != this) {
                sb.append(A03);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
