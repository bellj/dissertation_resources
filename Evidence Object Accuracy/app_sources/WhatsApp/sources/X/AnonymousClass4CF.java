package X;

/* renamed from: X.4CF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CF extends Exception {
    public static final long serialVersionUID = 1;

    public AnonymousClass4CF(String str) {
        super(str);
    }

    public AnonymousClass4CF(Throwable th) {
        super("Invalid quoted-printable encoding", th);
    }
}
