package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

/* renamed from: X.3NV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NV implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ float A00;
    public final /* synthetic */ C469728k A01;

    public AnonymousClass3NV(C469728k r1, float f) {
        this.A01 = r1;
        this.A00 = f;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C469728k r2 = this.A01;
        View view = r2.A02;
        C12980iv.A1F(view, this);
        float A03 = C12990iw.A03(view);
        float f = this.A00;
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f - (f / A03), 1, 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        view.startAnimation(translateAnimation);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
        animationSet.addAnimation(new TranslateAnimation(1, 0.0f, 1, 0.0f, 0, (A03 - f) * 1.4f, 0, 0.0f));
        animationSet.setDuration(300);
        r2.A04.startAnimation(animationSet);
        r2.A03.startAnimation(animationSet);
    }
}
