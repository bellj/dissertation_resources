package X;

import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0Fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03060Fy extends AnonymousClass072 {
    public int A00 = 0;
    public int A01;
    public ArrayList A02 = new ArrayList();
    public boolean A03 = true;
    public boolean A04 = false;

    @Override // X.AnonymousClass072
    public AnonymousClass072 A03() {
        C03060Fy r3 = (C03060Fy) super.clone();
        r3.A02 = new ArrayList();
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            r3.A0W(((AnonymousClass072) this.A02.get(i)).clone());
        }
        return r3;
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A04(long j) {
        super.A01 = j;
        if (j >= 0) {
            int size = this.A02.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass072) this.A02.get(i)).A01 = j;
            }
        }
        return this;
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A05(TimeInterpolator timeInterpolator) {
        this.A00 |= 1;
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A03 = timeInterpolator;
        }
        super.A03 = timeInterpolator;
        return this;
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A06(View view) {
        A0V(view);
        return this;
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A07(View view) {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A02;
            if (i < arrayList.size()) {
                ((AnonymousClass072) arrayList.get(i)).A07(view);
                i++;
            } else {
                super.A07(view);
                return this;
            }
        }
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A08(AbstractC018608t r1) {
        super.A08(r1);
        return this;
    }

    @Override // X.AnonymousClass072
    public /* bridge */ /* synthetic */ AnonymousClass072 A09(AbstractC018608t r1) {
        super.A09(r1);
        return this;
    }

    @Override // X.AnonymousClass072
    public String A0C(String str) {
        String A0C = super.A0C(str);
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A02;
            if (i >= arrayList.size()) {
                return A0C;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(A0C);
            sb.append("\n");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("  ");
            sb.append(((AnonymousClass072) arrayList.get(i)).A0C(sb2.toString()));
            A0C = sb.toString();
            i++;
        }
    }

    @Override // X.AnonymousClass072
    public void A0E() {
        ArrayList arrayList;
        if (this.A02.isEmpty()) {
            A0F();
            A0D();
            return;
        }
        AnonymousClass0G4 r2 = new AnonymousClass0G4(this);
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            ((AnonymousClass072) it.next()).A08(r2);
        }
        this.A01 = this.A02.size();
        if (!this.A03) {
            int i = 1;
            while (true) {
                arrayList = this.A02;
                if (i >= arrayList.size()) {
                    break;
                }
                ((AnonymousClass072) arrayList.get(i - 1)).A08(new AnonymousClass0G3((AnonymousClass072) arrayList.get(i), this));
                i++;
            }
            AnonymousClass072 r0 = (AnonymousClass072) arrayList.get(0);
            if (r0 != null) {
                r0.A0E();
                return;
            }
            return;
        }
        Iterator it2 = this.A02.iterator();
        while (it2.hasNext()) {
            ((AnonymousClass072) it2.next()).A0E();
        }
    }

    @Override // X.AnonymousClass072
    public void A0G(View view) {
        super.A0G(view);
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A0G(view);
        }
    }

    @Override // X.AnonymousClass072
    public void A0H(View view) {
        super.A0H(view);
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A0H(view);
        }
    }

    @Override // X.AnonymousClass072
    public void A0J(ViewGroup viewGroup, C04830Nf r17, C04830Nf r18, ArrayList arrayList, ArrayList arrayList2) {
        long j = super.A02;
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass072 r9 = (AnonymousClass072) this.A02.get(i);
            if (j > 0 && (this.A03 || i == 0)) {
                long j2 = r9.A02;
                if (j2 > 0) {
                    r9.A02 = j2 + j;
                } else {
                    r9.A02 = j;
                }
            }
            r9.A0J(viewGroup, r17, r18, arrayList, arrayList2);
        }
    }

    @Override // X.AnonymousClass072
    public void A0L(AnonymousClass0LF r4) {
        super.A0L(r4);
        this.A00 |= 4;
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A02;
            if (i < arrayList.size()) {
                ((AnonymousClass072) arrayList.get(i)).A0L(r4);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass072
    public void A0M(AnonymousClass0OE r4) {
        this.A05 = r4;
        this.A00 |= 8;
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A05 = r4;
        }
    }

    @Override // X.AnonymousClass072
    public void A0N(AnonymousClass0LG r4) {
        this.A06 = r4;
        this.A00 |= 2;
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A06 = r4;
        }
    }

    @Override // X.AnonymousClass072
    public void A0O(C05350Pf r4) {
        super.A0O(r4);
        int size = this.A02.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass072) this.A02.get(i)).A0O(r4);
        }
    }

    @Override // X.AnonymousClass072
    public void A0T(C05350Pf r4) {
        if (A0P(r4.A00)) {
            Iterator it = this.A02.iterator();
            while (it.hasNext()) {
                AnonymousClass072 r1 = (AnonymousClass072) it.next();
                if (r1.A0P(r4.A00)) {
                    r1.A0T(r4);
                    r4.A01.add(r1);
                }
            }
        }
    }

    @Override // X.AnonymousClass072
    public void A0U(C05350Pf r4) {
        if (A0P(r4.A00)) {
            Iterator it = this.A02.iterator();
            while (it.hasNext()) {
                AnonymousClass072 r1 = (AnonymousClass072) it.next();
                if (r1.A0P(r4.A00)) {
                    r1.A0U(r4);
                    r4.A01.add(r1);
                }
            }
        }
    }

    public void A0V(View view) {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A02;
            if (i < arrayList.size()) {
                ((AnonymousClass072) arrayList.get(i)).A06(view);
                i++;
            } else {
                super.A06(view);
                return;
            }
        }
    }

    public void A0W(AnonymousClass072 r6) {
        this.A02.add(r6);
        r6.A07 = this;
        long j = super.A01;
        if (j >= 0) {
            r6.A04(j);
        }
        if ((this.A00 & 1) != 0) {
            r6.A05(super.A03);
        }
        if ((this.A00 & 2) != 0) {
            r6.A0N(this.A06);
        }
        if ((this.A00 & 4) != 0) {
            r6.A0L(super.A04);
        }
        if ((this.A00 & 8) != 0) {
            r6.A0M(this.A05);
        }
    }
}
