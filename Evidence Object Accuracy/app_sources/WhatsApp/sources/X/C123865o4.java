package X;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.5o4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123865o4 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractActivityC121475iK A00;

    public C123865o4(AbstractActivityC121475iK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AbstractActivityC121475iK r4 = this.A00;
        C123595nP r2 = r4.A0R;
        Log.i("DyiViewModel/download-report");
        r2.A07(3);
        r2.A02.A0A(3);
        r2.A09.Ab2(new Runnable() { // from class: X.6HE
            @Override // java.lang.Runnable
            public final void run() {
                C123595nP r0 = C123595nP.this;
                AnonymousClass60U r22 = r0.A08;
                C128625wP r1 = new C128625wP(r0);
                String str = r0.A0A;
                synchronized (r22) {
                    Log.i("dyiReportManager/download-report");
                    C127315uI A03 = r22.A03(str);
                    if (A03 == null) {
                        Log.e("dyiReportManager/download-report no valid report info");
                    } else {
                        String str2 = A03.A02;
                        if (str2 == null) {
                            Log.e("dyiReportManager/download-report no url");
                        } else {
                            C14850m9 r8 = r22.A08;
                            C18790t3 r6 = r22.A04;
                            C120035fV r42 = new C120035fV(r22.A03, r6, r22.A07, r8, r22.A09, new C126655tE(str2), r22.A0B, r22.A02.A0G(str));
                            r22.A00 = r42;
                            r42.A5g(new AnonymousClass68V(r22, r1, str));
                            Log.i("dyiReportManager/on-report-downloading");
                            r22.A0A.A0B(3, str);
                            r22.A00.A01();
                        }
                    }
                    r22.A05(str);
                    r1.A00();
                }
            }
        });
        if ("personal".equals(r4.A0V) && ((ActivityC13810kN) r4).A0C.A07(1214)) {
            ClipboardManager A0B = ((ActivityC13810kN) r4).A08.A0B();
            if (A0B != null) {
                try {
                    A0B.setPrimaryClip(ClipData.newPlainText("password", r4.A0X));
                } catch (NullPointerException | SecurityException e) {
                    Log.e("paymentsDyi/clipboard/", e);
                }
            }
            C004802e r3 = new C004802e(r4, R.style.FbPayDialogTheme);
            r3.A07(R.string.dyi_report_password_dialog_title);
            StringBuilder A0h = C12960it.A0h();
            A0h.append(r4.getBaseContext().getString(R.string.dyi_report_password_dialog_message_1));
            A0h.append(" ");
            A0h.append(r4.A0X);
            A0h.append("\n\n");
            r3.A0A(C12960it.A0d(r4.getString(R.string.dyi_report_password_dialog_message_2), A0h));
            r3.setNegativeButton(R.string.ok, null);
            C12970iu.A1J(r3);
        }
    }
}
