package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2nF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57552nF extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57552nF A08;
    public static volatile AnonymousClass255 A09;
    public int A00;
    public int A01 = 0;
    public int A02;
    public AnonymousClass1K6 A03 = AnonymousClass277.A01;
    public C43261wh A04;
    public Object A05;
    public String A06 = "";
    public String A07 = "";

    static {
        C57552nF r0 = new C57552nF();
        A08 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c7, code lost:
        if (r12.A01 == 2) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cc, code lost:
        if (r12.A01 == 3) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d1, code lost:
        if (r12.A01 == 4) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d6, code lost:
        if (r12.A01 == 5) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d8, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d9, code lost:
        r0 = r14.Afv(r12.A05, r15.A05, r7);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r13, java.lang.Object r14, java.lang.Object r15) {
        /*
        // Method dump skipped, instructions count: 692
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57552nF.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if (this.A01 == 1) {
            i = CodedOutputStream.A07(1, (String) this.A05) + 0;
        } else {
            i = 0;
        }
        if (this.A01 == 2) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 2, i);
        }
        if (this.A01 == 3) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 3, i);
        }
        if (this.A01 == 4) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 4, i);
        }
        if (this.A01 == 5) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 5, i);
        }
        if ((this.A00 & 32) == 32) {
            i = AbstractC27091Fz.A04(6, this.A06, i);
        }
        if ((this.A00 & 64) == 64) {
            i = AbstractC27091Fz.A04(7, this.A07, i);
        }
        if ((this.A00 & 128) == 128) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i = AbstractC27091Fz.A08(r0, 8, i);
        }
        for (int i3 = 0; i3 < this.A03.size(); i3++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A03.get(i3), 9, i);
        }
        if ((this.A00 & 256) == 256) {
            i = AbstractC27091Fz.A03(10, this.A02, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0I(1, (String) this.A05);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 2);
        }
        if (this.A01 == 3) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 3);
        }
        if (this.A01 == 4) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 4);
        }
        if (this.A01 == 5) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(6, this.A06);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A07);
        }
        if ((this.A00 & 128) == 128) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 8);
        }
        int i = 0;
        while (i < this.A03.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A03, i, 9);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0E(10, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
