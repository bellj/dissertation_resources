package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* renamed from: X.3HI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HI {
    public Bitmap A00;
    public final Rect A01 = C12980iv.A0J();
    public final Rect A02 = C12980iv.A0J();
    public final Rect A03;
    public final AbstractC39021p8 A04;
    public final C88924Hz A05;
    public final C87754Cu A06;
    public final boolean A07;
    public final int[] A08;
    public final C91744Sy[] A09;

    public AnonymousClass3HI(Rect rect, C88924Hz r8, C87754Cu r9, boolean z) {
        int length;
        this.A06 = r9;
        this.A05 = r8;
        AbstractC39021p8 r5 = r8.A00;
        this.A04 = r5;
        int[] frameDurations = r5.getFrameDurations();
        this.A08 = frameDurations;
        int i = 0;
        while (true) {
            length = frameDurations.length;
            if (i >= length) {
                break;
            }
            if (frameDurations[i] < 11) {
                frameDurations[i] = 100;
            }
            i++;
        }
        int i2 = 0;
        do {
            i2++;
        } while (i2 < length);
        for (int i3 = 0; i3 < length; i3++) {
        }
        this.A03 = A00(rect, r5);
        this.A07 = z;
        this.A09 = new C91744Sy[r5.getFrameCount()];
        for (int i4 = 0; i4 < this.A04.getFrameCount(); i4++) {
            this.A09[i4] = this.A04.getFrameInfo(i4);
        }
    }

    public static Rect A00(Rect rect, AbstractC39021p8 r5) {
        int min;
        int min2;
        if (rect == null) {
            min = r5.getWidth();
            min2 = r5.getHeight();
        } else {
            min = Math.min(rect.width(), r5.getWidth());
            min2 = Math.min(rect.height(), r5.getHeight());
        }
        return new Rect(0, 0, min, min2);
    }

    public final synchronized void A01(int i, int i2) {
        Bitmap bitmap;
        Bitmap bitmap2 = this.A00;
        if (bitmap2 != null && ((bitmap2.getWidth() < i || this.A00.getHeight() < i2) && (bitmap = this.A00) != null)) {
            bitmap.recycle();
            this.A00 = null;
        }
        Bitmap bitmap3 = this.A00;
        if (bitmap3 == null) {
            bitmap3 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            this.A00 = bitmap3;
        }
        bitmap3.eraseColor(0);
    }

    public void A02(Canvas canvas, int i) {
        int width;
        int height;
        int xOffset;
        int yOffset;
        AbstractC39021p8 r6 = this.A04;
        AbstractC39031p9 frame = r6.getFrame(i);
        try {
            if (r6.doesRenderSupportScaling()) {
                Rect rect = this.A03;
                double width2 = ((double) rect.width()) / ((double) r6.getWidth());
                double height2 = ((double) rect.height()) / ((double) r6.getHeight());
                int round = (int) Math.round(((double) frame.getWidth()) * width2);
                int round2 = (int) Math.round(((double) frame.getHeight()) * height2);
                int xOffset2 = (int) (((double) frame.getXOffset()) * width2);
                int yOffset2 = (int) (((double) frame.getYOffset()) * height2);
                synchronized (this) {
                    int width3 = rect.width();
                    int height3 = rect.height();
                    A01(width3, height3);
                    frame.renderFrame(round, round2, this.A00);
                    Rect rect2 = this.A02;
                    rect2.set(0, 0, width3, height3);
                    Rect rect3 = this.A01;
                    rect3.set(xOffset2, yOffset2, width3 + xOffset2, height3 + yOffset2);
                    canvas.drawBitmap(this.A00, rect2, rect3, (Paint) null);
                }
            } else {
                if (this.A07) {
                    float max = Math.max(((float) frame.getWidth()) / ((float) Math.min(frame.getWidth(), canvas.getWidth())), ((float) frame.getHeight()) / ((float) Math.min(frame.getHeight(), canvas.getHeight())));
                    width = (int) (((float) frame.getWidth()) / max);
                    height = (int) (((float) frame.getHeight()) / max);
                    xOffset = (int) (((float) frame.getXOffset()) / max);
                    yOffset = (int) (((float) frame.getYOffset()) / max);
                } else {
                    width = frame.getWidth();
                    height = frame.getHeight();
                    xOffset = frame.getXOffset();
                    yOffset = frame.getYOffset();
                }
                synchronized (this) {
                    A01(width, height);
                    frame.renderFrame(width, height, this.A00);
                    canvas.save();
                    canvas.translate((float) xOffset, (float) yOffset);
                    canvas.drawBitmap(this.A00, 0.0f, 0.0f, (Paint) null);
                    canvas.restore();
                }
            }
        } finally {
            frame.dispose();
        }
    }
}
