package X;

/* renamed from: X.4wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107214wy implements AnonymousClass5X7 {
    public static final float[] A0B = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 1.0f};
    public long A00;
    public long A01;
    public AnonymousClass5X6 A02;
    public AnonymousClass4W7 A03;
    public String A04;
    public boolean A05;
    public final C94104bG A06;
    public final C92814Xn A07;
    public final C92494Wd A08;
    public final C95304dT A09;
    public final boolean[] A0A;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107214wy() {
        this(null);
    }

    public C107214wy(C92494Wd r3) {
        this.A08 = r3;
        this.A0A = new boolean[4];
        this.A06 = new C94104bG();
        this.A07 = new C92814Xn(178);
        this.A09 = new C95304dT();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0243, code lost:
        if (r8 >= 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01df, code lost:
        if (r5 == 179) goto L_0x01e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0229, code lost:
        if (r5 != 181) goto L_0x022b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012a  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r24) {
        /*
        // Method dump skipped, instructions count: 588
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107214wy.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r3, C92824Xo r4) {
        r4.A03();
        this.A04 = r4.A02();
        AnonymousClass5X6 Af4 = r3.Af4(r4.A01(), 2);
        this.A02 = Af4;
        this.A03 = new AnonymousClass4W7(Af4);
        this.A08.A00(r3, r4);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A00 = j;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AbP() {
        /*
            r3 = this;
            boolean[] r0 = r3.A0A
            boolean r2 = X.C72453ed.A1W(r0)
            X.4bG r0 = r3.A06
            r0.A03 = r2
            r0.A00 = r2
            r0.A01 = r2
            X.4W7 r1 = r3.A03
            if (r1 == 0) goto L_0x001b
            r1.A05 = r2
            r1.A04 = r2
            r1.A06 = r2
            r0 = -1
            r1.A00 = r0
        L_0x001b:
            X.4Xn r1 = r3.A07
            if (r1 == 0) goto L_0x0024
            r0 = 0
            r1.A02 = r0
            r1.A01 = r0
        L_0x0024:
            r0 = 0
            r3.A01 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107214wy.AbP():void");
    }
}
