package X;

import java.nio.charset.CharsetDecoder;
import java.util.regex.Pattern;

/* renamed from: X.3mY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76983mY extends AbstractC107514xS {
    public static final Pattern A02 = Pattern.compile("(.+?)='(.*?)';", 32);
    public final CharsetDecoder A00 = C88814Hf.A00.newDecoder();
    public final CharsetDecoder A01 = C88814Hf.A05.newDecoder();
}
