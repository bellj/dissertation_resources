package X;

/* renamed from: X.4VC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VC {
    public long A00 = -1;
    public final AnonymousClass5WT A01;

    public AnonymousClass4VC(AnonymousClass5WT r3) {
        this.A01 = r3;
    }

    public long A00() {
        long j = this.A00;
        if (j != -1) {
            return j;
        }
        this.A00 = 0;
        AnonymousClass5WT r6 = this.A01;
        int frameCount = r6.getFrameCount();
        for (int i = 0; i < frameCount; i++) {
            this.A00 += (long) r6.AD9(i);
        }
        return this.A00;
    }
}
