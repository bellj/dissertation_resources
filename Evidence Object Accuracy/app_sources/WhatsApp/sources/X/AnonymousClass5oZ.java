package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5oZ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oZ extends AbstractC16350or {
    public final ArrayList A00;
    public final /* synthetic */ PaymentGroupParticipantPickerActivity A01;

    public AnonymousClass5oZ(PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity, ArrayList arrayList) {
        ArrayList arrayList2;
        this.A01 = paymentGroupParticipantPickerActivity;
        if (arrayList != null) {
            arrayList2 = C12980iv.A0x(arrayList);
        } else {
            arrayList2 = null;
        }
        this.A00 = arrayList2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        ArrayList A0l = C12960it.A0l();
        HashSet A12 = C12970iu.A12();
        ArrayList arrayList = this.A00;
        if (arrayList == null || arrayList.isEmpty()) {
            A0l.addAll(this.A01.A0N);
            return A0l;
        }
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A01;
        Iterator it = paymentGroupParticipantPickerActivity.A0N.iterator();
        while (it.hasNext()) {
            C126985tl r4 = (C126985tl) it.next();
            C15370n3 r3 = r4.A00;
            Jid A0B = r3.A0B(UserJid.class);
            if (paymentGroupParticipantPickerActivity.A05.A0M(r3, arrayList, true) && !A12.contains(A0B)) {
                A0l.add(r4);
                A12.add(A0B);
            }
            if (this.A02.isCancelled()) {
                break;
            }
        }
        return A0l;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A01;
        paymentGroupParticipantPickerActivity.A0E = null;
        C117585aE r0 = paymentGroupParticipantPickerActivity.A0F;
        r0.A00 = (List) obj;
        r0.notifyDataSetChanged();
    }
}
