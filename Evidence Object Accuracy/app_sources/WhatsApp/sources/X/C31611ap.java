package X;

import java.util.LinkedList;

/* renamed from: X.1ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31611ap {
    public LinkedList A00;
    public C31661au A01;
    public boolean A02;

    public C31611ap() {
        this.A01 = new C31661au();
        this.A00 = new LinkedList();
        this.A02 = false;
        this.A02 = true;
    }

    public C31611ap(byte[] bArr) {
        this.A01 = new C31661au();
        this.A00 = new LinkedList();
        this.A02 = false;
        C32071bZ r2 = (C32071bZ) AbstractC27091Fz.A0E(C32071bZ.A03, bArr);
        C31321aM r1 = r2.A02;
        this.A01 = new C31661au(r1 == null ? C31321aM.A0E : r1);
        this.A02 = false;
        for (C31321aM r22 : r2.A01) {
            this.A00.add(new C31661au(r22));
        }
    }
}
