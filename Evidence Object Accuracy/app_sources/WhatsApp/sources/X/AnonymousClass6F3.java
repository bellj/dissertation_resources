package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.6F3  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6F3 implements Iterable {
    public final List A00 = C12960it.A0l();

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return this.A00.iterator();
    }
}
