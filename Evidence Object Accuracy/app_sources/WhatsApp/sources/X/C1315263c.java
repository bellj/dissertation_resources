package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315263c implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(36);
    public final C1316363n A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315263c(C1316363n r2) {
        this.A01 = null;
        this.A00 = r2;
    }

    public C1315263c(C1316363n r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }
}
