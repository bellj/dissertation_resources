package X;

/* renamed from: X.4BT  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BT {
    A01(0),
    A02(1),
    A03(2);
    
    public final int value;

    AnonymousClass4BT(int i) {
        this.value = i;
    }

    public static AnonymousClass4BT A00(int i) {
        if (i == 0) {
            return A01;
        }
        if (i == 1) {
            return A02;
        }
        if (i != 2) {
            return null;
        }
        return A03;
    }
}
