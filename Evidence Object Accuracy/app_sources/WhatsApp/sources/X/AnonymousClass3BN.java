package X;

import java.util.List;

/* renamed from: X.3BN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BN {
    public AbstractC14200l1 A00;
    public List A01;
    public final String A02;

    public AnonymousClass3BN(AnonymousClass28D r5) {
        List<AnonymousClass28D> A0M;
        String str = "";
        this.A02 = r5 != null ? r5.A0J(38, str) : str;
        AbstractC14200l1 r0 = null;
        if (r5 != null) {
            r5.A0I(42);
            r0 = r5.A0G(43);
        }
        this.A00 = r0;
        if (!(r5 == null || (A0M = r5.A0M(44)) == null)) {
            this.A01 = C12960it.A0l();
            for (AnonymousClass28D r2 : A0M) {
                this.A01.add(new AnonymousClass4LV(r2));
            }
        }
    }
}
