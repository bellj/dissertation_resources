package X;

/* renamed from: X.4Ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87134Ai {
    /* Fake field, exist only in values array */
    EF5(0),
    /* Fake field, exist only in values array */
    EF13(1),
    /* Fake field, exist only in values array */
    EF21(2);
    
    public final int mIntValue;

    EnumC87134Ai(int i) {
        this.mIntValue = i;
    }
}
