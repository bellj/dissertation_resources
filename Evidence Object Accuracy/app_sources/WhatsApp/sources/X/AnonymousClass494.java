package X;

import java.io.IOException;

/* renamed from: X.494  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass494 extends IOException {
    public static final long serialVersionUID = -6947486886997889499L;

    public AnonymousClass494() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }

    public AnonymousClass494(String str, Throwable th) {
        super(C12960it.A0d(str, C12960it.A0k("CodedOutputStream was writing to a flat byte array and ran out of space.: ")), th);
    }

    public AnonymousClass494(Throwable th) {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
    }
}
