package X;

/* renamed from: X.0SA  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0SA {
    public static final AnonymousClass0SA A01 = new AnonymousClass0SA("FOLD");
    public static final AnonymousClass0SA A02 = new AnonymousClass0SA("HINGE");
    public final String A00;

    public AnonymousClass0SA(String str) {
        this.A00 = str;
    }

    public String toString() {
        return this.A00;
    }
}
