package X;

import android.database.Observable;

/* renamed from: X.03Z  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03Z extends Observable {
    public void A00() {
        int size = ((Observable) this).mObservers.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AnonymousClass0QE) ((Observable) this).mObservers.get(size)).A00();
            } else {
                return;
            }
        }
    }

    public void A01(int i, int i2) {
        for (int size = ((Observable) this).mObservers.size() - 1; size >= 0; size--) {
            ((AnonymousClass0QE) ((Observable) this).mObservers.get(size)).A04(i, i2, 1);
        }
    }

    public void A02(int i, int i2) {
        int size = ((Observable) this).mObservers.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AnonymousClass0QE) ((Observable) this).mObservers.get(size)).A02(i, i2);
            } else {
                return;
            }
        }
    }

    public void A03(int i, int i2) {
        int size = ((Observable) this).mObservers.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AnonymousClass0QE) ((Observable) this).mObservers.get(size)).A03(i, i2);
            } else {
                return;
            }
        }
    }

    public void A04(Object obj, int i, int i2) {
        int size = ((Observable) this).mObservers.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AnonymousClass0QE) ((Observable) this).mObservers.get(size)).A05(obj, i, i2);
            } else {
                return;
            }
        }
    }

    public boolean A05() {
        return !((Observable) this).mObservers.isEmpty();
    }
}
