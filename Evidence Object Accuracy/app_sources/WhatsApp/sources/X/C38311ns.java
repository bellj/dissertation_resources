package X;

import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.1ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38311ns {
    public static boolean A00(C14850m9 r1, Jid jid) {
        ArrayList arrayList;
        if (jid == null || !r1.A07(1377)) {
            return false;
        }
        String A03 = r1.A03(1607);
        if (TextUtils.isEmpty(A03)) {
            arrayList = new ArrayList();
        } else {
            AnonymousClass009.A05(A03);
            arrayList = new ArrayList(Arrays.asList(A03.split(",")));
        }
        return arrayList.contains(jid.user);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r2.contains("session") != false) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01(X.C14850m9 r5, X.C30761Ys r6) {
        /*
            int r1 = r6.A03
            r0 = 2
            if (r0 != r1) goto L_0x005d
            java.lang.String r4 = r6.A05
            android.net.Uri r3 = android.net.Uri.parse(r4)     // Catch: Exception -> 0x0043
            java.util.Set r2 = r3.getQueryParameterNames()     // Catch: Exception -> 0x0043
            java.lang.String r1 = r3.getHost()     // Catch: Exception -> 0x0043
            java.lang.String r0 = "whatsapp.com"
            boolean r0 = r1.contains(r0)     // Catch: Exception -> 0x0043
            if (r0 == 0) goto L_0x005d
            java.lang.String r1 = r3.getPath()     // Catch: Exception -> 0x0043
            java.lang.String r0 = "/survey/"
            boolean r0 = r1.equals(r0)     // Catch: Exception -> 0x0043
            if (r0 == 0) goto L_0x005d
            java.lang.String r0 = "oid"
            boolean r0 = r2.contains(r0)     // Catch: Exception -> 0x0043
            if (r0 != 0) goto L_0x0039
            java.lang.String r0 = "session"
            boolean r0 = r2.contains(r0)     // Catch: Exception -> 0x0043
            if (r0 == 0) goto L_0x005d
        L_0x0039:
            r0 = 1377(0x561, float:1.93E-42)
            boolean r1 = r5.A07(r0)
            r0 = 1
            if (r1 != 0) goto L_0x005e
            goto L_0x005d
        L_0x0043:
            r2 = move-exception
            java.lang.String r0 = "InAppSurveyUtils/isInAppSurveyURL/<"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = "> is not a valid URL. Error="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x005d:
            r0 = 0
        L_0x005e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38311ns.A01(X.0m9, X.1Ys):boolean");
    }
}
