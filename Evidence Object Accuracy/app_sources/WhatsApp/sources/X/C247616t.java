package X;

import android.content.ContentValues;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import java.util.Map;

/* renamed from: X.16t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C247616t {
    public final AnonymousClass171 A00;

    public C247616t(AnonymousClass171 r1) {
        this.A00 = r1;
    }

    public void A00(C15580nU r20, long j) {
        AnonymousClass171 r11 = this.A00;
        C16310on A02 = r11.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("announcement_version", Long.valueOf(j));
            if (A02.A03.A00("group_notification_version", contentValues, "group_jid_row_id = ?", new String[]{String.valueOf(r11.A00.A01(r20))}) != 1) {
                r11.A01(r20, 0, j, 0);
            }
            A00.A00();
            A02.A03(new RunnableBRunnable0Shape4S0200000_I0_4(r11, 15, r20));
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(C15580nU r20, long j) {
        AnonymousClass171 r11 = this.A00;
        C16310on A02 = r11.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("participant_version", Long.valueOf(j));
            if (A02.A03.A00("group_notification_version", contentValues, "group_jid_row_id = ?", new String[]{String.valueOf(r11.A00.A01(r20))}) != 1) {
                r11.A01(r20, 0, 0, j);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(C15580nU r3, long j) {
        Map map = this.A00.A03;
        synchronized (map) {
            map.put(r3, Long.valueOf(j));
        }
    }

    public void A03(C15580nU r21, long j) {
        AnonymousClass171 r12 = this.A00;
        C16310on A02 = r12.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("subject_timestamp", Long.valueOf(j));
            if (A02.A03.A00("group_notification_version", contentValues, "group_jid_row_id = ?", new String[]{String.valueOf(r12.A00.A01(r21))}) != 1) {
                r12.A01(r21, j, 0, 0);
            }
            A00.A00();
            A02.A03(new RunnableBRunnable0Shape4S0200000_I0_4(r12, 14, r21));
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
