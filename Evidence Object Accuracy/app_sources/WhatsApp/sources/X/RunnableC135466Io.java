package X;

/* renamed from: X.6Io  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135466Io implements Runnable {
    public final /* synthetic */ C118025b9 A00;
    public final /* synthetic */ boolean A01;

    public /* synthetic */ RunnableC135466Io(C118025b9 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C118025b9 r6 = this.A00;
        boolean z = this.A01;
        AbstractC38191ng A0K = C117305Zk.A0K(r6.A03);
        if (A0K != null && C117305Zk.A1U(A0K.A07)) {
            AnonymousClass17Z r3 = r6.A04;
            AnonymousClass2S0 A00 = r3.A00();
            if (z) {
                r6.A01.A0A(AnonymousClass617.A00(A00));
            }
            r3.A06(new AnonymousClass2S2(new AnonymousClass69G(r6, z), r3, A0K.A0A(A00.A01, A00.A02)), false);
        }
    }
}
