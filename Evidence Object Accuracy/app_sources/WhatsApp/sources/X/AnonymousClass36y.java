package X;

import java.lang.ref.WeakReference;

/* renamed from: X.36y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36y extends AbstractC16350or {
    public final C22190yg A00;
    public final WeakReference A01;

    public AnonymousClass36y(AbstractC001200n r2, C91294Re r3, C22190yg r4) {
        super(r2);
        this.A00 = r4;
        this.A01 = C12970iu.A10(r3);
    }
}
