package X;

import java.io.Serializable;

/* renamed from: X.5Ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112625Ec implements AbstractC16710pd, Serializable {
    public final Object value;

    public C112625Ec(Object obj) {
        this.value = obj;
    }

    @Override // X.AbstractC16710pd
    public Object getValue() {
        return this.value;
    }

    @Override // java.lang.Object
    public String toString() {
        return String.valueOf(this.value);
    }
}
