package X;

import android.view.View;
import android.view.ViewParent;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar$Behavior;

/* renamed from: X.2eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53712eu extends AnonymousClass04X {
    public int A00 = -1;
    public int A01;
    public final /* synthetic */ SwipeDismissBehavior A02;

    public C53712eu(SwipeDismissBehavior swipeDismissBehavior) {
        this.A02 = swipeDismissBehavior;
    }

    @Override // X.AnonymousClass04X
    public void A00(int i) {
        AnonymousClass5R4 r2 = this.A02.A04;
        if (r2 != null) {
            AnonymousClass51A r22 = (AnonymousClass51A) r2;
            if (i == 0) {
                C64883Hh.A00().A03(r22.A00.A07);
            } else if (i == 1 || i == 2) {
                C64883Hh.A00().A02(r22.A00.A07);
            }
        }
    }

    @Override // X.AnonymousClass04X
    public int A01(View view) {
        return view.getWidth();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r3 != false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1 = r1 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r3 != false) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        r2 = r2 - r1;
        r1 = r4.A01;
     */
    @Override // X.AnonymousClass04X
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A03(android.view.View r5, int r6, int r7) {
        /*
            r4 = this;
            int r0 = X.AnonymousClass028.A05(r5)
            r1 = 1
            boolean r3 = X.C12960it.A1V(r0, r1)
            com.google.android.material.behavior.SwipeDismissBehavior r0 = r4.A02
            int r0 = r0.A02
            if (r0 != 0) goto L_0x0023
            int r2 = r4.A01
            int r1 = r5.getWidth()
            if (r3 == 0) goto L_0x002d
        L_0x0017:
            int r2 = r2 - r1
            int r1 = r4.A01
        L_0x001a:
            int r0 = java.lang.Math.max(r2, r6)
            int r0 = java.lang.Math.min(r0, r1)
            return r0
        L_0x0023:
            int r2 = r4.A01
            if (r0 != r1) goto L_0x002f
            int r1 = r5.getWidth()
            if (r3 == 0) goto L_0x0017
        L_0x002d:
            int r1 = r1 + r2
            goto L_0x001a
        L_0x002f:
            int r0 = r5.getWidth()
            int r2 = r2 - r0
            int r0 = r4.A01
            int r1 = r5.getWidth()
            int r1 = r1 + r0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53712eu.A03(android.view.View, int, int):int");
    }

    @Override // X.AnonymousClass04X
    public int A04(View view, int i, int i2) {
        return view.getTop();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005d, code lost:
        if (r2 != false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        if (r8 <= 0.0f) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007a, code lost:
        if (java.lang.Math.abs(r7.getLeft() - r6.A01) >= java.lang.Math.round(X.C12990iw.A02(r7) * 0.5f)) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if (r2 != false) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r8 >= 0.0f) goto L_0x007d;
     */
    @Override // X.AnonymousClass04X
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.view.View r7, float r8, float r9) {
        /*
            r6 = this;
            r0 = -1
            r6.A00 = r0
            int r5 = r7.getWidth()
            r4 = 0
            r3 = 1
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x0064
            int r0 = X.AnonymousClass028.A05(r7)
            boolean r2 = X.C12960it.A1V(r0, r3)
            com.google.android.material.behavior.SwipeDismissBehavior r0 = r6.A02
            int r1 = r0.A02
            r0 = 2
            if (r1 == r0) goto L_0x0024
            if (r1 != 0) goto L_0x005b
            if (r2 == 0) goto L_0x005f
        L_0x0020:
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x007d
        L_0x0024:
            int r0 = r7.getLeft()
            int r4 = r6.A01
            if (r0 >= r4) goto L_0x0059
            int r4 = r4 - r5
        L_0x002d:
            com.google.android.material.behavior.SwipeDismissBehavior r2 = r6.A02
            X.0Su r1 = r2.A03
            int r0 = r7.getTop()
            boolean r0 = r1.A0C(r4, r0)
            if (r0 == 0) goto L_0x0045
            r1 = 0
            com.facebook.redex.RunnableBRunnable0Shape1S0210000_I1 r0 = new com.facebook.redex.RunnableBRunnable0Shape1S0210000_I1
            r0.<init>(r2, r7, r1, r3)
            r7.postOnAnimation(r0)
        L_0x0044:
            return
        L_0x0045:
            if (r3 == 0) goto L_0x0044
            X.5R4 r1 = r2.A04
            if (r1 == 0) goto L_0x0044
            X.51A r1 = (X.AnonymousClass51A) r1
            r0 = 8
            r7.setVisibility(r0)
            X.0mf r1 = r1.A00
            r0 = 0
            r1.A04(r0)
            return
        L_0x0059:
            int r4 = r4 + r5
            goto L_0x002d
        L_0x005b:
            if (r1 != r3) goto L_0x007d
            if (r2 == 0) goto L_0x0020
        L_0x005f:
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x007d
            goto L_0x0024
        L_0x0064:
            int r2 = r7.getLeft()
            int r0 = r6.A01
            int r2 = r2 - r0
            float r1 = X.C12990iw.A02(r7)
            r0 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 * r0
            int r1 = java.lang.Math.round(r1)
            int r0 = java.lang.Math.abs(r2)
            if (r0 < r1) goto L_0x007d
            goto L_0x0024
        L_0x007d:
            int r4 = r6.A01
            r3 = 0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53712eu.A05(android.view.View, float, float):void");
    }

    @Override // X.AnonymousClass04X
    public void A06(View view, int i) {
        this.A00 = i;
        this.A01 = view.getLeft();
        ViewParent parent = view.getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    @Override // X.AnonymousClass04X
    public void A07(View view, int i, int i2, int i3, int i4) {
        float A02 = C12990iw.A02(view);
        SwipeDismissBehavior swipeDismissBehavior = this.A02;
        float f = ((float) this.A01) + (A02 * swipeDismissBehavior.A01);
        float A022 = ((float) this.A01) + (C12990iw.A02(view) * swipeDismissBehavior.A00);
        float f2 = (float) i;
        if (f2 <= f) {
            view.setAlpha(1.0f);
        } else if (f2 >= A022) {
            view.setAlpha(0.0f);
        } else {
            view.setAlpha(Math.min(Math.max(0.0f, 1.0f - ((f2 - f) / (A022 - f))), 1.0f));
        }
    }

    @Override // X.AnonymousClass04X
    public boolean A08(View view, int i) {
        boolean z;
        if (this.A00 == -1) {
            if (!(this.A02 instanceof BaseTransientBottomBar$Behavior)) {
                z = true;
            } else {
                z = view instanceof C34291fu;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }
}
