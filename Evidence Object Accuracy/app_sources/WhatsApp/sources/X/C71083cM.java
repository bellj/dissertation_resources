package X;

import android.app.Application;
import android.app.Service;

/* renamed from: X.3cM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71083cM implements AnonymousClass005 {
    public Object A00;
    public final Service A01;

    public C71083cM(Service service) {
        this.A01 = service;
    }

    @Override // X.AnonymousClass005
    public Object generatedComponent() {
        Object obj = this.A00;
        if (obj != null) {
            return obj;
        }
        Service service = this.A01;
        Application application = service.getApplication();
        Object[] A1b = C12970iu.A1b();
        A1b[0] = application.getClass();
        AnonymousClass2PX.A00("Hilt service must be attached to an @AndroidEntryPoint Application. Found: %s", A1b, application instanceof AnonymousClass005);
        C90034Mi r0 = new C90034Mi(((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, application)).ANr);
        r0.A00 = service;
        C58182oH r1 = new C58182oH(r0.A01);
        this.A00 = r1;
        return r1;
    }
}
