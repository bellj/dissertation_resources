package X;

import android.content.Context;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

/* renamed from: X.3FC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FC {
    public final int A00;
    public final View A01;
    public final View A02;

    public AnonymousClass3FC(View view, View view2, int i) {
        this.A02 = view;
        this.A01 = view2;
        this.A00 = i;
        if (Build.VERSION.SDK_INT >= 21) {
            view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oe
                @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                public final void onScrollChanged() {
                    AnonymousClass3FC.this.A01();
                }
            });
        }
    }

    public void A00() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A02.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101934oK(this));
        }
    }

    public final void A01() {
        float f;
        boolean canScrollVertically = this.A02.canScrollVertically(1);
        View view = this.A01;
        if (canScrollVertically) {
            f = (float) this.A00;
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
    }

    public void A02(Context context, ClickableSpan clickableSpan, TextView textView, String str, int i) {
        SpannableString spannableString = new SpannableString(str);
        int length = str.length();
        spannableString.setSpan(clickableSpan, 0, length, 0);
        spannableString.setSpan(new TextAppearanceSpan(context, i), 0, length, 0);
        spannableString.setSpan(new C73563gR(this), 0, length, 0);
        C12990iw.A1F(textView);
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }
}
