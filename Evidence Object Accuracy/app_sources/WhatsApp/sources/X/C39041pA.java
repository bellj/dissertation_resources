package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1pA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39041pA implements Closeable {
    public static final Charset A0D = AnonymousClass01V.A0A;
    public int A00;
    public long A01 = 0;
    public long A02 = 0;
    public Writer A03;
    public final int A04;
    public final int A05;
    public final long A06;
    public final File A07;
    public final File A08;
    public final File A09;
    public final LinkedHashMap A0A = new LinkedHashMap(0, 0.75f, true);
    public final Callable A0B = new AnonymousClass5DZ(this);
    public final ExecutorService A0C = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());

    public C39041pA(File file, long j) {
        this.A07 = file;
        this.A04 = 1;
        this.A08 = new File(file, "journal");
        this.A09 = new File(file, "journal.tmp");
        this.A05 = 1;
        this.A06 = j;
    }

    public static C39041pA A00(File file, long j) {
        String A01;
        if (j > 0) {
            C39041pA r5 = new C39041pA(file, j);
            File file2 = r5.A08;
            if (file2.exists()) {
                try {
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file2), DefaultCrypto.BUFFER_SIZE);
                    String A012 = A01(bufferedInputStream);
                    String A013 = A01(bufferedInputStream);
                    String A014 = A01(bufferedInputStream);
                    String A015 = A01(bufferedInputStream);
                    String A016 = A01(bufferedInputStream);
                    if ("libcore.io.DiskLruCache".equals(A012) && "1".equals(A013)) {
                        String num = Integer.toString(1);
                        if (num.equals(A014) && num.equals(A015) && "".equals(A016)) {
                            while (true) {
                                try {
                                    A01 = A01(bufferedInputStream);
                                    String[] split = A01.split(" ");
                                    int length = split.length;
                                    if (length >= 2) {
                                        String str = split[1];
                                        if (!split[0].equals("REMOVE") || length != 2) {
                                            LinkedHashMap linkedHashMap = r5.A0A;
                                            AnonymousClass4XO r12 = (AnonymousClass4XO) linkedHashMap.get(str);
                                            if (r12 == null) {
                                                r12 = new AnonymousClass4XO(r5, str);
                                                linkedHashMap.put(str, r12);
                                            }
                                            String str2 = split[0];
                                            if (str2.equals("CLEAN") && length == 3) {
                                                r12.A02 = true;
                                                r12.A01 = null;
                                                int min = Math.min(1, 1);
                                                Object[] objArr = (Object[]) Array.newInstance(split.getClass().getComponentType(), 1);
                                                System.arraycopy(split, 2, objArr, 0, min);
                                                String[] strArr = (String[]) objArr;
                                                int length2 = strArr.length;
                                                if (length2 == r12.A05.A05) {
                                                    for (int i = 0; i < length2; i++) {
                                                        try {
                                                            r12.A04[i] = Long.parseLong(strArr[i]);
                                                        } catch (NumberFormatException unused) {
                                                            StringBuilder sb = new StringBuilder("unexpected journal line: ");
                                                            sb.append(Arrays.toString(strArr));
                                                            throw new IOException(sb.toString());
                                                        }
                                                    }
                                                    continue;
                                                } else {
                                                    StringBuilder sb2 = new StringBuilder("unexpected journal line: ");
                                                    sb2.append(Arrays.toString(strArr));
                                                    throw new IOException(sb2.toString());
                                                }
                                            } else if (str2.equals("DIRTY") && length == 2) {
                                                r12.A01 = new C39061pE(r12, r5);
                                            } else if (!str2.equals("READ") || length != 2) {
                                                break;
                                            }
                                        } else {
                                            r5.A0A.remove(str);
                                        }
                                    } else {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("unexpected journal line: ");
                                        sb3.append(A01);
                                        throw new IOException(sb3.toString());
                                    }
                                } catch (EOFException unused2) {
                                    A03(bufferedInputStream);
                                    A05(r5.A09);
                                    Iterator it = r5.A0A.values().iterator();
                                    while (it.hasNext()) {
                                        AnonymousClass4XO r4 = (AnonymousClass4XO) it.next();
                                        if (r4.A01 == null) {
                                            r5.A02 += r4.A04[0];
                                        } else {
                                            r4.A01 = null;
                                            A05(r4.A00(0));
                                            A05(r4.A01(0));
                                            it.remove();
                                        }
                                    }
                                    r5.A03 = new BufferedWriter(new FileWriter(file2, true), DefaultCrypto.BUFFER_SIZE);
                                    return r5;
                                }
                            }
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("unexpected journal line: ");
                            sb4.append(A01);
                            throw new IOException(sb4.toString());
                        }
                    }
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("unexpected journal header: [");
                    sb5.append(A012);
                    sb5.append(", ");
                    sb5.append(A013);
                    sb5.append(", ");
                    sb5.append(A015);
                    sb5.append(", ");
                    sb5.append(A016);
                    sb5.append("]");
                    throw new IOException(sb5.toString());
                } catch (IOException unused3) {
                    r5.close();
                    A04(r5.A07);
                }
            }
            file.mkdirs();
            C39041pA r52 = new C39041pA(file, j);
            r52.A0A();
            return r52;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    public static String A01(InputStream inputStream) {
        StringBuilder sb = new StringBuilder(80);
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                throw new EOFException();
            } else if (read == 10) {
                int length = sb.length();
                if (length > 0) {
                    int i = length - 1;
                    if (sb.charAt(i) == '\r') {
                        sb.setLength(i);
                    }
                }
                return sb.toString();
            } else {
                sb.append((char) read);
            }
        }
    }

    public static /* synthetic */ void A02(C39061pE r10, C39041pA r11, boolean z) {
        synchronized (r11) {
            AnonymousClass4XO r5 = r10.A01;
            if (r5.A01 == r10) {
                if (z && !r5.A02) {
                    for (int i = 0; i < r11.A05; i++) {
                        if (!r5.A01(i).exists()) {
                            A02(r10, r10.A02, false);
                            StringBuilder sb = new StringBuilder();
                            sb.append("edit didn't create file ");
                            sb.append(i);
                            throw new IllegalStateException(sb.toString());
                        }
                    }
                }
                for (int i2 = 0; i2 < r11.A05; i2++) {
                    File A01 = r5.A01(i2);
                    if (!z) {
                        A05(A01);
                    } else if (A01.exists()) {
                        File A00 = r5.A00(i2);
                        A01.renameTo(A00);
                        long[] jArr = r5.A04;
                        long j = jArr[i2];
                        long length = A00.length();
                        jArr[i2] = length;
                        r11.A02 = (r11.A02 - j) + length;
                    }
                }
                r11.A00++;
                r5.A01 = null;
                if (r5.A02 || z) {
                    r5.A02 = true;
                    Writer writer = r11.A03;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("CLEAN ");
                    sb2.append(r5.A03);
                    StringBuilder sb3 = new StringBuilder();
                    long[] jArr2 = r5.A04;
                    for (long j2 : jArr2) {
                        sb3.append(' ');
                        sb3.append(j2);
                    }
                    sb2.append(sb3.toString());
                    sb2.append('\n');
                    writer.write(sb2.toString());
                    if (z) {
                        long j3 = r11.A01;
                        r11.A01 = 1 + j3;
                        r5.A00 = j3;
                    }
                } else {
                    LinkedHashMap linkedHashMap = r11.A0A;
                    String str = r5.A03;
                    linkedHashMap.remove(str);
                    Writer writer2 = r11.A03;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("REMOVE ");
                    sb4.append(str);
                    sb4.append('\n');
                    writer2.write(sb4.toString());
                }
                Writer writer3 = r11.A03;
                if (writer3 != null) {
                    writer3.flush();
                }
                if (r11.A02 > r11.A06 || r11.A0C()) {
                    r11.A0C.submit(r11.A0B);
                }
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public static void A03(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    public static void A04(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    A04(file2);
                }
                if (!file2.delete()) {
                    StringBuilder sb = new StringBuilder("failed to delete file: ");
                    sb.append(file2);
                    throw new IOException(sb.toString());
                }
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("not a directory: ");
        sb2.append(file);
        throw new IllegalArgumentException(sb2.toString());
    }

    public static void A05(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    public static final void A06(String str) {
        if (str.contains(" ") || str.contains("\n") || str.contains("\r")) {
            StringBuilder sb = new StringBuilder("keys must not contain spaces or newlines: \"");
            sb.append(str);
            sb.append("\"");
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0046, code lost:
        if (r1.A01 != null) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C39061pE A07(java.lang.String r6) {
        /*
            r5 = this;
            r4 = r5
            monitor-enter(r4)
            java.io.Writer r0 = r5.A03     // Catch: all -> 0x0052
            if (r0 == 0) goto L_0x004a
            A06(r6)     // Catch: all -> 0x0052
            java.util.LinkedHashMap r0 = r5.A0A     // Catch: all -> 0x0052
            java.lang.Object r1 = r0.get(r6)     // Catch: all -> 0x0052
            X.4XO r1 = (X.AnonymousClass4XO) r1     // Catch: all -> 0x0052
            r3 = 0
            if (r1 != 0) goto L_0x0044
            X.4XO r1 = new X.4XO     // Catch: all -> 0x0052
            r1.<init>(r5, r6)     // Catch: all -> 0x0052
            r0.put(r6, r1)     // Catch: all -> 0x0052
        L_0x001c:
            X.1pE r3 = new X.1pE     // Catch: all -> 0x0052
            r3.<init>(r1, r5)     // Catch: all -> 0x0052
            r1.A01 = r3     // Catch: all -> 0x0052
            java.io.Writer r2 = r5.A03     // Catch: all -> 0x0052
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0052
            r1.<init>()     // Catch: all -> 0x0052
            java.lang.String r0 = "DIRTY "
            r1.append(r0)     // Catch: all -> 0x0052
            r1.append(r6)     // Catch: all -> 0x0052
            r0 = 10
            r1.append(r0)     // Catch: all -> 0x0052
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0052
            r2.write(r0)     // Catch: all -> 0x0052
            java.io.Writer r0 = r5.A03     // Catch: all -> 0x0052
            r0.flush()     // Catch: all -> 0x0052
            goto L_0x0048
        L_0x0044:
            X.1pE r0 = r1.A01     // Catch: all -> 0x0052
            if (r0 == 0) goto L_0x001c
        L_0x0048:
            monitor-exit(r4)
            return r3
        L_0x004a:
            java.lang.String r1 = "cache is closed"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch: all -> 0x0052
            r0.<init>(r1)     // Catch: all -> 0x0052
            throw r0     // Catch: all -> 0x0052
        L_0x0052:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39041pA.A07(java.lang.String):X.1pE");
    }

    public synchronized C39051pB A08(String str) {
        C39051pB r5;
        if (this.A03 != null) {
            A06(str);
            AnonymousClass4XO r6 = (AnonymousClass4XO) this.A0A.get(str);
            r5 = null;
            if (r6 != null && r6.A02) {
                int i = this.A05;
                InputStream[] inputStreamArr = new InputStream[i];
                for (int i2 = 0; i2 < i; i2++) {
                    try {
                        inputStreamArr[i2] = new FileInputStream(r6.A00(i2));
                    } catch (FileNotFoundException unused) {
                        return null;
                    }
                }
                this.A00++;
                this.A03.append((CharSequence) "READ").append(' ').append((CharSequence) str).append('\n');
                if (A0C()) {
                    this.A0C.submit(this.A0B);
                }
                r5 = new C39051pB(this, inputStreamArr);
            }
        } else {
            throw new IllegalStateException("cache is closed");
        }
        return r5;
    }

    public final void A09() {
        while (this.A02 > this.A06) {
            A0B((String) ((Map.Entry) this.A0A.entrySet().iterator().next()).getKey());
        }
    }

    public final synchronized void A0A() {
        Writer writer = this.A03;
        if (writer != null) {
            writer.close();
        }
        File file = this.A09;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file), DefaultCrypto.BUFFER_SIZE);
        bufferedWriter.write("libcore.io.DiskLruCache");
        bufferedWriter.write("\n");
        bufferedWriter.write("1");
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.A04));
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.A05));
        bufferedWriter.write("\n");
        bufferedWriter.write("\n");
        for (AnonymousClass4XO r2 : this.A0A.values()) {
            if (r2.A01 != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("DIRTY ");
                sb.append(r2.A03);
                sb.append('\n');
                bufferedWriter.write(sb.toString());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("CLEAN ");
                sb2.append(r2.A03);
                StringBuilder sb3 = new StringBuilder();
                long[] jArr = r2.A04;
                for (long j : jArr) {
                    sb3.append(' ');
                    sb3.append(j);
                }
                sb2.append(sb3.toString());
                sb2.append('\n');
                bufferedWriter.write(sb2.toString());
            }
        }
        bufferedWriter.close();
        File file2 = this.A08;
        file.renameTo(file2);
        this.A03 = new BufferedWriter(new FileWriter(file2, true), DefaultCrypto.BUFFER_SIZE);
    }

    public synchronized void A0B(String str) {
        if (this.A03 != null) {
            A06(str);
            LinkedHashMap linkedHashMap = this.A0A;
            AnonymousClass4XO r7 = (AnonymousClass4XO) linkedHashMap.get(str);
            if (r7 != null && r7.A01 == null) {
                for (int i = 0; i < this.A05; i++) {
                    File A00 = r7.A00(i);
                    if (A00.delete()) {
                        long j = this.A02;
                        long[] jArr = r7.A04;
                        this.A02 = j - jArr[i];
                        jArr[i] = 0;
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("failed to delete ");
                        sb.append(A00);
                        throw new IOException(sb.toString());
                    }
                }
                this.A00++;
                this.A03.append((CharSequence) "REMOVE").append(' ').append((CharSequence) str).append('\n');
                linkedHashMap.remove(str);
                if (A0C()) {
                    this.A0C.submit(this.A0B);
                }
            }
        } else {
            throw new IllegalStateException("cache is closed");
        }
    }

    public final boolean A0C() {
        int i = this.A00;
        return i >= 2000 && i >= this.A0A.size();
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (this.A03 != null) {
            Iterator it = new ArrayList(this.A0A.values()).iterator();
            while (it.hasNext()) {
                C39061pE r2 = ((AnonymousClass4XO) it.next()).A01;
                if (r2 != null) {
                    A02(r2, r2.A02, false);
                }
            }
            A09();
            this.A03.close();
            this.A03 = null;
        }
    }
}
