package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3sO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80523sO extends C98344iX implements IInterface {
    public C80523sO(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.wearable.internal.IWearableService");
    }
}
