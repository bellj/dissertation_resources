package X;

/* renamed from: X.0xO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21420xO {
    public final C230910i A00;
    public final C21910yB A01;
    public final AnonymousClass1YD A02;

    public C21420xO(C230910i r3, C21910yB r4) {
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = new AnonymousClass1YD(r4, 1);
    }

    public void A00(String str) {
        AnonymousClass009.A00();
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A0C("DELETE FROM history_sync_companion WHERE message_id=?", new String[]{str});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
