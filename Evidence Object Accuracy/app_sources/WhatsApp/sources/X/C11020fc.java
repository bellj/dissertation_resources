package X;

import android.graphics.Rect;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: X.0fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11020fc extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ ClassLoader $classLoader;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C11020fc(ClassLoader classLoader) {
        super(0);
        this.$classLoader = classLoader;
    }

    /* renamed from: A00 */
    public final Boolean AJ3() {
        AnonymousClass0UQ r4 = AnonymousClass0UQ.A00;
        Class<?> loadClass = this.$classLoader.loadClass("androidx.window.extensions.layout.FoldingFeature");
        boolean z = false;
        Method method = loadClass.getMethod("getBounds", new Class[0]);
        Method method2 = loadClass.getMethod("getType", new Class[0]);
        Method method3 = loadClass.getMethod("getState", new Class[0]);
        C16700pc.A0B(method);
        if ((r4.A08(method, C88204Er.A00(Rect.class))) && Modifier.isPublic(method.getModifiers())) {
            C16700pc.A0B(method2);
            Class cls = Integer.TYPE;
            if ((r4.A08(method2, C88204Er.A00(cls))) && Modifier.isPublic(method2.getModifiers())) {
                C16700pc.A0B(method3);
                if ((r4.A08(method3, C88204Er.A00(cls))) && Modifier.isPublic(method3.getModifiers())) {
                    z = true;
                }
            }
        }
        return Boolean.valueOf(z);
    }
}
