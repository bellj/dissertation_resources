package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99864kz implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        int readInt = parcel.readInt();
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        AbstractC28901Pl A01 = AbstractC28901Pl.A01(C17930rd.A00(readString2), readString, parcel.readString(), parcel.readString(), readInt);
        if (A01 instanceof C30881Ze) {
            ((C30881Ze) A01).A01 = parcel.readInt();
        }
        C30821Yy A00 = C30821Yy.A00(parcel.readString(), parcel.readInt());
        int readInt2 = parcel.readInt();
        if (A00 == null) {
            return null;
        }
        return new AnonymousClass20Z(A00, A01, readInt2);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass20Z[i];
    }
}
