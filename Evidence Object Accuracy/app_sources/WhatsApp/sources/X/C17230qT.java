package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0qT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17230qT {
    public final C14830m7 A00;
    public final C14850m9 A01;
    public final Map A02;
    public final Map A03;
    public final Map A04;
    public final Map A05;
    public final int[] A06 = {0, 1, 2, 3};

    public C17230qT(C14830m7 r2, C14850m9 r3) {
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = new HashMap();
        this.A05 = new HashMap();
        this.A04 = new HashMap();
        this.A02 = new HashMap();
    }

    public final synchronized AnonymousClass1V4 A00(int i, long j) {
        return (AnonymousClass1V4) A02(i).get(Long.valueOf(j));
    }

    public AnonymousClass1V4 A01(long j) {
        for (int i : this.A06) {
            AnonymousClass1V4 A00 = A00(i, j);
            if (A00 != null) {
                return A00;
            }
        }
        return null;
    }

    public Map A02(int i) {
        if (i == 0) {
            return this.A03;
        }
        if (i == 1) {
            return this.A05;
        }
        if (i == 2) {
            return this.A04;
        }
        if (i == 3) {
            return this.A02;
        }
        throw new RuntimeException("LoggableStanzaCache/getStanzaMap not expected stanza type");
    }

    public void A03(long j) {
        int i = 4;
        if (A00(1, j) == null) {
            if (A00(0, j) != null) {
                i = 6;
            } else if (A00(2, j) != null) {
                i = 5;
            } else if (A00(3, j) == null) {
                i = -1;
            }
        }
        AnonymousClass1V4 A01 = A01(j);
        if (i != -1 && A01 != null) {
            A01.A02(i);
        }
    }

    public synchronized void A04(AnonymousClass1V4 r4, long j) {
        if (this.A01.A07(296)) {
            Map A02 = A02(r4.A02);
            Long valueOf = Long.valueOf(j);
            if (A02.containsKey(valueOf)) {
                StringBuilder sb = new StringBuilder();
                sb.append("LoggableStanzaCache/skipped caching loggable stanza:");
                sb.append(r4);
                Log.w(sb.toString());
            } else {
                A02.put(valueOf, r4);
            }
        }
    }
}
