package X;

import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity;

/* renamed from: X.3e3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72123e3 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarProfilePhotoActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72123e3(AvatarProfilePhotoActivity avatarProfilePhotoActivity) {
        super(1);
        this.this$0 = avatarProfilePhotoActivity;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:34:0x0062 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:35:0x00b5 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r10v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r9v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r9v1, types: [X.1WF] */
    /* JADX WARN: Type inference failed for: r9v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r10v1, types: [X.1WF] */
    /* JADX WARN: Type inference failed for: r10v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // X.AnonymousClass1J7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object AJ4(java.lang.Object r15) {
        /*
            r14 = this;
            r6 = r15
            X.4Ds r6 = (X.AbstractC87964Ds) r6
            r12 = 0
            X.C16700pc.A0E(r6, r12)
            boolean r0 = r6 instanceof X.C58722rH
            if (r0 == 0) goto L_0x00c7
            com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity r0 = r14.this$0
            X.0pd r0 = r0.A0B
            java.lang.Object r2 = r0.getValue()
            com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel r2 = (com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel) r2
            X.2rH r6 = (X.C58722rH) r6
            X.C16700pc.A0E(r6, r12)
            java.lang.String r0 = "AvatarProfilePhotoViewModel/onBackgroundColorSelected(item="
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r6)
            r0 = 41
            r1.append(r0)
            X.C12960it.A1F(r1)
            X.016 r4 = r2.A00
            java.lang.Object r0 = r4.A01()
            X.1Hx r0 = (X.C27541Hx) r0
            if (r0 == 0) goto L_0x0060
            java.util.List r0 = r0.A02
            if (r0 == 0) goto L_0x0060
            java.util.ArrayList r10 = X.C16760pi.A0E(r0)
            java.util.Iterator r5 = r0.iterator()
        L_0x0041:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0062
            java.lang.Object r1 = r5.next()
            X.2rH r1 = (X.C58722rH) r1
            int r3 = r1.A00
            int r0 = r6.A00
            boolean r2 = X.C12960it.A1V(r3, r0)
            int r1 = r1.A01
            X.2rH r0 = new X.2rH
            r0.<init>(r3, r1, r2)
            r10.add(r0)
            goto L_0x0041
        L_0x0060:
            X.1WF r10 = X.AnonymousClass1WF.A00
        L_0x0062:
            java.lang.Object r0 = r4.A01()
            X.1Hx r0 = (X.C27541Hx) r0
            if (r0 == 0) goto L_0x00b3
            java.util.List r0 = r0.A03
            if (r0 == 0) goto L_0x00b3
            java.util.ArrayList r9 = X.C16760pi.A0E(r0)
            java.util.Iterator r5 = r0.iterator()
        L_0x0076:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00b5
            java.lang.Object r3 = r5.next()
            X.3y7 r3 = (X.AbstractC83923y7) r3
            boolean r0 = r3 instanceof X.C58712rG
            if (r0 == 0) goto L_0x0099
            X.2rG r3 = (X.C58712rG) r3
            int r0 = r6.A01
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            boolean r0 = r3.A01
            X.2rG r3 = new X.2rG
            r3.<init>(r1, r0)
        L_0x0095:
            r9.add(r3)
            goto L_0x0076
        L_0x0099:
            boolean r0 = r3 instanceof X.C58702rF
            if (r0 == 0) goto L_0x00ae
            X.2rF r3 = (X.C58702rF) r3
            int r2 = r6.A01
            android.graphics.Bitmap r1 = r3.A01
            boolean r0 = r3.A02
            X.C16700pc.A0E(r1, r12)
            X.2rF r3 = new X.2rF
            r3.<init>(r1, r2, r0)
            goto L_0x0095
        L_0x00ae:
            X.5Gx r0 = X.C12990iw.A0v()
            throw r0
        L_0x00b3:
            X.1WF r9 = X.AnonymousClass1WF.A00
        L_0x00b5:
            X.1Hx r8 = X.C16700pc.A04(r4)
            r7 = 0
            r11 = 49
            r13 = 0
            X.1Hx r0 = X.C27541Hx.A00(r6, r7, r8, r9, r10, r11, r12, r13)
            r4.A0B(r0)
            X.1WZ r0 = X.AnonymousClass1WZ.A00
            return r0
        L_0x00c7:
            java.lang.String r0 = "Adapter can only handle background colors."
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C72123e3.AJ4(java.lang.Object):java.lang.Object");
    }
}
