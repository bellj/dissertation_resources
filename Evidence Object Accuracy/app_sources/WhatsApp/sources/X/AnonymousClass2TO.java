package X;

/* renamed from: X.2TO  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass2TO {
    A0E(0),
    A0F(1),
    A03(100),
    A0B(101),
    A0D(102),
    A05(103),
    A06(104),
    A07(105),
    A0A(106),
    A08(107),
    A02(C43951xu.A03),
    A01(109),
    A04(110),
    A0C(111),
    A09(112);
    
    public final int value;

    AnonymousClass2TO(int i) {
        this.value = i;
    }
}
