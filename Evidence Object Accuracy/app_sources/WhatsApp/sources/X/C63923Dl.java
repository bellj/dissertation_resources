package X;

import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63923Dl {
    public final /* synthetic */ AnonymousClass4RP A00;
    public final /* synthetic */ AnonymousClass19T A01;

    public C63923Dl(AnonymousClass4RP r1, AnonymousClass19T r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(int i) {
        AnonymousClass4RP r4 = this.A00;
        AnonymousClass19T r3 = r4.A00;
        r3.A04.A0A(Boolean.FALSE);
        if (i == 404) {
            r3.A0C.A0F(r4.A01, true);
        }
        r3.A02.A0A(new C84603za(r4.A01, r4.A02, i));
    }

    public void A01(C90084Mn r15, C91864Tn r16) {
        String str;
        AnonymousClass4RP r8 = this.A00;
        AnonymousClass19T r9 = r8.A00;
        r9.A04.A0A(Boolean.FALSE);
        String str2 = r16.A04;
        if (str2 == null || str2.equals(r8.A03)) {
            boolean z = true;
            boolean A1W = C12960it.A1W(str2);
            C19850um r4 = r9.A0C;
            UserJid userJid = r8.A01;
            synchronized (r4) {
                C44671zM r11 = r15.A01;
                List<C44691zO> list = r11.A04;
                for (C44691zO r7 : list) {
                    Map map = r4.A02;
                    String str3 = r7.A0D;
                    map.put(new C44701zP(userJid, str3), r7);
                    r4.A03.put(str3, userJid);
                }
                str = r11.A03;
                C44681zN r72 = (C44681zN) r4.A01(userJid).A04.get(str);
                if (r72 != null) {
                    if (!A1W) {
                        r72.A01.A04.clear();
                    }
                    C44671zM r1 = r72.A01;
                    r1.A02 = r11.A02;
                    r1.A01 = r11.A01;
                    r1.A00 = r11.A00;
                    r1.A04.addAll(list);
                } else {
                    C44661zL A01 = r4.A01(userJid);
                    r72 = new C44681zN(r11);
                    A01.A04.put(str, r72);
                }
                r72.A00 = r15.A00;
            }
            AnonymousClass016 r12 = r9.A02;
            if (r8.A03 != null) {
                z = false;
            }
            r12.A0A(new C84613zb(userJid, str, false, z));
        }
    }
}
