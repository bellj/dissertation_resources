package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.14o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241914o {
    public final C16490p7 A00;

    public C241914o(C16490p7 r1) {
        this.A00 = r1;
    }

    public void A00(byte[] bArr, long j) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("message_secret", bArr);
            A02.A03.A06(contentValues, "message_secret", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public byte[] A01(long j) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_secret FROM message_secret WHERE message_row_id = ?", new String[]{Long.toString(j)});
            byte[] blob = A09.moveToLast() ? A09.getBlob(A09.getColumnIndexOrThrow("message_secret")) : null;
            A09.close();
            A01.close();
            return blob;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
