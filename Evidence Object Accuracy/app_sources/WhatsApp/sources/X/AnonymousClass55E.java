package X;

import com.whatsapp.WaImageButton;
import com.whatsapp.voicerecorder.VoiceNoteSeekBar;

/* renamed from: X.55E  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass55E implements AnonymousClass2MF {
    public final WaImageButton A00;
    public final C30421Xi A01;
    public final VoiceNoteSeekBar A02;

    @Override // X.AnonymousClass2MF
    public void APa(boolean z) {
    }

    public AnonymousClass55E(WaImageButton waImageButton, C30421Xi r2, VoiceNoteSeekBar voiceNoteSeekBar) {
        this.A01 = r2;
        this.A00 = waImageButton;
        this.A02 = voiceNoteSeekBar;
    }

    @Override // X.AnonymousClass2MF
    public C30421Xi ACr() {
        return this.A01;
    }

    @Override // X.AnonymousClass2MF
    public void ATS(int i) {
        C40691s6.A06(this.A00);
    }

    @Override // X.AnonymousClass2MF
    public void AUL(int i) {
        this.A02.setProgress(i);
    }

    @Override // X.AnonymousClass2MF
    public void AVR() {
        C40691s6.A05(this.A00);
    }

    @Override // X.AnonymousClass2MF
    public void AWG(int i) {
        C40691s6.A05(this.A00);
        this.A02.setMax(i);
    }

    @Override // X.AnonymousClass2MF
    public void AWo(int i, boolean z) {
        C40691s6.A06(this.A00);
        if (z) {
            this.A02.setProgress(0);
        }
    }
}
