package X;

/* renamed from: X.1sV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40891sV {
    public final AnonymousClass1HL A00 = new AnonymousClass1HL();
    public final AnonymousClass1HL A01 = new AnonymousClass1HL();
    public volatile boolean A02 = false;

    public static final boolean A00(AnonymousClass1HL r3, boolean z) {
        boolean z2;
        synchronized (r3) {
            z2 = false;
            if (r3.A00 > 3) {
                z2 = true;
            }
        }
        if (z2) {
            return false;
        }
        if (z) {
            r3.A01();
            return true;
        }
        r3.A02();
        return true;
    }

    public final boolean A01() {
        boolean z;
        boolean z2;
        AnonymousClass1HL r2 = this.A01;
        synchronized (r2) {
            z = false;
            if (r2.A00 == 5) {
                z = true;
            }
        }
        if (z) {
            AnonymousClass1HL r22 = this.A00;
            synchronized (r22) {
                z2 = false;
                if (r22.A00 == 5) {
                    z2 = true;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final boolean A02() {
        boolean z;
        boolean z2;
        AnonymousClass1HL r2 = this.A01;
        synchronized (r2) {
            z = false;
            if (r2.A00 == 4) {
                z = true;
            }
        }
        if (!z) {
            AnonymousClass1HL r22 = this.A00;
            synchronized (r22) {
                z2 = false;
                if (r22.A00 == 4) {
                    z2 = true;
                }
            }
            if (!z2 && !A01()) {
                return false;
            }
        }
        return true;
    }
}
