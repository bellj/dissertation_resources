package X;

/* renamed from: X.45A  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45A extends C14510lY {
    public final String A00;

    @Override // X.C14510lY
    public int A00() {
        return 1;
    }

    public AnonymousClass45A(AnonymousClass1KC r1, C14380lL r2, String str) {
        super(r1, r2);
        this.A00 = str;
    }
}
