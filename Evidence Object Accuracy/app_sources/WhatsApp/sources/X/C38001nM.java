package X;

import java.io.File;
import java.io.FileInputStream;

/* renamed from: X.1nM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38001nM extends FileInputStream {
    public long A00;
    public final /* synthetic */ C37991nL A01;
    public final /* synthetic */ AnonymousClass153 A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C38001nM(C37991nL r1, AnonymousClass153 r2, File file) {
        super(file);
        this.A02 = r2;
        this.A01 = r1;
    }

    @Override // java.io.FileInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        while (getChannel().size() < this.A00 + ((long) i2) && (!this.A01.A01)) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException unused) {
                return 0;
            }
        }
        int read = super.read(bArr, i, i2);
        if (read >= 0) {
            this.A00 += (long) read;
        }
        return read;
    }
}
