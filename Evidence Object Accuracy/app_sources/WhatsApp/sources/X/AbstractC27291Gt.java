package X;

import android.content.res.Resources;
import android.icu.text.DecimalFormatSymbols;
import android.os.Build;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.1Gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC27291Gt {
    public static HashSet A00;
    public static final Object A01 = new Object();
    public static final Pattern A02 = Pattern.compile("[a-z]{2,3}");
    public static final Pattern A03 = Pattern.compile("[A-Z]{2}|[0-9]{3}");
    public static final String[] A04 = A0D('0');

    public static int A00(Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (!country.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(language);
            sb.append("-");
            sb.append(country);
            language = sb.toString();
        }
        AnonymousClass00O r1 = AnonymousClass2H6.A00;
        Number number = (Number) r1.get(language);
        if (number == null && (number = (Number) r1.get(country)) == null) {
            return 1;
        }
        return number.intValue();
    }

    public static String A01(Locale locale) {
        String language = locale.getLanguage();
        if ("zh".equals(language)) {
            return "Hans".equals(A02(locale)) ? "zh-Hans" : "zh-Hant";
        }
        return language;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0059, code lost:
        if (r0 == false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(java.util.Locale r7) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0011
            java.lang.String r1 = r7.getScript()
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0011
            return r1
        L_0x0011:
            java.lang.String r1 = r7.getCountry()
            java.lang.String r2 = r7.getLanguage()
            int r0 = r2.hashCode()
            switch(r0) {
                case 3129: goto L_0x0052;
                case 3569: goto L_0x004a;
                case 3679: goto L_0x0041;
                case 3749: goto L_0x0038;
                case 3886: goto L_0x002f;
                default: goto L_0x0020;
            }
        L_0x0020:
            r6 = -1
        L_0x0021:
            java.lang.String r5 = "Cyrl"
            java.lang.String r4 = "CN"
            java.lang.String r3 = "Latn"
            java.lang.String r2 = "Arab"
            switch(r6) {
                case 0: goto L_0x0086;
                case 1: goto L_0x00a7;
                case 2: goto L_0x00b2;
                case 3: goto L_0x0076;
                case 4: goto L_0x005c;
                default: goto L_0x002c;
            }
        L_0x002c:
            java.lang.String r0 = ""
            return r0
        L_0x002f:
            java.lang.String r0 = "zh"
            boolean r0 = r2.equals(r0)
            r6 = 4
            goto L_0x0059
        L_0x0038:
            java.lang.String r0 = "uz"
            boolean r0 = r2.equals(r0)
            r6 = 3
            goto L_0x0059
        L_0x0041:
            java.lang.String r0 = "sr"
            boolean r0 = r2.equals(r0)
            r6 = 2
            goto L_0x0059
        L_0x004a:
            java.lang.String r0 = "pa"
            boolean r0 = r2.equals(r0)
            r6 = 1
            goto L_0x0059
        L_0x0052:
            java.lang.String r0 = "az"
            boolean r0 = r2.equals(r0)
            r6 = 0
        L_0x0059:
            if (r0 != 0) goto L_0x0021
            goto L_0x0020
        L_0x005c:
            boolean r0 = r4.equals(r1)
            if (r0 != 0) goto L_0x0073
            java.lang.String r0 = "SG"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0073
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0073
            java.lang.String r0 = "Hant"
            return r0
        L_0x0073:
            java.lang.String r0 = "Hans"
            return r0
        L_0x0076:
            java.lang.String r0 = "AF"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0084
            boolean r0 = r4.equals(r1)
            if (r0 == 0) goto L_0x0085
        L_0x0084:
            return r2
        L_0x0085:
            return r3
        L_0x0086:
            java.lang.String r1 = r7.getCountry()
            int r0 = r1.hashCode()
            switch(r0) {
                case 2344: goto L_0x009e;
                case 2345: goto L_0x009b;
                case 2627: goto L_0x0092;
                default: goto L_0x0091;
            }
        L_0x0091:
            return r3
        L_0x0092:
            java.lang.String r0 = "RU"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00bb
            return r3
        L_0x009b:
            java.lang.String r0 = "IR"
            goto L_0x00a0
        L_0x009e:
            java.lang.String r0 = "IQ"
        L_0x00a0:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00b1
            return r3
        L_0x00a7:
            java.lang.String r0 = "PK"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x00b1
            java.lang.String r2 = "Guru"
        L_0x00b1:
            return r2
        L_0x00b2:
            java.lang.String r0 = "ME"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00bb
            return r3
        L_0x00bb:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC27291Gt.A02(java.util.Locale):java.lang.String");
    }

    public static String A03(Locale locale) {
        char c;
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            DecimalFormatSymbols instance = DecimalFormatSymbols.getInstance(locale);
            if (i >= 28) {
                return instance.getDigitStrings()[0];
            }
            c = instance.getZeroDigit();
        } else {
            c = java.text.DecimalFormatSymbols.getInstance(locale).getZeroDigit();
        }
        return Character.toString(c);
    }

    public static String A04(Locale locale) {
        String language = locale.getLanguage();
        switch (language.hashCode()) {
            case 3365:
                if (language.equals("in")) {
                    return "id";
                }
                break;
            case 3374:
                if (language.equals("iw")) {
                    return "he";
                }
                break;
            case 3588:
                if (language.equals("pt")) {
                    if (AnonymousClass1Th.A00.contains(locale.getCountry())) {
                        return "pt-PT";
                    }
                    return "pt-BR";
                }
                break;
            case 3704:
                if (language.equals("tl")) {
                    return "fil";
                }
                break;
            case 3886:
                if (language.equals("zh")) {
                    if ("Hans".equals(A02(locale))) {
                        return "zh-Hans";
                    }
                    return "zh-Hant";
                }
                break;
        }
        return language;
    }

    public static String A05(Locale locale) {
        if (Build.VERSION.SDK_INT >= 21) {
            return locale.toLanguageTag();
        }
        return AnonymousClass06C.A02(locale).A00.Aex();
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0012 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A06(java.util.Locale r5) {
        /*
            java.lang.String r1 = r5.getLanguage()
            int r0 = r1.hashCode()
            r4 = -1
            switch(r0) {
                case 3129: goto L_0x0081;
                case 3325: goto L_0x0077;
                case 3355: goto L_0x006d;
                case 3569: goto L_0x0063;
                case 3672: goto L_0x0058;
                case 3679: goto L_0x004d;
                case 3749: goto L_0x0042;
                case 3886: goto L_0x0037;
                case 101385: goto L_0x002c;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.String r3 = "Latn"
            r2 = 0
            switch(r4) {
                case 0: goto L_0x0013;
                case 1: goto L_0x0095;
                case 2: goto L_0x0092;
                case 3: goto L_0x001f;
                case 4: goto L_0x008f;
                case 5: goto L_0x0018;
                case 6: goto L_0x0013;
                case 7: goto L_0x0098;
                case 8: goto L_0x008b;
                default: goto L_0x0012;
            }
        L_0x0012:
            return r1
        L_0x0013:
            java.lang.String r0 = A02(r5)
            goto L_0x0025
        L_0x0018:
            java.lang.String r0 = A02(r5)
            java.lang.String r3 = "Cyrl"
            goto L_0x0025
        L_0x001f:
            java.lang.String r0 = A02(r5)
            java.lang.String r3 = "Guru"
        L_0x0025:
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x0012
            return r2
        L_0x002c:
            java.lang.String r0 = "fil"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 8
            goto L_0x000c
        L_0x0037:
            java.lang.String r0 = "zh"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 7
            goto L_0x000c
        L_0x0042:
            java.lang.String r0 = "uz"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 6
            goto L_0x000c
        L_0x004d:
            java.lang.String r0 = "sr"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 5
            goto L_0x000c
        L_0x0058:
            java.lang.String r0 = "sk"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 4
            goto L_0x000c
        L_0x0063:
            java.lang.String r0 = "pa"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 3
            goto L_0x000c
        L_0x006d:
            java.lang.String r0 = "id"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 2
            goto L_0x000c
        L_0x0077:
            java.lang.String r0 = "he"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 1
            goto L_0x000c
        L_0x0081:
            java.lang.String r0 = "az"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x000c
            r4 = 0
            goto L_0x000c
        L_0x008b:
            java.lang.String r0 = "tl"
            return r0
        L_0x008f:
            java.lang.String r0 = "cs"
            return r0
        L_0x0092:
            java.lang.String r0 = "in"
            return r0
        L_0x0095:
            java.lang.String r0 = "iw"
            return r0
        L_0x0098:
            java.lang.String r1 = r5.getCountry()
            java.lang.String r0 = "HK"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a8
            java.lang.String r0 = "zh-rHK"
            return r0
        L_0x00a8:
            java.lang.String r1 = A02(r5)
            java.lang.String r0 = "Hans"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00b8
            java.lang.String r0 = "zh-rCN"
            return r0
        L_0x00b8:
            java.lang.String r0 = "zh-rTW"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC27291Gt.A06(java.util.Locale):java.lang.String");
    }

    public static String A07(Locale locale, String str) {
        String[] A0D;
        String obj;
        String str2;
        char charAt;
        String[] strArr = (String[]) AbstractC41281tH.A02.get(locale.getLanguage());
        if (strArr == null) {
            return str;
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            DecimalFormatSymbols instance = DecimalFormatSymbols.getInstance(locale);
            if (i >= 28) {
                A0D = instance.getDigitStrings();
            } else {
                char[] digits = instance.getDigits();
                A0D = new String[10];
                int i2 = 0;
                do {
                    A0D[i2] = Character.toString(digits[i2]);
                    i2++;
                } while (i2 < 10);
            }
        } else {
            A0D = A0D(java.text.DecimalFormatSymbols.getInstance(locale).getZeroDigit());
        }
        if (Arrays.equals(strArr, A0D)) {
            obj = "[0-9]";
        } else if (Arrays.equals(A04, A0D)) {
            StringBuilder sb = new StringBuilder(19);
            sb.append(strArr[0]);
            int i3 = 1;
            do {
                sb.append('|');
                sb.append(strArr[i3]);
                i3++;
            } while (i3 < 10);
            obj = sb.toString();
        } else {
            StringBuilder sb2 = new StringBuilder("[0-9]|");
            StringBuilder sb3 = new StringBuilder(19);
            sb3.append(strArr[0]);
            int i4 = 1;
            do {
                sb3.append('|');
                sb3.append(strArr[i4]);
                i4++;
            } while (i4 < 10);
            sb2.append(sb3.toString());
            obj = sb2.toString();
        }
        Matcher matcher = Pattern.compile(obj).matcher(str);
        if (!matcher.find()) {
            return str;
        }
        int length = str.length();
        StringBuilder sb4 = new StringBuilder(length);
        int i5 = 0;
        do {
            int start = matcher.start();
            if (i5 < start) {
                sb4.append(str.substring(i5, start));
            }
            String group = matcher.group();
            if (group.length() != 1 || '0' > (charAt = group.charAt(0)) || charAt > '9') {
                str2 = "";
            } else {
                str2 = A0D[charAt - '0'];
            }
            if (str2.isEmpty()) {
                int i6 = 0;
                while (true) {
                    if (!group.equals(strArr[i6])) {
                        i6++;
                        if (i6 >= 10) {
                            break;
                        }
                    } else {
                        str2 = A0D[i6];
                        break;
                    }
                }
            }
            if (!str2.isEmpty()) {
                group = str2;
            }
            sb4.append(group);
            i5 = matcher.end();
        } while (matcher.find());
        if (i5 < length) {
            sb4.append(str.substring(i5));
        }
        return sb4.toString();
    }

    public static String A08(Locale[] localeArr) {
        int length = localeArr.length;
        if (length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(length * 6);
        sb.append(A05(localeArr[0]));
        for (int i = 1; i < length; i++) {
            sb.append(',');
            sb.append(A05(localeArr[i]));
        }
        return sb.toString();
    }

    public static Locale A09(String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            return Locale.forLanguageTag(str);
        }
        return AnonymousClass06C.A01(str).A00.AAS(0);
    }

    public static boolean A0A(String str) {
        return str != null && A02.matcher(str).matches();
    }

    public static boolean A0B(String str) {
        return str != null && A03.matcher(str).matches();
    }

    public static boolean A0C(String str) {
        boolean z;
        Object obj = A01;
        synchronized (obj) {
            if (A00 == null) {
                String[] locales = Resources.getSystem().getAssets().getLocales();
                if (Build.VERSION.SDK_INT >= 21) {
                    A00 = new HashSet(Arrays.asList(locales));
                } else {
                    int length = locales.length;
                    A00 = new HashSet(length);
                    for (String str2 : locales) {
                        A00.add(str2.replace('_', '-'));
                    }
                }
            }
        }
        synchronized (obj) {
            if (A00.contains(str)) {
                z = true;
            } else {
                z = A00.contains(A09(str).getLanguage());
            }
        }
        return z;
    }

    public static String[] A0D(char c) {
        String[] strArr = new String[10];
        int i = 0;
        do {
            strArr[i] = Character.toString((char) (c + i));
            i++;
        } while (i < 10);
        return strArr;
    }
}
