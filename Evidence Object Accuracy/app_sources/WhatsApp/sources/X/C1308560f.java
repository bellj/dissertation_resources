package X;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.60f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308560f {
    public static final UUID A07 = UUID.randomUUID();
    public Handler A00;
    public UUID A01;
    public final Handler A02;
    public final Handler A03;
    public final HandlerThread A04;
    public final HandlerThread A05;
    public final boolean A06 = true;

    public C1308560f() {
        HandlerThread handlerThread = new HandlerThread("Optic-Task-Handler-Thread");
        this.A05 = handlerThread;
        handlerThread.start();
        this.A03 = new Handler(handlerThread.getLooper());
        HandlerThread handlerThread2 = new HandlerThread("Optic-Camera-Handler-Thread");
        this.A04 = handlerThread2;
        handlerThread2.start();
        this.A02 = new Handler(handlerThread2.getLooper());
    }

    public synchronized AnonymousClass6LA A00(AbstractC129405xf r7, String str, Callable callable) {
        AnonymousClass6LA r4;
        r4 = new AnonymousClass6LA(this, str, this.A01, callable);
        if (r7 != null) {
            r4.A00(r7);
        }
        this.A03.postAtTime(r4, this.A01, SystemClock.uptimeMillis());
        return r4;
    }

    public synchronized AnonymousClass6LA A01(AbstractC129405xf r7, Callable callable) {
        AnonymousClass6LA r3;
        UUID uuid = A07;
        r3 = new AnonymousClass6LA(this, "load_camera_infos", uuid, callable);
        r3.A00(r7);
        this.A03.postAtTime(r3, uuid, SystemClock.uptimeMillis());
        return r3;
    }

    public synchronized AnonymousClass6LA A02(String str, Callable callable, long j) {
        AnonymousClass6LA r4;
        r4 = new AnonymousClass6LA(this, str, this.A01, callable);
        this.A03.postAtTime(r4, this.A01, SystemClock.uptimeMillis() + j);
        return r4;
    }

    public Object A03(String str, Callable callable) {
        AnonymousClass6LA r1;
        synchronized (this) {
            r1 = new AnonymousClass6LA(this, str, this.A01, callable);
            this.A02.post(r1);
        }
        return r1.get();
    }

    public Object A04(String str, Callable callable) {
        AnonymousClass6LA r1;
        synchronized (this) {
            r1 = new AnonymousClass6LA(this, str, this.A01, callable);
            this.A02.post(r1);
        }
        AnonymousClass6MJ r0 = (AnonymousClass6MJ) r1.get();
        r0.A6e();
        return r0.AGH();
    }

    public synchronized void A05(Runnable runnable, UUID uuid) {
        UUID uuid2 = this.A01;
        if ((uuid2 != null && uuid2.equals(uuid)) || A07.equals(uuid)) {
            Handler handler = this.A00;
            if (handler != null) {
                handler.postAtTime(runnable, uuid, SystemClock.uptimeMillis());
            } else {
                long uptimeMillis = SystemClock.uptimeMillis();
                synchronized (AnonymousClass61K.class) {
                    AnonymousClass61K.A00.postAtTime(runnable, uuid, uptimeMillis);
                }
            }
        }
    }

    public void A06(String str) {
        if (!A09()) {
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(" Current thread: ");
            throw C12990iw.A0m(C12960it.A0d(Thread.currentThread().getName(), A0j));
        }
    }

    public synchronized void A07(String str, Callable callable) {
        A00(null, str, callable);
    }

    public synchronized void A08(FutureTask futureTask) {
        this.A03.removeCallbacks(futureTask);
    }

    public boolean A09() {
        return C12970iu.A1Z(this.A03.getLooper().getThread(), Thread.currentThread());
    }

    public void finalize() {
        super.finalize();
        HandlerThread handlerThread = this.A04;
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            handlerThread.quitSafely();
        } else {
            handlerThread.quit();
        }
        try {
            handlerThread.join();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
        HandlerThread handlerThread2 = this.A05;
        if (i >= 18) {
            handlerThread2.quitSafely();
        } else {
            handlerThread2.quit();
        }
        try {
            handlerThread2.join();
        } catch (InterruptedException unused2) {
            Thread.currentThread().interrupt();
        }
    }
}
