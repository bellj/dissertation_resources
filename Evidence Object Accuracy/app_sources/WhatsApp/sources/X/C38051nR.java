package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1nR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38051nR {
    public final C20370ve A00;
    public final C241414j A01;
    public final AbstractC248317b A02;
    public final AbstractC14440lR A03;

    public C38051nR(C20370ve r1, C241414j r2, AbstractC248317b r3, AbstractC14440lR r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public C14580lf A00() {
        C14580lf r3 = new C14580lf();
        this.A03.Ab2(new RunnableBRunnable0Shape4S0200000_I0_4(this, 21, r3));
        return r3;
    }

    public C14580lf A01(String str) {
        C14580lf r3 = new C14580lf();
        this.A03.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, r3, str, 18));
        return r3;
    }

    public C14580lf A02(String str) {
        C14580lf r3 = new C14580lf();
        C14580lf A01 = A01(str);
        A01.A00(new AbstractC14590lg(r3, this, str) { // from class: X.3bf
            public final /* synthetic */ C14580lf A00;
            public final /* synthetic */ C38051nR A01;
            public final /* synthetic */ String A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C38051nR r0 = this.A01;
                String str2 = this.A02;
                C14580lf r1 = this.A00;
                if (r0.A01.A0L(str2)) {
                    r1.A02(obj);
                } else {
                    r1.A03(C12990iw.A0m(C12960it.A0d(str2, C12960it.A0k("Payments: failed to remove payment method by credId: "))));
                }
            }
        });
        A01.A00.A03(new AbstractC14590lg(str) { // from class: X.5AM
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C14580lf.this.A03(C12990iw.A0m(C12960it.A0d(this.A01, C12960it.A0k("Payments: failed to remove payment method by credId: "))));
            }
        }, null);
        return r3;
    }

    public void A03(AbstractC451720l r3, AbstractC28901Pl r4) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(r4);
        A04(r3, arrayList);
    }

    public void A04(AbstractC451720l r8, List list) {
        AbstractC14440lR r5 = this.A03;
        r5.Aaz(new C61302zo(r8, this.A01, this.A02, r5, list), new Void[0]);
    }

    public void A05(AbstractC451720l r8, List list) {
        AbstractC14440lR r5 = this.A03;
        r5.Aaz(new AnonymousClass42Y(r8, this.A01, this.A02, r5, list), new Void[0]);
    }

    public void A06(Runnable runnable, List list) {
        this.A03.Aaz(new AnonymousClass42Z(this, runnable, list), new Void[0]);
    }
}
