package X;

import java.util.concurrent.Callable;

/* renamed from: X.6Kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135856Kb implements Callable {
    public final /* synthetic */ AnonymousClass61Q A00;
    public final /* synthetic */ AnonymousClass66M A01;

    public CallableC135856Kb(AnonymousClass61Q r1, AnonymousClass66M r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass66M r1 = this.A01;
        r1.A00.A01();
        return r1;
    }
}
