package X;

import libcore.io.Memory;
import sun.misc.Unsafe;

/* renamed from: X.3qY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79413qY extends AnonymousClass4YX {
    public C79413qY(Unsafe unsafe) {
        super(unsafe);
    }

    @Override // X.AnonymousClass4YX
    public final void A07(long j, byte b) {
        Memory.pokeByte((int) (j & -1), b);
    }

    @Override // X.AnonymousClass4YX
    public final void A0E(byte[] bArr, long j, long j2, long j3) {
        Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
    }
}
