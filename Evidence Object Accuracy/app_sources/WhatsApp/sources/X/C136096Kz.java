package X;

/* renamed from: X.6Kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C136096Kz extends RuntimeException {
    public final int errorCode;

    public C136096Kz(int i, String str) {
        super(str);
        this.errorCode = i;
    }
}
