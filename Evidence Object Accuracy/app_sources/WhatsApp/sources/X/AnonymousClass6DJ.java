package X;

import com.whatsapp.R;
import java.util.concurrent.TimeUnit;

/* renamed from: X.6DJ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DJ implements AbstractC21730xt {
    public final /* synthetic */ C118115bI A00;

    public AnonymousClass6DJ(C118115bI r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A00.A00.A0A(AnonymousClass617.A02(new C127775v2(R.string.virality_payments_not_enabled_title, R.string.virality_payments_not_enabled_description_no_internet, R.string.cancel, 0), new C136096Kz(0, "No Internet!")));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        try {
            AnonymousClass1V8 A0E = r6.A0E("error");
            AnonymousClass009.A06(A0E, C12960it.A0d(" not found!", C12960it.A0j("error")));
            int A05 = A0E.A05("code", 500);
            this.A00.A04(new C136096Kz(A05, A0E.A0I("text", "Unknown!")), A05);
        } catch (AnonymousClass1V9 | NullPointerException e) {
            this.A00.A04(e, 500);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        try {
            AnonymousClass1V8 A0E = r9.A0E("account");
            AnonymousClass009.A06(A0E, C12960it.A0d(" not found!", C12960it.A0j("account")));
            AnonymousClass1V8 A0E2 = A0E.A0E("link");
            AnonymousClass009.A06(A0E2, C12960it.A0d(" not found!", C12960it.A0j("link")));
            int A05 = A0E2.A05("status", 0);
            int A052 = A0E2.A05("redirection_type", 0);
            C118115bI r7 = this.A00;
            if (A05 == 1) {
                r7.A03.A0D(r7.A02.A00() + TimeUnit.DAYS.toMillis(1));
                r7.A00.A0A(AnonymousClass617.A01(new C127775v2(R.string.virality_payments_enabled_title, R.string.virality_payments_enabled_description, R.string.done, A052)));
                return;
            }
            r7.A04(C12970iu.A0f(C12960it.A0W(A05, "Status is ")), 500);
        } catch (AnonymousClass1V9 | NullPointerException e) {
            this.A00.A04(e, 500);
        }
    }
}
