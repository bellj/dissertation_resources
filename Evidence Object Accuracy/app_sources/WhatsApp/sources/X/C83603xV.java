package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.ephemeral.ChangeEphemeralSettingActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3xV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83603xV extends AbstractC52172aN {
    public final /* synthetic */ ChangeEphemeralSettingActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83603xV(Context context, ChangeEphemeralSettingActivity changeEphemeralSettingActivity) {
        super(context);
        this.A00 = changeEphemeralSettingActivity;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ChangeEphemeralSettingActivity changeEphemeralSettingActivity = this.A00;
        int i = 4;
        if (changeEphemeralSettingActivity.A0E instanceof UserJid) {
            i = 3;
        }
        changeEphemeralSettingActivity.startActivity(C14960mK.A06(changeEphemeralSettingActivity, i));
    }
}
