package X;

import android.database.Cursor;

/* renamed from: X.0Zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07710Zx implements AbstractC12580i9 {
    public final AbstractC02880Ff A00;
    public final AnonymousClass0QN A01;
    public final AbstractC05330Pd A02;

    public C07710Zx(AnonymousClass0QN r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0FO(r2, this);
        this.A02 = new AnonymousClass0FU(r2, this);
    }

    @Override // X.AbstractC12580i9
    public AnonymousClass0PC AH2(String str) {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        AnonymousClass0PC r4 = null;
        Cursor A002 = AnonymousClass0LC.A00(r1, A00, false);
        try {
            int A003 = AnonymousClass0LB.A00(A002, "work_spec_id");
            int A004 = AnonymousClass0LB.A00(A002, "system_id");
            if (A002.moveToFirst()) {
                r4 = new AnonymousClass0PC(A002.getString(A003), A002.getInt(A004));
            }
            return r4;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12580i9
    public void AJ1(AnonymousClass0PC r3) {
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        r1.A03();
        try {
            this.A00.A04(r3);
            r1.A05();
        } finally {
            r1.A04();
        }
    }

    @Override // X.AbstractC12580i9
    public void AaR(String str) {
        AnonymousClass0QN r3 = this.A01;
        r3.A02();
        AbstractC05330Pd r2 = this.A02;
        AbstractC12830ic A00 = r2.A00();
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        r3.A03();
        try {
            ((C02980Fp) A00).A00.executeUpdateDelete();
            r3.A05();
        } finally {
            r3.A04();
            r2.A02(A00);
        }
    }
}
