package X;

import android.text.Editable;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment;

/* renamed from: X.5ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123805ny extends C469928m {
    public final /* synthetic */ IndiaUpiSendPaymentToVpaFragment A00;

    public C123805ny(IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment) {
        this.A00 = indiaUpiSendPaymentToVpaFragment;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment = this.A00;
        indiaUpiSendPaymentToVpaFragment.A02.setVisibility(4);
        indiaUpiSendPaymentToVpaFragment.A06.setEnabled(C12960it.A1U(editable.toString().length()));
        EditText editText = indiaUpiSendPaymentToVpaFragment.A00;
        AnonymousClass028.A0M(AnonymousClass00T.A03(editText.getContext(), R.color.primary), editText);
    }
}
