package X;

import android.widget.FrameLayout;

/* renamed from: X.0dQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09730dQ implements Runnable {
    public final /* synthetic */ FrameLayout A00;
    public final /* synthetic */ AnonymousClass0PL A01;

    public RunnableC09730dQ(FrameLayout frameLayout, AnonymousClass0PL r2) {
        this.A01 = r2;
        this.A00 = frameLayout;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.A01(this.A00);
    }
}
