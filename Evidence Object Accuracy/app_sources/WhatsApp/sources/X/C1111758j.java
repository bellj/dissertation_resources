package X;

import com.whatsapp.productinfra.avatar.ui.stickers.upsell.AvatarStickerUpsellView;

/* renamed from: X.58j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1111758j implements AbstractC17410ql {
    public final /* synthetic */ AvatarStickerUpsellView A00;

    @Override // X.AbstractC17410ql
    public void AMj() {
    }

    @Override // X.AbstractC17410ql
    public void AMk() {
    }

    public C1111758j(AvatarStickerUpsellView avatarStickerUpsellView) {
        this.A00 = avatarStickerUpsellView;
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        this.A00.getViewController().A00();
    }
}
