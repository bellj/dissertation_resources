package X;

import android.database.Cursor;
import android.util.Base64;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22210yi extends C22220yj {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C002701f A02;
    public final C15660nh A03;
    public final C38851oo A04;
    public final C22230yk A05;
    public final C28101Kq A06 = new C28101Kq();
    public final AnonymousClass146 A07;
    public final C235512c A08;
    public final ExecutorC27271Gr A09;
    public final boolean A0A;

    public C22210yi(C19450u8 r3, AnonymousClass18Z r4, C14330lG r5, C14900mE r6, C002701f r7, C15660nh r8, C22230yk r9, AnonymousClass146 r10, C235512c r11, AbstractC14440lR r12, boolean z) {
        super(new C38841on((C19450u8) r4.A00.A01.A7H.get(), z), 32);
        this.A01 = r6;
        this.A00 = r5;
        this.A0A = z;
        this.A05 = r9;
        this.A07 = r10;
        this.A08 = r11;
        this.A03 = r8;
        this.A04 = new C38851oo((C240413z) r3.A00.A01.AKZ.get(), z);
        this.A02 = r7;
        this.A09 = new ExecutorC27271Gr(r12, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0076, code lost:
        if (r3 == null) goto L_0x0061;
     */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.C22210yi r6, X.AnonymousClass1KS r7, boolean r8) {
        /*
        // Method dump skipped, instructions count: 301
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22210yi.A00(X.0yi, X.1KS, boolean):void");
    }

    @Override // X.C22220yj
    public void A08(int i) {
        AnonymousClass009.A00();
        C38861op r4 = (C38861op) A01(i);
        StringBuilder sb = new StringBuilder("RecentStickers/removeEntry/removing entry: ");
        sb.append(r4.toString());
        Log.i(sb.toString());
        C002701f r0 = this.A02;
        String str = r4.A01;
        r0.A09(str);
        C28101Kq r2 = this.A06;
        String str2 = r4.A02;
        synchronized (r2) {
            if (str2 != null) {
                r2.A01.remove(str2);
            }
            r2.A00.remove(str);
        }
        super.A08(i);
    }

    @Override // X.C22220yj
    public /* bridge */ /* synthetic */ void A09(AbstractC38871oq r4) {
        C38881or r42 = (C38881or) r4;
        AnonymousClass009.A00();
        StringBuilder sb = new StringBuilder("RecentStickers/addEntry/adding entry:");
        sb.append(r42.toString());
        Log.i(sb.toString());
        C28101Kq r2 = this.A06;
        C38861op r0 = r42.A01;
        r2.A01(r0.A01, r0.A02);
        super.A09(r42);
    }

    public List A0A() {
        List<C38861op> A02 = super.A02();
        ArrayList arrayList = new ArrayList();
        for (C38861op r3 : A02) {
            C28101Kq r1 = this.A06;
            String str = r3.A01;
            r1.A01(str, r3.A02);
            AnonymousClass1KS r32 = r3.A00;
            String str2 = r32.A0C;
            if (str2 == null) {
                r32.A0C = str;
                str2 = str;
            }
            r32.A0B = "image/webp";
            if (str2 == null) {
                Log.e("RecentStickers/setRecentStickerFilePath/sticker param has null file hash");
            } else {
                File A04 = this.A02.A04(str2);
                r32.A08 = A04.getAbsolutePath();
                r32.A01 = 1;
                if (A04.getAbsolutePath() != null) {
                    C37431mO.A00(r32);
                }
            }
            arrayList.add(r32.clone());
        }
        return arrayList;
    }

    public List A0B(boolean z) {
        List<C38861op> A02 = super.A02();
        for (C38861op r3 : A02) {
            if (z) {
                C38851oo r6 = this.A04;
                String str = r3.A01;
                boolean z2 = true;
                String[] strArr = {str};
                C16310on A01 = r6.A00.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT plaintext_hash, entry_weight, hash_of_image_part, url, enc_hash, direct_path, mimetype, media_key, file_size, width, height, emojis, is_first_party, is_avocado FROM recent_stickers WHERE plaintext_hash = ?", strArr);
                    if (A09.moveToNext()) {
                        AnonymousClass1KS r5 = new AnonymousClass1KS();
                        r5.A0C = str;
                        r5.A0F = A09.getString(A09.getColumnIndexOrThrow("url"));
                        r5.A07 = A09.getString(A09.getColumnIndexOrThrow("enc_hash"));
                        r5.A05 = A09.getString(A09.getColumnIndexOrThrow("direct_path"));
                        r5.A0B = A09.getString(A09.getColumnIndexOrThrow("mimetype"));
                        r5.A0A = A09.getString(A09.getColumnIndexOrThrow("media_key"));
                        r5.A00 = A09.getInt(A09.getColumnIndexOrThrow("file_size"));
                        r5.A03 = A09.getInt(A09.getColumnIndexOrThrow("width"));
                        r5.A02 = A09.getInt(A09.getColumnIndexOrThrow("height"));
                        r5.A06 = A09.getString(A09.getColumnIndexOrThrow("emojis"));
                        if (A09.getInt(A09.getColumnIndexOrThrow("is_first_party")) != 1) {
                            z2 = false;
                        }
                        r5.A0H = z2;
                        C37431mO.A00(r5);
                        A09.close();
                        A01.close();
                        if (r5.A05 == null) {
                            C29221Ri A08 = this.A03.A08(str, (byte) 20, true);
                            if (A08 != null) {
                                r5 = new AnonymousClass1KS();
                                r5.A0C = str;
                                r5.A0F = A08.A04;
                                r5.A07 = A08.A03;
                                C16150oX r4 = A08.A02;
                                r5.A05 = r4.A0G;
                                r5.A0B = "image/webp";
                                byte[] bArr = r4.A0U;
                                if (bArr != null) {
                                    r5.A0A = Base64.encodeToString(bArr, 3);
                                }
                                r5.A00 = (int) r4.A0A;
                                r5.A03 = r4.A08;
                                r5.A02 = r4.A06;
                                C37431mO.A00(r5);
                                r6.A00(r5);
                            }
                        }
                        r3.A00(r5);
                    } else {
                        A09.close();
                        A01.close();
                        StringBuilder sb = new StringBuilder("RecentStickerDBStorage/getStickerFromFileHash/sticker unable to be retrieved from recent stickers db: filehash = ");
                        sb.append(str);
                        Log.e(sb.toString());
                        Log.e("RecentStickers/getStickerListAndWeights/recent sticker not found in db");
                    }
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            AnonymousClass1KS r1 = r3.A00;
            if (r1.A0B == null) {
                r1.A0B = "image/webp";
            }
        }
        Map A04 = super.A04();
        ArrayList arrayList = new ArrayList();
        for (C38861op r42 : A02) {
            AnonymousClass1KS A012 = r42.A00.clone();
            String str2 = A012.A0C;
            if (str2 == null) {
                Log.e("RecentStickers/setRecentStickerFilePath/sticker param has null file hash");
            } else {
                File A042 = this.A02.A04(str2);
                A012.A08 = A042.getAbsolutePath();
                A012.A01 = 1;
                A042.getAbsolutePath();
            }
            arrayList.add(new Pair(A012, A04.get(r42)));
        }
        return arrayList;
    }

    public final void A0C(AnonymousClass1KS r4, boolean z) {
        if (r4.A0G == this.A0A) {
            this.A09.execute(new RunnableBRunnable0Shape0S0210000_I0(this, r4, 19, z));
        }
    }

    public void A0D(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, int i2, int i3, boolean z) {
        AnonymousClass1KS r2 = new AnonymousClass1KS();
        r2.A0C = str;
        if (str2 != null) {
            r2.A0F = str2;
        }
        if (str3 != null) {
            r2.A07 = str3;
        }
        if (str4 != null) {
            r2.A05 = str4;
        }
        r2.A0B = str5;
        if (str6 != null) {
            r2.A0A = str6;
        }
        r2.A00 = i;
        r2.A03 = i2;
        r2.A02 = i3;
        r2.A06 = str7;
        r2.A0H = z;
        this.A04.A00(r2);
        for (C38861op r1 : super.A02()) {
            if (str.equals(r1.A01)) {
                r1.A00(r2);
            }
        }
    }

    public boolean A0E(String str) {
        Map map;
        int size;
        boolean containsKey;
        C28101Kq r2 = this.A06;
        synchronized (r2) {
            map = r2.A00;
            size = map.size();
        }
        if (size > 0) {
            synchronized (r2) {
                containsKey = map.containsKey(str);
            }
            return containsKey;
        }
        for (AnonymousClass1KS r0 : A0A()) {
            if (str.equals(r0.A0C)) {
                return true;
            }
        }
        return false;
    }
}
