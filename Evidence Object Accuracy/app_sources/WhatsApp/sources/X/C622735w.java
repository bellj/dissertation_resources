package X;

import android.text.Editable;
import com.whatsapp.Conversation;

/* renamed from: X.35w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622735w extends C469928m {
    public final /* synthetic */ Conversation A00;

    public C622735w(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        Conversation conversation = this.A00;
        if (conversation.A4P) {
            return;
        }
        if (editable.toString().length() != 0) {
            ((AbstractActivityC13750kH) conversation).A03.A0B(C15370n3.A02(conversation.A2c), 0);
            return;
        }
        conversation.A4M = false;
        ((AbstractActivityC13750kH) conversation).A03.A09(C15370n3.A02(conversation.A2c));
    }
}
