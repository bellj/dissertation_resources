package X;

/* renamed from: X.3Bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63523Bx {
    public C90814Pi A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C30091Wb A03;
    public final C14950mJ A04;
    public final C22100yW A05;
    public final C14850m9 A06;
    public final C14840m8 A07;
    public final C22710zW A08;
    public final C25871Bd A09;
    public final C91684Sr A0A;
    public final C231910s A0B;
    public final C21440xQ A0C;
    public final C22990zy A0D;
    public final AbstractC14440lR A0E;

    public C63523Bx(C14900mE r2, C15570nT r3, C14950mJ r4, C22100yW r5, C14850m9 r6, C14840m8 r7, C22710zW r8, C25871Bd r9, C91684Sr r10, C231910s r11, C21440xQ r12, C22990zy r13, AbstractC14440lR r14, C232010t r15) {
        this.A06 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A0E = r14;
        this.A04 = r4;
        this.A07 = r7;
        this.A0A = r10;
        this.A0D = r13;
        this.A0B = r11;
        this.A08 = r8;
        this.A05 = r5;
        this.A09 = r9;
        this.A0C = r12;
        this.A03 = new C30091Wb(r15);
    }
}
