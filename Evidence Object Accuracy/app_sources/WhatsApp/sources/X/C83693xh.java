package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83693xh extends AnonymousClass2V9 {
    public TextView A00;

    public void A01(View.OnClickListener onClickListener, String str, String str2, int i) {
        super.A00(onClickListener, str, i);
        TextView A0I = C12960it.A0I(super.A00, R.id.share_link_action_item_description);
        this.A00 = A0I;
        A0I.setVisibility(0);
        TextView textView = this.A00;
        if (textView != null) {
            textView.setText(str2);
        }
    }
}
