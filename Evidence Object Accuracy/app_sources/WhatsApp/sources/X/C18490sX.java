package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.0sX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18490sX extends AbstractC18500sY {
    public final C15570nT A00;
    public final C18460sU A01;
    public final C18470sV A02;

    public C18490sX(C15570nT r3, C18460sU r4, C18470sV r5, C18480sW r6) {
        super(r6, "blank_me_jid", Integer.MIN_VALUE);
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
    }

    @Override // X.AbstractC18500sY
    public long A06() {
        return super.A06();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:52:0x00a7 */
    /* JADX DEBUG: Multi-variable search result rejected for r11v1, resolved type: android.content.ContentValues */
    /* JADX DEBUG: Multi-variable search result rejected for r14v3, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r0v30, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r14v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r14v7, types: [int] */
    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        long j;
        int i;
        Log.i("BlankMeJidDatabaseMigration/processBatch");
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("raw_string");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("type");
        long j2 = -1;
        int i2 = 0;
        while (cursor.moveToNext()) {
            j2 = cursor.getLong(columnIndexOrThrow);
            i2++;
            if (TextUtils.isEmpty(cursor.getString(columnIndexOrThrow2)) && cursor.getInt(columnIndexOrThrow3) == 11) {
                C18470sV r4 = this.A02;
                if (!r4.A0G()) {
                    Log.i("StatusStore/migrateStatusRowsForOldMeJid");
                    String valueOf = String.valueOf(j2);
                    C16310on A01 = r4.A01.A01.get();
                    try {
                        Cursor A09 = A01.A03.A09("SELECT _id FROM jid WHERE raw_string = ? AND type = ?", new String[]{C29831Uv.A00.getRawString(), String.valueOf(11)});
                        if (A09.moveToNext()) {
                            j = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                        } else {
                            j = -1;
                        }
                        A09.close();
                        A01.close();
                        String valueOf2 = String.valueOf(j);
                        C16310on A02 = r4.A02.A02();
                        try {
                            AnonymousClass1Lx A00 = A02.A00();
                            boolean z = true;
                            ContentValues contentValues = new ContentValues(1);
                            contentValues.put("jid_row_id", (String) valueOf2);
                            try {
                                valueOf2 = A02.A03.A00("status", contentValues, "jid_row_id=?", new String[]{valueOf});
                                i = valueOf2;
                            } catch (SQLiteConstraintException e) {
                                Log.e("StatusStore/migrateStatusRowsForOldMeJid/", e);
                                if (r4.A03(valueOf2) > r4.A03(valueOf)) {
                                    Log.i("StatusStore/migrateStatusRowsForOldMeJid/delete row with invalid me jid");
                                    i = A02.A03.A01("status", "jid_row_id=?", new String[]{valueOf});
                                } else {
                                    Log.i("StatusStore/migrateStatusRowsForOldMeJid/delete row with valid me jid");
                                    C16330op r3 = A02.A03;
                                    int A012 = r3.A01("status", "jid_row_id=?", new String[]{valueOf2});
                                    i = A012;
                                    if (A012 > 0) {
                                        Log.i("StatusStore/migrateStatusRowsForOldMeJid/update row with invalid me jid");
                                        i = r3.A00("status", contentValues, "jid_row_id=?", new String[]{valueOf});
                                    }
                                }
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.append("StatusStore/migrateStatusRowsForOldMeJid/success/");
                            if (i <= 0) {
                                z = false;
                            }
                            sb.append(z);
                            Log.i(sb.toString());
                            A00.A00();
                            A00.close();
                            A02.close();
                        } finally {
                        }
                    } finally {
                    }
                } else {
                    continue;
                }
            }
        }
        Log.i("BlankMeJidDatabaseMigration/processBatch/done");
        return new AnonymousClass2Ez(j2, i2);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("blank_me_jid_ready", 1);
    }

    @Override // X.AbstractC18500sY
    public void A0I() {
        Long l;
        C18460sU r6 = this.A01;
        C16310on A02 = r6.A01.A02();
        try {
            if (A02.A03.A01("jid", "raw_string = ? AND type = ?", new String[]{"", String.valueOf(11)}) > 0 && (l = (Long) r6.A03.remove(C29831Uv.A00)) != null) {
                r6.A04.remove(l);
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
