package X;

import android.view.View;
import com.whatsapp.mediacomposer.VideoComposerFragment;

/* renamed from: X.4mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnAttachStateChangeListenerC100944mj implements View.OnAttachStateChangeListener {
    public final /* synthetic */ VideoComposerFragment A00;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public View$OnAttachStateChangeListenerC100944mj(VideoComposerFragment videoComposerFragment) {
        this.A00 = videoComposerFragment;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        if (view != null) {
            VideoComposerFragment videoComposerFragment = this.A00;
            view.removeCallbacks(videoComposerFragment.A0U);
            view.removeOnAttachStateChangeListener(videoComposerFragment.A0T);
        }
    }
}
