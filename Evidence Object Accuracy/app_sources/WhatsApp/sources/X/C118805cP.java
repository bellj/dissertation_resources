package X;

import android.view.View;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.5cP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118805cP extends AnonymousClass03U {
    public final LinearLayout A00;
    public final RecyclerView A01;
    public final WaTextView A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C14830m7 A05;
    public final AnonymousClass018 A06;
    public final C17070qD A07;
    public final AnonymousClass14X A08;

    public C118805cP(View view, C15550nR r3, C15610nY r4, C14830m7 r5, AnonymousClass018 r6, C17070qD r7, AnonymousClass14X r8) {
        super(view);
        this.A05 = r5;
        this.A08 = r8;
        this.A06 = r6;
        this.A03 = r3;
        this.A04 = r4;
        this.A07 = r7;
        this.A00 = C117305Zk.A07(view, R.id.transaction_root_layout);
        this.A02 = C12960it.A0N(view, R.id.transaction_type_desc);
        this.A01 = (RecyclerView) AnonymousClass028.A0D(view, R.id.transaction_questions);
    }
}
