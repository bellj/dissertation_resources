package X;

import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.R;
import com.whatsapp.community.CommunitySubgroupsBottomSheet;

/* renamed from: X.2gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54372gc extends AnonymousClass02M {
    public final C15580nU A00;
    public final /* synthetic */ CommunitySubgroupsBottomSheet A01;

    public C54372gc(CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet, C15580nU r2) {
        this.A01 = communitySubgroupsBottomSheet;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet = this.A01;
        int i2 = ((AnonymousClass4NK) communitySubgroupsBottomSheet.A00.A02(i)).A00;
        if (i2 != 1) {
            return (long) i2;
        }
        return (long) ((AnonymousClass1OU) ((AnonymousClass4NK) communitySubgroupsBottomSheet.A00.A02(i)).A01).A02.hashCode();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.A00.A03;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC75673kE) r2).A08(((AnonymousClass4NK) this.A01.A00.A02(i)).A01, i);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C851741l(this.A01.A04().inflate(R.layout.community_home_new_group, viewGroup, false), new RunnableBRunnable0Shape15S0100000_I1_1(this, 14));
        }
        if (i == 1) {
            View inflate = C12960it.A0E(viewGroup).inflate(R.layout.conversations_row, viewGroup, false);
            CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet = this.A01;
            ActivityC000900k A0C = communitySubgroupsBottomSheet.A0C();
            C16590pI r0 = communitySubgroupsBottomSheet.A0H;
            C14830m7 r02 = communitySubgroupsBottomSheet.A0G;
            C14850m9 r03 = communitySubgroupsBottomSheet.A0R;
            AnonymousClass13H r04 = communitySubgroupsBottomSheet.A0T;
            C15570nT r05 = communitySubgroupsBottomSheet.A04;
            AbstractC14440lR r06 = communitySubgroupsBottomSheet.A0b;
            C19990v2 r07 = communitySubgroupsBottomSheet.A0K;
            C15450nH r08 = communitySubgroupsBottomSheet.A05;
            C63543Bz r11 = new C63543Bz(communitySubgroupsBottomSheet.A0C());
            AnonymousClass14X r09 = communitySubgroupsBottomSheet.A0W;
            AnonymousClass130 r010 = communitySubgroupsBottomSheet.A0B;
            C15550nR r011 = communitySubgroupsBottomSheet.A0C;
            C253519b r012 = communitySubgroupsBottomSheet.A01;
            C241714m r013 = communitySubgroupsBottomSheet.A0L;
            C15610nY r014 = communitySubgroupsBottomSheet.A0D;
            AnonymousClass018 r015 = communitySubgroupsBottomSheet.A0J;
            C17070qD r016 = communitySubgroupsBottomSheet.A0V;
            C238013b r017 = communitySubgroupsBottomSheet.A08;
            C20710wC r15 = communitySubgroupsBottomSheet.A0S;
            AnonymousClass10Y r14 = communitySubgroupsBottomSheet.A0P;
            AnonymousClass12F r13 = communitySubgroupsBottomSheet.A0a;
            C15860o1 r10 = communitySubgroupsBottomSheet.A0Y;
            C21400xM r9 = communitySubgroupsBottomSheet.A0Q;
            C14820m6 r8 = communitySubgroupsBottomSheet.A0I;
            C236812p r7 = communitySubgroupsBottomSheet.A0N;
            C22710zW r6 = communitySubgroupsBottomSheet.A0U;
            C15600nX r5 = communitySubgroupsBottomSheet.A0M;
            AnonymousClass11L r4 = communitySubgroupsBottomSheet.A06;
            AnonymousClass3J9 r49 = AnonymousClass3J9.A01;
            return new C60382ws(A0C, inflate, r012, r05, r08, r4, communitySubgroupsBottomSheet.A07, r017, r010, r011, r014, communitySubgroupsBottomSheet.A0E, new C63563Cb(new ExecutorC27271Gr(communitySubgroupsBottomSheet.A0b, true)), communitySubgroupsBottomSheet.A0A, r11, r02, r0, r8, r015, r07, r013, r5, r7, r14, r9, r03, r15, r04, r6, r016, r09, r10, r13, r49, r06);
        } else if (i == 2) {
            return new C60372wr(this.A01.A04().inflate(R.layout.view_community_action_rv_item, viewGroup, false), new RunnableBRunnable0Shape15S0100000_I1_1(this, 13));
        } else {
            StringBuilder A0k = C12960it.A0k("View type ");
            A0k.append(i);
            throw C12960it.A0U(C12960it.A0d(" not recognizable", A0k));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4NK) this.A01.A00.A02(i)).A00;
    }
}
