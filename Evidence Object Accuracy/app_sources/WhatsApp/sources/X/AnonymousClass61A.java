package X;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61A  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61A {
    public final C22680zT A00;
    public final AnonymousClass01d A01;
    public final C16590pI A02;
    public final C1329668y A03;
    public final AnonymousClass6BE A04;
    public final C30931Zj A05;

    public AnonymousClass61A(C16590pI r1, AnonymousClass01d r2, C30931Zj r3, C22680zT r4, AnonymousClass6BE r5, C1329668y r6) {
        this.A02 = r1;
        this.A01 = r2;
        this.A05 = r3;
        this.A00 = r4;
        this.A04 = r5;
        this.A03 = r6;
    }

    public static SmsManager A00(int i) {
        return SmsManager.getSmsManagerForSubscriptionId(i);
    }

    private String A01(SubscriptionInfo subscriptionInfo) {
        boolean isEmpty;
        String str;
        if (Build.VERSION.SDK_INT < 29) {
            return subscriptionInfo.getIccId();
        }
        C30931Zj r2 = this.A05;
        StringBuilder A0k = C12960it.A0k("Sub Id : ");
        A0k.append(subscriptionInfo.getSubscriptionId());
        C117295Zj.A1F(r2, A0k);
        StringBuilder A0h = C12960it.A0h();
        A0h.append(subscriptionInfo.getSubscriptionId());
        C1329668y r4 = this.A03;
        synchronized (r4) {
            isEmpty = true ^ TextUtils.isEmpty(r4.A0P("device_binding_sim_iccid")[0]);
        }
        if (isEmpty) {
            str = "";
        } else {
            str = Settings.Secure.getString(this.A02.A00.getContentResolver(), "android_id");
        }
        return C12960it.A0d(str, A0h);
    }

    public static List A02(Context context) {
        List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        ArrayList A0l = C12960it.A0l();
        if (activeSubscriptionInfoList != null && activeSubscriptionInfoList.size() == 2) {
            A0l.add(((SubscriptionInfo) C12980iv.A0o(activeSubscriptionInfoList)).getNumber());
            A0l.add(((SubscriptionInfo) C117315Zl.A0I(activeSubscriptionInfoList)).getNumber());
        }
        return A0l;
    }

    public int A03(C120475gF r20, String str) {
        List<SubscriptionInfo> activeSubscriptionInfoList;
        this.A05.A06("IndiaUpiSimSwapDetectionUtils : Check sim on version >= 22");
        SubscriptionManager A0M = this.A01.A0M();
        if (A0M == null || (activeSubscriptionInfoList = A0M.getActiveSubscriptionInfoList()) == null || activeSubscriptionInfoList.isEmpty()) {
            this.A05.A06("IndiaUpiSimSwapDetectionUtils : No subscription info found");
            return 1;
        }
        String A09 = this.A03.A09();
        JSONObject A0a = C117295Zj.A0a();
        JSONObject A0a2 = C117295Zj.A0a();
        int i = 1;
        boolean z = false;
        int i2 = 0;
        for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
            JSONObject A0a3 = C117295Zj.A0a();
            JSONObject A0a4 = C117295Zj.A0a();
            String A01 = A01(subscriptionInfo);
            String number = subscriptionInfo.getNumber();
            C30931Zj r1 = this.A05;
            StringBuilder A0k = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled simId : ");
            A0k.append(C1309060l.A01(A01));
            A0k.append(" | storedId : ");
            r1.A06(C12960it.A0d(C1309060l.A01(A09), A0k));
            if (AnonymousClass60W.A00(this.A00, this.A04, number, str)) {
                this.A05.A06("Phone matched");
                return 0;
            }
            C30931Zj r13 = this.A05;
            StringBuilder A0k2 = C12960it.A0k("checkSimWithWaRegisteredNumberIsInstalled Phone number not matched | sim number : ");
            A0k2.append(number);
            A0k2.append(" | waNumber : ");
            r13.A06(C12960it.A0d(str, A0k2));
            if (TextUtils.isEmpty(number) && (TextUtils.isEmpty(A01) || TextUtils.isEmpty(A09) || TextUtils.equals(A01, A09))) {
                i = 0;
            }
            if (!z) {
                z = TextUtils.equals(A01, A09);
            }
            try {
                A0a3.put("slotIndex", subscriptionInfo.getSimSlotIndex());
                A0a3.put("simPhoneNumber", number);
                A0a3.put("storedId", A09);
                A0a3.put("simId", A01);
                A0a3.put("waPhoneNumber", str);
                A0a4.put("isSimNumberEmpty", TextUtils.isEmpty(number));
                A0a4.put("isSimIdEmpty", TextUtils.isEmpty(A01));
                A0a4.put("isStoredIdEmpty", TextUtils.isEmpty(A09));
                A0a4.put("isSimIdMatched", TextUtils.equals(A01, A09));
                A0a4.put("isAddPaymentAttempted", z);
                A0a.put(C12960it.A0f(C12960it.A0j("subIndex_"), i2), A0a4);
                A0a2.put(C12960it.A0f(C12960it.A0j("subIndex_"), i2), A0a3);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i2++;
        }
        if (i != 0 && z && activeSubscriptionInfoList.size() > 1) {
            i = 2;
        }
        this.A05.A06(C12960it.A0W(i, "Fallback to ICCID match "));
        if (i != 0) {
            r20.A02 = A0a2;
            r20.A03 = A0a;
            r20.A00("SIM_SWAP", null);
        }
        return i;
    }

    public String A04(String str) {
        List<SubscriptionInfo> activeSubscriptionInfoList;
        SubscriptionManager A0M = this.A01.A0M();
        if (!(A0M == null || (activeSubscriptionInfoList = A0M.getActiveSubscriptionInfoList()) == null)) {
            int i = 0;
            int A03 = this.A03.A03();
            for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                if (A03 == subscriptionInfo.getSubscriptionId()) {
                    i = subscriptionInfo.getSimSlotIndex();
                }
                String A01 = A01(subscriptionInfo);
                if (AnonymousClass60W.A00(this.A00, this.A04, subscriptionInfo.getNumber(), str)) {
                    this.A05.A04("iccid matched number");
                    return A01;
                }
            }
            if (activeSubscriptionInfoList.size() > 0) {
                this.A05.A04("no matching phone number found, storing the selected iccid");
                return A01(activeSubscriptionInfoList.get(i));
            }
        }
        return null;
    }
}
