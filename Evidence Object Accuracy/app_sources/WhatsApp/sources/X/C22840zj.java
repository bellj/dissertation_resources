package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22840zj {
    public final C006202y A00 = new C006202y(2500);
    public final C18460sU A01;
    public final C16490p7 A02;

    public C22840zj(C18460sU r3, C16490p7 r4) {
        this.A01 = r3;
        this.A02 = r4;
    }

    public Long A00(UserJid userJid) {
        Long valueOf;
        C006202y r4 = this.A00;
        synchronized (r4) {
            Long l = (Long) r4.A04(userJid);
            if (l != null) {
                return l;
            }
            String[] strArr = {String.valueOf(this.A01.A01(userJid))};
            C16310on A01 = this.A02.get();
            try {
                Cursor A09 = A01.A03.A09("SELECT version FROM primary_device_version WHERE user_jid_row_id = ?", strArr);
                if (A09.moveToNext()) {
                    long j = A09.getLong(A09.getColumnIndexOrThrow("version"));
                    synchronized (r4) {
                        valueOf = Long.valueOf(j);
                        r4.A08(userJid, valueOf);
                    }
                    A09.close();
                    A01.close();
                    return valueOf;
                }
                A09.close();
                A01.close();
                return null;
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public Map A01(Set set) {
        Long valueOf;
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        C006202y r7 = this.A00;
        synchronized (r7) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                UserJid userJid = (UserJid) it.next();
                if (r7.A04(userJid) != null) {
                    hashMap.put(userJid, (Long) r7.A04(userJid));
                } else {
                    arrayList.add(userJid);
                }
            }
        }
        String[] strArr = new String[arrayList.size()];
        int i = 0;
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            strArr[i] = String.valueOf(this.A01.A01((Jid) it2.next()));
            i++;
        }
        C29841Uw r1 = new C29841Uw(strArr, 975);
        C16310on A01 = this.A02.get();
        try {
            Iterator it3 = r1.iterator();
            while (it3.hasNext()) {
                String[] strArr2 = (String[]) it3.next();
                C16330op r3 = A01.A03;
                int length = strArr2.length;
                StringBuilder sb = new StringBuilder("SELECT user_jid_row_id, version FROM primary_device_version WHERE user_jid_row_id IN ");
                sb.append(AnonymousClass1Ux.A00(length));
                Cursor A09 = r3.A09(sb.toString(), strArr2);
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("user_jid_row_id");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("version");
                while (A09.moveToNext()) {
                    UserJid userJid2 = (UserJid) this.A01.A07(UserJid.class, A09.getLong(columnIndexOrThrow));
                    AnonymousClass009.A05(userJid2);
                    long j = A09.getLong(columnIndexOrThrow2);
                    synchronized (r7) {
                        valueOf = Long.valueOf(j);
                        r7.A08(userJid2, valueOf);
                    }
                    hashMap.put(userJid2, valueOf);
                }
                A09.close();
            }
            A01.close();
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public synchronized void A02(UserJid userJid) {
        Long A00 = A00(userJid);
        long j = 1;
        if (A00 != null) {
            j = 1 + A00.longValue();
        }
        long A01 = this.A01.A01(userJid);
        C16310on A02 = this.A02.A02();
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("user_jid_row_id", Long.valueOf(A01));
        contentValues.put("version", Long.valueOf(j));
        A02.A03.A06(contentValues, "primary_device_version", 5);
        C006202y r1 = this.A00;
        synchronized (r1) {
            r1.A07(userJid);
        }
        A02.close();
    }
}
