package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.1HU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HU extends BroadcastReceiver {
    public final C20220vP A00;
    public final C14860mA A01;
    public final Object A02 = new Object();
    public volatile boolean A03 = false;

    public AnonymousClass1HU(C20220vP r2, C14860mA r3) {
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass22D.A00(context);
                    this.A03 = true;
                }
            }
        }
        this.A01.A0A = false;
        boolean booleanExtra = intent.getBooleanExtra("noPopup", true);
        boolean booleanExtra2 = intent.getBooleanExtra("isAndroidWearRefresh", false);
        this.A00.A0C(C38211ni.A02(intent), booleanExtra, booleanExtra2);
    }
}
