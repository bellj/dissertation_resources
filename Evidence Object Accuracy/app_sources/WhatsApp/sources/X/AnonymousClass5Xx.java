package X;

import java.nio.ByteBuffer;

/* renamed from: X.5Xx  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Xx {
    public static final ByteBuffer A00 = C72453ed.A0x(0);

    C94084bE A7V(C94084bE v);

    ByteBuffer AEn();

    boolean AJD();

    boolean AJN();

    void AZj();

    void AZk(ByteBuffer byteBuffer);

    void flush();

    void reset();
}
