package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3XA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XA implements AnonymousClass23E {
    public final /* synthetic */ View$OnClickListenerC55232i0 A00;
    public final /* synthetic */ AbstractC35611iN A01;
    public final /* synthetic */ AnonymousClass23D A02;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XA(View$OnClickListenerC55232i0 r1, AbstractC35611iN r2, AnonymousClass23D r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        View$OnClickListenerC55232i0 r0 = this.A00;
        AnonymousClass2T1 r1 = r0.A03;
        r1.setBackgroundColor(r0.A00);
        r1.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        int i;
        View$OnClickListenerC55232i0 r7 = this.A00;
        AnonymousClass2T1 r3 = r7.A03;
        if (r3.getTag() != this.A02) {
            return;
        }
        if (bitmap == MediaGalleryFragmentBase.A0U) {
            r3.setScaleType(ImageView.ScaleType.CENTER);
            AbstractC35611iN r5 = this.A01;
            int type = r5.getType();
            if (type == 0) {
                r3.setBackgroundColor(r7.A00);
                i = R.drawable.ic_missing_thumbnail_picture;
            } else if (type == 1 || type == 2) {
                r3.setBackgroundColor(r7.A00);
                i = R.drawable.ic_missing_thumbnail_video;
            } else if (type != 3) {
                r3.setBackgroundColor(r7.A00);
                if (type != 4) {
                    r3.setImageResource(0);
                    return;
                } else {
                    r3.setImageDrawable(C26511Dt.A03(r3.getContext(), r5.AES(), null, false));
                    return;
                }
            } else {
                C12970iu.A18(r3.getContext(), r3, R.color.music_scrubber);
                i = R.drawable.gallery_audio_item;
            }
            r3.setImageResource(i);
            return;
        }
        C12990iw.A1E(r3);
        r3.setBackgroundResource(0);
        ((AnonymousClass2T3) r3).A00 = bitmap;
        if (!z) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(r3.getResources(), bitmap);
            Drawable[] drawableArr = new Drawable[2];
            drawableArr[0] = r7.A01;
            C12960it.A15(r3, bitmapDrawable, drawableArr);
            return;
        }
        r3.setImageBitmap(bitmap);
    }
}
