package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119765f4 extends AbstractC30871Zd {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(21);
    public long A00;
    public C128505wD A01;
    public C128505wD A02;
    public String A03;
    public String A04;
    public String A05;
    public boolean A06;

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return null;
    }

    @Override // X.AnonymousClass1ZY
    public boolean A0A() {
        return true;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0049, code lost:
        if (X.C117295Zj.A1T(r10, "default-debit", null, "1") != false) goto L_0x004b;
     */
    @Override // X.AnonymousClass1ZP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass102 r9, X.AnonymousClass1V8 r10, int r11) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119765f4.A01(X.102, X.1V8, int):void");
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: NoviPaymentMethodCardCountryData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        try {
            JSONObject A0C = A0C();
            A0C.put("status", this.A0I);
            A0C.put("last4", this.A05);
            A0C.put("card-number", this.A04);
            A0C.put("account-id", this.A03);
            A0C.put("is-prepaid", this.A02);
            A0C.put("is-debit", this.A01);
            A0C.put("is-top-up", this.A06);
            A0C.put("last-update-ts", this.A00);
            return A0C.toString();
        } catch (JSONException unused) {
            Log.e("PAY: NoviPaymentMethodCardCountryData toDBString threw exception");
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        try {
            JSONObject A05 = C13000ix.A05(str);
            A0D(A05);
            this.A0I = A05.getString("status");
            this.A05 = A05.getString("last4");
            this.A04 = A05.getString("card-number");
            this.A03 = A05.getString("account-id");
            this.A02 = new C128505wD(A05.getString("is-prepaid"));
            this.A01 = new C128505wD(A05.getString("is-debit"));
            this.A06 = A05.getBoolean("is-top-up");
            this.A00 = A05.getLong("last-update-ts");
        } catch (JSONException unused) {
            Log.e("PAY: NoviPaymentMethodCardCountryData fromDBString threw exception");
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C17930rd A00 = C17930rd.A00(this.A0G);
        String str = this.A0H;
        int i = super.A00;
        int i2 = 0;
        int A002 = C117305Zk.A00(this.A0W ? 1 : 0);
        if (this.A0V) {
            i2 = 2;
        }
        return C30881Ze.A06(A00, this, str, this.A05, i, A002, i2, 0, 0, super.A01, super.A05);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A0G);
        parcel.writeString(this.A0H);
        parcel.writeInt(super.A00);
        parcel.writeLong(super.A05);
        parcel.writeInt(super.A01);
        parcel.writeInt(this.A0W ? 1 : 0);
        parcel.writeInt(this.A0V ? 1 : 0);
        parcel.writeInt(this.A0S ? 1 : 0);
        parcel.writeInt(this.A0R ? 1 : 0);
        parcel.writeString(this.A0I);
        parcel.writeString(this.A05);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02.toString());
        parcel.writeString(this.A01.toString());
        parcel.writeInt(this.A06 ? 1 : 0);
        parcel.writeLong(this.A00);
    }
}
