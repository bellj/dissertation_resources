package X;

/* renamed from: X.5Nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114935Nq extends C94474bs {
    public C114935Nq(AnonymousClass5XE r2) {
        if (!(r2 instanceof AbstractC112995Fp)) {
            this.A01 = r2;
            this.A05 = new byte[r2.AAt() << 1];
            this.A00 = 0;
            return;
        }
        throw C12970iu.A0f("CTSBlockCipher can only accept ECB, or CBC ciphers");
    }
}
