package X;

import java.util.Map;

/* renamed from: X.3t9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80903t9 extends AnonymousClass5DS {
    public final Object key;
    public int lastKnownIndex;
    public final /* synthetic */ AnonymousClass5I4 this$0;

    public C80903t9(AnonymousClass5I4 r2, int i) {
        this.this$0 = r2;
        this.key = r2.key(i);
        this.lastKnownIndex = i;
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public Object getKey() {
        return this.key;
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public Object getValue() {
        Map delegateOrNull = this.this$0.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.get(this.key);
        }
        updateLastKnownIndex();
        int i = this.lastKnownIndex;
        return i == -1 ? C87804Da.unsafeNull() : this.this$0.value(i);
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public Object setValue(Object obj) {
        Map delegateOrNull = this.this$0.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.put(this.key, obj);
        }
        updateLastKnownIndex();
        int i = this.lastKnownIndex;
        if (i == -1) {
            this.this$0.put(this.key, obj);
            return C87804Da.unsafeNull();
        }
        AnonymousClass5I4 r0 = this.this$0;
        Object obj2 = r0.value(i);
        r0.setValue(i, obj);
        return obj2;
    }

    private void updateLastKnownIndex() {
        int i = this.lastKnownIndex;
        if (i == -1 || i >= this.this$0.size() || !AnonymousClass28V.A00(this.key, this.this$0.key(this.lastKnownIndex))) {
            this.lastKnownIndex = this.this$0.indexOf(this.key);
        }
    }
}
