package X;

/* renamed from: X.43m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856143m extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public String A04;
    public String A05;

    public C856143m() {
        super(2574, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(4, this.A00);
        r3.Abe(8, this.A04);
        r3.Abe(1, this.A05);
        r3.Abe(6, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdLinkDevicePrimary {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdDurationS", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdLinkDevicePrimaryErrorCode", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdLinkDevicePrimaryStage", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdRegAttemptId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdSessionId", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mdTimestampS", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
