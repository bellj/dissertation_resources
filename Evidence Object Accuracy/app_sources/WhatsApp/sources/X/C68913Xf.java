package X;

import java.io.File;

/* renamed from: X.3Xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68913Xf implements AbstractC36361jl {
    public final /* synthetic */ AnonymousClass2A9 A00;
    public final /* synthetic */ C15370n3 A01;
    public final /* synthetic */ AnonymousClass1JV A02;

    public C68913Xf(AnonymousClass2A9 r1, C15370n3 r2, AnonymousClass1JV r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC36361jl
    public void APl(int i) {
        AnonymousClass2A9 r1 = this.A00;
        AnonymousClass2A9.A00(r1, r1.A0H.decrementAndGet());
    }

    @Override // X.AbstractC36361jl
    public void AWy(C15580nU r6, C47572Bl r7) {
        AnonymousClass2A9 r4 = this.A00;
        r4.A0G.remove(this.A02);
        File A00 = r4.A08.A00(this.A01);
        if (A00 != null && A00.exists()) {
            r4.A0F.A0B(r4.A07.A0B(r6), A00, false);
        }
        AnonymousClass2A9.A00(r4, r4.A0H.decrementAndGet());
    }

    @Override // X.AbstractC36361jl
    public void AXX() {
        AnonymousClass2A9 r1 = this.A00;
        AnonymousClass2A9.A00(r1, r1.A0H.decrementAndGet());
    }
}
