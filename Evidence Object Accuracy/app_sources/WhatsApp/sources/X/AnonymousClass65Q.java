package X;

import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import java.util.ArrayList;

/* renamed from: X.65Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65Q implements AnonymousClass07L {
    public final /* synthetic */ PaymentTransactionHistoryActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public AnonymousClass65Q(PaymentTransactionHistoryActivity paymentTransactionHistoryActivity) {
        this.A00 = paymentTransactionHistoryActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        PaymentTransactionHistoryActivity paymentTransactionHistoryActivity = this.A00;
        ArrayList A02 = C32751cg.A02(paymentTransactionHistoryActivity.A05, str);
        paymentTransactionHistoryActivity.A0L = A02;
        paymentTransactionHistoryActivity.A0K = str;
        if (A02.isEmpty()) {
            paymentTransactionHistoryActivity.A0L = null;
        }
        paymentTransactionHistoryActivity.A2f();
        return false;
    }
}
