package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31461aa extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C31461aa A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public C31471ab A03;

    static {
        C31461aa r0 = new C31461aa();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A01) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A02.get(i3), 2);
        }
        if ((this.A00 & 2) == 2) {
            C31471ab r0 = this.A03;
            if (r0 == null) {
                r0 = C31471ab.A03;
            }
            i += CodedOutputStream.A0A(r0, 3);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 2);
        }
        if ((this.A00 & 2) == 2) {
            C31471ab r0 = this.A03;
            if (r0 == null) {
                r0 = C31471ab.A03;
            }
            codedOutputStream.A0L(r0, 3);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
