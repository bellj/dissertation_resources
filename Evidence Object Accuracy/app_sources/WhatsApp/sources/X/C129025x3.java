package X;

/* renamed from: X.5x3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129025x3 {
    public boolean A00 = false;
    public boolean A01 = false;
    public boolean A02 = false;
    public final C30921Zi A03;

    public C129025x3(C30921Zi r2) {
        this.A03 = r2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("ExpressiveBackgroundData{backgroundMetaData=");
        A0k.append(this.A03);
        A0k.append(", isAssetDownloaded=");
        A0k.append(this.A00);
        A0k.append(", isDownloadFailed=");
        A0k.append(this.A01);
        A0k.append(", isSelected=");
        A0k.append(this.A02);
        return C12970iu.A0v(A0k);
    }
}
