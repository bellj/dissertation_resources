package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: X.0zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22680zT {
    public static volatile C22680zT A04;
    public Context A00;
    public AnonymousClass1NC A01 = null;
    public ArrayList A02;
    public boolean A03 = false;

    public static C22680zT A00() {
        if (A04 == null) {
            synchronized (C22680zT.class) {
                if (A04 == null) {
                    A04 = new C22680zT();
                }
            }
        }
        return A04;
    }

    public AnonymousClass1ND A01(String str) {
        synchronized (this) {
            if (this.A01 == null) {
                try {
                    A05();
                    AnonymousClass1NC r3 = new AnonymousClass1NC(this.A02.size());
                    Iterator it = this.A02.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1ND r1 = (AnonymousClass1ND) it.next();
                        r3.A03(r1.A03, r1);
                    }
                    this.A01 = r3;
                } catch (IOException unused) {
                    throw new RuntimeException("empty metadata");
                }
            }
        }
        try {
            return (AnonymousClass1ND) this.A01.A01(str);
        } catch (IllegalArgumentException unused2) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006b, code lost:
        r5 = r5 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A02(int r10, java.lang.String r11) {
        /*
            r9 = this;
            r9.A05()
            java.util.ArrayList r0 = r9.A02
            java.util.Iterator r2 = r0.iterator()
        L_0x0009:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x007b
            java.lang.Object r4 = r2.next()
            X.1ND r4 = (X.AnonymousClass1ND) r4
            int r1 = r4.A00
            if (r1 != r10) goto L_0x0009
            r0 = 7
            if (r1 == r0) goto L_0x0047
            r0 = 241(0xf1, float:3.38E-43)
            if (r1 == r0) goto L_0x0047
            r0 = 998(0x3e6, float:1.398E-42)
            if (r1 == r0) goto L_0x0047
            r7 = 0
            r5 = 0
        L_0x0026:
            int r0 = r11.length()
            if (r5 >= r0) goto L_0x007b
            java.lang.String[] r6 = r4.A0B
            if (r6 == 0) goto L_0x007b
            r3 = 0
            r2 = 0
        L_0x0032:
            int r0 = r6.length
            if (r3 >= r0) goto L_0x0071
            if (r2 != 0) goto L_0x0078
            r0 = r6[r3]
            char r1 = r0.charAt(r7)
            char r0 = r11.charAt(r5)
            if (r1 != r0) goto L_0x0044
            r2 = 1
        L_0x0044:
            int r3 = r3 + 1
            goto L_0x0032
        L_0x0047:
            int r8 = r11.length()
            r7 = 0
            r5 = 0
        L_0x004d:
            if (r5 >= r8) goto L_0x007b
            java.lang.String[] r6 = r4.A0B
            if (r6 == 0) goto L_0x007b
            int r0 = r8 - r5
            int r0 = r4.A00(r0)
            if (r0 <= 0) goto L_0x0073
            int r3 = r6.length
            r2 = 0
        L_0x005d:
            if (r2 >= r3) goto L_0x0073
            r0 = r6[r2]
            char r1 = r0.charAt(r7)
            char r0 = r11.charAt(r5)
            if (r1 != r0) goto L_0x006e
            int r5 = r5 + 1
            goto L_0x004d
        L_0x006e:
            int r2 = r2 + 1
            goto L_0x005d
        L_0x0071:
            if (r2 != 0) goto L_0x0078
        L_0x0073:
            java.lang.String r11 = r11.substring(r5)
            return r11
        L_0x0078:
            int r5 = r5 + 1
            goto L_0x0026
        L_0x007b:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22680zT.A02(int, java.lang.String):java.lang.String");
    }

    public String A03(String str) {
        if (!TextUtils.isEmpty(str)) {
            A05();
            try {
                int parseInt = Integer.parseInt(str);
                Iterator it = this.A02.iterator();
                while (it.hasNext()) {
                    AnonymousClass1ND r1 = (AnonymousClass1ND) it.next();
                    if (r1.A00 == parseInt) {
                        return r1.A05;
                    }
                }
            } catch (NumberFormatException e) {
                Log.i("countries/get-tos-region", e);
                return "";
            }
        }
        return "";
    }

    public String A04(String str) {
        int i;
        A05();
        int length = str.length();
        if (length != 2) {
            if (length == 3 && !str.equals("999")) {
                Iterator it = this.A02.iterator();
                while (it.hasNext()) {
                    AnonymousClass1ND r1 = (AnonymousClass1ND) it.next();
                    int[] iArr = r1.A07;
                    if (iArr != null) {
                        for (int i2 : iArr) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(i2);
                            if (str.equals(sb.toString())) {
                                i = r1.A00;
                                return Integer.toString(i);
                            }
                        }
                        continue;
                    }
                }
            }
            return null;
        }
        AnonymousClass1ND A01 = A01(str.toUpperCase(Locale.US));
        if (A01 != null) {
            i = A01.A00;
            return Integer.toString(i);
        }
        return null;
    }

    public final synchronized void A05() {
        String[] split;
        if (!this.A03) {
            this.A02 = new ArrayList(243);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.A00.getResources().openRawResource(R.raw.countries), AnonymousClass01V.A08));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    bufferedReader.close();
                    this.A03 = true;
                    break;
                }
                try {
                    split = TextUtils.split(readLine, "\t");
                } catch (NumberFormatException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("countries/load/bad-number: ");
                    sb.append(readLine);
                    Log.e(sb.toString(), e);
                } catch (IllegalArgumentException e2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("countries/load/bad-line: ");
                    sb2.append(readLine);
                    Log.e(sb2.toString(), e2);
                }
                if (split == null || split.length < 12) {
                    throw new IllegalArgumentException();
                    break;
                }
                this.A02.add(new AnonymousClass1ND(this, split));
            }
        }
    }
}
