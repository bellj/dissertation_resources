package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65963Lt implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(69);
    public long A00;
    public String A01;
    public boolean A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C65963Lt(Parcel parcel) {
        String readString = parcel.readString();
        this.A01 = readString == null ? "UNKNOWN" : readString;
        this.A00 = parcel.readLong();
        this.A02 = C12970iu.A1W(parcel.readInt());
    }

    public C65963Lt(String str, long j, boolean z) {
        this.A01 = str;
        this.A00 = j;
        this.A02 = z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeInt(this.A02 ? 1 : 0);
    }
}
