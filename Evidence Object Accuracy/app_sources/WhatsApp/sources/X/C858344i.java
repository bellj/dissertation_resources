package X;

import com.whatsapp.group.GroupAdminPickerActivity;
import java.util.Set;

/* renamed from: X.44i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858344i extends AbstractC33331dp {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    public C858344i(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A00 = groupAdminPickerActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        GroupAdminPickerActivity groupAdminPickerActivity = this.A00;
        groupAdminPickerActivity.A2h(groupAdminPickerActivity.A0N);
    }
}
