package X;

/* renamed from: X.5Kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC114145Kj extends AnonymousClass5LF implements AnonymousClass5WP, AnonymousClass5VG {
    public C10710f4 A00;

    @Override // X.AnonymousClass5WP
    public AnonymousClass5L9 ADu() {
        return null;
    }

    @Override // X.AnonymousClass5WP
    public boolean AJD() {
        return true;
    }

    public final void A0B(C10710f4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VG
    public void dispose() {
        C10710f4 r0 = this.A00;
        if (r0 != null) {
            r0.A0a(this);
            return;
        }
        throw C16700pc.A06("job");
    }

    @Override // X.C94524by
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C72453ed.A0p(this, A0h));
        A0h.append("[job@");
        C10710f4 r0 = this.A00;
        if (r0 != null) {
            A0h.append(C72453ed.A0o(r0));
            return C72453ed.A0t(A0h);
        }
        throw C16700pc.A06("job");
    }
}
