package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44911zl extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass10K A00;

    public C44911zl(AnonymousClass10K r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Log.i("gdrive-notification-manager/user-decided-to-restore-over-data-connection");
        AnonymousClass10K r5 = this.A00;
        Context context2 = r5.A0J.A00;
        Intent intent2 = new Intent();
        intent2.setClassName(context2.getPackageName(), "com.whatsapp.backup.google.SettingsGoogleDrive");
        if (!TextUtils.isEmpty("action_perform_media_restore_over_cellular")) {
            intent2.setAction("action_perform_media_restore_over_cellular");
        }
        intent2.setFlags(335544320);
        context2.startActivity(intent2);
        context2.unregisterReceiver(this);
        r5.A03();
    }
}
