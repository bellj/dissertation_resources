package X;

/* renamed from: X.3rz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80273rz extends AbstractC80283s0 {
    public final byte[] zzb;

    public C80273rz(byte[] bArr) {
        this.zzb = bArr;
    }

    public static AbstractC111925Bi A00(C80273rz r2, int i) {
        int A01 = AbstractC111925Bi.A01(0, i, r2.A02());
        if (A01 == 0) {
            return AbstractC111925Bi.A00;
        }
        return new C80263ry(r2.zzb, r2.A03(), A01);
    }

    public int A03() {
        if (!(this instanceof C80263ry)) {
            return 0;
        }
        return ((C80263ry) this).zzc;
    }

    @Override // X.AbstractC111925Bi, java.lang.Object
    public final boolean equals(Object obj) {
        int A02;
        AbstractC111925Bi r7;
        int A022;
        if (obj != this) {
            if ((obj instanceof AbstractC111925Bi) && (A02 = A02()) == (A022 = (r7 = (AbstractC111925Bi) obj).A02())) {
                if (A02 != 0) {
                    if (!(obj instanceof C80273rz)) {
                        return obj.equals(this);
                    }
                    int i = this.zzc;
                    int i2 = r7.zzc;
                    if (i == 0 || i2 == 0 || i == i2) {
                        if (A02 > A022) {
                            StringBuilder A0t = C12980iv.A0t(40);
                            A0t.append("Length too large: ");
                            A0t.append(A02);
                            throw C12970iu.A0f(C12960it.A0f(A0t, A02));
                        } else if (A02 <= A022) {
                            boolean z = r7 instanceof C80273rz;
                            C80273rz r72 = (C80273rz) r7;
                            if (!z) {
                                return A00(r72, A02).equals(A00(this, A02));
                            }
                            byte[] bArr = this.zzb;
                            byte[] bArr2 = r72.zzb;
                            int A03 = A03();
                            int i3 = A03 + A02;
                            int A032 = r72.A03();
                            while (A03 < i3) {
                                if (bArr[A03] != bArr2[A032]) {
                                    return false;
                                }
                                A03++;
                                A032++;
                            }
                            return true;
                        } else {
                            StringBuilder A0t2 = C12980iv.A0t(59);
                            A0t2.append("Ran off end of other: 0, ");
                            A0t2.append(A02);
                            throw C12970iu.A0f(C12960it.A0e(", ", A0t2, A022));
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }
}
