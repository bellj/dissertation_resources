package X;

import java.security.Permission;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.5HZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HZ extends Permission {
    public final Set actions;

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass5HZ) && this.actions.equals(((AnonymousClass5HZ) obj).actions);
    }

    @Override // java.security.Permission
    public String getActions() {
        return this.actions.toString();
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.actions.hashCode();
    }

    @Override // java.security.Permission
    public boolean implies(Permission permission) {
        if (!(permission instanceof AnonymousClass5HZ)) {
            return false;
        }
        AnonymousClass5HZ r4 = (AnonymousClass5HZ) permission;
        return getName().equals(r4.getName()) || this.actions.containsAll(r4.actions);
    }

    public AnonymousClass5HZ(String str) {
        super(str);
        HashSet A12 = C12970iu.A12();
        this.actions = A12;
        A12.add(str);
    }
}
