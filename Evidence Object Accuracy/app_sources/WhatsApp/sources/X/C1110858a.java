package X;

/* renamed from: X.58a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1110858a implements AnonymousClass5WA {
    public final /* synthetic */ AnonymousClass2DR A00;
    public final /* synthetic */ AnonymousClass17Q A01;

    public C1110858a(AnonymousClass2DR r1, AnonymousClass17Q r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WA
    public void AQE(AnonymousClass3FA r3) {
        C16700pc.A0E(r3, 0);
        this.A00.A00(AnonymousClass3I1.A00(r3));
    }

    @Override // X.AnonymousClass5WA
    public void AQF(AnonymousClass3EI r3) {
        AnonymousClass3I1.A02(this.A01, r3);
        this.A00.A01(AnonymousClass3I1.A01(r3));
    }
}
