package X;

import android.net.Uri;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;

/* renamed from: X.3U9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3U9 implements AnonymousClass2GV {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ ViewOnClickCListenerShape14S0100000_I0_1 A01;

    public AnonymousClass3U9(ActivityC13810kN r1, ViewOnClickCListenerShape14S0100000_I0_1 viewOnClickCListenerShape14S0100000_I0_1) {
        this.A01 = viewOnClickCListenerShape14S0100000_I0_1;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2GV
    public void AO1() {
        String str;
        C26151Cf r4 = ((AnonymousClass1OY) this.A01.A00).A17;
        Uri.Builder buildUpon = Uri.parse("https://faq.whatsapp.com/general/verification/about-autofilling-security-codes-on-whatsapp").buildUpon();
        AnonymousClass018 r2 = r4.A00;
        buildUpon.appendQueryParameter("lg", r2.A06());
        buildUpon.appendQueryParameter("lc", r2.A05());
        if (r4.A01.A04()) {
            str = "1";
        } else {
            str = "0";
        }
        buildUpon.appendQueryParameter("eea", str);
        this.A00.startActivity(C12970iu.A0B(buildUpon.build()));
    }
}
