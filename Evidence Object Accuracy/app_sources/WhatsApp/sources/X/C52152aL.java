package X;

import android.telephony.PhoneStateListener;

/* renamed from: X.2aL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52152aL extends PhoneStateListener {
    public boolean A00 = false;
    public final /* synthetic */ C29631Ua A01;

    public C52152aL(C29631Ua r2) {
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        if (r1.isSpeakerphoneOn() == false) goto L_0x002c;
     */
    @Override // android.telephony.PhoneStateListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCallStateChanged(int r7, java.lang.String r8) {
        /*
            r6 = this;
            X.1Ua r4 = r6.A01
            X.01d r0 = r4.A1n
            android.media.AudioManager r1 = r0.A0G()
            java.lang.String r0 = "voip/phoneStateListener/onCallStateChanged state: "
            java.lang.StringBuilder r2 = X.C12960it.A0k(r0)
            if (r7 == 0) goto L_0x009a
            r0 = 1
            if (r7 == r0) goto L_0x0097
            r0 = 2
            if (r7 == r0) goto L_0x0094
            java.lang.String r0 = "UNKNOWN_TELEPHONY_CALL_STATE"
        L_0x0019:
            r2.append(r0)
            java.lang.String r0 = " using speaker: "
            r2.append(r0)
            r5 = 0
            r3 = 1
            if (r1 == 0) goto L_0x002c
            boolean r1 = r1.isSpeakerphoneOn()
            r0 = 1
            if (r1 != 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r2.append(r0)
            X.C12960it.A1F(r2)
            java.lang.String r0 = com.whatsapp.voipcalling.Voip.getCurrentCallId()
            boolean r1 = r4.A14(r0)
            r1 = r1 ^ r3
            java.lang.String r0 = "PhoneStateListener is only used when Telecom Framework is not enabled"
            X.AnonymousClass009.A0A(r0, r1)
            int r2 = r4.A04
            r4.A04 = r7
            r0 = 2
            if (r7 == r0) goto L_0x004e
            boolean r0 = r6.A00
            if (r0 == 0) goto L_0x004f
            if (r7 != r3) goto L_0x004f
        L_0x004e:
            r5 = 1
        L_0x004f:
            boolean r0 = r6.A00
            if (r5 != r0) goto L_0x0064
            com.whatsapp.voipcalling.CallInfo r1 = com.whatsapp.voipcalling.Voip.getCallInfo()
            if (r1 == 0) goto L_0x0063
            if (r7 == 0) goto L_0x007d
            X.2Nu r0 = r4.A2P
            r0.A06(r1)
            r0.A01()
        L_0x0063:
            return
        L_0x0064:
            r6.A00 = r5
            android.os.Handler r0 = r4.A0L
            r1 = 6
            android.os.Message r3 = r0.obtainMessage(r1, r7, r5)
            android.os.Handler r0 = r4.A0L
            r0.removeMessages(r1)
            android.os.Handler r2 = r4.A0L
            if (r5 == 0) goto L_0x007a
            r2.sendMessage(r3)
            return
        L_0x007a:
            r0 = 1000(0x3e8, double:4.94E-321)
            goto L_0x0090
        L_0x007d:
            if (r2 == 0) goto L_0x0063
            android.os.Handler r0 = r4.A0L
            r1 = 38
            android.os.Message r3 = r0.obtainMessage(r1)
            android.os.Handler r0 = r4.A0L
            r0.removeMessages(r1)
            android.os.Handler r2 = r4.A0L
            r0 = 2000(0x7d0, double:9.88E-321)
        L_0x0090:
            r2.sendMessageDelayed(r3, r0)
            return
        L_0x0094:
            java.lang.String r0 = "CALL_STATE_OFFHOOK"
            goto L_0x0019
        L_0x0097:
            java.lang.String r0 = "CALL_STATE_RINGING"
            goto L_0x0019
        L_0x009a:
            java.lang.String r0 = "CALL_STATE_IDLE"
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52152aL.onCallStateChanged(int, java.lang.String):void");
    }
}
