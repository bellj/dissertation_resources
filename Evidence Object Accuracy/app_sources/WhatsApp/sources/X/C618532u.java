package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.32u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618532u extends AbstractC75713kI {
    public C15370n3 A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextView A03;
    public final TextEmojiLabel A04;
    public final TextEmojiLabel A05;
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A06;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618532u(View view, AbstractView$OnCreateContextMenuListenerC35851ir r4) {
        super(view, r4);
        this.A06 = r4;
        this.A04 = C12970iu.A0U(view, R.id.name);
        this.A02 = C12960it.A0J(view, R.id.status);
        this.A03 = C12960it.A0J(view, R.id.time_left);
        ImageView A0L = C12970iu.A0L(view, R.id.avatar);
        this.A01 = A0L;
        this.A05 = C12970iu.A0U(view, R.id.push_name);
        AnonymousClass028.A0a(A0L, 2);
    }

    @Override // X.AbstractC75713kI
    public void A08(C15370n3 r14, C30751Yr r15) {
        String A0X;
        this.A00 = r14;
        C12960it.A14(this.A0H, this, r15, 15);
        AbstractView$OnCreateContextMenuListenerC35851ir r7 = this.A06;
        C14830m7 r10 = r7.A1A;
        long A00 = r10.A00();
        C15370n3 r1 = this.A00;
        C15570nT r0 = r7.A0z;
        r0.A08();
        boolean equals = r1.equals(r0.A01);
        TextEmojiLabel textEmojiLabel = this.A04;
        if (equals) {
            textEmojiLabel.setText(R.string.you);
            AbstractView$OnClickListenerC34281fs.A01(this.A02, this, 31);
            long A03 = r7.A1J.A03(r7.A0c) - A00;
            if (A03 >= 0) {
                String A07 = C38131nZ.A07(r7.A1C, A03);
                TextView textView = this.A03;
                textView.setText(A07);
                textView.setVisibility(0);
            } else {
                this.A03.setVisibility(8);
            }
        } else {
            C15610nY r8 = r7.A17;
            textEmojiLabel.setText(r8.A04(this.A00));
            long j = r15.A05;
            if (A00 - j < 60000) {
                A0X = r7.A0E.getString(R.string.location_just_now);
            } else {
                A0X = C12960it.A0X(r7.A0E, AnonymousClass3JK.A00(r7.A1C, r10.A02(j)), C12970iu.A1b(), 0, R.string.live_location_last_updated);
            }
            this.A02.setText(A0X);
            boolean A0L = r8.A0L(this.A00, -1);
            TextEmojiLabel textEmojiLabel2 = this.A05;
            if (A0L) {
                textEmojiLabel2.setVisibility(0);
                textEmojiLabel2.A0G(null, r8.A09(this.A00));
            } else {
                textEmojiLabel2.setVisibility(8);
            }
        }
        r7.A0b.A07(this.A01, this.A00, false);
    }
}
