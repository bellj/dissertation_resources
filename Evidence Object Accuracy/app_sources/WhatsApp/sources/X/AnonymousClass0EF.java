package X;

import android.view.View;

/* renamed from: X.0EF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EF extends AnonymousClass05W {
    public final /* synthetic */ AnonymousClass01E A00;

    public AnonymousClass0EF(AnonymousClass01E r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05W
    public View A00(int i) {
        AnonymousClass01E r2 = this.A00;
        View view = r2.A0A;
        if (view != null) {
            return view.findViewById(i);
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(r2);
        sb.append(" does not have a view");
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass05W
    public boolean A01() {
        return this.A00.A0A != null;
    }
}
