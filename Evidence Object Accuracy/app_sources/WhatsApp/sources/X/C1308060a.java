package X;

import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.os.SystemClock;

/* renamed from: X.60a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308060a {
    public long A00;
    public C130185yw A01;
    public AnonymousClass61Q A02;
    public AbstractC1311561m A03;
    public C119085cr A04;
    public AbstractC130695zp A05;
    public AnonymousClass60Q A06;
    public AbstractC136426Mm A07;
    public final C130175yv A08;
    public final C129805yK A09;
    public final C1308560f A0A;
    public volatile CameraDevice A0B;
    public volatile boolean A0C;
    public volatile boolean A0D;

    public C1308060a(C130175yv r2, C1308560f r3) {
        this.A0A = r3;
        this.A08 = r2;
        this.A09 = new C129805yK(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f3, code lost:
        if (r0 == false) goto L_0x0084;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass60Q A01(android.hardware.camera2.CaptureRequest.Builder r14, X.AnonymousClass66P r15, X.AbstractC136406Mk r16, X.AnonymousClass66I r17, X.C129845yO r18, java.lang.String r19, int r20, int r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 267
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1308060a.A01(android.hardware.camera2.CaptureRequest$Builder, X.66P, X.6Mk, X.66I, X.5yO, java.lang.String, int, int, boolean):X.60Q");
    }

    public Exception A02() {
        Exception e;
        this.A09.A01("Method stopVideoRecording() must be run on the background thread.");
        AbstractC136426Mm r0 = this.A07;
        if (r0 != null) {
            try {
                r0.AeU();
                e = null;
            } catch (Exception e2) {
                e = e2;
            }
            this.A07 = null;
        } else {
            e = null;
        }
        AnonymousClass61Q r02 = this.A02;
        if (r02 != null) {
            r02.A06();
        }
        this.A06 = null;
        this.A0D = false;
        this.A0C = false;
        return e;
    }

    public void A03() {
        this.A09.A02("Failed to release VideoCaptureController.", false);
        this.A0B = null;
        this.A05 = null;
        this.A04 = null;
        this.A03 = null;
        this.A02 = null;
        this.A01 = null;
    }

    public void A04(CameraDevice cameraDevice, C130185yw r5, AnonymousClass61Q r6, AbstractC1311561m r7, C119085cr r8, AbstractC130695zp r9) {
        C129805yK r2 = this.A09;
        r2.A01("Can prepare only on the Optic thread");
        this.A0B = cameraDevice;
        this.A05 = r9;
        this.A04 = r8;
        this.A03 = r7;
        this.A02 = r6;
        this.A01 = r5;
        r2.A02("Failed to prepare VideoCaptureController.", true);
    }

    public void A05(CaptureRequest.Builder builder, AnonymousClass66P r18, AbstractC129405xf r19, AbstractC136406Mk r20, AnonymousClass66I r21, String str, int i, int i2, boolean z) {
        String str2;
        AnonymousClass61Q r0 = this.A02;
        if (r0 == null || !r0.A0G() || this.A04 == null) {
            str2 = "Cannot start recording video, camera is not ready or has been closed.";
        } else if (this.A0D) {
            str2 = "Cannot start recording video, there is a video already being recorded";
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            C119085cr r02 = this.A04;
            C125475rJ r2 = AbstractC130685zo.A0s;
            Object A03 = r02.A03(r2);
            C119085cr r03 = this.A04;
            if (A03 == null) {
                r2 = AbstractC130685zo.A0m;
            }
            C129845yO r10 = (C129845yO) r03.A03(r2);
            if (str == null) {
                r19.A00(C12970iu.A0f("Cannot start recording video, both filePath and fileDescriptor cannot be null, one must contain a valid value"));
                return;
            }
            this.A0D = true;
            this.A0C = false;
            this.A0A.A00(new C119035cm(builder, r19, this, r21, z), "start_video_recording", new CallableC135996Kp(builder, r18, r20, this, r21, r10, str, i, i2, elapsedRealtime));
            return;
        }
        r19.A00(C12960it.A0U(str2));
    }

    public void A06(CaptureRequest.Builder builder, AbstractC129405xf r10, AnonymousClass66I r11) {
        if (!this.A0D) {
            r10.A00(C12960it.A0U("Not recording video."));
            return;
        }
        this.A0A.A00(r10, "stop_video_capture", new CallableC135956Kl(builder, this, r11, SystemClock.elapsedRealtime()));
    }
}
