package X;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.4aU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93634aU {
    public final List A00;
    public final List A01;
    public final List A02;
    public final Map A03;

    public C93634aU() {
        this(null, null, null, null);
    }

    public C93634aU(List list, List list2, List list3, Map map) {
        this.A02 = list == null ? Collections.emptyList() : list;
        this.A03 = map == null ? Collections.emptyMap() : map;
        this.A01 = list2 == null ? Collections.emptyList() : list2;
        this.A00 = list3 == null ? Collections.emptyList() : list3;
    }
}
