package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0z2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22410z2 {
    public final AbstractC15710nm A00;
    public final C15450nH A01;
    public final C20670w8 A02;
    public final C16240og A03;
    public final C15550nR A04;
    public final C22700zV A05;
    public final C15610nY A06;
    public final C20840wP A07;
    public final C14820m6 A08;
    public final C20040v7 A09;
    public final C244515o A0A;
    public final C15860o1 A0B;
    public final AbstractC14440lR A0C;
    public final C14890mD A0D;
    public final C14860mA A0E;

    public C22410z2(AbstractC15710nm r1, C15450nH r2, C20670w8 r3, C16240og r4, C15550nR r5, C22700zV r6, C15610nY r7, C20840wP r8, C14820m6 r9, C20040v7 r10, C244515o r11, C15860o1 r12, AbstractC14440lR r13, C14890mD r14, C14860mA r15) {
        this.A00 = r1;
        this.A0C = r13;
        this.A0D = r14;
        this.A0E = r15;
        this.A01 = r2;
        this.A02 = r3;
        this.A09 = r10;
        this.A04 = r5;
        this.A06 = r7;
        this.A03 = r4;
        this.A0B = r12;
        this.A0A = r11;
        this.A05 = r6;
        this.A08 = r9;
        this.A07 = r8;
    }

    public String A00() {
        StringBuilder sb = new StringBuilder();
        int i = this.A07.A01().getInt("contact_version", 0);
        StringBuilder sb2 = new StringBuilder("contact-sync-prefs/getversion=");
        sb2.append(i);
        Log.i(sb2.toString());
        sb.append(i);
        sb.append(this.A08.A00.getString("web_contact_checksum", "unset"));
        return sb.toString();
    }

    public List A01(List list) {
        String str;
        int i;
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C15370n3 r1 = (C15370n3) it.next();
            if (r1 != null) {
                if (r1.A0B(AbstractC14640lm.class) != null) {
                    boolean z = false;
                    String str2 = null;
                    if (r1.A0J()) {
                        str = r1.A0D();
                        i = r1.A06;
                        AnonymousClass1M2 A00 = this.A05.A00((UserJid) r1.A0B(UserJid.class));
                        if (A00 != null && A00.A01()) {
                            z = true;
                        }
                    } else {
                        str = (TextUtils.isEmpty(r1.A0M) || r1.A0C == null) ? null : r1.A0M;
                        i = -1;
                    }
                    Jid A0B = r1.A0B(AbstractC14640lm.class);
                    AnonymousClass009.A05(A0B);
                    AbstractC14640lm r10 = (AbstractC14640lm) A0B;
                    AbstractC15710nm r8 = this.A00;
                    C15450nH r9 = this.A01;
                    if (!TextUtils.isEmpty(r1.A0K)) {
                        str2 = r1.A0K;
                    }
                    boolean z2 = r1.A0f;
                    String str3 = r1.A0U;
                    C15860o1 r3 = this.A0B;
                    AnonymousClass009.A05(r10);
                    arrayList.add(new C41651ts(r8, r9, r10, str2, str, str3, i, r1.A00, r1.A07, z, z2, r3.A08(r10.getRawString()).A0H));
                } else if (r1.A0D != null) {
                    StringBuilder sb = new StringBuilder("Contact with jid but not chat jid ");
                    sb.append(r1);
                    Log.w(sb.toString());
                }
            }
        }
        return arrayList;
    }

    public void A02(C15370n3 r3) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(r3);
        A05(arrayList);
    }

    public void A03(AbstractC14640lm r2) {
        C15370n3 A0A;
        if (this.A0D.A02() && (A0A = this.A04.A0A(r2)) != null) {
            A02(A0A);
        }
    }

    public void A04(String str, List list) {
        this.A0C.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, list, str, 24));
    }

    public void A05(List list) {
        if (this.A0D.A02() && list.size() != 0) {
            A04(null, list);
        }
    }
}
