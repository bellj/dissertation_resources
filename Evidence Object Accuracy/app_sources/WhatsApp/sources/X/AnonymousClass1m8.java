package X;

import com.whatsapp.Conversation;

/* renamed from: X.1m8  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1m8 {
    public final Conversation A00;
    public final C21820y2 A01;
    public final boolean A02;

    public AnonymousClass1m8(Conversation conversation, C21820y2 r2, boolean z) {
        this.A00 = conversation;
        this.A02 = z;
        this.A01 = r2;
    }

    public Conversation A00() {
        if (this.A02) {
            return this.A00;
        }
        throw new AssertionError("no active session");
    }

    public boolean A01(AbstractC14640lm r3) {
        return this.A02 && C29941Vi.A00(this.A00.A2s, r3) && !this.A01.A00;
    }
}
