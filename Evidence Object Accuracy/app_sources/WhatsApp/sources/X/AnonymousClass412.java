package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.412  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass412 extends AnonymousClass4K8 {
    public final List A00;
    public final Map A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass412) {
                AnonymousClass412 r5 = (AnonymousClass412) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public AnonymousClass412(List list, Map map) {
        super(list);
        this.A00 = list;
        this.A01 = map;
    }

    public int hashCode() {
        return C12990iw.A08(this.A01, this.A00.hashCode() * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CategoryGroupsWithLoadingChildItems(catalogCategoryGroups=");
        A0k.append(this.A00);
        A0k.append(", parentCategoryToChildItemMap=");
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
