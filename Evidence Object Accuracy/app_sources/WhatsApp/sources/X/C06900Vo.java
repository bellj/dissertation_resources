package X;

import android.os.IBinder;
import android.support.v4.app.INotificationSideChannel;

/* renamed from: X.0Vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06900Vo implements INotificationSideChannel {
    public IBinder A00;

    public C06900Vo(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
