package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.ui.voip.MultiContactThumbnail;

/* renamed from: X.2w6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60152w6 extends AbstractC92184Uw {
    public C14850m9 A00;
    public final View A01;
    public final ImageView A02;
    public final ImageView A03;
    public final ImageView A04;
    public final ImageView A05;
    public final TextView A06;
    public final TextView A07;
    public final C28801Pb A08;
    public final CallsHistoryFragment A09;
    public final SelectionCheckView A0A;
    public final C15550nR A0B;
    public final AnonymousClass1J1 A0C;
    public final MultiContactThumbnail A0D;
    public final AnonymousClass19Z A0E;

    public C60152w6(View view, CallsHistoryFragment callsHistoryFragment, C15550nR r11, C15610nY r12, AnonymousClass1J1 r13, C14850m9 r14, AnonymousClass12F r15, AnonymousClass19Z r16) {
        ImageView A0K = C12970iu.A0K(view, R.id.contact_photo);
        this.A03 = A0K;
        C28801Pb r7 = new C28801Pb(view, r12, r15, (int) R.id.contact_name);
        this.A08 = r7;
        TextView A0I = C12960it.A0I(view, R.id.date_time);
        this.A07 = A0I;
        ImageView A0K2 = C12970iu.A0K(view, R.id.call_type_icon);
        this.A02 = A0K2;
        TextView A0I2 = C12960it.A0I(view, R.id.count);
        this.A06 = A0I2;
        this.A05 = C12970iu.A0K(view, R.id.voice_call);
        this.A04 = C12970iu.A0K(view, R.id.video_call);
        SelectionCheckView selectionCheckView = (SelectionCheckView) AnonymousClass028.A0D(view, R.id.selection_check);
        this.A0A = selectionCheckView;
        this.A01 = AnonymousClass028.A0D(view, R.id.call_row_container);
        MultiContactThumbnail multiContactThumbnail = (MultiContactThumbnail) AnonymousClass028.A0D(view, R.id.multi_contact_photo);
        this.A0D = multiContactThumbnail;
        AnonymousClass028.A0a(multiContactThumbnail, 2);
        C27531Hw.A06(r7.A01);
        this.A00 = r14;
        this.A0E = r16;
        this.A0B = r11;
        this.A0C = r13;
        this.A09 = callsHistoryFragment;
        A0K.setVisibility(0);
        C12980iv.A1C(multiContactThumbnail, A0I, A0I2, 8);
        A0K2.setVisibility(8);
        selectionCheckView.setVisibility(8);
    }

    @Override // X.AbstractC92184Uw
    public void A00(int i) {
        C15370n3 A0B = this.A0B.A0B(((C1100954f) super.A00).A00);
        AnonymousClass1J1 r0 = this.A0C;
        ImageView imageView = this.A03;
        r0.A06(imageView, A0B);
        CallsHistoryFragment callsHistoryFragment = this.A09;
        imageView.setOnClickListener(new View$OnClickListenerC55912jq(super.A00, this, callsHistoryFragment, this.A00));
        imageView.setOnLongClickListener(null);
        C28801Pb r2 = this.A08;
        r2.A07(A0B, callsHistoryFragment.A0e, -1);
        r2.A01.setSingleLine(true);
        C12960it.A13(this.A05, this, A0B, 22);
        C12960it.A13(this.A04, this, A0B, 21);
    }
}
