package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreTabFragment;

/* renamed from: X.2hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55132hq extends AnonymousClass03U {
    public C54472gm A00;
    public final View A01;
    public final View A02;
    public final View A03;
    public final View A04;
    public final ImageView A05;
    public final ImageView A06;
    public final ImageView A07;
    public final ImageView A08;
    public final ProgressBar A09;
    public final ProgressBar A0A;
    public final TextView A0B;
    public final TextView A0C;
    public final TextView A0D;
    public final TextView A0E;
    public final GridLayoutManager A0F;
    public final RecyclerView A0G;
    public final CircularProgressBar A0H;
    public final /* synthetic */ StickerStoreTabFragment A0I;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55132hq(View view, StickerStoreTabFragment stickerStoreTabFragment) {
        super(view);
        this.A0I = stickerStoreTabFragment;
        this.A02 = view;
        this.A0D = C12960it.A0J(view, R.id.sticker_pack_title);
        this.A0B = C12960it.A0J(view, R.id.sticker_pack_author);
        this.A0C = C12960it.A0J(view, R.id.sticker_pack_filesize);
        this.A06 = C12970iu.A0L(view, R.id.button_one);
        this.A07 = C12970iu.A0L(view, R.id.button_two);
        this.A08 = C12970iu.A0K(view, R.id.sticker_pack_avatar_info_button);
        this.A09 = (ProgressBar) view.findViewById(R.id.pack_download_progress);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.sticker_row_recycler);
        this.A0G = recyclerView;
        view.getContext();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(stickerStoreTabFragment.A00);
        this.A0F = gridLayoutManager;
        gridLayoutManager.A1Q(1);
        recyclerView.setLayoutManager(gridLayoutManager);
        this.A01 = view.findViewById(R.id.bullet_file_size);
        this.A04 = view.findViewById(R.id.sticker_update_button);
        TextView A0J = C12960it.A0J(view, R.id.sticker_update_text);
        this.A0E = A0J;
        this.A0A = (ProgressBar) view.findViewById(R.id.pack_update_progress);
        this.A0H = (CircularProgressBar) view.findViewById(R.id.sticker_row_loading);
        this.A03 = view.findViewById(R.id.new_pack_badge);
        this.A05 = C12970iu.A0L(view, R.id.sticker_animationlist);
        C27531Hw.A06(A0J);
    }
}
