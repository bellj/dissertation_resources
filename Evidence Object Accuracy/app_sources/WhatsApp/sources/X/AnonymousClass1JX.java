package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.1JX  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1JX {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05 = -1;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public AbstractC14640lm A0B;
    public AbstractC14640lm A0C;
    public UserJid A0D;
    public UserJid A0E;
    public String A0F;
    public String A0G;
    public String A0H = null;
    public String A0I;
    public String A0J;
    public String A0K;
    public List A0L;
    public List A0M;
    public List A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public byte[] A0T;
    public byte[] A0U;
    public final AbstractC15710nm A0V;
    public final C15450nH A0W;
    public final AbstractC15340mz A0X;
    public final boolean A0Y;

    public AnonymousClass1JX(AbstractC15710nm r2, C15450nH r3, AbstractC15340mz r4, boolean z) {
        this.A0V = r2;
        this.A0W = r3;
        this.A0X = r4;
        this.A0Y = z;
    }

    public void A00() {
        synchronized (this) {
            if (this.A0T == null) {
                byte[] A01 = A01();
                synchronized (this) {
                    this.A0T = A01;
                }
            }
        }
    }

    public final byte[] A01() {
        AnonymousClass1G7 A02 = A02();
        byte[] A022 = A02.A02().A02();
        long A023 = ((long) this.A0W.A02(AbstractC15460nI.A2H)) * 1024;
        if (A023 <= 0 || ((long) A022.length) <= A023) {
            return A022;
        }
        A02.A03();
        AnonymousClass1G6 r1 = (AnonymousClass1G6) A02.A00;
        r1.A0L = null;
        r1.A01 &= -3;
        A02.A08(EnumC40021qv.A1u);
        return A02.A02().A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:253:0x07c3, code lost:
        if (r8 != null) goto L_0x07c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x08dc, code lost:
        if (r0 != null) goto L_0x091d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x091b, code lost:
        if (r0 != null) goto L_0x091d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x091d, code lost:
        r0 = r0.getRawString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x09ec, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0a87;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x0ac2, code lost:
        if (r8 != false) goto L_0x0b40;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x0aff, code lost:
        if (r8 != false) goto L_0x0b40;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x0b3e, code lost:
        if (r8 != false) goto L_0x0b40;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x0b40, code lost:
        r0 = "on";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x0b4a, code lost:
        r0 = "off";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x010b, code lost:
        if (r0 == null) goto L_0x0110;
     */
    /* JADX WARNING: Removed duplicated region for block: B:434:0x0da5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1G7 A02() {
        /*
        // Method dump skipped, instructions count: 3958
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1JX.A02():X.1G7");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:603:0x0b3e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r0v9. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v7. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v18. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v25. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r4v8. Raw type applied. Possible types: E, java.lang.Object */
    /* JADX DEBUG: Type inference failed for r4v10. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v54. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r3v19. Raw type applied. Possible types: E, java.lang.Object */
    /* JADX DEBUG: Type inference failed for r3v22. Raw type applied. Possible types: E, java.lang.Object */
    /* JADX DEBUG: Type inference failed for r0v65. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r3v26. Raw type applied. Possible types: E, java.lang.Object */
    /* JADX DEBUG: Type inference failed for r0v76. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v88. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v91. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v95. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v99. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v103. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v113. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v123. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v130. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v138. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v105. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r1v12. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v177. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v218. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v224. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v229. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v241. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v251. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v255. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v262. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v274. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v278. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v284. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v295. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v167. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v171. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v303. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v307. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v317. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v191. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v328. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r1v24. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r3v74. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v355. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v227. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r1v34. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v362. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r0v366. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r2v249. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r10v74. Raw type applied. Possible types: E */
    /* JADX WARN: Type inference failed for: r2v163, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r2v164, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r2v165, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x02aa, code lost:
        if (r5 != false) goto L_0x02a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:379:0x078f, code lost:
        if (r1 == 1) goto L_0x0791;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:438:0x094d, code lost:
        if (r1 == null) goto L_0x094f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:523:0x0b26, code lost:
        if (r2.equals(r3) != false) goto L_0x0b28;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC15340mz A03(X.AnonymousClass1G6 r20, X.AnonymousClass1IS r21, long r22) {
        /*
        // Method dump skipped, instructions count: 3344
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1JX.A03(X.1G6, X.1IS, long):X.0mz");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[id: ");
        sb.append(this.A0F);
        sb.append(" jid: ");
        sb.append(this.A0C);
        sb.append(" relay: ");
        sb.append(this.A0Y);
        sb.append("]");
        return sb.toString();
    }
}
