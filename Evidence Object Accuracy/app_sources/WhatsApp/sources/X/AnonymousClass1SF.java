package X;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.whatsapp.CallConfirmationFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.protocol.VoipStanzaChildNode;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1SF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SF {
    public static int A00(int i, int i2, int i3) {
        float f = (float) i3;
        int i4 = ((int) (1.402f * f)) + i;
        float f2 = (float) i2;
        int i5 = i - ((int) ((0.344f * f2) + (f * 0.714f)));
        int i6 = i + ((int) (f2 * 1.772f));
        if (i4 > 255) {
            i4 = 255;
        } else if (i4 < 0) {
            i4 = 0;
        }
        if (i5 > 255) {
            i5 = 255;
        } else if (i5 < 0) {
            i5 = 0;
        }
        if (i6 > 255) {
            i6 = 255;
        } else if (i6 < 0) {
            i6 = 0;
        }
        return i6 | -16777216 | (i4 << 16) | (i5 << 8);
    }

    public static C15370n3 A01(C15550nR r1, C21250x7 r2, C14850m9 r3, C20710wC r4, AnonymousClass1YT r5) {
        GroupJid groupJid;
        if (A0P(r3)) {
            groupJid = r5.A04;
        } else {
            groupJid = null;
        }
        return A02(r1, r2, r4, groupJid);
    }

    public static C15370n3 A02(C15550nR r4, C21250x7 r5, C20710wC r6, GroupJid groupJid) {
        C15370n3 A09;
        C15580nU r1;
        if (groupJid == null || (A09 = r4.A09(groupJid)) == null || (r1 = (C15580nU) A09.A0B(C15580nU.class)) == null || A09.A0K == null || r5.A03(r6, r1)) {
            return null;
        }
        return A09;
    }

    public static VoipStanzaChildNode A03(C15930o9 r12, VoipStanzaChildNode voipStanzaChildNode, byte b) {
        VoipStanzaChildNode.Builder builder = new VoipStanzaChildNode.Builder(voipStanzaChildNode.tag);
        builder.addAttributes(voipStanzaChildNode.getAttributesCopy());
        VoipStanzaChildNode voipStanzaChildNode2 = null;
        if (r12 != null) {
            voipStanzaChildNode2 = VoipStanzaChildNode.fromProtocolTreeNode(AnonymousClass2L8.A00(r12, null, null, null, b, false));
        }
        VoipStanzaChildNode[] childrenCopy = voipStanzaChildNode.getChildrenCopy();
        if (childrenCopy != null) {
            for (VoipStanzaChildNode voipStanzaChildNode3 : childrenCopy) {
                if ("enc".equals(voipStanzaChildNode3.tag)) {
                    voipStanzaChildNode3 = voipStanzaChildNode2;
                    if (voipStanzaChildNode2 == null) {
                    }
                }
                builder.addChild(voipStanzaChildNode3);
            }
        }
        return builder.build();
    }

    public static VoipStanzaChildNode A04(VoipStanzaChildNode voipStanzaChildNode) {
        VoipStanzaChildNode[] childrenCopy = voipStanzaChildNode.getChildrenCopy();
        if (childrenCopy != null) {
            for (VoipStanzaChildNode voipStanzaChildNode2 : childrenCopy) {
                if ("enc".equals(voipStanzaChildNode2.tag)) {
                    return voipStanzaChildNode2;
                }
            }
        }
        return null;
    }

    public static VoipStanzaChildNode A05(VoipStanzaChildNode voipStanzaChildNode, Map map, Set set) {
        ArrayList arrayList;
        VoipStanzaChildNode[] voipStanzaChildNodeArr = null;
        if (map == null) {
            AnonymousClass009.A0A("no destination jids", !set.isEmpty());
            arrayList = new ArrayList(set);
        } else {
            AnonymousClass009.A0A("some device are not encrypted!", map.keySet().equals(set));
            arrayList = null;
        }
        List A05 = AnonymousClass2L8.A05(null, null, null, arrayList, Collections.emptyMap(), null, map, 0, false, false);
        if (!A05.isEmpty()) {
            voipStanzaChildNodeArr = new VoipStanzaChildNode[A05.size()];
            for (int i = 0; i < A05.size(); i++) {
                voipStanzaChildNodeArr[i] = VoipStanzaChildNode.fromProtocolTreeNode((AnonymousClass1V8) A05.get(i));
            }
        }
        return A06(voipStanzaChildNode, voipStanzaChildNodeArr);
    }

    public static VoipStanzaChildNode A06(VoipStanzaChildNode voipStanzaChildNode, VoipStanzaChildNode[] voipStanzaChildNodeArr) {
        VoipStanzaChildNode.Builder builder = new VoipStanzaChildNode.Builder(voipStanzaChildNode.tag);
        builder.addAttributes(voipStanzaChildNode.getAttributesCopy());
        VoipStanzaChildNode[] childrenCopy = voipStanzaChildNode.getChildrenCopy();
        if (childrenCopy != null) {
            for (VoipStanzaChildNode voipStanzaChildNode2 : childrenCopy) {
                if ("destination".equals(voipStanzaChildNode2.tag)) {
                    if (voipStanzaChildNodeArr != null) {
                        VoipStanzaChildNode.Builder builder2 = new VoipStanzaChildNode.Builder("destination");
                        builder2.addChildren(voipStanzaChildNodeArr);
                        voipStanzaChildNode2 = builder2.build();
                    }
                }
                builder.addChild(voipStanzaChildNode2);
            }
        }
        return builder.build();
    }

    public static File A07(Context context) {
        File file = new File(context.getCacheDir(), "voip_time_series");
        if (!file.exists() || !file.isDirectory()) {
            if (file.exists()) {
                file.delete();
            }
            if (!file.mkdirs()) {
                StringBuilder sb = new StringBuilder("VoipUtil failed to create time series directory: ");
                sb.append(file.getAbsolutePath());
                Log.e(sb.toString());
                return null;
            }
        }
        return file;
    }

    public static Byte A08(VoipStanzaChildNode voipStanzaChildNode) {
        AnonymousClass1W9[] attributesCopy = voipStanzaChildNode.getAttributesCopy();
        byte b = 0;
        if (attributesCopy != null) {
            int length = attributesCopy.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                AnonymousClass1W9 r2 = attributesCopy[i];
                if ("count".equals(r2.A02)) {
                    try {
                        b = Byte.parseByte(r2.A03);
                        break;
                    } catch (NumberFormatException unused) {
                        return null;
                    }
                } else {
                    i++;
                }
            }
        }
        return Byte.valueOf(b);
    }

    public static String A09(C15550nR r1, C15610nY r2, C21250x7 r3, C20710wC r4, GroupJid groupJid) {
        C15370n3 A02 = A02(r1, r3, r4, groupJid);
        if (A02 != null) {
            return r2.A0A(A02, -1);
        }
        return null;
    }

    public static String A0A(String str) {
        return str.startsWith("call:") ? str.replaceFirst("call:", "") : str;
    }

    public static String A0B(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("call:");
        sb.append(str);
        return sb.toString();
    }

    public static List A0C(C15570nT r3, C15550nR r4, C15600nX r5, C15370n3 r6) {
        GroupJid groupJid = (GroupJid) r6.A0B(C15580nU.class);
        ArrayList arrayList = new ArrayList();
        if (groupJid != null) {
            for (AbstractC14640lm r0 : A0D(r3, r5, groupJid)) {
                arrayList.add(r4.A0B(r0));
            }
        } else {
            arrayList.add(r6);
        }
        return arrayList;
    }

    public static List A0D(C15570nT r2, C15600nX r3, GroupJid groupJid) {
        ArrayList arrayList = new ArrayList(new HashSet(r3.A02(groupJid).A06().A00));
        r2.A08();
        arrayList.remove(r2.A05);
        return arrayList;
    }

    public static void A0E(ActivityC000900k r6, C15550nR r7, GroupJid groupJid, List list, List list2, int i, boolean z) {
        Intent className;
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            UserJid userJid = (UserJid) it.next();
            if (r7.A0a(userJid) || !z) {
                arrayList.add(userJid);
            }
        }
        int i2 = Build.VERSION.SDK_INT;
        int size = list.size() - arrayList.size();
        if (i2 >= 21) {
            Integer valueOf = Integer.valueOf(i);
            AnonymousClass009.A0A("List must be non empty", !arrayList.isEmpty());
            className = new Intent();
            className.setClassName(r6.getPackageName(), "com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet");
            className.putStringArrayListExtra("jids", C15380n4.A06(arrayList));
            if (list2 != null && !list2.isEmpty()) {
                className.putStringArrayListExtra("selected", C15380n4.A06(list2));
            }
            if (groupJid != null) {
                className.putExtra("source_group_jid", groupJid);
            }
            className.putExtra("hidden_jids", size);
            className.putExtra("call_from_ui", valueOf);
        } else {
            className = new Intent().setClassName(r6.getPackageName(), "com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker");
            className.putStringArrayListExtra("jids", C15380n4.A06(arrayList));
            if (groupJid != null) {
                className.putExtra("source_group_jid", groupJid);
            }
            className.putExtra("hidden_jids", size);
            if (list2 != null && !list2.isEmpty()) {
                className.putStringArrayListExtra("selected", C15380n4.A06(list2));
            }
            className.putExtra("call_from_ui", i);
        }
        r6.startActivity(className);
        r6.overridePendingTransition(0, 0);
    }

    public static void A0F(ActivityC000900k r7, C15550nR r8, AnonymousClass1YT r9, int i) {
        List<AnonymousClass1YV> A04 = r9.A04();
        ArrayList arrayList = new ArrayList(A04.size());
        for (AnonymousClass1YV r0 : A04) {
            UserJid of = UserJid.of(r0.A02);
            if (of != null) {
                arrayList.add(of);
            }
        }
        ArrayList arrayList2 = null;
        if (arrayList.size() <= 8) {
            arrayList2 = new ArrayList(arrayList);
        }
        A0E(r7, r8, null, arrayList, arrayList2, i, false);
    }

    public static void A0G(ActivityC13810kN r5, C15370n3 r6, Integer num, boolean z) {
        CallConfirmationFragment callConfirmationFragment = new CallConfirmationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jid", C15380n4.A03(r6.A0B(AbstractC14640lm.class)));
        bundle.putBoolean("is_video_call", z);
        bundle.putInt("call_from_ui", num.intValue());
        callConfirmationFragment.A0U(bundle);
        StringBuilder sb = new StringBuilder("showCallConfirmationDialog groupJid: ");
        sb.append(r6.A0B(AbstractC14640lm.class));
        Log.i(sb.toString());
        r5.Adm(callConfirmationFragment);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0074, code lost:
        if ("video".equals(r7) != false) goto L_0x0076;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0H(android.net.Uri r12, X.ActivityC13810kN r13, X.C14900mE r14, X.C21740xu r15, X.C14850m9 r16, X.AnonymousClass19Z r17) {
        /*
            r1 = r16
            boolean r0 = A0O(r1)
            r4 = 1
            r5 = 0
            r9 = r13
            if (r0 == 0) goto L_0x009a
            java.util.List r8 = r12.getPathSegments()
            int r0 = r8.size()
            r6 = 0
            if (r0 <= 0) goto L_0x0098
            java.lang.Object r7 = r8.get(r5)
        L_0x001a:
            int r0 = r8.size()
            if (r0 <= r4) goto L_0x0026
            java.lang.Object r6 = r8.get(r4)
            java.lang.String r6 = (java.lang.String) r6
        L_0x0026:
            java.lang.String r1 = r12.getScheme()
            java.lang.String r0 = "whatsapp"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x003f
            java.lang.String r1 = r12.getHost()
            java.lang.String r0 = "call"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0063
        L_0x003f:
            java.lang.String r1 = r12.getScheme()
            java.lang.String r0 = "http"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0057
            java.lang.String r1 = r12.getScheme()
            java.lang.String r0 = "https"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0095
        L_0x0057:
            java.lang.String r1 = r12.getHost()
            java.lang.String r0 = "call.whatsapp.com"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0095
        L_0x0063:
            java.lang.String r0 = "voice"
            boolean r0 = r0.equals(r7)
            java.lang.String r3 = "video"
            if (r0 != 0) goto L_0x0076
            boolean r0 = r3.equals(r7)
            r2 = 0
            if (r0 == 0) goto L_0x0077
        L_0x0076:
            r2 = 1
        L_0x0077:
            if (r6 == 0) goto L_0x0096
            int r1 = r6.length()
            r0 = 22
            if (r1 != r0) goto L_0x0096
        L_0x0081:
            int r1 = r8.size()
            r0 = 2
            if (r1 != r0) goto L_0x00b7
            if (r2 == 0) goto L_0x00b7
            if (r4 == 0) goto L_0x00b7
            boolean r0 = r3.equals(r7)
            r1 = r17
            r1.A07(r13, r6, r0)
        L_0x0095:
            return r5
        L_0x0096:
            r4 = 0
            goto L_0x0081
        L_0x0098:
            r7 = r6
            goto L_0x001a
        L_0x009a:
            r0 = 1129(0x469, float:1.582E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0095
            r11 = 2131886890(0x7f12032a, float:1.9408372E38)
            r12 = 2131886889(0x7f120329, float:1.940837E38)
            r13 = 2131892484(0x7f121904, float:1.9419718E38)
            r14 = 2131889633(0x7f120de1, float:1.9413935E38)
            X.52j r10 = new X.52j
            r10.<init>(r9, r15)
            r9.A2K(r10, r11, r12, r13, r14)
            return r4
        L_0x00b7:
            r0 = 2131888986(0x7f120b5a, float:1.9412623E38)
            r14.A07(r0, r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1SF.A0H(android.net.Uri, X.0kN, X.0mE, X.0xu, X.0m9, X.19Z):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        if (r1.contains(r6.A05) != false) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0I(X.C15570nT r6, X.C15450nH r7, X.C15550nR r8, X.C15600nX r9, X.C15370n3 r10, X.C20710wC r11, com.whatsapp.jid.GroupJid r12) {
        /*
            r5 = 0
            if (r12 == 0) goto L_0x002b
            boolean r0 = X.C21280xA.A01()
            if (r0 != 0) goto L_0x002b
            boolean r0 = r11.A0Z(r10, r12)
            if (r0 != 0) goto L_0x002b
            X.1YM r0 = r9.A02(r12)
            X.1JO r4 = r0.A06()
            java.util.Set r1 = r4.A00
            int r0 = r1.size()
            r3 = 1
            if (r0 != r3) goto L_0x002c
            r6.A08()
            X.1Ih r0 = r6.A05
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x002c
        L_0x002b:
            return r5
        L_0x002c:
            int r2 = r1.size()
            X.0oW r0 = X.AbstractC15460nI.A1O
            int r1 = r7.A02(r0)
            X.0oW r0 = X.AbstractC15460nI.A1s
            int r0 = r7.A02(r0)
            int r0 = java.lang.Math.min(r1, r0)
            if (r2 <= r0) goto L_0x0058
            java.util.Iterator r1 = r4.iterator()
        L_0x0046:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x002b
            java.lang.Object r0 = r1.next()
            com.whatsapp.jid.UserJid r0 = (com.whatsapp.jid.UserJid) r0
            boolean r0 = r8.A0a(r0)
            if (r0 == 0) goto L_0x0046
        L_0x0058:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1SF.A0I(X.0nT, X.0nH, X.0nR, X.0nX, X.0n3, X.0wC, com.whatsapp.jid.GroupJid):boolean");
    }

    public static boolean A0J(C15450nH r3, C19990v2 r4, C15600nX r5, C15370n3 r6, GroupJid groupJid) {
        return groupJid != null && !r6.A0Y && r4.A02(groupJid) != 3 && r5.A0C(groupJid) && r5.A02(groupJid).A02.size() <= Math.min(r3.A02(AbstractC15460nI.A1O), r3.A02(AbstractC15460nI.A1s));
    }

    public static boolean A0K(AnonymousClass01d r2) {
        ActivityManager A03 = r2.A03();
        return Build.VERSION.SDK_INT >= 28 && A03 != null && A03.isBackgroundRestricted();
    }

    public static boolean A0L(C14820m6 r2, C14850m9 r3) {
        if (!r3.A07(1674) || r2.A00.getBoolean("detect_device_foldable", false)) {
            return r3.A07(1268);
        }
        return false;
    }

    public static boolean A0M(C14820m6 r0, C14850m9 r1) {
        return A0L(r0, r1) || r1.A07(1025);
    }

    public static boolean A0N(C14820m6 r0, C14850m9 r1) {
        return A0L(r0, r1) || r1.A07(742);
    }

    public static boolean A0O(C14850m9 r1) {
        return r1.A02(1372) >= 1;
    }

    public static boolean A0P(C14850m9 r2) {
        return r2.A02(643) >= 3;
    }

    public static boolean A0Q(GroupJid groupJid, CallInfo callInfo) {
        if (groupJid == null) {
            return false;
        }
        if (!C21280xA.A01() || (callInfo != null && callInfo.callState == Voip.CallState.ACTIVE && groupJid.equals(callInfo.groupJid))) {
            return true;
        }
        return false;
    }

    public static int[] A0R(byte[] bArr, int i, int i2, int i3) {
        try {
            if (i == 17) {
                int i4 = i3 * i2;
                int[] iArr = new int[i4];
                int i5 = 0;
                int i6 = 0;
                while (i5 < i4) {
                    int i7 = bArr[i5] & 255;
                    int i8 = i5 + 1;
                    int i9 = bArr[i8] & 255;
                    int i10 = i2 + i5;
                    int i11 = bArr[i10] & 255;
                    int i12 = i10 + 1;
                    int i13 = bArr[i12] & 255;
                    int i14 = i4 + i6;
                    int i15 = bArr[i14] & 255;
                    int i16 = (bArr[i14 + 1] & 255) - 128;
                    int i17 = i15 - 128;
                    iArr[i5] = A00(i7, i16, i17);
                    iArr[i8] = A00(i9, i16, i17);
                    iArr[i10] = A00(i11, i16, i17);
                    iArr[i12] = A00(i13, i16, i17);
                    if (i5 != 0 && (i5 + 2) % i2 == 0) {
                        i5 = i10;
                    }
                    i5 += 2;
                    i6 += 2;
                }
                return iArr;
            } else if (i == 35) {
                int i18 = ((i2 * 3) * i3) / 2;
                int length = bArr.length;
                if (length == i18) {
                    return A0S(bArr, i2, i3, true);
                }
                StringBuilder sb = new StringBuilder();
                sb.append("convertYUV420toARGB8888 YUV_420_888 expected length ");
                sb.append(i18);
                sb.append(" got ");
                sb.append(length);
                Log.e(sb.toString());
                return null;
            } else if (i == 842094169) {
                return A0S(bArr, i2, i3, false);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("convertYUV420toARGB8888 supported format ");
                sb2.append(i);
                Log.i(sb2.toString());
                return null;
            }
        } catch (OutOfMemoryError e) {
            StringBuilder sb3 = new StringBuilder("convertYUV420toARGB8888 OOM when convert data with format = ");
            sb3.append(i);
            sb3.append(" width = ");
            sb3.append(i2);
            sb3.append("height = ");
            sb3.append(i3);
            Log.i(sb3.toString(), e);
            return null;
        }
    }

    public static int[] A0S(byte[] bArr, int i, int i2, boolean z) {
        int i3 = i * i2;
        int[] iArr = new int[i3];
        int i4 = 0;
        int i5 = 0;
        while (i4 < i3) {
            int i6 = bArr[i4] & 255;
            int i7 = i4 + 1;
            int i8 = bArr[i7] & 255;
            int i9 = i + i4;
            int i10 = bArr[i9] & 255;
            int i11 = i9 + 1;
            int i12 = bArr[i11] & 255;
            int i13 = i3 + i5;
            int i14 = bArr[i13] & 255;
            int i15 = bArr[i13 + (i3 >> 2)] & 255;
            if (!z) {
                i14 = i15;
                i15 = i14;
            }
            int i16 = i14 - 128;
            int i17 = i15 - 128;
            iArr[i4] = A00(i6, i16, i17);
            iArr[i7] = A00(i8, i16, i17);
            iArr[i9] = A00(i10, i16, i17);
            iArr[i11] = A00(i12, i16, i17);
            if (i4 != 0 && (i4 + 2) % i == 0) {
                i4 = i9;
            }
            i4 += 2;
            i5++;
        }
        return iArr;
    }
}
