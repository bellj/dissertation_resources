package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2Po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50442Po extends AnonymousClass2PA {
    public final long A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public C50442Po(Jid jid, String str, String str2, String str3, String str4, long j, long j2, boolean z) {
        super(jid, str, j2);
        this.A04 = z;
        this.A03 = str2;
        this.A01 = str3;
        this.A00 = j;
        this.A02 = str4;
    }
}
