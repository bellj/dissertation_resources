package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* renamed from: X.0Gi  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gi extends AnonymousClass0Gj {
    public static final String A00 = C06390Tk.A01("StorageNotLowTracker");

    public AnonymousClass0Gi(Context context, AbstractC11500gO r2) {
        super(context, r2);
    }

    @Override // X.AbstractC06170Sl
    public /* bridge */ /* synthetic */ Object A00() {
        Intent registerReceiver = this.A01.registerReceiver(null, A05());
        if (!(registerReceiver == null || registerReceiver.getAction() == null)) {
            String action = registerReceiver.getAction();
            if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                return Boolean.FALSE;
            }
            if (!action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                return null;
            }
        }
        return Boolean.TRUE;
    }

    @Override // X.AnonymousClass0Gj
    public IntentFilter A05() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }

    @Override // X.AnonymousClass0Gj
    public void A06(Context context, Intent intent) {
        Boolean bool;
        if (intent.getAction() != null) {
            C06390Tk.A00().A02(A00, String.format("Received %s", intent.getAction()), new Throwable[0]);
            String action = intent.getAction();
            if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                bool = Boolean.FALSE;
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                bool = Boolean.TRUE;
            } else {
                return;
            }
            A04(bool);
        }
    }
}
