package X;

/* renamed from: X.6B7  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B7 implements AbstractC136276Lx {
    public final AbstractC136276Lx A00;
    public final Long A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final byte[] A06;

    public AnonymousClass6B7(Long l, String str, String str2, String str3, String str4, byte[] bArr) {
        this.A05 = str;
        this.A02 = str2;
        this.A03 = str3;
        this.A04 = str4;
        this.A06 = bArr;
        this.A01 = l;
        switch (str3.hashCode()) {
            case 100229:
                if (str3.equals("ecc")) {
                    this.A00 = new AnonymousClass6B5(bArr);
                    return;
                }
                break;
            case 113216:
                if (str3.equals("rsa")) {
                    this.A00 = new AnonymousClass6B6(bArr);
                    return;
                }
                break;
            case 3387192:
                if (str3.equals("none")) {
                    this.A00 = new AnonymousClass6B3();
                    return;
                }
                break;
            case 110541305:
                if (str3.equals("token")) {
                    this.A00 = new AnonymousClass6B4();
                    return;
                }
                break;
        }
        throw new AssertionError(C30931Zj.A01("PaymentProviderKey", C12960it.A0d(str3, C12960it.A0k("PaymentProviderKey invalid key type: "))));
    }

    @Override // X.AbstractC136276Lx
    public byte[] A9N(byte[] bArr, byte[] bArr2) {
        return this.A00.A9N(bArr, bArr2);
    }
}
