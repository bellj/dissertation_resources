package X;

import java.util.Iterator;

/* renamed from: X.5Bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111935Bj implements Iterable, AbstractC16910px {
    public final /* synthetic */ AnonymousClass1WO A00;

    public C111935Bj(AnonymousClass1WO r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return this.A00.iterator();
    }
}
