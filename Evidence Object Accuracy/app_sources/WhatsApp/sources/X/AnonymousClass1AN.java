package X;

/* renamed from: X.1AN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AN {
    public final C14850m9 A00;
    public final C16120oU A01;

    public AnonymousClass1AN(C14850m9 r1, C16120oU r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(Boolean bool, Integer num, String str, int i, int i2) {
        C614230h r2 = new C614230h();
        r2.A00 = bool;
        r2.A02 = Integer.valueOf(i);
        r2.A03 = num;
        r2.A01 = Integer.valueOf(i2);
        if (this.A00.A07(1252)) {
            r2.A04 = str;
        }
        this.A01.A07(r2);
    }

    public void A01(String str, boolean z, boolean z2) {
        AnonymousClass43V r2 = new AnonymousClass43V();
        r2.A01 = Boolean.valueOf(z);
        r2.A00 = Boolean.valueOf(z2);
        if (this.A00.A07(1252)) {
            r2.A02 = str;
        }
        this.A01.A06(r2);
    }
}
