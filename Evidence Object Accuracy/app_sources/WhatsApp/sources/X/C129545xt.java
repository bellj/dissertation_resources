package X;

/* renamed from: X.5xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129545xt {
    public final int A00;
    public final int A01;
    public final byte[] A02;

    public C129545xt(byte[] bArr, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = bArr;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 6, insn: 0x025b: MOVE  (r21 I:??[OBJECT, ARRAY]) = (r6 I:??[OBJECT, ARRAY]), block:B:146:0x025b
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final java.lang.Object A00(
/*
[646] Method generation error in method: X.5xt.A00(java.nio.ByteOrder):java.lang.Object, file: classes4.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r24v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public String toString() {
        StringBuilder A0k = C12960it.A0k("(");
        A0k.append(AnonymousClass60M.A0E[this.A00]);
        A0k.append(", data length:");
        A0k.append(this.A02.length);
        return C12960it.A0d(")", A0k);
    }
}
