package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1kA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36591kA extends AbstractC36601kB {
    public AnonymousClass04v A00;
    public AnonymousClass04v A01;
    public List A02 = new ArrayList();
    public final ContactPickerFragment A03;
    public final AnonymousClass01H A04;

    public C36591kA(ContactPickerFragment contactPickerFragment, AnonymousClass01H r3) {
        this.A03 = contactPickerFragment;
        this.A04 = r3;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A02.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A02.get(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0578, code lost:
        if (r1 != false) goto L_0x057a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x05b8, code lost:
        if (r10 != false) goto L_0x05ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005b, code lost:
        if (r0.A1O.A07(691) == false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x067e, code lost:
        if (r4.A0g == false) goto L_0x06e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x07f9, code lost:
        if (r0.A2I != false) goto L_0x07fb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0468  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x047f  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x04ac  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x0890  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0297  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x030a  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x032c  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x03b7  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x03d0  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x03e6  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x03f8  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0444  */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r37, android.view.View r38, android.view.ViewGroup r39) {
        /*
        // Method dump skipped, instructions count: 2498
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36591kA.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
