package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* renamed from: X.5Ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC117045Ya extends AnonymousClass5WT {
    boolean A9D(Canvas canvas, Drawable drawable, int i);

    int ADX();

    int ADY();

    void Abj(int i);

    void Abp(Rect rect);

    void Abu(ColorFilter colorFilter);
}
