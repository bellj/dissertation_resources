package X;

import android.util.SparseArray;
import android.view.View;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0sc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18540sc extends C18550sd {
    public final SparseArray A00 = new SparseArray();

    public C18540sc(Set set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC17880rY r2 = (AbstractC17880rY) it.next();
            this.A00.put(r2.A00, r2);
        }
    }

    @Override // X.C18550sd
    public Object A01(C14260l7 r4, AnonymousClass28D r5) {
        int i = r5.A01;
        SparseArray sparseArray = this.A00;
        if (sparseArray.indexOfKey(i) >= 0) {
            return ((AbstractC17880rY) sparseArray.get(i)).A00(r4, r5);
        }
        return super.A01(r4, r5);
    }

    @Override // X.C18550sd
    public void A02(C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7, Object obj) {
        View view = (View) obj;
        int i = r6.A01;
        SparseArray sparseArray = this.A00;
        if (sparseArray.indexOfKey(i) >= 0) {
            ((AbstractC17880rY) sparseArray.get(i)).A01(view, r5, r6, r7);
        }
        super.A02(r5, r6, r7, obj);
    }

    @Override // X.C18550sd
    public void A03(C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7, Object obj) {
        View view = (View) obj;
        int i = r6.A01;
        SparseArray sparseArray = this.A00;
        if (sparseArray.indexOfKey(i) >= 0) {
            ((AbstractC17880rY) sparseArray.get(i)).A02(view, r5, r6, r7);
        }
        super.A03(r5, r6, r7, obj);
    }
}
