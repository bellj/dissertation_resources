package X;

import java.util.List;

/* renamed from: X.2Ms  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ms {
    public final C470328r A00;
    public final C15580nU A01;
    public final String A02;
    public final List A03;
    public final List A04;

    public AnonymousClass2Ms(C470328r r1, C15580nU r2, String str, List list, List list2) {
        this.A02 = str;
        this.A01 = r2;
        this.A04 = list;
        this.A03 = list2;
        this.A00 = r1;
    }
}
