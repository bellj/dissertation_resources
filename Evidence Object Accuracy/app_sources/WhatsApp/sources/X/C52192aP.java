package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2aP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52192aP extends ClickableSpan {
    public final /* synthetic */ AnonymousClass29U A00;

    public C52192aP(AnonymousClass29U r1) {
        this.A00 = r1;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        AnonymousClass2KB r1 = new AnonymousClass2KB();
        r1.A00 = Boolean.TRUE;
        AnonymousClass29U r3 = this.A00;
        r3.A0Q.A07(r1);
        ((ActivityC13790kL) r3).A00.A06(view.getContext(), C12970iu.A0B(((ActivityC13790kL) r3).A02.A00("https://faq.whatsapp.com/android/chats/about-whatsapp-backup-capacity-on-android")));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(this.A00.getResources().getColor(R.color.link_color));
        textPaint.setUnderlineText(false);
    }
}
