package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2bZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52722bZ extends ArrayAdapter {
    public int A00 = 0;
    public final C22680zT A01;
    public final AnonymousClass018 A02;
    public final List A03;

    public C52722bZ(Context context, C22680zT r3, AnonymousClass018 r4, List list) {
        super(context, (int) R.layout.item_select_phone_number, list);
        this.A03 = list;
        this.A02 = r4;
        this.A01 = r3;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return this.A03.size();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C90994Qa r0;
        boolean z = false;
        if (view == null) {
            view = C12960it.A0E(viewGroup).inflate(R.layout.item_select_phone_number, viewGroup, false);
            r0 = new C90994Qa();
            view.setTag(r0);
            r0.A02 = C12960it.A0J(view, R.id.title);
            r0.A01 = C12960it.A0J(view, R.id.subtitle);
            r0.A00 = (RadioButton) view.findViewById(R.id.radio);
        } else {
            r0 = (C90994Qa) view.getTag();
        }
        C65993Lw r8 = (C65993Lw) this.A03.get(i);
        String str = r8.A00;
        r0.A02.setText(AnonymousClass23M.A0B(this.A01, str, C12960it.A0d(r8.A02, C12960it.A0j(str))));
        TextView textView = r0.A01;
        Context context = viewGroup.getContext();
        Object[] A1a = C12980iv.A1a();
        C12960it.A1P(A1a, i + 1, 0);
        textView.setText(C12960it.A0X(context, r8.A01, A1a, 1, R.string.select_phone_number_subtitle));
        RadioButton radioButton = r0.A00;
        if (i == this.A00) {
            z = true;
        }
        radioButton.setChecked(z);
        return view;
    }
}
