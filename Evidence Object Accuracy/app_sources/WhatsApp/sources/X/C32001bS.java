package X;

/* renamed from: X.1bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32001bS {
    public final AnonymousClass5XJ A00;

    public C32001bS(AnonymousClass5XJ r1) {
        this.A00 = r1;
    }

    public static C32001bS A00() {
        String str;
        if ("native".equals("best")) {
            str = "NativeCurve25519Provider";
        } else if ("java".equals("best")) {
            str = "JavaCurve25519Provider";
        } else if ("j2me".equals("best")) {
            str = "J2meCurve25519Provider";
        } else if ("best".equals("best")) {
            str = "OpportunisticCurve25519Provider";
        } else {
            throw new AnonymousClass5H6();
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("org.whispersystems.curve25519.");
            sb.append(str);
            return new C32001bS((AnonymousClass5XJ) Class.forName(sb.toString()).newInstance());
        } catch (ClassNotFoundException e) {
            throw new AnonymousClass5H6(e);
        } catch (IllegalAccessException e2) {
            throw new AnonymousClass5H6(e2);
        } catch (InstantiationException e3) {
            throw new AnonymousClass5H6(e3);
        }
    }

    public C90604On A01() {
        AnonymousClass5XJ r0 = this.A00;
        byte[] AAH = r0.AAH();
        return new C90604On(r0.generatePublicKey(AAH), AAH);
    }

    public boolean A02(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        if (bArr == null || bArr.length != 32) {
            throw new IllegalArgumentException("Invalid public key!");
        } else if (bArr2 == null || bArr3 == null || bArr3.length != 64) {
            return false;
        } else {
            return this.A00.verifySignature(bArr, bArr2, bArr3);
        }
    }

    public byte[] A03(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null) {
            throw new IllegalArgumentException("Keys must not be null!");
        } else if (bArr.length == 32 && bArr2.length == 32) {
            return this.A00.calculateAgreement(bArr2, bArr);
        } else {
            throw new IllegalArgumentException("Keys must be 32 bytes!");
        }
    }
}
