package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1MX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MX extends DeviceJid implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100004lD();

    @Override // com.whatsapp.jid.Jid
    @Deprecated
    public int getAgent() {
        return 1;
    }

    @Override // com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "lid";
    }

    @Override // com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid
    public int getType() {
        return 19;
    }

    public AnonymousClass1MX(AnonymousClass1MU r1, int i) {
        super(r1, i);
    }

    public AnonymousClass1MX(Parcel parcel) {
        super(parcel);
    }

    @Override // com.whatsapp.jid.DeviceJid
    public /* bridge */ /* synthetic */ UserJid getUserJid() {
        return this.userJid;
    }
}
