package X;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.5NS  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NS extends AnonymousClass1TL implements AnonymousClass5VP {
    public static final char[] A02 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public final int A00;
    public final byte[] A01;

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return new AnonymousClass5MA(this.A01, this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r9) {
        if (!(r9 instanceof AnonymousClass5NS)) {
            return false;
        }
        AnonymousClass5NS r92 = (AnonymousClass5NS) r9;
        int i = this.A00;
        if (i != r92.A00) {
            return false;
        }
        byte[] bArr = this.A01;
        byte[] bArr2 = r92.A01;
        int length = bArr.length;
        if (length != bArr2.length) {
            return false;
        }
        int i2 = length - 1;
        if (i2 < 0) {
            return true;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (bArr[i3] != bArr2[i3]) {
                return false;
            }
        }
        int i4 = 255 << i;
        return ((byte) (bArr[i2] & i4)) == ((byte) (bArr2[i2] & i4));
    }

    public byte[] A0B() {
        byte[] bArr = this.A01;
        int length = bArr.length;
        if (length == 0) {
            return bArr;
        }
        byte[] A022 = AnonymousClass1TT.A02(bArr);
        int i = length - 1;
        A022[i] = (byte) (A022[i] & (255 << this.A00));
        return A022;
    }

    public String toString() {
        return AGy();
    }

    public AnonymousClass5NS(byte[] bArr, int i) {
        if (bArr != null) {
            if (bArr.length == 0) {
                if (i != 0) {
                    throw C12970iu.A0f("zero length data with non-zero pad bits");
                }
            } else if (i > 7 || i < 0) {
                throw C12970iu.A0f("pad bits cannot be greater than 7 or less than 0");
            }
            this.A01 = AnonymousClass1TT.A02(bArr);
            this.A00 = i;
            return;
        }
        throw C12980iv.A0n("'data' cannot be null");
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r8, boolean z) {
        int i;
        if (!(this instanceof AnonymousClass5M9)) {
            byte[] bArr = this.A01;
            int length = bArr.length;
            if (!(length == 0 || (i = this.A00) == 0)) {
                int i2 = length - 1;
                byte b = bArr[i2];
                byte b2 = (byte) ((255 << i) & b);
                if (b != b2) {
                    byte b3 = (byte) i;
                    if (z) {
                        r8.A00.write(3);
                    }
                    r8.A02(i2 + 2);
                    OutputStream outputStream = r8.A00;
                    outputStream.write(b3);
                    outputStream.write(bArr, 0, i2);
                    outputStream.write(b2);
                    return;
                }
            }
            byte b4 = (byte) this.A00;
            if (z) {
                r8.A00.write(3);
            }
            r8.A02(length + 1);
            OutputStream outputStream2 = r8.A00;
            outputStream2.write(b4);
            outputStream2.write(bArr, 0, length);
            return;
        }
        byte b5 = (byte) this.A00;
        byte[] bArr2 = this.A01;
        if (z) {
            r8.A00.write(3);
        }
        int length2 = bArr2.length;
        r8.A02(length2 + 1);
        OutputStream outputStream3 = r8.A00;
        outputStream3.write(b5);
        outputStream3.write(bArr2, 0, length2);
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        StringBuffer stringBuffer = new StringBuffer("#");
        try {
            byte[] A01 = A01();
            for (int i = 0; i != A01.length; i++) {
                char[] cArr = A02;
                stringBuffer.append(cArr[(A01[i] >>> 4) & 15]);
                stringBuffer.append(cArr[A01[i] & 15]);
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            throw new AnonymousClass4CU(C12960it.A0d(e.getMessage(), C12960it.A0k("Internal error encoding BitString: ")), e);
        }
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        byte[] bArr = this.A01;
        int length = bArr.length - 1;
        if (length < 0) {
            return 1;
        }
        byte b = bArr[length];
        int i = this.A00;
        byte b2 = (byte) (b & (255 << i));
        int i2 = length + 1;
        while (true) {
            length--;
            int i3 = ((i2 * 257) ^ b2) ^ i;
            if (length < 0) {
                return i3;
            }
            i2 = (i2 * 257) ^ bArr[0 + length];
        }
    }
}
