package X;

import java.util.List;
import org.json.JSONObject;

/* renamed from: X.4bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94414bm {
    public AbstractC454821u A00;

    public AbstractC94414bm() {
    }

    public AbstractC94414bm(AbstractC454821u r1) {
        this.A00 = r1;
    }

    public String A00() {
        if (!(this instanceof AnonymousClass45P)) {
            return !(this instanceof AnonymousClass45O) ? "undo_add_shape" : "undo_change_z_order";
        }
        return "undo_delete_shape";
    }

    public void A01(List list) {
        int i;
        AbstractC454821u r0;
        if (this instanceof AnonymousClass45P) {
            AnonymousClass45P r02 = (AnonymousClass45P) this;
            i = r02.A00;
            r0 = ((AbstractC94414bm) r02).A00;
        } else if (!(this instanceof AnonymousClass45O)) {
            list.remove(this.A00);
            return;
        } else {
            AnonymousClass45O r1 = (AnonymousClass45O) this;
            list.remove(((AbstractC94414bm) r1).A00);
            i = r1.A00;
            r0 = ((AbstractC94414bm) r1).A00;
        }
        list.add(i, r0);
    }

    public void A02(JSONObject jSONObject) {
        if (this instanceof AnonymousClass45P) {
            ((AnonymousClass45P) this).A00 = jSONObject.getInt("index");
        }
    }

    public void A03(JSONObject jSONObject) {
        if (this instanceof AnonymousClass45P) {
            jSONObject.put("index", ((AnonymousClass45P) this).A00);
        }
    }
}
