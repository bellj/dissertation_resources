package X;

/* renamed from: X.3E4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E4 {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EK A01;

    public AnonymousClass3E4(AbstractC15710nm r2, AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "states");
        this.A01 = (AnonymousClass3EK) AnonymousClass3JT.A01(r2, r3, 20);
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E4.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3E4) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
