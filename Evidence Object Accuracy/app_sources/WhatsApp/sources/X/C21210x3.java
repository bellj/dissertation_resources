package X;

/* renamed from: X.0x3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21210x3 extends AbstractC16230of {
    public final AnonymousClass01H A00;

    public C21210x3(AnonymousClass01H r1, AnonymousClass01H r2) {
        super(r1);
        this.A00 = r2;
    }

    public final void A05(AbstractC18320sG r5, Exception exc) {
        String name = r5.getClass().getName();
        StringBuilder sb = new StringBuilder("Observer crashed: ");
        sb.append(name);
        ((AbstractC15710nm) this.A00.get()).A02("abprops-observer-crashed", name, new RuntimeException(sb.toString(), exc));
    }
}
