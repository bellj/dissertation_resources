package X;

import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.4P0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4P0 {
    public final int A00;
    public final C28741Ov A01;
    public final CopyOnWriteArrayList A02;

    public AnonymousClass4P0(C28741Ov r1, CopyOnWriteArrayList copyOnWriteArrayList, int i) {
        this.A02 = copyOnWriteArrayList;
        this.A00 = i;
        this.A01 = r1;
    }
}
