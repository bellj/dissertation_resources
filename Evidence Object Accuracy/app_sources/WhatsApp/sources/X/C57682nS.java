package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57682nS extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57682nS A0D;
    public static volatile AnonymousClass255 A0E;
    public int A00;
    public int A01;
    public int A02 = 1;
    public int A03 = 1;
    public long A04;
    public AbstractC27881Jp A05 = AbstractC27881Jp.A01;
    public C43261wh A06;
    public String A07 = "";
    public String A08 = "";
    public String A09 = "";
    public String A0A = "";
    public String A0B = "";
    public String A0C = "";

    static {
        C57682nS r0 = new C57682nS();
        A0D = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        int A02;
        C81603uH r1;
        switch (r15.ordinal()) {
            case 0:
                return A0D;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57682nS r2 = (C57682nS) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A08;
                int i2 = r2.A00;
                this.A08 = r7.Afy(str, r2.A08, A1R, C12960it.A1R(i2));
                this.A05 = r7.Afm(this.A05, r2.A05, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 4, 4);
                int i4 = this.A01;
                int i5 = r2.A00;
                this.A01 = r7.Afp(i4, r2.A01, A1V, C12960it.A1V(i5 & 4, 4));
                this.A02 = r7.Afp(this.A02, r2.A02, C12960it.A1V(i3 & 8, 8), C12960it.A1V(i5 & 8, 8));
                this.A03 = r7.Afp(this.A03, r2.A03, C12960it.A1V(i3 & 16, 16), C12960it.A1V(i5 & 16, 16));
                this.A07 = r7.Afy(this.A07, r2.A07, C12960it.A1V(i3 & 32, 32), C12960it.A1V(i5 & 32, 32));
                this.A09 = r7.Afy(this.A09, r2.A09, C12960it.A1V(i3 & 64, 64), C12960it.A1V(i5 & 64, 64));
                this.A0A = r7.Afy(this.A0A, r2.A0A, C12960it.A1V(i3 & 128, 128), C12960it.A1V(i5 & 128, 128));
                this.A0B = r7.Afy(this.A0B, r2.A0B, C12960it.A1V(i3 & 256, 256), C12960it.A1V(i5 & 256, 256));
                this.A04 = r7.Afs(this.A04, r2.A04, C12960it.A1V(i3 & 512, 512), C12960it.A1V(i5 & 512, 512));
                this.A0C = r7.Afy(this.A0C, r2.A0C, C12960it.A1V(i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), C12960it.A1V(i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
                this.A06 = (C43261wh) r7.Aft(this.A06, r2.A06);
                if (r7 == C463025i.A00) {
                    this.A00 |= r2.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        int i6 = 4;
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                String A0A = r72.A0A();
                                this.A00 |= 1;
                                this.A08 = A0A;
                                continue;
                            case 18:
                                this.A00 |= 2;
                                this.A05 = r72.A08();
                                continue;
                            case 24:
                                this.A00 |= 4;
                                this.A01 = r72.A02();
                                continue;
                            case 32:
                                A02 = r72.A02();
                                if (A02 != 1) {
                                    break;
                                } else {
                                    this.A00 |= 8;
                                    this.A02 = A02;
                                    continue;
                                }
                            case 40:
                                A02 = r72.A02();
                                if (A02 != 1) {
                                    i6 = 5;
                                    break;
                                } else {
                                    this.A00 |= 16;
                                    this.A03 = A02;
                                    continue;
                                }
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                String A0A2 = r72.A0A();
                                this.A00 |= 32;
                                this.A07 = A0A2;
                                continue;
                            case 58:
                                String A0A3 = r72.A0A();
                                this.A00 |= 64;
                                this.A09 = A0A3;
                                continue;
                            case 66:
                                String A0A4 = r72.A0A();
                                this.A00 |= 128;
                                this.A0A = A0A4;
                                continue;
                            case 74:
                                String A0A5 = r72.A0A();
                                this.A00 |= 256;
                                this.A0B = A0A5;
                                continue;
                            case 80:
                                this.A00 |= 512;
                                this.A04 = r72.A06();
                                continue;
                            case 90:
                                String A0A6 = r72.A0A();
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A0C = A0A6;
                                continue;
                            case 138:
                                if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                    r1 = (C81603uH) this.A06.A0T();
                                } else {
                                    r1 = null;
                                }
                                C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r72, r22, C43261wh.A0O);
                                this.A06 = r0;
                                if (r1 != null) {
                                    this.A06 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                                }
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                continue;
                            default:
                                if (!A0a(r72, A03)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        super.A0X(i6, A02);
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57682nS();
            case 5:
                return new C82183vD();
            case 6:
                break;
            case 7:
                if (A0E == null) {
                    synchronized (C57682nS.class) {
                        if (A0E == null) {
                            A0E = AbstractC27091Fz.A09(A0D);
                        }
                    }
                }
                return A0E;
            default:
                throw C12970iu.A0z();
        }
        return A0D;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A08, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A05, 2, i2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A03(3, this.A01);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A03(4, this.A02, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A03(5, this.A03, i2);
        }
        if ((i3 & 32) == 32) {
            i2 = AbstractC27091Fz.A04(6, this.A07, i2);
        }
        if ((this.A00 & 64) == 64) {
            i2 = AbstractC27091Fz.A04(7, this.A09, i2);
        }
        if ((this.A00 & 128) == 128) {
            i2 = AbstractC27091Fz.A04(8, this.A0A, i2);
        }
        if ((this.A00 & 256) == 256) {
            i2 = AbstractC27091Fz.A04(9, this.A0B, i2);
        }
        int i4 = this.A00;
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A05(10, this.A04);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 = AbstractC27091Fz.A04(11, this.A0C, i2);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C43261wh r0 = this.A06;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 17, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A08);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A05, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(3, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0E(5, this.A03);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(6, this.A07);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A09);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A0A);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0I(9, this.A0B);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0H(10, this.A04);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0I(11, this.A0C);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C43261wh r0 = this.A06;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
