package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.util.Log;

/* renamed from: X.151  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass151 {
    public final C14830m7 A00;
    public final C16490p7 A01;
    public final C14850m9 A02;

    public AnonymousClass151(C14830m7 r1, C16490p7 r2, C14850m9 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public void A00(AbstractC15340mz r10) {
        C14360lJ r6;
        long j = r10.A11;
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_row_id, direct_path, media_key, media_key_timestamp, enc_thumb_hash, thumb_hash, thumb_width, thumb_height, transferred, micro_thumbnail, insert_timestamp FROM mms_thumbnail_metadata WHERE message_row_id = ?", new String[]{Long.toString(j)});
            if (A09.moveToLast()) {
                r6 = new C14360lJ();
                r6.A04 = A09.getString(A09.getColumnIndexOrThrow("direct_path"));
                r6.A09 = A09.getBlob(A09.getColumnIndexOrThrow("media_key"));
                r6.A02 = A09.getLong(A09.getColumnIndexOrThrow("media_key_timestamp"));
                r6.A05 = A09.getString(A09.getColumnIndexOrThrow("enc_thumb_hash"));
                r6.A07 = A09.getString(A09.getColumnIndexOrThrow("thumb_hash"));
                r6.A01 = A09.getInt(A09.getColumnIndexOrThrow("thumb_width"));
                r6.A00 = A09.getInt(A09.getColumnIndexOrThrow("thumb_height"));
                boolean z = false;
                if (A09.getLong(A09.getColumnIndexOrThrow("transferred")) == 1) {
                    z = true;
                }
                r6.A08 = z;
                r6.A0A = A09.getBlob(A09.getColumnIndexOrThrow("micro_thumbnail"));
                A09.close();
                A01.close();
            } else {
                A09.close();
                A01.close();
                r6 = null;
            }
            r10.A0i(r6);
            if (r6 != null && C30041Vv.A0X(this.A02, r10)) {
                r6.A0B = true;
            }
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(C14360lJ r6, long j) {
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues(10);
            contentValues.put("message_row_id", Long.valueOf(j));
            C30021Vq.A04(contentValues, "direct_path", r6.A04);
            C30021Vq.A06(contentValues, "media_key", r6.A09);
            contentValues.put("media_key_timestamp", Long.valueOf(r6.A02));
            C30021Vq.A04(contentValues, "enc_thumb_hash", r6.A05);
            C30021Vq.A04(contentValues, "thumb_hash", r6.A07);
            contentValues.put("thumb_width", Integer.valueOf(r6.A01));
            contentValues.put("thumb_height", Integer.valueOf(r6.A00));
            C30021Vq.A05(contentValues, "transferred", r6.A08);
            C30021Vq.A06(contentValues, "micro_thumbnail", r6.A0A);
            contentValues.put("insert_timestamp", Long.valueOf(this.A00.A00()));
            A02.A03.A06(contentValues, "mms_thumbnail_metadata", 5);
            A02.close();
        } catch (SQLiteConstraintException e) {
            Log.e("MmsThumbnailMetadataMessageStore/insertMmsThumbnailMetadata/", e);
            throw e;
        }
    }
}
