package X;

import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.421  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass421 extends C27131Gd {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public AnonymousClass421(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        this.A00.A0h.A02();
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        this.A00.A0h.A02();
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A0h.A02();
    }
}
