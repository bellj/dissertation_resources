package X;

import android.database.Cursor;

/* renamed from: X.0v8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20050v8 {
    public final C16370ot A00;
    public final C16510p9 A01;
    public final C15240mn A02;
    public final C16490p7 A03;
    public final C21390xL A04;
    public final C18080rs A05;

    public C20050v8(C16370ot r1, C16510p9 r2, C15240mn r3, C16490p7 r4, C21390xL r5, C18080rs r6) {
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
    }

    public static String A00(AbstractC15340mz r2) {
        if (r2 instanceof AbstractC16390ow) {
            C16470p4 ABf = ((AbstractC16390ow) r2).ABf();
            if (ABf != null) {
                return ABf.A07;
            }
            return null;
        } else if (C35011h5.A04(r2)) {
            return C35011h5.A01(r2);
        } else {
            if (r2 instanceof C28861Ph) {
                return r2.A0I();
            }
            if ((r2 instanceof AnonymousClass1X7) || (r2 instanceof AnonymousClass1X3)) {
                return ((AbstractC16130oV) r2).A15();
            }
            return null;
        }
    }

    public int A01(AnonymousClass02N r7, AbstractC14640lm r8) {
        String l = Long.toString(this.A01.A02(r8));
        C16310on A01 = this.A03.get();
        try {
            Cursor A07 = A01.A03.A07(r7, "SELECT COUNT(*) AS count FROM message_link WHERE chat_row_id = ?", new String[]{l});
            if (A07.moveToFirst()) {
                int i = A07.getInt(A07.getColumnIndexOrThrow("count"));
                A07.close();
                A01.close();
                return i;
            }
            A07.close();
            A01.close();
            return 0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public int A02(AnonymousClass02N r7, AbstractC14640lm r8) {
        if (this.A04.A01("links_ready", 0) == 2) {
            return A01(r7, r8);
        }
        String rawString = r8.getRawString();
        C16310on A01 = this.A03.get();
        try {
            Cursor A07 = A01.A03.A07(r7, "SELECT COUNT(*) as count FROM messages_links WHERE key_remote_jid = ?", new String[]{rawString});
            if (A07.moveToFirst()) {
                int i = A07.getInt(A07.getColumnIndexOrThrow("count"));
                A07.close();
                A01.close();
                return i;
            }
            A07.close();
            A01.close();
            return 0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
