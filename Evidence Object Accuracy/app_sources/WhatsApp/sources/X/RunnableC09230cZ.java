package X;

import android.view.ViewGroup;

/* renamed from: X.0cZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09230cZ implements Runnable {
    public final /* synthetic */ AnonymousClass01E A00;

    public RunnableC09230cZ(AnonymousClass01E r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        ViewGroup viewGroup;
        AnonymousClass01E r2 = this.A00;
        AnonymousClass0O9 r1 = r2.A0C;
        if (r1 != null) {
            r1.A0F = false;
        }
        if (r2.A0A != null && (viewGroup = r2.A0B) != null && r2.A0H != null) {
            AbstractC06180Sm A01 = AbstractC06180Sm.A01(viewGroup);
            A01.A04();
            A01.A02();
        }
    }
}
