package X;

import android.app.Application;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2fI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53822fI extends AnonymousClass014 {
    public List A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass07E A02;
    public final Set A03;
    public final boolean A04;

    public C53822fI(Application application, AnonymousClass07E r4, C251118d r5, C30211Wn r6, List list, List list2) {
        super(application);
        HashSet A12 = C12970iu.A12();
        this.A03 = A12;
        this.A02 = r4;
        boolean A02 = r5.A02();
        this.A04 = A02;
        this.A00 = list;
        if (!A02) {
            A12.add(r6);
        } else if (list2 != null) {
            A12.addAll(list2);
        }
        Map map = r4.A02;
        List list3 = (List) map.get("saved_all_categories");
        if (list3 != null) {
            this.A00 = list3;
        }
        Collection collection = (Collection) map.get("saved_selected_categories");
        if (collection != null) {
            Set set = this.A03;
            set.clear();
            set.addAll(collection);
        }
        A04();
    }

    public final void A04() {
        AnonymousClass4Wo r0;
        AnonymousClass016 r8 = this.A01;
        List list = this.A00;
        Set set = this.A03;
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < list.size(); i++) {
            C30211Wn r2 = (C30211Wn) list.get(i);
            if (set.contains(r2)) {
                r0 = new AnonymousClass4Wo(r2, true);
            } else {
                r0 = new AnonymousClass4Wo(r2, false);
            }
            A0l.add(r0);
        }
        r8.A0A(A0l);
    }
}
