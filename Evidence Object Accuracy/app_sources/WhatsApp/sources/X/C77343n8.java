package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3n8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77343n8 extends C100594mA {
    public static final C77343n8 A0T = new C77333n7().A01();
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(33);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final SparseArray A0C;
    public final SparseBooleanArray A0D;
    public final AnonymousClass1Mr A0E;
    public final AnonymousClass1Mr A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    public final boolean A0Q;
    public final boolean A0R;
    public final boolean A0S;

    public C77343n8(Parcel parcel) {
        super(parcel);
        this.A05 = parcel.readInt();
        this.A04 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A02 = parcel.readInt();
        this.A09 = parcel.readInt();
        this.A08 = parcel.readInt();
        this.A07 = parcel.readInt();
        this.A06 = parcel.readInt();
        this.A0O = C12960it.A1S(parcel.readInt());
        this.A0K = C12960it.A1S(parcel.readInt());
        this.A0L = C12960it.A1S(parcel.readInt());
        this.A0B = parcel.readInt();
        this.A0A = parcel.readInt();
        this.A0S = C12960it.A1S(parcel.readInt());
        ArrayList A0l = C12960it.A0l();
        parcel.readList(A0l, null);
        this.A0F = AnonymousClass1Mr.copyOf(A0l);
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A0M = C12960it.A1S(parcel.readInt());
        this.A0H = C12960it.A1S(parcel.readInt());
        this.A0I = C12960it.A1S(parcel.readInt());
        this.A0G = C12960it.A1S(parcel.readInt());
        ArrayList A0l2 = C12960it.A0l();
        parcel.readList(A0l2, null);
        this.A0E = AnonymousClass1Mr.copyOf(A0l2);
        this.A0Q = C12960it.A1S(parcel.readInt());
        this.A0P = C12960it.A1S(parcel.readInt());
        this.A0N = C12960it.A1S(parcel.readInt());
        this.A0R = C12960it.A1S(parcel.readInt());
        this.A0J = C12960it.A1S(parcel.readInt());
        int readInt = parcel.readInt();
        SparseArray sparseArray = new SparseArray(readInt);
        for (int i = 0; i < readInt; i++) {
            int readInt2 = parcel.readInt();
            int readInt3 = parcel.readInt();
            HashMap hashMap = new HashMap(readInt3);
            for (int i2 = 0; i2 < readInt3; i2++) {
                hashMap.put(C12990iw.A0I(parcel, C100564m7.class), C12990iw.A0I(parcel, C100584m9.class));
            }
            sparseArray.put(readInt2, hashMap);
        }
        this.A0C = sparseArray;
        this.A0D = parcel.readSparseBooleanArray();
    }

    public C77343n8(SparseArray sparseArray, SparseBooleanArray sparseBooleanArray, AnonymousClass1Mr r5, AnonymousClass1Mr r6, AnonymousClass1Mr r7, AnonymousClass1Mr r8, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        super(r6, r8, i9);
        this.A05 = i;
        this.A04 = i2;
        this.A03 = i3;
        this.A02 = i4;
        this.A09 = 0;
        this.A08 = 0;
        this.A07 = 0;
        this.A06 = 0;
        this.A0O = z;
        this.A0K = false;
        this.A0L = z2;
        this.A0B = i5;
        this.A0A = i6;
        this.A0S = z3;
        this.A0F = r5;
        this.A01 = i7;
        this.A00 = i8;
        this.A0M = z4;
        this.A0H = false;
        this.A0I = false;
        this.A0G = false;
        this.A0E = r7;
        this.A0Q = false;
        this.A0P = false;
        this.A0N = z5;
        this.A0R = false;
        this.A0J = z6;
        this.A0C = sparseArray;
        this.A0D = sparseBooleanArray;
    }

    /* JADX WARNING: Removed duplicated region for block: B:82:0x011a  */
    @Override // X.C100594mA, java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 317
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77343n8.equals(java.lang.Object):boolean");
    }

    @Override // X.C100594mA, java.lang.Object
    public int hashCode() {
        AnonymousClass1Mr r0 = this.A0F;
        return (((((((((C12990iw.A08(this.A0E, ((((((((((((C12990iw.A08(r0, ((((((((((((((((((((((((((((super.hashCode() * 31) + this.A05) * 31) + this.A04) * 31) + this.A03) * 31) + this.A02) * 31) + this.A09) * 31) + this.A08) * 31) + this.A07) * 31) + this.A06) * 31) + (this.A0O ? 1 : 0)) * 31) + (this.A0K ? 1 : 0)) * 31) + (this.A0L ? 1 : 0)) * 31) + (this.A0S ? 1 : 0)) * 31) + this.A0B) * 31) + this.A0A) * 31) * 31) + this.A01) * 31) + this.A00) * 31) + (this.A0M ? 1 : 0)) * 31) + (this.A0H ? 1 : 0)) * 31) + (this.A0I ? 1 : 0)) * 31) + (this.A0G ? 1 : 0)) * 31) * 31) + (this.A0Q ? 1 : 0)) * 31) + (this.A0P ? 1 : 0)) * 31) + (this.A0N ? 1 : 0)) * 31) + (this.A0R ? 1 : 0)) * 31) + (this.A0J ? 1 : 0);
    }

    @Override // X.C100594mA, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A05);
        parcel.writeInt(this.A04);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A09);
        parcel.writeInt(this.A08);
        parcel.writeInt(this.A07);
        parcel.writeInt(this.A06);
        parcel.writeInt(this.A0O ? 1 : 0);
        parcel.writeInt(this.A0K ? 1 : 0);
        parcel.writeInt(this.A0L ? 1 : 0);
        parcel.writeInt(this.A0B);
        parcel.writeInt(this.A0A);
        parcel.writeInt(this.A0S ? 1 : 0);
        parcel.writeList(this.A0F);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A0M ? 1 : 0);
        parcel.writeInt(this.A0H ? 1 : 0);
        parcel.writeInt(this.A0I ? 1 : 0);
        parcel.writeInt(this.A0G ? 1 : 0);
        parcel.writeList(this.A0E);
        parcel.writeInt(this.A0Q ? 1 : 0);
        parcel.writeInt(this.A0P ? 1 : 0);
        parcel.writeInt(this.A0N ? 1 : 0);
        parcel.writeInt(this.A0R ? 1 : 0);
        parcel.writeInt(this.A0J ? 1 : 0);
        SparseArray sparseArray = this.A0C;
        int size = sparseArray.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            int keyAt = sparseArray.keyAt(i2);
            Map map = (Map) sparseArray.valueAt(i2);
            int size2 = map.size();
            parcel.writeInt(keyAt);
            parcel.writeInt(size2);
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                parcel.writeParcelable((Parcelable) A15.getKey(), 0);
                parcel.writeParcelable((Parcelable) A15.getValue(), 0);
            }
        }
        parcel.writeSparseBooleanArray(this.A0D);
    }
}
