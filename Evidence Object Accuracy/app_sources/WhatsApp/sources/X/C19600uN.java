package X;

import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0uN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19600uN {
    public final AnonymousClass018 A00;
    public final C18840t8 A01;
    public final C65963Lt A02;
    public final C19590uM A03;

    public C19600uN(AnonymousClass018 r6, C14850m9 r7, C18840t8 r8, C19590uM r9) {
        C16700pc.A0E(r7, 1);
        C16700pc.A0E(r8, 2);
        C16700pc.A0E(r6, 3);
        this.A01 = r8;
        this.A00 = r6;
        this.A03 = r9;
        this.A02 = new C65963Lt("PHOENIX", (long) r7.A02(1269), false);
    }

    public final C91064Qh A00(String str) {
        C16700pc.A0E(str, 0);
        C18840t8 r3 = this.A01;
        String str2 = this.A02.A01;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("BloksLayoutData:8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666:");
        sb.append(AnonymousClass018.A00(this.A00.A00));
        return (C91064Qh) r3.A01(str2, sb.toString());
    }

    public final void A01(AnonymousClass3D5 r10, Boolean bool, String str, String str2, Map map) {
        C19590uM r1 = this.A03;
        String obj = new JSONObject(map).toString();
        C65963Lt r2 = this.A02;
        C91074Qi r0 = new C91074Qi(r10, this, str);
        AbstractC16850pr r12 = r1.A00;
        r12.A02(r2, new C70963cA(r0), bool, str, obj, str2, r12.A00.contains(str));
    }

    public final void A02(AnonymousClass3D5 r9, String str, Map map, int i) {
        C16700pc.A0E(str, 0);
        C91064Qh A00 = A00(str);
        if (A00 == null || A00.A00 != 5) {
            String A002 = C130355zH.A00(Integer.valueOf(i));
            if (A002 != null) {
                A01(r9, Boolean.FALSE, str, A002, map);
                return;
            }
            return;
        }
        r9.A00();
    }
}
