package X;

/* renamed from: X.4Hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88794Hd {
    public static final C78603pB A00;
    public static final C78603pB A01;
    public static final C78603pB A02;
    public static final C78603pB A03;
    public static final C78603pB A04;
    public static final C78603pB[] A05;

    static {
        C78603pB r8 = new C78603pB("name_ulr_private", 1);
        A00 = r8;
        C78603pB r7 = new C78603pB("name_sleep_segment_request", 1);
        A01 = r7;
        C78603pB r6 = new C78603pB("support_context_feature_id", 1);
        A02 = r6;
        C78603pB r3 = new C78603pB("get_current_location", 1);
        A03 = r3;
        C78603pB r2 = new C78603pB("get_last_activity_feature_id", 1);
        A04 = r2;
        C78603pB[] r1 = new C78603pB[5];
        C72453ed.A1F(r8, r7, r6, r3, r1);
        r1[4] = r2;
        A05 = r1;
    }
}
