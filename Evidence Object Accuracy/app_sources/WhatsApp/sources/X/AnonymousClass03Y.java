package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: X.03Y  reason: invalid class name */
/* loaded from: classes.dex */
public interface AnonymousClass03Y {
    void setSupportImageTintList(ColorStateList colorStateList);

    void setSupportImageTintMode(PorterDuff.Mode mode);
}
