package X;

import android.database.Cursor;

/* renamed from: X.0Zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07700Zw implements AbstractC12460hx {
    public final AbstractC02880Ff A00;
    public final AnonymousClass0QN A01;

    public C07700Zw(AnonymousClass0QN r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0FN(r2, this);
    }

    @Override // X.AbstractC12460hx
    public Long AE0(String str) {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        AnonymousClass0QN r0 = this.A01;
        r0.A02();
        Long l = null;
        Cursor A002 = AnonymousClass0LC.A00(r0, A00, false);
        try {
            if (A002.moveToFirst() && !A002.isNull(0)) {
                l = Long.valueOf(A002.getLong(0));
            }
            return l;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12460hx
    public void AJ0(AnonymousClass0PB r3) {
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        r1.A03();
        try {
            this.A00.A04(r3);
            r1.A05();
        } finally {
            r1.A04();
        }
    }
}
