package X;

import java.io.File;

/* renamed from: X.4Rq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91414Rq {
    public final long A00;
    public final File A01;
    public final String A02;
    public final byte[] A03;

    public C91414Rq(File file, String str, byte[] bArr, long j) {
        this.A01 = file;
        this.A02 = str;
        this.A00 = j;
        this.A03 = bArr;
    }
}
