package X;

/* renamed from: X.0kA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13690kA {
    public final C13600jz A00 = new C13600jz();

    public void A00(Exception exc) {
        C13600jz r2 = this.A00;
        C13020j0.A02(exc, "Exception must not be null");
        synchronized (r2.A04) {
            if (!r2.A02) {
                r2.A02 = true;
                r2.A00 = exc;
                r2.A03.A01(r2);
            }
        }
    }

    public void A01(Object obj) {
        this.A00.A08(obj);
    }
}
