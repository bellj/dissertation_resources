package X;

import android.os.Binder;
import android.os.Process;
import java.util.concurrent.Callable;

/* renamed from: X.0eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10420eb implements Callable {
    public final /* synthetic */ RunnableC10240eG A00;

    public CallableC10420eb(RunnableC10240eG r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public Object call() {
        RunnableC10240eG r4 = this.A00;
        r4.A04.set(true);
        Object obj = null;
        try {
            Process.setThreadPriority(10);
            try {
                obj = r4.A06.A06();
            } catch (AnonymousClass04U e) {
                if (!r4.A03.get()) {
                    throw e;
                }
            }
            Binder.flushPendingCommands();
            return obj;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
