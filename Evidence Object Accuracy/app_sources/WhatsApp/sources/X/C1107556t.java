package X;

import com.whatsapp.account.delete.DeleteAccountConfirmation;
import com.whatsapp.util.Log;

/* renamed from: X.56t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1107556t implements AbstractC116695Wl {
    public final /* synthetic */ DeleteAccountConfirmation A00;

    public C1107556t(DeleteAccountConfirmation deleteAccountConfirmation) {
        this.A00 = deleteAccountConfirmation;
    }

    @Override // X.AbstractC116695Wl
    public void AOy() {
        Log.e("Failed to delete shops user.");
    }

    @Override // X.AbstractC116695Wl
    public void APp(Exception exc) {
        Log.e("Failed to delete shops user.");
    }

    @Override // X.AbstractC116695Wl
    public void AWx(C64063Ec r2) {
        Log.e("Shops user deleted successfully.");
    }
}
