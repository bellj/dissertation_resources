package X;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Display;
import android.view.DisplayCutout;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.BoundedLinearLayout;
import com.whatsapp.R;
import com.whatsapp.components.CircularRevealView;
import com.whatsapp.conversation.ConversationAttachmentContentView;
import java.lang.ref.WeakReference;

/* renamed from: X.1vP */
/* loaded from: classes2.dex */
public class C42561vP extends PopupWindow {
    public int A00;
    public int A01;
    public ViewTreeObserver.OnGlobalLayoutListener A02 = new ViewTreeObserver$OnGlobalLayoutListenerC42551vO(this);
    public boolean A03;
    public boolean A04;
    public final int A05;
    public final View A06;
    public final FrameLayout.LayoutParams A07;
    public final FrameLayout A08;
    public final BoundedLinearLayout A09;
    public final C14900mE A0A;
    public final CircularRevealView A0B;
    public final ConversationAttachmentContentView A0C;
    public final AnonymousClass01d A0D;
    public final C252718t A0E;
    public final WeakReference A0F;

    public C42561vP(Activity activity, View view, C14900mE r10, C42531vM r11, AnonymousClass01d r12, AbstractC14640lm r13, C252718t r14) {
        super(activity);
        this.A0A = r10;
        this.A0E = r14;
        this.A06 = view;
        this.A0F = new WeakReference(activity);
        this.A0D = r12;
        C52932bv r6 = new C52932bv(activity, activity, this);
        this.A08 = r6;
        r6.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        activity.getLayoutInflater().inflate(R.layout.attachment_picker, (ViewGroup) r6, true);
        CircularRevealView circularRevealView = (CircularRevealView) AnonymousClass028.A0D(r6, R.id.paper_clip_layout);
        this.A0B = circularRevealView;
        this.A09 = (BoundedLinearLayout) AnonymousClass028.A0D(r6, R.id.content);
        this.A07 = (FrameLayout.LayoutParams) circularRevealView.getLayoutParams();
        this.A05 = circularRevealView.getResources().getDimensionPixelSize(R.dimen.attach_popup_bottom_padding_old);
        circularRevealView.setVisibility(0);
        setContentView(r6);
        setBackgroundDrawable(new BitmapDrawable());
        setWidth(-1);
        setHeight(-2);
        setAnimationStyle(0);
        setTouchable(true);
        setFocusable(true);
        setOutsideTouchable(true);
        setInputMethodMode(2);
        setTouchInterceptor(new View.OnTouchListener(activity, this, r12) { // from class: X.3NA
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ C42561vP A01;
            public final /* synthetic */ AnonymousClass01d A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                C42561vP r3 = this.A01;
                if (motionEvent.getAction() != 4) {
                    if (motionEvent.getAction() != 0) {
                        return false;
                    }
                    float y = motionEvent.getY();
                    FrameLayout frameLayout = r3.A08;
                    if (y >= ((float) frameLayout.getTop()) && motionEvent.getY() <= ((float) frameLayout.getBottom())) {
                        float x = motionEvent.getX();
                        CircularRevealView circularRevealView2 = r3.A0B;
                        if (x >= ((float) circularRevealView2.getLeft()) && motionEvent.getX() <= ((float) circularRevealView2.getRight())) {
                            return false;
                        }
                    }
                }
                r3.dismiss();
                return true;
            }
        });
        ConversationAttachmentContentView conversationAttachmentContentView = (ConversationAttachmentContentView) AnonymousClass028.A0D(circularRevealView, R.id.conversation_content_view);
        this.A0C = conversationAttachmentContentView;
        conversationAttachmentContentView.A0K = r13;
        conversationAttachmentContentView.A0D = r11;
        conversationAttachmentContentView.A04();
    }

    public static final AnimationSet A00(int i, boolean z, boolean z2) {
        float f = 0.0f;
        float f2 = 1.0f;
        float f3 = 0.0f;
        if (z2) {
            f2 = 0.0f;
            f3 = 1.0f;
        }
        float f4 = (float) i;
        float f5 = 0.0f;
        if (z) {
            f5 = 1.0f;
        }
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 0, f4, 1, f5);
        if (!z2) {
            f = 1.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, 1.0f - f);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setDuration(300);
        return animationSet;
    }

    public static /* synthetic */ void A01(C42561vP r2) {
        r2.A0B.A02(false);
        super.dismiss();
    }

    public final int A03(Activity activity) {
        this.A08.measure(0, 0);
        View view = this.A06;
        view.measure(0, 0);
        if (!C252718t.A00(view) || (Build.VERSION.SDK_INT >= 24 && activity.isInMultiWindowMode())) {
            return -(view.getMeasuredHeight() + this.A0C.A01(view));
        }
        return 0;
    }

    public void A04() {
        A05();
        this.A0B.A02(false);
        super.dismiss();
    }

    public final void A05() {
        if (this.A04) {
            this.A04 = false;
            this.A08.getViewTreeObserver().removeOnGlobalLayoutListener(this.A02);
        }
    }

    public final void A06() {
        int[] iArr = new int[2];
        this.A06.getLocationOnScreen(iArr);
        int[] iArr2 = new int[2];
        CircularRevealView circularRevealView = this.A0B;
        circularRevealView.getLocationOnScreen(iArr2);
        int i = 0;
        int i2 = (iArr[0] + this.A00) - iArr2[0];
        if (this.A03) {
            i = this.A08.getMeasuredHeight();
        }
        circularRevealView.A01 = i2;
        circularRevealView.A02 = i;
    }

    public void A07(Activity activity) {
        boolean z;
        Resources resources = activity.getResources();
        int[] iArr = new int[2];
        View view = this.A06;
        view.getLocationOnScreen(iArr);
        int height = iArr[1] + view.getHeight();
        WindowManager A0O = this.A0D.A0O();
        Point point = new Point();
        A0O.getDefaultDisplay().getSize(point);
        if (point.y - height < activity.getResources().getDimensionPixelSize(R.dimen.attach_popup_max_anchor) || (Build.VERSION.SDK_INT >= 24 && activity.isInMultiWindowMode())) {
            z = true;
        } else {
            z = false;
        }
        this.A00 = view.getWidth() / 2;
        if (z) {
            A08(activity, 300, A03(activity), true);
        } else {
            A08(activity, 300, resources.getDimensionPixelSize(R.dimen.attach_popup_top_margin), false);
        }
    }

    public final void A08(Activity activity, int i, int i2, boolean z) {
        int i3;
        ConversationAttachmentContentView conversationAttachmentContentView;
        CircularRevealView circularRevealView;
        FrameLayout.LayoutParams layoutParams;
        WindowInsets rootWindowInsets;
        DisplayCutout displayCutout;
        this.A03 = z;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int[] iArr = new int[2];
        View view = this.A06;
        view.getLocationOnScreen(iArr);
        int height = iArr[1] + view.getHeight();
        this.A01 = iArr[0];
        int i4 = Build.VERSION.SDK_INT;
        if (i4 < 28 || (rootWindowInsets = view.getRootWindowInsets()) == null || (displayCutout = rootWindowInsets.getDisplayCutout()) == null) {
            i3 = 0;
        } else {
            i3 = displayCutout.getSafeInsetTop();
        }
        BoundedLinearLayout boundedLinearLayout = this.A09;
        if (!z) {
            boundedLinearLayout.A00 = Integer.MAX_VALUE;
            boundedLinearLayout.A01 = Integer.MAX_VALUE;
            circularRevealView = this.A0B;
            circularRevealView.setPadding(circularRevealView.getPaddingLeft(), circularRevealView.getPaddingTop(), circularRevealView.getPaddingRight(), 0);
            conversationAttachmentContentView = this.A0C;
            conversationAttachmentContentView.A07 = 0;
            layoutParams = this.A07;
            layoutParams.height = -1;
            int i5 = height + i2;
            boundedLinearLayout.getLayoutParams().height = (point.y + i3) - i5;
            showAtLocation(view, 8388661, 0, i5);
        } else {
            conversationAttachmentContentView = this.A0C;
            boundedLinearLayout.A00 = conversationAttachmentContentView.A01(view);
            boundedLinearLayout.A01 = conversationAttachmentContentView.A00(view);
            circularRevealView = this.A0B;
            circularRevealView.setPadding(circularRevealView.getPaddingLeft(), circularRevealView.getPaddingTop(), circularRevealView.getPaddingRight(), this.A05);
            boundedLinearLayout.getLayoutParams().height = -2;
            layoutParams = this.A07;
            layoutParams.height = -2;
            conversationAttachmentContentView.A07 = conversationAttachmentContentView.getResources().getDimensionPixelOffset(R.dimen.attach_popup_corner_radius);
            if (i4 >= 19) {
                showAsDropDown(view, 0, i2, 8388661);
            } else {
                showAsDropDown(view, 0, i2);
            }
        }
        circularRevealView.forceLayout();
        circularRevealView.A04 = i;
        if (z) {
            boundedLinearLayout.measure(0, 0);
            int measuredWidth = boundedLinearLayout.getMeasuredWidth();
            int dimensionPixelSize = (int) (((float) point.x) - ((((float) ((activity.getResources().getDimensionPixelSize(R.dimen.input_layout_buttons_margin) + activity.getResources().getDimensionPixelSize(R.dimen.input_layout_buttons_size)) + activity.getResources().getDimensionPixelSize(R.dimen.input_layout_buttons_positioning_margin))) + activity.getResources().getDimension(R.dimen.input_layout_buttons_density)) * 2.0f));
            if ((defaultDisplay.getRotation() == 1 || defaultDisplay.getRotation() == 3) && dimensionPixelSize > measuredWidth) {
                layoutParams.width = dimensionPixelSize;
                layoutParams.rightMargin = 0;
                layoutParams.leftMargin = 0;
            } else {
                layoutParams.width = -1;
                int dimensionPixelSize2 = activity.getResources().getDimensionPixelSize(R.dimen.attach_popup_horizontal_margin);
                layoutParams.rightMargin = dimensionPixelSize2;
                layoutParams.leftMargin = dimensionPixelSize2;
            }
            boundedLinearLayout.setBackground(AnonymousClass00T.A04(activity, R.drawable.ib_attach_panel));
        } else {
            layoutParams.width = -1;
            circularRevealView.setBackgroundColor(AnonymousClass00T.A00(activity, R.color.attachmentPickerBackgroundColor));
            circularRevealView.A03 = AnonymousClass00T.A00(activity, R.color.attach_popup_background);
        }
        circularRevealView.setVisibility(0);
        circularRevealView.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NZ(this, i, z));
        if (i > 0) {
            conversationAttachmentContentView.A07(i, z);
        }
    }

    @Override // android.widget.PopupWindow
    public void dismiss() {
        if (isShowing()) {
            A06();
            int[] iArr = new int[2];
            this.A06.getLocationOnScreen(iArr);
            AnimationSet A00 = A00(this.A00 + iArr[0], this.A03, false);
            CircularRevealView circularRevealView = this.A0B;
            circularRevealView.A04 = 300;
            circularRevealView.A0E = false;
            int i = Build.VERSION.SDK_INT;
            if (i >= 18 || !this.A03) {
                if (!circularRevealView.A0C) {
                    circularRevealView.A02(true);
                }
                if (i < 21 && !this.A03) {
                    this.A09.startAnimation(A00);
                }
            } else if (!circularRevealView.A0C) {
                circularRevealView.A01(A00);
            }
        }
        A05();
        this.A0A.A0J(new RunnableBRunnable0Shape4S0100000_I0_4(this, 39), 300);
    }
}
