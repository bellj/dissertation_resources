package X;

import com.whatsapp.chatinfo.ListChatInfo;
import java.lang.ref.WeakReference;

/* renamed from: X.2wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60342wg extends AnonymousClass2BN {
    public final WeakReference A00;

    public C60342wg(C14900mE r2, ListChatInfo listChatInfo, AnonymousClass1BB r4, C20000v3 r5, C20050v8 r6, C15660nh r7, C242114q r8, C15370n3 r9, C22710zW r10, C17070qD r11) {
        super(r2, listChatInfo, r4, r5, r6, r7, r8, r9, r10, r11);
        this.A00 = C12970iu.A10(listChatInfo);
    }
}
