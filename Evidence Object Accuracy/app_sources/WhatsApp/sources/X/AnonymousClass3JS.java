package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLException;

/* renamed from: X.3JS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JS {
    public static int A00(byte[] bArr) {
        if (bArr.length == 3) {
            return (bArr[2] & 255) | 0 | ((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8);
        }
        throw AnonymousClass1NR.A00("Invalid argument. Byte array is null or length != 3", (byte) 80);
    }

    public static int A01(byte[] bArr) {
        if (bArr == null || bArr.length != 2) {
            throw AnonymousClass1NR.A00("Invalid argument. Byte array is null or length != 2", (byte) 80);
        }
        return (bArr[1] & 255) | 0 | ((bArr[0] & 255) << 8);
    }

    public static long A02(byte[] bArr) {
        if (bArr != null && bArr.length == 4) {
            return (((long) (bArr[0] & 255)) << 24) | (((long) (bArr[1] & 255)) << 16) | (((long) (bArr[2] & 255)) << 8) | ((long) (bArr[3] & 255));
        }
        throw AnonymousClass1NR.A00("Invalid argument. byte array is null or length != 4", (byte) 80);
    }

    public static String A03(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        Formatter formatter = new Formatter();
        for (byte b : bArr) {
            Object[] A1b = C12970iu.A1b();
            A1b[0] = Byte.valueOf(b);
            formatter.format("%02x ", A1b);
        }
        return formatter.toString();
    }

    public static boolean A04(byte[] bArr, byte[] bArr2) {
        int length;
        if (bArr == null || (length = bArr.length) != bArr2.length) {
            return false;
        }
        boolean z = true;
        for (int i = 0; i < length; i++) {
            z &= C12960it.A1V(bArr[i], bArr2[i]);
        }
        return z;
    }

    public static byte[] A05(int i) {
        if (i >= 0 && i < 16777216) {
            return new byte[]{(byte) ((i >>> 16) & 255), (byte) ((i >>> 8) & 255), (byte) (i & 255)};
        }
        StringBuilder A0k = C12960it.A0k("Invalid argument. The supplied int value = ");
        A0k.append(i);
        throw AnonymousClass1NR.A00(C12960it.A0d(" does not fit in 3 bytes.", A0k), (byte) 80);
    }

    public static byte[] A06(int i) {
        if (i >= 0 && i < 65536) {
            return new byte[]{(byte) ((i >>> 8) & 255), (byte) (i & 255)};
        }
        StringBuilder A0k = C12960it.A0k("Invalid argument. The supplied int value = ");
        A0k.append(i);
        throw AnonymousClass1NR.A00(C12960it.A0d(" does not fit in 2 bytes.", A0k), (byte) 80);
    }

    public static byte[] A07(String str, String str2, int i) {
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("#");
        A0j.append(i);
        A0j.append("#");
        try {
            return C12960it.A0d(str2, A0j).getBytes(DefaultCrypto.UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new AnonymousClass1NR(new SSLException(e), (byte) 80);
        }
    }

    public static byte[] A08(String str, byte[] bArr, int i) {
        if (bArr != null) {
            int length = bArr.length;
            try {
                byte[] bytes = C12960it.A0d(str, C12960it.A0k("tls13 ")).getBytes(DefaultCrypto.UTF_8);
                int length2 = bytes.length;
                ByteBuffer allocate = ByteBuffer.allocate(length2 + 1 + 2 + length + 1);
                allocate.put(A06(i));
                short s = (short) length2;
                if (s < 0 || s >= 256) {
                    StringBuilder A0k = C12960it.A0k("Invalid argument. Short val = ");
                    A0k.append((int) s);
                    throw AnonymousClass1NR.A00(C12960it.A0d(" cannot fit in single byte", A0k), (byte) 80);
                }
                allocate.put((byte) (s & 255));
                allocate.put(bytes);
                allocate.put((byte) length);
                allocate.put(bArr);
                return allocate.array();
            } catch (UnsupportedEncodingException e) {
                throw new AnonymousClass1NR(new SSLException(e), (byte) 80);
            }
        } else {
            throw AnonymousClass1NR.A00("Context cannot be null when generating info", (byte) 80);
        }
    }

    public static byte[] A09(String str, byte[] bArr, byte[] bArr2) {
        try {
            str = str.replace("-", "");
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, C12960it.A0d(str, C12960it.A0j("Hmac")));
            Mac instance = Mac.getInstance(C12960it.A0d(str, C12960it.A0j("Hmac")));
            instance.init(secretKeySpec);
            return instance.doFinal(bArr2);
        } catch (InvalidKeyException e) {
            throw AnonymousClass1NR.A01("Invalid key", e, (byte) 80);
        } catch (NoSuchAlgorithmException e2) {
            StringBuilder A0j = C12960it.A0j("Hmac");
            A0j.append(str);
            throw AnonymousClass1NR.A01(C12960it.A0d(" not found", A0j), e2, (byte) 80);
        }
    }
}
