package X;

/* renamed from: X.0ri  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17980ri {
    public abstract AbstractC17980ri add(Object obj);

    public AbstractC17980ri addAll(Iterable iterable) {
        for (Object obj : iterable) {
            add(obj);
        }
        return this;
    }

    public static int expandedCapacity(int i, int i2) {
        if (i2 >= 0) {
            int i3 = i + (i >> 1) + 1;
            if (i3 < i2) {
                i3 = Integer.highestOneBit(i2 - 1) << 1;
            }
            if (i3 < 0) {
                return Integer.MAX_VALUE;
            }
            return i3;
        }
        throw new AssertionError("cannot store more than MAX_VALUE elements");
    }
}
