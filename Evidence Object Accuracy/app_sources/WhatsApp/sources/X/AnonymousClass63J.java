package X;

import android.media.ImageReader;

/* renamed from: X.63J  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63J implements ImageReader.OnImageAvailableListener {
    public final /* synthetic */ AnonymousClass66K A00;

    public AnonymousClass63J(AnonymousClass66K r1) {
        this.A00 = r1;
    }

    @Override // android.media.ImageReader.OnImageAvailableListener
    public void onImageAvailable(ImageReader imageReader) {
        AnonymousClass66K.A00(imageReader, this.A00);
    }
}
