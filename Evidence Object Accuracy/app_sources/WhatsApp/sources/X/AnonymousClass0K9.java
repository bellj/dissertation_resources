package X;

import android.content.res.Configuration;
import android.os.LocaleList;

/* renamed from: X.0K9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0K9 {
    public static void A00(Configuration configuration, Configuration configuration2, Configuration configuration3) {
        LocaleList locales = configuration.getLocales();
        LocaleList locales2 = configuration2.getLocales();
        if (!locales.equals(locales2)) {
            configuration3.setLocales(locales2);
            configuration3.locale = configuration2.locale;
        }
    }
}
