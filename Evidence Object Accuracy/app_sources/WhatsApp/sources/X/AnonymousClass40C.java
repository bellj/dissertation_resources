package X;

/* renamed from: X.40C  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass40C extends AnonymousClass4UW {
    public final boolean A00;

    public AnonymousClass40C() {
        this(false);
    }

    public AnonymousClass40C(boolean z) {
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass40C) && this.A00 == ((AnonymousClass40C) obj).A00);
    }

    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("HasCatalogChip(isSelected=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
