package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78303oh extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98894jQ();
    public final int A00 = 1;
    public final String A01;
    public final byte[] A02;

    public C78303oh(String str, byte[] bArr) {
        C13020j0.A01(str);
        this.A01 = str;
        C13020j0.A01(bArr);
        this.A02 = bArr;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0G(parcel, this.A02, 3, C95654e8.A0K(parcel, this.A01));
        C95654e8.A06(parcel, A00);
    }
}
