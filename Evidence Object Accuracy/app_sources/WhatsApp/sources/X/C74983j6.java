package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.biz.catalog.view.activity.CatalogListActivity;

/* renamed from: X.3j6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74983j6 extends AbstractC05270Ox {
    public final /* synthetic */ CatalogListActivity A00;

    public C74983j6(CatalogListActivity catalogListActivity) {
        this.A00 = catalogListActivity;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        CatalogListActivity catalogListActivity;
        AnonymousClass3CW r0;
        if (i2 > 5 && (r0 = (catalogListActivity = this.A00).A06) != null) {
            r0.A00();
            catalogListActivity.A06 = null;
        }
        this.A00.A2h();
    }
}
