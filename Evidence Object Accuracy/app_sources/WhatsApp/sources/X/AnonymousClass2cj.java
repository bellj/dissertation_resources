package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import com.google.android.material.tabs.TabLayout;

/* renamed from: X.2cj  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2cj extends LinearLayout {
    public float A00;
    public int A01 = -1;
    public int A02 = -1;
    public int A03 = -1;
    public int A04;
    public int A05 = -1;
    public ValueAnimator A06;
    public final Paint A07;
    public final GradientDrawable A08;
    public final /* synthetic */ TabLayout A09;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2cj(Context context, TabLayout tabLayout) {
        super(context);
        this.A09 = tabLayout;
        setWillNotDraw(false);
        this.A07 = C12990iw.A0F();
        this.A08 = new GradientDrawable();
    }

    public final void A00() {
        int i;
        int i2;
        View childAt = getChildAt(this.A05);
        if (childAt == null || childAt.getWidth() <= 0) {
            i = -1;
            i2 = -1;
        } else {
            i = childAt.getLeft();
            i2 = childAt.getRight();
            TabLayout tabLayout = this.A09;
            if (!tabLayout.A0S && (childAt instanceof C53092ck)) {
                RectF rectF = tabLayout.A0Y;
                A02(rectF, (C53092ck) childAt);
                i = (int) rectF.left;
                i2 = (int) rectF.right;
            }
            if (this.A00 > 0.0f && this.A05 < getChildCount() - 1) {
                View childAt2 = getChildAt(this.A05 + 1);
                int left = childAt2.getLeft();
                int right = childAt2.getRight();
                if (!tabLayout.A0S && (childAt2 instanceof C53092ck)) {
                    RectF rectF2 = tabLayout.A0Y;
                    A02(rectF2, (C53092ck) childAt2);
                    left = (int) rectF2.left;
                    right = (int) rectF2.right;
                }
                float f = this.A00;
                float f2 = 1.0f - f;
                i = (int) ((((float) left) * f) + (((float) i) * f2));
                i2 = (int) ((((float) right) * f) + (f2 * ((float) i2)));
            }
        }
        if (i != this.A01 || i2 != this.A02) {
            this.A01 = i;
            this.A02 = i2;
            postInvalidateOnAnimation();
        }
    }

    public void A01(int i, int i2) {
        ValueAnimator valueAnimator = this.A06;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.A06.cancel();
        }
        View childAt = getChildAt(i);
        if (childAt == null) {
            A00();
            return;
        }
        int left = childAt.getLeft();
        int right = childAt.getRight();
        TabLayout tabLayout = this.A09;
        if (!tabLayout.A0S && (childAt instanceof C53092ck)) {
            RectF rectF = tabLayout.A0Y;
            A02(rectF, (C53092ck) childAt);
            left = (int) rectF.left;
            right = (int) rectF.right;
        }
        int i3 = this.A01;
        int i4 = this.A02;
        if (i3 != left || i4 != right) {
            ValueAnimator valueAnimator2 = new ValueAnimator();
            this.A06 = valueAnimator2;
            valueAnimator2.setInterpolator(C50732Qs.A02);
            valueAnimator2.setDuration((long) i2);
            valueAnimator2.setFloatValues(0.0f, 1.0f);
            valueAnimator2.addUpdateListener(new C96044el(this, i3, left, i4, right));
            valueAnimator2.addListener(new C72543em(this, i));
            valueAnimator2.start();
        }
    }

    public final void A02(RectF rectF, C53092ck r7) {
        int contentWidth = r7.getContentWidth();
        TabLayout tabLayout = this.A09;
        if (contentWidth < tabLayout.A01(24)) {
            contentWidth = tabLayout.A01(24);
        }
        int left = (r7.getLeft() + r7.getRight()) >> 1;
        int i = contentWidth >> 1;
        rectF.set((float) (left - i), 0.0f, (float) (left + i), 0.0f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003b  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r7) {
        /*
            r6 = this;
            com.google.android.material.tabs.TabLayout r5 = r6.A09
            android.graphics.drawable.Drawable r0 = r5.A0I
            r1 = 0
            if (r0 == 0) goto L_0x006d
            int r4 = r0.getIntrinsicHeight()
        L_0x000b:
            int r0 = r6.A04
            if (r0 < 0) goto L_0x0010
            r4 = r0
        L_0x0010:
            int r3 = r5.A06
            if (r3 == 0) goto L_0x0063
            r2 = 1
            r0 = 2
            if (r3 == r2) goto L_0x0055
            if (r3 == r0) goto L_0x001e
            r0 = 3
            if (r3 == r0) goto L_0x0068
            r4 = 0
        L_0x001e:
            int r2 = r6.A01
            if (r2 < 0) goto L_0x004d
            int r0 = r6.A02
            if (r0 <= r2) goto L_0x004d
            android.graphics.drawable.Drawable r0 = r5.A0I
            if (r0 != 0) goto L_0x002c
            android.graphics.drawable.GradientDrawable r0 = r6.A08
        L_0x002c:
            android.graphics.drawable.Drawable r3 = X.C015607k.A03(r0)
            int r2 = r6.A01
            int r0 = r6.A02
            r3.setBounds(r2, r1, r0, r4)
            android.graphics.Paint r1 = r6.A07
            if (r1 == 0) goto L_0x004a
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            int r1 = r1.getColor()
            if (r2 != r0) goto L_0x0051
            android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.SRC_IN
            r3.setColorFilter(r1, r0)
        L_0x004a:
            r3.draw(r7)
        L_0x004d:
            super.draw(r7)
            return
        L_0x0051:
            X.C015607k.A0A(r3, r1)
            goto L_0x004a
        L_0x0055:
            int r1 = r6.getHeight()
            int r1 = r1 - r4
            int r1 = r1 / r0
            int r0 = r6.getHeight()
            int r0 = r0 + r4
            int r4 = r0 >> 1
            goto L_0x001e
        L_0x0063:
            int r1 = r6.getHeight()
            int r1 = r1 - r4
        L_0x0068:
            int r4 = r6.getHeight()
            goto L_0x001e
        L_0x006d:
            r4 = 0
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2cj.draw(android.graphics.Canvas):void");
    }

    public float getIndicatorPosition() {
        return ((float) this.A05) + this.A00;
    }

    @Override // android.widget.LinearLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        ValueAnimator valueAnimator = this.A06;
        if (valueAnimator == null || !valueAnimator.isRunning()) {
            A00();
            return;
        }
        this.A06.cancel();
        A01(this.A05, Math.round((1.0f - this.A06.getAnimatedFraction()) * ((float) this.A06.getDuration())));
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (View.MeasureSpec.getMode(i) == 1073741824) {
            TabLayout tabLayout = this.A09;
            if (tabLayout.A03 == 1 && tabLayout.A04 == 1) {
                int childCount = getChildCount();
                int i3 = 0;
                for (int i4 = 0; i4 < childCount; i4++) {
                    View childAt = getChildAt(i4);
                    if (childAt.getVisibility() == 0) {
                        i3 = Math.max(i3, childAt.getMeasuredWidth());
                    }
                }
                if (i3 > 0) {
                    if (i3 * childCount <= getMeasuredWidth() - (tabLayout.A01(16) << 1)) {
                        boolean z = false;
                        for (int i5 = 0; i5 < childCount; i5++) {
                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i5).getLayoutParams();
                            if (layoutParams.width != i3 || layoutParams.weight != 0.0f) {
                                layoutParams.width = i3;
                                layoutParams.weight = 0.0f;
                                z = true;
                            }
                        }
                        if (!z) {
                            return;
                        }
                    } else {
                        tabLayout.A04 = 0;
                        tabLayout.A0H(false);
                    }
                    super.onMeasure(i, i2);
                }
            }
        }
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onRtlPropertiesChanged(int i) {
        super.onRtlPropertiesChanged(i);
        if (Build.VERSION.SDK_INT < 23 && this.A03 != i) {
            requestLayout();
            this.A03 = i;
        }
    }

    public void setSelectedIndicatorColor(int i) {
        Paint paint = this.A07;
        if (paint.getColor() != i) {
            paint.setColor(i);
            postInvalidateOnAnimation();
        }
    }

    public void setSelectedIndicatorHeight(int i) {
        if (this.A04 != i) {
            this.A04 = i;
            postInvalidateOnAnimation();
        }
    }
}
