package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.3fN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72913fN extends BroadcastReceiver {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public C72913fN(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            AbstractView$OnCreateContextMenuListenerC35851ir r4 = this.A00;
            boolean A07 = r4.A0x.A07();
            if (r4.A0q != A07) {
                r4.A0q = A07;
                C30751Yr r0 = r4.A0n;
                if (r0 != null) {
                    if (A07) {
                        r4.A0p = true;
                    } else {
                        r0.A05 = 0;
                        r4.A0K();
                    }
                }
                r4.A0D = 0;
                r4.A0L();
            }
        }
    }
}
