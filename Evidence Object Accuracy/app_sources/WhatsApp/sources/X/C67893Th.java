package X;

import java.util.Iterator;
import java.util.Stack;

/* renamed from: X.3Th  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67893Th implements AnonymousClass5X9 {
    public AnonymousClass39n A00;
    public Object A01;
    public String A02;
    public final C1093151f A03 = new C1093151f();
    public final C14240l5 A04;
    public final Stack A05;
    public final Stack A06;

    public C67893Th(C14240l5 r4, Iterator it) {
        Stack stack = new Stack();
        this.A06 = stack;
        Stack stack2 = new Stack();
        this.A05 = stack2;
        this.A04 = r4;
        stack.add(it);
        stack2.add(AnonymousClass4AS.ReadObject);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e  */
    @Override // X.AnonymousClass5X9
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass39n ALh() {
        /*
            r5 = this;
            r4 = 0
            r5.A02 = r4
            r5.A00 = r4
            java.util.Stack r2 = r5.A05
            java.lang.Object r0 = r2.peek()
            X.4AS r0 = (X.AnonymousClass4AS) r0
            java.util.Stack r3 = r5.A06
            java.lang.Object r1 = r3.peek()
            java.util.Iterator r1 = (java.util.Iterator) r1
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0029;
                case 1: goto L_0x0033;
                case 2: goto L_0x007d;
                case 3: goto L_0x001f;
                case 4: goto L_0x0063;
                default: goto L_0x001c;
            }
        L_0x001c:
            X.39n r0 = r5.A00
            return r0
        L_0x001f:
            X.39n r0 = X.AnonymousClass39n.A09
            r5.A00 = r0
            r2.pop()
            X.4AS r0 = X.AnonymousClass4AS.ArrayReadValue
            goto L_0x0054
        L_0x0029:
            X.39n r0 = X.AnonymousClass39n.A0A
            r5.A00 = r0
            r2.pop()
            X.4AS r0 = X.AnonymousClass4AS.ObjectReadName
            goto L_0x0054
        L_0x0033:
            r5.A01 = r4
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0058
            java.util.Map$Entry r1 = X.C12970iu.A15(r1)
            java.lang.String r0 = X.C12990iw.A0r(r1)
            r5.A02 = r0
            java.lang.Object r0 = r1.getValue()
            r5.A01 = r0
            X.39n r0 = X.AnonymousClass39n.A06
            r5.A00 = r0
            r2.pop()
            X.4AS r0 = X.AnonymousClass4AS.ObjectReadValue
        L_0x0054:
            r2.push(r0)
            goto L_0x001c
        L_0x0058:
            X.39n r0 = X.AnonymousClass39n.A04
            r5.A00 = r0
            r2.pop()
            r3.pop()
            goto L_0x001c
        L_0x0063:
            r5.A01 = r4
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0072
            java.lang.Object r0 = r1.next()
            r5.A01 = r0
            goto L_0x0085
        L_0x0072:
            X.39n r0 = X.AnonymousClass39n.A02
            r5.A00 = r0
            r3.pop()
            r2.pop()
            goto L_0x001c
        L_0x007d:
            r2.pop()
            X.4AS r0 = X.AnonymousClass4AS.ObjectReadName
            r2.push(r0)
        L_0x0085:
            java.lang.Object r1 = r5.A01
            if (r1 != 0) goto L_0x008e
            X.39n r0 = X.AnonymousClass39n.A07
        L_0x008b:
            r5.A00 = r0
            goto L_0x001c
        L_0x008e:
            boolean r0 = r1 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x0095
            X.39n r0 = X.AnonymousClass39n.A01
            goto L_0x008b
        L_0x0095:
            boolean r0 = r1 instanceof java.lang.Number
            if (r0 == 0) goto L_0x009c
            X.39n r0 = X.AnonymousClass39n.A08
            goto L_0x008b
        L_0x009c:
            boolean r0 = r1 instanceof X.C94724cR
            if (r0 == 0) goto L_0x00a3
            X.39n r0 = X.AnonymousClass39n.A05
            goto L_0x008b
        L_0x00a3:
            boolean r0 = r1 instanceof java.lang.String
            if (r0 == 0) goto L_0x00aa
            X.39n r0 = X.AnonymousClass39n.A0B
            goto L_0x008b
        L_0x00aa:
            boolean r0 = r1 instanceof java.util.List
            if (r0 == 0) goto L_0x00bf
            java.util.List r1 = (java.util.List) r1
            java.util.Iterator r0 = r1.iterator()
            r3.push(r0)
            X.4AS r0 = X.AnonymousClass4AS.ArrayReadValue
            r2.push(r0)
            X.39n r0 = X.AnonymousClass39n.A09
            goto L_0x008b
        L_0x00bf:
            boolean r0 = r1 instanceof java.util.Map
            if (r0 == 0) goto L_0x00d4
            java.util.Map r1 = (java.util.Map) r1
            java.util.Iterator r0 = X.C12960it.A0n(r1)
            r3.push(r0)
            X.4AS r0 = X.AnonymousClass4AS.ObjectReadName
            r2.push(r0)
            X.39n r0 = X.AnonymousClass39n.A0A
            goto L_0x008b
        L_0x00d4:
            java.lang.String r0 = "unknown value type"
            java.lang.String r0 = X.C12960it.A0b(r0, r1)
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67893Th.ALh():X.39n");
    }

    @Override // X.AnonymousClass5X9
    public String AZ6() {
        return this.A02;
    }

    @Override // X.AnonymousClass5X9
    public AnonymousClass39n AZ7() {
        return this.A00;
    }

    @Override // X.AnonymousClass5X9
    public AnonymousClass5XA AZ8() {
        C1093151f r1 = this.A03;
        r1.A00 = this.A01;
        return r1;
    }

    @Override // X.AnonymousClass5X9
    public void Ae4() {
        AnonymousClass39n r1 = this.A00;
        AnonymousClass39n r3 = AnonymousClass39n.A09;
        if (r1 == r3 || r1 == AnonymousClass39n.A0A) {
            int i = 1;
            while (true) {
                AnonymousClass39n ALh = ALh();
                if (ALh == r3 || ALh == AnonymousClass39n.A0A) {
                    i++;
                } else if (ALh == AnonymousClass39n.A02 || ALh == AnonymousClass39n.A04) {
                    i--;
                }
                if (i == 0) {
                    return;
                }
            }
        }
    }
}
