package X;

/* renamed from: X.3aL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69853aL implements AbstractC116245Ur {
    public final /* synthetic */ AnonymousClass240 A00;

    public C69853aL(AnonymousClass240 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r5, Integer num, int i) {
        AnonymousClass1BQ r1;
        AnonymousClass240 r2 = this.A00;
        AbstractC116245Ur r0 = r2.A02;
        if (r0 != null) {
            r0.AWl(r5, num, i);
            if (r2.A08.A00 && (r1 = r2.A09) != null) {
                AnonymousClass016 r3 = r1.A03;
                if (r3.A01() != null && !C12980iv.A0z(r3).isEmpty()) {
                    C255619w r22 = r2.A0A;
                    AnonymousClass009.A05(r22);
                    r22.A00();
                    AnonymousClass009.A05(r1);
                    AnonymousClass009.A05(r3.A01());
                    r22.A01(num.intValue(), i, C12980iv.A0z(r3).size());
                }
            }
        }
    }
}
