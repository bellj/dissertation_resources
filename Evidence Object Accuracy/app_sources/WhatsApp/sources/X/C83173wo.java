package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.3wo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83173wo extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;

    public C83173wo(View view) {
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.setVisibility(8);
    }
}
