package X;

/* renamed from: X.30O  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30O extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;
    public String A02;

    public AnonymousClass30O() {
        super(2808, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamChatFolderOpen {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "activityIndicatorCount", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "folderType", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasImportantMessages", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
