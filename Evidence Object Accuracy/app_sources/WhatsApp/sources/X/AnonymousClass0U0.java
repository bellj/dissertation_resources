package X;

import android.graphics.PointF;
import java.util.List;

/* renamed from: X.0U0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U0 {
    public static PointF A00 = new PointF();

    public static int A00(float f, float f2) {
        int i = (int) f;
        int i2 = (int) f2;
        int i3 = i / i2;
        boolean z = false;
        if ((i ^ i2) >= 0) {
            z = true;
        }
        int i4 = i % i2;
        if (!z && i4 != 0) {
            i3--;
        }
        return i - (i2 * i3);
    }

    public static void A01(AbstractC12870ih r3, C06430To r4, C06430To r5, List list, int i) {
        String name = r3.getName();
        if (r4.A01(name, i)) {
            C06430To r1 = new C06430To(r5);
            r1.A01.add(name);
            C06430To r0 = new C06430To(r1);
            r0.A00 = r3;
            list.add(r0);
        }
    }
}
