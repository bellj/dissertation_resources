package X;

import android.content.res.ColorStateList;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;

/* renamed from: X.2Zi  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Zi extends RippleDrawable {
    public AnonymousClass2Zi(ColorStateList colorStateList, Drawable drawable, InsetDrawable insetDrawable) {
        super(colorStateList, insetDrawable, drawable);
    }

    @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        if (getDrawable(0) != null) {
            ((LayerDrawable) ((InsetDrawable) getDrawable(0)).getDrawable()).getDrawable(0).setColorFilter(colorFilter);
        }
    }
}
