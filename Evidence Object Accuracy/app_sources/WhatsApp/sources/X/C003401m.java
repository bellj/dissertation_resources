package X;

import android.os.Build;
import android.os.Trace;
import android.util.Log;
import java.lang.reflect.Method;

@Deprecated
/* renamed from: X.01m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C003401m {
    public static Method A00;
    public static Method A01;
    public static Method A02;
    public static Method A03;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18 && i < 29) {
            try {
                Trace.class.getField("TRACE_TAG_APP").getLong(null);
                Class cls = Long.TYPE;
                A02 = Trace.class.getMethod("isTagEnabled", cls);
                Class cls2 = Integer.TYPE;
                A00 = Trace.class.getMethod("asyncTraceBegin", cls, String.class, cls2);
                A01 = Trace.class.getMethod("asyncTraceEnd", cls, String.class, cls2);
                A03 = Trace.class.getMethod("traceCounter", cls, String.class, cls2);
            } catch (Exception e) {
                Log.i("TraceCompat", "Unable to initialize via reflection.", e);
            }
        }
    }

    public static void A00() {
        if (Build.VERSION.SDK_INT >= 18) {
            AnonymousClass035.A00();
        }
    }

    public static void A01(String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            AnonymousClass035.A01(str);
        }
    }
}
