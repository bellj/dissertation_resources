package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;

/* renamed from: X.3x6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83353x6 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnimationSet A01;

    public C83353x6(View view, AnimationSet animationSet) {
        this.A01 = animationSet;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnimationSet animationSet = this.A01;
        animationSet.setStartOffset(1500);
        this.A00.startAnimation(animationSet);
    }
}
