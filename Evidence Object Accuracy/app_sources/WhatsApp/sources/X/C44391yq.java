package X;

import com.whatsapp.registration.report.BanReportViewModel;
import java.io.IOException;

/* renamed from: X.1yq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44391yq implements AbstractC44401yr {
    public final /* synthetic */ BanReportViewModel A00;

    public C44391yq(BanReportViewModel banReportViewModel) {
        this.A00 = banReportViewModel;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r3) {
        this.A00.A02.A0A(4);
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        APp(iOException);
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        this.A00.A02.A0A(1);
    }
}
