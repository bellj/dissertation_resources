package X;

import android.content.Context;
import com.whatsapp.gallerypicker.GalleryPickerLauncher;

/* renamed from: X.3OZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OZ implements AbstractC009204q {
    public final /* synthetic */ GalleryPickerLauncher A00;

    public AnonymousClass3OZ(GalleryPickerLauncher galleryPickerLauncher) {
        this.A00 = galleryPickerLauncher;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        GalleryPickerLauncher galleryPickerLauncher = this.A00;
        if (!galleryPickerLauncher.A03) {
            galleryPickerLauncher.A03 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) galleryPickerLauncher.generatedComponent())).A1E;
            galleryPickerLauncher.A01 = C12960it.A0R(r1);
            galleryPickerLauncher.A02 = (AnonymousClass10Z) r1.AGM.get();
            galleryPickerLauncher.A00 = C12970iu.A0Y(r1);
        }
    }
}
