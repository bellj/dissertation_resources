package X;

/* renamed from: X.3DA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DA {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass03U A04;
    public AnonymousClass03U A05;

    public /* synthetic */ AnonymousClass3DA(AnonymousClass03U r1, AnonymousClass03U r2, int i, int i2, int i3, int i4) {
        this.A05 = r1;
        this.A04 = r2;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A03 = i4;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("ChangeInfo{oldHolder=");
        A0k.append(this.A05);
        A0k.append(", newHolder=");
        A0k.append(this.A04);
        A0k.append(", fromX=");
        A0k.append(this.A00);
        A0k.append(", fromY=");
        A0k.append(this.A01);
        A0k.append(", toX=");
        A0k.append(this.A02);
        A0k.append(", toY=");
        A0k.append(this.A03);
        return C12970iu.A0v(A0k);
    }
}
