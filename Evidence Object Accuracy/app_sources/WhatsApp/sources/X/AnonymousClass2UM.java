package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.2UM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2UM extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass2UM() {
        super(3178, new AnonymousClass00E(1, 1000, SearchActionVerificationClientService.NOTIFICATION_ID), 2, 37887164);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamClockSkewDifferenceT {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "clockSkewHourly", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
