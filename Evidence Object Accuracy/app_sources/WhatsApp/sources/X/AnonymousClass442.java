package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.442  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass442 extends AnonymousClass2T1 {
    public final /* synthetic */ AnonymousClass2UD A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass442(Context context, AnonymousClass2UD r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass2T3, android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.camera_thumb_size);
        setMeasuredDimension(dimensionPixelSize, dimensionPixelSize);
    }
}
