package X;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.00j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ActivityC000800j extends ActivityC000900k implements AbstractC002200x, AbstractC002300y, AbstractC002400z {
    public static final String A02 = "androidx:appcompat";
    public Resources A00;
    public AnonymousClass025 A01;

    public static void A04() {
    }

    public static void A05() {
    }

    @Deprecated
    public static void A06() {
    }

    @Deprecated
    public static void A07() {
    }

    @Deprecated
    public static void A08() {
    }

    @Deprecated
    public void A1Z() {
    }

    public void A1a() {
    }

    @Deprecated
    public void A1g(boolean z) {
    }

    @Override // X.AbstractC002200x
    public void AXC(AbstractC009504t r1) {
    }

    @Override // X.AbstractC002200x
    public void AXD(AbstractC009504t r1) {
    }

    public ActivityC000800j() {
        A02();
    }

    public ActivityC000800j(int i) {
        super(i);
        A02();
    }

    public static Intent A00(Activity activity) {
        Intent parentActivityIntent = activity.getParentActivityIntent();
        if (parentActivityIntent != null) {
            return parentActivityIntent;
        }
        try {
            String A00 = C014806z.A00(activity.getComponentName(), activity);
            if (A00 == null) {
                return null;
            }
            ComponentName componentName = new ComponentName(activity, A00);
            try {
                if (C014806z.A00(componentName, activity) == null) {
                    return Intent.makeMainActivity(componentName);
                }
                return new Intent().setComponent(componentName);
            } catch (PackageManager.NameNotFoundException unused) {
                StringBuilder sb = new StringBuilder("getParentActivityIntent: bad parentActivityName '");
                sb.append(A00);
                sb.append("' in manifest");
                Log.e("NavUtils", sb.toString());
                return null;
            }
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Intent A01(ComponentName componentName, Context context) {
        String A00 = C014806z.A00(componentName, context);
        if (A00 == null) {
            return null;
        }
        ComponentName componentName2 = new ComponentName(componentName.getPackageName(), A00);
        if (C014806z.A00(componentName2, context) == null) {
            return Intent.makeMainActivity(componentName2);
        }
        return new Intent().setComponent(componentName2);
    }

    private void A02() {
        this.A07.A00.A02(new C011305m(this), A02);
        A0R(new C011405n(this));
    }

    private void A03() {
        getWindow().getDecorView().setTag(R.id.view_tree_lifecycle_owner, this);
        getWindow().getDecorView().setTag(R.id.view_tree_view_model_store_owner, this);
        getWindow().getDecorView().setTag(R.id.view_tree_saved_state_registry_owner, this);
    }

    private boolean A09(KeyEvent keyEvent) {
        Window window;
        return Build.VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode()) && (window = getWindow()) != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent);
    }

    @Override // X.ActivityC000900k
    public void A0b() {
        A1V().A07();
    }

    public AbstractC005102i A1U() {
        LayoutInflater$Factory2C011505o r0 = (LayoutInflater$Factory2C011505o) A1V();
        r0.A0O();
        return r0.A0B;
    }

    public AnonymousClass025 A1V() {
        AnonymousClass025 r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        LayoutInflater$Factory2C011505o r02 = new LayoutInflater$Factory2C011505o(this, null, this, this);
        this.A01 = r02;
        return r02;
    }

    public AbstractC009504t A1W(AnonymousClass02Q r2) {
        return A1V().A05(r2);
    }

    public void A1X() {
        new AnonymousClass0XB((LayoutInflater$Factory2C011505o) A1V());
    }

    public void A1Y() {
        A00(this);
    }

    public void A1b(int i) {
        LayoutInflater$Factory2C011505o r4 = (LayoutInflater$Factory2C011505o) A1V();
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            i = C43951xu.A03;
        } else if (i == 9) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            i = 109;
        }
        if (!r4.A0i || i != 108) {
            if (r4.A0Z && i == 1) {
                r4.A0Z = false;
            } else if (i != 1) {
                if (i == 2) {
                    r4.A0P();
                    r4.A0X = true;
                    return;
                } else if (i == 5) {
                    r4.A0P();
                    r4.A0W = true;
                    return;
                } else if (i == 10) {
                    r4.A0P();
                    r4.A0f = true;
                    return;
                } else if (i == 108) {
                    r4.A0P();
                    r4.A0Z = true;
                    return;
                } else if (i != 109) {
                    r4.A08.requestFeature(i);
                    return;
                } else {
                    r4.A0P();
                    r4.A0e = true;
                    return;
                }
            }
            r4.A0P();
            r4.A0i = true;
        }
    }

    public void A1c(Intent intent) {
        navigateUpTo(intent);
    }

    public void A1d(Intent intent) {
        shouldUpRecreateTask(intent);
    }

    public void A1e(Toolbar toolbar) {
        LayoutInflater$Factory2C011505o r3 = (LayoutInflater$Factory2C011505o) A1V();
        Object obj = r3.A0m;
        if (obj instanceof Activity) {
            r3.A0O();
            AbstractC005102i r1 = r3.A0B;
            if (!(r1 instanceof AnonymousClass0C4)) {
                r3.A05 = null;
                if (r1 != null) {
                    r1.A05();
                }
                if (toolbar != null) {
                    AnonymousClass08Z r0 = new AnonymousClass08Z(r3.A0D, toolbar, ((Activity) obj).getTitle());
                    r3.A0B = r0;
                    r3.A08.setCallback(r0.A00);
                } else {
                    r3.A0B = null;
                    r3.A08.setCallback(r3.A0D);
                }
                r3.A07();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    public void A1f(C08970c9 r6) {
        Intent intent;
        if (((this instanceof AbstractC002400z) && (intent = A00(this)) != null) || (intent = A00(this)) != null) {
            ComponentName component = intent.getComponent();
            if (component == null) {
                component = intent.resolveActivity(r6.A00.getPackageManager());
            }
            ArrayList arrayList = r6.A01;
            int size = arrayList.size();
            try {
                Context context = r6.A00;
                for (Intent A01 = A01(component, context); A01 != null; A01 = A01(A01.getComponent(), context)) {
                    arrayList.add(size, A01);
                }
                arrayList.add(intent);
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
                throw new IllegalArgumentException(e);
            }
        }
    }

    public boolean A1h() {
        Intent A00 = A00(this);
        if (A00 == null) {
            return false;
        }
        if (shouldUpRecreateTask(A00)) {
            C08970c9 r5 = new C08970c9(this);
            A1f(r5);
            ArrayList arrayList = r5.A01;
            if (!arrayList.isEmpty()) {
                Intent[] intentArr = (Intent[]) arrayList.toArray(new Intent[0]);
                intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
                r5.A00.startActivities(intentArr, null);
                try {
                    finishAffinity();
                    return true;
                } catch (IllegalStateException unused) {
                    finish();
                    return true;
                }
            } else {
                throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
            }
        } else {
            navigateUpTo(A00);
            return true;
        }
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A03();
        A1V().A0F(view, layoutParams);
    }

    @Override // android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        super.attachBaseContext(A1V().A03(context));
    }

    @Override // android.app.Activity
    public void closeOptionsMenu() {
        AbstractC005102i A1U = A1U();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (A1U == null || !A1U.A0R()) {
            super.closeOptionsMenu();
        }
    }

    @Override // X.AbstractActivityC001100m, android.app.Activity, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        AbstractC005102i A1U = A1U();
        if (keyCode != 82 || A1U == null || !A1U.A0W(keyEvent)) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    @Override // android.app.Activity
    public View findViewById(int i) {
        LayoutInflater$Factory2C011505o r0 = (LayoutInflater$Factory2C011505o) A1V();
        r0.A0M();
        return r0.A08.findViewById(i);
    }

    @Override // android.app.Activity
    public MenuInflater getMenuInflater() {
        Context context;
        LayoutInflater$Factory2C011505o r2 = (LayoutInflater$Factory2C011505o) A1V();
        MenuInflater menuInflater = r2.A05;
        if (menuInflater != null) {
            return menuInflater;
        }
        r2.A0O();
        AbstractC005102i r0 = r2.A0B;
        if (r0 != null) {
            context = r0.A02();
        } else {
            context = r2.A0k;
        }
        AnonymousClass0Ax r1 = new AnonymousClass0Ax(context);
        r2.A05 = r1;
        return r1;
    }

    @Override // android.content.Context, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public Resources getResources() {
        Resources resources = this.A00;
        return resources == null ? super.getResources() : resources;
    }

    @Override // android.app.Activity
    public void invalidateOptionsMenu() {
        A1V().A07();
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.A00 != null) {
            this.A00.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
        A1V().A0C(configuration);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public void onContentChanged() {
        A1Z();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        A1V().A08();
    }

    @Override // android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (A09(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity, android.view.Window.Callback
    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        AbstractC005102i A1U = A1U();
        if (menuItem.getItemId() != 16908332 || A1U == null || (A1U.A01() & 4) == 0) {
            return false;
        }
        return A1h();
    }

    @Override // android.app.Activity
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        ((LayoutInflater$Factory2C011505o) A1V()).A0M();
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPostResume() {
        super.onPostResume();
        LayoutInflater$Factory2C011505o r0 = (LayoutInflater$Factory2C011505o) A1V();
        r0.A0O();
        AbstractC005102i r1 = r0.A0B;
        if (r1 != null) {
            r1.A0Q(true);
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        LayoutInflater$Factory2C011505o r1 = (LayoutInflater$Factory2C011505o) A1V();
        r1.A0g = true;
        r1.A0V(true);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        A1V().A09();
    }

    @Override // android.app.Activity
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        A1V().A0H(charSequence);
    }

    @Override // android.app.Activity
    public void openOptionsMenu() {
        AbstractC005102i A1U = A1U();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (A1U == null || !A1U.A0T()) {
            super.openOptionsMenu();
        }
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void setContentView(int i) {
        A03();
        A1V().A0A(i);
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void setContentView(View view) {
        A03();
        A1V().A0E(view);
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A03();
        A1V().A0G(view, layoutParams);
    }

    @Override // android.app.Activity, android.content.Context, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void setTheme(int i) {
        super.setTheme(i);
        ((LayoutInflater$Factory2C011505o) A1V()).A02 = i;
    }
}
