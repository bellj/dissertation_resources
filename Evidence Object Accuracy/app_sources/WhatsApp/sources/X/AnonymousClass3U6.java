package X;

import com.whatsapp.mediaview.DeleteMessagesDialogFragment;
import com.whatsapp.mediaview.RevokeNuxDialogFragment;

/* renamed from: X.3U6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3U6 implements AbstractC116445Vl {
    public final /* synthetic */ DeleteMessagesDialogFragment A00;

    public AnonymousClass3U6(DeleteMessagesDialogFragment deleteMessagesDialogFragment) {
        this.A00 = deleteMessagesDialogFragment;
    }

    @Override // X.AbstractC116445Vl
    public void AUr() {
        this.A00.A1B();
    }

    @Override // X.AbstractC116445Vl
    public void AW3(int i) {
        DeleteMessagesDialogFragment deleteMessagesDialogFragment = this.A00;
        if (deleteMessagesDialogFragment.A0c()) {
            new RevokeNuxDialogFragment(i).A1F(deleteMessagesDialogFragment.A0E(), null);
        }
    }
}
