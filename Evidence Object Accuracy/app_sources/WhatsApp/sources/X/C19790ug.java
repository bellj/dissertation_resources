package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;

/* renamed from: X.0ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19790ug {
    public final C16490p7 A00;
    public final C21390xL A01;

    public C19790ug(C16490p7 r1, C21390xL r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static final void A00(AbstractC15340mz r8) {
        boolean z = false;
        boolean z2 = false;
        if (r8.A11 > 0) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("FutureMessageStore/validateMessage/message must have row_id set; key=");
        AnonymousClass1IS r2 = r8.A0z;
        sb.append(r2);
        AnonymousClass009.A0B(sb.toString(), z2);
        if (r8.A08() == 1) {
            z = true;
        }
        StringBuilder sb2 = new StringBuilder("FutureMessageStore/validateMessage/message in main storage; key=");
        sb2.append(r2);
        AnonymousClass009.A0B(sb2.toString(), z);
    }

    public void A01(C30361Xc r13) {
        C21390xL r6 = this.A01;
        if (r6.A01("future_ready", 0) == 1 || (r13.A11 > 0 && r13.A11 <= r6.A01("migration_message_future_index", 0))) {
            A00(r13);
            C16310on A02 = this.A00.A02();
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("message_row_id", Long.valueOf(r13.A11));
                contentValues.put("version", Integer.valueOf(r13.A01));
                C30021Vq.A06(contentValues, "data", r13.A13());
                contentValues.put("future_message_type", Integer.valueOf(r13.A00));
                C16330op r8 = A02.A03;
                long A022 = r8.A02(contentValues, "message_future");
                boolean z = false;
                if (A022 != -1) {
                    if (A022 == r13.A11) {
                        z = true;
                    }
                    AnonymousClass009.A0C("FutureMessageStore/insertOrUpdateFutureMessage/inserted row should have same row_id", z);
                } else {
                    contentValues.remove("message_row_id");
                    if (r8.A00("message_future", contentValues, "message_row_id = ?", new String[]{String.valueOf(r13.A11)}) != 1) {
                        throw new SQLiteException("Failed to insert / update futureproof message");
                    }
                }
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
