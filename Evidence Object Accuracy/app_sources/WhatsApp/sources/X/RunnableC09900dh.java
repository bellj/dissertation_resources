package X;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.0dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09900dh implements Runnable {
    public final /* synthetic */ Bitmap A00;
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A01;
    public final /* synthetic */ String A02;

    public RunnableC09900dh(Bitmap bitmap, ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, String str) {
        this.A01 = shortcutInfoCompatSaverImpl;
        this.A00 = bitmap;
        this.A02 = str;
    }

    @Override // java.lang.Runnable
    public void run() {
        Bitmap bitmap = this.A00;
        String str = this.A02;
        if (bitmap == null) {
            throw new IllegalArgumentException("bitmap is null");
        } else if (!TextUtils.isEmpty(str)) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
                try {
                    if (bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)) {
                        fileOutputStream.close();
                        return;
                    }
                    Log.wtf("ShortcutInfoCompatSaver", "Unable to compress bitmap");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to compress bitmap for saving ");
                    sb.append(str);
                    throw new RuntimeException(sb.toString());
                } catch (Throwable th) {
                    try {
                        fileOutputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e) {
                Log.wtf("ShortcutInfoCompatSaver", "Unable to write bitmap to file", e);
                StringBuilder sb2 = new StringBuilder("Unable to write bitmap to file ");
                sb2.append(str);
                throw new RuntimeException(sb2.toString(), e);
            }
        } else {
            throw new IllegalArgumentException("path is empty");
        }
    }
}
