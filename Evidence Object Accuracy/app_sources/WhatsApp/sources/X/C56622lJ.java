package X;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.2lJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56622lJ extends AbstractC15040mS {
    public static final byte[] A02 = "\n".getBytes();
    public final C93924ay A00;
    public final String A01;

    public C56622lJ(C14160kx r8) {
        super(r8);
        String str = AnonymousClass4H2.A00;
        String str2 = Build.VERSION.RELEASE;
        String A01 = C64933Hm.A01(Locale.getDefault());
        String str3 = Build.MODEL;
        String str4 = Build.ID;
        Object[] objArr = new Object[6];
        C12990iw.A1P("GoogleAnalytics", str, objArr);
        C12980iv.A1P(str2, A01, str3, objArr);
        objArr[5] = str4;
        this.A01 = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", objArr);
        this.A00 = new C93924ay(r8.A04);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a0 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final int A00(X.C56622lJ r12, java.net.URL r13, byte[] r14) {
        /*
            java.lang.String r2 = "Error closing http post connection output stream"
            X.C13020j0.A01(r14)
            int r3 = r14.length
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)
            java.lang.String r10 = "POST bytes, url"
            r11 = 3
            r9 = 0
            r6 = r12
            r8 = r13
            X.C15050mT.A06(r6, r7, r8, r9, r10, r11)
            X.4Vl r0 = X.C88904Hw.A0K
            java.lang.Object r1 = r0.A00()
            java.lang.String r1 = (java.lang.String) r1
            r0 = 2
            boolean r0 = android.util.Log.isLoggable(r1, r0)
            if (r0 == 0) goto L_0x002c
            java.lang.String r1 = new java.lang.String
            r1.<init>(r14)
            java.lang.String r0 = "Post payload\n"
            r12.A0D(r0, r1)
        L_0x002c:
            r4 = 0
            X.0kx r1 = r12.A00     // Catch: IOException -> 0x0082, all -> 0x00ae
            android.content.Context r0 = r1.A00     // Catch: IOException -> 0x0082, all -> 0x00ae
            r0.getPackageName()     // Catch: IOException -> 0x0082, all -> 0x00ae
            java.net.HttpURLConnection r5 = r12.A0I(r13)     // Catch: IOException -> 0x0082, all -> 0x00ae
            r0 = 1
            r5.setDoOutput(r0)     // Catch: IOException -> 0x007e, all -> 0x009d
            r5.setFixedLengthStreamingMode(r3)     // Catch: IOException -> 0x007e, all -> 0x009d
            r5.connect()     // Catch: IOException -> 0x007e, all -> 0x009d
            java.io.OutputStream r4 = r5.getOutputStream()     // Catch: IOException -> 0x007e, all -> 0x009d
            r4.write(r14)     // Catch: IOException -> 0x007e, all -> 0x009d
            A01(r12, r5)     // Catch: IOException -> 0x007e, all -> 0x009d
            int r3 = r5.getResponseCode()     // Catch: IOException -> 0x007e, all -> 0x009d
            r0 = 200(0xc8, float:2.8E-43)
            if (r3 != r0) goto L_0x0069
            X.2lE r0 = r1.A06     // Catch: IOException -> 0x007e, all -> 0x009d
            X.C14160kx.A01(r0)     // Catch: IOException -> 0x007e, all -> 0x009d
            X.C14170ky.A00()     // Catch: IOException -> 0x007e, all -> 0x009d
            X.0mR r3 = r0.A00     // Catch: IOException -> 0x007e, all -> 0x009d
            X.C14170ky.A00()     // Catch: IOException -> 0x007e, all -> 0x009d
            long r0 = java.lang.System.currentTimeMillis()     // Catch: IOException -> 0x007e, all -> 0x009d
            r3.A00 = r0     // Catch: IOException -> 0x007e, all -> 0x009d
            r3 = 200(0xc8, float:2.8E-43)
        L_0x0069:
            java.lang.String r1 = "POST status"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch: IOException -> 0x007e, all -> 0x009d
            r12.A0B(r1, r0)     // Catch: IOException -> 0x007e, all -> 0x009d
            r4.close()     // Catch: IOException -> 0x0076
            goto L_0x007a
        L_0x0076:
            r0 = move-exception
            r6.A0C(r2, r0)
        L_0x007a:
            r5.disconnect()
            return r3
        L_0x007e:
            r1 = move-exception
            r9 = r4
            r4 = r5
            goto L_0x0083
        L_0x0082:
            r1 = move-exception
        L_0x0083:
            java.lang.String r0 = "Network POST connection error"
            r6.A0E(r0, r1)     // Catch: all -> 0x0099
            if (r9 == 0) goto L_0x0092
            r9.close()     // Catch: IOException -> 0x008e
            goto L_0x0092
        L_0x008e:
            r0 = move-exception
            r6.A0C(r2, r0)
        L_0x0092:
            if (r4 == 0) goto L_0x0097
            r4.disconnect()
        L_0x0097:
            r0 = 0
            return r0
        L_0x0099:
            r1 = move-exception
            r5 = r4
            r4 = r9
            goto L_0x009e
        L_0x009d:
            r1 = move-exception
        L_0x009e:
            if (r4 == 0) goto L_0x00a8
            r4.close()     // Catch: IOException -> 0x00a4
            goto L_0x00a8
        L_0x00a4:
            r0 = move-exception
            r6.A0C(r2, r0)
        L_0x00a8:
            if (r5 == 0) goto L_0x00af
            r5.disconnect()
            throw r1
        L_0x00ae:
            r1 = move-exception
        L_0x00af:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56622lJ.A00(X.2lJ, java.net.URL, byte[]):int");
    }

    public static final void A01(C56622lJ r4, HttpURLConnection httpURLConnection) {
        try {
            InputStream inputStream = httpURLConnection.getInputStream();
            try {
                do {
                } while (inputStream.read(new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH]) > 0);
                try {
                    inputStream.close();
                } catch (IOException e) {
                    r4.A0C("Error closing http connection input stream", e);
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                        throw th;
                    } catch (IOException e2) {
                        r4.A0C("Error closing http connection input stream", e2);
                        throw th;
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            throw th2;
        }
    }

    public static final void A02(String str, String str2, StringBuilder sb) {
        if (sb.length() != 0) {
            sb.append('&');
        }
        sb.append(URLEncoder.encode(str, DefaultCrypto.UTF_8));
        sb.append('=');
        sb.append(URLEncoder.encode(str2, DefaultCrypto.UTF_8));
    }

    public final String A0H(AnonymousClass3H2 r10, boolean z) {
        String str;
        long parseLong;
        C13020j0.A01(r10);
        StringBuilder A0h = C12960it.A0h();
        try {
            Map map = r10.A04;
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                String A0r = C12990iw.A0r(A15);
                if (!"ht".equals(A0r) && !"qt".equals(A0r) && !"AppUID".equals(A0r) && !"z".equals(A0r) && !"_gmsv".equals(A0r)) {
                    A02(A0r, (String) A15.getValue(), A0h);
                }
            }
            long j = r10.A02;
            A02("ht", String.valueOf(j), A0h);
            A02("qt", String.valueOf(System.currentTimeMillis() - j), A0h);
            if (z) {
                C13020j0.A05("_s");
                C13020j0.A03("Short param name required", !"_s".startsWith("&"));
                String A0t = C12970iu.A0t("_s", map);
                if (A0t == null) {
                    A0t = "0";
                }
                try {
                    parseLong = Long.parseLong(A0t);
                } catch (NumberFormatException unused) {
                }
                if (parseLong != 0) {
                    str = String.valueOf(parseLong);
                } else {
                    str = String.valueOf(r10.A01);
                }
                A02("z", str, A0h);
            }
            return A0h.toString();
        } catch (UnsupportedEncodingException e) {
            A0C("Failed to encode name or value", e);
            return null;
        }
    }

    public final HttpURLConnection A0I(URL url) {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.setConnectTimeout(C12960it.A05(C88904Hw.A04.A00()));
            httpURLConnection.setReadTimeout(C12960it.A05(C88904Hw.A05.A00()));
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setRequestProperty("User-Agent", this.A01);
            httpURLConnection.setDoInput(true);
            return httpURLConnection;
        }
        throw C12990iw.A0i("Failed to obtain http connection");
    }

    public final boolean A0J() {
        C14170ky.A00();
        A0G();
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) ((C15050mT) this).A00.A00.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return true;
            }
        } catch (SecurityException unused) {
        }
        A09("No network connectivity");
        return false;
    }
}
