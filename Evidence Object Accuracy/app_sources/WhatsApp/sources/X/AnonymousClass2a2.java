package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.Conversation;

/* renamed from: X.2a2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a2 extends Handler {
    public final /* synthetic */ Conversation A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a2(Conversation conversation) {
        super(Looper.getMainLooper());
        this.A00 = conversation;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        Conversation conversation = this.A00;
        if (!((ActivityC13790kL) conversation).A0C.A00) {
            conversation.A33.A07();
            if (conversation.A4V) {
                conversation.A33.A0D(false);
            }
            conversation.A4Y = false;
            conversation.A4V = true;
        }
    }
}
