package X;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130015yf {
    public final C18600si A00;
    public final C30931Zj A01 = C117305Zk.A0V("PaymentPinSharedPrefs", "infra");

    public C130015yf(C18600si r3) {
        this.A00 = r3;
    }

    public synchronized long A00() {
        long j;
        JSONObject optJSONObject;
        j = 0;
        try {
            String A04 = this.A00.A04();
            if (!TextUtils.isEmpty(A04) && (optJSONObject = C13000ix.A05(A04).optJSONObject("pin")) != null) {
                j = optJSONObject.optLong("pin_next_retry_ts");
            }
        } catch (JSONException e) {
            this.A01.A05(C12960it.A0Z(e, "getNextRetryTs threw: ", C12960it.A0h()));
        }
        return j;
    }

    public synchronized void A01() {
        try {
            C18600si r5 = this.A00;
            JSONObject A0b = C117295Zj.A0b(r5);
            JSONObject optJSONObject = A0b.optJSONObject("pin");
            if (optJSONObject == null) {
                optJSONObject = C117295Zj.A0a();
            }
            optJSONObject.put("v", "1");
            optJSONObject.put("pinSet", true);
            A0b.put("pin", optJSONObject);
            C117295Zj.A1E(r5, A0b);
        } catch (JSONException e) {
            this.A01.A05(C12960it.A0Z(e, "setPinSet threw: ", C12960it.A0h()));
        }
    }

    public synchronized void A02(long j) {
        try {
            C18600si r5 = this.A00;
            JSONObject A0b = C117295Zj.A0b(r5);
            JSONObject optJSONObject = A0b.optJSONObject("pin");
            if (optJSONObject == null) {
                optJSONObject = C117295Zj.A0a();
            }
            optJSONObject.put("v", "1");
            optJSONObject.put("pin_next_retry_ts", j);
            A0b.put("pin", optJSONObject);
            C117295Zj.A1E(r5, A0b);
        } catch (JSONException e) {
            this.A01.A05(C12960it.A0Z(e, "setPinSet threw: ", C12960it.A0h()));
        }
    }

    public synchronized boolean A03() {
        boolean z;
        JSONObject optJSONObject;
        z = false;
        try {
            String A04 = this.A00.A04();
            if (!TextUtils.isEmpty(A04) && (optJSONObject = C13000ix.A05(A04).optJSONObject("pin")) != null) {
                z = optJSONObject.optBoolean("pinSet");
            }
        } catch (JSONException e) {
            this.A01.A05(C12960it.A0Z(e, "isPinSet threw: ", C12960it.A0h()));
        }
        return z;
    }
}
