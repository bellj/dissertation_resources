package X;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.2gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54482gn extends AnonymousClass02M {
    public List A00;
    public final /* synthetic */ GroupCallLogActivity A01;

    public /* synthetic */ C54482gn(GroupCallLogActivity groupCallLogActivity) {
        this.A01 = groupCallLogActivity;
    }

    public static /* synthetic */ void A00(C54482gn r3, AbstractC14640lm r4) {
        int i = 0;
        for (AnonymousClass1YV r0 : r3.A00) {
            if (r0.A02.equals(r4)) {
                r3.A01.A00.A03(i);
                return;
            }
            i++;
        }
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r6, int i) {
        C54982hb r62 = (C54982hb) r6;
        UserJid userJid = ((AnonymousClass1YV) this.A00.get(i)).A02;
        GroupCallLogActivity groupCallLogActivity = this.A01;
        C15370n3 A0B = groupCallLogActivity.A01.A0B(userJid);
        AnonymousClass1J1 r0 = groupCallLogActivity.A05;
        ImageView imageView = r62.A02;
        r0.A06(imageView, A0B);
        C12990iw.A1C(imageView, this, A0B, r62, 10);
        r62.A04.A06(A0B);
        AnonymousClass1YT r02 = groupCallLogActivity.A09;
        if (r02 != null && !r02.A0B.A03 && i == 0) {
            TextView textView = r62.A03;
            textView.setVisibility(0);
            C12980iv.A14(groupCallLogActivity.getResources(), textView, R.color.list_item_sub_title);
            textView.setText(R.string.group_call_offer_called_you);
        }
        AbstractView$OnClickListenerC34281fs.A02(r62.A01, this, A0B, 23);
        AbstractView$OnClickListenerC34281fs.A02(r62.A00, this, A0B, 24);
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C54982hb(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.group_call_participant_row), this.A01);
    }
}
