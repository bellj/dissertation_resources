package X;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;

/* renamed from: X.5aX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117745aX extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public LinearLayout A02;
    public C14900mE A03;
    public TextEmojiLabel A04;
    public WaImageView A05;
    public AnonymousClass01d A06;
    public C253118x A07;
    public AnonymousClass2P7 A08;
    public boolean A09;

    public C117745aX(Context context, AnonymousClass018 r12, C30821Yy r13, C50942Ry r14, int i, boolean z) {
        super(context);
        String str;
        int i2;
        String str2;
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A03 = C12970iu.A0R(A00);
            this.A07 = C117315Zl.A0H(A00);
            this.A06 = C12960it.A0Q(A00);
        }
        LinearLayout.inflate(getContext(), R.layout.incentive_info_view, this);
        this.A01 = AnonymousClass028.A0D(this, R.id.incentive_info_container);
        this.A00 = AnonymousClass028.A0D(this, R.id.incentive_message_divider);
        this.A05 = C12980iv.A0X(this, R.id.incentive_icon);
        this.A04 = C12970iu.A0T(this, R.id.incentive_info_text);
        this.A02 = C117305Zk.A07(this, R.id.incentive_blurb_container);
        this.A01.setVisibility(0);
        if (z) {
            this.A00.setVisibility(0);
        }
        C30821Yy r4 = r14.A09.A00.A02;
        String str3 = r14.A0C;
        String string = context.getString(R.string.incentive_not_eligible_desc);
        if (i != 0) {
            if (i != 1) {
                if (!(i == 2 || i == 3)) {
                    if (i != 4) {
                        if (!(i == 5 || i == 7)) {
                            return;
                        }
                    } else if (r14.A01 != 0) {
                        return;
                    }
                }
                A00(this, this.A07.A01(this.A04.getContext(), string, new Runnable[]{new Runnable() { // from class: X.6FF
                    @Override // java.lang.Runnable
                    public final void run() {
                    }
                }}, new String[]{"fine-print"}, new String[]{str3}), 1);
            } else if (r14.A00 == 0) {
                this.A04.setText(R.string.incentive_not_eligible_offer_expired_desc);
                this.A02.getBackground().setLevel(1);
            } else {
                return;
            }
            this.A05.setVisibility(8);
            return;
        }
        if (r13.A00.compareTo(r4.A00) >= 0) {
            String str4 = r14.A0E;
            AnonymousClass20C r0 = r14.A07;
            if (r0 != null) {
                str2 = C12960it.A0X(context, AnonymousClass14X.A02(context, r12, r0.A01, r0.A02).toString(), C12970iu.A1b(), 0, R.string.incentive_confirmation_desc);
            } else {
                str2 = null;
            }
            i2 = 0;
            A00(this, this.A07.A01(this.A04.getContext(), str2, new Runnable[]{new Runnable() { // from class: X.6FE
                @Override // java.lang.Runnable
                public final void run() {
                }
            }}, new String[]{"cashback-terms"}, new String[]{str4}), 0);
        } else {
            AnonymousClass20C r2 = r14.A07;
            if (r2 != null) {
                AbstractC30791Yv r1 = r2.A01;
                String obj = AnonymousClass14X.A02(context, r12, r1, r4).toString();
                String obj2 = AnonymousClass14X.A02(context, r12, r1, r2.A02).toString();
                Object[] A1a = C12980iv.A1a();
                A1a[0] = obj2;
                str = C12960it.A0X(context, obj, A1a, 1, R.string.incentive_not_eligible_low_amount_desc);
            } else {
                str = null;
            }
            this.A04.setText(str);
            i2 = 0;
            this.A02.getBackground().setLevel(0);
        }
        this.A05.setVisibility(i2);
    }

    public static void A00(C117745aX r3, CharSequence charSequence, int i) {
        TextEmojiLabel textEmojiLabel = r3.A04;
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, r3.A06));
        r3.A04.A07 = new C52162aM();
        r3.A04.setText(charSequence);
        r3.A02.getBackground().setLevel(i);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }
}
