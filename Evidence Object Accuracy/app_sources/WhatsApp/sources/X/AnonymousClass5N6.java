package X;

import java.util.Enumeration;

/* renamed from: X.5N6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N6 extends AnonymousClass5NH {
    public final int A00;
    public final AnonymousClass5NH[] A01;

    @Override // X.AnonymousClass1TL
    public int A05() {
        Enumeration A0B = A0B();
        int i = 0;
        while (A0B.hasMoreElements()) {
            i += ((AnonymousClass1TN) A0B.nextElement()).Aer().A05();
        }
        return i + 2 + 2;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return true;
    }

    public Enumeration A0B() {
        return this.A01 == null ? new AnonymousClass5D1(this) : new AnonymousClass5D2(this);
    }

    public AnonymousClass5N6(byte[] bArr) {
        super(bArr);
        this.A01 = null;
        this.A00 = 1000;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5N6(X.AnonymousClass5NH[] r4) {
        /*
            r3 = this;
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            r1 = 0
        L_0x0006:
            int r0 = r4.length
            if (r1 == r0) goto L_0x0027
            r0 = r4[r1]     // Catch: IOException -> 0x0013
            byte[] r0 = r0.A00     // Catch: IOException -> 0x0013
            r2.write(r0)     // Catch: IOException -> 0x0013
            int r1 = r1 + 1
            goto L_0x0006
        L_0x0013:
            r2 = move-exception
            java.lang.String r0 = "exception converting octets "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r2.toString()
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0027:
            byte[] r1 = r2.toByteArray()
            r0 = 1000(0x3e8, float:1.401E-42)
            r3.<init>(r1)
            r3.A01 = r4
            r3.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5N6.<init>(X.5NH[]):void");
    }
}
