package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ij  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98464ij implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C53642em(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new C53642em(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C53642em[i];
    }
}
