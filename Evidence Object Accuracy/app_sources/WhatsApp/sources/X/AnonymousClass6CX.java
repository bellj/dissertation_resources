package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.6CX  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CX implements AbstractC136466Mq {
    public final /* synthetic */ AbstractC001200n A00;
    public final /* synthetic */ AbstractC118095bG A01;

    @Override // X.AbstractC136466Mq
    public void AOC() {
    }

    @Override // X.AbstractC136466Mq
    public void AYM(View view) {
    }

    public AnonymousClass6CX(AbstractC001200n r1, AbstractC118095bG r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC136466Mq
    public void AOV(View view) {
        String str;
        String str2;
        C1316163l r0;
        AbstractC118095bG r3 = this.A01;
        if (r3.A04 != null) {
            AnonymousClass017 A05 = r3.A05();
            if (A05 != null) {
                C117295Zj.A0s(this.A00, A05, this, 139);
            }
            if (!(r3 instanceof C123465nC)) {
                C123475nD r32 = (C123475nD) r3;
                AnonymousClass610 A03 = AnonymousClass610.A03("CONFIRM_DEPOSIT_AMT_CLICK", "ADD_MONEY", "REVIEW_TRANSACTION");
                C128365vz r2 = A03.A00;
                r2.A0T = "DEBIT";
                r2.A0L = r32.A0F.A00.getString(R.string.novi_confirm_button_label);
                A03.A07(((AbstractC118095bG) r32).A03, r32.A04);
                AnonymousClass6F2 r02 = ((AbstractC118095bG) r32).A02;
                if (r02 != null) {
                    r2.A09 = Long.valueOf(r02.A01.A00.longValue());
                }
                C30881Ze r03 = r32.A00;
                if (r03 != null) {
                    r2.A0S = r03.A0A;
                }
                r32.A0J.A05(r2);
                return;
            }
            C123465nC r33 = (C123465nC) r3;
            int i = r33.A00;
            if (i == 2) {
                str = "CONFIRM_WD_AMT_CLICK";
            } else {
                str = "CONFIRM_WD_CASH_AMT_CLICK";
            }
            AnonymousClass610 A032 = AnonymousClass610.A03(str, "WITHDRAW_MONEY", "REVIEW_TRANSACTION");
            if (i == 1) {
                str2 = "CASH";
            } else {
                str2 = "BANK";
            }
            C128365vz r22 = A032.A00;
            r22.A0T = str2;
            r22.A0L = r33.A0F.A00.getString(R.string.novi_confirm_button_label);
            A032.A08(((AbstractC118095bG) r33).A03, r33.A04);
            if (r33.A00 != 1 || (r0 = r33.A02) == null) {
                C30861Zc r04 = r33.A01;
                if (r04 != null) {
                    r22.A0S = r04.A0A;
                }
            } else {
                r22.A0h = r0.A04;
            }
            r33.A0J.A05(r22);
        }
    }
}
