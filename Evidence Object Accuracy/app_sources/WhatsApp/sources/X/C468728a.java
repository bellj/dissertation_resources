package X;

import java.util.Iterator;

/* renamed from: X.28a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468728a extends AbstractC468828b {
    public final /* synthetic */ AnonymousClass28S val$retainIfTrue;
    public final /* synthetic */ Iterator val$unfiltered;

    public C468728a(Iterator it, AnonymousClass28S r2) {
        this.val$unfiltered = it;
        this.val$retainIfTrue = r2;
    }

    @Override // X.AbstractC468828b
    public Object computeNext() {
        while (this.val$unfiltered.hasNext()) {
            Object next = this.val$unfiltered.next();
            if (this.val$retainIfTrue.A66(next)) {
                return next;
            }
        }
        return endOfData();
    }
}
