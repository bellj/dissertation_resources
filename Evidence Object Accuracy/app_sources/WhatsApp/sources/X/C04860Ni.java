package X;

/* renamed from: X.0Ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04860Ni {
    public final AnonymousClass0HA A00;
    public final AnonymousClass0H7 A01;
    public final AnonymousClass0JS A02;
    public final boolean A03;

    public C04860Ni(AnonymousClass0HA r1, AnonymousClass0H7 r2, AnonymousClass0JS r3, boolean z) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = z;
    }
}
