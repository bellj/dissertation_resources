package X;

import android.content.Context;
import android.view.View;
import java.util.List;

/* renamed from: X.4vU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106304vU implements AnonymousClass5WW {
    public final /* synthetic */ List A00;

    public C106304vU(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        AbstractC52532bB r4 = (AbstractC52532bB) obj;
        r4.setTouchDelegate(new C52412al(r4, this.A00));
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setTouchDelegate(null);
    }
}
