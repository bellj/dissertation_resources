package X;

import android.view.View;
import android.view.ViewTreeObserver;
import java.util.List;

/* renamed from: X.0WY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WY implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass0CM A00;

    public AnonymousClass0WY(AnonymousClass0CM r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass0CM r2 = this.A00;
        if (r2.AK4()) {
            List<AnonymousClass0NH> list = r2.A0O;
            if (list.size() > 0 && !((AnonymousClass0NH) list.get(0)).A02.A0H) {
                View view = r2.A06;
                if (view == null || !view.isShown()) {
                    r2.dismiss();
                    return;
                }
                for (AnonymousClass0NH r0 : list) {
                    r0.A02.Ade();
                }
            }
        }
    }
}
