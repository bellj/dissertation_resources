package X;

import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26631Ef {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C26631Ef(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        if (this.A00 == null) {
            C16630pM r2 = this.A01;
            SharedPreferences A01 = r2.A01("block_reasons_prefs");
            this.A00 = A01;
            if (A01.getBoolean("biz_block_reasons_migration_pending", true)) {
                HashMap hashMap = new HashMap();
                hashMap.put("biz_block_reasons", String.class);
                hashMap.put("biz_block_reasons_country", String.class);
                hashMap.put("biz_block_reasons_version", Integer.class);
                hashMap.put("biz_block_reasons_language", String.class);
                SharedPreferences A012 = r2.A01(C14820m6.A05);
                SharedPreferences sharedPreferences2 = this.A00;
                SharedPreferences.Editor editor = null;
                SharedPreferences.Editor editor2 = null;
                for (Map.Entry entry : hashMap.entrySet()) {
                    String str = (String) entry.getKey();
                    if (A012.contains(str)) {
                        if (editor == null) {
                            editor = sharedPreferences2.edit();
                        }
                        Class cls = (Class) entry.getValue();
                        if (cls == Boolean.class) {
                            editor.putBoolean(str, A012.getBoolean(str, false));
                        } else if (cls == Integer.class) {
                            editor.putInt(str, A012.getInt(str, 0));
                        } else if (cls == Float.class) {
                            editor.putFloat(str, A012.getFloat(str, 0.0f));
                        } else if (cls == Long.class) {
                            editor.putLong(str, A012.getLong(str, 0));
                        } else if (cls == String.class) {
                            editor.putString(str, A012.getString(str, null));
                        } else if (Set.class.isAssignableFrom(cls)) {
                            editor.putStringSet(str, A012.getStringSet(str, null));
                        } else {
                            StringBuilder sb = new StringBuilder("Cannot access value of type ");
                            sb.append(cls);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        if (editor2 == null) {
                            editor2 = A012.edit();
                        }
                        editor2.remove(str);
                    }
                }
                if (editor != null) {
                    editor.commit();
                }
                if (editor2 != null) {
                    editor2.commit();
                }
                this.A00.edit().putBoolean("biz_block_reasons_migration_pending", false).apply();
            }
        }
        sharedPreferences = this.A00;
        AnonymousClass009.A05(sharedPreferences);
        return sharedPreferences;
    }

    public void A01(int i) {
        A00().edit().putInt("biz_block_reasons_api_back_off_days", i).apply();
    }
}
