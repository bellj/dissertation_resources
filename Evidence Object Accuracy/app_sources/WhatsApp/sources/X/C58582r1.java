package X;

import android.app.Activity;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.2r1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58582r1 extends C28791Pa {
    public C625937v A00;
    public Runnable A01;
    public final ActivityC000800j A02;
    public final AnonymousClass12P A03;
    public final C89144Iv A04;
    public final AbstractC35511i9 A05;
    public final AbstractC13860kS A06;
    public final C14900mE A07;
    public final C20640w5 A08;
    public final AnonymousClass1FO A09;
    public final C21740xu A0A;
    public final C16170oZ A0B;
    public final C238013b A0C;
    public final C15550nR A0D;
    public final C15610nY A0E;
    public final C255319t A0F;
    public final C254119h A0G;
    public final AnonymousClass4Q5 A0H;
    public final AnonymousClass4UB A0I;
    public final C14830m7 A0J;
    public final C14820m6 A0K;
    public final AnonymousClass018 A0L;
    public final C20830wO A0M;
    public final C15600nX A0N;
    public final AnonymousClass19M A0O;
    public final C14850m9 A0P;
    public final C20710wC A0Q;
    public final AnonymousClass11G A0R;
    public final AbstractC14640lm A0S;
    public final AnonymousClass19J A0T;
    public final C252018m A0U;
    public final C255719x A0V;
    public final AbstractC14440lR A0W;

    public C58582r1(ActivityC000800j r3, AnonymousClass12P r4, C89144Iv r5, AbstractC35511i9 r6, AbstractC13860kS r7, C14900mE r8, C20640w5 r9, AnonymousClass1FO r10, C21740xu r11, C16170oZ r12, C238013b r13, C15550nR r14, C15610nY r15, C255319t r16, C254119h r17, AnonymousClass4Q5 r18, AnonymousClass4UB r19, C14830m7 r20, C14820m6 r21, AnonymousClass018 r22, C20830wO r23, C15600nX r24, AnonymousClass19M r25, C14850m9 r26, C20710wC r27, AnonymousClass11G r28, AbstractC14640lm r29, AnonymousClass19J r30, C252018m r31, C255719x r32, AbstractC14440lR r33) {
        this.A02 = r3;
        this.A0J = r20;
        this.A0P = r26;
        this.A0A = r11;
        this.A07 = r8;
        this.A0W = r33;
        this.A08 = r9;
        this.A0O = r25;
        this.A0B = r12;
        this.A03 = r4;
        this.A0D = r14;
        this.A0U = r31;
        this.A0E = r15;
        this.A0L = r22;
        this.A0C = r13;
        this.A0Q = r27;
        this.A0G = r17;
        this.A0R = r28;
        this.A09 = r10;
        this.A0K = r21;
        this.A0F = r16;
        this.A0V = r32;
        this.A0N = r24;
        this.A0M = r23;
        this.A0T = r30;
        this.A06 = r7;
        this.A05 = r6;
        this.A0H = r18;
        this.A04 = r5;
        this.A0S = r29;
        this.A0I = r19;
        this.A01 = new RunnableBRunnable0Shape3S0300000_I1(this, r7, r5, 18);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        super.onActivityDestroyed(activity);
        this.A01 = null;
        C625937v r1 = this.A00;
        if (r1 != null) {
            r1.A01 = null;
            r1.A00 = null;
            r1.A03(true);
            this.A00 = null;
        }
    }
}
