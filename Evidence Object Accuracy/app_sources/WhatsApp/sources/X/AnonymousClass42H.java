package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.42H  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42H extends C60962zB {
    @Override // X.AbstractC64483Fs
    public String A01() {
        return "novi_view_card_detail";
    }

    @Override // X.AbstractC64483Fs
    public String A02(Context context, AnonymousClass1Z8 r3) {
        return context.getString(R.string.native_flow_view_card);
    }
}
