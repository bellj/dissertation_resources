package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C462725f extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C462725f A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AnonymousClass229 A01;
    public AnonymousClass229 A02;

    static {
        C462725f r0 = new C462725f();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass229 r02 = this.A02;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            i2 += CodedOutputStream.A0A(r02, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass229 r02 = this.A02;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r02, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
