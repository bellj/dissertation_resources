package X;

import android.os.Looper;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.01I  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01I {
    public static void A00() {
        try {
            if (3 <= Log.level) {
                for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                    StringBuilder sb = new StringBuilder("\n");
                    Thread key = entry.getKey();
                    StackTraceElement[] value = entry.getValue();
                    sb.append("name=");
                    sb.append(key.getName());
                    sb.append(" state=");
                    sb.append(key.getState());
                    sb.append(" tid=");
                    sb.append(key.getId());
                    sb.append('\n');
                    sb.append(Log.stackTraceStartPhrase());
                    for (StackTraceElement stackTraceElement : value) {
                        sb.append("    at ");
                        sb.append(stackTraceElement.toString());
                        sb.append('\n');
                    }
                    sb.append("### end stack trace");
                    sb.append('\n');
                    Log.log(3, sb.toString());
                }
            }
        } catch (Throwable th) {
            Log.e("ThreadUtils/logAllStackTraces exception", th);
        }
    }

    public static boolean A01() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
