package X;

import java.util.List;

/* renamed from: X.4T8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4T8 {
    public final Double A00;
    public final String A01;
    public final String A02;
    public final List A03;
    public final List A04;
    public final List A05;

    public AnonymousClass4T8(Double d, String str, String str2, List list, List list2, List list3) {
        this.A03 = list;
        this.A04 = list2;
        this.A05 = list3;
        this.A01 = str;
        this.A00 = d;
        this.A02 = str2;
    }
}
