package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27481Hq extends DeviceJid implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass27F();

    @Override // com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid
    public int getType() {
        return 17;
    }

    public C27481Hq(Parcel parcel) {
        super(parcel);
    }

    public C27481Hq(UserJid userJid, int i) {
        super(userJid, i);
    }
}
