package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4xI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107414xI implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(4);
    public final int A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107414xI(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Ait(controlCode=");
        A0k.append(this.A00);
        A0k.append(",url=");
        A0k.append(this.A01);
        return C12960it.A0d(")", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeInt(this.A00);
    }
}
