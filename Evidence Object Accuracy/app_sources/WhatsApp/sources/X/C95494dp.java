package X;

import android.graphics.Point;
import android.media.MediaCodecInfo;
import android.util.Log;
import android.util.Pair;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.4dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95494dp {
    public final MediaCodecInfo.CodecCapabilities A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;

    public C95494dp(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2, String str3, boolean z, boolean z2) {
        this.A03 = str;
        this.A02 = str2;
        this.A01 = str3;
        this.A00 = codecCapabilities;
        this.A04 = z;
        this.A06 = z2;
        this.A05 = C95554dx.A05(str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        if ("Nexus 10".equals(r1) == false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        if ("OMX.Exynos.AVC.Decoder.secure".equals(r7) == false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        if (r6 != null) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C95494dp A00(android.media.MediaCodecInfo.CodecCapabilities r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, boolean r10) {
        /*
            r3 = r7
            r2 = r6
            if (r6 == 0) goto L_0x0036
            int r1 = X.AnonymousClass3JZ.A01
            r0 = 19
            if (r1 < r0) goto L_0x0036
            boolean r0 = A02(r6)
            if (r0 == 0) goto L_0x0036
            r0 = 22
            if (r1 > r0) goto L_0x005d
            java.lang.String r1 = X.AnonymousClass3JZ.A05
            java.lang.String r0 = "ODROID-XU3"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0026
            java.lang.String r0 = "Nexus 10"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x005d
        L_0x0026:
            java.lang.String r0 = "OMX.Exynos.AVC.Decoder"
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "OMX.Exynos.AVC.Decoder.secure"
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x005d
        L_0x0036:
            r6 = 0
            if (r2 == 0) goto L_0x0042
        L_0x0039:
            int r1 = X.AnonymousClass3JZ.A01
            r0 = 21
            if (r1 < r0) goto L_0x0042
            A01(r2)
        L_0x0042:
            if (r10 != 0) goto L_0x0052
            if (r2 == 0) goto L_0x005b
            int r1 = X.AnonymousClass3JZ.A01
            r0 = 21
            if (r1 < r0) goto L_0x005b
            boolean r0 = A03(r2)
            if (r0 == 0) goto L_0x005b
        L_0x0052:
            r7 = 1
        L_0x0053:
            r5 = r9
            r4 = r8
            X.4dp r1 = new X.4dp
            r1.<init>(r2, r3, r4, r5, r6, r7)
            return r1
        L_0x005b:
            r7 = 0
            goto L_0x0053
        L_0x005d:
            r6 = 1
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95494dp.A00(android.media.MediaCodecInfo$CodecCapabilities, java.lang.String, java.lang.String, java.lang.String, boolean):X.4dp");
    }

    public static void A01(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        codecCapabilities.isFeatureSupported("tunneled-playback");
    }

    public static boolean A02(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("adaptive-playback");
    }

    public static boolean A03(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("secure-playback");
    }

    public static boolean A04(MediaCodecInfo.VideoCapabilities videoCapabilities, double d, int i, int i2) {
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        Point point = new Point((((i + widthAlignment) - 1) / widthAlignment) * widthAlignment, (((i2 + heightAlignment) - 1) / heightAlignment) * heightAlignment);
        int i3 = point.x;
        int i4 = point.y;
        if (d == -1.0d || d < 1.0d) {
            return videoCapabilities.isSizeSupported(i3, i4);
        }
        return videoCapabilities.areSizeAndRateSupported(i3, i4, Math.floor(d));
    }

    public Point A05(int i, int i2) {
        MediaCodecInfo.VideoCapabilities videoCapabilities;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.A00;
        if (codecCapabilities == null || (videoCapabilities = codecCapabilities.getVideoCapabilities()) == null) {
            return null;
        }
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        return new Point((((i + widthAlignment) - 1) / widthAlignment) * widthAlignment, (((i2 + heightAlignment) - 1) / heightAlignment) * heightAlignment);
    }

    public AnonymousClass4XN A06(C100614mC r9, C100614mC r10) {
        int i = 0;
        if (!AnonymousClass3JZ.A0H(r9.A0T, r10.A0T)) {
            i = 8;
        }
        if (this.A05) {
            if (r9.A0E != r10.A0E) {
                i |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            }
            if (!this.A04 && !(r9.A0I == r10.A0I && r9.A09 == r10.A09)) {
                i |= 512;
            }
            if (!AnonymousClass3JZ.A0H(r9.A0M, r10.A0M)) {
                i |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
            }
            String str = this.A03;
            if (AnonymousClass3JZ.A05.startsWith("SM-T230") && "OMX.MARVELL.VIDEO.HW.CODA7542DECODER".equals(str) && !r9.A00(r10)) {
                i |= 2;
            }
            if (i == 0) {
                int i2 = 2;
                if (r9.A00(r10)) {
                    i2 = 3;
                }
                return new AnonymousClass4XN(r9, r10, str, i2, 0);
            }
        } else {
            if (r9.A06 != r10.A06) {
                i |= 4096;
            }
            if (r9.A0F != r10.A0F) {
                i |= DefaultCrypto.BUFFER_SIZE;
            }
            if (r9.A0B != r10.A0B) {
                i |= 16384;
            }
            if (i == 0 && "audio/mp4a-latm".equals(this.A02)) {
                Pair A01 = C95604e3.A01(r9);
                Pair A012 = C95604e3.A01(r10);
                if (!(A01 == null || A012 == null)) {
                    int A05 = C12960it.A05(A01.first);
                    int A052 = C12960it.A05(A012.first);
                    if (A05 == 42 && A052 == 42) {
                        return new AnonymousClass4XN(r9, r10, this.A03, 3, 0);
                    }
                }
            }
            if (!r9.A00(r10)) {
                i |= 32;
            }
            if ("audio/opus".equals(this.A02)) {
                i |= 2;
            }
            if (i == 0) {
                return new AnonymousClass4XN(r9, r10, this.A03, 1, 0);
            }
        }
        return new AnonymousClass4XN(r9, r10, this.A03, 0, i);
    }

    public final void A07(String str) {
        StringBuilder A0k = C12960it.A0k("NoSupport [");
        A0k.append(str);
        A0k.append("] [");
        A0k.append(this.A03);
        A0k.append(", ");
        A0k.append(this.A02);
        A0k.append("] [");
        A0k.append(AnonymousClass3JZ.A03);
        Log.d("MediaCodecInfo", C12960it.A0d("]", A0k));
    }

    public boolean A08(int i) {
        String A0W;
        int i2;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.A00;
        if (codecCapabilities == null) {
            A0W = "channelCount.caps";
        } else {
            MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
            if (audioCapabilities == null) {
                A0W = "channelCount.aCaps";
            } else {
                String str = this.A03;
                String str2 = this.A02;
                int maxInputChannelCount = audioCapabilities.getMaxInputChannelCount();
                if (maxInputChannelCount <= 1 && ((AnonymousClass3JZ.A01 < 26 || maxInputChannelCount <= 0) && !"audio/mpeg".equals(str2) && !"audio/3gpp".equals(str2) && !"audio/amr-wb".equals(str2) && !"audio/mp4a-latm".equals(str2) && !"audio/vorbis".equals(str2) && !"audio/opus".equals(str2) && !"audio/raw".equals(str2) && !"audio/flac".equals(str2) && !"audio/g711-alaw".equals(str2) && !"audio/g711-mlaw".equals(str2) && !"audio/gsm".equals(str2))) {
                    if ("audio/ac3".equals(str2)) {
                        i2 = 6;
                    } else {
                        i2 = 30;
                        if ("audio/eac3".equals(str2)) {
                            i2 = 16;
                        }
                    }
                    StringBuilder A0k = C12960it.A0k("AssumedMaxChannelAdjustment: ");
                    A0k.append(str);
                    A0k.append(", [");
                    A0k.append(maxInputChannelCount);
                    A0k.append(" to ");
                    A0k.append(i2);
                    Log.w("MediaCodecInfo", C12960it.A0d("]", A0k));
                    maxInputChannelCount = i2;
                }
                if (maxInputChannelCount >= i) {
                    return true;
                }
                A0W = C12960it.A0W(i, "channelCount.support, ");
            }
        }
        A07(A0W);
        return false;
    }

    public boolean A09(int i) {
        String A0W;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.A00;
        if (codecCapabilities == null) {
            A0W = "sampleRate.caps";
        } else {
            MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
            if (audioCapabilities == null) {
                A0W = "sampleRate.aCaps";
            } else if (audioCapabilities.isSampleRateSupported(i)) {
                return true;
            } else {
                A0W = C12960it.A0W(i, "sampleRate.support, ");
            }
        }
        A07(A0W);
        return false;
    }

    public boolean A0A(int i, int i2, double d) {
        String obj;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.A00;
        if (codecCapabilities == null) {
            obj = "sizeAndRate.caps";
        } else {
            MediaCodecInfo.VideoCapabilities videoCapabilities = codecCapabilities.getVideoCapabilities();
            if (videoCapabilities == null) {
                obj = "sizeAndRate.vCaps";
            } else if (A04(videoCapabilities, d, i, i2)) {
                return true;
            } else {
                if (i < i2) {
                    String str = this.A03;
                    if ((!"OMX.MTK.VIDEO.DECODER.HEVC".equals(str) || !"mcv5a".equals(AnonymousClass3JZ.A02)) && A04(videoCapabilities, d, i2, i)) {
                        StringBuilder A0k = C12960it.A0k("sizeAndRate.rotated, ");
                        A0k.append(i);
                        A0k.append("x");
                        A0k.append(i2);
                        A0k.append("x");
                        A0k.append(d);
                        String obj2 = A0k.toString();
                        StringBuilder A0k2 = C12960it.A0k("AssumedSupport [");
                        A0k2.append(obj2);
                        A0k2.append("] [");
                        A0k2.append(str);
                        A0k2.append(", ");
                        A0k2.append(this.A02);
                        A0k2.append("] [");
                        A0k2.append(AnonymousClass3JZ.A03);
                        Log.d("MediaCodecInfo", C12960it.A0d("]", A0k2));
                        return true;
                    }
                }
                StringBuilder A0k3 = C12960it.A0k("sizeAndRate.support, ");
                A0k3.append(i);
                A0k3.append("x");
                A0k3.append(i2);
                A0k3.append("x");
                A0k3.append(d);
                obj = A0k3.toString();
            }
        }
        A07(obj);
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0280, code lost:
        if (r1 < 800000) goto L_0x0282;
     */
    /* JADX WARNING: Removed duplicated region for block: B:198:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0B(X.C100614mC r13) {
        /*
        // Method dump skipped, instructions count: 707
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95494dp.A0B(X.4mC):boolean");
    }

    public boolean A0C(C100614mC r4) {
        if (this.A05) {
            return this.A04;
        }
        Pair A01 = C95604e3.A01(r4);
        return A01 != null && C12960it.A05(A01.first) == 42;
    }

    public String toString() {
        return this.A03;
    }
}
