package X;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.4VB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VB {
    public byte[] A00;

    public AnonymousClass4VB(byte[] bArr) {
        try {
            this.A00 = bArr.length > 32 ? MessageDigest.getInstance("SHA-256").digest(bArr) : bArr;
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public void A00(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(this.A00);
            this.A00 = instance.digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
