package X;

import java.util.Set;

/* renamed from: X.1uR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41961uR {
    public final long A00;
    public final long A01;
    public final AbstractC27881Jp A02;
    public final AbstractC27881Jp A03;
    public final Set A04;

    public C41961uR(AbstractC27881Jp r1, AbstractC27881Jp r2, Set set, long j, long j2) {
        this.A03 = r1;
        this.A01 = j;
        this.A02 = r2;
        this.A00 = j2;
        this.A04 = set;
    }
}
