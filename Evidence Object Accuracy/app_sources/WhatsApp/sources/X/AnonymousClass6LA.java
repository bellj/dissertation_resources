package X;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* renamed from: X.6LA  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6LA extends FutureTask {
    public final ArrayList A00 = C12960it.A0l();
    public final UUID A01;
    public volatile boolean A02;
    public final /* synthetic */ C1308560f A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass6LA(C1308560f r2, String str, UUID uuid, Callable callable) {
        super(callable);
        this.A03 = r2;
        if (str == null) {
            throw C12980iv.A0n("OpticFutureTask cannot have a null name.");
        } else if (uuid != null) {
            this.A01 = uuid;
        } else {
            throw C12980iv.A0n(C12960it.A0d(str, C12960it.A0k("SessionId is null! Attempting to schedule task: ")));
        }
    }

    public synchronized void A00(AbstractC129405xf r13) {
        AnonymousClass6K3 r6;
        C1308560f r1;
        UUID uuid;
        if (isDone()) {
            try {
                try {
                    Object obj = get();
                    this.A03.A05(new AnonymousClass6K3(r13, this, null, obj, true), this.A01);
                } catch (CancellationException e) {
                    r1 = this.A03;
                    uuid = this.A01;
                    r6 = new AnonymousClass6K3(r13, this, e, null, false);
                    r1.A05(r6, uuid);
                }
            } catch (InterruptedException | ExecutionException e2) {
                r1 = this.A03;
                uuid = this.A01;
                r6 = new AnonymousClass6K3(r13, this, e2, null, false);
                r1.A05(r6, uuid);
            }
        } else {
            this.A00.add(r13);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x007f A[DONT_GENERATE] */
    @Override // java.util.concurrent.FutureTask
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void done() {
        /*
            r13 = this;
            r3 = r13
            super.done()
            monitor-enter(r3)
            java.util.ArrayList r0 = r13.A00     // Catch: all -> 0x0081
            java.util.ArrayList r6 = X.C12980iv.A0x(r0)     // Catch: all -> 0x0081
            r0.clear()     // Catch: all -> 0x0081
            r4 = 0
            java.lang.Object r5 = r13.get()     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            java.util.Iterator r1 = r6.iterator()     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
        L_0x0017:
            boolean r0 = r1.hasNext()     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            if (r0 == 0) goto L_0x0021
            r1.next()     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            goto L_0x0017
        L_0x0021:
            r7 = 1
            X.60f r1 = r13.A03     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            java.util.UUID r0 = r13.A01     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            X.6K4 r2 = new X.6K4     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            r2.<init>(r3, r4, r5, r6, r7)     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            r1.A05(r2, r0)     // Catch: CancellationException -> 0x005e, ExecutionException | InterruptedException -> 0x002f, all -> 0x0081
            goto L_0x007d
        L_0x002f:
            r9 = move-exception
            boolean r0 = r6.isEmpty()     // Catch: all -> 0x0081
            if (r0 == 0) goto L_0x0050
            X.60f r2 = r3.A03     // Catch: all -> 0x0081
            android.os.HandlerThread r0 = r2.A04     // Catch: all -> 0x0081
            android.os.Looper r0 = r0.getLooper()     // Catch: all -> 0x0081
            java.lang.Thread r1 = r0.getThread()     // Catch: all -> 0x0081
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch: all -> 0x0081
            if (r1 == r0) goto L_0x007d
            boolean r0 = r2.A06     // Catch: all -> 0x0081
            if (r0 == 0) goto L_0x006d
            X.AnonymousClass616.A00()     // Catch: all -> 0x0081
            goto L_0x007d
        L_0x0050:
            java.util.Iterator r1 = r6.iterator()     // Catch: all -> 0x0081
        L_0x0054:
            boolean r0 = r1.hasNext()     // Catch: all -> 0x0081
            if (r0 == 0) goto L_0x006d
            r1.next()     // Catch: all -> 0x0081
            goto L_0x0054
        L_0x005e:
            r9 = move-exception
            r12 = 0
            X.60f r1 = r3.A03     // Catch: all -> 0x0081
            java.util.UUID r0 = r3.A01     // Catch: all -> 0x0081
            r8 = r3
            r10 = r4
            r11 = r6
            X.6K4 r7 = new X.6K4     // Catch: all -> 0x0081
            r7.<init>(r8, r9, r10, r11, r12)     // Catch: all -> 0x0081
            goto L_0x007a
        L_0x006d:
            r12 = 0
            X.60f r1 = r3.A03     // Catch: all -> 0x0081
            java.util.UUID r0 = r3.A01     // Catch: all -> 0x0081
            r8 = r3
            r10 = r4
            r11 = r6
            X.6K4 r7 = new X.6K4     // Catch: all -> 0x0081
            r7.<init>(r8, r9, r10, r11, r12)     // Catch: all -> 0x0081
        L_0x007a:
            r1.A05(r7, r0)     // Catch: all -> 0x0081
        L_0x007d:
            monitor-exit(r3)
            monitor-enter(r3)
            monitor-exit(r3)
            return
        L_0x0081:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6LA.done():void");
    }

    @Override // java.util.concurrent.FutureTask, java.util.concurrent.RunnableFuture, java.lang.Runnable
    public void run() {
        synchronized (this) {
        }
        super.run();
    }

    @Override // java.util.concurrent.FutureTask
    public boolean runAndReset() {
        synchronized (this) {
        }
        return super.runAndReset();
    }
}
