package X;

import java.lang.ref.SoftReference;

/* renamed from: X.0OW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OW {
    public SoftReference A00 = null;
    public SoftReference A01 = null;
    public SoftReference A02 = null;

    public Object A00() {
        SoftReference softReference = this.A00;
        if (softReference == null) {
            return null;
        }
        return softReference.get();
    }
}
