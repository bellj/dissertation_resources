package X;

/* renamed from: X.3Dy  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3Dy {
    public final AnonymousClass1V8 A00;
    public final String A01;

    public AnonymousClass3Dy(AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "screen");
        this.A01 = AnonymousClass3JT.A07(r3, "app_id", C13000ix.A08());
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3Dy.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3Dy) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
