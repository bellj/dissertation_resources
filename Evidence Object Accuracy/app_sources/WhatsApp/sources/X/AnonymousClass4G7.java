package X;

import java.util.Set;

/* renamed from: X.4G7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4G7 {
    public static final Set A00;

    static {
        String[] strArr = new String[3];
        strArr[0] = "default_reply_v1";
        strArr[1] = "status_update_pending_needs_more_info_v1";
        A00 = C12970iu.A13("status_update_under_review_v1", strArr, 2);
    }
}
