package X;

import android.util.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3To  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67963To implements AbstractC17450qp {
    public static String A00(Object obj, boolean z, boolean z2) {
        if ((obj instanceof List) || (obj instanceof Map)) {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            try {
                A03(jsonWriter, obj, z, z2);
                jsonWriter.close();
                return stringWriter.toString();
            } catch (IOException e) {
                return C12960it.A0b("Exception in serialization ", e);
            }
        } else if (obj == null) {
            return "";
        } else {
            return String.valueOf(obj);
        }
    }

    public static ArrayList A01(C90764Pd r5, JSONArray jSONArray) {
        int length = jSONArray.length();
        ArrayList A0w = C12980iv.A0w(length);
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj instanceof String) {
                A0w.add(obj);
            } else if (obj instanceof JSONObject) {
                A0w.add(A02(r5, (JSONObject) obj));
            } else if (obj instanceof JSONArray) {
                A0w.add(A01(r5, (JSONArray) obj));
            } else if (obj instanceof Integer) {
                C12980iv.A1R(A0w, C12960it.A05(obj));
            } else if (obj instanceof Double) {
                A0w.add(C64983Hr.A00(((Number) obj).doubleValue()));
            } else if (obj instanceof Float) {
                A0w.add(C64983Hr.A00((double) ((Number) obj).floatValue()));
            } else if (obj instanceof Boolean) {
                A0w.add(Boolean.valueOf(C12970iu.A1Y(obj)));
            } else if (obj instanceof Long) {
                A0w.add(Long.valueOf(C12980iv.A0G(obj)));
            }
        }
        return A0w;
    }

    public static HashMap A02(C90764Pd r5, JSONObject jSONObject) {
        HashMap A11 = C12970iu.A11();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String A0x = C12970iu.A0x(keys);
            Object obj = jSONObject.get(A0x);
            if (obj instanceof String) {
                A11.put(A0x, obj);
            } else if (obj instanceof JSONObject) {
                A11.put(A0x, A02(r5, (JSONObject) obj));
            } else if (obj instanceof JSONArray) {
                A11.put(A0x, A01(r5, (JSONArray) obj));
            } else if (obj instanceof Integer) {
                C12960it.A1K(A0x, A11, C12960it.A05(obj));
            } else if (obj instanceof Double) {
                A11.put(A0x, C64983Hr.A00(((Number) obj).doubleValue()));
            } else if (obj instanceof Float) {
                A11.put(A0x, C64983Hr.A00((double) ((Number) obj).floatValue()));
            } else if (obj instanceof Boolean) {
                A11.put(A0x, Boolean.valueOf(C12970iu.A1Y(obj)));
            } else if (obj instanceof Long) {
                A11.put(A0x, Long.valueOf(C12980iv.A0G(obj)));
            }
        }
        return A11;
    }

    public static void A03(JsonWriter jsonWriter, Object obj, boolean z, boolean z2) {
        String valueOf;
        if (obj == null) {
            jsonWriter.nullValue();
        } else if (obj instanceof Number) {
            jsonWriter.value((Number) obj);
        } else if (obj instanceof Boolean) {
            boolean A1Y = C12970iu.A1Y(obj);
            if (z2) {
                jsonWriter.value(A1Y ? 1 : 0);
            } else {
                jsonWriter.value(A1Y);
            }
        } else {
            if (obj instanceof String) {
                valueOf = (String) obj;
            } else if (obj instanceof Map) {
                jsonWriter.beginObject();
                if (z) {
                    TreeMap treeMap = new TreeMap();
                    Iterator A0n = C12960it.A0n((Map) obj);
                    while (A0n.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(A0n);
                        C12990iw.A1Q(String.valueOf(A15.getKey()), treeMap, A15);
                    }
                    Iterator A0s = C12990iw.A0s(treeMap);
                    while (A0s.hasNext()) {
                        Map.Entry A152 = C12970iu.A15(A0s);
                        jsonWriter.name(C12990iw.A0r(A152));
                        A03(jsonWriter, A152.getValue(), z, z2);
                    }
                } else {
                    Iterator A0n2 = C12960it.A0n((Map) obj);
                    while (A0n2.hasNext()) {
                        Map.Entry A153 = C12970iu.A15(A0n2);
                        jsonWriter.name(String.valueOf(A153.getKey()));
                        A03(jsonWriter, A153.getValue(), z, z2);
                    }
                }
                jsonWriter.endObject();
                return;
            } else if (obj instanceof List) {
                jsonWriter.beginArray();
                for (Object obj2 : (List) obj) {
                    A03(jsonWriter, obj2, z, z2);
                }
                jsonWriter.endArray();
                return;
            } else {
                valueOf = String.valueOf(obj);
            }
            jsonWriter.value(valueOf);
        }
    }

    @Override // X.AbstractC17450qp
    public Object A9j(C14220l3 r6, C1093651k r7, C14240l5 r8) {
        float f;
        String str = r7.A00;
        char c = 65535;
        switch (str.hashCode()) {
            case -2044261771:
                if (str.equals("bk.action.bloks.DangerouslyGetTreeFromParseResult")) {
                    c = 0;
                    break;
                }
                break;
            case -863899808:
                if (str.equals("bk.action.string.JsonDecode")) {
                    c = 1;
                    break;
                }
                break;
            case -826958968:
                if (str.equals("bk.action.string.JsonEncode")) {
                    c = 2;
                    break;
                }
                break;
            case -791813135:
                if (str.equals("bk.action.textspan.GetCenterX")) {
                    c = 3;
                    break;
                }
                break;
            case -791813134:
                if (str.equals("bk.action.textspan.GetCenterY")) {
                    c = 4;
                    break;
                }
                break;
            case -362131820:
                if (str.equals("bk.action.textspan.GetWidth")) {
                    c = 5;
                    break;
                }
                break;
            case -138615772:
                if (str.equals("bk.action.string.JsonEncodeV2")) {
                    c = 6;
                    break;
                }
                break;
            case -138615771:
                if (str.equals("bk.action.string.JsonEncodeV3")) {
                    c = 7;
                    break;
                }
                break;
            case 1225820697:
                if (str.equals("bk.action.textspan.GetHeight")) {
                    c = '\b';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return ((AnonymousClass3JI) r6.A00.get(0)).A01;
            case 1:
                String A0g = C12960it.A0g(r6.A00, 0);
                try {
                    return A02(r8.A01, C13000ix.A05(A0g));
                } catch (JSONException e) {
                    throw new IllegalArgumentException("Trying to decode malformed json", e);
                }
            case 2:
                return A00(r6.A00.get(0), false, true);
            case 3:
                f = ((AnonymousClass4RE) r6.A00.get(0)).A00;
                break;
            case 4:
                f = ((AnonymousClass4RE) r6.A00.get(0)).A01;
                break;
            case 5:
                f = ((AnonymousClass4RE) r6.A00.get(0)).A03;
                break;
            case 6:
                List list = r6.A00;
                return A00(list.get(0), C64983Hr.A02(list.get(1)), true);
            case 7:
                List list2 = r6.A00;
                return A00(list2.get(0), C64983Hr.A02(list2.get(1)), false);
            case '\b':
                f = ((AnonymousClass4RE) r6.A00.get(0)).A02;
                break;
            default:
                throw new AnonymousClass5H5(C12960it.A0d(str, C12960it.A0k("Unknown lispy action type: ")));
        }
        return C64983Hr.A00((double) f);
    }
}
