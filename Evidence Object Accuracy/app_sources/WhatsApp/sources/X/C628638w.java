package X;

import android.content.Intent;
import android.os.SystemClock;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

/* renamed from: X.38w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628638w extends AbstractC16350or {
    public long A00;
    public final long A01;
    public final C14820m6 A02;
    public final C22040yO A03;
    public final C25961Bm A04;
    public final C20800wL A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final WeakReference A09;
    public final JSONObject A0A;
    public final boolean A0B;

    public C628638w(C14820m6 r2, C22040yO r3, C25961Bm r4, AbstractActivityC452520u r5, C20800wL r6, String str, String str2, String str3, JSONObject jSONObject, long j, boolean z) {
        this.A06 = str;
        this.A08 = str2;
        this.A07 = str3;
        this.A0A = jSONObject;
        this.A0B = z;
        this.A01 = j;
        this.A03 = r3;
        this.A02 = r2;
        this.A05 = r6;
        this.A04 = r4;
        this.A09 = C12970iu.A10(r5);
    }

    public static String A00(AbstractActivityC452520u r4, String str) {
        Log.i(str);
        C91794Td r3 = r4.A09;
        TextView textView = r3.A04;
        if (textView == null) {
            return r4.A0I.A02(((ActivityC13830kP) r4).A01, r3.A06);
        }
        return textView.getText().toString();
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C64323Fc r5;
        String str;
        String str2;
        C64323Fc r1;
        int i;
        int i2;
        Object[] A1a;
        String A00;
        AnonymousClass1R1 r0;
        AnonymousClass01T r2 = (AnonymousClass01T) obj;
        AbstractActivityC452520u r3 = (AbstractActivityC452520u) this.A09.get();
        if (r3 != null) {
            C36021jC.A00(r3, 9);
            r3.A00 = null;
            boolean z = r3 instanceof RegisterPhone;
            if (z) {
                ((RegisterPhone) r3).A0H = null;
            }
            Object obj2 = r2.A00;
            AnonymousClass009.A05(obj2);
            int A05 = C12960it.A05(obj2);
            C29131Qz r4 = (C29131Qz) r2.A01;
            String str3 = this.A06;
            String str4 = this.A08;
            long j = this.A00;
            if (r4 != null) {
                AbstractActivityC452520u.A0T = r4.A08;
                AbstractActivityC452520u.A0P = r4.A00;
                int i3 = r4.A02;
                if (i3 > 0) {
                    C12970iu.A1B(C12960it.A08(((ActivityC13810kN) r3).A09), "registration_voice_code_length", i3);
                }
                int i4 = r4.A01;
                if (i4 > 0) {
                    C12970iu.A1B(C12960it.A08(((ActivityC13810kN) r3).A09), "registration_sms_code_length", i4);
                }
            }
            if (A05 != 4) {
                if (A05 != 3) {
                    r3.A0J = null;
                    if (A05 == 1) {
                        Log.i("enterphone/reinstalled");
                        r3.AQ2();
                        AnonymousClass009.A05(r4);
                        C18350sJ r02 = r3.A0D;
                        String str5 = r4.A0I;
                        r02.A0C(str3, str4, str5);
                        ((ActivityC13810kN) r3).A09.A12(r4.A0J);
                        r3.A2g(str3, str4, str5);
                        return;
                    }
                    if (A05 != 2) {
                        if (A05 == 5) {
                            Log.e("enterphone/blocked");
                            if (!(r4 == null || (r0 = r4.A06) == null)) {
                                r3.A0H = r0;
                                r3.A0F.A00 = r0.A00;
                            }
                            StringBuilder A0k = C12960it.A0k("+");
                            A0k.append(str3);
                            r3.A0J = C12960it.A0d(str4, A0k);
                            AbstractActivityC452520u.A0R = str3;
                            AbstractActivityC452520u.A0S = str4;
                            StringBuilder A0j = C12960it.A0j("+");
                            A0j.append(str3);
                            r3.A0J = C12960it.A0d(str4, A0j);
                            if (r4 == null || !r4.A0E || !z) {
                                int i5 = 124;
                                if (!r3.A0B.A02) {
                                    if (C12980iv.A1W(((ActivityC13810kN) r3).A09.A00, "underage_account_banned")) {
                                        i5 = 125;
                                    }
                                    C36021jC.A01(r3, i5);
                                    return;
                                }
                                return;
                            }
                            RegisterPhone registerPhone = (RegisterPhone) r3;
                            if (!((AbstractActivityC452520u) registerPhone).A0B.A02) {
                                registerPhone.A0X = true;
                                C36021jC.A01(registerPhone, 21);
                                return;
                            }
                            return;
                        } else if (A05 != 4) {
                            if (A05 != 3) {
                                if (A05 == 6) {
                                    A00 = A00(r3, "enterphone/phone-number-too-long");
                                    r5 = r3.A0B;
                                    i2 = R.string.register_bad_phone_too_long;
                                } else if (A05 == 7) {
                                    A00 = A00(r3, "enterphone/phone-number-too-short");
                                    r5 = r3.A0B;
                                    i2 = R.string.register_bad_phone_too_short;
                                } else if (A05 == 8) {
                                    String A002 = A00(r3, "enterphone/phone-number-bad-format");
                                    AnonymousClass018 r22 = ((ActivityC13830kP) r3).A01;
                                    StringBuilder A0k2 = C12960it.A0k("+");
                                    A0k2.append((Object) r3.A09.A02.getText());
                                    A0k2.append(AnonymousClass01V.A06);
                                    String A0G = r22.A0G(C12970iu.A0s(r3.A09.A03.getText(), A0k2));
                                    r5 = r3.A0B;
                                    i2 = R.string.register_bad_format_with_number;
                                    A1a = C12980iv.A1a();
                                    C12970iu.A1U(A0G, A002, A1a);
                                    str = r3.getString(i2, A1a);
                                    r5.A03(str);
                                    return;
                                } else {
                                    if (A05 == 9) {
                                        Log.i("enterphone/temporarily-unavailable");
                                        AnonymousClass009.A05(r4);
                                        String str6 = r4.A07;
                                        if (str6 == null) {
                                            r1 = r3.A0B;
                                            i = R.string.register_temporarily_unavailable;
                                        } else {
                                            try {
                                                long parseLong = Long.parseLong(str6) * 1000;
                                                AbstractActivityC452520u.A0Q = SystemClock.elapsedRealtime() + parseLong;
                                                r3.A0D.A0B(parseLong);
                                                r3.A0B.A03(C12960it.A0X(r3, C38131nZ.A08(((ActivityC13830kP) r3).A01, parseLong), C12970iu.A1b(), 0, R.string.register_temporarily_unavailable_with_time));
                                                return;
                                            } catch (NumberFormatException unused) {
                                                r1 = r3.A0B;
                                                i = R.string.register_temporarily_unavailable;
                                            }
                                        }
                                    } else if (A05 == 12) {
                                        Log.i("enterphone/old-version");
                                        r3.A02.A01 = true;
                                        r3.A0B.A01(114);
                                        return;
                                    } else {
                                        if (A05 == 14) {
                                            str2 = "enterphone/bad-token";
                                        } else if (A05 == 15) {
                                            str2 = "enterphone/invalid-skey";
                                        } else if (A05 == 11) {
                                            Log.w("enterphone/too-recent");
                                            if (j != 0) {
                                                long j2 = j * 1000;
                                                try {
                                                    AbstractActivityC452520u.A0Q = SystemClock.elapsedRealtime() + j2;
                                                    r3.A0D.A0B(j2);
                                                    r3.A0B.A03(C12960it.A0X(r3, C38131nZ.A08(((ActivityC13830kP) r3).A01, j2), C12970iu.A1b(), 0, R.string.register_try_is_too_recent));
                                                    return;
                                                } catch (NumberFormatException e) {
                                                    Log.w("enterphone/too-recent/time-not-int", e);
                                                    r1 = r3.A0B;
                                                    i = R.string.register_try_is_too_recent_unspecified;
                                                }
                                            } else {
                                                Log.w("enterphone/too-recent/time-not-int");
                                                r1 = r3.A0B;
                                                i = R.string.register_try_is_too_recent_unspecified;
                                            }
                                        } else if (A05 == 16) {
                                            r3.AQ2();
                                            r3.A0D.A0A(7);
                                            AnonymousClass009.A05(r4);
                                            ((ActivityC13810kN) r3).A09.A0w(r4.A0C, r4.A0B, r4.A05, -1, -1, ((ActivityC13790kL) r3).A05.A00());
                                            AbstractActivityC452520u.A0R = str3;
                                            AbstractActivityC452520u.A0S = str4;
                                            ((ActivityC13810kN) r3).A09.A0v(str3, str4);
                                            r3.A0E.A02("enter_number", "successful");
                                            boolean A02 = r3.A01.A02();
                                            Intent A0A = C12970iu.A0A();
                                            A0A.setClassName(r3.getPackageName(), "com.whatsapp.registration.VerifyTwoFactorAuth");
                                            A0A.putExtra("changenumber", A02);
                                            r3.A2G(A0A, false);
                                            r3.finish();
                                            return;
                                        } else if (A05 == 19) {
                                            Log.i("enterphone/onStatusNeedsAccountDefenceSecondCode");
                                            r3.A0N = true;
                                        } else {
                                            return;
                                        }
                                        Log.i(str2);
                                        r3.A0B.A03(C12960it.A0X(r3, "https://whatsapp.com/android", C12970iu.A1b(), 0, R.string.register_should_upgrade_website));
                                        return;
                                    }
                                    r1.A02(i);
                                    return;
                                }
                                A1a = C12970iu.A1b();
                                A1a[0] = A00;
                                str = r3.getString(i2, A1a);
                                r5.A03(str);
                                return;
                            }
                        }
                    }
                    Log.i("enterphone/new-installation");
                    AnonymousClass23M.A0J(((ActivityC13810kN) r3).A09, AnonymousClass23M.A00);
                    r3.A2f(15);
                    r3.AQ2();
                    AnonymousClass009.A05(r4);
                    r3.A0M = r4.A0D;
                    r3.AT0(r4.A09, r4.A0A, r4.A0F);
                    return;
                }
                Log.i("enterphone/error-connectivity");
                r5 = r3.A0B;
                str = C12960it.A0X(r3, r3.getString(R.string.connectivity_self_help_instructions), C12970iu.A1b(), 0, R.string.register_check_connectivity);
                r5.A03(str);
                return;
            }
            Log.i("enterphone/error-unspecified");
            if (!r3.A0B.A02) {
                C36021jC.A01(r3, 109);
            }
        }
    }
}
