package X;

import java.io.IOException;

/* renamed from: X.4R8  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R8 {
    public final int A00;
    public final C28721Ot A01;
    public final C28731Ou A02;
    public final IOException A03;

    public AnonymousClass4R8(C28721Ot r1, C28731Ou r2, IOException iOException, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = iOException;
        this.A00 = i;
    }
}
