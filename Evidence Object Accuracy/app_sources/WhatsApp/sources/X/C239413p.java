package X;

import android.content.SharedPreferences;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0800000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.13p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239413p implements AbstractC18870tC, AbstractC15920o8 {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15550nR A02;
    public final C20720wD A03;
    public final C14820m6 A04;
    public final C22130yZ A05;
    public final C18770sz A06;
    public final C20660w7 A07;
    public final C17230qT A08;
    public final ExecutorC27271Gr A09;
    public final AbstractC14440lR A0A;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{204};
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    public C239413p(AbstractC15710nm r3, C15570nT r4, C15550nR r5, C20720wD r6, C14820m6 r7, C22130yZ r8, C18770sz r9, C20660w7 r10, C17230qT r11, AbstractC14440lR r12) {
        this.A00 = r3;
        this.A0A = r12;
        this.A01 = r4;
        this.A07 = r10;
        this.A02 = r5;
        this.A03 = r6;
        this.A06 = r9;
        this.A08 = r11;
        this.A04 = r7;
        this.A05 = r8;
        this.A09 = new ExecutorC27271Gr(r12, false);
    }

    public final synchronized void A00() {
        SharedPreferences sharedPreferences = this.A04.A00;
        Set<String> stringSet = sharedPreferences.getStringSet("pending_side_list_hash", new HashSet());
        AnonymousClass009.A05(stringSet);
        stringSet.size();
        if (!stringSet.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (String str : stringSet) {
                arrayList.add(new C50722Qr(null, str));
            }
            this.A09.execute(new RunnableBRunnable0Shape0S0800000_I0(this.A00, this.A01, this.A07, this.A02, this.A03, this.A06, this.A05, arrayList, 1));
            sharedPreferences.edit().remove("pending_side_list_hash").apply();
        }
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        boolean z;
        String A0H;
        AnonymousClass1V8 A0E;
        byte[] bArr;
        long A09;
        C50722Qr r3;
        boolean z2 = false;
        if (i != 204) {
            return false;
        }
        AnonymousClass1OT r4 = (AnonymousClass1OT) message.getData().getParcelable("stanzaKey");
        AnonymousClass009.A06(r4, "stanzaKey is null");
        AnonymousClass2QR r1 = (AnonymousClass2QR) this.A08.A00(2, r4.A00);
        if (r1 != null) {
            r1.A02(3);
        }
        C18770sz r12 = this.A06;
        if (r12.A06.A05()) {
            AnonymousClass1V8 r7 = (AnonymousClass1V8) message.obj;
            AnonymousClass1V8 A0D = r7.A0D(0);
            if (!(r1 == null || A0D == null)) {
                r1.A00 = A0D.A00;
            }
            if (r7.A0E("update") != null) {
                z = true;
                r3 = new C50722Qr(r4, r7.A0F("update").A0H("hash"));
            } else {
                z = false;
                AnonymousClass1V8 A0E2 = r7.A0E("add");
                AnonymousClass1V8 A0E3 = r7.A0E("remove");
                if (A0E2 != null) {
                    A0H = A0E2.A0H("device_hash");
                    A0E = A0E2.A0E("key-index-list");
                } else if (A0E3 != null) {
                    A0H = A0E3.A0H("device_hash");
                    A0E = A0E3.A0E("key-index-list");
                } else {
                    Log.e("DeviceUpdateNotificationHandler/handleXmppMessage/unknown type of device notification.");
                    throw new AnonymousClass1V9("unknown device notification not found");
                }
                AbstractC15710nm r13 = this.A00;
                UserJid userJid = (UserJid) r7.A0B(r13, UserJid.class, "from");
                AnonymousClass009.A05(A0H);
                C28601Of A01 = C40981sh.A01(r13, A0E2);
                C28601Of A012 = C40981sh.A01(r13, A0E3);
                if (A0E == null) {
                    bArr = null;
                    A09 = 0;
                } else {
                    bArr = A0E.A01;
                    A09 = A0E.A09(A0E.A0H("ts"), "ts");
                }
                r3 = new C50722Qr(A01, A012, userJid, r4, A0H, bArr, A09);
            }
            if (r7.A0I("offline", null) != null) {
                z2 = true;
            }
            if (z) {
                if (z2) {
                    String str = r3.A06;
                    synchronized (this) {
                        if (!TextUtils.isEmpty(str)) {
                            SharedPreferences sharedPreferences = this.A04.A00;
                            Set<String> stringSet = sharedPreferences.getStringSet("pending_side_list_hash", new HashSet());
                            AnonymousClass009.A05(stringSet);
                            if (stringSet.add(str)) {
                                sharedPreferences.edit().putStringSet("pending_side_list_hash", stringSet).apply();
                            }
                        }
                    }
                }
                this.A09.execute(new RunnableBRunnable0Shape0S0800000_I0(this.A00, this.A01, this.A07, this.A02, this.A03, r12, this.A05, Collections.singletonList(r3), 1));
                return true;
            }
            if (z2) {
                C20720wD r5 = this.A03;
                if (r5.A06.A07(560)) {
                    UserJid userJid2 = r3.A03;
                    AnonymousClass009.A05(userJid2);
                    Set singleton = Collections.singleton(userJid2);
                    synchronized (r5) {
                        Set A00 = r5.A00();
                        if (A00.addAll(singleton)) {
                            r5.A04.A00.edit().putStringSet("pending_users_to_sync_device", new HashSet(Arrays.asList(C15380n4.A0Q(A00)))).apply();
                        }
                    }
                }
            }
            this.A09.execute(new RunnableBRunnable0Shape0S0800000_I0(this.A00, this.A01, this.A07, this.A02, this.A03, r12, this.A05, Collections.singletonList(r3), 1));
            return true;
        }
        this.A07.A0E(r4);
        return true;
    }

    @Override // X.AbstractC18870tC
    public void ARC() {
        if (this.A03.A06.A07(560)) {
            this.A0A.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 41));
        }
    }
}
