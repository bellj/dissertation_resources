package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;

/* renamed from: X.33R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33R extends AbstractC64423Fm {
    public static final int[] A01 = {R.drawable.ic_emoji_people, R.drawable.ic_emoji_nature, R.drawable.ic_emoji_food, R.drawable.ic_emoji_activity, R.drawable.ic_emoji_travel, R.drawable.ic_emoji_objects, R.drawable.ic_emoji_symbols, R.drawable.ic_emoji_flags};
    public static final int[] A02 = {R.string.emoji_label_people, R.string.emoji_label_nature, R.string.emoji_label_food, R.string.emoji_label_activity, R.string.emoji_label_travel, R.string.emoji_label_objects, R.string.emoji_label_symbols, R.string.emoji_label_flags};
    public final C54642h3 A00;

    public AnonymousClass33R(RecyclerView recyclerView, C89554Kk r6, ShapePickerRecyclerView shapePickerRecyclerView) {
        super(recyclerView, r6, shapePickerRecyclerView, false);
        Resources A09 = C12960it.A09(recyclerView);
        C54642h3 r0 = new C54642h3(A09.getDimensionPixelSize(R.dimen.shape_picker_emoji_subcategory_portrait_width), A09.getDimensionPixelSize(R.dimen.shape_picker_emoji_subcategory_item_landscape_width), A09.getDimensionPixelSize(R.dimen.shape_picker_emoji_subcategory_portrait_padding));
        this.A00 = r0;
        recyclerView.A0l(r0);
    }

    @Override // X.AbstractC64423Fm
    public void A02(C75453js r8, boolean z) {
        int i;
        super.A02(r8, z);
        ImageView imageView = r8.A01;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        RecyclerView recyclerView = this.A06;
        Resources A09 = C12960it.A09(recyclerView);
        int i2 = R.dimen.shape_picker_subcategory_selected_portrait_dimen;
        if (z) {
            i2 = R.dimen.shape_picker_subcategory_selected_landscape_dimen;
        }
        int dimensionPixelSize = A09.getDimensionPixelSize(i2);
        float f = (float) dimensionPixelSize;
        float dimensionPixelSize2 = (float) C12960it.A09(recyclerView).getDimensionPixelSize(R.dimen.shape_picker_emoji_subcategory_icon_dimen);
        float f2 = 1.0f;
        if (z) {
            f2 = 0.9f;
        }
        int i3 = ((int) (f - (dimensionPixelSize2 * f2))) >> 1;
        layoutParams.width = dimensionPixelSize;
        layoutParams.height = dimensionPixelSize;
        imageView.setLayoutParams(layoutParams);
        imageView.setPadding(i3, i3, i3, i3);
        View view = r8.A0H;
        ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
        if (z) {
            i = C12960it.A09(recyclerView).getDimensionPixelOffset(R.dimen.shape_picker_emoji_subcategory_item_landscape_width);
        } else {
            i = -2;
        }
        layoutParams2.width = i;
        view.setLayoutParams(layoutParams2);
        this.A00.A00 = z;
    }
}
