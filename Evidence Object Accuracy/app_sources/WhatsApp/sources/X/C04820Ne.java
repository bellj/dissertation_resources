package X;

import android.content.Context;

/* renamed from: X.0Ne  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04820Ne {
    public final Context A00;
    public final AnonymousClass0SX A01;
    public final String A02;
    public final boolean A03;

    public C04820Ne(Context context, AnonymousClass0SX r2, String str, boolean z) {
        this.A00 = context;
        this.A02 = str;
        this.A01 = r2;
        this.A03 = z;
    }
}
