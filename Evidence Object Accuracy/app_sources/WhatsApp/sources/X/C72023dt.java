package X;

import com.whatsapp.payments.ui.BusinessHubActivity;
import com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel;

/* renamed from: X.3dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72023dt extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ BusinessHubActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72023dt(BusinessHubActivity businessHubActivity) {
        super(0);
        this.this$0 = businessHubActivity;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AnonymousClass015 A00 = C13000ix.A02(this.this$0).A00(BusinessHubViewModel.class);
        C16700pc.A0B(A00);
        return A00;
    }
}
