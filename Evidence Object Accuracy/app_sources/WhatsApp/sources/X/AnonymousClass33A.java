package X;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Picture;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.33A  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass33A extends AnonymousClass45J {
    public final Context A00;

    @Override // X.AbstractC454821u
    public boolean A0K() {
        return false;
    }

    public AnonymousClass33A(Context context) {
        this.A00 = context;
    }

    public static Picture A02(Context context, String str) {
        try {
            AssetManager assets = context.getAssets();
            StringBuilder A0h = C12960it.A0h();
            A0h.append("graphics/");
            String A0d = C12960it.A0d(str, A0h);
            C06560Ud r0 = new C06560Ud();
            InputStream open = assets.open(A0d);
            try {
                AnonymousClass0Q5 A0N = r0.A0N(open);
                if (A0N != null) {
                    return A0N.A00(null);
                }
                return null;
            } finally {
                try {
                    open.close();
                } catch (IOException unused) {
                }
            }
        } catch (C11160fq | IOException e) {
            Log.e(C12960it.A0d(str, C12960it.A0k("failed to load SVG from ")), e);
            return null;
        }
    }

    @Override // X.AnonymousClass45J
    public float A0R() {
        if (!(this instanceof AnonymousClass33M)) {
            return 0.0f;
        }
        AnonymousClass33M r0 = (AnonymousClass33M) this;
        Picture picture = r0.A04;
        if ((picture == null && (picture = r0.A05) == null) || picture.getHeight() == 0) {
            return 0.0f;
        }
        return ((float) picture.getWidth()) / ((float) picture.getHeight());
    }
}
