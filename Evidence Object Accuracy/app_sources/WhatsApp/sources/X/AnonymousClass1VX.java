package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1VX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VX extends AbstractC15970oE {
    public static final AnonymousClass1VX A00 = new AnonymousClass1VX();
    public static final Parcelable.Creator CREATOR = new C100084lL();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "broadcast";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 5;
    }

    public AnonymousClass1VX() {
        super("status");
    }

    public AnonymousClass1VX(Parcel parcel) {
        super(parcel);
    }
}
