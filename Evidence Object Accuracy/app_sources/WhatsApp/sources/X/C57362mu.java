package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57362mu extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57362mu A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public AnonymousClass1K6 A01 = AnonymousClass277.A01;
    public C57182mc A02;
    public String A03 = "";

    static {
        C57362mu r0 = new C57362mu();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C82103v5 r1;
        switch (r7.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57362mu r9 = (C57362mu) obj2;
                this.A01 = r8.Afr(this.A01, r9.A01);
                this.A02 = (C57182mc) r8.Aft(this.A02, r9.A02);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                String str = this.A03;
                int i2 = r9.A00;
                this.A03 = r8.Afy(str, r9.A03, A1V, C12960it.A1V(i2 & 2, 2));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            AnonymousClass1K6 r12 = this.A01;
                            if (!((AnonymousClass1K7) r12).A00) {
                                r12 = AbstractC27091Fz.A0G(r12);
                                this.A01 = r12;
                            }
                            r12.add((C57192md) AbstractC27091Fz.A0H(r82, r92, C57192md.A03));
                        } else if (A03 == 18) {
                            if ((this.A00 & 1) == 1) {
                                r1 = (C82103v5) this.A02.A0T();
                            } else {
                                r1 = null;
                            }
                            C57182mc r0 = (C57182mc) AbstractC27091Fz.A0H(r82, r92, C57182mc.A03);
                            this.A02 = r0;
                            if (r1 != null) {
                                this.A02 = (C57182mc) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 1;
                        } else if (A03 == 26) {
                            String A0A = r82.A0A();
                            this.A00 |= 2;
                            this.A03 = A0A;
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A01);
                return null;
            case 4:
                return new C57362mu();
            case 5:
                return new C82113v6();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57362mu.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A01.size(); i3++) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G1) this.A01.get(i3), 1, i2);
        }
        if ((this.A00 & 1) == 1) {
            C57182mc r0 = this.A02;
            if (r0 == null) {
                r0 = C57182mc.A03;
            }
            i2 = AbstractC27091Fz.A08(r0, 2, i2);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(3, this.A03, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        int i = 0;
        while (i < this.A01.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A01, i, 1);
        }
        if ((this.A00 & 1) == 1) {
            C57182mc r0 = this.A02;
            if (r0 == null) {
                r0 = C57182mc.A03;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(3, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
