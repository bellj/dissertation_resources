package X;

import com.whatsapp.payments.ui.NoviPayBloksActivity;

/* renamed from: X.68i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328168i implements AnonymousClass1FK {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayBloksActivity A01;

    public C1328168i(AnonymousClass3FE r1, NoviPayBloksActivity noviPayBloksActivity) {
        this.A01 = noviPayBloksActivity;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        C117305Zk.A1E(this.A00);
        ((AbstractActivityC123635nW) this.A01).A05.A02(r3, null, null);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        C117305Zk.A1E(this.A00);
        ((AbstractActivityC123635nW) this.A01).A05.A02(r3, null, null);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        NoviPayBloksActivity noviPayBloksActivity = this.A01;
        if (!((AbstractActivityC123635nW) noviPayBloksActivity).A04.A0K.get()) {
            ((AbstractActivityC123635nW) noviPayBloksActivity).A04.A07(C117305Zk.A09(this.A00, this, 38));
            return;
        }
        C117315Zl.A0T(this.A00);
        NoviPayBloksActivity.A1S(noviPayBloksActivity);
    }
}
