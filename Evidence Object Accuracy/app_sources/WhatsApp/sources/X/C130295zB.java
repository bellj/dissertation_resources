package X;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.5zB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130295zB {
    public static AnonymousClass04S A00(Context context, C126995tm r3, C126995tm r4, String str, String str2, boolean z) {
        C004802e A0S = C12980iv.A0S(context);
        A0S.A0A(str2);
        A0S.A0B(z);
        C117295Zj.A0q(A0S, r3, 73, r3.A00);
        A0S.A08(new DialogInterface.OnCancelListener() { // from class: X.626
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                Runnable runnable = C126995tm.this.A01;
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        if (!TextUtils.isEmpty(str)) {
            A0S.setTitle(str);
        }
        if (r4 != null) {
            C117305Zk.A18(A0S, r4, 71, r4.A00);
        }
        return A0S.create();
    }

    public static AnonymousClass04S A01(Context context, C126995tm r3, String str) {
        C004802e A0S = C12980iv.A0S(context);
        A0S.A0A(str);
        A0S.A0B(false);
        C117295Zj.A0q(A0S, r3, 72, R.string.ok);
        A0S.A08(new DialogInterface.OnCancelListener() { // from class: X.625
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                Runnable runnable = C126995tm.this.A01;
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        return A0S.create();
    }
}
