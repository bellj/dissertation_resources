package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106354vZ implements AnonymousClass5WW {
    public final C14260l7 A00;
    public final AnonymousClass2k7 A01;

    public /* synthetic */ C106354vZ(C14260l7 r1, AnonymousClass2k7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        this.A01.A06((View) obj, this.A00, ((AnonymousClass2k7) obj2).A00, obj3);
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
        this.A01.A07((View) obj, this.A00, ((AnonymousClass2k7) obj2).A00, obj3);
    }
}
