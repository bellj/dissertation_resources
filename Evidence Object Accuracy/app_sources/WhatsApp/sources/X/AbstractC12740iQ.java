package X;

import android.graphics.drawable.Drawable;
import android.widget.ListAdapter;

/* renamed from: X.0iQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12740iQ {
    Drawable AAm();

    CharSequence ADM();

    int ADN();

    int AHX();

    boolean AK4();

    void Abi(ListAdapter listAdapter);

    void Abn(Drawable drawable);

    void AcC(int i);

    void AcD(int i);

    void Ace(CharSequence charSequence);

    void Ad7(int i);

    void Adf(int i, int i2);

    void dismiss();
}
