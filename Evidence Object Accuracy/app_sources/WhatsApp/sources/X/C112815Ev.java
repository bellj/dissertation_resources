package X;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.5Ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112815Ev implements AnonymousClass1WO {
    public final AtomicReference A00;

    public C112815Ev(AnonymousClass1WO r2) {
        this.A00 = new AtomicReference(r2);
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        AnonymousClass1WO r0 = (AnonymousClass1WO) this.A00.getAndSet(null);
        if (r0 != null) {
            return r0.iterator();
        }
        throw C12960it.A0U("This sequence can be consumed only once.");
    }
}
