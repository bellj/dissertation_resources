package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.GroupChatLiveLocationsActivity;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.LocationPicker2;

/* renamed from: X.15n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244415n extends AnonymousClass13N {
    public static final String[] A02 = AnonymousClass13N.A05;
    public long A00 = -1;
    public final AnonymousClass12P A01;

    public C244415n(AnonymousClass12P r3, C15570nT r4, AnonymousClass01d r5, C14830m7 r6, C14820m6 r7) {
        super(r4, r5, r6, r7);
        this.A01 = r3;
    }

    public void A07(Activity activity, AbstractC14640lm r5) {
        Class cls;
        if (A06(activity)) {
            cls = LocationPicker2.class;
        } else {
            cls = LocationPicker.class;
        }
        Intent intent = new Intent(activity, cls);
        intent.putExtra("jid", r5.getRawString());
        intent.putExtra("live_location_mode", true);
        activity.startActivityForResult(intent, 100);
    }

    public void A08(Context context, AbstractC14640lm r5, UserJid userJid) {
        Class cls;
        if (A06(context)) {
            cls = GroupChatLiveLocationsActivity2.class;
        } else {
            cls = GroupChatLiveLocationsActivity.class;
        }
        Intent intent = new Intent(context, cls);
        intent.putExtra("jid", r5.getRawString());
        intent.putExtra("target", C15380n4.A03(userJid));
        context.startActivity(intent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00dd, code lost:
        if (r7 >= 702000000) goto L_0x00df;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(android.content.Context r18, java.lang.String r19, java.lang.String r20, double r21, double r23) {
        /*
        // Method dump skipped, instructions count: 419
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C244415n.A09(android.content.Context, java.lang.String, java.lang.String, double, double):void");
    }
}
