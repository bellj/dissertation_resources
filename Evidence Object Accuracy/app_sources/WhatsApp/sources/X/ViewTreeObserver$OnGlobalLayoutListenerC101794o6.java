package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import com.whatsapp.R;
import com.whatsapp.softenforcementsmb.SMBSoftEnforcementEducationFragment;

/* renamed from: X.4o6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101794o6 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ScrollView A01;
    public final /* synthetic */ SMBSoftEnforcementEducationFragment A02;

    public ViewTreeObserver$OnGlobalLayoutListenerC101794o6(View view, ScrollView scrollView, SMBSoftEnforcementEducationFragment sMBSoftEnforcementEducationFragment) {
        this.A02 = sMBSoftEnforcementEducationFragment;
        this.A01 = scrollView;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        ScrollView scrollView = this.A01;
        int i = 0;
        if (C93004Yo.A01(scrollView)) {
            i = R.drawable.smb_soft_enforcement_acknowledgement_background;
        }
        this.A00.setBackgroundResource(i);
        C12980iv.A1E(scrollView, this);
    }
}
