package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99144jp implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C56492ky r1 = null;
        C78463ox r2 = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                r1 = (C56492ky) C95664e9.A07(parcel, C56492ky.CREATOR, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                r2 = (C78463ox) C95664e9.A07(parcel, C78463ox.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78323oj(r1, r2, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78323oj[i];
    }
}
