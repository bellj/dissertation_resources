package X;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.5I2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5I2 extends AbstractList<Integer> implements RandomAccess, Serializable {
    public static final long serialVersionUID = 0;
    public final int[] array;
    public final int end;
    public final int start;

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public boolean isEmpty() {
        return false;
    }

    public AnonymousClass5I2(int[] iArr, int i, int i2) {
        this.array = iArr;
        this.start = i;
        this.end = i2;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public boolean contains(Object obj) {
        if (obj instanceof Integer) {
            int[] iArr = this.array;
            int A05 = C12960it.A05(obj);
            int i = this.start;
            int i2 = this.end;
            while (true) {
                if (i >= i2) {
                    break;
                } else if (iArr[i] != A05) {
                    i++;
                } else if (i == -1) {
                    break;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof AnonymousClass5I2)) {
                return super.equals(obj);
            }
            AnonymousClass5I2 r8 = (AnonymousClass5I2) obj;
            int size = size();
            if (r8.size() == size) {
                for (int i = 0; i < size; i++) {
                    if (this.array[this.start + i] == r8.array[r8.start + i]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object get(int i) {
        C28291Mn.A01(i, size());
        return Integer.valueOf(this.array[this.start + i]);
    }

    @Override // java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public int hashCode() {
        int i = 1;
        for (int i2 = this.start; i2 < this.end; i2++) {
            i = (i * 31) + this.array[i2];
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        if (obj instanceof Integer) {
            int[] iArr = this.array;
            int A05 = C12960it.A05(obj);
            int i = this.start;
            int i2 = this.end;
            int i3 = i;
            while (true) {
                if (i3 >= i2) {
                    break;
                } else if (iArr[i3] == A05) {
                    int i4 = i3 - i;
                    if (i3 < 0) {
                        break;
                    }
                    return i4;
                } else {
                    i3++;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public int lastIndexOf(Object obj) {
        if (obj instanceof Integer) {
            int[] iArr = this.array;
            int A05 = C12960it.A05(obj);
            int i = this.start;
            int i2 = this.end;
            while (true) {
                i2--;
                if (i2 < i) {
                    break;
                } else if (iArr[i2] == A05) {
                    int i3 = i2 - i;
                    if (i2 < 0) {
                        break;
                    }
                    return i3;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        C28291Mn.A01(i, size());
        int[] iArr = this.array;
        int i2 = this.start + i;
        int i3 = iArr[i2];
        iArr[i2] = ((Number) obj).intValue();
        return Integer.valueOf(i3);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public int size() {
        return this.end - this.start;
    }

    @Override // java.util.AbstractList, java.util.List
    public List subList(int i, int i2) {
        C28291Mn.A03(i, i2, size());
        if (i == i2) {
            return Collections.emptyList();
        }
        int[] iArr = this.array;
        int i3 = this.start;
        return new AnonymousClass5I2(iArr, i + i3, i3 + i2);
    }

    @Override // java.util.AbstractCollection, java.lang.Object
    public String toString() {
        StringBuilder A0t = C12980iv.A0t(size() * 5);
        A0t.append('[');
        int[] iArr = this.array;
        int i = this.start;
        while (true) {
            A0t.append(iArr[i]);
            i++;
            if (i >= this.end) {
                return C72453ed.A0t(A0t);
            }
            A0t.append(", ");
        }
    }
}
