package X;

import android.util.SparseArray;
import android.util.SparseIntArray;

/* renamed from: X.4x1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C107244x1 implements AnonymousClass5VY {
    public final int A00;
    public final SparseArray A01 = new SparseArray();
    public final SparseIntArray A02 = new SparseIntArray();
    public final C95054d0 A03 = new C95054d0(new byte[5], 5);
    public final /* synthetic */ C106734wC A04;

    @Override // X.AnonymousClass5VY
    public void AIe(AbstractC14070ko r1, C92824Xo r2, AnonymousClass4YB r3) {
    }

    public C107244x1(C106734wC r4, int i) {
        this.A04 = r4;
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0221, code lost:
        if (r0 == 2) goto L_0x01c7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x01dd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01bd  */
    @Override // X.AnonymousClass5VY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r33) {
        /*
        // Method dump skipped, instructions count: 920
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107244x1.A7a(X.4dT):void");
    }
}
