package X;

/* renamed from: X.0oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C16260oi extends AbstractC16110oT {
    public String A00;
    public String A01;

    public C16260oi() {
        super(3146, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAcsSigningCredential {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "acsCredential", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "acsPublicKey", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
