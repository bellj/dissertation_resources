package X;

import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.3UI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UI implements AbstractC116455Vm {
    public final /* synthetic */ MessageReplyActivity A00;

    public AnonymousClass3UI(MessageReplyActivity messageReplyActivity) {
        this.A00 = messageReplyActivity;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0i);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        MessageReplyActivity messageReplyActivity = this.A00;
        if (messageReplyActivity.A12.A0P == null) {
            AbstractC36671kL.A08(messageReplyActivity.A0i, iArr, 0);
        }
    }
}
