package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78473oy extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98694j6();
    public final int A00;
    public final String A01;

    @Override // java.lang.Object
    public final int hashCode() {
        return this.A00;
    }

    public C78473oy(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof C78473oy) {
                C78473oy r5 = (C78473oy) obj;
                if (r5.A00 != this.A00 || !C13300jT.A00(r5.A01, this.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final String toString() {
        int i = this.A00;
        String str = this.A01;
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 12);
        A0t.append(i);
        A0t.append(":");
        return C12960it.A0d(str, A0t);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0C(parcel, this.A01, 2, A00);
    }
}
