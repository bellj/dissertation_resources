package X;

/* renamed from: X.0J0  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0J0 {
    /* Fake field, exist only in values array */
    ALWAYS_ANIMATED,
    /* Fake field, exist only in values array */
    DISABLED,
    NEVER_ANIMATED,
    /* Fake field, exist only in values array */
    ONLY_ANIMATED_WHILE_LOADING
}
