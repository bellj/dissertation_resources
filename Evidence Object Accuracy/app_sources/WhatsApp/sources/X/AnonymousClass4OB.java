package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.4OB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4OB {
    public final DeviceJid A00;
    public final VoipStanzaChildNode A01;

    public AnonymousClass4OB(DeviceJid deviceJid, VoipStanzaChildNode voipStanzaChildNode) {
        this.A00 = deviceJid;
        this.A01 = voipStanzaChildNode;
    }
}
