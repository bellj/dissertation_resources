package X;

/* renamed from: X.39r  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass39r {
    A01(1),
    A02(2),
    A03(3),
    A06(4),
    A04(5),
    A05(0);
    
    public final int value;

    AnonymousClass39r(int i) {
        this.value = i;
    }
}
