package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;

/* renamed from: X.2YW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YW extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ Interpolator A01;
    public final /* synthetic */ ViewTreeObserver$OnPreDrawListenerC66503Nv A02;

    public AnonymousClass2YW(View view, Interpolator interpolator, ViewTreeObserver$OnPreDrawListenerC66503Nv r3) {
        this.A02 = r3;
        this.A00 = view;
        this.A01 = interpolator;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        WallpaperPreview wallpaperPreview = this.A02.A04;
        wallpaperPreview.A06.setBackgroundColor(wallpaperPreview.getResources().getColor(R.color.primary_surface));
        wallpaperPreview.A0D = false;
        wallpaperPreview.A09.setScrollEnabled(true);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        WallpaperPreview wallpaperPreview = this.A02.A04;
        wallpaperPreview.A06.setBackgroundColor(wallpaperPreview.getResources().getColor(R.color.primary_surface));
        View view = this.A00;
        if (view != null) {
            C12980iv.A0Q(view, 1.0f).translationY(0.0f).setInterpolator(this.A01);
        }
        C12980iv.A0Q(wallpaperPreview.A07, 1.0f).setInterpolator(this.A01).setListener(new IDxLAdapterShape0S0100000_1_I1(this, 5));
    }
}
