package X;

import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/* renamed from: X.15Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15Z {
    public static final long A07 = TimeUnit.DAYS.toMillis(5);
    public final C14830m7 A00;
    public final C16590pI A01;
    public final AnonymousClass15S A02;
    public final C20470vo A03;
    public final AbstractC20460vn A04;
    public final Semaphore A05 = new Semaphore(1);
    public volatile File A06 = null;

    public AnonymousClass15Z(C14830m7 r3, C16590pI r4, AnonymousClass15S r5, C20470vo r6, AbstractC20460vn r7) {
        this.A01 = r4;
        this.A00 = r3;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
    }

    public void A00(File file) {
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                this.A04.A9b(e.getMessage());
            }
        }
    }

    public final File[] A01(String str) {
        File[] listFiles;
        File file = new File(this.A01.A00.getCacheDir(), "qpl");
        if (!file.exists() || (listFiles = file.listFiles(new FilenameFilter(str) { // from class: X.1OD
            public final /* synthetic */ String A00;

            {
                this.A00 = r1;
            }

            @Override // java.io.FilenameFilter
            public final boolean accept(File file2, String str2) {
                return str2.endsWith(this.A00);
            }
        })) == null) {
            return new File[0];
        }
        return listFiles;
    }
}
