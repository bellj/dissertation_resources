package X;

import android.view.MenuItem;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* renamed from: X.4mR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100764mR implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ ContactPickerFragment A00;

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    public MenuItem$OnActionExpandListenerC100764mR(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        ContactPickerFragment contactPickerFragment = this.A00;
        contactPickerFragment.A1z = null;
        contactPickerFragment.A1K();
        return true;
    }
}
