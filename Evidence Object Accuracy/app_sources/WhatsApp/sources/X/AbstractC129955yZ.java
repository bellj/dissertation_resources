package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.5yZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC129955yZ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C241414j A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final C17070qD A07;
    public final AnonymousClass60T A08;
    public final C129385xd A09;

    public AbstractC129955yZ(Context context, C14900mE r2, C15570nT r3, C14830m7 r4, C241414j r5, C18650sn r6, C18610sj r7, C17070qD r8, AnonymousClass60T r9, C129385xd r10) {
        this.A03 = r4;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A09 = r10;
        this.A06 = r7;
        this.A05 = r6;
        this.A08 = r9;
    }

    public void A00() {
        Log.i("PAY: BrazilStepUpVerificationBase getProviderEncryptionKeyAsync");
        AnonymousClass60T r8 = this.A08;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r8, "VISA", "STEP-UP");
        if (A0C == null) {
            new C129215xM(this.A00, this.A01, this.A05, this.A06, r8, "STEP-UP").A00(new C133346Ak(this), "VISA");
            return;
        }
        A01(null, A0C);
    }

    public void A01(C452120p r5, AnonymousClass6B7 r6) {
        C128765wd r0;
        C128915ws r02;
        if (!(this instanceof C120355g3)) {
            C120365g4 r3 = (C120365g4) this;
            if (r5 != null) {
                r02 = r3.A05;
            } else {
                String A03 = r3.A03.A03(r6, r3.A09);
                if (TextUtils.isEmpty(A03)) {
                    Log.e("PAY: BrazilVerifyCardOTPSendAction encryptAndSendOtp/enc otp failure");
                    r02 = r3.A05;
                    r5 = C117305Zk.A0L();
                } else {
                    Log.i("PAY: BrazilVerifyCardOTPSendAction encryptAndSendOtp");
                    r3.A03(A03);
                    return;
                }
            }
            r02.A00(null, r5);
            return;
        }
        C120355g3 r2 = (C120355g3) this;
        if (r5 != null) {
            Log.e(C12960it.A0d(r5.A09, C12960it.A0k("PAY: BrazilVerifyCardSendAuthCodeAction  onProviderKeyFetched auth code failure ")));
            r0 = r2.A04;
        } else {
            String A032 = r2.A03.A03(r6, r2.A05);
            if (TextUtils.isEmpty(A032)) {
                Log.e("PAY: BrazilVerifyCardSendAuthCodeAction  onProviderKeyFetched auth code failure");
                r0 = r2.A04;
                r5 = C117305Zk.A0L();
            } else {
                Log.i("PAY: BrazilVerifyCardSendAuthCodeAction onProviderKeyFetched success");
                r2.A03(A032);
                return;
            }
        }
        r0.A00(r5);
    }

    public void A02(C452120p r12, String str) {
        C120365g4 r3 = (C120365g4) this;
        if (r12 != null || str == null) {
            r3.A05.A00(null, r12);
            return;
        }
        ((AbstractC129955yZ) r3).A09.A00(str);
        AnonymousClass60T r9 = ((AbstractC129955yZ) r3).A08;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r9, "ELO", "ADD-CARD");
        if (A0C == null) {
            new C129215xM(r3.A00, ((AbstractC129955yZ) r3).A01, ((AbstractC129955yZ) r3).A05, ((AbstractC129955yZ) r3).A06, r9, "ADD-CARD").A00(new C133356Al(r3), "FB");
            return;
        }
        r3.A03(r3.A03.A02((AnonymousClass6B5) A0C.A00, r3.A09));
    }
}
