package X;

import android.net.Uri;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3St  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67753St implements AnonymousClass2BW {
    public long A00;
    public Uri A01 = Uri.EMPTY;
    public Map A02 = Collections.emptyMap();
    public final AnonymousClass2BW A03;

    public C67753St(AnonymousClass2BW r2) {
        this.A03 = r2;
    }

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r2) {
        this.A03.A5p(r2);
    }

    @Override // X.AnonymousClass2BW
    public Map AGF() {
        return this.A03.AGF();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A03.AHS();
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r5) {
        this.A01 = r5.A04;
        this.A02 = Collections.emptyMap();
        AnonymousClass2BW r3 = this.A03;
        long AYZ = r3.AYZ(r5);
        this.A01 = r3.AHS();
        this.A02 = r3.AGF();
        return AYZ;
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A03.close();
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        int read = this.A03.read(bArr, i, i2);
        if (read != -1) {
            this.A00 += (long) read;
        }
        return read;
    }
}
