package X;

/* renamed from: X.43o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856343o extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public C856343o() {
        super(2642, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(21, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(22, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(19, this.A05);
        r3.Abe(20, this.A06);
        r3.Abe(24, this.A07);
        r3.Abe(23, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidInfraHealth {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psDailyStartsBgCold", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psDailyStartsCold", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psDailyStartsFgCold", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psDailyStartsLukeWarm", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psDailyStartsWarms", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastColdStartInMin", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastEventInMin", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastLukewarmStartInMin", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastWarmStartInMin", this.A08);
        return C12960it.A0d("}", A0k);
    }
}
