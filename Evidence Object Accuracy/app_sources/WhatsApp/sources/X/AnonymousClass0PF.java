package X;

import android.text.TextUtils;
import androidx.preference.Preference;

/* renamed from: X.0PF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PF {
    public int A00;
    public int A01;
    public String A02;

    public AnonymousClass0PF(Preference preference) {
        this.A02 = preference.getClass().getName();
        this.A00 = preference.A01;
        this.A01 = preference.A03;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass0PF)) {
            return false;
        }
        AnonymousClass0PF r4 = (AnonymousClass0PF) obj;
        if (this.A00 == r4.A00 && this.A01 == r4.A01 && TextUtils.equals(this.A02, r4.A02)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((527 + this.A00) * 31) + this.A01) * 31) + this.A02.hashCode();
    }
}
