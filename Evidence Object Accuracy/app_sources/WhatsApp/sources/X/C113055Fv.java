package X;

import java.math.BigInteger;

/* renamed from: X.5Fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113055Fv implements AnonymousClass20L {
    public BigInteger A00;
    public BigInteger A01;
    public BigInteger A02;
    public AnonymousClass4X3 A03;

    public C113055Fv(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, AnonymousClass4X3 r4) {
        this.A00 = bigInteger3;
        this.A01 = bigInteger;
        this.A02 = bigInteger2;
        this.A03 = r4;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C113055Fv)) {
            return false;
        }
        C113055Fv r4 = (C113055Fv) obj;
        return r4.A01.equals(this.A01) && r4.A02.equals(this.A02) && r4.A00.equals(this.A00);
    }

    public int hashCode() {
        return (this.A01.hashCode() ^ this.A02.hashCode()) ^ this.A00.hashCode();
    }
}
