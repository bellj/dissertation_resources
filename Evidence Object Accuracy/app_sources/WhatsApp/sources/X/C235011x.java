package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;

/* renamed from: X.11x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C235011x implements AbstractC18870tC, AbstractC18270sB {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C20720wD A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C15680nj A05;
    public final C15600nX A06;
    public final C234911w A07;
    public final C18770sz A08;
    public final C14850m9 A09;
    public final AbstractC14440lR A0A;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    public C235011x(C15570nT r1, C15550nR r2, C20720wD r3, C14830m7 r4, C14820m6 r5, C15680nj r6, C15600nX r7, C234911w r8, C18770sz r9, C14850m9 r10, AbstractC14440lR r11) {
        this.A03 = r4;
        this.A09 = r10;
        this.A00 = r1;
        this.A0A = r11;
        this.A01 = r2;
        this.A02 = r3;
        this.A08 = r9;
        this.A07 = r8;
        this.A04 = r5;
        this.A05 = r6;
        this.A06 = r7;
    }

    @Override // X.AbstractC18870tC
    public void ARC() {
        ARF();
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        C15570nT r0 = this.A00;
        r0.A08();
        if (r0.A05 != null) {
            this.A0A.Ab4(new RunnableBRunnable0Shape6S0100000_I0_6(this, 16), "DeviceADVInfoChecker/checkDeviceListADVInfo");
        }
    }
}
