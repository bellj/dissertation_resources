package X;

/* renamed from: X.2WK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2WK extends AnonymousClass2Vu implements AbstractC51662Vw {
    public final C15370n3 A00;

    public AnonymousClass2WK(C15370n3 r2) {
        super(r2, 3);
        this.A00 = r2;
    }

    @Override // X.AbstractC51662Vw
    public AbstractC14640lm ADg() {
        return (AbstractC14640lm) this.A00.A0B(AbstractC14640lm.class);
    }
}
