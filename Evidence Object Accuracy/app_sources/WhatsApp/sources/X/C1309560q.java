package X;

import java.util.Arrays;

/* renamed from: X.60q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309560q {
    public static byte[] A00(byte[] bArr, int i, int i2) {
        if (i2 > bArr.length || i2 < i) {
            throw new IndexOutOfBoundsException();
        }
        int i3 = i2 - i;
        byte[] bArr2 = new byte[i3];
        System.arraycopy(bArr, i, bArr2, 0, i3);
        return bArr2;
    }

    public static final byte[] A01(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, boolean z) {
        AnonymousClass5GB r3 = new AnonymousClass5GB(new C71653dH());
        r3.AIf(new C113035Ft(new AnonymousClass20K(bArr), bArr3, bArr4, 128), z);
        int length = bArr2.length;
        byte[] bArr5 = new byte[r3.AEp(length)];
        try {
            r3.A97(bArr5, r3.AZZ(bArr2, 0, length, bArr5, 0));
            return bArr5;
        } catch (C114965Nt unused) {
            return null;
        }
    }

    public byte[] A02(C127675us r10, byte[] bArr, byte[] bArr2, boolean z) {
        C29361Rw A00 = C29361Rw.A00();
        byte[] bArr3 = A00.A02.A01;
        byte[][] A06 = C16050oM.A06(C32891cu.A01(AnonymousClass4F7.A00(A00.A01, r10.A02), bArr3, "walibra_hkdf_info".getBytes(), 44), 32, 12);
        byte[] bArr4 = A06[0];
        byte[] bArr5 = A06[1];
        if (z) {
            int length = 128 - (bArr.length % 128);
            byte[] bArr6 = new byte[length];
            Arrays.fill(bArr6, (byte) length);
            bArr = C16050oM.A05(bArr, bArr6);
        }
        byte[] A01 = A01(bArr4, bArr, bArr5, bArr2, true);
        byte b = 0;
        if (bArr2 != null) {
            b = (byte) 2;
        }
        if (z) {
            b = (byte) (b | 1);
        }
        byte[] A05 = C16050oM.A05(new byte[]{4}, r10.A03, new byte[]{b}, bArr3);
        if (bArr2 != null) {
            A05 = C16050oM.A05(A05, bArr2);
        }
        return C16050oM.A05(A05, A01);
    }
}
