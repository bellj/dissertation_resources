package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.facebook.animated.webp.WebPImage;
import com.facebook.webpsupport.WebpBitmapFactoryImpl;
import com.whatsapp.stickers.WebpInfo;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.0zK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22590zK {
    public final C16590pI A00;
    public volatile C39041pA A01;
    public volatile boolean A02;

    public C22590zK(C16590pI r2) {
        this.A00 = r2;
        try {
            AnonymousClass1Q8.A00(r2.A00);
        } catch (IOException unused) {
        }
    }

    public static WebPImage A00(byte[] bArr) {
        try {
            return WebPImage.createFromByteArray(bArr);
        } catch (IllegalArgumentException e) {
            Log.e("WebPImageLoader/createWebPImageFromBytes/failed to create webp image object", e);
            return null;
        } catch (UnsatisfiedLinkError e2) {
            Log.e("WebPImageLoader/createWebPImageFromBytes/unsatisfiedLinkError", e2);
            return null;
        }
    }

    public static final String A01(String str, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str.replace("/", "-"));
        sb.append("_");
        sb.append(i);
        sb.append("_");
        sb.append(i2);
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006c A[Catch: IllegalStateException -> 0x0070, TryCatch #0 {IllegalStateException -> 0x0070, blocks: (B:2:0x0000, B:4:0x0007, B:5:0x000d, B:8:0x0026, B:9:0x002a, B:10:0x002d, B:11:0x0033, B:16:0x0065, B:19:0x006c), top: B:24:0x0000 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Bitmap A02(com.facebook.animated.webp.WebPImage r10, java.lang.String r11, int r12, int r13) {
        /*
            r9 = this;
            int r0 = r10.getFrameCount()     // Catch: IllegalStateException -> 0x0070
            r2 = 0
            if (r0 <= 0) goto L_0x006a
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch: OutOfMemoryError -> 0x0062, IllegalStateException -> 0x0070
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r12, r13, r0)     // Catch: OutOfMemoryError -> 0x0062, IllegalStateException -> 0x0070
            r0 = 0
            com.facebook.animated.webp.WebPFrame r3 = r10.getFrame(r0)     // Catch: IllegalStateException -> 0x0070
            int r8 = r3.getWidth()     // Catch: IllegalStateException -> 0x0070
            int r7 = r3.getHeight()     // Catch: IllegalStateException -> 0x0070
            int r1 = r10.getWidth()     // Catch: IllegalStateException -> 0x0070
            int r0 = r10.getHeight()     // Catch: IllegalStateException -> 0x0070
            if (r0 != r7) goto L_0x002a
            if (r1 != r8) goto L_0x002a
            r3.renderFrame(r12, r13, r4)     // Catch: IllegalStateException -> 0x0070
            goto L_0x0069
        L_0x002a:
            float r5 = (float) r12     // Catch: IllegalStateException -> 0x0070
            float r0 = (float) r1     // Catch: IllegalStateException -> 0x0070
            float r5 = r5 / r0
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch: OutOfMemoryError -> 0x005e, IllegalStateException -> 0x0070
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r12, r13, r0)     // Catch: OutOfMemoryError -> 0x005e, IllegalStateException -> 0x0070
            float r0 = (float) r8     // Catch: IllegalStateException -> 0x0070
            float r0 = r0 * r5
            double r0 = (double) r0     // Catch: IllegalStateException -> 0x0070
            double r0 = java.lang.Math.ceil(r0)     // Catch: IllegalStateException -> 0x0070
            int r8 = (int) r0     // Catch: IllegalStateException -> 0x0070
            float r0 = (float) r7     // Catch: IllegalStateException -> 0x0070
            float r0 = r0 * r5
            double r0 = (double) r0     // Catch: IllegalStateException -> 0x0070
            double r0 = java.lang.Math.ceil(r0)     // Catch: IllegalStateException -> 0x0070
            int r7 = (int) r0     // Catch: IllegalStateException -> 0x0070
            r3.renderFrame(r8, r7, r6)     // Catch: IllegalStateException -> 0x0070
            android.graphics.Canvas r7 = new android.graphics.Canvas     // Catch: IllegalStateException -> 0x0070
            r7.<init>(r4)     // Catch: IllegalStateException -> 0x0070
            int r0 = r3.getXOffset()     // Catch: IllegalStateException -> 0x0070
            float r1 = (float) r0     // Catch: IllegalStateException -> 0x0070
            float r1 = r1 * r5
            int r0 = r3.getYOffset()     // Catch: IllegalStateException -> 0x0070
            float r0 = (float) r0     // Catch: IllegalStateException -> 0x0070
            float r0 = r0 * r5
            r7.drawBitmap(r6, r1, r0, r2)     // Catch: IllegalStateException -> 0x0070
            r6.recycle()     // Catch: IllegalStateException -> 0x0070
            goto L_0x0069
        L_0x005e:
            r1 = move-exception
            java.lang.String r0 = "WebPImageLoader/createStaticImage creating framebitmap"
            goto L_0x0065
        L_0x0062:
            r1 = move-exception
            java.lang.String r0 = "WebPImageLoader/createStaticImage creating bitmap"
        L_0x0065:
            com.whatsapp.util.Log.e(r0, r1)     // Catch: IllegalStateException -> 0x0070
            goto L_0x006a
        L_0x0069:
            r2 = r4
        L_0x006a:
            if (r2 == 0) goto L_0x006f
            r9.A07(r2, r11)     // Catch: IllegalStateException -> 0x0070
        L_0x006f:
            return r2
        L_0x0070:
            r1 = move-exception
            java.lang.String r0 = "WebPImageLoader/createBitmapFromWebPImageAndCache/failed to create static image"
            com.whatsapp.util.Log.e(r0, r1)
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22590zK.A02(com.facebook.animated.webp.WebPImage, java.lang.String, int, int):android.graphics.Bitmap");
    }

    public Bitmap A03(File file, String str, int i, int i2) {
        String A01 = A01(str, i, i2);
        Bitmap A04 = A04(A01);
        if (A04 == null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.outHeight = i2;
            options.outWidth = i;
            WebpInfo verifyWebpFileIntegrity = WebpUtils.verifyWebpFileIntegrity(file.getAbsolutePath());
            if (verifyWebpFileIntegrity == null || verifyWebpFileIntegrity.numFrames != 1) {
                A04 = null;
            } else {
                options.inSampleSize = C37501mV.A00(verifyWebpFileIntegrity.width, verifyWebpFileIntegrity.height, i, i2, 1);
                A04 = WebpBitmapFactoryImpl.hookDecodeFile(file.getAbsolutePath(), options);
            }
            if (A04 != null) {
                A07(A04, A01);
            } else {
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        C14350lI.A0G(fileInputStream, byteArrayOutputStream);
                        Bitmap A05 = A05(str, byteArrayOutputStream.toByteArray(), i, i2);
                        byteArrayOutputStream.close();
                        fileInputStream.close();
                        return A05;
                    } catch (Throwable th) {
                        try {
                            fileInputStream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (FileNotFoundException | IOException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadStaticImageAsBitmapFromFile/getting sticker bitmap failed:");
                    sb.append(str);
                    Log.e(sb.toString(), e);
                    return null;
                }
            }
        }
        return A04;
    }

    public final Bitmap A04(String str) {
        C39051pB r0;
        C39041pA A06 = A06();
        if (A06 != null) {
            try {
                r0 = A06.A08(str);
            } catch (IOException e) {
                Log.e("WebPImageLoader/error getting bitmap from cache", e);
                r0 = null;
            }
            if (r0 == null) {
                return null;
            }
            try {
                InputStream inputStream = r0.A00[0];
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
                if (inputStream != null) {
                    inputStream.close();
                }
                return decodeStream;
            } catch (IOException unused) {
            }
        }
        return null;
    }

    public Bitmap A05(String str, byte[] bArr, int i, int i2) {
        String A01 = A01(str, i, i2);
        Bitmap A04 = A04(A01);
        if (A04 != null) {
            return A04;
        }
        WebPImage A00 = A00(bArr);
        if (A00 == null) {
            return null;
        }
        return A02(A00, A01, i, i2);
    }

    public final C39041pA A06() {
        if (!this.A02) {
            synchronized (this) {
                if (!this.A02) {
                    File file = new File(this.A00.A00.getCacheDir(), "webp_static_cache");
                    if (file.exists() || file.mkdirs()) {
                        try {
                            this.A01 = C39041pA.A00(file, 2097152);
                        } catch (IOException e) {
                            Log.e("WebPImageLoader/getDiskLruCache error opening cache", e);
                        }
                    } else {
                        Log.e("WebPImageLoader/getDiskLruCache could not init directory");
                    }
                    this.A02 = true;
                }
            }
        }
        return this.A01;
    }

    public final void A07(Bitmap bitmap, String str) {
        C39041pA A06 = A06();
        if (A06 != null) {
            try {
                C39061pE A07 = A06.A07(str);
                if (A07 != null) {
                    OutputStream A00 = A07.A00();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    try {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        A00.write(byteArray, 0, byteArray.length);
                        A07.A01();
                        byteArrayOutputStream.close();
                        A00.close();
                    } catch (Throwable th) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e) {
                Log.e("WebPImageLoader/saving bitmap to cache", e);
            }
        }
    }
}
