package X;

/* renamed from: X.4wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107084wl implements AbstractC116585Wa {
    public long A00 = -1;
    public long A01 = -1;
    public C89884Lt A02;
    public C95424dg A03;

    public C107084wl(C89884Lt r3, C95424dg r4) {
        this.A03 = r4;
        this.A02 = r3;
    }

    @Override // X.AbstractC116585Wa
    public AnonymousClass5WY A8W() {
        long j = this.A00;
        C95314dV.A04(C12960it.A1S((j > -1 ? 1 : (j == -1 ? 0 : -1))));
        return new C106894wS(this.A03, j);
    }

    @Override // X.AbstractC116585Wa
    public long AZo(AnonymousClass5Yf r9) {
        long j = this.A01;
        if (j < 0) {
            return -1;
        }
        long j2 = -(j + 2);
        this.A01 = -1;
        return j2;
    }

    @Override // X.AbstractC116585Wa
    public void AeD(long j) {
        long[] jArr = this.A02.A01;
        this.A01 = jArr[AnonymousClass3JZ.A06(jArr, j, true)];
    }
}
