package X;

import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.0uB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19480uB {
    public final HashMap A00 = new HashMap();

    public void A00(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        C32761ch r1 = new C32761ch(bArr, bArr3, i);
        HashMap hashMap = this.A00;
        hashMap.put(new C32771ci(str, bArr2), r1);
        StringBuilder sb = new StringBuilder("BackupCipherKeys/updateKeyForBackupFile v=");
        sb.append(str);
        Log.i(sb.toString());
        hashMap.size();
    }
}
