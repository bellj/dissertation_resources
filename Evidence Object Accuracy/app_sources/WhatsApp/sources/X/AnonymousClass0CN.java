package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.0CN  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0CN extends AnonymousClass0XP implements AbstractC12690iL, PopupWindow.OnDismissListener, View.OnKeyListener, AdapterView.OnItemClickListener {
    public int A00;
    public int A01 = 0;
    public View A02;
    public View A03;
    public ViewTreeObserver A04;
    public PopupWindow.OnDismissListener A05;
    public AbstractC12280hf A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final Context A0D;
    public final View.OnAttachStateChangeListener A0E = new AnonymousClass0W6(this);
    public final ViewTreeObserver.OnGlobalLayoutListener A0F = new AnonymousClass0WZ(this);
    public final AnonymousClass0BQ A0G;
    public final AnonymousClass07H A0H;
    public final C02410Ce A0I;
    public final boolean A0J;

    @Override // X.AnonymousClass0XP
    public void A06(AnonymousClass07H r1) {
    }

    @Override // X.AbstractC12690iL
    public boolean AA1() {
        return false;
    }

    public AnonymousClass0CN(Context context, View view, AnonymousClass07H r6, int i, int i2, boolean z) {
        this.A0D = context;
        this.A0H = r6;
        this.A0J = z;
        this.A0G = new AnonymousClass0BQ(LayoutInflater.from(context), r6, R.layout.abc_popup_menu_item_layout, z);
        this.A0B = i;
        this.A0C = i2;
        Resources resources = context.getResources();
        this.A0A = Math.max(resources.getDisplayMetrics().widthPixels >> 1, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
        this.A02 = view;
        this.A0I = new C02410Ce(context, i, i2);
        r6.A08(context, this);
    }

    @Override // X.AnonymousClass0XP
    public void A01(int i) {
        this.A01 = i;
    }

    @Override // X.AnonymousClass0XP
    public void A02(int i) {
        this.A0I.A02 = i;
    }

    @Override // X.AnonymousClass0XP
    public void A03(int i) {
        this.A0I.Ad7(i);
    }

    @Override // X.AnonymousClass0XP
    public void A04(View view) {
        this.A02 = view;
    }

    @Override // X.AnonymousClass0XP
    public void A05(PopupWindow.OnDismissListener onDismissListener) {
        this.A05 = onDismissListener;
    }

    @Override // X.AnonymousClass0XP
    public void A07(boolean z) {
        this.A0G.A02 = z;
    }

    @Override // X.AnonymousClass0XP
    public void A08(boolean z) {
        this.A08 = z;
    }

    @Override // X.AbstractC12600iB
    public ListView ADv() {
        return this.A0I.A0E;
    }

    @Override // X.AbstractC12600iB
    public boolean AK4() {
        return !this.A09 && this.A0I.A0D.isShowing();
    }

    @Override // X.AbstractC12690iL
    public void AOF(AnonymousClass07H r2, boolean z) {
        if (r2 == this.A0H) {
            dismiss();
            AbstractC12280hf r0 = this.A06;
            if (r0 != null) {
                r0.AOF(r2, z);
            }
        }
    }

    @Override // X.AbstractC12690iL
    public boolean AWs(AnonymousClass0CK r14) {
        if (r14.hasVisibleItems()) {
            C05530Px r6 = new C05530Px(this.A0D, this.A03, r14, this.A0B, this.A0C, this.A0J);
            AbstractC12280hf r1 = this.A06;
            r6.A04 = r1;
            AnonymousClass0XP r0 = r6.A03;
            if (r0 != null) {
                r0.Abr(r1);
            }
            int size = r14.size();
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                MenuItem item = r14.getItem(i);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i++;
            }
            r6.A05 = z;
            AnonymousClass0XP r02 = r6.A03;
            if (r02 != null) {
                r02.A07(z);
            }
            r6.A02 = this.A05;
            this.A05 = null;
            this.A0H.A0F(false);
            C02410Ce r03 = this.A0I;
            int i2 = r03.A02;
            int AHX = r03.AHX();
            if ((Gravity.getAbsoluteGravity(this.A01, AnonymousClass028.A05(this.A02)) & 7) == 5) {
                i2 += this.A02.getWidth();
            }
            AnonymousClass0XP r04 = r6.A03;
            if (r04 == null || !r04.AK4()) {
                if (r6.A01 != null) {
                    AnonymousClass0XP A00 = r6.A00();
                    A00.A08(true);
                    if ((C05660Ql.A00(r6.A00, AnonymousClass028.A05(r6.A01)) & 7) == 5) {
                        i2 -= r6.A01.getWidth();
                    }
                    A00.A02(i2);
                    A00.A03(AHX);
                    int i3 = (int) ((r6.A08.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
                    A00.A00 = new Rect(i2 - i3, AHX - i3, i2 + i3, AHX + i3);
                    A00.Ade();
                }
            }
            AbstractC12280hf r05 = this.A06;
            if (r05 == null) {
                return true;
            }
            r05.ATF(r14);
            return true;
        }
        return false;
    }

    @Override // X.AbstractC12690iL
    public void Abr(AbstractC12280hf r1) {
        this.A06 = r1;
    }

    @Override // X.AbstractC12600iB
    public void Ade() {
        View view;
        Rect rect;
        if (AK4()) {
            return;
        }
        if (this.A09 || (view = this.A02) == null) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
        this.A03 = view;
        C02410Ce r6 = this.A0I;
        PopupWindow popupWindow = r6.A0D;
        popupWindow.setOnDismissListener(this);
        r6.A0B = this;
        r6.A0H = true;
        popupWindow.setFocusable(true);
        View view2 = this.A03;
        boolean z = false;
        if (this.A04 == null) {
            z = true;
        }
        ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
        this.A04 = viewTreeObserver;
        if (z) {
            viewTreeObserver.addOnGlobalLayoutListener(this.A0F);
        }
        view2.addOnAttachStateChangeListener(this.A0E);
        r6.A0A = view2;
        ((AnonymousClass0XR) r6).A00 = this.A01;
        if (!this.A07) {
            this.A00 = AnonymousClass0XP.A00(this.A0D, this.A0G, this.A0A);
            this.A07 = true;
        }
        r6.A01(this.A00);
        popupWindow.setInputMethodMode(2);
        Rect rect2 = super.A00;
        if (rect2 != null) {
            rect = new Rect(rect2);
        } else {
            rect = null;
        }
        r6.A09 = rect;
        r6.Ade();
        C02360Bs r4 = r6.A0E;
        r4.setOnKeyListener(this);
        if (this.A08) {
            AnonymousClass07H r3 = this.A0H;
            if (r3.A05 != null) {
                FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.A0D).inflate(R.layout.abc_popup_menu_header_item_layout, (ViewGroup) r4, false);
                TextView textView = (TextView) frameLayout.findViewById(16908310);
                if (textView != null) {
                    textView.setText(r3.A05);
                }
                frameLayout.setEnabled(false);
                r4.addHeaderView(frameLayout, null, false);
            }
        }
        r6.Abi(this.A0G);
        r6.Ade();
    }

    @Override // X.AbstractC12690iL
    public void AfQ(boolean z) {
        this.A07 = false;
        this.A0G.notifyDataSetChanged();
    }

    @Override // X.AbstractC12600iB
    public void dismiss() {
        if (AK4()) {
            this.A0I.dismiss();
        }
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        this.A09 = true;
        this.A0H.close();
        ViewTreeObserver viewTreeObserver = this.A04;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.A04 = this.A03.getViewTreeObserver();
            }
            this.A04.removeGlobalOnLayoutListener(this.A0F);
            this.A04 = null;
        }
        this.A03.removeOnAttachStateChangeListener(this.A0E);
        PopupWindow.OnDismissListener onDismissListener = this.A05;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @Override // android.view.View.OnKeyListener
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }
}
