package X;

import android.text.TextUtils;

/* renamed from: X.1Um  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29741Um {
    public static final String[] A00 = {""};

    public static void A00(C16330op r4) {
        if (TextUtils.isEmpty(AnonymousClass1Uj.A00(r4, "table", "message_ftsv2"))) {
            r4.A0B("CREATE VIRTUAL TABLE message_ftsv2 USING FTS4(content, fts_jid, fts_namespace)");
            C29731Ul.A00(r4, "fts_index_start", "FtsTable", 0);
        }
    }
}
