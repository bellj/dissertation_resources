package X;

/* renamed from: X.3wS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82953wS extends AbstractC94534c0 {
    public final Boolean A00;

    public /* synthetic */ C82953wS(CharSequence charSequence) {
        this.A00 = Boolean.valueOf(Boolean.parseBoolean(charSequence.toString()));
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C82953wS)) {
                return false;
            }
            Boolean bool = this.A00;
            Boolean bool2 = ((C82953wS) obj).A00;
            if (bool != null) {
                if (!bool.equals(bool2)) {
                    return false;
                }
            } else if (bool2 != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return this.A00.toString();
    }
}
