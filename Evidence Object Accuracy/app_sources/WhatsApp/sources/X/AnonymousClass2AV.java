package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.2AV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AV extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass2AS A01;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public AnonymousClass2AV(AnonymousClass2AS r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i;
        if (f < 1.0f) {
            int i2 = this.A00;
            i = i2 - ((int) (((float) i2) * f));
        } else {
            i = 0;
        }
        AnonymousClass2AS r1 = this.A01;
        r1.getLayoutParams().height = i;
        r1.requestLayout();
    }
}
