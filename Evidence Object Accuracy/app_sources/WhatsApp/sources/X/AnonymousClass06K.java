package X;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

/* renamed from: X.06K  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass06K implements AnonymousClass06D {
    public static final Locale A02 = AnonymousClass06C.A03("en-Latn");
    public static final Locale A03 = new Locale("ar", "XB");
    public static final Locale A04 = new Locale("en", "XA");
    public static final Locale[] A05 = new Locale[0];
    public final String A00;
    public final Locale[] A01;

    @Override // X.AnonymousClass06D
    public Object ADx() {
        return null;
    }

    public AnonymousClass06K(Locale... localeArr) {
        String obj;
        int length = localeArr.length;
        if (length == 0) {
            this.A01 = A05;
            obj = "";
        } else {
            ArrayList arrayList = new ArrayList();
            HashSet hashSet = new HashSet();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            do {
                Locale locale = localeArr[i];
                if (locale != null) {
                    if (!hashSet.contains(locale)) {
                        Locale locale2 = (Locale) locale.clone();
                        arrayList.add(locale2);
                        sb.append(locale2.getLanguage());
                        String country = locale2.getCountry();
                        if (country != null && !country.isEmpty()) {
                            sb.append('-');
                            sb.append(locale2.getCountry());
                        }
                        if (i < length - 1) {
                            sb.append(',');
                        }
                        hashSet.add(locale2);
                    }
                    i++;
                } else {
                    StringBuilder sb2 = new StringBuilder("list[");
                    sb2.append(i);
                    sb2.append("] is null");
                    throw new NullPointerException(sb2.toString());
                }
            } while (i < length);
            this.A01 = (Locale[]) arrayList.toArray(new Locale[0]);
            obj = sb.toString();
        }
        this.A00 = obj;
    }

    @Override // X.AnonymousClass06D
    public Locale AAS(int i) {
        Locale[] localeArr = this.A01;
        if (0 < localeArr.length) {
            return localeArr[0];
        }
        return null;
    }

    @Override // X.AnonymousClass06D
    public String Aex() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof AnonymousClass06K) {
                Locale[] localeArr = ((AnonymousClass06K) obj).A01;
                Locale[] localeArr2 = this.A01;
                int length = localeArr2.length;
                if (length == localeArr.length) {
                    for (int i = 0; i < length; i++) {
                        if (localeArr2[i].equals(localeArr[i])) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 1;
        for (Locale locale : this.A01) {
            i = (i * 31) + locale.hashCode();
        }
        return i;
    }

    @Override // X.AnonymousClass06D
    public int size() {
        return this.A01.length;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        int i = 0;
        while (true) {
            Locale[] localeArr = this.A01;
            int length = localeArr.length;
            if (i < length) {
                sb.append(localeArr[i]);
                if (i < length - 1) {
                    sb.append(',');
                }
                i++;
            } else {
                sb.append("]");
                return sb.toString();
            }
        }
    }
}
