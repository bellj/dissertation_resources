package X;

import com.whatsapp.calling.callhistory.CallLogActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.2Ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48032Ea extends AbstractC16350or {
    public final WeakReference A00;
    public final /* synthetic */ CallLogActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C48032Ea(CallLogActivity callLogActivity, CallLogActivity callLogActivity2) {
        super(callLogActivity2);
        this.A01 = callLogActivity;
        this.A00 = new WeakReference(callLogActivity2);
    }
}
