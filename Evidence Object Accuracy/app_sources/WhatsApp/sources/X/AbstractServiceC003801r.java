package X;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import java.util.ArrayList;
import java.util.HashMap;

@Deprecated
/* renamed from: X.01r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractServiceC003801r extends Service {
    public static final Object A05 = new Object();
    public static final HashMap A06 = new HashMap();
    public AnonymousClass0AK A00;
    public AbstractC12320hj A01;
    public AnonymousClass0Q4 A02;
    public boolean A03 = false;
    public final ArrayList A04;

    public boolean A04() {
        return true;
    }

    public abstract void A05(Intent intent);

    public AbstractServiceC003801r() {
        ArrayList arrayList;
        if (Build.VERSION.SDK_INT >= 26) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
        }
        this.A04 = arrayList;
    }

    public static void A00(Context context, Intent intent, Class cls, int i) {
        ComponentName componentName = new ComponentName(context, cls);
        if (intent != null) {
            synchronized (A05) {
                HashMap hashMap = A06;
                AnonymousClass0Q4 r0 = (AnonymousClass0Q4) hashMap.get(componentName);
                if (r0 == null) {
                    if (Build.VERSION.SDK_INT >= 26) {
                        r0 = new AnonymousClass0D6(componentName, context, i);
                    } else {
                        r0 = new AnonymousClass0D7(componentName, context);
                    }
                    hashMap.put(componentName, r0);
                }
                r0.A03(i);
                r0.A04(intent);
            }
            return;
        }
        throw new IllegalArgumentException("work must not be null");
    }

    public AbstractC12330hk A01() {
        AbstractC12330hk r0;
        AbstractC12320hj r02 = this.A01;
        if (r02 != null) {
            return r02.A8p();
        }
        ArrayList arrayList = this.A04;
        synchronized (arrayList) {
            r0 = arrayList.size() > 0 ? (AbstractC12330hk) arrayList.remove(0) : null;
        }
        return r0;
    }

    public void A02() {
        ArrayList arrayList = this.A04;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.A00 = null;
                if (arrayList.size() > 0) {
                    A03(false);
                } else if (!this.A03) {
                    this.A02.A00();
                }
            }
        }
    }

    public void A03(boolean z) {
        if (this.A00 == null) {
            this.A00 = new AnonymousClass0AK(this);
            AnonymousClass0Q4 r0 = this.A02;
            if (r0 != null && z) {
                r0.A01();
            }
            this.A00.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        AbstractC12320hj r0 = this.A01;
        if (r0 != null) {
            return r0.A7S();
        }
        return null;
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.A01 = new job.JobServiceEngineC019209b(this);
            this.A02 = null;
            return;
        }
        this.A01 = null;
        ComponentName componentName = new ComponentName(this, getClass());
        HashMap hashMap = A06;
        AnonymousClass0Q4 r0 = (AnonymousClass0Q4) hashMap.get(componentName);
        if (r0 == null) {
            if (Build.VERSION.SDK_INT >= 26) {
                throw new IllegalArgumentException("Can't be here without a job id");
            }
            r0 = new AnonymousClass0D7(componentName, this);
            hashMap.put(componentName, r0);
        }
        this.A02 = r0;
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        ArrayList arrayList = this.A04;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.A03 = true;
                this.A02.A00();
            }
        }
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        ArrayList arrayList = this.A04;
        if (arrayList == null) {
            return 2;
        }
        this.A02.A02();
        synchronized (arrayList) {
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new C07300Xl(intent, this, i2));
            A03(true);
        }
        return 3;
    }
}
