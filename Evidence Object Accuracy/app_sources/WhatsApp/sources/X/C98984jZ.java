package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/* renamed from: X.4jZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98984jZ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                latLng = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                latLng2 = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new LatLngBounds(latLng, latLng2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
