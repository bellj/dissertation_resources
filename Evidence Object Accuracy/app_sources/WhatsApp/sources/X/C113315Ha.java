package X;

import java.security.cert.CRLException;

/* renamed from: X.5Ha  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113315Ha extends CRLException {
    public Throwable cause;

    public C113315Ha(Throwable th) {
        super("Exception reading IssuingDistributionPoint");
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
