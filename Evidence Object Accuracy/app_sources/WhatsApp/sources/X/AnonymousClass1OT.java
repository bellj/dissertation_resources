package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1OT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OT implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100394lq();
    public final long A00;
    public final Jid A01;
    public final Jid A02;
    public final UserJid A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final List A09;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1OT(AnonymousClass1OT r3) {
        this.A01 = r3.A01;
        this.A05 = r3.A05;
        this.A07 = r3.A07;
        this.A08 = r3.A08;
        this.A04 = r3.A04;
        this.A02 = r3.A02;
        this.A03 = r3.A03;
        this.A06 = r3.A06;
        this.A00 = r3.A00;
        List list = r3.A09;
        this.A09 = list != null ? new ArrayList(list) : null;
    }

    public AnonymousClass1OT(Parcel parcel) {
        this.A01 = (Jid) parcel.readParcelable(Jid.class.getClassLoader());
        this.A05 = parcel.readString();
        this.A07 = parcel.readString();
        this.A08 = parcel.readString();
        this.A02 = (Jid) parcel.readParcelable(Jid.class.getClassLoader());
        this.A03 = (UserJid) parcel.readParcelable(UserJid.class.getClassLoader());
        this.A06 = parcel.readString();
        this.A09 = parcel.createTypedArrayList(AnonymousClass1W9.CREATOR);
        this.A04 = parcel.readString();
        this.A00 = parcel.readLong();
    }

    public /* synthetic */ AnonymousClass1OT(Jid jid, Jid jid2, UserJid userJid, String str, String str2, String str3, String str4, String str5, List list, long j) {
        this.A01 = jid;
        this.A05 = str;
        this.A07 = str2;
        this.A08 = str3;
        this.A04 = str4;
        this.A02 = jid2;
        this.A03 = userJid;
        this.A06 = str5;
        this.A00 = j;
        this.A09 = list;
    }

    public AnonymousClass1V8 A00() {
        if (!(this instanceof AnonymousClass34J)) {
            return null;
        }
        AnonymousClass34J r7 = (AnonymousClass34J) this;
        long j = r7.A00;
        if (j <= 0 && r7.A01 <= 0) {
            return null;
        }
        DeviceJid deviceJid = r7.A02;
        int i = 1;
        int i2 = 0;
        if (deviceJid != null) {
            i2 = 1;
        }
        int i3 = i2 + 1;
        int i4 = 0;
        if (j > 0) {
            i4 = 1;
        }
        int i5 = i3 + i4;
        long j2 = r7.A01;
        int i6 = 0;
        if (j2 > 0) {
            i6 = 1;
        }
        AnonymousClass1W9[] r6 = new AnonymousClass1W9[i5 + i6];
        r6[0] = new AnonymousClass1W9("call-id", r7.A03);
        if (deviceJid != null) {
            r6[1] = new AnonymousClass1W9(deviceJid, "call-creator");
            i = 2;
        }
        if (j > 0) {
            r6[i] = new AnonymousClass1W9("audio_duration", String.valueOf(j));
            i++;
        }
        if (j2 > 0) {
            r6[i] = new AnonymousClass1W9("video_duration", String.valueOf(j2));
        }
        return new AnonymousClass1V8("terminate", r6);
    }

    public AnonymousClass1VH A01() {
        AnonymousClass1VH r4 = new AnonymousClass1VH();
        r4.A01 = this.A01;
        r4.A05 = this.A05;
        r4.A07 = this.A07;
        r4.A08 = this.A08;
        r4.A04 = this.A04;
        r4.A02 = this.A02;
        r4.A03 = this.A03;
        r4.A06 = this.A06;
        r4.A00 = this.A00;
        List<AnonymousClass1W9> list = this.A09;
        if (list != null) {
            for (AnonymousClass1W9 r2 : list) {
                r4.A09.put(r2.A02, r2);
            }
        }
        return r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r0 != null) goto L_0x001b;
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            if (r3 != r4) goto L_0x0004
            r0 = 1
            return r0
        L_0x0004:
            r2 = 0
            if (r4 == 0) goto L_0x001b
            java.lang.Class r1 = r3.getClass()
            java.lang.Class r0 = r4.getClass()
            if (r1 != r0) goto L_0x001b
            X.1OT r4 = (X.AnonymousClass1OT) r4
            java.lang.String r1 = r3.A05
            java.lang.String r0 = r4.A05
            if (r1 != 0) goto L_0x001c
            if (r0 == 0) goto L_0x0023
        L_0x001b:
            return r2
        L_0x001c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0023
            return r2
        L_0x0023:
            com.whatsapp.jid.Jid r1 = r3.A01
            com.whatsapp.jid.Jid r0 = r4.A01
            if (r1 != 0) goto L_0x002c
            if (r0 == 0) goto L_0x0033
            return r2
        L_0x002c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0033
            return r2
        L_0x0033:
            java.lang.String r1 = r3.A07
            java.lang.String r0 = r4.A07
            if (r1 != 0) goto L_0x003c
            if (r0 == 0) goto L_0x0043
            return r2
        L_0x003c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0043
            return r2
        L_0x0043:
            com.whatsapp.jid.Jid r1 = r3.A02
            com.whatsapp.jid.Jid r0 = r4.A02
            if (r1 != 0) goto L_0x004c
            if (r0 == 0) goto L_0x0053
            return r2
        L_0x004c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0053
            return r2
        L_0x0053:
            com.whatsapp.jid.UserJid r1 = r3.A03
            com.whatsapp.jid.UserJid r0 = r4.A03
            if (r1 != 0) goto L_0x005c
            if (r0 == 0) goto L_0x0063
            return r2
        L_0x005c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0063
            return r2
        L_0x0063:
            java.lang.String r1 = r3.A08
            java.lang.String r0 = r4.A08
            if (r1 != 0) goto L_0x006c
            if (r0 == 0) goto L_0x0073
            return r2
        L_0x006c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0073
            return r2
        L_0x0073:
            java.lang.String r1 = r3.A06
            java.lang.String r0 = r4.A06
            if (r1 != 0) goto L_0x007c
            if (r0 == 0) goto L_0x0083
            return r2
        L_0x007c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0083
            return r2
        L_0x0083:
            java.util.List r1 = r3.A09
            java.util.List r0 = r4.A09
            if (r1 != 0) goto L_0x008c
            if (r0 == 0) goto L_0x0093
            return r2
        L_0x008c:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0093
            return r2
        L_0x0093:
            java.lang.String r1 = r3.A04
            java.lang.String r0 = r4.A04
            boolean r0 = X.C29941Vi.A00(r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OT.equals(java.lang.Object):boolean");
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode;
        int hashCode2;
        int hashCode3;
        int hashCode4;
        int hashCode5;
        int hashCode6;
        int hashCode7;
        int hashCode8;
        String str = this.A05;
        int i = 0;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        int i2 = (hashCode + 31) * 31;
        Jid jid = this.A01;
        if (jid == null) {
            hashCode2 = 0;
        } else {
            hashCode2 = jid.hashCode();
        }
        int i3 = (i2 + hashCode2) * 31;
        String str2 = this.A07;
        if (str2 == null) {
            hashCode3 = 0;
        } else {
            hashCode3 = str2.hashCode();
        }
        int i4 = (i3 + hashCode3) * 31;
        Jid jid2 = this.A02;
        if (jid2 == null) {
            hashCode4 = 0;
        } else {
            hashCode4 = jid2.hashCode();
        }
        int i5 = (i4 + hashCode4) * 31;
        UserJid userJid = this.A03;
        if (userJid == null) {
            hashCode5 = 0;
        } else {
            hashCode5 = userJid.hashCode();
        }
        int i6 = (i5 + hashCode5) * 31;
        String str3 = this.A08;
        if (str3 == null) {
            hashCode6 = 0;
        } else {
            hashCode6 = str3.hashCode();
        }
        int i7 = (i6 + hashCode6) * 31;
        String str4 = this.A06;
        if (str4 == null) {
            hashCode7 = 0;
        } else {
            hashCode7 = str4.hashCode();
        }
        int i8 = (i7 + hashCode7) * 31;
        List list = this.A09;
        if (list == null) {
            hashCode8 = 0;
        } else {
            hashCode8 = list.hashCode();
        }
        int i9 = (i8 + hashCode8) * 31;
        String str5 = this.A04;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return i9 + i;
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        StringBuilder sb = new StringBuilder("[StanzaKey");
        Jid jid = this.A01;
        String str = "";
        if (jid == null) {
            obj = str;
        } else {
            StringBuilder sb2 = new StringBuilder(" from=");
            sb2.append(jid);
            obj = sb2.toString();
        }
        sb.append(obj);
        String str2 = this.A05;
        if (str2 == null) {
            obj2 = str;
        } else {
            StringBuilder sb3 = new StringBuilder(" cls=");
            sb3.append(str2);
            obj2 = sb3.toString();
        }
        sb.append(obj2);
        String str3 = this.A07;
        if (str3 == null) {
            obj3 = str;
        } else {
            StringBuilder sb4 = new StringBuilder(" id=");
            sb4.append(str3);
            obj3 = sb4.toString();
        }
        sb.append(obj3);
        String str4 = this.A08;
        if (str4 == null) {
            obj4 = str;
        } else {
            StringBuilder sb5 = new StringBuilder(" type=");
            sb5.append(str4);
            obj4 = sb5.toString();
        }
        sb.append(obj4);
        String str5 = this.A04;
        if (str5 == null) {
            obj5 = str;
        } else {
            StringBuilder sb6 = new StringBuilder(" category=");
            sb6.append(str5);
            obj5 = sb6.toString();
        }
        sb.append(obj5);
        Jid jid2 = this.A02;
        if (jid2 == null) {
            obj6 = str;
        } else {
            StringBuilder sb7 = new StringBuilder(" participant=");
            sb7.append(jid2);
            obj6 = sb7.toString();
        }
        sb.append(obj6);
        UserJid userJid = this.A03;
        if (userJid == null) {
            obj7 = str;
        } else {
            StringBuilder sb8 = new StringBuilder(" recipient=");
            sb8.append(userJid);
            obj7 = sb8.toString();
        }
        sb.append(obj7);
        String str6 = this.A06;
        if (str6 != null) {
            StringBuilder sb9 = new StringBuilder(" editVersion=");
            sb9.append(str6);
            str = sb9.toString();
        }
        sb.append(str);
        sb.append(" loggableStanzaId=");
        sb.append(this.A00);
        sb.append("]");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A01, i);
        parcel.writeString(this.A05);
        parcel.writeString(this.A07);
        parcel.writeString(this.A08);
        parcel.writeParcelable(this.A02, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeString(this.A06);
        parcel.writeTypedList(this.A09);
        parcel.writeString(this.A04);
        parcel.writeLong(this.A00);
    }
}
