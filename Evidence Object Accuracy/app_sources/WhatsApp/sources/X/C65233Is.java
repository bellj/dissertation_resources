package X;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.lang.reflect.Method;

/* renamed from: X.3Is  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65233Is {
    public static String A00 = "0";
    public static C95564dy A01;
    public static final C471929k A02 = C471929k.A00;
    public static final Object A03 = C12970iu.A0l();

    @Deprecated
    public static void A00(Context context) {
        synchronized (A03) {
            if (!A01()) {
                C13020j0.A02(context, "Context must not be null");
                ClassLoader classLoader = C65233Is.class.getClassLoader();
                C13020j0.A01(classLoader);
                try {
                    classLoader.loadClass("org.chromium.net.CronetEngine");
                    C471929k r7 = A02;
                    C472329r.A01(context, 11925000);
                    try {
                        C95564dy A06 = C95564dy.A06(context, C95564dy.A09, "com.google.android.gms.cronet_dynamite");
                        try {
                            Class<?> loadClass = A06.A00.getClassLoader().loadClass("org.chromium.net.impl.ImplVersion");
                            if (loadClass.getClassLoader() != C65233Is.class.getClassLoader()) {
                                Method method = loadClass.getMethod("getApiLevel", new Class[0]);
                                Method method2 = loadClass.getMethod("getCronetVersion", new Class[0]);
                                Integer num = (Integer) method.invoke(null, new Object[0]);
                                C13020j0.A01(num);
                                int intValue = num.intValue();
                                String str = (String) method2.invoke(null, new Object[0]);
                                C13020j0.A01(str);
                                A00 = str;
                                if (3 > intValue) {
                                    Intent A012 = r7.A01(context, "cr", 2);
                                    if (A012 == null) {
                                        Log.e("CronetProviderInstaller", "Unable to fetch error resolution intent");
                                        throw new AnonymousClass29w(2);
                                    }
                                    String str2 = A00;
                                    StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str2) + 174);
                                    A0t.append("Google Play Services update is required. The API Level of the client is ");
                                    A0t.append(3);
                                    A0t.append(". The API Level of the implementation is ");
                                    A0t.append(intValue);
                                    A0t.append(". The Cronet implementation version is ");
                                    throw new AnonymousClass29x(A012, C12960it.A0d(str2, A0t), 2);
                                }
                                A01 = A06;
                            } else {
                                Log.e("CronetProviderInstaller", "ImplVersion class is missing from Cronet module.");
                                throw new AnonymousClass29w(8);
                            }
                        } catch (Exception e) {
                            Log.e("CronetProviderInstaller", "Unable to read Cronet version from the Cronet module ", e);
                            throw ((AnonymousClass29w) new AnonymousClass29w(8).initCause(e));
                        }
                    } catch (AnonymousClass4CB e2) {
                        Log.e("CronetProviderInstaller", "Unable to load Cronet module", e2);
                        throw ((AnonymousClass29w) new AnonymousClass29w(8).initCause(e2));
                    }
                } catch (ClassNotFoundException e3) {
                    Log.e("CronetProviderInstaller", "Cronet API is not available. Have you included all required dependencies?");
                    throw ((AnonymousClass29w) new AnonymousClass29w(10).initCause(e3));
                }
            }
        }
    }

    public static boolean A01() {
        C95564dy r0;
        synchronized (A03) {
            r0 = A01;
        }
        return C12960it.A1W(r0);
    }
}
