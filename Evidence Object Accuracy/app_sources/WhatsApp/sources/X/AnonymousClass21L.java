package X;

import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.21L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21L extends AnonymousClass04A {
    public final /* synthetic */ AnonymousClass21J A00;
    public final /* synthetic */ WeakReference A01;

    public AnonymousClass21L(AnonymousClass21J r1, WeakReference weakReference) {
        this.A00 = r1;
        this.A01 = weakReference;
    }

    @Override // X.AnonymousClass04A
    public void A00() {
        Log.i("AppAuthManager/authenticate: authentication failed");
        AnonymousClass21K r0 = (AnonymousClass21K) this.A01.get();
        if (r0 != null) {
            r0.AMc();
        }
    }

    @Override // X.AnonymousClass04A
    public void A01(int i, CharSequence charSequence) {
        StringBuilder sb = new StringBuilder("AppAuthManager/authenticate: authentication error=");
        sb.append(i);
        sb.append(" errString=");
        sb.append((Object) charSequence);
        Log.e(sb.toString());
        AnonymousClass21K r0 = (AnonymousClass21K) this.A01.get();
        if (r0 != null) {
            r0.AMb(i, charSequence);
        }
    }

    @Override // X.AnonymousClass04A
    public void A02(int i, CharSequence charSequence) {
        StringBuilder sb = new StringBuilder("AppAuthManager/authenticate: authentication help=");
        sb.append(i);
        sb.append(" errString=");
        sb.append((Object) charSequence);
        Log.i(sb.toString());
        AnonymousClass21K r0 = (AnonymousClass21K) this.A01.get();
        if (r0 != null) {
            r0.AMe(i, charSequence);
        }
    }

    @Override // X.AnonymousClass04A
    public void A03(AnonymousClass0MS r3) {
        Log.i("AppAuthManager/authenticate: authentication succeeded");
        AnonymousClass21K r1 = (AnonymousClass21K) this.A01.get();
        if (r1 != null) {
            r1.AMf(null);
        }
    }
}
