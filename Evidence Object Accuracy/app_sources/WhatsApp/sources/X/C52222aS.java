package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;

/* renamed from: X.2aS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52222aS extends ClickableSpan {
    public final /* synthetic */ ExportMigrationActivity A00;

    public C52222aS(ExportMigrationActivity exportMigrationActivity) {
        this.A00 = exportMigrationActivity;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        ExportMigrationActivity exportMigrationActivity = this.A00;
        exportMigrationActivity.A0E.A0C(exportMigrationActivity.A0L, 2);
        ((ActivityC13790kL) exportMigrationActivity).A00.A06(exportMigrationActivity, C14960mK.A00(exportMigrationActivity));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
        C12980iv.A12(this.A00, textPaint, R.color.link_color);
    }
}
