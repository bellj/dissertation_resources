package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77063mg extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(16);
    public final String A00;
    public final String A01;
    public final String A02;

    public C77063mg(Parcel parcel) {
        super("----");
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
        this.A02 = parcel.readString();
    }

    public C77063mg(String str, String str2, String str3) {
        super("----");
        this.A01 = str;
        this.A00 = str2;
        this.A02 = str3;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77063mg.class != obj.getClass()) {
                return false;
            }
            C77063mg r5 = (C77063mg) obj;
            if (!AnonymousClass3JZ.A0H(this.A00, r5.A00) || !AnonymousClass3JZ.A0H(this.A01, r5.A01) || !AnonymousClass3JZ.A0H(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = 0;
        int A05 = (C72453ed.A05(C72453ed.A0E(this.A01)) + C72453ed.A0E(this.A00)) * 31;
        String str = this.A02;
        if (str != null) {
            i = str.hashCode();
        }
        return A05 + i;
    }

    @Override // X.AbstractC107404xH, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.A00);
        A0h.append(": domain=");
        A0h.append(this.A01);
        A0h.append(", description=");
        return C12960it.A0d(this.A00, A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(super.A00);
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
    }
}
