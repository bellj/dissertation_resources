package X;

import android.widget.SeekBar;
import android.widget.TextView;
import java.util.Formatter;

/* renamed from: X.3OT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OT implements SeekBar.OnSeekBarChangeListener {
    public final /* synthetic */ AnonymousClass39B A00;
    public final /* synthetic */ AnonymousClass21T A01;

    public AnonymousClass3OT(AnonymousClass39B r1, AnonymousClass21T r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        int i2;
        if (z && ((long) this.A01.A02()) != -9223372036854775807L) {
            AnonymousClass39B r0 = this.A00;
            TextView textView = r0.A0e;
            StringBuilder sb = r0.A0j;
            Formatter formatter = r0.A0k;
            int progress = seekBar.getProgress();
            AnonymousClass21T r02 = r0.A06;
            if (r02 != null) {
                i2 = (int) C12980iv.A0D(((long) r02.A02()) * ((long) progress));
            } else {
                i2 = 0;
            }
            textView.setText(AnonymousClass2Bd.A01(sb, formatter, (long) i2));
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        AnonymousClass39B r1 = this.A00;
        r1.A0G = true;
        r1.A02();
        r1.removeCallbacks(r1.A0i);
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        int i;
        AnonymousClass39B r4 = this.A00;
        r4.A0G = false;
        r4.A0c.setProgress(seekBar.getProgress());
        int progress = seekBar.getProgress();
        AnonymousClass21T r0 = r4.A06;
        if (r0 != null) {
            i = (int) C12980iv.A0D(((long) r0.A02()) * ((long) progress));
        } else {
            i = 0;
        }
        AnonymousClass21T r1 = this.A01;
        if (i >= r1.A02()) {
            i -= 600;
        }
        r1.A09(i);
        r4.A03(800);
        r4.A0D();
    }
}
