package X;

import android.hardware.Camera;
import android.media.MediaRecorder;
import com.whatsapp.bloks.BloksCameraOverlay;

/* renamed from: X.674  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass674 implements AnonymousClass5RJ {
    public int A00;
    public int A01;
    public int A02;
    public Camera A03;
    public MediaRecorder A04;
    public BloksCameraOverlay A05;
    public SurfaceHolder$CallbackC119475eV A06;
    public String A07;
    public String A08;
    public String A09;
    public boolean A0A;
    public boolean A0B = false;
    public boolean A0C;
}
