package X;

/* renamed from: X.0c5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08930c5 implements Comparable {
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;

    public C08930c5(String str, String str2, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = str;
        this.A03 = str2;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C08930c5 r3 = (C08930c5) obj;
        int i = this.A00 - r3.A00;
        if (i == 0) {
            return this.A01 - r3.A01;
        }
        return i;
    }
}
