package X;

/* renamed from: X.5Fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113015Fr implements AnonymousClass20L {
    public byte[] A00;

    public C113015Fr(byte[] bArr) {
        int length = bArr.length;
        if (length <= 255) {
            byte[] bArr2 = new byte[length];
            this.A00 = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, length);
            return;
        }
        throw C12970iu.A0f("RC5 key length can be no greater than 255");
    }
}
