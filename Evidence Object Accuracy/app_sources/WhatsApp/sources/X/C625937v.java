package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C625937v extends AbstractC16350or {
    public C16170oZ A00;
    public WeakReference A01;
    public final long A02;
    public final AbstractC14640lm A03;
    public final boolean A04;
    public final boolean A05;

    public C625937v(C16170oZ r2, AbstractC14640lm r3, Runnable runnable, long j, boolean z, boolean z2) {
        this.A00 = r2;
        this.A01 = C12970iu.A10(runnable);
        this.A03 = r3;
        this.A05 = z;
        this.A04 = z2;
        this.A02 = j;
    }
}
