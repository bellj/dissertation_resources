package X;

/* renamed from: X.4b0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93944b0 {
    public final String A00;
    public final String A01;

    public C93944b0(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    public C93944b0(C93944b0... r8) {
        String str = "";
        String str2 = str;
        for (C93944b0 r2 : r8) {
            str = C12960it.A0d(r2.A01, C12960it.A0j(str));
            str2 = C12960it.A0d(r2.A00, C12960it.A0j(str2));
        }
        this.A01 = str;
        this.A00 = str2;
    }

    public boolean A00(char c, char c2) {
        int i = 0;
        while (true) {
            String str = this.A01;
            if (i >= str.length()) {
                return false;
            }
            if (str.charAt(i) == c && this.A00.charAt(i) == c2) {
                return true;
            }
            i++;
        }
    }
}
