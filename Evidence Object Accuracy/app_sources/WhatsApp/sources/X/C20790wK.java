package X;

import com.whatsapp.util.Log;

/* renamed from: X.0wK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20790wK {
    public final AnonymousClass016 A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C14830m7 A03;
    public final AnonymousClass150 A04;
    public final C16120oU A05;
    public final C17220qS A06;
    public final C22230yk A07;
    public final AbstractC14440lR A08;

    public C20790wK(C14900mE r2, C18640sm r3, C14830m7 r4, AnonymousClass150 r5, C16120oU r6, C17220qS r7, C22230yk r8, AbstractC14440lR r9) {
        this.A03 = r4;
        this.A01 = r2;
        this.A08 = r9;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A02 = r3;
        this.A04 = r5;
        this.A00 = r5.A00;
    }

    public void A00() {
        Log.i("DisappearingModeManager/getDisappearingModeSetting");
        C17220qS r4 = this.A06;
        String A01 = r4.A01();
        r4.A0D(new C41741u1(this), new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "disappearing_mode"), new AnonymousClass1W9("type", "get")}), A01, 296, 20000);
    }
}
