package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.0vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20520vt {
    public final AbstractC15710nm A00;
    public final C15810nw A01;
    public final C20530vu A02;
    public final C16510p9 A03;
    public final C20560vx A04;
    public final C20280vV A05;
    public final C18460sU A06;
    public final C20060v9 A07;
    public final C20090vC A08;
    public final C20120vF A09;
    public final C20130vG A0A;
    public final C21620xi A0B;
    public final C16490p7 A0C;
    public final C20500vr A0D;
    public final C20570vy A0E;
    public final C20370ve A0F;
    public final C20540vv A0G;
    public final C21390xL A0H;
    public final C242314s A0I;
    public final C20490vq A0J;
    public final C20510vs A0K;
    public final C20550vw A0L;
    public final C20320vZ A0M;

    public C20520vt(AbstractC15710nm r2, C15810nw r3, C20530vu r4, C16510p9 r5, C20560vx r6, C20280vV r7, C18460sU r8, C20060v9 r9, C20090vC r10, C20120vF r11, C20130vG r12, C21620xi r13, C16490p7 r14, C20500vr r15, C20570vy r16, C20370ve r17, C20540vv r18, C21390xL r19, C242314s r20, C20490vq r21, C20510vs r22, C20550vw r23, C20320vZ r24) {
        this.A06 = r8;
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A0M = r24;
        this.A08 = r10;
        this.A0J = r21;
        this.A0D = r15;
        this.A0H = r19;
        this.A0K = r22;
        this.A02 = r4;
        this.A0A = r12;
        this.A0B = r13;
        this.A0C = r14;
        this.A0G = r18;
        this.A07 = r9;
        this.A0I = r20;
        this.A09 = r11;
        this.A0L = r23;
        this.A05 = r7;
        this.A04 = r6;
        this.A0F = r17;
        this.A0E = r16;
    }

    public final ContentValues A00(AbstractC14640lm r6, AbstractC15340mz r7, long j) {
        long A01;
        byte b;
        ContentValues contentValues = new ContentValues();
        contentValues.put("message_row_id", Long.valueOf(j));
        C16510p9 r4 = this.A03;
        contentValues.put("parent_message_chat_row_id", Long.valueOf(r4.A02(r6)));
        AnonymousClass1IS r3 = r7.A0z;
        AbstractC14640lm r0 = r3.A00;
        AnonymousClass009.A05(r0);
        contentValues.put("chat_row_id", Long.valueOf(r4.A02(r0)));
        contentValues.put("from_me", Integer.valueOf(r3.A02 ? 1 : 0));
        AbstractC14640lm A0B = r7.A0B();
        if (A0B == null) {
            A01 = 0;
        } else {
            A01 = this.A06.A01(A0B);
        }
        contentValues.put("sender_jid_row_id", Long.valueOf(A01));
        contentValues.put("key_id", r3.A01);
        contentValues.put("timestamp", Long.valueOf(r7.A0I));
        if (!(r7 instanceof AnonymousClass1XB)) {
            b = r7.A0y;
        } else {
            b = 7;
        }
        contentValues.put("message_type", Integer.valueOf(b));
        contentValues.put("origin", Integer.valueOf(r7.A08));
        contentValues.put("text_data", r7.A0Q());
        contentValues.put("payment_transaction_id", r7.A0m);
        contentValues.put("lookup_tables", Long.valueOf(r7.A0A()));
        return contentValues;
    }

    public void A01(AbstractC15340mz r11) {
        AbstractC15340mz A0E = r11.A0E();
        if (A0E != null) {
            boolean z = false;
            if (A0E.A08() == 2) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            C16310on A02 = this.A0C.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                if (A06()) {
                    long j = r11.A11;
                    AbstractC14640lm r2 = r11.A0z.A00;
                    AnonymousClass009.A05(r2);
                    A02.A03.A05(A00(r2, A0E, j), "message_quoted");
                    A05(r11, false);
                }
                if (A0E instanceof AbstractC28871Pi) {
                    C242314s r6 = this.A0I;
                    AbstractC28871Pi r8 = (AbstractC28871Pi) A0E;
                    long j2 = r11.A11;
                    boolean z2 = true;
                    boolean z3 = false;
                    if (j2 > 0) {
                        z3 = true;
                    }
                    AnonymousClass009.A0B("TemplateMessageStore/fillQuotedTemplateData/parent message row must be set", z3);
                    A02 = r6.A01.A02();
                    try {
                        C28891Pk AH7 = r8.AH7();
                        ContentValues contentValues = new ContentValues(3);
                        contentValues.put("message_row_id", Long.valueOf(j2));
                        contentValues.put("content_text_data", AH7.A01);
                        contentValues.put("footer_text_data", AH7.A02);
                        if (j2 != A02.A03.A06(contentValues, "message_template_quoted", 5)) {
                            z2 = false;
                        }
                        AnonymousClass009.A0C("TemplateMessageStore/insertOrUpdateTemplateQuotedData/inserted row should have same row_id", z2);
                        A02.close();
                    } finally {
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
            } finally {
            }
        }
    }

    public void A02(AbstractC15340mz r12) {
        AbstractC14640lm r1;
        byte b;
        byte b2;
        AbstractC15340mz A0E = r12.A0E();
        if (A0E != null) {
            boolean z = false;
            boolean z2 = false;
            if (A0E.A08() == 2) {
                z2 = true;
            }
            AnonymousClass009.A0F(z2);
            C16310on A02 = this.A0C.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                ContentValues contentValues = new ContentValues();
                C30021Vq.A01(contentValues, this.A01, A0E);
                long A022 = A02.A03.A02(contentValues, "messages_quotes");
                r12.A0F = A022;
                if (A022 <= 0) {
                    AbstractC15710nm r7 = this.A00;
                    StringBuilder sb = new StringBuilder();
                    sb.append("quoted message type : ");
                    if (!(A0E instanceof AnonymousClass1XB)) {
                        b = A0E.A0y;
                    } else {
                        b = 7;
                    }
                    sb.append((int) b);
                    sb.append(" ,parent message type: ");
                    if (!(r12 instanceof AnonymousClass1XB)) {
                        b2 = r12.A0y;
                    } else {
                        b2 = 7;
                    }
                    sb.append((int) b2);
                    r7.AaV("QuotedMessageStore/insertQuotedMessageInOldTable/Error", sb.toString(), true);
                }
                if (r12.A0F > 0) {
                    z = true;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("QuotedMessageStore/insertQuotedMessageInOldTable/Error insert quoted message; parentMsg.key=");
                sb2.append(r12.A0z);
                AnonymousClass009.A0C(sb2.toString(), z);
                A03(r12);
                A00.A00();
                A00.close();
                A02.close();
                if ((A0E instanceof C30241Wq) && (r1 = A0E.A0z.A00) != null) {
                    this.A03.A02(r1);
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public final void A03(AbstractC15340mz r10) {
        C16310on A02;
        AbstractC15340mz A0E = r10.A0E();
        if (A0E instanceof AnonymousClass1XF) {
            C20500vr r8 = this.A0D;
            boolean z = true;
            boolean z2 = false;
            if (r10.A0F > 0) {
                z2 = true;
            }
            StringBuilder sb = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessageLegacy/message must have quoted_row_id set; key=");
            AnonymousClass1IS r4 = r10.A0z;
            sb.append(r4);
            AnonymousClass009.A0B(sb.toString(), z2);
            AbstractC15340mz A0E2 = r10.A0E();
            StringBuilder sb2 = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessageLegacy/message must be a order message; key=");
            sb2.append(r4);
            AnonymousClass009.A0B(sb2.toString(), A0E2 instanceof AnonymousClass1XF);
            boolean z3 = false;
            if (A0E2.A08() == 2) {
                z3 = true;
            }
            StringBuilder sb3 = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessageLegacy/message in main storage; key=");
            sb3.append(A0E2.A0z);
            AnonymousClass009.A0B(sb3.toString(), z3);
            try {
                C16310on A022 = r8.A01.A02();
                ContentValues contentValues = new ContentValues();
                r8.A00(contentValues, (AnonymousClass1XF) A0E2, r10.A0F);
                if (r10.A0F != A022.A03.A03(contentValues, "quoted_message_order")) {
                    z = false;
                }
                AnonymousClass009.A0C("OrderMessageStore/insertOrUpdateQuotedOrderMessageLegacy/inserted row should have same row_id", z);
                A022.close();
            } catch (SQLiteConstraintException e) {
                StringBuilder sb4 = new StringBuilder("OrderMessageStore/insertOrUpdateQuotedOrderMessageLegacy/fail to insert. Error message is: ");
                sb4.append(e);
                Log.e(sb4.toString());
            }
        }
        if (A0E instanceof AnonymousClass1XV) {
            C20540vv r82 = this.A0G;
            boolean z4 = true;
            boolean z5 = false;
            if (r10.A0F > 0) {
                z5 = true;
            }
            StringBuilder sb5 = new StringBuilder("ProductMessageStore/insertOrUpdateQuotedProductMessageLegacy/message must have quoted_row_id set; key=");
            AnonymousClass1IS r42 = r10.A0z;
            sb5.append(r42);
            AnonymousClass009.A0B(sb5.toString(), z5);
            AbstractC15340mz A0E3 = r10.A0E();
            StringBuilder sb6 = new StringBuilder("ProductMessageStore/insertOrUpdateQuotedProductMessageLegacy/message must be a product message; key=");
            sb6.append(r42);
            AnonymousClass009.A0B(sb6.toString(), A0E3 instanceof AnonymousClass1XV);
            boolean z6 = false;
            if (A0E3.A08() == 2) {
                z6 = true;
            }
            StringBuilder sb7 = new StringBuilder("ProductMessageStore/insertOrUpdateQuotedProductMessageLegacy/message in main storage; key=");
            sb7.append(A0E3.A0z);
            AnonymousClass009.A0B(sb7.toString(), z6);
            A02 = r82.A02.A02();
            try {
                ContentValues contentValues2 = new ContentValues();
                r82.A00(contentValues2, (AnonymousClass1XV) A0E3, r10.A0F);
                if (A02.A03.A06(contentValues2, "quoted_message_product", 5) != r10.A0F) {
                    z4 = false;
                }
                AnonymousClass009.A0C("ProductMessageStore/insertOrUpdateQuotedProductMessage/inserted row should have same row_id", z4);
                A02.close();
            } finally {
            }
        }
        if (A0E instanceof AnonymousClass1XJ) {
            C20530vu r83 = this.A02;
            boolean z7 = true;
            boolean z8 = false;
            if (r10.A0F > 0) {
                z8 = true;
            }
            StringBuilder sb8 = new StringBuilder("CatalogMessageStore/insertOrUpdateQuotedCatalogMessageLegacy/message must have quoted_row_id set; key=");
            AnonymousClass1IS r43 = r10.A0z;
            sb8.append(r43);
            AnonymousClass009.A0B(sb8.toString(), z8);
            AbstractC15340mz A0E4 = r10.A0E();
            StringBuilder sb9 = new StringBuilder("CatalogMessageStore/insertOrUpdateQuotedCatalogMessageLegacy/message must be a catalog message; key=");
            sb9.append(r43);
            AnonymousClass009.A0B(sb9.toString(), A0E4 instanceof AnonymousClass1XJ);
            boolean z9 = false;
            if (A0E4.A08() == 2) {
                z9 = true;
            }
            StringBuilder sb10 = new StringBuilder("CatalogMessageStore/insertOrUpdateQuotedCatalogMessageLegacy/message in main storage; key=");
            sb10.append(A0E4.A0z);
            AnonymousClass009.A0B(sb10.toString(), z9);
            A02 = r83.A01.A02();
            try {
                ContentValues contentValues3 = new ContentValues();
                r83.A00(contentValues3, (AnonymousClass1XJ) A0E4, r10.A0F);
                if (A02.A03.A06(contentValues3, "quoted_message_product", 5) != r10.A0F) {
                    z7 = false;
                }
                AnonymousClass009.A0C("CatalogMessageStore/insertOrUpdateQuotedCatalogMessageLegacy/inserted row should have same row_id", z7);
                A02.close();
            } finally {
            }
        }
        if (A0E instanceof C28581Od) {
            C20560vx r6 = this.A04;
            C28581Od r44 = (C28581Od) A0E;
            long j = r10.A0F;
            A02 = r6.A02.A02();
            try {
                A02.A03.A06(r6.A02(r44, j), "message_quoted_group_invite_legacy", 5);
                A02.close();
            } finally {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
            }
        }
        if (A0E instanceof AbstractC16390ow) {
            this.A05.A0F((AbstractC16390ow) A0E, r10.A0F);
        }
        if (A0E instanceof AnonymousClass1XE) {
            this.A05.A0D((AnonymousClass1XE) A0E, "message_quoted_ui_elements_reply_legacy", r10.A0F);
        }
        if (A0E instanceof AnonymousClass1XD) {
            this.A05.A0A((AnonymousClass1XD) A0E, "message_quoted_ui_elements_reply_legacy", r10.A0F);
        }
        if (A0E instanceof AnonymousClass1XC) {
            AnonymousClass1XC r0 = (AnonymousClass1XC) A0E;
            this.A0E.A01("messages_quotes_payment_invite_legacy", r0.A00, r10.A0F, r0.A01);
        }
        if (A0E != null && A0E.A0y()) {
            this.A05.A07(A0E.A0F().A00, "message_quoted_ui_elements", r10.A0F);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c3, code lost:
        if (r1 == null) goto L_0x00c5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(X.AbstractC15340mz r19, long r20) {
        /*
        // Method dump skipped, instructions count: 373
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20520vt.A04(X.0mz, long):void");
    }

    public final void A05(AbstractC15340mz r13, boolean z) {
        AnonymousClass009.A0F(A06());
        AbstractC15340mz A0E = r13.A0E();
        if (A0E instanceof AnonymousClass1XP) {
            this.A07.A01((AnonymousClass1XP) A0E, r13.A11);
        }
        if (A0E instanceof AbstractC16130oV) {
            this.A09.A09((AbstractC16130oV) A0E, r13.A11);
        }
        if (A0E instanceof AnonymousClass1XV) {
            this.A0G.A01((AnonymousClass1XV) A0E, r13.A11);
        }
        if (A0E instanceof AnonymousClass1XJ) {
            this.A02.A01((AnonymousClass1XJ) A0E, r13.A11);
        }
        if (A0E instanceof C28581Od) {
            this.A04.A05((C28581Od) A0E, r13.A11);
        }
        if (A0E instanceof AnonymousClass1XF) {
            this.A0D.A02((AnonymousClass1XF) A0E, r13.A11);
        }
        if (A0E instanceof AbstractC16390ow) {
            this.A05.A0F((AbstractC16390ow) A0E, r13.A11);
        }
        if (A0E instanceof AnonymousClass1XE) {
            this.A05.A0D((AnonymousClass1XE) A0E, "message_quoted_ui_elements_reply", r13.A11);
        }
        if (A0E instanceof AnonymousClass1XD) {
            this.A05.A0A((AnonymousClass1XD) A0E, "message_quoted_ui_elements_reply", r13.A11);
        }
        if (A0E != null) {
            if (A0E.A0z()) {
                this.A0A.A01(A0E, r13.A11);
            }
            if (A0E.A0y()) {
                this.A05.A07(A0E.A0F().A00, "message_quoted_ui_elements", r13.A11);
            }
        }
        if (A0E instanceof C30411Xh) {
            C20510vs r5 = this.A0K;
            long j = r13.A11;
            String A14 = ((C30411Xh) A0E).A14();
            if (!TextUtils.isEmpty(A14)) {
                r5.A06(A14, j);
            }
        } else if (A0E instanceof C30351Xb) {
            this.A0K.A04((C30351Xb) A0E, r13.A11);
        }
        if (A0E instanceof C28861Ph) {
            this.A0J.A00(A0E, r13.A11, z);
        }
        if (A0E instanceof AnonymousClass1XC) {
            AnonymousClass1XC r0 = (AnonymousClass1XC) A0E;
            this.A0E.A01("message_quoted_payment_invite", r0.A00, r13.A11, r0.A01);
        }
        if (A0E instanceof C30241Wq) {
            this.A0L.A00((C30241Wq) A0E, r13.A11);
        }
    }

    public boolean A06() {
        return this.A0H.A01("quoted_message_ready", 0) == 2;
    }
}
