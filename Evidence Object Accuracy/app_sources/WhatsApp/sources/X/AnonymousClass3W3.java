package X;

/* renamed from: X.3W3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W3 implements AbstractC115915Tk {
    public final /* synthetic */ C71573d9 A00;

    public AnonymousClass3W3(C71573d9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC115915Tk
    public AnonymousClass2K2 A7v(AbstractC115895Ti r5) {
        C71573d9 r0 = this.A00;
        AnonymousClass01J r1 = r0.A04;
        AnonymousClass018 A0R = C12960it.A0R(r1);
        return new AnonymousClass2K2(C12970iu.A0V(r1), r5, (AbstractC115905Tj) r0.A03.A0P.get(), A0R);
    }
}
