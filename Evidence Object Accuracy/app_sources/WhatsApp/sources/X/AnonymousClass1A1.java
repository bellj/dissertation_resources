package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.1A1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1A1 {
    public C30421Xi A00;
    public boolean A01 = false;
    public boolean A02 = false;
    public boolean A03 = false;
    public boolean A04 = false;
    public boolean A05 = false;
    public final C16210od A06;
    public final AnonymousClass11P A07;
    public final AnonymousClass52O A08;

    public AnonymousClass1A1(C16210od r2, AnonymousClass11P r3) {
        this.A07 = r3;
        this.A06 = r2;
        this.A08 = new AnonymousClass52O(r3, this);
    }

    public static boolean A00(View view) {
        View findViewById;
        if (view == null || (findViewById = view.findViewById(R.id.out_of_chat_playback_holder)) == null || findViewById.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public void A01() {
        this.A04 = false;
        this.A03 = false;
        this.A05 = false;
        this.A02 = false;
    }

    public void A02(View view) {
        boolean z = true;
        if (!A00(view)) {
            z = false;
        }
        this.A05 = z;
        this.A04 = z;
        this.A03 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r1 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.AbstractC14640lm r3) {
        /*
            r2 = this;
            X.11P r0 = r2.A07
            X.1Xi r0 = r0.A01()
            if (r3 == 0) goto L_0x0017
            if (r0 == 0) goto L_0x0017
            X.1IS r0 = r0.A0z
            X.0lm r0 = r0.A00
            if (r0 == 0) goto L_0x0017
            boolean r1 = r3.equals(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            r2.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1A1.A03(X.0lm):void");
    }
}
