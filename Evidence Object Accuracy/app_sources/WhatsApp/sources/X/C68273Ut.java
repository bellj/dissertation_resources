package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68273Ut implements AbstractC13960kc {
    public final /* synthetic */ AbstractActivityC37081lH A00;

    public C68273Ut(AbstractActivityC37081lH r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC13960kc
    public void AQH(UserJid userJid, int i) {
        AbstractActivityC37081lH r2 = this.A00;
        if (C29941Vi.A00(userJid, r2.A0J)) {
            C53842fM r1 = r2.A0F;
            r1.A01 = true;
            r1.A00 = Integer.valueOf(i);
            if (!r2.A0B.A00) {
                r2.A0E.A0N(i);
                r2.A0K.A06("catalog_collections_view_tag", false);
            }
        }
    }

    @Override // X.AbstractC13960kc
    public void AQI(UserJid userJid, boolean z, boolean z2) {
        AbstractActivityC37081lH r3 = this.A00;
        if (C29941Vi.A00(userJid, r3.A0J)) {
            if (!z && z2) {
                r3.A0F.A01 = true;
            }
            r3.A0F.A00 = null;
            if (!r3.A0B.A00) {
                r3.A0M = true;
                r3.invalidateOptionsMenu();
                C37101lJ r0 = r3.A0E;
                r0.A0P(userJid);
                r0.A0L();
                r0.A02();
                C53842fM r1 = r3.A0F;
                if (r1.A01 && r1.A02) {
                    r3.A0K.A06("catalog_collections_view_tag", true);
                }
            }
        }
    }
}
