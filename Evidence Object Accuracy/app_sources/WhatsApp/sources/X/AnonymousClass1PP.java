package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1PP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1PP extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1PP A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AnonymousClass1K6 A01 = AnonymousClass277.A01;
    public String A02 = "";

    static {
        AnonymousClass1PP r0 = new AnonymousClass1PP();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A02) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A01.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A01.get(i3), 2);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A02);
        }
        for (int i = 0; i < this.A01.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A01.get(i), 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
