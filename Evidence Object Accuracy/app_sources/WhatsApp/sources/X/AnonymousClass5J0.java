package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.5J0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5J0 extends AnonymousClass1WI implements AnonymousClass1WK {
    public AnonymousClass5J0() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        return new AtomicReference();
    }
}
