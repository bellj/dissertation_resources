package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.search.views.MessageThumbView;

/* renamed from: X.3bG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70423bG implements AbstractC41521tf {
    public final /* synthetic */ MessageThumbView A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70423bG(MessageThumbView messageThumbView) {
        this.A00 = messageThumbView;
    }

    public final void A00(Bitmap bitmap) {
        MessageThumbView messageThumbView = this.A00;
        Drawable drawable = messageThumbView.getDrawable();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(messageThumbView.getResources(), bitmap);
        if (drawable == null || (drawable instanceof ColorDrawable)) {
            messageThumbView.setImageDrawable(bitmapDrawable);
            return;
        }
        Drawable[] drawableArr = new Drawable[2];
        C12970iu.A1U(drawable, bitmapDrawable, drawableArr);
        TransitionDrawable transitionDrawable = new TransitionDrawable(drawableArr);
        messageThumbView.setImageDrawable(transitionDrawable);
        transitionDrawable.startTransition(150);
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.getWidth();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r6) {
        MessageThumbView messageThumbView = this.A00;
        if (messageThumbView.A00 > 0) {
            A00(C22200yh.A0A(bitmap, (float) messageThumbView.A00, messageThumbView.getResources().getDimensionPixelSize(R.dimen.search_media_thumbnail_size)));
            return;
        }
        A00(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A00.setImageDrawable(new ColorDrawable(view.getResources().getColor(R.color.search_grid_item_bg)));
    }
}
