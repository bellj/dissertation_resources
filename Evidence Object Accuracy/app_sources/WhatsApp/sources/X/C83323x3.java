package X;

import android.view.animation.Animation;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.3x3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83323x3 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ ExoPlaybackControlView A00;

    public C83323x3(ExoPlaybackControlView exoPlaybackControlView) {
        this.A00 = exoPlaybackControlView;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        ExoPlaybackControlView exoPlaybackControlView = this.A00;
        exoPlaybackControlView.setAlpha(1.0f);
        exoPlaybackControlView.removeCallbacks(exoPlaybackControlView.A0P);
        exoPlaybackControlView.removeCallbacks(exoPlaybackControlView.A0O);
        exoPlaybackControlView.A00 = null;
    }
}
