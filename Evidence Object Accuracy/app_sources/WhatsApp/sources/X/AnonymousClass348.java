package X;

import android.content.res.Resources;
import android.text.InputFilter;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;
import com.whatsapp.polls.PollCreatorViewModel;

/* renamed from: X.348  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass348 extends AbstractC75183jR {
    public WaEditText A00;
    public WaTextView A01;

    public AnonymousClass348(View view, C14850m9 r5, PollCreatorViewModel pollCreatorViewModel) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.poll_question_label);
        WaEditText waEditText = (WaEditText) AnonymousClass028.A0D(view, R.id.poll_question_edit_text);
        this.A00 = waEditText;
        C12990iw.A1I(waEditText, new InputFilter[1], r5.A02(1406));
        this.A00.setOnFocusChangeListener(new View.OnFocusChangeListener(view, this) { // from class: X.4mo
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass348 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z) {
                AnonymousClass348 r1 = this.A01;
                View view3 = this.A00;
                WaTextView waTextView = r1.A01;
                Resources resources = view3.getContext().getResources();
                int i = R.color.secondary_text;
                if (z) {
                    i = R.color.action_text;
                }
                waTextView.setTextColor(resources.getColor(i));
            }
        });
        this.A00.addTextChangedListener(new C100704mL(this, pollCreatorViewModel));
        this.A00.requestFocus();
    }
}
