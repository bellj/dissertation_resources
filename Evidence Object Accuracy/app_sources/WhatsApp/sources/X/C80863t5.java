package X;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3t5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80863t5 extends AbstractC80933tC implements AbstractC117165Yt {
    public static final long f$0 = 6588350623831699109L;
    public static final long serialVersionUID = 0;
    public transient AbstractC115805Sz factory;

    public C80863t5(Map map) {
        super(map);
    }

    public C80863t5(Map map, AbstractC115805Sz r2) {
        this(map);
        this.factory = r2;
    }

    @Override // X.AnonymousClass51S
    public Map createAsMap() {
        return createMaybeNavigableAsMap();
    }

    @Override // X.AbstractC80933tC
    public List createCollection() {
        return (List) this.factory.get();
    }

    @Override // X.AnonymousClass51S
    public Set createKeySet() {
        return createMaybeNavigableKeySet();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.factory = (AbstractC115805Sz) objectInputStream.readObject();
        setMap((Map) objectInputStream.readObject());
    }

    @Override // X.AbstractC80933tC
    public Collection unmodifiableCollectionSubclass(Collection collection) {
        return Collections.unmodifiableList((List) collection);
    }

    @Override // X.AbstractC80933tC
    public Collection wrapCollection(Object obj, Collection collection) {
        return wrapList(obj, (List) collection, null);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.factory);
        objectOutputStream.writeObject(backingMap());
    }
}
