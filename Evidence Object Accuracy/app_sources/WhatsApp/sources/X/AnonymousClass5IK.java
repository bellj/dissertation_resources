package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.5IK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IK extends LinkedHashMap<K, V> {
    public final /* synthetic */ AnonymousClass4MO this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5IK(AnonymousClass4MO r3, int i) {
        super(i, 0.75f, true);
        this.this$0 = r3;
    }

    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry entry) {
        return C72463ee.A0Y(size(), this.this$0.A00);
    }
}
