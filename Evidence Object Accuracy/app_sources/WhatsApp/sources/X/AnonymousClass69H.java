package X;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.invites.IndiaUpiPaymentInviteFragment;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import java.util.ArrayList;

/* renamed from: X.69H  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69H implements AbstractC38141na {
    public final C14830m7 A00;
    public final C16590pI A01;
    public final C14850m9 A02;
    public final C21860y6 A03;

    @Override // X.AbstractC38141na
    public int AFW() {
        return 3;
    }

    public AnonymousClass69H(C14830m7 r1, C16590pI r2, C14850m9 r3, C21860y6 r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    @Override // X.AbstractC38141na
    public boolean A6y() {
        return this.A03.A0C();
    }

    @Override // X.AbstractC38141na
    public boolean A6z(UserJid userJid) {
        if (!this.A02.A07(733)) {
            return false;
        }
        return this.A03.A0C();
    }

    @Override // X.AbstractC38141na
    public Intent AAY(AbstractC15340mz r4) {
        if (this.A03.A0C()) {
            return null;
        }
        Intent A0D = C12990iw.A0D(this.A01.A00, IndiaUpiPaymentsAccountSetupActivity.class);
        A0D.putExtra("extra_setup_mode", 2);
        A0D.putExtra("extra_payments_entry_type", 2);
        A0D.putExtra("extra_is_first_payment_method", true);
        A0D.putExtra("extra_skip_value_props_display", false);
        AbstractC14640lm r1 = r4.A0z.A00;
        if (r1 instanceof GroupJid) {
            r1 = r4.A0B();
        }
        String A03 = C15380n4.A03(r1);
        A0D.putExtra("extra_jid", A03);
        A0D.putExtra("extra_inviter_jid", A03);
        C35741ib.A00(A0D, "acceptInvite");
        return A0D;
    }

    @Override // X.AbstractC38141na
    public /* synthetic */ int ADZ() {
        return -1;
    }

    @Override // X.AbstractC38141na
    public /* synthetic */ AnonymousClass4S0 ADa() {
        return new AnonymousClass4S0(null, null, R.drawable.payment_invite_bubble_icon, false);
    }

    @Override // X.AbstractC38141na
    public /* synthetic */ C69973aX ADb(C16590pI r2, C22590zK r3, AbstractC14440lR r4) {
        return new C69973aX(r2, r3, r4);
    }

    @Override // X.AbstractC38141na
    public DialogFragment AFK(String str, ArrayList arrayList, boolean z, boolean z2) {
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        IndiaUpiPaymentInviteFragment indiaUpiPaymentInviteFragment = new IndiaUpiPaymentInviteFragment();
        indiaUpiPaymentInviteFragment.A0U(PaymentInviteFragment.A01(str, arrayList, 3, z, z2));
        paymentBottomSheet.A01 = indiaUpiPaymentInviteFragment;
        return paymentBottomSheet;
    }

    @Override // X.AbstractC38141na
    public String AFM(Context context, String str, boolean z) {
        int i = R.string.payment_invite_status_text_inbound;
        if (z) {
            i = R.string.payment_invite_status_text_outbound;
        }
        return C12960it.A0X(context, str, C12970iu.A1b(), 0, i);
    }

    @Override // X.AbstractC38141na
    public boolean AIF() {
        return this.A03.A0C();
    }
}
