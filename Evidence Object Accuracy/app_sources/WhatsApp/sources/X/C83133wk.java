package X;

import java.util.List;

/* renamed from: X.3wk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83133wk extends AnonymousClass4YR {
    public List A00;
    public final String A01;
    public final String A02;

    public C83133wk(String str, List list) {
        String str2;
        StringBuilder A0j = C12960it.A0j(str);
        if (list == null || list.size() <= 0) {
            str2 = "()";
        } else {
            str2 = "(...)";
        }
        this.A02 = C12960it.A0d(str2, A0j);
        if (str != null) {
            this.A01 = str;
            this.A00 = list;
            return;
        }
        this.A00 = null;
    }
}
