package X;

/* renamed from: X.5Mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114525Mb extends AnonymousClass1TM {
    public static final AnonymousClass5NG A03 = new AnonymousClass5NG(0);
    public AnonymousClass5NG A00 = A03;
    public AbstractC114775Na A01;
    public AnonymousClass5MX A02;

    public C114525Mb(AbstractC114775Na r2, AnonymousClass5MX r3) {
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(4);
        AnonymousClass5NG r1 = this.A00;
        if (!r1.A04(A03)) {
            C94954co.A02(r1, r3, 0, true);
        }
        r3.A06(this.A01);
        AnonymousClass5MX r12 = this.A02;
        if (r12 != null) {
            C94954co.A02(r12, r3, 2, true);
        }
        return new AnonymousClass5NZ(r3);
    }
}
