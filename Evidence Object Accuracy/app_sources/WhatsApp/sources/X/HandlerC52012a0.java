package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.2a0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC52012a0 extends Handler {
    public final /* synthetic */ AnonymousClass3HL A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC52012a0(Looper looper, AnonymousClass3HL r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 99) {
            this.A00.A00();
            return;
        }
        throw C12990iw.A0m(C12960it.A0W(i, "Unknown message: "));
    }
}
