package X;

/* renamed from: X.2iP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55402iP extends AbstractRunnableC47782Cq {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C64183Eo A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55402iP(C64183Eo r2, int i) {
        super("StreamingUploadDataTask_update");
        this.A01 = r2;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        C64183Eo r1 = this.A01;
        r1.A03.updateDataTaskUploadProgress(r1.A02.mTaskIdentifier, (long) this.A00, r1.A00, r1.A01);
    }
}
