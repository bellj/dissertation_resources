package X;

/* renamed from: X.5tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C126995tm {
    public final int A00;
    public final Runnable A01;

    public C126995tm(Runnable runnable, int i) {
        this.A00 = i;
        this.A01 = runnable;
    }

    public static C126995tm A00(Runnable runnable, int i) {
        return new C126995tm(runnable, i);
    }
}
