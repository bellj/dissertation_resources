package X;

import android.os.Bundle;
import com.whatsapp.util.Log;

/* renamed from: X.10N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10N extends AbstractC16230of {
    public volatile int A00 = -1;
    public volatile int A01 = -1;

    public void A05() {
        Log.i("google-backup-restore/backup-cancelled");
        this.A00 = 0;
        for (AnonymousClass10L r0 : A01()) {
            r0.AMs();
        }
    }

    public void A06() {
        Log.i("google-backup-restore/notify-media-restore-cancelled");
        this.A01 = -1;
        for (AnonymousClass10L r0 : A01()) {
            r0.ASU();
        }
    }

    public void A07(int i, Bundle bundle) {
        for (AnonymousClass10L r0 : A01()) {
            r0.APw(i, bundle);
        }
    }

    public void A08(long j, long j2) {
        for (AnonymousClass10L r0 : A01()) {
            r0.AN6(j, j2);
        }
    }

    public void A09(boolean z) {
        for (AnonymousClass10L r0 : A01()) {
            r0.ALq(z);
        }
    }

    public void A0A(boolean z) {
        StringBuilder sb = new StringBuilder("google-backup-restore/backup-end/success/");
        sb.append(z);
        Log.i(sb.toString());
        this.A00 = 0;
        for (AnonymousClass10L r0 : A01()) {
            r0.AMt(z);
        }
    }

    public void A0B(boolean z) {
        for (AnonymousClass10L r0 : A01()) {
            r0.ASl(z);
        }
    }
}
