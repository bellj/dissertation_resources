package X;

import android.util.Log;

/* renamed from: X.0t8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18840t8 {
    public AbstractC18450sT A00;
    public final C006202y A01;

    public C18840t8(AbstractC18450sT r3) {
        C74263hg r1 = new C74263hg(10);
        this.A01 = r1;
        r1.A00 = new AnonymousClass5VB() { // from class: X.5B2
            @Override // X.AnonymousClass5VB
            public final void APj(Object obj, Object obj2, Object obj3, boolean z) {
                String str = (String) obj;
                if (z) {
                    Log.d("Whatsapp", C12960it.A0d(str, C12960it.A0k("Bloks: CacheShards evicted ")));
                }
            }
        };
        this.A00 = r3;
        r3.Aa2(new AnonymousClass4LT(this));
        r3.AIp(this);
    }

    public final AnonymousClass4LU A00(String str) {
        AnonymousClass4LU r0;
        C006202y r3 = this.A01;
        AnonymousClass4LU r02 = (AnonymousClass4LU) r3.A04(str);
        if (r02 != null) {
            return r02;
        }
        synchronized (r3) {
            r0 = (AnonymousClass4LU) r3.A04(str);
            if (r0 == null) {
                int AAT = this.A00.AAT();
                int i = 50;
                if (AAT < 2016) {
                    i = 10;
                    if (AAT >= 2014) {
                        i = 30;
                    }
                }
                r0 = new AnonymousClass4LU(str, i);
                r3.A08(str, r0);
            }
        }
        return r0;
    }

    public Object A01(String str, String str2) {
        C94824cb r7 = (C94824cb) A00(str).A00.A04(str2);
        if (r7 != null) {
            long j = r7.A01;
            if (j == -1 || System.currentTimeMillis() < r7.A00 + j) {
                Object obj = r7.A02;
                if (obj != null) {
                    return obj;
                }
            } else {
                A04(str, str2);
            }
        }
        return null;
    }

    public void A02(C94824cb r2, String str, String str2) {
        A00(str).A00.A08(str2, r2);
        if ((r2.A02 instanceof String) && r2.A03) {
            this.A00.AbI(r2, str, str2);
        }
    }

    public void A03(Object obj, String str, String str2, long j, boolean z) {
        A02(new C94824cb(obj, j, z), str, str2);
    }

    public void A04(String str, String str2) {
        AnonymousClass4LU r0 = (AnonymousClass4LU) this.A01.A04(str);
        if (r0 != null) {
            r0.A00.A07(str2);
        }
        this.A00.AaJ(str, str2);
    }
}
