package X;

import com.whatsapp.util.Log;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0ue  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19770ue {
    public final C16490p7 A00;
    public final ThreadLocal A01 = new ThreadLocal();
    public final AtomicInteger A02 = new AtomicInteger(0);

    public C19770ue(C16490p7 r3) {
        this.A00 = r3;
    }

    public AnonymousClass1YE A00(String str) {
        ThreadLocal threadLocal = this.A01;
        AnonymousClass1YD r3 = (AnonymousClass1YD) threadLocal.get();
        if (r3 == null) {
            r3 = new AnonymousClass1YD(this.A00, this.A02.get());
            threadLocal.set(r3);
        }
        int i = this.A02.get();
        if (i != r3.A00) {
            r3.A00();
            r3.A00 = i;
        }
        Map map = r3.A01;
        if (!map.containsKey(str)) {
            C16310on r1 = (C16310on) r3.A02.get();
            try {
                map.put(str, r1.A03.A0A(str));
                r1.close();
            } catch (Throwable th) {
                if (r1 != null) {
                    try {
                        r1.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
        AnonymousClass1YE r12 = (AnonymousClass1YE) map.get(str);
        r12.A00.clearBindings();
        return r12;
    }

    public void A01() {
        Log.i("statementsmanager/resetstatements");
        this.A02.incrementAndGet();
        AnonymousClass1YD r0 = (AnonymousClass1YD) this.A01.get();
        if (r0 != null) {
            r0.A00();
        }
    }
}
