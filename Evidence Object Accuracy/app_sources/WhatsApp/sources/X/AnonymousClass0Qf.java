package X;

import android.os.PersistableBundle;

/* renamed from: X.0Qf  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Qf {
    public static PersistableBundle A00(C007303s r3) {
        String str;
        PersistableBundle persistableBundle = new PersistableBundle();
        CharSequence charSequence = r3.A01;
        if (charSequence != null) {
            str = charSequence.toString();
        } else {
            str = null;
        }
        persistableBundle.putString("name", str);
        persistableBundle.putString("uri", r3.A03);
        persistableBundle.putString("key", r3.A02);
        persistableBundle.putBoolean("isBot", r3.A04);
        persistableBundle.putBoolean("isImportant", r3.A05);
        return persistableBundle;
    }

    public static C007303s A01(PersistableBundle persistableBundle) {
        C007403t r1 = new C007403t();
        r1.A01 = persistableBundle.getString("name");
        r1.A03 = persistableBundle.getString("uri");
        r1.A02 = persistableBundle.getString("key");
        r1.A04 = persistableBundle.getBoolean("isBot");
        r1.A05 = persistableBundle.getBoolean("isImportant");
        return new C007303s(r1);
    }
}
