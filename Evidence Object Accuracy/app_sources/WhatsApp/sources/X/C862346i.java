package X;

/* renamed from: X.46i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C862346i extends AbstractC35941j2 {
    public final /* synthetic */ AbstractC32491cF A00;
    public final /* synthetic */ AnonymousClass2L8 A01;
    public final /* synthetic */ Runnable A02;
    public final /* synthetic */ String A03;

    public C862346i(AbstractC32491cF r1, AnonymousClass2L8 r2, Runnable runnable, String str) {
        this.A01 = r2;
        this.A03 = str;
        this.A02 = runnable;
        this.A00 = r1;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        AbstractC32491cF r0 = this.A00;
        if (r0 != null) {
            r0.Aaw(i);
        }
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r2) {
        Runnable runnable;
        if (r2.A0E(this.A03) != null && (runnable = this.A02) != null) {
            runnable.run();
        }
    }
}
