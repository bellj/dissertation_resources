package X;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1Yb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30601Yb {
    public final List A00 = new CopyOnWriteArrayList();

    public void A00(AbstractC30611Yc r3) {
        AnonymousClass009.A05(r3);
        List list = this.A00;
        if (!list.contains(r3)) {
            list.add(r3);
            return;
        }
        StringBuilder sb = new StringBuilder("Observer ");
        sb.append(r3);
        sb.append(" is already registered.");
        throw new IllegalStateException(sb.toString());
    }

    public void A01(Object obj) {
        for (AbstractC30611Yc r0 : this.A00) {
            r0.APz(obj);
        }
    }
}
