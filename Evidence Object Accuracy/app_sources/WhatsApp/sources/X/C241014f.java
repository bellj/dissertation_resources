package X;

import android.content.SharedPreferences;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;

/* renamed from: X.14f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241014f {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C241014f(C16630pM r1) {
        this.A01 = r1;
    }

    public void A00(GroupJid groupJid) {
        SharedPreferences sharedPreferences;
        Log.i("CommunitySharedPrefs/clearTempGroupType");
        synchronized (this) {
            sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A01.A01("community_shared_pref");
                this.A00 = sharedPreferences;
            }
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        StringBuilder sb = new StringBuilder("create_");
        sb.append(groupJid.getRawString());
        edit.remove(sb.toString()).apply();
    }
}
