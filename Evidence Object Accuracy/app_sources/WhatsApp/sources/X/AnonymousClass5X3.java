package X;

import com.whatsapp.voipcalling.VideoPort;

/* renamed from: X.5X3  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5X3 {
    void AOY(VideoPort videoPort);

    void APE(VideoPort videoPort);

    void ATu(VideoPort videoPort);

    void AUu(VideoPort videoPort);
}
