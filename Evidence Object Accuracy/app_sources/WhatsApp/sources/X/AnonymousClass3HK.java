package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.3HK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HK {
    public final AbstractC17190qP A00;
    public final C15580nU A01;
    public final UserJid A02;
    public final UserJid A03;
    public final AnonymousClass1S6 A04;
    public final Voip.CallState A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public /* synthetic */ AnonymousClass3HK(AbstractC17190qP r4, Voip.CallState callState) {
        this.A00 = r4;
        this.A07 = false;
        this.A0A = false;
        this.A05 = callState;
        this.A0D = false;
        this.A0B = false;
        this.A04 = null;
        this.A01 = null;
        this.A03 = null;
        this.A06 = "";
        this.A0C = false;
        this.A09 = false;
        this.A02 = null;
        this.A08 = false;
    }

    public AnonymousClass3HK(CallInfo callInfo) {
        AbstractC17190qP copyOf = AbstractC17190qP.copyOf(callInfo.participants);
        boolean isCallFull = callInfo.isCallFull();
        boolean isGroupCall = callInfo.isGroupCall();
        Voip.CallState callState = callInfo.callState;
        boolean z = callInfo.videoEnabled;
        boolean isInLonelyState = callInfo.isInLonelyState();
        AnonymousClass1S6 r7 = callInfo.self;
        C15580nU A02 = C15580nU.A02(callInfo.groupJid);
        UserJid peerJid = callInfo.getPeerJid();
        String str = callInfo.callId;
        boolean isSelfRequestingUpgrade = callInfo.isSelfRequestingUpgrade();
        boolean isEitherSideRequestingUpgrade = callInfo.isEitherSideRequestingUpgrade();
        UserJid userJid = callInfo.callLinkCreatorJid;
        boolean z2 = callInfo.isCallLinkLobbyState;
        this.A00 = copyOf;
        this.A07 = isCallFull;
        this.A0A = isGroupCall;
        this.A05 = callState;
        this.A0D = z;
        this.A0B = isInLonelyState;
        this.A04 = r7;
        this.A01 = A02;
        this.A03 = peerJid;
        this.A06 = str;
        this.A0C = isSelfRequestingUpgrade;
        this.A09 = isEitherSideRequestingUpgrade;
        this.A02 = userJid;
        this.A08 = z2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HK.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HK r5 = (AnonymousClass3HK) obj;
            if (!(this.A07 == r5.A07 && this.A0A == r5.A0A && this.A0D == r5.A0D && this.A0B == r5.A0B && this.A0C == r5.A0C && this.A09 == r5.A09 && this.A00.equals(r5.A00) && this.A05 == r5.A05 && C29941Vi.A00(this.A04, r5.A04) && C29941Vi.A00(this.A01, r5.A01) && C29941Vi.A00(this.A03, r5.A03) && C29941Vi.A00(this.A02, r5.A02) && this.A06.equals(r5.A06) && this.A08 == r5.A08)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[14];
        objArr[0] = this.A00;
        objArr[1] = Boolean.valueOf(this.A07);
        objArr[2] = Boolean.valueOf(this.A0A);
        objArr[3] = this.A05;
        objArr[4] = Boolean.valueOf(this.A0D);
        objArr[5] = Boolean.valueOf(this.A0B);
        objArr[6] = this.A04;
        objArr[7] = this.A01;
        objArr[8] = this.A03;
        objArr[9] = this.A06;
        objArr[10] = Boolean.valueOf(this.A0C);
        objArr[11] = Boolean.valueOf(this.A09);
        objArr[12] = this.A02;
        return C12980iv.A0B(Boolean.valueOf(this.A08), objArr, 13);
    }
}
