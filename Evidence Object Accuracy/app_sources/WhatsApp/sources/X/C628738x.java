package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.38x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628738x extends AbstractC16350or {
    public int A00;
    public boolean A01;
    public final C15550nR A02;
    public final C22700zV A03;
    public final C15610nY A04;
    public final AnonymousClass018 A05;
    public final C19990v2 A06;
    public final C15680nj A07;
    public final C14850m9 A08;
    public final AbstractC14640lm A09;
    public final C15860o1 A0A;
    public final String A0B;
    public final WeakReference A0C;
    public final ArrayList A0D;
    public final HashSet A0E;
    public final List A0F;
    public final List A0G;
    public final List A0H;
    public final List A0I;
    public final Set A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    public final boolean A0Q;
    public final boolean A0R;
    public final boolean A0S;
    public final boolean A0T;
    public final boolean A0U;
    public final boolean A0V;

    public C628738x(C15550nR r2, C22700zV r3, C15610nY r4, ContactPickerFragment contactPickerFragment, AnonymousClass018 r6, C19990v2 r7, C15680nj r8, C14850m9 r9, AbstractC14640lm r10, C15860o1 r11, String str, HashSet hashSet, List list, List list2, List list3, List list4, List list5, Set set, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13) {
        ArrayList arrayList;
        this.A0C = C12970iu.A10(contactPickerFragment);
        this.A0B = str;
        if (list != null) {
            arrayList = C12980iv.A0x(list);
        } else {
            arrayList = null;
        }
        this.A0D = arrayList;
        this.A0F = list2;
        this.A0I = list3;
        this.A0G = list4;
        this.A0H = list5;
        this.A0E = hashSet;
        this.A09 = r10;
        this.A0J = set;
        this.A0O = z;
        this.A0V = z2;
        this.A0N = z3;
        this.A0Q = z4;
        this.A0U = z5;
        this.A0M = z6;
        this.A0P = z7;
        this.A0S = z8;
        this.A0T = z9;
        this.A0R = z10;
        this.A0K = z11;
        this.A0L = z12;
        this.A01 = z13;
        this.A00 = i;
        this.A08 = r9;
        this.A06 = r7;
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r6;
        this.A0A = r11;
        this.A03 = r3;
        this.A07 = r8;
    }

    public static void A00(AnonymousClass01E r1, AbstractCollection abstractCollection, int i) {
        abstractCollection.add(new AnonymousClass552(r1.A0I(i)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0059, code lost:
        if (r22.A08.A07(691) == false) goto L_0x005b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003a  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r23) {
        /*
        // Method dump skipped, instructions count: 1409
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C628738x.A05(java.lang.Object[]):java.lang.Object");
    }

    public final boolean A08(C15370n3 r4, boolean z) {
        UserJid A05;
        if (r4.A0K()) {
            A05 = r4.A0E;
        } else {
            A05 = C15370n3.A05(r4);
        }
        if (!z && A05 != null && this.A03.A02(A05)) {
            return false;
        }
        if (!this.A0L) {
            return true;
        }
        if (!this.A0T && !this.A0V && !this.A0Q) {
            return true;
        }
        UserJid A052 = C15370n3.A05(r4);
        if (AnonymousClass3GN.A01(this.A08, A052) || !new C38301nr(this.A03, A052).A03()) {
            return true;
        }
        return false;
    }
}
