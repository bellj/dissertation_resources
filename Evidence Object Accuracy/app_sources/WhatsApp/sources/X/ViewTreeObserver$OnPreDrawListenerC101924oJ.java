package X;

import android.view.ViewTreeObserver;
import com.whatsapp.registration.ChangeNumberOverview;

/* renamed from: X.4oJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101924oJ implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ChangeNumberOverview A00;

    public ViewTreeObserver$OnPreDrawListenerC101924oJ(ChangeNumberOverview changeNumberOverview) {
        this.A00 = changeNumberOverview;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        ChangeNumberOverview changeNumberOverview = this.A00;
        C12980iv.A1G(changeNumberOverview.A02, this);
        changeNumberOverview.A2e();
        return false;
    }
}
