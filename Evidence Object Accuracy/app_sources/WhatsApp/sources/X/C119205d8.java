package X;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.WaEditText;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5d8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119205d8 extends AbstractC15280mr {
    public int A00 = this.A01.getMeasuredHeight();
    public NumberEntryKeyboard A01;
    public AnonymousClass1IT A02;
    public List A03;

    public C119205d8(Activity activity, AbstractC15710nm r13, AbstractC49822Mw r14, AnonymousClass01d r15, C14820m6 r16, AnonymousClass5W6 r17, AnonymousClass1IT r18, C252718t r19, List list) {
        super(activity, r13, r14, r15, r16, r19);
        this.A02 = r18;
        this.A03 = list;
        NumberEntryKeyboard numberEntryKeyboard = new NumberEntryKeyboard(activity);
        this.A01 = numberEntryKeyboard;
        numberEntryKeyboard.A06 = r18;
        numberEntryKeyboard.setCustomKey(r17);
        this.A02.setCustomCursorEnabled(true);
        setContentView(this.A01);
        setTouchable(true);
        setOutsideTouchable(true);
        setInputMethodMode(2);
        setAnimationStyle(0);
        setBackgroundDrawable(new ColorDrawable(-1));
        setTouchInterceptor(new View.OnTouchListener(list) { // from class: X.64y
            public final /* synthetic */ List A01;

            {
                this.A01 = r2;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                Rect rect;
                int i;
                int i2;
                C119205d8 r4 = C119205d8.this;
                List<WaEditText> list2 = this.A01;
                if (!(motionEvent.getActionMasked() == 2 || motionEvent.getActionMasked() == 1)) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    int[] iArr = new int[2];
                    view.getLocationOnScreen(iArr);
                    Point point = new Point(((int) x) + iArr[0], ((int) y) + iArr[1]);
                    for (WaEditText waEditText : list2) {
                        if (AbstractC15280mr.A02(point, waEditText) && ((rect = waEditText.A00) == null || ((i = point.x) >= rect.left && i <= rect.right && (i2 = point.y) >= rect.top && i2 <= rect.bottom))) {
                            r4.A08(waEditText);
                            return true;
                        }
                    }
                    if (motionEvent.getY() < 0.0f) {
                        return true;
                    }
                }
                r4.A01.A0I.onTouch(view, motionEvent);
                return false;
            }
        });
        this.A01.measure(View.MeasureSpec.makeMeasureSpec(activity.getWindowManager().getDefaultDisplay().getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    @Override // X.AbstractC15280mr
    public int A03(int i) {
        return this.A00;
    }

    @Override // X.AbstractC15280mr
    public void A06() {
        if (!isShowing()) {
            super.A06();
            Iterator it = this.A03.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                View view = (View) it.next();
                if (C252718t.A00(view)) {
                    if (view != null) {
                        AbstractC49822Mw r8 = this.A05;
                        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) r8;
                        keyboardPopupLayout.A06 = true;
                        if (!this.A06.A0Q().hideSoftInputFromWindow(view.getWindowToken(), 0, new ResultReceiverC73413gC(C12970iu.A0E(), new Runnable() { // from class: X.6FO
                            @Override // java.lang.Runnable
                            public final void run() {
                                C119205d8.this.A0A();
                            }
                        }, this.A0A))) {
                            keyboardPopupLayout.A06 = false;
                            ((View) r8).requestLayout();
                            return;
                        }
                        return;
                    }
                }
            }
            A0A();
        }
    }

    public final void A0A() {
        if (!isShowing()) {
            Activity activity = super.A03;
            if (activity.getCurrentFocus() != null) {
                activity.getCurrentFocus().clearFocus();
            }
            setHeight(this.A00);
            setWidth(-1);
            AbstractC49822Mw r3 = this.A05;
            r3.setKeyboardPopup(this);
            KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) r3;
            if (keyboardPopupLayout.A06) {
                View view = (View) r3;
                view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC1320164z(this));
                keyboardPopupLayout.A06 = false;
                view.requestLayout();
            } else if (!isShowing()) {
                showAtLocation((View) r3, 48, 0, 1000000);
            }
            this.A02.setHasFocus(true);
        }
    }

    @Override // X.AbstractC15280mr, android.widget.PopupWindow
    public void dismiss() {
        this.A02.setHasFocus(false);
        super.dismiss();
    }
}
