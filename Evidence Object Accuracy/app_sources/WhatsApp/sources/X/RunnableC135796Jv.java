package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.6Jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135796Jv implements Runnable {
    public final /* synthetic */ int A00 = R.string.upi_mandate_missing_payment_method_title;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ C118005b7 A03;

    public /* synthetic */ RunnableC135796Jv(C118005b7 r2, int i, int i2) {
        this.A03 = r2;
        this.A01 = i;
        this.A02 = i2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C128285vr r2;
        C118005b7 r6 = this.A03;
        int i = this.A00;
        int i2 = this.A01;
        int i3 = this.A02;
        C241414j r22 = r6.A0C;
        AnonymousClass1IR r1 = r6.A07;
        AbstractC28901Pl A08 = r22.A08(r1.A0H);
        r6.A05 = A08;
        if (A08 == null) {
            r2 = new C128285vr(3);
            Context context = r6.A04.A00;
            r2.A08 = context.getString(i);
            r2.A07 = context.getString(i2);
        } else {
            r2 = new C128285vr(i3);
            r2.A03 = r1;
        }
        r6.A09.A0A(r2);
    }
}
