package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.40Y  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass40Y extends AbstractC37191le {
    public final WaTextView A00;

    public AnonymousClass40Y(View view) {
        super(view);
        this.A00 = (WaTextView) AnonymousClass028.A0D(view, R.id.title);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        WaTextView waTextView = this.A00;
        int i = ((AnonymousClass406) obj).A00;
        int i2 = R.string.biz_neighbourhoods;
        if (i == 0) {
            i2 = R.string.biz_cities;
        }
        waTextView.setText(i2);
    }
}
