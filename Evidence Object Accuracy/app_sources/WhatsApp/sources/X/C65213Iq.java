package X;

import android.content.Context;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.3Iq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65213Iq {
    public static final Map A00 = new C71743dR();

    public static AbstractC35401hl A00(Context context) {
        AbstractC35401hl r0;
        Conversation conversation = (Conversation) AbstractC35731ia.A01(context, Conversation.class);
        if (conversation == null || (r0 = conversation.A3t) == null) {
            return new AnonymousClass2I4();
        }
        return r0;
    }

    public static Integer A01(C14850m9 r4, String str) {
        if (!r4.A07(1775)) {
            int length = str.length();
            if (length == 1) {
                if (!(str.charAt(0) == 57378 || str.charAt(0) == 10084)) {
                    return null;
                }
            } else if (!(length == 2 && str.charAt(0) == 10084 && str.charAt(1) == 65039)) {
                return null;
            }
            return Integer.valueOf((int) R.drawable.large_e022);
        } else if (C38491oB.A02(str)) {
            return (Integer) A00.get(new C37471mS(C38491oB.A04(new C37471mS(str).A00)).toString());
        } else {
            return null;
        }
    }

    public static boolean A02(C15570nT r4, C15550nR r5, C20830wO r6, C21250x7 r7, C20710wC r8, AbstractC15340mz r9) {
        C15370n3 r52;
        AnonymousClass1IS r2 = r9.A0z;
        if (r2.A02) {
            return true;
        }
        AbstractC14640lm r22 = r2.A00;
        AnonymousClass009.A05(r22);
        if (r7.A00(r22) == 1) {
            return true;
        }
        C15370n3 A01 = r6.A01(r22);
        if (A01.A0K()) {
            AbstractC14640lm A0B = r9.A0B();
            if (A0B == null) {
                return false;
            }
            C15370n3 A0B2 = r5.A0B(A0B);
            UserJid A0D = r5.A0D((GroupJid) A01.A0B(C15580nU.class));
            if (A0D != null) {
                r52 = r5.A0B(A0D);
            } else {
                r52 = null;
            }
            r4.A08();
            C27631Ih r42 = r4.A05;
            AnonymousClass009.A05(r42);
            if (r8.A0X.A0F((C15580nU) C15370n3.A03(A01, C15580nU.class))) {
                return true;
            }
            if ((r52 == null || (r52.A0C == null && !r42.equals(r52.A0D))) && A0B2.A0C == null && A0B2.A06 != 3) {
                return false;
            }
            return true;
        } else if (A01.A0C != null || A01.A06 == 3) {
            return true;
        } else {
            if (r22 == null) {
                return false;
            }
            int type = r22.getType();
            if (type == 8 || type == 7) {
                return true;
            }
            return false;
        }
    }
}
