package X;

import android.graphics.Bitmap;

/* renamed from: X.3he  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74243he extends C006202y {
    public final /* synthetic */ C64843Hc A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74243he(C64843Hc r1, int i) {
        super(i);
        this.A00 = r1;
    }

    @Override // X.C006202y
    public int A02(Object obj, Object obj2) {
        return ((Bitmap) obj2).getByteCount() >> 10;
    }
}
