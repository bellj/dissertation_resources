package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3b3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70293b3 implements AbstractC41521tf {
    public final /* synthetic */ C60892yw A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70293b3(C60892yw r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        C58552qq r0 = this.A00.A00;
        if (r0 != null) {
            return C12960it.A09(r0).getDimensionPixelSize(R.dimen.link_preview_thumb_width);
        }
        return 0;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r4) {
        if (bitmap != null) {
            ((ImageView) view).setImageBitmap(bitmap);
        } else {
            view.setVisibility(8);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        view.setVisibility(8);
    }
}
