package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128935wu {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;
    public final /* synthetic */ HashMap A02;

    public /* synthetic */ C128935wu(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, HashMap hashMap) {
        this.A01 = brazilPayBloksActivity;
        this.A02 = hashMap;
        this.A00 = r1;
    }

    public final void A00(C128075vW r18) {
        String str;
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        HashMap hashMap = this.A02;
        AnonymousClass3FE r2 = this.A00;
        if (r18 != null) {
            hashMap.put("network_name", C30881Ze.A07(r18.A00));
        }
        r2.A01("on_success", hashMap);
        C129385xd r1 = brazilPayBloksActivity.A0G;
        r1.A00 = r18;
        if (r18 == null || r18.A00 != 5 || (str = r18.A04) == null || !str.equals("0")) {
            r1.A0B.A05(r18);
            r1.A03 = false;
            return;
        }
        String str2 = r18.A06;
        if (str2 != null) {
            C18600si r5 = r1.A0E;
            String A0p = C12980iv.A0p(r5.A01(), "payment_trusted_device_elo_wallet_store");
            JSONObject jSONObject = null;
            try {
                if (A0p != null) {
                    jSONObject = C13000ix.A05(A0p);
                } else {
                    jSONObject = C117295Zj.A0a();
                }
                jSONObject.put("wallet_id", str2);
            } catch (JSONException unused) {
                r5.A02.A06("Failed to updated the wallet_id");
            }
            C12970iu.A1D(C117295Zj.A05(r5), "payment_trusted_device_elo_wallet_store", jSONObject.toString());
        }
        C14830m7 r8 = r1.A07;
        C129345xZ r3 = new C129345xZ(r1.A08.A00, r1.A04, r1.A05, r1.A06, r8, r1.A0A, r1.A0C, r1.A0D, r1.A0E, r1.A0F, r1.A0G, new C128535wG(r1), r1.A0H);
        AnonymousClass60T r9 = r3.A0A;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r9, "ELO", "ADD-CARD");
        if (A0C == null) {
            new C129215xM(r3.A00, r3.A01, r3.A07, r3.A09, r9, "ADD-CARD").A00(new C133336Aj(r3), "ELO");
        } else {
            r3.A00(null, A0C);
        }
    }
}
