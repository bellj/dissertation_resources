package X;

/* renamed from: X.49j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC868949j {
    A01(false),
    A02(true),
    A03(true),
    A04(false);
    
    public final boolean zzjk;

    EnumC868949j(boolean z) {
        this.zzjk = z;
    }
}
