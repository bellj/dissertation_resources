package X;

import android.content.Context;
import android.os.PowerManager;
import java.util.WeakHashMap;

/* renamed from: X.0RM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RM {
    public static final String A00 = C06390Tk.A01("WakeLocks");
    public static final WeakHashMap A01 = new WeakHashMap();

    public static PowerManager.WakeLock A00(Context context, String str) {
        StringBuilder sb = new StringBuilder("WorkManager: ");
        sb.append(str);
        String obj = sb.toString();
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getApplicationContext().getSystemService("power")).newWakeLock(1, obj);
        WeakHashMap weakHashMap = A01;
        synchronized (weakHashMap) {
            weakHashMap.put(newWakeLock, obj);
        }
        return newWakeLock;
    }
}
