package X;

/* renamed from: X.4zX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108714zX implements AbstractC115635Si {
    public final /* synthetic */ C15110ma A00;
    public final /* synthetic */ Runnable A01;

    public C108714zX(C15110ma r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // X.AbstractC115635Si
    public final void Agq(Throwable th) {
        this.A00.A01.post(this.A01);
    }
}
