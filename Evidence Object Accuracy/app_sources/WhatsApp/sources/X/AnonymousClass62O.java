package X;

import android.content.Context;
import android.content.DialogInterface;

/* renamed from: X.62O  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass62O implements DialogInterface.OnClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass69D A01;

    public /* synthetic */ AnonymousClass62O(Context context, AnonymousClass69D r2) {
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        Context context = this.A00;
        AnonymousClass009.A05("https://faq.whatsapp.com/payments/26000351");
        context.startActivity(C14960mK.A0a(context, "https://faq.whatsapp.com/payments/26000351", null, false, true));
    }
}
