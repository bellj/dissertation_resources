package X;

import com.whatsapp.util.Log;

/* renamed from: X.0wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21170wz {
    public final AbstractC15710nm A00;

    public C21170wz(AbstractC15710nm r1) {
        this.A00 = r1;
    }

    public void A00(Exception exc) {
        Log.e("wamsysJniBridge/caught exception", exc);
        this.A00.AaV("wamsysJniBridge/caught exception", exc.getMessage(), true);
    }
}
