package X;

import java.util.Collection;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20450vm implements AbstractC20460vn {
    public final C16120oU A00;
    public final AtomicInteger A01 = new AtomicInteger();

    public C20450vm(C16120oU r2) {
        this.A00 = r2;
    }

    public final void A00(String str, Object[] objArr, int i) {
        if (this.A01.getAndIncrement() < 5) {
            AnonymousClass43O r1 = new AnonymousClass43O();
            r1.A00 = Integer.valueOf(i);
            if (str != null) {
                r1.A01 = String.format(Locale.US, str, objArr);
            }
            this.A00.A07(r1);
        }
    }

    @Override // X.AbstractC20460vn
    public void A5w(int i, String str) {
        A00("markerId:%d, annotationKey:%s", new Object[]{Integer.valueOf(i), str}, 13);
    }

    @Override // X.AbstractC20460vn
    public void A5x(int i, String str, int i2) {
        A00("markerId:%d, annotationKey:%s", new Object[]{Integer.valueOf(i), str}, 1);
    }

    @Override // X.AbstractC20460vn
    public void A6J() {
        A00(null, new Object[0], 17);
    }

    @Override // X.AbstractC20460vn
    public void A9a(String str) {
        A00("errorString:%s", new Object[]{str}, 16);
    }

    @Override // X.AbstractC20460vn
    public void A9b(String str) {
        A00("errorString:%s", new Object[]{str}, 11);
    }

    @Override // X.AbstractC20460vn
    public void A9c(String str) {
        A00("errorString:%s", new Object[]{str}, 15);
    }

    @Override // X.AbstractC20460vn
    public void A9d(String str) {
        if (str != null && str.length() > 200) {
            str = str.substring(0, 200);
        }
        A00("errorString:%s", new Object[]{str}, 10);
    }

    @Override // X.AbstractC20460vn
    public void A9e(String str) {
        A00("errorString:%s", new Object[]{str}, 8);
    }

    @Override // X.AbstractC20460vn
    public void AIV(String str, double d, int i) {
        A00("markerId:%d, annotationKey:%s, value:%s", new Object[]{Integer.valueOf(i), str, Double.toString(d)}, 7);
    }

    @Override // X.AbstractC20460vn
    public void AKL(int i, String str) {
        A00("markerId:%d, errorString:%s", new Object[]{Integer.valueOf(i), str}, 6);
    }

    @Override // X.AbstractC20460vn
    public void ALM() {
        A00(null, new Object[0], 9);
    }

    @Override // X.AbstractC20460vn
    public void ALN(int i) {
        A00("markerId:%d", new Object[]{Integer.valueOf(i)}, 3);
    }

    @Override // X.AbstractC20460vn
    public void ALO(int i) {
        A00("markerId:%d", new Object[]{Integer.valueOf(i)}, 2);
    }

    @Override // X.AbstractC20460vn
    public void AZI(int i, String str) {
        A00("markerId:%d, pointName:%s", new Object[]{Integer.valueOf(i), str}, 12);
    }

    @Override // X.AbstractC20460vn
    public void AZJ(int i, String str) {
        A00("markerId:%d, pointName:%s", new Object[]{Integer.valueOf(i), "start_foreground_service"}, 5);
    }

    @Override // X.AbstractC20460vn
    public void Af3(Collection collection) {
        Object[] objArr = new Object[1];
        String str = "null";
        if (collection != null) {
            str = collection.toString();
        }
        objArr[0] = str;
        A00("allOpenMarkerIds:%s", objArr, 4);
    }

    @Override // X.AbstractC20460vn
    public void AfC() {
        A00(null, new Object[0], 18);
    }
}
