package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

/* renamed from: X.09I  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09I extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewPropertyAnimator A01;
    public final /* synthetic */ C05140Ok A02;
    public final /* synthetic */ AnonymousClass0FH A03;

    public AnonymousClass09I(View view, ViewPropertyAnimator viewPropertyAnimator, C05140Ok r3, AnonymousClass0FH r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = viewPropertyAnimator;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setListener(null);
        View view = this.A00;
        view.setAlpha(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        AnonymousClass0FH r3 = this.A03;
        C05140Ok r2 = this.A02;
        r3.A03(r2.A04);
        r3.A02.remove(r2.A04);
        if (!r3.A0B()) {
            r3.A02();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }
}
