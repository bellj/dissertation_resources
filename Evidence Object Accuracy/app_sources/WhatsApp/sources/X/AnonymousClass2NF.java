package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2NF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NF extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public AnonymousClass2NF(AnonymousClass2L8 r1, String str, String str2) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = str2;
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r6) {
        C450720b r3 = this.A00.A0H;
        String str = this.A01;
        String str2 = this.A02;
        Log.i("xmpp/reader/on-set-two-factor-auth-confirmation");
        AbstractC450820c r4 = r3.A00;
        Bundle bundle = new Bundle();
        bundle.putString("code", str);
        bundle.putString("email", str2);
        r4.AYY(Message.obtain(null, 0, 102, 0, bundle));
    }
}
