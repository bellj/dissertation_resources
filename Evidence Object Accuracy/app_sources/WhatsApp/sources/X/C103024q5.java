package X;

import android.content.Context;
import com.whatsapp.conversation.conversationrow.message.KeptMessagesActivity;

/* renamed from: X.4q5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103024q5 implements AbstractC009204q {
    public final /* synthetic */ KeptMessagesActivity A00;

    public C103024q5(KeptMessagesActivity keptMessagesActivity) {
        this.A00 = keptMessagesActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
