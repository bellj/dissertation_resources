package X;

import android.graphics.Paint;

/* renamed from: X.0Jg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03820Jg {
    /* Fake field, exist only in values array */
    MITER,
    /* Fake field, exist only in values array */
    ROUND,
    /* Fake field, exist only in values array */
    BEVEL;

    public Paint.Join A00() {
        switch (ordinal()) {
            case 0:
                return Paint.Join.MITER;
            case 1:
                return Paint.Join.ROUND;
            case 2:
                return Paint.Join.BEVEL;
            default:
                return null;
        }
    }
}
