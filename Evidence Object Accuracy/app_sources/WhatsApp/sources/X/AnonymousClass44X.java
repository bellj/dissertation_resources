package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import java.util.Set;

/* renamed from: X.44X  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44X extends AbstractC33331dp {
    public final /* synthetic */ C36691kN A00;

    public AnonymousClass44X(C36691kN r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A0E.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 4, set));
    }
}
