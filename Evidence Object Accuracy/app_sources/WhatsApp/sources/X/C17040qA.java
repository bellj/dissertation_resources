package X;

/* renamed from: X.0qA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17040qA {
    public final C18640sm A00;
    public final C14830m7 A01;
    public final C16120oU A02;
    public final C26491Dr A03;

    public C17040qA(C18640sm r1, C14830m7 r2, C16120oU r3, C26491Dr r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
    }

    public final int A00() {
        Integer A01 = C22050yP.A01(this.A00.A07());
        if (A01 == null) {
            return 0;
        }
        return A01.intValue();
    }

    public final long A01() {
        long A00 = this.A01.A00();
        return A00 - (A00 % 86400000);
    }

    public synchronized void A02(int i, int i2) {
        long A01 = A01();
        int A00 = A00();
        C26491Dr r5 = this.A03;
        C41441tX A012 = r5.A01(i2, A00, i, A01, false);
        A012.A05++;
        r5.A02(A012, i2, A00, i, A01, false);
    }

    public synchronized void A03(int i, int i2) {
        long A01 = A01();
        int A00 = A00();
        C26491Dr r5 = this.A03;
        C41441tX A012 = r5.A01(i, A00, i2, A01, false);
        A012.A08++;
        r5.A02(A012, i, A00, i2, A01, false);
    }
}
