package X;

import com.whatsapp.R;
import java.util.Map;
import java.util.UUID;

/* renamed from: X.0rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18120rw {
    public final C15570nT A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01N A02;
    public final AnonymousClass01N A03;

    public C18120rw(C15570nT r2, AnonymousClass01H r3, AnonymousClass01N r4, AnonymousClass01N r5) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r4, 2);
        C16700pc.A0E(r3, 3);
        C16700pc.A0E(r5, 4);
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r5;
    }

    public static /* synthetic */ void A00(ActivityC13810kN r17, C18120rw r18) {
        C16700pc.A0E(r17, 0);
        C235812f r1 = (C235812f) r18.A02.get();
        if (((C18160s0) r18.A03.get()).A01() == null) {
            C16700pc.A0B(r1);
            r17.Ado(R.string.avatar_launcher_failed_generic);
            r1.A03(1, null, "EXPIRED_TOKEN", 6);
            return;
        }
        r17.Ady(0, R.string.avatar_launcher_loading);
        AnonymousClass3CR r6 = new AnonymousClass3CR();
        String str = r1.A01;
        if (str == null) {
            str = UUID.randomUUID().toString();
            r1.A01 = str;
        }
        C16700pc.A0C(str);
        C16700pc.A0E(str, 1);
        Map map = r6.A00;
        map.put("logging_session_id", str);
        map.put("logging_surface", "wa_settings");
        map.put("logging_mechanism", "wa_settings_item");
        String A00 = r6.A00();
        AnonymousClass1AG r2 = (AnonymousClass1AG) r18.A01.get();
        boolean A08 = C41691tw.A08(r17);
        C15570nT r0 = r18.A00;
        r0.A08();
        C27631Ih r02 = r0.A05;
        C16700pc.A0C(r02);
        String rawString = r02.getRawString();
        C16700pc.A0B(rawString);
        C65963Lt r7 = AnonymousClass4GO.A00;
        AnonymousClass3UY r03 = new AnonymousClass3UY(r17, r18, r1);
        C64173En r12 = new C64173En(r17, r17.A0V(), r2.A00, rawString, null, A08);
        C70993cD r8 = new C70993cD(r17, r03, r12);
        r2.A02.A00(r17, r2.A01, r12);
        AbstractC16850pr r62 = r2.A03;
        r62.A02(r7, r8, null, "com.bloks.www.avatar.editor.cds.launcher.async", A00, null, r62.A00.contains("com.bloks.www.avatar.editor.cds.launcher.async"));
    }
}
