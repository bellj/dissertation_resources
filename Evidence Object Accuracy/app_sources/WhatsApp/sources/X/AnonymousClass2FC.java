package X;

import android.util.Base64;
import com.whatsapp.jid.UserJid;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.2FC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FC {
    public final AnonymousClass2FG A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public AnonymousClass2FC(AnonymousClass2FG r1, String str, String str2, String str3) {
        this.A02 = str;
        this.A01 = str2;
        this.A03 = str3;
        this.A00 = r1;
    }

    public static String A00(UserJid userJid) {
        String str = userJid.user;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes(C88814Hf.A05));
            return Base64.encodeToString(instance.digest(), 2);
        } catch (NoSuchAlgorithmException e) {
            throw new IOException(e);
        }
    }
}
