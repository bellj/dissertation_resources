package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC869049k extends Enum {
    public static final /* synthetic */ EnumC869049k[] A00;
    @Deprecated
    public static final EnumC869049k A01;
    public static final EnumC869049k A02;
    public static final EnumC869049k A03;
    public static final EnumC869049k A04;
    public static final EnumC869049k A05;
    public static final EnumC869049k A06;
    public static final EnumC869049k A07;
    public static final EnumC869049k A08;
    public static final EnumC869049k A09;
    public static final EnumC869049k A0A;
    public static final EnumC869049k A0B;
    public static final EnumC869049k A0C;
    public static final EnumC869049k A0D;
    public static final EnumC869049k A0E;
    public static final EnumC869049k A0F;
    public static final EnumC869049k A0G;
    public static final EnumC869049k A0H;
    public static final EnumC869049k A0I;
    public static final EnumC869049k A0J;
    public final String zzek;

    public EnumC869049k(String str, String str2, int i) {
        this.zzek = str2;
    }

    public static EnumC869049k[] values() {
        return (EnumC869049k[]) A00.clone();
    }

    static {
        EnumC869049k A002 = A00("CLIENT_LOGIN_DISABLED", "ClientLoginDisabled", 0);
        EnumC869049k A003 = A00("DEVICE_MANAGEMENT_REQUIRED", "DeviceManagementRequiredOrSyncDisabled", 1);
        A01 = A003;
        EnumC869049k A004 = A00("SOCKET_TIMEOUT", "SocketTimeout", 2);
        EnumC869049k A005 = A00("SUCCESS", "Ok", 3);
        EnumC869049k A006 = A00("UNKNOWN_ERROR", "UNKNOWN_ERR", 4);
        EnumC869049k A007 = A00("NETWORK_ERROR", "NetworkError", 5);
        A02 = A007;
        EnumC869049k A008 = A00("SERVICE_UNAVAILABLE", "ServiceUnavailable", 6);
        A03 = A008;
        EnumC869049k A009 = A00("INTNERNAL_ERROR", "InternalError", 7);
        A04 = A009;
        EnumC869049k A0010 = A00("BAD_AUTHENTICATION", "BadAuthentication", 8);
        A05 = A0010;
        EnumC869049k A0011 = A00("EMPTY_CONSUMER_PKG_OR_SIG", "EmptyConsumerPackageOrSig", 9);
        EnumC869049k A0012 = A00("NEEDS_2F", "InvalidSecondFactor", 10);
        EnumC869049k A0013 = A00("NEEDS_POST_SIGN_IN_FLOW", "PostSignInFlowRequired", 11);
        EnumC869049k A0014 = A00("NEEDS_BROWSER", "NeedsBrowser", 12);
        A06 = A0014;
        EnumC869049k A0015 = A00("UNKNOWN", "Unknown", 13);
        EnumC869049k A0016 = A00("NOT_VERIFIED", "NotVerified", 14);
        EnumC869049k A0017 = A00("TERMS_NOT_AGREED", "TermsNotAgreed", 15);
        EnumC869049k A0018 = A00("ACCOUNT_DISABLED", "AccountDisabled", 16);
        EnumC869049k A0019 = A00("CAPTCHA", "CaptchaRequired", 17);
        A07 = A0019;
        EnumC869049k A0020 = A00("ACCOUNT_DELETED", "AccountDeleted", 18);
        EnumC869049k A0021 = A00("SERVICE_DISABLED", "ServiceDisabled", 19);
        EnumC869049k A0022 = A00("NEED_PERMISSION", "NeedPermission", 20);
        A08 = A0022;
        EnumC869049k A0023 = A00("NEED_REMOTE_CONSENT", "NeedRemoteConsent", 21);
        A09 = A0023;
        EnumC869049k r13 = new EnumC869049k("INVALID_SCOPE", "INVALID_SCOPE", 22);
        EnumC869049k A0024 = A00("USER_CANCEL", "UserCancel", 23);
        A0A = A0024;
        EnumC869049k A0025 = A00("PERMISSION_DENIED", "PermissionDenied", 24);
        EnumC869049k r11 = new EnumC869049k("INVALID_AUDIENCE", "INVALID_AUDIENCE", 25);
        EnumC869049k r10 = new EnumC869049k("UNREGISTERED_ON_API_CONSOLE", "UNREGISTERED_ON_API_CONSOLE", 26);
        EnumC869049k A0026 = A00("THIRD_PARTY_DEVICE_MANAGEMENT_REQUIRED", "ThirdPartyDeviceManagementRequired", 27);
        A0B = A0026;
        EnumC869049k A0027 = A00("DM_INTERNAL_ERROR", "DeviceManagementInternalError", 28);
        A0C = A0027;
        EnumC869049k A0028 = A00("DM_SYNC_DISABLED", "DeviceManagementSyncDisabled", 29);
        A0D = A0028;
        EnumC869049k A0029 = A00("DM_ADMIN_BLOCKED", "DeviceManagementAdminBlocked", 30);
        A0E = A0029;
        EnumC869049k A0030 = A00("DM_ADMIN_PENDING_APPROVAL", "DeviceManagementAdminPendingApproval", 31);
        A0F = A0030;
        EnumC869049k A0031 = A00("DM_STALE_SYNC_REQUIRED", "DeviceManagementStaleSyncRequired", 32);
        A0G = A0031;
        EnumC869049k A0032 = A00("DM_DEACTIVATED", "DeviceManagementDeactivated", 33);
        A0H = A0032;
        EnumC869049k A0033 = A00("DM_SCREENLOCK_REQUIRED", "DeviceManagementScreenlockRequired", 34);
        A0I = A0033;
        EnumC869049k A0034 = A00("DM_REQUIRED", "DeviceManagementRequired", 35);
        A0J = A0034;
        EnumC869049k r4 = new EnumC869049k("ALREADY_HAS_GMAIL", "ALREADY_HAS_GMAIL", 36);
        EnumC869049k A0035 = A00("BAD_PASSWORD", "WeakPassword", 37);
        EnumC869049k A0036 = A00("BAD_REQUEST", "BadRequest", 38);
        EnumC869049k A0037 = A00("BAD_USERNAME", "BadUsername", 39);
        EnumC869049k A0038 = A00("DELETED_GMAIL", "DeletedGmail", 40);
        EnumC869049k A0039 = A00("EXISTING_USERNAME", "ExistingUsername", 41);
        EnumC869049k A0040 = A00("LOGIN_FAIL", "LoginFail", 42);
        EnumC869049k A0041 = A00("NOT_LOGGED_IN", "NotLoggedIn", 43);
        EnumC869049k A0042 = A00("NO_GMAIL", "NoGmail", 44);
        EnumC869049k A0043 = A00("REQUEST_DENIED", "RequestDenied", 45);
        EnumC869049k A0044 = A00("SERVER_ERROR", "ServerError", 46);
        EnumC869049k A0045 = A00("USERNAME_UNAVAILABLE", "UsernameUnavailable", 47);
        EnumC869049k A0046 = A00("GPLUS_OTHER", "GPlusOther", 48);
        EnumC869049k A0047 = A00("GPLUS_NICKNAME", "GPlusNickname", 49);
        EnumC869049k A0048 = A00("GPLUS_INVALID_CHAR", "GPlusInvalidChar", 50);
        EnumC869049k A0049 = A00("GPLUS_INTERSTITIAL", "GPlusInterstitial", 51);
        EnumC869049k A0050 = A00("GPLUS_PROFILE_ERROR", "ProfileUpgradeError", 52);
        EnumC869049k[] r2 = new EnumC869049k[53];
        C72453ed.A1F(A002, A003, A004, A005, r2);
        r2[4] = A006;
        C12970iu.A1R(A007, A008, A009, A0010, r2);
        C72453ed.A1G(A0011, A0012, A0013, A0014, r2);
        C72453ed.A1H(A0015, A0016, A0017, A0018, r2);
        C72453ed.A1I(A0019, A0020, A0021, r2);
        C12960it.A1G(A0022, A0023, r13, A0024, r2);
        r2[24] = A0025;
        C12960it.A1H(r11, r10, A0026, A0027, r2);
        C12970iu.A1S(A0028, A0029, A0030, r2);
        r2[32] = A0031;
        r2[33] = A0032;
        r2[34] = A0033;
        r2[35] = A0034;
        C12980iv.A1O(r4, A0035, A0036, r2);
        r2[39] = A0037;
        r2[40] = A0038;
        r2[41] = A0039;
        r2[42] = A0040;
        r2[43] = A0041;
        r2[44] = A0042;
        r2[45] = A0043;
        r2[46] = A0044;
        r2[47] = A0045;
        r2[48] = A0046;
        r2[49] = A0047;
        r2[50] = A0048;
        r2[51] = A0049;
        r2[52] = A0050;
        A00 = r2;
    }

    public static EnumC869049k A00(String str, String str2, int i) {
        return new EnumC869049k(str, str2, i);
    }
}
