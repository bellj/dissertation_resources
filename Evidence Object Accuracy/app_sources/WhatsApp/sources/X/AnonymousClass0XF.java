package X;

import android.view.MenuItem;
import androidx.appcompat.widget.ActionMenuView;

/* renamed from: X.0XF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XF implements AbstractC011605p {
    public final /* synthetic */ ActionMenuView A00;

    public AnonymousClass0XF(ActionMenuView actionMenuView) {
        this.A00 = actionMenuView;
    }

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r4) {
        AbstractC017308c r0;
        AbstractC014907a r02 = this.A00.A09;
        return (r02 == null || (r0 = ((AnonymousClass07Z) r02).A00.A0R) == null || !r0.onMenuItemClick(menuItem)) ? false : true;
    }

    @Override // X.AbstractC011605p
    public void ASi(AnonymousClass07H r2) {
        AbstractC011605p r0 = this.A00.A05;
        if (r0 != null) {
            r0.ASi(r2);
        }
    }
}
