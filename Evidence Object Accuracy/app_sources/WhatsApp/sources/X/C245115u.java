package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import java.util.Arrays;

/* renamed from: X.15u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245115u {
    public final AnonymousClass101 A00;
    public final C245315w A01;
    public final C245215v A02;
    public final C242914y A03;
    public final C243014z A04;
    public final AbstractC14440lR A05;

    public C245115u(AnonymousClass101 r1, C245315w r2, C245215v r3, C242914y r4, C243014z r5, AbstractC14440lR r6) {
        this.A05 = r6;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
    }

    public void A00(AbstractC14640lm r5) {
        this.A05.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 10, Arrays.asList(r5)));
    }
}
