package X;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1mv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37741mv {
    public AbstractC37731mu A00;
    public AtomicInteger A01 = new AtomicInteger();

    public C37741mv(AbstractC37731mu r2) {
        this.A00 = r2;
    }

    public Object A00(AnonymousClass221 r7) {
        AbstractC37731mu r0;
        C28481Nj ACB = this.A00.ACB();
        if (ACB == null) {
            return null;
        }
        C95514dr r3 = null;
        boolean z = false;
        while (ACB != null) {
            r3 = r7.Aav(ACB);
            if (z) {
                this.A01.incrementAndGet();
            }
            if (!r3.A05) {
                int i = r3.A00;
                z = true;
                if (i != 1 || (r0 = r3.A01) == null) {
                    this.A00.APu(r3.A04, i);
                    if (!r3.A03) {
                    }
                } else {
                    this.A00 = r0;
                    z = false;
                }
                try {
                    Thread.sleep(this.A00.AAo());
                    ACB = this.A00.ACB();
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                }
            }
            return r3.A02;
        }
        if (r3 != null) {
            return r3.A02;
        }
        return null;
    }
}
