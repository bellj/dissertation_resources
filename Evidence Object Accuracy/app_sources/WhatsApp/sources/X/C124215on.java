package X;

import android.text.TextUtils;
import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Locale;
import javax.crypto.Cipher;

/* renamed from: X.5on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124215on extends AbstractC16350or {
    public final AbstractC15710nm A00;
    public final AbstractC136436Mn A01;
    public final AbstractC17860rW A02;
    public final AnonymousClass162 A03;
    public final C18590sh A04;
    public final String A05 = "initial";
    public final WeakReference A06;
    public final C130165yu A07;
    public final C126615tA A08;
    public final boolean A09;

    public C124215on(AbstractC15710nm r2, C120445gC r3, AbstractC136436Mn r4, AbstractC17860rW r5, AnonymousClass162 r6, C18590sh r7, C130165yu r8, C126615tA r9, boolean z) {
        this.A04 = r7;
        this.A08 = r9;
        this.A00 = r2;
        this.A09 = z;
        this.A01 = r4;
        this.A06 = C12970iu.A10(r3);
        this.A07 = r8;
        this.A03 = r6;
        this.A02 = r5;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        String str;
        byte[] bArr;
        C126615tA r3 = this.A08;
        String str2 = this.A05;
        String A01 = this.A04.A01();
        boolean z = this.A09;
        synchronized (r3) {
            r3.A00.A09("onboarding", "getChallenge called", new C31021Zs[]{new C31021Zs("device_id", A01), new C31021Zs("challenge_type", str2)});
            byte[] bArr2 = new byte[32];
            new SecureRandom().nextBytes(bArr2);
            String A00 = AnonymousClass61J.A00(bArr2);
            byte[] bArr3 = new byte[32];
            new SecureRandom().nextBytes(bArr3);
            String A002 = AnonymousClass61J.A00(bArr3);
            if (C126615tA.A01 == null) {
                str = null;
            } else if (str2.equalsIgnoreCase("initial")) {
                C126615tA.A01.A07(A002, A00);
                StringBuilder A0j = C12960it.A0j(A00);
                A0j.append("|");
                A0j.append(A002);
                A0j.append("|");
                String A0d = C12960it.A0d(A01, A0j);
                try {
                    CertificateFactory instance = CertificateFactory.getInstance("X.509");
                    try {
                        try {
                            if (z) {
                                bArr = "-----BEGIN CERTIFICATE-----\nMIIErDCCA5SgAwIBAgIKFzMa/spgXyVfATANBgkqhkiG9w0BAQsFADCB9zELMAkG\nA1UEBhMCSU4xRTBDBgNVBAoTPEluc3RpdHV0ZSBmb3IgRGV2ZWxvcG1lbnQgYW5k\nIFJlc2VhcmNoIGluIEJhbmtpbmcgVGVjaG5vbG9neTEdMBsGA1UECxMUQ2VydGlm\neWluZyBBdXRob3JpdHkxDzANBgNVBBETBjUwMDA1NzEXMBUGA1UECBMOQW5kaHJh\nIFByYWRlc2gxKTAnBgNVBAkTIFJvYWQgTm8uMSwgTWFzYWIgVGFuaywgSHlkZXJh\nYmFkMRUwEwYDVQQzEwxDYXN0bGUgSGlsbHMxFjAUBgNVBAMTDUlEUkJUIENBIDIw\nMTQwHhcNMTYwMzMxMTIwMzUyWhcNMTgwMzMxMTIwMzUyWjASMRAwDgYDVQQDEwd1\ncGlucGNpMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsqMattxoTibl\naGsAneFo5TgFfvRltQ2CqteIgUVJlLMz95P35Y+GQc2EOjVEO0L1OUd5IDTIBaqh\nrTqeWypOyna+CsHPqDRY0vsklCYizw/VQkdwLBZHMMhU4jL4wciE7qcf2kdyri6B\n135SzUF5IGPn4SnSkeB3IqHzneUrabP0lJweLF7IU5l9qtpPa6A5cH1iVKQT5YAI\n4NWH+3E/MRNKvdqgU1PILlOEaVB0vJZ1pBWW1aMaecoHfzC8/pTfM2PJ5s/CDbR0\n4xqX9/AYmkt34jMwBKvboyWjD1UgCkPnJd3SaZ4vDpA+nyTITM6uGOI6e6sv/b6v\ntF7nbM3H7wIDAQABo4IBHDCCARgwDgYDVR0PAQH/BAQDAgWgMB0GA1UdDgQWBBQq\nlITKolqdgylQXHZgJ1WJMLhvnDAfBgNVHSMEGDAWgBSAdQI0B9ReDs4FarWs8BBr\nQhsHxzAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwOAYDVR0gBDEwLzAt\nBgZggmRkAgMwIzAhBggrBgEFBQcCAjAVGhNDbGFzcyAzIENlcnRpZmljYXRlMBIG\nA1UdEQQLMAmCB3VwaW5wY2kwWQYDVR0fBFIwUDAkoCKgIIYeaHR0cDovLzEwLjAu\nNjUuNjUvY3JsXzI3QjAuY3JsMCigJqAkhiJodHRwOi8vaWRyYnRjYS5vcmcuaW4v\nY3JsXzI3QjAuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQB6Y1FmD1GRKdEW4GINYutM\njDxZmHy4HGFmkE3xZjQx9aDT34TpLSNpsg8jUF+nW/KUMb+nWKJuIRZESYfBSCS9\n2fP1AJAO+PKopJMep/weJ+6a0ydPeoKbqKYllTlmFy+DPC56/UuEBQ9iIX3n5RW9\nxZFTB3NFC9A8zhxCBTsxizElrP8Jd4s+I3+qzJIw9ZqGknDgipyYoi0GGx2mjfwH\npg4YQ4/2xjG2ZRQwkgEmcxy/NvbXz1NV5P5u7dit9SwX3gaAZybu2guIcLFcVEvw\n1VpioZlgPjr5RVhyGzX+tBS8rapTNG1C8A7ANLxpk3nqYzNh4XjhMm5EQ3oKDD9d\n-----END CERTIFICATE-----".getBytes(DefaultCrypto.UTF_8);
                            } else {
                                bArr = "-----BEGIN CERTIFICATE-----\nMIID9zCCAt+gAwIBAgIJAIM6xLWVJlOZMA0GCSqGSIb3DQEBCwUAMIGRMQswCQYD\nVQQGEwJJTjESMBAGA1UECAwJVGFtaWxOYWR1MRAwDgYDVQQHDAdDaGVubmFpMQ0w\nCwYDVQQKDAROUENJMQwwCgYDVQQLDANVUEkxGzAZBgNVBAMMEnVwaXVhdC5ucGNp\nLm9yZy5pbjEiMCAGCSqGSIb3DQEJARYTdmliZWV0aEBucGNpLm9yZy5pbjAeFw0x\nNzEyMDcxMDQ2MjRaFw0yNzA5MDkxMDQ2MjRaMIGRMQswCQYDVQQGEwJJTjESMBAG\nA1UECAwJVGFtaWxOYWR1MRAwDgYDVQQHDAdDaGVubmFpMQ0wCwYDVQQKDAROUENJ\nMQwwCgYDVQQLDANVUEkxGzAZBgNVBAMMEnVwaXVhdC5ucGNpLm9yZy5pbjEiMCAG\nCSqGSIb3DQEJARYTdmliZWV0aEBucGNpLm9yZy5pbjCCASIwDQYJKoZIhvcNAQEB\nBQADggEPADCCAQoCggEBALjCsVn8tFnD6ffJrgVmusoaZm+mRmRkih0SDr9OVJSD\neU21D5eshHq4e1o9Picin7Dp7UXQFYZCyRkaB8B+oBBHhTR4Hu5D3mvwd93EAG6m\nt/Zpz+RdWmmU97YyZyq3yNVrptHK3W+3QDM37eCSCzhGvrN1IeznWBJf16exIrHU\nqCd+OQo9C03y2REM0XVRqL12a3ywcicrB9dVfbQ6yFb++Qv91J3cIx7dza6szQaF\nDIeVWURZHMLnm1AJISHneYL6MrCcHdMmBqwFFza1ySQ0wI+XYgrmrFsn1mUfZbF+\nZdeIXcRfpamnUf6YCCOdw1uaBMtizF4L7drv6pkppfMCAwEAAaNQME4wHQYDVR0O\nBBYEFOMI3fS6ZLwKnCvoH1xuEIWAy3eWMB8GA1UdIwQYMBaAFOMI3fS6ZLwKnCvo\nH1xuEIWAy3eWMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAJ4YflNo\nezlsuRbr8asboMw5kH0F6VpmozNxkDGbwLFGx7cVZ5m9xpmPr2eOA4uk598mFJqv\nG+6fx3naM1s7WtaEXB4L9eHCv2nPdYKav6GrB7aabXb2Y5IasIG1aa7oX+V6aw0z\nYufLTIv3Awy7Qp6lgGDvnd0zL3wJqS3kY80CpOFhLwifpPXUA0NMBU6kg1+R0fex\njKAOiC9OkDXmj0Uljeb3AbwE3jZgonoIwcbP8rSc5hN3uxUJyIfNzEzAmHGraP0M\nTPqEocFcvnj8VFJPr94MSZ9CnVUwdLlXTiWYoGkBL6tngh9MSHEaocW1M64Wp85Y\nurWQmF18TeJcJ4k=\n-----END CERTIFICATE-----\n".getBytes(DefaultCrypto.UTF_8);
                            }
                            PublicKey publicKey = instance.generateCertificate(new ByteArrayInputStream(bArr)).getPublicKey();
                            if (A0d.getBytes().length <= publicKey.getEncoded().length) {
                                try {
                                    byte[] bytes = A0d.getBytes();
                                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                    instance2.init(1, publicKey);
                                    str = C117305Zk.A0n(instance2.doFinal(bytes));
                                } catch (Exception e) {
                                    throw C117315Zl.A0J(e);
                                }
                            } else {
                                throw C12970iu.A0f("RSA cannot encrypt data larger than key-size.");
                            }
                        } catch (UnsupportedEncodingException e2) {
                            throw new Error(e2);
                        }
                    } catch (CertificateException e3) {
                        throw C117315Zl.A0J(e3);
                    }
                } catch (CertificateException e4) {
                    throw C117315Zl.A0J(e4);
                }
            } else if (str2.equalsIgnoreCase("rotate")) {
                String A003 = C126615tA.A01.A00();
                StringBuilder A0j2 = C12960it.A0j(A00);
                A0j2.append("|");
                A0j2.append(A002);
                A0j2.append("|");
                str = Base64.encodeToString(AnonymousClass61J.A04(C12960it.A0d(A01, A0j2).getBytes(), AnonymousClass61J.A01(A003)), 0);
                C126615tA.A01.A07(A002, A00);
            } else {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("Unsupported challenge type: ");
                throw C12980iv.A0u(C12960it.A0d(str2, A0h));
            }
        }
        return str;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        String str = (String) obj;
        if (!TextUtils.isEmpty(str)) {
            Log.i("PAY: IndiaUpiSetupCoordinator/challenge got");
            C120445gC r9 = (C120445gC) this.A06.get();
            if (r9 != null) {
                String str2 = this.A05;
                if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
                    Log.i("PAY: IndiaUpiSetupCoordinator/getToken called with invalid type/challenge");
                } else {
                    Log.i("PAY: IndiaUpiSetupCoordinator/getToken called");
                    if (!TextUtils.isEmpty(str)) {
                        r9.A06.AeG();
                        C17220qS r2 = r9.A02;
                        String A01 = r2.A01();
                        C130545za r3 = new C130545za(new AnonymousClass3CT(A01), str, r9.A07.A01(), r9.A03.A07(), str2.toLowerCase(Locale.US));
                        C64513Fv r11 = ((C126705tJ) r9).A00;
                        r11.A04("upi-get-token");
                        C117325Zm.A06(r2, new C120575gP(r9.A00, r9.A01, r9, r9.A05, r11), r3.A00, A01);
                    }
                }
            }
        } else {
            AbstractC136436Mn r0 = this.A01;
            if (r0 != null) {
                r0.AQw();
            }
            this.A00.AaV("payments/indiaupi", "Failed to get Challenge", true);
        }
        C129315xW.A0C = null;
    }
}
