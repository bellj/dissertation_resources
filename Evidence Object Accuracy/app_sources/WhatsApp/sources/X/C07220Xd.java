package X;

import java.util.Arrays;

/* renamed from: X.0Xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07220Xd implements AbstractC12730iP {
    public int A00 = 8;
    public int A01 = 0;
    public int A02 = -1;
    public int A03 = -1;
    public boolean A04 = false;
    public float[] A05 = new float[8];
    public int[] A06 = new int[8];
    public int[] A07 = new int[8];
    public final C07240Xf A08;
    public final AnonymousClass0NX A09;

    public C07220Xd(C07240Xf r4, AnonymousClass0NX r5) {
        this.A08 = r4;
        this.A09 = r5;
    }

    @Override // X.AbstractC12730iP
    public void A5c(AnonymousClass0QC r14, float f, boolean z) {
        int i;
        float f2 = -0.001f;
        if (f <= f2 || f >= 0.001f) {
            int i2 = this.A02;
            if (i2 == -1) {
                this.A02 = 0;
                this.A05[0] = f;
                this.A06[0] = r14.A02;
                this.A07[0] = -1;
                r14.A05++;
                r14.A01(this.A08);
                this.A01++;
                if (!this.A04) {
                    i = this.A03 + 1;
                    this.A03 = i;
                } else {
                    return;
                }
            } else {
                int i3 = 0;
                int i4 = -1;
                while (i2 != -1) {
                    int i5 = this.A01;
                    if (i3 >= i5) {
                        break;
                    }
                    int[] iArr = this.A06;
                    int i6 = iArr[i2];
                    int i7 = r14.A02;
                    if (i6 == i7) {
                        float[] fArr = this.A05;
                        float f3 = fArr[i2] + f;
                        if (f3 > f2 && f3 < 0.001f) {
                            f3 = 0.0f;
                        }
                        fArr[i2] = f3;
                        if (f3 == 0.0f) {
                            int[] iArr2 = this.A07;
                            int i8 = iArr2[i2];
                            if (i2 == i2) {
                                this.A02 = i8;
                            } else {
                                iArr2[i4] = i8;
                            }
                            if (z) {
                                r14.A02(this.A08);
                            }
                            if (this.A04) {
                                this.A03 = i2;
                            }
                            r14.A05--;
                            this.A01 = i5 - 1;
                            return;
                        }
                        return;
                    }
                    if (iArr[i2] < i7) {
                        i4 = i2;
                    }
                    i2 = this.A07[i2];
                    i3++;
                }
                int i9 = this.A03;
                int i10 = i9 + 1;
                if (this.A04) {
                    int[] iArr3 = this.A06;
                    if (iArr3[i9] != -1) {
                        i9 = iArr3.length;
                    }
                } else {
                    i9 = i10;
                }
                int[] iArr4 = this.A06;
                int length = iArr4.length;
                if (i9 >= length && this.A01 < length) {
                    int i11 = 0;
                    while (true) {
                        if (i11 >= length) {
                            break;
                        } else if (iArr4[i11] == -1) {
                            i9 = i11;
                            break;
                        } else {
                            i11++;
                        }
                    }
                }
                if (i9 >= length) {
                    i9 = length;
                    int i12 = this.A00 << 1;
                    this.A00 = i12;
                    this.A04 = false;
                    this.A03 = length - 1;
                    this.A05 = Arrays.copyOf(this.A05, i12);
                    this.A06 = Arrays.copyOf(this.A06, this.A00);
                    this.A07 = Arrays.copyOf(this.A07, this.A00);
                }
                this.A06[i9] = r14.A02;
                this.A05[i9] = f;
                int[] iArr5 = this.A07;
                if (i4 != -1) {
                    iArr5[i9] = iArr5[i4];
                    iArr5[i4] = i9;
                } else {
                    iArr5[i9] = this.A02;
                    this.A02 = i9;
                }
                r14.A05++;
                r14.A01(this.A08);
                this.A01++;
                if (!this.A04) {
                    this.A03++;
                }
                i = this.A03;
            }
            int length2 = this.A06.length;
            if (i >= length2) {
                this.A04 = true;
                this.A03 = length2 - 1;
            }
        }
    }

    @Override // X.AbstractC12730iP
    public boolean A7d(AnonymousClass0QC r7) {
        int i = this.A02;
        if (i != -1) {
            int i2 = 0;
            while (i != -1 && i2 < this.A01) {
                if (this.A06[i] == r7.A02) {
                    return true;
                }
                i = this.A07[i];
                i2++;
            }
        }
        return false;
    }

    @Override // X.AbstractC12730iP
    public void A95(float f) {
        int i = this.A02;
        int i2 = 0;
        while (i != -1 && i2 < this.A01) {
            float[] fArr = this.A05;
            fArr[i] = fArr[i] / f;
            i = this.A07[i];
            i2++;
        }
    }

    @Override // X.AbstractC12730iP
    public final float AAK(AnonymousClass0QC r5) {
        int i = this.A02;
        int i2 = 0;
        while (i != -1 && i2 < this.A01) {
            if (this.A06[i] == r5.A02) {
                return this.A05[i];
            }
            i = this.A07[i];
            i2++;
        }
        return 0.0f;
    }

    @Override // X.AbstractC12730iP
    public int ACD() {
        return this.A01;
    }

    @Override // X.AbstractC12730iP
    public AnonymousClass0QC AHV(int i) {
        int i2 = this.A02;
        int i3 = 0;
        while (i2 != -1 && i3 < this.A01) {
            if (i3 == i) {
                return this.A09.A03[this.A06[i2]];
            }
            i2 = this.A07[i2];
            i3++;
        }
        return null;
    }

    @Override // X.AbstractC12730iP
    public float AHW(int i) {
        int i2 = this.A02;
        int i3 = 0;
        while (i2 != -1 && i3 < this.A01) {
            if (i3 == i) {
                return this.A05[i2];
            }
            i2 = this.A07[i2];
            i3++;
        }
        return 0.0f;
    }

    @Override // X.AbstractC12730iP
    public void AJ2() {
        int i = this.A02;
        int i2 = 0;
        while (i != -1 && i2 < this.A01) {
            float[] fArr = this.A05;
            fArr[i] = fArr[i] * -1.0f;
            i = this.A07[i];
            i2++;
        }
    }

    @Override // X.AbstractC12730iP
    public final void AZg(AnonymousClass0QC r10, float f) {
        int length;
        int i;
        if (f == 0.0f) {
            AaE(r10, true);
            return;
        }
        int i2 = this.A02;
        if (i2 == -1) {
            this.A02 = 0;
            this.A05[0] = f;
            this.A06[0] = r10.A02;
            this.A07[0] = -1;
            r10.A05++;
            r10.A01(this.A08);
            this.A01++;
            if (!this.A04) {
                i = this.A03 + 1;
                this.A03 = i;
                length = this.A06.length;
            } else {
                return;
            }
        } else {
            int i3 = 0;
            int i4 = -1;
            while (i2 != -1 && i3 < this.A01) {
                int[] iArr = this.A06;
                int i5 = iArr[i2];
                int i6 = r10.A02;
                if (i5 == i6) {
                    this.A05[i2] = f;
                    return;
                }
                if (iArr[i2] < i6) {
                    i4 = i2;
                }
                i2 = this.A07[i2];
                i3++;
            }
            int i7 = this.A03;
            int i8 = i7 + 1;
            if (this.A04) {
                int[] iArr2 = this.A06;
                if (iArr2[i7] != -1) {
                    i7 = iArr2.length;
                }
            } else {
                i7 = i8;
            }
            int[] iArr3 = this.A06;
            int length2 = iArr3.length;
            if (i7 >= length2 && this.A01 < length2) {
                int i9 = 0;
                while (true) {
                    if (i9 >= length2) {
                        break;
                    } else if (iArr3[i9] == -1) {
                        i7 = i9;
                        break;
                    } else {
                        i9++;
                    }
                }
            }
            if (i7 >= length2) {
                i7 = length2;
                int i10 = this.A00 << 1;
                this.A00 = i10;
                this.A04 = false;
                this.A03 = length2 - 1;
                this.A05 = Arrays.copyOf(this.A05, i10);
                this.A06 = Arrays.copyOf(this.A06, this.A00);
                this.A07 = Arrays.copyOf(this.A07, this.A00);
            }
            this.A06[i7] = r10.A02;
            this.A05[i7] = f;
            int[] iArr4 = this.A07;
            if (i4 != -1) {
                iArr4[i7] = iArr4[i4];
                iArr4[i4] = i7;
            } else {
                iArr4[i7] = this.A02;
                this.A02 = i7;
            }
            r10.A05++;
            r10.A01(this.A08);
            int i11 = this.A01 + 1;
            this.A01 = i11;
            if (!this.A04) {
                this.A03++;
            }
            length = this.A06.length;
            if (i11 >= length) {
                this.A04 = true;
            }
            i = this.A03;
        }
        if (i >= length) {
            this.A04 = true;
            this.A03 = length - 1;
        }
    }

    @Override // X.AbstractC12730iP
    public final float AaE(AnonymousClass0QC r11, boolean z) {
        int i = this.A02;
        if (i != -1) {
            int i2 = 0;
            int i3 = -1;
            while (i != -1) {
                int i4 = this.A01;
                if (i2 >= i4) {
                    break;
                }
                int[] iArr = this.A06;
                if (iArr[i] == r11.A02) {
                    int[] iArr2 = this.A07;
                    int i5 = iArr2[i];
                    if (i == i) {
                        this.A02 = i5;
                    } else {
                        iArr2[i3] = i5;
                    }
                    if (z) {
                        r11.A02(this.A08);
                    }
                    r11.A05--;
                    this.A01 = i4 - 1;
                    iArr[i] = -1;
                    if (this.A04) {
                        this.A03 = i;
                    }
                    return this.A05[i];
                }
                i = this.A07[i];
                i2++;
                i3 = i;
            }
        }
        return 0.0f;
    }

    @Override // X.AbstractC12730iP
    public float Afd(C07240Xf r7, boolean z) {
        AnonymousClass0QC r0 = r7.A02;
        float AAK = AAK(r0);
        AaE(r0, z);
        AbstractC12730iP r4 = r7.A01;
        int ACD = r4.ACD();
        for (int i = 0; i < ACD; i++) {
            AnonymousClass0QC AHV = r4.AHV(i);
            A5c(AHV, r4.AAK(AHV) * AAK, z);
        }
        return AAK;
    }

    @Override // X.AbstractC12730iP
    public final void clear() {
        int i = this.A02;
        int i2 = 0;
        while (i != -1 && i2 < this.A01) {
            AnonymousClass0QC r1 = this.A09.A03[this.A06[i]];
            if (r1 != null) {
                r1.A02(this.A08);
            }
            i = this.A07[i];
            i2++;
        }
        this.A02 = -1;
        this.A03 = -1;
        this.A04 = false;
        this.A01 = 0;
    }

    public String toString() {
        int i = this.A02;
        String str = "";
        int i2 = 0;
        while (i != -1 && i2 < this.A01) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" -> ");
            String obj = sb.toString();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(this.A05[i]);
            sb2.append(" : ");
            String obj2 = sb2.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj2);
            sb3.append(this.A09.A03[this.A06[i]]);
            str = sb3.toString();
            i = this.A07[i];
            i2++;
        }
        return str;
    }
}
