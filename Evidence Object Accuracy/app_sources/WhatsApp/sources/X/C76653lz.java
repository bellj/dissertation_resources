package X;

import java.nio.ByteBuffer;

/* renamed from: X.3lz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76653lz extends AbstractC106504vo {
    public int[] A00;
    public int[] A01;

    @Override // X.AnonymousClass5Xx
    public void AZk(ByteBuffer byteBuffer) {
        int[] iArr = this.A00;
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        ByteBuffer A00 = A00(((limit - position) / super.A00.A00) * super.A01.A00);
        while (position < limit) {
            for (int i : iArr) {
                A00.putShort(byteBuffer.getShort((i << 1) + position));
            }
            position += super.A00.A00;
        }
        byteBuffer.position(limit);
        A00.flip();
    }
}
