package X;

/* renamed from: X.51q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094251q implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r4, AbstractC94534c0 r5, AnonymousClass4RG r6) {
        if (!(r4 instanceof C82973wU) || !(r5 instanceof C82973wU)) {
            if (C72473ef.A00(r4 instanceof C83003wX ? 1 : 0)) {
                AbstractC94534c0 A00 = C83003wX.A00(r4);
                if (!(A00 instanceof C82933wQ)) {
                    return A00.A07().A00.contains(r5);
                }
            }
            return false;
        }
        C82973wU A06 = r4.A06();
        return A06.A01.contains(r5.A06().A01);
    }
}
