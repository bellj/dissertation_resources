package X;

/* renamed from: X.3n0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77263n0 extends C76743m8 implements Comparable {
    public long A00;

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C77263n0 r8 = (C77263n0) obj;
        boolean A1V = C12960it.A1V(this.flags & 4, 4);
        if (A1V == C12960it.A1V(r8.flags & 4, 4)) {
            long j = ((C76763mA) this).A00 - ((C76763mA) r8).A00;
            if (j == 0) {
                j = this.A00 - r8.A00;
                if (j == 0) {
                    return 0;
                }
            }
            if (j > 0) {
                return 1;
            }
            return -1;
        } else if (A1V) {
            return 1;
        } else {
            return -1;
        }
    }
}
