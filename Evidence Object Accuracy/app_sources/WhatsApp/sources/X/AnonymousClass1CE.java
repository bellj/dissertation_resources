package X;

import android.os.Build;
import android.telephony.TelephonyManager;

/* renamed from: X.1CE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CE {
    public C51832Yz A00;
    public final AnonymousClass01d A01;
    public final C16590pI A02;
    public final C15890o4 A03;
    public final C14820m6 A04;
    public final C20800wL A05;
    public final AbstractC14440lR A06;

    public AnonymousClass1CE(AnonymousClass01d r1, C16590pI r2, C15890o4 r3, C14820m6 r4, C20800wL r5, AbstractC14440lR r6) {
        this.A02 = r2;
        this.A06 = r6;
        this.A01 = r1;
        this.A03 = r3;
        this.A04 = r4;
        this.A05 = r5;
    }

    public static boolean A00(AnonymousClass01d r3, int i) {
        if (Build.VERSION.SDK_INT < 28) {
            return false;
        }
        TelephonyManager A0N = r3.A0N();
        if ((A0N == null || A0N.getSimState() != 1) && i == 1) {
            return true;
        }
        return false;
    }

    public void A01() {
        C51832Yz r1 = this.A00;
        if (r1 != null) {
            this.A02.A00.unregisterReceiver(r1);
            this.A00 = null;
        }
    }
}
