package X;

import java.util.Arrays;

/* renamed from: X.0sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18570sf {
    public static final C18570sf A00 = new C18570sf(null, null, 1);
    public static final int[] A01 = {133};
    public static final int[] A02 = {36, 40, 132};
    public static final int[] A03 = {55};
    public static final int[] A04 = {52, 53, 54};
    public static final int[] A05 = {133};
    public static final int[] A06 = {35, 59, 63, 132};
    public static final int[] A07 = {35, 38, 45, 49, 52};
    public static final int[] A08 = {133};
    public static final int[] A09 = {132};
    public static final int[] A0A = {133};
    public static final int[] A0B = {35, 38, 41, 132};
    public static final int[] A0C = {133};
    public static final int[] A0D = {35, 132};
    public static final int[] A0E = {32, 133};
    public static final int[] A0F = {41, 70, 78, 81, 82, 87, 88, 94, 132};
    public static final int[] A0G = {36, 38, 44};
    public static final int[] A0H = {32, 43, 133};
    public static final int[] A0I = {38, 132};
    public static final int[] A0J = {35, 36, 38};
    public static final int[] A0K = {133};
    public static final int[] A0L = {35, 46, 53, 132};
    public static final int[] A0M = {133};
    public static final int[] A0N = {132};
    public static final int[] A0O = {133};
    public static final int[] A0P = {132};
    public static final int[] A0Q = {32, 133};
    public static final int[] A0R = {132};
    public static final int[] A0S = {133};
    public static final int[] A0T = {35, 132};
    public static final int[] A0U = {133};
    public static final int[] A0V = {132};
    public static final int[] A0W = {133};
    public static final int[] A0X = {132};
    public static final int[] A0Y = {133};
    public static final int[] A0Z = {35, 132};
    public static final int[] A0a = {133};
    public static final int[] A0b = {132};
    public static final int[] A0c = {32, 35, 44, 133};
    public static final int[] A0d = {45, 53, 132, 140};
    public static final int[] A0e = {38, 50};
    public static final int[] A0f = {133};
    public static final int[] A0g = {43, 44, 48, 132};
    public static final int[] A0h = {133};
    public static final int[] A0i = {35, 132};
    public static final int[] A0j = {35};
    public static final int[] A0k = {133};
    public static final int[] A0l = {132};
    public static final int[] A0m = {133};
    public static final int[] A0n = {40, 41, 42, 43, 44, 45, 46, 48, 49, 50, 132};
    public static final int[] A0o = {133};
    public static final int[] A0p = {38, 55, 65, 67, 132, 140};
    public static final int[] A0q = {133};
    public static final int[] A0r = {41, 44, 48, 53, 59, 132};
    public static final int[] A0s = {51, 133};
    public static final int[] A0t = {44, 48, 56, 132};
    public static final int[] A0u = {35};
    public static final int[] A0v = {32, 133};
    public static final int[] A0w = {132};
    public static final int[] A0x = {133};
    public static final int[] A0y = {132};
    public static final int[] A0z = {41};
    public static final int[] A10 = {36};
    public static final int[] A11 = {35};
    public static final int[] A12 = {44};
    public static final int[] A13 = {40};
    public static final int[] A14 = {42};
    public static final int[] A15 = {36, 38, 41, 51, 57};
    public static final int[] A16 = {38, 43};
    public static final int[] A17 = {38, 43};
    public static final int[] A18 = {40};
    public static final int[] A19 = {48};
    public static final int[] A1A = {75};
    public static final int[] A1B = {133};
    public static final int[] A1C = {41, 132};
    public static final int[] A1D = {45, 133};
    public static final int[] A1E = {36, 43, 46, 132};
    public static final int[] A1F = new int[0];
    public static final int[] A1G = {133};
    public static final int[] A1H = {36, 132};
    public static final int[] A1I = {133};
    public static final int[] A1J = {132};
    public static final int[] A1K = {133};
    public static final int[] A1L = {42, 132};
    public static final int[] A1M = {133};
    public static final int[] A1N = {132};
    public static final int[] A1O = {133};
    public static final int[] A1P = {132};
    public static final int[] A1Q = {133};
    public static final int[] A1R = {132};
    public static final int[] A1S = {133};
    public static final int[] A1T = {44, 132};
    public static final int[] A1U = {133};
    public static final int[] A1V = {132};
    public static final int[] A1W = {133};
    public static final int[] A1X = {35, 132};
    public static final int[] A1Y = {32, 133};
    public static final int[] A1Z = {43, 132};
    public static final int[] A1a = {133};
    public static final int[] A1b = {132};
    public static final int[] A1c = {133};
    public static final int[] A1d = {132};
    public static final int[] A1e = {133};
    public static final int[] A1f = {46, 132};
    public static final int[] A1g = {133};
    public static final int[] A1h = {132};
    public static final int[] A1i = {35};

    public C18570sf() {
    }

    public /* synthetic */ C18570sf(C18570sf r2, C51012Sk r3, int i) {
    }

    public int[] A00(AnonymousClass28D r5) {
        int i = r5.A01;
        switch (i) {
            case 13313:
            case 13314:
            case 13318:
            case 13319:
            case 13322:
            case 13330:
            case 13333:
            case 13337:
            case 13340:
            case 13365:
            case 13366:
            case 13368:
            case 13538:
            case 13566:
            case 13642:
            case 13656:
            case 13688:
            case 13704:
            case 13735:
            case 13747:
            case 13759:
            case 13761:
            case 13762:
            case 13764:
            case 13768:
            case 13774:
            case 13798:
            case 13799:
            case 13800:
            case 13808:
            case 13898:
            case 13914:
            case 13981:
            case 14003:
            case 14007:
            case 14008:
            case 14009:
            case 14047:
            case 14048:
            case 14059:
            case 14117:
            case 14143:
            case 14148:
            case 15700:
            case 15729:
            case 15752:
            case 15757:
            case 15772:
            case 15775:
            case 15777:
            case 15833:
            case 15960:
            case 16121:
            case 16131:
            case 16134:
            case 16138:
            case 16180:
            case 16205:
            case 16239:
            case 16269:
            case 16297:
            case 16310:
            case 16342:
            case 16372:
            case 16373:
                return A1F;
            case 13317:
                return A0E;
            case 13320:
                return A0H;
            case 13323:
                return A0K;
            case 13326:
                return A0W;
            case 13327:
                return A0Y;
            case 13329:
                return A0c;
            case 13332:
                return A0j;
            case 13334:
                return A0m;
            case 13335:
                return A0s;
            case 13336:
                return A0o;
            case 13339:
                return A0q;
            case 13343:
                return A0M;
            case 13344:
                return A0O;
            case 13346:
                return A0Q;
            case 13347:
                return A0S;
            case 13411:
                return A1D;
            case 13412:
                return A1B;
            case 13496:
                return A1G;
            case 13497:
                return A1K;
            case 13498:
                return A1M;
            case 13499:
                return A1O;
            case 13501:
                return A1Q;
            case 13502:
                return A1S;
            case 13503:
                return A1U;
            case 13504:
                return A1Y;
            case 13535:
                return A05;
            case 13648:
                return A0a;
            case 13666:
                return A0h;
            case 13784:
                return A14;
            case 13785:
                return A13;
            case 13797:
                return A0v;
            case 13889:
                return A1g;
            case 13901:
                return A0U;
            case 13948:
                return A1e;
            case 14001:
                return A03;
            case 14005:
                return A1a;
            case 14049:
                return A0x;
            case 14093:
                return A01;
            case 15728:
                return A0C;
            case 15763:
                return A1I;
            case 15778:
                return A0A;
            case 15972:
                return A1c;
            case 15981:
                return A08;
            case 16094:
                return A1W;
            case 16160:
                return A0f;
            case 16198:
                return A12;
            case 16229:
                return A0k;
            case 16245:
                return A0z;
            default:
                String format = String.format("Unexpected style key: %s", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                C16700pc.A0B(format);
                C28691Op.A00("WaBloksChildAttributeHelper", format);
                return A1F;
        }
    }

    public int[] A01(AnonymousClass28D r5) {
        int i = r5.A01;
        switch (i) {
            case 13313:
            case 13318:
            case 13319:
            case 13330:
            case 13332:
            case 13333:
            case 13337:
            case 13365:
            case 13366:
            case 13538:
            case 13566:
            case 13656:
            case 13688:
            case 13704:
            case 13735:
            case 13747:
            case 13759:
            case 13762:
            case 13768:
            case 13774:
            case 13785:
            case 13798:
            case 13800:
            case 13808:
            case 13898:
            case 13914:
            case 13981:
            case 14003:
            case 14008:
            case 14009:
            case 14047:
            case 14048:
            case 14059:
            case 14117:
            case 14143:
            case 14148:
            case 15700:
            case 15752:
            case 15757:
            case 15772:
            case 15777:
            case 15833:
            case 15960:
            case 16121:
            case 16131:
            case 16134:
            case 16138:
            case 16180:
            case 16198:
            case 16205:
            case 16239:
            case 16245:
            case 16297:
            case 16310:
            case 16342:
            case 16372:
                return A1F;
            case 13314:
                return A07;
            case 13317:
                return A0F;
            case 13320:
                return A0I;
            case 13322:
                return A0J;
            case 13323:
                return A0L;
            case 13326:
                return A0X;
            case 13327:
                return A0Z;
            case 13329:
                return A0d;
            case 13334:
                return A0n;
            case 13335:
                return A0t;
            case 13336:
                return A0p;
            case 13339:
                return A0r;
            case 13340:
                return A0u;
            case 13343:
                return A0N;
            case 13344:
                return A0P;
            case 13346:
                return A0R;
            case 13347:
                return A0T;
            case 13368:
                return A1A;
            case 13411:
                return A1E;
            case 13412:
                return A1C;
            case 13496:
                return A1H;
            case 13497:
                return A1L;
            case 13498:
                return A1N;
            case 13499:
                return A1P;
            case 13501:
                return A1R;
            case 13502:
                return A1T;
            case 13503:
                return A1V;
            case 13504:
                return A1Z;
            case 13535:
                return A06;
            case 13642:
                return A19;
            case 13648:
                return A0b;
            case 13666:
                return A0i;
            case 13761:
                return A0e;
            case 13764:
                return A1i;
            case 13784:
                return A15;
            case 13797:
                return A0w;
            case 13799:
                return A0G;
            case 13889:
                return A1h;
            case 13901:
                return A0V;
            case 13948:
                return A1f;
            case 14001:
                return A04;
            case 14005:
                return A1b;
            case 14007:
                return A17;
            case 14049:
                return A0y;
            case 14093:
                return A02;
            case 15728:
                return A0D;
            case 15729:
                return A11;
            case 15763:
                return A1J;
            case 15775:
                return A10;
            case 15778:
                return A0B;
            case 15972:
                return A1d;
            case 15981:
                return A09;
            case 16094:
                return A1X;
            case 16160:
                return A0g;
            case 16229:
                return A0l;
            case 16269:
                return A16;
            case 16373:
                return A18;
            default:
                String format = String.format("Unexpected style key: %s", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                C16700pc.A0B(format);
                C28691Op.A00("WaBloksChildAttributeHelper", format);
                return A1F;
        }
    }
}
