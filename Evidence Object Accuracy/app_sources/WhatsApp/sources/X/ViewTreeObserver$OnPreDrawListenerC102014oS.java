package X;

import android.view.ViewTreeObserver;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.4oS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC102014oS implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C36591kA A00;
    public final /* synthetic */ AnonymousClass4UF A01;
    public final /* synthetic */ boolean A02;

    public ViewTreeObserver$OnPreDrawListenerC102014oS(C36591kA r1, AnonymousClass4UF r2, boolean z) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = z;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        SelectionCheckView selectionCheckView = this.A01.A0E;
        C12980iv.A1G(selectionCheckView, this);
        selectionCheckView.A04(this.A02, true);
        return false;
    }
}
