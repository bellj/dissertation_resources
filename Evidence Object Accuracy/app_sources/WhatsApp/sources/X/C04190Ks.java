package X;

import android.view.ScaleGestureDetector;

/* renamed from: X.0Ks  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04190Ks {
    public static void A00(ScaleGestureDetector scaleGestureDetector, boolean z) {
        scaleGestureDetector.setQuickScaleEnabled(z);
    }
}
