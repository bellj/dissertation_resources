package X;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98734jA implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        IBinder iBinder = null;
        C56492ky r5 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                iBinder = C95664e9.A06(parcel, readInt);
            } else if (c == 3) {
                r5 = (C56492ky) C95664e9.A07(parcel, C56492ky.CREATOR, readInt);
            } else if (c != 4) {
                z2 = C95664e9.A0H(parcel, c, 5, readInt, z2);
            } else {
                z = C12960it.A1S(C95664e9.A02(parcel, readInt));
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78463ox(iBinder, r5, i, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78463ox[i];
    }
}
