package X;

import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.Looper;

/* renamed from: X.4hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C97954hu implements DisplayManager.DisplayListener {
    public final DisplayManager A00;
    public final /* synthetic */ C94494bu A01;

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayAdded(int i) {
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayRemoved(int i) {
    }

    public C97954hu(DisplayManager displayManager, C94494bu r2) {
        this.A01 = r2;
        this.A00 = displayManager;
    }

    public void A00() {
        DisplayManager displayManager = this.A00;
        Looper myLooper = Looper.myLooper();
        C95314dV.A01(myLooper);
        displayManager.registerDisplayListener(this, new Handler(myLooper, null));
    }

    public void A01() {
        this.A00.unregisterDisplayListener(this);
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayChanged(int i) {
        if (i == 0) {
            this.A01.A04();
        }
    }
}
