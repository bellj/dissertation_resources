package X;

/* renamed from: X.0SI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SI {
    public C05310Pb A00 = null;
    public AnonymousClass0SV A01 = null;
    public AnonymousClass0SG A02 = null;
    public AnonymousClass0SG A03 = null;
    public String A04 = null;

    public AnonymousClass0SI() {
    }

    public AnonymousClass0SI(AnonymousClass0SI r2) {
        this.A00 = r2.A00;
        this.A01 = r2.A01;
        this.A02 = r2.A02;
        this.A04 = r2.A04;
        this.A03 = r2.A03;
    }

    public void A00(String str) {
        AnonymousClass0UX r1 = new AnonymousClass0UX(AnonymousClass0J8.screen, AnonymousClass0J9.RenderOptions);
        AnonymousClass0I7 r0 = new AnonymousClass0I7(str);
        r0.A0C();
        this.A00 = r1.A06(r0);
    }
}
