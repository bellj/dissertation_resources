package X;

import java.util.Arrays;

/* renamed from: X.5O8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O8 extends AbstractC112995Fp implements AnonymousClass20H {
    public int A00 = 0;
    public byte[] A01;
    public byte[] A02;
    public byte[] A03;
    public final int A04;
    public final AnonymousClass5XE A05;

    public AnonymousClass5O8(AnonymousClass5XE r3) {
        super(r3);
        this.A05 = r3;
        int AAt = r3.AAt();
        this.A04 = AAt;
        this.A01 = new byte[AAt];
        this.A02 = new byte[AAt];
        this.A03 = new byte[AAt];
    }

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A05.AAt();
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = this.A04;
        A01(bArr, bArr2, i, i3, i2);
        return i3;
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A05);
        return C12960it.A0d("/SIC", A0h);
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r6, boolean z) {
        if (r6 instanceof C113075Fx) {
            C113075Fx r62 = (C113075Fx) r6;
            byte[] A02 = AnonymousClass1TT.A02(r62.A01);
            this.A01 = A02;
            int i = this.A04;
            int length = A02.length;
            if (i >= length) {
                int i2 = i >> 1;
                int i3 = 8;
                if (8 > i2) {
                    i3 = i2;
                }
                if (i - length <= i3) {
                    AnonymousClass20L r2 = r62.A00;
                    if (r2 != null) {
                        this.A05.AIf(r2, true);
                    }
                    reset();
                    return;
                }
                StringBuilder A0k = C12960it.A0k("CTR/SIC mode requires IV of at least: ");
                A0k.append(i - i3);
                throw C12970iu.A0f(C12960it.A0d(" bytes.", A0k));
            }
            StringBuilder A0k2 = C12960it.A0k("CTR/SIC mode requires IV no greater than: ");
            A0k2.append(i);
            throw C12970iu.A0f(C12960it.A0d(" bytes.", A0k2));
        }
        throw C12970iu.A0f("CTR/SIC mode requires ParametersWithIV");
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        byte[] bArr = this.A02;
        Arrays.fill(bArr, (byte) 0);
        byte[] bArr2 = this.A01;
        System.arraycopy(bArr2, 0, bArr, 0, bArr2.length);
        this.A05.reset();
        this.A00 = 0;
    }
}
