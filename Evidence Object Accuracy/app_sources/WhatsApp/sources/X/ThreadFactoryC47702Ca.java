package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0200000_I0_9;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2Ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ThreadFactoryC47702Ca implements ThreadFactory {
    public final String A00 = "JobRunner";
    public final AtomicInteger A01 = new AtomicInteger(1);

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        RunnableBRunnable0Shape9S0200000_I0_9 runnableBRunnable0Shape9S0200000_I0_9 = new RunnableBRunnable0Shape9S0200000_I0_9(this, 0, runnable);
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append(" #");
        sb.append(this.A01.getAndIncrement());
        return new Thread(runnableBRunnable0Shape9S0200000_I0_9, sb.toString());
    }
}
