package X;

import java.io.OutputStream;
import java.security.Signature;
import java.security.SignatureException;

/* renamed from: X.49G  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49G extends OutputStream {
    public Signature A00;

    public AnonymousClass49G(Signature signature) {
        this.A00 = signature;
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        try {
            this.A00.update((byte) i);
        } catch (SignatureException e) {
            throw C12990iw.A0i(e.getMessage());
        }
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        try {
            this.A00.update(bArr);
        } catch (SignatureException e) {
            throw C12990iw.A0i(e.getMessage());
        }
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        try {
            this.A00.update(bArr, i, i2);
        } catch (SignatureException e) {
            throw C12990iw.A0i(e.getMessage());
        }
    }
}
