package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3a3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69673a3 implements AnonymousClass5WH {
    public final C16590pI A00;

    public C69673a3(C16590pI r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WH
    public Intent AGj(List list, int i) {
        String str;
        if (i == 1 || i == 3) {
            str = "image/png";
        } else {
            str = "video/mp4";
        }
        ArrayList<? extends Parcelable> A0l = C12960it.A0l();
        for (int i2 = 0; i2 < list.size(); i2++) {
            AnonymousClass4TH r8 = (AnonymousClass4TH) list.get(i2);
            Bundle A0D = C12970iu.A0D();
            A0D.putString("story_media_caption", r8.A04);
            A0D.putParcelable("story_media_uri", r8.A02);
            A0D.putInt("story_media_video_length_sec", r8.A01);
            A0D.putDouble("story_media_aspect_ratio", r8.A00);
            if (r8.A05 != null) {
                A0D.putString("story_media_link_url", r8.A05);
            }
            A0l.add(A0D);
        }
        Intent putParcelableArrayListExtra = C12970iu.A0A().setAction("com.facebook.stories.ADD_TO_STORY").setPackage("com.facebook.katana").putExtra("com.facebook.platform.extra.APPLICATION_ID", "994766073959253").putExtra("editing_disabled", true).setType(str).putParcelableArrayListExtra("media_list", A0l);
        List<ResolveInfo> queryIntentActivities = this.A00.A00.getPackageManager().queryIntentActivities(putParcelableArrayListExtra, 65536);
        if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
            return null;
        }
        return putParcelableArrayListExtra;
    }

    @Override // X.AnonymousClass5WH
    public boolean AKG() {
        Intent A0E = C12990iw.A0E("com.facebook.stories.ADD_TO_STORY");
        A0E.setPackage("com.facebook.katana");
        A0E.setType("image/png");
        Context context = this.A00.A00;
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(A0E, 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
                return false;
            }
            try {
                if (Integer.parseInt(context.getPackageManager().getPackageInfo("com.facebook.katana", 0).versionName.split("\\.")[0]) >= 227) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                Log.w("Cannot get FB version number", e);
                return false;
            }
        } catch (Exception unused) {
            return false;
        }
    }
}
