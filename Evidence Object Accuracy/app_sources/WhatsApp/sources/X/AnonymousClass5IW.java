package X;

import java.security.InvalidParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyGeneratorSpi;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.5IW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IW extends KeyGeneratorSpi {
    public int A00;
    public String A01;
    public AnonymousClass4X1 A02;
    public boolean A03 = true;

    public AnonymousClass5IW(String str, AnonymousClass4X1 r3, int i) {
        this.A01 = str;
        this.A00 = i;
        this.A02 = r3;
    }

    @Override // javax.crypto.KeyGeneratorSpi
    public SecretKey engineGenerateKey() {
        if (this.A03) {
            this.A02.A00(new C90574Ok(this.A00, C95234dM.A00()));
            this.A03 = false;
        }
        return new SecretKeySpec(this.A02.A01(), this.A01);
    }

    @Override // javax.crypto.KeyGeneratorSpi
    public void engineInit(int i, SecureRandom secureRandom) {
        if (secureRandom == null) {
            try {
                secureRandom = C95234dM.A00();
            } catch (IllegalArgumentException e) {
                throw new InvalidParameterException(e.getMessage());
            }
        }
        this.A02.A00(new C90574Ok(i, secureRandom));
        this.A03 = false;
    }

    @Override // javax.crypto.KeyGeneratorSpi
    public void engineInit(SecureRandom secureRandom) {
        if (secureRandom != null) {
            this.A02.A00(new C90574Ok(this.A00, secureRandom));
            this.A03 = false;
        }
    }

    @Override // javax.crypto.KeyGeneratorSpi
    public void engineInit(AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) {
        throw C72463ee.A0J("Not Implemented");
    }
}
