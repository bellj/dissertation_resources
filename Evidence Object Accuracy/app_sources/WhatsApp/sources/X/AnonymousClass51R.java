package X;

import java.io.Serializable;
import java.util.List;

/* renamed from: X.51R  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass51R implements AbstractC115805Sz, Serializable {
    public final int expectedValuesPerKey = 2;

    public AnonymousClass51R(int i) {
        C28251Mi.checkNonnegative(2, "expectedValuesPerKey");
    }

    @Override // X.AbstractC115805Sz
    public List get() {
        return C12980iv.A0w(this.expectedValuesPerKey);
    }
}
