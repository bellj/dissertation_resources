package X;

/* renamed from: X.0uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19950uw {
    public final C18790t3 A00;
    public final C14850m9 A01;
    public final C19940uv A02;
    public final C18800t4 A03;
    public final C19930uu A04;

    public C19950uw(C18790t3 r1, C14850m9 r2, C19940uv r3, C18800t4 r4, C19930uu r5) {
        this.A01 = r2;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public C28471Ni A00(AbstractC28461Nh r11, String str, int i) {
        boolean A07;
        boolean A072 = this.A01.A07(1638);
        C18800t4 r3 = this.A03;
        String A00 = this.A04.A00();
        C19940uv r1 = this.A02;
        boolean A01 = r1.A01();
        if (!r1.A01()) {
            A07 = false;
        } else {
            A07 = r1.A03.A07(58);
        }
        return new C28471Ni(this.A00, r11, r3, str, A00, i, A01, A07, A072);
    }
}
