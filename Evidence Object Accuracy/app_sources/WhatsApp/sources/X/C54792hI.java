package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2hI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54792hI extends AbstractC05270Ox {
    public final C14260l7 A00;
    public final AnonymousClass28D A01;
    public final AbstractC14200l1 A02;

    public C54792hI(C14260l7 r1, AnonymousClass28D r2, AbstractC14200l1 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        AnonymousClass28D r3 = this.A01;
        AbstractC14200l1 r2 = this.A02;
        C28701Oq.A01(this.A00, r3, C14210l2.A02(r3), r2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C54792hI)) {
            return false;
        }
        C54792hI r4 = (C54792hI) obj;
        if (r4.A02 == this.A02 && r4.A01 == this.A01) {
            return true;
        }
        return false;
    }
}
