package X;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;

/* renamed from: X.3tC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC80933tC extends AnonymousClass51S implements Serializable {
    public static final long serialVersionUID = 2447537837011683357L;
    public transient Map map;
    public transient int totalSize;

    public abstract Collection createCollection();

    public abstract Collection unmodifiableCollectionSubclass(Collection collection);

    public abstract Collection wrapCollection(Object obj, Collection collection);

    public AbstractC80933tC(Map map) {
        if (map.isEmpty()) {
            this.map = map;
            return;
        }
        throw C72453ed.A0h();
    }

    public static /* synthetic */ int access$208(AbstractC80933tC r2) {
        int i = r2.totalSize;
        r2.totalSize = i + 1;
        return i;
    }

    public static /* synthetic */ int access$210(AbstractC80933tC r2) {
        int i = r2.totalSize;
        r2.totalSize = i - 1;
        return i;
    }

    public static /* synthetic */ int access$212(AbstractC80933tC r1, int i) {
        int i2 = r1.totalSize + i;
        r1.totalSize = i2;
        return i2;
    }

    public static /* synthetic */ int access$220(AbstractC80933tC r1, int i) {
        int i2 = r1.totalSize - i;
        r1.totalSize = i2;
        return i2;
    }

    public Map backingMap() {
        return this.map;
    }

    @Override // X.AnonymousClass5XH
    public void clear() {
        Iterator A0o = C12960it.A0o(this.map);
        while (A0o.hasNext()) {
            ((Collection) A0o.next()).clear();
        }
        this.map.clear();
        this.totalSize = 0;
    }

    public Collection createCollection(Object obj) {
        return createCollection();
    }

    public final Map createMaybeNavigableAsMap() {
        Map map = this.map;
        if (map instanceof NavigableMap) {
            return new C80793sy(this, (NavigableMap) map);
        }
        if (map instanceof SortedMap) {
            return new C80803sz(this, (SortedMap) map);
        }
        return new C81143tX(this, map);
    }

    public final Set createMaybeNavigableKeySet() {
        Map map = this.map;
        if (map instanceof NavigableMap) {
            return new C80813t0(this, (NavigableMap) map);
        }
        if (map instanceof SortedMap) {
            return new C80823t1(this, (SortedMap) map);
        }
        return new C81133tW(this, map);
    }

    @Override // X.AnonymousClass51S
    public Collection createValues() {
        return new C113505Hv(this);
    }

    public static Iterator iteratorOrListIterator(Collection collection) {
        if (collection instanceof List) {
            return ((List) collection).listIterator();
        }
        return collection.iterator();
    }

    @Override // X.AnonymousClass51S, X.AnonymousClass5XH
    public boolean put(Object obj, Object obj2) {
        Collection collection = (Collection) this.map.get(obj);
        if (collection == null) {
            Collection createCollection = createCollection(obj);
            if (createCollection.add(obj2)) {
                this.totalSize++;
                this.map.put(obj, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (!collection.add(obj2)) {
            return false;
        } else {
            this.totalSize++;
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void removeValuesForKey(Object obj) {
        Collection collection = (Collection) C28271Mk.safeRemove(this.map, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.totalSize -= size;
        }
    }

    public final void setMap(Map map) {
        this.map = map;
        this.totalSize = 0;
        Iterator A0o = C12960it.A0o(map);
        while (A0o.hasNext()) {
            Collection collection = (Collection) A0o.next();
            if (!collection.isEmpty()) {
                this.totalSize += collection.size();
            } else {
                throw C72453ed.A0h();
            }
        }
    }

    @Override // X.AnonymousClass5XH
    public int size() {
        return this.totalSize;
    }

    @Override // X.AnonymousClass51S
    public Iterator valueIterator() {
        return new AnonymousClass5DJ(this);
    }

    public final List wrapList(Object obj, List list, C113515Hw r4) {
        if (list instanceof RandomAccess) {
            return new C80843t3(this, obj, list, r4);
        }
        return new C80853t4(this, obj, list, r4);
    }
}
