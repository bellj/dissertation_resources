package X;

import android.os.SystemClock;

/* renamed from: X.01e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C002601e implements AnonymousClass01N, AnonymousClass01H {
    public long A00 = -1;
    public AnonymousClass01N A01;
    public volatile Object A02;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r3 != null) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C002601e(java.lang.Object r3, X.AnonymousClass01N r4) {
        /*
            r2 = this;
            r2.<init>()
            r0 = -1
            r2.A00 = r0
            if (r4 != 0) goto L_0x000c
            r1 = 0
            if (r3 == 0) goto L_0x000d
        L_0x000c:
            r1 = 1
        L_0x000d:
            java.lang.String r0 = "Either a provider or instance must be specified."
            X.AnonymousClass009.A0B(r0, r1)
            r2.A01 = r4
            r2.A02 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C002601e.<init>(java.lang.Object, X.01N):void");
    }

    public boolean A00() {
        boolean z;
        synchronized (this) {
            z = false;
            if (this.A02 != null) {
                z = true;
            }
        }
        return z;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        if (this.A02 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    AnonymousClass01N r1 = this.A01;
                    boolean z = false;
                    if (r1 != null) {
                        z = true;
                    }
                    AnonymousClass009.A0F(z);
                    long uptimeMillis = SystemClock.uptimeMillis();
                    Object obj = r1.get();
                    AnonymousClass009.A05(obj);
                    this.A02 = obj;
                    this.A00 = Math.max(0L, SystemClock.uptimeMillis() - uptimeMillis);
                    this.A01 = null;
                }
            }
        }
        return this.A02;
    }
}
