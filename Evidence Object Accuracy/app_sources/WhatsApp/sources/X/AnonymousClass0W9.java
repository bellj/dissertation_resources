package X;

import android.view.View;

/* renamed from: X.0W9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W9 implements View.OnAttachStateChangeListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C06460Ts A01;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
    }

    public AnonymousClass0W9(View view, C06460Ts r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        View view2 = this.A00;
        view2.removeOnAttachStateChangeListener(this);
        AnonymousClass028.A0R(view2);
    }
}
