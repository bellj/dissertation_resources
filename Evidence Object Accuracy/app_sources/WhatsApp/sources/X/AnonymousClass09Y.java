package X;

import android.app.SharedElementCallback;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Parcelable;
import android.view.View;
import java.util.List;
import java.util.Map;

/* renamed from: X.09Y  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09Y extends SharedElementCallback {
    public final AbstractC000600h A00;

    public AnonymousClass09Y(AbstractC000600h r1) {
        this.A00 = r1;
    }

    @Override // android.app.SharedElementCallback
    public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
        return this.A00.A01(view, matrix, rectF);
    }

    @Override // android.app.SharedElementCallback
    public View onCreateSnapshotView(Context context, Parcelable parcelable) {
        return AbstractC000600h.A00(context, parcelable);
    }

    @Override // android.app.SharedElementCallback
    public void onMapSharedElements(List list, Map map) {
        this.A00.A03(list, map);
    }

    @Override // android.app.SharedElementCallback
    public void onRejectSharedElements(List list) {
    }

    @Override // android.app.SharedElementCallback
    public void onSharedElementEnd(List list, List list2, List list3) {
        this.A00.A02(list, list2, list3);
    }

    @Override // android.app.SharedElementCallback
    public void onSharedElementStart(List list, List list2, List list3) {
    }

    @Override // android.app.SharedElementCallback
    public void onSharedElementsArrived(List list, List list2, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
        new Object(onSharedElementsReadyListener) { // from class: X.0OF
            public final /* synthetic */ SharedElementCallback.OnSharedElementsReadyListener A00;

            {
                this.A00 = r1;
            }

            public final void A00() {
                C000200d.A01(this.A00);
            }
        }.A00();
    }
}
