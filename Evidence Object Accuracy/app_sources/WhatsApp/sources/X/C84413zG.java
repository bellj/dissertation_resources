package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3zG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84413zG extends AnonymousClass2Cu {
    public final /* synthetic */ C628438u A00;
    public final /* synthetic */ UserJid A01;

    public C84413zG(C628438u r1, UserJid userJid) {
        this.A00 = r1;
        this.A01 = userJid;
    }

    @Override // X.AnonymousClass2Cu
    public void A00(UserJid userJid) {
        if (this.A01.equals(userJid)) {
            this.A00.A06.countDown();
        }
    }
}
