package X;

import android.content.Context;
import com.whatsapp.polls.PollResultsActivity;

/* renamed from: X.4qR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103244qR implements AbstractC009204q {
    public final /* synthetic */ PollResultsActivity A00;

    public C103244qR(PollResultsActivity pollResultsActivity) {
        this.A00 = pollResultsActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
