package X;

import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.3Cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63793Cy {
    public final AbstractC15710nm A00;
    public final C15580nU A01;
    public final C17220qS A02;
    public final AbstractC116765Ws A03;

    public C63793Cy(AbstractC15710nm r1, C15580nU r2, C17220qS r3, AbstractC116765Ws r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A00(List list) {
        C17220qS r11 = this.A02;
        String A01 = r11.A01();
        int size = list.size();
        AnonymousClass1V8[] r7 = new AnonymousClass1V8[size];
        for (int i = 0; i < size; i++) {
            r7[i] = new AnonymousClass1V8("group", new AnonymousClass1W9[]{new AnonymousClass1W9((Jid) list.get(i), "jid")});
        }
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[1];
        C12960it.A1M("unlink_type", "sub_group", r2, 0);
        AnonymousClass1V8 r4 = new AnonymousClass1V8("unlink", r2, r7);
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[4];
        C12960it.A1M("id", A01, r3, 0);
        C12960it.A1M("xmlns", "w:g2", r3, 1);
        C12960it.A1M("type", "set", r3, 2);
        r11.A09(new AnonymousClass3Z9(this.A00, this.A03), AnonymousClass1V8.A00(this.A01, r4, r3, 3), A01, 308, 32000);
    }
}
