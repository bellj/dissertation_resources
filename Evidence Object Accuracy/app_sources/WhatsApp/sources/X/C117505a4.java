package X;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/* renamed from: X.5a4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117505a4 extends ClickableSpan {
    public final /* synthetic */ C130205yy A00;

    public C117505a4(C130205yy r1) {
        this.A00 = r1;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        C123565nM r1 = this.A00.A08;
        Context context = view.getContext();
        r1.A06.A06(AnonymousClass12P.A00(context), C117295Zj.A04("https://novi.com"));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
    }
}
