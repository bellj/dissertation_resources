package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.10B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10B {
    public static final long[] A09 = {300000, 3600000, 7200000};
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C41891uK A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final AnonymousClass1GD A05 = new C41061sp(this);
    public final C22100yW A06;
    public final C14850m9 A07;
    public final C17220qS A08;

    public AnonymousClass10B(AbstractC15710nm r8, C15570nT r9, C14830m7 r10, C14820m6 r11, C15990oG r12, C18240s8 r13, C22100yW r14, C14850m9 r15, C17220qS r16, AbstractC14440lR r17) {
        this.A03 = r10;
        this.A07 = r15;
        this.A00 = r8;
        this.A01 = r9;
        this.A08 = r16;
        this.A04 = r11;
        this.A06 = r14;
        this.A02 = new C41891uK(r10, r11, r12, r13, r14, r17);
    }

    public void A00() {
        long A01 = this.A02.A01();
        if (A01 != -1) {
            A02(A01, this.A03.A00(), false);
            return;
        }
        Log.e("DeviceKeyIndexListUpdateHandler/updateKeyIndexList/fail to generate ts");
        A01(-1);
    }

    public void A01(int i) {
        long A00 = this.A03.A00();
        C41891uK r4 = this.A02;
        SharedPreferences sharedPreferences = r4.A01.A00;
        sharedPreferences.edit().putLong("adv_key_index_list_last_failure_time", A00).apply();
        sharedPreferences.edit().remove("adv_key_index_list_require_update").apply();
        int i2 = sharedPreferences.getInt("adv_key_index_list_update_retry_count", 0) + 1;
        sharedPreferences.edit().putInt("adv_key_index_list_update_retry_count", i2).apply();
        StringBuilder sb = new StringBuilder("DeviceKeyIndexListUpdateHandler/onError code=");
        sb.append(i);
        sb.append("; retryCount=");
        sb.append(i2);
        Log.e(sb.toString());
        if (i2 > 5) {
            Log.e("DeviceKeyIndexListUpdateHandler/onError logout all devices");
            this.A00.AaV("adv-key-index-list-update", "key index list update fails for more than 5 times", true);
            r4.A04();
        }
    }

    public final void A02(long j, long j2, boolean z) {
        C41891uK r4 = this.A02;
        SharedPreferences sharedPreferences = r4.A01.A00;
        try {
            C41921uN A03 = r4.A03(r4.A02(C41891uK.A00(sharedPreferences.getInt("adv_raw_id", -1), sharedPreferences.getInt("adv_current_key_index", -1), j)));
            if (A03 != null) {
                C41931uO r9 = new C41931uO(this, this.A08, j, j2, z);
                C17220qS r10 = r9.A03;
                String A01 = r10.A01();
                r10.A09(r9, new AnonymousClass1V8(new AnonymousClass1V8("key-index-list", A03.A02(), new AnonymousClass1W9[]{new AnonymousClass1W9("ts", r9.A00)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "md"), new AnonymousClass1W9("type", "set")}), A01, 268, 32000);
                return;
            }
        } catch (Exception e) {
            Log.e("CompanionDeviceAdvUtil/createADVSignedKeyIndexList ", e);
        }
        Log.e("DeviceKeyIndexListUpdateHandler/updateKeyIndexList fail to generate index list");
        A01(-1);
    }

    public final boolean A03() {
        if (this.A07.A02(477) == 0 || !(!this.A06.A07().isEmpty())) {
            return false;
        }
        this.A01.A08();
        return true;
    }
}
