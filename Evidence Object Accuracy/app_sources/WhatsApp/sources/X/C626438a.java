package X;

import android.util.SparseArray;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape16S0100000_I0_3;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.38a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626438a extends AbstractC16350or {
    public final C14650lo A00;
    public final C15550nR A01;
    public final C16590pI A02;
    public final AnonymousClass018 A03;
    public final C16370ot A04;
    public final C26501Ds A05;
    public final AnonymousClass4SA A06;
    public final WeakReference A07;

    public C626438a(C14650lo r2, C15550nR r3, C16590pI r4, AnonymousClass018 r5, C16370ot r6, C26501Ds r7, AnonymousClass4SA r8, ViewSharedContactArrayActivity viewSharedContactArrayActivity) {
        super(viewSharedContactArrayActivity);
        this.A02 = r4;
        this.A05 = r7;
        this.A01 = r3;
        this.A03 = r5;
        this.A04 = r6;
        this.A00 = r2;
        this.A07 = C12970iu.A10(viewSharedContactArrayActivity);
        this.A06 = r8;
    }

    public static Object A00(AbstractCollection abstractCollection, Iterator it, C30721Yo r5, int i, int i2) {
        Object next = it.next();
        abstractCollection.add(new AnonymousClass4S9(next, r5.A08.A08, i, i2));
        return next;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        int i;
        C15370n3 A0A;
        List<C30731Yp> list = (List) obj;
        ViewSharedContactArrayActivity viewSharedContactArrayActivity = (ViewSharedContactArrayActivity) this.A07.get();
        if (viewSharedContactArrayActivity != null) {
            viewSharedContactArrayActivity.AaN();
            if (list == null || list.isEmpty()) {
                Log.w("viewsharedcontactarrayactivity/oncreate/no vcards to display");
                ((ActivityC13810kN) viewSharedContactArrayActivity).A05.A07(R.string.error_parse_vcard, 0);
                viewSharedContactArrayActivity.finish();
                return;
            }
            HashSet A12 = C12970iu.A12();
            for (C30731Yp r0 : list) {
                C30721Yo r7 = r0.A01;
                String A08 = r7.A08();
                if (!A12.contains(A08)) {
                    viewSharedContactArrayActivity.A0N.add(r7);
                    viewSharedContactArrayActivity.A0O.add(new SparseArray());
                    A12.add(A08);
                } else if (r7.A05 != null) {
                    ArrayList arrayList = viewSharedContactArrayActivity.A0N;
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        C30721Yo r2 = (C30721Yo) it.next();
                        if (r2.A08().equals(A08) && r2.A05 != null && r7.A05.size() > r2.A05.size()) {
                            arrayList.set(arrayList.indexOf(r2), r7);
                        }
                    }
                }
            }
            if (viewSharedContactArrayActivity.A0I == null) {
                Collections.sort(viewSharedContactArrayActivity.A0N, new C71263ce(viewSharedContactArrayActivity.A0A));
            }
            ImageView imageView = (ImageView) viewSharedContactArrayActivity.findViewById(R.id.send_btn);
            if (viewSharedContactArrayActivity.A0M) {
                imageView.setVisibility(0);
                AnonymousClass2GF.A01(viewSharedContactArrayActivity, imageView, viewSharedContactArrayActivity.A0A, R.drawable.input_send);
                C12970iu.A0N(viewSharedContactArrayActivity).A0I(viewSharedContactArrayActivity.A0A.A0D((long) viewSharedContactArrayActivity.A0N.size(), R.plurals.send_contacts));
            } else {
                imageView.setVisibility(8);
                int size = list.size();
                AbstractC005102i A0N = C12970iu.A0N(viewSharedContactArrayActivity);
                Object[] A1b = C12970iu.A1b();
                C12960it.A1P(A1b, size, 0);
                A0N.A0I(viewSharedContactArrayActivity.A0A.A0I(A1b, R.plurals.view_contacts_title, (long) size));
            }
            RecyclerView recyclerView = (RecyclerView) viewSharedContactArrayActivity.findViewById(R.id.rvContacts);
            ArrayList arrayList2 = viewSharedContactArrayActivity.A0N;
            List list2 = viewSharedContactArrayActivity.A0I;
            ArrayList A0l = C12960it.A0l();
            for (int i2 = 0; i2 < arrayList2.size(); i2++) {
                C30721Yo r12 = (C30721Yo) arrayList2.get(i2);
                SparseArray sparseArray = (SparseArray) viewSharedContactArrayActivity.A0O.get(i2);
                A0l.add(new AnonymousClass4LN(r12));
                ArrayList A0l2 = C12960it.A0l();
                List<C30741Yq> list3 = r12.A05;
                if (list3 != null) {
                    i = 0;
                    for (C30741Yq r22 : list3) {
                        if (r22.A01 == null) {
                            A0l2.add(r22);
                        } else {
                            A0l.add(new AnonymousClass4S9(r22, r12.A08.A08, i2, i));
                            ViewSharedContactArrayActivity.A02(sparseArray, i).A00 = r22;
                            i++;
                        }
                    }
                } else {
                    i = 0;
                }
                List list4 = r12.A02;
                if (list4 != null) {
                    Iterator it2 = list4.iterator();
                    while (it2.hasNext()) {
                        ViewSharedContactArrayActivity.A02(sparseArray, i).A00 = A00(A0l, it2, r12, i2, i);
                        i++;
                    }
                }
                Iterator it3 = A0l2.iterator();
                while (it3.hasNext()) {
                    ViewSharedContactArrayActivity.A02(sparseArray, i).A00 = A00(A0l, it3, r12, i2, i);
                    i++;
                }
                List list5 = r12.A06;
                if (list5 != null) {
                    Iterator it4 = list5.iterator();
                    while (it4.hasNext()) {
                        ViewSharedContactArrayActivity.A02(sparseArray, i).A00 = A00(A0l, it4, r12, i2, i);
                        i++;
                    }
                }
                if (r12.A07 != null) {
                    ArrayList A0x = C12980iv.A0x(r12.A07.keySet());
                    Collections.sort(A0x);
                    ArrayList A0l3 = C12960it.A0l();
                    Iterator it5 = A0x.iterator();
                    while (it5.hasNext()) {
                        List<AnonymousClass3FO> list6 = (List) r12.A07.get(it5.next());
                        if (list6 != null) {
                            for (AnonymousClass3FO r1 : list6) {
                                if (r1.A01.equals("URL")) {
                                    r1.toString();
                                    Pattern pattern = viewSharedContactArrayActivity.A0J;
                                    if (pattern == null) {
                                        pattern = Pattern.compile("(http|https)://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&amp;=]*)?");
                                        viewSharedContactArrayActivity.A0J = pattern;
                                    }
                                    if (pattern.matcher(r1.A02).matches()) {
                                        A0l3.add(r1);
                                    }
                                }
                            }
                        }
                    }
                    Iterator it6 = A0x.iterator();
                    while (it6.hasNext()) {
                        List<AnonymousClass3FO> list7 = (List) r12.A07.get(it6.next());
                        if (list7 != null) {
                            for (AnonymousClass3FO r13 : list7) {
                                if (!r13.A01.equals("URL")) {
                                    r13.toString();
                                    A0l3.add(r13);
                                }
                            }
                        }
                    }
                    Iterator it7 = A0l3.iterator();
                    while (it7.hasNext()) {
                        ViewSharedContactArrayActivity.A02(sparseArray, i).A00 = A00(A0l, it7, r12, i2, i);
                        i++;
                    }
                }
                if (list2 != null) {
                    C65953Ls r14 = (C65953Ls) list2.get(i2);
                    UserJid nullable = UserJid.getNullable(r14.A02);
                    if (!(nullable == null || (A0A = viewSharedContactArrayActivity.A03.A0A(nullable)) == null)) {
                        A0l.add(new AnonymousClass2V5(A0A, nullable, viewSharedContactArrayActivity, r14.A00));
                    }
                }
                A0l.add(new AnonymousClass4LM());
            }
            ((AnonymousClass4LM) A0l.get(A0l.size() - 1)).A00 = true;
            recyclerView.setAdapter(new AnonymousClass2V6(viewSharedContactArrayActivity, A0l));
            C12990iw.A1K(recyclerView);
            imageView.setOnClickListener(new ViewOnClickCListenerShape16S0100000_I0_3(viewSharedContactArrayActivity, 4));
        }
    }
}
