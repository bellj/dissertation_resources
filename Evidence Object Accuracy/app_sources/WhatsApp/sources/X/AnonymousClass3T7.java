package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.3T7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3T7 implements AbstractC115685Sn {
    public final View A00;
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A01;

    public AnonymousClass3T7(GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        this.A01 = groupChatLiveLocationsActivity2;
        View A0F = C12960it.A0F(groupChatLiveLocationsActivity2.getLayoutInflater(), null, R.layout.live_location_map_info_window);
        this.A00 = A0F;
        AnonymousClass028.A0c(A0F, 3);
    }

    @Override // X.AbstractC115685Sn
    public View ADT(C36311jg r13) {
        int A00;
        AnonymousClass1YO A01;
        Object A012 = r13.A01();
        AnonymousClass009.A05(A012);
        C30751Yr r10 = ((C35991j8) A012).A02;
        View view = this.A00;
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = this.A01;
        C28801Pb r7 = new C28801Pb(view, groupChatLiveLocationsActivity2.A0D, groupChatLiveLocationsActivity2.A0R, (int) R.id.name_in_group_tv);
        TextView A0J = C12960it.A0J(view, R.id.participant_info);
        View findViewById = view.findViewById(R.id.info_btn);
        C15570nT r0 = ((ActivityC13790kL) groupChatLiveLocationsActivity2).A01;
        UserJid userJid = r10.A06;
        if (r0.A0F(userJid)) {
            C28801Pb.A00(groupChatLiveLocationsActivity2, r7, R.color.live_location_bubble_me_text);
            r7.A02();
            findViewById.setVisibility(8);
        } else {
            C15580nU A02 = C15580nU.A02(groupChatLiveLocationsActivity2.A0M.A0c);
            if (A02 == null || (A01 = groupChatLiveLocationsActivity2.A0I.A01(A02, userJid)) == null) {
                A00 = AnonymousClass00T.A00(groupChatLiveLocationsActivity2, R.color.live_location_bubble_unknown_text);
            } else {
                int[] intArray = groupChatLiveLocationsActivity2.getResources().getIntArray(R.array.group_participant_name_colors);
                A00 = intArray[A01.A00 % intArray.length];
            }
            r7.A04(A00);
            r7.A06(groupChatLiveLocationsActivity2.A0B.A0B(userJid));
            findViewById.setVisibility(0);
        }
        C27531Hw.A06(r7.A01);
        String str = "";
        int i = r10.A03;
        if (i != -1) {
            StringBuilder A0j = C12960it.A0j(str);
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, i, 0);
            str = C12960it.A0d(((ActivityC13830kP) groupChatLiveLocationsActivity2).A01.A0I(A1b, R.plurals.location_accuracy, (long) i), A0j);
        }
        C12980iv.A1J(A0J, str);
        return view;
    }
}
