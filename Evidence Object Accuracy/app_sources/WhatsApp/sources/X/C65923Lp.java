package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.util.List;

/* renamed from: X.3Lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65923Lp implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(63);
    public final List A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C65923Lp) && C16700pc.A0O(this.A00, ((C65923Lp) obj).A00));
    }

    @Override // java.lang.Object
    public int hashCode() {
        List list = this.A00;
        if (list == null) {
            return 0;
        }
        return list.hashCode();
    }

    public C65923Lp(List list) {
        this.A00 = list;
    }

    @Override // java.lang.Object
    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("AvatarGetProfilePhotoPosesEntity(urls="));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeStringList(this.A00);
    }
}
