package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ZK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZK implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100214lY();
    public String A00;
    public String A01;
    public final AnonymousClass2Bg A02;
    public final AnonymousClass1ZI A03;
    public final AnonymousClass1ZI A04;
    public final AnonymousClass1ZI A05;
    public final AnonymousClass1ZI A06;
    public final String A07;
    public final List A08;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZK(AnonymousClass2Bg r1, AnonymousClass1ZI r2, AnonymousClass1ZI r3, AnonymousClass1ZI r4, AnonymousClass1ZI r5, String str, String str2, String str3, List list) {
        this.A01 = str;
        this.A08 = list;
        this.A05 = r2;
        this.A06 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = str2;
        this.A02 = r1;
        this.A07 = str3;
    }

    public AnonymousClass1ZK(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A01 = readString;
        ArrayList arrayList = new ArrayList();
        this.A08 = arrayList;
        parcel.readList(arrayList, C66023Lz.class.getClassLoader());
        Parcelable readParcelable = parcel.readParcelable(AnonymousClass1ZI.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A05 = (AnonymousClass1ZI) readParcelable;
        this.A06 = (AnonymousClass1ZI) parcel.readParcelable(AnonymousClass1ZI.class.getClassLoader());
        this.A03 = (AnonymousClass1ZI) parcel.readParcelable(AnonymousClass1ZI.class.getClassLoader());
        this.A04 = (AnonymousClass1ZI) parcel.readParcelable(AnonymousClass1ZI.class.getClassLoader());
        this.A00 = parcel.readString();
        this.A07 = parcel.readString();
        this.A02 = (AnonymousClass2Bg) parcel.readParcelable(AnonymousClass2Bg.class.getClassLoader());
    }

    public String A00() {
        List list = this.A08;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String[] strArr = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            strArr[i] = ((C66023Lz) list.get(i)).A03;
        }
        return AnonymousClass1US.A09(", ", strArr);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeList(this.A08);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A06, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A04, i);
        parcel.writeString(this.A00);
        parcel.writeString(this.A07);
        parcel.writeParcelable(this.A02, i);
    }
}
