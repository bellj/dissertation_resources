package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.315  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass315 extends AbstractC16110oT {
    public Double A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;

    public AnonymousClass315() {
        super(1012, new AnonymousClass00E(1, SearchActionVerificationClientService.NOTIFICATION_ID, 1000000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A04);
        r3.Abe(1, this.A05);
        r3.Abe(6, this.A06);
        r3.Abe(9, this.A01);
        r3.Abe(8, this.A02);
        r3.Abe(3, this.A07);
        r3.Abe(5, this.A03);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamVideoPlay {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoAge", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoDuration", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoInitialBufferingT", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoPlayOrigin", C12960it.A0Y(this.A01));
        String str = null;
        Integer num = this.A02;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoPlaySurface", str);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoPlayT", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoPlayType", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "videoSize", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
