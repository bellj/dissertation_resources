package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1kN */
/* loaded from: classes2.dex */
public class C36691kN extends AnonymousClass015 {
    public final AnonymousClass016 A00;
    public final C15570nT A01;
    public final AnonymousClass1GV A02;
    public final C237913a A03;
    public final AnonymousClass1BG A04;
    public final C15550nR A05;
    public final C27131Gd A06;
    public final AnonymousClass10S A07;
    public final AnonymousClass5UJ A08;
    public final AnonymousClass11A A09;
    public final AbstractC33331dp A0A;
    public final C244215l A0B;
    public final C15580nU A0C;
    public final C36161jQ A0D;
    public final ExecutorC27271Gr A0E;
    public final HashMap A0F = new HashMap();
    public final Set A0G;

    public C36691kN(C15570nT r3, C237913a r4, AnonymousClass1BG r5, C15550nR r6, AnonymousClass10S r7, AnonymousClass11A r8, C244215l r9, C15580nU r10, AbstractC14440lR r11) {
        HashSet hashSet = new HashSet();
        this.A0G = hashSet;
        this.A0D = new C36161jQ(AnonymousClass1JO.A00(hashSet));
        this.A00 = new AnonymousClass016();
        this.A06 = new C36511k0(this);
        this.A0A = new AnonymousClass44X(this);
        this.A08 = new AnonymousClass579(this);
        this.A02 = new C1097752z(this);
        this.A03 = r4;
        this.A01 = r3;
        this.A05 = r6;
        this.A07 = r7;
        this.A09 = r8;
        this.A0B = r9;
        this.A04 = r5;
        this.A0C = r10;
        this.A0E = new ExecutorC27271Gr(r11, false);
    }

    public static /* synthetic */ boolean A00(C36691kN r11, AbstractC14640lm r12) {
        Integer num;
        AnonymousClass1YO r0;
        UserJid of = UserJid.of(r12);
        if (r12 == null || of == null) {
            return false;
        }
        HashMap hashMap = r11.A0F;
        AnonymousClass3FF r4 = (AnonymousClass3FF) hashMap.get(r12.getRawString());
        if (r4 == null) {
            return false;
        }
        AnonymousClass1BG r10 = r11.A04;
        C15580nU r1 = r11.A0C;
        AnonymousClass1YO r9 = (AnonymousClass1YO) r10.A00(r1).A02.get(of);
        AnonymousClass3FF r3 = null;
        if (r9 == null) {
        }
        try {
            hashMap.remove(r12.getRawString());
            Set set = r11.A0G;
            set.remove(r4);
            C15370n3 A0B = r11.A05.A0B(r12);
            C15570nT r5 = r11.A01;
            if (r5.A0F(r12)) {
                r11.A03.A00();
            }
            C15580nU A01 = r10.A01(r1);
            if (A01 == null || (r0 = (AnonymousClass1YO) r10.A03.A02(A01).A02.get(of)) == null) {
                num = null;
            } else {
                num = Integer.valueOf(r0.A01);
            }
            if (num == null) {
                num = -1;
            }
            AnonymousClass3FF r13 = new AnonymousClass3FF(A0B, r12, r9.A01, num.intValue());
            r3 = r13;
            set.add(r13);
            hashMap.put(r12.getRawString(), r13);
            if (!r5.A0F(r4.A03)) {
                return true;
            }
            r11.A00.A0A(r13);
            return true;
        } finally {
            if (r11.A01.A0F(r4.A03)) {
                r11.A00.A0A(r3);
            }
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C237913a r0 = this.A03;
        r0.A05.A04(this.A02);
        this.A07.A04(this.A06);
        this.A0B.A04(this.A0A);
        AnonymousClass11A r02 = this.A09;
        r02.A00.remove(this.A08);
    }

    public final void A04(AnonymousClass1YM r8) {
        Object obj;
        HashMap hashMap = this.A0F;
        hashMap.clear();
        AnonymousClass1JO A07 = r8.A07();
        AnonymousClass1BG r3 = this.A04;
        C15580nU r0 = this.A0C;
        HashMap hashMap2 = new HashMap();
        C15580nU A01 = r3.A01(r0);
        if (A01 != null) {
            AnonymousClass1YM A02 = r3.A03.A02(A01);
            for (AnonymousClass1YO r02 : r8.A02.values()) {
                UserJid userJid = r02.A03;
                AnonymousClass1YO r03 = (AnonymousClass1YO) A02.A02.get(userJid);
                if (r03 != null) {
                    hashMap2.put(userJid, Integer.valueOf(r03.A01));
                }
            }
        }
        AnonymousClass3P4 r4 = new AnonymousClass02O(hashMap2) { // from class: X.3P4
            public final /* synthetic */ Map A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass02O
            public final Object apply(Object obj2) {
                C36691kN r5 = C36691kN.this;
                Map map = this.A01;
                AnonymousClass1YO r7 = (AnonymousClass1YO) obj2;
                C15550nR r04 = r5.A05;
                UserJid userJid2 = r7.A03;
                C15370n3 A0B = r04.A0B(userJid2);
                if (r5.A01.A0F(A0B.A0D)) {
                    r5.A03.A00();
                }
                int i = map.get(userJid2);
                i = -1;
                if (i == null) {
                }
                AnonymousClass3FF r2 = new AnonymousClass3FF(A0B, userJid2, r7.A01, C12960it.A05(i));
                r5.A0F.put(userJid2.getRawString(), r2);
                return r2;
            }
        };
        HashSet hashSet = new HashSet();
        for (Object obj2 : A07.A00) {
            hashSet.add(r4.apply(obj2));
        }
        HashSet hashSet2 = new HashSet(AnonymousClass1JO.A00(hashSet).A00);
        Set set = this.A0G;
        set.retainAll(hashSet2);
        hashSet2.removeAll(set);
        set.addAll(hashSet2);
        C15570nT r04 = this.A01;
        r04.A08();
        C27631Ih r1 = r04.A05;
        C103854rQ r05 = new AnonymousClass02O() { // from class: X.4rQ
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj3) {
                return ((Jid) obj3).getRawString();
            }
        };
        if (r1 != null) {
            obj = r05.apply(r1);
        } else {
            obj = null;
        }
        this.A00.A0A(hashMap.get(obj));
    }
}
