package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1AT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AT {
    public static final List A02 = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
    public final C16120oU A00;
    public final AnonymousClass1AS A01;

    public AnonymousClass1AT(C16120oU r1, AnonymousClass1AS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(int i, int i2) {
        AnonymousClass1AS r4 = this.A01;
        String A00 = AnonymousClass1AS.A00(i);
        SharedPreferences sharedPreferences = r4.A00;
        if (sharedPreferences == null) {
            sharedPreferences = r4.A01.A01("privacy_highlight");
            r4.A00 = sharedPreferences;
        }
        String str = "0,0,0";
        String string = sharedPreferences.getString(A00, str);
        if (string != null) {
            str = string;
        }
        String[] split = str.split(",");
        if (i2 < split.length) {
            split[i2] = String.valueOf(Integer.parseInt(split[i2]) + 1);
            String join = TextUtils.join(",", split);
            SharedPreferences sharedPreferences2 = r4.A00;
            if (sharedPreferences2 == null) {
                sharedPreferences2 = r4.A01.A01("privacy_highlight");
                r4.A00 = sharedPreferences2;
            }
            sharedPreferences2.edit().putString(A00, join).apply();
            return;
        }
        throw new IllegalStateException("position exceeds event array length");
    }
}
