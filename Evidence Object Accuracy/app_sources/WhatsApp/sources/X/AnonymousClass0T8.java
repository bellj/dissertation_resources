package X;

import android.system.Os;
import java.io.FileDescriptor;

/* renamed from: X.0T8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T8 {
    public static FileDescriptor A00(FileDescriptor fileDescriptor) {
        return Os.dup(fileDescriptor);
    }

    public static void A01(FileDescriptor fileDescriptor) {
        Os.close(fileDescriptor);
    }

    public static void A02(FileDescriptor fileDescriptor, int i, long j) {
        Os.lseek(fileDescriptor, j, i);
    }
}
