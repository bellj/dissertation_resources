package X;

import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;

/* renamed from: X.5dV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119305dV extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119305dV() {
        C117295Zj.A0p(this, 107);
    }

    public static void A02(AnonymousClass01J r1, C249317l r2, PaymentTransactionHistoryActivity paymentTransactionHistoryActivity) {
        ((ActivityC13790kL) paymentTransactionHistoryActivity).A08 = r2;
        paymentTransactionHistoryActivity.A05 = (AnonymousClass018) r1.ANb.get();
        paymentTransactionHistoryActivity.A07 = (C15240mn) r1.A8F.get();
        paymentTransactionHistoryActivity.A0C = (C17070qD) r1.AFC.get();
        paymentTransactionHistoryActivity.A0B = (C22710zW) r1.AF7.get();
        paymentTransactionHistoryActivity.A09 = (C20380vf) r1.ACR.get();
        paymentTransactionHistoryActivity.A0D = (AnonymousClass1A8) r1.AER.get();
        paymentTransactionHistoryActivity.A0A = (C243515e) r1.AEt.get();
        paymentTransactionHistoryActivity.A0J = (AnonymousClass14X) r1.AFM.get();
        paymentTransactionHistoryActivity.A06 = (C15650ng) r1.A4m.get();
        paymentTransactionHistoryActivity.A0H = (C129795yJ) r1.AFK.get();
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            PaymentTransactionHistoryActivity paymentTransactionHistoryActivity = (PaymentTransactionHistoryActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, paymentTransactionHistoryActivity);
            ActivityC13810kN.A10(A1M, paymentTransactionHistoryActivity);
            A02(A1M, ActivityC13790kL.A0S(r3, A1M, paymentTransactionHistoryActivity, ActivityC13790kL.A0Y(A1M, paymentTransactionHistoryActivity)), paymentTransactionHistoryActivity);
        }
    }
}
