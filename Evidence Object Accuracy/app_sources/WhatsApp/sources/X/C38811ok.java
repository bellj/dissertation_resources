package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38811ok {
    public int A00;
    public int A01;
    public int A02;
    public String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;

    public C38811ok(String str, String str2, String str3, String str4, String str5, String str6, int i, int i2, int i3) {
        this.A09 = str;
        this.A05 = str2;
        this.A04 = str3;
        this.A06 = str4;
        this.A08 = str5;
        this.A07 = str6;
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
    }

    public static final void A00(String str, String str2, List list) {
        if (!TextUtils.isEmpty(str2)) {
            list.add(new AnonymousClass1W9(str, str2));
        }
    }

    public List A01(boolean z) {
        ArrayList arrayList = new ArrayList();
        A00("url", this.A09, arrayList);
        A00("media_key", this.A07, arrayList);
        A00("enc_filehash", this.A05, arrayList);
        A00("direct_path", this.A04, arrayList);
        A00("filehash", this.A06, arrayList);
        A00("mimetype", this.A08, arrayList);
        if (z) {
            A00("weight", this.A03, arrayList);
        }
        arrayList.add(new AnonymousClass1W9("size", Integer.toString(this.A01)));
        arrayList.add(new AnonymousClass1W9("width", Integer.toString(this.A02)));
        arrayList.add(new AnonymousClass1W9("height", Integer.toString(this.A00)));
        return arrayList;
    }
}
