package X;

import java.io.IOException;

/* renamed from: X.5FA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FA implements AnonymousClass5VM {
    @Override // X.AnonymousClass5VM
    public void A9f(Appendable appendable, String str) {
        try {
            int length = str.length();
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt == '\f') {
                    appendable.append("\\f");
                } else if (charAt == '\r') {
                    appendable.append("\\r");
                } else if (charAt == '\"') {
                    appendable.append("\\\"");
                } else if (charAt == '/') {
                    appendable.append("\\/");
                } else if (charAt != '\\') {
                    switch (charAt) {
                        case '\b':
                            appendable.append("\\b");
                            continue;
                        case '\t':
                            appendable.append("\\t");
                            continue;
                        case '\n':
                            appendable.append("\\n");
                            continue;
                        default:
                            if (charAt < 0 || (charAt > 31 && ((charAt < 127 || charAt > 159) && (charAt < 8192 || charAt > 8447)))) {
                                appendable.append(charAt);
                                break;
                            } else {
                                C72453ed.A1E(appendable, charAt);
                                continue;
                            }
                            break;
                    }
                } else {
                    appendable.append("\\\\");
                }
            }
        } catch (IOException unused) {
            throw C12990iw.A0m("Impossible Error");
        }
    }
}
