package X;

import com.whatsapp.picker.search.PickerSearchDialogFragment;
import com.whatsapp.picker.search.StickerSearchDialogFragment;
import java.util.List;

/* renamed from: X.3aP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69893aP implements AbstractC116245Ur, AnonymousClass5UF {
    public C15260mp A00;
    public AnonymousClass5UF A01;
    public AbstractC13950kb A02;
    public PickerSearchDialogFragment A03;
    public AbstractC116245Ur A04;
    public List A05;
    public boolean A06;
    public boolean A07;
    public final AnonymousClass1KT A08;

    public C69893aP(AnonymousClass1KT r1) {
        this.A08 = r1;
    }

    public void A00() {
        C15260mp r1 = this.A00;
        if (r1 != null) {
            r1.A03 = null;
            this.A00 = null;
        }
        this.A02 = null;
        this.A01 = null;
        this.A04 = null;
        this.A03 = null;
    }

    public void A01(PickerSearchDialogFragment pickerSearchDialogFragment) {
        this.A07 = true;
        this.A03 = pickerSearchDialogFragment;
        pickerSearchDialogFragment.A00 = this;
        if (pickerSearchDialogFragment instanceof StickerSearchDialogFragment) {
            this.A08.A01 = this;
        }
    }

    public void A02(boolean z) {
        this.A06 = z;
        PickerSearchDialogFragment pickerSearchDialogFragment = this.A03;
        if (pickerSearchDialogFragment != null) {
            pickerSearchDialogFragment.A1B();
        }
    }

    @Override // X.AnonymousClass5UF
    public void AR4(C66013Ly r2) {
        AnonymousClass5UF r0 = this.A01;
        if (r0 != null) {
            r0.AR4(r2);
        }
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r2, Integer num, int i) {
        AbstractC116245Ur r0 = this.A04;
        if (r0 != null) {
            r0.AWl(r2, num, i);
        }
    }
}
