package X;

import com.whatsapp.registration.RegisterName;

/* renamed from: X.52r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096952r implements AbstractC116455Vm {
    public final /* synthetic */ RegisterName A00;

    public C1096952r(RegisterName registerName) {
        this.A00 = registerName;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0A);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A0A, iArr, 25);
    }
}
