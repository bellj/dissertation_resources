package X;

import com.whatsapp.bloks.BloksCameraOverlay;
import com.whatsapp.util.Log;
import java.io.File;
import java.lang.ref.WeakReference;

/* renamed from: X.5ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124195ol extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final AbstractC11740gm A02;
    public final C128485wB A03;
    public final File A04;
    public final String A05;
    public final WeakReference A06;
    public final boolean A07;
    public final byte[] A08;

    public C124195ol(AbstractC11740gm r2, BloksCameraOverlay bloksCameraOverlay, C128485wB r4, File file, String str, byte[] bArr, int i, int i2, boolean z) {
        this.A02 = r2;
        this.A08 = bArr;
        this.A07 = z;
        this.A04 = file;
        this.A05 = str;
        this.A00 = i;
        this.A01 = i2;
        this.A06 = C12970iu.A10(bloksCameraOverlay);
        this.A03 = r4;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 1, insn: 0x014f: IF  (r1 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:65:?, block:B:45:0x014f
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ java.lang.Object A05(
/*
[377] Method generation error in method: X.5ol.A05(java.lang.Object[]):java.lang.Object, file: classes4.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r23v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Log.i("BloksStorePictureTask/onPostExecute start");
        AnonymousClass4N0 r2 = (AnonymousClass4N0) this.A02.get();
        if (r2 != null) {
            AnonymousClass5RJ r1 = (AnonymousClass5RJ) AnonymousClass3JV.A04(r2.A00, r2.A01);
            AnonymousClass009.A05(r1);
            AnonymousClass674 r12 = (AnonymousClass674) r1;
            r12.A0A = true;
            AnonymousClass675.A01(r2, r12, this.A03);
        }
        Log.i("BloksStorePictureTask/onPostExecute end");
    }
}
