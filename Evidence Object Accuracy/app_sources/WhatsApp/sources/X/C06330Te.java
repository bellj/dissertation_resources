package X;

import androidx.lifecycle.OnLifecycleEvent;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Te  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06330Te {
    public static C06330Te A02 = new C06330Te();
    public final Map A00 = new HashMap();
    public final Map A01 = new HashMap();

    public static final void A00(AnonymousClass0P7 r4, AnonymousClass074 r5, Class cls, Map map) {
        Object obj = map.get(r4);
        if (obj == null) {
            map.put(r4, r5);
        } else if (r5 != obj) {
            Method method = r4.A01;
            StringBuilder sb = new StringBuilder("Method ");
            sb.append(method.getName());
            sb.append(" in ");
            sb.append(cls.getName());
            sb.append(" already declared with different @OnLifecycleEvent value: previous value ");
            sb.append(obj);
            sb.append(", new value ");
            sb.append(r5);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public final C05840Re A01(Class cls, Method[] methodArr) {
        int i;
        Class superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (superclass != null) {
            C05840Re r0 = (C05840Re) this.A00.get(superclass);
            if (r0 == null) {
                r0 = A01(superclass, null);
            }
            hashMap.putAll(r0.A01);
        }
        Class<?>[] interfaces = cls.getInterfaces();
        for (Class<?> cls2 : interfaces) {
            C05840Re r02 = (C05840Re) this.A00.get(cls2);
            if (r02 == null) {
                r02 = A01(cls2, null);
            }
            for (Map.Entry entry : r02.A01.entrySet()) {
                A00((AnonymousClass0P7) entry.getKey(), (AnonymousClass074) entry.getValue(), cls, hashMap);
            }
        }
        if (methodArr == null) {
            try {
                methodArr = cls.getDeclaredMethods();
            } catch (NoClassDefFoundError e) {
                throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", e);
            }
        }
        boolean z = false;
        for (Method method : methodArr) {
            OnLifecycleEvent onLifecycleEvent = (OnLifecycleEvent) method.getAnnotation(OnLifecycleEvent.class);
            if (onLifecycleEvent != null) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                int length = parameterTypes.length;
                if (length <= 0) {
                    i = 0;
                } else if (parameterTypes[0].isAssignableFrom(AbstractC001200n.class)) {
                    i = 1;
                } else {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                AnonymousClass074 value = onLifecycleEvent.value();
                if (length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(AnonymousClass074.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (value == AnonymousClass074.ON_ANY) {
                        i = 2;
                    } else {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (length <= 2) {
                    A00(new AnonymousClass0P7(method, i), value, cls, hashMap);
                    z = true;
                } else {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
            }
        }
        C05840Re r2 = new C05840Re(hashMap);
        this.A00.put(cls, r2);
        this.A01.put(cls, Boolean.valueOf(z));
        return r2;
    }
}
