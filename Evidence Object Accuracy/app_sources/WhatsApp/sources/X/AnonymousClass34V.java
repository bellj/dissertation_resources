package X;

import android.widget.SeekBar;

/* renamed from: X.34V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34V extends AbstractC102584pN {
    public final /* synthetic */ C60752yZ A00;

    public AnonymousClass34V(C60752yZ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC102584pN
    public void A00(int i) {
        C60752yZ r3 = this.A00;
        r3.setDuration(C38131nZ.A04(((AbstractC28551Oa) r3).A0K, (long) i));
    }

    @Override // X.AbstractC102584pN, android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        C60752yZ r0 = this.A00;
        C35191hP.A01((C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) r0).A0O), r0.A09.A07.getProgress());
    }

    @Override // X.AbstractC102584pN, android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        C35191hP A00;
        C60752yZ r1 = this.A00;
        C30421Xi r2 = (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) r1).A0O);
        C35191hP.A01(r2, r1.A09.A07.getProgress());
        AnonymousClass11P r12 = r1.A07.A05;
        if (r12.A0D(r2) && (A00 = r12.A00()) != null) {
            A00.A08++;
        }
    }
}
