package X;

import java.util.Timer;

/* renamed from: X.1cK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC32541cK {
    public static Timer A02 = new Timer();
    public boolean A00;
    public final AnonymousClass5IO A01;

    public AbstractC32541cK() {
        AnonymousClass5IO r1 = new AnonymousClass5IO(this);
        this.A01 = r1;
        A02.schedule(r1, 20000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r4 = this;
            boolean r0 = r4 instanceof X.AnonymousClass2QF
            if (r0 != 0) goto L_0x0085
            boolean r0 = r4 instanceof X.AnonymousClass248
            if (r0 != 0) goto L_0x0063
            r3 = r4
            X.1cJ r3 = (X.RunnableC32531cJ) r3
            java.lang.String r0 = "groupmgr/group_request/timeout/type:"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            int r0 = r3.A00
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            r0 = 1
            r3.A00 = r0
            int r1 = r3.A00
            r0 = 30
            if (r1 == r0) goto L_0x0058
            r0 = 91
            if (r1 == r0) goto L_0x0055
            r0 = 92
            if (r1 == r0) goto L_0x0052
            switch(r1) {
                case 15: goto L_0x004c;
                case 16: goto L_0x004f;
                case 17: goto L_0x005b;
                default: goto L_0x0032;
            }
        L_0x0032:
            X.1P4 r0 = r3.A04
            if (r0 == 0) goto L_0x003f
            X.0mA r2 = r3.A05
            java.lang.String r1 = r0.A01
            r0 = 500(0x1f4, float:7.0E-43)
            r2.A0H(r1, r0)
        L_0x003f:
            X.0xE r2 = r3.A02
            X.0nU r1 = r3.A01
            r0 = 0
            r2.A09(r1, r0)
            r0 = 0
            r3.A01(r0)
            return
        L_0x004c:
            r1 = 1002(0x3ea, float:1.404E-42)
            goto L_0x005d
        L_0x004f:
            r1 = 1001(0x3e9, float:1.403E-42)
            goto L_0x005d
        L_0x0052:
            r1 = 1006(0x3ee, float:1.41E-42)
            goto L_0x005d
        L_0x0055:
            r1 = 1005(0x3ed, float:1.408E-42)
            goto L_0x005d
        L_0x0058:
            r1 = 1003(0x3eb, float:1.406E-42)
            goto L_0x005d
        L_0x005b:
            r1 = 1007(0x3ef, float:1.411E-42)
        L_0x005d:
            X.0nU r0 = r3.A01
            X.C20710wC.A02(r1, r0)
            goto L_0x0032
        L_0x0063:
            r3 = r4
            X.248 r3 = (X.AnonymousClass248) r3
            java.lang.String r0 = "groupmgr/group_request/timeout/type:"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            int r0 = r3.A00
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            r0 = 1
            r3.A00 = r0
            X.0xE r2 = r3.A02
            X.1Ve r1 = r3.A01
            r0 = 0
            r2.A09(r1, r0)
            return
        L_0x0085:
            r1 = r4
            X.2QF r1 = (X.AnonymousClass2QF) r1
            r0 = 1
            r1.A00 = r0
            java.lang.String r0 = "web/web-action/setgroupdescription/timeout"
            com.whatsapp.util.Log.w(r0)
            r0 = 0
            r1.APl(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC32541cK.A00():void");
    }
}
