package X;

import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.1er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC33661er extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC33661er() {
        A0R(new C103544qv(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            MessageReplyActivity messageReplyActivity = (MessageReplyActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) messageReplyActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) messageReplyActivity).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) messageReplyActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) messageReplyActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) messageReplyActivity).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) messageReplyActivity).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) messageReplyActivity).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) messageReplyActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) messageReplyActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) messageReplyActivity).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) messageReplyActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) messageReplyActivity).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) messageReplyActivity).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) messageReplyActivity).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) messageReplyActivity).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) messageReplyActivity).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) messageReplyActivity).A09 = r3.A06();
            ((ActivityC13790kL) messageReplyActivity).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) messageReplyActivity).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) messageReplyActivity).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) messageReplyActivity).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) messageReplyActivity).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) messageReplyActivity).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) messageReplyActivity).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) messageReplyActivity).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) messageReplyActivity).A08 = (C249317l) r2.A8B.get();
            messageReplyActivity.A0d = (C253719d) r2.A8V.get();
            messageReplyActivity.A0S = (AnonymousClass19P) r2.ACQ.get();
            messageReplyActivity.A0C = (C239613r) r2.AI9.get();
            messageReplyActivity.A0Z = (C16120oU) r2.ANE.get();
            messageReplyActivity.A0H = (AnonymousClass19X) r2.AIe.get();
            messageReplyActivity.A0D = (C16170oZ) r2.AM4.get();
            messageReplyActivity.A0G = (AnonymousClass19N) r2.A32.get();
            messageReplyActivity.A0h = (C14410lO) r2.AB3.get();
            messageReplyActivity.A0X = (C231510o) r2.AHO.get();
            messageReplyActivity.A0P = (C21270x9) r2.A4A.get();
            messageReplyActivity.A0g = (C244415n) r2.AAg.get();
            messageReplyActivity.A0M = (C15550nR) r2.A45.get();
            messageReplyActivity.A0p = (C22210yi) r2.AGm.get();
            messageReplyActivity.A0y = (C22190yg) r2.AB6.get();
            messageReplyActivity.A0J = (C253619c) r2.AId.get();
            messageReplyActivity.A0N = (C15610nY) r2.AMe.get();
            messageReplyActivity.A0K = (C238013b) r2.A1Z.get();
            messageReplyActivity.A0W = (C15650ng) r2.A4m.get();
            messageReplyActivity.A0r = (AnonymousClass146) r2.AKM.get();
            messageReplyActivity.A0l = (C21200x2) r2.A2q.get();
            messageReplyActivity.A0e = (AbstractC253919f) r2.AGb.get();
            messageReplyActivity.A0z = (AnonymousClass19O) r2.ACO.get();
            messageReplyActivity.A0T = (C17050qB) r2.ABL.get();
            messageReplyActivity.A0s = (C235512c) r2.AKS.get();
            messageReplyActivity.A0L = (AnonymousClass116) r2.A3z.get();
            messageReplyActivity.A0Y = (AnonymousClass193) r2.A6S.get();
            messageReplyActivity.A0U = (C15890o4) r2.AN1.get();
            messageReplyActivity.A11 = (C17020q8) r2.A6G.get();
            messageReplyActivity.A0E = (C254719n) r2.A2U.get();
            messageReplyActivity.A0F = (C14650lo) r2.A2V.get();
            messageReplyActivity.A0q = (AnonymousClass1AB) r2.AKI.get();
            messageReplyActivity.A0n = (C16630pM) r2.AIc.get();
            messageReplyActivity.A0R = (AnonymousClass11P) r2.ABp.get();
            messageReplyActivity.A13 = r3.A0N();
            messageReplyActivity.A0a = r3.A07();
            messageReplyActivity.A0k = (AnonymousClass1A2) r2.AET.get();
            messageReplyActivity.A0w = (AnonymousClass1A4) r2.AKX.get();
            messageReplyActivity.A0t = (C255519v) r2.AKF.get();
            messageReplyActivity.A0I = (AnonymousClass1AA) r2.ADr.get();
            messageReplyActivity.A0V = (AnonymousClass1AD) r2.A54.get();
        }
    }
}
