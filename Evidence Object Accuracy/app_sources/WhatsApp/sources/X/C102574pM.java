package X;

import android.widget.SeekBar;

/* renamed from: X.4pM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102574pM implements SeekBar.OnSeekBarChangeListener {
    public final /* synthetic */ AbstractC14670lq A00;

    public C102574pM(AbstractC14670lq r1) {
        this.A00 = r1;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        AbstractC14670lq.A01(this.A00, i, z);
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        AbstractC14670lq r5 = this.A00;
        r5.A1J.A02++;
        if (r5.A0N != null) {
            r5.A0a.removeCallbacks(r5.A1Q);
            r5.A08 = -1;
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        AbstractC14670lq.A00(this.A00);
    }
}
