package X;

import com.whatsapp.R;

/* renamed from: X.49l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC869149l {
    /* Fake field, exist only in values array */
    CONTENT_STICKERS(0, R.string.shape_picker_section_content_stickers),
    /* Fake field, exist only in values array */
    SHAPES(1, R.string.shape_picker_section_shapes);
    
    public final int sectionResId;
    public final AbstractC470728v[] shapeData;

    EnumC869149l(int i, int i2) {
        this.shapeData = r2;
        this.sectionResId = i2;
    }
}
