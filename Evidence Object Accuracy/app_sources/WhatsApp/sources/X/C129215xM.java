package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import java.util.ArrayList;

/* renamed from: X.5xM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129215xM {
    public final Context A00;
    public final C14900mE A01;
    public final C18650sn A02;
    public final C18610sj A03;
    public final AnonymousClass60T A04;
    public final C30931Zj A05 = C117305Zk.A0V("PaymentProviderKeyAction", "network");
    public final String A06;

    public C129215xM(Context context, C14900mE r4, C18650sn r5, C18610sj r6, AnonymousClass60T r7, String str) {
        this.A00 = context;
        this.A01 = r4;
        this.A03 = r6;
        this.A02 = r5;
        this.A04 = r7;
        this.A06 = str;
    }

    public void A00(AnonymousClass6MV r10, String str) {
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "get-provider-key", A0l);
        C117295Zj.A1M("provider", str, A0l);
        C117295Zj.A1M("key-scope", this.A06, A0l);
        C117305Zk.A1I(this.A03, new IDxRCallbackShape0S0200000_3_I1(this.A00, this.A01, this.A02, r10, this, 13), C117295Zj.A0K(A0l));
    }
}
