package X;

import com.whatsapp.jid.UserJid;
import java.util.AbstractCollection;

/* renamed from: X.2EI  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2EI implements AbstractC21730xt, AnonymousClass1W3 {
    public boolean A00 = false;
    public final C14650lo A01;

    public AnonymousClass2EI(C14650lo r2) {
        this.A01 = r2;
    }

    public static void A00(String str, String str2, AbstractCollection abstractCollection) {
        abstractCollection.add(new AnonymousClass1V8(str, str2, (AnonymousClass1W9[]) null));
    }

    public boolean A01(UserJid userJid, int i) {
        if (i != 421) {
            return false;
        }
        if (!this.A00) {
            this.A00 = true;
            this.A01.A07.A02(this, userJid, true);
            return true;
        }
        APB(userJid);
        return true;
    }
}
