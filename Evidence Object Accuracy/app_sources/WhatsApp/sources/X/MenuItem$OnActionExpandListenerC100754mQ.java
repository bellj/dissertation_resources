package X;

import android.view.MenuItem;

/* renamed from: X.4mQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100754mQ implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ AbstractActivityC36551k4 A00;

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    public MenuItem$OnActionExpandListenerC100754mQ(AbstractActivityC36551k4 r1) {
        this.A00 = r1;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        AbstractActivityC36551k4 r1 = this.A00;
        r1.A0I = null;
        AbstractActivityC36551k4.A02(r1);
        return true;
    }
}
