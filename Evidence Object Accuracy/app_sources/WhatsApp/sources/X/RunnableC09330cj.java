package X;

import androidx.work.Worker;

/* renamed from: X.0cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09330cj implements Runnable {
    public final /* synthetic */ Worker A00;

    public RunnableC09330cj(Worker worker) {
        this.A00 = worker;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            Worker worker = this.A00;
            worker.A00.A09(worker.A04());
        } catch (Throwable th) {
            this.A00.A00.A0A(th);
        }
    }
}
