package X;

import android.text.TextUtils;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2jB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC55712jB extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ AnonymousClass1BD A01;
    public final /* synthetic */ Integer A02;
    public final /* synthetic */ Long A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;

    public /* synthetic */ RunnableC55712jB(UserJid userJid, AnonymousClass1BD r2, Integer num, Long l, String str, String str2, String str3) {
        this.A01 = r2;
        this.A02 = num;
        this.A04 = str;
        this.A03 = l;
        this.A05 = str2;
        this.A06 = str3;
        this.A00 = userJid;
    }

    @Override // java.lang.Runnable
    public final void run() {
        long j;
        C15370n3 A0A;
        int i;
        AnonymousClass1BD r4 = this.A01;
        Integer num = this.A02;
        String str = this.A04;
        Long l = this.A03;
        String str2 = this.A05;
        String str3 = this.A06;
        UserJid userJid = this.A00;
        AnonymousClass31F r3 = new AnonymousClass31F();
        r3.A02 = num;
        C458423j r9 = r4.A01;
        if (r9 != null) {
            r3.A03 = C12970iu.A0g();
            r3.A06 = Long.valueOf(r9.A03);
            r3.A07 = Long.valueOf(r9.A04);
            r3.A08 = str;
            r3.A09 = r9.A09;
            if (str != null) {
                r3.A05 = l;
                Number number = (Number) r4.A0F.get(str2);
                if (number != null) {
                    i = number.intValue();
                } else {
                    i = 0;
                }
                r3.A04 = C12980iv.A0l(i);
            }
        } else {
            r3.A03 = 1;
            C458223h r0 = r4.A00;
            if (r0 == null) {
                j = 0;
            } else {
                j = r0.A05;
            }
            r3.A06 = Long.valueOf(j);
            r3.A09 = str3;
        }
        if (r4.A09.A07(1254) && (A0A = r4.A03.A0A(userJid)) != null) {
            r3.A00 = Boolean.valueOf(A0A.A0J());
            r3.A01 = Boolean.valueOf(A0A.A0I());
        }
        boolean isEmpty = TextUtils.isEmpty(r3.A09);
        C16120oU r1 = r4.A0A;
        if (isEmpty) {
            r1.A06(r3);
        } else {
            r1.A0B(r3, AnonymousClass4ZH.A00, true);
        }
    }
}
