package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2hi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55052hi extends AnonymousClass03U {
    public final TextView A00;
    public final C15450nH A01;
    public final C28801Pb A02;
    public final ThumbnailButton A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass018 A06;
    public final C16120oU A07;

    public C55052hi(View view, C15450nH r4, C15550nR r5, C15610nY r6, AnonymousClass018 r7, C16120oU r8, AnonymousClass12F r9) {
        super(view);
        this.A07 = r8;
        this.A01 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A06 = r7;
        this.A03 = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.storage_usage_chat_contact_photo);
        this.A00 = C12960it.A0I(view, R.id.storage_usage_chat_used_space);
        this.A02 = new C28801Pb(view, r6, r9, (int) R.id.storage_usage_chat_contact_name);
        AnonymousClass23N.A01(view);
    }
}
