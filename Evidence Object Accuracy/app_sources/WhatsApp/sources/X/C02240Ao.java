package X;

import android.util.Property;
import android.view.View;

/* renamed from: X.0Ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02240Ao extends Property {
    public C02240Ao(Class cls) {
        super(cls, "translationXPercent");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        View view = (View) obj;
        float width = (float) view.getWidth();
        float f = 0.0f;
        if (width > 0.0f) {
            f = view.getTranslationX() / width;
        }
        return Float.valueOf(f);
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        View view = (View) obj;
        view.setTranslationX(((float) view.getWidth()) * ((Number) obj2).floatValue());
    }
}
