package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Outline;
import android.graphics.Rect;
import android.view.Gravity;

/* renamed from: X.0DM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DM extends AnonymousClass0DN {
    public AnonymousClass0DM(Resources resources, Bitmap bitmap) {
        super(resources, bitmap);
    }

    @Override // X.AnonymousClass0DN
    public void A02(int i, int i2, int i3, Rect rect, Rect rect2) {
        Gravity.apply(i, i2, i3, rect, rect2, 0);
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        A01();
        outline.setRoundRect(this.A0B, this.A00);
    }
}
