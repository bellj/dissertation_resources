package X;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1QJ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1QJ extends AnonymousClass1QF {
    public String A00;
    public String[] A01;
    public final Context A02;
    public final Map A03 = new HashMap();

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1QJ(android.content.Context r3, java.lang.String r4) {
        /*
            r2 = this;
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            android.content.pm.ApplicationInfo r0 = r3.getApplicationInfo()
            java.lang.String r0 = r0.dataDir
            r1.append(r0)
            java.lang.String r0 = "/"
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            r0 = 1
            r2.<init>(r1, r0)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r2.A03 = r0
            r2.A02 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QJ.<init>(android.content.Context, java.lang.String):void");
    }

    public static void A02(File file) {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    A02(file2);
                }
            } else {
                return;
            }
        }
        if (!file.delete() && file.exists()) {
            StringBuilder sb = new StringBuilder("could not delete: ");
            sb.append(file);
            throw new IOException(sb.toString());
        }
    }

    public static void A03(File file, byte b) {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
        try {
            randomAccessFile.seek(0);
            randomAccessFile.write(b);
            randomAccessFile.setLength(randomAccessFile.getFilePointer());
            randomAccessFile.getFD().sync();
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                try {
                    randomAccessFile.close();
                } catch (Throwable unused) {
                }
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ff A[Catch: all -> 0x0528, TryCatch #27 {all -> 0x052d, blocks: (B:9:0x0042, B:11:0x005c, B:13:0x0064, B:14:0x006d, B:23:0x00af, B:24:0x00c2, B:39:0x0143, B:40:0x0146, B:45:0x0188, B:156:0x0476, B:158:0x047b, B:161:0x04ab, B:163:0x04be, B:164:0x04d9, B:46:0x01a2, B:48:0x01b1, B:49:0x01b8, B:51:0x01be, B:53:0x01c7, B:55:0x01cb, B:57:0x01da, B:58:0x01e3, B:155:0x0473, B:59:0x01eb, B:61:0x01ef, B:62:0x01fc, B:64:0x0208, B:65:0x0211, B:172:0x0519, B:25:0x00d0, B:27:0x00ea, B:29:0x00f6, B:31:0x00ff, B:32:0x0107, B:34:0x010f, B:35:0x0117, B:37:0x0126, B:38:0x012e, B:15:0x0075, B:17:0x0079, B:18:0x0085, B:19:0x0090, B:22:0x009e, B:41:0x015e, B:43:0x0164), top: B:183:0x0042 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0107 A[Catch: all -> 0x0528, TryCatch #27 {all -> 0x052d, blocks: (B:9:0x0042, B:11:0x005c, B:13:0x0064, B:14:0x006d, B:23:0x00af, B:24:0x00c2, B:39:0x0143, B:40:0x0146, B:45:0x0188, B:156:0x0476, B:158:0x047b, B:161:0x04ab, B:163:0x04be, B:164:0x04d9, B:46:0x01a2, B:48:0x01b1, B:49:0x01b8, B:51:0x01be, B:53:0x01c7, B:55:0x01cb, B:57:0x01da, B:58:0x01e3, B:155:0x0473, B:59:0x01eb, B:61:0x01ef, B:62:0x01fc, B:64:0x0208, B:65:0x0211, B:172:0x0519, B:25:0x00d0, B:27:0x00ea, B:29:0x00f6, B:31:0x00ff, B:32:0x0107, B:34:0x010f, B:35:0x0117, B:37:0x0126, B:38:0x012e, B:15:0x0075, B:17:0x0079, B:18:0x0085, B:19:0x0090, B:22:0x009e, B:41:0x015e, B:43:0x0164), top: B:183:0x0042 }] */
    @Override // X.AnonymousClass1QG
    public void A04(int r32) {
        /*
        // Method dump skipped, instructions count: 1352
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QJ.A04(int):void");
    }
}
