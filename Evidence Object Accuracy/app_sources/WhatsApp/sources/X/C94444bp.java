package X;

/* renamed from: X.4bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94444bp {
    public static final byte[] A04 = {78, 111, 105, 115, 101, 95, 88, 88, 102, 97, 108, 108, 98, 97, 99, 107, 95, 50, 53, 53, 49, 57, 95, 65, 69, 83, 71, 67, 77, 95, 83, 72, 65, 50, 53, 54};
    public static final byte[] A05 = {78, 111, 105, 115, 101, 95, 88, 88, 95, 50, 53, 53, 49, 57, 95, 65, 69, 83, 71, 67, 77, 95, 83, 72, 65, 50, 53, 54, 0, 0, 0, 0};
    public static final byte[] A06 = {78, 111, 105, 115, 101, 95, 73, 75, 95, 50, 53, 53, 49, 57, 95, 65, 69, 83, 71, 67, 77, 95, 83, 72, 65, 50, 53, 54, 0, 0, 0, 0};
    public long A00;
    public AnonymousClass20Q A01;
    public byte[] A02;
    public final AnonymousClass4VB A03;

    public C94444bp(byte[] bArr, byte[] bArr2) {
        AnonymousClass4VB r1 = new AnonymousClass4VB(bArr);
        this.A03 = r1;
        this.A02 = r1.A00;
        r1.A00(bArr2);
    }

    public AnonymousClass20P A00(AnonymousClass2ST r6) {
        byte[][] A062 = C16050oM.A06(C32891cu.A01(new byte[0], this.A02, null, 64), 32, 32);
        return new AnonymousClass20P(r6, A062[0], A062[1]);
    }

    public void A01(byte[] bArr) {
        byte[][] A062 = C16050oM.A06(C32891cu.A01(bArr, this.A02, null, 64), 32, 32);
        this.A02 = A062[0];
        this.A01 = new AnonymousClass20Q(A062[1]);
        this.A00 = 0;
    }

    public byte[] A02(byte[] bArr) {
        byte[] bArr2;
        AnonymousClass20Q r5 = this.A01;
        if (r5 != null) {
            long j = this.A00;
            this.A00 = 1 + j;
            bArr2 = r5.A01(this.A03.A00, bArr, j);
        } else {
            bArr2 = bArr;
        }
        this.A03.A00(bArr);
        return bArr2;
    }

    public byte[] A03(byte[] bArr) {
        byte[] bArr2 = bArr;
        AnonymousClass20Q r2 = this.A01;
        if (r2 != null) {
            long j = this.A00;
            this.A00 = 1 + j;
            bArr2 = r2.A00(this.A03.A00, bArr2, bArr.length, j);
        }
        this.A03.A00(bArr2);
        return bArr2;
    }
}
