package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3V3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3V3 implements AbstractC116485Vq {
    public final /* synthetic */ C53842fM A00;
    public final /* synthetic */ String A01;

    public AnonymousClass3V3(C53842fM r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AbstractC116485Vq
    public void AU2(String str) {
        this.A00.A0P.A0A(str);
    }

    @Override // X.AbstractC116485Vq
    public void AU3(C90134Ms r7) {
        String str = r7.A01;
        if (str.equals("success")) {
            C53842fM r1 = this.A00;
            AnonymousClass016 r0 = r1.A0A;
            String str2 = this.A01;
            r0.A0A(str2);
            AnonymousClass016 r02 = r1.A09;
            String str3 = r7.A00;
            r02.A0A(str3);
            C14820m6 r2 = r1.A0K;
            UserJid userJid = r1.A0M;
            r2.A0s(userJid.getRawString(), str2);
            r2.A0r(userJid.getRawString(), str3);
        }
        this.A00.A0P.A0A(str);
    }
}
