package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.service.NoviPaymentInviteFragment;
import java.util.Collections;

/* renamed from: X.5oR  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oR extends AbstractC16350or {
    public final UserJid A00;
    public final /* synthetic */ NoviPaymentInviteFragment A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5oR(UserJid userJid, NoviPaymentInviteFragment noviPaymentInviteFragment) {
        super(noviPaymentInviteFragment);
        this.A01 = noviPaymentInviteFragment;
        this.A00 = userJid;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        UserJid userJid = this.A00;
        C126895tc r7 = new C126895tc(userJid, 0);
        NoviPaymentInviteFragment noviPaymentInviteFragment = this.A01;
        AnonymousClass1ZO A0F = C117305Zk.A0F(userJid, noviPaymentInviteFragment.A04);
        AnonymousClass009.A05(A0F);
        if (A0F.A06(2) != 0 || C42391v8.A04(userJid)) {
            return r7;
        }
        if (!noviPaymentInviteFragment.A01.A00(AnonymousClass1JB.A0G, AnonymousClass1JA.A0B, Collections.singleton(userJid)).A00()) {
            return r7;
        }
        C17070qD r0 = noviPaymentInviteFragment.A04;
        r0.A03();
        r0.A09.A0H(userJid);
        AnonymousClass1ZO A0F2 = C117305Zk.A0F(userJid, noviPaymentInviteFragment.A04);
        AnonymousClass009.A05(A0F2);
        return new C126895tc(userJid, A0F2.A06(2));
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        this.A01.A1D(((C126895tc) obj).A00);
    }
}
