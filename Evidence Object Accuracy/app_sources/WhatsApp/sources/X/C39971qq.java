package X;

import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39971qq {
    public final C15570nT A00;
    public final C16590pI A01;
    public final C14850m9 A02;
    public final AnonymousClass1G3 A03;
    public final AnonymousClass1PG A04;
    public final JniBridge A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final byte[] A09;

    public C39971qq(C15570nT r1, C16590pI r2, C14850m9 r3, AnonymousClass1G3 r4, AnonymousClass1PG r5, JniBridge jniBridge, byte[] bArr, boolean z, boolean z2, boolean z3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = jniBridge;
        this.A03 = r4;
        this.A08 = z;
        this.A06 = z2;
        this.A04 = r5;
        this.A09 = bArr;
        this.A01 = r2;
        this.A07 = z3;
    }
}
