package X;

import android.os.IBinder;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.lang.reflect.Field;

/* renamed from: X.2l7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56502l7 extends AbstractBinderC79573qo {
    public final Object A00;

    public BinderC56502l7(Object obj) {
        this.A00 = obj;
    }

    public static Object A00(IObjectWrapper iObjectWrapper) {
        if (iObjectWrapper instanceof BinderC56502l7) {
            return ((BinderC56502l7) iObjectWrapper).A00;
        }
        IBinder asBinder = iObjectWrapper.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        int length = declaredFields.length;
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i == 1) {
            C13020j0.A01(field);
            if (!field.isAccessible()) {
                field.setAccessible(true);
                try {
                    return field.get(asBinder);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException("Could not access the field in remoteBinder.", e);
                } catch (NullPointerException e2) {
                    throw new IllegalArgumentException("Binder object is null.", e2);
                }
            } else {
                throw C12970iu.A0f("IObjectWrapper declared field not private!");
            }
        } else {
            throw C12970iu.A0f(C12960it.A0e("Unexpected number of IObjectWrapper declared fields: ", C12980iv.A0t(64), length));
        }
    }
}
