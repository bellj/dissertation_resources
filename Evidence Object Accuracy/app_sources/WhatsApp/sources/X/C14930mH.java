package X;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.TextView;

/* renamed from: X.0mH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14930mH {
    public boolean A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final ImageView A02;
    public final ImageView A03;
    public final ImageView A04;
    public final TextView A05;
    public final AnonymousClass018 A06;

    public C14930mH(ImageView imageView, ImageView imageView2, ImageView imageView3, TextView textView, AnonymousClass018 r7) {
        this.A06 = r7;
        AnonymousClass009.A03(imageView);
        this.A02 = imageView;
        AnonymousClass009.A03(imageView2);
        this.A03 = imageView2;
        AnonymousClass009.A03(imageView3);
        this.A04 = imageView3;
        AnonymousClass009.A03(textView);
        this.A05 = textView;
    }

    public static Drawable A00(Drawable drawable, Drawable drawable2) {
        if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            if (layerDrawable.getNumberOfLayers() > 0) {
                drawable = layerDrawable.getDrawable(layerDrawable.getNumberOfLayers() - 1);
            }
        }
        if (drawable == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 23) {
            return drawable2;
        }
        TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{drawable, drawable2});
        transitionDrawable.setCrossFadeEnabled(true);
        transitionDrawable.startTransition(50);
        A01(drawable, transitionDrawable, 0);
        A01(drawable2, transitionDrawable, 1);
        return transitionDrawable;
    }

    public static void A01(Drawable drawable, TransitionDrawable transitionDrawable, int i) {
        int intrinsicHeight = drawable.getIntrinsicHeight();
        transitionDrawable.setLayerWidth(i, drawable.getIntrinsicWidth());
        transitionDrawable.setLayerHeight(i, intrinsicHeight);
        transitionDrawable.setLayerGravity(i, 17);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0059, code lost:
        if (r10 == null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c6, code lost:
        if (r9 == null) goto L_0x00c8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(X.AbstractC14940mI r19) {
        /*
        // Method dump skipped, instructions count: 415
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14930mH.A02(X.0mI):void");
    }
}
