package X;

import android.os.Environment;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32861cr {
    public boolean A00;
    public boolean A01;
    public final C27421Hj A02;

    public C32861cr(C15810nw r5, C15890o4 r6, C14950mJ r7) {
        String obj;
        this.A02 = new C27421Hj(r6, r7, new File((File) r5.A03.get(), ".trash"));
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted".equals(externalStorageState)) {
            this.A00 = false;
            this.A01 = false;
            return;
        }
        if ("mounted_ro".equals(externalStorageState)) {
            this.A00 = false;
            this.A01 = true;
            obj = "media-state-manager/main/media/read-only";
        } else {
            this.A00 = true;
            this.A01 = false;
            StringBuilder sb = new StringBuilder("media-state-manager/main/media/unavailable ");
            sb.append(externalStorageState);
            obj = sb.toString();
        }
        Log.i(obj);
    }
}
