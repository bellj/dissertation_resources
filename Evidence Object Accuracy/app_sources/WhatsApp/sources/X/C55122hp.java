package X;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.components.SegmentedProgressBar;
import com.whatsapp.storage.SizeTickerView;

/* renamed from: X.2hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55122hp extends AnonymousClass03U {
    public long A00;
    public long A01;
    public AnimatorSet A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final View A07;
    public final AbstractC15710nm A08;
    public final WaImageView A09;
    public final WaTextView A0A;
    public final WaTextView A0B;
    public final SegmentedProgressBar A0C;
    public final AnonymousClass018 A0D;
    public final SizeTickerView A0E;
    public final SizeTickerView A0F;
    public final int[] A0G;

    public C55122hp(View view, AbstractC15710nm r9, AnonymousClass018 r10) {
        super(view);
        this.A08 = r9;
        this.A0D = r10;
        SizeTickerView sizeTickerView = (SizeTickerView) AnonymousClass028.A0D(view, R.id.used_space_text);
        this.A0F = sizeTickerView;
        View view2 = this.A0H;
        sizeTickerView.A0B(0, AnonymousClass00T.A00(view2.getContext(), R.color.settings_accented_text), false);
        this.A0B = C12960it.A0N(view, R.id.used_space_description_text);
        SizeTickerView sizeTickerView2 = (SizeTickerView) AnonymousClass028.A0D(view, R.id.free_space_text);
        this.A0E = sizeTickerView2;
        sizeTickerView2.A0B(0, AnonymousClass00T.A00(view2.getContext(), R.color.storage_usage_gray), false);
        this.A09 = C12980iv.A0X(view, R.id.free_space_critical_icon);
        this.A0A = C12960it.A0N(view, R.id.free_space_description_text);
        this.A0C = (SegmentedProgressBar) AnonymousClass028.A0D(view, R.id.progress_bar);
        this.A07 = AnonymousClass028.A0D(view, R.id.progress_bar_legend_container);
        TextView A0I = C12960it.A0I(view, R.id.media_description_text);
        TextView A0I2 = C12960it.A0I(view, R.id.other_description_text);
        Context context = view.getContext();
        int[] A07 = C13000ix.A07();
        this.A0G = A07;
        A07[0] = AnonymousClass00T.A00(context, R.color.settings_accented_text);
        A07[1] = AnonymousClass00T.A00(context, R.color.paletteHighlight);
        this.A05 = AnonymousClass00T.A00(context, R.color.settings_item_subtitle_text);
        this.A04 = AnonymousClass00T.A00(context, R.color.storage_usage_red);
        this.A06 = AnonymousClass00T.A00(context, R.color.storage_usage_progress_bar_background_color);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.storage_usage_summary_circle_size);
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.storage_usage_green_circle);
        A04.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
        A0I.setCompoundDrawables(A04, null, null, null);
        Drawable A042 = AnonymousClass00T.A04(context, R.drawable.storage_usage_yellow_circle);
        A042.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
        A0I2.setCompoundDrawables(A042, null, null, null);
    }
}
