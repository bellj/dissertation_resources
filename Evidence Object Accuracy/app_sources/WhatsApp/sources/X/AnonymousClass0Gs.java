package X;

import android.graphics.Path;
import java.util.List;

/* renamed from: X.0Gs  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gs extends AnonymousClass0QR {
    public final Path A00 = new Path();
    public final AnonymousClass0SE A01 = new AnonymousClass0SE();

    public AnonymousClass0Gs(List list) {
        super(list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r5.A01 != false) goto L_0x0021;
     */
    @Override // X.AnonymousClass0QR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A04(X.AnonymousClass0U8 r16, float r17) {
        /*
        // Method dump skipped, instructions count: 384
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Gs.A04(X.0U8, float):java.lang.Object");
    }
}
