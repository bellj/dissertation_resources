package X;

import android.content.Context;
import com.whatsapp.location.LocationPicker;

/* renamed from: X.4qL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103184qL implements AbstractC009204q {
    public final /* synthetic */ LocationPicker A00;

    public C103184qL(LocationPicker locationPicker) {
        this.A00 = locationPicker;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
