package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.0vy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20570vy {
    public final C16490p7 A00;

    public C20570vy(C16490p7 r1) {
        this.A00 = r1;
    }

    public final void A00(AnonymousClass1XC r7, String str) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09(str, new String[]{Long.toString(r7.A11)});
            if (A09.moveToLast()) {
                r7.A00 = A09.getInt(A09.getColumnIndexOrThrow("service"));
                r7.A01 = A09.getLong(A09.getColumnIndexOrThrow("expiration_timestamp"));
            }
            A09.close();
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A01(String str, int i, long j, long j2) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("service", Integer.valueOf(i));
            contentValues.put("expiration_timestamp", Long.valueOf(j2));
            A02.A03.A06(contentValues, str, 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
