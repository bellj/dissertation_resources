package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1LC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1LC {
    public final Context A00;
    public final Executor A01;
    public final AtomicBoolean A02;
    public final AtomicInteger A03 = new AtomicInteger(0);
    public final AnonymousClass1Lv A04;
    public final AnonymousClass1LH A05;
    public final AnonymousClass1LD A06;

    public /* synthetic */ AnonymousClass1LC(Context context, AnonymousClass2C5 r13, AnonymousClass2C4 r14, List list, int i, int i2, int i3, boolean z, boolean z2) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        this.A01 = newSingleThreadExecutor;
        this.A02 = new AtomicBoolean(false);
        this.A04 = new AnonymousClass1Lv(r13, z2);
        this.A00 = context;
        AnonymousClass1LH r1 = new AnonymousClass1LH();
        this.A05 = r1;
        this.A06 = new AnonymousClass1LD(context, r14, r1);
        newSingleThreadExecutor.execute(new RunnableBRunnable0Shape13S0100000_I0_13(this));
        if (!list.isEmpty()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((AbstractC47642Bu) it.next()).AcI(this);
            }
        }
        for (int i4 = 0; i4 < i; i4++) {
            StringBuilder sb = new StringBuilder("JobConsumer-");
            sb.append(i4);
            new AnonymousClass1L8(sb.toString(), this.A03, this.A04, this.A06, i2, i3, z).start();
        }
    }

    public void A00() {
        this.A01.execute(new RunnableBRunnable0Shape13S0100000_I0_13(this, 48));
    }
}
