package X;

/* renamed from: X.0Oh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05110Oh {
    public AnonymousClass04Q A00;
    public boolean A01 = true;
    public boolean A02;
    public boolean A03;
    public boolean A04;

    public C05110Oh(AnonymousClass04Q r2) {
        this.A00 = r2;
    }

    public void A00() {
        if (this.A01) {
            AnonymousClass04Q r1 = this.A00;
            if (r1.A0N && r1.A0H != null) {
                if (r1.A0G == null) {
                    AnonymousClass0IA r0 = new AnonymousClass0IA(r1);
                    r1.A0G = r0;
                    r1.A0C(r0);
                    return;
                }
                return;
            }
        }
        AnonymousClass04Q r12 = this.A00;
        AnonymousClass0IA r02 = r12.A0G;
        if (r02 != null) {
            r12.A0D(r02);
            r12.A0G = null;
        }
    }
}
