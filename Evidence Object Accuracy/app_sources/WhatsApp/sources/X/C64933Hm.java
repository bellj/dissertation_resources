package X;

import android.text.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.3Hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64933Hm {
    public static C56242kZ A00(C56582lF r3, String str) {
        C13020j0.A01(r3);
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Map A00 = AnonymousClass4ZX.A00(new URI(C12960it.A0c(String.valueOf(str), "?")));
            C56242kZ r1 = new C56242kZ();
            r1.A04 = C12970iu.A0t("utm_content", A00);
            r1.A02 = C12970iu.A0t("utm_medium", A00);
            r1.A00 = C12970iu.A0t("utm_campaign", A00);
            r1.A01 = C12970iu.A0t("utm_source", A00);
            r1.A03 = C12970iu.A0t("utm_term", A00);
            r1.A05 = C12970iu.A0t("utm_id", A00);
            r1.A06 = C12970iu.A0t("anid", A00);
            r1.A07 = C12970iu.A0t("gclid", A00);
            r1.A08 = C12970iu.A0t("dclid", A00);
            r1.A09 = C12970iu.A0t("aclid", A00);
            return r1;
        } catch (URISyntaxException e) {
            r3.A0E("No valid campaign data found", e);
            return null;
        }
    }

    public static String A01(Locale locale) {
        if (locale != null) {
            String language = locale.getLanguage();
            if (!TextUtils.isEmpty(language)) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append(language.toLowerCase(locale));
                if (!TextUtils.isEmpty(locale.getCountry())) {
                    A0h.append("-");
                    A0h.append(locale.getCountry().toLowerCase(locale));
                }
                return A0h.toString();
            }
        }
        return null;
    }

    public static void A02(String str, String str2, Map map) {
        if (str2 != null && !map.containsKey(str)) {
            map.put(str, str2);
        }
    }
}
