package X;

import java.io.Serializable;

/* renamed from: X.3tA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80913tA extends AnonymousClass5DS implements Serializable {
    public static final long serialVersionUID = 0;
    public final Object key;
    public final Object value;

    public C80913tA(Object obj, Object obj2) {
        this.key = obj;
        this.value = obj2;
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public final Object getKey() {
        return this.key;
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public final Object getValue() {
        return this.value;
    }

    @Override // X.AnonymousClass5DS, java.util.Map.Entry
    public final Object setValue(Object obj) {
        throw C12970iu.A0z();
    }
}
