package X;

import com.whatsapp.chatinfo.ContactInfoActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.2Dx  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Dx extends AnonymousClass2BN {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C15550nR A02;
    public final AnonymousClass131 A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final C15600nX A06;
    public final C15370n3 A07;
    public final WeakReference A08;

    public AnonymousClass2Dx(C14900mE r13, C15570nT r14, ContactInfoActivity contactInfoActivity, C15550nR r16, AnonymousClass131 r17, C16590pI r18, AnonymousClass018 r19, AnonymousClass1BB r20, C15600nX r21, C20000v3 r22, C20050v8 r23, C15660nh r24, C242114q r25, C15370n3 r26, C22710zW r27, C17070qD r28) {
        super(r13, contactInfoActivity, r20, r22, r23, r24, r25, r26, r27, r28);
        this.A04 = r18;
        this.A00 = r13;
        this.A08 = new WeakReference(contactInfoActivity);
        this.A01 = r14;
        this.A02 = r16;
        this.A05 = r19;
        this.A07 = r26;
        this.A06 = r21;
        this.A03 = r17;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02f4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02f5, code lost:
        if (r15 != null) goto L_0x02f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02fa, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01f6, code lost:
        r1 = r15.A06;
        r18 = new X.C28181Ma(true);
        r18.A03();
        r19 = ((X.AbstractC21570xd) r1).A00.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x020b, code lost:
        r15 = X.AbstractC21570xd.A03(r19, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "raw_contact_id = ? AND number = ?", null, "CONTACTS", X.C21580xe.A08, new java.lang.String[]{java.lang.String.valueOf(r7.A00), r7.A01});
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x022b, code lost:
        if (r15 != null) goto L_0x0245;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x022d, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("contact-mgr-db/unable to get contact by key ");
        r1.append(r7);
        X.AnonymousClass009.A07(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0241, code lost:
        r19.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0249, code lost:
        if (r15.moveToNext() == false) goto L_0x024f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x024b, code lost:
        r12 = X.AnonymousClass1VW.A00(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x024f, code lost:
        r15.getCount();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0252, code lost:
        r15.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0255, code lost:
        r19.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0258, code lost:
        if (r12 == null) goto L_0x0265;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x025a, code lost:
        r1.A0N(r12, X.AnonymousClass018.A00(r1.A05.A00));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0265, code lost:
        r18.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0268, code lost:
        if (r12 == null) goto L_0x027a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0288 A[Catch: all -> 0x0303, TryCatch #2 {all -> 0x0303, blocks: (B:61:0x0186, B:63:0x018c, B:65:0x0196, B:67:0x019c, B:70:0x01b8, B:71:0x01c2, B:72:0x01d6, B:81:0x01f6, B:85:0x0241, B:91:0x0255, B:93:0x025a, B:94:0x0265, B:96:0x026a, B:98:0x026e, B:99:0x027a, B:100:0x0282, B:102:0x0288, B:104:0x029a, B:106:0x02a0, B:107:0x02a6, B:109:0x02aa, B:111:0x02af, B:113:0x02b5, B:115:0x02bf, B:116:0x02c8, B:118:0x02d0, B:120:0x02db, B:121:0x02ec, B:73:0x01d7, B:74:0x01df, B:76:0x01e5, B:78:0x01f3, B:80:0x01f5, B:82:0x020b, B:90:0x0252, B:126:0x02fa), top: B:133:0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x02a6 A[SYNTHETIC] */
    @Override // X.AnonymousClass2BN
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void A08(java.lang.Void... r29) {
        /*
        // Method dump skipped, instructions count: 776
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Dx.A08(java.lang.Void[]):java.lang.Void");
    }
}
