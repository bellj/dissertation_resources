package X;

/* renamed from: X.2P6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2P6 extends AnonymousClass2P5 {
    public AnonymousClass01N A00;
    public AnonymousClass01N A01;
    public AnonymousClass01N A02;
    public final AnonymousClass2FL A03;
    public final C48722Hj A04;
    public final AnonymousClass2P6 A05 = this;
    public final AnonymousClass01J A06;

    public /* synthetic */ AnonymousClass2P6(AnonymousClass2FL r7, C48722Hj r8, AnonymousClass01J r9) {
        this.A06 = r9;
        this.A04 = r8;
        this.A03 = r7;
        this.A00 = C19970uy.A00(new C50312Pb(r7, r8, this, r9, 0));
        this.A01 = C19970uy.A00(new C50312Pb(r7, r8, this, r9, 1));
        this.A02 = C19970uy.A00(new C50312Pb(r7, r8, this, r9, 2));
    }

    public static AnonymousClass01J A00(Object obj) {
        return ((AnonymousClass2P6) ((AnonymousClass2P5) obj)).A06;
    }

    public final C50362Pg A01() {
        C50322Pc r3 = new C50322Pc();
        AnonymousClass01J r1 = this.A06;
        return new C50362Pg(r3, new C50352Pf((C16590pI) r1.AMg.get(), (AnonymousClass018) r1.ANb.get()));
    }

    public final C50382Pi A02() {
        return new C50382Pi((C14850m9) this.A06.A04.get());
    }

    public final C50512Pv A03() {
        AnonymousClass01J r1 = this.A06;
        C50402Pk r6 = new C50402Pk((C14900mE) r1.A8X.get());
        AnonymousClass19T r2 = (AnonymousClass19T) r1.A2y.get();
        C50432Pn r8 = new C50432Pn((AnonymousClass19Q) r1.A2u.get(), r2, (C16590pI) r1.AMg.get(), (C15650ng) r1.A4m.get());
        C50452Pp r10 = new C50452Pp();
        r10.A00 = (AnonymousClass1AA) r1.ADr.get();
        r10.A02 = (C22710zW) r1.AF7.get();
        r10.A01 = (AnonymousClass18T) r1.AE9.get();
        C50462Pq r12 = new C50462Pq((AbstractC15710nm) r1.A4o.get(), (AnonymousClass18U) r1.AAU.get(), (C253619c) r1.AId.get(), (C14850m9) r1.A04.get());
        C50482Ps r14 = new C50482Ps();
        r14.A00 = (C17000q6) r1.ACv.get();
        r14.A01 = (AnonymousClass018) r1.ANb.get();
        return new C50512Pv(AbstractC17190qP.of((Object) 2, (Object) r6, (Object) 1, (Object) r8, (Object) 3, (Object) r10, (Object) 4, (Object) r12, (Object) 5, (Object) r14));
    }
}
