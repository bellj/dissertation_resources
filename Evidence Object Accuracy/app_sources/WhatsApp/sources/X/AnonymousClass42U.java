package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.text.SeeMoreTextView;

/* renamed from: X.42U  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42U extends C75683kF {
    public final SeeMoreTextView A00;

    public AnonymousClass42U(View view) {
        super(view);
        this.A00 = (SeeMoreTextView) AnonymousClass028.A0D(view, R.id.welcome_message_text);
    }
}
