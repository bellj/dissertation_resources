package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* renamed from: X.2uX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59342uX extends AbstractC75723kJ {
    public final View A00;
    public final WaButton A01;
    public final C53842fM A02;

    public C59342uX(View view, C53842fM r4) {
        super(view);
        this.A00 = view;
        this.A02 = r4;
        WaButton waButton = (WaButton) AnonymousClass028.A0D(view, R.id.button_category_see_all);
        this.A01 = waButton;
        C12960it.A0z(waButton, r4, 0);
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r3) {
        this.A00.setVisibility(C12960it.A02(((C84653zf) r3).A00 ? 1 : 0));
    }
}
