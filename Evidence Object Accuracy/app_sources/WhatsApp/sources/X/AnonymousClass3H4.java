package X;

import java.util.Locale;

/* renamed from: X.3H4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3H4 {
    public int A00;
    public int A01;
    public String A02 = "";
    public String A03;
    public String[] A04;
    public String[] A05;
    public final Locale A06;
    public final Locale A07;

    public AnonymousClass3H4(String str, String str2, Locale locale, Locale locale2) {
        String A01;
        if (!"1".equals(str) || str2.length() != 10 || !"55501".equals(str2.substring(3, 8))) {
            A01 = C22650zQ.A01(str, str2);
        } else {
            A01 = "QQ";
        }
        this.A03 = A01;
        this.A07 = locale;
        this.A06 = locale2;
        A00();
    }

    public AnonymousClass3H4(String str, Locale locale, Locale locale2) {
        this.A03 = str;
        this.A07 = locale;
        this.A06 = locale2;
        A00();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:51:0x0020 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r10v0, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r10v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v3, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00() {
        /*
        // Method dump skipped, instructions count: 276
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3H4.A00():void");
    }
}
