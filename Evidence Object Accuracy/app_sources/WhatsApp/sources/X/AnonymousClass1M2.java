package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1M2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1M2 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final long A04;
    public final long A05;
    public final UserJid A06;
    public final String A07;
    public final String A08;
    public final byte[] A09;

    public AnonymousClass1M2(UserJid userJid, C32141bg r4, String str, String str2, byte[] bArr, int i, int i2, long j) {
        this.A06 = userJid;
        this.A05 = j;
        this.A07 = str;
        this.A08 = str2;
        this.A03 = i;
        this.A09 = bArr;
        this.A02 = i2;
        this.A01 = r4.hostStorage;
        this.A00 = r4.actualActors;
        this.A04 = r4.privacyModeTs;
    }

    public C32141bg A00() {
        return new C32141bg(this.A01, this.A04, this.A00);
    }

    public boolean A01() {
        String str = this.A07;
        return str != null && str.startsWith("ent:");
    }
}
