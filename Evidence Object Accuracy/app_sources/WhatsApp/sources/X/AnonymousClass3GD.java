package X;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;

/* renamed from: X.3GD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GD {
    public static int A00(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public static int A01(Context context, int i) {
        AnonymousClass009.A0E(C12960it.A1S(i));
        int A00 = A00(context);
        AnonymousClass009.A0E(C12960it.A1S(i));
        return Math.round((((float) A00) * ((float) i)) / 100.0f);
    }

    public static int A02(View view) {
        return A01(view.getContext(), 72);
    }
}
