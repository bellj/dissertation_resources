package X;

/* renamed from: X.5Nv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114985Nv extends AbstractC94944cn {
    public AnonymousClass5XI A00 = new AnonymousClass5OC();

    public final byte[] A05(int i) {
        AnonymousClass5XI r7 = this.A00;
        int ACZ = r7.ACZ();
        byte[] bArr = new byte[ACZ];
        byte[] bArr2 = new byte[i];
        int i2 = 0;
        while (true) {
            byte[] bArr3 = this.A01;
            r7.update(bArr3, 0, bArr3.length);
            byte[] bArr4 = this.A02;
            r7.update(bArr4, 0, bArr4.length);
            r7.A97(bArr, 0);
            int i3 = i;
            if (i > ACZ) {
                i3 = ACZ;
            }
            System.arraycopy(bArr, 0, bArr2, i2, i3);
            i2 += i3;
            i -= i3;
            if (i == 0) {
                return bArr2;
            }
            r7.reset();
            r7.update(bArr, 0, ACZ);
        }
    }
}
