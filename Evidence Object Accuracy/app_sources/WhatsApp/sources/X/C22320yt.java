package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

/* renamed from: X.0yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22320yt {
    public final Handler A00;
    public final Handler A01;
    public final HandlerThread A02;
    public final HandlerThread A03;

    public C22320yt() {
        HandlerThread handlerThread = new HandlerThread("Messages Async Commit Thread");
        this.A02 = handlerThread;
        handlerThread.start();
        this.A00 = new AnonymousClass1YQ(handlerThread.getLooper(), this);
        HandlerThread handlerThread2 = new HandlerThread("Receipt Processing Thread");
        this.A03 = handlerThread2;
        handlerThread2.start();
        this.A01 = new AnonymousClass1YR(handlerThread2.getLooper(), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r3.A01.getLooper() == android.os.Looper.myLooper()) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r3 = this;
            android.os.Handler r0 = r3.A00
            android.os.Looper r1 = r0.getLooper()
            android.os.Looper r0 = android.os.Looper.myLooper()
            if (r1 == r0) goto L_0x0019
            android.os.Handler r0 = r3.A01
            android.os.Looper r2 = r0.getLooper()
            android.os.Looper r1 = android.os.Looper.myLooper()
            r0 = 0
            if (r2 != r1) goto L_0x001a
        L_0x0019:
            r0 = 1
        L_0x001a:
            X.AnonymousClass009.A0F(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22320yt.A00():void");
    }

    public void A01(Runnable runnable, int i) {
        if (i < 61) {
            Handler handler = this.A00;
            Message obtain = Message.obtain(handler, runnable);
            obtain.arg1 = i;
            handler.sendMessage(obtain);
            return;
        }
        throw new IllegalArgumentException("Not supported TaskId");
    }
}
