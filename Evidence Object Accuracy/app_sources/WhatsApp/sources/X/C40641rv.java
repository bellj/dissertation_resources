package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1rv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40641rv {
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;

    public C40641rv(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11) {
        this.A01 = j;
        this.A00 = j2;
        this.A08 = j3;
        this.A09 = j4;
        this.A0A = j5;
        this.A04 = j6;
        this.A06 = j7;
        this.A07 = j8;
        this.A02 = j9;
        this.A03 = j10;
        this.A05 = j11;
    }

    public static C40641rv A00(String str) {
        String[] split = str.split(",");
        return new C40641rv(C40601rr.A01(split, 0), C40601rr.A01(split, 1), C40601rr.A01(split, 2), C40601rr.A01(split, 3), C40601rr.A01(split, 4), C40601rr.A01(split, 5), C40601rr.A01(split, 6), C40601rr.A01(split, 7), C40601rr.A01(split, 8), C40601rr.A01(split, 9), C40601rr.A01(split, 10));
    }

    public static String A01(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_voip");
        return sb.toString();
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(Long.valueOf(this.A01), Long.valueOf(this.A00), Long.valueOf(this.A08), Long.valueOf(this.A09), Long.valueOf(this.A0A), Long.valueOf(this.A04), Long.valueOf(this.A06), Long.valueOf(this.A07), Long.valueOf(this.A02), Long.valueOf(this.A03), Long.valueOf(this.A05)));
    }
}
