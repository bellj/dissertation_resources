package X;

import java.util.Comparator;

/* renamed from: X.5CQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass5CQ implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return -(((AbstractC15340mz) obj).A0I > ((AbstractC15340mz) obj2).A0I ? 1 : (((AbstractC15340mz) obj).A0I == ((AbstractC15340mz) obj2).A0I ? 0 : -1));
    }
}
