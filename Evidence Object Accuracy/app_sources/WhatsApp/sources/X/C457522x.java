package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.provider.MediaStore;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.22x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C457522x {
    public boolean A00;
    public final ContentResolver A01;
    public final Handler A02;
    public final AnonymousClass1O4 A03;
    public final Thread A04;
    public final ArrayList A05 = new ArrayList();

    public C457522x(ContentResolver contentResolver, Handler handler, C18720su r7, String str) {
        this.A01 = contentResolver;
        this.A02 = handler;
        AnonymousClass1O4 A02 = r7.A02();
        this.A03 = A02;
        this.A00 = false;
        RunnableBRunnable0Shape6S0100000_I0_6 runnableBRunnable0Shape6S0100000_I0_6 = new RunnableBRunnable0Shape6S0100000_I0_6(this);
        StringBuilder sb = new StringBuilder("image-loader-");
        sb.append(str);
        AnonymousClass1MS r0 = new AnonymousClass1MS(runnableBRunnable0Shape6S0100000_I0_6, sb.toString());
        this.A04 = r0;
        r0.start();
        StringBuilder sb2 = new StringBuilder("imageloader/cachesize:");
        sb2.append(A02.A00.A00());
        Log.i(sb2.toString());
    }

    public void A00() {
        ArrayList arrayList = this.A05;
        synchronized (arrayList) {
            this.A00 = true;
            arrayList.notifyAll();
        }
        AnonymousClass3HO A00 = AnonymousClass3HO.A00();
        Thread thread = this.A04;
        ContentResolver contentResolver = this.A01;
        synchronized (A00) {
            C92224Va A03 = A00.A03(thread);
            A03.A00 = 0;
            BitmapFactory.Options options = A03.A01;
            if (options != null) {
                options.requestCancelDecode();
            }
            A00.notifyAll();
            synchronized (A03) {
                if (A03.A02) {
                    MediaStore.Images.Thumbnails.cancelThumbnailRequest(contentResolver, -1, thread.getId());
                    MediaStore.Video.Thumbnails.cancelThumbnailRequest(contentResolver, -1, thread.getId());
                }
            }
        }
        thread.interrupt();
    }

    public void A01(AnonymousClass23D r4) {
        if (r4 != null) {
            ArrayList arrayList = this.A05;
            synchronized (arrayList) {
                int i = 0;
                while (true) {
                    if (i >= arrayList.size()) {
                        break;
                    } else if (((AnonymousClass23C) arrayList.get(i)).A00 != r4) {
                        i++;
                    } else if (i >= 0) {
                        arrayList.remove(i);
                    }
                }
            }
        }
    }

    public void A02(AnonymousClass23D r4, AnonymousClass23E r5) {
        AnonymousClass009.A0A("Thumb loader reused after destroy", !this.A04.isInterrupted());
        Bitmap bitmap = (Bitmap) this.A03.A00(r4.AH5());
        if (bitmap != null) {
            r5.AWw(bitmap, true);
            return;
        }
        r5.A6L();
        ArrayList arrayList = this.A05;
        synchronized (arrayList) {
            arrayList.add(new AnonymousClass23C(r4, r5));
            arrayList.notifyAll();
        }
    }
}
