package X;

/* renamed from: X.5d4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119195d4 extends ActivityC13810kN {
    public boolean A00 = false;

    public AbstractActivityC119195d4() {
        C117295Zj.A0p(this, 2);
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ActivityC13810kN.A10(ActivityC13830kP.A1M(C117295Zj.A09(this), this), this);
        }
    }
}
