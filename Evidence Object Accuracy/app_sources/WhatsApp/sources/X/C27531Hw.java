package X;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

/* renamed from: X.1Hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27531Hw {
    public static Typeface A00;
    public static Typeface A01;
    public static Boolean A02;

    public static int A00(Context context) {
        float f = context.getResources().getDisplayMetrics().density;
        float f2 = 2.0f;
        if (((float) context.getResources().getDisplayMetrics().widthPixels) / f >= 360.0f) {
            f2 = 7.0f;
        }
        return Math.round(f2 * f);
    }

    public static int A01(Context context) {
        return (int) (context.getResources().getDisplayMetrics().density * 83.333336f);
    }

    public static int A02(Context context, WindowManager windowManager) {
        int i;
        DisplayMetrics displayMetrics;
        if (Build.VERSION.SDK_INT < 30) {
            displayMetrics = new DisplayMetrics();
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            i = displayMetrics.densityDpi;
        } else {
            i = context.getResources().getConfiguration().densityDpi;
            displayMetrics = context.getResources().getDisplayMetrics();
        }
        float f = displayMetrics.density;
        if (i >= 320) {
            return (int) (f * 25.0f);
        }
        if (i < 240) {
            return i >= 160 ? 25 : 19;
        }
        return 38;
    }

    public static Typeface A03(Context context) {
        Typeface typeface = A01;
        if (typeface == null) {
            if (Build.VERSION.SDK_INT >= 21) {
                typeface = Typeface.create("sans-serif-medium", 0);
            } else {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
            }
            A01 = typeface;
        }
        return typeface;
    }

    public static void A04() {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 21) {
            TextPaint textPaint = new TextPaint();
            textPaint.setTextSize(20.0f);
            textPaint.setTypeface(Typeface.DEFAULT);
            if (textPaint.measureText("ABCabc123") != 105.0f) {
                z = false;
            }
        }
        A02 = Boolean.valueOf(z);
    }

    public static void A05(EditText editText) {
        float f = editText.getContext().getResources().getDisplayMetrics().density;
        int i = 6;
        if (f < 1.5f) {
            i = 4;
            if (f >= 1.0f) {
                i = 5;
            }
        }
        editText.setMaxLines(i);
    }

    public static void A06(TextView textView) {
        if (A02 == null) {
            A04();
        }
        if (A02.booleanValue()) {
            textView.setTypeface(A03(textView.getContext()));
            return;
        }
        Typeface typeface = A00;
        boolean z = true;
        if (typeface == null) {
            typeface = Typeface.create(Typeface.DEFAULT, 1);
            A00 = typeface;
        }
        textView.setTypeface(typeface);
        TextPaint paint = textView.getPaint();
        if ((A00.getStyle() & 1) != 0) {
            z = false;
        }
        paint.setFakeBoldText(z);
    }
}
