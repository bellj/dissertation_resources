package X;

/* renamed from: X.3Vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68543Vu implements AnonymousClass2J2 {
    public final /* synthetic */ C44341yl A00;

    public C68543Vu(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J2
    public C59432ui A83(C48122Ek r15, AnonymousClass2K1 r16, AnonymousClass1B4 r17) {
        AnonymousClass01J r1 = this.A00.A03;
        C14850m9 A0S = C12960it.A0S(r1);
        C14900mE A0R = C12970iu.A0R(r1);
        AbstractC15710nm A0Q = C12970iu.A0Q(r1);
        C16590pI A0X = C12970iu.A0X(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        AnonymousClass018 A0R2 = C12960it.A0R(r1);
        C17170qN A0a = C12990iw.A0a(r1);
        return new C59432ui(A0Q, A0R, C12990iw.A0W(r1), r15, r16, r17, (C16340oq) r1.A5z.get(), A0X, A0a, A0R2, A0S, A0T);
    }
}
