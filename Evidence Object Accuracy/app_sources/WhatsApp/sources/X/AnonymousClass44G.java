package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44G  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44G extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AnonymousClass34r A01;

    public AnonymousClass44G(SearchViewModel searchViewModel, AnonymousClass34r r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
