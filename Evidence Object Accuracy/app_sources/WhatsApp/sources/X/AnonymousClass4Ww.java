package X;

/* renamed from: X.4Ww  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Ww {
    public int A00;
    public Object A01;

    public AnonymousClass4Ww(int i, Object obj) {
        this.A00 = i;
        this.A01 = obj;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass4Ww r5 = (AnonymousClass4Ww) obj;
            if (this.A00 == r5.A00) {
                Object obj2 = this.A01;
                Object obj3 = r5.A01;
                if (obj2 != null) {
                    return obj2.equals(obj3);
                }
                if (obj3 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int i = this.A00 * 31;
        Object obj = this.A01;
        return i + (obj != null ? obj.hashCode() : 0);
    }
}
