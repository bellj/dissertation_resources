package X;

import android.view.View;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;

/* renamed from: X.40v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C850240v extends AbstractC75663kD {
    public final CategoryThumbnailLoader A00;
    public final AbstractC16710pd A01;
    public final AbstractC16710pd A02;
    public final AbstractC16710pd A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C850240v(View view, CategoryThumbnailLoader categoryThumbnailLoader) {
        super(view);
        C16700pc.A0E(categoryThumbnailLoader, 2);
        this.A00 = categoryThumbnailLoader;
        this.A03 = new AnonymousClass1WL(new AnonymousClass5JS(view));
        this.A01 = new AnonymousClass1WL(new AnonymousClass5JQ(view));
        this.A02 = new AnonymousClass1WL(new AnonymousClass5JR(view));
    }
}
