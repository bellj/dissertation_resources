package X;

/* renamed from: X.5Gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113155Gj implements AnonymousClass5VU {
    public long A00;
    public final AnonymousClass5VU A01;

    public C113155Gj(AnonymousClass5VU r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass5VU
    public String readLine() {
        long currentTimeMillis = System.currentTimeMillis();
        String readLine = this.A01.readLine();
        this.A00 += System.currentTimeMillis() - currentTimeMillis;
        return readLine;
    }
}
