package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5ph  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124535ph extends Enum {
    public static final /* synthetic */ EnumC124535ph[] A00;
    public static final EnumC124535ph A01;
    public static final EnumC124535ph A02;
    public static final EnumC124535ph A03;

    public static EnumC124535ph valueOf(String str) {
        return (EnumC124535ph) Enum.valueOf(EnumC124535ph.class, str);
    }

    public static EnumC124535ph[] values() {
        return (EnumC124535ph[]) A00.clone();
    }

    static {
        EnumC124535ph r5 = new EnumC124535ph("NONE", 0);
        A02 = r5;
        EnumC124535ph r4 = new EnumC124535ph("ORDER_NOT_FOUND", 1);
        A03 = r4;
        EnumC124535ph r2 = new EnumC124535ph("GET_MERCHANT_STATUS_FAILED", 2);
        A01 = r2;
        EnumC124535ph[] r1 = new EnumC124535ph[3];
        C12990iw.A1P(r5, r4, r1);
        r1[2] = r2;
        A00 = r1;
    }

    public EnumC124535ph(String str, int i) {
    }
}
