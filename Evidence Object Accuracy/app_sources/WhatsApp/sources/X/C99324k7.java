package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4k7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99324k7 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                z2 = C95664e9.A0H(parcel, c, 4, readInt, z2);
            } else {
                z = C12960it.A1S(C95664e9.A02(parcel, readInt));
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78333ok(i, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78333ok[i];
    }
}
