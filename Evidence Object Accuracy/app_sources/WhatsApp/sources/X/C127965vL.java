package X;

import android.content.Context;

/* renamed from: X.5vL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127965vL {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C18650sn A04;
    public final C18610sj A05;

    public C127965vL(Context context, C14900mE r2, C15570nT r3, C14830m7 r4, C18650sn r5, C18610sj r6) {
        this.A03 = r4;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
    }
}
