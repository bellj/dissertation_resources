package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.5l0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122155l0 extends AbstractC118815cQ {
    public final WaImageView A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final C21270x9 A03;

    public C122155l0(View view, C21270x9 r3) {
        super(view);
        this.A03 = r3;
        this.A00 = C12980iv.A0X(view, R.id.contact_photo);
        this.A01 = C12960it.A0N(view, R.id.contact_name);
        this.A02 = C12960it.A0N(view, R.id.reference_id);
    }
}
