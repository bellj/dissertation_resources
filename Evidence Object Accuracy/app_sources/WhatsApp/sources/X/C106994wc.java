package X;

import android.os.Looper;
import android.util.Log;
import java.io.EOFException;

/* renamed from: X.4wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106994wc implements AnonymousClass5X6 {
    public int A00;
    public int A01 = 1000;
    public int A02;
    public int A03;
    public int A04;
    public long A05 = Long.MIN_VALUE;
    public long A06 = Long.MIN_VALUE;
    public long A07 = Long.MIN_VALUE;
    public C100614mC A08;
    public C100614mC A09;
    public C100614mC A0A;
    public C100614mC A0B;
    public AnonymousClass5Q2 A0C;
    public AbstractC14110ks A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H = true;
    public boolean A0I = true;
    public int[] A0J = new int[1000];
    public int[] A0K = new int[1000];
    public int[] A0L = new int[1000];
    public long[] A0M = new long[1000];
    public long[] A0N = new long[1000];
    public C100614mC[] A0O = new C100614mC[1000];
    public AnonymousClass4XD[] A0P = new AnonymousClass4XD[1000];
    public final Looper A0Q;
    public final AnonymousClass4P0 A0R;
    public final AbstractC116865Xf A0S;
    public final C94934cm A0T;
    public final AnonymousClass4P7 A0U = new AnonymousClass4P7();

    public C106994wc(Looper looper, AnonymousClass4P0 r4, AbstractC116865Xf r5, AnonymousClass5VZ r6) {
        this.A0Q = looper;
        this.A0S = r5;
        this.A0R = r4;
        this.A0T = new C94934cm(r6);
    }

    public final int A00(int i, int i2, long j, boolean z) {
        int i3 = -1;
        for (int i4 = 0; i4 < i2; i4++) {
            long[] jArr = this.A0N;
            if (jArr[i] > j) {
                break;
            }
            if (!z || (this.A0J[i] & 1) != 0) {
                i3 = i4;
                if (jArr[i] == j) {
                    break;
                }
            }
            i++;
            if (i == this.A01) {
                i = 0;
            }
        }
        return i3;
    }

    public final long A01(int i) {
        long j = this.A05;
        long j2 = Long.MIN_VALUE;
        if (i != 0) {
            int i2 = this.A04 + (i - 1);
            int i3 = this.A01;
            if (i2 >= i3) {
                i2 -= i3;
            }
            for (int i4 = 0; i4 < i; i4++) {
                j2 = Math.max(j2, this.A0N[i2]);
                if ((this.A0J[i2] & 1) != 0) {
                    break;
                }
                i2--;
                if (i2 == -1) {
                    i2 = i3 - 1;
                }
            }
        }
        this.A05 = Math.max(j, j2);
        int i5 = this.A02 - i;
        this.A02 = i5;
        this.A00 += i;
        int i6 = this.A04 + i;
        this.A04 = i6;
        int i7 = this.A01;
        if (i6 >= i7) {
            i6 -= i7;
            this.A04 = i6;
        }
        int i8 = this.A03 - i;
        this.A03 = i8;
        if (i8 < 0) {
            this.A03 = 0;
        }
        if (i5 != 0) {
            return this.A0M[i6];
        }
        if (i6 != 0) {
            i7 = i6;
        }
        int i9 = i7 - 1;
        return this.A0M[i9] + ((long) this.A0K[i9]);
    }

    public final void A02() {
        long j;
        C94934cm r3 = this.A0T;
        synchronized (this) {
            int i = this.A02;
            if (i == 0) {
                j = -1;
            } else {
                j = A01(i);
            }
        }
        r3.A03(j);
    }

    public final void A03(C100614mC r7, C89864Lr r8) {
        boolean z;
        C112295Cv r4;
        C106604vy r1;
        Class<C106634w1> cls;
        C100614mC r0 = this.A08;
        if (r0 == null) {
            z = true;
            r4 = null;
        } else {
            z = false;
            r4 = r0.A0K;
        }
        this.A08 = r7;
        C112295Cv r3 = r7.A0K;
        AbstractC116865Xf r2 = this.A0S;
        if (r2 != null) {
            if (r3 != null) {
                cls = C106634w1.class;
            } else {
                cls = null;
            }
            C93844ap r02 = new C93844ap(r7);
            r02.A0L = cls;
            r7 = new C100614mC(r02);
        }
        r8.A00 = r7;
        r8.A01 = this.A0C;
        if (r2 == null) {
            return;
        }
        if (z || !AnonymousClass3JZ.A0H(r4, r3)) {
            if (r3 == null) {
                r1 = null;
            } else {
                r1 = new C106604vy(new C867148n(new C87544Bx()));
            }
            this.A0C = r1;
            r8.A01 = r1;
        }
    }

    public void A04(boolean z) {
        C94934cm r5 = this.A0T;
        AnonymousClass4SR r6 = r5.A01;
        if (r6.A02) {
            AnonymousClass4SR r0 = r5.A03;
            int i = (r0.A02 ? 1 : 0) + (((int) (r0.A04 - r6.A04)) / r5.A04);
            AnonymousClass4IL[] r3 = new AnonymousClass4IL[i];
            for (int i2 = 0; i2 < i; i2++) {
                r3[i2] = r6.A01;
                r6.A01 = null;
                AnonymousClass4SR r02 = r6.A00;
                r6.A00 = null;
                r6 = r02;
            }
            r5.A05.Aa5(r3);
        }
        AnonymousClass4SR r03 = new AnonymousClass4SR(0, r5.A04);
        r5.A01 = r03;
        r5.A02 = r03;
        r5.A03 = r03;
        r5.A00 = 0;
        r5.A05.Af6();
        this.A02 = 0;
        this.A00 = 0;
        this.A04 = 0;
        this.A03 = 0;
        this.A0I = true;
        this.A07 = Long.MIN_VALUE;
        this.A05 = Long.MIN_VALUE;
        this.A06 = Long.MIN_VALUE;
        this.A0E = false;
        this.A0A = null;
        if (z) {
            this.A09 = null;
            this.A0B = null;
            this.A0H = true;
        }
    }

    public final synchronized boolean A05(long j, boolean z) {
        int A00;
        this.A03 = 0;
        C94934cm r1 = this.A0T;
        r1.A02 = r1.A01;
        int i = this.A04 + 0;
        int i2 = this.A01;
        if (i >= i2) {
            i -= i2;
        }
        int i3 = this.A02;
        if (!C12980iv.A1V(0, i3) || j < this.A0N[i] || ((j > this.A06 && !z) || (A00 = A00(i, i3 - 0, j, true)) == -1)) {
            return false;
        }
        this.A07 = j;
        this.A03 = 0 + A00;
        return true;
    }

    public synchronized boolean A06(boolean z) {
        C100614mC r1;
        int i = this.A03;
        boolean z2 = true;
        if (C12980iv.A1V(i, this.A02)) {
            int i2 = this.A04 + i;
            int i3 = this.A01;
            if (i2 >= i3) {
                i2 -= i3;
            }
            if (this.A0O[i2] == this.A08) {
                boolean z3 = true;
                if (this.A0C != null) {
                    z3 = false;
                }
                return z3;
            }
        } else if (!z && !this.A0E && ((r1 = this.A0B) == null || r1 == this.A08)) {
            z2 = false;
        }
        return z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0070, code lost:
        if (r1 != 16) goto L_0x0072;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006d  */
    @Override // X.AnonymousClass5X6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AA6(X.C100614mC r7) {
        /*
            r6 = this;
            r4 = 0
            r6.A09 = r7
            r5 = r6
            monitor-enter(r5)
            r6.A0H = r4     // Catch: all -> 0x0086
            X.4mC r0 = r6.A0B     // Catch: all -> 0x0086
            boolean r0 = X.AnonymousClass3JZ.A0H(r7, r0)     // Catch: all -> 0x0086
            if (r0 == 0) goto L_0x0012
            monitor-exit(r5)
            r1 = 0
            goto L_0x0076
        L_0x0012:
            X.4mC r0 = r6.A0A     // Catch: all -> 0x0086
            boolean r0 = X.AnonymousClass3JZ.A0H(r7, r0)     // Catch: all -> 0x0086
            if (r0 == 0) goto L_0x001c
            X.4mC r7 = r6.A0A     // Catch: all -> 0x0086
        L_0x001c:
            r6.A0B = r7     // Catch: all -> 0x0086
            java.lang.String r3 = r7.A0T     // Catch: all -> 0x0086
            java.lang.String r1 = r7.A0O     // Catch: all -> 0x0086
            r2 = 0
            if (r3 == 0) goto L_0x002c
            int r0 = r3.hashCode()     // Catch: all -> 0x0086
            switch(r0) {
                case -2123537834: goto L_0x0065;
                case -432837260: goto L_0x0062;
                case -432837259: goto L_0x005f;
                case -53558318: goto L_0x0046;
                case 187078296: goto L_0x0043;
                case 187094639: goto L_0x0040;
                case 1504578661: goto L_0x003d;
                case 1504619009: goto L_0x003a;
                case 1504831518: goto L_0x0037;
                case 1903231877: goto L_0x0034;
                case 1903589369: goto L_0x0031;
                default: goto L_0x002c;
            }     // Catch: all -> 0x0086
        L_0x002c:
            r6.A0G = r2     // Catch: all -> 0x0086
            r6.A0F = r4     // Catch: all -> 0x0086
            goto L_0x0074
        L_0x0031:
            java.lang.String r0 = "audio/g711-mlaw"
            goto L_0x0067
        L_0x0034:
            java.lang.String r0 = "audio/g711-alaw"
            goto L_0x0067
        L_0x0037:
            java.lang.String r0 = "audio/mpeg"
            goto L_0x0067
        L_0x003a:
            java.lang.String r0 = "audio/flac"
            goto L_0x0067
        L_0x003d:
            java.lang.String r0 = "audio/eac3"
            goto L_0x0067
        L_0x0040:
            java.lang.String r0 = "audio/raw"
            goto L_0x0067
        L_0x0043:
            java.lang.String r0 = "audio/ac3"
            goto L_0x0067
        L_0x0046:
            java.lang.String r0 = "audio/mp4a-latm"
            boolean r0 = r3.equals(r0)     // Catch: all -> 0x0086
            if (r0 == 0) goto L_0x002c
            if (r1 == 0) goto L_0x002c
            X.4MD r0 = X.C95554dx.A01(r1)     // Catch: all -> 0x0086
            if (r0 == 0) goto L_0x002c
            int r0 = r0.A00     // Catch: all -> 0x0086
            int r1 = X.C94674cK.A00(r0)     // Catch: all -> 0x0086
            if (r1 == 0) goto L_0x002c
            goto L_0x006e
        L_0x005f:
            java.lang.String r0 = "audio/mpeg-L2"
            goto L_0x0067
        L_0x0062:
            java.lang.String r0 = "audio/mpeg-L1"
            goto L_0x0067
        L_0x0065:
            java.lang.String r0 = "audio/eac3-joc"
        L_0x0067:
            boolean r0 = r3.equals(r0)     // Catch: all -> 0x0086
            if (r0 != 0) goto L_0x0072
            goto L_0x002c
        L_0x006e:
            r0 = 16
            if (r1 == r0) goto L_0x002c
        L_0x0072:
            r2 = 1
            goto L_0x002c
        L_0x0074:
            monitor-exit(r5)
            r1 = 1
        L_0x0076:
            X.0ks r0 = r6.A0D
            if (r0 == 0) goto L_0x0085
            if (r1 == 0) goto L_0x0085
            X.0kn r0 = (X.C14060kn) r0
            android.os.Handler r1 = r0.A0O
            java.lang.Runnable r0 = r0.A0Z
            r1.post(r0)
        L_0x0085:
            return
        L_0x0086:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106994wc.AA6(X.4mC):void");
    }

    @Override // X.AnonymousClass5X6
    public /* synthetic */ void AbC(C95304dT r2, int i) {
        AbD(r2, i, 0);
    }

    @Override // X.AnonymousClass5X6
    public final void AbD(C95304dT r8, int i, int i2) {
        C94934cm r6 = this.A0T;
        while (i > 0) {
            int A02 = r6.A02(i);
            AnonymousClass4SR r1 = r6.A03;
            r8.A0V(r1.A01.A00, ((int) (r6.A00 - r1.A04)) + 0, A02);
            i -= A02;
            long j = r6.A00 + ((long) A02);
            r6.A00 = j;
            AnonymousClass4SR r5 = r6.A03;
            if (j == r5.A03) {
                r6.A03 = r5.A00;
            }
        }
    }

    @Override // X.AnonymousClass5X6
    public final int AbF(AnonymousClass2BY r9, int i, int i2, boolean z) {
        C94934cm r7 = this.A0T;
        int A02 = r7.A02(i);
        AnonymousClass4SR r1 = r7.A03;
        int read = r9.read(r1.A01.A00, ((int) (r7.A00 - r1.A04)) + 0, A02);
        if (read != -1) {
            long j = r7.A00 + ((long) read);
            r7.A00 = j;
            AnonymousClass4SR r3 = r7.A03;
            if (j != r3.A03) {
                return read;
            }
            r7.A03 = r3.A00;
            return read;
        } else if (z) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    @Override // X.AnonymousClass5X6
    public void AbG(AnonymousClass4XD r15, int i, int i2, int i3, long j) {
        int i4 = i & 1;
        boolean A1S = C12960it.A1S(i4);
        if (this.A0I) {
            if (A1S) {
                this.A0I = false;
            } else {
                return;
            }
        }
        long j2 = 0 + j;
        if (this.A0G) {
            if (j2 < this.A07) {
                return;
            }
            if (i4 == 0) {
                if (!this.A0F) {
                    Log.w("SampleQueue", C12970iu.A0s(this.A0B, C12960it.A0k("Overriding unexpected non-sync sample for format: ")));
                    this.A0F = true;
                }
                i |= 1;
            }
        }
        long j3 = (this.A0T.A00 - ((long) i2)) - ((long) i3);
        synchronized (this) {
            int i5 = this.A02;
            if (i5 > 0) {
                int i6 = this.A04 + (i5 - 1);
                int i7 = this.A01;
                if (i6 >= i7) {
                    i6 -= i7;
                }
                boolean z = false;
                if (this.A0M[i6] + ((long) this.A0K[i6]) <= j3) {
                    z = true;
                }
                C95314dV.A03(z);
            }
            this.A0E = C12960it.A1S(536870912 & i);
            this.A06 = Math.max(this.A06, j2);
            int i8 = this.A02;
            int i9 = this.A04;
            int i10 = i9 + i8;
            int i11 = this.A01;
            if (i10 >= i11) {
                i10 -= i11;
            }
            this.A0N[i10] = j2;
            long[] jArr = this.A0M;
            jArr[i10] = j3;
            this.A0K[i10] = i2;
            this.A0J[i10] = i;
            this.A0P[i10] = r15;
            C100614mC[] r1 = this.A0O;
            C100614mC r4 = this.A0B;
            r1[i10] = r4;
            this.A0L[i10] = 0;
            this.A0A = r4;
            int i12 = i8 + 1;
            this.A02 = i12;
            if (i12 == i11) {
                int i13 = i11 + 1000;
                int[] iArr = new int[i13];
                long[] jArr2 = new long[i13];
                long[] jArr3 = new long[i13];
                int[] iArr2 = new int[i13];
                int[] iArr3 = new int[i13];
                AnonymousClass4XD[] r2 = new AnonymousClass4XD[i13];
                C100614mC[] r12 = new C100614mC[i13];
                int i14 = i11 - i9;
                System.arraycopy(jArr, i9, jArr2, 0, i14);
                System.arraycopy(this.A0N, this.A04, jArr3, 0, i14);
                System.arraycopy(this.A0J, this.A04, iArr2, 0, i14);
                System.arraycopy(this.A0K, this.A04, iArr3, 0, i14);
                System.arraycopy(this.A0P, this.A04, r2, 0, i14);
                System.arraycopy(this.A0O, this.A04, r12, 0, i14);
                System.arraycopy(this.A0L, this.A04, iArr, 0, i14);
                int i15 = this.A04;
                System.arraycopy(this.A0M, 0, jArr2, i14, i15);
                System.arraycopy(this.A0N, 0, jArr3, i14, i15);
                System.arraycopy(this.A0J, 0, iArr2, i14, i15);
                System.arraycopy(this.A0K, 0, iArr3, i14, i15);
                System.arraycopy(this.A0P, 0, r2, i14, i15);
                System.arraycopy(this.A0O, 0, r12, i14, i15);
                System.arraycopy(this.A0L, 0, iArr, i14, i15);
                this.A0M = jArr2;
                this.A0N = jArr3;
                this.A0J = iArr2;
                this.A0K = iArr3;
                this.A0P = r2;
                this.A0O = r12;
                this.A0L = iArr;
                this.A04 = 0;
                this.A01 = i13;
            }
        }
    }
}
