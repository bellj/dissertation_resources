package X;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.security.auth.Destroyable;

/* renamed from: X.20B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20B implements Destroyable {
    public final AtomicBoolean A00 = new AtomicBoolean(false);
    public final byte[] A01;
    public final byte[] A02;

    public AnonymousClass20B(byte[] bArr, byte[] bArr2) {
        this.A02 = bArr;
        this.A01 = bArr2;
    }

    public static AnonymousClass20B A00() {
        C90604On A01 = C32001bS.A00().A01();
        return new AnonymousClass20B(A01.A01, A01.A00);
    }

    @Override // javax.security.auth.Destroyable
    public void destroy() {
        if (this.A00.compareAndSet(false, true)) {
            Arrays.fill(this.A01, (byte) 0);
            Arrays.fill(this.A02, (byte) 0);
        }
    }
}
