package X;

/* renamed from: X.3Be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63333Be {
    public final long A00;
    public final long A01;
    public final C37891nB A02;
    public final C37951nH A03;
    public final C28481Nj A04;
    public final String A05;
    public final boolean A06;
    public final boolean A07;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
        if (r11 != null) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C63333Be(X.C37891nB r7, X.C37951nH r8, X.C14370lK r9, X.C28481Nj r10, java.io.File r11, java.lang.String r12, long r13, boolean r15, boolean r16, boolean r17) {
        /*
            r6 = this;
            r6.<init>()
            r6.A05 = r12
            r6.A00 = r13
            r6.A02 = r7
            r6.A06 = r15
            r6.A03 = r8
            r0 = r16
            r6.A07 = r0
            r6.A04 = r10
            r1 = 0
            int r0 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0022
            if (r11 != 0) goto L_0x0024
            java.lang.String r0 = "Must provide file for upload continuation"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0022:
            if (r11 == 0) goto L_0x004a
        L_0x0024:
            X.0lK r0 = X.C14370lK.A0B
            if (r9 == r0) goto L_0x0038
            X.0lK r0 = X.C14370lK.A05
            if (r9 == r0) goto L_0x0038
            X.0lK r0 = X.C14370lK.A0X
            if (r9 == r0) goto L_0x0038
            X.0lK r0 = X.C14370lK.A08
            if (r9 == r0) goto L_0x0038
            X.0lK r0 = X.C14370lK.A0S
            if (r9 != r0) goto L_0x004a
        L_0x0038:
            long r4 = r11.length()
            if (r17 == 0) goto L_0x0047
            r2 = 16
            long r0 = r4 % r2
            long r4 = r4 - r0
            long r4 = r4 + r2
            r0 = 10
            long r4 = r4 + r0
        L_0x0047:
            r6.A01 = r4
            return
        L_0x004a:
            r4 = -1
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63333Be.<init>(X.1nB, X.1nH, X.0lK, X.1Nj, java.io.File, java.lang.String, long, boolean, boolean, boolean):void");
    }
}
