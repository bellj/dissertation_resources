package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.profile.WebImagePicker;

/* renamed from: X.3WZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WZ implements AbstractC32851cq {
    public final /* synthetic */ WebImagePicker A00;

    public AnonymousClass3WZ(WebImagePicker webImagePicker) {
        this.A00 = webImagePicker;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        WebImagePicker webImagePicker = this.A00;
        C14900mE r2 = ((ActivityC13810kN) webImagePicker).A05;
        boolean A00 = C14950mJ.A00();
        int i = R.string.need_sd_card_shared_storage;
        if (A00) {
            i = R.string.need_sd_card;
        }
        r2.A07(i, 1);
        webImagePicker.finish();
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        WebImagePicker webImagePicker = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_web_image_picking_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_web_image_picking;
        }
        RequestPermissionActivity.A0K(webImagePicker, R.string.permission_storage_need_write_access_on_web_image_picking_request, i2);
    }
}
