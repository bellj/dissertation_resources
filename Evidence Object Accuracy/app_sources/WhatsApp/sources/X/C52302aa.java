package X;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52302aa extends URLSpan implements AbstractC116465Vn {
    public int A00;
    public int A01;
    public boolean A02;

    public C52302aa(Context context, String str) {
        super(str);
        int A00 = AnonymousClass00T.A00(context, R.color.link_color);
        this.A01 = A00;
        this.A00 = C016907y.A06(A00, 72);
    }

    @Override // X.AbstractC116465Vn
    public void AXY(MotionEvent motionEvent, View view) {
        boolean z = true;
        if (motionEvent.getAction() == 1 && this.A02) {
            onClick(view);
        }
        if (motionEvent.getAction() != 0) {
            z = false;
        }
        this.A02 = z;
        view.invalidate();
    }

    @Override // android.text.style.CharacterStyle, android.text.style.ClickableSpan
    public void updateDrawState(TextPaint textPaint) {
        int i;
        super.updateDrawState(textPaint);
        textPaint.setColor(this.A01);
        if (this.A02) {
            i = this.A00;
        } else {
            i = 0;
        }
        textPaint.bgColor = i;
        textPaint.setUnderlineText(false);
    }
}
