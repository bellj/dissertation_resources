package X;

import android.text.Editable;
import com.whatsapp.notification.PopupNotification;

/* renamed from: X.361  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass361 extends C469928m {
    public final /* synthetic */ PopupNotification A00;

    public AnonymousClass361(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        PopupNotification popupNotification = this.A00;
        popupNotification.A13.A01(true);
        String obj = editable.toString();
        C15370n3 r0 = popupNotification.A0p;
        if (r0 != null) {
            int length = obj.length();
            C16170oZ r2 = popupNotification.A0O;
            AbstractC14640lm A02 = C15370n3.A02(r0);
            if (length != 0) {
                r2.A0B(A02, 0);
            } else {
                r2.A09(A02);
            }
        }
        C42971wC.A06(popupNotification, popupNotification.A0c.getPaint(), editable, ((ActivityC13810kN) popupNotification).A08, ((ActivityC13810kN) popupNotification).A0B, popupNotification.A12);
        boolean z = !AnonymousClass1US.A0C(obj);
        popupNotification.A0B.setEnabled(z);
        if (popupNotification.A0C.getVisibility() == 8 && !z) {
            C469928m.A00(popupNotification.A0C, true);
            popupNotification.A0C.setVisibility(0);
            C469928m.A00(popupNotification.A0B, false);
            popupNotification.A0B.setVisibility(8);
        } else if (popupNotification.A0C.getVisibility() == 0 && z) {
            C469928m.A00(popupNotification.A0C, false);
            popupNotification.A0C.setVisibility(8);
            C469928m.A00(popupNotification.A0B, true);
            popupNotification.A0B.setVisibility(0);
        }
    }
}
