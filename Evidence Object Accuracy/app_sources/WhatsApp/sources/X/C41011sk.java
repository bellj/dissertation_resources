package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0220000_I0;
import com.whatsapp.util.Log;

/* renamed from: X.1sk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41011sk implements AbstractC21730xt {
    public AnonymousClass1JO A00;
    public final C41001sj A01;
    public final C17220qS A02;

    public C41011sk(C41001sj r1, C17220qS r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.A00(this.A00, -1);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        this.A01.A00(this.A00, C41151sz.A00(r4));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        C41001sj r1 = this.A01;
        AnonymousClass1JO r4 = this.A00;
        Log.i("companion-device-manager/createDeviceRemoveRequestProtocolHelper/onSuccess");
        C22100yW r3 = r1.A00;
        r3.A0M.execute(new RunnableBRunnable0Shape0S0220000_I0(r4, r3, r1.A02));
    }
}
