package X;

import java.util.List;

/* renamed from: X.3CS  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CS {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3CS(String str) {
        C41141sy r1 = new C41141sy("iq");
        C41141sy.A02(r1, "get", str);
        this.A00 = r1.A03();
    }

    public void A00(C41141sy r3, List list) {
        r3.A09(this.A00, list, C12960it.A0l());
    }
}
