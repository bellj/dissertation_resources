package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;

/* renamed from: X.3j9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C75013j9 extends AbstractC05270Ox {
    public final /* synthetic */ CatalogSearchFragment A00;

    public C75013j9(CatalogSearchFragment catalogSearchFragment) {
        this.A00 = catalogSearchFragment;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i2 >= 0) {
            this.A00.A1B();
        }
        this.A00.A1C();
    }
}
