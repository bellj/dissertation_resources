package X;

import java.util.Collections;
import java.util.Map;

/* renamed from: X.4dO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95254dO {
    public static final C95254dO A01 = new C95254dO(true);
    public static volatile C95254dO A02;
    public static volatile C95254dO A03;
    public final Map A00;

    public C95254dO() {
        this.A00 = C12970iu.A11();
    }

    public C95254dO(boolean z) {
        this.A00 = Collections.emptyMap();
    }

    public static C95254dO A00() {
        C95254dO r0;
        C95254dO r02 = A02;
        if (r02 != null) {
            return r02;
        }
        synchronized (C95254dO.class) {
            r0 = A02;
            if (r0 == null) {
                r0 = A01;
                A02 = r0;
            }
        }
        return r0;
    }
}
