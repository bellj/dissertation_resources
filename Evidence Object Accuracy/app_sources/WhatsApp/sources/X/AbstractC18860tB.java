package X;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0211000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.conversation.ConversationListView;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.conversation.conversationrow.message.KeptMessagesActivity;
import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import com.whatsapp.gallery.GalleryFragmentBase;
import com.whatsapp.gallery.MediaGalleryFragment;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.jid.UserJid;
import com.whatsapp.media.transcode.MediaTranscodeService;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.notification.PopupNotificationViewPager;
import com.whatsapp.report.ReportActivity;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.status.playback.MyStatusesActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/* renamed from: X.0tB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18860tB {
    public void A00(AbstractC14640lm r12, Collection collection, Map map, boolean z) {
        AnonymousClass02M r0;
        AbstractC16130oV r02;
        List list;
        if (this instanceof C35241hV) {
            StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = ((C35241hV) this).A00;
            C35591iL r6 = (C35591iL) ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A0H;
            if (r6 != null && collection != null) {
                int count = r6.getCount();
                Iterator it = collection.iterator();
                boolean z2 = false;
                while (it.hasNext()) {
                    AbstractC15340mz r4 = (AbstractC15340mz) it.next();
                    int i = 0;
                    while (true) {
                        if (i < count) {
                            AbstractC35601iM r2 = (AbstractC35601iM) r6.A06.get(Integer.valueOf(i));
                            if (r2 != null && (r02 = r2.A03) != null && r02.A0z.equals(r4.A0z)) {
                                r2.A03.A0v = z;
                                z2 = true;
                                break;
                            }
                            i++;
                        } else {
                            break;
                        }
                    }
                }
                if (z2) {
                    r0 = ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A06;
                } else {
                    return;
                }
            } else {
                return;
            }
        } else if (this instanceof C35151hK) {
            ((C35151hK) this).A00.A0B.A0B(collection);
            return;
        } else if (this instanceof C35171hM) {
            AnonymousClass02P r42 = ((C35171hM) this).A00.A05;
            C35531iB r3 = (C35531iB) r42.A01();
            if (r3 != null && collection != null) {
                if (z) {
                    r3.A04.removeAll(collection);
                    list = r3.A03;
                } else {
                    r3.A03.removeAll(collection);
                    list = r3.A04;
                }
                TreeSet treeSet = new TreeSet(C35621iO.A00);
                treeSet.addAll(list);
                treeSet.addAll(collection);
                list.clear();
                list.addAll(treeSet);
                r42.A0A(r3);
                return;
            }
            return;
        } else if (this instanceof C35301hb) {
            C35301hb r32 = (C35301hb) this;
            if (collection != null && !collection.isEmpty()) {
                Iterator it2 = collection.iterator();
                while (it2.hasNext()) {
                    if (r32.A01.equals(((AbstractC15340mz) it2.next()).A0z.A00)) {
                    }
                }
                return;
            } else if (r12 != null && !r32.A01.equals(r12)) {
                return;
            }
            r32.A00.A00.A34();
            return;
        } else if (this instanceof C35311hc) {
            MediaGalleryFragment mediaGalleryFragment = ((C35311hc) this).A00;
            C35591iL r33 = (C35591iL) ((MediaGalleryFragmentBase) mediaGalleryFragment).A0H;
            if (r33 != null) {
                if (collection != null && !collection.isEmpty()) {
                    Iterator it3 = collection.iterator();
                    while (it3.hasNext()) {
                        AbstractC14640lm r1 = ((AbstractC15340mz) it3.next()).A0z.A00;
                        if (r1 == null || !r1.equals(mediaGalleryFragment.A03)) {
                        }
                    }
                    return;
                } else if (r12 != null && !r12.equals(mediaGalleryFragment.A03)) {
                    return;
                }
                r33.AaX();
                r0 = ((MediaGalleryFragmentBase) mediaGalleryFragment).A06;
            } else {
                return;
            }
        } else if (this instanceof C35321hd) {
            C35321hd r43 = (C35321hd) this;
            if (collection != null && !collection.isEmpty()) {
                Iterator it4 = collection.iterator();
                while (it4.hasNext()) {
                    AbstractC14640lm r22 = ((AbstractC15340mz) it4.next()).A0z.A00;
                    if (r22 != null) {
                        GalleryFragmentBase galleryFragmentBase = r43.A00;
                        if (r22.equals(galleryFragmentBase.A0D)) {
                            galleryFragmentBase.A1B();
                            return;
                        }
                    }
                }
                return;
            } else if (r12 == null || r12.equals(r43.A00.A0D)) {
                r43.A00.A1B();
                return;
            } else {
                return;
            }
        } else if (this instanceof C35331he) {
            C35331he r13 = (C35331he) this;
            if (collection != null) {
                if (collection.isEmpty()) {
                    return;
                }
            } else if (map != null) {
                return;
            }
            r13.A00.A2o();
            return;
        } else if (this instanceof C35181hO) {
            C35181hO r23 = (C35181hO) this;
            Iterator it5 = collection.iterator();
            while (it5.hasNext()) {
                r23.A03((AbstractC15340mz) it5.next());
            }
            return;
        } else if (this instanceof C35361hh) {
            C35361hh r5 = (C35361hh) this;
            if (collection != null) {
                boolean z3 = false;
                Iterator it6 = collection.iterator();
                while (it6.hasNext()) {
                    MediaAlbumActivity mediaAlbumActivity = r5.A00;
                    C35421hp r03 = mediaAlbumActivity.A08;
                    AnonymousClass1IS r24 = ((AbstractC15340mz) it6.next()).A0z;
                    List list2 = r03.A00;
                    if (list2 != null) {
                        Iterator it7 = list2.iterator();
                        while (true) {
                            if (it7.hasNext()) {
                                if (((AbstractC15340mz) it7.next()).A0z.equals(r24)) {
                                    mediaAlbumActivity.A0S.add(r24);
                                    z3 = true;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
                if (z3) {
                    r5.A00.A08.notifyDataSetChanged();
                    return;
                }
                return;
            }
            return;
        } else if (this instanceof C35381hj) {
            C35381hj r44 = (C35381hj) this;
            if (collection != null && !collection.isEmpty()) {
                Iterator it8 = collection.iterator();
                while (it8.hasNext()) {
                    ListChatInfo listChatInfo = r44.A00;
                    if (listChatInfo.A2u().equals(((AbstractC15340mz) it8.next()).A0z.A00)) {
                        listChatInfo.A2z();
                        return;
                    }
                }
                return;
            } else if (r12 == null || r12.equals(r44.A00.A2u())) {
                r44.A00.A2z();
                return;
            } else {
                return;
            }
        } else if (this instanceof C35391hk) {
            C35391hk r45 = (C35391hk) this;
            if (collection != null && !collection.isEmpty()) {
                Iterator it9 = collection.iterator();
                while (it9.hasNext()) {
                    ContactInfoActivity contactInfoActivity = r45.A00;
                    if (contactInfoActivity.A2v().equals(((AbstractC15340mz) it9.next()).A0z.A00)) {
                        contactInfoActivity.A31();
                        return;
                    }
                }
                return;
            } else if (r12 == null || r45.A00.A2v().equals(r12)) {
                r45.A00.A31();
                return;
            } else {
                return;
            }
        } else if (this instanceof C22520zD) {
            C22520zD r62 = (C22520zD) this;
            if (map != null) {
                HashMap hashMap = new HashMap();
                Iterator it10 = collection.iterator();
                while (it10.hasNext()) {
                    AbstractC15340mz r25 = (AbstractC15340mz) it10.next();
                    AbstractC14640lm r14 = r25.A0z.A00;
                    Collection collection2 = (Collection) hashMap.get(r14);
                    if (collection2 == null) {
                        collection2 = new ArrayList();
                        hashMap.put(r14, collection2);
                    }
                    collection2.add(r25);
                }
                r62.A0x.Ab2(new RunnableBRunnable0Shape0S0310000_I0(r62, hashMap, map, 0, z));
                return;
            }
            return;
        } else if (this instanceof C35141hJ) {
            Conversation conversation = ((C35141hJ) this).A00;
            C15350n0 conversationCursorAdapter = conversation.A1g.getConversationCursorAdapter();
            if (collection == null || collection.isEmpty()) {
                conversationCursorAdapter.A01++;
            } else {
                Iterator it11 = collection.iterator();
                while (it11.hasNext()) {
                    conversationCursorAdapter.A0T.add(((AbstractC15340mz) it11.next()).A0z);
                }
            }
            conversation.A20.A07(conversation.A2o(), conversation.A03);
            return;
        } else {
            return;
        }
        r0.A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0072, code lost:
        if (r11.A0y == 5) goto L_0x0074;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AbstractC15340mz r11, int r12) {
        /*
        // Method dump skipped, instructions count: 527
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18860tB.A01(X.0mz, int):void");
    }

    public void A02(AbstractC15340mz r15, int i) {
        C16150oX r1;
        AbstractC14640lm r0;
        View findViewWithTag;
        StickerView stickerView;
        AnonymousClass1OY r12;
        byte b;
        if (this instanceof C35121hH) {
            C35121hH r13 = (C35121hH) this;
            if (C15380n4.A0N(r15.A0z.A00) && i == 12) {
                StatusesViewModel.A01(UserJid.of(r15.A0B()), r13.A00);
            }
        } else if (this instanceof C35671iU) {
            C35671iU r3 = (C35671iU) this;
            if (r15 != null) {
                AnonymousClass1IS r2 = r15.A0z;
                C35681iV r14 = r3.A00;
                if (r2.equals(((AbstractC35561iG) r14).A09.A0z) && r2.A02) {
                    r14.A02.A02.post(new RunnableBRunnable0Shape0S0201000_I0(r3, r15, i, 31));
                }
            }
        } else if (this instanceof C35261hX) {
            C35261hX r32 = (C35261hX) this;
            if (r15 != null) {
                AnonymousClass1IS r22 = r15.A0z;
                C35551iF r16 = r32.A01;
                if (r22.equals(((AbstractC35561iG) r16).A09.A0z) && !r22.A02) {
                    ((AbstractC33621eg) r16).A0A.A02.post(new RunnableBRunnable0Shape0S0201000_I0(r32, r15, i, 30));
                }
            }
        } else if (this instanceof C35271hY) {
            StatusPlaybackContactFragment statusPlaybackContactFragment = ((C35271hY) this).A00;
            if (!(statusPlaybackContactFragment.A0b == null || statusPlaybackContactFragment.A0P != C29831Uv.A00 || r15 == null)) {
                AnonymousClass1IS r5 = r15.A0z;
                if (C15380n4.A0N(r5.A00) && r5.A02) {
                    C33651en r17 = ((StatusPlaybackBaseFragment) statusPlaybackContactFragment).A03;
                    AnonymousClass009.A06(r17, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
                    Set set = r17.A0E.A08;
                    set.clear();
                    int i2 = 0;
                    for (AbstractC15340mz r23 : statusPlaybackContactFragment.A0b) {
                        if ((r23 instanceof AbstractC16130oV) && (r1 = ((AbstractC16130oV) r23).A02) != null && !r1.A0P && !r1.A0a) {
                            set.add(Integer.valueOf(i2));
                        }
                        if (i2 == statusPlaybackContactFragment.A00 && r23.A0z.equals(r5)) {
                            statusPlaybackContactFragment.A1O(r15);
                        }
                        i2++;
                    }
                }
            }
        } else if (this instanceof C35281hZ) {
            C35281hZ r24 = (C35281hZ) this;
            if (r15 != null) {
                AnonymousClass1IS r18 = r15.A0z;
                if (C15380n4.A0N(r18.A00) && r18.A02) {
                    boolean z = false;
                    if (C37381mH.A00(r15.A0C, 4) > 0) {
                        z = true;
                    }
                    MyStatusesActivity myStatusesActivity = r24.A00;
                    if (z) {
                        MyStatusesActivity.A02(r15, myStatusesActivity, true);
                    } else {
                        myStatusesActivity.A0l.notifyDataSetChanged();
                    }
                }
            }
        } else if (this instanceof C248116y) {
            C248116y r19 = (C248116y) this;
            if (r15 instanceof AbstractC16130oV) {
                r19.A04(r15, i);
            }
        } else if (this instanceof C35131hI) {
            C35131hI r110 = (C35131hI) this;
            AnonymousClass1IS r4 = r15.A0z;
            if (C15380n4.A0N(r4.A00) && i != 8) {
                if (r4.A02) {
                    StatusesFragment statusesFragment = r110.A00;
                    if (statusesFragment.A0q.A00 != null) {
                        C37371mG r111 = statusesFragment.A0o;
                        if (r111 != null) {
                            r111.A03(true);
                        }
                        C37371mG r25 = new C37371mG(statusesFragment.A0T, statusesFragment);
                        statusesFragment.A0o = r25;
                        statusesFragment.A0r.Aaz(r25, new Void[0]);
                        if (r15.A0H > 0) {
                            AnonymousClass1m4 r7 = statusesFragment.A0n;
                            Context A01 = statusesFragment.A01();
                            if ((r15 instanceof C28861Ph) || (r15 instanceof AnonymousClass1XR) || (r15 instanceof AnonymousClass1X7) || (r15 instanceof AnonymousClass1X2)) {
                                String str = r4.A01;
                                Map map = r7.A03;
                                if (map.containsKey(str)) {
                                    map.put(str, r15);
                                    for (AbstractC15340mz r02 : map.values()) {
                                        if (r02.A0H <= 0) {
                                            return;
                                        }
                                    }
                                    ViewOnClickCListenerShape0S0300000_I0 viewOnClickCListenerShape0S0300000_I0 = new ViewOnClickCListenerShape0S0300000_I0(r7, new HashSet(map.values()), statusesFragment, 16);
                                    ViewTreeObserver$OnGlobalLayoutListenerC33691ev AGo = statusesFragment.AGo(R.string.status_sent, 3500, true);
                                    C34271fr r112 = AGo.A03;
                                    r112.A07(r112.A02.getString(R.string.undo), viewOnClickCListenerShape0S0300000_I0);
                                    ((TextView) AnonymousClass028.A0D(r112.A05, R.id.snackbar_action)).setTextColor(AnonymousClass00T.A00(A01, R.color.snackbarButton));
                                    AGo.A01();
                                    map.clear();
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                }
                if (i == 12) {
                    r110.A00.A1B();
                }
            }
        } else if (this instanceof C35151hK) {
            C35151hK r113 = (C35151hK) this;
            if (r15 instanceof C30421Xi) {
                r113.A00.A08.A0B(r15);
            }
        } else if (this instanceof AnonymousClass1m5) {
            AnonymousClass1m5 r42 = (AnonymousClass1m5) this;
            if (i != 8 && (r0 = r15.A0z.A00) != null && r0.getType() == 9) {
                ReportActivity reportActivity = r42.A00;
                if (i == 3) {
                    ((ActivityC13810kN) reportActivity).A05.A0J(new RunnableBRunnable0Shape11S0100000_I0_11(r42, 11), 2000);
                } else {
                    reportActivity.AZz();
                }
            }
        } else if (this instanceof AnonymousClass1m6) {
            PopupNotification popupNotification = ((AnonymousClass1m6) this).A00;
            PopupNotificationViewPager popupNotificationViewPager = popupNotification.A10;
            AnonymousClass1IS r43 = r15.A0z;
            View findViewWithTag2 = popupNotificationViewPager.findViewWithTag(r43);
            if (findViewWithTag2 instanceof ViewGroup) {
                View childAt = ((ViewGroup) findViewWithTag2).getChildAt(0);
                if (childAt instanceof ViewGroup) {
                    View childAt2 = ((ViewGroup) childAt).getChildAt(0);
                    if ((childAt2 instanceof AnonymousClass1OY) && (r12 = (AnonymousClass1OY) childAt2) != null) {
                        r12.A0s();
                    }
                }
            }
            if (r15.A0y == 20 && popupNotification.A1M.contains(r15) && i == 3 && (findViewWithTag = popupNotification.A10.findViewWithTag(r43)) != null && (stickerView = (StickerView) findViewWithTag.findViewById(R.id.popup_sticker_view)) != null) {
                popupNotification.A2c((C30061Vy) r15, stickerView);
            }
        } else if (this instanceof AnonymousClass1m7) {
            AnonymousClass1m7 r26 = (AnonymousClass1m7) this;
            if (MediaTranscodeService.A0B.containsKey(r15.A0z)) {
                r26.A00.A00();
            }
        } else if (this instanceof AnonymousClass107) {
            AnonymousClass107 r33 = (AnonymousClass107) this;
            if (r33.A04.A00.A05(AbstractC15460nI.A18) && r15.A0z.A02 && !r15.A0s) {
                r33.A06.Ab2(new RunnableBRunnable0Shape7S0100000_I0_7(r33, 20));
            }
        } else if (this instanceof C35301hb) {
            C35301hb r27 = (C35301hb) this;
            if ((r15 != null && r27.A01.equals(r15.A0z.A00) && C30041Vv.A0G(r15.A0y) && i == 3) || r27.A03(r15) || i == 30) {
                r27.A00.A00.A34();
            }
        } else if (this instanceof C35331he) {
            C35331he r114 = (C35331he) this;
            if (r15 != null && r15.A0v) {
                StarredMessagesActivity starredMessagesActivity = r114.A00;
                starredMessagesActivity.A2e().post(new RunnableBRunnable0Shape3S0200000_I0_3(starredMessagesActivity, 9, r15));
            }
        } else if (this instanceof C35181hO) {
            ((C35181hO) this).A03(r15);
        } else if (this instanceof C35351hg) {
            C35351hg r115 = (C35351hg) this;
            if (r15 != null && i == 30) {
                KeptMessagesActivity keptMessagesActivity = r115.A00;
                keptMessagesActivity.A2e().post(new RunnableBRunnable0Shape3S0200000_I0_3(keptMessagesActivity, 9, r15));
                keptMessagesActivity.A2o();
            }
        } else if (this instanceof C35361hh) {
            MediaAlbumActivity mediaAlbumActivity = ((C35361hh) this).A00;
            C35421hp r03 = mediaAlbumActivity.A08;
            AnonymousClass1IS r28 = r15.A0z;
            List<AbstractC15340mz> list = r03.A00;
            if (list != null) {
                for (AbstractC15340mz r04 : list) {
                    if (r04.A0z.equals(r28)) {
                        View findViewWithTag3 = mediaAlbumActivity.A2e().findViewWithTag(r28);
                        if (findViewWithTag3 != null) {
                            AnonymousClass1OY r116 = (AnonymousClass1OY) findViewWithTag3;
                            if (r116.A1L(r28)) {
                                if (i == 8) {
                                    if (r116.getFMessage() == r15) {
                                        r116.A0w();
                                        return;
                                    }
                                } else if (i == 12) {
                                    if (r116.getFMessage() == r15) {
                                        r116.A0t();
                                        return;
                                    }
                                } else if (i == 27) {
                                    r116.A1C(r15, i);
                                    return;
                                } else if (i == 30) {
                                    return;
                                }
                                r116.A1D(r15, true);
                                return;
                            }
                            throw new IllegalStateException();
                        }
                        HashSet hashSet = mediaAlbumActivity.A0R;
                        if (!hashSet.contains(r28)) {
                            hashSet.add(r28);
                            return;
                        }
                        return;
                    }
                }
            }
        } else if (this instanceof C35371hi) {
            C35371hi r34 = (C35371hi) this;
            if (i == 25 && !r15.A0z.A02 && C30041Vv.A0I(r15.A0y)) {
                r34.A00.A0R.execute(new RunnableBRunnable0Shape3S0200000_I0_3(r34, 3, r15));
            }
        } else if (this instanceof C35381hj) {
            C35381hj r05 = (C35381hj) this;
            if (r15 != null) {
                ListChatInfo listChatInfo = r05.A00;
                if (listChatInfo.A2u().equals(r15.A0z.A00) && C30041Vv.A0G(r15.A0y) && i == 3) {
                    listChatInfo.A2z();
                }
            }
        } else if (this instanceof C35391hk) {
            C35391hk r35 = (C35391hk) this;
            if (r15 != null) {
                ContactInfoActivity contactInfoActivity = r35.A00;
                if (contactInfoActivity.A2v().equals(r15.A0z.A00) && C30041Vv.A0G(r15.A0y) && (i == 3 || i == 9)) {
                    contactInfoActivity.A31();
                    return;
                }
            }
            if (i == 30) {
                r35.A00.A31();
            }
        } else if (this instanceof C35141hJ) {
            C35141hJ r29 = (C35141hJ) this;
            if (i == 30) {
                C15350n0 conversationCursorAdapter = r29.A00.A1g.getConversationCursorAdapter();
                if (r15 != null) {
                    conversationCursorAdapter.A0R.add(r15.A0z);
                } else {
                    conversationCursorAdapter.A01++;
                }
                conversationCursorAdapter.notifyDataSetChanged();
                return;
            }
            AnonymousClass1IS r36 = r15.A0z;
            AbstractC14640lm r117 = r36.A00;
            Conversation conversation = r29.A00;
            if (r117.equals(conversation.A2s) && (b = r15.A0y) != 8) {
                ConversationListView conversationListView = conversation.A1g;
                RunnableBRunnable0Shape0S0211000_I0 runnableBRunnable0Shape0S0211000_I0 = new RunnableBRunnable0Shape0S0211000_I0(conversationListView, r15, i, 0, false);
                conversationListView.A09 = runnableBRunnable0Shape0S0211000_I0;
                conversationListView.post(runnableBRunnable0Shape0S0211000_I0);
                if (i == 3) {
                    if (b != 1 || conversationListView.getLastVisiblePosition() < conversationListView.getCount() - 2) {
                        return;
                    }
                } else if (!(i == 28 || i == 27)) {
                    if (i == 20) {
                        conversationListView.getConversationCursorAdapter().A0F.clear();
                        conversationListView.A02();
                        return;
                    } else if (i == 24 && conversation.A0i.A00().A01(r117) && ((ActivityC13810kN) conversation).A09.A00.getBoolean("conversation_sound", true) && (!C30041Vv.A0h(r15))) {
                        C21300xC r210 = conversation.A3k;
                        StringBuilder sb = new StringBuilder();
                        sb.append(AnonymousClass01V.A04);
                        sb.append(R.raw.send_message);
                        r210.A01(Uri.parse(sb.toString()));
                        return;
                    } else {
                        return;
                    }
                }
                if (conversationListView.getChildCount() > 0 && conversationListView.A0A) {
                    int childCount = conversationListView.getChildCount() - 1;
                    View childAt3 = conversationListView.getChildAt(childCount);
                    if (r36.equals(childAt3.getTag())) {
                        conversationListView.A04();
                    } else if (childAt3 instanceof C37331m9) {
                        while ((childAt3 instanceof C37331m9) && childCount > 0) {
                            childCount--;
                            childAt3 = conversationListView.getChildAt(childCount);
                            if (r36.equals(childAt3.getTag())) {
                                conversationListView.A04();
                            }
                        }
                    }
                }
            }
        }
    }
}
