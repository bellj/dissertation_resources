package X;

import android.os.Build;
import java.security.Signature;
import java.security.SignatureException;

/* renamed from: X.5ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117855ap extends AnonymousClass04A {
    public final /* synthetic */ AnonymousClass21K A00;
    public final /* synthetic */ AnonymousClass61E A01;
    public final /* synthetic */ byte[] A02;

    public C117855ap(AnonymousClass21K r1, AnonymousClass61E r2, byte[] bArr) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = bArr;
    }

    @Override // X.AnonymousClass04A
    public void A00() {
        this.A01.A02.A06("sign: authentication failed");
        this.A00.AMc();
    }

    @Override // X.AnonymousClass04A
    public void A01(int i, CharSequence charSequence) {
        C30931Zj r2 = this.A01.A02;
        StringBuilder A0k = C12960it.A0k("sign: authentication error=");
        A0k.append(i);
        A0k.append(" errString=");
        r2.A05(C12960it.A0f(A0k, i));
        this.A00.AMb(i, charSequence);
    }

    @Override // X.AnonymousClass04A
    public void A02(int i, CharSequence charSequence) {
        C30931Zj r2 = this.A01.A02;
        StringBuilder A0k = C12960it.A0k("sign: authentication help=");
        A0k.append(i);
        r2.A06(C12960it.A0Z(charSequence, " errString=", A0k));
        this.A00.AMe(i, charSequence);
    }

    @Override // X.AnonymousClass04A
    public void A03(AnonymousClass0MS r5) {
        try {
            Signature signature = r5.A00.A00;
            AnonymousClass009.A05(signature);
            AnonymousClass21K r1 = this.A00;
            r1.AMg(signature);
            signature.update(this.A02);
            r1.AMf(signature.sign());
        } catch (SignatureException e) {
            C30931Zj r2 = this.A01.A02;
            StringBuilder A0k = C12960it.A0k("sign: api=");
            A0k.append(Build.VERSION.SDK_INT);
            A0k.append(" error: ");
            r2.A05(C12960it.A0d(e.toString(), A0k));
            this.A00.AMf(null);
        }
    }
}
