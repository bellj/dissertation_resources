package X;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.util.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

/* renamed from: X.0DG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DG extends AnonymousClass0DH {
    public final Class A00;
    public final Constructor A01;
    public final Method A02;
    public final Method A03;
    public final Method A04;
    public final Method A05;
    public final Method A06;

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0004 */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.reflect.Constructor */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.reflect.Constructor */
    /* JADX WARN: Multi-variable type inference failed */
    public AnonymousClass0DG() {
        Constructor constructor;
        Method method;
        Method method2;
        Class cls;
        Method method3;
        Method method4;
        Method method5;
        Class<?> cls2 = null;
        try {
            Class<?> cls3 = Class.forName("android.graphics.FontFamily");
            Constructor<?> constructor2 = cls3.getConstructor(new Class[0]);
            Class<?> cls4 = Integer.TYPE;
            method = cls3.getMethod("addFontFromAssetManager", AssetManager.class, String.class, cls4, Boolean.TYPE, cls4, cls4, cls4, FontVariationAxis[].class);
            Class<?> cls5 = Integer.TYPE;
            method2 = cls3.getMethod("addFontFromBuffer", ByteBuffer.class, cls5, FontVariationAxis[].class, cls5, cls5);
            method3 = cls3.getMethod("freeze", new Class[0]);
            method4 = cls3.getMethod("abortCreation", new Class[0]);
            method5 = A0A(cls3);
            cls2 = cls3;
            cls = cls2;
            constructor = constructor2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            StringBuilder sb = new StringBuilder("Unable to collect necessary methods for class ");
            sb.append(e.getClass().getName());
            Log.e("TypefaceCompatApi26Impl", sb.toString(), e);
            method5 = cls2;
            constructor = cls2;
            method = cls2;
            method2 = cls2;
            method3 = cls2;
            method4 = cls2;
            cls = cls2;
        }
        this.A00 = cls;
        this.A01 = constructor;
        this.A03 = method;
        this.A04 = method2;
        this.A06 = method3;
        this.A02 = method4;
        this.A05 = method5;
    }

    @Override // X.AnonymousClass0DH, X.C06400Tl
    public Typeface A04(Context context, Resources resources, AnonymousClass0MQ r16, int i) {
        Object obj;
        boolean z;
        Method method = this.A03;
        if (method == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        boolean z2 = false;
        if (method != null) {
            z2 = true;
        }
        if (!z2) {
            return super.A04(context, resources, r16, i);
        }
        try {
            obj = this.A01.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj = null;
        }
        if (obj != null) {
            C05020Ny[] r3 = r16.A00;
            int length = r3.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    C05020Ny r0 = r3[i2];
                    if (!A0B(context, obj, r0.A03, FontVariationAxis.fromFontVariationSettings(r0.A04), r0.A01, r0.A02, r0.A05 ? 1 : 0)) {
                        try {
                            this.A02.invoke(obj, new Object[0]);
                            break;
                        } catch (IllegalAccessException | InvocationTargetException unused2) {
                        }
                    } else {
                        i2++;
                    }
                } else {
                    try {
                        z = ((Boolean) this.A06.invoke(obj, new Object[0])).booleanValue();
                    } catch (IllegalAccessException | InvocationTargetException unused3) {
                        z = false;
                    }
                    if (z) {
                        return A09(obj);
                    }
                }
            }
        }
        return null;
    }

    @Override // X.C06400Tl
    public Typeface A05(Context context, Resources resources, String str, int i, int i2) {
        Object obj;
        boolean z;
        Method method = this.A03;
        if (method == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        boolean z2 = false;
        if (method != null) {
            z2 = true;
        }
        if (!z2) {
            return super.A05(context, resources, str, i, i2);
        }
        try {
            obj = this.A01.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj = null;
        }
        if (obj != null) {
            if (!A0B(context, obj, str, null, 0, -1, -1)) {
                try {
                    this.A02.invoke(obj, new Object[0]);
                } catch (IllegalAccessException | InvocationTargetException unused2) {
                }
            } else {
                try {
                    z = ((Boolean) this.A06.invoke(obj, new Object[0])).booleanValue();
                } catch (IllegalAccessException | InvocationTargetException unused3) {
                    z = false;
                }
                if (z) {
                    return A09(obj);
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e8, code lost:
        r17.A02.invoke(r2, new java.lang.Object[0]);
     */
    @Override // X.AnonymousClass0DH, X.C06400Tl
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Typeface A06(android.content.Context r18, android.os.CancellationSignal r19, X.C04920No[] r20, int r21) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DG.A06(android.content.Context, android.os.CancellationSignal, X.0No[], int):android.graphics.Typeface");
    }

    public Typeface A09(Object obj) {
        try {
            Object newInstance = Array.newInstance(this.A00, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) this.A05.invoke(null, newInstance, -1, -1);
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return null;
        }
    }

    public Method A0A(Class cls) {
        Class cls2 = Integer.TYPE;
        Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass(), cls2, cls2);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    public final boolean A0B(Context context, Object obj, String str, FontVariationAxis[] fontVariationAxisArr, int i, int i2, int i3) {
        try {
            return ((Boolean) this.A03.invoke(obj, context.getAssets(), str, 0, Boolean.FALSE, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), fontVariationAxisArr)).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }
}
