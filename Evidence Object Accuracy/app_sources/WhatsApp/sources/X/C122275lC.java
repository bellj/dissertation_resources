package X;

import android.view.View;

/* renamed from: X.5lC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122275lC extends AbstractC122475lW {
    public final C15550nR A00;
    public final C15610nY A01;
    public final C14830m7 A02;
    public final AnonymousClass018 A03;
    public final C17070qD A04;
    public final AnonymousClass14X A05;

    public C122275lC(View view, C15550nR r2, C15610nY r3, C14830m7 r4, AnonymousClass018 r5, C17070qD r6, AnonymousClass14X r7) {
        super(view);
        this.A02 = r4;
        this.A05 = r7;
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A04 = r6;
    }
}
