package X;

import java.util.Arrays;
import java.util.LinkedHashSet;

/* renamed from: X.27x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468427x {
    public static C17930rd[] A00;
    public static final C17930rd A01;
    public static final C17930rd A02;

    static {
        AbstractC30791Yv r8 = C468527y.A02;
        C17930rd r7 = new C17930rd(r8, "US", "1", new LinkedHashSet(Arrays.asList(r8, C468527y.A01)), new int[]{3, 4}, new int[]{2, 1}, new C32641cU[0], new C32641cU[0], 3, 1, false, true, false);
        A02 = r7;
        C17930rd r18 = new C17930rd(r8, "GT", "502", new LinkedHashSet(Arrays.asList(r8, C468527y.A00)), new int[]{3, 4}, new int[]{2, 1}, new C32641cU[0], new C32641cU[0], 3, 1, false, true, false);
        A01 = r18;
        A00 = new C17930rd[]{r7, r18};
    }
}
