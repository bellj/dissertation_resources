package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0Z0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Z0 implements AbstractC11850gy {
    public final /* synthetic */ RecyclerView A00;

    public AnonymousClass0Z0(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // X.AbstractC11850gy
    public void ARl(View view) {
        AnonymousClass03U A01 = RecyclerView.A01(view);
        if (A01 != null) {
            RecyclerView recyclerView = this.A00;
            int i = A01.A07;
            if (recyclerView.A09 > 0) {
                A01.A04 = i;
                recyclerView.A15.add(A01);
            } else {
                AnonymousClass028.A0a(A01.A0H, i);
            }
            A01.A07 = 0;
        }
    }
}
