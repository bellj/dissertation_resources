package X;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;

/* renamed from: X.2VP  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2VP extends AnonymousClass2VQ {
    public AbstractC14640lm A00 = null;
    public boolean A01 = false;

    public int A2e() {
        return R.layout.activity_downloadable_wallpaper_picker;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        View findViewById;
        super.onCreate(bundle);
        setContentView(A2e());
        C41691tw.A03(this, R.color.searchStatusBar);
        this.A01 = getIntent().getBooleanExtra("is_using_global_wallpaper", false);
        this.A00 = AbstractC14640lm.A01(getIntent().getStringExtra("chat_jid"));
        A1e((Toolbar) AnonymousClass00T.A05(this, R.id.toolbar));
        A1U().A0M(true);
        if (C28391Mz.A02() && (findViewById = findViewById(R.id.separator)) != null) {
            findViewById.setVisibility(8);
        }
    }
}
