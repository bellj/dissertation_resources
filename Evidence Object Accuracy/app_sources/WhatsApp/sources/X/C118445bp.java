package X;

import com.whatsapp.payments.ui.IndiaUpiMandateHistoryActivity;

/* renamed from: X.5bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118445bp extends AnonymousClass0Yo {
    public final /* synthetic */ AnonymousClass6BE A00;
    public final /* synthetic */ IndiaUpiMandateHistoryActivity A01;
    public final /* synthetic */ C128355vy A02;

    public C118445bp(AnonymousClass6BE r1, IndiaUpiMandateHistoryActivity indiaUpiMandateHistoryActivity, C128355vy r3) {
        this.A02 = r3;
        this.A01 = indiaUpiMandateHistoryActivity;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118175bO.class)) {
            IndiaUpiMandateHistoryActivity indiaUpiMandateHistoryActivity = this.A01;
            C128355vy r0 = this.A02;
            C16590pI r3 = r0.A0A;
            return new C118175bO(indiaUpiMandateHistoryActivity, r0.A00, r3, r0.A0F, this.A00, r0.A0k);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
