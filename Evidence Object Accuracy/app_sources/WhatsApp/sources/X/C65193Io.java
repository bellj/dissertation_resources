package X;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/* renamed from: X.3Io  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65193Io {
    public static ICameraUpdateFactoryDelegate A00;

    public static AnonymousClass4IX A00(CameraPosition cameraPosition) {
        C13020j0.A02(cameraPosition, "cameraPosition must not be null");
        try {
            ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate = A00;
            C13020j0.A02(iCameraUpdateFactoryDelegate, "CameraUpdateFactory is not initialized");
            C65873Li r2 = (C65873Li) iCameraUpdateFactoryDelegate;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, cameraPosition);
            return new AnonymousClass4IX(C65873Li.A00(A01, r2, 7));
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public static AnonymousClass4IX A01(LatLng latLng) {
        C13020j0.A02(latLng, "latLng must not be null");
        try {
            ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate = A00;
            C13020j0.A02(iCameraUpdateFactoryDelegate, "CameraUpdateFactory is not initialized");
            C65873Li r2 = (C65873Li) iCameraUpdateFactoryDelegate;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, latLng);
            return new AnonymousClass4IX(C65873Li.A00(A01, r2, 8));
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public static AnonymousClass4IX A02(LatLng latLng, float f) {
        C13020j0.A02(latLng, "latLng must not be null");
        try {
            ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate = A00;
            C13020j0.A02(iCameraUpdateFactoryDelegate, "CameraUpdateFactory is not initialized");
            C65873Li r2 = (C65873Li) iCameraUpdateFactoryDelegate;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, latLng);
            A01.writeFloat(f);
            return new AnonymousClass4IX(C65873Li.A00(A01, r2, 9));
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public static AnonymousClass4IX A03(LatLngBounds latLngBounds, int i) {
        C13020j0.A02(latLngBounds, "bounds must not be null");
        try {
            ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate = A00;
            C13020j0.A02(iCameraUpdateFactoryDelegate, "CameraUpdateFactory is not initialized");
            C65873Li r2 = (C65873Li) iCameraUpdateFactoryDelegate;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, latLngBounds);
            A01.writeInt(i);
            return new AnonymousClass4IX(C65873Li.A00(A01, r2, 10));
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
