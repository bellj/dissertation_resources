package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;

/* renamed from: X.2OU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OU extends AnonymousClass2ON {
    public final /* synthetic */ AnonymousClass19Z A00;
    public final /* synthetic */ C237612x A01;
    public final /* synthetic */ C236312k A02;

    public AnonymousClass2OU(AnonymousClass19Z r1, C237612x r2, C236312k r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass2ON
    public void A02(String str, boolean z) {
        AbstractC14440lR r2;
        int i;
        AnonymousClass009.A01();
        AnonymousClass19Z r5 = this.A00;
        AnonymousClass1MP r4 = r5.A0S;
        StringBuilder sb = new StringBuilder("app/startOutgoingCall/onCreateOutgoingConnection ");
        sb.append(str);
        sb.append(", pendingCallCommand: ");
        sb.append(r4);
        Log.i(sb.toString());
        if (r4 == null || !str.equals(r4.A05)) {
            this.A01.A0A(str);
            return;
        }
        long j = r5.A00;
        if (j > 0) {
            r4.A00 = SystemClock.elapsedRealtime() - j;
        } else {
            AnonymousClass009.A07("selfManagedConnectionNewCallTs is not set");
        }
        if (!z) {
            r2 = r5.A0N;
            i = 43;
        } else {
            if (r4.A04 != null) {
                r2 = r5.A0N;
                i = 42;
            }
            r5.A0S = null;
            r5.A01.removeMessages(1);
        }
        r2.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r5, i, r4));
        r5.A0S = null;
        r5.A01.removeMessages(1);
    }
}
