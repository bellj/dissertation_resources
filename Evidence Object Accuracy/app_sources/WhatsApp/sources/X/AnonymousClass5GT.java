package X;

import java.security.Provider;
import java.security.Security;

/* renamed from: X.5GT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GT implements AnonymousClass5S2 {
    public static volatile Provider A01;
    public final Provider A00;

    public AnonymousClass5GT() {
        Provider provider;
        synchronized (AnonymousClass5GT.class) {
            provider = Security.getProvider("SC");
            if (!(provider instanceof C27511Hu)) {
                if (A01 != null) {
                    provider = A01;
                } else {
                    A01 = new C27511Hu();
                    provider = A01;
                }
            }
        }
        this.A00 = provider;
    }
}
