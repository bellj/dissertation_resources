package X;

import android.view.ViewGroup;
import com.whatsapp.picker.search.StickerSearchTabFragment;

/* renamed from: X.3hy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74433hy extends AnonymousClass019 {
    public StickerSearchTabFragment A00;

    @Override // X.AnonymousClass01A
    public int A01() {
        return 7;
    }

    public C74433hy(AnonymousClass01F r2) {
        super(r2, 1);
    }

    @Override // X.AnonymousClass019, X.AnonymousClass01A
    public void A0C(ViewGroup viewGroup, Object obj, int i) {
        super.A0C(viewGroup, obj, i);
        if (this.A00 != obj) {
            this.A00 = (StickerSearchTabFragment) obj;
        }
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        return StickerSearchTabFragment.A00(i);
    }
}
