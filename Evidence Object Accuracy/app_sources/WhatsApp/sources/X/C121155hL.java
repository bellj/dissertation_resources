package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.5hL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121155hL extends AnonymousClass69F {
    @Override // X.AnonymousClass69F, X.AnonymousClass5X2
    public View buildPaymentHelpSupportSection(Context context, AbstractC28901Pl r4, String str) {
        C117725aV r1 = new C117725aV(context);
        r1.setContactInformation(this.A02);
        return r1;
    }
}
