package X;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

/* renamed from: X.0XP  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0XP implements AbstractC12690iL, AbstractC12600iB, AdapterView.OnItemClickListener {
    public Rect A00;

    public abstract void A01(int i);

    public abstract void A02(int i);

    public abstract void A03(int i);

    public abstract void A04(View view);

    public abstract void A05(PopupWindow.OnDismissListener onDismissListener);

    public abstract void A06(AnonymousClass07H v);

    public abstract void A07(boolean z);

    public abstract void A08(boolean z);

    public boolean A09() {
        return true;
    }

    @Override // X.AbstractC12690iL
    public boolean A7P(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public boolean A9n(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public void AIn(Context context, AnonymousClass07H r2) {
    }

    public static int A00(Context context, ListAdapter listAdapter, int i) {
        FrameLayout frameLayout = null;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        View view = null;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < count; i4++) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i3) {
                view = null;
                i3 = itemViewType;
            }
            if (frameLayout == null) {
                frameLayout = new FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, frameLayout);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth > i2) {
                i2 = measuredWidth;
            }
        }
        return i2;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ListAdapter listAdapter = (ListAdapter) adapterView.getAdapter();
        ListAdapter listAdapter2 = listAdapter;
        if (listAdapter instanceof HeaderViewListAdapter) {
            listAdapter2 = ((HeaderViewListAdapter) listAdapter2).getWrappedAdapter();
        }
        AnonymousClass07H r3 = ((AnonymousClass0BQ) listAdapter2).A01;
        MenuItem menuItem = (MenuItem) listAdapter.getItem(i);
        int i2 = 4;
        if (A09()) {
            i2 = 0;
        }
        r3.A0K(menuItem, this, i2);
    }
}
