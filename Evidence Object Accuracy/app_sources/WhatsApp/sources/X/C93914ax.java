package X;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/* renamed from: X.4ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93914ax {
    public final ByteArrayOutputStream A00;
    public final DataOutputStream A01;

    public C93914ax() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        this.A00 = byteArrayOutputStream;
        this.A01 = new DataOutputStream(byteArrayOutputStream);
    }

    public static void A00(DataOutputStream dataOutputStream, long j) {
        dataOutputStream.writeByte(((int) (j >>> 24)) & 255);
        dataOutputStream.writeByte(((int) (j >>> 16)) & 255);
        dataOutputStream.writeByte(((int) (j >>> 8)) & 255);
        dataOutputStream.writeByte(((int) j) & 255);
    }
}
