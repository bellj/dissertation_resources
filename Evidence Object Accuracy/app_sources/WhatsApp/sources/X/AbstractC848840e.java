package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;

/* renamed from: X.40e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC848840e extends AbstractC37191le {
    public WaImageButton A00;
    public WaImageView A01;
    public WaTextView A02;
    public WaTextView A03;

    public AbstractC848840e(View view) {
        super(view);
        this.A01 = (WaImageView) AnonymousClass028.A0D(view, R.id.icon);
        this.A03 = (WaTextView) AnonymousClass028.A0D(view, R.id.title);
        this.A02 = (WaTextView) AnonymousClass028.A0D(view, R.id.subtitle);
        this.A00 = (WaImageButton) AnonymousClass028.A0D(view, R.id.action_button);
    }

    @Override // X.AbstractC37191le
    public void A08() {
        this.A01.setImageDrawable(null);
        this.A03.setText("");
        WaTextView waTextView = this.A02;
        waTextView.setText("");
        waTextView.setVisibility(8);
        WaImageButton waImageButton = this.A00;
        waImageButton.setImageDrawable(null);
        waImageButton.setVisibility(8);
        waImageButton.setOnClickListener(null);
    }

    public void A0A(AnonymousClass403 r4) {
        this.A0H.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(this, 18, r4));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(this, 19, r4));
    }
}
