package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;
import com.whatsapp.R;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.2hz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55222hz extends AnonymousClass03U implements View.OnClickListener {
    public final View A00;
    public final View A01;
    public final ImageView A02;
    public final TextView A03;
    public final SelectionCheckView A04;
    public final /* synthetic */ AnonymousClass21W A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public View$OnClickListenerC55222hz(View view, AnonymousClass21W r3) {
        super(view);
        this.A05 = r3;
        this.A01 = view;
        this.A00 = view.findViewById(R.id.filter_thumb);
        this.A02 = C12970iu.A0L(view, R.id.filter_thumb_image);
        this.A04 = (SelectionCheckView) view.findViewById(R.id.selection_check);
        this.A03 = C12960it.A0J(view, R.id.filter_name);
        view.setOnClickListener(this);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        int A00 = A00();
        if (A00 == -1) {
            return;
        }
        if (A00 <= 0 || this.A05.A01[A00 - 1] != null) {
            AnonymousClass21U r3 = this.A05.A0A;
            if (A00 != r3.A01) {
                r3.A05(new RunnableBRunnable0Shape1S0101000_I1(this, A00, 9), new RunnableBRunnable0Shape1S0101000_I1(this, A00, 10), A00);
            }
        }
    }
}
