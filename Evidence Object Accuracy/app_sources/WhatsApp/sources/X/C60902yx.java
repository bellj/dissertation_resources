package X;

import android.content.Context;
import android.content.res.Resources;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import java.util.List;

/* renamed from: X.2yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60902yx extends AnonymousClass4UX {
    public WaImageView A00;
    public final Resources A01;
    public final AnonymousClass018 A02;
    public final AbstractC41521tf A03 = new C70273b1(this);
    public final AnonymousClass19O A04;

    public C60902yx(C16590pI r2, AnonymousClass018 r3, AnonymousClass19O r4) {
        this.A01 = C16590pI.A00(r2);
        this.A02 = r3;
        this.A04 = r4;
    }

    @Override // X.AnonymousClass4UX
    public void A00(FrameLayout frameLayout, AnonymousClass1OY r11, AbstractC15340mz r12, C16470p4 r13) {
        String quantityString;
        frameLayout.removeAllViews();
        if (!"review_order".equals(r13.A00()) && !"payment_method".equals(r13.A00())) {
            C58562qr r4 = new C58562qr(frameLayout.getContext());
            frameLayout.addView(r4);
            AnonymousClass1ZD r2 = r13.A01;
            AnonymousClass009.A05(r2);
            r4.A02.setText(C12960it.A0X(frameLayout.getContext(), r2.A07, C12970iu.A1b(), 0, R.string.checkout_native_flow_message_order_text));
            String A02 = r2.A02(this.A02);
            r4.A03.setText(r11.A0q(r2.A08));
            List list = r2.A04.A08;
            AnonymousClass009.A05(list);
            if (list.size() == 1) {
                Context context = frameLayout.getContext();
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, ((C66023Lz) list.get(0)).A00, 0);
                quantityString = context.getString(R.string.checkout_native_flow_message_quantity_text, objArr);
            } else {
                int i = 0;
                for (int i2 = 0; i2 < list.size(); i2++) {
                    i += ((C66023Lz) list.get(i2)).A00;
                }
                Resources A09 = C12960it.A09(frameLayout);
                Object[] objArr2 = new Object[1];
                C12960it.A1P(objArr2, i, 0);
                quantityString = A09.getQuantityString(R.plurals.number_of_items, i, objArr2);
            }
            r4.A01.setText(r11.A0q(quantityString));
            r4.A00.setText(r11.A0q(A02));
            this.A00 = r4.A04;
            C16460p3 A0G = r12.A0G();
            if (A0G == null || !A0G.A05()) {
                this.A00.setVisibility(8);
            } else {
                this.A04.A07(this.A00, r12, this.A03);
            }
        }
    }
}
