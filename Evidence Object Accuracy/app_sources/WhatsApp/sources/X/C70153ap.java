package X;

import androidx.fragment.app.DialogFragment;
import com.whatsapp.conversationslist.ConversationsFragment;
import java.util.Set;

/* renamed from: X.3ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70153ap implements AnonymousClass1Kp {
    public final /* synthetic */ AnonymousClass01F A00;
    public final /* synthetic */ C42611vV A01;

    public C70153ap(AnonymousClass01F r1, C42611vV r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1Kp
    public void A7o() {
        AnonymousClass01F r4 = this.A00;
        ConversationsFragment conversationsFragment = this.A01.A0F;
        conversationsFragment.A2B.Aaz(new AnonymousClass392((DialogFragment) new ConversationsFragment.BulkDeleteConversationDialogFragment(), r4, conversationsFragment.A1T, (Set) conversationsFragment.A2G, true), new Object[0]);
    }

    @Override // X.AnonymousClass1Kp
    public void AHv(boolean z) {
        AnonymousClass01F r4 = this.A00;
        ConversationsFragment conversationsFragment = this.A01.A0F;
        conversationsFragment.A2B.Aaz(new AnonymousClass392(new ConversationsFragment.BulkDeleteConversationDialogFragment(), r4, conversationsFragment.A1T, conversationsFragment.A2G, z), new Object[0]);
    }
}
