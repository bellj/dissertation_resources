package X;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: X.3gF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73443gF extends InputFilter.AllCaps {
    public final /* synthetic */ C57892nn A00;

    public C73443gF(C57892nn r1) {
        this.A00 = r1;
    }

    @Override // android.text.InputFilter.AllCaps, android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        return String.valueOf(charSequence).toLowerCase(this.A00.A00.AHl());
    }
}
