package X;

/* renamed from: X.665  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass665 implements AbstractC136166Lg {
    public final /* synthetic */ AnonymousClass643 A00;

    public AnonymousClass665(AnonymousClass643 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136166Lg
    public void AUE(C129975yb r14) {
        AnonymousClass643 r0 = this.A00;
        C89444Jz r5 = r0.A09;
        AbstractC1311761o r1 = r0.A0N;
        if (r1 != null && r1.isConnected()) {
            int ABB = r1.ABB();
            if (r5 != null) {
                r1.AGc(ABB);
                AnonymousClass65z[] r10 = null;
                C128845wl[] r6 = r14.A0C;
                if (r6 != null) {
                    int length = r6.length;
                    r10 = new AnonymousClass65z[length];
                    for (int i = 0; i < length; i++) {
                        C128845wl r02 = r6[i];
                        if (r02 != null) {
                            r10[i] = new AnonymousClass65z(r02.A02, r02.A01);
                        }
                    }
                }
                r5.A00.A02(new C130045yi(r14.A05, r14.A0A, r14.A0B, r10, r14.A02, r14.A00));
            }
        }
    }
}
