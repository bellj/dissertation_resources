package X;

import java.util.List;

/* renamed from: X.1JF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JF {
    public final AnonymousClass4QY A00;
    public final AnonymousClass4OC A01;
    public final C89704Kz A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final List A06;

    public AnonymousClass1JF(AnonymousClass4QY r1, AnonymousClass4OC r2, C89704Kz r3, String str, String str2, String str3, List list) {
        this.A04 = str;
        this.A03 = str2;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = str3;
        this.A06 = list;
    }
}
