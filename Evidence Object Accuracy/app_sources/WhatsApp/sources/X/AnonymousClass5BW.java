package X;

import java.io.Serializable;

/* renamed from: X.5BW  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5BW implements Serializable, AnonymousClass5ZW {
    public static final Object A01 = AnonymousClass5BV.A00;
    public transient AnonymousClass5ZW A00;
    public final boolean isTopLevel;
    public final String name;
    public final Class owner;
    public final Object receiver;
    public final String signature;

    public AnonymousClass5BW(Class cls, Object obj, String str, String str2, boolean z) {
        this.receiver = obj;
        this.owner = cls;
        this.name = str;
        this.signature = str2;
        this.isTopLevel = z;
    }

    public AbstractC115525Rw A00() {
        Class cls = this.owner;
        if (cls == null) {
            return null;
        }
        if (this.isTopLevel) {
            return new C112805Eu(cls);
        }
        return new C71583dA(cls);
    }
}
