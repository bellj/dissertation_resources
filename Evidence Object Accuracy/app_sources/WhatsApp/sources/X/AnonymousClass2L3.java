package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.2L3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2L3 extends Handler implements AbstractC450820c {
    public boolean A00;
    public final /* synthetic */ HandlerThreadC26611Ed A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2L3(HandlerThreadC26611Ed r2) {
        super(r2.getLooper());
        this.A01 = r2;
    }

    @Override // X.AbstractC450820c
    public void ARW(AnonymousClass1V8 r4, String str) {
        Message obtainMessage = obtainMessage(4, r4);
        obtainMessage.getData().putString("iqId", str);
        obtainMessage.sendToTarget();
    }

    @Override // X.AbstractC450820c
    public void ATl(long j) {
        Message obtainMessage = obtainMessage(3);
        obtainMessage.getData().putLong("timestamp", j);
        obtainMessage.sendToTarget();
    }

    @Override // X.AbstractC450820c
    public void AYY(Message message) {
        message.what = 0;
        sendMessage(message);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        boolean z;
        Message obtainMessage;
        int i = message.what;
        if (i == 0) {
            HandlerThreadC26611Ed.A00(message, this.A01);
        } else if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    message.getData().getLong("timestamp");
                    obtainMessage = ((Handler) this.A01.A02).obtainMessage(9);
                } else if (i == 4) {
                    HandlerThreadC26611Ed.A01(message, this.A01);
                    return;
                } else if (i == 5) {
                    C230710g r0 = this.A01.A0p;
                    obtainMessage = r0.A09.obtainMessage(6, message.obj);
                } else {
                    return;
                }
                obtainMessage.sendToTarget();
            } else if (!this.A00) {
                HandlerThreadC26611Ed r2 = this.A01;
                if (r2.A04.hasMessages(0)) {
                    r2.A04();
                    z = false;
                } else {
                    r2.A04();
                    z = true;
                }
                r2.A07(z);
            }
        } else if (!this.A00) {
            HandlerThreadC26611Ed r1 = this.A01;
            r1.A04();
            r1.A07(false);
        }
    }
}
