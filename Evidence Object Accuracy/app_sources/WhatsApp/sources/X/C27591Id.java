package X;

/* renamed from: X.1Id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27591Id {
    public final String A00;
    public final boolean A01;

    public C27591Id(String str, boolean z) {
        this.A00 = str;
        this.A01 = z;
    }

    public final String toString() {
        String str = this.A00;
        boolean z = this.A01;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
        sb.append("{");
        sb.append(str);
        sb.append("}");
        sb.append(z);
        return sb.toString();
    }
}
