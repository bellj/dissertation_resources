package X;

/* renamed from: X.2SP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2SP extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public String A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public String A0V;
    public String A0W;
    public String A0X;
    public String A0Y;
    public String A0Z;

    public AnonymousClass2SP() {
        super(2162, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A08);
        r3.Abe(24, this.A0G);
        r3.Abe(3, this.A09);
        r3.Abe(23, this.A0H);
        r3.Abe(32, this.A0I);
        r3.Abe(33, this.A00);
        r3.Abe(34, this.A01);
        r3.Abe(15, this.A0N);
        r3.Abe(13, this.A02);
        r3.Abe(11, this.A0O);
        r3.Abe(22, this.A0J);
        r3.Abe(21, this.A03);
        r3.Abe(18, this.A04);
        r3.Abe(20, this.A05);
        r3.Abe(19, this.A0P);
        r3.Abe(25, this.A0Q);
        r3.Abe(31, this.A0A);
        r3.Abe(2, this.A0R);
        r3.Abe(9, this.A0S);
        r3.Abe(10, this.A0T);
        r3.Abe(1, this.A0U);
        r3.Abe(40, this.A06);
        r3.Abe(36, this.A07);
        r3.Abe(38, this.A0V);
        r3.Abe(39, this.A0W);
        r3.Abe(17, this.A0B);
        r3.Abe(26, this.A0K);
        r3.Abe(27, this.A0L);
        r3.Abe(12, this.A0C);
        r3.Abe(14, this.A0M);
        r3.Abe(28, this.A0D);
        r3.Abe(30, this.A0E);
        r3.Abe(35, this.A0X);
        r3.Abe(6, this.A0Y);
        r3.Abe(5, this.A0Z);
        r3.Abe(8, this.A0F);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        String obj8;
        StringBuilder sb = new StringBuilder("WamPaymentsUserAction {");
        Integer num = this.A08;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "actionTarget", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentAccountRowSelected", this.A0G);
        Integer num2 = this.A09;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentActionType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentNumberOfAccountsAvailable", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentNumberOfPeopleInvited", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentPinSetUp", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentSent", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentSmsProviderNumber", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsAccountsExist", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBankId", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksRowSelected", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksScrolled", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksSearchActivated", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksSearchSelected", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksSearchString", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsBanksSelectedName", this.A0Q);
        Integer num3 = this.A0A;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsContactsBucket", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsCountryCode", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsErrorCode", this.A0S);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsErrorText", this.A0T);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsEventId", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsIsMandate", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsIsOrder", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsOrderType", this.A0V);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsP2mPaymentConfigId", this.A0W);
        Integer num4 = this.A0B;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsRequestName", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsRequestRetryCount", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsRequestRetryTimeDelaySeconds", this.A0L);
        Integer num5 = this.A0C;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsResponseResult", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsSmsProviderRetryCount", this.A0M);
        Integer num6 = this.A0D;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsUpiCheckPinErrorReason", obj6);
        Integer num7 = this.A0E;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsVerifyCardResult", obj7);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "queryParams", this.A0X);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "referral", this.A0Y);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "screen", this.A0Z);
        Integer num8 = this.A0F;
        if (num8 == null) {
            obj8 = null;
        } else {
            obj8 = num8.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "upiPaymentsPspId", obj8);
        sb.append("}");
        return sb.toString();
    }
}
