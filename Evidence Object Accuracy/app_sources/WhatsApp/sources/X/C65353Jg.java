package X;

import android.animation.ValueAnimator;
import android.widget.FrameLayout;

/* renamed from: X.3Jg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65353Jg implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AbstractActivityC36611kC A00;

    public /* synthetic */ C65353Jg(AbstractActivityC36611kC r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        AbstractActivityC36611kC r5 = this.A00;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) r5.A08.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, C12960it.A05(valueAnimator.getAnimatedValue()), layoutParams.rightMargin, layoutParams.bottomMargin);
        r5.A08.setLayoutParams(layoutParams);
    }
}
