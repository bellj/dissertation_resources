package X;

/* renamed from: X.1Mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28261Mj<K, V> extends AbstractC17190qP<K, V> {
    public static final AbstractC17190qP EMPTY = new C28261Mj(null, new Object[0], 0);
    public static final long serialVersionUID = 0;
    public final transient Object[] alternatingKeysAndValues;
    public final transient Object hashTable;
    public final transient int size;

    public C28261Mj(Object obj, Object[] objArr, int i) {
        this.hashTable = obj;
        this.alternatingKeysAndValues = objArr;
        this.size = i;
    }

    public static C28261Mj create(int i, Object[] objArr) {
        if (i == 0) {
            return (C28261Mj) EMPTY;
        }
        if (i == 1) {
            C28251Mi.checkEntryNotNull(objArr[0], objArr[1]);
            return new C28261Mj(null, objArr, 1);
        }
        C28291Mn.A02(i, objArr.length >> 1);
        return new C28261Mj(createHashTable(objArr, i, AbstractC17940re.chooseTableSize(i), 0), objArr, i);
    }

    @Override // X.AbstractC17190qP
    public AbstractC17940re createEntrySet() {
        return new C28311Mp(this, this.alternatingKeysAndValues, 0, this.size);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        r6[r2] = (byte) r5;
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0078, code lost:
        r6[r2] = (short) r5;
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00af, code lost:
        r6[r2] = r5;
        r7 = r7 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object createHashTable(java.lang.Object[] r11, int r12, int r13, int r14) {
        /*
            r10 = 0
            r0 = 1
            if (r12 != r0) goto L_0x000d
            r1 = r11[r10]
            r0 = r11[r0]
            X.C28251Mi.checkEntryNotNull(r1, r0)
            r0 = 0
            return r0
        L_0x000d:
            int r9 = r13 + -1
            r0 = 128(0x80, float:1.794E-43)
            r7 = 0
            r8 = -1
            if (r13 > r0) goto L_0x004f
            byte[] r6 = new byte[r13]
            java.util.Arrays.fill(r6, r8)
        L_0x001a:
            if (r7 >= r12) goto L_0x004e
            int r5 = r7 << 1
            int r5 = r5 + r10
            r4 = r11[r5]
            r0 = r5 ^ 1
            r3 = r11[r0]
            X.C28251Mi.checkEntryNotNull(r4, r3)
            int r0 = r4.hashCode()
            int r2 = X.C28301Mo.smear(r0)
        L_0x0030:
            r2 = r2 & r9
            byte r1 = r6[r2]
            r0 = 255(0xff, float:3.57E-43)
            r1 = r1 & r0
            if (r1 != r0) goto L_0x003e
            byte r0 = (byte) r5
            r6[r2] = r0
            int r7 = r7 + 1
            goto L_0x001a
        L_0x003e:
            r0 = r11[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0049
            int r2 = r2 + 1
            goto L_0x0030
        L_0x0049:
            java.lang.IllegalArgumentException r0 = duplicateKeyException(r4, r3, r11, r1)
            throw r0
        L_0x004e:
            return r6
        L_0x004f:
            r0 = 32768(0x8000, float:4.5918E-41)
            if (r13 > r0) goto L_0x008f
            short[] r6 = new short[r13]
            java.util.Arrays.fill(r6, r8)
        L_0x0059:
            if (r7 >= r12) goto L_0x008e
            int r5 = r7 << 1
            int r5 = r5 + r10
            r4 = r11[r5]
            r0 = r5 ^ 1
            r3 = r11[r0]
            X.C28251Mi.checkEntryNotNull(r4, r3)
            int r0 = r4.hashCode()
            int r2 = X.C28301Mo.smear(r0)
        L_0x006f:
            r2 = r2 & r9
            short r1 = r6[r2]
            r0 = 65535(0xffff, float:9.1834E-41)
            r1 = r1 & r0
            if (r1 != r0) goto L_0x007e
            short r0 = (short) r5
            r6[r2] = r0
            int r7 = r7 + 1
            goto L_0x0059
        L_0x007e:
            r0 = r11[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0089
            int r2 = r2 + 1
            goto L_0x006f
        L_0x0089:
            java.lang.IllegalArgumentException r0 = duplicateKeyException(r4, r3, r11, r1)
            throw r0
        L_0x008e:
            return r6
        L_0x008f:
            int[] r6 = new int[r13]
            java.util.Arrays.fill(r6, r8)
        L_0x0094:
            if (r7 >= r12) goto L_0x00c4
            int r5 = r7 << 1
            int r5 = r5 + r10
            r4 = r11[r5]
            r0 = r5 ^ 1
            r3 = r11[r0]
            X.C28251Mi.checkEntryNotNull(r4, r3)
            int r0 = r4.hashCode()
            int r2 = X.C28301Mo.smear(r0)
        L_0x00aa:
            r2 = r2 & r9
            r1 = r6[r2]
            if (r1 != r8) goto L_0x00b4
            r6[r2] = r5
            int r7 = r7 + 1
            goto L_0x0094
        L_0x00b4:
            r0 = r11[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x00bf
            int r2 = r2 + 1
            goto L_0x00aa
        L_0x00bf:
            java.lang.IllegalArgumentException r0 = duplicateKeyException(r4, r3, r11, r1)
            throw r0
        L_0x00c4:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28261Mj.createHashTable(java.lang.Object[], int, int, int):java.lang.Object");
    }

    @Override // X.AbstractC17190qP
    public AbstractC17940re createKeySet() {
        return new C28321Ms(this, new AnonymousClass1Mq(this.alternatingKeysAndValues, 0, this.size));
    }

    @Override // X.AbstractC17190qP
    public AbstractC17950rf createValues() {
        return new AnonymousClass1Mq(this.alternatingKeysAndValues, 1, this.size);
    }

    public static IllegalArgumentException duplicateKeyException(Object obj, Object obj2, Object[] objArr, int i) {
        String valueOf = String.valueOf(obj);
        String valueOf2 = String.valueOf(obj2);
        String valueOf3 = String.valueOf(objArr[i]);
        String valueOf4 = String.valueOf(objArr[i ^ 1]);
        StringBuilder sb = new StringBuilder(valueOf.length() + 39 + valueOf2.length() + valueOf3.length() + valueOf4.length());
        sb.append("Multiple entries with same key: ");
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        sb.append(" and ");
        sb.append(valueOf3);
        sb.append("=");
        sb.append(valueOf4);
        return new IllegalArgumentException(sb.toString());
    }

    @Override // X.AbstractC17190qP, java.util.Map
    public Object get(Object obj) {
        Object obj2 = get(this.hashTable, this.alternatingKeysAndValues, this.size, 0, obj);
        if (obj2 == null) {
            return null;
        }
        return obj2;
    }

    public static Object get(Object obj, Object[] objArr, int i, int i2, Object obj2) {
        int i3;
        if (obj2 == null) {
            return null;
        }
        if (i == 1) {
            if (objArr[0].equals(obj2)) {
                return objArr[1];
            }
            return null;
        } else if (obj == null) {
            return null;
        } else {
            if (obj instanceof byte[]) {
                byte[] bArr = (byte[]) obj;
                int length = bArr.length - 1;
                int smear = C28301Mo.smear(obj2.hashCode());
                while (true) {
                    int i4 = smear & length;
                    i3 = bArr[i4] & 255;
                    if (i3 != 255) {
                        if (obj2.equals(objArr[i3])) {
                            break;
                        }
                        smear = i4 + 1;
                    } else {
                        return null;
                    }
                }
            } else if (obj instanceof short[]) {
                short[] sArr = (short[]) obj;
                int length2 = sArr.length - 1;
                int smear2 = C28301Mo.smear(obj2.hashCode());
                while (true) {
                    int i5 = smear2 & length2;
                    i3 = sArr[i5] & 65535;
                    if (i3 != 65535) {
                        if (obj2.equals(objArr[i3])) {
                            break;
                        }
                        smear2 = i5 + 1;
                    } else {
                        return null;
                    }
                }
            } else {
                int[] iArr = (int[]) obj;
                int length3 = iArr.length - 1;
                int smear3 = C28301Mo.smear(obj2.hashCode());
                while (true) {
                    int i6 = smear3 & length3;
                    i3 = iArr[i6];
                    if (i3 != -1) {
                        if (obj2.equals(objArr[i3])) {
                            break;
                        }
                        smear3 = i6 + 1;
                    } else {
                        return null;
                    }
                }
            }
            return objArr[i3 ^ 1];
        }
    }

    @Override // java.util.Map
    public int size() {
        return this.size;
    }
}
