package X;

import android.view.animation.Animation;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.1Pe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class Abstractanimation.Animation$AnimationListenerC28831Pe implements Animation.AnimationListener {
    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        Runnable runnable;
        if (this instanceof C58162oF) {
            C58162oF r2 = (C58162oF) this;
            Log.i("voip/VoipActivityV2/shrinkPreviewToPip/onAnimationStart");
            r2.A00.A03 = 1;
            VoipActivityV2 voipActivityV2 = r2.A01;
            CallInfo A2i = voipActivityV2.A2i();
            if (A2i != null) {
                voipActivityV2.A1m = false;
                voipActivityV2.A3O(A2i);
                voipActivityV2.A1m = true;
            }
        } else if (this instanceof C58152oE) {
            ((C58152oE) this).A00.setVisibility(0);
        } else if (!(this instanceof C58132oC)) {
            if (this instanceof C58122oB) {
                AnonymousClass2AT r22 = ((C58122oB) this).A00;
                r22.A00.setEnabled(false);
                runnable = r22.A01;
            } else if (this instanceof AnonymousClass2AY) {
                ((AnonymousClass2AY) this).A00.setEnabled(false);
                return;
            } else if (this instanceof C58172oG) {
                C58172oG r23 = (C58172oG) this;
                r23.A02.setEnabled(false);
                runnable = r23.A04;
            } else if (this instanceof C58142oD) {
                ((C58142oD) this).A01.A01.A03.setTranscriptMode(2);
                return;
            } else {
                return;
            }
            runnable.run();
        } else {
            AbstractC36001jA r4 = ((C58132oC) this).A00;
            r4.A0I(r4.A06, Float.valueOf(-0.5f), r4.A0A.getHeight(), true);
        }
    }
}
