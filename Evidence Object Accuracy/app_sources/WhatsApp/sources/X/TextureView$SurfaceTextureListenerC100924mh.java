package X;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.Surface;
import android.view.TextureView;
import com.whatsapp.search.views.itemviews.MessageGifVideoPlayer;

/* renamed from: X.4mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class TextureView$SurfaceTextureListenerC100924mh implements TextureView.SurfaceTextureListener {
    public final /* synthetic */ MessageGifVideoPlayer A00;

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public TextureView$SurfaceTextureListenerC100924mh(MessageGifVideoPlayer messageGifVideoPlayer) {
        this.A00 = messageGifVideoPlayer;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        MessageGifVideoPlayer messageGifVideoPlayer = this.A00;
        messageGifVideoPlayer.A03 = new Surface(surfaceTexture);
        MessageGifVideoPlayer.A00(messageGifVideoPlayer);
        MediaPlayer mediaPlayer = messageGifVideoPlayer.A02;
        if (mediaPlayer != null) {
            mediaPlayer.setSurface(messageGifVideoPlayer.A03);
        }
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        MessageGifVideoPlayer messageGifVideoPlayer = this.A00;
        MediaPlayer mediaPlayer = messageGifVideoPlayer.A02;
        if (mediaPlayer != null) {
            mediaPlayer.setSurface(null);
            messageGifVideoPlayer.A05();
        }
        Surface surface = messageGifVideoPlayer.A03;
        AnonymousClass009.A05(surface);
        surface.release();
        messageGifVideoPlayer.A03 = null;
        return true;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        MessageGifVideoPlayer.A00(this.A00);
    }
}
