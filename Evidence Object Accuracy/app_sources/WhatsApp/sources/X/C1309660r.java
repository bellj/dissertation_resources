package X;

import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309660r {
    public final C16590pI A00;
    public final AnonymousClass018 A01;

    public C1309660r(C16590pI r1, AnonymousClass018 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static JSONArray A00(List list) {
        String str;
        try {
            JSONArray A0L = C117315Zl.A0L();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C119745f2 r3 = (C119745f2) it.next();
                JSONObject A0a = C117295Zj.A0a();
                A0a.put("bank_code", r3.A02);
                A0a.put("bank_name", ((AbstractC30851Zb) r3).A01);
                A0a.put("short_name", r3.A03);
                if (r3.A04) {
                    str = "1";
                } else {
                    str = "0";
                }
                A0a.put("accept_savings", str);
                A0L.put(A0a);
            }
            return A0L;
        } catch (JSONException e) {
            Log.e(C12960it.A0b("PAY: BrazilPayBloksActivity payoutBanksToJsonArrayException: ", e), e);
            return null;
        }
    }

    public static boolean A01(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (!((C130675zn) it.next()).A0B) {
                return false;
            }
        }
        return true;
    }

    public JSONArray A02(List list) {
        Object obj;
        int i;
        String str;
        try {
            JSONArray A0L = C117315Zl.A0L();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C130675zn r8 = (C130675zn) it.next();
                JSONObject A0a = C117295Zj.A0a();
                A0a.put("card_verify_identifier", r8.A02);
                String str2 = r8.A0A;
                A0a.put("card_verify_type", str2);
                AnonymousClass009.A05(str2);
                char c = 65535;
                int hashCode = str2.hashCode();
                if (hashCode != -1302107194) {
                    if (hashCode != -119226117) {
                        if (hashCode == 110379 && str2.equals("otp")) {
                            c = 0;
                        }
                    } else if (str2.equals("app-to-app")) {
                        c = 2;
                    }
                } else if (str2.equals("customer-service")) {
                    c = 1;
                }
                String str3 = "";
                if (c == 0) {
                    AnonymousClass018 r3 = this.A01;
                    String str4 = str3;
                    String str5 = r8.A04;
                    switch (str5.hashCode()) {
                        case 82233:
                            if (str5.equals("SMS")) {
                                str4 = r3.A09(R.string.brazil_verify_otp_sms_title);
                                i = R.string.brazil_verify_otp_sms_description;
                                str3 = r3.A0B(i, r8.A03);
                                break;
                            }
                            break;
                        case 2467610:
                            if (str5.equals("PUSH")) {
                                str4 = r3.A09(R.string.brazil_verify_otp_app_notification_title);
                                str3 = r3.A09(R.string.brazil_verify_otp_app_notification_description);
                                break;
                            }
                            break;
                        case 66081660:
                            if (str5.equals("EMAIL")) {
                                str4 = r3.A09(R.string.brazil_verify_otp_email_title);
                                i = R.string.brazil_verify_otp_email_description;
                                str3 = r3.A0B(i, r8.A03);
                                break;
                            }
                            break;
                        case 81425707:
                            if (str5.equals("VACAT")) {
                                str4 = r3.A09(R.string.brazil_verify_otp_sms_title);
                                break;
                            }
                            break;
                    }
                    AnonymousClass01T A05 = C117315Zl.A05(str4, str3);
                    str3 = (String) A05.A00;
                    obj = A05.A01;
                    A0a.put("card_verify_otp_type", str5);
                    String str6 = r8.A03;
                    if (!TextUtils.isEmpty(str6)) {
                        A0a.put("card_verify_otp_receiver_info", str6);
                    }
                    A0a.put("card_verify_otp_resend_interval_sec", String.valueOf(r8.A01));
                    int i2 = r8.A00;
                    A0a.put("otp_length", String.valueOf(i2));
                    StringBuilder A0h = C12960it.A0h();
                    for (int i3 = 0; i3 < i2; i3++) {
                        A0h.append("#  ");
                    }
                    A0a.put("otp_mask", A0h.toString().trim());
                    A0a.put("card_verify_method_disabled_state", r8.A0B);
                } else if (c == 1) {
                    AnonymousClass018 r4 = this.A01;
                    str3 = r4.A09(R.string.brazil_verify_customer_support_title);
                    String str7 = r8.A09;
                    obj = r4.A0B(R.string.brazil_verify_customer_support_description, C125115qi.A00(str7));
                    A0a.put("support_phone_number", str7);
                } else if (c != 2) {
                    obj = str3;
                } else {
                    String str8 = r8.A06;
                    String str9 = r8.A07;
                    AnonymousClass01T A00 = C130335zF.A00(str8, str9);
                    AnonymousClass018 r10 = this.A01;
                    str3 = r10.A09(R.string.brazil_verify_app_to_app_title);
                    if (A00 != null) {
                        String str10 = r8.A08;
                        Intent A0A = C12970iu.A0A();
                        A0A.putExtra("android.intent.extra.TEXT", str10);
                        A0A.setPackage((String) A00.A00);
                        A0A.setAction((String) A00.A01);
                        if (A0A.resolveActivity(this.A00.A00.getPackageManager()) != null) {
                            String str11 = r8.A05;
                            str = str11;
                            obj = str11;
                            A0a.put("app_to_app_request_payload", r8.A08);
                            A0a.put("app_to_app_partner_app_package", str8);
                            A0a.put("app_to_app_partner_app_name", str);
                            A0a.put("app_to_app_partner_intent_action", str9);
                        }
                    }
                    A0a.put("card_verify_method_disabled_state", true);
                    str = r8.A05;
                    obj = r10.A0B(R.string.brazil_verify_app_to_app_not_installed, str);
                    A0a.put("app_to_app_request_payload", r8.A08);
                    A0a.put("app_to_app_partner_app_package", str8);
                    A0a.put("app_to_app_partner_app_name", str);
                    A0a.put("app_to_app_partner_intent_action", str9);
                }
                if (!TextUtils.isEmpty(str3)) {
                    A0a.put("card_verify_method_title", str3);
                    A0a.put("card_verify_method_description", obj);
                }
                A0L.put(A0a);
            }
            return A0L;
        } catch (JSONException e) {
            Log.e(C12960it.A0b("PAY: BrazilPayBloksActivity cardVerifyMethodsToJsonArray: ", e));
            return null;
        }
    }
}
