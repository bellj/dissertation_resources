package X;

import android.view.animation.Animation;

/* renamed from: X.2oG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58172oG extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass238 A01;
    public final /* synthetic */ AnonymousClass2AS A02;
    public final /* synthetic */ Runnable A03;
    public final /* synthetic */ Runnable A04;

    public C58172oG(AnonymousClass238 r1, AnonymousClass2AS r2, Runnable runnable, Runnable runnable2, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = runnable;
        this.A00 = i;
        this.A03 = runnable2;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass238 r2 = this.A01;
        r2.A02.setVisibility(8);
        r2.A02.getLayoutParams().height = this.A00;
        this.A03.run();
        this.A02.setEnabled(true);
        r2.A0D = false;
    }
}
