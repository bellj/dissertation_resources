package X;

import android.content.Context;
import android.util.SparseArray;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.4Y0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Y0 {
    public SparseArray A00 = new SparseArray();
    public Map A01 = Collections.emptyMap();
    public Map A02 = Collections.emptyMap();
    public final Context A03;
    public final AnonymousClass3JI A04;
    public final C64173En A05;

    public /* synthetic */ AnonymousClass4Y0(Context context, AnonymousClass3JI r3, C64173En r4) {
        this.A03 = context;
        this.A04 = r3;
        this.A05 = r4;
    }

    public C64893Hi A00() {
        Context context = this.A03;
        AnonymousClass3JI r3 = this.A04;
        Map map = this.A01;
        Map map2 = this.A02;
        return new C64893Hi(context, this.A00, r3, this.A05, map, map2);
    }

    public void A01(SparseArray sparseArray) {
        this.A00 = sparseArray;
    }

    public void A02(Map map) {
        this.A01 = map;
    }

    public void A03(Map map) {
        this.A02 = map;
    }
}
