package X;

import android.widget.SeekBar;

/* renamed from: X.4pN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC102584pN implements SeekBar.OnSeekBarChangeListener {
    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void A00(int i) {
        if (!(this instanceof C863346u)) {
            AnonymousClass34W r0 = ((C863246t) this).A00;
            r0.A00.setDuration(C38131nZ.A04(r0.A02, (long) i));
            return;
        }
        C44061y8 r02 = ((C863346u) this).A00;
        r02.A08.setDescription(C38131nZ.A04(r02.A05, (long) i));
    }
}
