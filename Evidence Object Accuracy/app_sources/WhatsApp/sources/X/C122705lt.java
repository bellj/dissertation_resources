package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122705lt extends AbstractC118835cS {
    public final TextView A00;
    public final TextView A01;
    public final TextView A02;
    public final TextView A03;

    public C122705lt(View view) {
        super(view);
        TextView A0I = C12960it.A0I(view, R.id.display_payment_amount);
        this.A01 = A0I;
        this.A03 = C12960it.A0I(view, R.id.conversion_info);
        this.A02 = C12960it.A0I(view, R.id.conversion_additional_info);
        TextView A0I2 = C12960it.A0I(view, R.id.actionableButton);
        this.A00 = A0I2;
        C27531Hw.A06(A0I);
        C27531Hw.A06(A0I2);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r8, int i) {
        C123285mu r82 = (C123285mu) r8;
        TextView textView = this.A01;
        textView.setText(r82.A04);
        View view = this.A0H;
        C12980iv.A14(view.getResources(), textView, R.color.novi_payments_currency_amount_text_color);
        textView.setContentDescription(r82.A05);
        TextView textView2 = this.A03;
        CharSequence charSequence = r82.A07;
        int i2 = 8;
        textView2.setVisibility(C13010iy.A00(TextUtils.isEmpty(charSequence) ? 1 : 0));
        textView2.setText(charSequence);
        C12980iv.A14(view.getResources(), textView2, R.color.secondary_text);
        if (r82.A01) {
            C12980iv.A14(view.getResources(), textView, R.color.payment_unsuccessful_transaction_amount_color);
        }
        if (r82.A02) {
            C92984Yl.A00(textView);
            textView2.setPaintFlags(textView2.getPaintFlags() | 16);
        } else {
            C92984Yl.A01(textView);
            C92984Yl.A01(textView2);
        }
        CharSequence charSequence2 = r82.A06;
        TextView textView3 = this.A02;
        if (charSequence2 != null) {
            textView3.setText(charSequence2);
            textView3.setVisibility(0);
        } else {
            textView3.setVisibility(8);
        }
        TextView textView4 = this.A00;
        CharSequence charSequence3 = r82.A03;
        if (!TextUtils.isEmpty(charSequence3)) {
            i2 = 0;
        }
        textView4.setVisibility(i2);
        textView4.setText(charSequence3);
        textView4.setOnClickListener(r82.A00);
    }
}
