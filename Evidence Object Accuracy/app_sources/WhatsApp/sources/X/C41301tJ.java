package X;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.1tJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41301tJ {
    public final AnonymousClass1K3 A00;
    public final String A01;
    public final List A02;
    public final boolean A03;

    public C41301tJ(AnonymousClass1K3 r2, String str, List list, boolean z) {
        this.A03 = z;
        this.A01 = str;
        this.A02 = list;
        this.A00 = r2;
        Collections.sort(list, new Comparator() { // from class: X.5CD
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0023  */
            @Override // java.util.Comparator
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final int compare(java.lang.Object r8, java.lang.Object r9) {
                /*
                    r7 = this;
                    X.1K4 r8 = (X.AnonymousClass1K4) r8
                    X.1K4 r9 = (X.AnonymousClass1K4) r9
                    int r1 = r8.A00
                    r0 = 1
                    r1 = r1 & r0
                    if (r1 == r0) goto L_0x000b
                    r0 = 0
                L_0x000b:
                    r2 = 0
                    if (r0 == 0) goto L_0x0033
                    X.1tB r4 = r8.A08
                    if (r4 != 0) goto L_0x0015
                    X.1tB r4 = X.AnonymousClass1tB.A02
                L_0x0015:
                    int r1 = r4.A00
                    r0 = 1
                    r1 = r1 & r0
                    if (r1 != r0) goto L_0x0033
                    long r4 = r4.A01
                L_0x001d:
                    int r0 = r9.A00
                    r6 = 1
                    r0 = r0 & r6
                    if (r0 != r6) goto L_0x0030
                    X.1tB r1 = r9.A08
                    if (r1 != 0) goto L_0x0029
                    X.1tB r1 = X.AnonymousClass1tB.A02
                L_0x0029:
                    int r0 = r1.A00
                    r0 = r0 & r6
                    if (r0 != r6) goto L_0x0030
                    long r2 = r1.A01
                L_0x0030:
                    int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
                    return r0
                L_0x0033:
                    r4 = 0
                    goto L_0x001d
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5CD.compare(java.lang.Object, java.lang.Object):int");
            }
        });
    }
}
