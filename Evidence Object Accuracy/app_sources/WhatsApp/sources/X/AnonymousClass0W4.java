package X;

import android.os.Build;
import android.view.View;
import android.view.WindowInsets;

/* renamed from: X.0W4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W4 implements View.OnApplyWindowInsetsListener {
    public C018408o A00 = null;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass07F A02;

    public AnonymousClass0W4(View view, AnonymousClass07F r3) {
        this.A01 = view;
        this.A02 = r3;
    }

    @Override // android.view.View.OnApplyWindowInsetsListener
    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        C018408o AMH;
        C018408o A01 = C018408o.A01(view, windowInsets);
        int i = Build.VERSION.SDK_INT;
        if (i < 30) {
            AnonymousClass0Uc.A0B(this.A01, windowInsets);
            if (A01.equals(this.A00)) {
                AMH = this.A02.AMH(view, A01);
                return AMH.A07();
            }
        }
        this.A00 = A01;
        AMH = this.A02.AMH(view, A01);
        if (i < 30) {
            AnonymousClass028.A0R(view);
        }
        return AMH.A07();
    }
}
