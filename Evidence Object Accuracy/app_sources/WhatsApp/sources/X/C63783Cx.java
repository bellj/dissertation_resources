package X;

import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.3Cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63783Cx {
    public final AbstractC15710nm A00;
    public final C15580nU A01;
    public final C17220qS A02;
    public final AbstractC116755Wr A03;

    public C63783Cx(AbstractC15710nm r1, C15580nU r2, C17220qS r3, AbstractC116755Wr r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A00(List list) {
        C17220qS r11 = this.A02;
        String A01 = r11.A01();
        int size = list.size();
        AnonymousClass1V8[] r4 = new AnonymousClass1V8[size];
        int i = 0;
        while (true) {
            AnonymousClass1W9[] r3 = new AnonymousClass1W9[1];
            if (i < size) {
                r3[0] = new AnonymousClass1W9((Jid) list.get(i), "jid");
                r4[i] = new AnonymousClass1V8("group", r3);
                i++;
            } else {
                C12960it.A1M("link_type", "sub_group", r3, 0);
                AnonymousClass1V8 r42 = new AnonymousClass1V8(new AnonymousClass1V8("link", r3, r4), "links", (AnonymousClass1W9[]) null);
                AnonymousClass1W9[] r32 = new AnonymousClass1W9[4];
                C12960it.A1M("id", A01, r32, 0);
                C12960it.A1M("xmlns", "w:g2", r32, 1);
                C12960it.A1M("type", "set", r32, 2);
                r11.A09(new AnonymousClass3Z8(this.A00, this.A03), AnonymousClass1V8.A00(this.A01, r42, r32, 3), A01, 301, 32000);
                return;
            }
        }
    }
}
