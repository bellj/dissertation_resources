package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.32U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32U extends RunnableC32531cJ {
    public final /* synthetic */ AnonymousClass3FT A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ Integer A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass32U(C21320xE r11, AnonymousClass3FT r12, C20710wC r13, C15580nU r14, UserJid userJid, C14860mA r16, Integer num, List list) {
        super(r11, r13, r14, null, r16, null, list, 91);
        this.A00 = r12;
        this.A01 = userJid;
        this.A02 = num;
    }
}
