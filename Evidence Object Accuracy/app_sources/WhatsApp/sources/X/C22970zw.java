package X;

/* renamed from: X.0zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22970zw {
    public final C14900mE A00;
    public final AnonymousClass10A A01;
    public final C15550nR A02;
    public final C22700zV A03;
    public final C15650ng A04;
    public final C22410z2 A05;
    public final AbstractC14440lR A06;

    public C22970zw(C14900mE r1, AnonymousClass10A r2, C15550nR r3, C22700zV r4, C15650ng r5, C22410z2 r6, AbstractC14440lR r7) {
        this.A00 = r1;
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r2.A05 != r20) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(com.whatsapp.jid.UserJid r16, X.C32141bg r17, byte[] r18, int r19, long r20) {
        /*
            r15 = this;
            r5 = r15
            X.0zV r4 = r15.A03
            r6 = r16
            X.1M2 r2 = r4.A00(r6)
            r11 = 0
            if (r2 == 0) goto L_0x0013
            long r0 = r2.A05
            int r3 = (r0 > r20 ? 1 : (r0 == r20 ? 0 : -1))
            r0 = 1
            if (r3 == 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            r3 = r17
            r12 = r19
            r1 = r18
            if (r18 == 0) goto L_0x005a
            if (r0 != 0) goto L_0x005a
            boolean r1 = r4.A04(r6, r3, r1, r12)
        L_0x0022:
            if (r2 == 0) goto L_0x0026
            int r11 = r2.A03
        L_0x0026:
            r8 = 0
            if (r2 == 0) goto L_0x0058
            java.lang.String r9 = r2.A08
        L_0x002b:
            X.1M2 r0 = r4.A00(r6)
            if (r0 != 0) goto L_0x0055
            r10 = r8
        L_0x0032:
            if (r2 == 0) goto L_0x0053
            X.1bg r7 = r2.A00()
        L_0x0038:
            if (r0 == 0) goto L_0x003e
            X.1bg r8 = r0.A00()
        L_0x003e:
            int r13 = X.C38301nr.A00(r2)
            int r14 = X.C38301nr.A00(r0)
            if (r1 == 0) goto L_0x0052
            X.0lR r0 = r15.A06
            X.24X r4 = new X.24X
            r4.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0.Ab2(r4)
        L_0x0052:
            return
        L_0x0053:
            r7 = r8
            goto L_0x0038
        L_0x0055:
            java.lang.String r10 = r0.A08
            goto L_0x0032
        L_0x0058:
            r9 = r8
            goto L_0x002b
        L_0x005a:
            r0 = 1
            boolean r1 = r4.A03(r6, r3, r12, r0)
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22970zw.A00(com.whatsapp.jid.UserJid, X.1bg, byte[], int, long):void");
    }
}
