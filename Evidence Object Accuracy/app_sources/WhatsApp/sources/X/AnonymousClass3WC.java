package X;

import com.whatsapp.companiondevice.LinkedDevicesViewModel;
import java.util.List;

/* renamed from: X.3WC  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3WC implements AbstractC115995Ts {
    public final /* synthetic */ LinkedDevicesViewModel A00;

    public /* synthetic */ AnonymousClass3WC(LinkedDevicesViewModel linkedDevicesViewModel) {
        this.A00 = linkedDevicesViewModel;
    }

    @Override // X.AbstractC115995Ts
    public final void ATR(List list, List list2, List list3) {
        LinkedDevicesViewModel linkedDevicesViewModel = this.A00;
        linkedDevicesViewModel.A00 = list2;
        if (!list.isEmpty() || !list2.isEmpty() || !list3.isEmpty()) {
            linkedDevicesViewModel.A08.A0B(list);
            linkedDevicesViewModel.A07.A0B(list2);
            linkedDevicesViewModel.A06.A0B(list3);
            return;
        }
        linkedDevicesViewModel.A05.A0B(null);
    }
}
