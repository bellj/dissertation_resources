package X;

/* renamed from: X.6Jq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135746Jq implements Runnable {
    public final /* synthetic */ C125385rA A00;
    public final /* synthetic */ C130055yj A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;

    public RunnableC135746Jq(C125385rA r1, C130055yj r2, String str, String str2) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = str;
        this.A02 = str2;
    }

    @Override // java.lang.Runnable
    public void run() {
        C125385rA r0 = this.A00;
        String str = this.A03;
        String str2 = this.A02;
        AnonymousClass643 r3 = r0.A00;
        r3.A0M.A01();
        Object[] objArr = new Object[3];
        C12990iw.A1P(r3.A0T.A00, str, objArr);
        objArr[2] = str2;
        AnonymousClass643.A00(r3, objArr, 4);
    }
}
