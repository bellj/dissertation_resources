package X;

import android.widget.ImageView;
import java.io.File;
import java.util.Set;

/* renamed from: X.22c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455522c implements AnonymousClass22X {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ ImageView A02;
    public final /* synthetic */ C30921Zi A03;
    public final /* synthetic */ C22460z7 A04;

    @Override // X.AnonymousClass22Y
    public void AMI(C30921Zi r1, File file) {
    }

    @Override // X.AnonymousClass22X
    public void APk() {
    }

    public C455522c(ImageView imageView, C30921Zi r2, C22460z7 r3, int i, int i2) {
        this.A04 = r3;
        this.A03 = r2;
        this.A02 = imageView;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AnonymousClass22X
    public /* bridge */ /* synthetic */ void ASp(Object obj) {
        C30921Zi r4 = this.A03;
        if (((Set) obj).contains(r4.A0F)) {
            this.A04.A08.A00(this.A02, r4, this.A01, this.A00);
        }
    }
}
