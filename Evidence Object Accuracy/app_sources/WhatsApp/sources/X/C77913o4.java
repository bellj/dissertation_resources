package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;

/* renamed from: X.3o4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77913o4 extends AbstractC77963o9 {
    public final Bundle A00 = C12970iu.A0D();

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12451000;
    }

    public C77913o4(Context context, Looper looper, AbstractC14980mM r11, AbstractC15000mO r12, AnonymousClass3BW r13) {
        super(context, looper, r11, r12, r13, 128);
    }
}
