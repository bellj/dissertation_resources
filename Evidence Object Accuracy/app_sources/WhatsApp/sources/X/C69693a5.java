package X;

import java.util.Set;

/* renamed from: X.3a5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69693a5 implements AbstractC28951Pq {
    public static final Set A02;
    public final C16590pI A00;
    public final AnonymousClass4OP A01;

    static {
        String[] strArr = new String[3];
        strArr[0] = "com.facebook.inspiration.shortcut.shareintent.InpirationCameraShareDefaultAlias";
        strArr[1] = "com.facebook.inspiration.shortcut.shareintent.InpirationCameraShareTestAliasActionClarify";
        A02 = C12970iu.A13("com.facebook.inspiration.shortcut.shareintent.InpirationCameraShareTestAliasFamilyConsistency", strArr, 2);
    }

    public C69693a5(C16590pI r1, AnonymousClass4OP r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00da  */
    @Override // X.AbstractC28951Pq
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C28961Pr AGk(android.content.Context r14, java.util.List r15) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C69693a5.AGk(android.content.Context, java.util.List):X.1Pr");
    }
}
