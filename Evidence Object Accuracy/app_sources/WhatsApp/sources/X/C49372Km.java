package X;

import java.util.Set;

/* renamed from: X.2Km  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49372Km {
    public static int A00(int i) {
        if (i < 4) {
            return 1;
        }
        if (i < 8) {
            return 2;
        }
        if (i < 16) {
            return 3;
        }
        if (i < 32) {
            return 4;
        }
        if (i < 64) {
            return 5;
        }
        if (i < 128) {
            return 6;
        }
        if (i < 256) {
            return 7;
        }
        if (i < 512) {
            return 8;
        }
        if (i < 1000) {
            return 9;
        }
        if (i < 1500) {
            return 10;
        }
        if (i < 2000) {
            return 11;
        }
        if (i < 2500) {
            return 12;
        }
        if (i < 3000) {
            return 13;
        }
        if (i < 3500) {
            return 14;
        }
        if (i < 4000) {
            return 15;
        }
        if (i >= 4500) {
            return i < 5000 ? 17 : 18;
        }
        return 16;
    }

    public static int A01(AnonymousClass1JO r5) {
        Set set = r5.A00;
        if (set.size() < 4) {
            return 1;
        }
        if (set.size() < 8) {
            return 2;
        }
        if (set.size() < 16) {
            return 3;
        }
        if (set.size() < 32) {
            return 4;
        }
        if (set.size() < 64) {
            return 5;
        }
        if (set.size() < 128) {
            return 6;
        }
        if (set.size() < 256) {
            return 7;
        }
        if (set.size() < 512) {
            return 8;
        }
        if (set.size() < 999) {
            return 9;
        }
        if (set.size() < 1500) {
            return 10;
        }
        if (set.size() < 2000) {
            return 11;
        }
        if (set.size() < 2500) {
            return 12;
        }
        if (set.size() < 3000) {
            return 13;
        }
        if (set.size() < 3500) {
            return 14;
        }
        if (set.size() < 4000) {
            return 15;
        }
        if (set.size() < 4500) {
            return 16;
        }
        return 17;
    }

    public static int A02(C20710wC r2, C15580nU r3) {
        if (r2.A0b(r3)) {
            return 4;
        }
        if (!r2.A0V() || r3 == null || r2.A0O.A02(r3) != 2) {
            return r2.A0a(r3) ? 3 : 1;
        }
        return 2;
    }
}
