package X;

import java.util.Iterator;

/* renamed from: X.204  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass204 implements Iterator {
    public final Iterator A00;

    public /* synthetic */ AnonymousClass204(Iterator it) {
        this.A00 = it;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A00.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        return this.A00.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
