package X;

import com.whatsapp.emoji.EmojiEditTextBottomSheetDialogFragment;

/* renamed from: X.3UD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UD implements AbstractC116455Vm {
    public final /* synthetic */ EmojiEditTextBottomSheetDialogFragment A00;

    public AnonymousClass3UD(EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment) {
        this.A00 = emojiEditTextBottomSheetDialogFragment;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0A);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment = this.A00;
        AbstractC36671kL.A08(emojiEditTextBottomSheetDialogFragment.A0A, iArr, emojiEditTextBottomSheetDialogFragment.A04);
    }
}
