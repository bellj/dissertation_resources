package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.10U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10U {
    public final C14330lG A00;
    public final C125575rT A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C22320yt A04;
    public final C16510p9 A05;
    public final C15650ng A06;
    public final C21380xK A07;
    public final C18460sU A08;
    public final C15660nh A09;
    public final C003001i A0A;
    public final AnonymousClass12H A0B;
    public final C20850wQ A0C;
    public final C16490p7 A0D;
    public final C21390xL A0E;
    public final C242714w A0F;
    public final C18470sV A0G;
    public final C16630pM A0H;
    public final AtomicBoolean A0I = new AtomicBoolean(false);

    public AnonymousClass10U(C14330lG r3, C125575rT r4, C14830m7 r5, C16590pI r6, C22320yt r7, C16510p9 r8, C15650ng r9, C21380xK r10, C18460sU r11, C15660nh r12, C003001i r13, AnonymousClass12H r14, C20850wQ r15, C16490p7 r16, C21390xL r17, C242714w r18, C18470sV r19, C16630pM r20) {
        this.A02 = r5;
        this.A08 = r11;
        this.A05 = r8;
        this.A03 = r6;
        this.A00 = r3;
        this.A0G = r19;
        this.A07 = r10;
        this.A0A = r13;
        this.A06 = r9;
        this.A0B = r14;
        this.A09 = r12;
        this.A0E = r17;
        this.A04 = r7;
        this.A0D = r16;
        this.A0F = r18;
        this.A0H = r20;
        this.A01 = r4;
        this.A0C = r15;
    }

    public AbstractC15340mz A00(UserJid userJid) {
        AnonymousClass1V2 A04 = this.A0G.A04(userJid);
        if (A04 == null) {
            StringBuilder sb = new StringBuilder("statusmsgstore/getlaststatusmessage/no status for ");
            sb.append(userJid);
            Log.w(sb.toString());
            return null;
        }
        if (A04.A08() == null) {
            AbstractC15340mz A0D = this.A06.A0D(A04.A03());
            synchronized (A04) {
                A04.A08 = A0D;
            }
        }
        return A04.A08();
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public java.util.List A01(com.whatsapp.jid.UserJid r24) {
        /*
        // Method dump skipped, instructions count: 350
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10U.A01(com.whatsapp.jid.UserJid):java.util.List");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A02(boolean r40) {
        /*
        // Method dump skipped, instructions count: 1442
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10U.A02(boolean):void");
    }
}
