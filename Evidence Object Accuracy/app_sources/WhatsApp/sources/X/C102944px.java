package X;

import android.content.Context;
import com.whatsapp.community.EditCommunityActivity;

/* renamed from: X.4px  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102944px implements AbstractC009204q {
    public final /* synthetic */ EditCommunityActivity A00;

    public C102944px(EditCommunityActivity editCommunityActivity) {
        this.A00 = editCommunityActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
