package X;

import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.1US  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1US {
    public static Spanned A00(Context context, Object[] objArr, int i) {
        String string;
        int length = objArr.length;
        if (length == 0) {
            string = context.getString(i);
        } else {
            Object[] copyOf = Arrays.copyOf(objArr, length);
            for (int i2 = 0; i2 < copyOf.length; i2++) {
                if (copyOf[i2] instanceof CharSequence) {
                    copyOf[i2] = Html.escapeHtml((CharSequence) copyOf[i2]);
                }
            }
            string = context.getString(i, copyOf);
        }
        return Html.fromHtml(string);
    }

    public static CharSequence A01(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            Pattern pattern = AnonymousClass27H.A01;
            if (pattern == null) {
                pattern = Pattern.compile("[\\u1680\\u2000-\\u200a\\u205f\\u3000]+\\u0020");
                AnonymousClass27H.A01 = pattern;
            }
            Matcher matcher = pattern.matcher(charSequence);
            if (matcher.find()) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
                int i = 0;
                int i2 = 0;
                while (matcher.find(i)) {
                    i = matcher.end();
                    int start = matcher.start();
                    spannableStringBuilder.replace(start - i2, i - i2, (CharSequence) " ");
                    i2 += (i - start) - 1;
                }
                return spannableStringBuilder;
            }
        }
        return charSequence;
    }

    public static String A02(int i, String str) {
        int length;
        AnonymousClass009.A0E(true);
        return (str == null || (length = str.length()) <= i) ? str : str.substring(length - i);
    }

    public static String A03(int i, String str) {
        return (str == null || str.codePointCount(0, str.length()) <= i) ? str : str.substring(0, str.offsetByCodePoints(0, i));
    }

    public static String A04(int i, String str) {
        if (str == null) {
            return "";
        }
        String A03 = A03(i, str);
        if (str == A03) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A03);
        sb.append("…");
        return sb.toString();
    }

    public static String A05(Context context, int i) {
        return String.format(Locale.US, "%06X", Integer.valueOf(AnonymousClass00T.A00(context, i) & 16777215));
    }

    public static String A06(AnonymousClass018 r3, String str, String str2) {
        String str3;
        String A06 = r3.A06();
        if ("ar".equals(A06) || "fa".equals(A06) || "ur".equals(A06)) {
            str3 = " — ";
        } else {
            str3 = " • ";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(r3.A0F(str));
        sb.append(str3);
        sb.append(r3.A0F(str2));
        return sb.toString();
    }

    public static String A07(CharSequence charSequence) {
        String normalize = Normalizer.normalize(charSequence, Normalizer.Form.NFKD);
        Pattern pattern = AnonymousClass27H.A00;
        if (pattern == null) {
            pattern = Pattern.compile("\\p{Mn}+");
            AnonymousClass27H.A00 = pattern;
        }
        return pattern.matcher(normalize).replaceAll("").toLowerCase(Locale.US).replace((char) 305, 'i');
    }

    public static String A08(CharSequence charSequence) {
        AnonymousClass27J r2 = AnonymousClass27I.A00;
        if (r2 == null) {
            r2 = new AnonymousClass27J(new String[]{"ı", "i", "ة", "ه", "ـ", "", "ى", "ي", "ٱ", "ا", "ڤ", "ف", "ک", "ك", "ں", "ن", "ھ", "ه", "ۃ", "ۂ", "ی", "ي", "ے", "ي", "ە", "ه", "۰", "٠", "۱", "١", "۲", "٢", "۳", "٣", "۴", "٤", "۵", "٥", "۶", "٦", "۷", "٧", "۸", "٨", "۹", "٩", "ࢻ", "ف", "ࢼ", "ق", "ࢽ", "ن", "‌", ""});
            AnonymousClass27I.A00 = r2;
        }
        AnonymousClass27J r0 = AnonymousClass27I.A01;
        if (r0 == null) {
            r0 = new AnonymousClass27J(new String[]{"ٵ", "ٴا", "ٶ", "ٴو", "ٷ", "ٴۇ", "ٸ", "ٴى", "अॆ", "ऄ", "अा", "आ", "र्इ", "ई", "उु", "ऊ", "एॅ", "ऍ", "एॆ", "ऎ", "एे", "ऐ", "अॉ", "ऑ", "आॅ", "ऑ", "अॊ", "ऒ", "आॆ", "ऒ", "अो", "ओ", "आे", "ओ", "अौ", "औ", "आै", "औ", "अॅ", "ॲ", "अऺ", "ॳ", "अऻ", "ॴ", "आऺ", "ॴ", "अॏ", "ॵ", "अॖ", "ॶ", "अॗ", "ॷ", "অা", "আ", "ঋৃ", "ৠ", "ঌৢ", "ৡ", "ਅਾ", "ਆ", "ੲਿ", "ਇ", "ੲੀ", "ਈ", "ੳੁ", "ਉ", "ੳੂ", "ਊ", "ੲੇ", "ਏ", "ਅੈ", "ਐ", "ੳੋ", "ਓ", "ਅੌ", "ਔ", "અા", "આ", "અૅ", "ઍ", "અે", "એ", "અૈ", "ઐ", "અૉ", "ઑ", "અો", "ઓ", "અાૅ", "ઓ", "અૌ", "ઔ", "અાૈ", "ઔ", "ૅા", "ૉ", "ଅା", "ଆ", "ଏୗ", "ଐ", "ଓୗ", "ଔ", "ஸ்ரீ", "ஶ்ரீ", "ఒౕ", "ఓ", "ఒౌ", "ఔ", "ిౕ", "ీ", "ెౕ", "ే", "ొౕ", "ో", "ಉಾ", "ಊ", "ಒೌ", "ಔ", "ಋಾ", "ೠ", "ഇൗ", "ഈ", "ഉൗ", "ഊ", "എെ", "ഐ", "ഒാ", "ഓ", "ഒൗ", "ഔ", "ണ്‍", "ൺ", "ന്‍", "ൻ", "ര്‍", "ർ", "ല്‍", "ൽ", "ള്‍", "ൾ", "අා", "ආ", "අැ", "ඇ", "අෑ", "ඈ", "උෟ", "ඌ", "ඍෘ", "ඎ", "ඏෟ", "ඐ", "එ්", "ඒ", "එෙ", "ඓ", "ඔෟ", "ඖ"});
            AnonymousClass27I.A01 = r0;
        }
        String normalize = Normalizer.normalize(r0.A00(charSequence), Normalizer.Form.NFKD);
        Pattern pattern = AnonymousClass27H.A00;
        if (pattern == null) {
            pattern = Pattern.compile("\\p{Mn}+");
            AnonymousClass27H.A00 = pattern;
        }
        return r2.A00(pattern.matcher(normalize).replaceAll("").toLowerCase(Locale.US)).toString();
    }

    public static String A09(CharSequence charSequence, CharSequence... charSequenceArr) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int length = charSequenceArr.length;
            if (i >= length) {
                return sb.toString();
            }
            sb.append(charSequenceArr[i]);
            if (i < length - 1) {
                sb.append(charSequence);
            }
            i++;
        }
    }

    public static String A0A(String str) {
        return str.replace('+', '-').replace('/', '_');
    }

    public static String A0B(byte[] bArr) {
        int length = bArr.length;
        StringBuilder sb = new StringBuilder(length << 1);
        for (byte b : bArr) {
            sb.append(Character.forDigit((b >> 4) & 15, 16));
            sb.append(Character.forDigit(b & 15, 16));
        }
        return sb.toString();
    }

    public static boolean A0C(CharSequence charSequence) {
        int length;
        if (!(charSequence == null || (length = charSequence.length()) == 0)) {
            for (int i = 0; i < length; i++) {
                char charAt = charSequence.charAt(i);
                if (!(charAt == 8203 || Character.isWhitespace(charAt))) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean A0D(CharSequence charSequence, CharSequence charSequence2) {
        return (TextUtils.isEmpty(charSequence) && TextUtils.isEmpty(charSequence2)) || TextUtils.equals(charSequence, charSequence2);
    }

    public static byte[] A0E(String str) {
        int length = str.length();
        if (length % 2 == 0) {
            byte[] bArr = new byte[length >> 1];
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                int digit = Character.digit(charAt, 16);
                if (digit != -1) {
                    int i2 = i >> 1;
                    byte b = bArr[i2];
                    int i3 = 0;
                    if (i % 2 == 0) {
                        i3 = 4;
                    }
                    bArr[i2] = (byte) ((digit << i3) | b);
                } else {
                    StringBuilder sb = new StringBuilder("invalid character; char=");
                    sb.append(charAt);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            return bArr;
        }
        StringBuilder sb2 = new StringBuilder("even length input string required; length=");
        sb2.append(length);
        throw new IllegalArgumentException(sb2.toString());
    }
}
