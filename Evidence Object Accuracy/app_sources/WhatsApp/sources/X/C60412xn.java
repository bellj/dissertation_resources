package X;

import android.content.Context;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2xn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60412xn extends AnonymousClass1OY {
    public C21740xu A00;
    public boolean A01;

    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        return 191;
    }

    public C60412xn(Context context, AbstractC13890kV r9, AbstractC15340mz r10) {
        super(context, r9, r10);
        A0Z();
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.message_text);
        A0U.setLongClickable(AbstractC28491Nn.A07(A0U));
        ((AnonymousClass1OY) this).A0L.A08();
        Context context2 = getContext();
        Uri A00 = AnonymousClass1FO.A00(this.A00);
        ((AnonymousClass1OY) this).A0L.A08();
        boolean z = r10.A0z.A02;
        int i = z ? R.string.futureproof_message_text_sent_with_action : R.string.futureproof_message_text_with_action;
        if (r10 instanceof C30361Xc) {
            int i2 = ((C30361Xc) r10).A00;
            if (i2 == 56) {
                i = R.string.reaction_futureproof_message_text_with_action;
                if (z) {
                    i = R.string.reaction_futureproof_message_text_sent_with_action;
                }
            } else if (i2 == 68) {
                i = R.string.kic_future_proof;
            }
        }
        AnonymousClass1OY.A0Q(A0U, this, AnonymousClass1FO.A01(context2, A00, i));
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A00 = (C21740xu) A08.AM1.get();
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_unsupported_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_unsupported_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_unsupported_right;
    }
}
