package X;

/* renamed from: X.5KL  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KL extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public AnonymousClass5KL() {
        super(2);
    }

    @Override // X.AnonymousClass5ZQ
    public Object AJ5(Object obj, Object obj2) {
        AnonymousClass5X4 r6 = (AnonymousClass5X4) obj;
        AnonymousClass5ZU r7 = (AnonymousClass5ZU) obj2;
        C16700pc.A0F(r6, r7);
        AnonymousClass5X4 minusKey = r6.minusKey(r7.getKey());
        C112775Er r3 = C112775Er.A00;
        if (minusKey == r3) {
            return r7;
        }
        C112735En r0 = AnonymousClass5ZS.A00;
        AnonymousClass5ZU r2 = minusKey.get(r0);
        if (r2 == null) {
            return new C112765Eq(r7, minusKey);
        }
        AnonymousClass5X4 minusKey2 = minusKey.minusKey(r0);
        if (minusKey2 == r3) {
            return new C112765Eq(r2, r7);
        }
        return new C112765Eq(r2, new C112765Eq(r7, minusKey2));
    }
}
