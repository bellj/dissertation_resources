package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Z6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z6 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100244lb();
    public final String A00;
    public final String A01;
    public final byte[] A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1Z6(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
        this.A02 = parcel.createByteArray();
    }

    public AnonymousClass1Z6(String str, String str2, byte[] bArr) {
        this.A01 = str;
        this.A00 = str2;
        this.A02 = bArr;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
        parcel.writeByteArray(this.A02);
    }
}
