package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.57W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57W implements AnonymousClass13P {
    public final /* synthetic */ C74523iD A00;

    @Override // X.AnonymousClass13P
    public void AUg(C30751Yr r1) {
    }

    public AnonymousClass57W(C74523iD r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass13P
    public void AUh(AbstractC14640lm r2, UserJid userJid) {
        this.A00.A04.A0A(r2);
    }

    @Override // X.AnonymousClass13P
    public void AUi(AbstractC14640lm r2, UserJid userJid) {
        this.A00.A04.A0A(r2);
    }
}
