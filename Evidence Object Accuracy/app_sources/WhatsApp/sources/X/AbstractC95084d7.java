package X;

import java.util.Comparator;

/* renamed from: X.4d7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95084d7 {
    public AbstractC95084d7() {
    }

    public /* synthetic */ AbstractC95084d7(C81163tZ r2) {
        this();
    }

    public static AnonymousClass4XX treeKeys() {
        return treeKeys(AbstractC112285Cu.natural());
    }

    public static AnonymousClass4XX treeKeys(Comparator comparator) {
        return new C81173ta(comparator);
    }
}
