package X;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.0n0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15350n0 extends CursorAdapter {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05 = Long.MIN_VALUE;
    public AbstractC009504t A06;
    public AnonymousClass3DS A07;
    public C36051jF A08;
    public C48522Gp A09;
    public AbstractC15340mz A0A;
    public AnonymousClass1IS A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E = false;
    public final SparseArray A0F = new SparseArray();
    public final ActivityC000800j A0G;
    public final AbstractC15710nm A0H;
    public final AnonymousClass1OX A0I;
    public final AbstractC13890kV A0J;
    public final C14830m7 A0K;
    public final C15650ng A0L;
    public final C14850m9 A0M;
    public final AbstractC14640lm A0N;
    public final C22910zq A0O;
    public final C20320vZ A0P;
    public final ArrayList A0Q = new ArrayList();
    public final HashSet A0R = new HashSet();
    public final HashSet A0S = new HashSet();
    public final HashSet A0T = new HashSet();
    public final List A0U = new ArrayList();
    public final Set A0V = new HashSet();

    @Override // android.widget.CursorAdapter, android.widget.Adapter, android.widget.BaseAdapter
    public boolean hasStableIds() {
        return true;
    }

    @Override // android.widget.CursorAdapter
    public void onContentChanged() {
    }

    public C15350n0(ActivityC000800j r4, AbstractC15710nm r5, AnonymousClass1OX r6, AbstractC13890kV r7, C14830m7 r8, C15650ng r9, C14850m9 r10, AbstractC14640lm r11, C22910zq r12, C20320vZ r13) {
        super((Context) r4, (Cursor) null, false);
        this.A0K = r8;
        this.A0M = r10;
        this.A0H = r5;
        this.A0N = r11;
        this.A0P = r13;
        this.A0L = r9;
        this.A0O = r12;
        this.A0J = r7;
        this.A0G = r4;
        this.A0I = r6;
    }

    public static boolean A00(AnonymousClass1OY r3, AbstractC15340mz r4) {
        AbstractC15340mz fMessage = r3.getFMessage();
        if (fMessage.A0y != r4.A0y || ((r4 instanceof C30361Xc) && fMessage.A0z.A02 != r4.A0z.A02)) {
            return false;
        }
        if (!(r4 instanceof AbstractC16390ow) || !(fMessage instanceof AbstractC16390ow)) {
            return r4.getClass().equals(fMessage.getClass());
        }
        return false;
    }

    public int A01() {
        return (getCursor().getCount() + this.A0Q.size()) - this.A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (r1 != 2) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02(int r6, int r7) {
        /*
            r5 = this;
            r0 = -1
            if (r7 == r0) goto L_0x0016
            r4 = 1
            if (r7 == r4) goto L_0x0016
            X.0mz r3 = r5.getItem(r6)
            X.AnonymousClass009.A05(r3)
            int r1 = X.AbstractC60742yY.A0Y(r3)
            if (r1 == r4) goto L_0x0017
            r0 = 2
            if (r1 == r0) goto L_0x0033
        L_0x0016:
            return r6
        L_0x0017:
            int r2 = r6 + -1
        L_0x0019:
            if (r2 < 0) goto L_0x0033
            X.0mz r1 = r5.getItem(r2)
            if (r1 == 0) goto L_0x0039
            int r0 = r2 + 1
            boolean r0 = r5.A08(r1, r3, r2, r0)
            if (r0 == 0) goto L_0x0039
            boolean r0 = r5.A07(r1)
            if (r0 == 0) goto L_0x0039
            int r2 = r2 + -1
            r3 = r1
            goto L_0x0019
        L_0x0033:
            boolean r0 = r5.A0C
            if (r0 == 0) goto L_0x0016
            int r6 = r6 - r4
            return r6
        L_0x0039:
            int r2 = r2 + r4
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15350n0.A02(int, int):int");
    }

    public int A03(AbstractC15340mz r6) {
        Cursor cursor = getCursor();
        if (cursor != null) {
            SparseArray sparseArray = this.A0F;
            int indexOfValue = sparseArray.indexOfValue(r6);
            if (indexOfValue < 0) {
                int i = 0;
                while (true) {
                    ArrayList arrayList = this.A0Q;
                    if (i >= arrayList.size()) {
                        break;
                    } else if (r6.equals(arrayList.get(i))) {
                        int count = i + cursor.getCount();
                        return count >= A01() ? count + 1 : count;
                    } else {
                        i++;
                    }
                }
            } else {
                int keyAt = sparseArray.keyAt(indexOfValue);
                return keyAt >= A01() ? keyAt + 1 : keyAt;
            }
        }
        return -1;
    }

    public int A04(AbstractC15340mz r10, int i) {
        AbstractC15340mz A05;
        if (A07(r10)) {
            int A0Y = AbstractC60742yY.A0Y(r10);
            int i2 = 0;
            if (A0Y == 1) {
                int i3 = i - 1;
                AbstractC15340mz r2 = r10;
                int i4 = 0;
                while (i3 >= 0 && i4 < 3) {
                    AbstractC15340mz A052 = getItem(i3);
                    if (A052 == null || !A08(A052, r2, i3, i3 + 1) || !A07(A052)) {
                        break;
                    }
                    i4++;
                    i3--;
                    r2 = A052;
                }
                int i5 = i + 1;
                while (i5 < getCount() && i2 < 102 && (A05 = getItem(i5)) != null && A08(A05, r10, i5, i5 - 1) && A07(A05)) {
                    i2++;
                    i5++;
                    r10 = A05;
                }
                if (i4 + 1 + i2 >= 4 && i2 < 102) {
                    if (i2 == 101 || i4 == 0) {
                        return 1;
                    }
                    if (i2 != 0) {
                        return 2;
                    }
                    return 3;
                }
            } else if (A0Y == 2 && this.A0C) {
                int i6 = i - 1;
                AbstractC15340mz r22 = r10;
                while (i6 >= 0) {
                    AbstractC15340mz A053 = getItem(i6);
                    if (A053 == null || !A08(A053, r22, i6, i6 + 1) || !A07(A053)) {
                        break;
                    }
                    i2++;
                    i6--;
                    r22 = A053;
                }
                int i7 = i2 % 2;
                if (i7 != 0) {
                    return i7 != 1 ? 2 : 3;
                }
                int i8 = i + 1;
                AbstractC15340mz A054 = getItem(i8);
                if (A054 == null || !A08(A054, r10, i8, i) || !A07(A054)) {
                    return -1;
                }
                return 1;
            }
        }
        return -1;
    }

    /* renamed from: A05 */
    public AbstractC15340mz getItem(int i) {
        AnonymousClass1IR r2;
        if (this.A04 <= 0 || i != A01()) {
            Cursor cursor = getCursor();
            AbstractC15340mz r3 = null;
            if (cursor != null) {
                int A01 = A01();
                int i2 = i;
                if (i > A01) {
                    i2 = i - 1;
                }
                int count = cursor.getCount();
                if (i2 < count) {
                    SparseArray sparseArray = this.A0F;
                    r3 = (AbstractC15340mz) sparseArray.get(i2);
                    if (r3 == null) {
                        int position = cursor.getPosition();
                        cursor.moveToPosition((count - 1) - i2);
                        int position2 = cursor.getPosition();
                        try {
                            r3 = this.A0L.A0K.A02(cursor, this.A0N, false, true);
                            if (position2 < position) {
                                int i3 = this.A00;
                                if (position2 > i3) {
                                    if (position2 > i3 + 50) {
                                    }
                                }
                                int max = Math.max(0, position2 - 50);
                                this.A00 = max;
                                cursor.moveToPosition(max);
                            }
                            sparseArray.put(i2, r3);
                        } catch (CursorIndexOutOfBoundsException e) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("ConversationCursorAdapter/getItem out-of-bounds");
                            StringBuilder sb2 = new StringBuilder(" unseenRowCount:");
                            sb2.append(this.A04);
                            sb2.append(" unseenMsgCount:");
                            sb2.append(this.A02);
                            sb2.append(" unseenCallCount:");
                            sb2.append(this.A03);
                            sb2.append(" cachePos:");
                            sb2.append(this.A00);
                            sb2.append(" appended:");
                            sb2.append(this.A0Q.size());
                            sb2.append(" db:");
                            sb2.append(sparseArray.size());
                            sb2.append(" jidString:");
                            AbstractC14640lm r1 = this.A0N;
                            sb2.append(r1);
                            sb2.append(" jidType:");
                            sb2.append(r1.getType());
                            sb.append(sb2.toString());
                            sb.append(" cursorCount:");
                            sb.append(count);
                            sb.append(" dataPos:");
                            sb.append(i2);
                            sb.append(" viewPos:");
                            sb.append(i);
                            sb.append(" dividerPos:");
                            sb.append(A01);
                            sb.append(" oldPos:");
                            sb.append(position);
                            sb.append(" newPos:");
                            sb.append(position2);
                            Log.e(sb.toString());
                            throw e;
                        }
                    }
                } else {
                    int i4 = i2 - count;
                    if (i4 >= 0) {
                        ArrayList arrayList = this.A0Q;
                        if (i4 < arrayList.size()) {
                            r3 = (AbstractC15340mz) arrayList.get(i4);
                        }
                    }
                }
                for (AnonymousClass1A8 r4 : this.A0U) {
                    if (!(r3 == null || (r2 = r3.A0L) == null || r2.A03 != 1000)) {
                        C15570nT r12 = r4.A04;
                        if (r12.A0F(r2.A0D) || r12.A0F(r3.A0L.A0E)) {
                            String str = r3.A0L.A0K;
                            if (!TextUtils.isEmpty(str)) {
                                ArrayList arrayList2 = new ArrayList();
                                arrayList2.add(str);
                                r4.A02(null, arrayList2);
                            }
                        } else {
                            r4.A03(r3.A0z, r3.A0L.A0K);
                        }
                    }
                }
            }
            return r3;
        }
        AbstractC15340mz r13 = this.A0A;
        if (r13 != null) {
            return r13;
        }
        C20320vZ r5 = this.A0P;
        AbstractC15340mz A012 = r5.A01(r5.A07.A02(null, true), (byte) 0, this.A0K.A00());
        A012.A0l("dummy msg!");
        this.A0A = A012;
        return A012;
    }

    public final boolean A06(AnonymousClass1OY r3, AbstractC15340mz r4) {
        HashSet hashSet = this.A0T;
        AnonymousClass1IS r1 = r4.A0z;
        return hashSet.contains(r1) || this.A0R.contains(r1) || this.A0S.contains(r1) || this.A06 != null || r3.A02 != this.A01 || (r4 instanceof C30341Xa);
    }

    public final boolean A07(AbstractC15340mz r6) {
        byte b;
        if (r6.A11 <= 0 || r6.A12 > this.A05 || (((b = r6.A0y) == 20 && r6.A0E() != null) || C35011h5.A04(r6) || r6.A0N != null || ((b == 20 && C30041Vv.A06(this.A0K, this.A0O, r6) != null) || AbstractC60742yY.A0Y(r6) == -1))) {
            return false;
        }
        return true;
    }

    public final boolean A08(AbstractC15340mz r13, AbstractC15340mz r14, int i, int i2) {
        boolean z;
        boolean z2;
        AbstractC14640lm A0B;
        long j = r13.A0I;
        long j2 = r14.A0I;
        boolean z3 = false;
        if (Math.abs(j - j2) <= 610000) {
            z3 = true;
        }
        boolean A0A = C38121nY.A0A(j, j2);
        boolean z4 = false;
        if (r13.A12(1) == r14.A12(1)) {
            z4 = true;
        }
        boolean z5 = r13.A0z.A02;
        if (z5 != r14.A0z.A02 || (!z5 && (A0B = r13.A0B()) != null && !A0B.equals(r14.A0B()))) {
            z = false;
        } else {
            z = true;
        }
        boolean z6 = false;
        if (i < A01()) {
            z6 = true;
        }
        boolean z7 = false;
        if (i2 < A01()) {
            z7 = true;
        }
        boolean z8 = false;
        if (z6 == z7) {
            z8 = true;
        }
        boolean z9 = false;
        if (AbstractC60742yY.A0Y(r13) == AbstractC60742yY.A0Y(r14)) {
            z9 = true;
        }
        AbstractC15340mz A0E = r13.A0E();
        AbstractC15340mz A0E2 = r14.A0E();
        if (A0E == A0E2 || !(A0E == null || A0E2 == null || !A0E.A0z.equals(A0E2.A0z))) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z3 || !A0A || !z || !z8 || !z4 || !z9 || !z2) {
            return false;
        }
        return true;
    }

    @Override // android.widget.CursorAdapter
    public void bindView(View view, Context context, Cursor cursor) {
        throw new IllegalStateException("should not be called, getView is defined");
    }

    @Override // android.widget.CursorAdapter
    public void changeCursor(Cursor cursor) {
        this.A0D = true;
        super.changeCursor(cursor);
    }

    @Override // android.widget.CursorAdapter, android.widget.Adapter
    public int getCount() {
        int i = 0;
        if (!this.A0D || getCursor() == null) {
            return 0;
        }
        int count = getCursor().getCount() + this.A0Q.size();
        if (this.A04 > 0) {
            i = 1;
        }
        return count + i;
    }

    @Override // android.widget.CursorAdapter
    public Cursor getCursor() {
        Cursor cursor = super.getCursor();
        if (cursor == null || !cursor.isClosed()) {
            return cursor;
        }
        return null;
    }

    @Override // android.widget.CursorAdapter, android.widget.Adapter
    public long getItemId(int i) {
        long j;
        AbstractC15340mz A05 = getItem(i);
        if (A05 == null) {
            return 0;
        }
        if (A05.A11 == 0) {
            j = (long) A05.A0z.hashCode();
        } else {
            j = A05.A11;
        }
        return (j & 4294967295L) | (((long) A05.A0y) << 32);
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getItemViewType(int i) {
        if (!this.A0D) {
            return -1;
        }
        if (this.A04 > 0 && i == A01()) {
            return 18;
        }
        AbstractC15340mz A05 = getItem(i);
        if (A05 == null) {
            return -1;
        }
        AnonymousClass1OX r2 = this.A0I;
        int A04 = A04(A05, i);
        if (A04 == -1) {
            return r2.A00(A05);
        }
        if (A04 != 1) {
            return 34;
        }
        int A0Y = AbstractC60742yY.A0Y(A05);
        boolean z = A05.A0z.A02;
        return A0Y != 2 ? z ? 32 : 33 : z ? 41 : 42;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0101, code lost:
        if (A00(r1, r8) != false) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        if (r1 != false) goto L_0x001a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02e7  */
    @Override // android.widget.CursorAdapter, android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r18, android.view.View r19, android.view.ViewGroup r20) {
        /*
        // Method dump skipped, instructions count: 832
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15350n0.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getViewTypeCount() {
        int i = 0;
        if (this.A04 > 0) {
            i = 1;
        }
        return i + 95;
    }

    @Override // android.widget.CursorAdapter
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        throw new IllegalStateException("should not be called, getView is defined");
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        this.A0D = false;
    }
}
