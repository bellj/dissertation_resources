package X;

/* renamed from: X.0Fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02930Fk extends AnonymousClass0OL {
    public C02930Fk() {
        super(7, 8);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        ((AnonymousClass0ZE) r3).A00.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
    }
}
