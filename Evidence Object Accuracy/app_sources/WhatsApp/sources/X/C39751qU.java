package X;

/* renamed from: X.1qU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39751qU extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public String A0O;
    public String A0P;

    public C39751qU() {
        super(1138, new AnonymousClass00E(1, 20, 200), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(10, this.A05);
        r3.Abe(8, this.A06);
        r3.Abe(11, this.A07);
        r3.Abe(7, this.A08);
        r3.Abe(17, this.A09);
        r3.Abe(14, this.A0O);
        r3.Abe(1, this.A00);
        r3.Abe(20, this.A0A);
        r3.Abe(26, this.A01);
        r3.Abe(15, this.A02);
        r3.Abe(24, this.A0B);
        r3.Abe(23, this.A0C);
        r3.Abe(27, this.A0D);
        r3.Abe(25, this.A0E);
        r3.Abe(13, this.A0P);
        r3.Abe(22, this.A0F);
        r3.Abe(19, this.A03);
        r3.Abe(4, this.A0G);
        r3.Abe(5, this.A0H);
        r3.Abe(3, this.A0I);
        r3.Abe(6, this.A0J);
        r3.Abe(2, this.A0K);
        r3.Abe(21, this.A0L);
        r3.Abe(18, this.A0M);
        r3.Abe(16, this.A0N);
        r3.Abe(12, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAndroidMediaTranscodeEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dstDurationSec", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dstHeight", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dstSize", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dstWidth", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "durationMs", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "errorType", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "fileIsDoodle", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "firstScanSize", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "hasStatusMessage", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isSuccess", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lowQualitySize", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "maxEdge", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaId", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "midQualitySize", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "operation", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "photoCompressionQuality", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "progressiveJpeg", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "srcBitrate", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "srcDurationSec", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "srcHeight", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "srcSize", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "srcWidth", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "thumbnailSize", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalQueueMs", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "transcodeMediaType", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "transcoderSupported", this.A04);
        sb.append("}");
        return sb.toString();
    }
}
