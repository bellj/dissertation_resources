package X;

import android.graphics.Bitmap;

/* renamed from: X.4Xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92764Xi {
    public final int A00;
    public final int A01 = 100;
    public final Bitmap.Config A02;
    public final boolean A03;

    public C92764Xi(C90654Os r2) {
        this.A00 = r2.A00;
        this.A03 = r2.A02;
        this.A02 = r2.A01;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C92764Xi r5 = (C92764Xi) obj;
                if (!(this.A01 == r5.A01 && this.A00 == r5.A00 && this.A03 == r5.A03 && this.A02 == r5.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((((((((((((((this.A01 * 31) + this.A00) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + (this.A03 ? 1 : 0)) * 31) + this.A02.ordinal()) * 31) + 0) * 31) + 0) * 31) + 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("ImageDecodeOptions{");
        AnonymousClass0PG r3 = new AnonymousClass0PG(C12980iv.A0r(this));
        r3.A00(String.valueOf(this.A01), "minDecodeIntervalMs");
        r3.A00(String.valueOf(this.A00), "maxDimensionPx");
        r3.A00("false", "decodePreviewFrame");
        r3.A00("false", "useLastFrameForPreview");
        r3.A00("false", "decodeAllFrames");
        r3.A00(String.valueOf(this.A03), "forceStaticImage");
        r3.A00(this.A02.name(), "bitmapConfigName");
        r3.A00(null, "customImageDecoder");
        r3.A00(null, "bitmapTransformation");
        r3.A00(null, "colorSpace");
        C12970iu.A1V(r3, A0k);
        return C12960it.A0d("}", A0k);
    }
}
