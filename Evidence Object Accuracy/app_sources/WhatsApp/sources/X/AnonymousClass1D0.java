package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1D0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D0 implements AbstractC16990q5 {
    public final C21500xW A00;
    public final C17690rE A01;
    public final C22610zM A02;
    public final C21540xa A03;
    public final C17660rB A04;
    public final C19360tz A05;
    public final C21520xY A06;
    public final C21550xb A07;

    public AnonymousClass1D0(C21500xW r1, C17690rE r2, C22610zM r3, C21540xa r4, C17660rB r5, C19360tz r6, C21520xY r7, C21550xb r8) {
        this.A00 = r1;
        this.A06 = r7;
        this.A07 = r8;
        this.A05 = r6;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        this.A01.A01();
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        String str;
        String str2;
        String str3;
        C14850m9 r2 = this.A00.A00;
        if (r2.A07(1058)) {
            C21520xY r0 = this.A06;
            synchronized (r0) {
                if (!r0.A00()) {
                    ArrayList arrayList = new ArrayList();
                    try {
                        C237712y r7 = r0.A04.A02;
                        ArrayList arrayList2 = new ArrayList();
                        C16310on A01 = r7.A02.get();
                        try {
                            Cursor A09 = A01.A03.A09("SELECT * FROM conversion_tuples", null);
                            while (A09.moveToNext()) {
                                UserJid userJid = (UserJid) r7.A01.A07(UserJid.class, (long) A09.getInt(A09.getColumnIndexOrThrow("jid_row_id")));
                                if (userJid != null) {
                                    String string = A09.getString(A09.getColumnIndexOrThrow("data"));
                                    String string2 = A09.getString(A09.getColumnIndexOrThrow("source"));
                                    int i = A09.getInt(A09.getColumnIndexOrThrow("biz_count"));
                                    boolean z = false;
                                    if (A09.getInt(A09.getColumnIndexOrThrow("has_user_sent_last_message")) > 0) {
                                        z = true;
                                    }
                                    arrayList2.add(new AnonymousClass22F(userJid, string, string2, i, A09.getLong(A09.getColumnIndexOrThrow("last_interaction")), z));
                                }
                            }
                            A09.close();
                            A01.close();
                            arrayList.addAll(arrayList2);
                        } catch (Throwable th) {
                            try {
                                A01.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } catch (Exception e) {
                        AbstractC15710nm r6 = r0.A00;
                        StringBuilder sb = new StringBuilder("CTWA: ConversionTupleDataMigrator/getAllV1LoggingDataFromStorage/error retrieving v1 conversions from storage e=");
                        sb.append(e.getMessage());
                        r6.AaV(sb.toString(), null, true);
                    }
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AnonymousClass22F r8 = (AnonymousClass22F) it.next();
                        long j = r8.A01;
                        if (System.currentTimeMillis() - j <= AnonymousClass22F.A06 && !r8.A02 && r8.A00 == 0 && (str2 = r8.A04) != null && (str3 = r8.A05) != null) {
                            r0.A01.A04(new C455922g(r8.A03, str2, str3, j));
                        }
                    }
                    r0.A05.A01("ctwa_logging_v2_migration").edit().putBoolean(r0.A06, true).apply();
                }
            }
            r2.A07(823);
        }
        C22610zM r22 = this.A02;
        r22.A01(r22.A01);
        r22.A01(r22.A00);
        C455622d r62 = this.A03.A00;
        ArrayList arrayList3 = new ArrayList();
        C16630pM r4 = r62.A01;
        String str4 = r62.A02;
        Map<String, ?> all = r4.A01(str4).getAll();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            Object obj = all.get(entry.getKey());
            if (obj != null) {
                try {
                    JSONObject jSONObject = new JSONObject(obj.toString());
                    UserJid userJid2 = UserJid.get(jSONObject.getString("uj"));
                    String str5 = null;
                    if (jSONObject.has("d")) {
                        str = jSONObject.getString("d");
                    } else {
                        str = null;
                    }
                    if (jSONObject.has("s")) {
                        str5 = jSONObject.getString("s");
                    }
                    arrayList3.add(new AnonymousClass22F(userJid2, str, str5, jSONObject.getInt("bc"), jSONObject.getLong("lit"), jSONObject.getBoolean("huslm")));
                } catch (AnonymousClass1MW e2) {
                    StringBuilder sb2 = new StringBuilder("CTWA: ClickToWhatsAppAdsConversionStore/getConversion/invalid jid error");
                    sb2.append(e2);
                    r62.A00(sb2.toString(), e2);
                } catch (JSONException e3) {
                    r62.A00("CTWA: ClickToWhatsAppAdsConversionStore/getConversionFromJson/json error", e3);
                }
            } else {
                StringBuilder sb3 = new StringBuilder("CTWA: ClickToWhatsAppAdsConversionStore/getAllConversions/ null pref value for key=");
                sb3.append(entry);
                Log.e(sb3.toString());
            }
        }
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            AnonymousClass22F r72 = (AnonymousClass22F) it2.next();
            if (System.currentTimeMillis() - r72.A01 > AnonymousClass22F.A06) {
                r4.A01(str4).edit().remove(r72.A03.getRawString()).apply();
            }
        }
    }
}
