package X;

/* renamed from: X.5zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130575zd {
    public final double A00;
    public final double A01;

    public C130575zd(double d, double d2) {
        this.A00 = d;
        this.A01 = d2;
    }

    public static C130575zd A00(AnonymousClass1V8 r5) {
        try {
            return new C130575zd(Double.parseDouble(r5.A0H("latitude")), Double.parseDouble(r5.A0H("longitude")));
        } catch (AnonymousClass1V9 unused) {
            return null;
        }
    }
}
