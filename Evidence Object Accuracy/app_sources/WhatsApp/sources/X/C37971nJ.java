package X;

/* renamed from: X.1nJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37971nJ {
    public final String A00;
    public final boolean A01;

    public C37971nJ(String str, boolean z) {
        this.A00 = str;
        this.A01 = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[hash=");
        sb.append(this.A00);
        sb.append(", optimistic=");
        sb.append(this.A01);
        sb.append("]");
        return sb.toString();
    }
}
