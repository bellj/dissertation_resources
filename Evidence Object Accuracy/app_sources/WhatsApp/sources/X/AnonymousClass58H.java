package X;

/* renamed from: X.58H  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58H implements AnonymousClass5UW {
    public static final AnonymousClass58Q A02 = new AnonymousClass58Q();
    public final String A00;
    public final boolean A01;

    public AnonymousClass58H(String str, boolean z) {
        this.A00 = str;
        this.A01 = z;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r3) {
        C16700pc.A0E(r3, 0);
        Object A00 = r3.A00(this.A00);
        if (A00 != null) {
            return A00.equals(Boolean.valueOf(this.A01));
        }
        return false;
    }
}
