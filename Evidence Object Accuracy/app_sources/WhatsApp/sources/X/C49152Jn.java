package X;

/* renamed from: X.2Jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C49152Jn {
    public static final int[] A00 = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    /* JADX WARNING: Code restructure failed: missing block: B:366:0x06d4, code lost:
        r17 = r17 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0076, code lost:
        r8 = X.AnonymousClass39v.A02;
     */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0245  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0263 A[LOOP:8: B:134:0x0261->B:135:0x0263, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0296  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02d8 A[LOOP:12: B:156:0x02d8->B:157:0x02da, LOOP_START, PHI: r0 
      PHI: (r0v230 int) = (r0v37 int), (r0v231 int) binds: [B:155:0x02d6, B:157:0x02da] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:392:0x0753  */
    /* JADX WARNING: Removed duplicated region for block: B:396:0x0777  */
    /* JADX WARNING: Removed duplicated region for block: B:402:0x0024 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:404:0x0186 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:406:0x016d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:415:0x006c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0220 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0084 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0128  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C49162Jo A00(X.EnumC49142Jm r28, java.lang.String r29, java.util.Map r30) {
        /*
        // Method dump skipped, instructions count: 1962
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49152Jn.A00(X.2Jm, java.lang.String, java.util.Map):X.2Jo");
    }

    public static boolean A01(EnumC49142Jm r7, C65243It r8, int i) {
        int i2 = r8.A00;
        AnonymousClass4MU r0 = r8.A03[r7.ordinal()];
        int i3 = r0.A00;
        int i4 = 0;
        for (AnonymousClass4MT r02 : r0.A01) {
            i4 += r02.A00;
        }
        return i2 - (i3 * i4) >= ((i + 7) >> 3);
    }
}
