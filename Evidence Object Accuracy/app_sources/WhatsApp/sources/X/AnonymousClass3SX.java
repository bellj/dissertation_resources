package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.3SX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SX implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView = (RecyclerView) obj;
        List<AbstractC018308n> list = ((C56002kA) obj2).A0A;
        if (list != null) {
            for (AbstractC018308n r0 : list) {
                recyclerView.A0l(r0);
            }
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        List list = ((C56002kA) obj).A0A;
        List list2 = ((C56002kA) obj2).A0A;
        if (list != list2) {
            return list == null || !list.equals(list2);
        }
        return false;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        RecyclerView recyclerView = (RecyclerView) obj;
        List<AbstractC018308n> list = ((C56002kA) obj2).A0A;
        if (list != null) {
            for (AbstractC018308n r0 : list) {
                recyclerView.A0m(r0);
            }
        }
    }
}
