package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0H9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0H9 extends AbstractC08110ab {
    public AnonymousClass0H9() {
        super(Collections.singletonList(new AnonymousClass0U8(Float.valueOf(0.0f))));
    }

    public AnonymousClass0H9(List list) {
        super(list);
    }

    @Override // X.AbstractC12590iA
    public AnonymousClass0QR A88() {
        return new AnonymousClass0H1(this.A00);
    }
}
