package X;

import android.view.accessibility.AccessibilityManager;

/* renamed from: X.0Wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg implements AccessibilityManager.TouchExplorationStateChangeListener {
    public final AbstractC11760go A00;

    public accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg(AbstractC11760go r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg)) {
            return false;
        }
        return this.A00.equals(((accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // android.view.accessibility.AccessibilityManager.TouchExplorationStateChangeListener
    public void onTouchExplorationStateChanged(boolean z) {
        this.A00.onTouchExplorationStateChanged(z);
    }
}
