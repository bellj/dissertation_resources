package X;

import java.util.List;

/* renamed from: X.5kq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122055kq extends C125965s6 {
    public final C30821Yy A00;
    public final AbstractC136536Mx A01;
    public final AbstractC16390ow A02;
    public final String A03;
    public final String A04;
    public final List A05;

    public C122055kq(C30821Yy r2, AbstractC136536Mx r3, AbstractC16390ow r4, String str, String str2, List list) {
        super(6);
        this.A00 = r2;
        this.A04 = str;
        this.A03 = str2;
        this.A01 = r3;
        this.A02 = r4;
        this.A05 = list;
    }
}
