package X;

import android.content.ContentValues;

/* renamed from: X.1Ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29731Ul {
    public static void A00(C16330op r3, String str, String str2, long j) {
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("key", str);
        contentValues.put("value", Long.valueOf(j));
        C29721Uk.A00(str2, "setProp", "REPLACE_PROPS");
        r3.A05(contentValues, "props");
    }
}
