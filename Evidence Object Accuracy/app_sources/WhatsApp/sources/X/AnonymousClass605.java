package X;

import java.io.UnsupportedEncodingException;

/* renamed from: X.605  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass605 {
    public C129215xM A00;
    public C130775zx A01;
    public final C14900mE A02;
    public final C18640sm A03;
    public final C16590pI A04;
    public final C18650sn A05;
    public final C129135xE A06;
    public final AnonymousClass60T A07;
    public final C30931Zj A08 = C117305Zk.A0V("PaymentPinHelper", "network");
    public final AnonymousClass61E A09;
    public final C130015yf A0A;

    public AnonymousClass605(C14900mE r8, C15570nT r9, C18640sm r10, C14830m7 r11, C16590pI r12, C18650sn r13, C18610sj r14, C129135xE r15, AnonymousClass60T r16, AnonymousClass61E r17, C130015yf r18) {
        this.A04 = r12;
        this.A02 = r8;
        this.A06 = r15;
        this.A0A = r18;
        this.A03 = r10;
        this.A05 = r13;
        this.A09 = r17;
        this.A07 = r16;
        this.A01 = new C130775zx(r9, r11, r14);
        this.A00 = new C129215xM(r12.A00, r8, r13, r14, r16, "PIN");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: byte[][] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v28, resolved type: java.lang.Object[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static byte[] A00(Object... objArr) {
        int length = objArr.length;
        byte[][] bArr = new byte[length];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            try {
                if (objArr[i2] == null) {
                    bArr[i2] = new byte[0];
                } else if (objArr[i2] instanceof Long) {
                    bArr[i2] = C117315Zl.A0a(String.valueOf(C12980iv.A0G(objArr[i2])));
                } else if (objArr[i2] instanceof Integer) {
                    bArr[i2] = C117315Zl.A0a(String.valueOf(C12960it.A05(objArr[i2])));
                } else if (objArr[i2] instanceof byte[]) {
                    bArr[i2] = objArr[i2];
                } else if (objArr[i2] instanceof String) {
                    bArr[i2] = C117315Zl.A0a((String) objArr[i2]);
                } else {
                    throw C12970iu.A0f(C30931Zj.A01("PaymentPinHelper", "constructPayload: should only accept long, byte[], and String args"));
                }
                i += bArr[i2].length;
            } catch (UnsupportedEncodingException e) {
                C117305Zk.A1N("PaymentPinHelper", C12960it.A0b(" UTF-8 not supported: ", e));
                throw new Error(e);
            }
        }
        byte[] bArr2 = new byte[i];
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            Object[] objArr2 = bArr[i4];
            System.arraycopy(objArr2, 0, bArr2, i3, objArr2.length);
            i3 += objArr2.length;
        }
        return bArr2;
    }

    public final void A01(AbstractC136296Lz r3, AnonymousClass6M0 r4, String str) {
        AnonymousClass6B7 A0C = C117315Zl.A0C(this.A07, str, "PIN");
        if (A0C == null) {
            this.A00.A00(new C133446Au(r3, r4, this), str);
        } else {
            r3.AVF(new C128545wH(A0C));
        }
    }
}
