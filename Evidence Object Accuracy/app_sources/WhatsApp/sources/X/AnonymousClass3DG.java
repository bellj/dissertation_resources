package X;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.quickcontact.QuickContactActivity;

/* renamed from: X.3DG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DG {
    public float A00 = 1.0f;
    public int A01 = 2;
    public String A02;
    public final C14850m9 A03;
    public final AbstractC14640lm A04;
    public final Integer A05;

    public /* synthetic */ AnonymousClass3DG(C14850m9 r2, AbstractC14640lm r3, Integer num) {
        this.A04 = r3;
        this.A03 = r2;
        this.A05 = num;
    }

    public void A00(Activity activity, View view) {
        int dimensionPixelOffset;
        Intent A0D = C12990iw.A0D(activity, QuickContactActivity.class);
        int[] A07 = C13000ix.A07();
        view.getLocationOnScreen(A07);
        Rect A0J = C12980iv.A0J();
        int i = A07[0];
        float f = this.A00;
        A0J.left = (int) ((((float) i) * f) + 0.5f);
        A0J.top = (int) ((((float) A07[1]) * f) + 0.5f);
        A0J.right = (int) ((((float) (i + view.getWidth())) * f) + 0.5f);
        A0J.bottom = (int) ((((float) (A07[1] + view.getHeight())) * f) + 0.5f);
        A0D.setSourceBounds(A0J);
        if (this.A03.A07(604)) {
            dimensionPixelOffset = -2;
        } else {
            dimensionPixelOffset = activity.getResources().getDimensionPixelOffset(R.dimen.quick_contact_top_position);
        }
        A0D.putExtra("position_top", dimensionPixelOffset);
        Integer num = this.A05;
        if (num != null) {
            A0D.putExtra("profile_entry_point", num);
        }
        String str = this.A02;
        if (str != null) {
            A0D.putExtra("transition_name", str);
        }
        if (C28391Mz.A02()) {
            A0D.putExtra("status_bar_color", activity.getWindow().getStatusBarColor());
        }
        if (C28391Mz.A04()) {
            A0D.putExtra("navigation_bar_color", activity.getWindow().getNavigationBarColor());
        }
        A0D.putExtra("jid", C15380n4.A03(this.A04));
        A0D.putExtra("animation_style", this.A01);
        activity.startActivity(A0D, C018108l.A00().A03());
        activity.overridePendingTransition(0, 0);
    }
}
