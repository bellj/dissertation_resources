package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.Formatter;

/* renamed from: X.2Bd  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Bd {
    public static String A00(Context context, C15610nY r6, C20830wO r7, AnonymousClass1X4 r8, AnonymousClass2Ba r9) {
        int i;
        int i2;
        synchronized (r9) {
            i = r9.A02;
        }
        if (!r9.A08() && i != 9) {
            if (i == 4) {
                boolean A00 = C14950mJ.A00();
                i2 = R.string.insufficient_space_for_download_shared_storage;
                if (A00) {
                    i2 = R.string.insufficient_space_for_download;
                }
            } else {
                if (i == 5) {
                    AbstractC14640lm r1 = r8.A0z.A00;
                    if (C15380n4.A0J(r1) || C15380n4.A0N(r1)) {
                        r1 = r8.A0B();
                    }
                    if (r1 != null) {
                        return context.getString(R.string.too_old_for_download, r6.A04(r7.A01(r1)));
                    }
                } else if (i != 8) {
                    if (i == 0) {
                        return null;
                    }
                }
                i2 = R.string.invalid_url_for_download;
            }
            return context.getString(i2);
        }
        i2 = R.string.unable_to_finish_download;
        return context.getString(i2);
    }

    public static String A01(StringBuilder sb, Formatter formatter, long j) {
        Object[] objArr;
        String str;
        if (j == -9223372036854775807L) {
            j = 0;
        }
        long j2 = (j + 500) / 1000;
        long j3 = j2 % 60;
        long j4 = (j2 / 60) % 60;
        long j5 = j2 / 3600;
        sb.setLength(0);
        if (j5 > 0) {
            objArr = new Object[]{Long.valueOf(j5), Long.valueOf(j4), Long.valueOf(j3)};
            str = "%d:%02d:%02d";
        } else {
            objArr = new Object[]{Long.valueOf(j4), Long.valueOf(j3)};
            str = "%02d:%02d";
        }
        return formatter.format(str, objArr).toString();
    }

    public static void A02(AnonymousClass3IJ r3, WebPagePreviewView webPagePreviewView) {
        webPagePreviewView.A03();
        webPagePreviewView.A0F.animate().cancel();
        webPagePreviewView.A04.animate().cancel();
        webPagePreviewView.A05.animate().cancel();
        webPagePreviewView.A0F.setAlpha(0.0f);
        webPagePreviewView.A04.setAlpha(1.0f);
        webPagePreviewView.A05.setAlpha(0.0f);
        webPagePreviewView.A06.setAlpha(0.0f);
        webPagePreviewView.A0B.setAlpha(0.0f);
        webPagePreviewView.A03();
        webPagePreviewView.setVideoLargeLogo(r3.A01);
    }
}
