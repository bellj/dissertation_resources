package X;

import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.util.concurrent.Callable;

/* renamed from: X.0ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10460ef implements Callable {
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A00;
    public final /* synthetic */ String A01;

    public CallableC10460ef(ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, String str) {
        this.A00 = shortcutInfoCompatSaverImpl;
        this.A01 = str;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return this.A00.A04.get(this.A01);
    }
}
