package X;

import java.io.OutputStream;
import java.util.Arrays;

/* renamed from: X.2AX  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2AX {
    public void A00(OutputStream outputStream) {
        C44641zJ r0;
        if (!(this instanceof C47332Af)) {
            C47312Ad r1 = (C47312Ad) this;
            if (!(r1 instanceof AnonymousClass2AZ)) {
                C32821cn r6 = r1.A00;
                int i = 0;
                byte[][] bArr = {C32781cj.A02, new byte[]{Byte.parseByte(r6.A00)}, r6.A04, r6.A02, r6.A01};
                do {
                    byte[] bArr2 = bArr[i];
                    Arrays.toString(bArr2);
                    outputStream.write(bArr2);
                    i++;
                } while (i < 5);
                return;
            }
            r0 = ((AnonymousClass2AZ) r1).A00;
        } else {
            r0 = ((C47332Af) this).A00;
        }
        r0.A01(outputStream);
    }

    public byte[] A01() {
        byte[] bArr;
        if (!(this instanceof C47332Af)) {
            bArr = ((C47312Ad) this).A03;
        } else {
            bArr = ((C47332Af) this).A02;
        }
        boolean z = false;
        if (bArr != null) {
            z = true;
        }
        AnonymousClass009.A0C("backup-prefix/get-key/key is null", z);
        return bArr;
    }
}
