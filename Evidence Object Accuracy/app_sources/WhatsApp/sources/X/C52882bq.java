package X;

import android.text.TextUtils;
import android.widget.Filter;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ConversationsFragment;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52882bq extends Filter {
    public boolean A00;
    public boolean A01;
    public final /* synthetic */ C36901kp A02;

    public /* synthetic */ C52882bq(C36901kp r1) {
        this.A02 = r1;
    }

    @Override // android.widget.Filter
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        ArrayList A0l;
        Filter.FilterResults filterResults = new Filter.FilterResults();
        this.A00 = true;
        this.A01 = true;
        C28181Ma r4 = new C28181Ma("conversations/filter/performFiltering");
        if (TextUtils.isEmpty(charSequence)) {
            A0l = this.A02.A07.A1B();
        } else {
            A0l = C12960it.A0l();
            C36901kp r10 = this.A02;
            ArrayList A02 = C32751cg.A02(r10.A05, (String) charSequence);
            HashSet A12 = C12970iu.A12();
            ArrayList A0l2 = C12960it.A0l();
            if (!A02.isEmpty()) {
                ArrayList A0x = C12980iv.A0x(A0l2);
                ConversationsFragment conversationsFragment = r10.A07;
                A0l2.add(new AnonymousClass2W4(conversationsFragment.A0l, conversationsFragment.A1H, A02));
                C15250mo r0 = conversationsFragment.A2T;
                r0.A0A = A0x;
                r0.A03(charSequence);
                r0.A04(A02);
            }
            ConversationsFragment conversationsFragment2 = r10.A07;
            C15250mo r8 = conversationsFragment2.A2T;
            AnonymousClass2LB r1 = r10.A00;
            List list = r1.A02;
            if (list == null) {
                list = C12960it.A0l();
                r1.A02 = list;
            }
            r8.A0D = list;
            r8.A00 = 0;
            r8.A01 = 100;
            r4.A00();
            List list2 = (List) conversationsFragment2.A1L.A05(null, r8, null).second;
            r4.A00();
            Iterator it = conversationsFragment2.A1J.A04().iterator();
            while (it.hasNext()) {
                AbstractC14640lm A0b = C12990iw.A0b(it);
                if (C20830wO.A00(A0b, A0l2)) {
                    A12.add(A0b);
                    if (this.A01) {
                        A0l.add(new C1103455e(conversationsFragment2.A0I(R.string.search_section_chats)));
                        this.A01 = false;
                    }
                    A0l.add(new AnonymousClass2WJ(A0b));
                }
            }
            r4.A00();
            Iterator it2 = conversationsFragment2.A1H.A02().iterator();
            while (it2.hasNext()) {
                C15370n3 A0a = C12970iu.A0a(it2);
                if (A0a.A0C != null && !A12.contains(A0a.A0B(AbstractC14640lm.class)) && C20830wO.A00((AbstractC14640lm) C15370n3.A03(A0a, AbstractC14640lm.class), A0l2)) {
                    if (this.A00) {
                        A0l.add(new C1103455e(conversationsFragment2.A0I(R.string.search_section_contacts)));
                        this.A00 = false;
                    }
                    A0l.add(new AnonymousClass2WK(A0a));
                }
            }
            r4.A00();
            ArrayList A0l3 = C12960it.A0l();
            ArrayList A0l4 = C12960it.A0l();
            AnonymousClass009.A05(list2);
            Iterator it3 = list2.iterator();
            while (it3.hasNext()) {
                AbstractC15340mz A0f = C12980iv.A0f(it3);
                AnonymousClass009.A05(A0f.A0z.A00);
                if (A0f.A0v) {
                    A0l3.add(A0f);
                } else {
                    A0l4.add(A0f);
                }
            }
            if (A0l3.size() > 0) {
                A0l.add(new C1103455e(conversationsFragment2.A0I(R.string.search_section_starred_messages)));
            }
            Iterator it4 = A0l3.iterator();
            while (it4.hasNext()) {
                A0l.add(new AnonymousClass2WN(C12980iv.A0f(it4)));
            }
            if (A0l4.size() > 0) {
                A0l.add(new C1103455e(conversationsFragment2.A0I(R.string.search_section_messages)));
            }
            Iterator it5 = A0l4.iterator();
            while (it5.hasNext()) {
                A0l.add(new AnonymousClass2WN(C12980iv.A0f(it5)));
            }
        }
        filterResults.values = A0l;
        filterResults.count = A0l.size();
        r4.A01();
        return filterResults;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        if (r6.A00 <= 0) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006d, code lost:
        if (r6.A1D.A00.getInt("delete_chat_count", 0) < 3) goto L_0x006f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    @Override // android.widget.Filter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void publishResults(java.lang.CharSequence r13, android.widget.Filter.FilterResults r14) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52882bq.publishResults(java.lang.CharSequence, android.widget.Filter$FilterResults):void");
    }
}
