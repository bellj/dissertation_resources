package X;

/* renamed from: X.4VT  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4VT {
    public final int A00;
    public final byte[] A01;

    public AnonymousClass4VT(byte[] bArr, int i) {
        this.A01 = bArr;
        this.A00 = i;
    }

    public String toString() {
        byte[] bArr = this.A01;
        int i = this.A00;
        byte b = bArr[i];
        StringBuilder A0t = C12980iv.A0t(b << 1);
        for (int i2 = 0; i2 < b; i2++) {
            byte b2 = bArr[(i2 << 1) + i + 1];
            char c = '[';
            if (b2 != 0) {
                c = '.';
                if (b2 != 1) {
                    c = '*';
                    if (b2 == 2) {
                        continue;
                    } else if (b2 == 3) {
                        A0t.append((int) bArr[(i2 << 1) + i + 2]);
                        c = ';';
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    continue;
                }
            }
            A0t.append(c);
        }
        return A0t.toString();
    }
}
