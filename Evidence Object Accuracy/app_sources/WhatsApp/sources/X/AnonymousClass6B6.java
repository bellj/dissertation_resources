package X;

import java.security.PublicKey;
import javax.crypto.Cipher;

/* renamed from: X.6B6  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B6 implements AbstractC136276Lx {
    public final byte[] A00;

    public AnonymousClass6B6(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AbstractC136276Lx
    public byte[] A9N(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = this.A00;
        int length = bArr3.length;
        int length2 = bArr.length;
        if (length >= length2) {
            try {
                PublicKey A0p = C117305Zk.A0p(bArr3);
                Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance.init(1, A0p);
                return instance.doFinal(bArr);
            } catch (Exception e) {
                throw C117315Zl.A0J(e);
            }
        } else {
            StringBuilder A0k = C12960it.A0k("PAY: RsaKey encrypt: payload length is ");
            A0k.append(length2);
            A0k.append(" bytes while key can encrypt at most ");
            A0k.append(length);
            throw new AssertionError(C30931Zj.A01("RsaKey", C12960it.A0d(" bytes", A0k)));
        }
    }
}
