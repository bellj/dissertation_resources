package X;

import java.util.Random;

/* renamed from: X.17V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17V {
    public long A00;
    public C14830m7 A01;
    public String A02;
    public Random A03 = new Random();
    public final C30931Zj A04 = C30931Zj.A00("PaymentFieldStats", "notification", "COMMON");

    public AnonymousClass17V(C14830m7 r4) {
        this.A01 = r4;
    }

    public String A00() {
        String str = this.A02;
        if (str != null) {
            return str;
        }
        byte[] bArr = new byte[8];
        this.A03.nextBytes(bArr);
        String A03 = C003501n.A03(bArr);
        this.A02 = A03;
        return A03;
    }

    public void A01() {
        this.A04.A06("PaymentWamEvent timer reset.");
        this.A00 = this.A01.A00();
    }
}
