package X;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.3rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC80173rp extends AnonymousClass50T {
    public static Map zzd = new ConcurrentHashMap();
    public C94994cs zzb = C94994cs.A05;
    public int zzc = -1;

    public static Object A00(Object obj, Method method, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw cause;
            } else if (cause instanceof Error) {
                throw cause;
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public static final String A01(String str) {
        StringBuilder A0h = C12960it.A0h();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                A0h.append("_");
            }
            A0h.append(Character.toLowerCase(charAt));
        }
        return A0h.toString();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v24, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v27, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v29, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public static void A02(AbstractC117135Yq r13, StringBuilder sb, int i) {
        String str;
        boolean equals;
        int i2;
        Method method;
        String A01;
        HashMap A11 = C12970iu.A11();
        HashMap A112 = C12970iu.A11();
        TreeSet treeSet = new TreeSet();
        for (Method method2 : r13.getClass().getDeclaredMethods()) {
            C72453ed.A1P(method2, treeSet, A112, A11);
        }
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (A0x.startsWith("get")) {
                str = A0x.substring(3);
            } else {
                str = A0x;
            }
            if (str.endsWith("List") && !str.endsWith("OrBuilderList") && !str.equals("List")) {
                String A0r = C72453ed.A0r(C72453ed.A0q(str), str.substring(1, str.length() - 4));
                method = (Method) A11.get(A0x);
                if (method != null && method.getReturnType().equals(List.class)) {
                    A01 = A01(A0r);
                    A03(A00(r13, method, new Object[0]), A01, sb, i);
                }
            }
            if (str.endsWith("Map") && !str.equals("Map")) {
                String A0r2 = C72453ed.A0r(C72453ed.A0q(str), str.substring(1, str.length() - 3));
                method = (Method) A11.get(A0x);
                if (method != null && method.getReturnType().equals(Map.class) && !method.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method.getModifiers())) {
                    A01 = A01(A0r2);
                    A03(A00(r13, method, new Object[0]), A01, sb, i);
                }
            }
            int length = str.length();
            if (A112.get(C72453ed.A0s("set", str, length)) != null && (!str.endsWith("Bytes") || !A11.containsKey(C72453ed.A0r("get", str.substring(0, length - 5))))) {
                String A0r3 = C72453ed.A0r(C72453ed.A0q(str), str.substring(1));
                Method method3 = (Method) A11.get(C72453ed.A0s("get", str, length));
                Method method4 = (Method) A11.get(C72453ed.A0s("has", str, length));
                if (method3 != null) {
                    Object A00 = A00(r13, method3, new Object[0]);
                    if (method4 == null) {
                        if (A00 instanceof Boolean) {
                            i2 = C12970iu.A1Y(A00);
                        } else if (A00 instanceof Integer) {
                            i2 = C12960it.A05(A00);
                        } else if (A00 instanceof Float) {
                            i2 = (C72453ed.A02(A00) > 0.0f ? 1 : (C72453ed.A02(A00) == 0.0f ? 0 : -1));
                        } else if (A00 instanceof Double) {
                            i2 = (C72453ed.A00(A00) > 0.0d ? 1 : (C72453ed.A00(A00) == 0.0d ? 0 : -1));
                        } else {
                            if (A00 instanceof String) {
                                equals = A00.equals("");
                            } else if (A00 instanceof AbstractC111925Bi) {
                                equals = A00.equals(AbstractC111925Bi.A00);
                            } else {
                                if (A00 instanceof AbstractC117135Yq) {
                                    if (A00 == ((AbstractC115675Sm) A00).AhS()) {
                                    }
                                } else if (A00 instanceof Enum) {
                                    i2 = ((Enum) A00).ordinal();
                                }
                                A03(A00, A01(A0r3), sb, i);
                            }
                            if (!equals) {
                                A03(A00, A01(A0r3), sb, i);
                            }
                        }
                        if (i2) {
                            A03(A00, A01(A0r3), sb, i);
                        }
                    } else if (C12970iu.A1Y(A00(r13, method4, new Object[0]))) {
                        A03(A00, A01(A0r3), sb, i);
                    }
                }
            }
        }
        C94994cs r3 = ((AbstractC80173rp) r13).zzb;
        if (r3 != null) {
            for (int i3 = 0; i3 < r3.A00; i3++) {
                A03(r3.A04[i3], String.valueOf(r3.A03[i3] >>> 3), sb, i);
            }
        }
    }

    public static final void A03(Object obj, String str, StringBuilder sb, int i) {
        String A00;
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                A03(obj2, str, sb, i);
            }
        } else if (obj instanceof Map) {
            Iterator A0n = C12960it.A0n((Map) obj);
            while (A0n.hasNext()) {
                A03(A0n.next(), str, sb, i);
            }
        } else {
            sb.append('\n');
            int i2 = 0;
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                A00 = AnonymousClass4DW.A00(new C80273rz(((String) obj).getBytes(C93104Zc.A02)));
            } else if (obj instanceof AbstractC111925Bi) {
                sb.append(": \"");
                A00 = AnonymousClass4DW.A00((AbstractC111925Bi) obj);
            } else {
                if (obj instanceof AbstractC80173rp) {
                    sb.append(" {");
                    A02((AnonymousClass50T) obj, sb, i + 2);
                    sb.append("\n");
                    while (i2 < i) {
                        sb.append(' ');
                        i2++;
                    }
                } else if (obj instanceof Map.Entry) {
                    sb.append(" {");
                    Map.Entry entry = (Map.Entry) obj;
                    int i4 = i + 2;
                    A03(entry.getKey(), "key", sb, i4);
                    A03(entry.getValue(), "value", sb, i4);
                    sb.append("\n");
                    while (i2 < i) {
                        sb.append(' ');
                        i2++;
                    }
                } else {
                    sb.append(": ");
                    C12970iu.A1V(obj, sb);
                    return;
                }
                sb.append("}");
                return;
            }
            sb.append(A00);
            sb.append('\"');
        }
    }

    public static Object[] A04(int i) {
        Object[] objArr = new Object[i];
        objArr[0] = "zzc";
        objArr[1] = "zzd";
        objArr[2] = "zze";
        return objArr;
    }

    public static Object[] A05(int i) {
        Object[] objArr = new Object[i];
        objArr[0] = "zzc";
        objArr[1] = "zzd";
        objArr[2] = "zze";
        objArr[3] = "zzf";
        objArr[4] = "zzg";
        return objArr;
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v8, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v18, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v20, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v30, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v37, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v39, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v45, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v55, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v61, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v67, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v69, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v79, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v81, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARN: Type inference failed for: r3v89, types: [X.5Qw, java.lang.Object, X.4Zn] */
    /* JADX WARNING: Unknown variable types count: 15 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A06(int r6) {
        /*
        // Method dump skipped, instructions count: 1496
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC80173rp.A06(int):java.lang.Object");
    }

    @Override // X.AbstractC115675Sm
    public final /* synthetic */ AbstractC117135Yq AhS() {
        return (AnonymousClass50T) A06(6);
    }

    public boolean equals(Object obj) {
        Class<?> cls;
        if (this == obj) {
            return true;
        }
        if (obj == null || (cls = getClass()) != obj.getClass()) {
            return false;
        }
        return C93994b5.A02.A00(cls).Agt(this, obj);
    }

    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        int Agk = C72463ee.A0C(this).Agk(this);
        this.zza = Agk;
        return Agk;
    }

    public String toString() {
        String obj = super.toString();
        StringBuilder A0k = C12960it.A0k("# ");
        A0k.append(obj);
        A02(this, A0k, 0);
        return A0k.toString();
    }
}
