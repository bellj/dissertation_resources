package X;

import android.view.View;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.WaImageView;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;

/* renamed from: X.3kD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75663kD extends AnonymousClass03U {
    public AbstractC75663kD(View view) {
        super(view);
    }

    public void A08(AnonymousClass4K7 r7) {
        if (this instanceof C850340w) {
            C850340w r5 = (C850340w) this;
            C16700pc.A0E(r7, 0);
            AnonymousClass4SX r3 = ((AnonymousClass2wU) r7).A00;
            ((TextView) r5.A02.getValue()).setText(r3.A02);
            View view = r5.A0H;
            view.setOnClickListener(new ViewOnClickCListenerShape2S0200000_I1(r5, 27, r7));
            C42631vX.A00(view);
            CategoryThumbnailLoader categoryThumbnailLoader = r5.A00;
            C44741zT r32 = r3.A00;
            C16700pc.A0B(r32);
            categoryThumbnailLoader.A00(r32, new AnonymousClass5JW(r5), new AnonymousClass5JX(r5), new AnonymousClass5KC(r5));
        } else if (this instanceof C850540y) {
            ((ShimmerFrameLayout) ((C850540y) this).A00.getValue()).A02();
        } else if (this instanceof C850140u) {
            C850140u r33 = (C850140u) this;
            C16700pc.A0E(r7, 0);
            ((TextView) r33.A01.getValue()).setText(((AnonymousClass2wT) r7).A00.A02);
            View view2 = r33.A0H;
            view2.setOnClickListener(new ViewOnClickCListenerShape2S0200000_I1(r33, 26, r7));
            ((WaImageView) r33.A00.getValue()).A01 = true;
            C42631vX.A00(view2);
        } else if (this instanceof C849940s) {
        } else {
            if (!(this instanceof C850240v)) {
                C850040t r52 = (C850040t) this;
                C16700pc.A0E(r7, 0);
                AnonymousClass4SX r1 = ((AnonymousClass2wR) r7).A00;
                ((TextView) r52.A02.getValue()).setText(r1.A02);
                C42631vX.A00(r52.A0H);
                CategoryThumbnailLoader categoryThumbnailLoader2 = r52.A00;
                C44741zT r34 = r1.A00;
                C16700pc.A0B(r34);
                categoryThumbnailLoader2.A00(r34, new AnonymousClass5JK(r52), new AnonymousClass5JL(r52), new AnonymousClass5KA(r52));
                return;
            }
            C850240v r53 = (C850240v) this;
            C16700pc.A0E(r7, 0);
            AnonymousClass4SX r12 = ((AnonymousClass2wS) r7).A00;
            ((TextView) r53.A03.getValue()).setText(r12.A02);
            CategoryThumbnailLoader categoryThumbnailLoader3 = r53.A00;
            C44741zT r35 = r12.A00;
            C16700pc.A0B(r35);
            categoryThumbnailLoader3.A00(r35, new AnonymousClass5JO(r53), new AnonymousClass5JP(r53), new AnonymousClass5KB(r53));
        }
    }
}
