package X;

import java.util.List;

/* renamed from: X.51i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093451i implements AnonymousClass5T5 {
    public AnonymousClass28D A00;
    public final String A01;
    public final List A02;

    public C1093451i(String str, List list) {
        this.A01 = str;
        this.A02 = list;
    }

    @Override // X.AnonymousClass5T5
    public boolean Aej(AnonymousClass28D r3) {
        if (!this.A01.equals(r3.A0H())) {
            return false;
        }
        this.A00 = r3;
        return AnonymousClass08r.A00(r3.A06, this.A02);
    }
}
