package X;

/* renamed from: X.0Rf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05850Rf {
    public final AnonymousClass5I3 A00;
    public final String[] A01;

    public C05850Rf(AnonymousClass5I3 r1, String[] strArr) {
        this.A01 = strArr;
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0036 A[Catch: IOException -> 0x0065, TryCatch #0 {IOException -> 0x0065, blocks: (B:2:0x0000, B:4:0x000b, B:6:0x001a, B:8:0x0022, B:18:0x0036, B:19:0x0039, B:20:0x003e, B:22:0x0043, B:23:0x0046, B:24:0x0055), top: B:29:0x0000 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C05850Rf A00(java.lang.String... r11) {
        /*
            int r10 = r11.length     // Catch: IOException -> 0x0065
            X.0c8[] r9 = new X.C08960c8[r10]     // Catch: IOException -> 0x0065
            X.0f6 r8 = new X.0f6     // Catch: IOException -> 0x0065
            r8.<init>()     // Catch: IOException -> 0x0065
            r7 = 0
        L_0x0009:
            if (r7 >= r10) goto L_0x0055
            r6 = r11[r7]     // Catch: IOException -> 0x0065
            java.lang.String[] r5 = X.AbstractC08850bx.A04     // Catch: IOException -> 0x0065
            r8.A0B()     // Catch: IOException -> 0x0065
            int r4 = r6.length()     // Catch: IOException -> 0x0065
            r3 = 0
            r2 = 0
        L_0x0018:
            if (r3 >= r4) goto L_0x0041
            char r1 = r6.charAt(r3)     // Catch: IOException -> 0x0065
            r0 = 128(0x80, float:1.794E-43)
            if (r1 >= r0) goto L_0x0027
            r0 = r5[r1]     // Catch: IOException -> 0x0065
            if (r0 != 0) goto L_0x0034
            goto L_0x003e
        L_0x0027:
            r0 = 8232(0x2028, float:1.1535E-41)
            if (r1 != r0) goto L_0x002e
            java.lang.String r0 = "\\u2028"
            goto L_0x0034
        L_0x002e:
            r0 = 8233(0x2029, float:1.1537E-41)
            if (r1 != r0) goto L_0x003e
            java.lang.String r0 = "\\u2029"
        L_0x0034:
            if (r2 >= r3) goto L_0x0039
            r8.A0F(r2, r6, r3)     // Catch: IOException -> 0x0065
        L_0x0039:
            r8.A0I(r0)     // Catch: IOException -> 0x0065
            int r2 = r3 + 1
        L_0x003e:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0041:
            if (r2 >= r4) goto L_0x0046
            r8.A0F(r2, r6, r4)     // Catch: IOException -> 0x0065
        L_0x0046:
            r8.A0B()     // Catch: IOException -> 0x0065
            r8.readByte()     // Catch: IOException -> 0x0065
            X.0c8 r0 = r8.A08()     // Catch: IOException -> 0x0065
            r9[r7] = r0     // Catch: IOException -> 0x0065
            int r7 = r7 + 1
            goto L_0x0009
        L_0x0055:
            java.lang.Object r2 = r11.clone()     // Catch: IOException -> 0x0065
            java.lang.String[] r2 = (java.lang.String[]) r2     // Catch: IOException -> 0x0065
            X.5I3 r1 = X.AnonymousClass5I3.A00(r9)     // Catch: IOException -> 0x0065
            X.0Rf r0 = new X.0Rf     // Catch: IOException -> 0x0065
            r0.<init>(r1, r2)     // Catch: IOException -> 0x0065
            return r0
        L_0x0065:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05850Rf.A00(java.lang.String[]):X.0Rf");
    }
}
