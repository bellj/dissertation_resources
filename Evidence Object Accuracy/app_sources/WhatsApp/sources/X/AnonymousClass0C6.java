package X;

import android.content.IntentFilter;

/* renamed from: X.0C6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C6 extends AnonymousClass0Q2 {
    public final AnonymousClass0NW A00;
    public final /* synthetic */ LayoutInflater$Factory2C011505o A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0C6(LayoutInflater$Factory2C011505o r1, AnonymousClass0NW r2) {
        super(r1);
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a2  */
    @Override // X.AnonymousClass0Q2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00() {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C6.A00():int");
    }

    @Override // X.AnonymousClass0Q2
    public IntentFilter A01() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        intentFilter.addAction("android.intent.action.TIME_TICK");
        return intentFilter;
    }

    @Override // X.AnonymousClass0Q2
    public void A04() {
        this.A01.A0V(true);
    }
}
