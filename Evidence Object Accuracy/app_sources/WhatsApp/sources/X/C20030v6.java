package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0v6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20030v6 {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C15550nR A01;
    public final C20020v5 A02;
    public final C14830m7 A03;
    public final C19990v2 A04;
    public final C15600nX A05;
    public final C20000v3 A06;
    public final C16120oU A07;

    public C20030v6(C15550nR r3, C20020v5 r4, C14830m7 r5, C19990v2 r6, C15600nX r7, C20000v3 r8, C16120oU r9) {
        this.A03 = r5;
        this.A04 = r6;
        this.A07 = r9;
        this.A01 = r3;
        this.A06 = r8;
        this.A02 = r4;
        this.A05 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r1 == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC15340mz r5) {
        /*
            r4 = this;
            r0 = 5
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            X.1IS r0 = r5.A0z
            X.0lm r3 = r0.A00
            X.30v r2 = new X.30v
            r2.<init>()
            r2.A02 = r1
            if (r3 == 0) goto L_0x0034
            boolean r0 = X.C15380n4.A0J(r3)
            if (r0 == 0) goto L_0x0034
            X.0nX r1 = r4.A05
            com.whatsapp.jid.GroupJid r3 = (com.whatsapp.jid.GroupJid) r3
            boolean r0 = r1.A0C(r3)
            boolean r1 = r1.A0D(r3)
            if (r0 == 0) goto L_0x0029
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2.A01 = r0
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r2.A00 = r0
        L_0x0034:
            X.0oU r0 = r4.A07
            r0.A07(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20030v6.A00(X.0mz):void");
    }
}
