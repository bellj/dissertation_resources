package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.09f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C019609f extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass04L A00;

    public C019609f(AnonymousClass04L r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent == null || !intent.getBooleanExtra("noConnectivity", false)) {
            this.A00.invalidate();
        }
    }
}
