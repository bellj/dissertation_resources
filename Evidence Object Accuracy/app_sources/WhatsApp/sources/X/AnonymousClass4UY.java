package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.net.tls13.WtCachedPsk;
import com.whatsapp.util.Log;
import com.whatsapp.watls13.WtPersistentSession;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;

/* renamed from: X.4UY  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UY {
    public static AnonymousClass46C A00(AbstractC115365Rg r5, C89634Ks r6, C91654So r7, Throwable th, int i) {
        return new AnonymousClass46C(r5, r6, r7, "Unexpected event type", th, i);
    }

    public static void A01(AnonymousClass584 r2) {
        r2.A0C.A02 = System.currentTimeMillis();
    }

    public static byte[] A02(C63953Dp r3, byte[] bArr) {
        return r3.A01(bArr, AnonymousClass3JS.A08("key", new byte[0], 16), 16);
    }

    public static byte[] A03(AnonymousClass584 r4, byte[] bArr) {
        return r4.A09.A01(bArr, AnonymousClass3JS.A08("iv", new byte[0], 12), 12);
    }

    public void A04(AbstractC115365Rg r18, C89634Ks r19, C91654So r20, int i) {
        String str;
        if (this instanceof AnonymousClass45r) {
            AnonymousClass584 r0 = (AnonymousClass584) r18;
            A01(r0);
            try {
                r0.A0D.A00((byte[]) r19.A00);
            } catch (AnonymousClass1NR e) {
                throw new AnonymousClass46C(r0, r19, r20, "Failed to update transcripts.", e, i);
            }
        } else if (this instanceof C860945q) {
            AnonymousClass584 r02 = (AnonymousClass584) r18;
            if (r19 instanceof AnonymousClass46B) {
                try {
                    A01(r02);
                    AnonymousClass4ER.A00(r02, AnonymousClass4Yk.A00((byte[]) ((AnonymousClass46B) r19).A00), false);
                } catch (AnonymousClass1NR e2) {
                    throw new AnonymousClass46C(r02, r19, r20, e2.ex.getMessage(), e2, i);
                } catch (RuntimeException e3) {
                    throw new AnonymousClass46C(r02, r19, r20, "Server Hello parse error.", e3, i);
                }
            } else {
                throw A00(r02, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860845p) {
            AnonymousClass584 r03 = (AnonymousClass584) r18;
            if (r19 instanceof AnonymousClass46A) {
                try {
                    A01(r03);
                    if (!AnonymousClass3JS.A04(AnonymousClass3JS.A09(r03.A0P, C72463ee.A0a("server_finished", r03.A0U), r03.A0D.A02()), AnonymousClass4Yk.A00((byte[]) r19.A00))) {
                        throw new AnonymousClass46C(r03, r19, r20, "Failed to verify server fin.", AnonymousClass1NR.A00("Failed to verify server fin.", (byte) 80), i);
                    }
                } catch (AnonymousClass1NR e4) {
                    throw new AnonymousClass46C(r03, r19, r20, "Failed to process finished message.", e4, i);
                }
            } else {
                throw A00(r03, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860745o) {
            AnonymousClass584 r04 = (AnonymousClass584) r18;
            A01(r04);
            if (r19 instanceof AnonymousClass469) {
                try {
                    byte[] bytes = "TLS 1.3, server CertificateVerify".getBytes(DefaultCrypto.UTF_8);
                    byte[] bArr = new byte[64];
                    Arrays.fill(bArr, (byte) 32);
                    try {
                        ByteBuffer wrap = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) r19.A00));
                        short s = wrap.getShort();
                        byte[] A1X = C72453ed.A1X(wrap);
                        if (s == 1027) {
                            byte[] A02 = r04.A0D.A02();
                            ByteBuffer allocate = ByteBuffer.allocate(bytes.length + 64 + 1 + A02.length);
                            allocate.put(bArr);
                            allocate.put(bytes);
                            allocate.put((byte) 0);
                            allocate.put(A02);
                            byte[] array = allocate.array();
                            ArrayList A0l = C12960it.A0l();
                            for (AnonymousClass4VN r4 : r04.A0T) {
                                try {
                                    A0l.add(CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(r4.A01)));
                                } catch (CertificateException e5) {
                                    throw new AnonymousClass46C(r04, r19, r20, "Bad certificate", new SSLException(e5), i);
                                }
                            }
                            try {
                                C90524Of r7 = r04.A0J;
                                X509Certificate[] x509CertificateArr = (X509Certificate[]) A0l.toArray(new X509Certificate[0]);
                                String str2 = r04.A0Q;
                                try {
                                    if (r7.A00 == null) {
                                        throw AnonymousClass1NR.A00("Trust Manager cannot be null.", (byte) 80);
                                    } else if (x509CertificateArr == null || x509CertificateArr.length == 0) {
                                        throw AnonymousClass1NR.A00("Null or empty certificates certificates", (byte) 80);
                                    } else {
                                        Signature instance = Signature.getInstance("SHA256withECDSA");
                                        instance.initVerify(x509CertificateArr[0]);
                                        instance.update(array);
                                        boolean verify = instance.verify(A1X);
                                        if (verify) {
                                            try {
                                                r7.A00.checkServerTrusted(x509CertificateArr, "EC");
                                            } catch (CertificateException e6) {
                                                throw AnonymousClass1NR.A01("Certificate could not be verified", e6, (byte) 42);
                                            }
                                        }
                                        AnonymousClass1NQ r42 = new AnonymousClass1NQ(null, "", "", -1);
                                        r42.A01(x509CertificateArr);
                                        boolean verify2 = HttpsURLConnection.getDefaultHostnameVerifier().verify(str2, r42);
                                        if (!verify || !verify2) {
                                            throw new AnonymousClass46C(r04, r19, r20, "Certificates could not be verified.", AnonymousClass1NR.A00("ServerCertificate verify failed.", (byte) 42), i);
                                        }
                                        r04.A0C.A01((Certificate[]) A0l.toArray(new Certificate[0]));
                                        r04.A0b = true;
                                    }
                                } catch (InvalidKeyException unused) {
                                    throw AnonymousClass1NR.A00("Certificate key is invalid.", (byte) 42);
                                } catch (NoSuchAlgorithmException unused2) {
                                    throw AnonymousClass1NR.A00("SHA256withECDSA not found.", (byte) 80);
                                } catch (SignatureException unused3) {
                                    throw AnonymousClass1NR.A00("Signature is invalid", (byte) 42);
                                }
                            } catch (AnonymousClass1NR e7) {
                                throw new AnonymousClass46C(r04, r19, r20, "Certificates verify failed.", e7, i);
                            }
                        } else {
                            StringBuilder A0j = C12960it.A0j("Expected signature scheme ");
                            A0j.append(1027);
                            String A0e = C12960it.A0e(" got ", A0j, s);
                            StringBuilder A0j2 = C12960it.A0j("Expected signature scheme ");
                            A0j2.append(1027);
                            throw new AnonymousClass46C(r04, r19, r20, A0e, AnonymousClass1NR.A00(C12960it.A0e(" got ", A0j2, s), (byte) 80), i);
                        }
                    } catch (AnonymousClass1NR e8) {
                        throw new AnonymousClass46C(r04, r19, r20, "", e8, i);
                    }
                } catch (UnsupportedEncodingException e9) {
                    throw new AnonymousClass46C(r04, r19, r20, "Could not encode context string in UTF-8", C72453ed.A0f(e9), i);
                }
            } else {
                throw A00(r04, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860645n) {
            AnonymousClass584 r05 = (AnonymousClass584) r18;
            if (r19 instanceof AnonymousClass468) {
                try {
                    A01(r05);
                    ByteBuffer wrap2 = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) ((AnonymousClass468) r19).A00));
                    if (wrap2.get() == 0) {
                        byte[] bArr2 = new byte[3];
                        wrap2.get(bArr2);
                        int A00 = AnonymousClass3JS.A00(bArr2);
                        byte[] bArr3 = new byte[A00];
                        wrap2.get(bArr3);
                        ArrayList A0l2 = C12960it.A0l();
                        ByteBuffer wrap3 = ByteBuffer.wrap(bArr3);
                        int i2 = 0;
                        while (i2 < A00) {
                            byte[] bArr4 = new byte[3];
                            wrap3.get(bArr4);
                            int A002 = AnonymousClass3JS.A00(bArr4);
                            byte[] bArr5 = new byte[A002];
                            wrap3.get(bArr5);
                            byte[] bArr6 = new byte[2];
                            wrap3.get(bArr6);
                            int A01 = AnonymousClass3JS.A01(bArr6);
                            byte[] bArr7 = new byte[A01];
                            wrap3.get(bArr7);
                            i2 += A002 + 3 + 2 + A01;
                            A0l2.add(new AnonymousClass4VN(new C94354bf(bArr7), bArr5));
                        }
                        r05.A0T = A0l2;
                        return;
                    }
                    throw new AnonymousClass46C(r05, r19, r20, "Unexpected certificate size", AnonymousClass1NR.A00("Unexpected Message", (byte) 80), i);
                } catch (AnonymousClass1NR e10) {
                    throw new AnonymousClass46C(r05, r19, r20, "Failed to process certificate", e10, i);
                } catch (RuntimeException e11) {
                    throw new AnonymousClass46C(r05, r19, r20, "Failed to parse certificate.", e11, i);
                }
            } else {
                throw A00(r05, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860545m) {
            AnonymousClass584 r06 = (AnonymousClass584) r18;
            A01(r06);
            throw A00(r06, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
        } else if (this instanceof C860445l) {
            AnonymousClass584 r07 = (AnonymousClass584) r18;
            A01(r07);
            if (r19 instanceof AnonymousClass467) {
                try {
                    ByteBuffer wrap4 = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) r19.A00));
                    byte b = wrap4.get();
                    if (b == 1 || b == 0) {
                        byte[] A012 = r07.A09.A01(C72463ee.A0a("server_app_traffic_secret", r07.A0U), AnonymousClass3JS.A08("traffic upd", new byte[0], r07.A02), r07.A02);
                        r07.A0U.put("server_app_traffic_secret", A012);
                        r07.A0U.put("server_app_key", r07.A09.A01(A012, AnonymousClass3JS.A08("key", new byte[0], 16), 16));
                        r07.A0U.put("server_app_iv", r07.A09.A01(A012, AnonymousClass3JS.A08("iv", new byte[0], 12), 12));
                        int i3 = r07.A03 + 1;
                        r07.A03 = i3;
                        AnonymousClass4AG r5 = AnonymousClass4AG.DEBUG;
                        AnonymousClass4ZF.A00(r5, C12960it.A0W(i3, "Updated Server App Traffic Keys : Current Gen = "));
                        AbstractC92904Xx r2 = r07.A0A;
                        if (r2 == null || r2.A00.available() <= 0) {
                            AbstractC116735Wp A003 = r07.A0E.A00();
                            A003.AId(C72463ee.A0a("server_app_key", r07.A0U), C72463ee.A0a("server_app_iv", r07.A0U));
                            r07.A0A = new AnonymousClass46F(A003, r07.A08, r07.A0M);
                            if (b == 1) {
                                byte[] A013 = AnonymousClass4Yk.A01(new byte[]{0}, (byte) 24);
                                r07.A0B.A01(A013, 0, A013.length, (byte) 23);
                                byte[] A004 = AnonymousClass584.A00(r07.A09, r07, "traffic upd", new byte[0], C72463ee.A0a("client_app_traffic_secret", r07.A0U));
                                r07.A0U.put("client_app_traffic_secret", A004);
                                r07.A0U.put("client_app_key", A02(r07.A09, A004));
                                r07.A0U.put("client_app_iv", A03(r07, A004));
                                int i4 = r07.A01 + 1;
                                r07.A01 = i4;
                                AnonymousClass4ZF.A00(r5, C12960it.A0W(i4, "Updated Client App Traffic Keys : Current Gen = "));
                                AbstractC116735Wp A005 = r07.A0E.A00();
                                A005.AId(C72463ee.A0a("client_app_key", r07.A0U), C72463ee.A0a("client_app_iv", r07.A0U));
                                r07.A0B = new AnonymousClass46H(A005, r07.A0N);
                            }
                            if (wrap4.hasRemaining()) {
                                throw new AnonymousClass46C(r07, r19, r20, "Key update message has more than expected bytes.", AnonymousClass1NR.A00("Key update message has more than expected bytes.", (byte) 80), i);
                            }
                            return;
                        }
                        throw new AnonymousClass46C(r07, r19, r20, "Unexpected Messages: Found pending handshake messages", AnonymousClass1NR.A00("Found unprocessed messages in handshake buffer.", (byte) 10), i);
                    }
                    throw new AnonymousClass46C(r07, r19, r20, C12960it.A0e("Invalid key update type ", C12960it.A0h(), b), AnonymousClass1NR.A00(C12960it.A0e("Invalid key update type ", C12960it.A0h(), b), (byte) 10), i);
                } catch (AnonymousClass1NR e12) {
                    throw new AnonymousClass46C(r07, r19, r20, "Receive key update failed.", e12, i);
                }
            } else {
                throw A00(r07, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860345k) {
            AnonymousClass584 r08 = (AnonymousClass584) r18;
            A01(r08);
            if (r19 instanceof AnonymousClass465) {
                try {
                    ByteBuffer wrap5 = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) r19.A00));
                    byte[] bArr8 = new byte[4];
                    wrap5.get(bArr8);
                    long j = 0;
                    if (AnonymousClass3JS.A02(bArr8) != 0) {
                        byte[] bArr9 = new byte[4];
                        wrap5.get(bArr9);
                        byte[] bArr10 = new byte[(short) (((short) (wrap5.get() & 255)) | 0)];
                        wrap5.get(bArr10);
                        byte[] A1X2 = C72453ed.A1X(wrap5);
                        AnonymousClass4VO A006 = new C94354bf(C72453ed.A1X(wrap5)).A00(42);
                        if (A006 != null) {
                            j = AnonymousClass3JS.A02(A006.A01);
                        }
                        r08.A0C.A07.add(new WtCachedPsk(r08.A0Q, AnonymousClass584.A00(r08.A09, r08, "resumption", bArr10, C72463ee.A0a("resumption_master_secret", r08.A0U)), bArr9, bArr8, A1X2, r08.A0C.A00().byteValue(), j));
                        if (!wrap5.hasRemaining()) {
                            AnonymousClass14K r3 = r08.A0L;
                            AnonymousClass1NQ r72 = r08.A0C;
                            synchronized (r3) {
                                AnonymousClass1NP r6 = new AnonymousClass1NP(r3, r72.getId());
                                try {
                                    Map map = r3.A01;
                                    synchronized (map) {
                                        AnonymousClass1NQ r52 = (AnonymousClass1NQ) map.get(r6);
                                        if (r52 == null) {
                                            r52 = new AnonymousClass1NQ(r3, r72.getPeerHost(), r72.getCipherSuite(), r72.getPeerPort());
                                            map.put(r6, r52);
                                        }
                                        r52.A08.put(r72.A00(), r72.getPeerCertificates());
                                        Iterator it = r72.A07.iterator();
                                        while (it.hasNext()) {
                                            r52.A07.add((WtCachedPsk) it.next());
                                        }
                                        AnonymousClass14J r1 = r3.A00;
                                        if (r1 != null) {
                                            r1.A02(new WtPersistentSession(r52.getPeerHost(), r52.getCipherSuite(), r52.A07, r52.A08, r52.getPeerPort()), r6.A01);
                                        }
                                    }
                                } catch (AnonymousClass1NR e13) {
                                    StringBuilder A0h = C12960it.A0h();
                                    A0h.append("Encountered Exception ");
                                    Log.e(C12960it.A0d(e13.toString(), A0h));
                                }
                            }
                            return;
                        }
                        throw new AnonymousClass46C(r08, r19, r20, "New session ticket has excess bytes than expected", AnonymousClass1NR.A00("New session ticket has more bytes than expected.", (byte) 80), i);
                    }
                } catch (AnonymousClass1NR e14) {
                    throw new AnonymousClass46C(r08, r19, r20, "Failed to process new session ticket", e14, i);
                }
            } else {
                throw A00(r08, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860245j) {
            AnonymousClass584 r09 = (AnonymousClass584) r18;
            if (r19 instanceof AnonymousClass464) {
                try {
                    A01(r09);
                    byte[] bArr11 = (byte[]) ((AnonymousClass464) r19).A00;
                    AnonymousClass4ER.A00(r09, AnonymousClass4Yk.A00(bArr11), true);
                    r09.A0c = true;
                    r09.A0f = false;
                    byte[] A022 = r09.A0D.A02();
                    C92694Xb r43 = r09.A0D;
                    r43.A00.reset();
                    r43.A01 = null;
                    r09.A0D.A00(AnonymousClass4Yk.A01(A022, (byte) -2));
                    r09.A0D.A00(bArr11);
                } catch (AnonymousClass1NR e15) {
                    throw new AnonymousClass46C(r09, r19, r20, e15.ex.getMessage(), e15, i);
                } catch (RuntimeException e16) {
                    throw new AnonymousClass46C(r09, r19, r20, "Hello retry parse error.", e16, i);
                }
            } else {
                throw A00(r09, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860145i) {
            AnonymousClass584 r010 = (AnonymousClass584) r18;
            A01(r010);
            if (r19 instanceof AnonymousClass463) {
                try {
                    ByteBuffer wrap6 = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) r19.A00));
                    C94354bf r53 = new C94354bf(C72453ed.A1X(wrap6));
                    AnonymousClass4VO A007 = r53.A00(16);
                    if (!(A007 == null || (str = r010.A0O) == null || str.isEmpty())) {
                        ByteBuffer wrap7 = ByteBuffer.wrap(A007.A01);
                        byte[] bArr12 = new byte[2];
                        wrap7.get(bArr12);
                        int A014 = AnonymousClass3JS.A01(bArr12);
                        int i5 = 0;
                        HashSet A12 = C12970iu.A12();
                        while (i5 < A014) {
                            int i6 = wrap7.get();
                            byte[] bArr13 = new byte[i6];
                            wrap7.get(bArr13);
                            try {
                                A12.add(new String(bArr13, DefaultCrypto.UTF_8));
                                i5 += i6 + 1;
                            } catch (UnsupportedEncodingException e17) {
                                throw new AnonymousClass46C(r010, r19, r20, "Server protocol is not encoded using UTF-8", C72453ed.A0f(e17), i);
                            }
                        }
                        if (!A12.contains(r010.A0O)) {
                            StringBuilder A0h2 = C12960it.A0h();
                            A0h2.append("Server selected wrong supported version ");
                            C12970iu.A1V(A007, A0h2);
                            A0h2.append(" expected: ");
                            throw new AnonymousClass46C(r010, r19, r20, "Server sent unsupported protocol version.", AnonymousClass1NR.A00(C12960it.A0d(r010.A0O, A0h2), (byte) 110), i);
                        }
                    }
                    if (r53.A00(42) != null) {
                        if (!r010.A0Z) {
                            throw new AnonymousClass46C(r010, r19, r20, "Received server early data indication without sending early data.", AnonymousClass1NR.A00("Should not have received early data indication without sending early data.", (byte) 10), i);
                        } else if (r010.A0C.A03 != null) {
                            r010.A0f = true;
                        }
                    }
                    Set set = C88834Hh.A02;
                    HashSet hashSet = new HashSet(r53.A03.keySet());
                    hashSet.removeAll(set);
                    if (hashSet.size() != 0) {
                        throw new AnonymousClass46C(r010, r19, r20, "Unexpected extension provided by the server", AnonymousClass1NR.A00("Unexpected extension provided by the server", (byte) 47), i);
                    } else if (wrap6.hasRemaining()) {
                        throw new AnonymousClass46C(r010, r19, r20, "Encrypted extensions has excess bytes than expected", AnonymousClass1NR.A00("Encrypted extensions has more bytes than expected.", (byte) 80), i);
                    }
                } catch (AnonymousClass1NR e18) {
                    throw new AnonymousClass46C(r010, r19, r20, "Failed to process encrypted extensions", e18, i);
                } catch (RuntimeException e19) {
                    throw new AnonymousClass46C(r010, r19, r20, "Failed to parse encrypted extensions", AnonymousClass1NR.A01(e19.getMessage(), C72453ed.A0w(e19), (byte) 80), i);
                }
            } else {
                throw A00(r010, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C860045h) {
            AnonymousClass584 r011 = (AnonymousClass584) r18;
            AnonymousClass1NQ r62 = r011.A0C;
            r62.A02 = System.currentTimeMillis();
            if (r19 instanceof AnonymousClass461) {
                C92264Ve r9 = (C92264Ve) r19.A00;
                WtCachedPsk wtCachedPsk = r62.A03;
                try {
                    int i7 = r9.A00;
                    if (((long) i7) <= wtCachedPsk.maxEarlyDataSize) {
                        r011.A0B.A01(r9.A02, r9.A01, i7, (byte) 23);
                        return;
                    }
                    throw new AnonymousClass46C(r011, r19, r20, "Data size exceeds early data", AnonymousClass1NR.A00("Data to be written more than early data size", (byte) 80), i);
                } catch (AnonymousClass1NR e20) {
                    throw new AnonymousClass46C(r011, r19, r20, "App write for early data failed.", e20, i);
                }
            } else {
                throw A00(r011, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (this instanceof C859945g) {
            AnonymousClass584 r012 = (AnonymousClass584) r18;
            try {
                byte[] A015 = r012.A0D.A01();
                byte[] A0a = C72463ee.A0a("master_secret", r012.A0U);
                if (A0a != null) {
                    r012.A0U.put("resumption_master_secret", AnonymousClass584.A00(r012.A09, r012, "res master", A015, A0a));
                    AbstractC116735Wp A008 = r012.A0E.A00();
                    A008.AId(C72463ee.A0a("client_app_key", r012.A0U), C72463ee.A0a("client_app_iv", r012.A0U));
                    r012.A0B = new AnonymousClass46H(A008, r012.A0N);
                    AbstractC92904Xx r22 = r012.A0A;
                    if (r22 == null || r22.A00.available() <= 0) {
                        AbstractC116735Wp A009 = r012.A0E.A00();
                        A009.AId(C72463ee.A0a("server_app_key", r012.A0U), C72463ee.A0a("server_app_iv", r012.A0U));
                        r012.A0A = new AnonymousClass46F(A009, r012.A08, r012.A0M);
                        return;
                    }
                    throw new AnonymousClass46C(r012, r19, r20, "Unexpected Messages: Found pending handshake messages", AnonymousClass1NR.A00("Found unprocessed messages in handshake buffer.", (byte) 10), i);
                }
                throw new AnonymousClass46C(r012, r19, r20, "Master Secret is null", AnonymousClass1NR.A00("Master Secret is null", (byte) 80), i);
            } catch (AnonymousClass1NR e21) {
                throw new AnonymousClass46C(r012, r19, r20, "Failed to derive resumption keys", e21, i);
            }
        } else if (this instanceof C859845f) {
            AnonymousClass584 r013 = (AnonymousClass584) r18;
            A01(r013);
            try {
                byte[] A016 = r013.A0D.A01();
                byte[] A03 = r013.A0G.A00.A03(r013.A0m, r013.A0h);
                int i8 = r013.A02;
                byte[] bArr14 = new byte[i8];
                WtCachedPsk wtCachedPsk2 = r013.A0C.A03;
                if (wtCachedPsk2 != null && r013.A0g) {
                    bArr14 = wtCachedPsk2.pskVal;
                }
                byte[] A0010 = r013.A09.A00(new byte[i8], bArr14);
                r013.A0U.put("early_secret", A0010);
                MessageDigest instance2 = MessageDigest.getInstance(r013.A0P);
                byte[] A0011 = AnonymousClass584.A00(r013.A09, r013, "derived", instance2.digest(), A0010);
                r013.A0U.put("derived_secret", A0011);
                byte[] A0012 = r013.A09.A00(A0011, A03);
                r013.A0U.put("handshake_secret", A0012);
                byte[] A0013 = AnonymousClass584.A00(r013.A09, r013, "c hs traffic", A016, A0012);
                r013.A0U.put("client_hs_traffic_secret", A0013);
                byte[] A0014 = AnonymousClass584.A00(r013.A09, r013, "s hs traffic", A016, A0012);
                r013.A0U.put("server_hs_traffic_secret", A0014);
                r013.A0U.put("derived_secret", AnonymousClass584.A00(r013.A09, r013, "derived", instance2.digest(), A0012));
                r013.A0U.put("client_hs_key", A02(r013.A09, A0013));
                r013.A0U.put("client_hs_iv", A03(r013, A0013));
                byte[] A023 = A02(r013.A09, A0014);
                r013.A0U.put("server_hs_key", A023);
                byte[] A032 = A03(r013, A0014);
                r013.A0U.put("server_hs_iv", A032);
                r013.A0U.put("client_finished", AnonymousClass584.A00(r013.A09, r013, "finished", new byte[0], A0013));
                r013.A0U.put("server_finished", AnonymousClass584.A00(r013.A09, r013, "finished", new byte[0], A0014));
                AbstractC116735Wp A0015 = r013.A0E.A00();
                A0015.AId(A023, A032);
                AbstractC92904Xx r23 = r013.A0A;
                if (r23 == null || r23.A00.available() <= 0) {
                    r013.A0A = new AnonymousClass46F(A0015, r013.A08, r013.A0M);
                    return;
                }
                throw new AnonymousClass46C(r013, r19, r20, "Unexpected Messages: Found pending handshake messages", AnonymousClass1NR.A00("Found unprocessed messages in handshake buffer.", (byte) 10), i);
            } catch (AnonymousClass1NR e22) {
                throw new AnonymousClass46C(r013, r19, r20, "Failed in action handshake traffic keys", e22, i);
            } catch (NoSuchAlgorithmException e23) {
                StringBuilder A0h3 = C12960it.A0h();
                A0h3.append(r013.A0P);
                throw new AnonymousClass46C(r013, r19, r20, C12960it.A0d(" algorithm not found", A0h3), C72453ed.A0f(e23), i);
            }
        } else if (this instanceof C859745e) {
            AnonymousClass584 r014 = (AnonymousClass584) r18;
            AnonymousClass1NQ r24 = r014.A0C;
            r24.A02 = System.currentTimeMillis();
            try {
                byte[] A0016 = r014.A09.A00(new byte[r014.A02], r24.A03.pskVal);
                r014.A0U.put("early_secret", A0016);
                byte[] A0017 = AnonymousClass584.A00(r014.A09, r014, "c e traffic", r014.A0D.A01(), A0016);
                r014.A0U.put("client_early_traffic_secret", A0017);
                byte[] A024 = A02(r014.A09, A0017);
                byte[] A017 = r014.A09.A01(A0017, AnonymousClass3JS.A08("iv", new byte[0], 12), 12);
                AbstractC116735Wp A0018 = r014.A0E.A00();
                A0018.AId(A024, A017);
                r014.A0B = new AnonymousClass46H(A0018, r014.A0N);
            } catch (AnonymousClass1NR e24) {
                throw new AnonymousClass46C(r014, r19, r20, "Failed in action early data keys", e24, i);
            }
        } else if (this instanceof C859645d) {
            AnonymousClass584 r015 = (AnonymousClass584) r18;
            A01(r015);
            try {
                byte[] A018 = r015.A0D.A01();
                byte[] A0a2 = C72463ee.A0a("derived_secret", r015.A0U);
                if (A0a2 != null) {
                    byte[] A0019 = r015.A09.A00(A0a2, new byte[r015.A02]);
                    r015.A0U.put("master_secret", A0019);
                    byte[] A0020 = AnonymousClass584.A00(r015.A09, r015, "c ap traffic", A018, A0019);
                    r015.A0U.put("client_app_traffic_secret", A0020);
                    r015.A0U.put("client_app_key", A02(r015.A09, A0020));
                    r015.A0U.put("client_app_iv", A03(r015, A0020));
                    byte[] A0021 = AnonymousClass584.A00(r015.A09, r015, "s ap traffic", A018, A0019);
                    r015.A0U.put("server_app_traffic_secret", A0021);
                    r015.A0U.put("server_app_key", A02(r015.A09, A0021));
                    r015.A0U.put("server_app_iv", A03(r015, A0021));
                    r015.A0U.put("exporter_master_secret", AnonymousClass584.A00(r015.A09, r015, "exp master", A018, A0019));
                    return;
                }
                throw new AnonymousClass46C(r015, r19, r20, "Derived secret not found.", AnonymousClass1NR.A00("Derived secret not found.", (byte) 80), i);
            } catch (AnonymousClass1NR e25) {
                throw new AnonymousClass46C(r015, r19, r20, "Failed to derive app traffic keys", e25, i);
            }
        } else if (this instanceof C859545c) {
            AnonymousClass584 r016 = (AnonymousClass584) r18;
            if (r19 instanceof C861545x) {
                try {
                    ByteBuffer wrap8 = ByteBuffer.wrap(AnonymousClass4Yk.A00((byte[]) r19.A00));
                    if (((short) (((short) (wrap8.get() & 255)) | 0)) == 0) {
                        new C94354bf(C72453ed.A1X(wrap8));
                        r016.A0X = true;
                        return;
                    }
                    throw new AnonymousClass46C(r016, r19, r20, "Certificate context is not expected", AnonymousClass1NR.A00("Certificate context is not expected.", (byte) 10), i);
                } catch (AnonymousClass1NR e26) {
                    throw new AnonymousClass46C(r016, r19, r20, "Failed to process certificate request ", e26, i);
                }
            } else {
                throw A00(r016, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else if (!(this instanceof C859445b)) {
            AnonymousClass584 r017 = (AnonymousClass584) r18;
            if (!(this instanceof C859345a)) {
                A01(r017);
                return;
            }
            A01(r017);
            if (r19 instanceof C861345v) {
                byte[] bArr15 = (byte[]) r19.A00;
                if (bArr15 != null) {
                    try {
                        r017.A07.A60(bArr15, 0, bArr15.length);
                    } catch (IOException e27) {
                        throw new AnonymousClass46C(r017, r19, r20, "App read failed.", e27, i);
                    }
                } else {
                    throw new AnonymousClass46C(r017, r19, r20, "App read failed.", new SSLException("App read failed."), i);
                }
            } else {
                throw A00(r017, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        } else {
            AnonymousClass584 r018 = (AnonymousClass584) r18;
            A01(r018);
            if (r19 instanceof C861445w) {
                C92264Ve r25 = (C92264Ve) r19.A00;
                try {
                    r018.A0B.A01(r25.A02, r25.A01, r25.A00, (byte) 23);
                } catch (AnonymousClass1NR e28) {
                    throw new AnonymousClass46C(r018, r19, r20, "App write failed.", e28, i);
                }
            } else {
                throw A00(r018, r19, r20, AnonymousClass1NR.A00("Unexpected event", (byte) 80), i);
            }
        }
    }
}
