package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.R;

/* renamed from: X.13a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237913a {
    public String A00;
    public boolean A01;
    public final Handler A02 = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A03;
    public final C15570nT A04;
    public final AnonymousClass1GW A05 = new AnonymousClass1GW();
    public final C16240og A06;
    public final C16590pI A07;
    public final C14820m6 A08;
    public final C17220qS A09;

    public C237913a(AbstractC15710nm r3, C15570nT r4, C16240og r5, C16590pI r6, C14820m6 r7, C17220qS r8) {
        this.A07 = r6;
        this.A03 = r3;
        this.A04 = r4;
        this.A09 = r8;
        this.A06 = r5;
        this.A08 = r7;
    }

    public String A00() {
        String str = this.A00;
        if (str != null) {
            return str;
        }
        A01(null);
        String string = this.A08.A00.getString("my_current_status", null);
        if (string == null) {
            return this.A07.A00.getString(R.string.info_default_empty);
        }
        return string;
    }

    public void A01(AnonymousClass1JI r6) {
        if (!this.A01) {
            AnonymousClass1VQ r3 = new AnonymousClass1VQ(this.A03, this.A07, this.A09, new AnonymousClass1VP(this, r6));
            C15570nT r0 = this.A04;
            r0.A08();
            C27631Ih r2 = r0.A05;
            AnonymousClass009.A05(r2);
            r3.A00(r2, 0);
            this.A01 = true;
        }
    }

    public void A02(String str, String str2) {
        this.A00 = str;
        this.A01 = false;
        this.A08.A0t(str, str2);
        this.A02.post(new RunnableBRunnable0Shape1S0100000_I0_1(this.A05, 21));
    }
}
