package X;

import com.whatsapp.stickers.StickerInfoDialogFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.37s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625637s extends AbstractC16350or {
    public final AnonymousClass12V A00;
    public final C18170s1 A01;
    public final AnonymousClass1KS A02;
    public final C235512c A03;
    public final WeakReference A04;

    public C625637s(AnonymousClass12V r2, C18170s1 r3, AnonymousClass1KS r4, StickerInfoDialogFragment stickerInfoDialogFragment, C235512c r6) {
        this.A03 = r6;
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
        this.A04 = C12970iu.A10(stickerInfoDialogFragment);
    }
}
