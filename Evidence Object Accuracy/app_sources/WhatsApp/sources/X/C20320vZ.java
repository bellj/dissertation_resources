package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.0vZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20320vZ {
    public final AbstractC15710nm A00;
    public final C14650lo A01;
    public final C15550nR A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final C20950wa A05;
    public final C14850m9 A06;
    public final AnonymousClass15O A07;

    public C20320vZ(AbstractC15710nm r1, C14650lo r2, C15550nR r3, C16590pI r4, AnonymousClass018 r5, C20950wa r6, C14850m9 r7, AnonymousClass15O r8) {
        this.A06 = r7;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A05 = r6;
        this.A01 = r2;
    }

    public static C30241Wq A00(GroupJid groupJid, String str, String str2, long j) {
        C30241Wq r1 = new C30241Wq(new AnonymousClass1IS(groupJid, "", false), j);
        r1.A0e(groupJid);
        r1.A0Z(2);
        r1.A00 = str;
        r1.A01 = str2;
        return r1;
    }

    public AbstractC15340mz A01(AnonymousClass1IS r9, byte b, long j) {
        AbstractC30271Wt A00 = C30261Ws.A00(r9, b, j);
        if (A00 != null) {
            return A00;
        }
        switch (b) {
            case 0:
                return new C28861Ph(r9, j);
            case 1:
                return new AnonymousClass1X7(r9, j);
            case 2:
                return new C30421Xi(r9, j);
            case 3:
                return new AnonymousClass1X2(r9, j);
            case 4:
                return new C30411Xh(r9, j);
            case 5:
                return new AnonymousClass1XO(r9, j);
            case 6:
            case 7:
            case 17:
            case 18:
            case 34:
            case 35:
            case 38:
            case 39:
            case 40:
            case 41:
            case 47:
            case 48:
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
            case 59:
            case 60:
            case 61:
            case 65:
            default:
                StringBuilder sb = new StringBuilder("FMessageFactory/newFMessage/message type not handled; type=");
                sb.append((int) b);
                AnonymousClass009.A07(sb.toString());
                return new C30281Wu(r9, b, j);
            case 8:
                return new C30401Xg(r9, j);
            case 9:
                return new C16440p1(r9, j);
            case 10:
                return new C30381Xe(r9, j);
            case 11:
                return new C30371Xd(r9, j);
            case 12:
                return new C30361Xc(r9, j);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass1XR(r9, j);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C16590pI r3 = this.A03;
                return new C30351Xb(this.A01, this.A02, r3, this.A04, r9, j);
            case 15:
                return new C30331Wz(r9, (byte) 15, 7, j);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new C30341Xa(r9, j);
            case 19:
                return new AnonymousClass1XZ(r9, j);
            case C43951xu.A01 /* 20 */:
                return new C30061Vy(r9, j);
            case 21:
                return new AnonymousClass1XY(r9, j);
            case 22:
                return new AnonymousClass1XW(r9, j);
            case 23:
                return new AnonymousClass1XV(r9, j);
            case 24:
                return new C28581Od(r9, j);
            case 25:
                return new AnonymousClass1XU(r9, j);
            case 26:
                return new AnonymousClass1XT(r9, j);
            case 27:
                return new C28851Pg(r9, j);
            case 28:
                return new AnonymousClass1XS(r9, j);
            case 29:
                return new AnonymousClass1XQ(r9, j);
            case C25991Bp.A0S /* 30 */:
                return new AnonymousClass1XN(r9, j);
            case 31:
                return new AnonymousClass1XM(r9, j);
            case 32:
                return new AnonymousClass1XL(r9, j);
            case 33:
                return new C30241Wq(r9, j);
            case 36:
                return new AnonymousClass1XK(r9, j);
            case 37:
                return new AnonymousClass1XJ(r9, j);
            case 42:
                return new AnonymousClass1XI(r9, j);
            case 43:
                return new AnonymousClass1XG(r9, j);
            case 44:
                return new AnonymousClass1XF(this.A00, r9, j);
            case 45:
                return new C16380ov(r9, (byte) 45, j);
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                return new AnonymousClass1XE(r9, j);
            case 49:
                return new AnonymousClass1XD(r9, j);
            case 51:
                return new AnonymousClass1XC(r9, j);
            case 52:
                return new C16380ov(r9, (byte) 52, j);
            case 53:
                return new AnonymousClass1XA(r9, j);
            case 54:
                return new C16380ov(r9, (byte) 54, j);
            case 55:
                return new C16380ov(r9, (byte) 55, j);
            case 56:
                return new AnonymousClass1X9(r9, j);
            case 57:
                return new AnonymousClass1X6(r9, j);
            case 58:
                return new AnonymousClass1X5(r9, j);
            case 62:
                return new AnonymousClass1X1(r9, j);
            case 63:
                return new AnonymousClass1X0(r9, j);
            case 64:
                return new C30321Wy(r9, j);
            case 66:
                return new C27671Iq(r9, j);
            case 67:
                return new C27711Iw(r9, j);
            case 68:
                return new C30311Wx(r9, j);
            case 69:
                return new C30291Wv(r9, j);
        }
    }

    public C30331Wz A02(AbstractC15340mz r10, long j) {
        AnonymousClass15O r3 = this.A07;
        AnonymousClass1IS r2 = r10.A0z;
        C30331Wz r32 = new C30331Wz(r3.A02(r2.A00, true), (byte) 15, 7, r10.A0I);
        r32.A0M = r10.A0B();
        r32.A0p = r10.A0R();
        r32.A01 = r2.A01;
        r32.A00 = j;
        r32.A06 = r10.A06();
        return r32;
    }

    public C28861Ph A03(C14320lF r5, AbstractC14640lm r6, AbstractC15340mz r7, String str, List list, long j, boolean z) {
        String str2;
        byte[] bArr;
        C28861Ph r2 = new C28861Ph(this.A07.A02(r6, true), j);
        r2.A0l(str);
        r2.A0v(list);
        if (r5 != null && r5.A0B()) {
            r2.A05 = r5.A0E;
            Integer num = r5.A08;
            if (num == null || num.intValue() != 2 || (str2 = r5.A0A) == null) {
                str2 = r5.A0B;
            }
            r2.A03 = str2;
            r2.A06 = r5.A09;
            r2.A01 = r5.A02;
            if (!z || (bArr = r5.A0I) == null) {
                bArr = r5.A0H;
            }
            r2.A16(bArr);
            if (num != null) {
                r2.A00 = num.intValue();
            }
        }
        A04(r2, r7);
        return r2;
    }

    public void A04(AbstractC15340mz r4, AbstractC15340mz r5) {
        byte[] bArr;
        AbstractC15340mz r1;
        AbstractC15340mz A0E;
        if (r5 != null) {
            C16460p3 A0G = r5.A0G();
            if (A0G == null || !A0G.A04()) {
                bArr = null;
            } else {
                if (!A0G.A05()) {
                    byte[] A06 = A0G.A06();
                    if (A06 == null) {
                        A06 = this.A05.A09(r5);
                    }
                    A0G.A01(A06);
                }
                bArr = A0G.A07();
            }
            if (r5 instanceof AbstractC16400ox) {
                r1 = ((AbstractC16400ox) r5).A7M(r5.A0z);
                r1.A0m = r5.A0m;
                r1.A0L = r5.A0L;
            } else {
                r1 = null;
                if (r5 instanceof C30241Wq) {
                    r1 = r5;
                }
                r4.A0g(r1);
                A0E = r4.A0E();
                if (A0E != null && A0E.A0G() != null) {
                    r4.A0E().A0G().A03(bArr, true);
                    return;
                }
            }
            r1.A0U(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            r1.A0Z(2);
            if (r5.A0y()) {
                r1.A0h(r5.A0F().A00);
            }
            r4.A0g(r1);
            A0E = r4.A0E();
            if (A0E != null) {
            }
        }
    }
}
