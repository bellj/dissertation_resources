package X;

import android.content.Context;
import android.content.res.Resources;
import com.whatsapp.R;
import java.util.Collection;

/* renamed from: X.3Hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64993Hs {
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e7, code lost:
        if (r36.A0C(r5) != false) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f1, code lost:
        if (r9.isChecked() == false) goto L_0x01f3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x02a5  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0131 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0131 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x003e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f4 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01e9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.app.Dialog A00(android.content.Context r25, X.AbstractC116445Vl r26, X.AbstractC35511i9 r27, X.C14900mE r28, X.C16170oZ r29, X.C15550nR r30, X.C15610nY r31, X.AnonymousClass4Q5 r32, X.C14830m7 r33, X.C14820m6 r34, X.AnonymousClass018 r35, X.C15600nX r36, X.AnonymousClass19M r37, X.C14850m9 r38, X.C20710wC r39, X.AnonymousClass11G r40, X.AnonymousClass19J r41, X.AbstractC14440lR r42, java.lang.String r43, java.util.Set r44, boolean r45) {
        /*
        // Method dump skipped, instructions count: 681
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64993Hs.A00(android.content.Context, X.5Vl, X.1i9, X.0mE, X.0oZ, X.0nR, X.0nY, X.4Q5, X.0m7, X.0m6, X.018, X.0nX, X.19M, X.0m9, X.0wC, X.11G, X.19J, X.0lR, java.lang.String, java.util.Set, boolean):android.app.Dialog");
    }

    public static String A01(Context context, C15550nR r8, C15610nY r9, AbstractC14640lm r10, Collection collection) {
        String str;
        C15370n3 r2 = null;
        if (collection.isEmpty()) {
            return null;
        }
        if (r10 != null) {
            r2 = r8.A0B(r10);
        }
        if (collection.size() == 1) {
            AbstractC15340mz A0f = C12980iv.A0f(collection.iterator());
            if (A0f.A0z.A02 || r2 == null) {
                return context.getString(R.string.delete_confirmation_self);
            }
            if (r2.A0K()) {
                AbstractC14640lm A0B = A0f.A0B();
                if (A0B != null) {
                    str = r9.A04(r8.A0B(A0B));
                }
                str = "";
            } else {
                if (r9.A04(r2) != null) {
                    str = r9.A04(r2);
                }
                str = "";
            }
            return C12960it.A0X(context, str, new Object[1], 0, R.string.delete_confirmation);
        }
        Resources resources = context.getResources();
        int size = collection.size();
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, collection.size(), 0);
        return resources.getQuantityString(R.plurals.delete_confirmation_multiple, size, objArr);
    }
}
