package X;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: X.4mG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100654mG implements InputFilter {
    public final int A00;

    public C100654mG(int i) {
        this.A00 = i;
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        int A01 = AnonymousClass2VC.A01(spanned, 0, spanned.length());
        int A012 = AnonymousClass2VC.A01(spanned, i3, i4);
        int A013 = AnonymousClass2VC.A01(charSequence, i, i2);
        int i5 = (this.A00 - A01) + A012;
        if (i5 <= 0) {
            return "";
        }
        if (i5 >= A013) {
            return null;
        }
        return AbstractC32741cf.A01(charSequence, i, i2, i5);
    }
}
