package X;

/* renamed from: X.0ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08170ah implements AbstractC12040hH {
    public final AnonymousClass0H9 A00;
    public final AnonymousClass0H9 A01;
    public final AnonymousClass0H9 A02;
    public final AnonymousClass0H9 A03;
    public final AnonymousClass0H9 A04;
    public final AnonymousClass0HA A05;
    public final C08130ad A06;
    public final AnonymousClass0HB A07;
    public final AbstractC12590iA A08;

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return null;
    }

    public C08170ah(AnonymousClass0H9 r1, AnonymousClass0H9 r2, AnonymousClass0H9 r3, AnonymousClass0H9 r4, AnonymousClass0H9 r5, AnonymousClass0HA r6, C08130ad r7, AnonymousClass0HB r8, AbstractC12590iA r9) {
        this.A06 = r7;
        this.A08 = r9;
        this.A07 = r8;
        this.A01 = r1;
        this.A05 = r6;
        this.A04 = r2;
        this.A00 = r3;
        this.A02 = r4;
        this.A03 = r5;
    }
}
