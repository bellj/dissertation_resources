package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* renamed from: X.63d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315363d implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(41);
    public final CharSequence A00;
    public final CharSequence A01;
    public final CharSequence A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315363d(Parcel parcel) {
        this.A03 = parcel.readString();
        this.A00 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A01 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A02 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
    }

    public C1315363d(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, String str) {
        this.A03 = str;
        this.A00 = charSequence;
        this.A01 = charSequence2;
        this.A02 = charSequence3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A03);
        TextUtils.writeToParcel(this.A00, parcel, i);
        TextUtils.writeToParcel(this.A01, parcel, i);
        TextUtils.writeToParcel(this.A02, parcel, i);
    }
}
