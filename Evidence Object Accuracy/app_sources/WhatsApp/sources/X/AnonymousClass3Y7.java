package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.3Y7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Y7 implements AbstractC28771Oy {
    public final /* synthetic */ AnonymousClass1CW A00;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public AnonymousClass3Y7(AnonymousClass1CW r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        Log.i("statusdownload/status-cancelled");
        AnonymousClass1CW r1 = this.A00;
        r1.A00 = null;
        r1.A01 = null;
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r5, C28781Oz r6) {
        AbstractC16130oV r2;
        Log.i("statusdownload/status-completed");
        AnonymousClass1CW r3 = this.A00;
        while (true) {
            ArrayList arrayList = r3.A03;
            if (arrayList.size() <= 0) {
                r2 = null;
                break;
            }
            r2 = (AbstractC16130oV) arrayList.remove(0);
            AbstractC16130oV r0 = r3.A00;
            if (r0 == null || !r2.A0z.equals(r0.A0z)) {
                C16150oX r02 = r2.A02;
                if (r02 != null && !r02.A0P) {
                    break;
                }
            }
        }
        r3.A00 = null;
        r3.A01 = null;
        if (r2 != null) {
            int i = 0;
            if (C30041Vv.A0o(r2)) {
                i = 3;
            }
            r3.A00(r2, i);
        }
    }
}
