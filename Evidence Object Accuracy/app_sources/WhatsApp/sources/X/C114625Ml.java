package X;

/* renamed from: X.5Ml  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114625Ml extends AnonymousClass1TM {
    public int A00;
    public byte[] A01;

    public C114625Ml(byte[] bArr, int i) {
        this.A01 = AnonymousClass1TT.A02(bArr);
        this.A00 = i;
    }

    public C114625Ml(AbstractC114775Na r3) {
        this.A01 = AnonymousClass5NH.A05(AbstractC114775Na.A00(r3));
        this.A00 = r3.A0B() == 2 ? AnonymousClass5NG.A00(AbstractC114775Na.A01(r3)).A0B() : 12;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(new AnonymousClass5N5(this.A01));
        int i = this.A00;
        if (i != 12) {
            A00.A06(new AnonymousClass5NG((long) i));
        }
        return new AnonymousClass5NZ(A00);
    }
}
