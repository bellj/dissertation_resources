package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52912bt extends FrameLayout {
    public final ViewGroup.MarginLayoutParams A00;
    public final TextView A01 = C12960it.A0I(this, R.id.title);

    public C52912bt(Context context) {
        super(context);
        FrameLayout.inflate(getContext(), R.layout.community_subgroup_header, this);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -2);
        this.A00 = marginLayoutParams;
        setLayoutParams(marginLayoutParams);
    }
}
