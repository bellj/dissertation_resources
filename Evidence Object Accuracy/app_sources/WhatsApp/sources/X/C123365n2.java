package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import com.whatsapp.R;

/* renamed from: X.5n2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123365n2 extends AbstractC118045bB {
    public C27691It A00 = C13000ix.A03();
    public final C16590pI A01;
    public final C14850m9 A02;
    public final AnonymousClass61E A03;
    public final C130105yo A04;

    public C123365n2(C16590pI r2, C14850m9 r3, AnonymousClass61F r4, AnonymousClass61E r5, C130105yo r6) {
        super(r4);
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r6;
        this.A03 = r5;
    }

    public final CharSequence A07(int i, int i2) {
        Context context = this.A01.A00;
        String string = context.getString(R.string.learn_more);
        String A0X = C12960it.A0X(context, string, C12970iu.A1b(), 0, i);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A0X);
        C117325Zm.A04(spannableStringBuilder, new C117535a7(this, i2), A0X, string);
        return spannableStringBuilder;
    }
}
