package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/* renamed from: X.2ZM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZM extends BitmapDrawable {
    public final AbstractC16130oV A00;

    public AnonymousClass2ZM(Resources resources, Bitmap bitmap, AbstractC16130oV r3) {
        super(resources, bitmap);
        this.A00 = r3;
    }

    @Override // android.graphics.drawable.BitmapDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        int i = AbstractC15340mz.A00(this.A00).A06;
        return i <= 0 ? super.getIntrinsicHeight() : i;
    }

    @Override // android.graphics.drawable.BitmapDrawable, android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        int i = AbstractC15340mz.A00(this.A00).A08;
        return i <= 0 ? super.getIntrinsicWidth() : i;
    }
}
