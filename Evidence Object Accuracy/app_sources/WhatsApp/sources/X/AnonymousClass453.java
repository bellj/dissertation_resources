package X;

import android.content.Context;
import android.widget.ImageView;
import com.google.android.gms.maps.GoogleMapOptions;
import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;

/* renamed from: X.453  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass453 extends C618632v {
    public final /* synthetic */ DirectorySetLocationMapActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass453(Context context, GoogleMapOptions googleMapOptions, DirectorySetLocationMapActivity directorySetLocationMapActivity) {
        super(context, googleMapOptions);
        this.A00 = directorySetLocationMapActivity;
    }

    @Override // X.C618632v
    public void A0A(int i) {
        DirectorySetLocationMapActivity directorySetLocationMapActivity;
        ImageView imageView;
        int i2;
        if (i == 0) {
            directorySetLocationMapActivity = this.A00;
            imageView = directorySetLocationMapActivity.A08.A04;
            i2 = R.drawable.btn_compass_mode_tilt;
        } else if (i == 1) {
            DirectorySetLocationMapActivity directorySetLocationMapActivity2 = this.A00;
            directorySetLocationMapActivity2.A08.A04.setImageResource(R.drawable.btn_myl_active);
            directorySetLocationMapActivity2.A08.A0F = true;
            return;
        } else if (i == 2) {
            directorySetLocationMapActivity = this.A00;
            imageView = directorySetLocationMapActivity.A08.A04;
            i2 = R.drawable.btn_myl;
        } else {
            return;
        }
        imageView.setImageResource(i2);
        directorySetLocationMapActivity.A08.A0F = false;
    }
}
