package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.util.ViewOnClickCListenerShape3S0300000_I1;

/* renamed from: X.2hT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54902hT extends AnonymousClass03U {
    public final AnonymousClass2T3 A00;

    public C54902hT(MediaGalleryFragmentBase mediaGalleryFragmentBase, AnonymousClass2T3 r4) {
        super(r4);
        this.A00 = r4;
        if (Build.VERSION.SDK_INT >= 21) {
            r4.setSelector(null);
        }
        r4.setOnClickListener(new ViewOnClickCListenerShape3S0300000_I1(this, mediaGalleryFragmentBase, r4, 10));
        r4.setOnLongClickListener(new View.OnLongClickListener(mediaGalleryFragmentBase, r4) { // from class: X.4nA
            public final /* synthetic */ MediaGalleryFragmentBase A00;
            public final /* synthetic */ AnonymousClass2T3 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                AnonymousClass2T3 r2 = this.A01;
                MediaGalleryFragmentBase mediaGalleryFragmentBase2 = this.A00;
                AbstractC35611iN r0 = r2.A05;
                return r0 != null && mediaGalleryFragmentBase2.A1L(r0, r2);
            }
        });
    }
}
