package X;

import java.util.Map;

/* renamed from: X.3th  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81243th<K, V> extends AnonymousClass5I5<K> {
    public final Map map;

    public C81243th(Map map) {
        this.map = map;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        return map().containsKey(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean isEmpty() {
        return map().isEmpty();
    }

    public Map map() {
        return this.map;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return map().size();
    }
}
