package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.Calendar;
import java.util.List;

/* renamed from: X.5c8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118635c8 extends AnonymousClass02M implements AnonymousClass2TX {
    public Context A00;
    public List A01 = C12960it.A0l();
    public List A02 = C12960it.A0l();

    public C118635c8(Context context) {
        this.A00 = context;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.size();
    }

    @Override // X.AnonymousClass2TX
    public int ABl(int i) {
        return ((AnonymousClass6L2) this.A01.get(i)).count;
    }

    @Override // X.AnonymousClass2TX
    public int ADH() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass2TX
    public long ADI(int i) {
        return -((Calendar) this.A01.get(i)).getTimeInMillis();
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ void ANF(AnonymousClass03U r3, int i) {
        ((C118715cG) r3).A00.setText(this.A01.get(i).toString());
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r3, int i) {
        C122415lQ r32 = (C122415lQ) r3;
        r32.A08((AbstractC125975s7) this.A02.get(i), i);
        if (!((C123175mj) this.A02.get(i)).A02) {
            r32.A00.setVisibility(8);
        }
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ AnonymousClass03U AOh(ViewGroup viewGroup) {
        Context context = this.A00;
        return new C118715cG(C117305Zk.A04(context, LayoutInflater.from(context), viewGroup, R.layout.transaction_history_section_row));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C122415lQ(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payout_history_row_item));
    }

    @Override // X.AnonymousClass2TX
    public boolean AWm(MotionEvent motionEvent, AnonymousClass03U r3, int i) {
        return false;
    }
}
