package X;

import java.nio.ByteBuffer;

/* renamed from: X.5XR  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XR {
    void A7m(AnonymousClass5XR v, int i, int i2, int i3);

    ByteBuffer AB3();

    int AGm();

    long AHP();

    byte AZm(int i);

    int AZr(byte[] bArr, int i, int i2, int i3);

    int AgC(byte[] bArr, int i, int i2, int i3);

    @Override // java.lang.AutoCloseable, X.AnonymousClass5XR
    void close();

    boolean isClosed();
}
