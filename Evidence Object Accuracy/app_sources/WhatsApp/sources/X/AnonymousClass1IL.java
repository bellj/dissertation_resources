package X;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

/* renamed from: X.1IL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IL implements SharedPreferences {
    public int A00;
    public long A01;
    public long A02;
    public Throwable A03;
    public Map A04 = new HashMap();
    public boolean A05;
    public final int A06;
    public final Handler A07 = new Handler(Looper.getMainLooper());
    public final AnonymousClass1IK A08;
    public final AnonymousClass167 A09;
    public final Object A0A = new Object();
    public final Object A0B = new Object();
    public final String A0C = UUID.randomUUID().toString();
    public final Map A0D = new HashMap();

    public AnonymousClass1IL(AnonymousClass1IK r5, AnonymousClass167 r6, int i) {
        this.A08 = r5;
        this.A09 = r6;
        this.A06 = i;
        synchronized (this.A0A) {
            this.A05 = false;
        }
        this.A09.A00(new RunnableBRunnable0Shape9S0100000_I0_9(this, 29), this.A06, false);
    }

    public final void A00() {
        while (!this.A05) {
            try {
                this.A0A.wait();
            } catch (InterruptedException unused) {
            }
        }
        Throwable th = this.A03;
        if (th != null) {
            throw new IllegalStateException(th);
        }
    }

    @Override // android.content.SharedPreferences
    public boolean contains(String str) {
        boolean containsKey;
        synchronized (this.A0A) {
            A00();
            containsKey = this.A04.containsKey(str);
        }
        return containsKey;
    }

    @Override // android.content.SharedPreferences
    public /* bridge */ /* synthetic */ SharedPreferences.Editor edit() {
        synchronized (this.A0A) {
            A00();
        }
        return new AnonymousClass1IM(this);
    }

    @Override // android.content.SharedPreferences
    public Map getAll() {
        HashMap hashMap;
        synchronized (this.A0A) {
            A00();
            hashMap = new HashMap(this.A04);
        }
        return hashMap;
    }

    @Override // android.content.SharedPreferences
    public boolean getBoolean(String str, boolean z) {
        synchronized (this.A0A) {
            A00();
            Boolean bool = (Boolean) this.A04.get(str);
            if (bool != null) {
                z = bool.booleanValue();
            }
        }
        return z;
    }

    @Override // android.content.SharedPreferences
    public float getFloat(String str, float f) {
        synchronized (this.A0A) {
            A00();
            Float f2 = (Float) this.A04.get(str);
            if (f2 != null) {
                f = f2.floatValue();
            }
        }
        return f;
    }

    @Override // android.content.SharedPreferences
    public int getInt(String str, int i) {
        synchronized (this.A0A) {
            A00();
            Integer num = (Integer) this.A04.get(str);
            if (num != null) {
                i = num.intValue();
            }
        }
        return i;
    }

    @Override // android.content.SharedPreferences
    public long getLong(String str, long j) {
        synchronized (this.A0A) {
            A00();
            Long l = (Long) this.A04.get(str);
            if (l != null) {
                j = l.longValue();
            }
        }
        return j;
    }

    @Override // android.content.SharedPreferences
    public String getString(String str, String str2) {
        synchronized (this.A0A) {
            A00();
            String str3 = (String) this.A04.get(str);
            if (str3 != null) {
                str2 = str3;
            }
        }
        return str2;
    }

    @Override // android.content.SharedPreferences
    public Set getStringSet(String str, Set set) {
        synchronized (this.A0A) {
            A00();
            Set set2 = (Set) this.A04.get(str);
            if (set2 != null) {
                set = set2;
            }
        }
        return set;
    }

    @Override // android.content.SharedPreferences
    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        String str = this.A0C;
        Handler handler = this.A07;
        synchronized (this.A0A) {
            Map map = this.A0D;
            AnonymousClass009.A05(str);
            WeakHashMap weakHashMap = (WeakHashMap) map.get(str);
            if (weakHashMap == null) {
                weakHashMap = new WeakHashMap();
                map.put(str, weakHashMap);
            }
            AnonymousClass009.A05(onSharedPreferenceChangeListener);
            AnonymousClass009.A05(handler);
            weakHashMap.put(onSharedPreferenceChangeListener, handler);
        }
    }

    @Override // android.content.SharedPreferences
    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        String str = this.A0C;
        synchronized (this.A0A) {
            Map map = this.A0D;
            AnonymousClass009.A05(str);
            Map map2 = (Map) map.get(str);
            if (map2 != null) {
                AnonymousClass009.A05(onSharedPreferenceChangeListener);
                map2.remove(onSharedPreferenceChangeListener);
            }
        }
    }
}
