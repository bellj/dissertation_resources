package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.27E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27E implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C27631Ih(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C27631Ih[i];
    }
}
