package X;

import com.whatsapp.util.Log;

/* renamed from: X.3ZL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZL implements AbstractC21730xt {
    public final C17200qQ A00;
    public final C29361Rw A01;
    public final /* synthetic */ C17210qR A02;

    public AnonymousClass3ZL(C17200qQ r1, C17210qR r2, C29361Rw r3) {
        this.A02 = r2;
        this.A00 = r1;
        this.A01 = r3;
    }

    public final void A00() {
        int i;
        C17200qQ r4 = this.A00;
        C14850m9 r5 = r4.A0A;
        if (r5.A07(1689)) {
            C12970iu.A1C(r4.A0C.A01("keystore").edit(), "last_failed_auth_key_rotation_attempt", r4.A06.A00());
        }
        if (!r5.A07(1689)) {
            i = 0;
        } else {
            i = r4.A0C.A01("keystore").getInt("remaining_auth_key_rotation_attempts", 0);
        }
        r4.A09(i - 1);
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.w("AuthkeyRotationManager/SetAuthkeyIqResponseCallBack/onDeliveryFailure");
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.w("AuthkeyRotationManager/SetAuthkeyIqResponseCallBack/onError: 500 IQ error");
        A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0056, code lost:
        if (r10 > ((long) r7)) goto L_0x0058;
     */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r16, java.lang.String r17) {
        /*
        // Method dump skipped, instructions count: 268
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3ZL.AX9(X.1V8, java.lang.String):void");
    }
}
