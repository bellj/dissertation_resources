package X;

import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;

/* renamed from: X.3Ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66653Ok implements AnonymousClass02Q {
    public final /* synthetic */ CallsHistoryFragment A00;

    public C66653Ok(CallsHistoryFragment callsHistoryFragment) {
        this.A00 = callsHistoryFragment;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r8) {
        C64503Fu r0;
        if (menuItem.getItemId() != R.id.menuitem_calls_delete) {
            return false;
        }
        ArrayList A0l = C12960it.A0l();
        CallsHistoryFragment callsHistoryFragment = this.A00;
        Iterator it = callsHistoryFragment.A0q.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!TextUtils.isEmpty(A0x)) {
                LinkedHashMap linkedHashMap = callsHistoryFragment.A0g;
                if (!linkedHashMap.isEmpty() && linkedHashMap.containsKey(A0x) && (r0 = (C64503Fu) callsHistoryFragment.A0g.get(A0x)) != null) {
                    A0l.addAll(r0.A03);
                }
            }
        }
        if (!A0l.isEmpty()) {
            callsHistoryFragment.A0L.A0C(A0l);
        }
        callsHistoryFragment.A1A();
        AbstractC009504t r02 = callsHistoryFragment.A01;
        if (r02 == null) {
            return true;
        }
        r02.A05();
        return true;
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r5) {
        menu.add(0, R.id.menuitem_calls_delete, 0, R.string.clear_single_log).setIcon(R.drawable.ic_action_delete).setShowAsAction(2);
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r3) {
        CallsHistoryFragment callsHistoryFragment = this.A00;
        callsHistoryFragment.A1A();
        callsHistoryFragment.A01 = null;
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r9) {
        CallsHistoryFragment callsHistoryFragment = this.A00;
        if (!callsHistoryFragment.A0c()) {
            Log.i("calls/actionmode/fragment is not attached to activity.");
            return false;
        }
        HashSet hashSet = callsHistoryFragment.A0q;
        if (hashSet.isEmpty()) {
            r9.A05();
            return true;
        }
        Locale A14 = C12970iu.A14(callsHistoryFragment.A0K);
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, hashSet.size(), 0);
        r9.A0B(String.format(A14, "%d", objArr));
        AnonymousClass12P.A04(callsHistoryFragment.A0B().findViewById(R.id.action_mode_bar), callsHistoryFragment.A0B().getWindowManager());
        return true;
    }
}
