package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59282uI extends AnonymousClass32K {
    public final AbstractC15710nm A00;
    public final C14650lo A01;
    public final C19830uk A02;
    public final AbstractC116505Vs A03;
    public final C18640sm A04;
    public final AnonymousClass28H A05;
    public final C15680nj A06;
    public final C19870uo A07;
    public final C17220qS A08;
    public final C19840ul A09;

    public C59282uI(AbstractC15710nm r9, C14650lo r10, C19810ui r11, C17560r0 r12, AnonymousClass4RQ r13, C17570r1 r14, C19830uk r15, AbstractC116505Vs r16, C18640sm r17, AnonymousClass28H r18, C15680nj r19, C19870uo r20, C17220qS r21, C19840ul r22, AbstractC14440lR r23) {
        super(r11, r12, r13, r14, r23, 1);
        this.A03 = r16;
        this.A00 = r9;
        this.A09 = r22;
        this.A01 = r10;
        this.A04 = r17;
        this.A02 = r15;
        this.A05 = r18;
        this.A06 = r19;
        this.A08 = r21;
        this.A07 = r20;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r8, JSONObject jSONObject, int i) {
        A04(null, "/onErrorResponse", r8.A00, i, true, false);
    }

    public final void A04(Exception exc, String str, int i, int i2, boolean z, boolean z2) {
        AnonymousClass1Q5 A00;
        Log.e("GetProductCatalogGraphQLService/onError/response-error");
        AnonymousClass28H r2 = this.A05;
        if (r2.A06 == null && (A00 = C19840ul.A00(this.A09)) != null) {
            A00.A07("datasource_catalog");
        }
        if (!A03(r2.A05, i, z)) {
            String A0d = C12960it.A0d(str, C12960it.A0j("GetProductCatalogGraphQLService"));
            if (exc != null) {
                Log.e(A0d, exc);
            } else {
                Log.e(A0d);
            }
            this.A03.AQ9(r2, i2);
            if (!z2) {
                this.A00.AaV(C12960it.A0d(str, C12960it.A0j("GetProductCatalogGraphQLService")), C12960it.A0W(i2, "error_code="), true);
            }
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        A04(iOException, "/delivery-error", -1, -1, false, true);
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e(C12960it.A0b("GetProductCatalogGraphQLServicesendGetBizProductCatalog/direct-connection-error/jid=", userJid));
        this.A03.AQ9(this.A05, 422);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        A04(exc, "/onError", 0, 0, false, false);
    }
}
