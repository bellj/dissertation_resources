package X;

/* renamed from: X.0C8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C8 extends AbstractC05490Pt {
    public final AnonymousClass07M A00;

    public AnonymousClass0C8(AnonymousClass07M r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05490Pt
    public void A01() {
        this.A00.start();
    }

    @Override // X.AbstractC05490Pt
    public void A02() {
        this.A00.stop();
    }
}
