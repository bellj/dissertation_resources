package X;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.location.PlaceInfo;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape2S0300000_I0;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1jA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC36001jA implements LocationListener {
    public int A00;
    public int A01 = -1;
    public int A02;
    public int A03;
    public int A04;
    public Bitmap A05;
    public Location A06;
    public Handler A07;
    public Handler A08;
    public HandlerThread A09;
    public View A0A;
    public View A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public View A0F;
    public View A0G;
    public View A0H;
    public View A0I;
    public View A0J;
    public View A0K;
    public View A0L;
    public View A0M;
    public View A0N;
    public View A0O;
    public View A0P;
    public View A0Q;
    public ImageView A0R;
    public ImageView A0S;
    public ListView A0T;
    public ProgressBar A0U;
    public ProgressBar A0V;
    public TextView A0W;
    public ActivityC000800j A0X;
    public C48232Fc A0Y;
    public AbstractC14640lm A0Z;
    public C36271jb A0a;
    public C92254Vd A0b;
    public RunnableC55512if A0c;
    public C625037m A0d;
    public C52812bj A0e;
    public PlaceInfo A0f;
    public C36011jB A0g;
    public C38721ob A0h;
    public Runnable A0i;
    public Runnable A0j;
    public String A0k;
    public boolean A0l = true;
    public boolean A0m = false;
    public boolean A0n = false;
    public boolean A0o = false;
    public boolean A0p;
    public boolean A0q;
    public boolean A0r = false;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u = true;
    public final AnonymousClass12P A0v;
    public final AbstractC15710nm A0w;
    public final C244615p A0x;
    public final C14900mE A0y;
    public final C15570nT A0z;
    public final C18790t3 A10;
    public final C16170oZ A11;
    public final AnonymousClass130 A12;
    public final C22700zV A13;
    public final AnonymousClass131 A14;
    public final AnonymousClass01d A15;
    public final C14830m7 A16;
    public final C16590pI A17;
    public final C15890o4 A18;
    public final C14820m6 A19;
    public final AnonymousClass018 A1A;
    public final C15650ng A1B;
    public final AnonymousClass19M A1C;
    public final C231510o A1D;
    public final AnonymousClass193 A1E;
    public final C14850m9 A1F;
    public final C253719d A1G;
    public final C18810t5 A1H;
    public final C16030oK A1I;
    public final C244415n A1J;
    public final PlaceInfo A1K = new PlaceInfo();
    public final AnonymousClass3DJ A1L;
    public final WhatsAppLibLoader A1M;
    public final C16630pM A1N;
    public final C252018m A1O;
    public final C252718t A1P;
    public final AbstractC14440lR A1Q;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AbstractC36001jA(AnonymousClass12P r3, AbstractC15710nm r4, C244615p r5, C14900mE r6, C15570nT r7, C18790t3 r8, C16170oZ r9, AnonymousClass130 r10, C22700zV r11, AnonymousClass131 r12, AnonymousClass01d r13, C14830m7 r14, C16590pI r15, C15890o4 r16, C14820m6 r17, AnonymousClass018 r18, C15650ng r19, AnonymousClass19M r20, C231510o r21, AnonymousClass193 r22, C14850m9 r23, C253719d r24, C18810t5 r25, C16030oK r26, C244415n r27, AnonymousClass3DJ r28, WhatsAppLibLoader whatsAppLibLoader, C16630pM r30, C252018m r31, C252718t r32, AbstractC14440lR r33) {
        this.A17 = r15;
        this.A16 = r14;
        this.A1F = r23;
        this.A1G = r24;
        this.A0y = r6;
        this.A1P = r32;
        this.A0w = r4;
        this.A0z = r7;
        this.A1Q = r33;
        this.A10 = r8;
        this.A1C = r20;
        this.A11 = r9;
        this.A1D = r21;
        this.A0v = r3;
        this.A1J = r27;
        this.A12 = r10;
        this.A15 = r13;
        this.A1A = r18;
        this.A1O = r31;
        this.A1B = r19;
        this.A1L = r28;
        this.A1M = whatsAppLibLoader;
        this.A1E = r22;
        this.A13 = r11;
        this.A1H = r25;
        this.A18 = r16;
        this.A19 = r17;
        this.A0x = r5;
        this.A1I = r26;
        this.A1N = r30;
        this.A14 = r12;
    }

    public static LatLngBounds A00(List list) {
        AnonymousClass3EP r13 = new AnonymousClass3EP();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            r13.A01((LatLng) it.next());
        }
        LatLngBounds A00 = r13.A00();
        LatLng latLng = A00.A00;
        double d = latLng.A00;
        LatLng latLng2 = A00.A01;
        double d2 = latLng2.A00;
        double d3 = latLng.A01;
        double d4 = latLng2.A01;
        double d5 = (d - d2) / 10.0d;
        double d6 = (d3 - d4) / 10.0d;
        r13.A01(new LatLng(d + d5, d3 + d6));
        r13.A01(new LatLng(d2 - d5, d4 - d6));
        return r13.A00();
    }

    public int A01() {
        if (!(this instanceof AnonymousClass32x)) {
            C618732w r0 = (C618732w) this;
            Location A03 = r0.A03();
            AnonymousClass04Q r02 = r0.A01.A03;
            if (r02 == null || A03 == null) {
                return 0;
            }
            C05430Pn A06 = r02.A0R.A06();
            Location location = new Location("");
            AnonymousClass03T r7 = A06.A02;
            double d = r7.A00;
            AnonymousClass03T r6 = A06.A03;
            location.setLatitude((d + r6.A00) / 2.0d);
            location.setLongitude((r7.A01 + r6.A01) / 2.0d);
            return (int) A03.distanceTo(location);
        }
        AnonymousClass3FV r03 = ((AnonymousClass32x) this).A01.A0Q;
        if (r03 == null) {
            return 0;
        }
        r03.A00();
        return 0;
    }

    public Dialog A02(int i) {
        ActivityC000800j r1;
        int i2;
        ActivityC000800j r4;
        int i3;
        Uri A03;
        if (i != 2) {
            int i4 = 3;
            if (i != 3) {
                i4 = 4;
                if (i != 4) {
                    return null;
                }
            }
            View inflate = this.A0X.getLayoutInflater().inflate(R.layout.location_new_user_dialog, (ViewGroup) null, false);
            ImageView imageView = (ImageView) AnonymousClass028.A0D(inflate, R.id.header_logo);
            if (i4 == 3) {
                r1 = this.A0X;
                i2 = R.string.share_live_location;
            } else if (i4 == 4) {
                r1 = this.A0X;
                i2 = R.string.send_this_location;
            } else {
                StringBuilder sb = new StringBuilder("Unknown new user dialog type: ");
                sb.append(i4);
                throw new IllegalArgumentException(sb.toString());
            }
            imageView.setContentDescription(r1.getString(i2));
            int i5 = R.drawable.nux_live_location;
            if (i4 != 3) {
                if (i4 == 4) {
                    i5 = R.drawable.nux_location;
                } else {
                    StringBuilder sb2 = new StringBuilder("Unknown new user dialog type: ");
                    sb2.append(i4);
                    throw new IllegalArgumentException(sb2.toString());
                }
            }
            imageView.setImageResource(i5);
            TextEmojiLabel textEmojiLabel = (TextEmojiLabel) inflate.findViewById(R.id.location_new_user_description);
            Context context = this.A17.A00;
            C14900mE r12 = this.A0y;
            AnonymousClass12P r11 = this.A0v;
            AnonymousClass01d r14 = this.A15;
            if (i4 == 3) {
                r4 = this.A0X;
                boolean A07 = this.A1F.A07(332);
                i3 = R.string.live_location_first_use_dialog_description;
                if (A07) {
                    i3 = R.string.live_location_new_user_dialog_description;
                }
            } else if (i4 == 4) {
                r4 = this.A0X;
                i3 = R.string.nearby_location_new_user_dialog_description;
            } else {
                StringBuilder sb3 = new StringBuilder("Unknown new user dialog type: ");
                sb3.append(i4);
                throw new IllegalArgumentException(sb3.toString());
            }
            String string = r4.getString(i3, "learn-more");
            C14850m9 r42 = this.A1F;
            boolean A072 = r42.A07(332);
            C252018m r7 = this.A1O;
            if (A072) {
                Uri.Builder A01 = r7.A01();
                A01.appendPath("android");
                A01.appendPath("chats");
                A01.appendPath("how-to-use-location-features");
                r7.A05(A01);
                A03 = A01.build();
            } else {
                A03 = r7.A03("26000049");
            }
            C42971wC.A08(context, A03, r11, r12, textEmojiLabel, r14, string, "learn-more");
            C004802e r6 = new C004802e(this.A0X);
            r6.setView(inflate);
            r6.A0B(true);
            r6.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(i4) { // from class: X.4gd
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i6) {
                    AbstractC36001jA r2 = AbstractC36001jA.this;
                    C36021jC.A00(r2.A0X, this.A00);
                    if (r2.A0o) {
                        r2.A08();
                    }
                }
            });
            r6.A08(new DialogInterface.OnCancelListener() { // from class: X.4f7
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    AbstractC36001jA r13 = AbstractC36001jA.this;
                    if (r13.A0o) {
                        r13.A08();
                    }
                }
            });
            boolean A073 = r42.A07(332);
            int i6 = R.string.btn_continue;
            if (A073) {
                i6 = R.string.agree;
            }
            r6.setPositiveButton(i6, new DialogInterface.OnClickListener(i4) { // from class: X.3KT
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i7) {
                    AbstractC36001jA r3 = AbstractC36001jA.this;
                    int i8 = this.A00;
                    C36021jC.A00(r3.A0X, i8);
                    if (i8 == 3) {
                        r3.A19.A13(false);
                        r3.A0T(true);
                    } else if (i8 == 4) {
                        C12960it.A0t(C12960it.A08(r3.A19), "nearby_location_new_user", false);
                        Runnable runnable = r3.A0i;
                        AnonymousClass009.A05(runnable);
                        runnable.run();
                    }
                }
            });
            return r6.create();
        }
        DialogInterface$OnClickListenerC96704fs r2 = new DialogInterface.OnClickListener() { // from class: X.4fs
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i7) {
                AbstractC36001jA r3 = AbstractC36001jA.this;
                r3.A0X.startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 0);
                C36021jC.A00(r3.A0X, 2);
            }
        };
        C004802e r13 = new C004802e(this.A0X);
        r13.A07(R.string.gps_required_title);
        r13.A06(R.string.gps_required_body);
        r13.A0B(true);
        r13.setPositiveButton(R.string.ok, r2);
        return r13.create();
    }

    public Location A03() {
        if (!(this instanceof AnonymousClass32x)) {
            AnonymousClass04Q r0 = ((C618732w) this).A01.A03;
            if (r0 == null) {
                return null;
            }
            AnonymousClass03T r3 = r0.A02().A03;
            Location location = new Location("");
            location.setLatitude(r3.A00);
            location.setLongitude(r3.A01);
            return location;
        }
        AnonymousClass3FV r02 = ((AnonymousClass32x) this).A01.A0Q;
        if (r02 != null) {
            return r02.A01();
        }
        return null;
    }

    public void A04() {
        if (!(this instanceof AnonymousClass32x)) {
            LocationPicker locationPicker = ((C618732w) this).A01;
            AnonymousClass04Q r1 = locationPicker.A03;
            if (r1 != null) {
                locationPicker.A07 = null;
                r1.A06();
                return;
            }
            return;
        }
        LocationPicker2 locationPicker2 = ((AnonymousClass32x) this).A01;
        C35961j4 r12 = locationPicker2.A02;
        if (r12 != null) {
            locationPicker2.A06 = null;
            r12.A06();
        }
    }

    public void A05() {
        C36011jB r0;
        C36011jB r02;
        if (!(this instanceof AnonymousClass32x)) {
            C618732w r1 = (C618732w) this;
            LocationPicker locationPicker = r1.A01;
            if (locationPicker.A03 != null) {
                if (!r1.A0s && locationPicker.A07 == null) {
                    r1.A04();
                }
                if (!r1.A0s && (r02 = r1.A0g) != null) {
                    Iterator it = r02.A08.iterator();
                    while (it.hasNext()) {
                        PlaceInfo placeInfo = (PlaceInfo) it.next();
                        C06050Rz r6 = new C06050Rz();
                        r6.A01 = new AnonymousClass03T(placeInfo.A01, placeInfo.A02);
                        if (!TextUtils.isEmpty(placeInfo.A06)) {
                            r6.A03 = placeInfo.A06;
                        }
                        if (!TextUtils.isEmpty(placeInfo.A0B)) {
                            r6.A02 = placeInfo.A0B;
                        }
                        r6.A00 = locationPicker.A05;
                        float[] fArr = r6.A04;
                        fArr[0] = 0.5f;
                        fArr[1] = 0.5f;
                        AnonymousClass03R A03 = locationPicker.A03.A03(r6);
                        A03.A0L = placeInfo;
                        placeInfo.A0D = A03;
                    }
                    return;
                }
                return;
            }
            return;
        }
        AnonymousClass32x r12 = (AnonymousClass32x) this;
        LocationPicker2 locationPicker2 = r12.A01;
        if (locationPicker2.A02 != null) {
            if (!r12.A0s && locationPicker2.A06 == null) {
                r12.A04();
            }
            if (!r12.A0s && (r0 = r12.A0g) != null) {
                Iterator it2 = r0.A08.iterator();
                while (it2.hasNext()) {
                    PlaceInfo placeInfo2 = (PlaceInfo) it2.next();
                    C56482kx r62 = new C56482kx();
                    r62.A08 = new LatLng(placeInfo2.A01, placeInfo2.A02);
                    if (!TextUtils.isEmpty(placeInfo2.A06)) {
                        r62.A09 = placeInfo2.A06;
                    }
                    if (!TextUtils.isEmpty(placeInfo2.A0B)) {
                        r62.A0A = placeInfo2.A0B;
                    }
                    r62.A07 = locationPicker2.A04;
                    r62.A00 = 0.5f;
                    r62.A01 = 0.5f;
                    C36311jg A032 = locationPicker2.A02.A03(r62);
                    A032.A07(placeInfo2);
                    placeInfo2.A0D = A032;
                }
            }
        }
    }

    public void A06() {
        Handler handler = this.A08;
        if (handler != null) {
            handler.removeCallbacks(this.A0j);
        }
        C625037m r1 = this.A0d;
        if (r1 != null) {
            r1.A03(true);
            this.A0d = null;
        }
        this.A0h.A02.A02(false);
        C36271jb r2 = this.A0a;
        r2.A01.getViewTreeObserver().removeGlobalOnLayoutListener(r2.A02);
        r2.A07.A08();
        this.A09.quit();
    }

    public void A07() {
        if (this.A0o || this.A0s) {
            A0T(false);
        }
        this.A0x.A05(this, "location-picker-onresume", 0.0f, 3, 5000, 1000);
        A0P(null, false);
        System.currentTimeMillis();
    }

    public final void A08() {
        this.A0s = false;
        boolean z = this.A0o;
        ActivityC000800j r0 = this.A0X;
        if (z) {
            r0.finish();
            return;
        }
        View currentFocus = r0.getCurrentFocus();
        if (currentFocus != null) {
            this.A1P.A01(currentFocus);
        }
        if (this.A0A != null) {
            this.A0J.clearAnimation();
            if (this.A0J.getVisibility() == 0) {
                A0O(null, false);
                C74003h9 r2 = new C74003h9(this);
                r2.setDuration(350);
                r2.setAnimationListener(new C83293x0(this));
                r2.setInterpolator(new AccelerateInterpolator());
                this.A0J.startAnimation(r2);
            } else {
                this.A0J.setVisibility(8);
                A0G(0);
            }
            if (this.A0b != null) {
                this.A0A.clearAnimation();
                int visibility = this.A0A.getVisibility();
                View view = this.A0A;
                if (visibility != 0) {
                    view.setVisibility(0);
                    A0O(null, false);
                    C92254Vd r3 = this.A0b;
                    C58132oC r1 = new C58132oC(this);
                    C73983h7 r22 = new C73983h7(r3);
                    r22.setAnimationListener(new C83503xL(r1, r3));
                    r22.setDuration(400);
                    r22.setInterpolator(new AccelerateInterpolator());
                    r3.A01.startAnimation(r22);
                    return;
                }
                view.setVisibility(0);
                if (this.A0A.getHeight() == 0) {
                    this.A0A.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66403Nl(this));
                    return;
                }
                C92254Vd r23 = this.A0b;
                r23.A02.ATC((float) this.A0A.getHeight());
                A0J(A03(), null, A01(), false);
                A0P(null, true);
                return;
            }
            return;
        }
        A0O(null, false);
        A0J(A03(), null, A01(), false);
        A0P(null, true);
    }

    public final void A09() {
        int indexOf;
        PlaceInfo placeInfo = this.A0f;
        if (placeInfo != null && (indexOf = this.A0g.A08.indexOf(placeInfo)) >= 0) {
            A0D();
            ListView listView = this.A0T;
            listView.smoothScrollToPosition(indexOf + listView.getHeaderViewsCount());
        }
    }

    public final void A0A() {
        if (!this.A1F.A07(332) || !this.A19.A00.getBoolean("nearby_location_new_user", true)) {
            if (this.A0t) {
                Intent intent = new Intent();
                intent.putExtra("locations_string", this.A0k);
                PlaceInfo placeInfo = this.A1K;
                intent.putExtra("longitude", placeInfo.A02);
                intent.putExtra("latitude", placeInfo.A01);
                this.A0X.setResult(-1, intent);
            } else {
                Location location = this.A06;
                AbstractC15340mz r2 = null;
                if (location != null && location.getAccuracy() > 200.0f) {
                    location = null;
                }
                long longExtra = this.A0X.getIntent().getLongExtra("quoted_message_row_id", 0);
                C15580nU A04 = C15580nU.A04(this.A0X.getIntent().getStringExtra("quoted_group_jid"));
                if (longExtra > 0) {
                    r2 = this.A1B.A0K.A00(longExtra);
                } else if (A04 != null) {
                    r2 = C20320vZ.A00(A04, null, null, this.A16.A00());
                }
                AbstractC14640lm r12 = this.A0Z;
                if (r12 != null) {
                    C16170oZ r6 = this.A11;
                    AnonymousClass009.A05(r12);
                    boolean booleanExtra = this.A0X.getIntent().getBooleanExtra("has_number_from_url", false);
                    C20320vZ r9 = r6.A1G;
                    C14830m7 r0 = r6.A0R;
                    AnonymousClass1XO r8 = new AnonymousClass1XO(r9.A07.A02(r12, true), r0.A00());
                    if (location != null) {
                        ((AnonymousClass1XP) r8).A00 = location.getLatitude();
                        ((AnonymousClass1XP) r8).A01 = location.getLongitude();
                    }
                    r8.A0Y(1);
                    r9.A04(r8, r2);
                    if (booleanExtra) {
                        r8.A0T(4);
                    }
                    r6.A0M(r8);
                    C15650ng r14 = r6.A0f;
                    r14.A0Z(r8, 2);
                    AbstractC15710nm r02 = r6.A02;
                    C18790t3 r15 = r6.A0A;
                    AnonymousClass12H r13 = r6.A0n;
                    C18810t5 r122 = r6.A0z;
                    C15890o4 r11 = r6.A0T;
                    AnonymousClass24M r22 = new AnonymousClass24M(r6.A01, r02, r6.A04, r15, r0, r11, r14, r6.A0j, r13, r6.A0o, r122, r6.A10, r8);
                    r22.A01 = 15;
                    r6.A1Q.Aaz(r22, new Void[0]);
                }
                this.A0X.setResult(-1);
            }
            this.A0X.finish();
            return;
        }
        this.A0i = new RunnableBRunnable0Shape7S0100000_I0_7(this, 36);
        C36021jC.A01(this.A0X, 4);
    }

    public final void A0B() {
        String str;
        C36011jB r2 = this.A0g;
        if (r2 == null || r2.A08.isEmpty()) {
            str = null;
        } else if (r2.A0E == 3) {
            str = this.A0X.getString(R.string.location_data_provided_by_fousquare, "<a href='https://foursquare.com/'>foursquare</a>");
        } else {
            str = r2.A03;
        }
        this.A0M.setVisibility(8);
        if (str == null || this.A0n) {
            this.A0W.setVisibility(8);
            return;
        }
        this.A0W.setText(Html.fromHtml(str));
        this.A0W.setVisibility(0);
    }

    public final void A0C() {
        View findViewById;
        String A0I;
        if (!this.A0m) {
            findViewById = this.A0X.findViewById(R.id.location_accuracy);
        } else if (!this.A0n) {
            findViewById = this.A0G.findViewById(R.id.location_description);
        } else {
            return;
        }
        TextView textView = (TextView) findViewById;
        if (textView != null) {
            if (this.A0t && !TextUtils.isEmpty(this.A0k)) {
                textView.setVisibility(0);
                A0I = this.A0k;
            } else if (this.A0t || this.A01 <= 0) {
                textView.setVisibility(8);
                return;
            } else {
                textView.setVisibility(0);
                AnonymousClass018 r6 = this.A1A;
                int i = this.A01;
                A0I = r6.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.location_accuracy, (long) i);
            }
            textView.setText(A0I);
        }
    }

    public final void A0D() {
        ArrayList arrayList;
        C52812bj r1 = this.A0e;
        C36011jB r0 = this.A0g;
        if (r0 != null) {
            arrayList = r0.A08;
        } else {
            arrayList = null;
        }
        r1.A01 = arrayList;
        r1.A00 = this.A0f;
        r1.notifyDataSetChanged();
    }

    public void A0E(double d, double d2) {
        PlaceInfo placeInfo = this.A1K;
        placeInfo.A01 = d;
        placeInfo.A02 = d2;
        placeInfo.A06 = null;
        placeInfo.A04 = null;
        if ((this.A0n && !this.A0r) || (this.A0t && TextUtils.isEmpty(this.A0k))) {
            if (this.A0f == null && this.A0m) {
                this.A0B.setVisibility(0);
            }
            this.A07.removeCallbacks(this.A0c);
            RunnableC55512if r1 = new RunnableC55512if(this, d, d2);
            this.A0c = r1;
            this.A07.post(r1);
        }
    }

    public void A0F(int i) {
        if (!(this instanceof AnonymousClass32x)) {
            AnonymousClass04Q r1 = ((C618732w) this).A01.A03;
            if (r1 != null) {
                r1.A08(0, 0, i);
                return;
            }
            return;
        }
        C35961j4 r12 = ((AnonymousClass32x) this).A01.A02;
        if (r12 != null) {
            r12.A08(0, 0, 0, i);
        }
    }

    public final void A0G(int i) {
        this.A02 = i;
        int max = Math.max(this.A00, i);
        this.A0K.setPadding(0, 0, 0, max);
        this.A0K.requestLayout();
        A0F(max);
    }

    public void A0H(Intent intent) {
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            A0J(A03(), intent.getStringExtra("query"), Math.max(A01(), 50000), true);
        }
    }

    public void A0I(Location location, Float f, int i, boolean z) {
        float floatValue;
        if (!(this instanceof AnonymousClass32x)) {
            C618732w r4 = (C618732w) this;
            LocationPicker locationPicker = r4.A01;
            if (locationPicker.A03 != null && location != null) {
                AnonymousClass03T r6 = new AnonymousClass03T(location.getLatitude(), location.getLongitude());
                float f2 = locationPicker.A03.A02().A02;
                if (f == null) {
                    floatValue = 0.0f;
                } else {
                    floatValue = f.floatValue();
                }
                locationPicker.A03.A08(0, 0, i);
                C05200Oq A01 = AnonymousClass0R9.A01(r6, f2 + floatValue);
                AnonymousClass04Q r2 = locationPicker.A03;
                if (z) {
                    r2.A0B(A01, r4.A00, 400);
                } else {
                    r2.A0A(A01);
                }
            }
        } else {
            AnonymousClass32x r1 = (AnonymousClass32x) this;
            AnonymousClass3FV r5 = r1.A01.A0Q;
            if (r5 != null) {
                r5.A03(location, r1.A00, f, Integer.valueOf(i), z);
            }
        }
    }

    public final void A0J(Location location, String str, int i, boolean z) {
        ProgressBar progressBar;
        this.A08.removeCallbacks(this.A0j);
        if (this.A0n) {
            progressBar = this.A0V;
        } else {
            progressBar = this.A0U;
        }
        progressBar.setVisibility(0);
        this.A0f = null;
        A04();
        this.A0X.findViewById(R.id.places_empty).setVisibility(8);
        this.A0W.setVisibility(8);
        this.A0M.setVisibility(8);
        this.A0g = new C36011jB();
        A0D();
        C625037m r2 = new C625037m(location, this, str, i, z);
        this.A0d = r2;
        this.A1Q.Aaz(r2, new Void[0]);
    }

    public void A0K(Bundle bundle) {
        bundle.putParcelable("places", this.A0g);
        bundle.putBoolean("show_live_location_setting", this.A0s);
        bundle.putBoolean("fullscreen", this.A0n);
        bundle.putBoolean("zoom_to_user", this.A0u);
    }

    public void A0L(Bundle bundle, ActivityC000800j r28) {
        AbstractC14640lm r0;
        LocationManager A0F;
        AbstractC14640lm r02;
        UserJid nullable;
        this.A0X = r28;
        View inflate = r28.getLayoutInflater().inflate(R.layout.location_picker, (ViewGroup) null, false);
        ((TextView) inflate.findViewById(R.id.duration_15_min)).setText(r28.getString(R.string.live_location_share_15_minutes, 15));
        ((TextView) inflate.findViewById(R.id.duration_60_min)).setText(r28.getString(R.string.live_location_share_1_hour, 1));
        ((TextView) inflate.findViewById(R.id.duration_480_min)).setText(r28.getString(R.string.live_location_share_8_hours, 8));
        r28.setContentView(inflate);
        if (!this.A1M.A03()) {
            Log.i("aborting due to native libraries missing");
        } else {
            C15570nT r2 = this.A0z;
            r2.A08();
            if (r2.A00 != null) {
                if (bundle != null) {
                    this.A0g = (C36011jB) bundle.getParcelable("places");
                    this.A0s = bundle.getBoolean("show_live_location_setting", false);
                    bundle.remove("places");
                    this.A0n = bundle.getBoolean("fullscreen", false);
                    this.A0u = bundle.getBoolean("zoom_to_user", false);
                }
                this.A0Z = AbstractC14640lm.A01(this.A0X.getIntent().getStringExtra("jid"));
                this.A0o = this.A0X.getIntent().getBooleanExtra("live_location_mode", false);
                boolean z = false;
                if ((!this.A1F.A07(1506) || (r02 = this.A0Z) == null || (nullable = UserJid.getNullable(r02.getRawString())) == null || !new C38301nr(this.A13, nullable).A03()) && (r0 = this.A0Z) != null && !C15380n4.A0F(r0)) {
                    r2.A08();
                    z = true;
                }
                this.A0m = z;
                if (r28.getIntent() != null) {
                    this.A0t = r28.getIntent().getBooleanExtra("sticker_mode", false);
                }
                this.A0Q = this.A0X.findViewById(R.id.main);
                Toolbar toolbar = (Toolbar) r28.findViewById(R.id.toolbar);
                r28.A1e(toolbar);
                AbstractC005102i A1U = r28.A1U();
                A1U.A0M(true);
                boolean z2 = this.A0t;
                int i = R.string.send_location;
                if (z2) {
                    i = R.string.select_location;
                }
                A1U.A0A(i);
                View findViewById = r28.findViewById(R.id.search_holder);
                findViewById.addOnLayoutChangeListener(new View.OnLayoutChangeListener(findViewById, new ViewTreeObserver$OnGlobalLayoutListenerC101714ny(this)) { // from class: X.4mw
                    public final /* synthetic */ View A00;
                    public final /* synthetic */ ViewTreeObserver.OnGlobalLayoutListener A01;

                    {
                        this.A00 = r1;
                        this.A01 = r2;
                    }

                    @Override // android.view.View.OnLayoutChangeListener
                    public final void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
                        View view2 = this.A00;
                        ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = this.A01;
                        int visibility = view2.getVisibility();
                        ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                        if (visibility == 0) {
                            viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener);
                        } else {
                            viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener);
                        }
                    }
                });
                AnonymousClass018 r9 = this.A1A;
                this.A0Y = new C48232Fc(r28, findViewById, new C103794rK(this), toolbar, r9);
                View findViewById2 = r28.findViewById(R.id.map_frame);
                this.A0L = findViewById2;
                if (findViewById2 != null) {
                    findViewById2.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101614no(this));
                }
                this.A0O = r28.findViewById(R.id.picker_list);
                this.A0P = r28.findViewById(R.id.places_holder);
                this.A0E = r28.findViewById(R.id.map_center);
                View findViewById3 = r28.findViewById(R.id.map_center_pin);
                this.A0D = findViewById3;
                findViewById3.setContentDescription(r28.getString(R.string.current_location_marker_content_description));
                this.A0C = r28.findViewById(R.id.map_center_filler);
                View A05 = AnonymousClass00T.A05(r28, R.id.map_center_info);
                this.A0B = A05;
                A05.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 34));
                View findViewById4 = this.A0X.findViewById(R.id.send_my_location_btn);
                this.A0F = findViewById4;
                findViewById4.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 33));
                View findViewById5 = this.A0X.findViewById(R.id.live_location_btn);
                this.A0H = findViewById5;
                r2.A08();
                findViewById5.setVisibility(0);
                this.A0H.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 35));
                ImageView imageView = (ImageView) AnonymousClass00T.A05(r28, R.id.full_screen);
                this.A0R = imageView;
                imageView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 36));
                Handler handler = new Handler(Looper.getMainLooper());
                this.A08 = handler;
                RunnableBRunnable0Shape7S0100000_I0_7 runnableBRunnable0Shape7S0100000_I0_7 = new RunnableBRunnable0Shape7S0100000_I0_7(this, 37);
                this.A0j = runnableBRunnable0Shape7S0100000_I0_7;
                if (this.A0g == null) {
                    handler.postDelayed(runnableBRunnable0Shape7S0100000_I0_7, 15000);
                }
                File file = new File(r28.getCacheDir(), "Places");
                if (!file.mkdirs() && !file.isDirectory()) {
                    Log.w("LocationPickerUI/create unable to create places directory");
                }
                C38771og r11 = new C38771og(this.A0y, this.A10, this.A1H, file, "location-picker");
                r11.A00 = this.A0X.getResources().getDimensionPixelSize(R.dimen.location_picker_ui_thumb_size);
                this.A0h = r11.A00();
                this.A0S = (ImageView) this.A0X.findViewById(R.id.my_location);
                this.A0N = this.A0X.findViewById(R.id.permissions_request);
                this.A0I = this.A0X.findViewById(R.id.live_location_setting);
                ProgressBar progressBar = (ProgressBar) AnonymousClass00T.A05(r28, R.id.progressbar_small);
                this.A0U = progressBar;
                int i2 = 8;
                if (this.A0g == null) {
                    i2 = 0;
                }
                progressBar.setVisibility(i2);
                this.A0V = (ProgressBar) r28.findViewById(R.id.progressbar_map);
                View inflate2 = View.inflate(this.A0X, R.layout.location_picker_attributions, null);
                TextView textView = (TextView) inflate2.findViewById(R.id.location_picker_attributions_textview);
                this.A0W = textView;
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                View inflate3 = View.inflate(this.A0X, R.layout.location_picker_loading, null);
                View findViewById6 = inflate3.findViewById(R.id.location_picker_loading_progress);
                this.A0M = findViewById6;
                findViewById6.setVisibility(8);
                this.A0e = new C52812bj(r28, this.A0h, true);
                this.A0T = (ListView) this.A0X.findViewById(R.id.places_list);
                if (this.A0m) {
                    this.A0T.addHeaderView(this.A0X.getLayoutInflater().inflate(R.layout.location_nearby_places_row, (ViewGroup) null), null, false);
                    View inflate4 = this.A0X.getLayoutInflater().inflate(R.layout.location_picker_this_location, (ViewGroup) null);
                    this.A0G = inflate4;
                    this.A0T.addHeaderView(inflate4, null, true);
                } else {
                    this.A0G = this.A0F;
                }
                this.A0T.setAdapter((ListAdapter) this.A0e);
                this.A0T.setFooterDividersEnabled(true);
                this.A0T.addFooterView(inflate2, null, true);
                this.A0T.addFooterView(inflate3, null, false);
                A0B();
                A0D();
                this.A0T.setOnItemClickListener(new AdapterView.OnItemClickListener(r28, this) { // from class: X.3OE
                    public final /* synthetic */ ActivityC000800j A00;
                    public final /* synthetic */ AbstractC36001jA A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.widget.AdapterView.OnItemClickListener
                    public final void onItemClick(AdapterView adapterView, View view, int i3, long j) {
                        PlaceInfo placeInfo;
                        AbstractC36001jA r22 = this.A01;
                        ActivityC000800j r1 = this.A00;
                        int headerViewsCount = i3 - r22.A0T.getHeaderViewsCount();
                        if (!r22.A0m || headerViewsCount != -1) {
                            C36011jB r12 = r22.A0g;
                            if (r12 != null && headerViewsCount < r12.A08.size()) {
                                placeInfo = (PlaceInfo) r12.A08.get(headerViewsCount);
                            } else {
                                return;
                            }
                        } else if (r22.A0n || !r22.A1J.A06(r1)) {
                            placeInfo = r22.A1K;
                        } else {
                            r22.A0A();
                            return;
                        }
                        r22.A0M(placeInfo);
                    }
                });
                C102564pL r5 = new C102564pL();
                ((RadioGroup) this.A0X.findViewById(R.id.duration)).setOnCheckedChangeListener(r5);
                ActivityC000800j r03 = this.A0X;
                C252718t r04 = this.A1P;
                AbstractC15710nm r15 = this.A0w;
                AnonymousClass19M r14 = this.A1C;
                C231510o r13 = this.A1D;
                AnonymousClass01d r3 = this.A15;
                this.A0a = new C36271jb(r03, this.A0Q, r15, r3, this.A19, r9, r14, r13, this.A1E, this.A0Z, this.A1N, r04);
                ImageView imageView2 = (ImageView) this.A0X.findViewById(R.id.send);
                imageView2.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this.A0X, R.drawable.input_send), r9));
                imageView2.setOnClickListener(new ViewOnClickCListenerShape2S0300000_I0(this, r28, r5, 4));
                Bitmap bitmap = null;
                View inflate5 = View.inflate(this.A0X, R.layout.contact_photo_marker, null);
                r2.A08();
                C27621Ig r92 = r2.A01;
                if (r92 != null && (bitmap = this.A14.A01(r28, r92, this.A0X.getResources().getDimension(R.dimen.small_avatar_radius), this.A0X.getResources().getDimensionPixelSize(R.dimen.small_avatar_size))) == null) {
                    AnonymousClass130 r1 = this.A12;
                    bitmap = r1.A03(r28, r1.A01(r92));
                }
                ((ImageView) inflate5.findViewById(R.id.contact_photo)).setImageBitmap(bitmap);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                inflate5.measure(makeMeasureSpec, makeMeasureSpec);
                int measuredWidth = inflate5.getMeasuredWidth();
                int measuredHeight = inflate5.getMeasuredHeight();
                this.A05 = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
                inflate5.layout(0, 0, measuredWidth, measuredHeight);
                inflate5.draw(new Canvas(this.A05));
                this.A0A = r28.findViewById(R.id.bottom_sheet);
                this.A0K = r28.findViewById(R.id.map_center_frame);
                View view = this.A0A;
                if (view != null) {
                    view.setVisibility(0);
                    this.A0b = new C92254Vd(r28.getResources(), this.A0A, new AnonymousClass5UM() { // from class: X.57U
                        @Override // X.AnonymousClass5UM
                        public final void ATC(float f) {
                            AbstractC36001jA r32 = AbstractC36001jA.this;
                            int i3 = (int) f;
                            r32.A00 = i3;
                            int max = Math.max(i3, r32.A02);
                            r32.A0K.setPadding(0, 0, 0, max);
                            r32.A0K.requestLayout();
                            r32.A0F(max);
                        }
                    });
                } else {
                    this.A0a.A07.setMaxLines(2);
                }
                View findViewById7 = r28.findViewById(R.id.live_location_sheet);
                this.A0J = findViewById7;
                if (findViewById7 != null) {
                    findViewById7.setVisibility(8);
                }
                if (bundle == null && this.A18.A03() && (A0F = r3.A0F()) != null && !A0F.isProviderEnabled("gps") && !A0F.isProviderEnabled("network")) {
                    C36021jC.A01(this.A0X, 2);
                }
                HandlerThread handlerThread = new HandlerThread("GeoCode");
                this.A09 = handlerThread;
                handlerThread.start();
                this.A07 = new Handler(this.A09.getLooper());
                ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0 = new ViewOnClickCListenerShape4S0200000_I0(r28, 23, this);
                AnonymousClass00T.A05(r28, R.id.button_open_permission_settings).setOnClickListener(viewOnClickCListenerShape4S0200000_I0);
                View findViewById8 = r28.findViewById(R.id.button_open_permission_settings_minimized);
                if (findViewById8 != null) {
                    findViewById8.setOnClickListener(viewOnClickCListenerShape4S0200000_I0);
                    return;
                }
                return;
            }
        }
        this.A0X.finish();
    }

    public final void A0M(PlaceInfo placeInfo) {
        if (!this.A1F.A07(332) || !this.A19.A00.getBoolean("nearby_location_new_user", true)) {
            if (this.A0t) {
                Intent intent = new Intent();
                intent.putExtra("locations_string", placeInfo.A06);
                PlaceInfo placeInfo2 = this.A1K;
                intent.putExtra("longitude", placeInfo2.A02);
                intent.putExtra("latitude", placeInfo2.A01);
                this.A0X.setResult(-1, intent);
            } else {
                long longExtra = this.A0X.getIntent().getLongExtra("quoted_message_row_id", 0);
                C15580nU A04 = C15580nU.A04(this.A0X.getIntent().getStringExtra("quoted_group_jid"));
                AbstractC15340mz r2 = null;
                if (longExtra > 0) {
                    r2 = this.A1B.A0K.A00(longExtra);
                } else if (A04 != null) {
                    r2 = C20320vZ.A00(A04, null, null, this.A16.A00());
                }
                AbstractC14640lm r12 = this.A0Z;
                if (r12 != null) {
                    C16170oZ r8 = this.A11;
                    AnonymousClass009.A05(r12);
                    boolean booleanExtra = this.A0X.getIntent().getBooleanExtra("has_number_from_url", false);
                    C20320vZ r9 = r8.A1G;
                    C14830m7 r0 = r8.A0R;
                    AnonymousClass1XO r5 = new AnonymousClass1XO(r9.A07.A02(r12, true), r0.A00());
                    r5.A0Y(1);
                    ((AnonymousClass1XP) r5).A00 = placeInfo.A01;
                    ((AnonymousClass1XP) r5).A01 = placeInfo.A02;
                    r5.A01 = placeInfo.A06;
                    r5.A00 = placeInfo.A04;
                    r5.A02 = placeInfo.A0A;
                    r9.A04(r5, r2);
                    if (booleanExtra) {
                        r5.A0T(4);
                    }
                    r8.A0M(r5);
                    C15650ng r14 = r8.A0f;
                    r14.A0Z(r5, 2);
                    AbstractC14440lR r13 = r8.A1Q;
                    AbstractC15710nm r02 = r8.A02;
                    C18790t3 r15 = r8.A0A;
                    AnonymousClass12H r122 = r8.A0n;
                    C18810t5 r11 = r8.A0z;
                    C15890o4 r10 = r8.A0T;
                    C244615p r92 = r8.A04;
                    C22830zi r7 = r8.A0o;
                    C16030oK r22 = r8.A10;
                    r13.Aaz(new AnonymousClass24M(r8.A01, r02, r92, r15, r0, r10, r14, r8.A0j, r122, r7, r11, r22, r5), new Void[0]);
                }
                this.A0X.setResult(-1);
            }
            this.A0X.finish();
            return;
        }
        this.A0i = new RunnableBRunnable0Shape5S0200000_I0_5(this, 29, placeInfo);
        C36021jC.A01(this.A0X, 4);
    }

    public final void A0N(Float f, int i, boolean z) {
        this.A0A.clearAnimation();
        A0I(A03(), f, i, z);
        C92254Vd r0 = this.A0b;
        if (r0 != null) {
            r0.A00(i, z);
        }
    }

    public void A0O(Float f, boolean z) {
        PlaceInfo placeInfo;
        Object obj;
        PlaceInfo placeInfo2;
        Object obj2;
        PlaceInfo placeInfo3;
        Object obj3;
        View view;
        int height;
        PlaceInfo placeInfo4;
        Object obj4;
        boolean z2 = this instanceof AnonymousClass32x;
        if (!z2) {
            if (((C618732w) this).A01.A03 == null) {
                return;
            }
        } else if (((AnonymousClass32x) this).A01.A02 == null) {
            return;
        }
        if (this.A0s) {
            if (this.A0Y.A05()) {
                this.A0Y.A04(true);
            }
            this.A0n = false;
            this.A0R.setVisibility(8);
            this.A0P.setVisibility(8);
        } else {
            C15890o4 r4 = this.A18;
            if (r4.A03()) {
                if (!z2) {
                    LocationPicker locationPicker = ((C618732w) this).A01;
                    if (locationPicker.A03 != null && locationPicker.A0F.A03()) {
                        locationPicker.A03.A0E(true);
                    }
                } else {
                    LocationPicker2 locationPicker2 = ((AnonymousClass32x) this).A01;
                    if (locationPicker2.A02 != null && locationPicker2.A0K.A03()) {
                        locationPicker2.A02.A0L(true);
                    }
                }
            }
            A04();
            A05();
            this.A0R.setVisibility(0);
            if (this.A0n) {
                this.A0R.setImageResource(R.drawable.btn_map_fullscreen_off);
                this.A0R.setContentDescription(this.A0X.getString(R.string.show_places_list));
                if (this.A0m) {
                    ImageView imageView = (ImageView) this.A0T.findViewById(R.id.send_current_location_icon);
                    if (imageView != null) {
                        imageView.setImageResource(R.drawable.ic_current_location);
                    }
                    TextView textView = (TextView) this.A0T.findViewById(R.id.send_current_location_text);
                    if (textView != null) {
                        textView.setText(R.string.send_this_location);
                    }
                }
                TextView textView2 = (TextView) this.A0X.findViewById(R.id.location_picker_current_location_text);
                if (this.A0t && textView2 != null) {
                    textView2.setText(R.string.select_this_location);
                }
                if (!z2) {
                    LocationPicker locationPicker3 = ((C618732w) this).A01;
                    if (!(locationPicker3.A03 == null || (placeInfo4 = locationPicker3.A0M.A0f) == null || (obj4 = placeInfo4.A0D) == null)) {
                        AnonymousClass03R r1 = (AnonymousClass03R) obj4;
                        r1.A0E(locationPicker3.A06);
                        r1.A0B();
                    }
                } else {
                    LocationPicker2 locationPicker22 = ((AnonymousClass32x) this).A01;
                    if (!(locationPicker22.A02 == null || (placeInfo3 = locationPicker22.A0S.A0f) == null || (obj3 = placeInfo3.A0D) == null)) {
                        C36311jg r12 = (C36311jg) obj3;
                        r12.A05(locationPicker22.A05);
                        r12.A04();
                    }
                }
                this.A0W.setVisibility(8);
                View view2 = this.A0A;
                View view3 = this.A0P;
                if (view2 != null) {
                    view3.setVisibility(0);
                    this.A0T.setAdapter((ListAdapter) null);
                    this.A0T.setOnScrollListener(null);
                    if (this.A0m) {
                        height = this.A0T.findViewById(R.id.nearby_places_header).getHeight() + this.A0T.findViewById(R.id.send_current_location_btn).getHeight() + this.A0H.getHeight();
                    } else {
                        height = this.A0F.getHeight();
                    }
                    this.A0O.getLayoutParams().height = height;
                    A0N(f, height, true);
                } else {
                    view3.setVisibility(8);
                }
                PlaceInfo placeInfo5 = this.A0f;
                if ((placeInfo5 == null || placeInfo5.A0D == null) && (!this.A0Y.A05() || !C252718t.A00(this.A0Q))) {
                    this.A0E.setVisibility(0);
                    this.A0X.invalidateOptionsMenu();
                }
                view = this.A0E;
                view.setVisibility(8);
                this.A0X.invalidateOptionsMenu();
            }
            if (this.A0m) {
                ImageView imageView2 = (ImageView) this.A0T.findViewById(R.id.send_current_location_icon);
                if (imageView2 != null) {
                    imageView2.setImageResource(R.drawable.btn_send_current_location);
                }
                TextView textView3 = (TextView) this.A0T.findViewById(R.id.send_current_location_text);
                if (textView3 != null) {
                    textView3.setText(R.string.send_your_current_location);
                }
                A0C();
            }
            TextView textView4 = (TextView) this.A0X.findViewById(R.id.location_picker_current_location_text);
            if (this.A0t && textView4 != null) {
                textView4.setText(R.string.select_your_current_location);
            }
            this.A0R.setImageResource(R.drawable.btn_map_fullscreen_on);
            this.A0R.setContentDescription(this.A0X.getString(R.string.hide_places_list));
            if (!z2) {
                LocationPicker locationPicker4 = ((C618732w) this).A01;
                if (!(locationPicker4.A03 == null || (placeInfo2 = locationPicker4.A0M.A0f) == null || (obj2 = placeInfo2.A0D) == null)) {
                    AnonymousClass03R r13 = (AnonymousClass03R) obj2;
                    r13.A0E(locationPicker4.A06);
                    r13.A0A();
                }
            } else {
                LocationPicker2 locationPicker23 = ((AnonymousClass32x) this).A01;
                if (!(locationPicker23.A02 == null || (placeInfo = locationPicker23.A0S.A0f) == null || (obj = placeInfo.A0D) == null)) {
                    C36311jg r14 = (C36311jg) obj;
                    r14.A05(locationPicker23.A05);
                    r14.A03();
                }
            }
            boolean A03 = r4.A03();
            View view4 = this.A0P;
            if (A03) {
                view4.setVisibility(0);
            } else {
                view4.setVisibility(8);
            }
            A0B();
            if (this.A0A != null) {
                int i = this.A04;
                if (C252718t.A00(this.A0Q)) {
                    i >>= 1;
                }
                this.A0O.getLayoutParams().height = i;
                if (r4.A03()) {
                    A0N(f, i, z);
                }
                this.A0T.setAdapter((ListAdapter) this.A0e);
                A09();
            }
        }
        this.A0E.setVisibility(8);
        view = this.A0B;
        view.setVisibility(8);
        this.A0X.invalidateOptionsMenu();
    }

    public void A0P(Float f, boolean z) {
        boolean A03 = this.A18.A03();
        LocationSharingService.A01(this.A17.A00, this.A1I);
        if (A03) {
            this.A0S.setVisibility(0);
            this.A0N.setVisibility(8);
            if (this.A0s) {
                this.A0F.setVisibility(8);
                this.A0P.setVisibility(8);
                this.A0H.setVisibility(8);
                if (this.A0J == null) {
                    this.A0I.setVisibility(0);
                }
            } else {
                this.A0P.setVisibility(0);
                boolean z2 = this.A0m;
                View view = this.A0H;
                if (z2) {
                    view.setVisibility(0);
                    this.A0F.setVisibility(8);
                } else {
                    view.setVisibility(8);
                    this.A0F.setVisibility(0);
                }
                if (this.A0J == null) {
                    this.A0I.setVisibility(8);
                }
            }
            View findViewById = this.A0X.findViewById(R.id.permissions_request_minimized);
            if (findViewById != null) {
                findViewById.setVisibility(4);
                this.A03 = (int) (((double) this.A0L.getMeasuredHeight()) * 0.66d);
                ViewGroup.LayoutParams layoutParams = this.A0N.getLayoutParams();
                int i = this.A03;
                layoutParams.height = i;
                A0N(null, i, false);
            }
            A0O(f, z);
            return;
        }
        this.A0F.setVisibility(8);
        this.A0H.setVisibility(8);
        this.A0P.setVisibility(8);
        this.A0S.setVisibility(8);
        if (this.A0J == null) {
            this.A0I.setVisibility(8);
        }
        C14820m6 r0 = this.A19;
        r0.A13(true);
        r0.A00.edit().putBoolean("nearby_location_new_user", true).apply();
        View findViewById2 = this.A0X.findViewById(R.id.permissions_request_minimized);
        this.A0E.setVisibility(8);
        boolean z3 = this.A0n;
        ImageView imageView = this.A0R;
        if (z3) {
            imageView.setImageResource(R.drawable.btn_map_fullscreen_off);
            if (findViewById2 != null) {
                findViewById2.setVisibility(0);
                if (findViewById2.getMeasuredHeight() > 0) {
                    A0N(f, findViewById2.getMeasuredHeight(), z);
                } else {
                    findViewById2.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101774o4(findViewById2, this, f));
                }
            }
            this.A0N.setVisibility(8);
            return;
        }
        imageView.setImageResource(R.drawable.btn_map_fullscreen_on);
        this.A0N.setVisibility(0);
        if (findViewById2 != null) {
            findViewById2.setVisibility(4);
            if (this.A03 > 0) {
                ViewGroup.LayoutParams layoutParams2 = this.A0N.getLayoutParams();
                int i2 = this.A03;
                layoutParams2.height = i2;
                A0N(f, i2, z);
            }
        }
    }

    public void A0Q(Object obj) {
        Iterator it = this.A0g.A08.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            PlaceInfo placeInfo = (PlaceInfo) it.next();
            if (obj.equals(placeInfo.A0D)) {
                this.A0f = placeInfo;
                break;
            }
        }
        A09();
    }

    public void A0R(String str, Object obj) {
        if (str != null) {
            Iterator it = this.A0g.A08.iterator();
            while (it.hasNext()) {
                PlaceInfo placeInfo = (PlaceInfo) it.next();
                if (obj.equals(placeInfo.A0D)) {
                    A0M(placeInfo);
                    return;
                }
            }
        }
    }

    public void A0S(boolean z) {
        if (!(this instanceof AnonymousClass32x)) {
            C618732w r4 = (C618732w) this;
            LocationPicker locationPicker = r4.A01;
            if (locationPicker.A03 != null) {
                if (locationPicker.A07 == null) {
                    r4.A04();
                }
                Location location = r4.A06;
                if (location != null) {
                    AnonymousClass03T r5 = new AnonymousClass03T(location.getLatitude(), r4.A06.getLongitude());
                    LocationPicker.A03(r5, locationPicker);
                    locationPicker.A03.A0E(false);
                    AnonymousClass0OV r1 = new AnonymousClass0OV();
                    r1.A02 = r5;
                    r1.A01 = 15.0f;
                    r1.A00 = 0.0f;
                    C06860Vj A00 = r1.A00();
                    AnonymousClass04Q r3 = locationPicker.A03;
                    C05200Oq A002 = AnonymousClass0R9.A00(A00);
                    if (z) {
                        r3.A0B(A002, r4.A00, 400);
                    } else {
                        r3.A0A(A002);
                    }
                }
            }
        } else {
            AnonymousClass32x r52 = (AnonymousClass32x) this;
            LocationPicker2 locationPicker2 = r52.A01;
            if (locationPicker2.A02 != null) {
                if (locationPicker2.A06 == null) {
                    r52.A04();
                }
                Location location2 = r52.A06;
                if (location2 != null) {
                    LatLng latLng = new LatLng(location2.getLatitude(), r52.A06.getLongitude());
                    LocationPicker2.A03(latLng, locationPicker2);
                    locationPicker2.A02.A0L(false);
                    AnonymousClass3EO r12 = new AnonymousClass3EO();
                    r12.A03 = latLng;
                    r12.A00 = 15.0f;
                    r12.A01 = 0.0f;
                    r12.A02 = 0.0f;
                    CameraPosition A003 = r12.A00();
                    C35961j4 r2 = locationPicker2.A02;
                    AnonymousClass4IX A004 = C65193Io.A00(A003);
                    if (z) {
                        r2.A0C(A004, r52.A00);
                    } else {
                        r2.A0A(A004);
                    }
                }
            }
        }
    }

    public final void A0T(boolean z) {
        ActivityC000800j r1;
        int i;
        if (this.A19.A00.getBoolean("live_location_is_new_user", true)) {
            r1 = this.A0X;
            i = 3;
        } else {
            LocationManager A0F = this.A15.A0F();
            if (A0F != null && !A0F.isProviderEnabled("gps") && !A0F.isProviderEnabled("network")) {
                r1 = this.A0X;
                i = 2;
            } else if (!this.A18.A03()) {
                this.A0s = false;
                A0P(null, false);
                return;
            } else {
                this.A0g = new C36011jB();
                this.A0s = true;
                View view = this.A0A;
                if (view != null) {
                    if (this.A0b != null) {
                        view.clearAnimation();
                        C92254Vd r5 = this.A0b;
                        if (z) {
                            View view2 = r5.A01;
                            if (view2.getVisibility() == 0) {
                                C73993h8 r3 = new C73993h8(r5);
                                r3.setAnimationListener(new C83283wz(r5));
                                r3.setDuration(350);
                                r3.setInterpolator(new AccelerateInterpolator());
                                view2.startAnimation(r3);
                            }
                        }
                        r5.A01.setVisibility(8);
                        r5.A02.ATC(0.0f);
                    }
                    this.A0J.clearAnimation();
                    if (!z || this.A0J.getVisibility() == 0) {
                        this.A0J.setVisibility(0);
                        int height = this.A0J.getHeight();
                        View view3 = this.A0J;
                        if (height == 0) {
                            view3.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101884oF(this));
                            return;
                        }
                        A0G(view3.getHeight());
                        A0S(false);
                        A0O(null, false);
                        return;
                    }
                    this.A0J.setVisibility(0);
                    A0O(null, false);
                    C74013hA r2 = new C74013hA(this);
                    r2.setDuration(400);
                    r2.setAnimationListener(new C83443xF(this));
                    r2.setInterpolator(new AccelerateInterpolator());
                    this.A0J.startAnimation(r2);
                    return;
                }
                A0S(true);
                A0P(null, true);
                return;
            }
        }
        C36021jC.A01(r1, i);
    }

    public boolean A0U(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 0) {
            this.A0X.onSearchRequested();
            return true;
        } else if (itemId == 1) {
            this.A0l = false;
            A0J(A03(), null, A01(), false);
            return true;
        } else if (itemId != 16908332) {
            return false;
        } else {
            if (this.A0s) {
                this.A0a.A05.dismiss();
                A08();
                return true;
            }
            this.A0X.finish();
            return true;
        }
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        int i;
        if (AnonymousClass13N.A03(location, this.A06)) {
            boolean z = true;
            if (location.hasAccuracy()) {
                i = Math.max(1, (int) location.getAccuracy());
            } else {
                i = -1;
            }
            if (i != this.A01) {
                this.A01 = i;
            }
            A0C();
            C36011jB r0 = this.A0g;
            if (r0 == null || r0.A00() == null || !this.A0l || location.getAccuracy() >= 200.0f || this.A0g.A00().distanceTo(location) <= 1000.0f) {
                z = false;
            } else {
                this.A0l = false;
            }
            this.A06 = location;
            if (this.A0g != null && !z) {
                return;
            }
            if ((location.getAccuracy() < 200.0f && location.getTime() + 60000 > System.currentTimeMillis()) || this.A0q) {
                this.A0y.A0H(new RunnableBRunnable0Shape0S0210000_I0(this, location, 15, z));
            }
        }
    }
}
